<%-- 

Pagina en blanco a la que se redirige, desde el formulario de identificacion, 
para ejecutar el filtro de identificacion.

Es necesaria en WebSphere, porque alli se comprueba antes si existe la pagina, 
lanzando un error 404.

--%>