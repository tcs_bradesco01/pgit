<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>

<br:brDataTable id="tabela" var="filtroVar"
	value="#{filtroBean.listaAgConta}" width="50%" align="left"
	style="text-align:center">
	<br:brColumn>
		<f:facet name="header">
			<br:brOutputTextBold value="#{msgs.label_agencia}" />
		</f:facet>
		<br:brOutputText style="text-align:center"
			value="#{filtroVar.agencia}" />
	</br:brColumn>
	<br:brColumn>
		<f:facet name="header">
			<br:brOutputTextBold value="#{msgs.label_conta}" />
		</f:facet>
		<br:brOutputText style="text-align:center"
			value="#{filtroVar.conta} - #{filtroVar.digito} " />
	</br:brColumn>
</br:brDataTable>

<h:form id="filtroForm">
	<br:brPanelGrid columns="2">
	   <br:brPanelGroup>
		<br:brOutputText value="#{msgs.filtro_agencia} " />
		<br:brInputText value="#{filtroBean.agencia}" maxlength="4" size="8"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
		</br:brPanelGroup>
		<br:brPanelGroup>
		<br:brOutputText value="#{msgs.filtro_conta} " />
		<br:brInputText value="#{filtroBean.conta}" maxlength="7" size="8"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brOutputText value="#{msgs.filtro_digito} " />
			<br:brInputText value="#{filtroBean.digito}" maxlength="1" size="2" />
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brCommandButton id="b_inserir"
				actionListener="#{filtroBean.processarEventos}" value="#{msgs.filtro_botao_inserir}" />
			<br:brCommandButton id="b_delete"
				actionListener="#{filtroBean.processarEventos}" value="#{msgs.filtro_botao_apagar}" />
		</br:brPanelGroup>
	</br:brPanelGrid>
</h:form>
