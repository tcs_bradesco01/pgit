<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="pesqArqRemessaRecebidoForm" name="pesqArqRemessaRecebidoForm" >
<h:inputHidden id="hiddenRadio" value="#{cmpi0001Bean.codListaRemessaRadio}"/>
<h:inputHidden id="hiddenRadioFiltro" value="#{cmpi0001Bean.filtro}"/>
<h:inputHidden id="hiddenRadioValidaCPF" value="#{cmpi0001Bean.validaCpf}"/>
<h:inputHidden id="hiddenRadioValidaCnpj" value="#{cmpi0001Bean.validaCnpj}"/>
<h:inputHidden id="hiddenRadioFiltroArgumento" value="#{cmpi0001Bean.cmpi0000Bean.hiddenRadioFiltroArgumento}"/>
<h:inputHidden id="hiddenCPFCNPJ" value="#{cmpi0001Bean.cmpi0000Bean.cpf}"/>
<h:inputHidden id="hiddenObrigatoriedade" value="#{cmpi0001Bean.obrigatoriedade}"/>
<br:brPanelGrid columns="1" width="750"  cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid styleClass="mainPanel" style="margin-top:9px" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
	    		<t:selectOneRadio value="#{cmpi0001Bean.identificacao}" onclick="javascript:habilitarCamposFiltroIdenticacaoCliente(document.forms[1], this);" id="radioFiltro" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<f:selectItem itemValue="3" itemLabel="" />
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

     <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="0" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" onkeyup="proximoCampo(9,'pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCnpj1','pesqArqRemessaRecebidoForm:txtCnpj2');"  onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCnpj1',numeros);"  readonly="false" value="#{cmpi0001Bean.cmpi0000Bean.cnpj1}" size="12" maxlength="9" id="txtCnpj1">
				    </br:brInputText>	
				</br:brPanelGroup>									    
  	
			    <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
	
			    <br:brPanelGroup>	
				    <br:brInputText styleClass="HtmlInputTextBradesco" onkeyup="proximoCampo(4,'pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCnpj2','pesqArqRemessaRecebidoForm:txtCnpj3');"  onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCnpj2',numeros);" readonly="false" value="#{cmpi0001Bean.cmpi0000Bean.cnpj2}" size="6" maxlength="4" id="txtCnpj2">
				    </br:brInputText>	
   				</br:brPanelGroup>					

    		    <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGroup>						
				    <br:brInputText styleClass="HtmlInputTextBradesco"  onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCnpj3',numeros);" readonly="false" value="#{cmpi0001Bean.cmpi0000Bean.cnpj3}" size="4" maxlength="2" id="txtCnpj3">
				    </br:brInputText>					    
				</br:brPanelGroup>		
							
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="1" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brInputText styleClass="HtmlInputTextBradesco"  value="#{cmpi0001Bean.cmpi0000Bean.cpf1}" size="12" maxlength="9" onkeyup="proximoCampo(9,'pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCpf1','pesqArqRemessaRecebidoForm:txtCpf2');"  onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCpf1',numeros);" id="txtCpf1" readonly="true" />
				</br:brPanelGroup>								
				 <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>		
				<br:brPanelGroup>
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{cmpi0001Bean.cmpi0000Bean.cpf2}" size="4"   maxlength="2" id="txtCpf2" onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCpf2',numeros);" readonly="true" />
				</br:brPanelGroup>				
				
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="2" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nome_razao_social}:"/>
				</br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-right:20px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>				
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" readonly="true" value="#{cmpi0001Bean.cmpi0000Bean.nomeRazaoIdentificaCliente}" size="50" maxlength="70" id="txtNomeRazao" style="margin-right:20px"/>  
				</br:brPanelGroup>					
			</br:brPanelGrid>			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				</br:brPanelGroup>	
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_nascimento_fundacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>				
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		    
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>		    					

				<br:brPanelGroup>
					 <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" readonly="true" onkeyup="proximoCampo(2,'pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtDiaNasc','pesqArqRemessaRecebidoForm:txtMesNasc');" value="#{cmpi0001Bean.cmpi0000Bean.diaNasc}"  size="4" maxlength="2"  onblur="validarData('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtDiaNasc', 'dia');" onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtDiaNasc',numeros);"  id="txtDiaNasc">
				    </br:brInputText>	
				</br:brPanelGroup>									    
  	
			    <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<f:verbatim>/</f:verbatim>
					</br:brPanelGroup>
				</br:brPanelGrid>	
	
			    <br:brPanelGroup>	
				    <br:brInputText styleClass="HtmlInputTextBradesco" readonly="true" onkeyup="proximoCampo(2,'pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtMesNasc','pesqArqRemessaRecebidoForm:txtAnosNasc');" value="#{cmpi0001Bean.cmpi0000Bean.mesNasc}" size="4" maxlength="2"  onblur="validarData('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtMesNasc', 'mes');" onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtMesNasc',numeros);" id="txtMesNasc">
				    </br:brInputText>	
   				</br:brPanelGroup>					

    		    <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<f:verbatim>/</f:verbatim>
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGroup>						
				    <br:brInputText styleClass="HtmlInputTextBradesco" readonly="true" value="#{cmpi0001Bean.cmpi0000Bean.anoNasc}" size="8" maxlength="4"  onblur="validarData('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtAnosNasc', 'ano');" onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtAnosNasc',numeros);" id="txtAnosNasc">
				    </br:brInputText>	
				</br:brPanelGroup>		
							
			</br:brPanelGrid>
				</br:brPanelGroup>					
			</br:brPanelGrid>			
			
		</br:brPanelGroup>		
		
	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
  <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="3" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0000_label_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" readonly="true" onkeyup="proximoCampo(3,'pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtBanco','pesqArqRemessaRecebidoForm:txtAgencia');"  onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtBanco',numeros);" value="#{cmpi0001Bean.cmpi0000Bean.bancoIdentificacaoCliente}" size="4" maxlength="3" id="txtBanco" style="margin-right:20px"/>  
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0000_label_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" readonly="true" onkeyup="proximoCampo(5,'pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtAgencia','pesqArqRemessaRecebidoForm:txtConta');"  onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtAgencia',numeros);" value="#{cmpi0001Bean.cmpi0000Bean.agenciaIdentificacaoCliente}" size="6" maxlength="5" id="txtAgencia" />  
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>				
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0000_label_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" readonly="true" value="#{cmpi0001Bean.cmpi0000Bean.contaIdentificacaoCliente}" onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtConta',numeros);" size="14" maxlength="13" id="txtConta" />  
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>				
		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnConsultarClientes" styleClass="HtmlCommandButtonBradesco" value="#{msgs.label_botao_consultar}" onclick="javascript:validarCamposComValorZero(pesqArqRemessaRecebidoForm);" action="#{cmpi0001Bean.consultarCliente}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="750" border="0">
		<br:brPanelGroup style="width:240px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{cmpi0001Bean.cmpi0000Bean.tela == 'REMESSA'}" value="#{cmpi0001Bean.cmpi0000Bean.cpf}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{cmpi0001Bean.cmpi0000Bean.tela != 'REMESSA'}" value=""/>
		</br:brPanelGroup>		
		<br:brPanelGroup style="width:510px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{cmpi0001Bean.cmpi0000Bean.tela == 'REMESSA'}" value="#{cmpi0001Bean.cmpi0000Bean.nomeRazaoFundacao}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{cmpi0001Bean.cmpi0000Bean.tela != 'REMESSA'}" value=""/>
		</br:brPanelGroup>		
		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_remessa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_recepcao}:" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_servico}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px" >
		</br:brPanelGroup>			
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_perfil}:"/>
		</br:brPanelGroup>	
		<%--<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.label_de}" />
			
			 <app:calendar id="calendarioDe" value="#{cmpi0001Bean.dataDeFiltro}"  inputClass="HtmlInputTextBradesco">
			 </app:calendar>
			
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_ate}"/>
			
			 <app:calendar id="calendarioAte" value="#{cmpi0001Bean.dataAteFiltro}" inputClass="HtmlInputTextBradesco">
			 </app:calendar>
			
		</br:brPanelGroup>--%>
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.label_de}" />
			
			 <app:calendar id="calendarioDe" value="#{cmpi0001Bean.dataDeFiltro}" styleClass="HtmlInputTextBradesco">
			 </app:calendar>
			
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_ate}"/>
			
			 <app:calendar id="calendarioAte" value="#{cmpi0001Bean.dataAteFiltro}" styleClass="HtmlInputTextBradesco">
			 </app:calendar>
			
		</br:brPanelGroup>

		<br:brPanelGroup style="width:20px" >
		</br:brPanelGroup>

		<br:brPanelGroup>
		<br:brSelectOneMenu id="produto" onchange="javascript:habilitarCaixasDeTextoSequenciaCoEmpresa();" value="#{cmpi0001Bean.produtoFiltro}" disabled="#{cmpi0001Bean.cmpi0000Bean.desabServico}" >
				<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
				<f:selectItems value="#{cmpi0001Bean.listaProduto}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px" >
		</br:brPanelGroup>			
		
	    <br:brPanelGroup>	
	    	<br:brInputText value="" size="15" maxlength="9" id="txtPerfil" value="#{cmpi0001Bean.perfilFiltro}" onkeyup="habilitaCaixaDePesquisaSequenciaRemessa();" onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtPerfil',numeros);" readonly="#{cmpi0001Bean.desabPerfil}"  >  
		    	<brArq:commonsValidator type="integer" arg="#{msgs.check_perfil}" server="false" client="true" />
		    </br:brInputText>	
		</br:brPanelGroup>	
								
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sequencia_remessa_lote}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-bottom:5px" >
		</br:brPanelGroup>		
			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_processamento}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_resultado_processamento}:"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brInputText value="" size="15" maxlength="9" id="txtSeqRemessa" onkeypress="aplicamascara('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtSeqRemessa',numeros);" value="#{cmpi0001Bean.sequenciaFiltro}" readonly="#{cmpi0001Bean.desabTextRemessa}">  
				<brArq:commonsValidator type="integer" arg="#{msgs.label_sequencia_remessa_lote}" server="false" client="true" />
			</br:brInputText>
		</br:brPanelGroup>
		
		<br:brPanelGroup >
		</br:brPanelGroup>			
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="processamento" value="#{cmpi0001Bean.processamentoFiltro}" onchange="javascript:habilitarComboSitProc();" disabled="#{cmpi0001Bean.desabSitProc}">
				<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.label_combo_processamento_AguardandoProcessamento}"/>
				<f:selectItem itemValue="2" itemLabel="#{msgs.label_combo_processamento_EmProcessamento}"/>
				<f:selectItem itemValue="3" itemLabel="#{msgs.label_combo_processamento_Processado}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px" >
		</br:brPanelGroup>			
		
		<br:brPanelGroup>
			<f:verbatim><div id="comboDesabilitado" style="display:block;"></f:verbatim> 
						<br:brSelectOneMenu id="resultadoProcessamentoHabilitado"  value="" disabled="true">
							<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_combo_resultadoProcessamento_Rejeitada}"/>
							<f:selectItem itemValue="2" itemLabel="#{msgs.label_combo_resultadoProcessamento_Acolhida}"/>
							<f:selectItem itemValue="3" itemLabel="#{msgs.label_combo_resultadoProcessamento_AcolhidaParcial}"/>							
						</br:brSelectOneMenu>
			<f:verbatim></div></f:verbatim> 
			<f:verbatim><div id="comboHabilitado" style="display:none;"></f:verbatim> 
						<br:brSelectOneMenu id="resultadoProcessamentoOculto"  value="#{cmpi0001Bean.resultadoProcessamentoFiltro}">
							<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_combo_resultadoProcessamento_Rejeitada}"/>
							<f:selectItem itemValue="2" itemLabel="#{msgs.label_combo_resultadoProcessamento_Acolhida}"/>
							<f:selectItem itemValue="3" itemLabel="#{msgs.label_combo_resultadoProcessamento_AcolhidaParcial}"/>
						</br:brSelectOneMenu>
			<f:verbatim></div></f:verbatim> 
		</br:brPanelGroup>				
		
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnConsultar" disabled="true" onclick="javascript:validarCamposComValorZero2();" styleClass="HtmlCommandButtonBradesco" value="#{msgs.label_botao_consultar}" action="#{cmpi0001Bean.carregaLista}" disabled="#{cmpi0001Bean.cmpi0000Bean.desabBtnConsultarRem}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
		<f:verbatim> <div id="rolagem" style="width:750px; overflow-x:scroll"></f:verbatim> 
			<t:dataTable id="dataTable" value="#{cmpi0001Bean.listaGridRemessa}" var="result" rows="10" 
						 cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
						 columnClasses="alinhamento_centro, alinhamento_direita, alinhamento_esquerda, alinhamento_esquerda, alinhamento_direita, alinhamento_direita, alinhamento_direita, alinhamento_esquerda,alinhamento_esquerda,alinhamento_direita, alinhamento_direita"
						 headerClass="tabela_celula_destaque_acentuado" width="1530px">
			<t:column width="30px"  style="padding-right:5px; padding-left:5px">
				<f:facet name="header">
			      <br:brOutputText value=""  escape="false" />
			    </f:facet>		
				<t:selectOneRadio onclick="javascript:habilitarBotaoLoteEOcorrencia(document.forms[1], this,'pesqArqRemessaRecebidoForm:btnPesquisarLotes', 'pesqArqRemessaRecebidoForm:btnPesquisarOcorrenciaRemessa', '#{result.habilitaBtnLote}', '#{result.habilitaBtnOcorrencia}');" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
					<f:selectItems value="#{cmpi0001Bean.listaControleRemessa}"/>
				</t:selectOneRadio>
		    	<t:radio for="sor" index="#{result.totalRegistros}" />
			</t:column>
			  <t:column width="140px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_cpf_cnpj}" />
			    </f:facet>
			    <br:brOutputText value="#{result.cpf_completo}" />
			  </t:column>
			  <t:column width="350px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.label_nome_razao_social}" />
			    </f:facet>
			    <br:brOutputText value="#{result.nome}" />
			  </t:column>
			  <t:column width="270px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_servico}" />
			    </f:facet>
			    <br:brOutputText value="#{result.produto}" />
			  </t:column>
			  <t:column width="70px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_perfil}" />
			    </f:facet>
			    <br:brOutputText value="#{result.perfil}" />
			  </t:column>
			  <t:column width="70px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_title_remessa}" />
			    </f:facet>
			    <br:brOutputText value="#{result.remessa}" />
			  </t:column>
			  <t:column width="150px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_data_hora_recepcao}" />
			    </f:facet>
			    <br:brOutputText value="#{result.dataRecepcao}" />
			  </t:column>
			  <t:column width="250px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_processamento}" />
			    </f:facet>			   
				<br:brOutputText value="#{result.situacaoProcessamento}" />
			  </t:column>
			  <t:column width="200px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_resultado_processamento}" />
			    </f:facet>
				<br:brOutputText value="#{result.resultadoProcessamento}" />
			  </t:column>
			  <t:column width="80px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_quantidade_registros}" />
			    </f:facet>
			    <br:brOutputText value="#{result.qtdRegistros}" />
			  </t:column>
			  <t:column width="110px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_valor_total}" />
			    </f:facet>
			    <br:brOutputText value="#{result.valorTotal}" />
			  </t:column>
			</t:dataTable>		
			<f:verbatim> </div> </f:verbatim>	
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" rendered="#{cmpi0001Bean.listaGridRemessa!= null && cmpi0001Bean.mostraBotoes0001}">	
		<br:brPanelGroup>
			
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{cmpi0001Bean.pesquisarArquivoRecebido}">

                <f:facet name="previous">
					<brArq:pdcCommandButton id="anterior" styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"	value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
				</f:facet>
				<f:facet name="next">
					<brArq:pdcCommandButton id="proxima" styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
				</f:facet>	

           </brArq:pdcDataScroller>
			 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	<f:verbatim> <br> </f:verbatim> 
 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimpar" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.label_botao_limpar}" action="#{cmpi0001Bean.limpar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnPesquisarLotes" styleClass="HtmlCommandButtonBradesco"  style="margin-right:5px" value="#{msgs.label_botao_consultar_lotes}"  action="#{cmpi0001Bean.consultarLotes}" disabled="true">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnPesquisarOcorrenciaRemessa" styleClass="HtmlCommandButtonBradesco" value="#{msgs.label_botao_ocorrencias_remessa}"  action="#{cmpi0001Bean.consultarOcorrenciasRemessa}" disabled="true">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>


	
	<f:verbatim>
		<script language="javascript">
			if(document.forms['pesqArqRemessaRecebidoForm'] != null){	
				if(document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtCnpj1'] != null ){		
					document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtCnpj1'].focus();
				
				}	
			}	
	
		</script>
	</f:verbatim>	
	
	<brArq:validatorScript functionName="validateForm"/>
	
	<f:verbatim> 	

	<script language="javascript">

		function validarCamposComValorZero(form){
		
			for(i=0; i<form.elements.length; i++){
				
				if(form.elements[i].name == 'radioFiltro'){
					
					
					if ((form.elements[i].value == 0) && (form.elements[i].checked)) {
					
						document.forms ['pesqArqRemessaRecebidoForm'].elements ['pesqArqRemessaRecebidoForm:hiddenRadioFiltro'].value = form.elements[i].value;
					
						if((validarValorVazio('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCnpj1', 'CNPJ')) || (validarValorMenorZero('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCnpj1', 'CNPJ'))){
							return;
						}
						
						if((validarValorVazio('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCnpj2', 'FILIAL')) || (validarValorMenorZero('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCnpj2', 'FILIAL'))){
							return;
						}
						
						if(validarValorVazio('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCnpj3', 'CONTROLE DO CNPJ')){
							return;
						}
					}
					
					if ((form.elements[i].value == 1) && (form.elements[i].checked)) {
						
						document.forms ['pesqArqRemessaRecebidoForm'].elements ['pesqArqRemessaRecebidoForm:hiddenRadioFiltro'].value = form.elements[i].value;
						
						if((validarValorVazio('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCpf1', 'CPF')) || (validarValorMenorZero('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCpf1', 'CPF'))){
							return;
						}
						
						if(validarValorVazio('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtCpf2', 'CONTROLE DO CPF')){
							return;
						}	
					}
					
					if ((form.elements[i].value == 2) && (form.elements[i].checked)) {
						
						document.forms ['pesqArqRemessaRecebidoForm'].elements ['pesqArqRemessaRecebidoForm:hiddenRadioFiltro'].value = form.elements[i].value;
						
						if(validarValorVazio('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtNomeRazao', 'NOME / RAZAO SOCIAL')){
							return;
						}
					}
					
					if ((form.elements[i].value == 3) && (form.elements[i].checked)) {
						
						document.forms ['pesqArqRemessaRecebidoForm'].elements ['pesqArqRemessaRecebidoForm:hiddenRadioFiltro'].value = form.elements[i].value;
						
						if((validarValorVazio('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtBanco', 'BANCO')) || (validarValorMenorZero('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtBanco', 'BANCO'))){
							return;
						}

						if((validarValorVazio('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtAgencia', 'AGENCIA')) || (validarValorMenorZero('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtAgencia', 'AGENCIA'))){
							return;
						}
						
						if((validarValorVazio('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtConta', 'CONTA')) || (validarValorMenorZero('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtConta', 'CONTA'))){
							return;
						}
						
					}					
				}
			}
		}
		
		
		function validarCamposComValorZero2(){
			if((!document.forms ['pesqArqRemessaRecebidoForm'].elements ['pesqArqRemessaRecebidoForm:txtPerfil'].value == '') && (validarValorIgualAZero('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtPerfil', 'PERFIL'))){
				return;
			}
			
			if((!document.forms ['pesqArqRemessaRecebidoForm'].elements ['pesqArqRemessaRecebidoForm:txtSeqRemessa'].value == '') && validarValorIgualAZero('pesqArqRemessaRecebidoForm','pesqArqRemessaRecebidoForm:txtSeqRemessa', 'SEQUENCIA')){
				return;
			}	
		}
	
		function validarValorMenorZero(sform, scampo, fieldName){
			
			var valor = parseFloat(document.forms [sform].elements [scampo].value);
			
			if(valor <= 0){
				document.forms [sform].elements [scampo].value = '';
				document.forms [sform].elements [scampo].focus();
				alert(fieldName + ' DEVE SER INFORMADO.');		
				return true;
			}
		}
		
		function validarValorVazio(sform, scampo, fieldName){
			
			var valor = document.forms [sform].elements [scampo].value;
			
			if(valor == null || valor == ''){
				document.forms [sform].elements [scampo].value = '';
				document.forms [sform].elements [scampo].focus();
				alert(fieldName + ' DEVE SER INFORMADO.');		
				return true;
			}
		}
			
		habilitarComboSitProc();
		
	</script>
	
	
	</f:verbatim>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
