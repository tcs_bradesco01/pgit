<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="pesqEstatiscasLoteForm">
<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0" border="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.cmpi0002_cadastro}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_cpfcnpj}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">
	    	<f:verbatim>&nbsp;</f:verbatim>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.cpf}"/>	
		</br:brPanelGroup>		
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_nomerazaosocial}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20pxs">	
		    <f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.nomeRazao}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_servico}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">
		    <f:verbatim>&nbsp;</f:verbatim>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.produto}"/>	
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_perfil}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.perfil}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="10" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_banco}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.banco}"/>	
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_agencia}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.agencia}"/>	
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_razao}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.razao}"/>	
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_conta}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.conta}"/>	
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_codigodolancamento}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.codigoLancamento}"/>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_canaldetransmissao}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup>
	    	<f:verbatim>&nbsp;</f:verbatim>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.canalTransmissao}"/>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="750px" cellpadding="0" cellspacing="0" >
		<f:verbatim><hr class="lin"> </f:verbatim>
	</br:brPanelGrid>
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.cmpi0002_remessa}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="8" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_sequencia}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">
	    	<f:verbatim>&nbsp;</f:verbatim>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.sequenciaRemessa}"/>	
		</br:brPanelGroup>					
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_datahorarecepcao}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">
	    	<f:verbatim>&nbsp;</f:verbatim>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.dataHoraRecepcao}"/>	
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_ambienteprocessamento}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">
	    	<f:verbatim>&nbsp;</f:verbatim>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.ambienteProcessamento}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="6" cellpadding="0" cellspacing="0" width="750">		
		<br:brPanelGroup style="width:45px;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_layout}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px; width:10px;">
		    <f:verbatim>&nbsp;</f:verbatim>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.layout}"/>	
		</br:brPanelGroup>
		<br:brPanelGroup style="width:170px;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_situacaodeprocessamento}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px; width:100px">	
	    	<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.situacaoProcessamentoRemessa}"/> 
   		</br:brPanelGroup>		
		<br:brPanelGroup style="width:180px;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_resultprocessamento}"/>	
		</br:brPanelGroup>		
	  	<br:brPanelGroup style="margin-right: 20px; width:150px">	
	    	<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.resultadoProcessamento}"/> 
		</br:brPanelGroup>		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_quantidadederegistros}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
	    	<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.qtdRegistrosRemessa}"/>	
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0002_valortotal}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
		    <f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.valorTotalRemessa}"/>	
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="750px" cellpadding="0" cellspacing="0" >
		<f:verbatim><hr class="lin"> </f:verbatim>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_lote}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="margin-right: 20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_sequencia}:"/>
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.sequenciaLote}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="margin-right: 20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao_processamento}:"/>
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.situacaoProcessamentoLote}"  /> 
		</br:brPanelGroup>		
		<br:brPanelGroup style="margin-right: 20px">	
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_resultado_processamento}:"/>
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.resultadoProcessamentoLote}" /> 
		</br:brPanelGroup>		
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="margin-right: 20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_banco_agencia_conta}:"/>
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.bancoAgenciaConta}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup style="margin-right: 20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_modalidade_pagamento}:"/>
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.modalidadePagamentoLote}"/>
		</br:brPanelGroup>		
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="margin-right: 20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_registros}:"/>
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.qtdRegistrosLote}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup style="margin-right: 20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_total}:"/>
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cmpi0001Bean.valorTotalLote}"/>
		</br:brPanelGroup>		
		
	</br:brPanelGrid>

	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
			<br:brPanelGrid columns="4" width="730px" border="0" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup style="width:140px;">
					<f:verbatim>&nbsp;</f:verbatim>
				</br:brPanelGroup>
				<br:brPanelGroup style="width:200px; text-align: center; padding-left: 10px;">
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_registros_consistentes}" />
				</br:brPanelGroup>
				<br:brPanelGroup style="width:200px; text-align: center; padding-left: 20px;">
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_registros_inconsistentes}"/>
				</br:brPanelGroup>
				<br:brPanelGroup style="width:200px; text-align: center; padding-left: 25px;" >
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_total_geral}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>	
		
			<t:dataTable id="dataTable" value="#{cmpi0001Bean.listaGridEstatisticaLote}" var="result" rows="10" 
				cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
				columnClasses="alinhamento_esquerda, alinhamento_direita, alinhamento_direita, alinhamento_direita, alinhamento_direita, alinhamento_direita, alinhamento_direita, alinhamento_direita"
				headerClass="tabela_celula_destaque_acentuado" width="750px" border="0">
				  <t:column width="150px" style="padding-right:5px; padding-left:5px">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_instrucao_movimento}"  />
				    </f:facet>
					<br:brOutputText value="#{result.instrucaoMovimento}" />
				  </t:column>
				  <t:column width="100px" style="padding-right:5px; padding-left:5px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_quantidade}"  />
				    </f:facet>
				    <br:brOutputText value="#{result.regConsistenteQtde}" />
				  </t:column>
				  <t:column width="100px" style="padding-right:5px; padding-left:5px">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_valor}"  />
				    </f:facet>
				    <br:brOutputText value="#{result.regConsistenteValor}" />
				  </t:column>
				  <t:column width="100px" style="padding-right:5px; padding-left:5px">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_quantidade}"  />
				    </f:facet>
				    <br:brOutputText value="#{result.regInconsistenteQtde}"  />
				  </t:column>
				  <t:column width="100px" style="padding-right:5px; padding-left:5px">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_valor}"  />
				    </f:facet>
				    <br:brOutputText value="#{result.regInconsistenteValor}" />
				  </t:column>
				  <t:column width="100px" style="padding-right:5px; padding-left:5px">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_quantidade}"  />
				    </f:facet>
				    <br:brOutputText value="#{result.regTotalGeralQtde}" />
				  </t:column>
				  <t:column width="100px" style="padding-right:5px; padding-left:5px">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_valor}"  />
				    </f:facet>
				    <br:brOutputText value="#{result.regTotalGeralValor}"  />
				  </t:column>
			  </t:dataTable>						
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" rendered="#{cmpi0001Bean.listaGridEstatisticaLote!= null && cmpi0001Bean.mostraBotoes0003}">	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{cmpi0001Bean.pesquisarEstatisticaLote}">
				<f:facet name="previous">
					<brArq:pdcCommandButton id="anterior" styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"	value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
				</f:facet>
				<f:facet name="next">
					<brArq:pdcCommandButton id="proxima" styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
				</f:facet>	
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="2" width="750" border="0" style="text-align:right" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>		
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brCommandButton id="btnEstatisticasLote"  style="margin-right:5px" styleClass="HtmlCommandButtonBradesco" value="#{msgs.label_botao_inconsistentes}" action="#{cmpi0001Bean.carregaListaIncosistentes}" disabled="#{cmpi0001Bean.desabEstatisticaIncons}" >				
				<brArq:submitCheckClient/>
			</br:brCommandButton> 
			<br:brCommandButton styleClass="HtmlCommandButtonBradesco" value="#{msgs.label_botao_voltar}" action="#{cmpi0001Bean.voltarLote}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
		<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
		<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
		<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
		<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
		<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>

</br:brPanelGrid>

</brArq:form>
