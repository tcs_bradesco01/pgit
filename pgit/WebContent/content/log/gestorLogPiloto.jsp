<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<h:form>
<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="Gestor de Logs" />
					
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
<br:brPanelGrid>
	<br:brPanelGroup>
		<br:brOutputText value="void "/>
		<br:brOutputText value="updateApplicationLevel ("/>
		<br:brSelectOneMenu value="#{logBean.applicationLevel}">
			<f:selectItem itemLabel="DEBUG" itemValue="DEBUG"/>
			<f:selectItem itemLabel="INFO" itemValue="INFO"/>
			<f:selectItem itemLabel="WARN" itemValue="WARN"/>
			<f:selectItem itemLabel="ERROR" itemValue="ERROR"/>
			<f:selectItem itemLabel="FATAL" itemValue="FATAL"/>
		</br:brSelectOneMenu>
		<br:brOutputText value=" )"/>
	</br:brPanelGroup>
	<br:brPanelGroup>
		<br:brOutputText value="void "/>
		<br:brOutputText value="updateLevel ("/>
		<br:brInputText value="#{logBean.updateCategory}"/>
		<br:brOutputText value=" , "/>
		<br:brSelectOneMenu value="#{logBean.updateLevel}">
			<f:selectItem itemLabel="DEBUG" itemValue="DEBUG"/>
			<f:selectItem itemLabel="INFO" itemValue="INFO"/>
			<f:selectItem itemLabel="WARN" itemValue="WARN"/>
			<f:selectItem itemLabel="ERROR" itemValue="ERROR"/>
			<f:selectItem itemLabel="FATAL" itemValue="FATAL"/>
		</br:brSelectOneMenu>
		<br:brOutputText value=" )"/>
	</br:brPanelGroup>
	<br:brPanelGroup>
		<br:brOutputText value="void "/>
		<br:brOutputText value="business ("/>
		<br:brInputText value="#{logBean.business}"/>
		<br:brOutputText value=" ) "/>
	</br:brPanelGroup>
	<br:brPanelGroup>
		<br:brOutputText value="java.lang.String "/>
		<br:brOutputText value="showLevelForCategory ("/>
		<br:brInputText value="#{logBean.showCategory}"/>
		<br:brOutputText value=" ) "/>
		<br:brOutputTextBold value="#{logBean.showLevel}"/>
	</br:brPanelGroup>
	<br:brPanelGroup>
		<f:verbatim><br><br></f:verbatim>
		<br:brCommandButton action="nav_log" actionListener="#{logBean.atualizarLog}"
						value="CONFIRMAR"/> 
	</br:brPanelGroup>
</br:brPanelGrid>
</h:form>