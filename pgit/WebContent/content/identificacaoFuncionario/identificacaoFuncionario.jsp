<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="identificacaoFuncionarioForm" name="identificacaoFuncionarioForm" >

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0" styleClass="CorpoPagina">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{identificacaoFuncionarioBean.itemSelecionadoPesquisa}">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<a4j:support event="onclick" reRender="txtCodigoFunc,txtCodigoJuncao,txtCodigoSecao, txtCpf1, txtCpf2" action="#{identificacaoFuncionarioBean.limparCamposPesquisa}"/>				
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="0" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoFuncionario_codigo_funcionario}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{identificacaoFuncionarioBean.codigoFuncionarioFiltro}" size="13" maxlength="10" id="txtCodigoFunc" readonly="#{identificacaoFuncionarioBean.itemSelecionadoPesquisa != 0}">
			    		<brArq:commonsValidator type="integer" arg="#{msgs.identificacaoFuncionario_codigo_funcionario}" server="false" client="true" />			  
					</br:brInputText>	    		  	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="1" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoFuncionario_codigo_juncao}"/>
				</br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-right:20px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>				
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{identificacaoFuncionarioBean.codigoJuncaoFiltro}" size="30" maxlength="70" id="txtCodigoJuncao" readonly="#{identificacaoFuncionarioBean.itemSelecionadoPesquisa != 1}" style="margin-right:20px"/>  
				</br:brPanelGroup>					
			</br:brPanelGrid>		
		</br:brPanelGroup>	
		<br:brPanelGroup>		
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoFuncionario_codigo_secao}"/>
				</br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-right:20px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>				
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{identificacaoFuncionarioBean.codigoSecaoFiltro}" size="30" maxlength="70" id="txtCodigoSecao" readonly="#{identificacaoFuncionarioBean.itemSelecionadoPesquisa != 1}" style="margin-right:20px"/>  
				</br:brPanelGroup>					
			</br:brPanelGrid>			
			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<t:radio for="sor" index="2" />			
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoFuncionario_cpf_cnpj}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:5px">					
				    <br:brPanelGroup>
				    	<br:brInputText styleClass="HtmlInputTextBradesco"  value="#{identificacaoFuncionarioBean.cpfFiltro1}" size="12"
				    	 maxlength="9" onkeyup="proximoCampo(9,'identificacaoFuncionarioForm','identificacaoFuncionarioForm:txtCpf1','identificacaoFuncionarioForm:txtCpf2');" 
				    	 onkeypress="aplicamascara('identificacaoFuncionarioForm','identificacaoFuncionarioForm:txtCpf1',numeros);" id="txtCpf1"  readonly="#{identificacaoFuncionarioBean.itemSelecionadoPesquisa != 2}">
				    		<brArq:commonsValidator type="integer" arg="#{msgs.label_cpf_cnpj}" server="false" client="true" />		
						</br:brInputText>
					</br:brPanelGroup>								
					 <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
							</br:brPanelGroup>
					</br:brPanelGrid>		
					<br:brPanelGroup>
				    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{identificacaoFuncionarioBean.cpfFiltro2}" 
				    	size="4" maxlength="2" onkeypress="aplicamascara('identificacaoFuncionarioForm','identificacaoFuncionarioForm:txtCpf2',numeros);" id="txtCpf2"
				    	readonly="#{identificacaoFuncionarioBean.itemSelecionadoPesquisa != 2}" >
				    		<brArq:commonsValidator type="integer" arg="#{msgs.label_cpf_cnpj}" server="false" client="true" />		
						</br:brInputText>
					</br:brPanelGroup>				
				</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>	 	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.label_botao_limpar_dados}" disabled="false" action="#{identificacaoFuncionarioBean.limparDados}" id="btnLimparDados" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
						<br:brCommandButton styleClass="HtmlCommandButtonBradesco" value="#{msgs.label_botao_consultar}" action="#{identificacaoFuncionarioBean.carregaLista}" id="btnConsultar" disabled="false" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	 
	<f:verbatim><br/> </f:verbatim>	
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup  style="overflow-x:auto;width:820px" >	
			<app:scrollableDataTable  id="dataTable" value="#{identificacaoFuncionarioBean.listaFuncionarios}" var="result" rows="10" rowIndexVar="parametroKey"
			rowClasses="tabela_celula_normal, tabela_celula_destaque"			
			width="820px">
			 <app:scrollableColumn width="30px" styleClass="colTabCenter">
				 <f:facet name="header">
			      <br:brOutputText value=""  escape="false"  styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>	
				<t:selectOneRadio id="sor2" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{identificacaoFuncionarioBean.itemSelecionadoGrid}">  
					<f:selectItems value="#{identificacaoFuncionarioBean.listaControleRadio}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
				<t:radio for="sor2" index="#{parametroKey}" />
			  </app:scrollableColumn>
			  <app:scrollableColumn width="250px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.identificacaoFuncionario_funcionario}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.codigo}" /><f:verbatim>&nbsp; </f:verbatim>	 	
			    <br:brOutputText value="#{result.nome}" />
			  </app:scrollableColumn>
			 <app:scrollableColumn width="140px"  styleClass="colTabRight">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.identificacaoFuncionario_codigo_juncao}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.codigoJuncao}"/>
			  </app:scrollableColumn>
			  <app:scrollableColumn width="200px"  styleClass="colTabRight">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.identificacaoFuncionario_codigo_secao}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.codigoSecao}"/>
			  </app:scrollableColumn>			 	 
			  <app:scrollableColumn width="200px"  styleClass="colTabRight">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.identificacaoFuncionario_cpf_cnpj}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.cpfCnpj}"/>
			  </app:scrollableColumn>			 	 
			</app:scrollableDataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
 		
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{identificacaoFuncionarioBean.pesquisar}" rendered="#{identificacaoFuncionarioBean.listaFuncionarios!= null && identificacaoFuncionarioBean.mostraBotoes}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="HtmlCommandButtonBradesco"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
			
	<f:verbatim><hr class="lin"> </f:verbatim>
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="HtmlCommandButtonBradesco" style="align:left" value="#{msgs.botao_voltar}" action="#{identificacaoFuncionarioBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="width:505px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnLimpar" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px"  value="#{msgs.botao_limpar}" rendered="#{identificacaoFuncionarioBean.listaFuncionarios == null}" action="#{identificacaoFuncionarioBean.limparLista}" disabled="true">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnLimpar1" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.botao_limpar}" rendered="#{identificacaoFuncionarioBean.listaFuncionarios != null}" action="#{identificacaoFuncionarioBean.limparLista}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			
			<br:brCommandButton id="btnConfirmar" styleClass="HtmlCommandButtonBradesco" value="#{msgs.identificacaoFuncionario_selecionar}" action="#{identificacaoFuncionarioBean.confirmar}" disabled="#{empty identificacaoFuncionarioBean.itemSelecionadoGrid}">
				<brArq:submitCheckClient/> 
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		
	</a4j:outputPanel>
</br:brPanelGrid>

	<brArq:validatorScript functionName="validateForm" />
</brArq:form>
