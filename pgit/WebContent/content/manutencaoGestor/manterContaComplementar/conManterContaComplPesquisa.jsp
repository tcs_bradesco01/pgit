<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterContaComplPesquisa" name="conManterContaComplPesquisa">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	  	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_empresa_contrato}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{contaComplementarBean.manterContratoParticipantesBean.empresaContrato}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_numero}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.numeroContrato}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_descricao_contrato}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{contaComplementarBean.manterContratoParticipantesBean.descricaoContrato}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_situacao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{contaComplementarBean.manterContratoParticipantesBean.situacaoContrato}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_label_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<t:selectOneRadio id="radioIncluir" value="#{contaComplementarBean.manterContratoParticipantesBean.itemSelecionado}" 
				onclick="document.getElementById('conManterContaComplPesquisa:btnLimparRadioCliente').click();"
				styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
		</t:selectOneRadio>
		
		<br:brCommandButton id="btnLimparRadioCliente" styleClass="bto1"
			value=""
			action="#{contaComplementarBean.manterContratoParticipantesBean.limparPesquisaComplementar}"
			style="display:none">
			<brArq:submitCheckClient />
		</br:brCommandButton>		
		
		
		 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<t:radio for="radioIncluir" index="0" />			
			
			 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conParticipantesManterContrato_label_cnpj_only}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(9,'conManterContaComplPesquisa','conManterContaComplPesquisa:txtCnpj','conManterContaComplPesquisa:txtFilial');" style="margin-right: 5" onkeypress="onlyNum();" size="11" maxlength="9"  styleClass="HtmlInputTextBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.cnpj}" disabled="#{contaComplementarBean.manterContratoParticipantesBean.itemSelecionado != '0'}" />
				    <br:brInputText id="txtFilial" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(4,'conManterContaComplPesquisa','conManterContaComplPesquisa:txtFilial','conManterContaComplPesquisa:txtControle');" style="margin-right: 5"  onkeypress="onlyNum();" size="5" maxlength="4"  styleClass="HtmlInputTextBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.cnpj2}" disabled="#{contaComplementarBean.manterContratoParticipantesBean.itemSelecionado != '0'}"/>
					<br:brInputText id="txtControle" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" style="margin-right: 5" onkeypress="onlyNum();" size="3" maxlength="2"  styleClass="HtmlInputTextBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.cnpj3}" disabled="#{contaComplementarBean.manterContratoParticipantesBean.itemSelecionado != '0'}"/>
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
			<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
			<t:radio for="radioIncluir" index="1" />	
			
			 <a4j:outputPanel id="panelCpf" ajaxRendered="true">		
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conParticipantesManterContrato_label_cpf_only}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
				    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" style="margin-right: 5" onkeypress="onlyNum();" value="#{contaComplementarBean.manterContratoParticipantesBean.cpf}" size="11" maxlength="9" id="txtCpf" 
				    onkeyup="proximoCampo(9,'conManterContaComplPesquisa','conManterContaComplPesquisa:txtCpf','conManterContaComplPesquisa:txtControleCpf');" disabled="#{contaComplementarBean.manterContratoParticipantesBean.itemSelecionado != '1'}"/>
				    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  onkeypress="onlyNum();" value="#{contaComplementarBean.manterContratoParticipantesBean.cpf2}" maxlength="2" size="3" id="txtControleCpf" disabled="#{contaComplementarBean.manterContratoParticipantesBean.itemSelecionado != '1'}"/>
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  border="0">
			<t:radio for="radioIncluir" index="2" />			
			
			<a4j:outputPanel id="panelBanco" ajaxRendered="true">			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conParticipantesManterContrato_label_banco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" 
					value="#{contaComplementarBean.manterContratoParticipantesBean.banco}" size="4" maxlength="3" id="txtBanco" 
					onkeyup="proximoCampo(3,'conManterContaComplPesquisa','conManterContaComplPesquisa:txtBanco','conManterContaComplPesquisa:txtAgencia');" 
					disabled="#{contaComplementarBean.manterContratoParticipantesBean.itemSelecionado != '2'}"/>			
			</a4j:outputPanel>
	
			<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conParticipantesManterContrato_label_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
				<br:brInputText style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  
					value="#{contaComplementarBean.manterContratoParticipantesBean.agencia}" size="6" maxlength="5" id="txtAgencia" 
					onkeyup="proximoCampo(5,'conManterContaComplPesquisa','conManterContaComplPesquisa:txtAgencia','conManterContaComplPesquisa:txtConta');" 
					disabled="#{contaComplementarBean.manterContratoParticipantesBean.itemSelecionado != '2'}"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelConta" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conParticipantesManterContrato_label_conta}:" style="margin-right:60px"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"   onkeypress="onlyNum();" 
					value="#{contaComplementarBean.manterContratoParticipantesBean.conta}" size="17" maxlength="13" id="txtConta"  style="margin-right:5px" 
					disabled="#{contaComplementarBean.manterContratoParticipantesBean.itemSelecionado != '2'}"/>
			</a4j:outputPanel>		
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
		 		
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >		
				<br:brCommandButton id="btnLimparDados" style="margin-right:5px" styleClass="bto1" 
						value="#{msgs.conParticipantesManterContrato_btnLimparDadados}" 
						action="#{contaComplementarBean.limparDadosIncluir}" >							
					<brArq:submitCheckClient/>
				</br:brCommandButton>	
				<br:brCommandButton id="btnConsultar" styleClass="bto1" 
						value="#{msgs.conParticipantesManterContrato_btnConsultar}" 
						action="#{contaComplementarBean.carregaListaContaComplementar}" style="cursor:hand;" 
						style="cursor:hand;"
						onmouseover="javascript:alteraBotao('visualizacao', this.id);" 
						onmouseout="javascript:alteraBotao('normal', this.id);"> 
					<brArq:submitCheckClient/>
				</br:brCommandButton>			
			</br:brPanelGroup>
		</br:brPanelGrid>
				
				
	<f:verbatim><br> </f:verbatim> 
	
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">
	 
				<app:scrollableDataTable id="dataTable" value="#{contaComplementarBean.listaGridComplementar}" var="result" 
					rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
	
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>							
				    	<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
							value="#{contaComplementarBean.itemSelecionadoListaManterCtaPesquisar}">
							<f:selectItems value="#{contaComplementarBean.listaPesquisaControle}"/>
							<a4j:support event="onclick" reRender="panelBotoes" />
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />    	
					</app:scrollableColumn>
	
					<app:scrollableColumn styleClass="colTabRight" width="150px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.conManterContaComplementar_contrato_conta_pointer}" style="text-align:center;width:100%" />
						</f:facet>
						<br:brOutputText value="#{result.cdContaComplementar}"/>
					</app:scrollableColumn>
	
					<app:scrollableColumn styleClass="colTabRight" width="230px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.conManterContratosContaComplementar_cpfCnpj}" style="text-align:center;width:100%"/>
						</f:facet>
						<br:brOutputText value="#{result.cnpjOuCpfFormatado}"/>
					</app:scrollableColumn>
	 
					<app:scrollableColumn styleClass="colTabRight" width="150px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.conManterContratosContaComplementar_agencia}" style="text-align:center;width:100%"/>
						</f:facet>
						<br:brOutputText value="#{result.cdAgencia}"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="150px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.conManterContratosContaComplementar_conta}" style="text-align:center;width:100%"/>
						</f:facet>
						<br:brOutputText value="#{result.contaDigito}"/>
					</app:scrollableColumn>
					
				</app:scrollableDataTable>
							
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{contaComplementarBean.carregarPaginacaoContaComplementar}"  > 
				 <f:facet name="first">
				    <brArq:pdcCommandButton id="primeira"
				      styleClass="bto1" 
				      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido"
				      styleClass="bto1"  style="margin-left: 3px;"
				      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior"
				      styleClass="bto1"  style="margin-left: 3px;"
				      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton id="proxima"
				      styleClass="bto1"  style="margin-left: 3px;"
				      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido"
				      styleClass="bto1"  style="margin-left: 3px;"
				      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton id="ultima"
				      styleClass="bto1"  style="margin-left: 3px;"
				      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  </f:facet>
				</brArq:pdcDataScroller>	
			</br:brPanelGroup>
		</br:brPanelGrid>				
				
		<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid columns="8" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" disabled="false" styleClass="bto1" style="margin-right:5px" 
						value="#{msgs.conParticipantesManterContrato_btnVoltar}" 
						action="#{contaComplementarBean.voltarPesquisa}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>		 		
			</br:brPanelGroup>
			<br:brPanelGroup id="panelBotoes" style="text-align:right;width:600px" >
				<br:brCommandButton id="btnLimpar" 
						disabled="#{contaComplementarBean.listaGridComplementar == null}" 
						style="margin-left:5px" styleClass="bto1" 
						value="#{msgs.conParticipantesManterContrato_btnLimpar}" 
						action="#{contaComplementarBean.limparIncluirComplementar}" >							
					<brArq:submitCheckClient/>
				</br:brCommandButton>			
				<br:brCommandButton id="btnDetalhar" disabled="#{empty contaComplementarBean.itemSelecionadoListaManterCtaPesquisar}" 
						style="margin-left:5px" styleClass="bto1" 
						value="#{msgs.conParticipantesManterContrato_btnDetalhar}" 
						action="#{contaComplementarBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" style="margin-left:5px" styleClass="bto1" 
						value="#{msgs.conParticipantesManterContrato_btnIncluir}" 
						action="#{contaComplementarBean.incluir}" >							
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
				<br:brCommandButton id="btnAlterar" 
						disabled="#{empty contaComplementarBean.itemSelecionadoListaManterCtaPesquisar}" 
						style="margin-left:5px" styleClass="bto1" 
						value="#{msgs.conParticipantesManterContrato_btnAlterar}" 
						action="#{contaComplementarBean.alterar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" 
						disabled="#{empty contaComplementarBean.itemSelecionadoListaManterCtaPesquisar}" 
						style="margin-left:5px" styleClass="bto1" 
						value="#{msgs.conParticipantesManterContrato_btnExcluir}" 
						action="#{contaComplementarBean.excluir}" >							
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>				
		</br:brPanelGrid>
	</br:brPanelGrid>
</brArq:form>	