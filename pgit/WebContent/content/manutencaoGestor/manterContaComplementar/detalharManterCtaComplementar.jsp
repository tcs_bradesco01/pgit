<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detalharManterCtaComplementarForm" name="detalharManterCtaComplementarForm">
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterContratoBean.manterContratoParticipantesBean.obrigatoriedade}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_cliente_representante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_cpfcnpj}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{contaComplementarBean.manterContratoParticipantesBean.cpfCnpjRepresentante}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_nomerazaosocial}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.nomeRazaoRepresentante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_grupo_economico}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{contaComplementarBean.manterContratoParticipantesBean.grupEconRepresentante}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_atividade_economica}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.ativEconRepresentante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_segmento}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{contaComplementarBean.manterContratoParticipantesBean.segRepresentante}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_sub_segmento}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.subSegRepresentante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.gerenteResponsavelFormatado}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.nomeRazaoParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>

 	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_empresa}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{contaComplementarBean.manterContratoParticipantesBean.empresaContrato}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.tipoContrato}"/>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_numero}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.numeroContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_descricao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{contaComplementarBean.manterContratoParticipantesBean.descricaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_situacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{contaComplementarBean.manterContratoParticipantesBean.situacaoContrato}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_motivo}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.manterContratoParticipantesBean.motivoContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo_participacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{contaComplementarBean.manterContratoParticipantesBean.participacaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
<f:verbatim><hr class="lin"> </f:verbatim>

    <br:brPanelGrid styleClass="mainPanel" columns="2" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterContratosContaComplementar_conta_pointer}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.cdContaPointerIncComplementar}" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.path_manterContaComplementar_participante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

		<br:brPanelGrid columns="6" cellpadding="0" cellspacing="0" width="100%" border="0">
		
			<a4j:outputPanel id="panelCpf" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.cdCpfCGCIncComplementar}" />
					</br:brPanelGroup>
				</br:brPanelGrid>
			</a4j:outputPanel>

			<a4j:outputPanel id="panelRazao" ajaxRendered="true">

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"	styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.cdnomeRazaoSocialIncComplementar}" /> 
					</br:brPanelGroup>
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.path_manterContaComplementar_participante_conta}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid columns="6" cellpadding="0" cellspacing="0" width="100%" border="0">
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_banco}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.cdBancoIncComplementar}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>
	
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_agencia}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.cdAgenciaIncComplementar}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>
			
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_conta}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.cdContaIncComplementar}" /> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>	
			
	</br:brPanelGrid>
		
<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="6" cellpadding="0" cellspacing="0" border="0">

		<a4j:outputPanel id="panelDataHoraInclusao" ajaxRendered="true">			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_data_hora_inclusao}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.cdDataHoraInclusao}" converter="timestampPdcConverter" />
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>
	
		<a4j:outputPanel id="panelDataHoraInclusaoUsuario" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_data_hora_inclusao_usuario}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.cdDataHoraInclusaoUsuario}" />
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>

	</br:brPanelGrid>
	
<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" border="0">

		<a4j:outputPanel id="panelDataHoraManutencao" ajaxRendered="true">			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_data_hora_manutencao}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.cdDataHoraManutencao}" converter="timestampPdcConverter" />
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>
	
		<a4j:outputPanel id="panelDataHoraManutencaoUsuario" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_data_hora_inclusao_usuario}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{contaComplementarBean.cdDataHoraManutencaoUsuario}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>

	</br:brPanelGrid>	
	
<f:verbatim><hr class="lin"> </f:verbatim>
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:100%" >
			<br:brCommandButton id="btnVoltar" disabled="false" styleClass="bto1" style="margin-right:5px" 
					value="#{msgs.conParticipantesManterContrato_btnVoltar}" 
					action="#{contaComplementarBean.voltarLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		 		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brMessages showDetail="true"></br:brMessages>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	