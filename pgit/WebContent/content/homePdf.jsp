<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="brHtml"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="brApp" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>
 
<arq:form id="homePdfForm" name="homePdfForm">
	<brHtml:brPanelGrid styleClass="HtmlPanelGridBradesco">
		
		<brHtml:brCommandLink id="btoHiddenPdf" action="#{contratoPdfBean.imprimirContrato}" style="display:none">		
    		<f:param name="cdClubPessoa" value="#{param['cdClubPessoa']}"/>
    		<f:param name="cdPessoaJuridicaContrato" value="#{param['cdPessoaJuridicaContrato']}"/>
    		<f:param name="cdTipoContratoNegocio" value="#{param['cdTipoContratoNegocio']}"/>
    		<f:param name="nrSequenciaContratoNegocio" value="#{param['nrSequenciaContratoNegocio']}"/>
    		<f:param name="cdCorpoCpfCnpj" value="#{param['cdCorpoCpfCnpj']}"/>
    		<f:param name="cdControleCpfCnpj" value="#{param['cdControleCpfCnpj']}"/>
    		<f:param name="cdDigitoCpfCnpj" value="#{param['cdDigitoCpfCnpj']}"/>
    		<f:param name="cdCpfCnpjParticipante" value="#{param['cdCpfCnpjParticipante']}"/>
    		<f:param name="codFuncionalBradesco" value="#{param['codFuncionalBradesco']}"/>
    		<f:param name="nmParticipante" value="#{param['nmParticipante']}"/>
    		<f:param name="nmEmpresa" value="#{param['nmEmpresa']}"/>
    		<arq:submitCheckClient />
  		</brHtml:brCommandLink>

	</brHtml:brPanelGrid>
</arq:form>