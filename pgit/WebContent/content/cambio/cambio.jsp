<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>

<h:form id="cambioForm">
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="0">
		<br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputLabelTitleColor value="#{msgs.label_header_cambio}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brDataTable var="cotacaoVar" value="#{cambioBean.cotacoes}" 
								styleClass="standardTable" headerClass="standardTable_Header" 
								footerClass="standardTable_Header" columnClasses="standardTable_Column"
								rowClasses="standardTable_Row1,standardTable_Row2">
					<br:brColumn>
						<f:facet name="header">
							<br:brOutputTextBold value="#{msgs.label_header_moeda}"/>
						</f:facet>
						<br:brOutputText value="#{cotacaoVar.moeda}"/>
					</br:brColumn>
					<br:brColumn>
						<f:facet name="header">
							<br:brOutputTextBold value="#{msgs.label_header_compra}"/>
						</f:facet>
						<br:brOutputText value="#{cotacaoVar.compra}"/>
					</br:brColumn>
					<br:brColumn>
						<f:facet name="header">
							<br:brOutputTextBold value="#{msgs.label_header_venda}"/>
						</f:facet>
						<br:brOutputText value="#{cotacaoVar.venda}"/>
					</br:brColumn>
					<br:brColumn>
						<f:facet name="header">
							<br:brOutputTextBold value="#{msgs.label_header_variacao}"/>
						</f:facet>
						<br:brOutputText value="#{cotacaoVar.variacao}"/>
					</br:brColumn>
				</br:brDataTable>
			</br:brPanelGroup>
			<f:verbatim><br><br></f:verbatim>
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="0">
					<br:brPanelGroup>
						<br:brOutputTextTitleColor value="#{msgs.label_conversion_title}"/>
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brPanelGrid columns="2">
							<br:brPanelGroup style="width: 130px">
								<br:brOutputLabelColor value="#{msgs.label_value_to_convert}"/>
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brInputText value="#{cambioBean.valueToConvert}" size="26" maxlength="20">
									<brArq:commonsValidator type="required" arg="#{msgs.msg_text_value_required}" server="false" client="true"/>
								</br:brInputText>
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brPanelGrid columns="2">
							<br:brPanelGroup style="width: 130px">
								<br:brOutputLabelColor value="#{msgs.label_day}"/>
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brInputText size="12" maxlength="10" value="#{cambioBean.dataCotacaoToConvert}">
									<brArq:commonsValidator type="required" arg="#{msgs.msg_text_day_required}" server="false" client="true"/>
									<brArq:commonsValidator type="date" arg="#{msgs.msg_text_day_not_date}" datePatternStrict="#{cambioBean.dataPadrao}" server="false" client="true"/>
								</br:brInputText>
							</br:brPanelGroup>
						</br:brPanelGrid>				
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brPanelGrid columns="2">
							<br:brPanelGroup style="width: 130px">
								<br:brOutputLabelColor value="#{msgs.label_convert_of}"/>
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brSelectOneMenu id="vDe" value="#{cambioBean.moedaDe}">
									<f:selectItems value="#{cambioBean.moedas}"/>
								</br:brSelectOneMenu>
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brPanelGrid columns="2">
							<br:brPanelGroup style="width: 130px">
								<br:brOutputLabelColor value="#{msgs.label_convert_to}"/>
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brSelectOneMenu id="vPara" value="#{cambioBean.moedaPara}">
									<f:selectItems value="#{cambioBean.moedas}"/>
								</br:brSelectOneMenu>
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>
					<f:verbatim><br><br></f:verbatim>
					<br:brPanelGroup>
						<f:verbatim><b>
							<br:brOutputLabelBold value="#{msgs.label_result_conversion}"/>
							<br:brOutputLabelBold value="#{cambioBean.resultado}"/>
							<h:messages/>
						</b><br><br></f:verbatim>
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brCommandLink action="home_page2">
							<br:brGraphicImage alt="#{msgs.botao_voltar}" url="/images/bt_voltar.gif"/>
						</br:brCommandLink>
						<br:brCommandLink  id="limpar" action="nav_cambio" actionListener="#{cambioBean.entrarCambio}">
							<br:brGraphicImage url="/images/bt_limpar.gif" alt="#{msgs.botao_limpar}"/>
						</br:brCommandLink>
						<br:brCommandButton id="entrar" alt="#{msgs.botao_entrar}" action="nav_cambio" image="/images/bt_entrar.gif"
							onclick="javascript: return validateForm(document.forms[0]);" actionListener="#{cambioBean.entrarCambio}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<brArq:validatorScript functionName="validateForm"/>
</h:form>