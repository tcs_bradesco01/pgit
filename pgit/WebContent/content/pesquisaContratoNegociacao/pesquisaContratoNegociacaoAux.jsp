<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="pesquisaContratoNegociacaoAuxForm" name="pesquisaContratoNegociacaoAuxForm"  >


<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup style="text-align:right;width:600px" >					
				<br:brCommandButton id="btnRedirecionar" style="visibility:hidden"  styleClass="bto1" action="#{pesquisaContratoNegociacaoBean.iniciarPagina}" value="#{msgs.btn_selecionar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</a4j:outputPanel>
	

</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>
