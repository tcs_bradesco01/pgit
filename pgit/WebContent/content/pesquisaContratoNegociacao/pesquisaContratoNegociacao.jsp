<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="pesquisaContratoNegociacaoForm" name="pesquisaContratoNegociacaoForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.obrigatoriedade}"/>
<h:inputHidden id="habilitaCampos" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaCampos}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.bloqueiaRadio}">  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
		<a4j:support event="onclick" reRender="panelCnpj, panelCpf, panelNomeRazao, panelBanco, panelAgencia, panelConta, panelBtoConsultarCliente" ajaxSingle="true"  action="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.limparCampos}"/>
	</t:selectOneRadio>
	</a4j:region>
	
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'pesquisaContratoNegociacaoForm','pesquisaContratoNegociacaoForm:txtCnpj','pesquisaContratoNegociacaoForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"/>
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'pesquisaContratoNegociacaoForm','pesquisaContratoNegociacaoForm:txtFilial','pesquisaContratoNegociacaoForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" />
				<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" />
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente }" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'pesquisaContratoNegociacaoForm','pesquisaContratoNegociacaoForm:txtCpf','pesquisaContratoNegociacaoForm:txtControleCpf');" />
			    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '2' || pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  border="0">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'pesquisaContratoNegociacaoForm','pesquisaContratoNegociacaoForm:txtBanco','pesquisaContratoNegociacaoForm:txtAgencia');"/>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'pesquisaContratoNegociacaoForm','pesquisaContratoNegociacaoForm:txtAgencia','pesquisaContratoNegociacaoForm:txtConta');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" />
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCampos2" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.limparDadosCliente}" style="margin-right:5px">  
				<brArq:submitCheckClient/>
			</br:brCommandButton>
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemFiltroSelecionado || pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{msgs.identificacaoClienteContrato_label_consultar_cliente}" action="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.identificacaoClienteContrato_cnpj}','#{msgs.identificacaoClienteContrato_cpf}', '#{msgs.identificacaoClienteContrato_label_nomeRazao}','#{msgs.identificacaoClienteContrato_banco}','#{msgs.identificacaoClienteContrato_agencia}','#{msgs.identificacaoClienteContrato_conta}');">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolManutContratos_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="empresaGestora" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{!pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.identificacaoClienteContrato_selecione}"/>
						<f:selectItems  value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.listaEmpresaGestora}" />								
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_tipo_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoContrato" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{!pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.identificacaoClienteContrato_selecione}"/>
						<f:selectItems  value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.listaTipoContrato}" />
					</br:brSelectOneMenu>
					
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_numero}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.numeroFiltro}" size="12" maxlength="10" readonly="#{!pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
					<a4j:support event="onblur" action="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.desabilitaCamposContrato}"	reRender="habilitaCampos,situacao, agenciaOperadora, gerenteId, gerenteNome" />		
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.identificacaoClienteContrato_agencia_operadora}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="agenciaOperadora" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.agenciaOperadoraFiltro}" size="30" maxlength="30" 
					readonly="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaCampos || !pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<a4j:support event="onblur" reRender="gerenteId,gerenteNome" action="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.limparGerenteResponsavel}" />
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_gerente_responsavel}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="gerenteId" onkeypress="javascript:limparNomeGerente(document.forms[1]);" onkeydown="javascript:limparNomeGerente(document.forms[1]);" styleClass="HtmlInputTextBradesco" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.codigoFunc}" size="15" maxlength="15" readonly="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaCampos || !pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato || 
					      pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.agenciaOperadoraFiltro == ''}"  onblur="javascript: buscarGerente();">
						<brArq:commonsValidator type="integer" arg="#{msgs.identificacaoClienteContrato_gerente_responsavel}" server="false" client="true" />
					</br:brInputText>
					<br:brCommandButton id="btnBuscar" style="display: none;" styleClass="bto1" value="hiddenBtn" action="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.carregaListaFuncionarios}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>	
				</br:brPanelGroup>
	    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
			    <br:brPanelGroup>
					<br:brInputText id="gerenteNome" styleClass="HtmlInputTextBradesco" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.funcionarioDTO.dsFuncionario}" size="50" maxlength="50" readonly="true"  />				
				</br:brPanelGroup>		
				<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
				<br:brPanelGroup>
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >							
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>				
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_situacao_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="situacao" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.situacaoFiltro}" disabled="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaCampos || !pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.identificacaoClienteContrato_selecione}"/>
						<f:selectItems  value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.listaSituacaoContrato}" />						
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.limparDadosPesquisaContrato}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" onclick="javascript:desbloquearTela(); return validaCamposContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}', '#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}','#{msgs.identificacaoClienteContrato_tipo_contrato}', '#{msgs.identificacaoClienteContrato_mensagem_obrigatorio}', '#{msgs.identificacaoClienteContrato_obrigatorio_consulta_gerente}');" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_consultar}" action="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.consultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim><br></f:verbatim> 

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

			<app:scrollableDataTable id="dataTable" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.listaGridPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemSelecionadoLista}">
						<f:selectItems value="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes"/>
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.identificacaoClienteContrato_club_representante}" style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdClubRepresentante}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="170px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_cpf_cnpj_representante}"  style="width:170; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cnpjOuCpfFormatado}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_nome_razao_social}" style="width:250; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.nmRazaoSocialRepresentante}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_empresa_gestora}"  style="width:300; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsPessoaJuridica}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_tipo_contrato}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoContrato}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_numero}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrSequenciaContrato}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_situacao_contrato}" style="width:150; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoContrato}" />
				</app:scrollableColumn>	

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_agencia_operadora}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsAgenciaOperadora}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_gerente_responsavel}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.descFuncionarioBradesco}" />
				</app:scrollableColumn>					
				
			</app:scrollableDataTable>
			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{pesquisaContratoNegociacaoBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >				
		<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{pesquisaContratoNegociacaoBean.voltar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >		
				<br:brCommandButton id="btnLimparTela" styleClass="bto1" value="#{msgs.conManterContratos_limpar}" style="margin-right:5px" action="#{pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.limparTela}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnSelecionar" styleClass="bto1" disabled="#{empty pesquisaContratoNegociacaoBean.identificacaoClienteContratoBean.itemSelecionadoLista}" action="#{pesquisaContratoNegociacaoBean.selecionar}" value="#{msgs.btn_selecionar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</a4j:outputPanel>
	
	<br:brPanelGroup style="position:absolute; width: 100%; text-align: center; top:145">
      <a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()">
        <f:facet name="start">
          <h:graphicImage value="" />
        </f:facet>
      </a4j:status>
    </br:brPanelGroup>
    
    <f:verbatim>
	  	<script language="javascript">
	  		function buscarGerente() {
	  			document.getElementById("pesquisaContratoNegociacaoForm:btnBuscar").click();
	  		}
	  	</script>
	  </f:verbatim>

</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>
