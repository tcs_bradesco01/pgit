<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
	<h:column>
		<h:panelGroup>
			<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="Gestor de Cache" />
		</h:panelGroup>
	</h:column>
</h:panelGrid>
<h:form id="formularioLista">
	<t:selectOneRadio id="id" required="true" layout="spread" forceId="true" forceIdIndex="false" value="#{contatoBean.contato.id}" >
		<f:selectItems value="#{contatoBean.radios}" />
   	</t:selectOneRadio>
	<h:messages errorStyle="color:red" infoStyle="color:#5ac67e"  />
	<br:brPanelGrid>
		<t:dataTable var="contato" value="#{contatoBean.contatosData}" width="500" align="center" styleClass="HtmlIntranetTable" rowIndexVar="index" >
			<br:brColumn>
				<f:facet name="header">
	            	<br:brOutputLabelTitle value="Id" />
	            </f:facet>
	            <t:radio id="id" for=":formularioLista:id" index="#{index}"></t:radio>
			</br:brColumn>
			<br:brColumn>
				<f:facet name="header">
	            	<br:brOutputLabelTitle value="Nome" />
	            </f:facet>
	            <br:brOutputText value="#{contato.nome}" />
			</br:brColumn>
			<br:brColumn>
				<f:facet name="header">
	            	<br:brOutputLabelTitle value="E-mail" />
	            </f:facet>
	            <br:brOutputText value="#{contato.email}" />
			</br:brColumn>
			<br:brColumn>
				<f:facet name="header">
	            	<br:brOutputLabelTitle value="Telefone" />
	            </f:facet>
	            <br:brOutputText value="#{contato.telefone}" />
			</br:brColumn>			
		</t:dataTable>
	</br:brPanelGrid>
	<br:brCommandButton value="Editar" action="#{contatoBean.editar}" /> 
    <h:outputText value=" " />
  	<br:brCommandButton value="Deletar" action="#{contatoBean.deletar}" />
</h:form>
<br:brPanelGrid>
	<br:brPanelGroup>
		<h:form>
			<br:brCommandButton action="CacheManagerEdit" value="Novo" />
		</h:form>	
	</br:brPanelGroup>
</br:brPanelGrid>