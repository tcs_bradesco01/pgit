<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
	<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
		<h:column>
			<h:panelGroup>
				<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="Gestor de Cache" />
			</h:panelGroup>
		</h:column>
	</h:panelGrid>
	<h:messages errorStyle="color:red" infoStyle="color:#5ac67e"  />
	<h:form id="formularioCadastro">
		<h:inputHidden value="#{contatoBean.contato.id}" id="id" />
		<br:brPanelGrid columns="2">
			<br:brOutputText value="Nome"/>
			<br:brInputText value="#{contatoBean.contato.nome}" id="nome" required="true" />
			<br:brOutputText value="E-mail"/>
			<br:brInputText value="#{contatoBean.contato.email}" id="email" required="true" />		
			<br:brOutputText value="Telefone"/>
			<br:brInputText value="#{contatoBean.contato.telefone}" id="telefone" required="true" />
			<br:brCommandButton value="Salvar" action="CacheManagerEdit" actionListener="#{contatoBean.salvar}" />
		</br:brPanelGrid>
	</h:form>
	<br:brPanelGrid>
		<br:brPanelGroup>
			<h:form>	
				<br:brCommandButton value="Listar" action="CacheManagerList" />
			</h:form>
		</br:brPanelGroup>
	</br:brPanelGrid>	