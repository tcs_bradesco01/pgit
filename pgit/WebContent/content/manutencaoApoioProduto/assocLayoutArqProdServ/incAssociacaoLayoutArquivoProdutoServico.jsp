<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="assocLayoutArqProdServForm" name="assocLayoutArqProdServForm">
<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina" >
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup> 
	</br:brPanelGrid>   
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Dados da Associação:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>


	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incAssociacaoLayoutArquivoProdutoServico_servicos}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoServico" value="#{associacaoLayoutArqProdServBean.filtroServicoIncluir}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
				<f:selectItems value="#{associacaoLayoutArqProdServBean.listaTipoServico}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.incAssociacaoLayoutArquivoProdutoServico_servicos}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incAssociacaoLayoutArquivoProdutoServico_tipoLayoutArquivo}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoLayoutArquivo" value="#{associacaoLayoutArqProdServBean.filtroTipoLayoutArquivoIncluir}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
				<f:selectItems value="#{associacaoLayoutArqProdServBean.listaTipoLayoutArquivo}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.incAssociacaoLayoutArquivoProdutoServico_tipoLayoutArquivo}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_finalidade_layout_arquivo}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="finalidadeLayout" value="#{associacaoLayoutArqProdServBean.cdFinalidade}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.label_layout_padrao_negociacao_novos_clientes}"/>  
		        <f:selectItem itemValue="2" itemLabel="#{msgs.label_layout_padrao_utilizado_sistemas_legados}"/>
		        <f:selectItem itemValue="3" itemLabel="#{msgs.label_layout_proprietario}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.label_finalidade_layout_arquivo}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
		<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incAssociacaoLayoutArquivoProdutoServico_layout_default}:"/>
	</br:brPanelGroup>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brSelectOneRadio id="rdoLayoutDefault" styleClass="HtmlSelectOneRadioBradesco" value="#{associacaoLayoutArqProdServBean.rdoLayoutDefault}">
				<f:selectItem  itemValue="1" itemLabel="#{msgs.combo_sim}"/>  
       			<f:selectItem  itemValue="2" itemLabel="#{msgs.combo_nao}"/>
       			<a4j:support event="onclick" /> 
       		</br:brSelectOneRadio>
   		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.incAssociacaoLayoutArquivoProdutoServico_btn_voltar}" action="#{associacaoLayoutArqProdServBean.voltar}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" onclick="javascript: return validateForm(document.forms[1]);" value="#{msgs.incAssociacaoLayoutArquivoProdutoServico_btn_avancar}" action="#{associacaoLayoutArqProdServBean.incluirAvancar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	
	
	