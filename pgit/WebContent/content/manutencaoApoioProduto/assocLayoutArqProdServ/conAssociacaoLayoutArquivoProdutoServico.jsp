<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="assocLayoutArqProdServForm">
<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina" >

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup> 
	</br:brPanelGrid>   
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>


	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_tipo_servico}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoServico" value="#{associacaoLayoutArqProdServBean.filtroServico}" disabled="#{associacaoLayoutArqProdServBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
				<f:selectItems value="#{associacaoLayoutArqProdServBean.listaTipoServico}"/>
				<brArq:commonsValidator type="required" arg="O campo Tipo Servi�o � necess�rio" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_tipoLayoutArquivo}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoLayoutArquivo" value="#{associacaoLayoutArqProdServBean.filtroTipoLayoutArquivo}" disabled="#{associacaoLayoutArqProdServBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
				<f:selectItems value="#{associacaoLayoutArqProdServBean.listaTipoLayoutArquivo}"/>
				<brArq:commonsValidator type="required" arg="O campo Tipo Layout Arquivo � necess�rio" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_finalidade_layout_arquivo}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="finalidadeLayoutArquivo" value="#{associacaoLayoutArqProdServBean.filtroFinalidadeLayoutArquivo}" disabled="#{associacaoLayoutArqProdServBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.label_layout_padrao_negociacao_novos_clientes}"/>  
		        <f:selectItem itemValue="2" itemLabel="#{msgs.label_layout_padrao_utilizado_sistemas_legados}"/>
		        <f:selectItem itemValue="3" itemLabel="#{msgs.label_layout_proprietario}"/>
			<brArq:commonsValidator type="required" arg="O campo Finalidade Layout Arquivo � necess�rio" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_botao_limpar_campos}" action="#{associacaoLayoutArqProdServBean.limparCampos}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_botao_consultar}" action="#{associacaoLayoutArqProdServBean.submit}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
			<a4j:region id="regionRadioParametro">
				<t:selectOneRadio  id="rdoLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{associacaoLayoutArqProdServBean.itemSelecionadoLista}" converter="assoLoteLayoutArquivoProdutoServicoConveter">
					<si:selectItems value="#{associacaoLayoutArqProdServBean.listaGrid}" var="layout" itemLabel="" itemValue="#{layout}" />
					<a4j:support event="onclick" reRender="panelBotoes" limitToList="true" ajaxSingle="true" />
				</t:selectOneRadio>
			</a4j:region>
			
			<app:scrollableDataTable id="dataTable" value="#{associacaoLayoutArqProdServBean.listaGrid}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" height="170">			
			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
				<t:radio for=":assocLayoutArqProdServForm:rdoLista" index="#{parametroKey}" />
			</app:scrollableColumn>
	
			<app:scrollableColumn  width="300px" styleClass="colTabLeft" >
			    <f:facet name="header">
			       <br:brOutputText value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_grid_servico}" style="width:300; text-align:center" />
			    </f:facet>
			     <br:brOutputText value="#{result.dsTipoServico}" />
			 </app:scrollableColumn>
	
			<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_grid_tipoLayoutArquivo}" style="width:250; text-align:center"/>
			    </f:facet>
			   <br:brOutputText value="#{result.dsTipoLayoutArquivo}"/>
			 </app:scrollableColumn>

			 <app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.label_finalidade_layout_arquivo}" style="width:200; text-align:center"/>
			    </f:facet>
			   <br:brOutputText value="#{result.dsIdentificadorLayoutNegocio}"/>
			 </app:scrollableColumn>
			</app:scrollableDataTable>				
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup rendered="#{!empty associacaoLayoutArqProdServBean.listaGrid}">
			<brArq:pdcDataScroller  id="dataScroller" for="dataTable" actionListener="#{associacaoLayoutArqProdServBean.pesquisar}" >
			   <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
		<br:brPanelGrid columns="4" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
			<br:brPanelGroup style="width:750px">
				<br:brCommandButton id="btnDetalhar" disabled="#{empty associacaoLayoutArqProdServBean.itemSelecionadoLista}" styleClass="bto1"  style="margin-right:5px;" value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_botao_detalhar}" action="#{associacaoLayoutArqProdServBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" disabled="false" styleClass="bto1" style="margin-right:5px;" value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_botao_incluir}" action="#{associacaoLayoutArqProdServBean.incluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" disabled="#{empty associacaoLayoutArqProdServBean.itemSelecionadoLista}" styleClass="bto1"  style="margin-right:5px;" value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_botao_alterar}" action="#{associacaoLayoutArqProdServBean.alterar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
				<br:brCommandButton id="btnExcluir" disabled="#{empty associacaoLayoutArqProdServBean.itemSelecionadoLista}" styleClass="bto1"  value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_botao_excluir}" action="#{associacaoLayoutArqProdServBean.excluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>		
		</br:brPanelGrid>	
	</a4j:outputPanel>
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	</br:brPanelGrid>
	
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>	
	
	