<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="pesqmanterMoedasSistemaForm" name="pesqmanterMoedasSistemaForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterMoedasSistema_label_title_argumentoPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>   
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMoedasSistema_label_tipo_layout_arquivo}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brPanelGroup>			
			<br:brSelectOneMenu id="tipoLayoutArquivo" value="#{manterMoedasSistemaBean.tipoLayoutArquivoFiltro}" disabled="#{manterMoedasSistemaBean.btoAcionado}">
				<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
				<f:selectItems value="#{manterMoedasSistemaBean.listaTipoLayoutArquivo}"/>
			</br:brSelectOneMenu>		    	
		</br:brPanelGroup>
		
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup></br:brPanelGroup>
			</br:brPanelGrid>  
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMoedasSistema_label_moeda_indice_padrao_BANCO}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brSelectOneMenu id="moedaIndicePadraoBancoFiltro" value="#{manterMoedasSistemaBean.moedaIndicePadraoBancoFiltro}" disabled="#{manterMoedasSistemaBean.btoAcionado}">
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
			<f:selectItems value="#{manterMoedasSistemaBean.listaMoedaEconomica}"/>
		</br:brSelectOneMenu>			 
								
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conManterMoedasSistema_label_botao_limpar_campos}" action="#{manterMoedasSistemaBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conManterMoedasSistema_label_botao_consultar}" action="#{manterMoedasSistemaBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim>  

		<br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="width:750px">
			<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterMoedasSistemaBean.itemSelecionadoLista}" converter="moedaSistemaConverter" onclick="javascript: habilitaBotoes()">
				<si:selectItems value="#{manterMoedasSistemaBean.listaPesquisa}" var="layout" itemLabel="" itemValue="#{layout}" />
			</t:selectOneRadio>
			<app:scrollableDataTable id="dataTable" value="#{manterMoedasSistemaBean.listaPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%" height="170">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:radio for=":pesqmanterMoedasSistemaForm:sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="240px" styleClass="colTabLeft" > 
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterMoedasSistema_label_grid_tipo_layout_arquivo}" style="text-align:center;width:240" />
					</f:facet>
					<br:brOutputText value="#{result.dsTipoLayoutArquivo}"/>
				 </app:scrollableColumn>
				 
				<app:scrollableColumn  width="240px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="Moeda/�ndice no Padr�o do Arquivo" style="text-align:center;width:240" />
					</f:facet>
					<br:brOutputText value="#{result.dsIndicadorEconomicoLayout}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="240px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterMoedasSistema_label_grid_moeda_indice_padrao_BANCO}"  style="text-align:center;width:240"/>
					</f:facet>
					<br:brOutputText value="#{result.dsIndicadorEconomicoMoeda}"/>
				</app:scrollableColumn>
				 
			</app:scrollableDataTable>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterMoedasSistemaBean.pesquisar}" rendered="#{manterMoedasSistemaBean.listaPesquisa != null}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>				
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGroup id="panelBotoes" style="width: 100%; text-align: right" >	
		<br:brPanelGrid columns="3" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
				<br:brPanelGroup style="width:750px">			
					<br:brCommandButton id="btnDetalhar" disabled="#{empty manterMoedasSistemaBean.listaPesquisa}" styleClass="bto1" style="margin-right:5px;" value="#{msgs.conManterMoedasSistema_label_botao_detalhar}" action="#{manterMoedasSistemaBean.avancarDetalhar}">				
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnIncluir" disabled="false" styleClass="bto1"  style="margin-right:5px;" value="#{msgs.conManterMoedasSistema_label_botao_incluir}" action="#{manterMoedasSistemaBean.avancarIncluir}">				
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnExcluir" disabled="#{empty manterMoedasSistemaBean.listaPesquisa}" styleClass="bto1"  value="#{msgs.conManterMoedasSistema_label_botao_excluir}" action="#{manterMoedasSistemaBean.avancarExcluir}">				
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>		
		</br:brPanelGrid>
	</br:brPanelGroup>		
</br:brPanelGrid>
<f:verbatim><script>desabilitaBotoes()</script></f:verbatim>
</brArq:form>
