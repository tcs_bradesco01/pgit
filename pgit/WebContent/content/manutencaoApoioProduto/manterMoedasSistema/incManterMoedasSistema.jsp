<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="pesqmanterMoedasSistemaIncluirForm" name="pesqmanterMoedasSistemaIncluirForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterMoedasSistema_label_title_moeda}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>   
	
   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterMoedasSistema_tipo_layout_arquivo}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brSelectOneMenu id="tipoLayoutArquivo" value="#{manterMoedasSistemaBean.incluirTipoLayoutArquivoFiltro}">
				<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
				<f:selectItems value="#{manterMoedasSistemaBean.listaTipoLayoutArquivo}"/>
				<brArq:commonsValidator type="required" arg="Tipo Layout Arquivo" server="false" client="true"/>
			</br:brSelectOneMenu>    	
		</br:brPanelGroup>					
				
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup></br:brPanelGroup>
			</br:brPanelGrid>  
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Moeda/�ndice no Padr�o do Arquivo:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brPanelGroup>
			<br:brInputText  styleClass="HtmlInputTextBradesco" value="#{manterMoedasSistemaBean.moedaIndicePadraoBanco}" size="15" maxlength="10" id="txtMoedaIndiceArquivo">
				<brArq:commonsValidator type="required" arg="Moeda/�ndice do Arquivo" server="false" client="true"/>
			</br:brInputText>
		</br:brPanelGroup>
		
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup></br:brPanelGroup>
			</br:brPanelGrid>  
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterMoedasSistema_label_moeda_indice_padrao_BANCO}:"/>
		</br:brPanelGroup>
								
		<br:brPanelGroup>
			<br:brSelectOneMenu id="moedaIndicePadraoBanco" value="#{manterMoedasSistemaBean.incluirMoedaIndicePadraoBancoFiltro}">
				<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
				<f:selectItems value="#{manterMoedasSistemaBean.listaMoedaEconomica}"/>
				<brArq:commonsValidator type="required" arg="Moeda/�ndice no Padr�o Banco" server="false" client="true"/>
			</br:brSelectOneMenu>		 
		</br:brPanelGroup>							
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.incManterMoedasSistema_label_botao_voltar}" action="#{manterMoedasSistemaBean.voltarConsultar}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton  id="btnAvancar"  styleClass="bto1" value="#{msgs.incManterMoedasSistema_label_botao_avancar}" action="#{manterMoedasSistemaBean.avancarConfirmar}" onclick="javascript: return validateForm(document.forms[1]);">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" style="height:220px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>		
	</br:brPanelGrid> 		
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
