<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="pesqhistoricoManutencaoOrgaoPagadorForm" name="pesqhistoricoManutencaoOrgaoPagadorForm" >
<h:inputHidden id="hiddenRadio" value=""/>

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.PGIC0142_label_title_argumentoPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>   
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0142_label_orgaoPagador}:"/>
		</br:brPanelGroup>		
				
		<br:brPanelGroup ></br:brPanelGroup>		
	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0142_label_periodoManutencao}:" />
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>	
	    	<br:brInputText size="7"  maxlength="5" id="txtOrgaoPagador" value="#{historicoManutencaoOrgaoPagadorBean.historicoOrgaoPagadorFiltro.cdOrgaoPagador}" disabled="#{historicoManutencaoOrgaoPagadorBean.btoAcionado}" onkeypress="onlyNum()" onfocus="cleanClipboard()"/>
		</br:brPanelGroup>			

		<br:brPanelGroup></br:brPanelGroup>

		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >	
			<br:brPanelGroup>
				<!--br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.PGIC0142_label_de}"/-->			
				<br:brPanelGroup rendered="#{!historicoManutencaoOrgaoPagadorBean.btoAcionado}">
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioDe');" id="calendarioDe" value="#{historicoManutencaoOrgaoPagadorBean.historicoOrgaoPagadorFiltro.dtDe}" >
					</app:calendar>
				</br:brPanelGroup>
				<br:brPanelGroup rendered="#{historicoManutencaoOrgaoPagadorBean.btoAcionado}">
					<app:calendar id="calendarioDeDes" value="#{historicoManutencaoOrgaoPagadorBean.historicoOrgaoPagadorFiltro.dtDe}" disabled="true">
					</app:calendar>
				</br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.PGIC0142_label_ate}"/>
				<br:brPanelGroup rendered="#{!historicoManutencaoOrgaoPagadorBean.btoAcionado}">
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioAte');" id="calendarioAte" value="#{historicoManutencaoOrgaoPagadorBean.historicoOrgaoPagadorFiltro.dtAte}" >
					</app:calendar>	
				</br:brPanelGroup>	
				<br:brPanelGroup rendered="#{historicoManutencaoOrgaoPagadorBean.btoAcionado}">
					<app:calendar id="calendarioAteDes" value="#{historicoManutencaoOrgaoPagadorBean.historicoOrgaoPagadorFiltro.dtAte}" disabled="true">
					</app:calendar>	
				</br:brPanelGroup>			
			</br:brPanelGroup>	  
		</a4j:outputPanel>  
								
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.PGIC0142_label_botao_limpar_campos}" action="#{historicoManutencaoOrgaoPagadorBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" onclick="javascript:if(!validarFiltroHistoricoManutencao('#{msgs.label_ocampo}', '#{msgs.label_necessario}', 
					'#{msgs.PGIC0142_label_periodo_inicial}','#{msgs.PGIC0142_label_periodo_final}')) { desbloquearTela(); return false; }"
					styleClass="bto1" value="#{msgs.PGIC0142_label_botao_consultar}" action="#{historicoManutencaoOrgaoPagadorBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="OverflowScrollableDataTable">	
		<t:selectOneRadio id="rdoOrgaoPagador" styleClass="HtmlSelectOneRadioBradesco" 
			layout="spread" forceId="true" forceIdIndex="false" 
			value="#{historicoManutencaoOrgaoPagadorBean.itemSelecionado}"
			converter="historicoOrgaoPagadorConverter">
				<si:selectItems value="#{historicoManutencaoOrgaoPagadorBean.listaGridPesquisa}" var="orgaoPagador" itemLabel="" itemValue="#{orgaoPagador}"/>
				<a4j:support event="onclick" reRender="panelBotoes" />
			</t:selectOneRadio>
		<app:scrollableDataTable id="dataTable" value="#{historicoManutencaoOrgaoPagadorBean.listaGridPesquisa}" var="result" 
			rows="10" rowIndexVar="parametroKey" 
			rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">

			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
		    	<t:radio for=":pesqhistoricoManutencaoOrgaoPagadorForm:rdoOrgaoPagador" index="#{parametroKey}" />
			</app:scrollableColumn>
			
			<app:scrollableColumn  width="100px" styleClass="colTabRight" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0142_label_grid_orgaoPagador}" style="width:100; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.cdOrgaoPagador}"/>
			</app:scrollableColumn>
			
			<app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0142_label_grid_tipo_unidade_organizacional}" style="width:200; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoUnidadeOrganizacional}"/>
			</app:scrollableColumn>

			<app:scrollableColumn  width="130px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0142_label_grid_empresa}" style="width:130; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsPessoJuridica}"/>
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn  width="130px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0142_label_grid_agencia_pacb}" style="width:130; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsSeqUnidadeOrganizacional}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="160px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0142_label_grid_situacao}" style="width:160; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{msgs.conOrgaoPagador_label_combo_aberta}" rendered="#{result.dsSituacao == 1}"/>
				<br:brOutputText value="#{msgs.conOrgaoPagador_label_combo_fechada}" rendered="#{result.dsSituacao == 2}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="180px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0142_label_grid_dataManutencao}" style="width:180; text-align:center"/>
			    </f:facet>
				<br:brOutputText value="#{result.horario}"/>
			</app:scrollableColumn>
			
			<app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0142_label_grid_responsavel}" style="width:200; text-align:center"/>
			    </f:facet>
				<br:brOutputText value="#{result.dsUsuarioResponsavel}"/>
			</app:scrollableColumn>
			
			<app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0142_label_grid_tipoManutencao}" style="width:200; text-align:center"/>
			    </f:facet>
				<br:brOutputText value="#{msgs.PGIC0142_label_tipoManutencao_inclusao}" rendered="#{result.cdIndicadorTipoManutencao == 1}"/>
				<br:brOutputText value="#{msgs.PGIC0142_label_tipoManutencao_manutencao}" rendered="#{result.cdIndicadorTipoManutencao == 2}"/>
				<br:brOutputText value="#{msgs.PGIC0142_label_tipoManutencao_exclusao}" rendered="#{result.cdIndicadorTipoManutencao == 3}"/>
				<br:brOutputText value="#{msgs.PGIC0142_label_tipoManutencao_aberta}" rendered="#{result.cdIndicadorTipoManutencao == 4}"/>
				<br:brOutputText value="#{msgs.PGIC0142_label_tipoManutencao_fechada}" rendered="#{result.cdIndicadorTipoManutencao == 5}"/>
			</app:scrollableColumn>
		</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{historicoManutencaoOrgaoPagadorBean.pesquisar}" rendered="#{!empty historicoManutencaoOrgaoPagadorBean.listaGridPesquisa}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>			
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" value="#{msgs.PGIC0142_label_botao_detalhar}" disabled="#{empty historicoManutencaoOrgaoPagadorBean.itemSelecionado}" action="#{historicoManutencaoOrgaoPagadorBean.avancarDetalhar}" >				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
