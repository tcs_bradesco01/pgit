<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="pesqDuplicacaoArquivoRetornoIncluirConfirmar" name="pesqDuplicacaoArquivoRetornoIncluirConfirmar" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contrato_origem}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detDuplicacaoArquivoRetorno_cpf_cnpj_origem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detDuplicacaoArquivoRetorno_nome_razao_origem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	

	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
     <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detDuplicacaoArquivoRetorno_empresa_origem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>
		</br:brPanelGroup>			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detDuplicacaoArquivoRetorno_situacao_origem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detDuplicacaoArquivoRetorno_tipo_origem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detDuplicacaoArquivoRetorno_numero_origem} do Contrato:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detDuplicacaoArquivoRetorno_descricao_contrato_origem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_duplicacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detDuplicacaoArquivoRetorno_tipo_layout}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.dsTipoLayout}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_retorno}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.dsTipoArquivo}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incDuplicacaoArquivoRetorno2_duplicacao_participante_mesmo_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_sim}" rendered = "#{duplicacaoArquivoRetornoBean.duplicacaoParticipanteMesmoContrato == 1}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_nao}" rendered = "#{duplicacaoArquivoRetornoBean.duplicacaoParticipanteMesmoContrato == 2}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.cmpi0000_label_cpf_cnpj}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px;" >
		</br:brPanelGroup>	
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_nome_razao}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.nomeRazaoSocialParticipante}"/>
		</br:brPanelGroup>
	 </br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incDuplicacaoArquivoRetorno2_tipo_duplicacao}:"/>			
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"value="#{msgs.conDuplicacaoArquivoRetorno_integral}" rendered="#{duplicacaoArquivoRetornoBean.tipoDuplicacao=='1'}"/>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"value="#{msgs.conDuplicacaoArquivoRetorno_parcial}" rendered="#{duplicacaoArquivoRetornoBean.tipoDuplicacao=='2'}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incDuplicacaoArquivoRetorno2_instrucao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_radio_duplicar}" rendered="#{duplicacaoArquivoRetornoBean.instrucao == 1}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_radio_redirecionar}" rendered="#{duplicacaoArquivoRetornoBean.instrucao == 2}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	

	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" id="empresaGestora" value="#{duplicacaoArquivoRetornoBean.dsPessoaJuridicaClienteContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.dsTipoContratoNegocioClienteContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.nrSeqContratoNegocioClienteContrato}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
	
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_descricao_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.dsContrato}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_situacao}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>		
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_participante}:"/>
	 
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGroup id="panelParticipante" >
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.participanteCpfCnpj}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>
			
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao_social}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.participanteNomeRazaoSocial}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.participanteNivelParticipacao}"/>
			</br:brPanelGroup>

			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_situacaoParticipacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.participanteSituacaoParticipacao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGroup>

	<f:verbatim> <hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.incDuplicacaoArquivoRetorno2_btn_voltar}" action="#{duplicacaoArquivoRetornoBean.voltarIncluir}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar"   styleClass="bto1" value="#{msgs.incDuplicacaoArquivoRetorno2_btn_confirmar}" action="#{duplicacaoArquivoRetornoBean.confirmarIncluir}" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela(); return false; }">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" style="width:150px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> <verbatim>&nbsp;</verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
