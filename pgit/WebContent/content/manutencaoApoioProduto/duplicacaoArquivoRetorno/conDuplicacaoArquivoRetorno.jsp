<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="pesqDuplicacaoArquivoRetorno" name="pesqDuplicacaoArquivoRetorno" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{duplicacaoArquivoRetornoBean.itemFiltroSelecionado}" onclick="submit();" disabled="#{duplicacaoArquivoRetornoBean.bloqueaRadio}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
	</t:selectOneRadio>
	</a4j:region>
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'pesqDuplicacaoArquivoRetorno','pesqDuplicacaoArquivoRetorno:txtCnpj','pesqDuplicacaoArquivoRetorno:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{duplicacaoArquivoRetornoBean.itemFiltroSelecionado != '0' || duplicacaoArquivoRetornoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'pesqDuplicacaoArquivoRetorno','pesqDuplicacaoArquivoRetorno:txtFilial','pesqDuplicacaoArquivoRetorno:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{duplicacaoArquivoRetornoBean.itemFiltroSelecionado != '0' || duplicacaoArquivoRetornoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{duplicacaoArquivoRetornoBean.itemFiltroSelecionado != '0'|| duplicacaoArquivoRetornoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{duplicacaoArquivoRetornoBean.itemFiltroSelecionado != '1'|| duplicacaoArquivoRetornoBean.habilitaFiltroDeCliente}" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'pesqDuplicacaoArquivoRetorno','pesqDuplicacaoArquivoRetorno:txtCpf','pesqDuplicacaoArquivoRetorno:txtControleCpf');" />
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{duplicacaoArquivoRetornoBean.itemFiltroSelecionado != '1'|| duplicacaoArquivoRetornoBean.habilitaFiltroDeCliente}" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_nome_razao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{duplicacaoArquivoRetornoBean.itemFiltroSelecionado != '2'|| duplicacaoArquivoRetornoBean.habilitaFiltroDeCliente}" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  border="0">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{duplicacaoArquivoRetornoBean.itemFiltroSelecionado != '3'|| duplicacaoArquivoRetornoBean.habilitaFiltroDeCliente}" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'pesqDuplicacaoArquivoRetorno','pesqDuplicacaoArquivoRetorno:txtBanco','pesqDuplicacaoArquivoRetorno:txtAgencia');"/>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{duplicacaoArquivoRetornoBean.itemFiltroSelecionado != '3'|| duplicacaoArquivoRetornoBean.habilitaFiltroDeCliente}" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'pesqDuplicacaoArquivoRetorno','pesqDuplicacaoArquivoRetorno:txtAgencia','pesqDuplicacaoArquivoRetorno:txtConta');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{duplicacaoArquivoRetornoBean.itemFiltroSelecionado != '3'|| duplicacaoArquivoRetornoBean.habilitaFiltroDeCliente}" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" />
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCamposCliente" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{duplicacaoArquivoRetornoBean.limparDadosCliente}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty duplicacaoArquivoRetornoBean.itemFiltroSelecionado|| duplicacaoArquivoRetornoBean.habilitaFiltroDeCliente}" value="#{msgs.conDuplicacaoArquivoRetorno_consultar_cliente}" action="#{duplicacaoArquivoRetornoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" 
	   		onclick="javascript:desbloquearTela(); 
	   		return validaCpoConsultaCliente('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conDuplicacaoArquivoRetorno_cnpj}','#{msgs.conDuplicacaoArquivoRetorno_cpf}', '#{msgs.conDuplicacaoArquivoRetorno_nome_razao}',
	   		'#{msgs.conDuplicacaoArquivoRetorno_banco}','#{msgs.conDuplicacaoArquivoRetorno_agencia}','#{msgs.conDuplicacaoArquivoRetorno_conta}');">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.cmpi0000_label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_identificacao_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
				
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="selEmpresaGestoraContrada" converter="longConverter" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaContratosPessoas.cdPessoaJuridicaContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
						<f:selectItems  value="#{duplicacaoArquivoRetornoBean.preencherListaEmpresaGestora}" />
						<brArq:commonsValidator type="required" arg="#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}" server="false" client="true"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="selTipoContrato" converter="inteiroConverter" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaContratosPessoas.cdTipoContratoNegocio}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
						<f:selectItems  value="#{duplicacaoArquivoRetornoBean.preencherTipoContrato}" />
						<brArq:commonsValidator type="required" arg="#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}" server="false" client="true"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			   <br:brPanelGroup>					
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" value="#{duplicacaoArquivoRetornoBean.entradaConsultarListaContratosPessoas.nrSeqContratoNegocio}" size="15" maxlength="10" id="txtNumeroContrato" />
				</br:brPanelGroup>				
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	 
	 <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<br:brPanelGroup style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCamposContrato" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{duplicacaoArquivoRetornoBean.limparDadosContrato}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
	   		<br:brCommandButton id="btoConsultarContrato" styleClass="bto1" value="#{msgs.PGIC0010_label_botao_consultar}" action="#{duplicacaoArquivoRetornoBean.pesquisarContratos}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript: desbloquearTela(); return validaCpoContrato('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}','#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}', '#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}', '#{duplicacaoArquivoRetornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}');">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
     <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_contrato}:"/>
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_descricao_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_situacao}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
		
    </br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_tipo_layout}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoLayout" value="#{duplicacaoArquivoRetornoBean.filtroTipoLayout}" style="margin-top:5px" disabled="#{duplicacaoArquivoRetornoBean.desabilataFiltro || duplicacaoArquivoRetornoBean.btoAcionado}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.conCadastroProcessosControle_label_combo_selecione}"/>
				<f:selectItems value="#{duplicacaoArquivoRetornoBean.listaTipoLayout}"/>
				<a4j:support status="statusAguarde" event="onchange" reRender="tipoRetorno" action="#{duplicacaoArquivoRetornoBean.listarTipoArquivoRetornoFiltro}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>				
		
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_retorno}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoRetorno" value="#{duplicacaoArquivoRetornoBean.filtroTipoArquivoRetorno}" style="margin-top:5px" disabled="#{duplicacaoArquivoRetornoBean.filtroTipoLayout == null || duplicacaoArquivoRetornoBean.filtroTipoLayout == 0 || duplicacaoArquivoRetornoBean.desabilataFiltro || duplicacaoArquivoRetornoBean.btoAcionado}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.conCadastroProcessosControle_label_combo_selecione}"/>
				<f:selectItems value="#{duplicacaoArquivoRetornoBean.listaTipoArquivoRetornoFiltro}"/>					
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
   
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conDuplicacaoArquivoRetorno_btn_limpar_campos}" action="#{duplicacaoArquivoRetornoBean.limparCampos}" style="margin-right:5px" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conDuplicacaoArquivoRetorno_btn_consultar}" action="#{duplicacaoArquivoRetornoBean.carregarGrid}" disabled="#{duplicacaoArquivoRetornoBean.desabilataFiltro || duplicacaoArquivoRetornoBean.btoAcionado}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	

		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
		
			<app:scrollableDataTable id="dataTable" value="#{duplicacaoArquivoRetornoBean.listaGridPesquisa}" var="result" 
			rows="10" rowIndexVar="parametroKey" 
			rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	

					<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false"
						value="#{duplicacaoArquivoRetornoBean.itemSelecionadoLista}" >
						<f:selectItems value="#{duplicacaoArquivoRetornoBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />					
					</t:selectOneRadio>
			    	<t:radio for="sor" index="#{parametroKey}" />
			    	
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conDuplicacaoArquivoRetorno_grid_contrato_empresa}" style="text-align:center;width:200"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsPessoaJuridVinc}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conDuplicacaoArquivoRetorno_grid_contrato_tipo}" style="text-align:center;width:300"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoContratoVinc}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conDuplicacaoArquivoRetorno_grid_contrato_numero}" style="text-align:center;width:200"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrSeqContratoVinc}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_tipo_retorno}"  style="width:250; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoArquivoRetorno}" />
				</app:scrollableColumn>		
				<app:scrollableColumn styleClass="colTabLeft" width="130px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conDuplicacaoArquivoRetorno_grid_tipo_layout}"  style="width:130; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoLayoutArquivo}" />
				</app:scrollableColumn>			

				<app:scrollableColumn styleClass="colTabLeft" width="130px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conDuplicacaoArquivoRetorno_grid_tipo_duplicacao}" style="width:130; text-align:center"  />
				    </f:facet>
				    <br:brOutputText value="#{msgs.conDuplicacaoArquivoRetorno_integral}" rendered="#{result.cdIndicadorDuplicidadeRetorno==1}"/>
   				    <br:brOutputText value="#{msgs.conDuplicacaoArquivoRetorno_parcial}" rendered="#{result.cdIndicadorDuplicidadeRetorno==2}"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="100px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conDuplicacaoArquivoRetorno_grid_instrucao}" style="width:100; text-align:center"  />
				    </f:facet>
				    <br:brOutputText value="#{msgs.conDuplicacaoArquivoRetorno_radio_duplicar}" rendered="#{result.cdInstrucaoEnvioRetorno==1}"/>
   				    <br:brOutputText value="#{msgs.conDuplicacaoArquivoRetorno_radio_redirecionar}" rendered="#{result.cdInstrucaoEnvioRetorno==2}"/>
				</app:scrollableColumn>				

			</app:scrollableDataTable>
			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{duplicacaoArquivoRetornoBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet> 
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btoLimpar" styleClass="bto1" value="Limpar" disabled="#{duplicacaoArquivoRetornoBean.desabilataFiltro}" action="#{duplicacaoArquivoRetornoBean.limparPaginaArgumentos}" style="cursor:hand; margin-right:5px;" onmouseout="javascript:alteraBotao('normal', this.id);">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnDetalhar" disabled="#{empty duplicacaoArquivoRetornoBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conDuplicacaoArquivoRetorno_btn_detalhar}" action="#{duplicacaoArquivoRetornoBean.detalhar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" disabled="#{duplicacaoArquivoRetornoBean.desabilataFiltro}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conDuplicacaoArquivoRetorno_btn_incluir}" action="#{duplicacaoArquivoRetornoBean.incluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" disabled="#{empty duplicacaoArquivoRetornoBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conDuplicacaoArquivoRetorno_btn_alterar}"  action="#{duplicacaoArquivoRetornoBean.alterar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty duplicacaoArquivoRetornoBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.conDuplicacaoArquivoRetorno_btn_excluir}" action="#{duplicacaoArquivoRetornoBean.excluir}" >				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</a4j:outputPanel>
	
	<t:inputHidden value="#{duplicacaoArquivoRetornoBean.habilitaFiltroDeCliente}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('pesqDuplicacaoArquivoRetorno:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>

</brArq:form>
