<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="pesqDuplicacaoArquivoRetornoIncluir" name="pesqDuplicacaoArquivoRetornoIncluir" >
<h:inputHidden id="hiddenRadioDuplicacao" value="#{duplicacaoArquivoRetornoBean.duplicacaoParticipanteMesmoContrato}"/>
<h:inputHidden id="hiddenRadioTipoDuplicacao" value="#{duplicacaoArquivoRetornoBean.tipoDuplicacao}"/>
<h:inputHidden id="hiddenRadioFiltroInstrucao" value="#{duplicacaoArquivoRetornoBean.instrucao}"/>
<h:inputHidden id="hiddenObrigatoriedade" value="#{duplicacaoArquivoRetornoBean.obrigatoriedade}"/>
<h:inputHidden id="hiddenCpfCnpjParticipante" value="#{duplicacaoArquivoRetornoBean.participanteCpfCnpj}"/>


<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contrato_origem}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_cpf_cnpj_origem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_nome_razao_origem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_empresa_origem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>
		</br:brPanelGroup>			
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_situacao_origem}:"  />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_tipo_origem}:"  />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
		
    </br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_numero_origem} do Contrato:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px;" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_descricao_contrato_origem}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		
    </br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_duplicacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_tipo_layout}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
				
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				<br:brPanelGroup>
					<br:brSelectOneMenu id="tipoLayout" value="#{duplicacaoArquivoRetornoBean.cdTipoLayout}" style="margin-top:5px">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conDuplicacaoArquivoRetorno_label_combo_selecione}"/>
						<f:selectItems value="#{duplicacaoArquivoRetornoBean.listaTipoLayout}"/>	
						<a4j:support status="statusAguarde" event="onchange" reRender="tipoArquivo" action="#{duplicacaoArquivoRetornoBean.listarTipoArquivoRetorno}"/>				
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_retorno}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoArquivo" value="#{duplicacaoArquivoRetornoBean.cdTipoArquivo}" style="margin-top:5px" disabled="#{duplicacaoArquivoRetornoBean.cdTipoLayout == null || duplicacaoArquivoRetornoBean.cdTipoLayout == 0}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conDuplicacaoArquivoRetorno_label_combo_selecione}"/>
						<f:selectItems value="#{duplicacaoArquivoRetornoBean.listaTipoArquivoRetorno}"/>					
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	 
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_duplicacao_participante_mesmo_contrato}:"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brSelectOneRadio id="radioDuplicacao" styleClass="HtmlSelectOneRadioBradesco" value="#{duplicacaoArquivoRetornoBean.duplicacaoParticipanteMesmoContrato}">
					<f:selectItem itemLabel="#{msgs.incDuplicacaoArquivoRetorno_sim}" itemValue="1" />   
		            <f:selectItem itemLabel="#{msgs.incDuplicacaoArquivoRetorno_nao}" itemValue="2" />   
		            <a4j:support event="onclick" reRender="btoParticipantes, hiddenRadioDuplicacao, hiddenCpfCnpjParticipante, txtCpfCnpjParticipante, txtNomeRazaoSocialParticipante,btoConsultarContrato,panelContrato,btnAvancar,panelParticipante" action="#{duplicacaoArquivoRetornoBean.habilitaParticipantes}" /> 
		    	</br:brSelectOneRadio>
		    </br:brPanelGroup>	
		 </br:brPanelGrid>	
		 
		 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	 
			 <br:brPanelGroup style="width:20px;" >
			</br:brPanelGroup>		
		 </br:brPanelGrid>	
		 
		 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	 
			 <br:brPanelGroup  >
					<br:brCommandButton id="btoParticipantes" styleClass="bto1" value="#{msgs.btn_pesquisar}" action="#{duplicacaoArquivoRetornoBean.participantesIncluir}"  disabled="#{! duplicacaoArquivoRetornoBean.btoConsultarParticipantes}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>	
		</br:brPanelGrid>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	 
		 <br:brPanelGroup style="width:6px;" >
		</br:brPanelGroup>		
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.cmpi0000_label_cpf_cnpj}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" id="txtCpfCnpjParticipante" value="#{duplicacaoArquivoRetornoBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px;" >
		</br:brPanelGroup>	
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_nome_razao}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" id="txtNomeRazaoSocialParticipante" value="#{duplicacaoArquivoRetornoBean.nomeRazaoSocialParticipante}"/>
		</br:brPanelGroup>
	 </br:brPanelGrid>	
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	 
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_tipo_duplicacao}:"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brSelectOneRadio id="radioTipoDuplicacao" styleClass="HtmlSelectOneRadioBradesco" value="#{duplicacaoArquivoRetornoBean.tipoDuplicacao}">
					<f:selectItem itemLabel="#{msgs.incDuplicacaoArquivoRetorno_integral}" itemValue="1" />   
		            <f:selectItem itemLabel="#{msgs.incDuplicacaoArquivoRetorno_parcial}" itemValue="2" /> 
		            <a4j:support event="onclick" reRender="hiddenRadioTipoDuplicacao" /> 
		    	</br:brSelectOneRadio>
		    </br:brPanelGroup>			
		</br:brPanelGrid>	
	
	 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	 
			<br:brPanelGroup style="width:20px;" >
			</br:brPanelGroup>		
		</br:brPanelGrid>	
    
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incDuplicacaoArquivoRetorno_instrucao}:"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brSelectOneRadio id="radioInstrucao" styleClass="HtmlSelectOneRadioBradesco" value="#{duplicacaoArquivoRetornoBean.instrucao}">
					<f:selectItem itemLabel="#{msgs.incDuplicacaoArquivoRetorno_radio_duplicar}" itemValue="1" />  
		            <f:selectItem itemLabel="#{msgs.incDuplicacaoArquivoRetorno_radio_redirecionar}" itemValue="2" /> 
		            <a4j:support event="onclick" reRender="hiddenRadioFiltroInstrucao" />
		    	</br:brSelectOneRadio>
		    </br:brPanelGroup>			
	    </br:brPanelGrid>
     </br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">	
		 <br:brPanelGrid styleClass="mainPanel" columns="1"  cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_identificacao_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	 
			<br:brPanelGroup style="width:115px;" >
			</br:brPanelGroup>		
		</br:brPanelGrid>	
		
		 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	 
			 <br:brPanelGroup >
				<br:brCommandButton id="btoConsultarContrato" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{duplicacaoArquivoRetornoBean.pesquisarContratoIncluir}" disabled="#{duplicacaoArquivoRetornoBean.duplicacaoParticipanteMesmoContrato != 2}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_contrato}:"/>
	 
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGroup id="panelContrato" >
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}:"  />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" id="empresaGestora" value="#{duplicacaoArquivoRetornoBean.dsPessoaJuridicaClienteContrato}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>	
			
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}:"  />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.dsTipoContratoNegocioClienteContrato}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}:"  />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.nrSeqContratoNegocioClienteContrato}"/>
			</br:brPanelGroup>
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>	
		
			<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_descricao_contrato}:"  />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.dsContrato}"/>
			</br:brPanelGroup>
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>	
			
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_situacao}:"  />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.dsSituacaoContratoNegocio}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGroup>

	<f:verbatim> <hr class="lin"> </f:verbatim>
	
	 <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_participante}:"/>
	 
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGroup id="panelParticipante" >
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.participanteCpfCnpj}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>
			
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao_social}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.participanteNomeRazaoSocial}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.participanteNivelParticipacao}"/>
			</br:brPanelGroup>

			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_situacaoParticipacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{duplicacaoArquivoRetornoBean.participanteSituacaoParticipacao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGroup>

	<f:verbatim> <hr class="lin"> </f:verbatim>

    <br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
    	<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.incDuplicacaoArquivoRetorno_btn_voltar}" action="#{duplicacaoArquivoRetornoBean.voltarPesquisar}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
    
    	<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" onclick="javascript:checaCamposObrigatorios(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.incDuplicacaoArquivoRetorno_tipo_layout}', '#{msgs.label_tipo_retorno}', '#{msgs.incDuplicacaoArquivoRetorno_duplicacao_participante_mesmo_contrato}', '#{msgs.incDuplicacaoArquivoRetorno_tipo_duplicacao}',
			 '#{msgs.incDuplicacaoArquivoRetorno_instrucao}', '#{msgs.label_participante}', '#{msgs.label_identificacao_contrato}')"  styleClass="bto1" value="#{msgs.incDuplicacaoArquivoRetorno_btn_avancar}" action="#{duplicacaoArquivoRetornoBean.avancarIncluirConfirmar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
    </br:brPanelGrid>	
	
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
