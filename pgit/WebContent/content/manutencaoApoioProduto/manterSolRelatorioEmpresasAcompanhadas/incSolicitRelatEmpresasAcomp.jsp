<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>

<brArq:form id="incSolicitRelatEmpresasAcomp" name="incSolicitRelatEmpresasAcomp">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina"  cellpadding="0" cellspacing="0">
		<br:brPanelGrid style="margin-top:15px" />

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_empresaAcomp_dadosSolic}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid style="margin-top:11px" />

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresaAcomp_userSolicitante}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>
		    <br:brPanelGrid  style="margin-left:5px;">
			    <br:brPanelGroup>
			    	<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{solicitacaoRelatorioEmpresasAcompBean.saidaInclusaoEmpresaAcompanhada.cdUsuarioInclusao}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<br:brPanelGrid style="margin-top:10px" />
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresaAcomp_dtSolicitacao}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>
		    <br:brPanelGrid  style="margin-left:5px;">
			    <br:brPanelGroup>
			    	<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{solicitacaoRelatorioEmpresasAcompBean.saidaInclusaoEmpresaAcompanhada.dtPagtoIntegrado}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin" /></f:verbatim>

		<br:brPanelGrid columns="2" width="100%" >	
			<br:brPanelGroup  style="float:left" >
				<br:brCommandButton id="btnvoltar"
				 styleClass="bto1"
				 value="#{msgs.btn_voltar}"
				 action="#{solicitacaoRelatorioEmpresasAcompBean.voltarGeral}"
				 style="margin-right:5px">
				 <brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>	
			<br:brPanelGroup  style="float:right" >
				<br:brCommandButton id="btnConfirmar"
				 styleClass="bto1"
				 value="#{msgs.btn_confirmar}"  onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela();  return false; }"
				 action="#{solicitacaoRelatorioEmpresasAcompBean.confirmarIncluir}">
				 <brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	</br:brPanelGrid>
</brArq:form>