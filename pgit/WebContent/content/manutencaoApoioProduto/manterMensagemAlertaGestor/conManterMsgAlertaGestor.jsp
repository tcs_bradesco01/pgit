<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="pesqManterMensagemAlertaGestorForm" name="pesqManterMensagemAlertaGestorForm" >
<h:inputHidden id="hiddenObrigatoriedadeConsultar" value="#{manterMensagemAlertaGestorBean.obrigatoriedadeConsultar}"/>

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMsgAlertaGestor_label_centro_custo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<a4j:region renderRegionOnly="true">
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterMensagemAlertaGestorBean.inicialCentroCustoFiltro}" size="5" maxlength="4" id="txtInicioCentroCusto" disabled="#{manterMensagemAlertaGestorBean.btoAcionado}"/>
					<f:verbatim>&nbsp;</f:verbatim>
					<br:brGraphicImage url="/images/lupa.gif" style="cursor: hand;">
						<a4j:support event="onclick" action="#{manterMensagemAlertaGestorBean.buscarCentroCusto}" reRender="centroCusto" status="statusAguarde"/>
					</br:brGraphicImage>
					<f:verbatim>&nbsp;</f:verbatim>
					<f:verbatim>&nbsp;</f:verbatim>
					<br:brSelectOneMenu id="centroCusto" value="#{manterMensagemAlertaGestorBean.centroCustoFiltro}"   disabled="#{empty manterMensagemAlertaGestorBean.listaCentroCustoFiltro || manterMensagemAlertaGestorBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterMsgAlertaGestor_label_combo_selecione}"/>
						<f:selectItems value="#{manterMensagemAlertaGestorBean.listaCentroCustoFiltro}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
			</a4j:region>		
			
		</br:brPanelGroup>	
	</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMsgAlertaGestor_label_tipo_processo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoProcesso" value="#{manterMensagemAlertaGestorBean.tipoProcessoFiltro}" disabled="#{manterMensagemAlertaGestorBean.btoAcionado}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterMsgAlertaGestor_label_combo_selecione}"/>
						<f:selectItems value="#{manterMensagemAlertaGestorBean.listaTipoProcessoFiltro}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>	

	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >		
	<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMsgAlertaGestor_recurso}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="recurso" value="#{manterMensagemAlertaGestorBean.recursoFiltro}" disabled="#{manterMensagemAlertaGestorBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterMsgAlertaGestor_label_combo_selecione}"/>
						<f:selectItems value="#{manterMensagemAlertaGestorBean.listaRecursoFiltro}"/>
						<a4j:support event="onchange" reRender="idioma,tipoMensagem" action="#{manterMensagemAlertaGestorBean.limparCamposRecurso}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMsgAlertaGestor_idioma}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="idioma" value="#{manterMensagemAlertaGestorBean.idiomaFiltro}" disabled="#{manterMensagemAlertaGestorBean.recursoFiltro == 0  || manterMensagemAlertaGestorBean.recursoFiltro == null || manterMensagemAlertaGestorBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterMsgAlertaGestor_label_combo_selecione}"/>
						<f:selectItems value="#{manterMensagemAlertaGestorBean.listaIdiomaFiltro}"/>
						<a4j:support event="onchange" reRender="tipoMensagem" action="#{manterMensagemAlertaGestorBean.limparCamposIdioma}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_da_mensagem}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    
			    	<br:brInputText size="10" maxlength="4" onkeypress="onlyNum();" id="tipoMensagem" value="#{manterMensagemAlertaGestorBean.tipoMensagemFiltro}" disabled="#{manterMensagemAlertaGestorBean.idiomaFiltro == 0 || manterMensagemAlertaGestorBean.idiomaFiltro == null
					|| manterMensagemAlertaGestorBean.recursoFiltro == 0 || manterMensagemAlertaGestorBean.recursoFiltro == null || manterMensagemAlertaGestorBean.btoAcionado}"
  					onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">  
				    	<brArq:commonsValidator type="integer" arg="#{msgs.label_codigo_da_mensagem}" server="false" client="true" />
				    </br:brInputText>	
			
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoFuncionario_codigo_funcionario}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" value="#{manterMensagemAlertaGestorBean.codFuncionarioFiltro}" disabled="#{manterMensagemAlertaGestorBean.btoAcionado}" size="13" maxlength="10" id="txtCodigoFunc"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"			    	>
					</br:brInputText>	    		  	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="Limpar Campos" action="#{manterMensagemAlertaGestorBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conManterMsgAlertaGestor_label_botao_consultar}" action="#{manterMensagemAlertaGestorBean.consultar}"  onclick=" javascript:  checaCamposObrigatoriosConsultar(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.conManterMsgAlertaGestor_label_centro_custo}', '#{msgs.conManterMsgAlertaGestor_label_tipo_processo}'); return validateForm(document.forms[1]);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	

		
				<app:scrollableDataTable id="dataTable" value="#{manterMensagemAlertaGestorBean.listaGridPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{manterMensagemAlertaGestorBean.itemSelecionadoLista}">
						<f:selectItems value="#{manterMensagemAlertaGestorBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			    	<t:radio for="sor" index="#{parametroKey}" />
				</app:scrollableColumn>
				
			  	<app:scrollableColumn styleClass="colTabLeft" width="160px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterMsgAlertaGestor_label_centro_custo}" style="text-align:center;width:160" />
			    </f:facet>
			    <br:brOutputText value="#{result.centroCusto}" />
				</app:scrollableColumn>
				<app:scrollableColumn styleClass="colTabLeft" width="160px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterMsgAlertaGestor_label_tipo_processo}" style="text-align:center;width:160" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoProcesso}" />
				</app:scrollableColumn>
				<app:scrollableColumn styleClass="colTabLeft" width="190px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterMsgAlertaGestor_recurso}" style="text-align:center;width:190" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsRecurso}" />
			  </app:scrollableColumn>
				<app:scrollableColumn styleClass="colTabLeft" width="190px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterMsgAlertaGestor_idioma}" style="text-align:center;width:190" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsIdioma}" />
			  </app:scrollableColumn>
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_codigo_da_mensagem}" style="text-align:center;width:300" />
			    </f:facet>
			    <br:brOutputText value="#{result.tipoMensagem} - #{result.dsTipoMensagem}" />
			  </app:scrollableColumn>
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterMsgAlertaGestor_label_funcionario}" style="text-align:center;width:300"/>
			    </f:facet>
			    <br:brOutputText value="#{result.cdPessoa} - #{result.dsPessoa}" />
			  </app:scrollableColumn>
			</app:scrollableDataTable>
			

		</br:brPanelGroup>
		
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterMensagemAlertaGestorBean.pesquisar}" >
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	 
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterMensagemAlertaGestorBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterMsgAlertaGestor_path_detalhar}" action="#{manterMensagemAlertaGestorBean.detalhar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterMsgAlertaGestor_path_incluir}" action="#{manterMensagemAlertaGestorBean.incluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterMsgAlertaGestor_path_alterar}" action="#{manterMensagemAlertaGestorBean.alterar}" disabled="#{empty manterMensagemAlertaGestorBean.itemSelecionadoLista}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty manterMensagemAlertaGestorBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.conManterMsgAlertaGestor_path_excluir}" action="#{manterMensagemAlertaGestorBean.excluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
