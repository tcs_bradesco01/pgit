<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>


<brArq:form id="incluirManterMensagemAlertaGestorForm" name="incluirManterMensagemAlertaGestorForm" >


<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterMsgAlertaGestor_label_mensagem_alerta}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMsgAlertaGestor_label_centro_custo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		       	<br:brPanelGroup>
   			    	<br:brInputText styleClass="HtmlInputTextBradesco"  value="#{manterMensagemAlertaGestorBean.centroCusto}" size="10" maxlength="4" id="txtInicioCentroCusto">
				    </br:brInputText>	
   				</br:brPanelGroup>					
   				 <br:brPanelGroup>   				 
					<h:commandLink id="link" action="#{manterMensagemAlertaGestorBean.buscarCentroCustoIncluir}" >
						<br:brGraphicImage url="/images/ficha.gif" style="margin-right:5px;margin-left:5px"   />
					</h:commandLink>   				 
   				</br:brPanelGroup>						
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="centroCusto" value="#{manterMensagemAlertaGestorBean.centroCustoCombo}" disabled="#{empty manterMensagemAlertaGestorBean.listaCentroCustoCombo}" >
						<f:selectItem itemValue="" itemLabel="#{msgs.conManterMsgAlertaGestor_label_combo_selecione}"/>
						<f:selectItems value="#{manterMensagemAlertaGestorBean.listaCentroCustoCombo}"/>
					</br:brSelectOneMenu>
	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		
	</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMsgAlertaGestor_label_tipo_processo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoProcesso" value="#{manterMensagemAlertaGestorBean.tipoProcesso}"  >
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterMsgAlertaGestor_label_combo_selecione}"/>
						<f:selectItems value="#{manterMensagemAlertaGestorBean.listaTipoProcessoFiltro}"/>
					</br:brSelectOneMenu>

				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		
 	</br:brPanelGrid>		
 	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMsgAlertaGestor_recurso}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup style="margin-right:20px">
					<br:brSelectOneMenu id="recurso" value="#{manterMensagemAlertaGestorBean.recurso}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterMsgAlertaGestor_label_combo_selecione}"/>
						<f:selectItems value="#{manterMensagemAlertaGestorBean.listaRecursoFiltro}"/>	
						<a4j:support event="onchange" reRender="idioma,tipoMensagem" action="#{manterMensagemAlertaGestorBean.limparCamposRecursoCombo}"/>					
					</br:brSelectOneMenu>				
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>
		
				<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMsgAlertaGestor_idioma}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup style="margin-right:20px">
					<br:brSelectOneMenu id="idioma" value="#{manterMensagemAlertaGestorBean.idioma}" disabled="#{manterMensagemAlertaGestorBean.recurso == 0  || manterMensagemAlertaGestorBean.recurso == null}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterMsgAlertaGestor_label_combo_selecione}"/>
						<f:selectItems value="#{manterMensagemAlertaGestorBean.listaIdiomaFiltro}"/>
						<a4j:support event="onchange" reRender="tipoMensagem" action="#{manterMensagemAlertaGestorBean.limparCamposIdiomaCombo}"/>					
					</br:brSelectOneMenu>				
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_da_mensagem}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brInputText size="15" maxlength="4" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="tipoMensagem" value="#{manterMensagemAlertaGestorBean.tipoMensagem}" onkeypress="onlyNum();" disabled="#{manterMensagemAlertaGestorBean.idioma == 0 || manterMensagemAlertaGestorBean.idioma == null
					|| manterMensagemAlertaGestorBean.recurso == 0 || manterMensagemAlertaGestorBean.recurso == null }">  
				    	<brArq:commonsValidator type="integer" arg="#{msgs.conManterMsgAlertaGestor_label_tipo_mensagem}" server="false" client="true" />
				    </br:brInputText>			    
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>	
	</br:brPanelGrid>	
		
					
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detManterMsgAlertaGestor_meio_transmissao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
<h:inputHidden id="hiddenRadioMeioTransmissao" value="#{manterMensagemAlertaGestorBean.meioTrasmissao}"/>
<h:inputHidden id="hiddenFuncionario" value="#{manterMensagemAlertaGestorBean.codigoFuncionarioSelecionado}"/>
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterMensagemAlertaGestorBean.obrigatoriedade}"/>
<h:inputHidden id="hiddenObrigatoriedadeFuncionario" value="#{manterMensagemAlertaGestorBean.obrigatoriedadeFuncionario}"/>
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneRadio id="radioMeioTransmissao" styleClass="HtmlSelectOneRadioBradesco" value="#{manterMensagemAlertaGestorBean.meioTrasmissao}"  >
						<f:selectItem itemLabel="#{msgs.incManterMsgAlertaGestor_label_email}" itemValue="1" />  
			            <f:selectItem itemLabel="#{msgs.incManterMsgAlertaGestor_label_sms}" itemValue="2" />  
        			    <a4j:support event="onclick" reRender="hiddenRadioMeioTransmissao" />	 
			    	</br:brSelectOneRadio>
			    </br:brPanelGroup>	
			</br:brPanelGrid>
										
		</br:brPanelGroup>		

	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterMsgAlertaGestor_label_argumentos_pesquisa_funcionario}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoFuncionario_codigo_funcionario}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" value="#{manterMensagemAlertaGestorBean.codFuncionario}" size="13" maxlength="9" id="txtCodigoFunc"
			    	onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
			    		<a4j:support event="onblur" reRender="panelCodigo,panelFuncionario,panelJuncao" action="#{manterMensagemAlertaGestorBean.limparUsuarioConsulta}"/>
					</br:brInputText>	    		  	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	 	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton styleClass="bto1" value="#{msgs.label_botao_consultar}" action="#{manterMensagemAlertaGestorBean.carregaListaFuncionarios}" id="btnConsultar" disabled="false"
					onclick="checaCamposObrigatoriosIncluirFuncionario(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.identificacaoFuncionario_codigo_funcionario}')"						 >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>	
	 
	<br:brPanelGrid columns="3"  cellpadding="0" cellspacing="0" >
			<br:brPanelGroup id="panelCodigo">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_codigo_funcionario}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterMensagemAlertaGestorBean.funcionarioDTO.cdFuncionario}"  />
			</br:brPanelGroup>
			<br:brPanelGroup id="panelFuncionario">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_nome_funcionario}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterMensagemAlertaGestorBean.funcionarioDTO.dsFuncionario}"  />
			</br:brPanelGroup>	
			<br:brPanelGroup id="panelJuncao">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_juncao_pertecente}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterMensagemAlertaGestorBean.funcionarioDTO.cdJuncao}"  />
			</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterMensagemAlertaGestorBean.voltarPesquisa}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnConfirmar" styleClass="bto1" disabled="false"  value="#{msgs.incCadastroProcessosControle_label_botao_avancar}"  action="#{manterMensagemAlertaGestorBean.avancarIncluir}" onclick="javascript: return checaCamposObrigatoriosIncluir(document.forms[1], 
			 '#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.conManterMsgAlertaGestor_label_centro_custo}', '#{msgs.conManterMsgAlertaGestor_label_tipo_processo}', '#{msgs.conManterMsgAlertaGestor_label_codigo_mensagem}', '#{msgs.conManterMsgAlertaGestor_prefixo}', '#{msgs.detManterMsgAlertaGestor_meio_transmissao}',
			   '#{msgs.conManterMsgAlertaGestor_recurso}', '#{msgs.conManterMsgAlertaGestor_idioma}','#{msgs.identificacaoFuncionario_funcionario_valido}' ) ">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
</br:brPanelGrid>


<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
