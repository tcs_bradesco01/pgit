<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>

<brArq:form id="altManterClientesOrgaoPublicoFederalForm" name="altManterClientesOrgaoPublicoFederalForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina"  cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_numContratoPGIT}:"/>
				</br:brPanelGroup>
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterClientesOrgaoPublicoFederalBean.nrSequenciaContratoNegocio}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterClientesOrgaoPublicoFederalBean.cpfCnpjFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterClientesOrgaoPublicoFederalBean.dsNomeRazaoSocial}"/>
		</br:brPanelGroup>
		
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_agencia}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterClientesOrgaoPublicoFederalBean.codAgenciaDesc}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_conta}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterClientesOrgaoPublicoFederalBean.contaDigito}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGroup>			
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
		<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_grid_situacao}:" />
		<br:brOutputText id="descricaoSituacao" styleClass="HtmlOutputTextBoldBradesco" value="#{manterClientesOrgaoPublicoFederalBean.dsSituacaoRegistro}"/>
	</br:brPanelGroup>
	

	<f:verbatim><hr class="lin"> </f:verbatim>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_dataHrInclusao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterClientesOrgaoPublicoFederalBean.saidaDetalharPartOrgaoPublico.dataHoraInclusao}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_usuario}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterClientesOrgaoPublicoFederalBean.saidaDetalharPartOrgaoPublico.cdUsuarioInclusao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
   	<f:verbatim><hr class="lin"> </f:verbatim>
    	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_dataHrManutencao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterClientesOrgaoPublicoFederalBean.saidaDetalharPartOrgaoPublico.dataHoraManut}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_usuario}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterClientesOrgaoPublicoFederalBean.saidaDetalharPartOrgaoPublico.cdUsuarioManutencao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup>
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterClienteOrgaoPublico_situacao}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
	    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
		    <br:brPanelGroup>
		    	<br:brSelectOneMenu id="situacaoCliente"  value="#{manterClientesOrgaoPublicoFederalBean.filtroSituacaoClienteAlterar}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px">						
					<f:selectItem itemValue="0" itemLabel="Selecione"/>								
					<f:selectItem itemValue="1" itemLabel="Ativo"/>								
					<f:selectItem itemValue="2" itemLabel="Inativo"/>		
				</br:brSelectOneMenu>
			</br:brPanelGroup>					
		</br:brPanelGrid>
	</br:brPanelGroup>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: left" ajaxRendered="true">	
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px"  >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.manterClientesOrgaoPublicoFederal_btn_voltar}" action="#{manterClientesOrgaoPublicoFederalBean.voltarAlterar}">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
			
			<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnConfirmar" styleClass="bto1" value="#{msgs.excManterSolicitacaoRelContrato_btn_confirmar}"
					 action="#{manterClientesOrgaoPublicoFederalBean.confirmarAlterarPartOrgaoPublico}"
					 onclick="	if(!validaSituacaoAlterar()){desbloquearTela(); return false;}">				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	</a4j:outputPanel>	

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>