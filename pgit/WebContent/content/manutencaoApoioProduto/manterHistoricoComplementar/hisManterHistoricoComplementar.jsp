<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="hisManterHistoricoComplementarForm" name="hisManterHistoricoComplementarForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.hisManterHistoricoComplementar_argumentopesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_segunda_linha_extrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterHistoricoComplementarBean.codigoHistoricoComplementarFiltro2}" size="25" maxlength="5" id="txtCodigoHistoricoComplementar" disabled="#{manterHistoricoComplementarBean.btoAcionadoHistorico}">
			    		<brArq:commonsValidator type="integer" arg="#{msgs.label_codigo_segunda_linha_extrato}" server="false" client="true" />
			    		<brArq:commonsValidator type="required" arg="#{msgs.label_codigo_segunda_linha_extrato}" server="false" client="true" />
			    	</br:brInputText>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.hisManterHistoricoComplementar_periodoManutencao}:"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
			
	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >				
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.label_de}:"/>
			<br:brPanelGroup rendered="#{!manterHistoricoComplementarBean.btoAcionadoHistorico}">
				<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioDe');" id="calendarioDe" value="#{manterHistoricoComplementarBean.filtroDataDe}" >
					<brArq:commonsValidator type="required" arg="#{msgs.hisManterHistoricoComplementar_periodo_inicial_solicitacao}" server="false" client="true"/>
				</app:calendar>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterHistoricoComplementarBean.btoAcionadoHistorico}" >
				<app:calendar id="calendarioDeDes" value="#{manterHistoricoComplementarBean.filtroDataDe}" disabled="true">
				</app:calendar>
			</br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_ate}:"/>
			<br:brPanelGroup rendered="#{!manterHistoricoComplementarBean.btoAcionadoHistorico}">
				<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioAte');" id="calendarioAte" value="#{manterHistoricoComplementarBean.filtroDataAte}" >
					<brArq:commonsValidator type="required" arg="#{msgs.hisManterHistoricoComplementar_periodo_final_solicitacao}" server="false" client="true"/>
				</app:calendar>	
			</br:brPanelGroup>		
			<br:brPanelGroup rendered="#{manterHistoricoComplementarBean.btoAcionadoHistorico}">
				<app:calendar id="calendarioAteDes" value="#{manterHistoricoComplementarBean.filtroDataAte}" disabled="true">
				</app:calendar>	
			</br:brPanelGroup>
		</br:brPanelGroup>	
	</a4j:outputPanel>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.hisManterHistoricoComplementar_limparcampos}" action="#{manterHistoricoComplementarBean.limparCamposHistorico}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.hisManterHistoricoComplementar_consultar}" action="#{manterHistoricoComplementarBean.carregaListaHistorico}" onclick="javascript: return validateForm(document.forms[1]);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
			<app:scrollableDataTable id="dataTableHist" value="#{manterHistoricoComplementarBean.listaGrid2}" var="result" rows="10" 
			rowIndexVar="parametroKey" 
			rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >
			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
				<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
					value="#{manterHistoricoComplementarBean.itemSelecionadoLista2}">
					<f:selectItems value="#{manterHistoricoComplementarBean.listaControle2}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sorLista" index="#{parametroKey}" />
			</app:scrollableColumn>			

			<app:scrollableColumn  width="200px" styleClass="colTabRight">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_codigo_segunda_linha_extrato}" style="width:200; text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.codigoHistoricoComplementar}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="150px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.hisManterHistoricoComplementar_label_grid_dataManutencao}" style="width:150; text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.dataManutencaoDesc}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="120px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.hisManterHistoricoComplementar_label_grid_responsavel}" style="width:120; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.responsavel}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="150px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.hisManterHistoricoComplementar_label_grid_tipomanutencao}" style="width:150; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{msgs.hisManterHistoricoComplementar_inclusao}" rendered = "#{result.tipoManutencao==1}" />
			    <br:brOutputText value="#{msgs.hisManterHistoricoComplementar_alteracao}" rendered = "#{result.tipoManutencao==2}" />
			    <br:brOutputText value="#{msgs.incManterHistoricoComplementar_exclusao}" rendered = "#{result.tipoManutencao==3}" />
			 </app:scrollableColumn>

			</app:scrollableDataTable>				 

		</br:brPanelGroup>
	</br:brPanelGrid>		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTableHist" actionListener="#{manterHistoricoComplementarBean.pesquisarHistorico}">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
		<f:verbatim><hr class="lin"> </f:verbatim>
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
 	<br:brPanelGrid columns="3" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" border="0">	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" disabled="False" value="#{msgs.hisManterHistoricoComplementar_voltar}" action="#{manterHistoricoComplementarBean.historicoVoltar}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnDetalhar"  styleClass="bto1" disabled="#{empty manterHistoricoComplementarBean.itemSelecionadoLista2}" value="#{msgs.hisManterHistoricoComplementar_detalhar}" action="#{manterHistoricoComplementarBean.historicoDetalhar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>		
	
	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
