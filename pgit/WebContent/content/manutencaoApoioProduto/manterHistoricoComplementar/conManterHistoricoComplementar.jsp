<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterHistoricoComplementarForm" name="conManterHistoricoComplementarForm" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterHistoricoComplementar_argumentopesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio value="#{manterHistoricoComplementarBean.radioArgumentoPesquisa}" disabled="#{manterHistoricoComplementarBean.btoAcionado}" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<a4j:support event="onclick" action="#{manterHistoricoComplementarBean.limparCampos}" reRender="txtCodigoHistoricoComplementar, tipoServico, modalidadeServico" />
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="0" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_segunda_linha_extrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0">	
			    <br:brPanelGroup>
				</br:brPanelGroup>	
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>	
			    	<br:brInputText readonly="#{manterHistoricoComplementarBean.radioArgumentoPesquisa != 0}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();"
			    		styleClass="HtmlInputTextBradesco" value="#{manterHistoricoComplementarBean.codigoHistoricoComplementarFiltro}" size="30" maxlength="5" id="txtCodigoHistoricoComplementar" disabled="#{manterHistoricoComplementarBean.btoAcionado}">
			    		<brArq:commonsValidator type="integer" arg="#{msgs.conManterHistoricoComplementar_label_codigoHistoricoComplementar}" server="false" client="true" />
			    	</br:brInputText>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid style="margin-top:6px" cellpadding="0" cellspacing="0" >	
	    <br:brPanelGroup >
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="1" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterHistoricoComplementar_label_tipoServico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>		
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>	
					<br:brSelectOneMenu id="tipoServico" value="#{manterHistoricoComplementarBean.codTipoServicoFiltro}" disabled="#{manterHistoricoComplementarBean.radioArgumentoPesquisa != 1 || manterHistoricoComplementarBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterHistoricoComplementar_label_combo_selecione}"/>
						<f:selectItems value="#{manterHistoricoComplementarBean.listaTipoServico}"/>
							<a4j:support event="onchange" action="#{manterHistoricoComplementarBean.listarModalidadeServicoFiltro}" reRender="modalidadeServico" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>		    
		    </br:brPanelGrid>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px">
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterHistoricoComplementar_label_modalidadeServico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>		
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>	
					<br:brSelectOneMenu id="modalidadeServico" value="#{manterHistoricoComplementarBean.codModalidadeServicoFiltro}" disabled="#{manterHistoricoComplementarBean.radioArgumentoPesquisa != 1 || manterHistoricoComplementarBean.codTipoServicoFiltro == 0 || manterHistoricoComplementarBean.codTipoServicoFiltro == null || manterHistoricoComplementarBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterHistoricoComplementar_label_combo_selecione}"/>
						<f:selectItems value="#{manterHistoricoComplementarBean.listaModalidadeServicoFiltro}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>		    
		    </br:brPanelGrid>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.conManterHistoricoComplementar_btn_limparcampos}" action="#{manterHistoricoComplementarBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conManterHistoricoComplementar_btn_consultar}" action="#{manterHistoricoComplementarBean.carregaLista}" onclick="javascript: return validateForm(document.forms[1]);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
		
			<app:scrollableDataTable id="dataTable" value="#{manterHistoricoComplementarBean.listaGrid}" var="result" rows="10" 
			rowIndexVar="parametroKey" 
			rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
				<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
					value="#{manterHistoricoComplementarBean.itemSelecionadoLista}">
					<f:selectItems value="#{manterHistoricoComplementarBean.listaControle}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sorLista" index="#{parametroKey}" />
			</app:scrollableColumn>			

			<app:scrollableColumn  width="200px" styleClass="colTabRight">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_codigo_segunda_linha_extrato}" style="width:200; text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.codLancamentoPersonalizado}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="50px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterHistoricoComplementar_label_grid_restricao}"  style="width:50; text-align:center"/>
			    </f:facet>
			  	 <br:brOutputText value="#{msgs.incManterHistoricoComplementar_label_sim}" rendered = "#{result.restricao==1}" />
			  	 <br:brOutputText value="#{msgs.incManterHistoricoComplementar_label_nao}" rendered = "#{result.restricao==2}" />
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn  width="200px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterHistoricoComplementar_label_grid_tipoLancamento}"  style="width:200; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{msgs.incManterHistoricoComplementar_tipolancamento_extratodebito}" rendered = "#{result.tipoLancamento==1}" />
			    <br:brOutputText value="#{msgs.incManterHistoricoComplementar_tipolancamento_extratocredito}" rendered = "#{result.tipoLancamento==2}" />
			    <br:brOutputText value="#{msgs.incManterHistoricoComplementar_tipolancamento_extratocreditoDebito}" rendered = "#{result.tipoLancamento==3}" />
			 </app:scrollableColumn>

			<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterHistoricoComplementar_label_grid_tipoServico}"  style="width:250; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoServico}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterHistoricoComplementar_label_grid_modalidadeServico}" style="width:250; text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsModalidadeServico}"/>
			 </app:scrollableColumn>
			 
			</app:scrollableDataTable>				 

		</br:brPanelGroup>
	</br:brPanelGrid>	
 	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterHistoricoComplementarBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterHistoricoComplementarBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.conManterHistoricoComplementar_btn_detalhar}" action="#{manterHistoricoComplementarBean.detalhar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="bto1" disabled="False" style="margin-right:5px" value="#{msgs.conManterHistoricoComplementar_btn_incluir}" action="#{manterHistoricoComplementarBean.incluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAlterar" styleClass="bto1" disabled="#{empty manterHistoricoComplementarBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.conManterHistoricoComplementar_btn_alterar}" action="#{manterHistoricoComplementarBean.alterar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty manterHistoricoComplementarBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.conManterHistoricoComplementar_btn_excluir}" action="#{manterHistoricoComplementarBean.excluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnHistorico" styleClass="bto1"  value="#{msgs.conManterHistoricoComplementar_btn_historico}" action="#{manterHistoricoComplementarBean.historico}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	</a4j:outputPanel>	

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
