<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="altManterHistoricoComplementarForm" name="altManterHistoricoComplementarForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterHistoricoComplementarBean.obrigatoriedade}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" border="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo_segunda_linha_extrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterHistoricoComplementarBean.codigoHistoricoComplementar}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid style="margin-top:6px" cellpadding="0" cellspacing="0" >	
	    <br:brPanelGroup >
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-bottom:2px" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.altManterHistoricoComplementar_tipoServico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterHistoricoComplementarBean.dsTipoServico}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid style="margin-top:6px" cellpadding="0" cellspacing="0" >	
	    <br:brPanelGroup >
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-bottom:2px" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.altManterHistoricoComplementar_modalidadeServico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterHistoricoComplementarBean.dsModalidadeServico}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid style="margin-top:6px" cellpadding="0" cellspacing="0" >	
	    <br:brPanelGroup >
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" border="0">
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.altManterHistoricoComplementar_tipolancamento}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" >	
		    <br:brPanelGroup >
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<t:selectBooleanCheckbox id="tipoLancamentoCredito" styleClass="HtmlSelectOneRadioBradesco" value="#{manterHistoricoComplementarBean.lancamentoCredito}">
				<a4j:support event="onclick" reRender="panelCredito" action="#{manterHistoricoComplementarBean.carregaCombos}"/>
			</t:selectBooleanCheckbox>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterHistoricoComplementar_tipolancamento_extratocredito}" style="margin-right:5px"/>
		
		 	<t:selectBooleanCheckbox id="tipoLancamentoDebito" disabled="true" styleClass="HtmlSelectOneRadioBradesco" value="#{manterHistoricoComplementarBean.lancamentoDebito}">
		 		<a4j:support event="onclick" reRender="panelDebito" action="#{manterHistoricoComplementarBean.carregaCombos}"/>
		 	</t:selectBooleanCheckbox>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterHistoricoComplementar_tipolancamento_extratodebito}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterHistoricoComplementar_lancamentodecredito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="panelCredito" ajaxRendered="true">	
	 	<br:brPanelGrid cellpadding="0" cellspacing="0"  style="vertical-align: top;">
	 		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_descricao_segunda_linha_extrato}:"/>
			</br:brPanelGroup>
	 		
	 		<br:brInputText styleClass="HtmlInputTextBradesco" 
	 				value="#{manterHistoricoComplementarBean.dsSegundaLinhaExtratoCredito}" size="30" maxlength="32" 
	 				id="txDsSegundaLinhaExtratoCredito">
			</br:brInputText>
	 	</br:brPanelGrid >
	</a4j:outputPanel>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterHistoricoComplementar_lancamentodedebito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid cellpadding="0" cellspacing="0"  style="vertical-align: top;">
	 	<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_descricao_segunda_linha_extrato}:"/>
		</br:brPanelGroup>
	 		
		<br:brInputText styleClass="HtmlInputTextBradesco" 
	 		value="#{manterHistoricoComplementarBean.dsSegundaLinhaExtratoDebito}" disabled="true" size="30" maxlength="32" 
	 		id="txtDsSegundaLinhaExtratoDebito">
		</br:brInputText>
	</br:brPanelGrid >
	 
	<f:verbatim><hr class="lin"> </f:verbatim>
	 
	<h:inputHidden id="hiddenRestricao" value="#{manterHistoricoComplementarBean.codRestricao}" />
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" border="0">
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterHistoricoComplementar_restricao}:"/>
		</br:brPanelGroup>
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" >	
		    <br:brPanelGroup >
			</br:brPanelGroup>	
		</br:brPanelGrid>
		<br:brPanelGroup  >				
			<t:selectOneRadio id="restricao" styleClass="HtmlSelectOneRadioBradesco" value="#{manterHistoricoComplementarBean.codRestricao}" >
				<f:selectItem itemValue="1" itemLabel="#{msgs.incManterHistoricoComplementar_label_sim}"/>
				<f:selectItem itemValue="2" itemLabel="#{msgs.incManterHistoricoComplementar_label_nao}"/>
				<a4j:support event="onclick" reRender="hiddenRestricao"/>
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>


	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" border="0">	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.altManterHistoricoComplementar_btn_voltar}" action="#{manterHistoricoComplementarBean.alterarVoltar}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnAvancar"
					onclick="javascript:checaCamposAlterar(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.altManterHistoricoComplementar_title_tipoLancamento}',  '#{msgs.incManterHistoricoComplementar_centroDeCustoCredito}', '#{msgs.altManterHistoricoComplementar_title_idiomaCredito}', '#{msgs.altManterHistoricoComplementar_title_recursoCredito}', '#{msgs.altManterHistoricoComplementar_title_eventoMensagemCredito}', '#{msgs.incManterHistoricoComplementar_centroDeCustoDebito}', '#{msgs.altManterHistoricoComplementar_title_idiomaDebito}', '#{msgs.altManterHistoricoComplementar_title_recursoDebito}', '#{msgs.altManterHistoricoComplementar_title_eventoMensagemDebito}', '#{msgs.altManterHistoricoComplementar_title_restricao}'); return validateForm(document.forms[1]);"
					styleClass="bto1"
					value="#{msgs.altManterHistoricoComplementar_btn_avancar}"
					action="#{manterHistoricoComplementarBean.alterarAvancar}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
	</br:brPanelGrid>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>