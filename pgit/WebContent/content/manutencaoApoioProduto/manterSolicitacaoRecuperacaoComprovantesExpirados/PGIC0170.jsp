<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>

<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterSolicitacaoRecuperacaoComprovantesExpiradosForm" name="manterSolicitacaoRecuperacaoComprovantesExpiradosForm" >
<h:inputHidden id="hiddenRadio" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.codListaRadio}"/>
<h:inputHidden id="hiddenRadioFiltro" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.filtro}"/>
<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.PGIC0170_label_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brSelectOneRadio id="radioFiltro" styleClass="HtmlSelectOneRadioBradesco" onclick="javascript:habilitarBotaoConsultarClientemanterSolicitacaoRecuperacaoComprovantesExpirados(document.forms[1], this);" >
				<f:selectItem itemLabel="#{msgs.PGIC0170_label_filtroCliente}" itemValue="1" />  
	            <f:selectItem itemLabel="#{msgs.PGIC0170_label_filtroContrato}" itemValue="2" />  
	    	</br:brSelectOneRadio>
	    </br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnConsultarCliente" styleClass="HtmlCommandButtonBradesco" disabled="true" value="#{msgs.PGIC0170_botao_consultar}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.consultarCliente}">			
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.PGIC0170_label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.PGIC0170_label_cpfcnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identclienteBean.cpf}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identclienteBean.tela == 'PGIC0170'}" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identclienteBean.cpf}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identclienteBean.tela != 'PGIC0170'}" />
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.PGIC0170_label_nomeRazao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identclienteBean.nomeRazao}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identclienteBean.tela == 'PGIC0170'}" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identclienteBean.nomeRazao}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identclienteBean.tela != 'PGIC0170'}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.PGIC0170_label_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.PGIC0170_label_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.empresa}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tela == 'PGIC0170'}" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.empresa}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tela != 'PGIC0170'}" />
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.PGIC0170_label_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tipo}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tela == 'PGIC0170'}" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tipo}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tela != 'PGIC0170'}" />
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.PGIC0170_label_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.numero}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tela == 'PGIC0170'}" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.numero}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tela != 'PGIC0170'}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" style="margin-top:2px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.PGIC0170_label_descricaoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.descricaoContrato}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tela == 'PGIC0170'}" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.descricaoContrato}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tela != 'PGIC0170'}" />
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.PGIC0170_label_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.situacao}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tela == 'PGIC0170'}" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.situacao}" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identcontratoBean.tela != 'PGIC0170'}" />			
		</br:brPanelGroup>		
		
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	 <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.PGIC0170_label_solicitacoes}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0170_label_situacao2}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0170_label_periodo}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboSituacao" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.situacaoFiltro}">
				<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0170_label_combo_selecione}"/>
				<!--f:selectItems value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.listaSituacao}"/-->			
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
					
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<!--br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.PGIC0170_label_de}"/-->			
			<app:calendar id="calendarioDe" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.dataInicioPeriodoFiltro}" >
				<brArq:commonsValidator type="required" arg="#{msgs.PGIC0170_check_Periodo}" server="false" client="true"/>
			</app:calendar>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.PGIC0170_label_ate}"/>
			<app:calendar id="calendarioAte" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.dataFimPeriodoFiltro}" >
				<brArq:commonsValidator type="required" arg="#{msgs.PGIC0170_check_Periodo}" server="false" client="true"/>
			</app:calendar>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="HtmlCommandButtonBradesco" value="#{msgs.PGIC0170_botao_limpar_campos}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar2"  styleClass="HtmlCommandButtonBradesco" value="#{msgs.PGIC0170_botao_consultar2}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 	
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
			<f:verbatim> <div id="rolagem" style="width:750px; overflow:scroll"></f:verbatim> 
			<t:dataTable id="dataTable" value="#{cmpi0001Bean.listaGridRemessa}" var="result" rows="5" 
			cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
			columnClasses="alinhamento_centro, alinhamento_direita, alinhamento_direita, alinhamento_esquerda, 
			alinhamento_direita, alinhamento_direita, alinhamento_esquerda, alinhamento_esquerda" 
			headerClass="tabela_celula_destaque_acentuado" width="750px">
			<t:column width="30px">
				<f:facet name="header">
			      <br:brOutputText value="" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>		
				<t:selectOneRadio  onclick="javascript:habilitarBotaosLoteOcorrencia(document.forms[1], this);" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
					<f:selectItems value="#{cmpi0001Bean.listaControleRemessa}"/>
				</t:selectOneRadio>
		    	<t:radio for="sor" index="#{result.totalRegistros}" />
			</t:column>
			  <t:column width="150px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0170_grid_numero_solicitacao}" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.cpf_completo}"  style="float:right;"/> 
			  </t:column>
			  <t:column width="150px" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.PGIC0170_grid_data_hora_solicitacao}" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.nome}" style="float:left;" />
			  </t:column>
			  <t:column width="270px" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.PGIC0170_grid_situacao_motivo}" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.nome}" style="float:left;" />
			  </t:column>
			  <t:column width="150px" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.PGIC0170_grid_data_hora_atendimento}" style="font-weight: bold;font-family: verdana;font-size: 10 px;  margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.nome}" style="float:left;" />
			  </t:column>
			</t:dataTable>	
			<f:verbatim> </div> </f:verbatim>				
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{arquivoRetornoBean.pesquisar}" rendered="#{arquivoRetornoBean.listaGridRetorno!= null && manterSolicitacaoRecuperacaoComprovantesExpiradosBean.mostraBotoes}">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="HtmlCommandButtonBradesco"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>		
	
	<br:brPanelGrid columns="3" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
		<br:brPanelGroup style="width:750px">			
			<br:brCommandButton id="btnDetalhar" disabled="false" styleClass="HtmlCommandButtonBradesco"  style="margin-right:5px;" value="#{msgs.PGIC0170_botao_detalhar}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.detalhar}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px;" value="#{msgs.PGIC0170_botao_incluir}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.incluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
			<br:brCommandButton id="btnExcluir"  disabled="false"  styleClass="HtmlCommandButtonBradesco"  value="#{msgs.PGIC0170_botao_excluir}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.excluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>