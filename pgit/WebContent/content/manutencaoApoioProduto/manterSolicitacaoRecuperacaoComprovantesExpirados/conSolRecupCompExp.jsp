<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="consultarSolicitacaoRecuperacaoComprovantesExpiradosForm" name="consultarSolicitacaoRecuperacaoComprovantesExpiradosForm" >

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conSolRecupCompExp_label_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brSelectOneRadio id="radioFiltro" styleClass="HtmlSelectOneRadioBradesco" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.itemSelecionadoPesquisaCliente}">
				<f:selectItem itemLabel="#{msgs.conSolRecupCompExp_label_filtroCliente}" itemValue="1" />  
	            <f:selectItem itemLabel="#{msgs.conSolRecupCompExp_label_filtroContrato}" itemValue="2" />  
	            <a4j:support event="onclick" reRender="btnConsultarCliente" ajaxSingle="true" />
	    	</br:brSelectOneRadio>
	    </br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnConsultarCliente" disabled="#{empty manterSolicitacaoRecuperacaoComprovantesExpiradosBean.itemSelecionadoPesquisaCliente}" styleClass="HtmlCommandButtonBradesco" value="#{msgs.conSolRecupCompExp_botao_consultar}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.consultarClienteContrato}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conSolRecupCompExp_label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conSolRecupCompExp_label_cpfcnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.cpf}"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.cpf}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
				rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela != 'conSolRecupCompExp' && manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela != 'conSolRecupCompExp'}" />
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conSolRecupCompExp_label_nomeRazao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.nomeRazao}"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.nomeRazao}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
				rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela != 'conSolRecupCompExp' && manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela != 'conSolRecupCompExp'}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conSolRecupCompExp_label_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conSolRecupCompExp_label_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.empresa}"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.empresa}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
				rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela != 'conSolRecupCompExp' && manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela != 'conSolRecupCompExp'}" />
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conSolRecupCompExp_label_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tipo}"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tipo}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
				rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela != 'conSolRecupCompExp' && manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela != 'conSolRecupCompExp'}" />
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conSolRecupCompExp_label_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.numero}"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.numero}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
				rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela != 'conSolRecupCompExp' && manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela != 'conSolRecupCompExp'}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conSolRecupCompExp_label_descricaoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.descricaoContrato}"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.descricaoContrato}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
				rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela != 'conSolRecupCompExp' && manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela != 'conSolRecupCompExp'}" />
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conSolRecupCompExp_label_situacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.situacao}"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela == 'conSolRecupCompExp'}" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.situacao}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
				rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoClienteBean.tela != 'conSolRecupCompExp' && manterSolicitacaoRecuperacaoComprovantesExpiradosBean.identificacaoContratoBean.tela != 'conSolRecupCompExp'}" />				
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conSolRecupCompExp_label_solicitacoes}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conSolRecupCompExp_label_situacao2}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0"  style="width:20px">
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conSolRecupCompExp_label_periodo}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>		
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboSituacao" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.situacaoFiltro}">
				<f:selectItem itemValue="" itemLabel="#{msgs.conSolRecupCompExp_label_combo_selecione}"/>
				<f:selectItems value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.listaSituacao}"/>			
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
					
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>	
		
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="  "/>
			<app:calendar id="calendarioDe" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.dataInicioPeriodoFiltro}" >
				<!--<brArq:commonsValidator type="required" arg="#{msgs.PGIC0170_check_Periodo}" server="false" client="true"/>-->
			</app:calendar>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.conSolRecupCompExp_label_a}"/>
			<app:calendar id="calendarioAte" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.dataFimPeriodoFiltro}" >
				<!--<brArq:commonsValidator type="required" arg="#{msgs.PGIC0170_check_Periodo}" server="false" client="true"/>-->
			</app:calendar>			
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="HtmlCommandButtonBradesco" value="#{msgs.conSolRecupCompExp_botao_limpar_campos}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar2"  styleClass="HtmlCommandButtonBradesco" value="#{msgs.conSolRecupCompExp_botao_consultar2}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
	<br:brPanelGroup style="overflow-x:auto;width:750px" >	
			<app:scrollableDataTable id="dataTable" value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.listaGridComprovantes}" var="result" 
			rows="10" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >			

			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
				<t:selectOneRadio  id="sorLista2" styleClass="HtmlSelectOneRadioBradesco" 
					layout="spread" forceId="true" forceIdIndex="false" 
					value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.itemSelecionadoListaComprovante}">
					<f:selectItems value="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.listaControleRadioComprovantes}"/>
					<a4j:support event="onclick" reRender="panelBotoes2" />
				</t:selectOneRadio>
		    	<t:radio for="sorLista2" index="#{result.linha}" />
			</app:scrollableColumn>


			<app:scrollableColumn  width="200px" styleClass="colTabRight" >
			    <f:facet name="header">
			     <h:outputText value="#{msgs.conSolRecupCompExp_grid_numero_solicitacao}"  />
			    </f:facet>
			    <br:brOutputText value="#{result.numero}" />
			 </app:scrollableColumn>
			 
			<app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conSolRecupCompExp_grid_data_hora_solicitacao}"  />
			    </f:facet>
			    <br:brOutputText value="#{result.dataHoraSol}"  />
			 </app:scrollableColumn>
			  
			 <app:scrollableColumn  width="300px" styleClass="colTabLeft" >
			    <f:facet name="header">
			       <h:outputText value="#{msgs.conSolRecupCompExp_grid_situacao_motivo}" />
			    </f:facet>
			    <br:brOutputText value="#{result.situacaoMotivo}"  />
			 </app:scrollableColumn>
			 
			  <app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			        <h:outputText value="#{msgs.conSolRecupCompExp_grid_data_hora_atendimento}" />
			    </f:facet>
			    <br:brOutputText value="#{result.dataHoraAten}"/>	
			 </app:scrollableColumn>

			</app:scrollableDataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="" rendered="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.listaGridComprovantes!= null && manterSolicitacaoRecuperacaoComprovantesExpiradosBean.mostraBotoes}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="HtmlCommandButtonBradesco"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>		
	
	<a4j:outputPanel id="panelBotoes2" style="width: 100%; text-align: right" ajaxRendered="true">
	<br:brPanelGrid columns="3" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
		<br:brPanelGroup style="width:750px">			
			<br:brCommandButton id="btnDetalhar" disabled="#{empty manterSolicitacaoRecuperacaoComprovantesExpiradosBean.itemSelecionadoListaComprovante}" styleClass="HtmlCommandButtonBradesco"  style="margin-right:5px;" value="#{msgs.conSolRecupCompExp_botao_detalhar}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.detalhar}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px;" value="#{msgs.conSolRecupCompExp_botao_incluir}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.incluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
			<br:brCommandButton id="btnExcluir"  disabled="#{empty manterSolicitacaoRecuperacaoComprovantesExpiradosBean.itemSelecionadoListaComprovante}"  styleClass="HtmlCommandButtonBradesco"  value="#{msgs.conSolRecupCompExp_botao_excluir}" action="#{manterSolicitacaoRecuperacaoComprovantesExpiradosBean.excluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	</a4j:outputPanel>
		
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>