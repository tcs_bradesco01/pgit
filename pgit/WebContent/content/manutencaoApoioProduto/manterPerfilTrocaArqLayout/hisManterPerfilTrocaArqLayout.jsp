<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="hisManterPerfilTrocaArqLayoutForm" name="hisManterPerfilTrocaArqLayoutForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="7" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_do_perfil}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtCodigoPerfilHist" value="#{manterPerfilTrocaArqLayoutBean.codigoPerfilHist}" disabled="#{manterPerfilTrocaArqLayoutBean.disableArgumentosConsultaHistorico}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" size="20" maxlength="9" onkeypress="onlyNum()" converter="javax.faces.Long" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>	
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoLayoutArquivoFiltro" value="#{manterPerfilTrocaArqLayoutBean.tipoLayoutArquivoHist}" disabled="#{manterPerfilTrocaArqLayoutBean.disableArgumentosConsultaHistorico}" converter="javax.faces.Integer">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaTipoLayoutArquivo}" />
						</br:brSelectOneMenu>				
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup> 
		</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_manutencao}:" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
		
		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.consultarPagamentosIndividual_de}"/>
					<br:brPanelGroup rendered="#{!manterPerfilTrocaArqLayoutBean.disableArgumentosConsultaHistorico}">						
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicioManutencao');" id="dataInicioManutencao" value="#{manterPerfilTrocaArqLayoutBean.dataIncialManutencao}" >
						</app:calendar>
					</br:brPanelGroup>
					<br:brPanelGroup rendered="#{manterPerfilTrocaArqLayoutBean.disableArgumentosConsultaHistorico}">						
						<app:calendar id="dataInicioManutencaoDisabled" value="#{manterPerfilTrocaArqLayoutBean.dataIncialManutencao}" disabled="true">
						</app:calendar>
					</br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
					<br:brPanelGroup rendered="#{!manterPerfilTrocaArqLayoutBean.disableArgumentosConsultaHistorico}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFimManutencao');" id="dataFimManutencao" value="#{manterPerfilTrocaArqLayoutBean.dataFimManutencao}" >
		 				</app:calendar>	
					</br:brPanelGroup>	
					<br:brPanelGroup rendered="#{manterPerfilTrocaArqLayoutBean.disableArgumentosConsultaHistorico}">
						<app:calendar id="dataFimManutencaoDisabled" value="#{manterPerfilTrocaArqLayoutBean.dataFimManutencao}" disabled="true">
		 				</app:calendar>	
					</br:brPanelGroup>
				</br:brPanelGroup>			    
			</br:brPanelGrid>
		</a4j:outputPanel>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{manterPerfilTrocaArqLayoutBean.limparHistorico}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{manterPerfilTrocaArqLayoutBean.consultarHistorico}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
		<f:verbatim><br></f:verbatim> 
	
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="OverflowScrollableDataTable" >	
				<app:scrollableDataTable id="dataTable" value="#{manterPerfilTrocaArqLayoutBean.listaGridHistorico}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio value="#{manterPerfilTrocaArqLayoutBean.itemSelecionadoHistorico}" id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaControleHistorico}"/>
							<a4j:support event="onclick" reRender="btnDetalhar"/>
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
				
					<app:scrollableColumn styleClass="colTabRight" width="180px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_perfil_troca_arquivos}" style="width:180; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdPerfilTrocaArquivo}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft" width="220px" >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_tipo_layout_arquivo}"  style="width:220; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsTipoLayoutArquivo}" />
					</app:scrollableColumn>			
					
					<app:scrollableColumn styleClass="colTabCenter" width="260px" >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_data_hora_manutencao}"  style="width:260; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dataHoraFormatada}" />
					</app:scrollableColumn>			
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>		

	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
			    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterPerfilTrocaArqLayoutBean.pesquisarHistorico}" >
					<f:facet name="first">				    
						<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					</f:facet>
				  	<f:facet name="fastrewind">
				    	<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  	</f:facet>
				  	<f:facet name="previous">
				    	<brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  	</f:facet>
				  	<f:facet name="next">
				    	<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  	</f:facet>
				  	<f:facet name="fastforward">
				    	<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  	</f:facet>
				  	<f:facet name="last">
				    	<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  	</f:facet>
				</brArq:pdcDataScroller>			  
			</br:brPanelGroup>
		</br:brPanelGrid>	

		<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{manterPerfilTrocaArqLayoutBean.voltarConsulta}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px">		
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterPerfilTrocaArqLayoutBean.itemSelecionadoHistorico}"  value="#{msgs.btn_detalhar}"  action="#{manterPerfilTrocaArqLayoutBean.detalharHistorico}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
	</br:brPanelGrid>  

	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>