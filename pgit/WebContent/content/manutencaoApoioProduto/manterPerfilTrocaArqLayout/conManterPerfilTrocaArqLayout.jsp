<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterPerfilTrocaArqLayoutForm" name="conManterPerfilTrocaArqLayoutForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_do_perfil}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtCodigoPerfilFiltro" value="#{manterPerfilTrocaArqLayoutBean.codigoPerfilFiltro}"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" size="20" maxlength="9" onkeypress="onlyNum()" converter="javax.faces.Long" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>	
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoLayoutArquivoFiltro" value="#{manterPerfilTrocaArqLayoutBean.tipoLayoutArquivoFiltro}" converter="javax.faces.Integer">
							<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaTipoLayoutArquivo}" />
						</br:brSelectOneMenu>				
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		
	    <f:verbatim><hr class="lin"></f:verbatim>	
    
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;">
	   		<br:brCommandButton id="btoConsultarContrato"  styleClass="bto1" value="#{msgs.label_contrato}" action="#{manterPerfilTrocaArqLayoutBean.consultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" disabled="#{!empty manterPerfilTrocaArqLayoutBean.listaGridConsultar}">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGrid>	  
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	 
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.empresaGestoraDescContrato}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.numeroDescContrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.descricaoContratoDescContrato}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.situacaoDescContrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	

	
		<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{manterPerfilTrocaArqLayoutBean.limpar}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{manterPerfilTrocaArqLayoutBean.consultar}" disabled="#{!empty manterPerfilTrocaArqLayoutBean.listaGridConsultar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<f:verbatim><br></f:verbatim> 
	
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="OverflowScrollableDataTable" >	
				<app:scrollableDataTable id="dataTable" value="#{manterPerfilTrocaArqLayoutBean.listaGridConsultar}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio value="#{manterPerfilTrocaArqLayoutBean.itemSelecionadoConsultar}" id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaControleConsultar}"/>
							<a4j:support event="onclick" reRender="panelBotoes"/>
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
				
					<app:scrollableColumn styleClass="colTabRight"   >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_perfil}" style="text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdPerfilTrocaArquivo}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft"  >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_utiliza_layout_proprio}"  style=" text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsLayoutProprio}" />
					</app:scrollableColumn>			

					<app:scrollableColumn styleClass="colTabLeft"   >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_layout}"  style=" text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsTipoLayoutArquivo}" />
					</app:scrollableColumn>			

					<app:scrollableColumn styleClass="colTabLeft" width="150">			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_servico}" style="text-align:center;width: 150px;" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsProdutoServico}" style="width: 150px;" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_aplicativo_formatacao}"  style=" text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsAplicacaoTransmicaoPagamento}" />
					</app:scrollableColumn>			

					<app:scrollableColumn styleClass="colTabLeft"  >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_meio_transmissao_principal_remessa}"  style=" text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsMeioPrincRemss}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabLeft" >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_meio_transmissao_alternativo_remessa}"  style=" text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsMeioAltrnRemss}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabLeft"  >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_meio_transmissao_principal_retorno}"  style=" text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsMeioPrincRetor}" />
					</app:scrollableColumn>	

					<app:scrollableColumn styleClass="colTabLeft"  >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_meio_transmissao_alternativo_retorno}"  style=" text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsMeioAltrnRetor}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabLeft"  >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_empresa_transmissao}"  style=" text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsRzScialPcero}" />
					</app:scrollableColumn>	
					
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
			    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterPerfilTrocaArqLayoutBean.pesquisar}">
					<f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
				</brArq:pdcDataScroller>			  
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
			<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup >
					<br:brCommandButton id="btoLimpar" styleClass="bto1" disabled="#{empty manterPerfilTrocaArqLayoutBean.listaGridConsultar}" style="margin-right:5px" value="#{msgs.btn_limpar}"  action="#{manterPerfilTrocaArqLayoutBean.limparInformacaoConsulta}">	
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterPerfilTrocaArqLayoutBean.itemSelecionadoConsultar}" style="margin-right:5px" value="#{msgs.btn_detalhar}"  action="#{manterPerfilTrocaArqLayoutBean.detalhar}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnIncluir" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_incluir}"  action="#{manterPerfilTrocaArqLayoutBean.incluir}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton  id="btnAlterar" styleClass="bto1" disabled="#{empty manterPerfilTrocaArqLayoutBean.itemSelecionadoConsultar}"  style="margin-right:5px" value="#{msgs.btn_alterar}"  action="#{manterPerfilTrocaArqLayoutBean.alterar}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty manterPerfilTrocaArqLayoutBean.itemSelecionadoConsultar}" style="margin-right:5px" value="#{msgs.btn_excluir}"  action="#{manterPerfilTrocaArqLayoutBean.excluir}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnHistorico" styleClass="bto1" value="#{msgs.label_historico}"  action="#{manterPerfilTrocaArqLayoutBean.historico}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>