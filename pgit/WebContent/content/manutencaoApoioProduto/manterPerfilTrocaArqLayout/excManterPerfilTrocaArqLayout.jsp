<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="excManterPerfilTrocaArqLayoutForm" name="excManterPerfilTrocaArqLayoutForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_perfil_troca_arquivos}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.codigo}"  />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoLayoutArquivo}"  />
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0006_servico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoServico}"  />
			</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_aplicativo_formatacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.aplicativoFormatacao}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>			
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_sistema_origem_arquivo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.sistemaOrigemArquivo}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>			
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_responsavel_transmissao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.empresaResponsavelTransmissao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_username}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.username}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_arquivo_remessa}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nomeArquivoRemessa}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_arquivo_retorno}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nomeArquivoRetorno}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" rendered="#{manterPerfilTrocaArqLayoutBean.renderizaCamposGeracaoSegmentosBeZ}" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_radio_gerar_segmento_b}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dsSegmentoB}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_combo_gerar_segmento_z}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dsSegmentoZ}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_remessa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_acolhimento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoRejeicaoAcolhimento}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_registros_inconsistentes}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.quantidadeRegistrosInconsistentesFormatada}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_percentual_registros_inconsistentes}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.percentualRegistrosInconsistentes}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lavel_nivel_controle}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nivelControleRemessa}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_controle}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoControleRemessa}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_periodicidade_inicializacao_contagem}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.periodicidadeInicializacaoContagemRemessa}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_maximo_remessa}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.numeroMaximoRemessa}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_meio_transmissao_principal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.meioTransmissaoPrincipalRemessa}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_meio_transmissao_alternativo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.meioTransmissaoAlternativoRemessa}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid cellpadding="0" cellspacing="0" rendered="#{manterPerfilTrocaArqLayoutBean.exibirCamposCNAB}">
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_exige_cnpjcpf_pagador_header}:"/>
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dsIndicadorCpfLayout}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_validar_camara_compensacao}:"/>
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dsIndicadorContaComplementar}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_consiste_conta_debito_header}:"/>
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dsIndicadorConsisteContaDebito}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<!-- Campos ocultados pois ser�o introduzidos numa solicita��o futura -->
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" rendered="false">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_consiste_conta_debito_header}:"/>
				</br:brPanelGroup>		
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="???" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_retorno}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lavel_nivel_controle}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nivelControleRetorno}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_controle}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoControleRetorno}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_periodicidade_inicializacao_contagem}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.periodicidadeInicializacaoContagemRetorno}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.altRetornoLayoutsArquivos1_numero_maximo_retorno}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.numeroMaximoRetorno}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_meio_transmissao_principal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.meioTransmissaoPrincipalRetorno}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_meio_transmissao_alternativo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.meioTransmissaoAlternativoRetorno}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" rendered="#{manterPerfilTrocaArqLayoutBean.exibirCamposCNAB}" cellpadding="0" cellspacing="0">
		
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_concatena_agencia_padrao_sap}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dsIndicadorAssociacaoLayout}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
					
					
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_retorna_segmentoB}:"/>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dsIndicadorGeracaoSegmentoB}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_retorna_segmentoZ}:"/>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dsIndicadorGeracaoSegmentoZ}" />
					</br:brPanelGroup>
				</br:brPanelGrid>
					
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid>
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_contato_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nomeContatoCliente}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_ddd}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dddContatoCliente}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_telefone}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.telefoneContatoCliente}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_email}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.emailContatoCliente}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="0" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_solicitante_agencia_gerente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nomeContatoSolicitante}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_ddd}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dddContatoSolicitante}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_telefone}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.telefoneContatoSolicitante}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_email}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.emailContatoSolicitante}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_justificativa_obs}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.justificativaSolicitante}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_hora_inclusao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dataHoraInclusao}"  />			
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_usuario}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.usuarioInclusao}"  />			
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_canal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoCanalInclusao}"  />
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_complemento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.complementoInclusao}"  />			
			</br:brPanelGroup>					
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_hora_manutencao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dataHoraManutencao}"  />			
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_usuario}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.usuarioManutencao}"  />			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_canal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoCanalManutencao}"  />
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_complemento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.complementoManutencao}"  />			
			</br:brPanelGroup>					
		</br:brPanelGrid>
	
	<br:brPanelGrid id="gridContratoVinc" rendered="#{manterPerfilTrocaArqLayoutBean.flagContratoVinc}">
		<f:verbatim><hr class="lin"> </f:verbatim>
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="0" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contratos_afetados}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" rendered="#{manterPerfilTrocaArqLayoutBean.listaContratosVinculados != null}">	
				<br:brPanelGroup styleClass="OverflowScrollableDataTable" >	
					<app:scrollableDataTable id="dataTable" value="#{manterPerfilTrocaArqLayoutBean.listaContratosVinculados}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">				
						<app:scrollableColumn styleClass="colTabRight"  >			
						    <f:facet name="header">
						      <br:brOutputText value="#{msgs.label_club_representante}" style="width:100; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.cdClubRepresentante}" styleClass="tableFontStyle"/>
						</app:scrollableColumn>				
						
						<app:scrollableColumn styleClass="colTabRight" >			
							<f:facet name="header">
						    	<h:outputText value="#{msgs.identificacaoClienteContrato_cpf_cnpj_representante}"  style="width:130; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.cpfCnpjFormatado}" />
						</app:scrollableColumn>			
	
						<app:scrollableColumn styleClass="colTabLeft"  >			
							<f:facet name="header">
						    	<h:outputText value="#{msgs.identificacaoClienteContrato_nome_razao_social}"  style="width:350; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.nmRazaoRepresentante}" />
						</app:scrollableColumn>			
	
						<app:scrollableColumn styleClass="colTabLeft"  >			
							<f:facet name="header">
						    	<h:outputText value="#{msgs.label_empresa_gestora_contrato}"  style="width:120; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.dsPessoaJuridica}" />
						</app:scrollableColumn>			
	
						<app:scrollableColumn styleClass="colTabLeft"  >			
							<f:facet name="header">
						    	<h:outputText value="#{msgs.label_tipo_contrato}"  style="width:170; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.dsTipoContrato}" />
						</app:scrollableColumn>			
	
						<app:scrollableColumn styleClass="colTabRight"  >			
							<f:facet name="header">
						    	<h:outputText value="#{msgs.label_numero_contrato}"  style="width:90; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.nrSequenciaContrato}" />
						</app:scrollableColumn>	
						
						<app:scrollableColumn styleClass="colTabLeft"  >			
							<f:facet name="header">
						    	<h:outputText value="#{msgs.label_situacao_contrato}"  style="width:150; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.dsSituacaoContrato}" />
						</app:scrollableColumn>	
						
						<app:scrollableColumn styleClass="colTabLeft"  >			
							<f:facet name="header">
						    	<h:outputText value="#{msgs.label_agencia_gestora_contrato}"  style="width:150; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.cdAgenciaOperadora}-" />
						    <br:brOutputText value="#{result.dsAgenciaOperadora}" />
						</app:scrollableColumn>	
	
						<app:scrollableColumn styleClass="colTabLeft"  >			
							<f:facet name="header">
						    	<h:outputText value="#{msgs.label_gerente_responsavel}"  style="width:150; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.dsFuncionarioBradesco}" />
						</app:scrollableColumn>						
					</app:scrollableDataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" rendered="#{manterPerfilTrocaArqLayoutBean.listaContratosVinculados != null}">	
				<br:brPanelGroup>
				    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterPerfilTrocaArqLayoutBean.pesquisarContratosVinculados}" >
						<f:facet name="first">				    
							<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
						</f:facet>
					  	<f:facet name="fastrewind">
					    	<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					  	</f:facet>
					  	<f:facet name="previous">
					    	<brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					  	</f:facet>
					  	<f:facet name="next">
					    	<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					  	</f:facet>
					  	<f:facet name="fastforward">
					    	<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					  	</f:facet>
					  	<f:facet name="last">
					    	<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					  	</f:facet>
					</brArq:pdcDataScroller>			  
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
	</br:brPanelGrid>
		
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton styleClass="bto1" value="#{msgs.label_botao_voltar}" action="#{manterPerfilTrocaArqLayoutBean.voltarConsulta}" >	
					<brArq:submitCheckClient/>
				</br:brCommandButton>			
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >
				<br:brCommandButton id="btnExcluir" styleClass="bto1" style="align:left" value="#{msgs.btn_confirmar}" action="#{manterPerfilTrocaArqLayoutBean.confirmarExcluir}" onclick="javascript: if (!confirm('#{msgs.label_conf_exclusao}')) { desbloquearTela(); return false; }">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</br:brPanelGrid>
</brArq:form>