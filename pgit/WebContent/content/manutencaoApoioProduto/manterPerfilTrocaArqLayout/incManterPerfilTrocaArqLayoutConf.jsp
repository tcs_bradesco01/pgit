	 <%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incManterPerfilTrocaArqLayout2Form" name="incManterPerfilTrocaArqLayout2Form" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_perfil_troca_arquivos}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoLayoutArquivo}"  />
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid id="panelGroupRepresentantes" >
			<app:scrollableDataTable id="tableRepresentantes"
				value="#{manterPerfilTrocaArqLayoutBean.listaItensSelecionados}"
				width="100%" rowIndexVar="parametroKey" var="result" rows="10"
				rowClasses="tabela_celula_normal, tabela_celula_destaque">

				<app:scrollableColumn width="280" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_tipo_de_servico}" styleClass="tableFontStyle"
							style="width:280; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsProdutoOperacaoRelacionado}" />
				</app:scrollableColumn>

				<app:scrollableColumn width="220" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_utiliza_layout_proprio}"
							styleClass="tableFontStyle" style="width:220; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsIndicadorLayoutProprio}" />
				</app:scrollableColumn>

				<app:scrollableColumn width="220" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_aplicativo_formatacao}" styleClass="tableFontStyle"
							style="width:220; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsApliFormat}" />
				</app:scrollableColumn>
				
			</app:scrollableDataTable>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_remessa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_acolhimento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoRejeicaoAcolhimento}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_registros_inconsistentes}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.quantidadeRegistrosInconsistentesFormatada}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_percentual_registros_inconsistentes}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.percentualRegistrosInconsistentes}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lavel_nivel_controle}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nivelControleRemessa}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_controle}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoControleRemessa}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_periodicidade_inicializacao_contagem}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.periodicidadeInicializacaoContagemRemessa}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_maximo_remessa}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.numeroMaximoRemessa}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_meio_transmissao_principal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.meioTransmissaoPrincipalRemessa}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_meio_transmissao_alternativo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.meioTransmissaoAlternativoRemessa}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid cellpadding="0" cellspacing="0" rendered="#{manterPerfilTrocaArqLayoutBean.exibirCamposCNAB}">
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_exige_cnpjcpf_pagador_header}:"/>
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim_maiusculo}" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorCpfLayout == 1}"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao_maiusculo}" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorCpfLayout == 2}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_validar_camara_compensacao}:"/>
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim_maiusculo}" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorContaComplementar == 1}"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao_maiusculo}" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorContaComplementar == 2}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_consiste_conta_debito_header}:"/>
				</br:brPanelGroup>		
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim_maiusculo}" rendered="#{manterPerfilTrocaArqLayoutBean.cdContaDebito == 1}"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao_maiusculo}" rendered="#{manterPerfilTrocaArqLayoutBean.cdContaDebito == 2}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_retorno}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lavel_nivel_controle}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nivelControleRetorno}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_controle}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoControleRetorno}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_periodicidade_inicializacao_contagem}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.periodicidadeInicializacaoContagemRetorno}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incRetornoLayoutsArquivos2_numero_maximo_retorno}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.numeroMaximoRetorno}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_meio_transmissao_principal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.meioTransmissaoPrincipalRetorno}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_meio_transmissao_alternativo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.meioTransmissaoAlternativoRetorno}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" rendered="#{manterPerfilTrocaArqLayoutBean.exibirCamposCNAB}" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_concatena_agencia_padrao_sap}:"/>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim_maiusculo}" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorAssociacaoLayout == 1}"/>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao_maiusculo}" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorAssociacaoLayout == 2}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
						
						
				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_retorna_segmentoB}:"/>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_devolve_maiusculo}" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorGeracaoSegmentoB == 1}"/>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao_devolve_maiusculo}" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorGeracaoSegmentoB == 2}"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_retorna_segmentoZ}:"/>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="TODOS OS SERVI�OS" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorGeracaoSegmentoZ == 1}" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="TRIBUTO" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorGeracaoSegmentoZ == 2}" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="SAL�RIO" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorGeracaoSegmentoZ == 3}" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="FORNECEDOR" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorGeracaoSegmentoZ == 4}" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="N�O" rendered="#{manterPerfilTrocaArqLayoutBean.cdIndicadorGeracaoSegmentoZ == 5}" />
						</br:brPanelGroup>
					</br:brPanelGrid>
						
				</br:brPanelGrid>
			</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_contratacao}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_responsavel_transmissao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.empresaResponsavelTransmissao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_custo_transmissao_arquivo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.juncaoRespCustoTrans}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_banco}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.txtPercentualCustoTransArq}" converter="decimalBrazillianConverter" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_title_cliente}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.clienteFormatado}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_volume_mensal_registros_trafegados}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.volumeMensalTraf}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_juncao_responsavel_custo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.juncaoRespCustoTrans}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			    
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_arquivo_remessa}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nomeArquivoRemessa}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_arquivo_retorno}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nomeArquivoRetorno}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_username}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.username}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_contato_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nomeContatoCliente}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_ddd}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dddContatoCliente}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_telefone}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.telefoneContatoCliente}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_email}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.emailContatoCliente}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_solicitante_agencia_gerente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.nomeContatoSolicitante}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_ddd}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.dddContatoSolicitante}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_telefone}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.telefoneContatoSolicitante}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_email}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.emailContatoSolicitante}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_justificativa_obs}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.justificativaSolicitante}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton styleClass="bto1" value="#{msgs.label_botao_voltar}" action="#{manterPerfilTrocaArqLayoutBean.voltarIncluir}" >	
					<brArq:submitCheckClient/>
				</br:brCommandButton>			
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >
				<br:brCommandButton id="btnIncluir" styleClass="bto1" style="align:left" value="#{msgs.btn_confirmar}" action="#{manterPerfilTrocaArqLayoutBean.confirmarIncluir}" onclick="javascript: if (!confirm('#{msgs.label_conf_inclusao}')) { desbloquearTela(); return false; }">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</br:brPanelGrid>
</brArq:form>