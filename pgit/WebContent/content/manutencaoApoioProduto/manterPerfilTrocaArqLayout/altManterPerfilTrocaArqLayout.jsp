<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="altManterPerfilTrocaArqLayoutForm" name="altManterPerfilTrocaArqLayoutForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_perfil_troca_arquivos}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoLayoutArquivo}"  />
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.codigo}"  />
			</br:brPanelGroup>						
		</br:brPanelGrid>


		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0006_servico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPerfilTrocaArqLayoutBean.tipoServico}"  />
			</br:brPanelGroup>	

		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sistema_origem_arquivo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			     <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterPerfilTrocaArqLayoutBean.sistemaOrigemArquivoFiltro}"  size="5" maxlength="4" id="txtSistemaOrigemArquivoFiltro"/>
						<f:verbatim>&nbsp;</f:verbatim>
			 			<br:brCommandButton id="btnSistemaOrigemArquivo" style="cursor: hand;" image="/images/lupa.gif" value=""  action="#{manterPerfilTrocaArqLayoutBean.obterSistemaOrigemArquivo}" onclick="javascript: desbloquearTela(); return validarSistemaOrigemArquivo(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_sistema_origem_arquivo}');" >
							<brArq:submitCheckClient/>
						</br:brCommandButton>					
						<f:verbatim>&nbsp;</f:verbatim>
						<f:verbatim>&nbsp;</f:verbatim>
						<br:brSelectOneMenu id="cboSistemaOrigemArquivo" value="#{manterPerfilTrocaArqLayoutBean.cboSistemaOrigemArquivo}" disabled="#{empty manterPerfilTrocaArqLayoutBean.listaSistemaOrigemArquivo}" >
							<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaSistemaOrigemArquivo}" />			
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>				
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_utiliza_aplicativo_formatacao_proprio}:"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup >
				<t:selectOneRadio id="rdoAplicFormProprio"  styleClass="HtmlSelectOneRadioBradesco" value="#{manterPerfilTrocaArqLayoutBean.rdoAplicFormProprio}" forceId="true" forceIdIndex="false" >
					<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
					<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
					<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="cboAplicativoFormatacao, cboTipoLayoutArquivo" action="#{manterPerfilTrocaArqLayoutBean.limparCboAplicFormatacaoAlterar}" status="statusAguarde" />								
				</t:selectOneRadio>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_aplicativo_formatacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
		
				<br:brPanelGrid id="cboAplicativoFormatacao" columns="1" cellpadding="0" cellspacing="0" >					
					<br:brPanelGroup>
						<br:brSelectOneMenu disabled="#{manterPerfilTrocaArqLayoutBean.rdoAplicFormProprio == '1'}"  value="#{manterPerfilTrocaArqLayoutBean.cboAplicativoFormatacao}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaAplicativoFormatacao}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
		
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					<br:brPanelGroup>
						<br:brSelectOneMenu disabled="true" id="cboTipoLayoutArquivo" value="#{manterPerfilTrocaArqLayoutBean.cboTipoLayoutArquivo}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaTipoLayoutArquivo}" />
							<a4j:support  event="onchange" reRender="cboAplicativoFormatacao" action="#{manterPerfilTrocaArqLayoutBean.listarAplicativoFormatacao}" status="statusAguarde" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_remessa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio id="radioFiltro" value="#{manterPerfilTrocaArqLayoutBean.tipoFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterPerfilTrocaArqLayoutBean.desabilitaRadio}" >  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="formulario" action="#{manterPerfilTrocaArqLayoutBean.limparRadios}" status="statusAguarde" />				
			</t:selectOneRadio>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_rejeicao_acolhimento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
					       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicaoAcolhimento" value="#{manterPerfilTrocaArqLayoutBean.cboTipoRejeicaoAcolhimento}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaTipoRejeicaoAcolhimento}" />
							<a4j:support event="onchange" reRender="radioFiltro" action="#{manterPerfilTrocaArqLayoutBean.habilitarRadios}"/>	
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">	
			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;"  >
						<br:brPanelGroup>			
							<t:radio for="radioFiltro" index="0"  />
						</br:brPanelGroup>		
					</br:brPanelGrid>
					
					<br:brPanelGroup>
						<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  style="text-align:left;">
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_quantidade_registros_inconsistentes}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
					
					    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
		
			       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brInputText disabled="#{manterPerfilTrocaArqLayoutBean.desabilitaRadio || manterPerfilTrocaArqLayoutBean.tipoFiltroSelecionado != '0'}" id="txtQuantidadeRegistrosInconsistentes" value="#{manterPerfilTrocaArqLayoutBean.quantidadeRegistrosInconsistentes}" size="10" maxlength="5" onkeypress="onlyNum()" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" styleClass="HtmlInputTextBradesco">
						    </br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;"  >
					<br:brPanelGroup>			
						<t:radio for="radioFiltro" index="1"  />
					</br:brPanelGroup>		
				</br:brPanelGrid>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_percentual_registros_inconsistentes}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
			       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brInputText disabled="#{manterPerfilTrocaArqLayoutBean.desabilitaRadio || manterPerfilTrocaArqLayoutBean.tipoFiltroSelecionado != '1'}" id="txtPercentualRegistrosInconsistentes" value="#{manterPerfilTrocaArqLayoutBean.percentualRegistrosInconsistentes}" styleClass="HtmlInputTextBradesco" size="10" maxlength="6" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" alt="percentual" onfocus="loadMasks();" converter="decimalBrazillianConverter">
						    </br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
				
			</br:brPanelGrid>
		</a4j:outputPanel>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.lavel_nivel_controle}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboNivelControleRemessa" value="#{manterPerfilTrocaArqLayoutBean.cboNivelControleRemessa}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaNivelControle}" />
							<a4j:support oncomplete="javascript:foco(this);" event="onchange" reRender="cboTipoControleRemessa" action="#{manterPerfilTrocaArqLayoutBean.listarTipoControleIncluir}" status="statusAguarde" />				
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_de_controle}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoControleRemessa" disabled="#{manterPerfilTrocaArqLayoutBean.cboNivelControleRemessa == 3}" value="#{manterPerfilTrocaArqLayoutBean.cboTipoControleRemessa}"  styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaTipoControle}" />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>						
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_periodicidade_inicializacao_contagem}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
		
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboPeriodicidadeInicializacaoContagemRemessa" value="#{manterPerfilTrocaArqLayoutBean.cboPeriodicidadeInicializacaoContagemRemessa}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaPeriodicidadeInicializacaoContagem}" />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_numero_maximo_remessa}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
		
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					<br:brPanelGroup>
						<br:brInputText id="txtNumeroMaximoRemessa" value="#{manterPerfilTrocaArqLayoutBean.numeroMaximoRemessa}" size="20" maxlength="10" onkeypress="onlyNum()" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Long" styleClass="HtmlInputTextBradesco">
						</br:brInputText>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_meio_transmissao_principal}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboMeioTransmissaoPrincipalRemessa" value="#{manterPerfilTrocaArqLayoutBean.cboMeioTransmissaoPrincipalRemessa}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaMeioTransmissao}" />
							<a4j:support event="onchange" reRender="cboMeioTransmissaoPrincipalRemessa,cboMeioTransmissaoAlternativoRemessa,cboMeioTransmissaoPrincipalRetorno,
							cboMeioTransmissaoAlternativoRetorno,cboEmpresaRespTransDadosControle,cboRespCustoTransArqDadosControle,txtPercentualCustoTransArq,txtVolumeMensalRegTraf,txtJuncaoRespCustoTransm,
							txtNomeContato,txtDDDContato,txtTelefoneContato,txtRamalContato,txtEmailContato,txtNomeSolictante,txtDDDSolictante,txtTelefoneSolictante,txtRamalSolictante,txtEmailSolictante,
							txtJustificativa,txtNomeArquivoRemessa,txtNomeArquivoRetorno,txtUsername,txtBanco, txtCliente"
							action="#{manterPerfilTrocaArqLayoutBean.habilitarFiltroMeioTransmissao}" status="statusAguarde" />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_meio_transmissao_alternativo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboMeioTransmissaoAlternativoRemessa" value="#{manterPerfilTrocaArqLayoutBean.cboMeioTransmissaoAlternativoRemessa}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaMeioTransmissao}" />
							<a4j:support event="onchange" reRender="cboMeioTransmissaoPrincipalRemessa,cboMeioTransmissaoAlternativoRemessa,cboMeioTransmissaoPrincipalRetorno,
							cboMeioTransmissaoAlternativoRetorno,cboEmpresaRespTransDadosControle,cboRespCustoTransArqDadosControle,txtPercentualCustoTransArq,txtVolumeMensalRegTraf,
							txtJuncaoRespCustoTransm,txtNomeContato,txtDDDContato,txtTelefoneContato,txtRamalContato,txtEmailContato,txtNomeSolictante,txtDDDSolictante,txtTelefoneSolictante,
							txtRamalSolictante,txtEmailSolictante,txtJustificativa,txtNomeArquivoRemessa,txtNomeArquivoRetorno,txtUsername,txtBanco,txtCliente"
								 action="#{manterPerfilTrocaArqLayoutBean.habilitarFiltroMeioTransmissao}" status="statusAguarde" />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
				
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" rendered="#{manterPerfilTrocaArqLayoutBean.exibirCamposCNAB}">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_exige_cnpjcpf_pagador_header}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<t:selectOneRadio id="rdoExigeCnpjCpf" styleClass="HtmlSelectOneRadioBradesco" value="#{manterPerfilTrocaArqLayoutBean.cdIndicadorCpfLayout}" forceId="true" forceIdIndex="false">
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}"/>  
							<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}"/> 
						</t:selectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_validar_camara_compensacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<t:selectOneRadio id="rdoCamaraCompensacao" styleClass="HtmlSelectOneRadioBradesco" value="#{manterPerfilTrocaArqLayoutBean.cdIndicadorContaComplementar}" forceId="true" forceIdIndex="false">
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}"/>  
							<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}"/> 
						</t:selectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_consiste_conta_debito_header}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<t:selectOneRadio id="rdoContaDebito" styleClass="HtmlSelectOneRadioBradesco" value="#{manterPerfilTrocaArqLayoutBean.cdContaDebito}" forceId="true" forceIdIndex="false">
								<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}"/>  
								<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}"/> 
							</t:selectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_retorno}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.lavel_nivel_controle}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboNivelControleRetorno" value="#{manterPerfilTrocaArqLayoutBean.cboNivelControleRetorno}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaNivelControle}" />
							<a4j:support oncomplete="javascript:foco(this);" event="onchange" reRender="cboTipoControleRetorno" action="#{manterPerfilTrocaArqLayoutBean.listarTipoControleRetorno}" status="statusAguarde" />				
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_de_controle}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoControleRetorno" disabled="#{manterPerfilTrocaArqLayoutBean.cboNivelControleRetorno == 3}" value="#{manterPerfilTrocaArqLayoutBean.cboTipoControleRetorno}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaTipoControleRetorno}" />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>								
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_periodicidade_inicializacao_contagem}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
		
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboPeriodicidadeInicializacaoContagemRetorno" value="#{manterPerfilTrocaArqLayoutBean.cboPeriodicidadeInicializacaoContagemRetorno}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaPeriodicidadeInicializacaoContagem}" />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_numero_maximo_retorno}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
		
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					<br:brPanelGroup>
						<br:brInputText id="txtNumeroMaximoRetorno" value="#{manterPerfilTrocaArqLayoutBean.numeroMaximoRetorno}" size="20" maxlength="10" onkeypress="onlyNum()" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Long" styleClass="HtmlInputTextBradesco">
						</br:brInputText>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_meio_transmissao_principal}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboMeioTransmissaoPrincipalRetorno" value="#{manterPerfilTrocaArqLayoutBean.cboMeioTransmissaoPrincipalRetorno}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaMeioTransmissao}" />
							<a4j:support event="onchange" reRender="cboMeioTransmissaoPrincipalRemessa,cboMeioTransmissaoAlternativoRemessa,cboMeioTransmissaoPrincipalRetorno,cboMeioTransmissaoAlternativoRetorno,
							cboEmpresaRespTransDadosControle,cboRespCustoTransArqDadosControle,txtPercentualCustoTransArq,txtVolumeMensalRegTraf,txtJuncaoRespCustoTransm,txtNomeContato,txtDDDContato,
							txtTelefoneContato,txtRamalContato,txtEmailContato,txtNomeSolictante,txtDDDSolictante,txtTelefoneSolictante,txtRamalSolictante,txtEmailSolictante,txtJustificativa,
							txtBanco,txtNomeArquivoRemessa,txtNomeArquivoRetorno,txtUsername,txtCliente"
								 action="#{manterPerfilTrocaArqLayoutBean.habilitarFiltroMeioTransmissao}" status="statusAguarde" />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_meio_transmissao_alternativo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboMeioTransmissaoAlternativoRetorno" value="#{manterPerfilTrocaArqLayoutBean.cboMeioTransmissaoAlternativoRetorno}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaMeioTransmissao}" />
								<a4j:support event="onchange" reRender="cboMeioTransmissaoPrincipalRemessa,cboMeioTransmissaoAlternativoRemessa,cboMeioTransmissaoPrincipalRetorno,
								cboMeioTransmissaoAlternativoRetorno,cboEmpresaRespTransDadosControle,cboRespCustoTransArqDadosControle,txtPercentualCustoTransArq,txtVolumeMensalRegTraf,
								txtJuncaoRespCustoTransm,txtNomeContato,txtDDDContato,txtTelefoneContato,txtRamalContato,txtEmailContato,txtNomeSolictante,txtDDDSolictante,txtTelefoneSolictante,
								txtRamalSolictante,txtBanco,txtEmailSolictante,txtJustificativa,txtNomeArquivoRemessa,txtNomeArquivoRetorno,txtUsername,txtCliente"
								action="#{manterPerfilTrocaArqLayoutBean.habilitarFiltroMeioTransmissao}" status="statusAguarde" />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" rendered="#{manterPerfilTrocaArqLayoutBean.exibirCamposCNAB}">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_concatena_agencia_padrao_sap}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<t:selectOneRadio id="rdoPadraoSap" styleClass="HtmlSelectOneRadioBradesco" value="#{manterPerfilTrocaArqLayoutBean.cdIndicadorAssociacaoLayout}" forceId="true" forceIdIndex="false">
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}"/>  
							<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}"/> 
						</t:selectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
						
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_retorna_segmentoB}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<t:selectOneRadio id="rdoRetornaSegmentoB" styleClass="HtmlSelectOneRadioBradesco" value="#{manterPerfilTrocaArqLayoutBean.cdIndicadorGeracaoSegmentoB}" forceId="true" forceIdIndex="false">
									<f:selectItem itemValue="1" itemLabel="#{msgs.label_radio_devolve}"/>  
									<f:selectItem itemValue="2" itemLabel="#{msgs.label_radio_nao_devolve}"/> 
								</t:selectOneRadio>
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGrid>
					
				    <br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0">
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					 <br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0">
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					 <br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0">
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brPanelGrid cellpadding="0" cellspacing="0">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_retorna_segmentoZ}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						 <br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0">
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<br:brSelectOneMenu id="cboSegmentoZ" value="#{manterPerfilTrocaArqLayoutBean.cdIndicadorGeracaoSegmentoZ}" styleClass="HtmlSelectOneMenuBradesco">
									<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
									<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaSegmentoZ}" />
								</br:brSelectOneMenu>
							</br:brPanelGroup>
							
						</br:brPanelGrid>
					</br:brPanelGrid>
				
				</br:brPanelGrid>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
			<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_contratacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_responsavel_transmissao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

	       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu disabled="#{manterPerfilTrocaArqLayoutBean.flagDadosControle}" id="cboEmpresaRespTransDadosControle" value="#{manterPerfilTrocaArqLayoutBean.cboEmpresaRespTransDadosContr}" styleClass="HtmlSelectOneMenuBradesco">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaEmpresaResponsavelTransmissao}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>
			
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_custo_transmissao_arquivo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

	       <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}" id="cboRespCustoTransArqDadosControle"
						value="#{manterPerfilTrocaArqLayoutBean.cboRespCustoTransArqDadosContr}" styleClass="HtmlSelectOneMenuBradesco">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterPerfilTrocaArqLayoutBean.listaResponsavelCustoTransArq}" />
						<a4j:support  event="onchange" reRender="cboMeioTransmissaoPrincipalRemessa,cboMeioTransmissaoAlternativoRemessa,cboMeioTransmissaoPrincipalRetorno,
						  	cboMeioTransmissaoAlternativoRetorno,cboEmpresaRespTransDadosControle,cboRespCustoTransArqDadosControle,txtPercentualCustoTransArq,
						  	txtVolumeMensalRegTraf,txtJuncaoRespCustoTransm,txtNomeContato,txtDDDContato,txtTelefoneContato,txtRamalContato,txtEmailContato,txtNomeSolictante,
						  	txtDDDSolictante,txtTelefoneSolictante,txtRamalSolictante,txtEmailSolictante,txtJustificativa, txtBanco, txtCliente"  status="statusAguarde"
						  	action="#{manterPerfilTrocaArqLayoutBean.atribuirPercentualBancoCliente}" />				
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="465">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>

		<br:brPanelGroup>
			<br:brInputText id="txtBanco" value="#{manterPerfilTrocaArqLayoutBean.txtPercentualCustoTransArq}" size="10" maxlength="5" styleClass="HtmlInputTextBradesco"
				alt="percentual" onfocus="loadMasks();" converter="decimalBrazillianConverter" disabled="#{manterPerfilTrocaArqLayoutBean.desabilitaPercentualBanco}">
				<a4j:support event="onblur" reRender="txtCliente" action="#{manterPerfilTrocaArqLayoutBean.calcularPercentualCliente}"
					onsubmit="validarPercentualBanco('altManterPerfilTrocaArqLayoutForm')" oncomplete="validaCampoDecimal(this, '#{msgs.label_numero_invalido}')" />
			</br:brInputText>
			<br:brOutputText  value="#{msgs.label_porcentagem}"  styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brInputText id="txtCliente" value="#{manterPerfilTrocaArqLayoutBean.percentualCliente}" size="10" maxlength="5" styleClass="HtmlInputTextBradesco"
				onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" alt="percentual" onfocus="loadMasks();" converter="decimalBrazillianConverter"
				disabled="true" />
			<br:brOutputText  value="#{msgs.label_porcentagem}"  styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_volume_mensal_registros_trafegados}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

	       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			    <br:brPanelGroup>
					<br:brInputText id="txtVolumeMensalRegTraf" value="#{manterPerfilTrocaArqLayoutBean.volumeMensalTraf}" disabled="#{manterPerfilTrocaArqLayoutBean.flagMeioTransmissao}"
						size="12" maxlength="9" alt="decimalBr9Ponto" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum()">
				    </br:brInputText>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>
			
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_juncao_responsavel_custo}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

       		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
				</br:brPanelGroup>	
				<br:brPanelGroup>
					<br:brInputText id="txtJuncaoRespCustoTransm" value="#{manterPerfilTrocaArqLayoutBean.juncaoRespCustoTrans}"  disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}"  size="12" maxlength="6" styleClass="HtmlInputTextBradesco" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum()">
				    </br:brInputText>				    
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
		
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nome_arquivo_remessa}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtNomeArquivoRemessa" value="#{manterPerfilTrocaArqLayoutBean.nomeArquivoRemessa}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}"  size="70" maxlength="40" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nome_arquivo_retorno}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:20px" >					
				    <br:brPanelGroup>
						 <br:brInputText id="txtNomeArquivoRetorno" value="#{manterPerfilTrocaArqLayoutBean.nomeArquivoRetorno}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}"  size="70" maxlength="40" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_username}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtUsername" value="#{manterPerfilTrocaArqLayoutBean.username}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}"  size="35" maxlength="20" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>				
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_contato_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nome}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtNomeContato" value="#{manterPerfilTrocaArqLayoutBean.nomeContatoCliente}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}" size="100" maxlength="70" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_ddd}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtDDDContato" value="#{manterPerfilTrocaArqLayoutBean.dddContatoCliente}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}" size="10" maxlength="3" styleClass="HtmlInputTextBradesco" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum()">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_telefone}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtTelefoneContato" value="#{manterPerfilTrocaArqLayoutBean.telefoneContatoCliente}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}"
							styleClass="HtmlInputTextBradesco" maxlength="9" size="12" alt="phoneNumber9" onkeypress="onlyNum()" onfocus="loadMasks();">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_email}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtEmailContato" value="#{manterPerfilTrocaArqLayoutBean.emailContatoCliente}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}" size="100" maxlength="70" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_solicitante_agencia_gerente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nome}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtNomeSolictante" value="#{manterPerfilTrocaArqLayoutBean.nomeContatoSolicitante}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}" size="100" maxlength="70" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_ddd}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtDDDSolictante" value="#{manterPerfilTrocaArqLayoutBean.dddContatoSolicitante}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}" size="10" maxlength="3" styleClass="HtmlInputTextBradesco" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum()">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			 <br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			 
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_telefone}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtTelefoneSolictante" value="#{manterPerfilTrocaArqLayoutBean.telefoneContatoSolicitante}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}"
							maxlength="9" size="12" alt="phoneNumber9" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum()" onfocus="loadMasks();">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_email}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtEmailSolictante" value="#{manterPerfilTrocaArqLayoutBean.emailContatoSolicitante}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}" size="100" maxlength="70" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_justificativa_obs}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtJustificativa" value="#{manterPerfilTrocaArqLayoutBean.justificativaSolicitante}" disabled="#{manterPerfilTrocaArqLayoutBean.flagClienteSolicitante}" size="100" maxlength="250" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton styleClass="bto1" value="#{msgs.label_botao_voltar}" action="#{manterPerfilTrocaArqLayoutBean.voltarConsulta}" >	
					<brArq:submitCheckClient/>
				</br:brCommandButton>			
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >
				<br:brCommandButton id="btnAvancar" styleClass="bto1" style="align:left" value="#{msgs.btn_avancar}" action="#{manterPerfilTrocaArqLayoutBean.avancarAlterar}"
					onclick="desbloquearTela(); return validaCamposAvancarAlterar(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.lavel_nivel_controle}',
					'#{msgs.label_tipo_de_controle}','#{msgs.label_periodicidade_inicializacao_contagem}','#{msgs.label_meio_transmissao_principal_remessa}',
					'#{msgs.label_meio_transmissao_principal_retorno}','#{msgs.label_empresa_responsavel_transmissao}','#{msgs.label_responsavel_custo_transmissao_arquivo}',
					'#{msgs.label_volume_mensal_registros_trafegados}','#{msgs.label_nome_cliente}','#{msgs.label_ddd_cliente}','#{msgs.label_telefone_cliente}','#{msgs.label_ramal_cliente}',
					'#{msgs.label_email_cliente}','#{msgs.label_nome_solicitante}','#{msgs.label_ddd_solicitante}','#{msgs.label_telefone_solicitante}','#{msgs.label_ramal_solicitante}',
					'#{msgs.label_email_solicitante}','#{msgs.label_justificativa_obs}','#{msgs.label_juncao_responsavel_custo_transmissao}','#{msgs.label_percentual_custo_transmissao_arquivo}');" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
</brArq:form>