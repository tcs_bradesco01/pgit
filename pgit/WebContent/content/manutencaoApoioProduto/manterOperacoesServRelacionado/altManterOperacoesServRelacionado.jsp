<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="altManterOperacoesServRelacionadoForm" name="altManterOperacoesServRelacionadoForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid> 
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_operacao_servico_relacionado}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_servico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterOperacoesServRelacionadoBean.dsTipoServico}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_servico_relacionado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterOperacoesServRelacionadoBean.dsModalidadeServico}"
					rendered="#{manterOperacoesServRelacionadoBean.dsTipoServico != manterOperacoesServRelacionadoBean.dsModalidadeServico}" />
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_operacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterOperacoesServRelacionadoBean.dsOperacao}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo_operacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterOperacoesServRelacionadoBean.codigoOperacao}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_dados_alteracao}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_natureza_operacao}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<t:selectOneRadio id="rdoTarifavelContabil" value="#{manterOperacoesServRelacionadoBean.rdoTarifavelContabil}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false" >  
					<f:selectItem itemValue="1" itemLabel="#{msgs.label_tarifavel}" />
					<f:selectItem itemValue="2" itemLabel="#{msgs.label_contabil}" />
				</t:selectOneRadio>	
		    </br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tarifa_alcada_agencia}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<t:selectOneRadio id="rdoTarifaAlcadaAgencia"
			value="#{manterOperacoesServRelacionadoBean.rdoTarifaAlcadaAgencia}"
			styleClass="HtmlSelectOneRadioBradesco" layout="spread"
			forceId="true" forceIdIndex="false"
			disabled="#{manterOperacoesServRelacionadoBean.desabilitarTarifaAlcada}">
			<f:selectItem itemValue="1" itemLabel="#{msgs.label_valor}" />
			<f:selectItem itemValue="2" itemLabel="#{msgs.label_percentual}" />
			
			<a4j:support event="onclick" reRender="txtValorTarifa,txtPercentualTarifa" />
		</t:selectOneRadio>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" id="painelValor">
			<t:radio for="rdoTarifaAlcadaAgencia" index="0" />
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="text-align:right;">
				<br:brPanelGroup style="width: 45px">
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brInputText id="txtValorTarifa" value="#{manterOperacoesServRelacionadoBean.valorAlcadaTarifaAgencia}" converter="decimalBrazillianConverter" alt="percentual" maxlength="6" size="10" 
						styleClass="HtmlInputTextBradesco" onfocus="loadMasks();" disabled="#{manterOperacoesServRelacionadoBean.desabilitarValorTarifa}" style="text-align: right">
						
						<a4j:support event="onblur" reRender="txtPercentualTarifa" action="#{manterOperacoesServRelacionadoBean.calcularPercentualTarifa}"/>
					</br:brInputText>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" id="painelPercentua">
			<t:radio for="rdoTarifaAlcadaAgencia" index="1" />
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="text-align:right;">
				<br:brPanelGroup style="width: 15px">
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brInputText size="10" id="txtPercentualTarifa" alt="percentualDefault" value="#{manterOperacoesServRelacionadoBean.percentualAlcadaTarifaAgencia}" converter="decimalBrazillianConverter" 
						styleClass="HtmlInputTextBradesco" onfocus="loadMasks();" disabled="#{manterOperacoesServRelacionadoBean.desabilitarPercentualTarifa}" style="text-align: right">
						
						<a4j:support event="onblur" reRender="txtValorTarifa" action="#{manterOperacoesServRelacionadoBean.calcularValorTarifa}"/>
					</br:brInputText>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton  id="btoVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{manterOperacoesServRelacionadoBean.voltar}" >							
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >		
				<br:brCommandButton id="btnAvancar" style="margin-left:5px" styleClass="bto1" value="#{msgs.btn_avancar}" action="#{manterOperacoesServRelacionadoBean.avancarAlterar}" 
					onclick="javascript:desbloquearTela(); return validaManterOpServRelacionadoAlt(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
																																			 '#{msgs.label_codigo_operacao}',
																																			 '#{msgs.label_codigo_natureza_serv_op_PGIT}',
																																			 '#{msgs.label_valor}',
																																			 '#{msgs.label_percentual}');">							
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	</br:brPanelGrid>
</brArq:form>