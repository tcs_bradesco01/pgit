<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detHistManterOperacoesServRelacionadoForm"
	name="detHistManterOperacoesServRelacionadoForm">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"
		cellspacing="0">

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_servico}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.dsTipoServico}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_servico_relacionado}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.dsModalidadeServico}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_operacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.dsOperacao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_codigo_operacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.cdOperacaoServicoPagamentoIntegrado}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_natureza_operacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.dsNaturezaOperacaoPagamento}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_tarifa_alcada_agencia}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.vrAlcadaTarifaAgencia}"
					converter="currencyDecimalConverter"
					rendered="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.cdTipoCondcTarifa == 1}" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.percentualAlcadaTarifaAgenciaFormatado}"
					rendered="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.cdTipoCondcTarifa == 2}" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{msgs.layoutsArquivos_label_combo_nao_aplica}"
					rendered="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.cdTipoCondcTarifa == 0}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_label_grid_tipomanutencao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.dsIndicadorManutencao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_data_hora_inclusao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.dataHoraInclusaoRegistroFormatada}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_usuario}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.usuarioInclusao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_tipo_canal}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.tipoCanalInclusaoFormatado}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_complemento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.cdOperCanalInclusao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_dataHoraManutencao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.dataHoraManutencaoRegistroFormatada}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_usuario}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.usuarioManutencao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_tipo_canal}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.tipoCanalManutencaoFormatado}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_complemento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterOperacoesServRelacionadoBean.saidaDetalheHistorico.cdOperacaoCanalManutencao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="2" width="100%" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup style="text-align:left;width:150px">
				<br:brCommandButton id="btoVoltar" styleClass="bto1"
					value="#{msgs.btn_voltar}"
					action="#{manterOperacoesServRelacionadoBean.voltar}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>
