<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="histManterOperacoesServRelacionadoForm" name="histManterOperacoesServRelacionadoForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_servico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboTipoServico" value="#{manterOperacoesServRelacionadoBean.tipoServicoHistorico}" disabled="#{manterOperacoesServRelacionadoBean.btoAcionado}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterOperacoesServRelacionadoBean.listarArgumentoPesquisaServicoModalidade}"/>		
							<a4j:support event="onchange" reRender="cboModalidadeServico,cboOperacao" action="#{manterOperacoesServRelacionadoBean.carregaComboModalidadeHistorico}"/>	
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>										
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_servico_relacionado}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboModalidadeServico" value="#{manterOperacoesServRelacionadoBean.modalidadeServicoHistorico}" disabled="#{manterOperacoesServRelacionadoBean.tipoServicoHistorico == 0 
				    		|| manterOperacoesServRelacionadoBean.tipoServicoHistorico == null || manterOperacoesServRelacionadoBean.btoAcionado}">
				    		
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterOperacoesServRelacionadoBean.listaModalidadeHistorico}" />	
							<a4j:support event="onchange" reRender="cboOperacao" action="#{manterOperacoesServRelacionadoBean.carregaComboOperacaoHistorico}"/>			
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>										
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_operacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboOperacao" value="#{manterOperacoesServRelacionadoBean.operacaoHistorico}" disabled="#{manterOperacoesServRelacionadoBean.tipoServicoHistorico == 0 
				    		|| manterOperacoesServRelacionadoBean.tipoServicoHistorico == null || manterOperacoesServRelacionadoBean.btoAcionado}">
				    		
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterOperacoesServRelacionadoBean.listaOperacaoHistorico}"/>		
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>										
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		 
		 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0142_label_grid_dataManutencao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
				<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">	
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
							<br:brPanelGroup rendered="#{manterOperacoesServRelacionadoBean.btoAcionado}">
							 	<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioDe');" 
							 		id="calendarioDe" value="#{manterOperacoesServRelacionadoBean.dataManutencaoInicio}" disabled="true">
								</app:calendar>
							</br:brPanelGroup>
							<br:brPanelGroup rendered="#{!manterOperacoesServRelacionadoBean.btoAcionado}">
							 	<app:calendar id="calendarioDeDes" value="#{manterOperacoesServRelacionadoBean.dataManutencaoInicio}" >
								</app:calendar>
							</br:brPanelGroup>	
												
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.histContasManterContrato_a}"/>
							
							<br:brPanelGroup rendered="#{manterOperacoesServRelacionadoBean.btoAcionado}">
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioAte');" 
									id="calendarioAte" value="#{manterOperacoesServRelacionadoBean.dataManutencaoFim}" disabled="true">
								</app:calendar>		
							</br:brPanelGroup>
							<br:brPanelGroup rendered="#{!manterOperacoesServRelacionadoBean.btoAcionado}">
								<app:calendar id="calendarioAteDes" value="#{manterOperacoesServRelacionadoBean.dataManutencaoFim}" >
								</app:calendar>		
							</br:brPanelGroup>							
						</br:brPanelGroup>			    
					</br:brPanelGrid>
				</a4j:outputPanel>										
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >		
				<br:brCommandButton id="btnLimparCampos" style="margin-left:5px"   styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{manterOperacoesServRelacionadoBean.limparCamposHistorico}" >							
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" style="margin-left:5px" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{manterOperacoesServRelacionadoBean.consultarHistorico}" 
					disabled="#{manterOperacoesServRelacionadoBean.btoAcionado}">							
					<brArq:submitCheckClient/>
				</br:brCommandButton>	
			</br:brPanelGroup> 
		</br:brPanelGrid>
		
		<f:verbatim> <br> </f:verbatim>  
	
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">		
	 
				<app:scrollableDataTable id="dataTable" value="#{manterOperacoesServRelacionadoBean.listaGridHistorico}" var="varResult" 
					rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >
					
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
						  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
						</f:facet>		
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
							layout="spread" forceId="true" forceIdIndex="false"
							value="#{manterOperacoesServRelacionadoBean.itemSelecionadoHistorico}" >
							<f:selectItems value="#{manterOperacoesServRelacionadoBean.listaControleRadioHistorico}"/>
							<a4j:support event="onclick" reRender="btnDetalhar" />
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_servico}" style="text-align:center;width:250" />
						</f:facet>
						<br:brOutputText value="#{varResult.tipoServicoFormatado}"/>
					</app:scrollableColumn>  
	
					<app:scrollableColumn styleClass="colTabLeft" width="400px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_servico_relacionado}" style="text-align:center;width:400px" />
						</f:facet>
						<br:brOutputText value="#{varResult.modalidadeServicoFormatado}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabLeft" width="400px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_operacao_catalogo}" style="text-align:center;width:400px" />
						</f:facet>
						<br:brOutputText value="#{varResult.operacaoCatalogoFormatado}"/>
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabRight" width="200px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_datahoramanutencao}" style="text-align:center;width:200" />
						</f:facet>
						 <br:brOutputText value="#{varResult.dataHoraManutencaoFormatada}"/>
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >
						<f:facet name="header"> 
						  <br:brOutputText value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_usuario_manutencao}" style="text-align:center;width:200" />
						</f:facet>
						 <br:brOutputText value="#{varResult.usuarioManutencao}"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >
						<f:facet name="header"> 
						  <br:brOutputText value="#{msgs.hisManterCadastroContaSalarioContaDestino2_tipo_manutencao}" style="text-align:center;width:200" />
						</f:facet>
						 <br:brOutputText value="#{varResult.dsIndicadorTipoManutencao}"/>
					</app:scrollableColumn>		
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterOperacoesServRelacionadoBean.consultarHistorico}"  > 
				 <f:facet name="first">
				    <brArq:pdcCommandButton id="primeira"
				      styleClass="bto1" 
				      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido"
				      styleClass="bto1"  style="margin-left: 3px;"
				      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior"
				      styleClass="bto1"  style="margin-left: 3px;"
				      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton id="proxima"
				      styleClass="bto1"  style="margin-left: 3px;"
				      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido"
				      styleClass="bto1"  style="margin-left: 3px;"
				      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton id="ultima"
				      styleClass="bto1"  style="margin-left: 3px;"
				      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  </f:facet>
				</brArq:pdcDataScroller>	 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton  id="btoVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{manterOperacoesServRelacionadoBean.voltar}" >							
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="text-align:right;width:600px" >		
				<br:brCommandButton id="btnLimpar" style="margin-left:5px" styleClass="bto1" value="#{msgs.label_limpar}" action="#{manterOperacoesServRelacionadoBean.limparHistorico}" >							
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton  id="btnDetalhar" style="margin-left:5px" disabled="#{empty manterOperacoesServRelacionadoBean.itemSelecionadoHistorico}" styleClass="bto1" value="#{msgs.btn_detalhar}" action="#{manterOperacoesServRelacionadoBean.detalharHistorico}" >							
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>