<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detFormaLiquidacaoPagamentoIntegradoForm" name="detFormaLiquidacaoPagamentoIntegradoForm" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_formaliquidacao}: "/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_formaliquidacao}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value=" #{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.dsFormaLiquidacao}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_path_horariolimiteAgendamento}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value=" #{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.hrLimiteProcessamentoPagamento}" />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_margemseguranca}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtMinutosMargemSeguranca}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" rendered="#{formaLiqPagtoIntBean.habilitaCamposHrMinValorSuperior}">
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.horario_limite_agendamento_valor_superior}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value=" #{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.hrLimiteValorSuperior}" />
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.margem_seguranca_minutos_valor_superior}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtMinutosValorSuperior}" />
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{formaLiqPagtoIntBean.habilitaCamposHrMinValorSuperior}" />

	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_path_horarioInicioConsultaSaldoDiario}: "/>
		<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.hrConsultasSaldoPagamento}"/>
	</br:brPanelGroup>
	
 
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"  rendered="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.cdFormaLiquidacao == 1}"  cellpadding="0" cellspacing="0" >
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_horarioInicioConsultaSaldoSalarioDiario}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.hrConsultaFolhaPgto}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_centrocusto}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.situacao}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" rendered="#{formaLiqPagtoIntBean.formaLiquidacaoTituloDet}">
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_parametrosCamaraInterbancariaPagamentos}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteAgendamento}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdTempoAgendamentoCobranca}"
					rendered="#{not empty formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdTempoAgendamentoCobranca && formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdTempoAgendamentoCobranca > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteEfetivacaoCiclica}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdTempoEfetivacaoCiclicaCobranca}"
					rendered="#{not empty formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdTempoEfetivacaoCiclicaCobranca && formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdTempoEfetivacaoCiclicaCobranca > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteEfetivacaoDiaria}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdTempoEfetivacaoDiariaCobranca}"
					rendered="#{not empty formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdTempoEfetivacaoDiariaCobranca && formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdTempoEfetivacaoDiariaCobranca > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_QtdeLimiteReprocessamentoTempoExcedido}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdLimiteProcessamentoExcedido}"
					rendered="#{not empty formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdLimiteProcessamentoExcedido && formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.qtdLimiteProcessamentoExcedido > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>			
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>	
		
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_data_hora_inclusao}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.hrInclusaoRegistro}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_usuario}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.cdAutenticacaoSegregacaoInclusao}"  />			
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_tipo_canal}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.canalInclusao}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_complemento}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.nmOperacaoFluxoInlcusao}"  />			
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_data_hora_manutencao}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.hrManutencaoRegistro}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_usuario}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.cdAutenticacaoSegregacaoManutencao}"  />			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>				
		
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_tipo_canal}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.canalManutencao}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_complemento}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharLiquidacaoPagto.nmOperacaoFluxoManutencao}"  />			
		</br:brPanelGroup>					
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:left" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_btn_voltar}" action=" #{formaLiqPagtoIntBean.voltarInicio}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
