<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="altFormaLiquidacaoPagamentoIntegradoForm" name="altFormaLiquidacaoPagamentoIntegradoForm" >
<br:brPanelGrid columns="1" width="955" cellpadding="0" cellspacing="0" style="margin-left:5px">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.altFormaLiquidacaoPagamentoIntegrado_formaliquidacao}: "/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" border="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.altFormaLiquidacaoPagamentoIntegrado_formaliquidacao}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.dsFormaLiquidacao}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.altFormaLiquidacaoPagamentoIntegrado_horariolimitepagamento}: "/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" />
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" />
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" />
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" />

		<br:brPanelGroup style="margin-bottom:5px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.altFormaLiquidacaoPagamentoIntegrado_path_horariolimiteAgendamento}: "/>
		</br:brPanelGroup>

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_margemseguranca}:" />
		</br:brPanelGroup>

		<br:brPanelGroup style="margin-bottom:5px;" rendered="#{formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperiorAlt}">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.horario_limite_agendamento_valor_superior}:"/>
		</br:brPanelGroup>
   		<br:brPanelGroup rendered="#{!formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperiorAlt}">
		</br:brPanelGroup>

		<br:brPanelGroup rendered="#{formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperiorAlt}">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.margem_seguranca_minutos_valor_superior}:" />
		</br:brPanelGroup>
   		<br:brPanelGroup rendered="#{!formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperiorAlt}">
		</br:brPanelGroup>

	    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
		    <br:brInputText id="txtHorarioLimite"
		    	onblur="validarHora(this,'#{msgs.label_hora_invalida}')"
		    	styleClass="HtmlInputTextBradesco"
		    	size="13"
		    	maxlength="5"
		    	alt="horaMinSeg"
		    	value="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.hrLimiteProcessamento}" />
		</br:brPanelGrid>

		<br:brPanelGroup>
   			<br:brInputText id="txtMargemSeguranca"
   				onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
   				onkeypress="onlyNum();"
   				size="13"
   				maxlength="3"
   				styleClass="HtmlInputTextBradesco"
   				style="margin-left:20px"
   				value="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.qtMinutoMargemSeguranca}" />
   		</br:brPanelGroup>

   		<br:brPanelGroup rendered="#{formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperiorAlt}">
		    <br:brInputText id="hrLimiteValorSuperior"
		    	onblur="validarHora(this,'#{msgs.label_hora_invalida}')"
		    	styleClass="HtmlInputTextBradesco"
		    	size="13"
		    	maxlength="5"
		    	alt="horaMinSeg"
		    	value="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.hrLimiteValorSuperior}" />
		</br:brPanelGroup>
   		<br:brPanelGroup rendered="#{!formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperiorAlt}">
		</br:brPanelGroup>

		<br:brPanelGroup rendered="#{formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperiorAlt}">
   			<br:brInputText id="qtMinutosValorSuperior"
   				onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
   				converter="javax.faces.Integer"
   				onkeypress="onlyNum();"
   				size="13"
   				maxlength="3"
   				styleClass="HtmlInputTextBradesco"
   				value="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.qtMinutosValorSuperior}" />
   		</br:brPanelGroup>
   		<br:brPanelGroup rendered="#{!formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperiorAlt}">
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" id="panelHrInicioConsulta">
		
			<br:brPanelGroup style="margin-bottom:5px;"  >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.altFormaLiquidacaoPagamentoIntegrado_path_horarioInicioConsultaSaldoDiario}: "/>
			</br:brPanelGroup>
			
		   <br:brPanelGroup>					
			    <br:brInputText onblur="validarHora(this,'#{msgs.label_hora_invalida}')" id="txtHorarioLimiteSaldoDiario" styleClass="HtmlInputTextBradesco" size="13" maxlength="5" value="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.hrConsultasSaldoPagamento}" alt="horaMinSeg" />
			</br:brPanelGroup>
			
			<br:brPanelGroup rendered="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.cdFormaLiquidacao == 1}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_horarioInicioConsultaSaldoSalarioDiario}: " />
			</br:brPanelGroup>
			
			<br:brPanelGroup rendered="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.cdFormaLiquidacao == 1}">
	   			<br:brInputText onblur="validarHora(this,'#{msgs.label_hora_invalida}')" id="txtHorarioLimiteSaldoDiarioSalario"  size="13" maxlength="5" styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.hrConsultaFolhaPgto}"  alt="horaMinSeg"/>
	   		</br:brPanelGroup>
		</br:brPanelGrid>
		
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		 
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" >	
		    <br:brPanelGroup >
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="600" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_centrocusto}: "/>
			</br:brPanelGroup>

			
			<a4j:region renderRegionOnly="true">
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
					<br:brInputText   styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.centroCustoLupaFiltro}" size="5" maxlength="4" id="centroCustoFiltro"/>
					<f:verbatim>&nbsp;</f:verbatim>
		 			<br:brCommandButton id="btnCentroCusto" style="cursor: hand;" image="/images/lupa.gif" value=""  action="#{formaLiqPagtoIntBean.obterCentroCusto}" onclick="javascript: desbloquearTela(); return checaCentroCusto(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_centrocusto}');" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>					
					<f:verbatim>&nbsp;</f:verbatim>
					<f:verbatim>&nbsp;</f:verbatim>
					<br:brSelectOneMenu id="centroCusto" value="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.cdSistema}" disabled="#{empty formaLiqPagtoIntBean.listaCentroCustoLupa}" >
						<f:selectItem itemValue="" itemLabel="#{msgs.conCadastroProcessosControle_label_combo_selecione}"/>
						<f:selectItems value="#{formaLiqPagtoIntBean.listaCentroCustoLupaSelectItem}" />		
						<brArq:commonsValidator type="required" arg="#{msgs.conCadastroProcessosControle_label_centro_custo}" server="false" client="true"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
			</a4j:region>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" rendered="#{formaLiqPagtoIntBean.formaLiquidacaoTituloAlt}">
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_parametrosCamaraInterbancariaPagamentos}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteAgendamento}: " />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brInputText id="qtdTempoAgendamentoCobranca" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
	   				converter="javax.faces.Integer" onkeypress="onlyNum();" size="13" maxlength="4"
	   				styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.qtdTempoAgendamentoCobranca}" />
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteEfetivacaoCiclica}: " />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brInputText id="qtdTempoEfetivacaoCiclicaCobranca" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
	   				converter="javax.faces.Integer" onkeypress="onlyNum();" size="13" maxlength="4"
	   				styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.qtdTempoEfetivacaoCiclicaCobranca}" />
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteEfetivacaoDiaria}: " />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brInputText id="qtdTempoEfetivacaoDiariaCobranca" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
	   				converter="javax.faces.Integer" onkeypress="onlyNum();" size="13" maxlength="4"
	   				styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.qtdTempoEfetivacaoDiariaCobranca}" />
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_QtdeLimiteReprocessamentoTempoExcedido}: " />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brInputText id="qtdLimiteProcessamentoExcedido" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
	   				converter="javax.faces.Integer" onkeypress="onlyNum();" size="13" maxlength="3"
	   				styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaAlterarHistLiquidacaoPagto.qtdLimiteProcessamentoExcedido}" />
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>

 	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup style="text-align:left;">
			<br:brCommandButton styleClass="bto1" value="#{msgs.altFormaLiquidacaoPagamentoIntegrado_btn_voltar}" action="#{formaLiqPagtoIntBean.voltarInicio}">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:100%">
			<br:brCommandButton id="btnAvancar" onclick="desbloquearTela();return checaCamposObrigatoriosAlterar('#{msgs.label_ocampo}',
				'#{msgs.label_necessario}', '#{msgs.altFormaLiquidacaoPagamentoIntegrado_label_horariolimite}',
				'#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_prioridadedebito}')" styleClass="bto1"
				value="#{msgs.altFormaLiquidacaoPagamentoIntegrado_btn_avancar}" action="#{formaLiqPagtoIntBean.avancarAlterarLiqPagtoIntegrado}">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>		
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>