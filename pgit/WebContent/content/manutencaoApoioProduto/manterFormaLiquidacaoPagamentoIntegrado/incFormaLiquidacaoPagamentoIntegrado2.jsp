<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incFormaLiquidacaoPagamentoIntegrado2Form" name="incFormaLiquidacaoPagamentoIntegrado2Form" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_formaliquidacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_formaliquidacao}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.formaLiquidacao}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_grid_horariolimiteAgendamento}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.hrLimiteProcessamento}" />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado2_label_margemseguranca}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtMinutoMargemSeguranca}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" rendered="#{formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperior}">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.horario_limite_agendamento_valor_superior}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.hrLimiteValorSuperior}" />
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.margem_seguranca_minutos_valor_superior}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtMinutosValorSuperior}" />
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperior}"/>

	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_grid_horarioInicioConsultaSaldoDiario}: "/>
		<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.hrConsultasSaldoPagamento}"/>
	</br:brPanelGroup>
	
 
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"  rendered="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.cdFormaLiquidacao == 1}"  cellpadding="0" cellspacing="0" >
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_horarioInicioConsultaSaldoSalarioDiario}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.hrConsultaFolhaPgto}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>


	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado2_label_centrocusto}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.centroCusto}" /> <%--entradaIncluirLiquidacaoPagto.cdSistema}" />--%>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid id="panelCIP" columns="1" cellpadding="0" cellspacing="0" rendered="#{formaLiqPagtoIntBean.formaLiquidacaoTituloInc}">
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_parametrosCamaraInterbancariaPagamentos}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteAgendamento}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoAgendamentoCobranca}"
					rendered="#{not empty formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoAgendamentoCobranca && formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoAgendamentoCobranca > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteEfetivacaoCiclica}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoEfetivacaoCiclicaCobranca}"
					rendered="#{not empty formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoEfetivacaoCiclicaCobranca && formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoEfetivacaoCiclicaCobranca > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteEfetivacaoDiaria}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoEfetivacaoDiariaCobranca}"
					rendered="#{not empty formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoEfetivacaoDiariaCobranca && formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoEfetivacaoDiariaCobranca > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_QtdeLimiteReprocessamentoTempoExcedido}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdLimiteProcessamentoExcedido}"
					rendered="#{not empty formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdLimiteProcessamentoExcedido && formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdLimiteProcessamentoExcedido > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

 	<br:brPanelGrid columns="3" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" border="0">	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton styleClass="bto1" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado2_btn_voltar}" action="#{formaLiqPagtoIntBean.voltarInclusao}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnConfirmar"   styleClass="bto1" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado2_btn_confirmar}" action="#{formaLiqPagtoIntBean.confirmarIncluisaoLiqPagtoIntegrado}" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela(); return false; }">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" style="height:350px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
