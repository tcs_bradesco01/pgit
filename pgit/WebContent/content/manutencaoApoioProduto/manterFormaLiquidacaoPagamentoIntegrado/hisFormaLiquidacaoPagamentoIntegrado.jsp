<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="hisFormaLiquidacaoPagamentoIntegradoForm" name="hisFormaLiquidacaoPagamentoIntegradoForm" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_argumentopesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_formaliquidacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				<br:brPanelGroup>		
					<br:brSelectOneMenu id="formaLiquidacao" value="#{formaLiqPagtoIntBean.entradaListarHistLiquidacaoPagto.cdFormaLiquidacao}" disabled="#{formaLiqPagtoIntBean.btoAcionadoHistorico}">
						<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
						<f:selectItems value="#{formaLiqPagtoIntBean.listaFormaLiquidacao}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>			
			</br:brPanelGrid>
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Per�odo de Manuten��o: "/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >	
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				<br:brPanelGroup>
					<br:brPanelGroup rendered="#{!formaLiqPagtoIntBean.btoAcionadoHistorico}">
						<app:calendar id="calendarioDe" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioDe');" value="#{formaLiqPagtoIntBean.dataInicioManutencao}">
							<f:converter converterId="dateBrazillianConverter"/>
						</app:calendar>
					</br:brPanelGroup>
					<br:brPanelGroup rendered="#{formaLiqPagtoIntBean.btoAcionadoHistorico}">
						<app:calendar id="calendarioDeDes" value="#{formaLiqPagtoIntBean.dataInicioManutencao}" disabled="true">
							<f:converter converterId="dateBrazillianConverter"/>
						</app:calendar>
					</br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.cmpi0007_label_ate}"/>
					<br:brPanelGroup rendered="#{!formaLiqPagtoIntBean.btoAcionadoHistorico}">
						<app:calendar id="calendarioAte" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioAte');" value="#{formaLiqPagtoIntBean.dataFimManutencao}" disabled="false">
							<f:converter converterId="dateBrazillianConverter"/>
						</app:calendar>	
					</br:brPanelGroup>
					<br:brPanelGroup rendered="#{formaLiqPagtoIntBean.btoAcionadoHistorico}">
						<app:calendar id="calendarioAteDes" value="#{formaLiqPagtoIntBean.dataFimManutencao}" disabled="true">
							<f:converter converterId="dateBrazillianConverter"/>
						</app:calendar>	
					</br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
		</a4j:outputPanel>
			
					
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_btn_limparcampos}" action="#{formaLiqPagtoIntBean.limparCamposHistorico}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton disabled="#{formaLiqPagtoIntBean.btoAcionadoHistorico}" id="btnConsultar"  styleClass="bto1" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_btn_consultar}" action="#{formaLiqPagtoIntBean.listarHistoricoLiqPagtoIntegrado}" onclick="javascript:desbloquearTela(); return validarData(document.forms[1],'#{msgs.label_periodo_invalido}','#{msgs.label_periodo_manutencao}', '#{msgs.label_ocampo}', '#{msgs.label_necessario}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px;  height:170" >	
			<app:scrollableDataTable id="dataTable" value="#{formaLiqPagtoIntBean.listaHistLiquidacaoPagto}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">			
			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
				<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
					value="#{formaLiqPagtoIntBean.itemSelecionadoListaHistorico}">
					<f:selectItems value="#{formaLiqPagtoIntBean.selecionaHistLiquidacaoPagto}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sorLista" index="#{parametroKey}" />
			</app:scrollableColumn>			
			
			<app:scrollableColumn  width="100px" styleClass="colTabRight" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_codigo}" style="width:100;text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.cdFormaLiquidacao}"/>
			 </app:scrollableColumn>
			
			<app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_descricao}" style="width:200px;text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsFormaLiquidacao}"/>
			 </app:scrollableColumn>			

			<app:scrollableColumn  width="200px" styleClass="colTabCenter" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_label_grid_datamanutencao}" style="width:200px;text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.hrManutencaoRegistro}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_label_grid_responsavel}"style="width:200px;text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdUsuarioManutencao}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_label_grid_tipomanutencao}" style="width:200px;text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoManutencao}"/>
			 </app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{formaLiqPagtoIntBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
 	<br:brPanelGrid columns="3" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" border="0">	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton styleClass="bto1" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_btn_voltar}" action="#{formaLiqPagtoIntBean.voltarInicio}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty formaLiqPagtoIntBean.itemSelecionadoListaHistorico}" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_btn_detalhar}" action="#{formaLiqPagtoIntBean.detalharHistoricoLiqPagtoIntegrado}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	</a4j:outputPanel>
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
