<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="incFormaLiquidacaoPagamentoIntegradoForm" name="incFormaLiquidacaoPagamentoIntegradoForm" >
<br:brPanelGrid columns="1" width="955" cellpadding="0" cellspacing="0" style="margin-left:5px">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_formaliquidacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" >	
	    <br:brPanelGroup >
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" border="0">
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_formaliquidacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" >	
	    <br:brPanelGroup >
		</br:brPanelGroup>	
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>		
			<br:brSelectOneMenu id="formaLiquidacao" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.cdFormaLiquidacao}">
				<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
				<f:selectItems value="#{formaLiqPagtoIntBean.listaFormaLiquidacao}"/>
				<a4j:support status="statusAguarde" event="onchange"
					reRender="panelHrInicioConsulta,panelHorarioLimite,panelCentroCusto,panelCIP"
					oncomplete="loadMasks();"
					action="#{formaLiqPagtoIntBean.limparCamposInclusao}" />
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid style="margin-top:9px" cellpadding="0" cellspacing="0" >	
	    <br:brPanelGroup >
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>		
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_horariolimitepagamento}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" >	
	    <br:brPanelGroup >
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid id="panelHorarioLimite" columns="4" cellpadding="0" cellspacing="0">
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" />
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" />
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" />
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" />

		<br:brPanelGroup style="margin-bottom:5px;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado2_label_horariolimiteAgendamento}:" />
		</br:brPanelGroup>

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_margemseguranca}:" />
		</br:brPanelGroup>

		<br:brPanelGroup style="margin-bottom:5px;" rendered="#{formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperior}">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.horario_limite_agendamento_valor_superior}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-bottom:5px;" rendered="#{!formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperior}">
		</br:brPanelGroup>

		<br:brPanelGroup rendered="#{formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperior}">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.margem_seguranca_minutos_valor_superior}:" />
		</br:brPanelGroup>
		
		<br:brPanelGroup rendered="#{!formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperior}">
		</br:brPanelGroup>

	    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
		    <br:brInputText id="txtHorarioLimite" onblur="validarHora(this,'#{msgs.label_hora_invalida}')"
		    	styleClass="HtmlInputTextBradesco" size="13" maxlength="5"
		    	value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.hrLimiteProcessamento}" alt="horaMinSeg" />
		</br:brPanelGrid>

		<br:brPanelGroup>
   			<br:brInputText id="txtMargemSeguranca" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
   				converter="javax.faces.Integer" onkeypress="onlyNum();" size="13" maxlength="3"
   				styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtMinutoMargemSeguranca}" />
   		</br:brPanelGroup>

	    <br:brPanelGroup rendered="#{formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperior}">
		    <br:brInputText id="hrLimiteValorSuperior" onblur="validarHora(this,'#{msgs.label_hora_invalida}')"
		    	styleClass="HtmlInputTextBradesco" size="13" maxlength="5"
		    	value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.hrLimiteValorSuperior}" alt="horaMinSeg" />
		</br:brPanelGroup>

	    <br:brPanelGroup rendered="#{!formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperior}">
		</br:brPanelGroup>

		<br:brPanelGroup rendered="#{formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperior}">
   			<br:brInputText id="qtMinutosValorSuperior" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
   				converter="javax.faces.Integer" onkeypress="onlyNum();" size="13" maxlength="3"
   				styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtMinutosValorSuperior}" />
   		</br:brPanelGroup>
   		
		<br:brPanelGroup rendered="#{!formaLiqPagtoIntBean.habilitaHoraQtdeLimiteValorSuperior}">
   		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" />	

	<br:brPanelGrid columns="0" cellpadding="0" cellspacing="0" id="panelHrInicioConsulta">
		<br:brPanelGroup style="margin-bottom:5px;"  >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_grid_horarioInicioConsultaSaldoDiario}:"/>
		</br:brPanelGroup>
		
	   <br:brPanelGroup>					
		    <br:brInputText onblur="validarHora(this,'#{msgs.label_hora_invalida}')" id="txtHorarioLimiteSaldoDiario" styleClass="HtmlInputTextBradesco" size="13" maxlength="5" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.hrConsultasSaldoPagamento}" alt="horaMinSeg" />
		</br:brPanelGroup>
		
		<br:brPanelGroup rendered="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.cdFormaLiquidacao == 1}">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_horarioInicioConsultaSaldoSalarioDiario}:" />
		</br:brPanelGroup>
		
		<br:brPanelGroup rendered="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.cdFormaLiquidacao == 1}">
   			<br:brInputText onblur="validarHora(this,'#{msgs.label_hora_invalida}')" id="txtHorarioLimiteSaldoDiarioSalario"  size="13" maxlength="5" styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.hrConsultaFolhaPgto}"  alt="horaMinSeg"/>
   		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGrid style="margin-top:5px" cellpadding="0" cellspacing="0" >	
		    <br:brPanelGroup >
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" id="panelCentroCusto" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_centrocusto}:"/>
			</br:brPanelGroup>
			
			<a4j:region renderRegionOnly="true">
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.centroCustoLupaFiltro}"  size="5" maxlength="4" id="centroCustoFiltro"/>
					<f:verbatim>&nbsp;</f:verbatim>
		 			<br:brCommandButton id="btnCentroCusto" style="cursor: hand;" image="/images/lupa.gif" value=""  action="#{formaLiqPagtoIntBean.obterCentroCusto}" onclick="javascript: desbloquearTela(); return checaCentroCusto(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_centrocusto}');" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>					
					<f:verbatim>&nbsp;</f:verbatim>
					<f:verbatim>&nbsp;</f:verbatim>
					<br:brSelectOneMenu id="centroCusto" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.cdSistema}" disabled="#{empty formaLiqPagtoIntBean.listaCentroCustoLupa}" >
						<f:selectItem itemValue="" itemLabel="#{msgs.conCadastroProcessosControle_label_combo_selecione}"/>
						<f:selectItems value="#{formaLiqPagtoIntBean.listaCentroCustoLupaSelectItem}" />			
						<brArq:commonsValidator type="required" arg="#{msgs.conCadastroProcessosControle_label_centro_custo}" server="false" client="true"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
			</a4j:region>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGroup id="panelCIP">
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" rendered="#{formaLiqPagtoIntBean.formaLiquidacaoTituloInc}">
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />
			
			<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_parametrosCamaraInterbancariaPagamentos}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />	
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteAgendamento}: " />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brInputText id="qtdTempoAgendamentoCobranca" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
		   				converter="javax.faces.Integer" onkeypress="onlyNum();" size="13" maxlength="4"
		   				styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoAgendamentoCobranca}" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteEfetivacaoCiclica}: " />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brInputText id="qtdTempoEfetivacaoCiclicaCobranca" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
		   				converter="javax.faces.Integer" onkeypress="onlyNum();" size="13" maxlength="4"
		   				styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoEfetivacaoCiclicaCobranca}" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteEfetivacaoDiaria}: " />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brInputText id="qtdTempoEfetivacaoDiariaCobranca" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
		   				converter="javax.faces.Integer" onkeypress="onlyNum();" size="13" maxlength="4"
		   				styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdTempoEfetivacaoDiariaCobranca}" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_QtdeLimiteReprocessamentoTempoExcedido}: " />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brInputText id="qtdLimiteProcessamentoExcedido" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
		   				converter="javax.faces.Integer" onkeypress="onlyNum();" size="13" maxlength="3"
		   				styleClass="HtmlInputTextBradesco" value="#{formaLiqPagtoIntBean.entradaIncluirLiquidacaoPagto.qtdLimiteProcessamentoExcedido}" />
				</br:brPanelGroup>
			</br:brPanelGrid>			
		</br:brPanelGrid>
	</br:brPanelGroup>

	<f:verbatim><hr class="lin"></f:verbatim>

 	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup style="text-align:left;">
			<br:brCommandButton styleClass="bto1" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_btn_voltar}" action="#{formaLiqPagtoIntBean.voltarInicio}">	
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:100%">
			<br:brCommandButton id="btnAvancar" onclick="javascript:desbloquearTela(); return checaCamposObrigatoriosIncluir('#{msgs.label_ocampo}',
				'#{msgs.label_necessario}', '#{msgs.incFormaLiquidacaoPagamentoIntegrado_formaliquidacao}', '#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_horariolimite}',
				'#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_prioridadedebito}')" styleClass="bto1" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_btn_avancar}"
				action="#{formaLiqPagtoIntBean.avancarIncluirLiqPagtoIntegrado}">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>	
<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>