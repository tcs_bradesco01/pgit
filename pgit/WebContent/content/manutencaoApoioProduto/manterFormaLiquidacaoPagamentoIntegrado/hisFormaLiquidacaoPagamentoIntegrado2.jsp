<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="hisFormaLiquidacaoPagamentoIntegrado2Form" name="hisFormaLiquidacaoPagamentoIntegrado2Form" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_formaliquidacao}: "/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_formaliquidacao}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.dsFormaLiquidacao}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_path_horariolimiteAgendamento}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.hrLimiteProcedimentoPagamento}" />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado2_label_margemseguranca}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtMinutoMargemSeguranca}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.horario_limite_agendamento_valor_superior}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.hrLimiteValorSuperior}" />
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.margem_seguranca_minutos_valor_superior}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtMinutosValorSuperior}" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado2_label_centrocusto}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.centroCusto}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado_path_horarioInicioConsultaSaldoDiario}: "/>
		<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.hrConsultasSaldoPagamento}"/>
	</br:brPanelGroup>
 
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"  rendered="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.cdFormaLiquidacao == 1}"  cellpadding="0" cellspacing="0" >
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_horarioInicioConsultaSaldoSalarioDiario}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.hrConsultaFolhaPgto}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" rendered="#{formaLiqPagtoIntBean.formaLiquidacaoTituloHist}">
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_parametrosCamaraInterbancariaPagamentos}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteAgendamento}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdTempoAgendamentoCobranca}"
					rendered="#{not empty formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdTempoAgendamentoCobranca && formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdTempoAgendamentoCobranca > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteEfetivacaoCiclica}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdTempoEfetivacaoCiclicaCobranca}"
					rendered="#{not empty formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdTempoEfetivacaoCiclicaCobranca && formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdTempoEfetivacaoCiclicaCobranca > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_tempoLimiteEfetivacaoDiaria}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdTempoEfetivacaoDiariaCobranca}"
					rendered="#{not empty formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdTempoEfetivacaoDiariaCobranca && formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdTempoEfetivacaoDiariaCobranca > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" />
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detFormaLiquidacaoPagamentoIntegrado_label_QtdeLimiteReprocessamentoTempoExcedido}: " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdLimiteProcessamentoExcedido}"
					rendered="#{not empty formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdLimiteProcessamentoExcedido && formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.qtdLimiteProcessamentoExcedido > 0}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_data_hora_inclusao}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.hrInclusaoRegistro}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_usuario}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.cdAutenticacaoSegurancaInclusao}"  />			
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_tipo_canal}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.canalInclusao}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_complemento}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.nrOperacaoFluxoInclusao}"  />			
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_data_hora_manutencao}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.hrManutencaoRegistroManutencao}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_usuario}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.cdAutenticacaoSegurancaManutencao}"  />			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>				
		
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_tipo_canal}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.canalManutencao}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_complemento}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{formaLiqPagtoIntBean.saidaDetalharHistLiquidacaoPagto.nrOperacaoFluxoManutencao}"  />			
		</br:brPanelGroup>					
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:left" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.hisFormaLiquidacaoPagamentoIntegrado2_btn_voltar}" action="#{formaLiqPagtoIntBean.voltarHistorico}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
