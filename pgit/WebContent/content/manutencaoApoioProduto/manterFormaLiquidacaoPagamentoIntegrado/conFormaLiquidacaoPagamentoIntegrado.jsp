<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conFormaLiquidacaoPagamentoIntegradoForm" name="conFormaLiquidacaoPagamentoIntegradoForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" style="margin-left:5px">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_argumentopesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
    
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Forma de Liquidação Corporativa:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>		
					<br:brSelectOneMenu id="formaLiquidacao" value="#{formaLiqPagtoIntBean.entradaListarLiquidacaoPagto.cdFormaLiquidacao}" disabled="#{formaLiqPagtoIntBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
						<f:selectItems value="#{formaLiqPagtoIntBean.listaFormaLiquidacao}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>	
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	

	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_btn_limparcampos}" action="#{formaLiqPagtoIntBean.limparCampos}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" style="margin-left:5px" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_btn_consultar}" action="#{formaLiqPagtoIntBean.pequisarPagtoIntegrado}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0"  >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px;  height:170" >	
			<app:scrollableDataTable id="dataTable" value="#{formaLiqPagtoIntBean.listaLiquidacaoPagto}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
			
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>		
					<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{formaLiqPagtoIntBean.itemSelecionadoLista}">
						<f:selectItems value="#{formaLiqPagtoIntBean.selecionaLiquidacaoPagto}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>			

				<app:scrollableColumn  width="100px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_codigo}" style="width:100;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdFormaLiquidacao}"  styleClass="tableFontStyle" />
				</app:scrollableColumn>
	
				<app:scrollableColumn  width="210px" styleClass="colTabLeft">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_descricao}" style="width:210;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsFormaLiquidacao}"  styleClass="tableFontStyle" />
				</app:scrollableColumn>
	
				<app:scrollableColumn  width="200px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_horariolimiteAgendamento}" style="width:200;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.hrLimiteProcessamento}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="300px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_margemseguranca}" style="width:300;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.qtMinutosMargemSeguranca}"/>
				</app:scrollableColumn>

				<app:scrollableColumn width="200px" styleClass="colTabRight">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.horario_limite_agendamento_valor_superior}" style="width:200;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.hrLimiteValorSuperior}" />
				</app:scrollableColumn>

				<app:scrollableColumn width="200px" styleClass="colTabRight">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.margem_seguranca_minutos_valor_superior}" style="width:200;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.qtMinutosValorSuperior}" />
				</app:scrollableColumn>

				<app:scrollableColumn  width="200px" styleClass="colTabCenter" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_centrocusto}"style="width:200;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdSistema}"/>
				</app:scrollableColumn>

				<app:scrollableColumn  width="200px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_horarioInicioConsultaSaldoDiario}" style="width:200;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.hrConsultasSaldoPagamento}" styleClass="tableFontStyle"  />
				</app:scrollableColumn>

				<app:scrollableColumn  width="200px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_horarioInicioConsultaSaldoSalarioDiario}" style="width:200;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.hrConsultaFolhaPgto}" styleClass="tableFontStyle"  />
				</app:scrollableColumn>

				<app:scrollableColumn width="200px" styleClass="colTabRight">
					<f:facet name="header">
						<br:brOutputText
							value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_tempoLimiteAgendamentoSegundos}"
							style="width:100%;text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.qtdTempoAgendamentoCobranca}"
						styleClass="tableFontStyle" />
				</app:scrollableColumn>

				<app:scrollableColumn width="200px" styleClass="colTabRight">
					<f:facet name="header">
						<br:brOutputText
							value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_tempoLimiteEfetivacaoCiclicaSegundos}"
							style="width:100%;text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.qtdTempoEfetivacaoCiclicaCobranca}"
						styleClass="tableFontStyle" />
				</app:scrollableColumn>
				
				<app:scrollableColumn width="200px" styleClass="colTabRight">
					<f:facet name="header">
						<br:brOutputText
							value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_tempoLimiteEfetivacaoDiariaSegundos}"
							style="width:100%;text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.qtdTempoEfetivacaoDiariaCobranca}"
						styleClass="tableFontStyle" />
				</app:scrollableColumn>
				
			</app:scrollableDataTable>				 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{formaLiqPagtoIntBean.pesquisar}" > <%--rendered="#{formaLiquidacaoPgtoIntBean.listaGrid!= null && formaLiquidacaoPgtoIntBean.mostraBotoes0001}">--%>
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnDetalhar" styleClass="bto1"  disabled="#{empty formaLiqPagtoIntBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_btn_detalhar}" action="#{formaLiqPagtoIntBean.detalaharLiqPagtoIntegrado}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="bto1"  disabled="False" style="margin-right:5px" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_btn_incluir}" action="#{formaLiqPagtoIntBean.incluirLiqPagtoIntegrado}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAlterar" styleClass="bto1" disabled="#{empty formaLiqPagtoIntBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_btn_alterar}"  action="#{formaLiqPagtoIntBean.alterarLiqPagtoIntegrado}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty formaLiqPagtoIntBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_btn_excluir}" action="#{formaLiqPagtoIntBean.excluirLiqPagtoIntegrado}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnHistorico" styleClass="bto1" disabled="False" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_btn_historico}" action="#{formaLiqPagtoIntBean.historicoLiqPagtoIntegrado}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	</a4j:outputPanel>
	
	<br:brPanelGrid columns="1" style="height:100px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
