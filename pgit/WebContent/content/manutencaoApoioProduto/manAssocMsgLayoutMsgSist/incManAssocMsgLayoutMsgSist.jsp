<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incManAssocMsgLayoutMsgSistForm" name="incManAssocMsgLayoutMsgSistForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_mensagem_do_layout}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoLayoutArquivo" value="#{manAssocMsgLayoutMsgSistBean.cboTipoLayoutArquivo}" converter="javax.faces.Integer">
							<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manAssocMsgLayoutMsgSistBean.listaTipoLayoutArquivo}" />
						</br:brSelectOneMenu>	
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
		    <br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_da_mensagem}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtCodigoMensagem" value="#{manAssocMsgLayoutMsgSistBean.codigoMensagem}" size="9" maxlength="5" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>	
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_mensagem_do_sistema}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_centro_custo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>

			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manAssocMsgLayoutMsgSistBean.centroCusto}"  size="5" maxlength="4" id="txtCentroCusto"/>
						<f:verbatim>&nbsp;</f:verbatim>
			 			<br:brCommandButton id="btnCentroCusto" style="cursor: hand;" image="/images/lupa.gif" value=""  action="#{manAssocMsgLayoutMsgSistBean.obterCentroCusto}" onclick="javascript: desbloquearTela(); return validarCentroCusto(document.forms[1], 'txtCentroCusto', '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_centro_custo}');" >
							<brArq:submitCheckClient/>
						</br:brCommandButton>					
						<f:verbatim>&nbsp;</f:verbatim>
						<f:verbatim>&nbsp;</f:verbatim>
						<br:brSelectOneMenu id="cboCentroCusto" value="#{manAssocMsgLayoutMsgSistBean.cboCentroCusto}" disabled="#{empty manAssocMsgLayoutMsgSistBean.listaCentroCusto}" >
							<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manAssocMsgLayoutMsgSistBean.listaCentroCusto}" />			
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_recurso}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboRecurso" value="#{manAssocMsgLayoutMsgSistBean.cboRecurso}" converter="javax.faces.Integer">
							<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manAssocMsgLayoutMsgSistBean.listaRecurso}" />
						</br:brSelectOneMenu>	
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
		    <br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_idioma}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboIdioma" value="#{manAssocMsgLayoutMsgSistBean.cboIdioma}" converter="javax.faces.Integer">
							<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manAssocMsgLayoutMsgSistBean.listaIdioma}" />
						</br:brSelectOneMenu>	
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
		    <br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_do_evento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtCodigoEvento" value="#{manAssocMsgLayoutMsgSistBean.codigoEvento}" size="9" maxlength="4" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum()" converter="javax.faces.Integer" styleClass="HtmlInputTextBradesco">
					    </br:brInputText>	
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>				
		</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="width:150px">
				<br:brCommandButton styleClass="bto1" value="#{msgs.label_botao_voltar}" action="#{manAssocMsgLayoutMsgSistBean.voltarConsultar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >
				<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.btn_avancar}" action="#{manAssocMsgLayoutMsgSistBean.avancarIncluir}" onclick="javascript: desbloquearTela(); return validarInclusao(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.label_tipo_layout_arquivo}','#{msgs.label_codigo_da_mensagem}','#{msgs.label_centro_custo}','#{msgs.label_recurso}','#{msgs.label_idioma}','#{msgs.label_codigo_do_evento}');">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>			
		</br:brPanelGrid>
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>