<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManAssocMsgLayoutMsgSistForm" name="conManAssocMsgLayoutMsgSistForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio disabled="#{manAssocMsgLayoutMsgSistBean.disableArgumentosConsulta}" id="radioTipoConsultaFiltro" value="#{manAssocMsgLayoutMsgSistBean.radioTipoConsultaFiltro}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="camposConsulta" action="#{manAssocMsgLayoutMsgSistBean.limparCamposConsulta}" status="statusAguarde" />			
			</t:selectOneRadio>
		</br:brPanelGrid>
		
		<a4j:outputPanel id="camposConsulta" style="width: 100%; text-align: left" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					<t:radio for="radioTipoConsultaFiltro" index="0" />
				</br:brPanelGrid>
		
				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
					    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
			
					    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						    <br:brPanelGroup>
								<br:brSelectOneMenu id="cboTipoLayoutArquivoFiltro" value="#{manAssocMsgLayoutMsgSistBean.cboTipoLayoutArquivoFiltro}" disabled="#{manAssocMsgLayoutMsgSistBean.radioTipoConsultaFiltro != 0 || manAssocMsgLayoutMsgSistBean.disableArgumentosConsulta}" converter="javax.faces.Integer">
									<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
									<f:selectItems value="#{manAssocMsgLayoutMsgSistBean.listaTipoLayoutArquivo}" />
								</br:brSelectOneMenu>	
							</br:brPanelGroup>					
						</br:brPanelGrid>
					</br:brPanelGroup>
					
				    <br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>

					<br:brPanelGroup>
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_da_mensagem}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
					    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
			
					    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						    <br:brPanelGroup>
								<br:brInputText id="txtCodigoMensagemFiltro" value="#{manAssocMsgLayoutMsgSistBean.codigoMensagemFiltro}" disabled="#{manAssocMsgLayoutMsgSistBean.radioTipoConsultaFiltro != 0 || manAssocMsgLayoutMsgSistBean.disableArgumentosConsulta}" size="9" maxlength="5" styleClass="HtmlInputTextBradesco">
							    </br:brInputText>	
							</br:brPanelGroup>					
						</br:brPanelGrid>
					</br:brPanelGroup>				
				</br:brPanelGrid>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					<t:radio for="radioTipoConsultaFiltro" index="1" />
				</br:brPanelGrid>
		
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_centro_custo}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
					    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
		
					    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>
								<br:brInputText disabled="#{manAssocMsgLayoutMsgSistBean.radioTipoConsultaFiltro != 1 || manAssocMsgLayoutMsgSistBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{manAssocMsgLayoutMsgSistBean.centroCustoFiltro}"  size="5" maxlength="4" id="txtCentroCustoFiltro"/>
								<f:verbatim>&nbsp;</f:verbatim>
					 			<br:brCommandButton disabled="#{manAssocMsgLayoutMsgSistBean.radioTipoConsultaFiltro != 1 || manAssocMsgLayoutMsgSistBean.disableArgumentosConsulta}" id="btnCentroCusto" style="cursor: hand;" image="/images/lupa.gif" value=""  action="#{manAssocMsgLayoutMsgSistBean.obterCentroCustoFiltro}" onclick="javascript: desbloquearTela(); return validarCentroCusto(document.forms[1], 'txtCentroCustoFiltro', '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_centro_custo}');" >
									<brArq:submitCheckClient/>
								</br:brCommandButton>					
								<f:verbatim>&nbsp;</f:verbatim>
								<f:verbatim>&nbsp;</f:verbatim>
								<br:brSelectOneMenu id="cboCentroCustoFiltro" value="#{manAssocMsgLayoutMsgSistBean.cboCentroCustoFiltro}" disabled="#{manAssocMsgLayoutMsgSistBean.radioTipoConsultaFiltro != 1 || manAssocMsgLayoutMsgSistBean.disableArgumentosConsulta || empty manAssocMsgLayoutMsgSistBean.listaCentroCustoFiltro}" >
									<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
									<f:selectItems value="#{manAssocMsgLayoutMsgSistBean.listaCentroCustoFiltro}" />			
								</br:brSelectOneMenu>
							</br:brPanelGroup>					
						</br:brPanelGrid>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>
					
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>					
			
			<br:brPanelGrid style="margin-left:25px" columns="5" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_recurso}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brSelectOneMenu id="cboRecursoFiltro" value="#{manAssocMsgLayoutMsgSistBean.cboRecursoFiltro}" disabled="#{manAssocMsgLayoutMsgSistBean.radioTipoConsultaFiltro != 1 || manAssocMsgLayoutMsgSistBean.disableArgumentosConsulta}" converter="javax.faces.Integer">
								<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{manAssocMsgLayoutMsgSistBean.listaRecurso}" />
							</br:brSelectOneMenu>	
						</br:brPanelGroup>					
					</br:brPanelGrid>
				</br:brPanelGroup>
				
			    <br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_idioma}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brSelectOneMenu id="cboIdiomaFiltro" value="#{manAssocMsgLayoutMsgSistBean.cboIdiomaFiltro}" disabled="#{manAssocMsgLayoutMsgSistBean.radioTipoConsultaFiltro != 1 || manAssocMsgLayoutMsgSistBean.disableArgumentosConsulta}" converter="javax.faces.Integer">
								<f:selectItem itemValue="" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{manAssocMsgLayoutMsgSistBean.listaIdioma}" />
							</br:brSelectOneMenu>	
						</br:brPanelGroup>					
					</br:brPanelGrid>
				</br:brPanelGroup>
				
			    <br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_do_evento}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brInputText id="txtCodigoEventoFiltro" value="#{manAssocMsgLayoutMsgSistBean.codigoEventoFiltro}" disabled="#{manAssocMsgLayoutMsgSistBean.radioTipoConsultaFiltro != 1 || manAssocMsgLayoutMsgSistBean.disableArgumentosConsulta}" size="9" maxlength="4" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum()" converter="javax.faces.Integer" styleClass="HtmlInputTextBradesco">
						    </br:brInputText>	
						</br:brPanelGroup>					
					</br:brPanelGrid>
				</br:brPanelGroup>				
			</br:brPanelGrid>
		
		</a4j:outputPanel>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{manAssocMsgLayoutMsgSistBean.limpar}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{manAssocMsgLayoutMsgSistBean.consultar}" disabled="#{manAssocMsgLayoutMsgSistBean.disableArgumentosConsulta}" onclick="javascript: desbloquearTela(); return validarPesquisa(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.label_tipo_layout_arquivo}','#{msgs.label_codigo_da_mensagem}','#{msgs.label_centro_custo}','#{msgs.label_recurso}','#{msgs.label_idioma}','#{msgs.label_codigo_do_evento}','#{msgs.label_necessario_selecionar_filtro_pesquisa}');">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<f:verbatim><br></f:verbatim> 
	
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="OverflowScrollableDataTable" >	
				<app:scrollableDataTable id="dataTable" value="#{manAssocMsgLayoutMsgSistBean.listaGridConsultar}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio value="#{manAssocMsgLayoutMsgSistBean.itemSelecionadoConsultar}" id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
							<f:selectItems value="#{manAssocMsgLayoutMsgSistBean.listaControleConsultar}"/>
							<a4j:support event="onclick" reRender="panelBotoes"/>
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
				
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_tipo_layout_arquivo}" style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.tipoLayoutArquivoFormatado}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_mensagem}"  style="width:300; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.mensagemFormatada}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_centro_custo}"  style="width:300; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.centroCustoFormatado}" />
					</app:scrollableColumn>					
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_recurso}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.recursoFormatado}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_idioma}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsIdiomaTextoMensagem}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
						<f:facet name="header">
					    	<h:outputText value="#{msgs.label_evento}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.eventoFormatado}" />
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
			    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manAssocMsgLayoutMsgSistBean.pesquisar}" >
					<f:facet name="first">				    
						<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					</f:facet>
				  	<f:facet name="fastrewind">
				    	<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  	</f:facet>
				  	<f:facet name="previous">
				    	<brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  	</f:facet>
				  	<f:facet name="next">
				    	<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  	</f:facet>
				  	<f:facet name="fastforward">
				    	<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  	</f:facet>
				  	<f:facet name="last">
				    	<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  	</f:facet>
				</brArq:pdcDataScroller>			  
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
			<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup >
					<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.label_limpar}" action="#{manAssocMsgLayoutMsgSistBean.limpar}" style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manAssocMsgLayoutMsgSistBean.itemSelecionadoConsultar}" style="margin-right:5px" value="#{msgs.btn_detalhar}"  action="#{manAssocMsgLayoutMsgSistBean.detalhar}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnIncluir" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_incluir}"  action="#{manAssocMsgLayoutMsgSistBean.incluir}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty manAssocMsgLayoutMsgSistBean.itemSelecionadoConsultar}" value="#{msgs.btn_excluir}"  action="#{manAssocMsgLayoutMsgSistBean.excluir}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>