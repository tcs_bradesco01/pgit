<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conFuncoesAlcadaForm" name="conFuncoesAlcadaForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	
	
	
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFuncoesAlcada_label_centro_custo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			
			 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
   			    <br:brPanelGroup>
   			    	<br:brInputText styleClass="HtmlInputTextBradesco"  value="#{manterFuncoesAlcadaBean.centroCustoPesqFiltro}" disabled="#{manterFuncoesAlcadaBean.btoAcionado}" size="10" maxlength="8" id="txtInicioCentroCusto">
				    </br:brInputText>	
   				</br:brPanelGroup>					
   				 <br:brPanelGroup>   				 
					<h:commandLink id="link" action="#{manterFuncoesAlcadaBean.buscarCentroCustoConsultar}" >
						<br:brGraphicImage url="/images/ficha.gif" style="margin-right:5px;margin-left:5px"   />
					</h:commandLink>   				 
   				</br:brPanelGroup>	
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="centroCusto" value="#{manterFuncoesAlcadaBean.centroCustoFiltro}"  disabled="#{empty manterFuncoesAlcadaBean.listaCentroCustoConsulta || manterFuncoesAlcadaBean.btoAcionado}">
						<f:selectItem itemValue="" itemLabel="#{msgs.incFuncoesAlcada_label_combo_selecione}"/>
						<f:selectItems value="#{manterFuncoesAlcadaBean.listaCentroCustoConsulta}"/>
					</br:brSelectOneMenu>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
					
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFuncoesAlcada_label_codigo_aplicacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText  styleClass="HtmlInputTextBradesco" value="#{manterFuncoesAlcadaBean.codigoAplicacaoFiltro}" size="20" maxlength="8" id="codigoAplicacao" disabled="#{manterFuncoesAlcadaBean.btoAcionado}">
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFuncoesAlcada_label_acao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="acao" value="#{manterFuncoesAlcadaBean.acaoFiltro}" disabled="#{manterFuncoesAlcadaBean.btoAcionado}" >
						<f:selectItem itemValue="" itemLabel="#{msgs.incFuncoesAlcada_label_combo_selecione}"/>
						<f:selectItems value="#{manterFuncoesAlcadaBean.listaAcao}"/>							
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>		
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFuncoesAlcada_label_indicador_acesso}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneRadio id="indicadorAcessoFiltro" styleClass="HtmlSelectOneRadioBradesco" value="#{manterFuncoesAlcadaBean.indicadorAcessoFiltro}" disabled="#{manterFuncoesAlcadaBean.btoAcionado}">  
						<f:selectItem itemValue="1" itemLabel="#{msgs.conFuncoesAlcada_label_regra}" />
						<f:selectItem itemValue="2" itemLabel="#{msgs.conFuncoesAlcada_label_servico}" />
			    	</br:brSelectOneRadio>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>		
		
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="Limpar Campos" action="#{manterFuncoesAlcadaBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conFuncoesAlcada_label_botao_consultar}" action="#{manterFuncoesAlcadaBean.consultar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim> <br> </f:verbatim> 
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
		
		<app:scrollableDataTable id="dataTable" value="#{manterFuncoesAlcadaBean.listaGridPesquisa}" var="result" 
			rows="10" rowIndexVar="parametroKey" 
			rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >
			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
	
				<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
					layout="spread" forceId="true" forceIdIndex="false" 
					value="#{manterFuncoesAlcadaBean.itemSelecionadoLista}">
					<f:selectItems value="#{manterFuncoesAlcadaBean.listaControleRadio}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sorLista" index="#{parametroKey}" />
			</app:scrollableColumn>

			<app:scrollableColumn  width="100px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conFuncoesAlcada_label_centro_custo}" style="text-align:center;width:100" />
			    </f:facet>
			    <br:brOutputText value="#{result.centroCusto}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="120px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conFuncoesAlcada_label_codigo_aplicacao}" style="text-align:center;width:120"  />
			    </f:facet>
			    <br:brOutputText value="#{result.codigoAplicacao}"/>
			 </app:scrollableColumn>
			
			<app:scrollableColumn  width="300px" styleClass="colTabLeft"  >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conFuncoesAlcada_label_acao}" style="text-align:center;width:300"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsAcao}"/>
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn  width="120px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conFuncoesAlcada_label_indicador_acesso}" style="text-align:center;width:120" />
			    </f:facet>
			    <br:brOutputText value="#{msgs.conFuncoesAlcada_label_regra}" rendered="#{result.indicadorAcesso == 1}" />
   			    <br:brOutputText value="#{msgs.conFuncoesAlcada_label_servico}" rendered="#{result.indicadorAcesso == 2}" />
			 </app:scrollableColumn>
			 <app:scrollableColumn  width="300px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conFuncoesAlcada_label_servico}" style="text-align:center;width:300" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsModalidadeServico}"/>
			 </app:scrollableColumn>
			 <app:scrollableColumn  width="150px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.detFuncoesAlcada_label_operacao}" style="text-align:center;width:150" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoServico}"/>
			 </app:scrollableColumn>
			 <app:scrollableColumn  width="60px" styleClass="colTabRight" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conFuncoesAlcada_label_regra}" style="text-align:center;width:60" />
			    </f:facet>			 
			    <br:brOutputText value="#{result.cdRegra}" rendered="#{result.cdRegra != '0'}"/>   			  
   			    <br:brOutputText value="" rendered="#{result.cdRegra == '0'}"/>   			  
			 </app:scrollableColumn>
			</app:scrollableDataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterFuncoesAlcadaBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet> 
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >
			<br:brCommandButton id="btnDetalhar" styleClass="bto1"  disabled="#{empty manterFuncoesAlcadaBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.conFuncoesAlcada_path_detalhar}"  action="#{manterFuncoesAlcadaBean.detalhar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="bto1"  disabled="False" style="margin-right:5px" value="#{msgs.conFuncoesAlcada_path_incluir}"  action="#{manterFuncoesAlcadaBean.incluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAlterar" styleClass="bto1" disabled="#{empty manterFuncoesAlcadaBean.itemSelecionadoLista}"  style="margin-right:5px" value="#{msgs.conFuncoesAlcada_path_alterar}"  action="#{manterFuncoesAlcadaBean.alterar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty manterFuncoesAlcadaBean.itemSelecionadoLista}"  value="#{msgs.conFuncoesAlcada_path_excluir}"  action="#{manterFuncoesAlcadaBean.excluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>
	
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
