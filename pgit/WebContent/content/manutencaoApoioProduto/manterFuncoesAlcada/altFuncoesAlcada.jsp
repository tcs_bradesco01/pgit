<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="alterarManterFuncoesAlcadaForm" name="alterarManterFuncoesAlcadaForm" >


<h:inputHidden id="hiddenObrigatoriedade" value="#{manterFuncoesAlcadaBean.obrigatoriedade}"/>



<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conFuncoesAlcada_label_centro_custo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFuncoesAlcadaBean.centroCustoDesc}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>			
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detFuncoesAlcada_label_codigo_aplicacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFuncoesAlcadaBean.codigoAplicacao}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conFuncoesAlcada_label_acao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFuncoesAlcadaBean.dsAcao}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	<h:inputHidden id="hiddenRadioRegra" value="#{manterFuncoesAlcadaBean.regraOuServicoSelecionado}"/>
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="radio" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterFuncoesAlcadaBean.regraOuServicoSelecionado}">
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
                <a4j:support event="onclick" action="#{manterFuncoesAlcadaBean.habilitaRegraOuServico}" reRender="hiddenRadioRegra,alterarManterFuncoesAlcadaForm"/>
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>
			<t:radio for="radio" index="0" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFuncoesAlcada_label_regra}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" readonly="#{manterFuncoesAlcadaBean.habilitaRegra}"
			    	value="#{manterFuncoesAlcadaBean.regra}" size="30" maxlength="8" id="regra" onkeypress="onlyNum();">
	    		    <brArq:commonsValidator type="integer" arg="#{msgs.conFuncoesAlcada_label_servico}" server="false" client="true" />
				 </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>
			<t:radio for="radio" index="1" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFuncoesAlcada_label_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="servico" value="#{manterFuncoesAlcadaBean.servico}" disabled="#{manterFuncoesAlcadaBean.habilitaServico}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.incFuncoesAlcada_label_combo_selecione}"/>
							<f:selectItems value="#{manterFuncoesAlcadaBean.listaServico}"/>								
			                <a4j:support event="onchange" action="#{manterFuncoesAlcadaBean.listarOperacao}" reRender="servicoRelacionado"/>			
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detFuncoesAlcada_label_operacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="servicoRelacionado" value="#{manterFuncoesAlcadaBean.servicoRelacionado}" disabled="#{manterFuncoesAlcadaBean.habilitaServico ||  manterFuncoesAlcadaBean.servico == null || manterFuncoesAlcadaBean.servico == 0}"  >
						<f:selectItem itemValue="0" itemLabel="#{msgs.incFuncoesAlcada_label_combo_selecione}"/>
						<f:selectItems value="#{manterFuncoesAlcadaBean.listaServicoRelacionado}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<h:inputHidden id="hiddenRadioTratamentoAlcada" value="#{manterFuncoesAlcadaBean.tipoTratamentoAlcada}"/>
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;">
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFuncoesAlcada_label_tipo_tratamento_alcada}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<t:selectOneRadio id="tipoTratamentoAlcada" styleClass="HtmlSelectOneRadioBradesco" value="#{manterFuncoesAlcadaBean.tipoTratamentoAlcada}">
						<f:selectItem itemValue="1" itemLabel="#{msgs.incFuncoesAlcada_label_alcada}" />
						<f:selectItem itemValue="2" itemLabel="#{msgs.incFuncoesAlcada_label_limite_alcada}" />
						<a4j:support event="onclick" reRender="hiddenRadioTratamentoAlcada"/>
			    	</t:selectOneRadio>

				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<h:inputHidden id="hiddenRadioRegraAlcada" value="#{manterFuncoesAlcadaBean.tipoRegraAlcada}"/>
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;">
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFuncoesAlcada_label_tipo_regra_alcada}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<t:selectOneRadio id="tipoRegraAlcada" styleClass="HtmlSelectOneRadioBradesco" value="#{manterFuncoesAlcadaBean.tipoRegraAlcada}">
						<f:selectItem itemValue="1" itemLabel="#{msgs.incFuncoesAlcada_label_sincrono}" />
						<f:selectItem itemValue="2" itemLabel="#{msgs.incFuncoesAlcada_label_assincrono}" />
						<a4j:support event="onclick" action="#{manterFuncoesAlcadaBean.habilitaDadosRetorno}" reRender="centroCustoRetorno,codigoAplicacaoRetorno, hiddenRadioRegraAlcada,txtInicioCentroCustoRetorno" />
			    	</t:selectOneRadio>
			    </br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incFuncoesAlcada_label_dados_retorno_assincrono}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;">
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFuncoesAlcada_label_centro_custo_retorno}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
   			    <br:brPanelGroup>
   			    	<br:brInputText styleClass="HtmlInputTextBradesco"  value="#{manterFuncoesAlcadaBean.centroCustoRetornoPesq}" size="10" maxlength="8" id="txtInicioCentroCustoRetorno" disabled="#{manterFuncoesAlcadaBean.habilitaAssincrono}" >
				    </br:brInputText>	
   				</br:brPanelGroup>					
   				 <br:brPanelGroup>   				 
					<h:commandLink id="linkCentroCustoRetorno" action="#{manterFuncoesAlcadaBean.buscarCentroCustoRetorno}" >
						<br:brGraphicImage url="/images/ficha.gif" style="margin-right:5px;margin-left:5px"   />
					</h:commandLink>   				 
   				</br:brPanelGroup>	
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="centroCustoRetorno" value="#{manterFuncoesAlcadaBean.centroCustoRetorno}" disabled="#{manterFuncoesAlcadaBean.habilitaAssincrono || empty manterFuncoesAlcadaBean.listaCentroCustoRetorno}" >
						<f:selectItem itemValue="" itemLabel="#{msgs.incFuncoesAlcada_label_combo_selecione}"/>
						<f:selectItems value="#{manterFuncoesAlcadaBean.listaCentroCustoRetorno}"/>
					</br:brSelectOneMenu>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;">
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFuncoesAlcada_label_codigo_aplicacao_retorno}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterFuncoesAlcadaBean.codigoAplicacaoRetorno}" size="20" maxlength="8" id="codigoAplicacaoRetorno" disabled="#{manterFuncoesAlcadaBean.habilitaAssincrono}">					
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterFuncoesAlcadaBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar"  onclick="javascript:checaCamposObrigatorios(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.conFuncoesAlcada_label_regra}', '#{msgs.conFuncoesAlcada_label_servico}', '#{msgs.detFuncoesAlcada_label_operacao}', '#{msgs.detFuncoesAlcada_label_selecao_radio}', '#{msgs.incFuncoesAlcada_label_tipo_tratamento_alcada}', '#{msgs.incFuncoesAlcada_label_tipo_regra_alcada}', '#{msgs.incFuncoesAlcada_label_centro_custo_retorno}', '#{msgs.incFuncoesAlcada_label_codigo_aplicacao_retorno}'); return validateForm(document.forms[1]);" 
			   styleClass="bto1" 
			   disabled="#{empty manterFuncoesAlcadaBean.regraOuServicoSelecionado}"
			   value="#{msgs.altFuncoesAlcada_label_botao_avancar}"  action="#{manterFuncoesAlcadaBean.avancarAlterar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
