<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="avancarAlterarManterLancamentoForm" name="avancarAlterarManterLancamentoForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detLancamentoPersonalizado_title_dados_lancamento}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	

	
	<br:brPanelGrid columns="1" style="margin-bottom:2px" cellpadding="0" cellspacing="0" >								
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incLancamentoPersonalizado_label_codigo_lancamento_personalizado}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.codigoLancamentoPersonalizado}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >								
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conLancamentoPersonalizado_label_combo_tipo_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.tipoServicoDesc}"  />
		</br:brPanelGroup>	
	</br:brPanelGrid>			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >								
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conLancamentoPersonalizado_label_combo_modalidade_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.modalidadeServicoDesc}"  />
		</br:brPanelGroup>			
	</br:brPanelGrid>			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >				    
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detLancamentoPersonalizado_label_tipo_lancamento}:"/>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_debito}" rendered="#{manterLancamentoPersonalizadoBean.tipoLancamento==1}"/>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_credito}" rendered="#{manterLancamentoPersonalizadoBean.tipoLancamento==2}"/>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_debito_credito}" rendered="#{manterLancamentoPersonalizadoBean.tipoLancamento==3}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >						
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detLancamentoPersonalizado_label_lancamento_credito}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.codigoLancamentoCredito}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >							
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detLancamentoPersonalizado_label_lancamento_debito}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.codigoLancamentoDebito}" />
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>			
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >						
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conLancamentoPersonalizado_label_restricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterLancamentoPersonalizadoBean.restricao==1}"/>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterLancamentoPersonalizadoBean.restricao==2}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>			
	
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="VOLTAR_ALTERAR">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" disabled="false"  value="#{msgs.incLancamentoPersonalizado2_label_botao_confirmar}"  action="#{manterLancamentoPersonalizadoBean.confirmarAlterar}" onclick="javascript: if (!confirm('Confirma Altera��o?')) { desbloquearTela(); return false; }">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		  
		
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
