<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="historicoManterLancamentoForm" name="historicoManterLancamentoForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conLancamentoPersonalizado_title_argumentosPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_personalizado}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brInputText id="codigoMensagem" styleClass="HtmlInputTextBradesco" value="#{manterLancamentoPersonalizadoBean.codLancamentoPersonalizadoHistorico}" size="30" maxlength="5" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{manterLancamentoPersonalizadoBean.btoAcionadoHistorico}">
						<brArq:commonsValidator type="required" arg="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_personalizado}" server="false" client="true"/>
						<brArq:commonsValidator type="integer" arg="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_personalizado}"  server="false" client="true" />							
				    </br:brInputText>
				</br:brPanelGroup>					
			</br:brPanelGrid>
			
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_periodo_manutencao}:"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
			
	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >								
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.label_de}:"/>
				<br:brPanelGroup rendered="#{!manterLancamentoPersonalizadoBean.btoAcionadoHistorico}">
					<app:calendar id="calendarioDe" value="#{manterLancamentoPersonalizadoBean.filtroDataDe}" 
							oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioDe');">
						<brArq:commonsValidator type="required" arg="#{msgs.histLancamentoPersonalizado_label_data_inicial}" server="false" client="true"/>
					</app:calendar>
				</br:brPanelGroup>
				<br:brPanelGroup rendered="#{manterLancamentoPersonalizadoBean.btoAcionadoHistorico}">
					<app:calendar id="calendarioDeDes" value="#{manterLancamentoPersonalizadoBean.filtroDataDe}" disabled="true">
					</app:calendar>
				</br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_ate}:"/>
				<br:brPanelGroup rendered="#{!manterLancamentoPersonalizadoBean.btoAcionadoHistorico}">
					<app:calendar id="calendarioAte" value="#{manterLancamentoPersonalizadoBean.filtroDataAte}" 
						oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioAte');"					>
						<brArq:commonsValidator type="required" arg="#{msgs.histLancamentoPersonalizado_label_data_final}" server="false" client="true"/>
					</app:calendar>	
				</br:brPanelGroup>
				<br:brPanelGroup rendered="#{manterLancamentoPersonalizadoBean.btoAcionadoHistorico}">
					<app:calendar id="calendarioAteDes" value="#{manterLancamentoPersonalizadoBean.filtroDataAte}" disabled="true">
					</app:calendar>	
				</br:brPanelGroup>		
			</br:brPanelGroup>	
		</br:brPanelGrid>
	</a4j:outputPanel>			
			
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="Limpar Campos" action="#{manterLancamentoPersonalizadoBean.limparDadosHistorico}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" disabled="#{manterLancamentoPersonalizadoBean.listaGridHistorico != null}" onclick="javascript:return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.conLancamentoPersonalizado_label_botao_consultar}" action="#{manterLancamentoPersonalizadoBean.consultarHistorico}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
		
		
			<app:scrollableDataTable id="dataTable" value="#{manterLancamentoPersonalizadoBean.listaGridHistorico}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{manterLancamentoPersonalizadoBean.itemSelecionadoHistorico}">
						<f:selectItems value="#{manterLancamentoPersonalizadoBean.listaControleRadioHistorico}"/>
						<a4j:support event="onclick" reRender="btnDetalhar" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabRight" width="250px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_personalizado}" style="width:250; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.codigoLancamentoPersonalizado}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="170px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.histLancamentoPersonalizado_label_data_manutencao}" style="width:170; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dataManutencaoDesc}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.histLancamentoPersonalizado2_label_responsavel}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.responsavel}"/>
				</app:scrollableColumn>
		
				<app:scrollableColumn styleClass="colTabLeft" width="150px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.histLancamentoPersonalizado_label_tipo_manutencao}" style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{msgs.histLancamentoPersonalizado_label_inclusao}" rendered="#{result.tipoManutencao==1}"/>
   				    <br:brOutputText value="#{msgs.histLancamentoPersonalizado_label_alteracao}" rendered="#{result.tipoManutencao==2}"/>
   				    <br:brOutputText value="#{msgs.histLancamentoPersonalizado_label_exclusao}" rendered="#{result.tipoManutencao==3}"/>

				</app:scrollableColumn>
				
			</app:scrollableDataTable>
			
		</br:brPanelGroup>
	</br:brPanelGrid>		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterLancamentoPersonalizadoBean.pesquisarHistorico}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
		
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterLancamentoPersonalizadoBean.voltarHistorico}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnDetalhar" disabled="#{empty manterLancamentoPersonalizadoBean.itemSelecionadoHistorico}" styleClass="bto1" value="#{msgs.conLancamentoPersonalizado_label_botao_detalhar}"  action="#{manterLancamentoPersonalizadoBean.detalharHistorico}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		  
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
