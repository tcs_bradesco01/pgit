<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detalharManterLancamentoForm" name="detalharManterLancamentoForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detLancamentoPersonalizado_title_dados_lancamento}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >								
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incLancamentoPersonalizado_label_codigo_lancamento_personalizado}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.codigoLancamentoPersonalizado}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >								
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conLancamentoPersonalizado_label_combo_tipo_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.tipoServicoDesc}"  />
		</br:brPanelGroup>	
	</br:brPanelGrid>			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >								
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conLancamentoPersonalizado_label_combo_modalidade_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.modalidadeServicoDesc}"  />
		</br:brPanelGroup>			
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detLancamentoPersonalizado_label_tipo_lancamento}:"/>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_debito}" rendered="#{manterLancamentoPersonalizadoBean.tipoLancamento==1}"/>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_credito}" rendered="#{manterLancamentoPersonalizadoBean.tipoLancamento==2}"/>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_debito_credito}" rendered="#{manterLancamentoPersonalizadoBean.tipoLancamento==3}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >							
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detLancamentoPersonalizado_label_lancamento_credito}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.codigoLancamentoCredito} - #{manterLancamentoPersonalizadoBean.lancamentoCreditoDesc}" rendered="#{manterLancamentoPersonalizadoBean.codigoLancamentoCredito != ''}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="" rendered="#{manterLancamentoPersonalizadoBean.codigoLancamentoCredito == ''}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >							
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detLancamentoPersonalizado_label_lancamento_debito}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.codigoLancamentoDebito} - #{manterLancamentoPersonalizadoBean.lancamentoDebitoDesc}" rendered="#{manterLancamentoPersonalizadoBean.codigoLancamentoDebito != ''}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="" rendered="#{manterLancamentoPersonalizadoBean.codigoLancamentoDebito == ''}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conLancamentoPersonalizado_label_restricao}:"/>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterLancamentoPersonalizadoBean.restricao==1}"/>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterLancamentoPersonalizadoBean.restricao==2}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>			
			
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lancamentoPersonalizado_data_hora_inclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.dataHoraInclusao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lancamentoPersonalizado_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.usuarioInclusao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lancamentoPersonalizado_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.tipoCanalInclusao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lancamentoPersonalizado_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.complementoInclusao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lancamentoPersonalizado_data_hora_manutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.dataHoraManutencao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lancamentoPersonalizado_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.usuarioManutencao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lancamentoPersonalizado_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.tipoCanalManutencao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lancamentoPersonalizado_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLancamentoPersonalizadoBean.complementoManutencao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"   cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="width:750px">
					<br:brCommandButton styleClass="bto1" value="#{msgs.label_botao_voltar}" action="#{manterLancamentoPersonalizadoBean.voltarConsulta}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
