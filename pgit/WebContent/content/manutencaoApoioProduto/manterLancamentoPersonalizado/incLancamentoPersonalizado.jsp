<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>


<brArq:form id="incluirManterLancamentoForm" name="incluirManterLancamentoForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterLancamentoPersonalizadoBean.obrigatoriedade}"/>
<h:inputHidden id="hiddenRestricao" value="#{manterLancamentoPersonalizadoBean.restricao}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detLancamentoPersonalizado_title_dados_lancamento}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
    	
    <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_combo_tipo_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
		    	<br:brSelectOneMenu id="tipoServico" value="#{manterLancamentoPersonalizadoBean.tipoServico}" >
					<f:selectItem itemValue="0" itemLabel="#{msgs.conLancamentoPersonalizado_label_combo_selecione}"/>
					<f:selectItems value="#{manterLancamentoPersonalizadoBean.listaTipoServico}"/>
					<a4j:support event="onchange" action="#{manterLancamentoPersonalizadoBean.listarModalidadeServicoIncluir}"
									reRender="modalidadeServico" />
				</br:brSelectOneMenu>
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_combo_modalidade_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brSelectOneMenu id="modalidadeServico" value="#{manterLancamentoPersonalizadoBean.modalidadeServico}" disabled="#{manterLancamentoPersonalizadoBean.tipoServico==0}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conLancamentoPersonalizado_label_combo_selecione}"/>
						<f:selectItems value="#{manterLancamentoPersonalizadoBean.listaModalidadeServicoIncluir}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
    <f:verbatim><hr class="lin"> </f:verbatim>
    
     <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conLancamentoPersonalizado_label_tipo_lancamento}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
		    <t:selectBooleanCheckbox id="tipoLancamentoCredito" styleClass="HtmlSelectOneRadioBradesco" 
		    		value="#{manterLancamentoPersonalizadoBean.extratoCredito}">
                <a4j:support event="onclick" action="#{manterLancamentoPersonalizadoBean.habilitaCredito}" reRender="codLancCredito"/>
		    </t:selectBooleanCheckbox>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incLancamentoPersonalizado_label_extrato_credito}"/>
		</br:brPanelGroup>					

	    <br:brPanelGroup>
		    <t:selectBooleanCheckbox id="tipoLancamentoDebito" styleClass="HtmlSelectOneRadioBradesco" 
		    		value="#{manterLancamentoPersonalizadoBean.extratoDebito}">
                <a4j:support event="onclick" action="#{manterLancamentoPersonalizadoBean.habilitaDebito}" reRender="codLancDebito"/>
		    </t:selectBooleanCheckbox>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incLancamentoPersonalizado_label_extrato_debito}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incLancamentoPersonalizado_label_codigo_lancamento_credito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterLancamentoPersonalizadoBean.codigoLancamentoCredito}" size="13" maxlength="5" id="codLancCredito" disabled="#{manterLancamentoPersonalizadoBean.habilitaCredito}" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
	    		<brArq:commonsValidator type="integer" arg="#{msgs.incLancamentoPersonalizado_label_codigo_lancamento_credito}"  server="false" client="true" />							
			</br:brInputText>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incLancamentoPersonalizado_label_codigo_lancamento_debito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterLancamentoPersonalizadoBean.codigoLancamentoDebito}" size="13" maxlength="5" id="codLancDebito" disabled="#{manterLancamentoPersonalizadoBean.habilitaDebito}" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
	    		<brArq:commonsValidator type="integer" arg="#{msgs.incLancamentoPersonalizado_label_codigo_lancamento_debito}"  server="false" client="true" />							
			</br:brInputText>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	<f:verbatim><hr class="lin"> </f:verbatim>	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_restricao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
			<t:selectOneRadio id="restricao"  value="#{manterLancamentoPersonalizadoBean.restricao}" styleClass="HtmlSelectOneRadioBradesco" >  
				<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}" />
				<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}" />
				<a4j:support event="onclick" reRender="hiddenRestricao"/>
	    	</t:selectOneRadio>

		</br:brPanelGroup>					
	</br:brPanelGrid>
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterLancamentoPersonalizadoBean.voltarConsulta}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" disabled="false"  value="#{msgs.incLancamentoPersonalizado_label_botao_avancar}"  action="#{manterLancamentoPersonalizadoBean.avancarIncluir}" onclick="javascript:checaCamposObrigatoriosIncluir(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.incLancamentoPersonalizado_label_codigo_lancamento_personalizado}', '#{msgs.conLancamentoPersonalizado_label_combo_tipo_servico}', '#{msgs.conLancamentoPersonalizado_label_combo_modalidade_servico}', '#{msgs.incLancamentoPersonalizado_label_codigo_lancamento_credito}', '#{msgs.incLancamentoPersonalizado_label_codigo_lancamento_debito}', '#{msgs.conLancamentoPersonalizado_label_restricao}', '#{msgs.detLancamentoPersonalizado_label_tipo_lancamento}' );return validateForm(document.forms[1]); ">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		  
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
