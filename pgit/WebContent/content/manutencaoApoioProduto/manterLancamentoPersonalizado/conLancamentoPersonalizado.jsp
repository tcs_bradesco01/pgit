<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>



<brArq:form id="pesqManterLancamentoForm" name="pesqManterLancamentoForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterLancamentoPersonalizadoBean.obrigatoriedade}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conLancamentoPersonalizado_title_argumentosPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio value="#{manterLancamentoPersonalizadoBean.radioArgumentoPesquisa}" disabled="#{manterLancamentoPersonalizadoBean.btoAcionado}" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<a4j:support event="onclick" action="#{manterLancamentoPersonalizadoBean.limparCampos}"
									reRender="codigoMensagem, tipoServico, modalidadeServico" />
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="0" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_personalizado}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brInputText id="codigoMensagem" readonly="#{manterLancamentoPersonalizadoBean.radioArgumentoPesquisa != 0}" disabled="#{manterLancamentoPersonalizadoBean.btoAcionado}" styleClass="HtmlInputTextBradesco" value="#{manterLancamentoPersonalizadoBean.codLancamentoPersonalizadoFiltro}" size="30" maxlength="5" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
						<brArq:commonsValidator type="integer" arg="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_personalizado}"  server="false" client="true" />							
				    </br:brInputText>	

				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="1" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_combo_tipo_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="tipoServico" value="#{manterLancamentoPersonalizadoBean.tipoServicoFiltro}" disabled="#{manterLancamentoPersonalizadoBean.radioArgumentoPesquisa != 1 || manterLancamentoPersonalizadoBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conLancamentoPersonalizado_label_combo_selecione}"/>
						<f:selectItems value="#{manterLancamentoPersonalizadoBean.listaTipoServico}"/>
						<a4j:support event="onchange" action="#{manterLancamentoPersonalizadoBean.listarModalidadeServico}"
									reRender="modalidadeServico" />
					</br:brSelectOneMenu>

				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conLancamentoPersonalizado_label_combo_modalidade_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="modalidadeServico" value="#{manterLancamentoPersonalizadoBean.modalidadeServicoFiltro}" disabled="#{manterLancamentoPersonalizadoBean.radioArgumentoPesquisa != 1 ||  manterLancamentoPersonalizadoBean.tipoServicoFiltro == null || manterLancamentoPersonalizadoBean.tipoServicoFiltro == 0 || manterLancamentoPersonalizadoBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conLancamentoPersonalizado_label_combo_selecione}"/>
						<f:selectItems value="#{manterLancamentoPersonalizadoBean.listaModalidadeServico}"/>
					</br:brSelectOneMenu>
	    		</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="Limpar Campos" action="#{manterLancamentoPersonalizadoBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conLancamentoPersonalizado_label_botao_consultar}" action="#{manterLancamentoPersonalizadoBean.consultar}" onclick="return validateForm(document.forms[1]);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

			<app:scrollableDataTable id="dataTable" value="#{manterLancamentoPersonalizadoBean.listaGridLancamento}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{manterLancamentoPersonalizadoBean.itemSelecionadoLancamento}">
						<f:selectItems value="#{manterLancamentoPersonalizadoBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabRight" width="200px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conLancamentoPersonalizado_label_cod_lancamento_personalizado}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.codLancamento}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="100px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_restricao}" style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{msgs.label_sim}" rendered="#{result.restricao==1}"/>
   				    <br:brOutputText value="#{msgs.label_nao}" rendered="#{result.restricao==2}"/>
				</app:scrollableColumn>
				<app:scrollableColumn styleClass="colTabLeft" width="100px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_tipo_lancamento}" style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{msgs.conLancamentoPersonalizado_label_debito}" rendered="#{result.tipoLancamento==1}"/>
   				    <br:brOutputText value="#{msgs.conLancamentoPersonalizado_label_credito}" rendered="#{result.tipoLancamento==2}"/>
   				    <br:brOutputText value="#{msgs.conLancamentoPersonalizado_label_debito_credito}" rendered="#{result.tipoLancamento==3}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_debito}" style="width:180; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsLancDebito}" rendered="#{result.cdLancDebito > 0}"/>
				    <br:brOutputText value="" rendered="#{result.cdLancDebito == 0}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_credito}" style="width:180; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsLancCredito}" rendered="#{result.cdLancCredito > 0}"/>
				    <br:brOutputText value="" rendered="#{result.cdLancCredito == 0}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_combo_tipo_servico}"style="width:180; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoServico}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="280px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_combo_modalidade_servico}"style="width:280; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoModalidade}"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>
						
		</br:brPanelGroup>
	</br:brPanelGrid>		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterLancamentoPersonalizadoBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	

	<f:verbatim><hr class="lin"> </f:verbatim>

	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup >
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterLancamentoPersonalizadoBean.itemSelecionadoLancamento}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conLancamentoPersonalizado_label_botao_detalhar}"  action="#{manterLancamentoPersonalizadoBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" styleClass="bto1" disabled="false"  style="margin-right:5px" value="#{msgs.conLancamentoPersonalizado_label_botao_incluir}"  action="#{manterLancamentoPersonalizadoBean.incluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" disabled="#{empty manterLancamentoPersonalizadoBean.itemSelecionadoLancamento}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conLancamentoPersonalizado_label_botao_alterar}"  action="#{manterLancamentoPersonalizadoBean.alterar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty manterLancamentoPersonalizadoBean.itemSelecionadoLancamento}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conLancamentoPersonalizado_label_botao_excluir}"  action="#{manterLancamentoPersonalizadoBean.excluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnHistorico" styleClass="bto1" value="#{msgs.conLancamentoPersonalizado_label_botao_historico}"  action="#{manterLancamentoPersonalizadoBean.historico}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
		
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
