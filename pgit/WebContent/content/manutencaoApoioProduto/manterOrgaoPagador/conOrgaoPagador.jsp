<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="conOrgaoPagadorForm" name="conOrgaoPagadorForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="7" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrgaoPagador_label_orgao_pagador}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterOrgaoPagadorBean.orgaoPagadorFiltro.cdOrgaoPagador}" disabled="#{manterOrgaoPagadorBean.btoAcionado}" size="20" maxlength="5" id="orgaoPagador" onkeypress="onlyNum()" onfocus="cleanClipboard()">
						<brArq:commonsValidator type="integer" arg="#{msgs.conOrgaoPagador_label_orgao_pagador}" server="false" client="true" />
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
					
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrgaoPagador_label_situacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="situacao" value="#{manterOrgaoPagadorBean.orgaoPagadorFiltro.cdSituacao}" disabled="#{manterOrgaoPagadorBean.btoAcionado}" style="width:130px" >
						<f:selectItem itemValue="" itemLabel="#{msgs.conOrgaoPagador_label_combo_selecione}"/>
						<f:selectItem itemValue="1" itemLabel="#{msgs.conOrgaoPagador_label_combo_aberta}"/>
						<f:selectItem itemValue="2" itemLabel="#{msgs.conOrgaoPagador_label_combo_fechada}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrgaoPagador_label_categoria}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="categoria" value="#{manterOrgaoPagadorBean.orgaoPagadorFiltro.cdTarifOrgaoPagador}" disabled="#{manterOrgaoPagadorBean.btoAcionado}" style="width:130px" >
						<f:selectItem itemValue="" itemLabel="#{msgs.conOrgaoPagador_label_combo_selecione}"/>
						<f:selectItem itemValue="1" itemLabel="#{msgs.conOrgaoPagador_label_combo_convencional}"/>
						<f:selectItem itemValue="2" itemLabel="#{msgs.conOrgaoPagador_label_combo_pioneira_normal}"/>
						<f:selectItem itemValue="3" itemLabel="#{msgs.conOrgaoPagador_label_combo_pioneira_diferenciada}"/>
					</br:brSelectOneMenu>				
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>		
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrgaoPagador_label_tipo_unidade_organizacional}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoUnidade" value="#{manterOrgaoPagadorBean.orgaoPagadorFiltro.cdTipoUnidadeOrganizacional}" disabled="#{manterOrgaoPagadorBean.btoAcionado}" converter="javax.faces.Integer">
						<f:selectItem itemValue="" itemLabel="#{msgs.conOrgaoPagador_label_combo_selecione}"/>
						<si:selectItems value="#{manterOrgaoPagadorBean.listaTipoUnidade}" var="tipoUnidade" itemLabel="#{tipoUnidade.dsTipoUnidade}" itemValue="#{tipoUnidade.cdTipoUnidade}" />
					</br:brSelectOneMenu>				
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrgaoPagador_label_empresa}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="empresa" value="#{manterOrgaoPagadorBean.orgaoPagadorFiltro.cdPessoaJuridica}" disabled="#{manterOrgaoPagadorBean.btoAcionado}" converter="javax.faces.Long">
						<si:selectItems value="#{manterOrgaoPagadorBean.listaEmpresaGrupoBradesco}" var="empresaBradesco" itemLabel="#{empresaBradesco.codClub} - #{empresaBradesco.dsRazaoSocial}" itemValue="#{empresaBradesco.codClub}" />
					</br:brSelectOneMenu>				
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px">
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrgaoPagador_label_agencia_pacb}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterOrgaoPagadorBean.orgaoPagadorFiltro.nrSeqUnidadeOrganizacional}" size="30" maxlength="8" disabled="#{manterOrgaoPagadorBean.btoAcionado}" id="unidadeOrganizacional" onkeypress="onlyNum()" onfocus="cleanClipboard()" />
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="Limpar Campos" action="#{manterOrgaoPagadorBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conOrgaoPagador_label_botao_consultar}" action="#{manterOrgaoPagadorBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<t:selectOneRadio id="rdoOrgaoPagador" styleClass="HtmlSelectOneRadioBradesco" 
			layout="spread" forceId="true" forceIdIndex="false" 
			value="#{manterOrgaoPagadorBean.itemSelecionadoLista}"
			converter="javax.faces.Integer">
				<si:selectItems value="#{manterOrgaoPagadorBean.listaGridPesquisa}" var="orgaoPagador" itemLabel="" itemValue="#{orgaoPagador.cdOrgaoPagador}"/>
				<a4j:support event="onclick" reRender="panelBotoes" />
			</t:selectOneRadio>

		<br:brPanelGroup styleClass="OverflowScrollableDataTable" >	
		<app:scrollableDataTable id="dataTable" value="#{manterOrgaoPagadorBean.listaGridPesquisa}" var="result" 
			rows="10" rowIndexVar="parametroKey" 
			rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">

			<app:scrollableColumn styleClass="colTabCenter" width="30px">
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
		    	<t:radio for=":conOrgaoPagadorForm:rdoOrgaoPagador" index="#{parametroKey}" />
			</app:scrollableColumn>
			
			<app:scrollableColumn  width="100px" styleClass="colTabRight">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conOrgaoPagador_label_orgao_pagador}" style="width:100; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.cdOrgaoPagador}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conOrgaoPagador_label_tipo_unidade_organizacional}" style="width:200; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoUnidadeOrganizacional}"/>
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn width="160px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conOrgaoPagador_label_empresa}" style="width:160; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsPessoaJuridica}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="190px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conOrgaoPagador_label_agencia_pacb}" style="width:190; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.nrSeqUnidadeOrganizacional} - #{result.dsNrSeqUnidadeOrganizacional}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="80px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conOrgaoPagador_label_situacao}" style="width:80; text-align:center"/>
			    </f:facet>
				<br:brOutputText value="#{msgs.conOrgaoPagador_label_combo_aberta}" rendered="#{result.cdSituacao == 1}"/>
				<br:brOutputText value="#{msgs.conOrgaoPagador_label_combo_fechada}" rendered="#{result.cdSituacao == 2}"/>
			</app:scrollableColumn>
		</app:scrollableDataTable>
		</br:brPanelGroup>

	</br:brPanelGrid>		
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterOrgaoPagadorBean.pesquisar}" rendered="#{!empty manterOrgaoPagadorBean.listaGridPesquisa}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterOrgaoPagadorBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.conOrgaoPagador_path_detalhar}"  action="#{manterOrgaoPagadorBean.detalhar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="bto1" disabled="false"  style="margin-right:5px" value="#{msgs.conOrgaoPagador_path_incluir}"  action="#{manterOrgaoPagadorBean.incluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton  id="btnAlterar" styleClass="bto1" disabled="#{empty manterOrgaoPagadorBean.itemSelecionadoLista}"  style="margin-right:5px" value="#{msgs.conOrgaoPagador_path_alterar}"  action="#{manterOrgaoPagadorBean.alterar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty manterOrgaoPagadorBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.conOrgaoPagador_path_excluir}"  action="#{manterOrgaoPagadorBean.excluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnBloquear" styleClass="bto1" disabled="#{empty manterOrgaoPagadorBean.itemSelecionadoLista}" value="#{msgs.conOrgaoPagador_path_bloquear}"  action="#{manterOrgaoPagadorBean.bloquear}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
