<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>


<brArq:form id="bloqOrgaoPagadorForm" name="bloqOrgaoPagadorForm" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conOrgaoPagador_label_orgao_pagador}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detOrgaoPagador_label_codigo_orgao_pagador}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterOrgaoPagadorBean.detalheSaidaDTO.cdOrgaoPagador}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>				
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conOrgaoPagador_label_tipo_unidade_organizacional}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterOrgaoPagadorBean.detalheSaidaDTO.dsTipoUnidade}"  />
		</br:brPanelGroup>	
	</br:brPanelGrid>					

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conOrgaoPagador_label_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterOrgaoPagadorBean.detalheSaidaDTO.dsPessoa}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>					
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conOrgaoPagador_label_agencia_pacb}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterOrgaoPagadorBean.detalheSaidaDTO.dsSeqUnidadeOrganizacional}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>				

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detOrgaoPagador_label_categoria}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrgaoPagador_label_combo_convencional}" rendered="#{manterOrgaoPagadorBean.detalheSaidaDTO.cdTarifOrgaoPagador == 1}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrgaoPagador_label_combo_pioneira_normal}" rendered="#{manterOrgaoPagadorBean.detalheSaidaDTO.cdTarifOrgaoPagador == 2}"/>
 			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrgaoPagador_label_combo_pioneira_diferenciada}" rendered="#{manterOrgaoPagadorBean.detalheSaidaDTO.cdTarifOrgaoPagador == 3}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>				
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.bloqOrgaoPagador_label_situacao_orgao_pagador}:"/>
				</br:brPanelGroup>			
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<t:selectOneRadio id="situacao" value="#{manterOrgaoPagadorBean.orgaoPagadorBloqueio.cdSituacao}" styleClass="HtmlSelectOneRadioBradesco">  
						<f:selectItem itemValue="1" itemLabel="#{msgs.bloqOrgaoPagador_label_aberto}" />
						<f:selectItem itemValue="2" itemLabel="#{msgs.bloqOrgaoPagador_label_fechado}" />
						<brArq:commonsValidator type="required" arg="#{msgs.bloqOrgaoPagador_label_situacao_orgao_pagador}" server="false" client="true"/>
			    	</t:selectOneRadio>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
					
		<br:brPanelGroup style="width:20px; margin-top:5px" >
		</br:brPanelGroup>	
				<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.bloqOrgaoPagador_label_motivo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText  styleClass="HtmlInputTextBradesco" value="#{manterOrgaoPagadorBean.orgaoPagadorBloqueio.dsMotivoBloqueio}" size="50" maxlength="200" id="motivo" style="margin-top:5px" >
				    	<brArq:commonsValidator type="required" arg="#{msgs.bloqOrgaoPagador_label_motivo}" server="false" client="true"/>
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		
	</br:brPanelGrid>	
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterOrgaoPagadorBean.voltarPesquisa}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" disabled="false" onclick="javascript: if (!confirm('Confirma Bloqueio?')) { desbloquearTela();  return false; } else { return validateForm(document.forms[1]); }" value="#{msgs.incOrgaoPagador2_label_botao_confirmar}" action="#{manterOrgaoPagadorBean.confirmarBloquear}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>