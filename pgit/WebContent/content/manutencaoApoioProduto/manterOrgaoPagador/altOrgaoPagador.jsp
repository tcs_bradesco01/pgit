<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="altOrgaoPagadorForm" name="altOrgaoPagadorForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup> 
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conOrgaoPagador_label_orgao_pagador}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio value="#{manterOrgaoPagadorBean.codEmpresa}" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<a4j:support event="onclick" action="#{manterOrgaoPagadorBean.habilitarCamposEmpresaAlteracao}" reRender="hiddenRadioEmpresa, empresaGrupoBradesco, agenciaResponsavel, empresaParceira, loja" />								
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>	
			<t:selectOneRadio value="#{manterOrgaoPagadorBean.orgaoPagadorAlteracao.cdTarifOrgaoPagador}" id="categoriaRadio" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">    
				<f:selectItems value="#{manterOrgaoPagadorBean.listaCategoria}"/>
	    	</t:selectOneRadio>
		</br:brPanelGroup>	
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detOrgaoPagador_label_codigo_orgao_pagador}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterOrgaoPagadorBean.orgaoPagadorAlteracao.cdOrgaoPagador}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrgaoPagador_label_tipo_unidade_organizacional}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
				    <br:brSelectOneMenu id="tipoUnidadeOrganizacional" value="#{manterOrgaoPagadorBean.orgaoPagadorAlteracao.cdTipoUnidadeOrganizacional}" converter="javax.faces.Integer">
						<f:selectItem itemValue="" itemLabel="#{msgs.conOrgaoPagador_label_combo_selecione}"/>
						<si:selectItems value="#{manterOrgaoPagadorBean.listaTipoUnidade}" var="tipoUnidade" itemLabel="#{tipoUnidade.dsTipoUnidade}" itemValue="#{tipoUnidade.cdTipoUnidade}" />
					</br:brSelectOneMenu>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detOrgaoPagador_label_empresa_grupo_bradesco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		    	<br:brPanelGroup>
					<t:radio for="sor" index="0" />			
				</br:brPanelGroup>			
			    <br:brPanelGroup>
				    <br:brSelectOneMenu id="empresaGrupoBradesco" value="#{manterOrgaoPagadorBean.orgaoPagadorAlteracao.cdPessoaJuridicaBradesco}" disabled="#{manterOrgaoPagadorBean.codEmpresa != 1}" converter="javax.faces.Long">
						<si:selectItems value="#{manterOrgaoPagadorBean.listaEmpresaGrupoBradesco}" var="empresaBradesco" itemLabel="#{empresaBradesco.codClub} - #{empresaBradesco.dsRazaoSocial}" itemValue="#{empresaBradesco.codClub}" />
					</br:brSelectOneMenu>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>
						
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detOrgaoPagador_label_agencia_responsavel}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterOrgaoPagadorBean.orgaoPagadorAlteracao.nrSeqUnidadeOrganizacionalBradesco}" size="30" maxlength="8" id="agenciaResponsavel" 
					disabled="#{manterOrgaoPagadorBean.codEmpresa != 1}" onkeypress="onlyNum()" onfocus="cleanClipboard()"/>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
					
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detOrgaoPagador_label_empresa_parceira}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		    	<br:brPanelGroup>
					<t:radio for="sor" index="1" />			
				</br:brPanelGroup>			
			    <br:brPanelGroup>
				    <br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterOrgaoPagadorBean.orgaoPagadorAlteracao.cdPessoaJuridicaParceira}" size="30" maxlength="10" id="empresaParceira" 
					disabled="#{manterOrgaoPagadorBean.codEmpresa != 2}" onkeypress="onlyNum()" onfocus="cleanClipboard()" />
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>
						
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detOrgaoPagador_label_loja}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
				    <br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterOrgaoPagadorBean.orgaoPagadorAlteracao.nrSeqUnidadeOrganizacionalParceira}" size="30" maxlength="8" id="loja" 
					disabled="#{manterOrgaoPagadorBean.codEmpresa != 2}" onkeypress="onlyNum()" onfocus="cleanClipboard()" />
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detOrgaoPagador_label_categoria}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>	
					<t:radio for="categoriaRadio" index="0" />			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>	
					<t:radio for="categoriaRadio" index="1" />			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		    <br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>	
					<t:radio for="categoriaRadio" index="2" />											
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>
		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterOrgaoPagadorBean.voltarPesquisa}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" onclick="javascript:if(!checaCampoObrigatorio(document.forms[1], 
				'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.detOrgaoPagador_label_codigo_orgao_pagador}',
				 '#{msgs.conOrgaoPagador_label_tipo_unidade_organizacional}', '#{msgs.detOrgaoPagador_label_empresa_grupo_bradesco}', 
				 '#{msgs.detOrgaoPagador_label_agencia_responsavel}', '#{msgs.detOrgaoPagador_label_empresa_parceira}', 
				 '#{msgs.detOrgaoPagador_label_loja}', '#{msgs.detOrgaoPagador_label_categoria}',
				 '#{msgs.detOrgaoPagador_label_radio_obrigatorio}')) { desbloquearTela(); return false; }" styleClass="bto1" disabled="false" value="#{msgs.altOrgaoPagador_label_botao_avancar}" action="#{manterOrgaoPagadorBean.avancarAlterar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		  

</br:brPanelGrid>
</brArq:form>
