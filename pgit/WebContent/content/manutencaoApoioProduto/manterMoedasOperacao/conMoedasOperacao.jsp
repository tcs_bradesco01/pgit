<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="pesqManterMoedasOperacaoPesquisar" name="pesqManterMoedasOperacaoPesquisar" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conMoedasOperacao_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conMoedasOperacao_subtitulo_servico}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conMoedasOperacao_servico}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conMoedasOperacao_servico_relacionado}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
		   	<br:brSelectOneMenu id="tipoServico" value="#{manterMoedasOperacaoBean.filtroTipoServico}" disabled="#{manterMoedasOperacaoBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.conMoedasOperacao_label_combo_selecione}"/>
				<f:selectItems value="#{manterMoedasOperacaoBean.listaTipoServico}"/>
				<a4j:support event="onchange" reRender="panelServico" action="#{manterMoedasOperacaoBean.preencheListaModalidade}" />												
			</br:brSelectOneMenu>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px;"> </br:brPanelGroup> 
		<br:brPanelGroup id="panelServico">
			<br:brSelectOneMenu id="modalidadeServico" value="#{manterMoedasOperacaoBean.filtroModalidadeServico}" disabled="#{manterMoedasOperacaoBean.filtroTipoServico == null || manterMoedasOperacaoBean.filtroTipoServico == 0 || manterMoedasOperacaoBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.conMoedasOperacao_label_combo_selecione}"/>
				<f:selectItems value="#{manterMoedasOperacaoBean.listaModalidadeServico}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conMoedasOperacao_codigo_moeda_indice}:"/>
		</br:brPanelGroup>			
		<br:brPanelGroup>		
			<br:brSelectOneMenu id="c" value="#{manterMoedasOperacaoBean.filtroCodigoMoedaIndice}" disabled="#{manterMoedasOperacaoBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.conMoedasOperacao_label_combo_selecione}"/>
				<f:selectItems value="#{manterMoedasOperacaoBean.listaCodigoMoedaIndice}"/>
			</br:brSelectOneMenu>		
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conMoedasOperacao_btn_limpar_campos}" action="#{manterMoedasOperacaoBean.limparCampos}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.conMoedasOperacao_btn_consultar}" action="#{manterMoedasOperacaoBean.consultar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
		
			<app:scrollableDataTable id="dataTable" value="#{manterMoedasOperacaoBean.listaGrid}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%" height="170" >
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{manterMoedasOperacaoBean.itemSelecionadoLista}">
						<f:selectItems value="#{manterMoedasOperacaoBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			    	<t:radio for="sor" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="220px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conMoedasOperacao_grid_servico}" style="text-align:center;width:220" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoServico}"/>
				</app:scrollableColumn>			
				
				<app:scrollableColumn styleClass="colTabLeft" width="220px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conMoedasOperacao_grid_servico_relacionado}" style="text-align:center;width:220" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsModalidadeServico}"/>
				</app:scrollableColumn>			
				
				<app:scrollableColumn styleClass="colTabRight" width="100px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conMoedasOperacao_grid_moeda_indice}" style="text-align:center;width:100" />
				    </f:facet>
				    <br:brOutputText value="#{result.codigoMoedaIndice}"/>
				</app:scrollableColumn>			
				
				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conMoedasOperacao_grid_descricao}" style="text-align:center;width:180" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsEconomicoMoeda}"/>
				</app:scrollableColumn>			
							
			</app:scrollableDataTable>
			
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
 <br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup rendered="#{!empty manterMoedasOperacaoBean.listaGrid}">
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterMoedasOperacaoBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet> 
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>				
		
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterMoedasOperacaoBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conMoedasOperacao_btn_detalhar}" action="#{manterMoedasOperacaoBean.avancarDetalhar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" styleClass="bto1" style="margin-right:5px" value="#{msgs.conMoedasOperacao_btn_incluir}" action="#{manterMoedasOperacaoBean.avancarIncluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty manterMoedasOperacaoBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.conMoedasOperacao_btn_excluir}" action="#{manterMoedasOperacaoBean.avancarExcluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
	
	<br:brPanelGrid columns="1" style="margin-top:140px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
</br:brPanelGrid>
<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
