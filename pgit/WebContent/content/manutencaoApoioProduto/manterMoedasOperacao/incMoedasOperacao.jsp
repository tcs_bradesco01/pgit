<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="pesqManterMoedasOperacaoIncluir" name="pesqManterMoedasOperacaoIncluir" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conMoedasOperacao_subtitulo_servico}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incMoedasOperacao_servico}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoServico" value="#{manterMoedasOperacaoBean.tipoServico}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conMoedasOperacao_label_combo_selecione}"/>
				<f:selectItems value="#{manterMoedasOperacaoBean.listaTipoServico}"/>
				<a4j:support event="onchange" reRender="panelServico" action="#{manterMoedasOperacaoBean.carregaModalidadeIncluir}" />												
				<brArq:commonsValidator type="required" arg="#{msgs.incMoedasOperacao_servico}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incMoedasOperacao_servico_relacionado}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup id="panelServico">
			<br:brSelectOneMenu id="modalidadeServico" value="#{manterMoedasOperacaoBean.modalidadeServico}" disabled="#{manterMoedasOperacaoBean.tipoServico == null || manterMoedasOperacaoBean.tipoServico == 0}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conMoedasOperacao_label_combo_selecione}"/>
				<f:selectItems value="#{manterMoedasOperacaoBean.listaModalidadeServicoIncluir}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.incMoedasOperacao_servico_relacionado}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incMoedasOperacao_codigo_moeda_indice}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="codigoMoedaIndice" value="#{manterMoedasOperacaoBean.codigoMoedaIndice}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conMoedasOperacao_label_combo_selecione}"/>
				<f:selectItems value="#{manterMoedasOperacaoBean.listaCodigoMoedaIndice}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.incMoedasOperacao_codigo_moeda_indice}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>		
    
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.incMoedasOperacao_btn_voltar}" action="#{manterMoedasOperacaoBean.voltarPesquisar}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.incMoedasOperacao_btn_avancar}" action="#{manterMoedasOperacaoBean.avancarIncluirConfirmar}" >				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:170px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
</br:brPanelGrid>
<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>

