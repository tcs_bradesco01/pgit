<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="assocLoteOperacaoProdServ" name="assocLoteOperacaoProdServ" >
<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina" >
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_servicos}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboServicos" value="#{assocLoteLayArqProdServBean.filtroServicos}" disabled="#{assocLoteLayArqProdServBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_selecione}"/>	
				<f:selectItems value="#{assocLoteLayArqProdServBean.listaTipoServico}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_tipo_lote}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboTipoLote" value="#{assocLoteLayArqProdServBean.filtroTipoLote}" disabled="#{assocLoteLayArqProdServBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_selecione}"/>	
				<f:selectItems value="#{assocLoteLayArqProdServBean.listaTipoLote}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_tipo_layout_arquivo}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboTipoLayoutArquivo" value="#{assocLoteLayArqProdServBean.filtroTipoLayoutArquivo}" disabled="#{assocLoteLayArqProdServBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_selecione}"/>	
				<f:selectItems value="#{assocLoteLayArqProdServBean.listaTipoLayoutArquivo}"/>						
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>										

	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_btn_limpar_campos}" action="#{assocLoteLayArqProdServBean.limparCampos}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_btn_consultar}" action="#{assocLoteLayArqProdServBean.carregaLista}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto;width:750px" >
		
			<app:scrollableDataTable id="dataTable" value="#{assocLoteLayArqProdServBean.listaGrid}" var="result" 
			rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" height="170">

			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		

				<t:selectOneRadio  id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
					value="#{assocLoteLayArqProdServBean.itemSelecionadoLista}">
					<f:selectItems value="#{assocLoteLayArqProdServBean.listaControle}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sor" index="#{parametroKey}" />
			</app:scrollableColumn>

			<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_grid_servico}" style="text-align:center;width:250px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoServico}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="160px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_grid_tipo_lote}" style="text-align:center;width:160px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoLote}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_grid_tipo_layout_arquivo}" style="text-align:center;width:250px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoLayoutArquivo}"/>
			 </app:scrollableColumn>
			</app:scrollableDataTable>				 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
 <br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{assocLoteLayArqProdServBean.pesquisar}" >
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnDetalhar" disabled="#{empty assocLoteLayArqProdServBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_btn_detalhar}" action="#{assocLoteLayArqProdServBean.avancarDetalhar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir"  disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_btn_incluir}" action="#{assocLoteLayArqProdServBean.avancarIncluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir"  disabled="#{empty assocLoteLayArqProdServBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_btn_excluir}" action="#{assocLoteLayArqProdServBean.avancarExcluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>
	
	<br:brPanelGrid columns="1" style="margin-top:150px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>
