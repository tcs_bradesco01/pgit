<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="assocLoteOperacaoProdServIncluir" name="assocLoteOperacaoProdServIncluir" >
<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina" >
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_incAssociacaoLoteLayoutArquivoProdutoServico_inclusao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_servico}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="filtroIncluirServicos" value="#{assocLoteLayArqProdServBean.codServicos}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_selecione}"/>	
				<f:selectItems value="#{assocLoteLayArqProdServBean.listaTipoServico}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_servico}" server="false" client="true"/>							
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_tipo_lote}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="filtroIncluirTipoLote" value="#{assocLoteLayArqProdServBean.codTipoLote}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_selecione}"/>	
				<f:selectItems value="#{assocLoteLayArqProdServBean.listaTipoLote}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_tipo_lote}" server="false" client="true"/>							
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_tipo_layout_arquivo}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="filtroIncluirTipoLayoutArquivo" value="#{assocLoteLayArqProdServBean.codTipoLayoutArquivo}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_selecione}"/>	
				<f:selectItems value="#{assocLoteLayArqProdServBean.listaTipoLayoutArquivo}"/>		
				<brArq:commonsValidator type="required" arg="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_tipo_layout_arquivo}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_btn_voltar}" action="#{assocLoteLayArqProdServBean.voltarPesquisar}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton  id="btnConfirmar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.incAssociacaoLoteLayoutArquivoProdutoServico_btn_avancar}" action="#{assocLoteLayArqProdServBean.avancarConfirmarIncluir}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>										

	
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
			
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>
