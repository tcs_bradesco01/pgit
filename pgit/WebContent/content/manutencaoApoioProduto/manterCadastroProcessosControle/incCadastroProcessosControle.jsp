<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="incluirManterCadastroControlesProcessoForm" name="incluirManterCadastroControlesProcessoForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"  cellspacing="0">
    <br:brPanelGrid styleClass="mainPanel" style="margin-top:9px" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detCadastroProcessosControle_title_processo}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conCadastroProcessosControle_label_centro_custo}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<a4j:region renderRegionOnly="true">
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
	    <br:brPanelGroup>
			<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroProcessosControleBean.centroCustoLupa}" size="5" maxlength="4" id="centroCustoFiltro"/>
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brCommandButton id="btnCentroCusto" style="cursor: hand;" image="/images/lupa.gif" value=""  action="#{manterCadastroProcessosControleBean.obterCentroCusto}" onclick="javascript: desbloquearTela(); return checaCentroCusto(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_centrocusto}');" >
						<brArq:submitCheckClient/>
			</br:brCommandButton>
			<f:verbatim>&nbsp;</f:verbatim>
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brSelectOneMenu id="centroCusto" value="#{manterCadastroProcessosControleBean.processoMassivoInclusao.cdSistema}" disabled="#{empty manterCadastroProcessosControleBean.listaCentroCustoLupa}">
				<f:selectItem itemValue="" itemLabel="#{msgs.conCadastroProcessosControle_label_combo_selecione}"/>
				<si:selectItems value="#{manterCadastroProcessosControleBean.listaCentroCustoLupa}" var="centroCusto" itemLabel="#{centroCusto.codigoDescricao}" itemValue="#{centroCusto.cdCentroCusto}" />
				<brArq:commonsValidator type="required" arg="#{msgs.conCadastroProcessosControle_label_centro_custo}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	</a4j:region>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incCadastroProcessosControle_label_tipo_processo}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<br:brSelectOneMenu id="tipoProcesso" value="#{manterCadastroProcessosControleBean.processoMassivoInclusao.cdTipoProcessoSistema}" converter="javax.faces.Integer">
				<f:selectItem itemValue="" itemLabel="#{msgs.conCadastroProcessosControle_label_combo_selecione}"/>
				<si:selectItems value="#{manterCadastroProcessosControleBean.listaTipoProcesso}" var="tipoProcesso" itemLabel="#{tipoProcesso.dsTipoProcesso}" itemValue="#{tipoProcesso.cdTipoProcesso}" />
			</br:brSelectOneMenu>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detCadastroProcessosControle_label_descricao_tipo_servico}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
			<br:brInputTextarea rows="4" cols="100" value="#{manterCadastroProcessosControleBean.processoMassivoInclusao.dsProcessoSistema}" onkeydown="javascript:maxLength(this,200);" onkeyup="javascript:maxLength(this,200);" id="descricao" />
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conCadastroProcessosControle_label_net}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
			<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoInclusao.cdNetProcessamentoPgto}" size="30" maxlength="8" id="net" />
		</br:brPanelGroup>					
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conCadastroProcessosControle_label_job}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
			<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoInclusao.cdJobProcessamentoPgto}" size="30" maxlength="8" id="job" />
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conCadastroProcessosControle_label_periodicidade}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<br:brSelectOneMenu id="periodicidade" value="#{manterCadastroProcessosControleBean.processoMassivoInclusao.cdPeriodicidade}" converter="javax.faces.Integer">
				<f:selectItem itemValue="" itemLabel="#{msgs.conCadastroProcessosControle_label_combo_selecione}"/>
				<si:selectItems value="#{manterCadastroProcessosControleBean.listaPeriodicidadeExecucao}" var="periodicidade" itemLabel="#{periodicidade.dsPeriodicidade}" itemValue="#{periodicidade.cdPeriodicidade}" />
			</br:brSelectOneMenu>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterCadastroProcessosControleBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" disabled="false"  style="margin-right:5px" value="#{msgs.incCadastroProcessosControle_label_botao_limpar}"  action="#{manterCadastroProcessosControleBean.incluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>

			<br:brCommandButton id="btnAvancar" onclick="javascript:if(!checaCampoObrigatorioInclusao(document.forms[1], 
				'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.conCadastroProcessosControle_label_centro_custo}',
				 '#{msgs.incCadastroProcessosControle_label_tipo_processo}', '#{msgs.detCadastroProcessosControle_label_descricao_tipo_servico}', 
				 '#{msgs.conCadastroProcessosControle_label_net}', '#{msgs.conCadastroProcessosControle_label_job}', 
				 '#{msgs.conCadastroProcessosControle_label_periodicidade}')) { desbloquearTela(); return false; }" styleClass="bto1" disabled="false"  value="#{msgs.incCadastroProcessosControle_label_botao_avancar}"  action="#{manterCadastroProcessosControleBean.avancarIncluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		  
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
