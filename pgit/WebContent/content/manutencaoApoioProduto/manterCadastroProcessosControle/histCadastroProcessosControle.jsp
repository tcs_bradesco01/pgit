<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="historicoManterCadastroControlesProcessoForm" name="historicoManterCadastroControlesProcessoForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_centro_custo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdSistema} - #{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsCodigoSistema}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incCadastroProcessosControle_label_tipo_processo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdProcessoSistema} - #{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsTipoProcessoSistema}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.histCadastroProcessosControle_label_periodo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				<br:brPanelGroup>
					<br:brPanelGroup rendered="#{!manterCadastroProcessosControleBean.btoAcionadoHistorico}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioDe');" id="calendarioDe" value="#{manterCadastroProcessosControleBean.historicoBloqueioFiltro.dtDe}" >
							<brArq:commonsValidator type="required" arg="#{msgs.PGIC0170_check_periodo_inicial}" server="false" client="true"/>
						</app:calendar>
					</br:brPanelGroup>
					<br:brPanelGroup rendered="#{manterCadastroProcessosControleBean.btoAcionadoHistorico}">
						<app:calendar id="calendarioDeDes" value="#{manterCadastroProcessosControleBean.historicoBloqueioFiltro.dtDe}" disabled="true">
						</app:calendar>
					</br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_ate}"/>
					<br:brPanelGroup rendered="#{!manterCadastroProcessosControleBean.btoAcionadoHistorico}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioAte');"  id="calendarioAte" value="#{manterCadastroProcessosControleBean.historicoBloqueioFiltro.dtAte}" >
							<brArq:commonsValidator type="required" arg="#{msgs.PGIC0170_check_periodo_final}" server="false" client="true"/>
						</app:calendar>
					</br:brPanelGroup>
					<br:brPanelGroup rendered="#{manterCadastroProcessosControleBean.btoAcionadoHistorico}">
						<app:calendar id="calendarioAteDes" value="#{manterCadastroProcessosControleBean.historicoBloqueioFiltro.dtAte}" disabled="true">
						</app:calendar>
					</br:brPanelGroup>
				</br:brPanelGroup>
 		    </br:brPanelGrid>
		</a4j:outputPanel>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="Limpar Campos" action="#{manterCadastroProcessosControleBean.limparHistorico}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar"  onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.conCadastroProcessosControle_label_botao_consultar}" action="#{manterCadastroProcessosControleBean.consultarHistorico}" onclick="javascript:desbloquearTela(); return validarData(document.forms[1],'#{msgs.label_periodo_invalido}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">	
		<t:selectOneRadio id="rdoHistorico" styleClass="HtmlSelectOneRadioBradesco" 
				layout="spread" forceId="true" forceIdIndex="false" 
				value="#{manterCadastroProcessosControleBean.itemSelecionadoHistorico}">
				<f:selectItems value="#{manterCadastroProcessosControleBean.selectItemLista}" />
				<a4j:support event="onclick" reRender="panelBotoes" />
			</t:selectOneRadio>
		<br:brPanelGroup styleClass="OverflowScrollableDataTable" >	
			<app:scrollableDataTable id="dataTable" value="#{manterCadastroProcessosControleBean.listaGridHistorico}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>		
					<t:radio for=":historicoManterCadastroControlesProcessoForm:rdoHistorico" index="#{parametroKey}" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_data_hora_manutencao}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.hrBloqueioProcessoSistema}" converter="timestampPdcConverter" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_usuario_manutencao}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsUsuarioInclusao}" />
				</app:scrollableColumn>			

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_tipo_manutencao}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsIndicadorTipoManutencao}" />
				</app:scrollableColumn>			

			</app:scrollableDataTable>
		</br:brPanelGroup>
		
	</br:brPanelGrid>		
	

 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterCadastroProcessosControleBean.pesquisarHistorico}" rendered="#{!empty manterCadastroProcessosControleBean.listaGridHistorico}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<f:verbatim><hr class="lin"> </f:verbatim>

	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterCadastroProcessosControleBean.voltar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
				<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterCadastroProcessosControleBean.itemSelecionadoHistorico}" value="#{msgs.conCadastroProcessosControle_path_detalhar}"  action="#{manterCadastroProcessosControleBean.detalharHistorico}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>		
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</a4j:outputPanel>	

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
