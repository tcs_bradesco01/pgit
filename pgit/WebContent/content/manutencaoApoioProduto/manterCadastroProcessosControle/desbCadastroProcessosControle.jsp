<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>


<brArq:form id="bloquearManterCadastroControlesProcessoForm" name="bloquearManterCadastroControlesProcessoForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.desbCadastroProcessosControle_label_dados_processo}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_centro_custo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdSistema} - #{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsCodigoSistema}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incCadastroProcessosControle_label_tipo_processo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdProcessoSistema} - #{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsTipoProcessoSistema}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detCadastroProcessosControle_label_descricao_tipo_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsProcessoSistema}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_net}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdNetProcessamentoPgto}"  />
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_job}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdJobProcessamentoPgto}"  />
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_periodicidade}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsPeriodicidade}"  />
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.desbCadastroProcessosControle_label_bloqueio_desbloqueio}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.desbCadastroProcessosControle_label_situacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<t:selectOneRadio value="#{manterCadastroProcessosControleBean.processoMassivoBloqueio.cdSituacaoProcessoSistema}" id="situacao" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItems value="#{manterCadastroProcessosControleBean.listaSituacao}"/>
			</t:selectOneRadio>
			
			<t:radio for="situacao" index="0" />
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<t:radio for="situacao" index="1" />
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.desbCadastroProcessosControle_label_motivo_bloqueio}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
			<br:brInputTextarea rows="4" cols="100" value="#{manterCadastroProcessosControleBean.processoMassivoBloqueio.dsBloqueioProcessoSistema}" onkeydown="javascript:maxLength(this,200);" onkeyup="javascript:maxLength(this,200);" id="motivo" />
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterCadastroProcessosControleBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" disabled="false"  style="margin-right:5px" value="#{msgs.incCadastroProcessosControle_label_botao_limpar}"  action="#{manterCadastroProcessosControleBean.limparBloqueio}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConfirmar" onclick="javascript: if(!checaCampoObrigatorio(document.forms[1], 
				'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.desbCadastroProcessosControle_label_situacao}',
				 '#{msgs.desbCadastroProcessosControle_label_motivo_bloqueio}')) { desbloquearTela(); return false; }; if (!confirm('Confirma Bloqueio/Desbloqueio?')) { desbloquearTela();  return false; }" styleClass="bto1" disabled="false"  value="#{msgs.incCadastroProcessosControle2_label_botao_confirmar}"  action="#{manterCadastroProcessosControleBean.confirmarBloquear}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		  
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
