<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="pesqManterCadastroControlesProcessoForm" name="pesqManterCadastroControlesProcessoForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conCadastroProcessosControle_label_centro_custo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		   <a4j:region renderRegionOnly="true">
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroProcessosControleBean.centroCustoLupaFiltro}" disabled="#{manterCadastroProcessosControleBean.btoAcionado}" size="5" maxlength="4" id="centroCustoFiltro"/>
						<f:verbatim>&nbsp;</f:verbatim>
						<br:brCommandButton id="btnCentroCusto" style="cursor: hand;" image="/images/lupa.gif" value=""  action="#{manterCadastroProcessosControleBean.obterCentroCustoFiltro}" onclick="javascript: desbloquearTela(); return checaCentroCusto(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conCadastroProcessosControle_label_centro_custo}');" >
							<brArq:submitCheckClient/>
						</br:brCommandButton>
						<f:verbatim>&nbsp;</f:verbatim>
						<f:verbatim>&nbsp;</f:verbatim>
						<br:brSelectOneMenu id="centroCusto" value="#{manterCadastroProcessosControleBean.processoMassivoFiltro.cdSistema}" disabled="#{empty manterCadastroProcessosControleBean.listaCentroCusto || manterCadastroProcessosControleBean.btoAcionado}">
							<f:selectItem itemValue="" itemLabel="#{msgs.conCadastroProcessosControle_label_combo_selecione}"/>
							<si:selectItems value="#{manterCadastroProcessosControleBean.listaCentroCusto}" var="centroCusto" itemLabel="#{centroCusto.codigoDescricao}" itemValue="#{centroCusto.cdCentroCusto}" />							
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</a4j:region>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_processo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="codigoTipoProcesso" value="#{manterCadastroProcessosControleBean.processoMassivoFiltro.cdTipoProcessoSistema}" disabled="#{manterCadastroProcessosControleBean.btoAcionado}" converter="javax.faces.Integer">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conCadastroProcessosControle_label_combo_selecione}"/>
						<si:selectItems value="#{manterCadastroProcessosControleBean.listaTipoProcesso}" var="tipoProcesso" itemLabel="#{tipoProcesso.dsTipoProcesso}" itemValue="#{tipoProcesso.cdTipoProcesso}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conCadastroProcessosControle_label_periodicidade_execucao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid id="panelPeriodicidade" columns="1" cellpadding="0" cellspacing="0">					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="periodicidadeExecucao" value="#{manterCadastroProcessosControleBean.processoMassivoFiltro.cdPeriodicidade}" disabled="#{manterCadastroProcessosControleBean.btoAcionado}" converter="javax.faces.Integer">
						<f:selectItem itemValue="0" itemLabel="#{msgs.PGIC0010_label_combo_selecione}"/>
						<si:selectItems value="#{manterCadastroProcessosControleBean.listaPeriodicidadeExecucao}" var="periodicidade" itemLabel="#{periodicidade.dsPeriodicidade}" itemValue="#{periodicidade.cdPeriodicidade}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>		
		
	</br:brPanelGrid>	
		
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>


	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="Limpar Campos" action="#{manterCadastroProcessosControleBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conCadastroProcessosControle_label_botao_consultar}" 
			action="#{manterCadastroProcessosControleBean.consultarProcessos}"  
			onclick="javascript:desbloquearTela(); return validarCamposConsultar(document.forms[1],'#{msgs.label_ocampo}','#{msgs.label_necessario}','#{msgs.conCadastroProcessosControle_label_centro_custo}')" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	
		
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="OverflowScrollableDataTable" >	
			<t:selectOneRadio id="rdoProcessoMassivo" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{manterCadastroProcessosControleBean.itemSelecionadoConsulta}"
						converter="processoMassivoConveter">
						<si:selectItems value="#{manterCadastroProcessosControleBean.listaGridPesquisa}" var="processo" itemLabel="" itemValue="#{processo}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			<app:scrollableDataTable id="dataTable" value="#{manterCadastroProcessosControleBean.listaGridPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
			    	<t:radio for=":pesqManterCadastroControlesProcessoForm:rdoProcessoMassivo" index="#{parametroKey}" />
				</app:scrollableColumn>
		
				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conCadastroProcessosControle_label_centro_custo}" style="width:180; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsCodigoSistema}"/>
				</app:scrollableColumn>			

				<app:scrollableColumn styleClass="colTabRight" width="140px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conCadastroProcessosControle_label_codigo_tipo_processo}" style="width:140; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdProcessoSistema}"/>
				</app:scrollableColumn>			

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conCadastroProcessosControle_label_nome_processo}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoProcessoSistema}"/>
				</app:scrollableColumn>			

				<app:scrollableColumn styleClass="colTabLeft" width="100px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conCadastroProcessosControle_label_net}" style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdNetProcessamentoPgto}"/>
				</app:scrollableColumn>			

				<app:scrollableColumn styleClass="colTabLeft" width="100px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conCadastroProcessosControle_label_job}" style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdJobProcessamentoPgto}"/>
				</app:scrollableColumn>			

				<app:scrollableColumn styleClass="colTabLeft" width="100px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conCadastroProcessosControle_label_situacao}" style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsSistemaProcessoSistema}"/>
				</app:scrollableColumn>			

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conCadastroProcessosControle_label_periodicidade}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsPeriodicidade}"/>
				</app:scrollableColumn>			
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>		

 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterCadastroProcessosControleBean.pesquisar}" rendered="#{!empty manterCadastroProcessosControleBean.listaGridPesquisa}">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup >
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterCadastroProcessosControleBean.itemSelecionadoConsulta}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conCadastroProcessosControle_path_detalhar}"  action="#{manterCadastroProcessosControleBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" styleClass="bto1" disabled="false"  style="margin-right:5px" value="#{msgs.conCadastroProcessosControle_path_incluir}"  action="#{manterCadastroProcessosControleBean.incluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" disabled="#{empty manterCadastroProcessosControleBean.itemSelecionadoConsulta}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conCadastroProcessosControle_path_alterar}"  action="#{manterCadastroProcessosControleBean.alterar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty manterCadastroProcessosControleBean.itemSelecionadoConsulta}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conCadastroProcessosControle_path_excluir}"  action="#{manterCadastroProcessosControleBean.excluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnBloquear" disabled="#{empty manterCadastroProcessosControleBean.itemSelecionadoConsulta}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conCadastroProcessosControle_path_bloquear}" action="#{manterCadastroProcessosControleBean.bloquear}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnHistorico" disabled="#{empty manterCadastroProcessosControleBean.itemSelecionadoConsulta}" styleClass="bto1" value="#{msgs.conCadastroProcessosControle_path_historico}" action="#{manterCadastroProcessosControleBean.historico}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>	

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />

</br:brPanelGrid>
</brArq:form>
