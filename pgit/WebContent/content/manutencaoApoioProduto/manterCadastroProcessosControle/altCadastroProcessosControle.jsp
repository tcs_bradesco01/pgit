<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="alterarManterCadastroControlesProcessoForm" name="alterarManterCadastroControlesProcessoForm" >


<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"  cellspacing="0">
    <br:brPanelGrid styleClass="mainPanel" style="margin-top:9px" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detCadastroProcessosControle_title_processo}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_centro_custo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoAlteracao.cdSistema} - #{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsCodigoSistema}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incCadastroProcessosControle_label_tipo_processo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoAlteracao.cdTipoProcessoSistema} - #{manterCadastroProcessosControleBean.processoMassivoAlteracao.dsTipoProcessoSistema}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detCadastroProcessosControle_label_descricao_tipo_servico}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
			<br:brInputTextarea rows="4" cols="210" value="#{manterCadastroProcessosControleBean.processoMassivoAlteracao.dsProcessoSistema}" onkeydown="javascript:maxLength(this,200);" onkeyup="javascript:maxLength(this,200);" id="descricao">
				<brArq:commonsValidator type="required" arg="#{msgs.detCadastroProcessosControle_label_descricao_tipo_servico}" server="false" client="true"/>
		    </br:brInputTextarea>	
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conCadastroProcessosControle_label_net}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
			<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoAlteracao.cdNetProcessamentoPgto}" size="30" maxlength="8" id="net" >
				<brArq:commonsValidator type="required" arg="#{msgs.conCadastroProcessosControle_label_net}" server="false" client="true"/>
		    </br:brInputText>	
		</br:brPanelGroup>					
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conCadastroProcessosControle_label_job}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
			<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoAlteracao.cdJobProcessamentoPgto}" size="30" maxlength="8" id="job" >
					<brArq:commonsValidator type="required" arg="#{msgs.conCadastroProcessosControle_label_job}" server="false" client="true"/>
		    </br:brInputText>	
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conCadastroProcessosControle_label_periodicidade}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<br:brSelectOneMenu id="periodicidade" value="#{manterCadastroProcessosControleBean.processoMassivoAlteracao.cdPeriodicidade}" converter="javax.faces.Integer">
				<f:selectItem itemValue="" itemLabel="#{msgs.conCadastroProcessosControle_label_combo_selecione}"/>
				<si:selectItems value="#{manterCadastroProcessosControleBean.listaPeriodicidadeExecucao}" var="periodicidade" itemLabel="#{periodicidade.dsPeriodicidade}" itemValue="#{periodicidade.cdPeriodicidade}" />
				<brArq:commonsValidator type="required" arg="#{msgs.conCadastroProcessosControle_label_periodicidade}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterCadastroProcessosControleBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" disabled="false"  style="margin-right:5px" value="#{msgs.incCadastroProcessosControle_label_botao_limpar}"  action="#{manterCadastroProcessosControleBean.limparAlterar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<br:brCommandButton id="btnAvancar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" disabled="false"  value="#{msgs.incCadastroProcessosControle_label_botao_avancar}"  action="#{manterCadastroProcessosControleBean.avancarAlterar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		  
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
