<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>


<brArq:form id="detalharManterCadastroControlesProcessoForm" name="detalharManterCadastroControlesProcessoForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detCadastroProcessosControle_title_processo}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_centro_custo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdSistema} - #{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsCodigoSistema}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incCadastroProcessosControle_label_tipo_processo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdProcessoSistema} - #{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsTipoProcessoSistema}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detCadastroProcessosControle_label_descricao_tipo_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsProcessoSistema}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_net}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdNetProcessamentoPgto}"  />
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_job}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdJobProcessamentoPgto}"  />
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_periodicidade}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsPeriodicidade}"  />
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conCadastroProcessosControle_label_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.dsSistemaProcessoSistema}"  />
		</br:brPanelGroup>			
	</br:brPanelGrid>



		<f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid columns="2" >
			
			<br:brPanelGroup style="text-align:left;" >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detCadastroProcessosControle_data_hora_inclusao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.hrInclusaoRegistro}"  />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detCadastroProcessosControle_usuario}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdAutenSegregacaoInclusao}"  />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" >
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detCadastroProcessosControle_tipo_canal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.canalInclusao}"  />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detCadastroProcessosControle_complemento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.nmOperacaoFluxoInclusao}"  />
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"></f:verbatim>
		<br:brPanelGrid columns="2" > 
			
			<br:brPanelGroup style="text-align:left;" >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detCadastroProcessosControle_data_hora_manutencao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.hrInclusaoManutencao}"  />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detCadastroProcessosControle_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.cdAutenSegregacaoManutencao}"  />
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" style="margin-top: 6">	
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detCadastroProcessosControle_tipo_canal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.canalManutencao}"  />
			</br:brPanelGroup>
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detCadastroProcessosControle_complemento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroProcessosControleBean.processoMassivoDetalhe.nmOperacaoFluxoManutencao}"  />
			</br:brPanelGroup>
			
		</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"   cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="width:750px">
			<br:brCommandButton styleClass="bto1" value="#{msgs.label_botao_voltar}" action="#{manterCadastroProcessosControleBean.voltar}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>
</brArq:form>
