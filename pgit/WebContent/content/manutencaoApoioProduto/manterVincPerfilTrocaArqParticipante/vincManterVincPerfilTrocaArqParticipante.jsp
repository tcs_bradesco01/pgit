<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="vincManterVincPerfilTrocaArqParticipante" name="vincManterVincPerfilTrocaArqParticipante" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

		<jsp:include page="/content/manutencaoApoioProduto/manterVincPerfilTrocaArqParticipante/cabecPadraoVincPerfilTrocaArq.jsp" flush="false" />

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.convincperfiltrocaarqparticipante_title_argumentosPesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	

		<a4j:region id="regionFiltro2">
			<t:selectOneRadio id="rdoFiltro" value="#{manterVincPerfilTrocaArqPartBean.itemFiltro}" disabled="#{manterVincPerfilTrocaArqPartBean.disableArgumentosConsulta}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >   
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<a4j:support event="onclick" reRender="panelPerfil, panelParticipante, panelParticipanteDados" action="#{manterVincPerfilTrocaArqPartBean.limparCampos}" ajaxSingle="true" />
			</t:selectOneRadio>
		</a4j:region>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<t:radio for="rdoFiltro" index="0" />			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_perfil}:"/>		
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<a4j:outputPanel id="panelPerfil" ajaxRendered="true">	

			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
				</br:brPanelGroup>	
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_codigo_perfil}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
	
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>		
							<br:brInputText id="txtCodigoPerfil" disabled="#{manterVincPerfilTrocaArqPartBean.itemFiltro != '0'|| manterVincPerfilTrocaArqPartBean.disableArgumentosConsulta}" value="#{manterVincPerfilTrocaArqPartBean.codigoPerfil}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" size="20" maxlength="9" onkeypress="onlyNum()" converter="javax.faces.Long" styleClass="HtmlInputTextBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>	
				</br:brPanelGroup>	
	
				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
				</br:brPanelGroup>	
	
				<br:brPanelGroup>
	
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_tipo_layout_arquivo}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
	
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
			 			</br:brPanelGroup>
					</br:brPanelGrid>
	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>			
							<br:brSelectOneMenu id="cboTipoLayout" converter="inteiroConverter" value="#{manterVincPerfilTrocaArqPartBean.tipoLayout}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{manterVincPerfilTrocaArqPartBean.itemFiltro != '0'|| manterVincPerfilTrocaArqPartBean.disableArgumentosConsulta}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems  value="#{manterVincPerfilTrocaArqPartBean.listaTipoLayout}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>	
				</br:brPanelGroup>
			</br:brPanelGrid>

		</a4j:outputPanel>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="100%">
			<br:brPanelGroup>
		      <t:radio for="rdoFiltro" index="1" />			
			  <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_participante}:"/>		
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<a4j:outputPanel id="panelParticipanteDados" ajaxRendered="true" >	
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="text-align:left;">
				<br:brPanelGroup style="margin-left:20px">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_cpfcnpj_participante}:"/>			
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterVincPerfilTrocaArqPartBean.cpfCnpjParticipante}"/>
				</br:brPanelGroup>	

				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
				</br:brPanelGroup>	

				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_nomerazaosocial_participante}:"/>			
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterVincPerfilTrocaArqPartBean.nomeRazaoSocialParticipante}" />
				</br:brPanelGroup>			
			</br:brPanelGrid>
		</a4j:outputPanel>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<a4j:outputPanel id="panelParticipante" ajaxRendered="true" >	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="100%" style="text-align:right" >
				<br:brPanelGroup>
					<br:brCommandButton id="btnPesquisar" styleClass="bto1" value="#{msgs.convincperfiltrocaarqparticipante_label_botao_pesquisar}" disabled="#{manterVincPerfilTrocaArqPartBean.itemFiltro != '1' || manterVincPerfilTrocaArqPartBean.disableArgumentosConsulta}" action="#{manterVincPerfilTrocaArqPartBean.flagTelaConsultar}" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>			
		</a4j:outputPanel>

		<f:verbatim><hr class="lin"> </f:verbatim>

		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{manterVincPerfilTrocaArqPartBean.limparArgumentosPesquisar}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar"  styleClass="bto1" value="#{msgs.convincperfiltrocaarqparticipante_label_botao_consultar}" 
					onclick="javascript: desbloquearTela(); return validarConsulta(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{manterVincPerfilTrocaArqPartBean.cpfCnpjParticipante}','#{msgs.convincperfiltrocaarqparticipante_label_codigo_perfil}', 
					'#{msgs.convincperfiltrocaarqparticipante_label_tipo_layout_arquivo}','#{msgs.convincperfiltrocaarqparticipante_label_participante}','#{msgs.convincperfiltrocaarqparticipante_label_msg}');"
					action="#{manterVincPerfilTrocaArqPartBean.carregaLista}" disabled="#{manterVincPerfilTrocaArqPartBean.identificacaoClienteBean.desabilataFiltro || manterVincPerfilTrocaArqPartBean.disableArgumentosConsulta}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim> <br> </f:verbatim> 

		<br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">		

				<app:scrollableDataTable id="dataTable" value="#{manterVincPerfilTrocaArqPartBean.listaPesquisa}" var="result" 
					rows="10" rowIndexVar="parametroKey" 
					rowClasses="tabela_celula_normal, tabela_celula_destaque" 
					width="100%">

					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
						  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
						</f:facet>		
						<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
							layout="spread" forceId="true" forceIdIndex="false" 
							value="#{manterVincPerfilTrocaArqPartBean.itemSelecionadoLista}">
							<f:selectItems value="#{manterVincPerfilTrocaArqPartBean.listaPesquisaControle}"/>
							<a4j:support event="onclick" reRender="panelBotoes" />
						</t:selectOneRadio>
						<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
					<app:scrollableColumn width="160px" styleClass="colTabRight" > 
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_label_club_participante}" style="width:160; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.cdClub}"/>
					 </app:scrollableColumn>
					<app:scrollableColumn width="160px"  styleClass="colTabRight">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_label_cpfcnpj_participante}" style="width:160; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.cpfCnpjFormatado}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="300px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_nomerazaosocial_participante}" style="width:300; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.cdRazaoSocial}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="160px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_label_tipo_participacao}" style="width:160; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.dsTipoParticipante}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="100px"  styleClass="colTabRight">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_label_perfil}" style="width:100; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.cdPerfilTrocaArq}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="100px"  styleClass="colTabCenter">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.detManterSolManutContratos_layout_proprio}" style="width:100; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.dsLayoutProprio}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="160px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_label_tipo_layout}" style="width:160; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.dsTipoLayoutArquivo}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="160px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.incSolicitacaoRenegociacaoContrato_label_servico}" style="width:160; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.dsProdutoServico}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="160px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.altLayoutsArquivos1_aplicativo_formatacao}" style="width:160; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.dsAplicacaoTransmicaoPagamento}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="160px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_meio_transmissao_principal_remessa}" style="width:160; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.dsMeioPrincRemss}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="160px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_meio_transmissao_alternativo_remessa}" style="width:160; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.dsMeioAltrnRemss}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="160px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_meio_transmissao_principal_retorno}" style="width:160; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.dsMeioPrincRetor}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="160px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_meio_transmissao_alternativo_retorno}" style="width:160; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.dsMeioAltrnRetor}"/>
					</app:scrollableColumn>
					<app:scrollableColumn width="160px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_empresa_transmissao}" style="width:160; text-align:center"  />
						</f:facet>
						<br:brOutputText value="#{result.dsRzScialPcero}"/>
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>		
		</br:brPanelGrid>

	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterVincPerfilTrocaArqPartBean.carregaListaPaginacao}" >
					<f:facet name="first">
						<brArq:pdcCommandButton id="primeira"
						styleClass="bto1"
						value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					</f:facet>
					<f:facet name="fastrewind">
						<brArq:pdcCommandButton id="retrocessoRapido"
						styleClass="bto1" style="margin-left: 3px;"
						value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					</f:facet>
					<f:facet name="previous">
						<brArq:pdcCommandButton id="anterior"
						styleClass="bto1" style="margin-left: 3px;"
						value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton id="proxima"
						styleClass="bto1" style="margin-left: 3px;"
						value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					</f:facet>
					<f:facet name="fastforward">
						<brArq:pdcCommandButton id="avancoRapido"
						styleClass="bto1" style="margin-left: 3px;"
						value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton id="ultima"
						styleClass="bto1" style="margin-left: 3px;"
						value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					</f:facet>
				</brArq:pdcDataScroller> 
			</br:brPanelGroup>
		</br:brPanelGrid>	

		<f:verbatim><hr class="lin"> </f:verbatim>

		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
			<br:brPanelGrid columns="2" width="100%" style="text-align:left" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup style="text-align:left">
					<br:brCommandButton id="btoVoltar" styleClass="bto1" value="#{msgs.label_voltar}" action="#{manterVincPerfilTrocaArqPartBean.voltar}">	
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
				<br:brPanelGroup style="width:100%;text-align:right">
					<br:brCommandButton id="btoLimpar" styleClass="bto1" value="#{msgs.label_limpar}" disabled="#{manterVincPerfilTrocaArqPartBean.identificacaoClienteBean.desabilataFiltro}" action="#{manterVincPerfilTrocaArqPartBean.limparPaginaArgumentos}" style="cursor:hand; margin-right:5px;" onmouseout="javascript:alteraBotao('normal', this.id);">	
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterVincPerfilTrocaArqPartBean.itemSelecionadoLista}"  style="margin-right:5px" value="#{msgs.convincperfiltrocaarqparticipante_label_botao_detalhar}"  action="#{manterVincPerfilTrocaArqPartBean.detalhar}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnIncluir" styleClass="bto1" disabled="#{manterVincPerfilTrocaArqPartBean.identificacaoClienteBean.desabilataFiltro}"  style="margin-right:5px" value="#{msgs.convincperfiltrocaarqparticipante_label_botao_incluir}"  action="#{manterVincPerfilTrocaArqPartBean.incluir}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="alterarContratoMae" styleClass="bto1" disabled="#{empty manterVincPerfilTrocaArqPartBean.itemSelecionadoLista}"  style="margin-right:5px" value="#{msgs.convincperfiltrocaarqparticipante_label_botao_alterar_contrato_mae}"  action="#{manterVincPerfilTrocaArqPartBean.alterarContratoMae}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty manterVincPerfilTrocaArqPartBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.convincperfiltrocaarqparticipante_label_botao_excluir}"  action="#{manterVincPerfilTrocaArqPartBean.excluir}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnHistorico" styleClass="bto1" disabled="#{manterVincPerfilTrocaArqPartBean.identificacaoClienteBean.desabilataFiltro}" value="#{msgs.convincperfiltrocaarqparticipante_label_botao_historico}"  action="#{manterVincPerfilTrocaArqPartBean.historico}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>		

	</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>