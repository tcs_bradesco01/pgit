<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="altVincManterVincPerfilTrocaArqParticipante" name="altVincManterVincPerfilTrocaArqParticipante" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

		<jsp:include page="/content/manutencaoApoioProduto/manterVincPerfilTrocaArqParticipante/cabecPadraoVincPerfilTrocaArq.jsp" flush="false" />
		
		<f:verbatim> <hr class="lin"> </f:verbatim>	
		
		<br:brOutputLabel styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_participante}:"/>
		<br:brPanelGrid id="panelParticipante" columns="2" style="margin-top: 9px">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_cpfcnpj}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.cpfCnpjParticipanteIncluir}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.nomeRazaoSocialParticipanteIncluir}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_participacao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.tipoParticipacao}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao_participacao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.situacaoParticipacao}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<f:verbatim> <hr class="lin"> </f:verbatim>
		
		<br:brOutputLabel styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_perfil}:"/>
		<br:brPanelGrid id="panelPerfil" columns="2" style="margin-top: 9px">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo_perfil}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.codigoPerfilIncluir}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.dsTipoLayoutArquivo}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>	
		
		<br:brOutputLabel styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contrato_mae}:"/>
		<br:brPanelGrid id="panelContratoMae" style="margin-top: 9px">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_contrato_mae}:"/>
				<br:brInputText
					value="#{manterVincPerfilTrocaArqPartBean.nrSequencialContratoMae}"
					maxlength="10"
					onkeypress="onlyNum()"
					onblur="validaCampoNumerico(this, 'O campo deve conter apenas n�meros.');"
					styleClass="HtmlInputTextBradesco" />
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>		
		
		<br:brPanelGrid id="painelBotoes" columns="2" width="100%">
			<br:brPanelGroup>
				<br:brCommandButton id="btoVoltar" styleClass="bto1" value="#{msgs.label_voltar}" action="#{manterVincPerfilTrocaArqPartBean.voltarVinculacao}">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="float:right">			
				<br:brCommandButton id="alterar" styleClass="bto1" style="margin-right:5px" value="#{msgs.conLancamentoPersonalizado_path_alterar}"  action="#{manterVincPerfilTrocaArqPartBean.detalharAlteracaoContratoMae}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>		
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>