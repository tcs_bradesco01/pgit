<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="incManterVincPerfilTrocaArqParticipanteConf" name="incManterVincPerfilTrocaArqParticipanteConf" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <jsp:include page="/content/manutencaoApoioProduto/manterVincPerfilTrocaArqParticipante/cabecPadraoVincPerfilTrocaArq.jsp" flush="false" />
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_participante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	  <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_cpfcnpj_participante}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.cpfCnpjParticipanteIncluir}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_nomerazaosocial_participante}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.nomeRazaoSocialParticipanteIncluir}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	


	  <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incvincperfiltrocaarqparticipante_label_tipo_participacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.tipoParticipacao}"/>
		</br:brPanelGroup>	

		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incvincperfiltrocaarqparticipante_label_situacao_participacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.situacaoParticipacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_perfil}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_codigo_perfil}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.codigoPerfilIncluir}"/>
		</br:brPanelGroup>	

		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_tipo_layout_arquivo}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.descTipoLayout}"/>
		</br:brPanelGroup>	
						
	</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contrato_mae}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.cdContratoMae}" rendered="#{manterVincPerfilTrocaArqPartBean.cdContratoMae != 0}"  />			
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.cdCpfCnpjContratoMae}" rendered="#{manterVincPerfilTrocaArqPartBean.cdCpfCnpjContratoMae != '000.000.000-00'}"  />			
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.dsRazaoSocialContratoMae}" />			
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo_perfil}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.cdPerfilContratoMae}" rendered="#{manterVincPerfilTrocaArqPartBean.cdPerfilContratoMae != 0}" />			
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.dsTipoLayoutArquivoContratoMae}"  />			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_hora_inclusao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.dataHoraInclusao}"  />			
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_usuario}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.usuarioInclusao}"  />			
			</br:brPanelGroup>
		</br:brPanelGrid>
				
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_canal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.tipoCanalInclusao}"  />
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_complemento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.complementoInclusao}"  />			
			</br:brPanelGroup>					
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_hora_manutencao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.dataHoraManutencao}"  />
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_usuario}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.usuarioManutencao}"  />			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>				
			
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_canal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.tipoCanalManutencao}"  />
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_complemento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.complementoManutencao}"  />			
			</br:brPanelGroup>					
		</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%"   cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="width:750px">
				<br:brCommandButton styleClass="bto1" value="#{msgs.label_botao_voltar}" action="#{manterVincPerfilTrocaArqPartBean.voltarVinculacao}" >	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>	

	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
