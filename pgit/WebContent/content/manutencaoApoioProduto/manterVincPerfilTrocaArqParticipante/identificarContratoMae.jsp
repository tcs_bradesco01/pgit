<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="identificarContratoMae" name="identificarContratoMae" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contrato_mae}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_numero}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.cdContratoMae}" />
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
	
		<f:verbatim><br></f:verbatim> 
	
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="OverflowScrollableDataTable" >	
				<app:scrollableDataTable id="dataTable" value="#{manterVincPerfilTrocaArqPartBean.saidaContratoMae.ocorrencias}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio value="#{manterVincPerfilTrocaArqPartBean.itemSelecionadoContratoMae}" id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
							<f:selectItems value="#{manterVincPerfilTrocaArqPartBean.listaControleContratoMae}"/>
							<a4j:support event="onclick" reRender="panelBotoes"/>
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabCenter" width="150px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_cpf_cnpj}" style="width:100%; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cpfCnpjFormatado}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabCenter" width="450px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_nome_razao_social}" style="width:500px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsNomeRazao}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabCenter" width="100px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_filtro_perfil}" style="width:100%; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdPerfilTrocaArquivo}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabCenter" width="100px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_layout}" style="width:100%; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsTipoLayoutArquivo}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
									
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
			    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterVincPerfilTrocaArqPartBean.paginarContratoMae}" >
					<f:facet name="first">				    
						<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					</f:facet>
				  	<f:facet name="fastrewind">
				    	<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  	</f:facet>
				  	<f:facet name="previous">
				    	<brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  	</f:facet>
				  	<f:facet name="next">
				    	<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  	</f:facet>
				  	<f:facet name="fastforward">
				    	<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  	</f:facet>
				  	<f:facet name="last">
				    	<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  	</f:facet>
				</brArq:pdcDataScroller>			  
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<a4j:outputPanel id="panelBotoes" style="width: 100%; " ajaxRendered="true">	
   		
   	   <br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
    	<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.incDuplicacaoArquivoRetorno_btn_voltar}" action="#{manterVincPerfilTrocaArqPartBean.voltarIncluirContratoMae}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup> 
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
    
    	<br:brPanelGroup style="text-align:right;width:150px" >
		<br:brCommandButton id="btnSelecionar" styleClass="bto1" value="Selecionar" action="#{manterVincPerfilTrocaArqPartBean.selecionarContratoMae}" disabled="#{empty manterVincPerfilTrocaArqPartBean.itemSelecionadoContratoMae}"  style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
       </br:brPanelGrid>	
       
	</a4j:outputPanel> 
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
