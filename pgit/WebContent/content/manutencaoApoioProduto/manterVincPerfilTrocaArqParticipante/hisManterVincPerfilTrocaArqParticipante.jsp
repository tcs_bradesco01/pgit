<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%> 
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="hisManterPerfilTrocaArqLayoutForm" name="hisManterPerfilTrocaArqLayoutForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <jsp:include page="/content/manutencaoApoioProduto/manterVincPerfilTrocaArqParticipante/cabecPadraoVincPerfilTrocaArq.jsp" flush="false" />

	<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	<t:selectOneRadio id="rdoFiltro" value="#{manterVincPerfilTrocaArqPartBean.itemFiltroHistorico}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterVincPerfilTrocaArqPartBean.disableArgumentosConsultaHistorico}">   
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<a4j:support event="onclick" reRender="panelPerfil, lblCpfCnpjParticipante, lblNomeRazaoParticipante" action="#{manterVincPerfilTrocaArqPartBean.limparArgumentosHistorico}" />
	</t:selectOneRadio>
	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
	      <t:radio for="rdoFiltro" index="0" />			
		  <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_perfil}:"/>		
		</br:brPanelGroup>	

	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="panelPerfil" ajaxRendered="true">	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-left:20px">
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_codigo_perfil}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>		
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterVincPerfilTrocaArqPartBean.codigoPerfilHistorico}" id="txtCodigoPerfil" disabled="#{manterVincPerfilTrocaArqPartBean.itemFiltroHistorico != '0' || manterVincPerfilTrocaArqPartBean.disableArgumentosConsultaHistorico}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" size="20" maxlength="9" onkeypress="onlyNum()" converter="javax.faces.Long" />
				</br:brPanelGroup>
			</br:brPanelGrid>	
										
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
			
			
		<br:brPanelGroup>
		
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_tipo_layout_arquivo}:"/>
					</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
	 			</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
					<br:brSelectOneMenu id="cboTipoLayout" converter="inteiroConverter" value="#{manterVincPerfilTrocaArqPartBean.tipoLayoutHistorico}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{manterVincPerfilTrocaArqPartBean.itemFiltroHistorico != '0' || manterVincPerfilTrocaArqPartBean.disableArgumentosConsultaHistorico}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems  value="#{manterVincPerfilTrocaArqPartBean.listaTipoLayout}" />						
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
	      <t:radio for="rdoFiltro" index="1" />			
		  <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_participante}:"/>		
		</br:brPanelGroup>	
		
		<a4j:outputPanel id="panelParticipante" ajaxRendered="true" >	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="100%" style="text-align:right" >
		<br:brPanelGroup>
			<br:brCommandButton id="btnPesquisar"  styleClass="bto1" value="#{msgs.convincperfiltrocaarqparticipante_label_botao_pesquisar}" disabled="#{manterVincPerfilTrocaArqPartBean.itemFiltroHistorico != '1' || manterVincPerfilTrocaArqPartBean.disableArgumentosConsultaHistorico}" action="#{manterVincPerfilTrocaArqPartBean.pesquisarParticipanteHistorico}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		</br:brPanelGrid>			
		</a4j:outputPanel>
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	  <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_cpfcnpj_participante}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" id="lblCpfCnpjParticipante" value="#{manterVincPerfilTrocaArqPartBean.cpfCnpjParticipanteHistorico}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_nomerazaosocial_participante}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" id="lblNomeRazaoParticipante" value="#{manterVincPerfilTrocaArqPartBean.nomeRazaoSocialParticipanteHistorico}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_manutencao}:" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
	
		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.consultarPagamentosIndividual_de}"/>
					<br:brPanelGroup rendered="#{!manterVincPerfilTrocaArqPartBean.disableArgumentosConsultaHistorico}">						
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicioManutencao');" id="dataInicioManutencao" value="#{manterVincPerfilTrocaArqPartBean.dataIncialManutencao}" >
						</app:calendar>
					</br:brPanelGroup>
					<br:brPanelGroup rendered="#{manterVincPerfilTrocaArqPartBean.disableArgumentosConsultaHistorico}">						
						<app:calendar id="dataInicioManutencaoDisabled" value="#{manterVincPerfilTrocaArqPartBean.dataIncialManutencao}" disabled="true">
						</app:calendar>
					</br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
					<br:brPanelGroup rendered="#{!manterVincPerfilTrocaArqPartBean.disableArgumentosConsultaHistorico}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFimManutencao');" id="dataFimManutencao" value="#{manterVincPerfilTrocaArqPartBean.dataFimManutencao}" >
		 				</app:calendar>	
					</br:brPanelGroup>	
					<br:brPanelGroup rendered="#{manterVincPerfilTrocaArqPartBean.disableArgumentosConsultaHistorico}">
						<app:calendar id="dataFimManutencaoDisabled" value="#{manterVincPerfilTrocaArqPartBean.dataFimManutencao}" disabled="true">
		 				</app:calendar>	
					</br:brPanelGroup>
				</br:brPanelGroup>			    
			</br:brPanelGrid>
		</a4j:outputPanel> 
		
	   <f:verbatim><hr class="lin"> </f:verbatim>	
	   
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{manterVincPerfilTrocaArqPartBean.limparDadosHistorico}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{manterVincPerfilTrocaArqPartBean.consultarHistorico}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
		<f:verbatim><br></f:verbatim> 
			
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="OverflowScrollableDataTable" >	
				<app:scrollableDataTable id="dataTable" value="#{manterVincPerfilTrocaArqPartBean.listaGridHistorico}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio value="#{manterVincPerfilTrocaArqPartBean.itemSelecionadoHistorico}" id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
							<f:selectItems value="#{manterVincPerfilTrocaArqPartBean.listaControleHistorico}"/>
							<a4j:support event="onclick" reRender="btnDetalhar"/>
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
				
					<app:scrollableColumn styleClass="colTabRight" width="180px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_label_club_participante}" style="width:180; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdClub}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>		
					
					<app:scrollableColumn styleClass="colTabRight" width="180px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_label_cpfcnpj_participante}" style="width:180; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cpfCnpjFormatado}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_nomerazaosocial_participante}" style="width:250; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdRazaoSocial}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabLeft" width="180px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_label_tipo_participacao}" style="width:180; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdDsTipoParticipante}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabLeft" width="180px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_label_tipo_layout}" style="width:180; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsLayout}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabRight" width="180px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.convincperfiltrocaarqparticipante_label_perfil}" style="width:180; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdPerfilTrocaArquivo}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabCenter" width="180px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_data_hora_manutencao}" style="width:180; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dataHoraFormatada}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>	
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
			    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterPerfilTrocaArqLayoutBean.pesquisarHistorico}" >
					<f:facet name="first">				    
						<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					</f:facet>
				  	<f:facet name="fastrewind">
				    	<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  	</f:facet>
				  	<f:facet name="previous">
				    	<brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  	</f:facet>
				  	<f:facet name="next">
				    	<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  	</f:facet>
				  	<f:facet name="fastforward"> 
				    	<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  	</f:facet>
				  	<f:facet name="last">
				    	<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  	</f:facet>
				</brArq:pdcDataScroller>			  
			</br:brPanelGroup>
		</br:brPanelGrid>	

	   	<f:verbatim><hr class="lin"> </f:verbatim>				

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{manterVincPerfilTrocaArqPartBean.voltarVinculacao}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px">		
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterVincPerfilTrocaArqPartBean.itemSelecionadoHistorico}"  value="#{msgs.btn_detalhar}"  action="#{manterVincPerfilTrocaArqPartBean.detalharHistorico}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
	</br:brPanelGrid>  

	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>