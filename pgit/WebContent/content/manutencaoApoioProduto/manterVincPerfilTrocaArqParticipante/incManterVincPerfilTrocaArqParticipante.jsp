<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="incManterVincPerfilTrocaArqParticipante" name="incManterVincPerfilTrocaArqParticipante" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<jsp:include page="/content/manutencaoApoioProduto/manterVincPerfilTrocaArqParticipante/cabecPadraoVincPerfilTrocaArq.jsp" flush="false" />

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_participante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	  <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_cpfcnpj_participante}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterVincPerfilTrocaArqPartBean.cpfCnpjParticipanteIncluir}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_nomerazaosocial_participante}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterVincPerfilTrocaArqPartBean.nomeRazaoSocialParticipanteIncluir}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	  <br:brPanelGrid width="100%" columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incvincperfiltrocaarqparticipante_label_tipo_participacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.tipoParticipacao}"/>

			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incvincperfiltrocaarqparticipante_label_situacao_participacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.situacaoParticipacao}"/>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="100%" style="text-align:right" >
			<br:brPanelGroup>
				<br:brCommandButton id="btnPesquisar" styleClass="bto1" value="#{msgs.convincperfiltrocaarqparticipante_label_botao_pesquisar}" action="#{manterVincPerfilTrocaArqPartBean.flagTelaIncluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1"  cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_perfil}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid width="100%" columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_codigo_perfil}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.codigoPerfilIncluir}"/>

			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_tipo_layout_arquivo}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.descTipoLayout}"/>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="100%" style="text-align:right" >
			<br:brPanelGroup>
				<br:brCommandButton id="btnPesquisar2" styleClass="bto1" value="#{msgs.convincperfiltrocaarqparticipante_label_botao_pesquisar}" action="#{manterVincPerfilTrocaArqPartBean.consultarPerfil}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contrato_mae}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid width="100%" columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_contrato_mae}:"/>			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brInputText id="contratoMae" value="#{manterVincPerfilTrocaArqPartBean.cdContratoMae}">
				<a4j:support event="onblur" reRender="btnPesquisar3, btnAvancar" />
			</br:brInputText>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.cdCpfCnpjContratoMae}"/>

			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.dsRazaoSocialContratoMae}"/>
		</br:brPanelGroup>	
		
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid width="100%" columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo_perfil}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincPerfilTrocaArqPartBean.cdPerfilContratoMae}"/>
	
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterMoedasSistema_label_grid_tipo_layout_arquivo}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincPerfilTrocaArqPartBean.dsTipoLayoutArquivoContratoMae}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="100%" style="text-align:right" >
				<br:brPanelGroup>
					<br:brCommandButton id="btnPesquisar3" styleClass="bto1" value="#{msgs.convincperfiltrocaarqparticipante_label_botao_pesquisar}" action="#{manterVincPerfilTrocaArqPartBean.pesquisarContratoMae}" disabled="#{manterVincPerfilTrocaArqPartBean.cdContratoMae == null}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>	
		</br:brPanelGrid>
		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" disabled="false"  style="align:left" value="#{msgs.botao_voltar}" action="#{manterVincPerfilTrocaArqPartBean.voltarVinculacao}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.incvincperfiltrocaarqparticipante_label_botao_avancar}"  action="#{manterVincPerfilTrocaArqPartBean.avancarIncluir}" 
			onclick="javascript: desbloquearTela(); return validarInclusao('#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{manterVincPerfilTrocaArqPartBean.codigoPerfilIncluir}', '#{manterVincPerfilTrocaArqPartBean.cpfCnpjParticipanteIncluir}', '#{msgs.convincperfiltrocaarqparticipante_label_perfil}', '#{msgs.convincperfiltrocaarqparticipante_label_participante}', '#{msgs.label_contrato_mae}', '#{manterVincPerfilTrocaArqPartBean.cdCpfCnpjContratoMae}', '#{manterVincPerfilTrocaArqPartBean.dsRazaoSocialContratoMae}', '#{manterVincPerfilTrocaArqPartBean.cdPerfilContratoMae}', '#{manterVincPerfilTrocaArqPartBean.dsTipoLayoutArquivoContratoMae}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		  
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
