<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="identificacaoParticipanteIncluirForm" name="identificacaoParticipanteIncluirForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

    <jsp:include page="/content/manutencaoApoioProduto/manterVincPerfilTrocaArqParticipante/cabecPadraoVincPerfilTrocaArq.jsp" flush="false" />
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">
 
			<app:scrollableDataTable id="dataTable" value="#{manterVincPerfilTrocaArqPartBean.listaGridIncluir}" var="varResult" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterVincPerfilTrocaArqPartBean.itemSelecionadoGridIncluir}" >
						<f:selectItems value="#{manterVincPerfilTrocaArqPartBean.listaGridControleIncluir}"/>
						<a4j:support event="onclick" reRender="btoSelecionar" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="180px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conParticipantesManterContrato_grid_clubParticipante}" style="text-align:center;width:180" />
					</f:facet>
					<br:brOutputText value="#{varResult.cdParticipante}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="180px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conParticipantesManterContrato_grid_cpfcnpj_participante}" style="text-align:center;width:180" />
					</f:facet>
					<br:brOutputText value="#{varResult.cpf}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="250px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conParticipantesManterContrato_label_nomerazaosocial_participante}" style="text-align:center;width:250"/>
					</f:facet>
					<br:brOutputText value="#{varResult.nomeRazao}"/>
				</app:scrollableColumn>
 
				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conParticipantesManterContrato_grid_tipoParticipacao}" style="text-align:center;width:180"/>
					</f:facet>
					<br:brOutputText value="#{varResult.tipoParticipacaoFormatador}"/>
				</app:scrollableColumn>
 
				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conParticipantesManterContrato_grid_situacaoParticipacao}" style="text-align:center;width:180"/>
					</f:facet>
					<br:brOutputText value="#{varResult.dsSituacaoParticipacao}"/>
				</app:scrollableColumn>
				
			</app:scrollableDataTable>
						
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterVincPerfilTrocaArqPartBean.pesquisarParticipante}"  > 
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1" 
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>	
		</br:brPanelGroup>
	</br:brPanelGrid>

<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_voltar}" action="#{manterVincPerfilTrocaArqPartBean.voltarIncluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		 		
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btoSelecionar" disabled="#{empty manterVincPerfilTrocaArqPartBean.itemSelecionadoGridIncluir}" style="margin-left:5px" styleClass="bto1" value="#{msgs.btn_selecionar}" action="#{manterVincPerfilTrocaArqPartBean.selecionarParticipanteIncluir}">							
				<brArq:submitCheckClient/>
			</br:brCommandButton>					
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	