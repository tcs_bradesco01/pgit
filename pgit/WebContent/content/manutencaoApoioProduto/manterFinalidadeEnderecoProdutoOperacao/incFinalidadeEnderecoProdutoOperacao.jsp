<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incFinalidadeEnderecoProdutoServico" name="incFinalidadeEnderecoProdutoServico" >

<h:inputHidden id="hiddenObrigatoriedade" value="#{finalidadeEndProdutoServBean.hiddenObrigatoriedade}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_tipo_servico}:"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					</br:brPanelGrid>	
				</br:brPanelGroup>	
				
				<br:brPanelGroup style="margin-right:20px">		
					<br:brSelectOneMenu id="tipoServico" value="#{finalidadeEndProdutoServBean.incTipoServico}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_selecione}"/>
						<f:selectItems  value="#{finalidadeEndProdutoServBean.listaTipoServico}" />	
						<a4j:support event="onchange" reRender="modalidadeServico, operacao" action="#{finalidadeEndProdutoServBean.carregaListaIncluir}" />										
					</br:brSelectOneMenu>
				</br:brPanelGroup>				
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
		
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid> 
			
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_modalidade_servico}:"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					</br:brPanelGrid>	
				</br:brPanelGroup>	
				
				<br:brPanelGroup>	
					<br:brSelectOneMenu id="modalidadeServico" value="#{finalidadeEndProdutoServBean.incModalidadeServico}" disabled="#{finalidadeEndProdutoServBean.incTipoServico == 0 || finalidadeEndProdutoServBean.incTipoServico == null}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_selecione}"/>
						<f:selectItems  value="#{finalidadeEndProdutoServBean.listaModalidadeServicoIncluir}" />			
					</br:brSelectOneMenu>
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_operacao}:"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					</br:brPanelGrid>	
				</br:brPanelGroup>	
				
				<br:brPanelGroup>	
					<br:brSelectOneMenu id="operacao" value="#{finalidadeEndProdutoServBean.incOperacao}" disabled="#{finalidadeEndProdutoServBean.incTipoServico == 0 || finalidadeEndProdutoServBean.incTipoServico == null}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_selecione}"/>
						<f:selectItems  value="#{finalidadeEndProdutoServBean.listaOperacaoIncluir}" />			
					</br:brSelectOneMenu>
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_finalidade_endereco}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboFinalidadeEndereco" value="#{finalidadeEndProdutoServBean.incFinalidadeEndereco}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_selecione}"/>
				<f:selectItems  value="#{finalidadeEndProdutoServBean.listaFinalidadeEndereco}" />			
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_btn_voltar}" action="#{finalidadeEndProdutoServBean.voltar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btnAvancar" disabled="false" styleClass="bto1"  value="#{msgs.conFinalidadeEnderecoProdutoOperacao_btn_avancar}" action="#{finalidadeEndProdutoServBean.avancar}"  onclick="javascript: validar(document.forms[1],'#{msgs.label_ocampo}','#{msgs.label_necessario}','#{msgs.conFinalidadeEnderecoProdutoOperacao_label_tipo_servico}', '#{msgs.conFinalidadeEnderecoProdutoOperacao_label_modalidade_servico}', '#{msgs.label_operacao}', '#{msgs.conFinalidadeEnderecoProdutoOperacao_label_finalidade_endereco}');" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>					
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>