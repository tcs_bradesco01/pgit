<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conFinalidadeEnderecoProdutoServico" name="conFinalidadeEnderecoProdutoServico" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_title_argumentoPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>  
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_tipo_servico}:"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					</br:brPanelGrid>	
				</br:brPanelGroup>	
				
				<br:brPanelGroup style="margin-right:20px">		
					<br:brSelectOneMenu id="tipoServico" value="#{finalidadeEndProdutoServBean.tipoServicoFiltro}" disabled="#{finalidadeEndProdutoServBean.btoAcionado}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_selecione}"/>
						<f:selectItems  value="#{finalidadeEndProdutoServBean.listaTipoServico}" />
						<a4j:support event="onchange" reRender="modalidadeServico, operacao" action="#{finalidadeEndProdutoServBean.carregaCombos}" />		
					</br:brSelectOneMenu>
				</br:brPanelGroup>				
			</br:brPanelGrid>
		</br:brPanelGroup>
		
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_modalidade_servico}:"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					</br:brPanelGrid>	
				</br:brPanelGroup>	
				
				<br:brPanelGroup>	
					<br:brSelectOneMenu id="modalidadeServico" value="#{finalidadeEndProdutoServBean.modalidadeServicoFiltro}" disabled="#{finalidadeEndProdutoServBean.tipoServicoFiltro == 0 || finalidadeEndProdutoServBean.tipoServicoFiltro == null || finalidadeEndProdutoServBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_selecione}"/>
						<f:selectItems  value="#{finalidadeEndProdutoServBean.listaModalidadeServico}" />			
					</br:brSelectOneMenu>
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_operacao}:"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					</br:brPanelGrid>	
				</br:brPanelGroup>	
				
				<br:brPanelGroup style="margin-right:20px">		
					<br:brSelectOneMenu id="operacao" value="#{finalidadeEndProdutoServBean.operacaoFiltro}" disabled="#{finalidadeEndProdutoServBean.tipoServicoFiltro == 0 || finalidadeEndProdutoServBean.tipoServicoFiltro == null || finalidadeEndProdutoServBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_selecione}"/>
						<f:selectItems  value="#{finalidadeEndProdutoServBean.listaOperacao}" />			
					</br:brSelectOneMenu>
				</br:brPanelGroup>				
			</br:brPanelGrid>
		</br:brPanelGroup>
	
		<br:brPanelGroup>
		 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		 	<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_finalidade_endereco}:"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				</br:brPanelGrid>	
			</br:brPanelGroup>						
			
			<br:brPanelGroup>
				<br:brSelectOneMenu id="cboFinalidadeEndereco" value="#{finalidadeEndProdutoServBean.finalidadeEnderecoFiltro}" disabled="#{finalidadeEndProdutoServBean.btoAcionado}">
					<f:selectItem itemValue="0" itemLabel="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_selecione}"/>
					<f:selectItems  value="#{finalidadeEndProdutoServBean.listaFinalidadeEndereco}" />			
				</br:brSelectOneMenu>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >				
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnLimparDados" disabled="false" styleClass="bto1" style="margin-right:5px" value="Limpar Campos" action="#{finalidadeEndProdutoServBean.limparDados}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
			<br:brCommandButton id="btnConsultar" style="margin-left:5px" styleClass="bto1" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_btn_consultar}" action="#{finalidadeEndProdutoServBean.consultar}" disabled="#{finalidadeEndProdutoServBean.btoAcionado}">						
				<brArq:submitCheckClient/>
			</br:brCommandButton>					
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><br> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
			
			<app:scrollableDataTable id="tableServico" value="#{finalidadeEndProdutoServBean.listaGrid}" var="varResult" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false"
						value="#{finalidadeEndProdutoServBean.itemSelecionadoLista}" >
						<f:selectItems value="#{finalidadeEndProdutoServBean.listaControle}"/>
						<a4j:support event="onclick" reRender="btnDetalhar,btnExluir" />	
					</t:selectOneRadio>
					<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="230px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_tipo_servico}" style="width:230; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{varResult.dsTipoServico}"/>
				</app:scrollableColumn>
	
				<app:scrollableColumn styleClass="colTabLeft" width="230px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_modalidade_servico}" style="width:230; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{varResult.dsModalidadeServico}"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="350px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_operacao}" style="width:350px; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{varResult.dsOperacaoProdutoServico}"/>
				</app:scrollableColumn>		
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conFinalidadeEnderecoProdutoOperacao_label_finalidade_endereco}" style="width:200; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{varResult.dsFinalidadeEndereco}"/>
				</app:scrollableColumn>
				
			</app:scrollableDataTable>
						
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="tableServico" actionListener="#{finalidadeEndProdutoServBean.pesquisar}"  > 
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1" 
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >		
	<br:brPanelGroup style="text-align:right;width:750px" >		
		<br:brCommandButton id="btnDetalhar" style="margin-left:5px"  disabled="#{empty finalidadeEndProdutoServBean.itemSelecionadoLista}"  styleClass="bto1" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_btn_detalhar}" action="#{finalidadeEndProdutoServBean.detalhar}" >						
			<brArq:submitCheckClient/>
		</br:brCommandButton>
		<br:brCommandButton id="btnIncluir" style="margin-left:5px"  styleClass="bto1" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_btn_incluir}" action="#{finalidadeEndProdutoServBean.incluir}" >						
			<brArq:submitCheckClient/>
		</br:brCommandButton>
		<br:brCommandButton id="btnExluir" style="margin-left:5px" disabled="#{empty finalidadeEndProdutoServBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.conFinalidadeEnderecoProdutoOperacao_btn_excluir}" action="#{finalidadeEndProdutoServBean.excluir}" >						
			<brArq:submitCheckClient/>
		</br:brCommandButton>			
	</br:brPanelGroup>
</br:brPanelGrid>	
  
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
