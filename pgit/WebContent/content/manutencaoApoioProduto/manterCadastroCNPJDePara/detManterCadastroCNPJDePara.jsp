<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>


<brArq:form id="detManterCadastroCNPJDeParaForm" name="detManterCadastroCNPJDeParaForm" >

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup> 
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
	</br:brPanelGroup>
	
	<br:brPanelGrid columns="1" style="margin-top:15px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="3" cellspacing="3" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_convenioPagFor}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.saidaDetalharCnpjFicticio.cdContratoPerfil}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cnpjDePara}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.saidaDetalharCnpjFicticio.cpfCnpjFormatado}"/>
		</br:brPanelGroup>
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razaoSocial}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.saidaDetalharCnpjFicticio.dsRazaoSocial}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cnpjOriginal}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.cpfCnpjOriginalLista}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_contratoPGIT}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.contratoPGITLista}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_dataHrInclusao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterCadastroCnpjDeParaBean.dataHoraInclusao}"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_usuario}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterCadastroCnpjDeParaBean.saidaDetalharCnpjFicticio.usuarioInclusaoFormatado}"/>
		</br:brPanelGroup>
		
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_tipoCanal}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterCadastroCnpjDeParaBean.saidaDetalharCnpjFicticio.canalInclusaoFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_complemento}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value=""/>
		</br:brPanelGroup>
  		 </br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_dataHrManutencao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterCadastroCnpjDeParaBean.dataHoraManutencao}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.manterClientesOrgaoPublicoFederal_label_usuario}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterCadastroCnpjDeParaBean.saidaDetalharCnpjFicticio.usuarioManutencaoFormatado}"/>
		</br:brPanelGroup>
		
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_tipoCanal}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterCadastroCnpjDeParaBean.saidaDetalharCnpjFicticio.canalManutencaoFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_complemento}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value=""/>
		</br:brPanelGroup>
  		 </br:brPanelGrid>


	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:left" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnVoltar"  styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_voltar}"  action="#{manterCadastroCnpjDeParaBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
			
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
