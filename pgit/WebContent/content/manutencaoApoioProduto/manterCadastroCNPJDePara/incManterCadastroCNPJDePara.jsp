<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>


<brArq:form id="incManterCadastroCNPJDeParaForm" name="incManterCadastroCNPJDeParaForm" >
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.convincperfiltrocaarqparticipante_label_title_identificacao_cliente}:"/>
		</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:15px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
         <a4j:outputPanel id="painelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cnpjDePara}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>	
			    	<br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'incManterCadastroCNPJDeParaForm','incManterCadastroCNPJDeParaForm:txtCnpj','incManterCadastroCNPJDeParaForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9"  styleClass="HtmlInputTextBradesco" value="#{manterCadastroCnpjDeParaBean.entradaValidarCNPJFicticio.cdCnpjCpf}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			 	    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'incManterCadastroCNPJDeParaForm','incManterCadastroCNPJDeParaForm:txtFilial','incManterCadastroCNPJDeParaForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4"  styleClass="HtmlInputTextBradesco" value="#{manterCadastroCnpjDeParaBean.entradaValidarCNPJFicticio.cdFilialCnpjCpf}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					<br:brInputText id="txtControle" style="margin-right: 5" onkeypress="onlyNum();" size="3" maxlength="2" styleClass="HtmlInputTextBradesco" value="#{manterCadastroCnpjDeParaBean.cdCtrlCpfCnpjString}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" >		
						<a4j:support event="onblur" reRender="btnAvancar"  />	
					</br:brInputText>
				</br:brPanelGroup>	
			</br:brPanelGrid>
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			</br:brPanelGrid>
		</a4j:outputPanel>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: left" ajaxRendered="true">	
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px"  >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.manterClientesOrgaoPublicoFederal_btn_voltar}" action="#{manterCadastroCnpjDeParaBean.voltar}">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
			
			<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnlimpar" styleClass="bto1" value="#{msgs.btn_limpar}" action="#{manterCadastroCnpjDeParaBean.limparDadosValidacao}">				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAvancar"  style="margin-left:5px" styleClass="bto1" value="#{msgs.btn_avancar}" disabled="#{manterCadastroCnpjDeParaBean.desabilitaBotaoAvancar}"  action="#{manterCadastroCnpjDeParaBean.avancarValidarInclusao}" >				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	</a4j:outputPanel>		
			
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
