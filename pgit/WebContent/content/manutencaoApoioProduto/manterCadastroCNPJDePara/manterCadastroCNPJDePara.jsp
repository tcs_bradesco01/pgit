<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>


<brArq:form id="manterCadastroCNPJDeParaForm" name="manterCadastroCNPJDeParaForm" >
		<h:messages showDetail="true"> </h:messages>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<br:brPanelGroup> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_radio_filtro_cliente}:"/>
		</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:15px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<t:selectOneRadio id="rdoFiltroCNPJ" value="#{manterCadastroCnpjDeParaBean.itemFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterCadastroCnpjDeParaBean.disableArgumentosConsulta}">  
			<f:selectItems value="#{manterCadastroCnpjDeParaBean.radiosFiltro}"/>
			<a4j:support  event="onclick"  reRender="painelConvenio,codConvenio,painelCpf,painelContrato, btnConsultar,panelBotoes,botoesSituacao" action="#{manterCadastroCnpjDeParaBean.limparAposAlterarOpcaoCNPJ}"/>
		</t:selectOneRadio>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCNPJ" index="0" />			
			
				<br:brPanelGrid id="painelConvenio" >
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_convenioPagFor}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" id="codConvenio">	
				  		 <br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{manterCadastroCnpjDeParaBean.itemFiltroSelecionado != '0' || manterCadastroCnpjDeParaBean.disableArgumentosConsulta }" value="#{manterCadastroCnpjDeParaBean.entradaListarCnpjFicticio.cdContrato}" 
				  		 converter="javax.faces.Long" onkeypress="onlyNum();" id="txtCodConvenio">
				  		 <a4j:support event="onblur" reRender="btnConsultar"  />
				  		 </br:brInputText>				
					</br:brPanelGrid>
				</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:15px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCNPJ" index="1" />			
			
				<br:brPanelGrid id="painelCpf" >
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cnpjDePara}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>	
					    	<br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'manterCadastroCNPJDeParaForm','manterCadastroCNPJDeParaForm:txtCnpj','manterCadastroCNPJDeParaForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{manterCadastroCnpjDeParaBean.itemFiltroSelecionado != '1'  || manterCadastroCnpjDeParaBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{manterCadastroCnpjDeParaBean.entradaListarCnpjFicticio.cdCnpjCpf}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					 	    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'manterCadastroCNPJDeParaForm','manterCadastroCNPJDeParaForm:txtFilial','manterCadastroCNPJDeParaForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{manterCadastroCnpjDeParaBean.itemFiltroSelecionado != '1' || manterCadastroCnpjDeParaBean.disableArgumentosConsulta }" styleClass="HtmlInputTextBradesco" value="#{manterCadastroCnpjDeParaBean.entradaListarCnpjFicticio.cdFilialCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
							<br:brInputText id="txtControle" style="margin-right: 5" converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{manterCadastroCnpjDeParaBean.itemFiltroSelecionado != '1'  || manterCadastroCnpjDeParaBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{manterCadastroCnpjDeParaBean.entradaListarCnpjFicticio.cdCtrlCpfCnpj}"onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" >		
								<a4j:support event="onblur" reRender="btnConsultar"  />	
							</br:brInputText>
						</br:brPanelGroup>	
					</br:brPanelGrid>
				   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
					</br:brPanelGrid>
				</br:brPanelGrid>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	<br:brPanelGrid id="painelContrato" columns="1"  cellpadding="0" cellspacing="0" style="margin-left:20px">
			<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
				<t:radio for="rdoFiltroCNPJ" index="2" />		
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_filtrarContrato}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
	                </br:brPanelGrid>
			</br:brPanelGrid>
			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:20px">
					<br:brPanelGroup>
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
					    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
					    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						    <br:brPanelGroup>
						    	<br:brSelectOneMenu id="empresaGestora" disabled="#{manterCadastroCnpjDeParaBean.itemFiltroSelecionado != '2' || manterCadastroCnpjDeParaBean.disableArgumentosConsulta}" value="#{manterCadastroCnpjDeParaBean.entradaListarCnpjFicticio.cdpessoaJuridicaContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px" >						
									<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaEmpresaGestora}" />	
								</br:brSelectOneMenu>
							</br:brPanelGroup>					
						</br:brPanelGrid>
					</br:brPanelGroup>	
					<br:brPanelGroup style="width:20px; margin-bottom:5px" >
					</br:brPanelGroup>		
					<br:brPanelGroup>
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_contrato}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
					    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
					    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						    <br:brPanelGroup>
								<br:brSelectOneMenu id="tipoContrato" disabled="#{manterCadastroCnpjDeParaBean.itemFiltroSelecionado != '2' || manterCadastroCnpjDeParaBean.disableArgumentosConsulta}" value="#{manterCadastroCnpjDeParaBean.entradaListarCnpjFicticio.cdTipoContratoNegocio}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
									<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaTipoContrato}" />
								</br:brSelectOneMenu>
							</br:brPanelGroup>					
						</br:brPanelGrid>
					</br:brPanelGroup>	
					<br:brPanelGroup style="width:20px; margin-bottom:5px" >
					</br:brPanelGroup>		
					<br:brPanelGroup>
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_contrato}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
					    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
			
					    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						    <br:brPanelGroup>
								<br:brInputText id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  value="#{manterCadastroCnpjDeParaBean.entradaListarCnpjFicticio.nrSequenciaContratoNegocio}" size="12" maxlength="10" disabled="#{manterCadastroCnpjDeParaBean.itemFiltroSelecionado != '2' || manterCadastroCnpjDeParaBean.disableArgumentosConsulta}">
							  		<a4j:support event="onblur" reRender="btnConsultar"  />	
							    </br:brInputText>	
							</br:brPanelGroup>					
						</br:brPanelGrid>
					</br:brPanelGroup>	
				</br:brPanelGrid>			
		</br:brPanelGrid>	
   
		 <f:verbatim><hr class="lin"></f:verbatim>
		 
		 <a4j:outputPanel id="botoesSituacao" style="width: 100%; text-align: right" ajaxRendered="true">		
			<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<br:brCommandButton id="btnLimpar" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.label_botao_limpar}"  action="#{manterCadastroCnpjDeParaBean.limparTudo}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnConsultar" disabled="#{manterCadastroCnpjDeParaBean.desabilitaBotaoConsultar}"  styleClass="bto1" value="#{msgs.label_botao_consultar}"  
						action="#{manterCadastroCnpjDeParaBean.listarCNPJFicticio}" onclick="javascript:desbloquearTela(); return validaCamposCNPJDePara(document.forms[1],
								'#{msgs.label_ocampo}',	'#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.consultarPagamentosIndividual_tipo_contrato}',
								'#{msgs.label_numero_contrato}', '#{msgs.label_convenioPagFor}', '#{msgs.label_cnpjDePara}', '#{msgs.label_deve_ser_diferente_de_zeros}');">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>		
		
		 <br:brPanelGrid columns="1" style="margin-top:12px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
				<app:scrollableDataTable id="dataTable" value="#{manterCadastroCnpjDeParaBean.listaCnpjFicticio}" var="result" 
					rows="10" rowIndexVar="parametroKey" 
					rowClasses="tabela_celula_normal, tabela_celula_destaque" 
					width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:30; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterCadastroCnpjDeParaBean.itemSelecionadoLista}">
							<f:selectItems value="#{manterCadastroCnpjDeParaBean.listaControleRadio}"/>
							<a4j:support event="onclick" reRender="panelBotoes"/>
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
				
					<app:scrollableColumn styleClass="colTabCenter" width="150px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_convenioPagFor}" style="width:130; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdContratoSaida}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabCenter" width="100px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_cnpjDePara}"  style="width:150; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cpfCnpjFormatado}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabCenter" width="300px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_nome_razaoSocial}" style="width:130; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsRazaoSocial}" />
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabCenter" width="100px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_cnpjOriginal}"  style="width:100; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cpfCnpjOriginalFormatado}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabCenter" width="100px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_contratoPGIT}" style="width:100; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdNumeroContrato}" />
					</app:scrollableColumn>				
	
				</app:scrollableDataTable>
			</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterCadastroCnpjDeParaBean.paginarCNPJFicticio}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
 	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimpar2" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.label_botao_limpar}"  action="#{manterCadastroCnpjDeParaBean.limparLista}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_incluir}"  action="#{manterCadastroCnpjDeParaBean.avancarIncluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterCadastroCnpjDeParaBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_detalhar}" action="#{manterCadastroCnpjDeParaBean.detalharCNPJFicticio}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty manterCadastroCnpjDeParaBean.itemSelecionadoLista}" style="margin-right:5px"   styleClass="bto1" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_excluir}"  action="#{manterCadastroCnpjDeParaBean.avancarExcluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>			
			
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
