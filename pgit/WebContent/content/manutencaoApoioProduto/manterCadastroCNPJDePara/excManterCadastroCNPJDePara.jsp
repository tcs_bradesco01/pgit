<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>


<brArq:form id="excManterCadastroCNPJDeParaForm" name="excManterCadastroCNPJDeParaForm" >

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup> 
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:" style="margin-left:5px"/>
	</br:brPanelGroup>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="4" cellspacing="4" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_convenioPagFor}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.saidaDetalharCnpjFicticio.cdContratoPerfil}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cnpjDePara}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.saidaDetalharCnpjFicticio.cpfCnpjFormatado}"/>
		</br:brPanelGroup>
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razaoSocial}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.saidaDetalharCnpjFicticio.dsRazaoSocial}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cnpjOriginal}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.cpfCnpjOriginalLista}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_contratoPGIT}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.contratoPGITLista}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup   style="float:left">
			<br:brCommandButton id="btnVoltar"  styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_voltar}"  action="#{manterCadastroCnpjDeParaBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup   style="float:right">
			<br:brCommandButton id="btnConfirmar"  styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_confirmar}"  onclick="javascript: if (!confirm('Confirma Exclus�o?')) { desbloquearTela();  return false; }" action="#{manterCadastroCnpjDeParaBean.confirmarExclusao}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
