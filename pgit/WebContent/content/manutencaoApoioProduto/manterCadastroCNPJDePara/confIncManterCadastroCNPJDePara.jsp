<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>


<brArq:form id="confIncmanterCadastroCNPJDeParaForm" name="confIncmanterCadastroCNPJDeParaForm" >

	<br:brPanelGrid columns="1" style="margin-top:10px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" style="margin-left:7px">
		<br:brPanelGroup> 
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:10px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" cellpadding="4" cellspacing="4" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_convenioPagFor}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.saidaValidarCNPJFicticio.cdContratoPerfil}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cnpjDePara}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.saidaValidarCNPJFicticio.cpfCnpjFormatado}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razaoSocial}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  style="margin-left:5px" value="#{manterCadastroCnpjDeParaBean.saidaValidarCNPJFicticio.dsRazaoSocial}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		
	<f:verbatim><hr class="lin"> </f:verbatim>

	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: left" ajaxRendered="true">	
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px"  >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.manterClientesOrgaoPublicoFederal_btn_voltar}" action="#{manterCadastroCnpjDeParaBean.voltar}">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
			
			<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnConfirmar"  styleClass="bto1" value="#{msgs.btn_confirmar}" action="#{manterCadastroCnpjDeParaBean.confirmarInclusao}" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela();  return false; }">				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	</a4j:outputPanel>		
			
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
