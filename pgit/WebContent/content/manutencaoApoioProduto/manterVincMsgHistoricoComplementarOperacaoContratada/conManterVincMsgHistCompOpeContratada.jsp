<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="manterVincMsgHistoricoComplementarOperacaoContratadaForm" name="manterVincMsgHistoricoComplementarOperacaoContratadaForm" >

<br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	  
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_label_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 	
	
	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.bloqueaRadio}">  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
	</t:selectOneRadio>
	</a4j:region>
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'manterVincMsgHistoricoComplementarOperacaoContratadaForm','manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtCnpj','manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '0' || vincMsgHisCompOpConBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'manterVincMsgHistoricoComplementarOperacaoContratadaForm','manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtFilial','manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '0' || vincMsgHisCompOpConBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '0' || vincMsgHisCompOpConBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '1' || vincMsgHisCompOpConBean.habilitaFiltroDeCliente }" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'manterVincMsgHistoricoComplementarOperacaoContratadaForm','manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtCpf','manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtControleCpf');" />
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '1' || vincMsgHisCompOpConBean.habilitaFiltroDeCliente}" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_nome_razao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '2' || vincMsgHisCompOpConBean.habilitaFiltroDeCliente}" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	

	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  border="0">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '3' || vincMsgHisCompOpConBean.habilitaFiltroDeCliente}" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'manterVincMsgHistoricoComplementarOperacaoContratadaForm','manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtBanco','manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtAgencia');"/>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '3' || vincMsgHisCompOpConBean.habilitaFiltroDeCliente}" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'manterVincMsgHistoricoComplementarOperacaoContratadaForm','manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtAgencia','manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtConta');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '3' || vincMsgHisCompOpConBean.habilitaFiltroDeCliente}" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" />
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCamposCliente" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{vincMsgHisCompOpConBean.limparDadosCliente}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty vincMsgHisCompOpConBean.identificacaoClienteBean.itemFiltroSelecionado || vincMsgHisCompOpConBean.habilitaFiltroDeCliente}" value="#{msgs.conManterVincMsgHistCompOpeContratada_btn_consultar_cliente}" action="#{vincMsgHisCompOpConBean.identificacaoClienteBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" 
	   		onclick="javascript:desbloquearTela(); 
	   		return validaCpoConsultaCliente('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conManterVincMsgHistCompOpeContratada_cnpj}','#{msgs.conManterVincMsgHistCompOpeContratada_cpf}', '#{msgs.conManterVincMsgHistCompOpeContratada_nome_razao}',
	   		'#{msgs.conManterVincMsgHistCompOpeContratada_banco}','#{msgs.conManterVincMsgHistCompOpeContratada_agencia}','#{msgs.conManterVincMsgHistCompOpeContratada_conta}');">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.cmpi0000_label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_label_contrato}:"/>
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_empresa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGroup style="width:100px; " >
		</br:brPanelGroup>	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_tipo}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGroup style="width:152px; " >
		</br:brPanelGroup>	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_numero} do Contrato:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>	
			<br:brSelectOneMenu id="selEmpresaGestoraContrada" converter="longConverter" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.cdPessoaJuridicaContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
				<f:selectItems  value="#{vincMsgHisCompOpConBean.listaEmpresaGestora}" />
				<brArq:commonsValidator type="required" arg="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_empresa_gestora_contrato}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>			
			<br:brSelectOneMenu id="selTipoContrato" converter="inteiroConverter" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.cdTipoContratoNegocio}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
				<f:selectItems  value="#{vincMsgHisCompOpConBean.listaTipoContrato}" />
				<brArq:commonsValidator type="required" arg="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_tipo_contrato}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>					
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.nrSeqContratoNegocio}" size="10" maxlength="10" id="txtNumeroContrato" >
			</br:brInputText>
		</br:brPanelGroup>		 	 
	 </br:brPanelGrid>
	 
	 <f:verbatim><hr class="lin"></f:verbatim>
	 
	 <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<br:brPanelGroup style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCamposContrato" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{vincMsgHisCompOpConBean.limparDadosContrato}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
	   		<br:brCommandButton id="btoConsultarContrato" styleClass="bto1" value="#{msgs.conManterVincMsgHistCompOpeContratada_btn_consultar_contrato}" action="#{vincMsgHisCompOpConBean.identificacaoClienteBean.consultarContratos}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript: desbloquearTela(); return validaCpoContrato('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conManterVincMsgHistCompOpeContratada_empresa}','#{msgs.conManterVincMsgHistCompOpeContratada_tipo}', '#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}', '#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}');">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
     <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_title_contrato}:"/>
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_empresa}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_tipo}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_numero} do Contrato:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_descricao_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_situacao}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
	 </br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	 <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_label_argsPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_label_codigoSegundaLinhaExtrato}:"/>
				</br:brPanelGroup>
		    
				<br:brPanelGroup>
			    	<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" size="10" maxlength="5" id="codigoHistoricoComplementar" value="#{vincMsgHisCompOpConBean.codigoHistoricoComplementarFiltro}" onkeypress="onlyNum();" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.desabilataFiltro || vincMsgHisCompOpConBean.btoAcionado}" style="margin-top:5px;">  
				    	<brArq:commonsValidator type="integer" arg="#{msgs.conManterVincMsgHistCompOpeContratada_label_codigoSegundaLinhaExtrato}" server="false" client="true" />
				    </br:brInputText>	
				</br:brPanelGroup>	
			</br:brPanelGrid>			    	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_label_tipoServico}:"/>
				</br:brPanelGroup>		

				<br:brPanelGroup>				
					<br:brSelectOneMenu id="tipoServico" value="#{vincMsgHisCompOpConBean.tipoServicoFiltro}" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.desabilataFiltro || vincMsgHisCompOpConBean.btoAcionado}" style="margin-top:5px;margin-right:20px"> 
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterVincMsgHistCompOpeContratada_label_combo_selecione}"/>
						<f:selectItems value="#{vincMsgHisCompOpConBean.listaTipoServicoFiltro}"/>
						<a4j:support event="onchange" reRender="modalidadeServico" action="#{vincMsgHisCompOpConBean.listarModalidadeServicoFiltro}" />			
					</br:brSelectOneMenu>							
				</br:brPanelGroup>					
			</br:brPanelGrid>			    	
		</br:brPanelGroup>	

		<br:brPanelGroup>
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterVincMsgHistCompOpeContratada_label_modalidadeServico}:"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brSelectOneMenu id="modalidadeServico" value="#{vincMsgHisCompOpConBean.modalidadeServicoFiltro}" disabled="#{vincMsgHisCompOpConBean.tipoServicoFiltro == null || vincMsgHisCompOpConBean.tipoServicoFiltro == 0 || vincMsgHisCompOpConBean.btoAcionado}" style="margin-top:5px;">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterVincMsgHistCompOpeContratada_label_combo_selecione}"/>
						<f:selectItems value="#{vincMsgHisCompOpConBean.listaModalidadeServicoFiltro}"/>			
					</br:brSelectOneMenu>
				</br:brPanelGroup>			
			</br:brPanelGrid>			    	
		</br:brPanelGroup>	
	</br:brPanelGrid>			    			
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conManterVincMsgHistCompOpeContratada_botao_limpar_campos}" action="#{vincMsgHisCompOpConBean.limparArgumentosPesquisa}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar2"  styleClass="bto1" value="#{msgs.conManterVincMsgHistCompOpeContratada_botao_consultar2}" action="#{vincMsgHisCompOpConBean.carregarGrid}" disabled="#{vincMsgHisCompOpConBean.habilitaConsu}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170px">		
		
			<app:scrollableDataTable id="dataTable" value="#{vincMsgHisCompOpConBean.listaGridPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{vincMsgHisCompOpConBean.itemSelecionadoLista}">
						<f:selectItems value="#{vincMsgHisCompOpConBean.listaPesquisaControle}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
					<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="190px" styleClass="colTabRight" > 
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterVincMsgHistCompOpeContratada_label_codigoSegundaLinhaExtrato}" style="text-align:center;width:190" />
					</f:facet>
					<br:brOutputText value="#{result.cdMensagemLinhaExtrato}"/>
				 </app:scrollableColumn>
				 
				<app:scrollableColumn  width="340px"  styleClass="colTabLeft" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterVincMsgHistCompOpeContratada_grid_lancamentoDebito}" style="text-align:center;width:340" />
					</f:facet>
					<br:brOutputText value="#{result.dsMensagemLinhaDebito}"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn  width="240px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterVincMsgHistCompOpeContratada_grid_lancamentoCredito}" style="text-align:center;width:240"/>
					</f:facet>
					<br:brOutputText value="#{result.dsMensagemLinhaCredito}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="250px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterVincMsgHistCompOpeContratada_grid_tipo_servico}" style="text-align:center;width:250"/>
					</f:facet>
					<br:brOutputText value="#{result.dsProdutoServicoOperacao}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="240px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterVincMsgHistCompOpeContratada_grid_modalidade_servico}"  style="text-align:center;width:240"/>
					</f:facet>
					<br:brOutputText value="#{result.dsProdutoOperacaoRelacionado}"/>
				</app:scrollableColumn>
				 
			</app:scrollableDataTable>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{vincMsgHisCompOpConBean.paginarLista}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>
					
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
		<br:brPanelGrid columns="3" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
				<br:brPanelGroup style="100%">
					<br:brCommandButton id="btnLimpar" styleClass="bto1" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.desabilataFiltro}"  style="margin-right:5px" value="#{msgs.pesq_emissaoaut_label_botao_limpar}"  action="#{vincMsgHisCompOpConBean.limpar}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>	
					<br:brCommandButton id="btnDetalhar" disabled="#{empty vincMsgHisCompOpConBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px;" value="#{msgs.conManterVincMsgHistCompOpeContratada_botao_detalhar}" action="#{vincMsgHisCompOpConBean.detalhar}">				
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnIncluir" disabled="false" styleClass="bto1"  style="margin-right:5px;" value="#{msgs.conManterVincMsgHistCompOpeContratada_botao_incluir}" action="#{vincMsgHisCompOpConBean.incluir}" disabled="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio == null}">				
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnExcluir" disabled="#{empty vincMsgHisCompOpConBean.itemSelecionadoLista}" styleClass="bto1"  value="#{msgs.conManterVincMsgHistCompOpeContratada_botao_excluir}" action="#{vincMsgHisCompOpConBean.excluir}">				
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>		
		</br:brPanelGrid>
	</a4j:outputPanel>		
	
	<t:inputHidden value="#{vincMsgHisCompOpConBean.habilitaFiltroDeCliente}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>