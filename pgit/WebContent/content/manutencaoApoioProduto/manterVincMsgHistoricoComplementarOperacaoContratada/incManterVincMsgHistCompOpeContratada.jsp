<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>

<brArq:form id="manterVincMsgHistComplOperacContIncluirForm" name="manterVincMsgHistComplOperacContIncluirForm" >

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada_label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada_label_cpfcnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada_label_nomeRazao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterAssociacaoMsgComprovanteSalarial_title_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada_label_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada_label_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada_label_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada_label_descricaoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada_label_situacaoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	 <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada_label_argsPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>		    
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_segunda_linha_extrato}:"/>
				</br:brPanelGroup>				
				
				<br:brPanelGroup>
			    	<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" size="10" maxlength="5" id="codigoHistoricoComplementar" value="#{vincMsgHisCompOpConBean.codigoHistoricoComplementar}" onkeypress=" onlyNum();" style="margin-top:5px;">  
				    	<brArq:commonsValidator type="integer" arg="#{msgs.label_codigo_segunda_linha_extrato}" server="false" client="true" />
				    </br:brInputText>
				</br:brPanelGroup>					
			</br:brPanelGrid>			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>		    
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada_label_servico}:"/>
				</br:brPanelGroup>				
				
				<br:brPanelGroup>
					<br:brSelectOneMenu id="tipoServico" value="#{vincMsgHisCompOpConBean.tipoServico}" style="margin-top:5px;margin-right:20px"> 
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterVincMsgHistCompOpeContratada_label_combo_selecione}"/>
						<f:selectItems value="#{vincMsgHisCompOpConBean.listaTipoServicoFiltro}"/>
						<a4j:support event="onchange" reRender="modalidadeServico" action="#{vincMsgHisCompOpConBean.listarModalidadeServico}" />			
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>			
		</br:brPanelGroup>

		<br:brPanelGroup>
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>		    
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada_label_operacao}:"/>
				</br:brPanelGroup>				
				
				<br:brPanelGroup>
					<br:brSelectOneMenu id="modalidadeServico" value="#{vincMsgHisCompOpConBean.modalidadeServico}" disabled="#{vincMsgHisCompOpConBean.tipoServico == null || vincMsgHisCompOpConBean.tipoServico == 0}" style="margin-top:5px;" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterVincMsgHistCompOpeContratada_label_combo_selecione}"/>
						<f:selectItems value="#{vincMsgHisCompOpConBean.listaModalidadeServico}"/>			
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>			
		</br:brPanelGroup>
		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.incManterVincMsgHistCompOpeContratada_botao_limpar_campos}" action="#{vincMsgHisCompOpConBean.limparDadosIncluir}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar2"  styleClass="bto1" value="#{msgs.incManterVincMsgHistCompOpeContratada_botao_consultar2}" action="#{vincMsgHisCompOpConBean.carregarGridIncluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 		
		
			<app:scrollableDataTable id="dataTable" value="#{vincMsgHisCompOpConBean.listaGridIncluir}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{vincMsgHisCompOpConBean.itemSelecionadoListaInc}">
						<f:selectItems value="#{vincMsgHisCompOpConBean.listaPesquisaControleIncluir}"/>
						<a4j:support event="onclick" reRender="btnAvancar" />
					</t:selectOneRadio>
					<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="210px" styleClass="colTabRight" > 
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_codigo_segunda_linha_extrato}" style="text-align:center;width:210"/>
					</f:facet>
					<br:brOutputText value="#{result.cdMensagemLinhaExtrato}"/>
				 </app:scrollableColumn>
				 
				<app:scrollableColumn  width="150px" styleClass="colTabLeft" > 
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.incManterVincMsgHistCompOpeContratada_grid_restricao}" style="text-align:center;width:150" />
					</f:facet>
					<br:brOutputText value="#{result.restricaoDesc}"/>
				 </app:scrollableColumn>				 
				 
				<app:scrollableColumn  width="250px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.incManterVincMsgHistCompOpeContratada_grid_servico}" style="text-align:center;width:250"/>
					</f:facet>
					<br:brOutputText value="#{result.dsProdutoServicoOperacao}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="240px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.incManterVincMsgHistCompOpeContratada_grid_operacao}"  style="text-align:center;width:240"/>
					</f:facet>
					<br:brOutputText value="#{result.dsProdutoOperacaoRelacionado}"/>
				</app:scrollableColumn>
				 
			</app:scrollableDataTable>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{vincMsgHisCompOpConBean.pesquisar}" >
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.incManterVincMsgHistCompOpeContratada_botao_voltar}" action="#{vincMsgHisCompOpConBean.voltarConsultar}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton  id="btnAvancar" styleClass="bto1" disabled="#{empty vincMsgHisCompOpConBean.itemSelecionadoListaInc}" value="#{msgs.incManterVincMsgHistCompOpeContratada_botao_avancar}" action="#{vincMsgHisCompOpConBean.avancarIncluir}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>