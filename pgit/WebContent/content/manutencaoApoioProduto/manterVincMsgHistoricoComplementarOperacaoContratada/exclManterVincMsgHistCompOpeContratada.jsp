<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterVincMsgHistoricoComplemeOperContInclConfirForm" name="manterVincMsgHistoricoComplemeOperContInclConfirForm" >
<h:inputHidden id="hiddenConfirma" value="#{vincMsgHisCompOpConBean.hiddenConfirma}"/>

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.exclManterVincMsgHistCompOpeContratada_label_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.exclManterVincMsgHistCompOpeContratada_label_cpfcnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.exclManterVincMsgHistCompOpeContratada_label_nomeRazao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.exclManterVincMsgHistCompOpeContratada_label_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.exclManterVincMsgHistCompOpeContratada_label_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.exclManterVincMsgHistCompOpeContratada_label_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.exclManterVincMsgHistCompOpeContratada_label_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.exclManterVincMsgHistCompOpeContratada_label_descricaoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.exclManterVincMsgHistCompOpeContratada_label_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada2_label_historico}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>   
	   
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada2_label_tipo_lancamento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{vincMsgHisCompOpConBean.detalheVinMsgHistComOpSaidaDTO.cdTipoMensagemExtrato == 1}" value="#{msgs.detManterVincMsgHistCompOpeContratada_label_debito}" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{vincMsgHisCompOpConBean.detalheVinMsgHistComOpSaidaDTO.cdTipoMensagemExtrato == 2}" value="#{msgs.detManterVincMsgHistCompOpeContratada_label_credito}" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{vincMsgHisCompOpConBean.detalheVinMsgHistComOpSaidaDTO.cdTipoMensagemExtrato == 3}" value="#{msgs.detManterVincMsgHistCompOpeContratada_label_debito_credito}" />						
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada2_label_restricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{vincMsgHisCompOpConBean.detalheVinMsgHistComOpSaidaDTO.cdIndicadorRestricaoContrato == 1}" value="#{msgs.detManterVincMsgHistCompOpeContratada_label_sim}" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{vincMsgHisCompOpConBean.detalheVinMsgHistComOpSaidaDTO.cdIndicadorRestricaoContrato == 2}" value="#{msgs.detManterVincMsgHistCompOpeContratada_label_nao}" />						
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_segunda_linha_extrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.detalheVinMsgHistComOpSaidaDTO.cdMensagemLinhaExtrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada2_label_lancamentoCreditoArgPes}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>   	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_segunda_linha_extrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.detalheVinMsgHistComOpSaidaDTO.dsMensagemLinhaCredito}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada2_label_lancamentoDebitoArgPes}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>   	

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_segunda_linha_extrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.detalheVinMsgHistComOpSaidaDTO.dsMensagemLinhaDebito}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada2_label_tipo_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.detalheVinMsgHistComOpSaidaDTO.dsProdutoServicoOperacao}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterVincMsgHistCompOpeContratada2_label_modalidade_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgHisCompOpConBean.detalheVinMsgHistComOpSaidaDTO.dsProdutoOperacaoRelacionado}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

 	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.exclManterVincMsgHistCompOpeContratada_botao_voltar}" action="#{vincMsgHisCompOpConBean.voltarConsultar}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton  id="btnAvancar" styleClass="bto1" value="#{msgs.exclManterVincMsgHistCompOpeContratada_botao_confirmar}" action="#{vincMsgHisCompOpConBean.confirmarExclusao}" onclick="javascript:confirmar(document.forms[1],'#{msgs.exclManterVincMsgHistCompOpeContratada_confirma_exclusao}')">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
