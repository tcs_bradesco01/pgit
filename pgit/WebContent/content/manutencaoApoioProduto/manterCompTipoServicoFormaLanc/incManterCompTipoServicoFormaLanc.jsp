<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incManterCompTipoServicoFormaLancForm" name="incManterCompTipoServicoFormaLancForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" style="margin-left:5px">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >    
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_servico_cnab}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>		
					<br:brSelectOneMenu id="cboTipoServicoCnab" value="#{manterCompTipoServicoFormaLancBean.tipoServicoCnab}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterCompTipoServicoFormaLancBean.listaTipoServicoCnab}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>									
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_forma_lacamento_cnab}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>		
					<br:brSelectOneMenu id="cboFormaLancCnab" value="#{manterCompTipoServicoFormaLancBean.formaLancCnab}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterCompTipoServicoFormaLancBean.listaFormaLancCnab}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>	
					 					
		</br:brPanelGroup> 
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >    
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup style="margin-right:20px">		
					<br:brSelectOneMenu id="cboTipoServico" value="#{manterCompTipoServicoFormaLancBean.tipoServico}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterCompTipoServicoFormaLancBean.listaTipoServico}"/>
						<a4j:support oncomplete="javascript:foco(this);"  status="statusAguarde" event="onchange" reRender="cboModalidadeServico" action="#{manterCompTipoServicoFormaLancBean.listarComboListarModalidadesInc}"/>						
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>	
										
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_modalidade_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>		
					<br:brSelectOneMenu id="cboModalidadeServico" value="#{manterCompTipoServicoFormaLancBean.modalidadeServico}" disabled="#{manterCompTipoServicoFormaLancBean.tipoServico == 0 || manterCompTipoServicoFormaLancBean.tipoServico == null}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterCompTipoServicoFormaLancBean.listaModalidadeServicoInc}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>	
										
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >    
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_forma_liquidacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>		
					<br:brSelectOneMenu id="cboFormaLiquidacao" value="#{manterCompTipoServicoFormaLancBean.formaLiquidacao}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterCompTipoServicoFormaLancBean.listaFormaLiquidacao}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>	
										
		</br:brPanelGroup>	
	</br:brPanelGrid>	

	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{manterCompTipoServicoFormaLancBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btnAvancar"  styleClass="bto1" value="#{msgs.btn_avancar}" action="#{manterCompTipoServicoFormaLancBean.avancarIncluir}"
				onclick="javascript:desbloquearTela(); return validaCamposIncluir(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}', 
					'#{msgs.label_tipo_servico_cnab}',
					'#{msgs.label_forma_lacamento_cnab}',
					'#{msgs.label_tipo_servico}',
					'#{msgs.label_modalidade_servico}',
					'#{msgs.label_forma_liquidacao}' );">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>