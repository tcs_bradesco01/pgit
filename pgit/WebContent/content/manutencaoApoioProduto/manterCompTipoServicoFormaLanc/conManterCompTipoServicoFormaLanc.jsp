<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterCompTipoServicoFormaLancForm" name="conManterCompTipoServicoFormaLancForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" style="margin-left:5px">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_argumentopesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
<a4j:outputPanel id="panelRadios" style="width: 100%" ajaxRendered="true">

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="sorRadio" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterCompTipoServicoFormaLancBean.rdoPesquisa}" disabled="#{!manterCompTipoServicoFormaLancBean.habilitaArgumentosPesquisa}">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />	
				<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="panelRadios" action="#{manterCompTipoServicoFormaLancBean.limparRadioPesquisa}"/>
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sorRadio" index="0" />
		</br:brPanelGroup>
		
		<br:brPanelGroup>
		    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >    
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>								
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_servico_cnab}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
				
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup style="margin-right:20px;">		
							<br:brSelectOneMenu id="cboTipoServicoCnab" value="#{manterCompTipoServicoFormaLancBean.tipoServicoCnabFiltro}" disabled="#{manterCompTipoServicoFormaLancBean.rdoPesquisa == null || manterCompTipoServicoFormaLancBean.rdoPesquisa != '0'  ||!manterCompTipoServicoFormaLancBean.habilitaArgumentosPesquisa}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{manterCompTipoServicoFormaLancBean.listaTipoServicoCnab}"/>
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>	
												
				</br:brPanelGroup>	
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_forma_lacamento_cnab}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
				
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>		
							<br:brSelectOneMenu id="cboFormaLancCnab" value="#{manterCompTipoServicoFormaLancBean.formaLancCnabFiltro}" disabled="#{manterCompTipoServicoFormaLancBean.rdoPesquisa == null || manterCompTipoServicoFormaLancBean.rdoPesquisa != '0'  ||!manterCompTipoServicoFormaLancBean.habilitaArgumentosPesquisa}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{manterCompTipoServicoFormaLancBean.listaFormaLancCnab}"/>
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>											
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sorRadio" index="1" />
		</br:brPanelGroup>
		
		<br:brPanelGroup>
		    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >    
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>								
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_servico}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
				
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup style="margin-right:20px">		
							<br:brSelectOneMenu id="cboTipoServico" value="#{manterCompTipoServicoFormaLancBean.tipoServicoFiltro}" disabled="#{manterCompTipoServicoFormaLancBean.rdoPesquisa == null || manterCompTipoServicoFormaLancBean.rdoPesquisa != '1'  ||!manterCompTipoServicoFormaLancBean.habilitaArgumentosPesquisa}"> 
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{manterCompTipoServicoFormaLancBean.listaTipoServico}"/>
								<a4j:support oncomplete="javascript:foco(this);"  status="statusAguarde" event="onchange" reRender="cboModalidadeServico" action="#{manterCompTipoServicoFormaLancBean.listarComboListarModalidades}"/>						
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>										
				</br:brPanelGroup>	
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_modalidade_servico}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
				
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>		
							<br:brSelectOneMenu id="cboModalidadeServico" value="#{manterCompTipoServicoFormaLancBean.modalidadeServicoFiltro}" disabled="#{manterCompTipoServicoFormaLancBean.rdoPesquisa == null || manterCompTipoServicoFormaLancBean.rdoPesquisa != '1'  || manterCompTipoServicoFormaLancBean.tipoServicoFiltro == 0 || manterCompTipoServicoFormaLancBean.tipoServicoFiltro == null || !manterCompTipoServicoFormaLancBean.habilitaArgumentosPesquisa}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{manterCompTipoServicoFormaLancBean.listaModalidadeServico}"/>
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>	
												
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>	
			<t:radio for="sorRadio" index="2" />
		</br:brPanelGroup>
		
		<br:brPanelGroup>
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >    
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_forma_liquidacao}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
				
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>		
							<br:brSelectOneMenu id="cboFormaLiquidacao" value="#{manterCompTipoServicoFormaLancBean.formaLiquidacaoFiltro}" disabled="#{manterCompTipoServicoFormaLancBean.rdoPesquisa == null || manterCompTipoServicoFormaLancBean.rdoPesquisa != '2'  || !manterCompTipoServicoFormaLancBean.habilitaArgumentosPesquisa}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{manterCompTipoServicoFormaLancBean.listaFormaLiquidacao}"/>
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>	
												
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
 
</a4j:outputPanel>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.btn_limpar_campos}" action="#{manterCompTipoServicoFormaLancBean.limparCampos}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" style="margin-left:5px" value="#{msgs.btn_consultar}" action="#{manterCompTipoServicoFormaLancBean.consultar}" 
				onclick="javascript:desbloquearTela(); return validaCamposConsulta(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}',
					'#{msgs.label_tipo_servico_cnab}',
					'#{msgs.label_forma_lacamento_cnab}',
					'#{msgs.label_tipo_servico}',
					'#{msgs.label_modalidade_servico}',
					'#{msgs.label_forma_liquidacao}' );">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 	 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0"  >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px;  height:170">	
			<app:scrollableDataTable id="dataTable" value="#{manterCompTipoServicoFormaLancBean.listaGridConsultar}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
			
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>		
					<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterCompTipoServicoFormaLancBean.itemGridSelecionado}">
						<f:selectItems value="#{manterCompTipoServicoFormaLancBean.listaGridControle}"/>
						<a4j:support event="onclick" reRender="btnDetalhar, btnExcluir" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>			

				<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_tipo_servico_cnab}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoServicoCnab}"  styleClass="tableFontStyle" />
				</app:scrollableColumn>
	
				<app:scrollableColumn  width="250px" styleClass="colTabLeft">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_forma_lacamento_cnab}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsFormaLancamentoCnab}"  styleClass="tableFontStyle" />
				</app:scrollableColumn>
	
				<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_tipo_servico}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsProdutoServicoOperacao}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_modalidade_servico}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsProdutoServicoRelacionado}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
	
				<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_forma_liquidacao}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsFormaLiquidacao}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
	
			</app:scrollableDataTable>				 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterCompTipoServicoFormaLancBean.pesquisar}" > 
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" style="text-align:right" cellpadding="0" cellspacing="0" width="100%">					
		<br:brPanelGroup>
			<br:brCommandButton disabled="#{empty manterCompTipoServicoFormaLancBean.itemGridSelecionado}" style="margin-right:5px" id="btnDetalhar" styleClass="bto1" value="#{msgs.btn_detalhar}" action="#{manterCompTipoServicoFormaLancBean.detalhar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>

			<br:brCommandButton disabled="false" id="btnIncluir" styleClass="bto1" style="margin-right:5px"  value="#{msgs.btn_incluir}"  action="#{manterCompTipoServicoFormaLancBean.incluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>

			<br:brCommandButton disabled="#{empty manterCompTipoServicoFormaLancBean.itemGridSelecionado}" id="btnExcluir"  styleClass="bto1" value="#{msgs.btn_excluir}" action="#{manterCompTipoServicoFormaLancBean.excluir}" >							
					<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>