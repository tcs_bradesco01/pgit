<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="excManterMensagemLayoutForm" name="excManterMensagemLayoutForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterMensagemLayoutBean.dsTipoLayoutArquivo}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo_da_mensagem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterMensagemLayoutBean.codigoMensagem}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_mensagem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_mensagem_erro}" rendered="#{manterMensagemLayoutBean.cdTipoMensagem == 1}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_mensagem_informativa}" rendered="#{manterMensagemLayoutBean.cdTipoMensagem == 2}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nivel_mensagem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_header_arquivo}" rendered="#{manterMensagemLayoutBean.cdNivelMensagem == 1}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_header_lote}" rendered="#{manterMensagemLayoutBean.cdNivelMensagem == 2}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_detalhe_arquivo}" rendered="#{manterMensagemLayoutBean.cdNivelMensagem == 3}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_trailer_lote}" rendered="#{manterMensagemLayoutBean.cdNivelMensagem == 4}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_trailer_arquivo}" rendered="#{manterMensagemLayoutBean.cdNivelMensagem == 5}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_posicao_inicial_campo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterMensagemLayoutBean.posicaoInicialCampo}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_posicao_final_campo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterMensagemLayoutBean.posicaoFinalCampo}"/>
		</br:brPanelGroup>						
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_descricao_mensagem_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brInputTextarea rows="7" disabled="true" cols="100" value="#{manterMensagemLayoutBean.descricaoMensagemCliente}">
				</br:brInputTextarea>	
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{manterMensagemLayoutBean.voltarPesquisar}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton  id="btnConfirmar" style="margin-left:5px" styleClass="bto1" value="#{msgs.btn_confirmar}" action="#{manterMensagemLayoutBean.confirmarExcluir}" onclick="javascript: if (!confirm('Confirma Exclus�o?')) { desbloquearTela(); return false; }">							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
