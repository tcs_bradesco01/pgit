<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="conManterMensagemLayoutForm" name="conManterMensagemLayoutForm" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid> 

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="cboTipoLayoutArquivo" value="#{manterMensagemLayoutBean.cdTipoLayoutArquivoFiltro}" disabled="#{manterMensagemLayoutBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterMensagemLayoutBean.listaTipoLayoutArquivoFiltro}"/>		
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_da_mensagem}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				<br:brPanelGroup>
					<br:brInputText disabled="#{manterMensagemLayoutBean.btoAcionado}" styleClass="HtmlInputTextBradesco" value="#{manterMensagemLayoutBean.codigoMensagemFiltro}" size="9" maxlength="5" id="txtCodigoMensagem">
					</br:brInputText>	
				</br:brPanelGroup>						
			</br:brPanelGrid>										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	 
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btnLimparCampos" style="margin-left:5px"   styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{manterMensagemLayoutBean.limparCampos}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" style="margin-left:5px" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{manterMensagemLayoutBean.consultar}" disabled="#{manterMensagemLayoutBean.btoAcionado}">							
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
		</br:brPanelGroup> 
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim>  
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">		
 
			<app:scrollableDataTable id="dataTable" value="#{manterMensagemLayoutBean.listaGrid}" var="varResult" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false"
						value="#{manterMensagemLayoutBean.itemSelecionadoLista}" >
						<f:selectItems value="#{manterMensagemLayoutBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="btnDetalhar, btnAlterar, btnExcluir" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_tipo_layout_arquivo}" style="text-align:center;width:200px" />
					</f:facet>
					<br:brOutputText value="#{varResult.tipoLayoutArquivoFormatado}"/>
				</app:scrollableColumn>  
				
				<app:scrollableColumn styleClass="colTabCenter" width="150px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_codigo_da_mensagem}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{varResult.cdMensagemArquivoRetorno}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="250px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_mensagem}" style="text-align:center;width:250px" />
					</f:facet>
					<br:brOutputText value="#{varResult.dsMensagemLayoutRetorno}"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_tipo_mensagem}" style="text-align:center;width:200px" />
					</f:facet>
					<br:brOutputText value="#{varResult.cdTipoMensagemRetorno} - #{varResult.dsTipoMansagemRetorno}"/>
				</app:scrollableColumn>	
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterMensagemLayoutBean.pesquisar}"  > 
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1" 
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>	 
		</br:brPanelGroup>
	</br:brPanelGrid>
 
	<f:verbatim><hr class="lin"> </f:verbatim>
	
 
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton  id="btnDetalhar" style="margin-left:5px" disabled="#{empty manterMensagemLayoutBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.btn_detalhar}" action="#{manterMensagemLayoutBean.detalhar}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" style="margin-left:5px" styleClass="bto1" value="#{msgs.btn_incluir}" action="#{manterMensagemLayoutBean.incluir}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
			<br:brCommandButton id="btnAlterar" style="margin-left:5px" disabled="#{empty manterMensagemLayoutBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.btn_alterar}" action="#{manterMensagemLayoutBean.alterar}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnExcluir" style="margin-left:5px" disabled="#{empty manterMensagemLayoutBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.btn_excluir}" action="#{manterMensagemLayoutBean.excluir}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>