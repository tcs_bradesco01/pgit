<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detManterSolicitacaoPagamentosForm" name="detManterSolicitacaoPagamentosForm" >

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolicitacaoRelPgto_servico}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.dsTipoRelatorio}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolicitacaoRelPgto_NumContrato}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.nrSequenciaContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
		
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolicitacaoRelPgto_Contrato_ServicoRela}:"/>
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="2">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_negocio_servico}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.dsProdutoServicoOperacao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_modalidade_servico}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.dsProdutoOperRelacionado}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolicitacaoRelPgto_participante}: "/>
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="2">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_CpjCnpj}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.cpfCnpjFormatado}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_nomeRaZAO}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.dsNomeParticipante}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_agenciaDebito}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.codAgenciaDesc}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_contaDebito}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.contaDigito}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolicitacaoRelPgto_dtPgto}: "/>
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="2">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_periodoDe}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.dataInicioMov}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_Ate}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.dataFimMov}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGrid columns="2">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_dtHoraInc}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.dataHoraInclusao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_user}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.cdUsuarioInclusao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_tipoCanal}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.dsCanalInclusao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_complemento}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.cdOperacaoCanalInclusao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGrid columns="2">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_dtHoraManut}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.dataHoraManut}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_user}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.cdUsuarioManutencao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_tipoCanal}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.dsCanalManutencao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterSolicitacaoRelPgto_complemento}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.saidaDetalhePagamento.cdOperacaoCanalManutencao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>								
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.btn_conManterSolicitacaoRelPgto_voltar}" action="#{manterSolicitacaoRelatorioPagamentosBean.voltarPesquisar}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
