<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incManterSolicitacaoPagamentos" name="incManterSolicitacaoPagamentos">
	<h:inputHidden id="hiddenTipoRelatorio" value="#{manterSolicitacaoRelatorioPagamentosBean.tipoRelatorio}" />
	<h:inputHidden id="hiddenObrigatoriedade" value="#{manterSolicitacaoRelatorioPagamentosBean.hiddenObrigatoriedade}" />
	<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_servico}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brSelectOneRadio id="tipoRelatorio" styleClass="HtmlSelectOneRadioBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.tipoRelatorio}">
					<f:selectItem itemLabel="#{msgs.incManterSolicitacaoRelContrato_radio_analitico}" itemValue="1" />
					<f:selectItem itemLabel="#{msgs.incManterSolicitacaoRelContrato_radio_estatístico}" itemValue="2" />
					<a4j:support event="onclick" reRender="hiddenTipoRelatorio" />
				</br:brSelectOneRadio>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_data_pagamento}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<a4j:outputPanel id="dataPagamento" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGroup>
						<app:calendar oninputkeydown="javascript: cleanClipboard();"
							oninputkeypress="onlyNum();"
							oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialPagamento');"
							id="dataInicialPagamento"
							value="#{manterSolicitacaoRelatorioPagamentosBean.dataInicialPagamentoFiltro}">
						</app:calendar>
					</br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}" />
					<br:brPanelGroup>
						<app:calendar oninputkeydown="javascript: cleanClipboard();"
							oninputkeypress="onlyNum();"
							oninputchange="recuperarDataCalendario(document.forms[1],'dataFinalPagamento');"
							id="dataFinalPagamento"
							value="#{manterSolicitacaoRelatorioPagamentosBean.dataFinalPagamentoFiltro}">
						</app:calendar>
					</br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.title_conManterSolicitacaoRelPgto_criteriosSelecao}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" id="gridContrato">
			<h:selectBooleanCheckbox id="chkContrato"
				value="#{manterSolicitacaoRelatorioPagamentosBean.chkContrato}"
				style="margin-right:5px">
				<a4j:support oncomplete="javascript:foco(this);"
					status="statusAguarde" event="onclick"
					reRender="empresaGestora,tipoContrato,numero,gridContrato,btnAvancar,contrato2linha,contrato1linha"
					action="#{manterSolicitacaoRelatorioPagamentosBean.limparFiltroContrato}" />
			</h:selectBooleanCheckbox>
		
		
			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
	
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="empresaGestora"
								disabled="#{manterSolicitacaoRelatorioPagamentosBean.disableArgumentosConsulta || !manterSolicitacaoRelatorioPagamentosBean.chkContrato}"
								value="#{manterSolicitacaoRelatorioPagamentosBean.empresaGestoraFiltro}"
								styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
								<f:selectItems value="#{manterSolicitacaoRelatorioPagamentosBean.listaEmpresaGestora}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
	
				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>
	
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
	
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="tipoContrato"
								disabled="#{manterSolicitacaoRelatorioPagamentosBean.disableArgumentosConsulta || !manterSolicitacaoRelatorioPagamentosBean.chkContrato}"
								value="#{manterSolicitacaoRelatorioPagamentosBean.tipoContratoFiltro}"
								styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
								<f:selectItems value="#{manterSolicitacaoRelatorioPagamentosBean.listaTipoContrato}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
	
				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>
	
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
	
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText id="numero" styleClass="HtmlInputTextBradesco"
								onkeypress="onlyNum();"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								value="#{manterSolicitacaoRelatorioPagamentosBean.numeroFiltro}"
								size="12" maxlength="10"
								disabled="#{manterSolicitacaoRelatorioPagamentosBean.disableArgumentosConsulta || !manterSolicitacaoRelatorioPagamentosBean.chkContrato}">
							</br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;">
			<br:brCommandButton id="btoConsultarContrato" styleClass="bto1"
				value="#{msgs.consultarPagamentosIndividual_btn_consultar_contrato}"
				action="#{manterSolicitacaoRelatorioPagamentosBean.consultarContrato}"
				style="cursor:hand;"
				onmouseover="javascript:alteraBotao('visualizacao', this.id);"
				onmouseout="javascript:alteraBotao('normal', this.id);"
				onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.consultarPagamentosIndividual_tipo_contrato}','#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');"
				actionListener="#{manterSolicitacaoRelatorioPagamentosBean.limparArgsListaAposPesquisaClienteContrato}">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGrid>


		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" id="contrato1linha">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.empresaGestoraDescContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" id="contrato2linha">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.numeroDescContrato}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.descricaoContratoDescContrato}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioPagamentosBean.situacaoDescContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<h:selectBooleanCheckbox id="chkParticipanteContrato"
				value="#{manterSolicitacaoRelatorioPagamentosBean.chkParticipanteContrato}"
				style="margin-right:5px">
				<a4j:support oncomplete="javascript:foco(this);"
					status="statusAguarde" event="onclick"
					reRender="nomePartDescricao,cpfCnpjDescricao,btoBuscar,cpfCnpjParticipante,txtCodigoParticipante,txtNomeParticipante"
					action="#{manterSolicitacaoRelatorioPagamentosBean.limparParticipanteContrato}" />
			</h:selectBooleanCheckbox>

			<br:brPanelGroup id="cpfCnpjParticipante">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf_cnpj_participante}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brInputText styleClass="HtmlInputTextBradesco"
					style="margin-right:5px"
					value="#{manterSolicitacaoRelatorioPagamentosBean.corpoCpfCnpjParticipanteFiltro}"
					converter="javax.faces.Long" size="12" maxlength="9"
					onkeypress="onlyNum();" id="txtCorpoCpfCnpjParticipanteFiltro"
					disabled="#{!manterSolicitacaoRelatorioPagamentosBean.chkParticipanteContrato || manterSolicitacaoRelatorioPagamentosBean.cpfCnpjParticipanteFiltro != ''}"
					onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
				</br:brInputText>
				<br:brInputText styleClass="HtmlInputTextBradesco"
					style="margin-right:5px"
					value="#{manterSolicitacaoRelatorioPagamentosBean.filialCpfCnpjParticipanteFiltro}"
					size="6" maxlength="4" converter="javax.faces.Integer"
					onkeypress="onlyNum();" id="txtFilialCpfCnpjParticipanteFiltro"
					disabled="#{!manterSolicitacaoRelatorioPagamentosBean.chkParticipanteContrato || manterSolicitacaoRelatorioPagamentosBean.cpfCnpjParticipanteFiltro != ''}"
					onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">

				</br:brInputText>
				<br:brInputText styleClass="HtmlInputTextBradesco"
					style="margin-right:5px"
					value="#{manterSolicitacaoRelatorioPagamentosBean.controleCpfCnpjParticipanteFiltro}"
					size="4" maxlength="2" converter="javax.faces.Integer"
					onkeypress="onlyNum();" id="txtControleCpfCnpjParticipanteFiltro"
					disabled="#{!manterSolicitacaoRelatorioPagamentosBean.chkParticipanteContrato  || manterSolicitacaoRelatorioPagamentosBean.cpfCnpjParticipanteFiltro != ''}"
					onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
				</br:brInputText>

				<h:inputHidden id="txtCodigoParticipante"
					value="#{manterSolicitacaoRelatorioPagamentosBean.codigoParticipanteFiltro}" />
				<br:brCommandButton id="btoBuscar" styleClass="bto1"
					value="#{msgs.consultarPagamentosIndividual_btn_buscar}"
					action="#{manterSolicitacaoRelatorioPagamentosBean.buscarParticipante}"
					style="cursor:hand;margin-left:20px"
					onmouseover="javascript:alteraBotao('visualizacao', this.id);"
					onmouseout="javascript:alteraBotao('normal', this.id);"
					onclick="javascript:desbloquearTela(); return validaCampoParticipante(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_cpfCnpj}','#{msgs.label_base}','#{msgs.label_filial}','#{msgs.label_controle}','#{msgs.label_deve_ser_diferente_de_zeros}');"
					disabled="#{!manterSolicitacaoRelatorioPagamentosBean.chkParticipanteContrato}">
					<brArq:submitCheckClient />
				</br:brCommandButton>

			</br:brPanelGroup>

		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>


		<br:brPanelGrid columns="2" style="margin-left:20px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_cpfCnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					id="cpfCnpjDescricao"
					value="#{manterSolicitacaoRelatorioPagamentosBean.cpfCnpjParticipanteFiltro}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_nomeRazaoSocial}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					id="nomePartDescricao"
					value="#{manterSolicitacaoRelatorioPagamentosBean.nomeParticipanteFiltro}" />
			</br:brPanelGroup>

		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<h:selectBooleanCheckbox id="chkContaDebito"
				value="#{manterSolicitacaoRelatorioPagamentosBean.chkContaDebito}"
				style="margin-right:5px">
				<a4j:support oncomplete="javascript:foco(this);"
					status="statusAguarde" event="onclick"
					reRender="panelBancoContaDebitoCheck,panelAgenciaContaDebitoCheck,panelContaDebitoCheck"
					action="#{manterSolicitacaoRelatorioPagamentosBean.limparContaDebito}" />
			</h:selectBooleanCheckbox>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.consultarPagamentosIndividual_conta_debito}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"
			style="margin-left:25px">
			<a4j:outputPanel id="panelBancoContaDebitoCheck" ajaxRendered="true">

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.consultarPagamentosIndividual_banco}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brInputText styleClass="HtmlInputTextBradesco"
					disabled="#{!manterSolicitacaoRelatorioPagamentosBean.chkContaDebito}"
					onkeypress="onlyNum();" converter="javax.faces.Integer"
					value="#{manterSolicitacaoRelatorioPagamentosBean.cdBancoContaDebito}"
					size="4" maxlength="3" id="txtBancoContaDebitoCheck"
					onkeyup="proximoCampo(3,'incManterSolicitacaoPagamentos','incManterSolicitacaoPagamentos:txtBancoContaDebitoCheck','incManterSolicitacaoPagamentos:txtAgenciaContaDebitoCheck');"
					onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			</a4j:outputPanel>

			<a4j:outputPanel id="panelAgenciaContaDebitoCheck"
				ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.consultarPagamentosIndividual_agencia}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brInputText styleClass="HtmlInputTextBradesco"
					disabled="#{!manterSolicitacaoRelatorioPagamentosBean.chkContaDebito}"
					onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
					onkeypress="onlyNum();" converter="javax.faces.Integer"
					value="#{manterSolicitacaoRelatorioPagamentosBean.cdAgenciaBancariaContaDebito}"
					size="6" maxlength="5" id="txtAgenciaContaDebitoCheck"
					onkeyup="proximoCampo(5,'incManterSolicitacaoPagamentos','incManterSolicitacaoPagamentos:txtAgenciaContaDebitoCheck','incManterSolicitacaoPagamentos:txtContaDebitoCheck');" />
			</a4j:outputPanel>

			<a4j:outputPanel id="panelContaDebitoCheck" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.consultarPagamentosIndividual_conta}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco"
							disabled="#{!manterSolicitacaoRelatorioPagamentosBean.chkContaDebito}"
							converter="javax.faces.Long" onkeypress="onlyNum();"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							value="#{manterSolicitacaoRelatorioPagamentosBean.cdContaBancariaContaDebito}"
							size="17" maxlength="13" id="txtContaDebitoCheck" />
					</br:brPanelGroup>
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" id="gridServicoModalidade">
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
					style="margin-bottom:40px">
					<h:selectBooleanCheckbox id="chkServicoModalidade"
						value="#{manterSolicitacaoRelatorioPagamentosBean.chkServicoModalidade}"
						style="margin-right:5px">
						<a4j:support oncomplete="javascript:foco(this);"
							status="statusAguarde" event="onclick"
							reRender="cboTipoServico,cboModalidade,gridContrato"
							action="#{manterSolicitacaoRelatorioPagamentosBean.controleServicoModalidade}" />
					</h:selectBooleanCheckbox>
				</br:brPanelGrid>
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.consultarPagamentosIndividual_tipo_servico}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoServico"
							value="#{manterSolicitacaoRelatorioPagamentosBean.tipoServicoFiltro}"
							styleClass="HtmlSelectOneMenuBradesco"
							disabled="#{!manterSolicitacaoRelatorioPagamentosBean.chkServicoModalidade}">
							<f:selectItem itemValue="0"
								itemLabel="#{msgs.label_combo_selecione}" />
							<f:selectItems
								value="#{manterSolicitacaoRelatorioPagamentosBean.listaTipoServicoFiltro}" />
							<a4j:support oncomplete="javascript:foco(this);"
								status="statusAguarde" event="onchange" reRender="cboModalidade,gridContrato,btnAvancar,panelBotoes"
								action="#{manterSolicitacaoRelatorioPagamentosBean.listarModalidadesPagto}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.consultarPagamentosIndividual_modalidade}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboModalidade"
							value="#{manterSolicitacaoRelatorioPagamentosBean.modalidadeFiltro}"
							styleClass="HtmlSelectOneMenuBradesco"
							disabled="#{manterSolicitacaoRelatorioPagamentosBean.tipoServicoFiltro == null || manterSolicitacaoRelatorioPagamentosBean.tipoServicoFiltro == 0 
				    	   || !manterSolicitacaoRelatorioPagamentosBean.chkServicoModalidade}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_todos}" />
							<f:selectItems
								value="#{manterSolicitacaoRelatorioPagamentosBean.listaModalidadeFiltro}" />
								
							<a4j:support action="#{manterSolicitacaoRelatorioPagamentosBean.teste}"
								 event="onclick" reRender="btnAvancar,panelBotoes" />
								
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>


		<f:verbatim>
			<hr class="lin">
		</f:verbatim>
		<a4j:outputPanel id="panelBotoes" style="width: 100%; "
			ajaxRendered="true">
			<br:brPanelGrid columns="3" width="100%" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup style="text-align:left;width:150px">
					<br:brCommandButton id="btnVoltar" styleClass="bto1"
						value="#{msgs.identificacaoClienteContratoAgendamento_btn_voltar}"
						action="#{manterSolicitacaoRelatorioPagamentosBean.voltarIncluir}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="width:450px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup style="text-align:right;width:150px">
					<br:brCommandButton id="btnLimpar"
						action="#{manterSolicitacaoRelatorioPagamentosBean.limparCamposIncluir}"
						styleClass="bto1" value="#{msgs.label_botao_limpar}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
					<br:brCommandButton id="btnAvancar"
						action="#{manterSolicitacaoRelatorioPagamentosBean.validarDetIncluir}"
						onclick="if(!validaPesquisaCpf()){desbloquearTela(); return false;}"
						disabled="#{!manterSolicitacaoRelatorioPagamentosBean.desabilitaBotaoAvancar}"
						style="margin-left:5px" styleClass="bto1"
						value="#{msgs.identificacaoClienteContratoAgendamento_btn_avancar}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>

	<br:brPanelGroup
		style="position:absolute; width: 100%; text-align: center; top:145">
		<a4j:status id="statusAguarde" onstart="bloquearTela()"
			onstop="desbloquearTela()" />
	</br:brPanelGroup>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>
