<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterSolicitacaoPagamentosForm" name="conManterSolicitacaoPagamentosForm" >

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolicitacaoRelPgto_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterSolicitacaoRelPgto_tipo_relatorio}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoRelatorio" value="#{manterSolicitacaoRelatorioPagamentosBean.filtroTipoRelatorio}" disabled="#{manterSolicitacaoRelatorioPagamentosBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conManterSolicitacaoRelPgto_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.conManterSolicitacaoRelPgto_analitico}"/>
				<f:selectItem itemValue="2" itemLabel="#{msgs.conManterSolicitacaoRelPgto_estatistico}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterSolicitacaoRelPgto_periodo_solicitacao}:"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
			
	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >	
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.label_de}:"/>
			<br:brPanelGroup rendered="#{!manterSolicitacaoRelatorioPagamentosBean.btoAcionado}" >
				<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioDe');" id="calendarioDe" value="#{manterSolicitacaoRelatorioPagamentosBean.filtroDataDe}" >
					<brArq:commonsValidator type="required" arg="#{msgs.conManterSolicitacaoRelPgto_periodo_inicial_solicitacao}" server="false" client="true"/>
				</app:calendar>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterSolicitacaoRelatorioPagamentosBean.btoAcionado}" >
				<app:calendar id="calendarioDeDes" value="#{manterSolicitacaoRelatorioPagamentosBean.filtroDataDe}" disabled="true">
				</app:calendar>
			</br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_ate}:"/>
			<br:brPanelGroup rendered="#{!manterSolicitacaoRelatorioPagamentosBean.btoAcionado}" >
				<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioAte');" id="calendarioAte" value="#{manterSolicitacaoRelatorioPagamentosBean.filtroDataAte}" >
					<brArq:commonsValidator type="required" arg="#{msgs.conManterSolicitacaoRelPgto_periodo_final_solicitacao}" server="false" client="true"/>
				</app:calendar>	
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{manterSolicitacaoRelatorioPagamentosBean.btoAcionado}" >
				<app:calendar id="calendarioAteDes" value="#{manterSolicitacaoRelatorioPagamentosBean.filtroDataAte}" disabled="true">
				</app:calendar>	
			</br:brPanelGroup>	
		</br:brPanelGroup>	
	</a4j:outputPanel>

	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conManterSolicitacaoRelPgto_btn_limpar_campos}" action="#{manterSolicitacaoRelatorioPagamentosBean.limparCampos}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conManterSolicitacaoRelPgto_btn_consultar}" disabled="#{manterSolicitacaoRelatorioPagamentosBean.listaGrid != null}" action="#{manterSolicitacaoRelatorioPagamentosBean.listarRelatorioPagamento}" onclick="javascript: return validateForm(document.forms[1]);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="width:750px" >	
			<app:scrollableDataTable id="dataTable" value="#{manterSolicitacaoRelatorioPagamentosBean.listaGrid}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%" height="170" >
				<app:scrollableColumn styleClass="colTabCenter" width="30px">
					<f:facet name="header">
				      	<br:brOutputText value=" "styleClass="tableFontStyle" style="width:25; text-align:center"   />
				    </f:facet>		
					<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{manterSolicitacaoRelatorioPagamentosBean.itemSelecionadoPgto}">
						<f:selectItems value="#{manterSolicitacaoRelatorioPagamentosBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
		    		<t:radio for="sor" index="#{parametroKey}" />
				</app:scrollableColumn>
				<app:scrollableColumn width="240px" styleClass="colTabCenter">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conManterSolicitacaoRelPgto_grid_tipo_relatorio}" style="width:100%;text-align:center" />
				    </f:facet>			
				    <br:brOutputText value="#{result.dsTipoRelatorio}" />    
				</app:scrollableColumn>
				<app:scrollableColumn width="240px"  styleClass="colTabCenter">
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conManterSolicitacaoRelPgto_grid_data_hora_solicitacao}"  style="width:100%;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dataHoraSolicitacao}" />
				</app:scrollableColumn>
				<app:scrollableColumn width="240px" styleClass="colTabCenter">
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conManterSolicitacaoRelPgto_grid_situacao}"  style="width:100%;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdSituacao}" />
				</app:scrollableColumn>
			</app:scrollableDataTable>	
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterSolicitacaoRelatorioPagamentosBean.paginarRelPagamento}">
			    <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterSolicitacaoRelatorioPagamentosBean.itemSelecionadoPgto}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterSolicitacaoRelPgto_btn_detalhar}" action="#{manterSolicitacaoRelatorioPagamentosBean.avancarDetalhar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir"  styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterSolicitacaoRelPgto_btn_incluir}" action="#{manterSolicitacaoRelatorioPagamentosBean.avancarIncluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled= "#{empty manterSolicitacaoRelatorioPagamentosBean.itemSelecionadoPgto}" styleClass="bto1"  value="#{msgs.conManterSolicitacaoRelPgto_btn_excluir}" action="#{manterSolicitacaoRelatorioPagamentosBean.avancarExcluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
