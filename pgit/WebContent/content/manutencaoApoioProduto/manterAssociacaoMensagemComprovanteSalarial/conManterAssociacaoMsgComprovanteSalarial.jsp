<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="pesqManterAssociacaoMensagemComprovanteSalarial" name="pesqManterAssociacaoMensagemComprovanteSalarial" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_title_filtros_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{manterAssociacaoMensagemComprovanteSalarialBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.bloqueaRadio}">  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
	</t:selectOneRadio>
	</a4j:region>
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(9,'pesqManterAssociacaoMensagemComprovanteSalarial','pesqManterAssociacaoMensagemComprovanteSalarial:txtCnpj','pesqManterAssociacaoMensagemComprovanteSalarial:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.itemFiltroSelecionado != '0' || manterAssociacaoMensagemComprovanteSalarialBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" />
			    <br:brInputText id="txtFilial" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(4,'pesqManterAssociacaoMensagemComprovanteSalarial','pesqManterAssociacaoMensagemComprovanteSalarial:txtFilial','pesqManterAssociacaoMensagemComprovanteSalarial:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.itemFiltroSelecionado != '0' || manterAssociacaoMensagemComprovanteSalarialBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" />
				<br:brInputText id="txtControle" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.itemFiltroSelecionado != '0' || manterAssociacaoMensagemComprovanteSalarialBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaClientePessoas.cdControleCnpj}" />
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.itemFiltroSelecionado != '1' || manterAssociacaoMensagemComprovanteSalarialBean.habilitaFiltroDeCliente }" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'pesqManterAssociacaoMensagemComprovanteSalarial','pesqManterAssociacaoMensagemComprovanteSalarial:txtCpf','pesqManterAssociacaoMensagemComprovanteSalarial:txtControleCpf');" />
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.itemFiltroSelecionado != '1' || manterAssociacaoMensagemComprovanteSalarialBean.habilitaFiltroDeCliente}" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_nome_razao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.itemFiltroSelecionado != '2' || manterAssociacaoMensagemComprovanteSalarialBean.habilitaFiltroDeCliente}" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  border="0">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.itemFiltroSelecionado != '3' || manterAssociacaoMensagemComprovanteSalarialBean.habilitaFiltroDeCliente}" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'pesqManterAssociacaoMensagemComprovanteSalarial','pesqManterAssociacaoMensagemComprovanteSalarial:txtBanco','pesqManterAssociacaoMensagemComprovanteSalarial:txtAgencia');"/>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.itemFiltroSelecionado != '3' || manterAssociacaoMensagemComprovanteSalarialBean.habilitaFiltroDeCliente}" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'pesqManterAssociacaoMensagemComprovanteSalarial','pesqManterAssociacaoMensagemComprovanteSalarial:txtAgencia','pesqManterAssociacaoMensagemComprovanteSalarial:txtConta');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.itemFiltroSelecionado != '3' || manterAssociacaoMensagemComprovanteSalarialBean.habilitaFiltroDeCliente}" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" />
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCamposCliente" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{manterAssociacaoMensagemComprovanteSalarialBean.limparDadosCliente}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty manterAssociacaoMensagemComprovanteSalarialBean.itemFiltroSelecionado || manterAssociacaoMensagemComprovanteSalarialBean.habilitaFiltroDeCliente}" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_btn_consultar_cliente}" action="#{manterAssociacaoMensagemComprovanteSalarialBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" 
	   		onclick="javascript:desbloquearTela(); 
	   		return validaCpoConsultaCliente('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conManterAssociacaoMsgComprovanteSalarial_cnpj}','#{msgs.conManterAssociacaoMsgComprovanteSalarial_cpf}', '#{msgs.conManterAssociacaoMsgComprovanteSalarial_nome_razao}',
	   		'#{msgs.conManterAssociacaoMsgComprovanteSalarial_banco}','#{msgs.conManterAssociacaoMsgComprovanteSalarial_agencia}','#{msgs.conManterAssociacaoMsgComprovanteSalarial_conta}');">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_title_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.cmpi0000_label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterAssociacaoMensagemComprovanteSalarialBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterAssociacaoMensagemComprovanteSalarialBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_title_filtros_contrato}:"/>
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_empresa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGroup style="width:100px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_tipo}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGroup style="width:152px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_numero}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>	
			<br:brSelectOneMenu id="selEmpresaGestoraContrada" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaContratosPessoas.cdPessoaJuridicaContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
				<f:selectItems  value="#{manterAssociacaoMensagemComprovanteSalarialBean.preencherListaEmpresaGestora}" />
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>			
			<br:brSelectOneMenu id="selTipoContrato" converter="inteiroConverter" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaContratosPessoas.cdTipoContratoNegocio}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
				<f:selectItems  value="#{manterAssociacaoMensagemComprovanteSalarialBean.preencherTipoContrato}" />
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>					
			<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Long" onkeypress="onlyNum();" value="#{manterAssociacaoMensagemComprovanteSalarialBean.entradaConsultarListaContratosPessoas.nrSeqContratoNegocio}" size="12" maxlength="10" id="txtNumeroContrato" >
			</br:brInputText>
		</br:brPanelGroup>		 	 
	 </br:brPanelGrid>
	  
	 <f:verbatim><hr class="lin"></f:verbatim>
	 
	 <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<br:brPanelGroup style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCamposContrato" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{manterAssociacaoMensagemComprovanteSalarialBean.limparDadosContrato}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
	   		<br:brCommandButton id="btoConsultarContrato" styleClass="bto1" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_btn_consultar_contrato}" action="#{manterAssociacaoMensagemComprovanteSalarialBean.consultarContratos}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); 
	   		return validaCpoContrato('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conManterAssociacaoMsgComprovanteSalarial_empresa}','#{msgs.conManterAssociacaoMsgComprovanteSalarial_tipo}','#{msgs.conManterAssociacaoMsgComprovanteSalarial_numero}', '#{manterAssociacaoMensagemComprovanteSalarialBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}');">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
     <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_title_contrato}:"/>
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_empresa}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterAssociacaoMensagemComprovanteSalarialBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_tipo}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterAssociacaoMensagemComprovanteSalarialBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup >			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_numero}:"  />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterAssociacaoMensagemComprovanteSalarialBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
				</br:brPanelGroup>
				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
				</br:brPanelGroup>	
				<br:brPanelGroup >
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_descricao_contrato}:"  />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterAssociacaoMensagemComprovanteSalarialBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
				</br:brPanelGroup>
				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
				</br:brPanelGroup>	
				<br:brPanelGroup >			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_situacao}:"  />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterAssociacaoMensagemComprovanteSalarialBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
				</br:brPanelGroup>
			 </br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_title_mensagem_comprovante_salarial_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_mensagem}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>			
					<br:brSelectOneMenu id="selTipoMsg" converter="inteiroConverter" value="#{manterAssociacaoMensagemComprovanteSalarialBean.codigoTipoMensagemFiltro}" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.desabilataFiltro || manterAssociacaoMensagemComprovanteSalarialBean.btoAcionado}" styleClass="HtmlSelectOneMenuBradesco" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterAssociacaoMsgComprovanteSalarial_selecione}"/>
						<f:selectItems  value="#{manterAssociacaoMensagemComprovanteSalarialBean.preencherTipoMensagem}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_btn_limpar_campos}" action="#{manterAssociacaoMensagemComprovanteSalarialBean.limparArgumentosPesquisa}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_btn_consultar}" action="#{manterAssociacaoMensagemComprovanteSalarialBean.carregaLista}"  disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.desabilataFiltro || manterAssociacaoMensagemComprovanteSalarialBean.btoAcionado}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170px">	
 
			<app:scrollableDataTable id="dataTable" value="#{manterAssociacaoMensagemComprovanteSalarialBean.listaGrid}" var="varResult" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false"
						value="#{manterAssociacaoMensagemComprovanteSalarialBean.itemSelecionadoListaGrid}" >
						<f:selectItems value="#{manterAssociacaoMensagemComprovanteSalarialBean.listaGridControle}"/>
						<a4j:support event="onclick" reRender="btnDetalhar, btnExcluir" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="120px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_grid_codigo}" style="text-align:center;width:120" />
					</f:facet>
					<br:brOutputText value="#{varResult.codMensagemSalarial}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="450px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_grid_descricao}" style="text-align:center;width:450" />
					</f:facet>
					<br:brOutputText value="#{varResult.dsMensagemSalarial}"/>
				</app:scrollableColumn>			
				
			</app:scrollableDataTable>
						
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterAssociacaoMensagemComprovanteSalarialBean.pesquisar}"  > 
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1" 
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>	 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >	
			<br:brCommandButton id="btnLimpar" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.desabilataFiltro}"  styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_btn_limpar}" action="#{manterAssociacaoMensagemComprovanteSalarialBean.limpaCampos}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnDetalhar" disabled="#{empty manterAssociacaoMensagemComprovanteSalarialBean.itemSelecionadoListaGrid}"  styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_btn_detalhar}" action="#{manterAssociacaoMensagemComprovanteSalarialBean.detalhar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="bto1" style="margin-right:5px" disabled="#{manterAssociacaoMensagemComprovanteSalarialBean.desabilataFiltro}" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_btn_incluir}" action="#{manterAssociacaoMensagemComprovanteSalarialBean.incluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" disabled="#{empty manterAssociacaoMensagemComprovanteSalarialBean.itemSelecionadoListaGrid}" styleClass="bto1" value="#{msgs.conManterAssociacaoMsgComprovanteSalarial_btn_excluir}"  action="#{manterAssociacaoMensagemComprovanteSalarialBean.excluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>

	<t:inputHidden value="#{manterAssociacaoMensagemComprovanteSalarialBean.bloqueaRadio}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
	