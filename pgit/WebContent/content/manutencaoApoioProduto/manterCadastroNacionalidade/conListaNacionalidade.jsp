<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>

<brArq:form id="conListaNacionalidadeForm" name="conListaNacionalidadeForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.manterCadastroNacionalidade_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>						
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brSelectOneRadio id="radioFiltro" styleClass="HtmlSelectOneRadioBradesco" value="#{nacionalidadeBean.filtroIdentificacao}">
				<f:selectItem itemLabel="#{msgs.manterCadastroNacionalidade_dados_pessoa}" itemValue="1" />  
	            <f:selectItem itemLabel="#{msgs.manterCadastroNacionalidade_numero_contrato}" itemValue="2" />  
				<a4j:support event="onclick" reRender="panelBotaoConsultar,txtNumeroContrato" action="#{nacionalidadeBean.limpaContrato}"/>
	    	</br:brSelectOneRadio>
	    </br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="width:155px;"> </br:brPanelGroup> 
		<br:brPanelGroup>	
    		<br:brInputText size="15" maxlength="9" id="txtNumeroContrato" value="#{nacionalidadeBean.filtroNumeroContrato}" disabled="#{nacionalidadeBean.filtroIdentificacao != '2'}"> 
	    	</br:brInputText>	
		</br:brPanelGroup>					 
	</br:brPanelGrid>
			
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotaoConsultar" style="width: 100%; text-align: right" ajaxRendered="true">			
		<br:brPanelGrid columns="2" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" disabled="false" styleClass="HtmlCommandButtonBradesco" value="#{msgs.manterCadastroNacionalidade_btn_limpar_campos}" action="#{nacionalidadeBean.limparCampos}" style="margin-right:5px" >	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" disabled="#{empty nacionalidadeBean.filtroIdentificacao}" styleClass="HtmlCommandButtonBradesco" value="#{msgs.manterCadastroNacionalidade_btn_consultar}" action="#{nacionalidadeBean.consultarCliente}" style="margin-right:5px" >	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.manterCadastroNacionalidade_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterCadastroNacionalidade_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{nacionalidadeBean.identificacaoClienteBean.cpf}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterCadastroNacionalidade_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{nacionalidadeBean.identificacaoClienteBean.numero}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterCadastroNacionalidade_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{nacionalidadeBean.identificacaoClienteBean.situacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterCadastroNacionalidade_grupo_economico}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{nacionalidadeBean.identificacaoClienteBean.grupoEconomico}"/>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterCadastroNacionalidade_segmento}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{nacionalidadeBean.identificacaoClienteBean.segmento}"/>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterCadastroNacionalidade_sub_segmento}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{nacionalidadeBean.identificacaoClienteBean.subSegmento}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto;width:750px">	
		<app:scrollableDataTable id="dataTable" value="#{nacionalidadeBean.listaGridPesquisa}" var="result" 
			rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">			
	
			<app:scrollableColumn styleClass="colTabCenter" width="30px">
				<f:facet name="header">
			     <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>
				<t:selectOneRadio  id="sor" styleClass="HtmlSelectOneRadioBradesco" 
					layout="spread" forceId="true" forceIdIndex="false" 
					value="#{nacionalidadeBean.itemSelecionadoLista}">
					<f:selectItems value="#{nacionalidadeBean.listaControleRadio}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sor" index="#{parametroKey}" />			    
			</app:scrollableColumn>
	
			<app:scrollableColumn width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.manterCadastroNacionalidade_cod_nacionalidade}" />
			    </f:facet>
			    <br:brOutputText value="#{result.codigoNacionalidade}" styleClass="tableFontStyle"/>
			</app:scrollableColumn>
			<app:scrollableColumn width="500px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.manterCadastroNacionalidade_descricao_nacionalidade}"  />
			    </f:facet>
			    <br:brOutputText value="#{result.descricaoNacionalidade}" />
			</app:scrollableColumn>
			  
			</app:scrollableDataTable>	
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{nacionalidadeBean.pesquisar}" rendered="#{nacionalidadeBean.listaGridPesquisa!= null && nacionalidadeBean.mostraBotoes}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="HtmlCommandButtonBradesco"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnDetalhar" disabled="#{empty nacionalidadeBean.itemSelecionadoLista}" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.manterCadastroNacionalidade_btn_detalhar}" action="#{nacionalidadeBean.detalhar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" disabled="false"  styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.manterCadastroNacionalidade_btn_incluir}" action="#{nacionalidadeBean.incluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" disabled="#{empty nacionalidadeBean.itemSelecionadoLista}" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.manterCadastroNacionalidade_btn_alterar}" action="#{nacionalidadeBean.alterar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty nacionalidadeBean.itemSelecionadoLista}" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.manterCadastroNacionalidade_btn_excluir}" action="#{nacionalidadeBean.excluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</a4j:outputPanel>			
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>

</brArq:form>
