<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>

<brArq:form id="conTipoComprovante" name="conTipoComprovante" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conTipoComprovante_label_codigo_tipo_comprovante}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{manterTipoComprovanteBean.codigoTipoComprovanteFiltro}" disabled="#{manterTipoComprovanteBean.btoAcionado}" size="4" maxlength="3" id="codigoTipoComprovante" >
			    		<brArq:commonsValidator type="integer" arg="#{msgs.conTipoComprovante_label_codigo_tipo_comprovante}" server="false" client="true" />					
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>			
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="Limpar Campos" action="#{manterTipoComprovanteBean.limparCampos}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.conTipoComprovante_label_botao_consultar}" action="#{manterTipoComprovanteBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
		
			<app:scrollableDataTable id="dataTable" value="#{manterTipoComprovanteBean.listaGrid}" var="result" 
			rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >

			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		

				<t:selectOneRadio  id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
					value="#{manterTipoComprovanteBean.itemSelecionadoLista}">
					<f:selectItems value="#{manterTipoComprovanteBean.listaControle}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sor" index="#{parametroKey}" />
			</app:scrollableColumn>
			
			<app:scrollableColumn  width="170px" styleClass="colTabRight" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conTipoComprovante_label_codigo_tipo_comprovante}" style="text-align:center;width:170px" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdComprovanteSalarial}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="300px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.detTipoComprovante_label_descricao}" style="text-align:center;width:300px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsComprovanteSalarial}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_memsagem_padrao_da_organizacao}" style="text-align:center;width:200px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsMensagemPadraoOrganizacao}"/>
			 </app:scrollableColumn>
			</app:scrollableDataTable>				 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterTipoComprovanteBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.conTipoComprovante_path_detalhar}"  action="#{manterTipoComprovanteBean.detalhar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="bto1" disabled="false"  style="margin-right:5px" value="#{msgs.conTipoComprovante_path_incluir}"  action="#{manterTipoComprovanteBean.incluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAlterar" styleClass="bto1" disabled="#{empty manterTipoComprovanteBean.itemSelecionadoLista}"  style="margin-right:5px" value="#{msgs.conTipoComprovante_path_alterar}"  action="#{manterTipoComprovanteBean.alterar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty manterTipoComprovanteBean.itemSelecionadoLista}"  value="#{msgs.conTipoComprovante_path_excluir}"  action="#{manterTipoComprovanteBean.excluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
