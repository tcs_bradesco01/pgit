<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incExManterRegraSegregacaoAcessoDadosForm" name="incExManterRegraSegregacaoAcessoDadosForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{regSegregAcessoDadosBean.obrigatoriedade}"/>
<h:inputHidden id="hiddenTipoExcecao"  value="#{regSegregAcessoDadosBean.tipoExcecaoRadio}" />
<h:inputHidden id="hiddenTipoAbrangencia"  value="#{regSegregAcessoDadosBean.tipoAbrangenciaExcecaoRadio}" />

<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
	 	<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detExRegraSegregacaoAcessoDados_label_codRegra}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.codigoRegraEx}" style="margin-left:5px" />
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalProprietarioUsuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrgProprietarioUsuarioDescEx}" style="margin-left:5px" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalUsuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrgUsuarioExDesc}" style="margin-left:5px" />
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	

	<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_abrangenciaExcecao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<br:brSelectOneRadio id="tipoAbrangenciaExcecao" styleClass="HtmlSelectOneRadioBradesco"  value="#{regSegregAcessoDadosBean.tipoAbrangenciaExcecaoRadio}">
						<f:selectItem itemLabel="#{msgs.incExRegraSegregacaoAcessoDados_label_abrangenciaIndividual}" itemValue="1" />  
			            <f:selectItem itemLabel="#{msgs.incExRegraSegregacaoAcessoDados_label_abrangenciaGeral}" itemValue="2" />  
        				<a4j:support event="onclick" reRender="hiddenTipoAbrangencia,tipoUnidadeOrganizacionalUsuarioExcecao,empresaConglomeradoProprietario, empresaConglomeradoUsuario,txtUnidadeOrganizacionalUsuario" action="#{regSegregAcessoDadosBean.limparUsuario}"/>
			    	</br:brSelectOneRadio>
			    </br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_unidade_organizacional_proprietario_usuario}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			 	<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_empresaConglomerado}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup >
					<br:brSelectOneMenu id="empresaConglomeradoProprietario" value="#{regSegregAcessoDadosBean.empresaConglomeradoProprietario}" style="margin-top:5px" disabled="#{regSegregAcessoDadosBean.tipoUnidadeOrgProprietarioUsuarioEx == null || regSegregAcessoDadosBean.tipoUnidadeOrgProprietarioUsuarioEx == 0 }">
						<f:selectItem itemValue="0" itemLabel="#{msgs.manterRegraSegregacaoAcessoDados_label_combo_selecione}"/>
						<f:selectItems value="#{regSegregAcessoDadosBean.listaEmpresaConglomeradoProprietario}" />						
					</br:brSelectOneMenu>		
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			 	<br:brPanelGroup >
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_unidadeOrganizacionalProprietario}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup>
					<br:brInputText onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{regSegregAcessoDadosBean.unidadeOrgProprietarioUsuarioEx}" size="7" id="txtUnidadeOrganizacionalProprietarioUsuario" style="margin-top:5px" maxlength="5" >  	
						
					</br:brInputText>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_unidade_organizacional_usuario}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" id="teste">		
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			 	<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_empresaConglomerado}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup >
					<br:brSelectOneMenu  id="empresaConglomeradoUsuario" value="#{regSegregAcessoDadosBean.empresaConglomeradoUsuario}" disabled="#{regSegregAcessoDadosBean.tipoUnidadeOrgUsuarioEx == null || regSegregAcessoDadosBean.tipoUnidadeOrgUsuarioEx == 0 || regSegregAcessoDadosBean.tipoAbrangenciaExcecaoRadio != '1'}" style="margin-top:5px">
						<f:selectItem itemValue="0" itemLabel="#{msgs.manterRegraSegregacaoAcessoDados_label_combo_selecione}"/>
						<f:selectItems value="#{regSegregAcessoDadosBean.listaEmpresaConglomerado}"/>
						
					</br:brSelectOneMenu>		
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			 	<br:brPanelGroup >
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_unidadeOrganizacionalUsuario}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" readonly="#{regSegregAcessoDadosBean.tipoAbrangenciaExcecaoRadio != '1'}" styleClass="HtmlInputTextBradesco" value="#{regSegregAcessoDadosBean.unidadeOrgUsuarioEx}" size="7" id="txtUnidadeOrganizacionalUsuario" onkeypress="onlyNum()" style="margin-top:5px" maxlength="5" >  	
						
					</br:brInputText>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">		
		<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_tipoExcecao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>
						<br:brSelectOneRadio id="tipoExcecaoRadio" styleClass="HtmlSelectOneRadioBradesco" value="#{regSegregAcessoDadosBean.tipoExcecaoRadio}">
							<f:selectItem itemLabel="#{msgs.incExRegraSegregacaoAcessoDados_label_acessoLiberado}" itemValue="1" />  
				            <f:selectItem itemLabel="#{msgs.incExRegraSegregacaoAcessoDados_label_acessoBloqueado}" itemValue="2" />  
				            <a4j:support event="onclick" reRender="hiddenTipoExcecao"/>
				    	</br:brSelectOneRadio>
				    </br:brPanelGroup>
				</br:brPanelGrid>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-right:20px">
		</br:brPanelGroup>

	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  > 
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.manterRegraSegregacaoAcessoDados_botao_voltar}" action="#{regSegregAcessoDadosBean.voltarPesquisaExcecao}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton  id="btnAvancarIncluir"  onclick="javascript: validaCamposExcecao(document.forms[1],  '#{msgs.label_ocampo}', '#{msgs.label_necessario_exc}', '#{msgs.incExRegraSegregacaoAcessoDados_label_abrangenciaExcecao}', '#{msgs.incExRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalUsuarioMensagem}','#{msgs.incExRegraSegregacaoAcessoDados_label_empresaConglomeradoUsuario}', '#{msgs.incExRegraSegregacaoAcessoDados_label_unidadeOrganizacionalUsuario}',
			 '#{msgs.incExRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalProprietarioUsuario}','#{msgs.incExRegraSegregacaoAcessoDados_label_empresaConglomeradoProprietario}', '#{msgs.incExRegraSegregacaoAcessoDados_label_unidadeOrganizacionalProprietario}', '#{msgs.incExRegraSegregacaoAcessoDados_label_tipoExcecao}');" styleClass="bto1" value="#{msgs.manterRegraSegregacaoAcessoDados_botao_avancar}" action="#{regSegregAcessoDadosBean.avancarIncluirExcecao}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:150px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
