<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detManterRegraSegregacaoAcessoDadosForm" name="detManterRegraSegregacaoAcessoDadosForm" >

<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detRegraSegregacaoAcessoDados_label_codRegra}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.cdRegraAcessoDado}" style="margin-left:5px" />
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalProprietarioDados}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrganizacionalProprietarioDadosDesc}" style="margin-left:5px" />
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalUsuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrganizacionalUsuarioDesc}" style="margin-left:5px" />
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_tipoAcao}:"/>
   		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_consulta}" rendered = "#{regSegregAcessoDadosBean.tipoManutencao==1}" style="margin-left:5px" />			    	
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_manutencao}" rendered = "#{regSegregAcessoDadosBean.tipoManutencao==2}" style="margin-left:5px" />			    				
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados}:"/>
 		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados1}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao==1}" style="margin-left:5px" />			    	
   			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados2}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao==2}" style="margin-left:5px" />			    	
   			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados3}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao==3}" style="margin-left:5px" />			    	
   			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados9}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao==9}" style="margin-left:5px" />	
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_data_hora_inclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.dataHoraInclusao}" style="margin-left:5px" />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.usuarioInclusao}" style="margin-left:5px" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoCanalInclusao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.complementoInclusao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_data_hora_manutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.dataHoraManutencao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.usuarioManutencao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoCanalManutencao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.complementoManutencao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>

<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
	<br:brPanelGroup style="text-align:left;width:150px" >
		<br:brCommandButton id="btnVoltar" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.manterRegraSegregacaoAcessoDados_botao_voltar}" action="#{regSegregAcessoDadosBean.voltarDetalhar}" >
			<brArq:submitCheckClient/>
		</br:brCommandButton>				
	</br:brPanelGroup>
	<br:brPanelGroup style="text-align:right;width:600px" >		
				
	</br:brPanelGroup>
</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>