<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incExManterRegraSegregacaoAcessoDadosForm2" name="incExManterRegraSegregacaoAcessoDadosForm2" >
<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_codRegra}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.codigoRegraEx}" style="margin-left:5px" />
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalProprietarioUsuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrgProprietarioUsuarioDescEx}" style="margin-left:5px" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalUsuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrgUsuarioExDesc}" style="margin-left:5px" />
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_abrangenciaExcecao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_individual}" rendered = "#{regSegregAcessoDadosBean.tipoAbrangenciaExcecaoRadio == 1}" style="margin-left:5px" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_geral}" rendered = "#{regSegregAcessoDadosBean.tipoAbrangenciaExcecaoRadio == 2}" style="margin-left:5px" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_unidade_organizacional_proprietario_usuario}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacional}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrgProprietarioUsuarioDescEx}" style="margin-left:5px" />
		</br:brPanelGroup>					
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_empresaConglomerado}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.empresaConglomeradoProprietarioDesc}" style="margin-left:5px" />
		</br:brPanelGroup>
	</br:brPanelGrid> 			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_unidadeOrganizacionalProprietario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.unidadeOrgProprietarioUsuarioExDesc}" style="margin-left:5px" />
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_tipoExcecao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_liberado}"  rendered = "#{regSegregAcessoDadosBean.tipoExcecaoRadio == 1}" style="margin-left:5px" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_bloqueado}"  rendered = "#{regSegregAcessoDadosBean.tipoExcecaoRadio == 2}" style="margin-left:5px" />			
		</br:brPanelGroup>	
	</br:brPanelGrid>	
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_unidade_organizacional_usuario}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacional}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrgUsuarioExDesc}" style="margin-left:5px" rendered="#{regSegregAcessoDadosBean.tipoAbrangenciaExcecaoRadio == 1}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="" style="margin-left:5px" rendered="#{regSegregAcessoDadosBean.tipoAbrangenciaExcecaoRadio == 2}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_empresaConglomerado}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.empresaConglomeradoUsuarioDesc}" style="margin-left:5px" />
		</br:brPanelGroup>				
	</br:brPanelGrid>  
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incExRegraSegregacaoAcessoDados_label_unidadeOrganizacionalUsuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.unidadeOrgUsuarioExDesc}" style="margin-left:5px" />
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>		
		
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.manterRegraSegregacaoAcessoDados_botao_voltar}" action="#{regSegregAcessoDadosBean.voltarIncluirExcecao}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton  id="btnConfirmar" styleClass="bto1" value="#{msgs.manterRegraSegregacaoAcessoDados_botao_confirmar}" action="#{regSegregAcessoDadosBean.confirmarIncluirExcecao}" onclick="javascript: if (!confirm('Confirma inclus�o?')) { desbloquearTela(); return false; }" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>