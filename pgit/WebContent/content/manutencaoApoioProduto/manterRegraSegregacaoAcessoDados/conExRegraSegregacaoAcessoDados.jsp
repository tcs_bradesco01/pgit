<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conExManterRegraSegregacaoAcessoDadosForm" name="conExManterRegraSegregacaoAcessoDadosForm" >
<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_argsPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
	 	<br:brPanelGroup style="margin-bottom:5px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_codRegra}:"/>
	    	<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.codigoRegraEx}" style="margin-left:5px" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalProprietarioUsuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrgProprietarioUsuarioDescEx}" style="margin-left:5px" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalUsuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrgUsuarioExDesc}" style="margin-left:5px" />
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_tipoAcao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterRegraSegregacaoAcessoDados_label_consulta}" rendered = "#{regSegregAcessoDadosBean.tipoManutencao == 1}" style="margin-left:5px"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterRegraSegregacaoAcessoDados_label_manutencao}" rendered = "#{regSegregAcessoDadosBean.tipoManutencao == 2}" style="margin-left:5px"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados1}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao == 1}" style="margin-left:5px" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados2}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao == 2}" style="margin-left:5px" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados3}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao == 3}" style="margin-left:5px" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados9}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao == 9}" style="margin-left:5px" />
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_TipoExcecaoAcesso}:"/>
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_TipoAbrangenciaAcesso}:"/>
		</br:brPanelGroup>		
			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conExRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalUsuario}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoManutencao" value="#{regSegregAcessoDadosBean.tipoExececaoAcessoFiltro}" disabled="#{regSegregAcessoDadosBean.btoAcionadoExcecao}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.manterRegraSegregacaoAcessoDadosExcecao_label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.conExRegraSegregacaoAcessoDados_label_liberado}"/>
				<f:selectItem itemValue="2" itemLabel="#{msgs.conExRegraSegregacaoAcessoDados_label_bloqueado}"/>
					
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoAbrangenciaAcesso" value="#{regSegregAcessoDadosBean.tipoAbrangenciaAcessoFiltro}" disabled="#{regSegregAcessoDadosBean.btoAcionadoExcecao}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.manterRegraSegregacaoAcessoDadosExcecao_label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.conExRegraSegregacaoAcessoDados_label_individual}"/>
				<f:selectItem itemValue="2" itemLabel="#{msgs.conExRegraSegregacaoAcessoDados_label_geral}"/>			
			</br:brSelectOneMenu>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoUnidadeOrganizacionalUsuario" value="#{regSegregAcessoDadosBean.tipoUnidadeOrgFiltroEx}" disabled="#{regSegregAcessoDadosBean.btoAcionadoExcecao}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.manterRegraSegregacaoAcessoDadosExcecao_label_combo_selecione}"/>
				<f:selectItems value="#{regSegregAcessoDadosBean.listaTipoUnidadeOrganizacional}"/>			
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
					
	</br:brPanelGrid> 
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conExRegraSegregacaoAcessoDados_botao_limparCampos}" action="#{regSegregAcessoDadosBean.limparDadosExcecao}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar"  styleClass="bto1" value="#{msgs.conExRegraSegregacaoAcessoDados_botao_consultar}" action="#{regSegregAcessoDadosBean.carregaListaExcecao}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170px">	
			<app:scrollableDataTable id="dataTable" value="#{regSegregAcessoDadosBean.listaGridExcecoesFiltro}" var="result" 
			rows="10" rowIndexVar="parametroKey" 
			rowClasses="tabela_celula_normal, tabela_celula_destaque" 
			width="100%" >			

			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
				<t:selectOneRadio  id="sorLista2" styleClass="HtmlSelectOneRadioBradesco" 
					layout="spread" forceId="true" forceIdIndex="false" 
					value="#{regSegregAcessoDadosBean.itemSelecionadoListaExcecaoFiltro}">
					<f:selectItems value="#{regSegregAcessoDadosBean.listaControleRadioExcecoesFiltro}"/>
					<a4j:support event="onclick" reRender="btnDetalhar,btnAlterar,btnExcluir" />
				</t:selectOneRadio>
		    	<t:radio for="sorLista2" index="#{parametroKey}" />
			</app:scrollableColumn>
 

			<app:scrollableColumn  width="180px" styleClass="colTabRight" >
			    <f:facet name="header">
			     <h:outputText value="#{msgs.conExRegraSegregacaoAcessoDados_grid_codigoExcecao}" style="text-align:center;width:180" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdExcecao}" />
			 </app:scrollableColumn>
			 
			<app:scrollableColumn  width="300px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conExRegraSegregacaoAcessoDados_grid_tipoUnidadeOrganizacionalUsuario}" style="text-align:center;width:300" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoUnidadeOrganizacionalUsuario}"  />
			 </app:scrollableColumn>
			  
			 <app:scrollableColumn  width="300px" styleClass="colTabLeft" >
			    <f:facet name="header">
			       <h:outputText value="#{msgs.conExRegraSegregacaoAcessoDados_grid_unidadeOrgUsuario}" style="text-align:center;width:300"/>
			    </f:facet>
			    <br:brOutputText value="#{result.cdUnidadeOrganizacionalUsuario} - #{result.dsUnidadeOrganizacionalUsuario}"  rendered = "#{result.cdUnidadeOrganizacionalUsuario > 0}"/>
			    <br:brOutputText value=""  rendered = "#{result.cdUnidadeOrganizacionalUsuario == 0}"/>
			 </app:scrollableColumn>
			  
			  <app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			         <h:outputText value="#{msgs.conExRegraSegregacaoAcessoDados_grid_tipoExcecao}" style="text-align:center;width:200" />
			    </f:facet>
			    <br:brOutputText value="#{msgs.conExRegraSegregacaoAcessoDados_label_liberado}" rendered = "#{result.tipoExcecao==1}" />			    	
   			    <br:brOutputText value="#{msgs.conExRegraSegregacaoAcessoDados_label_bloqueado}" rendered = "#{result.tipoExcecao==2}" />
			 </app:scrollableColumn>
			  
			   <app:scrollableColumn  width="250px" styleClass="colTabLeft" >
			    <f:facet name="header">
			        <h:outputText value="#{msgs.conExRegraSegregacaoAcessoDados_grid_abrangenciaExcecao}" style="text-align:center;width:250" />
			    </f:facet>
			    <br:brOutputText value="#{msgs.conExRegraSegregacaoAcessoDados_label_individual}" rendered = "#{result.tipoAbrangenciaExcecao==1}" />			    	
   			    <br:brOutputText value="#{msgs.conExRegraSegregacaoAcessoDados_label_geral}" rendered = "#{result.tipoAbrangenciaExcecao==2}" />
			 </app:scrollableColumn> 
			</app:scrollableDataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{regSegregAcessoDadosBean.pesquisar}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	
	<br:brPanelGrid columns="3" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
		<br:brPanelGroup style="width:750px">
			<br:brPanelGroup style="text-align:left;width:537px"  >
				<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.manterRegraSegregacaoAcessoDados_botao_voltar}" action="#{regSegregAcessoDadosBean.voltarExececao}" >	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
			<br:brCommandButton id="btnDetalhar" disabled="#{empty regSegregAcessoDadosBean.itemSelecionadoListaExcecaoFiltro}" styleClass="bto1"  style="margin-right:5px;" value="#{msgs.conExRegraSegregacaoAcessoDados_botao_detalhar}" action="#{regSegregAcessoDadosBean.detalharExcecao}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" disabled="false" styleClass="bto1" style="margin-right:5px;" value="#{msgs.conExRegraSegregacaoAcessoDados_botao_incluir}" action="#{regSegregAcessoDadosBean.incluirExcecao}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAlterar" disabled="#{empty regSegregAcessoDadosBean.itemSelecionadoListaExcecaoFiltro}" styleClass="bto1" style="margin-right:5px;" value="#{msgs.conExRegraSegregacaoAcessoDados_botao_alterar}" action="#{regSegregAcessoDadosBean.alterarExcecao}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" disabled="#{empty regSegregAcessoDadosBean.itemSelecionadoListaExcecaoFiltro}" styleClass="bto1"  value="#{msgs.conExRegraSegregacaoAcessoDados_botao_excluir}" action="#{regSegregAcessoDadosBean.excluirExcecao}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>						
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
			
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>