<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="altManterRegraSegregacaoAcessoDadosForm2" name="altManterRegraSegregacaoAcessoDadosForm2" >

<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detRegraSegregacaoAcessoDados_label_codRegra}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.cdRegraAcessoDado}" style="margin-left:5px" />
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalProprietarioDados}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrganizacionalProprietarioDadosDesc}" />
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalUsuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{regSegregAcessoDadosBean.tipoUnidadeOrganizacionalUsuarioDesc}" />
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_tipoAcao}:"/>
   		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_consulta}" rendered = "#{regSegregAcessoDadosBean.tipoManutencao==1}" />			    	
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_manutencao}" rendered = "#{regSegregAcessoDadosBean.tipoManutencao==2}" />			    				

		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados}:"/>				
 		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados1}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao==1}" />			    	
   			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados2}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao==2}" />			    	
   			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados3}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao==3}" />			    	
   			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados9}" rendered = "#{regSegregAcessoDadosBean.nivelPermissao==9}" />	
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>			
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.manterRegraSegregacaoAcessoDados_botao_voltar}" action="VOLTAR_ALTERAR" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton  id="btnConfirmarIncluir" styleClass="bto1" value="#{msgs.manterRegraSegregacaoAcessoDados_botao_confirmar}" action="#{regSegregAcessoDadosBean.confirmarAlterar}"  onclick="javascript: if (!confirm('Confirma altera��o?')) { desbloquearTela(); return false; }">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>	

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>