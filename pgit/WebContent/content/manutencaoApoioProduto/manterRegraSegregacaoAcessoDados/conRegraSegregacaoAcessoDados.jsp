<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="manterRegraSegregacaoAcessoDadosForm" name="manterRegraSegregacaoAcessoDadosForm" >

<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_argsPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="radioFiltroSelArgPesquisa" value="#{regSegregAcessoDadosBean.rdoSelArgPesq}" disabled="#{regSegregAcessoDadosBean.btoAcionado}"  styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false"  >  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<a4j:support event="onclick" reRender="txtCodigoRegra, tipoManutencao, tipoUnidadeOrganizacionalUsuario, tipoNivelPermissaoAcessoDados" action="#{regSegregAcessoDadosBean.limparTelaPrincipal}" status="statusAguarde" />				
		</t:selectOneRadio>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">		
		 <t:radio for="radioFiltroSelArgPesquisa" index="0" />	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_codRegra}:"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				</br:brPanelGrid>	
			</br:brPanelGroup>						
			
			<br:brPanelGroup>
		    	<br:brInputText onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{regSegregAcessoDadosBean.codigoRegraFiltro}" disabled="#{regSegregAcessoDadosBean.rdoSelArgPesq != 0 || regSegregAcessoDadosBean.btoAcionado}" size="20" id="txtCodigoRegra" maxlength="8" onkeypress="onlyNum();">  
		    		<brArq:commonsValidator type="integer" arg="#{msgs.conRegraSegregacaoAcessoDados_label_codRegra}" server="false" client="true" />
		    		<a4j:support event="onkeyup" reRender="tipoManutencao, tipoUnidadeOrganizacionalUsuario,tipoNivelPermissaoAcessoDados"  action="#{regSegregAcessoDadosBean.limpar}"/>
				 	<a4j:support event="onblur" reRender="tipoManutencao, tipoUnidadeOrganizacionalUsuario,tipoNivelPermissaoAcessoDados"  action="#{regSegregAcessoDadosBean.limpar}"/>
		    	</br:brInputText>
			</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
		<t:radio for="radioFiltroSelArgPesquisa" index="1"  />	
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_tipoAcao}:"/>
					</br:brPanelGroup>
					
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						</br:brPanelGrid>	
					</br:brPanelGroup>						
					
					<br:brPanelGroup>
						<br:brSelectOneMenu style="margin-right:20px" id="tipoManutencao" value="#{regSegregAcessoDadosBean.tipoManutencaoFiltro}" 
							disabled = "#{regSegregAcessoDadosBean.codigoRegraFiltro != null && regSegregAcessoDadosBean.codigoRegraFiltro != '' || regSegregAcessoDadosBean.btoAcionado || regSegregAcessoDadosBean.rdoSelArgPesq != 1}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.manterRegraSegregacaoAcessoDados_label_combo_selecione}"/>
							<f:selectItem itemValue="1" itemLabel="#{msgs.conRegraSegregacaoAcessoDados_label_consulta}"/>
							<f:selectItem itemValue="2" itemLabel="#{msgs.conRegraSegregacaoAcessoDados_label_manutencao}"/>				
						</br:brSelectOneMenu>
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_grid_tipoUnidadeOrganizacionalUsuario}:"/>
					</br:brPanelGroup>
					
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						</br:brPanelGrid>	
					</br:brPanelGroup>						
					
					<br:brPanelGroup>
						<br:brSelectOneMenu id="tipoUnidadeOrganizacionalUsuario" value="#{regSegregAcessoDadosBean.tipoUnidadeOrgFiltro}" disabled = "#{regSegregAcessoDadosBean.codigoRegraFiltro != null && regSegregAcessoDadosBean.codigoRegraFiltro != '' || regSegregAcessoDadosBean.btoAcionado || regSegregAcessoDadosBean.rdoSelArgPesq != 1}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.manterRegraSegregacaoAcessoDados_label_combo_selecione}"/>
							<f:selectItems  value="#{regSegregAcessoDadosBean.listaTipoUnidadeOrganizacional}" />			
						</br:brSelectOneMenu>
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados}:"/>
					</br:brPanelGroup>
					
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						</br:brPanelGrid>	
					</br:brPanelGroup>						
					
					<br:brPanelGroup>
						<br:brSelectOneMenu id="tipoNivelPermissaoAcessoDados" value="#{regSegregAcessoDadosBean.nivelPermissaoFiltro}" disabled = "#{regSegregAcessoDadosBean.codigoRegraFiltro != null && regSegregAcessoDadosBean.codigoRegraFiltro != '' || regSegregAcessoDadosBean.btoAcionado || regSegregAcessoDadosBean.rdoSelArgPesq != 1}"> 
							<f:selectItem itemValue="0" itemLabel="#{msgs.manterRegraSegregacaoAcessoDados_label_combo_selecione}"/>
							<f:selectItem itemValue="1" itemLabel="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados1}"/>
							<f:selectItem itemValue="2" itemLabel="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados2}"/>
							<f:selectItem itemValue="3" itemLabel="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados3}"/>
							<f:selectItem itemValue="9" itemLabel="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados9}"/>			
						</br:brSelectOneMenu>
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim> 

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
				
		</br:brPanelGroup> 
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btnLimparCampos" style="margin-left:5px" styleClass="bto1" value="#{msgs.conRegraSegregacaoAcessoDados_botao_limparCampos}" action="#{regSegregAcessoDadosBean.limparCampos}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" style="margin-left:5px" styleClass="bto1" value="#{msgs.conRegraSegregacaoAcessoDados_botao_consultar}" action="#{regSegregAcessoDadosBean.consultar}"  onclick="javascript: return validateForm(document.forms[1]);">							
				<brArq:submitCheckClient/>
			</br:brCommandButton> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><br> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170px">	
			<app:scrollableDataTable id="dataTable" value="#{regSegregAcessoDadosBean.listaGridRegrasFiltro}" var="result" 
			rows="10" rowIndexVar="parametroKey" 
			rowClasses="tabela_celula_normal, tabela_celula_destaque" 
			width="100%" >			
			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
				<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
					layout="spread" forceId="true" forceIdIndex="false" 
					value="#{regSegregAcessoDadosBean.itemSelecionadoListaRegrasFiltro}">
					<f:selectItems value="#{regSegregAcessoDadosBean.listaControleRadioRegrasFiltro}"/>
					<a4j:support event="onclick" reRender="btnDetalhar,btnExcluir,btnAlterar,btnExececao" />
				</t:selectOneRadio>
		    	<t:radio for="sorLista" index="#{parametroKey}" />
			</app:scrollableColumn>
			
			<app:scrollableColumn  width="200px" styleClass="colTabRight" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conRegraSegregacaoAcessoDados_grid_codigoDaRegra}" style="text-align:center;width:200" />
			    </f:facet>
			    <br:brOutputText value="#{result.codigoRegra}"/>
			 </app:scrollableColumn>
			
			<app:scrollableColumn  width="300px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conRegraSegregacaoAcessoDados_grid_tipoUnidadeOrganizacionalProprietarioDados}" style="text-align:center;width:300"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoUnidadeOrganizacionalProprietario}"/>
			 </app:scrollableColumn>
			 
			<app:scrollableColumn  width="300px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conRegraSegregacaoAcessoDados_grid_tipoUnidadeOrganizacionalUsuario}" style="text-align:center;width:300" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoUnidadeOrganizacionalUsuario}"/>
			 </app:scrollableColumn>
		  
			<app:scrollableColumn  width="130px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conRegraSegregacaoAcessoDados_label_tipoAcao}" style="text-align:center;width:130" />
			    </f:facet>
			    <br:brOutputText value="#{msgs.conRegraSegregacaoAcessoDados_label_consulta}" rendered = "#{result.tipoAcao==1}" />			    	
   			    <br:brOutputText value="#{msgs.conRegraSegregacaoAcessoDados_label_manutencao}" rendered = "#{result.tipoAcao==2}" />			    	
			 </app:scrollableColumn>

			<app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conRegraSegregacaoAcessoDados_grid_NivelPermissao}" style="text-align:center;width:250" />
			    </f:facet>
   			    <br:brOutputText value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados1}" rendered = "#{result.nivelPermissao==1}" />			    	
   			    <br:brOutputText value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados2}" rendered = "#{result.nivelPermissao==2}" />			    	
   			    <br:brOutputText value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados3}" rendered = "#{result.nivelPermissao==3}" />			    	
   			    <br:brOutputText value="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados9}" rendered = "#{result.nivelPermissao==9}" />			    				 
			 </app:scrollableColumn>
			 
			</app:scrollableDataTable>			
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{regSegregAcessoDadosBean.pesquisar}"  > 
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1" 
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
=			
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btnDetalhar" disabled="#{empty regSegregAcessoDadosBean.itemSelecionadoListaRegrasFiltro}" style="margin-left:5px"   styleClass="bto1" value="#{msgs.conRegraSegregacaoAcessoDados_botao_detalhar}" action="#{regSegregAcessoDadosBean.detalhar}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" style="margin-left:5px"  styleClass="bto1" value="#{msgs.conRegraSegregacaoAcessoDados_botao_incluir}" action="#{regSegregAcessoDadosBean.incluir}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAlterar" disabled="#{empty regSegregAcessoDadosBean.itemSelecionadoListaRegrasFiltro}" style="margin-left:5px"   styleClass="bto1" value="#{msgs.conRegraSegregacaoAcessoDados_botao_alterar}" action="#{regSegregAcessoDadosBean.alterar}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnExcluir" disabled="#{empty regSegregAcessoDadosBean.itemSelecionadoListaRegrasFiltro}" style="margin-left:5px"  styleClass="bto1" value="#{msgs.conRegraSegregacaoAcessoDados_botao_excluir}" action="#{regSegregAcessoDadosBean.excluir}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnExececao"  disabled="#{empty regSegregAcessoDadosBean.itemSelecionadoListaRegrasFiltro}"   style="margin-left:5px"  styleClass="bto1" value="#{msgs.conRegraSegregacaoAcessoDados_botao_excecao}" action="#{regSegregAcessoDadosBean.excecao}" >							
				<brArq:submitCheckClient/> 
			</br:brCommandButton>							
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>