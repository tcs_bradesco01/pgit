<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incManterRegraSegregacaoAcessoDadosForm" name="incManterRegraSegregacaoAcessoDadosForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{regSegregAcessoDadosBean.obrigatoriedade}"/>


<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalProprietarioDados}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoUnidadeOrganizacionalProprietarioDados" value="#{regSegregAcessoDadosBean.tipoUnidadeOrgProprietario}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.manterRegraSegregacaoAcessoDados_label_combo_selecione}"/>
				<f:selectItems value="#{regSegregAcessoDadosBean.listaTipoUnidadeOrganizacional}"/>			
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalUsuario}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoUnidadeOrganizacionalUsuario" value="#{regSegregAcessoDadosBean.tipoUnidadeOrg}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.manterRegraSegregacaoAcessoDados_label_combo_selecione}"/>
				<f:selectItems value="#{regSegregAcessoDadosBean.listaTipoUnidadeOrganizacional}" />			
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_tipoAcao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<h:inputHidden id="hiddenTipoManutencao"  value="#{regSegregAcessoDadosBean.tipoManutencao}" />
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brSelectOneRadio id="radioFiltro" styleClass="HtmlSelectOneRadioBradesco"  value="#{regSegregAcessoDadosBean.tipoManutencao}">
				<f:selectItem itemLabel="#{msgs.manterRegraSegregacaoAcessoDados_label_consulta}" itemValue="1" />  
	            <f:selectItem itemLabel="#{msgs.manterRegraSegregacaoAcessoDados_label_manutencao}" itemValue="2" />  
	            <a4j:support event="onclick" reRender="hiddenTipoManutencao"/>
	    	</br:brSelectOneRadio>
	    </br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<!-- Acesso Dados-->
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brSelectOneMenu id="nivelPermissaoAcessoDados" value="#{regSegregAcessoDadosBean.nivelPermissao}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.manterRegraSegregacaoAcessoDados_label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados1}"/>
						<f:selectItem itemValue="2" itemLabel="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados2}"/>
						<f:selectItem itemValue="3" itemLabel="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados3}"/>
						<f:selectItem itemValue="9" itemLabel="#{msgs.conRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados9}"/>		
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	<!-- /Acesso Dados-->
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.manterRegraSegregacaoAcessoDados_botao_voltar}" action="#{regSegregAcessoDadosBean.voltarIncluir}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton  id="btnAvancarIncluir" onclick="javascript: validaCampos(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario_exc}', '#{msgs.incRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalUsuario}', '#{msgs.incRegraSegregacaoAcessoDados_label_tipoUnidadeOrganizacionalProprietarioDados}', '#{msgs.incRegraSegregacaoAcessoDados_label_tipoAcao}', '#{msgs.incRegraSegregacaoAcessoDados_label_nivelPermissaoAcessoDados}' );" styleClass="bto1" value="#{msgs.manterRegraSegregacaoAcessoDados_botao_avancar}" action="#{regSegregAcessoDadosBean.avancarIncluir}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>