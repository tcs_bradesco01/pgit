<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="confIncManterAssocVersaoLayLoteAqrServ" name="confIncManterAssocVersaoLayLoteAqrServ" >

<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina" >
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterAssocRetornoLayArqProdServ_tipoLayoutArquivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{assocLayoutLoteArqServBean.descTipoLayoutArqCompl}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_numVersLayoutArquivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{assocLayoutLoteArqServBean.entradaIncluirLoteServ.nrVersaoLayoutArquivo}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoLote}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{assocLayoutLoteArqServBean.descTipoLoteLayoutCompl}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_numVersLoteLayoutArquivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{assocLayoutLoteArqServBean.entradaIncluirLoteServ.nrVersaoLoteLayout}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoServico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{assocLayoutLoteArqServBean.descTipoServicoCnabCompl}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" ajaxRendered="true">		
	 	<br:brPanelGrid columns="2" width="750px" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="width: 100%; text-align: left" >
					<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.detManterAssocRetornoLayArqProdServ_btnVoltar}" action="#{assocLayoutLoteArqServBean.voltar}" >	
						<brArq:submitCheckClient/>
					</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="width: 100%; text-align: right" >
				<br:brCommandButton id="btnConfirmar"  styleClass="bto1" value="#{msgs.incManterAssocRetornoLayArqProdServ2_btnConfirmar}" 
				onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela(); return false; }" action="#{assocLayoutLoteArqServBean.incluirAssVersaoLayoutLoteServico}">
					<brArq:submitCheckClient/>
				</br:brCommandButton> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
		
</br:brPanelGrid>
</brArq:form>