<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incManterAssocVersaoLayLoteAqrServ" name="incManterAssocVersaoLayLoteAqrServ" >
<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina" >
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

 	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
	 		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoLayoutArquivo}:"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_numVersLayArq}:"/>
			</br:brPanelGroup>
	 	</br:brPanelGrid>
	 	
	 	<br:brPanelGrid >
	 		<br:brPanelGroup style="margin-top:5px"/>
	 	</br:brPanelGrid>
	 	
	 	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
	 		<br:brPanelGroup style="margin-left:10px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{assocLayoutLoteArqServBean.descLayoutArquivo}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="margin-left:10px">
				 <br:brInputText id="txnumeroLayout" onkeypress="onlyNum();" onfocus="validaCampoNumericoColado(this);" size="11" maxlength="3"  
					 styleClass="HtmlInputTextBradesco" 
					 value="#{assocLayoutLoteArqServBean.entradaIncluirLoteServ.nrVersaoLayoutArquivo}" 
					 onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
				
					 <a4j:support event="onblur"  reRender="btnAvancar"/>	 
				 </br:brInputText>
			</br:brPanelGroup>
	 	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoLote}:"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brSelectOneMenu id="cboTipoLoteArquivo" value="#{assocLayoutLoteArqServBean.entradaIncluirLoteServ.cdTipoLoteLayout}" disabled="#{assocLayoutLoteArqServBean.btoAcionado}" style="margin-top:5px">
					<f:selectItem itemValue="" itemLabel="#{msgs.conManterAssocRetornoLayArqProdServ_selecione}"/>	
					<f:selectItems value="#{assocLayoutLoteArqServBean.listaTipoLote}"/>	
					<a4j:support event="onchange" reRender="btnAvancar"/>					
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-left:15px">
			<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_numVersLoteLayArq}:"/>
			</br:brPanelGroup>	
			<br:brPanelGroup style="margin-left:10px">
				<br:brPanelGroup>
				 <br:brInputText id="txnumeroLote" onkeypress="onlyNum();" size="11" maxlength="3" styleClass="HtmlInputTextBradesco" 
					 value="#{assocLayoutLoteArqServBean.entradaIncluirLoteServ.nrVersaoLoteLayout}" 
					 onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
					 onfocus="validaCampoNumericoColado(this);">
					 <a4j:support event="onblur"  reRender="btnAvancar"/>		
				 </br:brInputText>
			    </br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoServico}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboTipoServico" value="#{assocLayoutLoteArqServBean.entradaIncluirLoteServ.cdTipoServicoCnab}" disabled="#{assocLayoutLoteArqServBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conManterAssocRetornoLayArqProdServ_selecione}"/>	
				<f:selectItems value="#{assocLayoutLoteArqServBean.listaTipoServicoCnab}"/>
				<a4j:support event="onchange"  reRender="btnAvancar"/>					
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
	 	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="width:100%; text-align:left;">
				<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.incManterAssocRetornoLayArqProdServ_btnVoltar}" 
					action="#{assocLayoutLoteArqServBean.voltarIncluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="width:100%; text-align:right" >
				<br:brCommandButton id="btnAvancar" disabled="#{assocLayoutLoteArqServBean.desabilitaBtnAvancarInc}" styleClass="bto1" style="margin-right:5px" 
					value="#{msgs.incManterAssocRetornoLayArqProdServ_btnAvancar}" action="#{assocLayoutLoteArqServBean.avancarConfirmarIncluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
	
	<br:brPanelGrid columns="1" style="margin-top:150px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>