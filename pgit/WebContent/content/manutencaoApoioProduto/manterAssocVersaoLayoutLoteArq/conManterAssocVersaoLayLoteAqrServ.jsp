<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterAssocVersaoLayLoteAqrServ" name="conManterAssocVersaoLayLoteAqrServ" >
<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina" >
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid  styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_argumentosPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
	 		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoLayoutArquivo}:"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_numVersLayArq}:"/>
			</br:brPanelGroup>
	 	</br:brPanelGrid>
	 	
	 	<br:brPanelGrid >
	 		<br:brPanelGroup style="margin-top:5px"/>
	 	</br:brPanelGrid>
	 	
	 	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
	 		<br:brPanelGroup style="margin-left:10px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{assocLayoutLoteArqServBean.descLayoutArquivo}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="margin-left:10px">
				 <br:brInputText id="txnumeroLayout" onkeypress="onlyNum();" onfocus="validaCampoNumericoColado(this);" size="11" maxlength="3"  
					 disabled="#{assocLayoutLoteArqServBean.listaLayoutLoteServ != null}" styleClass="HtmlInputTextBradesco" 
					 value="#{assocLayoutLoteArqServBean.entradaListarLote.nrVersaoLayoutArquivo}" 
					 onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</br:brPanelGroup>
	 	</br:brPanelGrid>	
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoLote}:"/>
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brSelectOneMenu id="cboTipoLoteArquivo" value="#{assocLayoutLoteArqServBean.entradaListarLote.cdTipoLoteLayout}" 
					   disabled="#{assocLayoutLoteArqServBean.listaLayoutLoteServ != null}" style="margin-top:5px">
						<f:selectItem itemValue="" itemLabel="#{msgs.conManterAssocRetornoLayArqProdServ_selecione}"/>	
						<f:selectItems value="#{assocLayoutLoteArqServBean.listaTipoLote}"/>	
						<a4j:support event="onclick" reRender="btnConsultar"/>					
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-left:15px">
				<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_numVersLoteLayArq}:"/>
				</br:brPanelGroup>	
				<br:brPanelGroup style="margin-left:10px">
					<br:brPanelGroup>
						 <br:brInputText id="txnumeroLote" onkeypress="onlyNum();" onfocus="validaCampoNumericoColado(this);" size="11" maxlength="3"  
						 styleClass="HtmlInputTextBradesco" value="#{assocLayoutLoteArqServBean.entradaListarLote.nrVersaoLoteLayout}"
						 disabled="#{assocLayoutLoteArqServBean.listaLayoutLoteServ != null}"  
						 onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				    </br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
		</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoServico}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboTipoServico" value="#{assocLayoutLoteArqServBean.entradaListarLote.cdTipoServicoCnab}" disabled="#{assocLayoutLoteArqServBean.listaLayoutLoteServ != null}"  style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conManterAssocRetornoLayArqProdServ_selecione}"/>	
				<f:selectItems value="#{assocLayoutLoteArqServBean.listaTipoServicoCnab}"/>	
				<a4j:support event="onclick" reRender="btnConsultar"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conManterAssocRetornoLayArqProdServ_btn_limparCampos}" action="#{assocLayoutLoteArqServBean.limparCampos}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conManterAssocRetornoLayArqProdServ_btn_consultar}" action="#{assocLayoutLoteArqServBean.listarAssVersaoLayoutLoteServico}" 
			disabled="#{assocLayoutLoteArqServBean.desabilitaBotaoConsultar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><br></f:verbatim> 

	<br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto;width:750px;height:300px" >
		
			<app:scrollableDataTable id="dataTable" value="#{assocLayoutLoteArqServBean.listaLayoutLoteServ}" var="result" 
			rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="750px" height="300">

			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		

				<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
					value="#{assocLayoutLoteArqServBean.itemSelecionadoLista}">
					<f:selectItems value="#{assocLayoutLoteArqServBean.listaControle}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sor" index="#{parametroKey}" />
			</app:scrollableColumn>

			<app:scrollableColumn width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoDeLayoutArquivo}" style="text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoLayoutArquivo}"/>
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn width="150px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterAssocRetornoLayArqProdServ_numVersLayoutArquivo}" style="text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.nrVersaoLayoutArquivo}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn width="260px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoLote}" style="text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoLoteLayout}"/>
			 </app:scrollableColumn>
			 
			<app:scrollableColumn width="260px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterAssocRetornoLayArqProdServ_numVersLoteLayoutArquivo}" style="text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.nrVersaoLoteLayout}"/>
			 </app:scrollableColumn>
			 
			<app:scrollableColumn width="260px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoServico}" style="text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoServicoCnab}"/>
			 </app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnDetalhar" disabled="#{empty assocLayoutLoteArqServBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterAssocRetornoLayArqProdServ_btn_detalhar}" action="#{assocLayoutLoteArqServBean.avancarDetalhar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir"  disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterAssocRetornoLayArqProdServ_btn_incluir}" action="#{assocLayoutLoteArqServBean.incluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir"  disabled="#{empty assocLayoutLoteArqServBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterAssocRetornoLayArqProdServ_btn_excluir}" action="#{assocLayoutLoteArqServBean.avancarExcluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton> 
				<br:brCommandButton id="btnLimparGrid"  styleClass="bto1" value="#{msgs.label_botao_limpar}" action="#{assocLayoutLoteArqServBean.limpar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
</br:brPanelGrid>
	
<br:brPanelGrid columns="1" style="margin-top:150px" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
	</br:brPanelGroup>
</br:brPanelGrid>		

<brArq:validatorScript functionName="validateForm" />
</brArq:form>