<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>


<brArq:form id="estatisticaProcessamentoForm" name="estatisticaProcessamentoForm" >
<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
	  <br:brPanelGroup> 
		</br:brPanelGroup>
  	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.PGIC0053_label_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0053_grid_label_centro_custo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<a4j:region renderRegionOnly="true">
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" value="#{estatisticaProcessamentoBean.centroCustoLupaFiltro}" disabled="#{estatisticaProcessamentoBean.btoAcionado}" size="5" maxlength="4" id="centroCustoFiltro"/>
						<f:verbatim>&nbsp;</f:verbatim>
						<br:brGraphicImage url="/images/lupa.gif" style="cursor: hand;">
							<a4j:support event="onclick" action="#{estatisticaProcessamentoBean.obterCentroCustoFiltro}" reRender="centroCusto" status="statusAguarde"/>
						</br:brGraphicImage>
						<f:verbatim>&nbsp;</f:verbatim>
						<f:verbatim>&nbsp;</f:verbatim>
						<br:brSelectOneMenu id="centroCusto" value="#{estatisticaProcessamentoBean.estatisticaProcessamentoFiltro.cdSistema}" disabled="#{empty estatisticaProcessamentoBean.listaCentroCusto || estatisticaProcessamentoBean.btoAcionado}">
							<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0053_label_combo_selecione}"/>
							<si:selectItems value="#{estatisticaProcessamentoBean.listaCentroCusto}" var="centroCusto" itemLabel="#{centroCusto.codigoDescricao}" itemValue="#{centroCusto.cdCentroCusto}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</a4j:region>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_de_execucao}:" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-top:5px"></br:brPanelGroup> 
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0053_label_periodo_horario_inicio_execucao}:" />
		</br:brPanelGroup>
		
	<br:brPanelGroup>
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGroup>	
	
	<br:brPanelGroup>
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGroup>	
	
	<br:brPanelGroup>
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.PGIC0053_label_de}"/>
			<br:brPanelGroup rendered="#{!estatisticaProcessamentoBean.btoAcionado}">
			 	<app:calendar id="calendarioDe" value="#{estatisticaProcessamentoBean.estatisticaProcessamentoFiltro.dtInicioEstatisticaProcessoInicio}" >
				</app:calendar>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{estatisticaProcessamentoBean.btoAcionado}">
			 	<app:calendar id="calendarioDeDes" value="#{estatisticaProcessamentoBean.estatisticaProcessamentoFiltro.dtInicioEstatisticaProcessoInicio}" disabled="true" >
				</app:calendar>
			</br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.PGIC0053_label_a}"/>
			<br:brPanelGroup rendered="#{!estatisticaProcessamentoBean.btoAcionado}">
				<app:calendar id="calendarioAte" value="#{estatisticaProcessamentoBean.estatisticaProcessamentoFiltro.dtInicioEstatisticaProcessoFim}" >
				</app:calendar>		
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{estatisticaProcessamentoBean.btoAcionado}">
				<app:calendar id="calendarioAteDes" value="#{estatisticaProcessamentoBean.estatisticaProcessamentoFiltro.dtInicioEstatisticaProcessoFim}" disabled="true">
				</app:calendar>		
			</br:brPanelGroup>	
		</br:brPanelGroup>

		<br:brPanelGroup style="width:20px" />
		
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.PGIC0053_label_de}"/>
			<br:brInputText styleClass="HtmlInputTextBradesco" value="#{estatisticaProcessamentoBean.estatisticaProcessamentoFiltro.hrInicioEstatisticaProcessoInicio}" disabled="#{estatisticaProcessamentoBean.btoAcionado}" size="15" maxlength="5" alt="hora" id="horarioFiltro" onblur="validarHora(this,'#{msgs.label_hora_invalida}')">
				<%--<f:validator validatorId="horaValidator" />--%>
			</br:brInputText>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.PGIC0053_label_ate}"/>
			<br:brInputText styleClass="HtmlInputTextBradesco" value="#{estatisticaProcessamentoBean.estatisticaProcessamentoFiltro.hrInicioEstatisticaProcessoFim}" disabled="#{estatisticaProcessamentoBean.btoAcionado}" size="15" maxlength="5" alt="hora" id="horarioFiltroFim" onblur="validarHora(this,'#{msgs.label_hora_invalida}')">
				<%--<f:validator validatorId="horaValidator" />--%>
			</br:brInputText>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0053_label_tipo_processo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="processamento" value="#{estatisticaProcessamentoBean.estatisticaProcessamentoFiltro.cdTipoProcessoSistema}" disabled="#{estatisticaProcessamentoBean.btoAcionado}" converter="javax.faces.Integer">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0053_label_combo_selecione}"/>
						<si:selectItems value="#{estatisticaProcessamentoBean.listaTipoProcesso}" var="tipoProcesso" itemLabel="#{tipoProcesso.dsTipoProcesso}" itemValue="#{tipoProcesso.cdTipoProcesso}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0053_label_situacao_processamento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="situacao" value="#{estatisticaProcessamentoBean.estatisticaProcessamentoFiltro.cdSituacaoEstatisticaSistema}" disabled="#{estatisticaProcessamentoBean.btoAcionado}" converter="javax.faces.Integer">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0053_label_combo_selecione}"/>
						<f:selectItem itemValue="1" itemLabel="#{msgs.PGIC0053_label_combo_situacao_em_processamento_1}"/>
						<f:selectItem itemValue="2" itemLabel="#{msgs.PGIC0053_label_combo_situacao_processado_2}"/>
						<f:selectItem itemValue="3" itemLabel="#{msgs.PGIC0053_label_combo_situacao_nao_processado_3}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="Limpar Campos" action="#{estatisticaProcessamentoBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" onclick="javascript:if(!validarFiltroEstatistica('#{msgs.label_ocampo}', '#{msgs.label_necessario}', 
				'#{msgs.PGIC0053_label_periodo_inicial}','#{msgs.PGIC0053_label_periodo_final}',
				'#{msgs.PGIC0053_label_periodo_horario_inicio_execucao}',
				'#{msgs.PGIC0053_grid_label_centro_custo}','#{msgs.PGIC0053_label_tipo_processo}','#{msgs.label_horario_invalido}','#{msgs.label_valida_data}','#{msgs.PGIC0053_label_periodo_horario_inicio_execucao}' )) { 
					desbloquearTela(); return false; }" styleClass="bto1" value="#{msgs.PGIC0053_label_botao_consultar}" action="#{estatisticaProcessamentoBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	 <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
			<app:scrollableDataTable id="dataTable" value="#{estatisticaProcessamentoBean.listaEstatisticaProcessamento}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">

				<app:scrollableColumn styleClass="colTabLeft" width="100px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.PGIC0053_grid_label_centro_custo}" style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdSistema}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabRight" width="70px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.PGIC0053_grid_label_codigo}" style="width:70; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdProcessoSistema}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="150px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.PGIC0053_grid_label_nome}" style="width:110; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.idTipoProcessoSIstema}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabCenter" width="110px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.PGIC0053_grid_label_inicio}" style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.hrInicioEstatisticaProcessoInicio}" converter="timestampPdcConverter"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabCenter" width="110px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.PGIC0053_grid_label_fim}" style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.hrInicioEstatisticaProcessoFim}" converter="timestampPdcConverter"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="120px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.PGIC0053_grid_label_situacao_processamento}" style="width:120; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoEstatisticaSistema}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="100px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.PGIC0053_grid_label_volume_total_recebido}" style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.qtRegistroProcessoSistema}" converter="inteiroMilharConverter"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="100px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.PGIC0053_grid_label_volume_total_processado}" style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.vlRegistroProcessoSistema}" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>		
 	<br:brPanelGrid columns="0" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{estatisticaProcessamentoBean.pesquisa}" rendered="#{!empty estatisticaProcessamentoBean.listaEstatisticaProcessamento}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>	
    </br:brPanelGrid>
  </br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimpar" styleClass="bto1" value="Limpar" action="#{estatisticaProcessamentoBean.limpar}" style="margin-right:5px" disabled="#{empty estatisticaProcessamentoBean.listaEstatisticaProcessamento}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>

<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
