<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterAssociacaoLinhaTipoMensagemForm" name="manterAssociacaoLinhaTipoMensagemForm">
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterAssociacaoLinhaTipoMensagemBean.obrigatoriedade}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" >
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup> 
	</br:brPanelGrid>   
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.associacaoLinhasPorTipoMensagemIncluir_label_associacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="text-align:left" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_de_mensagem}:"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brSelectOneMenu id="mensagem" value="#{manterAssociacaoLinhaTipoMensagemBean.tipoMensagem}" >
					<f:selectItem itemValue="" itemLabel="#{msgs.associacaoLinhasPorTipoMensagem_selecione}"/>
					<f:selectItems value="#{manterAssociacaoLinhaTipoMensagemBean.listaTipoMensagem}"/>
					<brArq:commonsValidator type="integer"  arg="#{msgs.label_mensagem}" server="false" client="true" />
				</br:brSelectOneMenu>	
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.associacaoLinhasPorTipoMensagem_centro_custo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
   			    <br:brPanelGroup>
   			    	<br:brInputText styleClass="HtmlInputTextBradesco"  value="#{manterAssociacaoLinhaTipoMensagemBean.centroCustoPesq}" size="10" maxlength="4" id="txtInicioCentroCusto">
				    </br:brInputText>	
   				</br:brPanelGroup>					
   				 <br:brPanelGroup>   				 
					<h:commandLink id="link" action="#{manterAssociacaoLinhaTipoMensagemBean.buscarCentroCusto}" >
						<br:brGraphicImage url="/images/ficha.gif" style="margin-right:5px;margin-left:5px"   />
					</h:commandLink>   				 
   				</br:brPanelGroup>	
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="centroCusto" value="#{manterAssociacaoLinhaTipoMensagemBean.centroCusto}" disabled="#{empty manterAssociacaoLinhaTipoMensagemBean.listaCentroCusto}" >
						<f:selectItem itemValue="" itemLabel="#{msgs.associacaoLinhasPorTipoMensagem_selecione}"/>
						<f:selectItems value="#{manterAssociacaoLinhaTipoMensagemBean.listaCentroCusto}"/>
					</br:brSelectOneMenu>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>	
		
		
		  <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.associacaoLinhasPorTipoMensagemIncluir_label_recurso}:"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brSelectOneMenu id="recurso" value="#{manterAssociacaoLinhaTipoMensagemBean.recurso}">
					<f:selectItem itemValue="0" itemLabel="#{msgs.associacaoLinhasPorTipoMensagem_label_combo_selecione}"/>
					<f:selectItems value="#{manterAssociacaoLinhaTipoMensagemBean.listaRecurso}"/>
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.associacaoLinhasPorTipoMensagemIncluir_label_idioma}:"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brSelectOneMenu id="idioma" value="#{manterAssociacaoLinhaTipoMensagemBean.idioma}">
					<f:selectItem itemValue="0" itemLabel="#{msgs.associacaoLinhasPorTipoMensagem_label_combo_selecione}"/>
					<f:selectItems value="#{manterAssociacaoLinhaTipoMensagemBean.listaIdioma}"/>
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.associacaoLinhasPorTipoMensagemIncluir_label_codigo_mensagem}:"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>			
				  <br:brInputText id="codigoMensagem" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{manterAssociacaoLinhaTipoMensagemBean.entradaMensagem.nrEventoMensagemNegocio}" size="20" maxlength="4" >  	
					<brArq:commonsValidator type="integer"  arg="#{msgs.associacaoLinhasPorTipoMensagemIncluir_label_codigo_mensagem}" server="false" client="true" /> 											
				</br:brInputText>	
			</br:brPanelGroup>
		</br:brPanelGrid>					
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.associacaoLinhasPorTipoMensagemIncluir_label_ordem_linha_mensagem}:"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
			  <br:brInputText id="ordemLinhaMensagem" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{manterAssociacaoLinhaTipoMensagemBean.entradaMensagem.ordemLinhaMensagem}" size="20" maxlength="10" onfocus="cleanClipboard();" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.associacaoLinhasPorTipoMensagem_botao_voltar}" action="#{manterAssociacaoLinhaTipoMensagemBean.voltarPesquisa}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton id="btnAvancar" disabled="false" styleClass="bto1" value="#{msgs.associacaoLinhasPorTipoMensagem_botao_avancar}" action="#{manterAssociacaoLinhaTipoMensagemBean.avancarIncluir}" onclick="javascript: validar(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.associacaoLinhasPorTipoMensagemPesquisar_label_tipo_mensagem}','#{msgs.associacaoLinhasPorTipoMensagem_centro_custo}','#{msgs.associacaoLinhasPorTipoMensagemIncluir_check_recurso}','#{msgs.associacaoLinhasPorTipoMensagemIncluir_check_idioma}', '#{msgs.associacaoLinhasPorTipoMensagemIncluir_check_codigoMensagem}', '#{msgs.associacaoLinhasPorTipoMensagemIncluir_check_ordem_linha_mensagem}'); return validateForm(document.forms[1]); " >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	
	