 <%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterAssociacaoLinhaTipoMensagemForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup> 
	</br:brPanelGrid>   
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:left" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_de_mensagem}:"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>		
				<br:brSelectOneMenu id="tipoMensagem" value="#{manterAssociacaoLinhaTipoMensagemBean.tipoMensagemFiltro}" disabled="#{manterAssociacaoLinhaTipoMensagemBean.btoAcionado}">
					<f:selectItem itemValue="0" itemLabel="#{msgs.associacaoLinhasPorTipoMensagem_selecione}"/>
					<f:selectItems value="#{manterAssociacaoLinhaTipoMensagemBean.listaTipoMensagem}"/>
				</br:brSelectOneMenu>		
			</br:brPanelGroup>
		</br:brPanelGrid>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGroup>
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.associacaoLinhasPorTipoMensagem_centro_custo}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
		    <br:brPanelGroup>
		    	<br:brInputText styleClass="HtmlInputTextBradesco"  value="#{manterAssociacaoLinhaTipoMensagemBean.centroCustoPesq}" disabled="#{manterAssociacaoLinhaTipoMensagemBean.btoAcionado}" size="10" maxlength="4" id="txtInicioCentroCusto">
			    </br:brInputText>	
			</br:brPanelGroup>					
			 <br:brPanelGroup>   				 
				<h:commandLink id="link" action="#{manterAssociacaoLinhaTipoMensagemBean.buscarCentroCusto}" >
					<br:brGraphicImage url="/images/ficha.gif" style="margin-right:5px;margin-left:5px"   />
				</h:commandLink>   				 
			</br:brPanelGroup>	
		    <br:brPanelGroup>
				<br:brSelectOneMenu id="centroCusto" value="#{manterAssociacaoLinhaTipoMensagemBean.centroCusto}" disabled="#{empty manterAssociacaoLinhaTipoMensagemBean.listaCentroCusto || manterAssociacaoLinhaTipoMensagemBean.btoAcionado}" >
					<f:selectItem itemValue="" itemLabel="#{msgs.associacaoLinhasPorTipoMensagem_selecione}"/>
					<f:selectItems value="#{manterAssociacaoLinhaTipoMensagemBean.listaCentroCusto}"/>
				</br:brSelectOneMenu>	
			</br:brPanelGroup>					
		</br:brPanelGrid>
	</br:brPanelGroup>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.associacaoLinhasPorTipoMensagemPesquisar_botao_limpar_campos}" action="#{manterAssociacaoLinhaTipoMensagemBean.limparCampos}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.associacaoLinhasPorTipoMensagemPesquisar_botao_consultar}" action="#{manterAssociacaoLinhaTipoMensagemBean.carregaLista}"  onclick="javascript: return validateForm(document.forms[1]);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170px">	
		<app:scrollableDataTable  id="dataTable" value="#{manterAssociacaoLinhaTipoMensagemBean.listaPesquisaMensagem}" var="result" 
			rows="10" 
			rowClasses="tabela_celula_normal, tabela_celula_destaque" 			
			width="100%"
			 rowIndexVar="parametroKey">
			<app:scrollableColumn  styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
				<t:selectOneRadio   id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false"
					value="#{manterAssociacaoLinhaTipoMensagemBean.itemSelecionadoLista}" >
					<f:selectItems value="#{manterAssociacaoLinhaTipoMensagemBean.listaControleRadio}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sorLista" index="#{parametroKey}"  />
			  </app:scrollableColumn>
			  <app:scrollableColumn  width="300px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.detManterAssociacaoMsgComprovanteSalarial_tipo_mensagem}"  style="text-align:center;width:300px" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdTipoMensagem} - #{result.dsTipoMensagem}"/>
			  </app:scrollableColumn>
			  <app:scrollableColumn  width="80px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.associacaoLinhasPorTipoMensagem_centro_custo}" style="text-align:center;width:80px" />
			    </f:facet>
			    <br:brOutputText value="#{result.prefixo}"/>
			  </app:scrollableColumn>
			  <app:scrollableColumn  width="80px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.associacaoLinhasPorTipoMensagemPesquisar_grid_idioma}"style="text-align:center;width:80px"  />
			    </f:facet>
			    <br:brOutputText value="#{result.dsIdioma}"/>
			  </app:scrollableColumn>
			  <app:scrollableColumn  width="80px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.associacaoLinhasPorTipoMensagemPesquisar_grid_recurso}" style="text-align:center;width:80px"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsRecurso}" />
			 </app:scrollableColumn>
			 <app:scrollableColumn  width="100px" styleClass="colTabRight">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.associacaoLinhasPorTipoMensagemPesquisar_grid_codigo}" style="text-align:center;width:100px" />
			    </f:facet>
			    <br:brOutputText value="#{result.codigoLinhaMsgComprovante}" />
			  </app:scrollableColumn>
			  <app:scrollableColumn  width="250px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.associacaoLinhasPorTipoMensagemPesquisar_grid_mensagem}" style="text-align:center;width:250px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsMensagemComprovante}" />
			  </app:scrollableColumn>
			  <app:scrollableColumn  width="120px" styleClass="colTabRight">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.associacaoLinhasPorTipoMensagemPesquisar_grid_ordem_linha}" style="text-align:center;width:120px"/>
			    </f:facet>
			    <br:brOutputText value="#{result.ordemLinha}" />
		
			  </app:scrollableColumn>
		   </app:scrollableDataTable >	
		  
			</br:brPanelGroup>
		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterAssociacaoLinhaTipoMensagemBean.pesquisar}" >
			   <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			     styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			     styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
	<br:brPanelGrid columns="4" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
		<br:brPanelGroup style="width:750px">
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterAssociacaoLinhaTipoMensagemBean.itemSelecionadoLista}"  style="margin-right:5px;" value="#{msgs.associacaoLinhasPorTipoMensagemPesquisar_botao_detalhar}" action="#{manterAssociacaoLinhaTipoMensagemBean.detalhar}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="bto1"  style="margin-right:5px;" value="#{msgs.associacaoLinhasPorTipoMensagemPesquisar_botao_incluir}" action="#{manterAssociacaoLinhaTipoMensagemBean.incluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAlterar" styleClass="bto1" disabled="#{empty manterAssociacaoLinhaTipoMensagemBean.itemSelecionadoLista}"  style="margin-right:5px;" value="#{msgs.associacaoLinhasPorTipoMensagemPesquisar_botao_alterar}" action="#{manterAssociacaoLinhaTipoMensagemBean.alterar}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty manterAssociacaoLinhaTipoMensagemBean.itemSelecionadoLista}"  value="#{msgs.associacaoLinhasPorTipoMensagemPesquisar_botao_excluir}" action="#{manterAssociacaoLinhaTipoMensagemBean.excluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>		
	</br:brPanelGrid>	
    </a4j:outputPanel>
    
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>	
</brArq:form>	
	
	