<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterConEnvioArqFormaLiqForm" name="conManterConEnvioArqFormaLiqForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" style="margin-left:5px">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_argumentopesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
    
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_forma_liquidacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>		
					<br:brSelectOneMenu id="formaLiquidacao" value="#{manterControleEnvioArqFormaLiqBean.cdFormaLiquidacaoFiltro}" disabled="#{manterControleEnvioArqFormaLiqBean.btoAcionado}"  >
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterControleEnvioArqFormaLiqBean.listaFormaLiquidacao}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>	
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
    
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sequencia_envio}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtSequenciaEnvio" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{manterControleEnvioArqFormaLiqBean.btoAcionado}"   onkeypress="onlyNum();" size="10" maxlength="2" value="#{manterControleEnvioArqFormaLiqBean.sequenciaEnvioFiltro}" styleClass="HtmlInputTextBradesco" />
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	

	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.btn_limpar_campos}" action="#{manterControleEnvioArqFormaLiqBean.limparCampos}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" style="margin-left:5px" value="#{msgs.btn_consultar}" action="#{manterControleEnvioArqFormaLiqBean.consultar}"  disabled="#{manterControleEnvioArqFormaLiqBean.btoAcionado}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0"  >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px;  height:170" >	
			<app:scrollableDataTable id="dataTable" value="#{manterControleEnvioArqFormaLiqBean.listaGrid}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
			
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>		
					<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterControleEnvioArqFormaLiqBean.itemSelecionado}">
						<f:selectItems value="#{manterControleEnvioArqFormaLiqBean.listaControle}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>		
				
				<app:scrollableColumn  width="150px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_cod_forma_liquidacao}" style="width:150;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdFormaLiquidacao}"  styleClass="tableFontStyle" />
				</app:scrollableColumn>
	
				<app:scrollableColumn  width="240px" styleClass="colTabLeft">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_descricao_forma_liquidacao}" style="width:240;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsFormaLiquidacao}"  styleClass="tableFontStyle" />
				</app:scrollableColumn>
	
				<app:scrollableColumn  width="150px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_sequencia_envio}" style="width:150;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.nrEnvioFormaLiquidacaoDoc}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="100px" styleClass="colTabCenter" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_horario}" style="width:100%;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.hrEnvioFormaLiquidacaoDoc}"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>				 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable"actionListener="#{manterControleEnvioArqFormaLiqBean.pesquisar}" > 
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
		<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton disabled="#{empty manterControleEnvioArqFormaLiqBean.itemSelecionado}" style="margin-right:5px" id="btnDetalhar" styleClass="bto1" value="#{msgs.btn_detalhar}" action="#{manterControleEnvioArqFormaLiqBean.detalhar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" styleClass="bto1" style="margin-right:5px"  value="#{msgs.btn_incluir}"  action="#{manterControleEnvioArqFormaLiqBean.incluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir"  disabled="#{empty manterControleEnvioArqFormaLiqBean.itemSelecionado}" styleClass="bto1" value="#{msgs.btn_excluir}" action="#{manterControleEnvioArqFormaLiqBean.excluir}" >							
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</a4j:outputPanel>
	
<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
