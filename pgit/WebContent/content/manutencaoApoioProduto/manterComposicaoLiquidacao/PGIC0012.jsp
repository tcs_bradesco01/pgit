<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>

<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j"%>

<brArq:form id="incluirManterComposicaoForm" name="incluirManterComposicaoForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterComposicaoBean.obrigatoriedadeIncluir}"/>

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.PGIC0011_title_forma_liquidacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0010_label_forma_liquidacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<br:brSelectOneMenu id="formaLiquidacao" value="#{manterComposicaoBean.formaLiquidacao}" >
				<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0010_label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.PGIC0010_label_combo_selecione1}"/>
				<f:selectItems value="#{manterComposicaoBean.listaFormaLiquidacao}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.PGIC0010_label_forma_liquidacao}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0010_label_produto}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<br:brSelectOneMenu id="produto" value="#{manterComposicaoBean.produto}" >
				<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0010_label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="Servi�o1"/>
				<f:selectItem itemValue="2" itemLabel="Servi�o2"/>
				<f:selectItem itemValue="3" itemLabel="Servi�o3"/>
				<f:selectItems value="#{manterComposicaoBean.listaProduto}"/>				
				<a4j:support event="onchange" ajaxSingle="true"	action="#{manterComposicaoBean.getListaOperacao}" reRender="produto,operacao" />				
				<brArq:commonsValidator type="required" arg="#{msgs.PGIC0010_label_produto}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0010_label_operacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<br:brSelectOneMenu id="operacao" value="#{manterComposicaoBean.operacao}" disabled="#{manterComposicaoBean.comboServicoRelac}">
				<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0010_label_combo_selecione}"/>				
				<f:selectItems value="#{manterComposicaoBean.listaOperacao}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.PGIC0010_label_operacao}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0010_label_tipo_servico}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<br:brSelectOneMenu id="tipoServico" value="#{manterComposicaoBean.tipoServico}" >
				<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0010_label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.PGIC0010_label_combo_selecione1}"/>
				<f:selectItems value="#{manterComposicaoBean.listaTipoServico}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.PGIC0010_label_tipo_servico}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0010_label_forma_lancamento}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
	    	<br:brSelectOneMenu id="formaLancamento" value="#{manterComposicaoBean.formaLancamento}" >
				<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0010_label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.PGIC0010_label_combo_selecione1}"/>
				<f:selectItems value="#{manterComposicaoBean.listaFormaLancamento}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.PGIC0010_label_forma_lancamento}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="HtmlCommandButtonBradesco" style="align:left" value="#{msgs.botao_voltar}" action="VOLTAR_DADOS">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="HtmlCommandButtonBradesco" disabled="false"  value="#{msgs.PGIC0012_label_botao_avancar}"  action="#{manterComposicaoBean.avancarIncluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		  
	<br:brPanelGrid columns="1" style="height:25px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
