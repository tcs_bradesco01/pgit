<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j"%>


<brArq:form id="pesqManterComposicaoForm" name="pesqManterComposicaoForm" >

<h:inputHidden id="hiddenRadio" value="#{manterComposicaoBean.codListaRadio}"/>
<h:inputHidden id="hiddenRadioFiltro" value="#{manterComposicaoBean.filtro}"/>


<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio onclick="javascript:habilitarCamposFiltroManterComposicao(document.forms[1], this);" id="filtroRadio" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="0" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0010_label_forma_liquidacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="formaLiquidacao" value="#{manterComposicaoBean.formaLiquidacaoFiltro}" disabled="true">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0010_label_combo_selecione}"/>
						<f:selectItem itemValue="1" itemLabel="#{msgs.PGIC0010_label_combo_selecione1}"/>
						<f:selectItems value="#{manterComposicaoBean.listaFormaLiquidacao}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0010_label_produto}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="produto" value="#{manterComposicaoBean.produtoFiltro}" disabled="#{manterComposicaoBean.disabledSelecao1}">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0010_label_combo_selecione}"/>
						<f:selectItem itemValue="1" itemLabel="Item 1"/>
						<f:selectItem itemValue="2" itemLabel="Item 2"/>
						<f:selectItem itemValue="3" itemLabel="Item 3"/>					
						<a4j:support event="onchange" ajaxSingle="true"	action="#{manterComposicaoBean.getListaOperacaoFiltro}" reRender="produto,operacao" />										
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0010_label_operacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="operacao" value="#{manterComposicaoBean.operacaoFiltro}" disabled="#{manterComposicaoBean.disabledSelecao2}">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0010_label_combo_selecione}"/>
						<f:selectItems value="#{manterComposicaoBean.listaOperacaoFiltro}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="1" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0010_label_tipo_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="tipoServico" value="#{manterComposicaoBean.tipoServicoFiltro}" disabled="true">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0010_label_combo_selecione}"/>
						<f:selectItem itemValue="1" itemLabel="#{msgs.PGIC0010_label_combo_selecione1}"/>
						<f:selectItems value="#{manterComposicaoBean.listaTipoServico}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0010_label_forma_lancamento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="formaLancamento" value="#{manterComposicaoBean.formaLancamentoFiltro}" disabled="true">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0010_label_combo_selecione}"/>
						<f:selectItem itemValue="1" itemLabel="#{msgs.PGIC0010_label_combo_selecione1}"/>
						<f:selectItems value="#{manterComposicaoBean.listaFormaLancamento}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>		
		
	</br:brPanelGrid>	
		
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="HtmlCommandButtonBradesco" value="#{msgs.PGIC0010_label_botao_limpar_dados}" action="#{manterComposicaoBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="HtmlCommandButtonBradesco" value="#{msgs.PGIC0010_label_botao_consultar}" action="#{manterComposicaoBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
			<f:verbatim> <div id="rolagem" style="width:750px; overflow-x:scroll"></f:verbatim> 
			<t:dataTable id="dataTable" value="#{manterComposicaoBean.listaGridComposicao}" var="result" rows="10" 
			cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
			columnClasses="alinhamento_centro, alinhamento_esquerda, alinhamento_esquerda, alinhamento_esquerda, alinhamento_esquerda, alinhamento_esquerda"
			headerClass="tabela_celula_destaque_acentuado" width="800px">		
			<t:column width="30px" style="padding-right:5px; padding-left:5px">
				<f:facet name="header">
			      <br:brOutputText value="" escape="false" />
			    </f:facet>		
				<t:selectOneRadio  onclick="javascript:habilitarBotaosPesqManterComposicao(document.forms[1], this);" id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
					<f:selectItems value="#{manterComposicaoBean.listaControleRadio}"/>
				</t:selectOneRadio>
		    	<t:radio for="sorLista" index="#{result.totalRegistros}" />
			</t:column>
			  <t:column width="160px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0010_label_forma_liquidacao}"  />
			    </f:facet>
			    <br:brOutputText value="#{result.cpf_completo}" />
			  </t:column>
			  <t:column width="160px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0010_label_produto}"  />
			    </f:facet>
			    <br:brOutputText value="#{result.cpf_completo}" />
			  </t:column>
			  <t:column width="160px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0010_label_operacao}"  />
			    </f:facet>
			    <br:brOutputText value="#{result.cpf_completo}" />
			  </t:column>
			  <t:column width="160px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0010_label_tipo_servico}"  />
			    </f:facet>
			    <br:brOutputText value="#{result.cpf_completo}" />
			  </t:column>
			  <t:column width="250px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0010_label_forma_lancamento}" />
			    </f:facet>
			    <br:brOutputText value="#{result.cpf_completo}" />
			  </t:column>
			</t:dataTable>		
			<f:verbatim> </div> </f:verbatim>	
			
		</br:brPanelGroup>
		
	</br:brPanelGrid>		
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{arquivoRetornoBean.pesquisar}" rendered="#{arquivoRetornoBean.listaGridRetorno!= null && cmpi0001Bean.mostraBotoes0001}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="HtmlCommandButtonBradesco"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	 
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >
			<br:brCommandButton id="btnDetalhar" styleClass="HtmlCommandButtonBradesco" disabled="false"  style="margin-right:5px" value="#{msgs.PGIC0010_label_botao_detalhar}"  action="#{manterComposicaoBean.detalhar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="HtmlCommandButtonBradesco" disabled="false"  style="margin-right:5px" value="#{msgs.PGIC0010_label_botao_incluir}"  action="#{manterComposicaoBean.incluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" styleClass="HtmlCommandButtonBradesco" disabled="false"  style="margin-right:5px" value="#{msgs.PGIC0010_label_botao_excluir}"  action="#{manterComposicaoBean.excluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="height:350px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>

<f:verbatim>  
	
<script language="javascript"> 

	var campos, hidden, valorRadioSelecionado;
	campos = '';
	
	var formulario = document.forms[1];

	for(i=0; i<formulario.elements.length; i++)
	{
		/* Rotina para receber o valor do Hidden e passa-lo ao Radio */
		if (formulario.elements[i].id == 'pesqManterComposicaoForm:hiddenRadioFiltro'){
			valorRadioSelecionado = formulario.elements[i].value
		}		

		if (formulario.elements[i].name == 'filtroRadio'){		
			
			//Se valor do Hidden for igual ao Radio em foco, checamos o radio
			if ( formulario.elements[i].value == valorRadioSelecionado ) {
			   formulario.elements[i].checked = true;

         	   /*Comando para sair do loop,ganha desempenho e impede de executar esta rotina novamente
	 		   devido ao for dentro do form	*/
			   break;	
			}
	 		else
			   formulario.elements[i].checked = false;
		}
	}	
	
function habilitarCamposFiltroManterComposicao(form, radio){	
	var hidden;

	if ( radio.value == 1 ) {
		for(i=0; i<form.elements.length; i++){
			if (form.elements[i].id == 'pesqManterComposicaoForm:hiddenRadioFiltro'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			
			if (form.elements[i].id == 'pesqManterComposicaoForm:formaLiquidacao'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:produto'){
				form.elements[i].disabled=false;
			}			
			if (form.elements[i].id == 'pesqManterComposicaoForm:tipoServico'){
				form.elements[i].value = '';
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:formaLancamento'){
				form.elements[i].value = '';
				form.elements[i].disabled=true;
			}
		}
	}
	
	if ( radio.value == 2 ) {
		for(i=0; i<form.elements.length; i++){
			if (form.elements[i].id == 'pesqManterComposicaoForm:hiddenRadioFiltro'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:formaLiquidacao'){
				form.elements[i].value = '';
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:produto'){
				form.elements[i].value = '';
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:operacao'){
				form.elements[i].value = '';
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:tipoServico'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:formaLancamento'){
				form.elements[i].disabled=false;
			}
		}
	}
}			

	
</script>

</f:verbatim>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
