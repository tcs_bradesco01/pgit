<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="incluirManterPrioridadeCreditoTipoCompromissoForm" name="incluirManterPrioridadeCreditoTipoCompromissoForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterPrioridadeCredTipoCompBean.obrigatoriedade}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_tipo_compromisso}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterPrioridadeCredTipoCompBean.tipoCompromisso}" size="4" maxlength="3" id="tipoCompromisso" onkeypress="onlyNum()">
					<brArq:commonsValidator type="integer" arg="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_tipo_compromisso}" server="false" client="true" />
				    </br:brInputText>
				</br:brPanelGroup>					
			</br:brPanelGrid>		
		</br:brPanelGroup>	
	</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_centrocusto}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		 <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<a4j:region renderRegionOnly="true">
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterPrioridadeCredTipoCompBean.centroCustoLupaFiltro}"  size="5" maxlength="4" id="centroCustoFiltro"/>
					<f:verbatim>&nbsp;</f:verbatim>
		 			<br:brCommandButton id="btnCentroCusto" style="cursor: hand;" image="/images/lupa.gif" value=""  action="#{manterPrioridadeCredTipoCompBean.obterCentroCusto}" onclick="javascript: desbloquearTela(); return checaCentroCusto(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_centrocusto}');" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>					
					<f:verbatim>&nbsp;</f:verbatim>
					<f:verbatim>&nbsp;</f:verbatim>
					<br:brSelectOneMenu id="centroCusto" value="#{manterPrioridadeCredTipoCompBean.cdSistema}" disabled="#{empty manterPrioridadeCredTipoCompBean.listaCentroCustoLupa}" >
						<f:selectItem itemValue="" itemLabel="#{msgs.conCadastroProcessosControle_label_combo_selecione}"/>
						<f:selectItems value="#{manterPrioridadeCredTipoCompBean.listaCentroCustoLupaSelectItem}" />			
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
			</a4j:region>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_prioridade}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterPrioridadeCredTipoCompBean.prioridade}" size="3" maxlength="2" id="prioridade" onkeypress="onlyNum()">
					<brArq:commonsValidator type="integer" arg="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_prioridade}" server="false" client="true" />
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		</br:brPanelGrid>		
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	
	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_data_inicio_vigencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
					<app:calendar id="dataVigencia" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" value="#{manterPrioridadeCredTipoCompBean.dataInicioVigencia}" >
					</app:calendar>
				</br:brPanelGroup>				
				</br:brPanelGrid>
			</br:brPanelGroup>	
	
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_data_final_vigencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
					<app:calendar id="dataFinalVigencia" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" value="#{manterPrioridadeCredTipoCompBean.dataFimVigencia}" >
					</app:calendar>
				</br:brPanelGroup>				
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			
		</br:brPanelGrid>	
		</a4j:outputPanel>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterPrioridadeCredTipoCompBean.voltarPesquisar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.incManterPrioridadeCreditoProduto_label_botao_avancar}"
				onclick="javascript:  checaCampoObrigatorioIncluir(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_tipo_compromisso}', '#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_prioridade}', '#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_data_inicio_vigencia}', '#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_centrocusto}');return validateForm(document.forms[1]);"
				action="#{manterPrioridadeCredTipoCompBean.avancarIncluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>	

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
