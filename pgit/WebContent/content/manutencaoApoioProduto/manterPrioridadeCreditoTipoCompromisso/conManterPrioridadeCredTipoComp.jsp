<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>


<brArq:form id="pesqManterPrioridadeCreditoTipoCompromissoForm" name="pesqManterPrioridadeCreditoTipoCompromissoForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina"  cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_tipo_compromisso}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup></br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup id="panelCompromisso">
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterPrioridadeCredTipoCompBean.tipoCompromissoFiltro}"  size="4" maxlength="3" id="tipoCompromisso" onkeypress="javascript: onlyNum();limpaCampos(document.forms[1]);" onchange="javascript:limpaCampos(document.forms[1]); validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
						<a4j:support event="onblur" reRender="panelDataInicioVigencia"  />
						<brArq:commonsValidator type="integer" arg="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_tipo_compromisso}" server="false" client="true" />
				    </br:brInputText>	
				</br:brPanelGroup>	
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMsgAlertaGestor_label_centro_custo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<a4j:region renderRegionOnly="true">
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterPrioridadeCredTipoCompBean.inicialCentroCustoFiltro}" size="5" maxlength="4" id="txtInicioCentroCusto" />
					<f:verbatim>&nbsp;</f:verbatim>
					<br:brGraphicImage url="/images/lupa.gif" style="cursor: hand;">
						<a4j:support event="onclick" action="#{manterPrioridadeCredTipoCompBean.buscarCentroCusto}" reRender="centroCusto" status="statusAguarde"/>
					</br:brGraphicImage>
					<f:verbatim>&nbsp;</f:verbatim>
					<f:verbatim>&nbsp;</f:verbatim>
					<br:brSelectOneMenu id="centroCusto" value="#{manterPrioridadeCredTipoCompBean.centroCustoFiltro}"   disabled="#{empty manterPrioridadeCredTipoCompBean.listaCentroCustoFiltro}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conManterMsgAlertaGestor_label_combo_selecione}"/>
						<f:selectItems value="#{manterPrioridadeCredTipoCompBean.listaCentroCustoFiltro}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>
			</a4j:region>

		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_prioridade}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterPrioridadeCredTipoCompBean.prioridadeFiltro}" size="3" maxlength="2" onkeypress="onlyNum()" id="prioridade" >
						<brArq:commonsValidator type="integer" arg="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_prioridade}" server="false" client="true" />
				    </br:brInputText>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterPrioridadeCredTipoComp_dtInicioVig}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
					<app:calendar id="dtInicioVigencia" value="#{manterPrioridadeCredTipoCompBean.dtInicioVig}" />
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_botao_limpar_dados}" action="#{manterPrioridadeCredTipoCompBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_botao_consulta}" action="#{manterPrioridadeCredTipoCompBean.carregaLista}" onclick="javascript:return validateForm(document.forms[1]);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

   <br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 	
			<app:scrollableDataTable  id="dataTable" value="#{manterPrioridadeCredTipoCompBean.listaGridPesquisa}" var="result" rows="10" rowIndexVar="parametroKey"
			rowClasses="tabela_celula_normal, tabela_celula_destaque"  width="100%">
			 <app:scrollableColumn width="30px" styleClass="colTabCenter">
				 <f:facet name="header">
			      <br:brOutputText value=""  escape="false"  styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>	
			    
				<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
				  value="#{manterPrioridadeCredTipoCompBean.itemSelecionadoLista}">  
					<f:selectItems value="#{manterPrioridadeCredTipoCompBean.listaControleRadio}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />					
				</t:selectOneRadio>
				<t:radio for="sor" index="#{parametroKey}" />
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="140px"  styleClass="colTabRight">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_tipo_compromisso}" style="text-align:center;width:140"/>
			    </f:facet>
			    <br:brOutputText value="#{result.tipoCompromisso}" />
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="350px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterMsgAlertaGestor_label_centro_custo}" style="text-align:center;width:350"/>
			    </f:facet>
			    <br:brOutputText value="#{result.centroCusto}" />
			  </app:scrollableColumn>			  
			  
			 <app:scrollableColumn width="140px"  styleClass="colTabRight">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_prioridade}" style="text-align:center;width:140"/>
			    </f:facet>
			    <br:brOutputText value="#{result.prioridade}"/>
			  </app:scrollableColumn>
			  
			 <app:scrollableColumn width="180px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_data_inicio_vigencia}" style="text-align:center;width:180"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dataInicioVigencia}"/>
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="180px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_data_final_vigencia}" style="text-align:center;width:180"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dataFimVigencia}"/>
			  </app:scrollableColumn>
			  
			</app:scrollableDataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" > 	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterPrioridadeCredTipoCompBean.pesquisar}" >
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			       styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
 	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup >
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterPrioridadeCredTipoCompBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_detalhar}"  action="#{manterPrioridadeCredTipoCompBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_incluir}"  action="#{manterPrioridadeCredTipoCompBean.incluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" disabled="#{empty manterPrioridadeCredTipoCompBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_alterar}"  action="#{manterPrioridadeCredTipoCompBean.alterar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty manterPrioridadeCredTipoCompBean.itemSelecionadoLista}"  styleClass="bto1" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_excluir}"  action="#{manterPrioridadeCredTipoCompBean.excluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>	
	
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
