<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="excManterPrioridadeCreditoTipoCompromissoForm" name="excManterPrioridadeCreditoTipoCompromissoForm" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_tipo_compromisso}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPrioridadeCredTipoCompBean.tipoCompromisso}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excRetornoLayoutsArquivos_centro_custo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPrioridadeCredTipoCompBean.centroCusto}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_prioridade}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPrioridadeCredTipoCompBean.prioridade}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_data_inicio_vigencia}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPrioridadeCredTipoCompBean.dataInicioVigenciaDesc}" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_label_data_final_vigencia}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPrioridadeCredTipoCompBean.dataFinalVigenciaDesc}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterPrioridadeCredTipoCompBean.voltarPesquisar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" disabled="false"  value="#{msgs.incManterPrioridadeCreditoProduto_label_botao_confirmar}" 
                   onclick="javascript: if (!confirm('Confirma Exclus�o?')) { desbloquearTela(); return false; }" action="#{manterPrioridadeCredTipoCompBean.confirmarExcluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
	</br:brPanelGrid>
	
		
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
