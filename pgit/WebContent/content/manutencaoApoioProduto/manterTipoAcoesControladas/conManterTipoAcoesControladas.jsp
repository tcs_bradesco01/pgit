<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterTipoAcoesControladasForm" name="manterTipoAcoesControladasForm" >

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.manterTipoAcoesControladas_label_argsPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			 	<br:brPanelGroup >
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterTipoAcoesControladas_label_codigo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterTipoAcoesControladasBean.codigo}" size="20" id="txtCodigo"  style="margin-top:5px"/>  	
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="HtmlCommandButtonBradesco" value="#{msgs.manterTipoAcoesControladas_botao_limparCampos}" action="#{manterTipoAcoesControladasBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar"  styleClass="HtmlCommandButtonBradesco" value="#{msgs.manterTipoAcoesControladas_botao_consultar}" action="#{manterTipoAcoesControladasBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
	<br:brPanelGroup style="overflow-x:auto;width:750px" >	
			<app:scrollableDataTable id="dataTable" value="#{manterTipoAcoesControladasBean.listaGridAcoesControladas}" var="result" 
			rows="5" rowClasses="tabela_celula_normal, tabela_celula_destaque" 
			width="100%" >			

			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		
				<t:selectOneRadio  id="sorLista2" styleClass="HtmlSelectOneRadioBradesco" 
					layout="spread" forceId="true" forceIdIndex="false" 
					value="#{manterTipoAcoesControladasBean.codListaRadio}">
					<f:selectItems value="#{manterTipoAcoesControladasBean.listaControleRadioAcoes}"/>
					<a4j:support event="onclick" reRender="panelBotoes2" />
				</t:selectOneRadio>
		    	<t:radio for="sorLista2" index="#{result.linhaSelecionada}" />
			</app:scrollableColumn>

			<app:scrollableColumn  width="200px" styleClass="colTabRight" >
			    <f:facet name="header">
			     <h:outputText value="#{msgs.manterTipoAcoesControladas_label_codigo}"  />
			    </f:facet>
			    <br:brOutputText value="#{result.codigo}" />
			 </app:scrollableColumn>
			 
			<app:scrollableColumn  width="400px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.manterTipoAcoesControladas_label_descricao}"  />
			    </f:facet>
			    <br:brOutputText value="#{result.descricao}"  />
			 </app:scrollableColumn>
			</app:scrollableDataTable>
			<f:verbatim> </div> </f:verbatim>		
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterRegraSegregacaoAcessoDadosBean.pesquisarExcecao}" rendered="#{manterRegraSegregacaoAcessoDadosBean.listaGridExcecoes!= null && manterRegraSegregacaoAcessoDadosBean.mostraBotoes}">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="HtmlCommandButtonBradesco"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<a4j:outputPanel id="panelBotoes2" style="width: 100%; text-align: right" ajaxRendered="true">		
	<br:brPanelGrid columns="3" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
		<br:brPanelGroup style="width:750px">
			<br:brCommandButton id="btnLimpar"  disabled="#{empty manterTipoAcoesControladasBean.listaGridAcoesControladas}" styleClass="HtmlCommandButtonBradesco"  style="margin-right:5px;" value="#{msgs.manterTipoAcoesControladas_botao_limpar}" action="#{manterTipoAcoesControladasBean.limparDados2}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px;" value="#{msgs.manterTipoAcoesControladas_botao_incluir}" action="#{manterTipoAcoesControladasBean.incluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" disabled="#{empty manterTipoAcoesControladasBean.codListaRadio}" styleClass="HtmlCommandButtonBradesco"  value="#{msgs.manterTipoAcoesControladas_botao_excluir}" action="#{manterTipoAcoesControladasBean.excluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>						
		</br:brPanelGroup>		
	</br:brPanelGrid>
	</a4j:outputPanel>
	
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	
		