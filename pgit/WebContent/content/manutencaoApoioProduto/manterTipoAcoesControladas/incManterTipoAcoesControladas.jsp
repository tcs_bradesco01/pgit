<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>

<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterTipoAcoesControladasForm" name="manterTipoAcoesControladasForm" >

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.manterTipoAcoesControladas_label_eventoContabilPGIT}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			 	<br:brPanelGroup >
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterTipoAcoesControladas_label_numero}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup >
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterTipoAcoesControladasBean.numero}" size="20" id="txtNumero" style="margin-top:5px">  
						<brArq:commonsValidator type="integer" arg="#{msgs.manterTipoAcoesControladas_label_numero}" server="false" client="true" />
						<brArq:commonsValidator type="required" arg="#{msgs.manterTipoAcoesControladas_label_numero}" server="false" client="true"/>
				    </br:brInputText>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			 	<br:brPanelGroup >
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterTipoAcoesControladas_label_descricao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup >
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterTipoAcoesControladasBean.descricao}" size="40" id="txtDescricao" style="margin-top:5px">
						<brArq:commonsValidator type="required" arg="#{msgs.manterTipoAcoesControladas_label_descricao}" server="false" client="true"/>
				    </br:brInputText>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="HtmlCommandButtonBradesco" value="#{msgs.manterTipoAcoesControladas_botao_voltar}" action="#{manterTipoAcoesControladasBean.voltarPesquisa}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton id="btnLimparIncluir" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px;" value="#{msgs.manterTipoAcoesControladas_botao_limpar}" action="#{manterTipoAcoesControladasBean.limparIncluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton  id="btnIncluir" styleClass="HtmlCommandButtonBradesco" value="#{msgs.manterTipoAcoesControladas_botao_confirmar}" onclick="javascript: return validateForm(document.forms[1]);" action="#{manterTipoAcoesControladasBean.confirmarIncluir}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	
			