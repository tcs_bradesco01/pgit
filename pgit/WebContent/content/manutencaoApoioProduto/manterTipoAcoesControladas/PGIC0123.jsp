<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>

<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterTipoAcoesControladasForm" name="manterTipoAcoesControladasForm" >
<h:inputHidden id="hiddenRadio" value="#{manterTipoAcoesControladasBean.codListaRadio}"/>
<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.manterTipoAcoesControladas_label_argsPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			 	<br:brPanelGroup >
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterTipoAcoesControladas_label_codigo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterTipoAcoesControladasBean.codigo}" size="20" id="txtCodigo"  style="margin-top:5px"/>  	
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="HtmlCommandButtonBradesco" value="#{msgs.manterTipoAcoesControladas_botao_limparCampos}" action="#{manterTipoAcoesControladasBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar"  styleClass="HtmlCommandButtonBradesco" value="#{msgs.manterTipoAcoesControladas_botao_consultar}" action="#{manterTipoAcoesControladasBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
			<f:verbatim> <div id="rolagem" style="width:750px;  overflow:scroll"></f:verbatim> 
			<t:dataTable id="dataTable" value="#{manterTipoAcoesControladasBean.listaGridAcoesControladas}" var="result" rows="5" 
			cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
			columnClasses="alinhamento_centro, alinhamento_direita, alinhamento_direita, alinhamento_esquerda, alinhamento_direita, alinhamento_direita, alinhamento_esquerda, alinhamento_esquerda"
			headerClass="tabela_celula_destaque_acentuado" width="750px" >
			<t:column width="30px">
				<f:facet name="header">
			      <br:brOutputText value="" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>		
				<t:selectOneRadio  onclick="javascript:habilitarBotaosPesqAcoesControladas(document.forms[1], this);" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
					<f:selectItems value="#{manterTipoAcoesControladasBean.listaControleRadioAcoes}"/>
				</t:selectOneRadio>
		    	<t:radio for="sor" index="#{result.linhaSelecionada}" />
			</t:column>
			<t:column width="360px" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.manterRegraSegregacaoAcessoDados_grid_tipoUnidadeOrganizacionalUsuario}" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.codigo}" style="float:right;" />
			  </t:column>
			  <t:column width="360px" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.manterRegraSegregacaoAcessoDados_grid_tipoUnidadeOrganizacionalProprietarioDados}" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left; " escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.descricao}" style="float:left;" />
			  </t:column>
			</t:dataTable>		
			<f:verbatim> </div> </f:verbatim>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterRegraSegregacaoAcessoDadosBean.pesquisarExcecao}" rendered="#{manterRegraSegregacaoAcessoDadosBean.listaGridExcecoes!= null && manterRegraSegregacaoAcessoDadosBean.mostraBotoes}">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="HtmlCommandButtonBradesco"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>			
	<br:brPanelGrid columns="3" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
		<br:brPanelGroup style="width:750px">
			<br:brCommandButton id="btnLimpar"  styleClass="HtmlCommandButtonBradesco"  style="margin-right:5px;" value="#{msgs.manterTipoAcoesControladas_botao_limpar}" action="#{manterTipoAcoesControladasBean.limpar}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px;" value="#{msgs.manterTipoAcoesControladas_botao_incluir}" action="#{manterTipoAcoesControladasBean.incluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" disabled="true" styleClass="HtmlCommandButtonBradesco"  value="#{msgs.manterTipoAcoesControladas_botao_excluir}" action="#{manterTipoAcoesControladasBean.excluir}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>						
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	
		