<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="pesqManterCadastroTipoPendencia" name="pesqManterCadastroTipoPendencia" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterCadastroTipoPendencia_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup>
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterCadastroTipoPendencia_codigo_tipo_pendencia}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
		    <br:brPanelGroup>	
		    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroTipoPendenciaBean.filtroCodigoTipoPendencia}" disabled="#{manterCadastroTipoPendenciaBean.btoAcionado}" size="14" maxlength="10" id="txtTipoPendencia"
		    	 onkeyup="habilitaFiltros();" onkeypress="onlyNum();" onkeydown="habilitaFiltros();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');habilitaFiltros();"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGroup>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterCadastroTipoPendencia_title_area_responsavel}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterCadastroTipoPendencia_tipo_baixa}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup></br:brPanelGroup>				
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterCadastroTipoPendencia_indicador_responsabilidade}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="selTipoBaixa" value="#{manterCadastroTipoPendenciaBean.filtroTipoBaixa}" disabled="#{manterCadastroTipoPendenciaBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.conManterCadastroTipoPendencia_selecione}"/>
				<f:selectItem itemLabel="MANUAL" itemValue="1" />  
	            <f:selectItem itemLabel="AUTOM�TICA" itemValue="2" />
			</br:brSelectOneMenu>
	    </br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px;">
		</br:brPanelGroup>				
		
		<br:brPanelGroup>
	    	<br:brSelectOneMenu id="selResponsabilidade" value="#{manterCadastroTipoPendenciaBean.filtroIndicadorResponsabilidade}" disabled="#{manterCadastroTipoPendenciaBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.conManterCadastroTipoPendencia_selecione}"/>
				<f:selectItem itemLabel="DIN�MICA" itemValue="1" />  
	            <f:selectItem itemLabel="EST�TICA" itemValue="2" />
	            <a4j:support event="onchange" reRender="panelTipoUnidadeOrganizacional,panelConglomerado,panelUnidadeOrganizacional" action="#{manterCadastroTipoPendenciaBean.limpaConglomerado}"/> 
			</br:brSelectOneMenu>
	    </br:brPanelGroup>					
		
	</br:brPanelGrid>	
	 
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.conManterCadastroTipoPendencia_tipo_unidade_organizacional_responsavel}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-left:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup id="panelTipoUnidadeOrganizacional">
			<br:brSelectOneMenu id="filtroTipoUnidadeOrganizacional" value="#{manterCadastroTipoPendenciaBean.filtroTipoUnidadeOrganizacional}" disabled="#{manterCadastroTipoPendenciaBean.btoAcionado}" >
				<f:selectItem itemValue="0" itemLabel="#{msgs.conManterCadastroTipoPendencia_selecione}"/>
				<f:selectItems value="#{manterCadastroTipoPendenciaBean.listaTipoUnidadeOrganizacional}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>				
		
	 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.conManterCadastroTipoPendencia_empresa_do_conglomerado}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-left:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup id="panelConglomerado">
			<br:brSelectOneMenu id="empresaConglomerado" value="#{manterCadastroTipoPendenciaBean.filtroEmpresaConglomerado}" disabled="#{conManterCadastroTipoPendencia.btoAcionado}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.conManterCadastroTipoPendencia_selecione}"/>
				<f:selectItems value="#{manterCadastroTipoPendenciaBean.listaEmpresaConglomerado}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
			
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>				
		
	 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.conManterCadastroTipoPendencia_unidade_organizacional}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-left:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>			
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup id="panelUnidadeOrganizacional">
			<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroTipoPendenciaBean.filtroUnidadeOrganizacional}" disabled="#{manterCadastroTipoPendenciaBean.btoAcionado}" size="7" maxlength="5"
			 id="txtUnidadeOrganizacional" onkeypress="onlyNum()" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />  
		</br:brPanelGroup>	
			
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>				
		
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conManterCadastroTipoPendencia_btn_limpar_campos}" action="#{manterCadastroTipoPendenciaBean.limparCampos}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conManterCadastroTipoPendencia_btn_consultar}" action="#{manterCadastroTipoPendenciaBean.carregaLista}" onclick="javascript:if (!validaConsulta()) { desbloquearTela(); return false; };">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim> 

	<br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
			<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterCadastroTipoPendenciaBean.itemSelecionadoLista}" converter="cadTipoPendenciaConveter" onclick="javascript: habilitaBotoes()">
				<si:selectItems value="#{manterCadastroTipoPendenciaBean.listaPesquisa}" var="layout" itemLabel="" itemValue="#{layout}" />
			</t:selectOneRadio>
			<app:scrollableDataTable id="dataTable" value="#{manterCadastroTipoPendenciaBean.listaPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:radio for=":pesqManterCadastroTipoPendencia:sorLista" index="#{parametroKey}" /> 
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="100px" styleClass="colTabRight" > 
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterCadastroTipoPendencia_grid_codigo}" style="width:130; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.cdPendenciaPagamentoIntegrado}"/>
				 </app:scrollableColumn> 
				 
				<app:scrollableColumn  width="286px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterCadastroTipoPendencia_grid_descricao}"  style="width:286; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{result.dsPendenciaPagamentoIntegrado}"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn  width="260px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterCadastroTipoPendencia_grid_tipo_unidade_organizacional}" style="width:260; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsTipoUnidadeOrganizacional}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="200px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterCadastroTipoPendencia_grid_indicador_responsabilidade}" style="width:200; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsIndicadorResponsavelPagamento}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="200px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conManterCadastroTipoPendencia_grid_tipo_baixa}" style="width:200; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsTipoBaixaPendencia}" />
				</app:scrollableColumn>
				 
			</app:scrollableDataTable>	
		</br:brPanelGroup>		
	</br:brPanelGrid>		
	
<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup rendered="#{!empty manterCadastroTipoPendenciaBean.listaPesquisa}">
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterCadastroTipoPendenciaBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGroup id="panelBotoes" style="width: 100%; text-align: right">	 	
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton disabled="#{empty manterCadastroTipoPendenciaBean.itemSelecionadoLista}" id="btnDetalhar"  styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterCadastroTipoPendencia_btn_detalhar}" action="#{manterCadastroTipoPendenciaBean.avancarDetalhar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterCadastroTipoPendencia_btn_incluir}" action="#{manterCadastroTipoPendenciaBean.avancarIncluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton disabled="#{empty manterCadastroTipoPendenciaBean.itemSelecionadoLista}" id="btnAlterar" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterCadastroTipoPendencia_btn_alterar}"  action="#{manterCadastroTipoPendenciaBean.avancarAlterar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton disabled="#{empty manterCadastroTipoPendenciaBean.itemSelecionadoLista}" id="btnExcluir" styleClass="bto1" value="#{msgs.conManterCadastroTipoPendencia_btn_excluir}" action="#{manterCadastroTipoPendenciaBean.avancarExcluir}" >				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	</br:brPanelGroup>		
	
</br:brPanelGrid>
<f:verbatim><script>desabilitaBotoes()</script></f:verbatim>
</brArq:form>
