<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="pesqManterCadastroTipoPendenciaIncluir" name="pesqManterCadastroTipoPendenciaIncluir" >

<h:inputHidden id="hiddenObrigatoriedade" value="#{manterCadastroTipoPendenciaBean.obrigatoriedade}"/>
<h:inputHidden id="hiddenTipoBaixa" value="#{manterCadastroTipoPendenciaBean.tipoBaixa}"/>
<h:inputHidden id="hiddenIndicadorResponsabilidade" value="#{manterCadastroTipoPendenciaBean.indicadorResponsabilidade}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterCadastroTipoPendencia_title_tipo_pendencia}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0"  >	
		<br:brPanelGroup >
		<f:verbatim> <div style="height:100%"> </f:verbatim>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterCadastroTipoPendencia_codigo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px;" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroTipoPendenciaBean.codigo}" size="14" maxlength="10" id="txtCodigo" onkeypress="onlyNum()" 
			    	onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" >  
					<a4j:support event="onchange" reRender="panelEmail" />
			    	</br:brInputText>
			    	
				</br:brPanelGroup>
			</br:brPanelGrid>
		<f:verbatim> </div> </f:verbatim>			
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px;">
		</br:brPanelGroup>
		
		<br:brPanelGroup >
	    <f:verbatim> <div style="height:100%"> </f:verbatim>		
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterCadastroTipoPendencia_descricao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px;" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="text-align:left;">					
			    <br:brPanelGroup>	
			    	<br:brInputTextarea styleClass="HtmlInputTextBradesco" value="#{manterCadastroTipoPendenciaBean.descricao}"  rows="4" cols="50"
			    	 onkeydown="javascript:maxLength(this,100);" onkeyup="javascript:maxLength(this,100);"  onblur="javascript:maxLength(this,100);" id="txtDescricao"
			    	  />  
				</br:brPanelGroup>
			</br:brPanelGrid>
		<f:verbatim> </div> </f:verbatim>			
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px;">
		</br:brPanelGroup>
		
		<br:brPanelGroup >
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterCadastroTipoPendencia_observacoes}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px;" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="text-align:left;">					
			    <br:brPanelGroup>	
			    	<br:brInputTextarea styleClass="HtmlInputTextBradesco" value="#{manterCadastroTipoPendenciaBean.observacoes}"  rows="10" cols="50"
			    	 onkeydown="javascript:maxLength(this,200);" onkeyup="javascript:maxLength(this,200);"  onblur="javascript:maxLength(this,200);" id="txtObservacoes"
			    	 />  
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>						
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
		
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterCadastroTipoPendencia_title_area_responsavel}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.incManterCadastroTipoPendencia_tipo_baixa}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.incManterCadastroTipoPendencia_indicador_responsabilidade}:"/>
		</br:brPanelGroup>	
		
	
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGroup>
			<br:brSelectOneRadio id="radioTipoBaixa" styleClass="HtmlSelectOneRadioBradesco" value="#{manterCadastroTipoPendenciaBean.tipoBaixa}">
				<f:selectItem itemLabel="#{msgs.incManterCadastroTipoPendencia_radio_manual}" itemValue="1" />  
	            <f:selectItem itemLabel="#{msgs.incManterCadastroTipoPendencia_radio_automatica}" itemValue="2" />  
	            <a4j:support event="onclick" reRender="hiddenTipoBaixa" /> 
	    	</br:brSelectOneRadio>
	    </br:brPanelGroup>	
	    
	    <br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	    
		<br:brPanelGroup>
		
			<br:brSelectOneRadio id="radioIndicadorResponsabilidade" styleClass="HtmlSelectOneRadioBradesco" value="#{manterCadastroTipoPendenciaBean.indicadorResponsabilidade}">
				<f:selectItem itemLabel="#{msgs.incManterCadastroTipoPendencia_radio_dinamica}" itemValue="1" />  
	            <f:selectItem itemLabel="#{msgs.incManterCadastroTipoPendencia_radio_estatistica}" itemValue="2" />  
	            <a4j:support event="onclick" reRender="hiddenIndicadorResponsabilidade,panelTipoUnidade,panelConglomerado" action="#{manterCadastroTipoPendenciaBean.limpaConglomeradoAlterar}"/> 
	    	</br:brSelectOneRadio>

	    </br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.incManterCadastroTipoPendencia_tipo_unidade_organizacional_responsavel}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup id="panelTipoUnidade">
			<br:brSelectOneMenu id="inputTipoUnidadeOrganizacional" value="#{manterCadastroTipoPendenciaBean.tipoUnidadeOrganizacional}" disabled="#{manterCadastroTipoPendenciaBean.indicadorResponsabilidade != 2}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conManterCadastroTipoPendencia_selecione}"/>
				<f:selectItems value="#{manterCadastroTipoPendenciaBean.listaTipoUnidadeOrganizacional}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup id="panelConglomerado">
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Empresa do Conglomerado:"/>
				</br:brPanelGroup>
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:15;"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterCadastroTipoPendencia_unidade_organizacional}:"/>
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:25;"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterCadastroTipoPendencia_email}:"/>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brSelectOneMenu id="empresaConglomerado" value="#{manterCadastroTipoPendenciaBean.empresaConglomerado}" disabled="#{manterCadastroTipoPendenciaBean.indicadorResponsabilidade != 2}">
						<f:selectItems value="#{manterCadastroTipoPendenciaBean.listaEmpresaConglomerado}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>
				<br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroTipoPendenciaBean.unidadeOrganizacional}" size="7" maxlength="5" id="txtUnidadeOrganizacional" disabled="#{manterCadastroTipoPendenciaBean.indicadorResponsabilidade != 2}" onkeypress="onlyNum()"
			    	onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  style="margin-left:15;"/>  
				</br:brPanelGroup>
				<br:brPanelGroup id="panelEmail">	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroTipoPendenciaBean.email}" size="30" maxlength="70" id="txtEmail" style="margin-left:25;" 
			    			disabled="#{manterCadastroTipoPendenciaBean.codigo != 8 && manterCadastroTipoPendenciaBean.codigo != 9 && manterCadastroTipoPendenciaBean.codigo != 10 && manterCadastroTipoPendenciaBean.codigo != 11}" />  
				</br:brPanelGroup>
				
			</br:brPanelGrid>
			
		</br:brPanelGroup>	
	</br:brPanelGrid>			
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.incManterCadastroTipoPendencia_btn_voltar}" action="#{manterCadastroTipoPendenciaBean.voltarPesquisar}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" onclick="javascript:checaCamposObrigatoriosIncluir(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.incManterCadastroTipoPendencia_codigo}', '#{msgs.incManterCadastroTipoPendencia_descricao}', '#{msgs.incManterCadastroTipoPendencia_observacoes}', '#{msgs.incManterCadastroTipoPendencia_tipo_unidade_organizacional_responsavel}', '#{msgs.incManterCadastroTipoPendencia_tipo_baixa}', '#{msgs.incManterCadastroTipoPendencia_indicador_responsabilidade}', '#{msgs.incManterCadastroTipoPendencia_unidade_organizacional}','#{msgs.incManterCadastroTipoPendencia_email}', 'Empresa do Conglomerado');return validateForm(document.forms[1]);" value="#{msgs.incManterCadastroTipoPendencia_btn_avancar}" action="#{manterCadastroTipoPendenciaBean.avancarIncluirConfirmar}" >				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
</br:brPanelGrid>

	
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>