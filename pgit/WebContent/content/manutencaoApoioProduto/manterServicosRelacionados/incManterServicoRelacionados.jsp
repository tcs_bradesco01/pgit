<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="incManterServicosRelacionados" name="incManterServicosRelacionados" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterServicosRelacionadosBean.obrigatoriedade}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid> 

	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_label_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="tipoServico" value="#{manterServicosRelacionadosBean.tipoServico}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_combo_selecione}"/>
						<f:selectItems value="#{manterServicosRelacionadosBean.listarArgumentoPesquisaServico}"/>	
						<a4j:support event="onchange" reRender="modalidadeServico" action="#{manterServicosRelacionadosBean.listarModalidadeServico}"/>		
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_label_servico_relacionado}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="modalidadeServico" value="#{manterServicosRelacionadosBean.servicoRelacionado}" disabled="#{manterServicosRelacionadosBean.tipoServico == 0 || manterServicosRelacionadosBean.tipoServico == null}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_combo_selecione}"/>
						<f:selectItems value="#{manterServicosRelacionadosBean.listaModalidadeServico}"/>			
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
				
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_label_servico_composto}:"/>
				</br:brPanelGroup>	
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brSelectOneMenu id="servicoComposto" converter="longConverter" value="#{manterServicosRelacionadosBean.servicoComposto}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_combo_selecione}"/>
						<f:selectItems value="#{manterServicosRelacionadosBean.listaServicoComposto}"/>			
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_label_natureza_servico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="naturezaServico" value="#{manterServicosRelacionadosBean.naturezaServico}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_combo_selecione}"/>
							<f:selectItem itemValue="1" itemLabel="#{msgs.manterServicoRelacionado_combo_pagamento}"/>
							<f:selectItem itemValue="2" itemLabel="#{msgs.manterServicoRelacionado_combo_avisos}"/>	
							<f:selectItem itemValue="3" itemLabel="#{msgs.manterServicoRelacionado_combo_comprovantes}"/>
							<f:selectItem itemValue="4" itemLabel="#{msgs.manterServicoRelacionado_combo_recadastramento}"/>
							<f:selectItem itemValue="5" itemLabel="#{msgs.manterServicoRelacionado_combo_cadastramento_favorecido}"/>
							<f:selectItem itemValue="6" itemLabel="#{msgs.manterServicoRelacionado_combo_rastreamento_titulos}"/>	
							<f:selectItem itemValue="7" itemLabel="#{msgs.manterServicoRelacionado_combo_prova_vida}"/>
							<f:selectItem itemValue="8" itemLabel="#{msgs.manterServicoRelacionado_combo_emissao_comprovante_salarial}"/>
									
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_indicador_ordenacao}:"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brInputText id="txtIndicadorOrdenacao" value="#{manterServicosRelacionadosBean.nrOrdenacaoServicoRelacionado}"  size="10" maxlength="2" 
			onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_servico_relacionado_negociavel}:"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<t:selectOneRadio id="rdoServicoRelacionadoNegociavel" value="#{manterServicosRelacionadosBean.cdIndicadorServicoNegociavel}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}" />
				<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}" />
				<a4j:support  oncomplete="javascript:foco(this);" event="onclick" status="statusAguarde" action="#{manterServicosRelacionadosBean.limparCheckBoxesInclusao}" reRender="panelCkeck"/>			
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid id="panelCkeck" columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<h:selectBooleanCheckbox id="chkNegociacao" value="#{manterServicosRelacionadosBean.chkNegociacao}" disabled="#{not manterServicosRelacionadosBean.indicadorServicoNegociavel}"/>  
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_negociacao}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="margin-left:10px">
			<h:selectBooleanCheckbox id="chkContingencia" value="#{manterServicosRelacionadosBean.chkContingencia}" disabled="#{not manterServicosRelacionadosBean.indicadorServicoNegociavel}"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_contingencia}:"/>  
		</br:brPanelGroup>
		<br:brPanelGroup style="margin-left:10px">
			<h:selectBooleanCheckbox id="chkManutencao" value="#{manterServicosRelacionadosBean.chkManutencao}" disabled="#{not manterServicosRelacionadosBean.indicadorServicoNegociavel}"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_manutencao}:"/>  
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	 <f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="vtnVoltar" style="margin-left:5px" styleClass="bto1" value="#{msgs.manterServicoRelacionado_btn_voltar}" action="#{manterServicosRelacionadosBean.voltar}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >	
			<br:brCommandButton id="btnAvancar" onclick="javascript: validaCampos(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario_exc}', 
					'#{msgs.manterServicoRelacionado_label_servico}', '#{msgs.manterServicoRelacionado_label_servico_relacionado}', 
					'#{msgs.manterServicoRelacionado_label_natureza_servico}', '#{msgs.manterServicoRelacionado_label_servico_composto}',
					'#{msgs.label_indicador_ordenacao}', '#{msgs.label_servico_relacionado_negociavel}', '#{msgs.label_negociacao}', '#{msgs.label_contingencia}', '#{msgs.label_manutencao}')" 
					style="margin-left:5px" styleClass="bto1" value="#{msgs.manterServicoRelacionado_btn_avancar}" action="#{manterServicosRelacionadosBean.avancarIncluir}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
		</br:brPanelGroup>
	</br:brPanelGrid>

</br:brPanelGrid>
</brArq:form>