<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="altManterServicosRelacionados" name="altManterServicosRelacionados" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterServicoRelacionado_label_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterServicosRelacionadosBean.servicoDesc}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterServicoRelacionado_label_servico_relacionado}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterServicosRelacionadosBean.servicoRelacionadoDesc}"
				rendered="#{manterServicosRelacionadosBean.servicoDesc != manterServicosRelacionadosBean.servicoRelacionadoDesc}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterServicoRelacionado_label_servico_composto}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterServicosRelacionadosBean.dsServicoComposto}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterServicoRelacionado_label_natureza_servico}:"/>
			
			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_combo_pagamento}"  rendered = "#{manterServicosRelacionadosBean.naturezaServico == 1}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{msgs.manterServicoRelacionado_combo_avisos}"  rendered = "#{manterServicosRelacionadosBean.naturezaServico == 2}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{msgs.manterServicoRelacionado_combo_comprovantes}"  rendered = "#{manterServicosRelacionadosBean.naturezaServico == 3}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_combo_recadastramento}"  rendered = "#{manterServicosRelacionadosBean.naturezaServico == 4}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_combo_cadastramento_favorecido}"  rendered = "#{manterServicosRelacionadosBean.naturezaServico == 5}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_combo_rastreamento_titulos}"  rendered = "#{manterServicosRelacionadosBean.naturezaServico == 6}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_combo_prova_vida}"   rendered = "#{manterServicosRelacionadosBean.naturezaServico == 7}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_combo_emissao_comprovante_salarial}"   rendered = "#{manterServicosRelacionadosBean.naturezaServico == 8}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_indicador_ordenacao}:"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brInputText id="txtIndicadorOrdenacao" value="#{manterServicosRelacionadosBean.entradaAlteracao.nrOrdenacaoServicoRelacionado}"  size="10" maxlength="2" 
			onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_servico_relacionado_negociavel}:"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<t:selectOneRadio id="rdoServicoRelacionadoNegociavel" value="#{manterServicosRelacionadosBean.entradaAlteracao.cdIndicadorServicoNegociavel}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}" />
				<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}" />
				<a4j:support  oncomplete="javascript:foco(this);" event="onclick" status="statusAguarde" action="#{manterServicosRelacionadosBean.limparCheckBoxesAlteracao}" reRender="panelCkeck"/>			
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid id="panelCkeck" columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<h:selectBooleanCheckbox id="chkNegociacao" value="#{manterServicosRelacionadosBean.chkNegociacao}" disabled="#{not manterServicosRelacionadosBean.entradaAlteracao.indicadorServicoNegociavel}"/>  
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_negociacao}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="margin-left:10px">
			<h:selectBooleanCheckbox id="chkContingencia" value="#{manterServicosRelacionadosBean.chkContingencia}" disabled="#{not manterServicosRelacionadosBean.entradaAlteracao.indicadorServicoNegociavel}"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_contingencia}:"/>  
		</br:brPanelGroup>
		<br:brPanelGroup style="margin-left:10px">
			<h:selectBooleanCheckbox id="chkManutencao" value="#{manterServicosRelacionadosBean.chkManutencao}" disabled="#{not manterServicosRelacionadosBean.entradaAlteracao.indicadorServicoNegociavel}"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_manutencao}:"/>  
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
			
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" style="margin-left:5px" styleClass="bto1" value="#{msgs.manterServicoRelacionado_btn_voltar}" action="#{manterServicosRelacionadosBean.voltar}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >	
			<br:brCommandButton id="btnAvancar" onclick="javascript:desbloquearTela(); return validaCamposAlteracao(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario_exc}', 
					'#{msgs.label_indicador_ordenacao}', '#{msgs.label_servico_relacionado_negociavel}', '#{msgs.label_negociacao}', '#{msgs.label_contingencia}', '#{msgs.label_manutencao}')" 
					style="margin-left:5px" styleClass="bto1" value="#{msgs.manterServicoRelacionado_btn_avancar}" action="#{manterServicosRelacionadosBean.avancarAlterar}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>