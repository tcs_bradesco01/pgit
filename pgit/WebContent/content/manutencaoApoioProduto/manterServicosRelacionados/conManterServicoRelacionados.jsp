<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="conManterServicosRelacionados" name="conManterServicosRelacionados" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid> 

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.manterServicoRelacionado_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_label_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="TipoServico" value="#{manterServicosRelacionadosBean.tipoServicoFiltro}" disabled="#{manterServicosRelacionadosBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_combo_selecione}"/>
						<f:selectItems value="#{manterServicosRelacionadosBean.listarArgumentoPesquisaServico}"/>		
						<a4j:support event="onchange" reRender="modalidadeServico" action="#{manterServicosRelacionadosBean.listarModalidadeServicoFiltro}"/>	
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_label_servico_relacionado}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="modalidadeServico" value="#{manterServicosRelacionadosBean.modalidadeServicoFiltro}" disabled="#{manterServicosRelacionadosBean.tipoServicoFiltro == 0 || manterServicosRelacionadosBean.tipoServicoFiltro == null || manterServicosRelacionadosBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_combo_selecione}"/>
						<f:selectItems value="#{manterServicosRelacionadosBean.listaModalidadeServicoFiltro}" />			
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>
	</br:brPanelGrid>

 <f:verbatim><hr class="lin"> </f:verbatim>
 
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btnLimparCampos" style="margin-left:5px"   styleClass="bto1" value="#{msgs.manterServicoRelacionado_btn_limparcampos}" action="#{manterServicosRelacionadosBean.limparCampos}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" style="margin-left:5px" styleClass="bto1" value="#{msgs.manterServicoRelacionado_btn_consultar}" action="#{manterServicosRelacionadosBean.consultar}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
		</br:brPanelGroup> 
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim> 
	
<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">		
 
			<app:scrollableDataTable id="dataTable" value="#{manterServicosRelacionadosBean.listaGrid}" var="varResult" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false"
						value="#{manterServicosRelacionadosBean.itemSelecionadoListaGrid}" >
						<f:selectItems value="#{manterServicosRelacionadosBean.listaGridControle}"/>
						<a4j:support event="onclick" reRender="btnDetalhar, btnExcluir, btnAlterar" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="380px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.manterServicoRelacionado_label_servico}" style="text-align:center;width:380" />
					</f:facet>
					<br:brOutputText value="#{varResult.tipoServicoFormatado}"/>
				</app:scrollableColumn>  

				<app:scrollableColumn styleClass="colTabLeft" width="350px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.manterServicoRelacionado_label_servico_relacionado}" style="text-align:center;width:350" />
					</f:facet>
					<br:brOutputText value="#{varResult.modalidadeFormatado}" 
						rendered="#{varResult.tipoServicoFormatado != varResult.modalidadeFormatado}"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.manterServicoRelacionado_label_servico_composto}" style="text-align:center;width:300" />
					</f:facet>
					<br:brOutputText value="#{varResult.servicoCompostoFormatado}"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.manterServicoRelacionado_label_natureza_servico}" style="text-align:center;width:200" />
					</f:facet>
					<br:brOutputText  value="#{msgs.manterServicoRelacionado_combo_pagamento}"  rendered = "#{varResult.codNaturezaServico == 1}"/>
					<br:brOutputText  value="#{msgs.manterServicoRelacionado_combo_avisos}"  rendered = "#{varResult.codNaturezaServico == 2}"/>
					<br:brOutputText  value="#{msgs.manterServicoRelacionado_combo_comprovantes}"  rendered = "#{varResult.codNaturezaServico == 3}"/>
					<br:brOutputText  value="#{msgs.manterServicoRelacionado_combo_recadastramento}"  rendered = "#{varResult.codNaturezaServico == 4}"/>
					<br:brOutputText  value="#{msgs.manterServicoRelacionado_combo_cadastramento_favorecido}"  rendered = "#{varResult.codNaturezaServico== 5}"/>
					<br:brOutputText  value="#{msgs.manterServicoRelacionado_combo_rastreamento_titulos}"  rendered = "#{varResult.codNaturezaServico == 6}"/>
					<br:brOutputText  value="#{msgs.manterServicoRelacionado_combo_prova_vida}"   rendered = "#{varResult.codNaturezaServico == 7}"/>
					<br:brOutputText  value="#{msgs.manterServicoRelacionado_combo_emissao_comprovante_salarial}"   rendered = "#{varResult.codNaturezaServico == 8}"/>					
				</app:scrollableColumn>				
				
			</app:scrollableDataTable>
						  
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterServicosRelacionadosBean.pesquisar}"  > 
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1" 
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>	 
		</br:brPanelGroup>
	</br:brPanelGrid>
 
	 <f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton  id="btnDetalhar" style="margin-left:5px" disabled="#{empty manterServicosRelacionadosBean.itemSelecionadoListaGrid}" styleClass="bto1" value="#{msgs.manterServicoRelacionado_btn_detalhar}" action="#{manterServicosRelacionadosBean.detalhar}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" style="margin-left:5px" styleClass="bto1" value="#{msgs.manterServicoRelacionado_btn_incluir}" action="#{manterServicosRelacionadosBean.incluir}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAlterar" style="margin-left:5px" disabled="#{empty manterServicosRelacionadosBean.itemSelecionadoListaGrid}" styleClass="bto1" value="#{msgs.label_alterar}" action="#{manterServicosRelacionadosBean.alterar}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
			<br:brCommandButton id="btnExcluir" style="margin-left:5px" disabled="#{empty manterServicosRelacionadosBean.itemSelecionadoListaGrid}" styleClass="bto1" value="#{msgs.manterServicoRelacionado_btn_excluir}" action="#{manterServicosRelacionadosBean.excluir}" >							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>