<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="conManterMensagemComprovanteSalarialForm" name="conManterMensagemComprovanteSalarialForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterMensagemComprovanteSalarialBean.obrigatoriedade}"/>


<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_codigo_tipo_mensagem}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterMensagemComprovanteSalarialBean.codigoTipoMensagemFiltro}" disabled="#{manterMensagemComprovanteSalarialBean.btoAcionado}" size="30" maxlength="6" id="codigoTipoMensagem"  onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  onkeypress="onlyNum();" >
						<brArq:commonsValidator type="integer" arg="#{msgs.conManterMensagemComprovanteSalarial_label_codigo_tipo_mensagem}" server="false" client="true" />
						<a4j:support event="onblur" reRender="rdoNivelMensagem,tipoComprovante,rdoRestricao" />											
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_nivel_mensagem}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<t:selectOneRadio id="rdoNivelMensagem" value="#{manterMensagemComprovanteSalarialBean.rdoNivelMensagemFiltro}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false" 
				disabled="#{manterMensagemComprovanteSalarialBean.btoAcionado || manterMensagemComprovanteSalarialBean.desabilitaCampos}">  
				<f:selectItem itemValue="1" itemLabel="#{msgs.label_organizacao}" />
				<f:selectItem itemValue="2" itemLabel="#{msgs.label_cliente}" />
				<a4j:support event="onclick" reRender="rdoRestricao, tipoComprovante" action="#{manterMensagemComprovanteSalarialBean.limpaRadioRestricao}" />		
			</t:selectOneRadio>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;">		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_tipo_comprovante}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    		<br:brSelectOneMenu id="tipoComprovante" value="#{manterMensagemComprovanteSalarialBean.tipoComprovanteFiltro}"  
			    			disabled="#{manterMensagemComprovanteSalarialBean.rdoNivelMensagemFiltro != 2 || manterMensagemComprovanteSalarialBean.btoAcionado || manterMensagemComprovanteSalarialBean.desabilitaCampos}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.conManterMensagemComprovanteSalarial_label_combo_selecione}"/>
							<f:selectItems value="#{manterMensagemComprovanteSalarialBean.listaTipoComprovante}"/>
						</br:brSelectOneMenu>
			    
				</br:brPanelGroup>					
			</br:brPanelGrid>									
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_restricao}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<t:selectOneRadio id="rdoRestricao" disabled="#{manterMensagemComprovanteSalarialBean.rdoNivelMensagemFiltro != 2 || manterMensagemComprovanteSalarialBean.btoAcionado || manterMensagemComprovanteSalarialBean.desabilitaCampos}" value="#{manterMensagemComprovanteSalarialBean.rdoRestricaoFiltro}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false" >  
					<f:selectItem itemValue="1" itemLabel="#{msgs.conManterMensagemComprovanteSalarial_label_generico}" />
					<f:selectItem itemValue="2" itemLabel="#{msgs.conManterMensagemComprovanteSalarial_label_restrito}" />
				</t:selectOneRadio>	
		    </br:brPanelGroup>
		</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.conManterMensagemComprovanteSalarial_label_botao_limpar_dados}" action="#{manterMensagemComprovanteSalarialBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conManterMensagemComprovanteSalarial_label_botao_consultar}"  action="#{manterMensagemComprovanteSalarialBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
		
		<app:scrollableDataTable id="dataTable" value="#{manterMensagemComprovanteSalarialBean.listaGridPesquisa}" var="result" 
			rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%" >
			
			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>		
				<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
					layout="spread" forceId="true" forceIdIndex="false" 
					value="#{manterMensagemComprovanteSalarialBean.itemSelecionadoLista}">
					<f:selectItems value="#{manterMensagemComprovanteSalarialBean.listaControleRadio}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sorLista" index="#{parametroKey}" />
			</app:scrollableColumn>
		
			<app:scrollableColumn  width="150px"  styleClass="colTabRight">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterMensagemComprovanteSalarial_label_codigo_tipo_mensagem}"  style="text-align:center;width:150px"  />
			    </f:facet>
			    <br:brOutputText value="#{result.codigo}"/>
			 </app:scrollableColumn>
			<app:scrollableColumn  width="450px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.detManterMensagemComprovanteSalarial_label_descricao}"  style="text-align:center;width:450px" />
			    </f:facet>
			    <br:brOutputText value="#{result.descricao}" />
			 </app:scrollableColumn>
			 <app:scrollableColumn  width="250px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="Tipo de Comprovante"  style="text-align:center;width:250px" />
			    </f:facet>
			    <br:brOutputText value="#{result.descTipoComprovanteSalarial}" />
			 </app:scrollableColumn>
			 	<app:scrollableColumn  width="100px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterMensagemComprovanteSalarial_label_tipo_de_mensagem}"   style="text-align:center;width:100px" />
			    </f:facet>
			    <br:brOutputText value="#{msgs.incManterMensagemComprovanteSalarial_label_organizacao}" rendered="#{result.nivelMensagem == '1'}"/>
				<br:brOutputText value="#{msgs.conManterMensagemComprovanteSalarial_label_cliente}" rendered="#{result.nivelMensagem == '2'}"/>
			 </app:scrollableColumn>
			<app:scrollableColumn width="100px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterMensagemComprovanteSalarial_label_restricao}"  style="text-align:center;width:100px"  />
			    </f:facet>
			    <br:brOutputText value="#{msgs.conManterMensagemComprovanteSalarial_label_generico}" rendered="#{result.restricao == '1'}"/>
				<br:brOutputText value="#{msgs.conManterMensagemComprovanteSalarial_label_restrito}" rendered="#{result.restricao == '2'}"/>
			 </app:scrollableColumn>
			</app:scrollableDataTable>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterMensagemComprovanteSalarialBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			       styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			       styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			       styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			     styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterMensagemComprovanteSalarialBean.itemSelecionadoLista}"  style="margin-right:5px" value="#{msgs.conManterMensagemComprovanteSalarial_path_detalhar}"  action="#{manterMensagemComprovanteSalarialBean.detalhar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" styleClass="bto1" disabled="false"  style="margin-right:5px" value="#{msgs.conManterMensagemComprovanteSalarial_path_incluir}"  action="#{manterMensagemComprovanteSalarialBean.incluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAlterar" styleClass="bto1" disabled="#{empty manterMensagemComprovanteSalarialBean.itemSelecionadoLista}"  style="margin-right:5px" value="#{msgs.conManterMensagemComprovanteSalarial_path_alterar}"  action="#{manterMensagemComprovanteSalarialBean.alterar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty manterMensagemComprovanteSalarialBean.itemSelecionadoLista}" value="#{msgs.conManterMensagemComprovanteSalarial_path_excluir}"  action="#{manterMensagemComprovanteSalarialBean.excluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	