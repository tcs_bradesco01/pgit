<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>

<brArq:form id="alterarManterMensagemComprovanteSalarialForm" name="alterarManterMensagemComprovanteSalarialForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterMensagemComprovanteSalarialBean.obrigatoriedade}"/>
<h:inputHidden id="hiddenNivelMensagem"  value="#{manterMensagemComprovanteSalarialBean.tipoMensagem}" />
<h:inputHidden id="hiddenRestricao"  value="#{manterMensagemComprovanteSalarialBean.indicadorRestricao}" />

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_codigo_tipo_mensagem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterMensagemComprovanteSalarialBean.codigoTipoMensagem}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>			
		  
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
					
		

	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;">
	 
	 <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detManterMensagemComprovanteSalarial_label_descricao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterMensagemComprovanteSalarialBean.descricao}" size="120" maxlength="100" id="descricao" >
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_tipo_mensagem}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		   	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<t:selectOneRadio id="NivelMensagem" styleClass="HtmlSelectOneRadioBradesco"  value="#{manterMensagemComprovanteSalarialBean.tipoMensagem}">  
						<f:selectItem itemValue="1" itemLabel="#{msgs.incManterMensagemComprovanteSalarial_label_organizacao}" />
						<f:selectItem itemValue="2" itemLabel="#{msgs.conManterMensagemComprovanteSalarial_label_cliente}" />
						<a4j:support event="onclick" action="#{manterMensagemComprovanteSalarialBean.habilitaCampos}" reRender="hiddenNivelMensagem, tipoComprovante, indicadorRestricao"/>           												
			    	</t:selectOneRadio>
				</br:brPanelGroup>					
			</br:brPanelGrid>			
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
		 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;">
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_tipo_comprovante}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    		<br:brSelectOneMenu id="tipoComprovante" value="#{manterMensagemComprovanteSalarialBean.tipoComprovante}"  disabled="#{manterMensagemComprovanteSalarialBean.habilitaCliente}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.conManterMensagemComprovanteSalarial_label_combo_selecione}"/>
							<f:selectItems value="#{manterMensagemComprovanteSalarialBean.listaTipoComprovante}"/>
									
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_restricao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

 			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<t:selectOneRadio id="indicadorRestricao" styleClass="HtmlSelectOneRadioBradesco" value="#{manterMensagemComprovanteSalarialBean.indicadorRestricao}" disabled="#{manterMensagemComprovanteSalarialBean.habilitaCliente}">  
						<f:selectItem itemValue="1" itemLabel="#{msgs.conManterMensagemComprovanteSalarial_label_generico}" />
						<f:selectItem itemValue="2" itemLabel="#{msgs.conManterMensagemComprovanteSalarial_label_restrito}" />
						<a4j:support event="onclick" reRender="hiddenRestricao"/>
					</t:selectOneRadio>
				</br:brPanelGroup>					
			</br:brPanelGrid>											
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.altManterMensagemComprovanteSalarial_label_botao_voltar}" action="#{manterMensagemComprovanteSalarialBean.voltarAlterar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar"   onclick="javascript: validaCamposAlterar(document.forms[1], '#{msgs.incManterMensagemComprovanteSalarial_label_campo}', '#{msgs.incManterMensagemComprovanteSalarial_label_necessario}', '#{msgs.incManterMensagemComprovanteSalarial_label_restricao}', '#{msgs.incManterMensagemComprovanteSalarial_label_nivel_mensagem}', '#{msgs.incManterMensagemComprovanteSalarial_label_codigo_tipo_mensagem}', '#{msgs.incManterMensagemComprovanteSalarial_label_descricao_objetivo}', '#{msgs.incManterMensagemComprovanteSalarial_label_tipo_comprovante}'); return validateForm(document.forms[1]);" styleClass="bto1" disabled="false"  value="#{msgs.altManterMensagemComprovanteSalarial_label_botao_avancar}"  action="#{manterMensagemComprovanteSalarialBean.avancarAlterar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		 	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	