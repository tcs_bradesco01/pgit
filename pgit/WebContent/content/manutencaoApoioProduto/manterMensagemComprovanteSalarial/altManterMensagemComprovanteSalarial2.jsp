<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="alterarManterMensagemComprovanteSalarial2Form" name="alterarManterMensagemComprovanteSalarial2Form" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_codigo_tipo_mensagem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterMensagemComprovanteSalarialBean.codigoTipoMensagem}" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterMensagemComprovanteSalarial_label_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterMensagemComprovanteSalarialBean.descricao}" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_tipo_mensagem}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterMensagemComprovanteSalarial_label_organizacao}" rendered="#{manterMensagemComprovanteSalarialBean.tipoMensagem == '1'}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_cliente}" rendered="#{manterMensagemComprovanteSalarialBean.tipoMensagem == '2'}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_tipo_comprovante}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterMensagemComprovanteSalarialBean.tipoComprovanteDesc}" />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterMensagemComprovanteSalarial_label_indicador_restricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterMensagemComprovanteSalarialBean.indicadorRestricao == '1'}" value="#{msgs.conManterMensagemComprovanteSalarial_label_generico}" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{manterMensagemComprovanteSalarialBean.indicadorRestricao == '2'}" value="#{msgs.conManterMensagemComprovanteSalarial_label_restrito}" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
		
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.altManterMensagemComprovanteSalarial_label_botao_voltar}" action="#{manterMensagemComprovanteSalarialBean.voltarAlterar2}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" disabled="false"  value="#{msgs.altManterMensagemComprovanteSalarial_label_botao_confirmar}"  action="#{manterMensagemComprovanteSalarialBean.confirmarAlterar}" onclick="javascript: if (!confirm('Confirma Altera��o?')) { desbloquearTela(); return false; }">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>	 		
		
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
