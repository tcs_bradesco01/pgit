<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="pesqEmissaoAutomaticaForm" name="pesqEmissaoAutomaticaForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{emissaoAutomaticaBean.obrigatoriedade}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_label_title_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{emissaoAutomaticaBean.identificacaoClienteBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.bloqueaRadio}">   
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
	</t:selectOneRadio>
	</a4j:region>
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'pesqEmissaoAutomaticaForm','pesqEmissaoAutomaticaForm:txtCnpj','pesqEmissaoAutomaticaForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"
			    onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'pesqEmissaoAutomaticaForm','pesqEmissaoAutomaticaForm:txtFilial','pesqEmissaoAutomaticaForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" 
			    onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdControleCnpj}"
				onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.itemFiltroSelecionado != '1'}" value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'pesqEmissaoAutomaticaForm','pesqEmissaoAutomaticaForm:txtCpf','pesqEmissaoAutomaticaForm:txtControleCpf');" />
			    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.itemFiltroSelecionado != '1'}" value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_nomerazaosocial}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.itemFiltroSelecionado != '2'}" value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  border="0">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
			 disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.itemFiltroSelecionado != '3'}" 
			 value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'pesqEmissaoAutomaticaForm','pesqEmissaoAutomaticaForm:txtBanco','pesqEmissaoAutomaticaForm:txtAgencia');"/>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
			converter="javax.faces.Integer" disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.itemFiltroSelecionado != '3'}" value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'pesqEmissaoAutomaticaForm','pesqEmissaoAutomaticaForm:txtAgencia','pesqEmissaoAutomaticaForm:txtConta');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.itemFiltroSelecionado != '3'}" value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta"
			onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
		</a4j:outputPanel>
    </br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBtoConsultarCliente" style="width: 100%; text-align: right" ajaxRendered="true">
		<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCamposCliente" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{emissaoAutomaticaBean.limparDadosCliente}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
				<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty emissaoAutomaticaBean.identificacaoClienteBean.itemFiltroSelecionado}"  value="#{msgs.pesq_emissaoaut_btn_consultar_cliente}" action="#{emissaoAutomaticaBean.identificacaoClienteBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" 
				onclick="javascript:desbloquearTela(); 
				return validaCpoConsultaCliente('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.pesq_emissaoaut_label_cnpj}','#{msgs.pesq_emissaoaut_label_cpf}', '#{msgs.pesq_emissaoaut_nomerazaosocial}',
	   		'#{msgs.pesq_emissaoaut_label_banco}','#{msgs.pesq_emissaoaut_label_agencia}','#{msgs.pesq_emissaoaut_label_conta}');">			
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_title_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_cpfcnpj}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_nomerazaosocial}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	    <f:verbatim><hr class="lin"></f:verbatim>
	
	 <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_label_title_identificacao_contrato}:"/>
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 
	 <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			 <br:brPanelGrid columns="5" style="margin-top:5px" cellpadding="0" cellspacing="0">	
	 			<br:brPanelGroup>
	 			</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >					
				 <br:brPanelGroup>	
					<br:brSelectOneMenu id="selEmpresaGestoraContrada" converter="longConverter" value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.cdPessoaJuridicaContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
						<f:selectItems  value="#{emissaoAutomaticaBean.listaEmpresaGestora}" />
						<brArq:commonsValidator type="required" arg="#{msgs.pesq_emissaoaut_label_empresa}" server="false" client="true"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>		
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
			
			
		<br:brPanelGroup>
		
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_contrato}:"/>
					</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
	 			</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
					<br:brSelectOneMenu id="selTipoContrato" converter="inteiroConverter" value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.cdTipoContratoNegocio}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
						<f:selectItems  value="#{emissaoAutomaticaBean.tipoContrato}" />
						<brArq:commonsValidator type="required" arg="#{msgs.pesq_emissaoaut_label_tipo}" server="false" client="true"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
			
					
		<br:brPanelGroup>	
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_numero}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
				
			<br:brPanelGrid columns="2" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
		 	</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>					
							<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							 value="#{emissaoAutomaticaBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.nrSeqContratoNegocio}" size="15" maxlength="10" id="txtNumeroContrato" >
							<brArq:commonsValidator type="required" arg="#{msgs.pesq_emissaoaut_label_numero}" server="false" client="true"/>
						</br:brInputText>
					</br:brPanelGroup>		 	 
	 		</br:brPanelGrid>
	 		
 		</br:brPanelGroup>
 	</br:brPanelGrid>
	
	 <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<br:brPanelGroup style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCamposContrato" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{emissaoAutomaticaBean.limparDadosContrato}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btoConsultarContrato" styleClass="bto1" value="#{msgs.pesq_emissaoaut_btn_consultar_contrato}" action="#{emissaoAutomaticaBean.consultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); 
	   		return validaCpoContrato('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.label_tipo_contrato}','#{msgs.pesq_emissaoaut_label_numero}', '#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}');">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_title_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="margin-right:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />	
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>		
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-right:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	 
	 	<br:brPanelGroup style="margin-right:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_numero}:" />	
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>	
		</br:brPanelGroup>
	 
		<br:brPanelGroup style="margin-right:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_descContrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsContrato}"/>			
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>		
		</br:brPanelGroup>						
	</br:brPanelGrid>


	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_title_argumentosPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_combo_ServicoEmissao}:" />
		</br:brPanelGroup>	
			
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup >
			<br:brSelectOneMenu id="servicoEmissao" value="#{emissaoAutomaticaBean.servicoEmissaoFiltro}"  disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.desabilataFiltro}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.pesq_emissaoaut_label_combo_selecione}"/>
				<f:selectItems value="#{emissaoAutomaticaBean.listaServicoEmissao}"/>
					<a4j:support event="onchange" reRender="tipoServico,modalidadeServico,btnConsultar" action="#{emissaoAutomaticaBean.listarTipoServicoFiltro}" />
			</br:brSelectOneMenu>
		</br:brPanelGroup>
		
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_label_ServicoPagamento}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	

	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_combo_tipo_servico}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_combo_modalidade_servico}:" style="margin-right:0px"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup id="tipoServico">
			<br:brSelectOneMenu  value="#{emissaoAutomaticaBean.tipoServicoFiltro}"  style="margin-top:5px" disabled="#{emissaoAutomaticaBean.servicoEmissaoFiltro == 0}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.pesq_emissaoaut_label_combo_selecione}"/>
				<f:selectItems value="#{emissaoAutomaticaBean.listaTipoServicoFiltro}"/>
				<a4j:support event="onchange" reRender="modalidadeServico" action="#{emissaoAutomaticaBean.listarModalidadeServicoFiltro}" />			
			</br:brSelectOneMenu>
		</br:brPanelGroup>		
				
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		<br:brPanelGroup id="modalidadeServico">
			<br:brSelectOneMenu  value="#{emissaoAutomaticaBean.modalidadeServicoFiltro}" style="margin-top:5px" disabled="#{emissaoAutomaticaBean.tipoServicoFiltro == 0}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.pesq_emissaoaut_label_combo_selecione}"/>
				<f:selectItems value="#{emissaoAutomaticaBean.listaTipoModalidade}"/>			
			</br:brSelectOneMenu>
		</br:brPanelGroup>				
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="Limpar Campos" action="#{emissaoAutomaticaBean.limparArgumentosPesquisa}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar"  styleClass="bto1" value="#{msgs.pesq_emissaoaut_label_botao_consultar}" action="#{emissaoAutomaticaBean.carregaLista}" disabled="#{emissaoAutomaticaBean.servicoEmissaoFiltro == 0}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">		
		
			<app:scrollableDataTable id="dataTable" value="#{emissaoAutomaticaBean.listaPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{emissaoAutomaticaBean.itemSelecionadoLista}">
						<f:selectItems value="#{emissaoAutomaticaBean.listaPesquisaControle}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
					<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="360px" styleClass="colTabLeft" > 
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.pesq_emissaoaut_label_combo_tipo_servico}" style="width:360; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsTipoServico}"/>
				 </app:scrollableColumn>
				 
				<app:scrollableColumn  width="320px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.pesq_emissaoaut_label_combo_modalidade_servico}" style="width:320; text-align:center"  />
					</f:facet>
					<br:brOutputText value="#{result.dsModalidadeServico}"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn  width="200px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.pesq_emissaoaut_label_situacao_vinculacao}" style="width:200; text-align:center"  />
					</f:facet>
					<br:brOutputText value="#{result.dsSituacaoVinculacaoOperacao}"/>
				</app:scrollableColumn>				
								 
			</app:scrollableDataTable>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
			
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{emissaoAutomaticaBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup >
				<br:brCommandButton id="btnLimpar" styleClass="bto1" disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.desabilataFiltro}"  style="margin-right:5px" value="#{msgs.pesq_emissaoaut_label_botao_limpar}"  action="#{emissaoAutomaticaBean.limpar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty emissaoAutomaticaBean.itemSelecionadoLista}"  style="margin-right:5px" value="#{msgs.pesq_emissaoaut_label_botao_detalhar}"  action="#{emissaoAutomaticaBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" styleClass="bto1" disabled="#{emissaoAutomaticaBean.identificacaoClienteBean.desabilataFiltro}"  style="margin-right:5px" value="#{msgs.pesq_emissaoaut_label_botao_incluir}"  action="#{emissaoAutomaticaBean.incluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty emissaoAutomaticaBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.pesq_emissaoaut_label_botao_excluir}"  action="#{emissaoAutomaticaBean.excluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnBloqDesbloq" styleClass="bto1" disabled="#{empty emissaoAutomaticaBean.itemSelecionadoLista}" style="margin-right:5px" value="#{msgs.label_bloquear_desbloquear}"  action="#{emissaoAutomaticaBean.bloquearDesbloquear}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnHistorico" styleClass="bto1" disabled="#{empty emissaoAutomaticaBean.itemSelecionadoLista}" value="#{msgs.label_historico_bloqueio}"  action="#{emissaoAutomaticaBean.historico}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>		
	
	<t:inputHidden value="#{emissaoAutomaticaBean.identificacaoClienteBean.bloqueaRadio}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('pesqEmissaoAutomaticaForm:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	
	