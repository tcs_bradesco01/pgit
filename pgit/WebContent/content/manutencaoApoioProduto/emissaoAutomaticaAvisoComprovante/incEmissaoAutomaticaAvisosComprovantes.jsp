<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="incluirEmissaoAutomaticaForm" name="incluirEmissaoAutomaticaForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{emissaoAutomaticaBean.obrigatoriedade}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_title_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_cpfcnpj}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_nomerazaosocial}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_title_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>		
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
	 	<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_numero}:" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>	
		</br:brPanelGroup>
		 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_descContrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsContrato}"/>			
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>		
		</br:brPanelGroup>						
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_combo_ServicoEmissao}:" />
		</br:brPanelGroup>	
			
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup >
			<br:brSelectOneMenu id="servicoEmissao" value="#{emissaoAutomaticaBean.servicoEmissaoFiltro}" style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.pesq_emissaoaut_label_combo_selecione}"/>				
				<f:selectItems value="#{emissaoAutomaticaBean.listaServicoEmissao}"/>
				<a4j:support event="onchange" reRender="tipoServico,modalidadeServico" action="#{emissaoAutomaticaBean.listarTipoServicoFiltro}" />	
			</br:brSelectOneMenu>
		</br:brPanelGroup>
		
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_label_ServicoPagamento}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_combo_tipo_servico}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.pesq_emissaoaut_label_combo_modalidade_servico}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup id="tipoServico">
			<br:brSelectOneMenu  value="#{emissaoAutomaticaBean.tipoServicoFiltroInclusao}" style="margin-top:5px" disabled="#{emissaoAutomaticaBean.servicoEmissaoFiltro == 0}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.pesq_emissaoaut_label_combo_selecione}"/>
				<f:selectItems value="#{emissaoAutomaticaBean.listaTipoServicoFiltro}"/>			
				<a4j:support event="onchange" reRender="modalidadeServico" action="#{emissaoAutomaticaBean.listarModalidadeServicoFiltroInclusao}" />	
			</br:brSelectOneMenu>
		</br:brPanelGroup>			
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		<br:brPanelGroup id="modalidadeServico">
			<br:brSelectOneMenu  value="#{emissaoAutomaticaBean.modalidadeServico}" disabled="#{emissaoAutomaticaBean.tipoServicoFiltroInclusao == 0}"style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.pesq_emissaoaut_label_combo_selecione}"/>
				<f:selectItems value="#{emissaoAutomaticaBean.listaTipoModalidade}"/>			
			</br:brSelectOneMenu>
		</br:brPanelGroup>		
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.pesq_emissaoaut_label_botao_limpar_dados}" action="#{emissaoAutomaticaBean.limparDadosIncluir}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar"  styleClass="bto1" value="#{msgs.pesq_emissaoaut_label_botao_consultar}" action="#{emissaoAutomaticaBean.carregaListaIncluir}" onclick="javascript: validaCampos(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario_exc}', '#{msgs.pesq_emissaoaut_label_combo_ServicoEmissao}', '#{msgs.pesq_emissaoaut_label_combo_tipo_servico}', '#{msgs.pesq_emissaoaut_label_combo_modalidade_servico}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">		
		
			<app:scrollableDataTable id="dataTable" value="#{emissaoAutomaticaBean.listaPesquisaInclusao}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				 
			
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">					
						  <t:selectBooleanCheckbox disabled="#{emissaoAutomaticaBean.listaPesquisaInclusao == null}" style="width:30;text-align:center;margin-left:1px" id="todos" styleClass="HtmlSelectOneRadioBradesco" value="#{emissaoAutomaticaBean.selecionarTodos}" >
						 		<a4j:support event="onclick" reRender="dataTable, btnAvancar" action="#{emissaoAutomaticaBean.checarTodosGrid}"/>
						</t:selectBooleanCheckbox>
					</f:facet>					 		
				
					<t:selectBooleanCheckbox  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" value="#{result.check}">
						<f:selectItems value="#{emissaoAutomaticaBean.listaControleCheckIncluir}"/>	
						<a4j:support event="onclick" reRender="btnAvancar" action="#{emissaoAutomaticaBean.habilitaBotaoAvancar}" />	
					</t:selectBooleanCheckbox>
				</app:scrollableColumn>				
				
				<app:scrollableColumn  width="340px" styleClass="colTabLeft" > 
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.pesq_emissaoaut_label_combo_tipo_servico}"  style="width:340; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{result.dsServico}"/>
				 </app:scrollableColumn>
				 
				<app:scrollableColumn  width="340x"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.pesq_emissaoaut_label_combo_modalidade_servico}"  style="width:340; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dcModalidade}"/>
				</app:scrollableColumn>	
				 
			</app:scrollableDataTable>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{emissaoAutomaticaBean.pesquisar}">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" disabled="false"  style="align:left" value="#{msgs.botao_voltar}" action="#{emissaoAutomaticaBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" disabled="#{emissaoAutomaticaBean.btnAvancar == false }"  value="#{msgs.inc_emissaoaut_label_botao_avancar}"  action="#{emissaoAutomaticaBean.avancarIncluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>		  
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
