<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="bloqDesbloqEmissaoAutomaticaAvisosComprovantesForm" name="bloqDesbloqEmissaoAutomaticaAvisosComprovantesForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_title_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_cpfcnpj}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_nomerazaosocial}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_title_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>		
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
	 	<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_numero}:" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>	
		</br:brPanelGroup>
		 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_descContrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsContrato}"/>			
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>		
		</br:brPanelGroup>						
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup style="margin-right:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_combo_ServicoEmissao}:" /> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.servicoEmissaoDesc}" />
		</br:brPanelGroup>			
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pesq_emissaoaut_label_ServicoPagamento}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_combo_tipo_servico}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.tipoServicoDesc}"/>
		</br:brPanelGroup>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.pesq_emissaoaut_label_combo_modalidade_servico}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{emissaoAutomaticaBean.modalidadeServicoDesc}"/>			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_bloquear_desbloquear}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_situacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="rdoBloqDesblo" value="#{emissaoAutomaticaBean.bloquearDesbloquear}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" > 
				<f:selectItem itemValue="1" itemLabel="#{msgs.label_liberado}" />
				<f:selectItem itemValue="2" itemLabel="#{msgs.label_bloqueado_m}" />
			</t:selectOneRadio>		
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;"  >
		<br:brPanelGroup>			
			<t:radio for="rdoBloqDesblo" index="0"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;"  >
		<br:brPanelGroup>			
			<t:radio for="rdoBloqDesblo" index="1"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>			
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  style="align:left" value="#{msgs.botao_voltar}" action="#{emissaoAutomaticaBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>					
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.btn_limpar}" style="margin-right:5px" action="#{emissaoAutomaticaBean.limparBloqDesblo}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnConfirmar" styleClass="bto1" value="#{msgs.btn_confirmar}" action="#{emissaoAutomaticaBean.confirmarBloqDesblo}" onclick="javascript: desbloquearTela(); return validaCamposBloqDesbloq(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario_exc}', '#{msgs.label_situacao}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
		</br:brPanelGroup>
	</br:brPanelGrid>			  
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
