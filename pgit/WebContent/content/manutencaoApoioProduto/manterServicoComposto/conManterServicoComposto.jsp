<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="frmManterServicoComposto" name="frmManterServicoComposto" >

<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina" >
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterServicoComposto_path_ArgPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"> 	
		<br:brPanelGroup >
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterServicoComposto_label_combo_servico}:"/>
			</br:brPanelGrid>

			<br:brPanelGrid >
			</br:brPanelGrid>

			<br:brPanelGrid>
				<br:brInputText id="txtServicoComposto" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" maxlength="9" size="20" onkeypress="onlyNum()" converter="javax.faces.Long" value="#{manterServicoCompostoBean.cdServicoCompostoPagamentoFiltro}" disabled="#{manterServicoCompostoBean.btoAcionado}" styleClass="HtmlInputTextBradesco">		
					<a4j:support event="onblur" reRender="panelFormaLiquidacao,panelTipoServico" status="statusAguarde"/>
				</br:brInputText>
			</br:brPanelGrid>	

		</br:brPanelGroup>

	</br:brPanelGrid>

  	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conManterServicoComposto_btn_limpar_campos}" action="#{manterServicoCompostoBean.limparDados}" style="margin-right:5px" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar"  styleClass="bto1" value="#{msgs.conManterServicoComposto_btn_consultar}" action="#{manterServicoCompostoBean.carregaLista}"  onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
			<t:selectOneRadio id="rdoServicoComp" value="#{manterServicoCompostoBean.itemSelecionadoLista}" styleClass="HtmlSelectOneRadioBradesco" converter="servicoCompostoConverter" layout="spread" forceId="true" forceIdIndex="false">  
				<si:selectItems value="#{manterServicoCompostoBean.listaServico}" var="layout" itemLabel="" itemValue="#{layout}" />
				<a4j:support event="onclick" reRender="panelBotoes" limitToList="true" ajaxSingle="true" />
			</t:selectOneRadio>		
		
			<app:scrollableDataTable width="100%" id="tableServico" value="#{manterServicoCompostoBean.listaServico}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque">
			 <app:scrollableColumn width="30px" styleClass="colTabCenter">
				 <f:facet name="header">
					<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>	
				<t:radio for=":frmManterServicoComposto:rdoServicoComp" index="#{parametroKey}" />			    
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="350px" styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterServicoComposto_label_combo_servico}" style="text-align:center;width:350px"/>
			    </f:facet>
			    <br:brOutputText value="#{result.cdServicoCompostoPagamento} - #{result.dsServicoCompostoPagamento}" />
			  </app:scrollableColumn>
			</app:scrollableDataTable>		
	</br:brPanelGroup>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >
		    <brArq:pdcDataScroller id="dataScroller" for="tableServico" actionListener="#{manterServicoCompostoBean.submitPesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			  </f:facet> 
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
 
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGroup id="panelBotoes" style="width: 100%; text-align: right" >	
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterServicoCompostoBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterServicoComposto_btn_detalhar}" action="#{manterServicoCompostoBean.detalhar}" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" disabled="false" styleClass="bto1"  style="margin-right:5px" value="#{msgs.conManterServicoComposto_btn_incluir}" action="#{manterServicoCompostoBean.incluir}" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" disabled="#{empty manterServicoCompostoBean.itemSelecionadoLista}" styleClass="bto1"  style="margin-right:5px" value="#{msgs.conManterServicoComposto_btn_alterar}" action="#{manterServicoCompostoBean.alterar}" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty manterServicoCompostoBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.conManterServicoComposto_btn_excluir}" action="#{manterServicoCompostoBean.excluir}" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
					<brArq:submitCheckClient/>
				</br:brCommandButton> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGroup>
	
</br:brPanelGrid>
<%--<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />--%>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>
	