<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>


<brArq:form id="manterServicoCompostoForm" name="manterServicoCompostoForm" >

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterServicoComposto_path_ArgPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterServicoComposto_label_combo_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterServicoCompostoBean.cdServicoCompostoPagamento}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterServicoComposto_label_ds_servico_composto}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterServicoCompostoBean.dsServicoCompostoPagamento}"  />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<a4j:outputPanel id="ajaxPainelLancamentoFuturo" style="width: 100%; text-align: left" ajaxRendered="true">
	 	<br:brPanelGrid  columns="1">
	 		<br:brPanelGroup id="painelLancamentoFuturo" >
	 			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterServicoComposto_label_qtde_dia_uteis_Lanc_futuro}:"/>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterServicoCompostoBean.qtdeDiasLancFuturo}"  />
					</br:brPanelGroup>		
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
			
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterServicoComposto_label_permitir_lancamento_futuro}:"/>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterServicoCompostoBean.descPermitirLancFuturo}"  />
					</br:brPanelGroup>		
				</br:brPanelGrid>
	 		</br:brPanelGroup>
	 	</br:brPanelGrid>
	 </a4j:outputPanel>
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton styleClass="bto1" value="#{msgs.detManterServicoComposto_btn_voltar}" action="#{manterServicoCompostoBean.voltarIncluir}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnConfirmar" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela(); return false; }" styleClass="bto1"  value="#{msgs.conManterServicoComposto_path_confirmacao}" action="#{manterServicoCompostoBean.confirmaIncluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
