<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="manterServicoCompostoForm" name="manterServicoCompostoForm" >

<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina" >
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterServicoComposto_path_ArgPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid  columns="3"> 	
	  	<br:brPanelGroup >
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterServicoComposto_label_combo_servico}:"/>
			</br:brPanelGrid>
			
	       <br:brPanelGrid >
		   </br:brPanelGrid>
			
			<br:brPanelGrid>
				<br:brInputText id="txtServicoComposto" maxlength="9" size="20" converter="javax.faces.Long" disabled="true" onkeypress="onlyNum()" value="#{manterServicoCompostoBean.cdServicoCompostoPagamento}" styleClass="HtmlInputTextBradesco">		
				</br:brInputText>
			</br:brPanelGrid>	
		</br:brPanelGroup>
	 </br:brPanelGrid>
	 		
	 <br:brPanelGrid  columns="3"> 			
		<br:brPanelGroup >
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterServicoComposto_label_ds_servico_composto}:"/>
			</br:brPanelGrid>
			
	       	<br:brPanelGrid >
		   	</br:brPanelGrid>
			
			<br:brPanelGrid>
				<br:brInputText id="txtDescServicoComposto" maxlength="100" size="120" value="#{manterServicoCompostoBean.dsServicoCompostoPagamento}" styleClass="HtmlInputTextBradesco">		
					<brArq:commonsValidator type="required" arg="#{msgs.incManterServicoComposto_label_ds_servico_composto}" server="false" client="true"/>
				</br:brInputText>
				
			</br:brPanelGrid>	
		</br:brPanelGroup>
	 </br:brPanelGrid>
	 
	 <a4j:outputPanel id="ajaxPainelLancamentoFuturo" style="width: 100%; text-align: left" ajaxRendered="true">
	 	<br:brPanelGrid  columns="1">
	 		<br:brPanelGroup id="painelLancamentoFuturo" >
	 			<br:brPanelGrid  columns="3"> 			
					<br:brPanelGroup >
						<br:brPanelGrid columns="2">
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterServicoComposto_label_qtde_dia_uteis_Lanc_futuro}:"/>
						</br:brPanelGrid>
						
				       	<br:brPanelGrid >
					   	</br:brPanelGrid>
						
						<br:brPanelGrid>
							<br:brInputText id="txtQtdeDias" maxlength="3" size="5" converter="javax.faces.Integer" onkeypress="onlyNum()" onfocus="validaCampoNumericoColado(this);" value="#{manterServicoCompostoBean.qtdeDiasLancFuturo}" styleClass="HtmlInputTextBradesco"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}'); return validaValorDias(this, '#{msgs.label_msg_365_dias}');">		
							</br:brInputText>
							
						</br:brPanelGrid>	
					</br:brPanelGroup>
				 </br:brPanelGrid>
				 <t:selectOneRadio id="rdoLancFuturo" value="#{manterServicoCompostoBean.permitirLancFuturo}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
						<f:selectItems value="#{manterServicoCompostoBean.listaPermiteLancFuturo}" />
					</t:selectOneRadio>
				 <br:brPanelGrid  columns="3"> 			
					<br:brPanelGroup >
						<br:brPanelGrid columns="2">
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterServicoComposto_label_permitir_lancamento_futuro}:"/>
						</br:brPanelGrid>
						
				       	<br:brPanelGrid >
					   	</br:brPanelGrid>
						
						<br:brPanelGrid>
							<br:brPanelGrid columns="4" style="margin-top:5px" cellpadding="0" cellspacing="0" >
								<t:radio for="rdoLancFuturo" index="0" />	
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}"/>
								<t:radio for="rdoLancFuturo" index="1" />	
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}"/>
							</br:brPanelGrid>
							
						</br:brPanelGrid>	
					</br:brPanelGroup>
				 </br:brPanelGrid>
	 		</br:brPanelGroup>
	 	</br:brPanelGrid>
	 </a4j:outputPanel>

	<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.detManterServicoComposto_btn_voltar}" action="#{manterServicoCompostoBean.voltarAlt}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton  id="btnAvancar" onclick="javascript:desbloquearTela(); return validaCamposAlteracao(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.conManterServicoComposto_label_combo_servico}', '#{msgs.incManterServicoComposto_label_ds_servico_composto}','#{msgs.incManterServicoComposto_label_qtde_dia_uteis_Lanc_futuro}', '#{msgs.incManterServicoComposto_label_permitir_lancamento_futuro}');"  styleClass="bto1" value="#{msgs.incAssociacaoLoteLayoutArquivo_btn_avancar}" action="#{manterServicoCompostoBean.avancarAlterar}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>
	