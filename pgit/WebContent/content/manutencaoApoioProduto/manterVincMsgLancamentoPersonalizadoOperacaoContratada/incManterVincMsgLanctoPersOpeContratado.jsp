<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>


<brArq:form id="manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm" name="manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm" >
<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_cpfcnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_nomeRazao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	 <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_argsPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_codigoLancamentoPersonalizado}:"/>
		</br:brPanelGroup>
		
			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_tipo_servico}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_modalidade_servico}:"/>
		</br:brPanelGroup>
		
		
		<br:brPanelGroup>
    		<br:brInputText size="10" maxlength="5" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"   id="txtOrdemLinhaMensagem" value="#{vincMsgLanctoPersOpConBean.codigoLancamentoPersonalizado}" disabled="#{vincMsgLanctoPersOpConBean.btoAcionadoIncluir}">  
				<brArq:commonsValidator type="integer" arg="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_codigoLancamentoPersonalizado}" server="false" client="true" />
			</br:brInputText>
		</br:brPanelGroup>	
					
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoServico" value="#{vincMsgLanctoPersOpConBean.tipoServico}" disabled="#{vincMsgLanctoPersOpConBean.btoAcionadoIncluir}">
					<f:selectItem itemValue="0" itemLabel="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_combo_selecione}"/>
					<f:selectItems value="#{vincMsgLanctoPersOpConBean.listaTipoServico}"/>
					<a4j:support event="onchange" action="#{vincMsgLanctoPersOpConBean.listarModalidadeServicoIncluir}"
									reRender="modalidadeServico" />
				</br:brSelectOneMenu>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="modalidadeServico" value="#{vincMsgLanctoPersOpConBean.modalidadeServico}" disabled="#{vincMsgLanctoPersOpConBean.tipoServico == null || vincMsgLanctoPersOpConBean.tipoServico == 0 || vincMsgLanctoPersOpConBean.btoAcionadoIncluir}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_combo_selecione}"/>
				<f:selectItems value="#{vincMsgLanctoPersOpConBean.listaModalidadeServicoIncluir}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_botao_limpar_campos}" action="#{vincMsgLanctoPersOpConBean.limparDadosIncluir}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar2" onclick="javascript:return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_botao_consultar}" action="#{vincMsgLanctoPersOpConBean.consultarIncluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

			<app:scrollableDataTable id="dataTable" value="#{vincMsgLanctoPersOpConBean.listaGridPesquisaIncluir}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{vincMsgLanctoPersOpConBean.itemSelecionadoListaIncluir}">
						<f:selectItems value="#{vincMsgLanctoPersOpConBean.listaControleRadioIncluir}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>

			<app:scrollableColumn  width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			     <h:outputText value="#{msgs.conLancamentoPersonalizado_label_cod_lancamento_personalizado}" style="width:200; text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.codLancamento}" />
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn styleClass="colTabLeft" width="100px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_restricao}" style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{msgs.label_sim}" rendered="#{result.restricao==1}"/>
   				    <br:brOutputText value="#{msgs.label_nao}" rendered="#{result.restricao==2}"/>
				</app:scrollableColumn>
				<app:scrollableColumn styleClass="colTabLeft" width="100px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_tipo_lancamento}" style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{msgs.conLancamentoPersonalizado_label_debito}" rendered="#{result.tipoLancamento==1}"/>
   				    <br:brOutputText value="#{msgs.conLancamentoPersonalizado_label_credito}" rendered="#{result.tipoLancamento==2}"/>
   				    <br:brOutputText value="#{msgs.conLancamentoPersonalizado_label_debito_credito}" rendered="#{result.tipoLancamento==3}"/>
				</app:scrollableColumn>
			 
			<app:scrollableColumn  width="230px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_grid_lancamentoDebito}"  style="width:230; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsLancDebito}"  />
			 </app:scrollableColumn>
		
			
			<app:scrollableColumn  width="280px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_grid_lancamentoCredito}" style="width:280; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsLancCredito}"/>
			</app:scrollableColumn>
				
				<app:scrollableColumn  width="300px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conLancamentoPersonalizado_label_combo_tipo_servico}"  style="width:300; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{result.dsTipoServico}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="200px"  styleClass="colTabLeft">
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_modalidade_servico}"  style="width:200; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{result.dsTipoModalidade}"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>
						
		</br:brPanelGroup>
	</br:brPanelGrid>		
			
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{vincMsgLanctoPersOpConBean.paginarListaIncluir}" >
			<f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	<a4j:outputPanel id="panelBotoes2" style="width: 100%; text-align: right" ajaxRendered="true">
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_botao_voltar}" action="#{vincMsgLanctoPersOpConBean.voltarPesquisa}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton  disabled="#{empty vincMsgLanctoPersOpConBean.itemSelecionadoListaIncluir}" id="btnAvancar" styleClass="bto1" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_botao_avancar}" action="#{vincMsgLanctoPersOpConBean.avancarIncluir}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	 </br:brPanelGrid>
	</a4j:outputPanel>	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>