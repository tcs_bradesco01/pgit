<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="manterVincMsgLancamentoPersonalizadoOperacaoContratada3Form" name="manterVincMsgLancamentoPersonalizadoOperacaoContratada3Form" >
		<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_cpfcnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_nomeRazao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
			
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_situacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_codigoLancamentoPersonalizado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.codigoLancamentoPersonalizadoDesc}" />
			</br:brPanelGroup>						
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.lancDebitoDesc}" />
			</br:brPanelGroup>						
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_credito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.lancCreditoDesc}" />
			</br:brPanelGroup>						
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conLancamentoPersonalizado_label_combo_tipo_servico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.tipoServicoDesc}" />
			</br:brPanelGroup>						
		</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_modalidade_servico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{vincMsgLanctoPersOpConBean.modalidadeServicoDesc}" />
			</br:brPanelGroup>						
		</br:brPanelGrid>	
		
		<br:brPanelGrid cellpadding="0" cellspacing="0" width="100%" style="margin-top: 9px">
			
			<br:brPanelGrid>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tipo_de_pagamento}:"/>
			</br:brPanelGrid>
			
			<br:brPanelGrid cellpadding="0" cellspacing="0" width="100%" style="margin-top: 9px">			
				<app:scrollableDataTable id="dataTable" value="#{vincMsgLanctoPersOpConBean.listaTiposPagtoSelecionados}" var="result" width="100%" rows="10">					
					<app:scrollableColumn>
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_tipo_de_pagamento}"/>
						</f:facet>
							<br:brOutputText value="#{result.dsTipoPagamentoCliente}"/>				
					</app:scrollableColumn>			
				</app:scrollableDataTable>			
			</br:brPanelGrid>	
			
			<br:brPanelGrid width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="" >
					 <f:facet name="first">
						<brArq:pdcCommandButton id="primeira"
						  styleClass="bto1"
						  value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					  </f:facet>
					  <f:facet name="fastrewind">
						<brArq:pdcCommandButton id="retrocessoRapido"
						  styleClass="bto1" style="margin-left: 3px;"
						  value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					  </f:facet>
					  <f:facet name="previous">
						<brArq:pdcCommandButton id="anterior"
						  styleClass="bto1" style="margin-left: 3px;"
						  value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					  </f:facet>
					  <f:facet name="next">
						<brArq:pdcCommandButton id="proxima"
						  styleClass="bto1" style="margin-left: 3px;"
						  value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					  </f:facet>
					  <f:facet name="fastforward">
						<brArq:pdcCommandButton id="avancoRapido"
						  styleClass="bto1" style="margin-left: 3px;"
						  value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					  </f:facet>
					  <f:facet name="last">
						<brArq:pdcCommandButton id="ultima"
						  styleClass="bto1" style="margin-left: 3px;"
						  value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					  </f:facet>
					</brArq:pdcDataScroller>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		
	 	<f:verbatim><hr class="lin" style="margin-top: 9x"> </f:verbatim> 	
	   
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px"  >
				<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.associacaoLinhasPorTipoMensagem_botao_voltar}" action="#{vincMsgLanctoPersOpConBean.voltarIncluir}" >	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:200px" >
				<br:brCommandButton  id="btnConfirmar" styleClass="bto1" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_botao_confirmar}" action="#{vincMsgLanctoPersOpConBean.confirmarIncluir}" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela(); return false; }">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	