<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>


<brArq:form id="manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm" name="manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
   	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.bloqueaRadio}">  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
	</t:selectOneRadio>
	</a4j:region>
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm','manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtCnpj','manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '0' || vincMsgLanctoPersOpConBean.identificacaoClienteBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm','manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtFilial','manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '0' || vincMsgLanctoPersOpConBean.identificacaoClienteBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '0' || vincMsgLanctoPersOpConBean.identificacaoClienteBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '1' || vincMsgLanctoPersOpConBean.identificacaoClienteBean.habilitaFiltroDeCliente }" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm','manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtCpf','manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtControleCpf');" />
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '1' || vincMsgLanctoPersOpConBean.identificacaoClienteBean.habilitaFiltroDeCliente}" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_nomeRazao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '2' || vincMsgLanctoPersOpConBean.identificacaoClienteBean.habilitaFiltroDeCliente}" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
		
	
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  border="0">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '3' || vincMsgLanctoPersOpConBean.identificacaoClienteBean.habilitaFiltroDeCliente}" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm','manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtBanco','manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtAgencia');"/>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '3' || vincMsgLanctoPersOpConBean.identificacaoClienteBean.habilitaFiltroDeCliente}" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm','manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtAgencia','manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtConta');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.itemFiltroSelecionado != '3' || vincMsgLanctoPersOpConBean.identificacaoClienteBean.habilitaFiltroDeCliente}" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" />
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCamposCliente" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{vincMsgLanctoPersOpConBean.limparDadosCliente}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty vincMsgLanctoPersOpConBean.identificacaoClienteBean.itemFiltroSelecionado || vincMsgLanctoPersOpConBean.identificacaoClienteBean.habilitaFiltroDeCliente}" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_consultar_cliente}" action="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="if(!validaCpoConsultaCliente(document.forms[1])){desbloquearTela();return false;}else{return true;}">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_contrato}:"/>
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_empresa_gestora_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGroup style="width:100px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_tipo_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGroup style="width:152px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>	
			<br:brSelectOneMenu id="selEmpresaGestoraContrada" converter="longConverter" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.cdPessoaJuridicaContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
				<f:selectItems  value="#{vincMsgLanctoPersOpConBean.listaEmpresaGestora}" />
				<brArq:commonsValidator type="required" arg="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_empresa_gestora_contrato}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>			
			<br:brSelectOneMenu id="selTipoContrato" converter="inteiroConverter" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.cdTipoContratoNegocio}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
				<f:selectItems  value="#{vincMsgLanctoPersOpConBean.listaTipoContrato}" />
				<brArq:commonsValidator type="required" arg="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_tipo_contrato}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>					
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.nrSeqContratoNegocio}" size="15" maxlength="10" id="txtNumeroContrato" >
			</br:brInputText>
		</br:brPanelGroup>		 	 
	 </br:brPanelGrid>
	 
	 <f:verbatim><hr class="lin"></f:verbatim>
	 
	 <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<br:brPanelGroup style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCamposContrato" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{vincMsgLanctoPersOpConBean.limparDadosContrato}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
	   		<br:brCommandButton id="btoConsultarContrato" styleClass="bto1" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_consultar_contrato}" action="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.consultarContratos}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript: desbloquearTela(); return validaCpoContrato('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_empresa_gestora_contrato}','#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_tipo_contrato}', '#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}', '#{duplicacaoArquivoRetornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}');">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_contrato}:"/>
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"   />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_descricao_contrato}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conDuplicacaoArquivoRetorno_situacao}:"  />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
		
    </br:brPanelGrid>
    
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	 <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_argsPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	 <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_codigoLancamentoPersonalizado}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>


		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    		<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" size="10"  maxlength="5"    id="txtOrdemLinhaMensagem" value="#{vincMsgLanctoPersOpConBean.codigoLancamentoPersonalizadoFiltro}" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.desabilataFiltro || vincMsgLanctoPersOpConBean.btoAcionado}">  
							<brArq:commonsValidator type="integer" arg="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_codigoLancamentoPersonalizado}" server="false" client="true" />
						</br:brInputText>
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>	



		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_tipo_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="produto" value="#{vincMsgLanctoPersOpConBean.tipoServicoFiltro}" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.desabilataFiltro || vincMsgLanctoPersOpConBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_combo_selecione}"/>
						<f:selectItems value="#{vincMsgLanctoPersOpConBean.listaTipoServico}"/>		
						<a4j:support event="onchange" action="#{vincMsgLanctoPersOpConBean.listarModalidadeServico}"
									reRender="operacao" />	
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>
										
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_modalidade_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="operacao" value="#{vincMsgLanctoPersOpConBean.modalidadeServicoFiltro}" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.desabilataFiltro || vincMsgLanctoPersOpConBean.tipoServicoFiltro == null || vincMsgLanctoPersOpConBean.tipoServicoFiltro == 0 || vincMsgLanctoPersOpConBean.btoAcionado}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_label_combo_selecione}"/>
						<f:selectItems value="#{vincMsgLanctoPersOpConBean.listaModalidadeServico}"/>			
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_botao_limpar_campos}" action="#{vincMsgLanctoPersOpConBean.limparArgumentosPesquisa}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar2"  onclick="javascript:return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.vincMsgLancamentoPersonalizadoOperacaoContratada_botao_consultar}" action="#{vincMsgLanctoPersOpConBean.carregaLista}" disabled="#{vincMsgLanctoPersOpConBean.habilitaConsu}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

			<app:scrollableDataTable id="dataTable" value="#{vincMsgLanctoPersOpConBean.listaGridPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{vincMsgLanctoPersOpConBean.itemSelecionadoLista}">
						<f:selectItems value="#{vincMsgLanctoPersOpConBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabRight" width="200px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conLancamentoPersonalizado_label_cod_lancamento_personalizado}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.codigoHistorico}"/>
				</app:scrollableColumn>
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_debito}" style="width:300; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsLanctDebito}" rendered="#{result.lancDebito!=0}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_codigo_lancamento_credito}" style="width:300; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsLanctCredito}" rendered="#{result.lancCredito!=0}" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_combo_tipo_servico}"style="width:300; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoServico}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conLancamentoPersonalizado_label_combo_modalidade_servico}"style="width:300; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsModalidadeServico}"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid id="dataTableCtas" columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScrollerCtas" for="dataTable"  actionListener="#{vincMsgLanctoPersOpConBean.paginarLista}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira1"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido2"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior3"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima4"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido5"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima6"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimpar" styleClass="bto1" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.desabilataFiltro}"  style="margin-right:5px" value="#{msgs.pesq_emissaoaut_label_botao_limpar}"  action="#{vincMsgLanctoPersOpConBean.limpar}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>	
				<br:brCommandButton id="btnDetalhar" disabled="#{empty vincMsgLanctoPersOpConBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conDuplicacaoArquivoRetorno_btn_detalhar}" action="#{vincMsgLanctoPersOpConBean.detalhar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" disabled="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.desabilataFiltro}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conDuplicacaoArquivoRetorno_btn_incluir}" action="#{vincMsgLanctoPersOpConBean.incluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty vincMsgLanctoPersOpConBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.conDuplicacaoArquivoRetorno_btn_excluir}" action="#{vincMsgLanctoPersOpConBean.excluir}" >				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</a4j:outputPanel>
		
	<t:inputHidden value="#{vincMsgLanctoPersOpConBean.identificacaoClienteBean.habilitaFiltroDeCliente}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>