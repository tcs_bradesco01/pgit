<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>


<brArq:form id="manterCadastroEmpresaAcompForm" name="manterCadastroEmpresaAcompForm" >

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
 
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cnpj}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'manterCadastroEmpresaAcompForm','manterCadastroEmpresaAcompForm:txtCnpj','manterCadastroEmpresaAcompForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9"  styleClass="HtmlInputTextBradesco" value="#{manterCadastroEmpresaAcompBean.manterDto.cdCnpjCpf}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'manterCadastroEmpresaAcompForm','manterCadastroEmpresaAcompForm:txtFilial','manterCadastroEmpresaAcompForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4"  styleClass="HtmlInputTextBradesco" value="#{manterCadastroEmpresaAcompBean.manterDto.cdFilialCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					<br:brInputText id="txtControle" style="margin-right: 5" converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2"  styleClass="HtmlInputTextBradesco" value="#{manterCadastroEmpresaAcompBean.manterDto.cdCtrlCpfCnpjSaida}"onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="margin-left:20px;text-align:left;" >
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadastroEmpresaAcompBean.manterDto.dsRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-left:20px">
			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" value="#{manterCadastroEmpresaAcompBean.manterDto.cdAgenciaSaida}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'manterCadastroEmpresaAcompForm','manterCadastroEmpresaAcompForm:txtAgencia','manterCadastroEmpresaAcompForm:txtConta');" />
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  onkeypress="onlyNum();"
							 value="#{manterCadastroEmpresaAcompBean.manterDto.cdContaCorrenteSaida}" size="17" maxlength="7" id="txtConta" style="margin-right:5px"  onkeyup="proximoCampo(7,'manterCadastroEmpresaAcompForm','manterCadastroEmpresaAcompForm:txtConta','manterCadastroEmpresaAcompForm:txtDigConta');"/>
						<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  onkeypress="onlyNum();"
							 value="#{manterCadastroEmpresaAcompBean.manterDto.cdDigitoConta}" size="3" maxlength="1" id="txtDigConta" style="margin-right:5px" />
					</br:brPanelGroup>
				</br:brPanelGrid>				
	    </br:brPanelGrid>
	    
	    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	    
	    <br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterCadastroEmpresaAcompanhamento_convenioSalario}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" value="#{manterCadastroEmpresaAcompBean.manterDto.cdConveCtaSalarial}" size="15" maxlength="9" id="txtConvenioSalario" onkeypress="onlyNum();"/>
					</br:brPanelGroup>	
				</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<t:selectOneRadio id="rdoFiltroCliente" value="#{manterCadastroEmpresaAcompBean.cdIndicadorSimNao}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
			<f:selectItem itemValue="S" itemLabel="" />
			<f:selectItem itemValue="N" itemLabel="" />
		</t:selectOneRadio>
		
		 <br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_title_indPrevidencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="4" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<t:radio for="rdoFiltroCliente" index="0" />	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}"/>
					<t:radio for="rdoFiltroCliente" index="1" />	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}"/>
				</br:brPanelGrid>
		</br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">
	    	<br:brPanelGrid columns="2" style="text-align:left;width:100%"  >
	    		<br:brCommandButton id="btnLimparTela" styleClass="bto1" style="margin-right:5px" value="#{msgs.botao_voltar}" action="conManterCadastroEmpresaAcomp">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
	    	</br:brPanelGrid>	
	    	<br:brPanelGrid columns="1" style="text-align:right;width:100%" >
	    		<br:brPanelGroup>
					<br:brCommandButton id="botaoLimpar" styleClass="bto1" style="margin-right:5px" value="#{msgs.label_botao_limpar}" action="#{manterCadastroEmpresaAcompBean.limparManter}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				
					<br:brCommandButton id="botaoAvancar" styleClass="bto1" style="margin-right:5px" value="#{msgs.botao_avancar}" action="#{manterCadastroEmpresaAcompBean.consistirIncluir}" 
						onclick="javascript:desbloquearTela(); return validaCamposManutencao(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}', 'Incluir', '#{msgs.manterCadastroEmpresaAcompanhamento_convenioSalario}', '#{msgs.label_title_indPrevidencia}');">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>	
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   
	
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
