<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>


<brArq:form id="manterCadastroEmpresaAcompForm" name="manterCadastroEmpresaAcompForm" >

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
 
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroEmpresaAcompBean.manterDto.cnpjFormatado}"/>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="margin-left:20px;text-align:left;" >
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroEmpresaAcompBean.manterDto.dsRazaoSocial}"  />
			
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="6" cellpadding="0" cellspacing="0" style="margin-left:20px">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroEmpresaAcompBean.manterDto.cdAgenciaSaida}"/>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroEmpresaAcompBean.manterDto.contaFormatada}"/>
	    </br:brPanelGrid>
	    
	    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	    
	    <br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.manterCadastroEmpresaAcompanhamento_convenioSalario}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroEmpresaAcompBean.manterDto.cdConveCtaSalarial}"/>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		
		 <br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_title_indPrevidencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadastroEmpresaAcompBean.manterDto.descPrevidencia}"/>
		</br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">
	    	<br:brPanelGrid columns="2" style="text-align:left;width:100%"  >
	    		<br:brCommandButton id="btnLimparTela" styleClass="bto1" style="margin-right:5px" value="#{msgs.botao_voltar}" action="incManterCadastroEmpresaAcomp">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
	    	</br:brPanelGrid>	
	    	<br:brPanelGrid columns="1" style="text-align:right;width:100%" >
	    		<br:brPanelGroup>
					<br:brCommandButton id="botaoConfirmar" styleClass="bto1" style="margin-right:5px" value="#{msgs.botao_confirmar}" action="#{manterCadastroEmpresaAcompBean.confirmarIncluir}" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela();  return false; }" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>	
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   
	
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
