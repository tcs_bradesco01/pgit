<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>


<brArq:form id="manterCadastroEmpresaAcompForm" name="manterCadastroEmpresaAcompForm" >

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">			
	
		
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
 
		<a4j:region id="regionFiltro">
		<t:selectOneRadio id="rdoFiltroCliente" value="#{manterCadastroEmpresaAcompBean.itemFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="" />
			<a4j:support event="onclick"  status="statusAguarde" reRender="panelCnpj, botoesPaginar, botoeslinha, camposCliente, panelCpf, panelNomeRazao, panelBanco, panelAgencia, panelConta, panelBtoConsultarCliente, btnConsultar,panelBotoes,dataTable,dataScroller" ajaxSingle="true" oncomplete="validarProxCampoIdentClienteContrato(document.forms[1]);" action="#{manterCadastroEmpresaAcompBean.limparFiltro}"/>
		</t:selectOneRadio>
		</a4j:region>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="0" />			
			
			 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cnpj}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'manterCadastroEmpresaAcompForm','manterCadastroEmpresaAcompForm:txtCnpj','manterCadastroEmpresaAcompForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{manterCadastroEmpresaAcompBean.itemFiltroSelecionado != '1'}" styleClass="HtmlInputTextBradesco" value="#{manterCadastroEmpresaAcompBean.cdCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'manterCadastroEmpresaAcompForm','manterCadastroEmpresaAcompForm:txtFilial','manterCadastroEmpresaAcompForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{manterCadastroEmpresaAcompBean.itemFiltroSelecionado != '1'}" styleClass="HtmlInputTextBradesco" value="#{manterCadastroEmpresaAcompBean.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					<br:brInputText id="txtControle" style="margin-right: 5" converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{manterCadastroEmpresaAcompBean.itemFiltroSelecionado != '1'}" styleClass="HtmlInputTextBradesco" value="#{manterCadastroEmpresaAcompBean.cdControleCnpj}"onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
			<t:radio for="rdoFiltroCliente" index="1" />		
			
			 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{manterCadastroEmpresaAcompBean.itemFiltroSelecionado != '2'}" value="#{manterCadastroEmpresaAcompBean.razaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial" />
			</a4j:outputPanel>
			
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="2" />			
			
			
			
			<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" disabled="#{manterCadastroEmpresaAcompBean.itemFiltroSelecionado != '3'}" value="#{manterCadastroEmpresaAcompBean.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'manterCadastroEmpresaAcompForm','manterCadastroEmpresaAcompForm:txtAgencia','manterCadastroEmpresaAcompForm:txtConta');" />
				
			</a4j:outputPanel>
		
			<a4j:outputPanel id="panelConta" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  onkeypress="onlyNum();" disabled="#{manterCadastroEmpresaAcompBean.itemFiltroSelecionado != '3'}"
							 value="#{manterCadastroEmpresaAcompBean.cdContaBancaria}" size="17" maxlength="7" id="txtConta" style="margin-right:5px"  onkeyup="proximoCampo(7,'manterCadastroEmpresaAcompForm','manterCadastroEmpresaAcompForm:txtConta','manterCadastroEmpresaAcompForm:txtDigConta');"/>
						<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  onkeypress="onlyNum();" disabled="#{manterCadastroEmpresaAcompBean.itemFiltroSelecionado != '3'}"
							 value="#{manterCadastroEmpresaAcompBean.cdDigitoConta}" size="3" maxlength="1" id="txtDigConta" style="margin-right:5px" />
					</br:brPanelGroup>
				</br:brPanelGrid>				
			</a4j:outputPanel>
	    </br:brPanelGrid>
	  </a4j:outputPanel>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<a4j:outputPanel id="panelBtoConsultar" ajaxRendered="true" style="width:100%; text-align: right">
		   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" value="#{msgs.consultarPagamentosIndividual_btn_consultar_cliente}" action="#{manterCadastroEmpresaAcompBean.pesquisar}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
		   		 onclick="javascript:desbloquearTela(); return validaCamposConsulta(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');">		
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</a4j:outputPanel>
		</br:brPanelGrid>	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
			<app:scrollableDataTable id="dataTable" value="#{manterCadastroEmpresaAcompBean.saidaConsulta.ocorrencias}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterCadastroEmpresaAcompBean.itemSelecionadoLista}">
						<f:selectItems value="#{manterCadastroEmpresaAcompBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes"/>
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabCenter" width="120px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_grid_cpfCnpj}"  style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cnpjFormatado}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabCenter" width="170px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_grid_nomeRazaoSocial}" style="width:170; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsRazaoSocial}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabCenter" width="80px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_grid_agencia}"  style="width:80; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdAgenciaSaida}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabCenter" width="80px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_grid_conta}" style="width:70; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.contaFormatada}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabCenter" width="80px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_title_convenio}"  style="width:90; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdConveCtaSalarial}" />
				</app:scrollableColumn>	
							
				<app:scrollableColumn styleClass="colTabCenter" width="80px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_title_previdencia}"  style="width:90; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.descPrevidencia}" />
				</app:scrollableColumn>				
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid id="botoesPaginar" columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup rendered="#{manterCadastroEmpresaAcompBean.ativarBotoes}">
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterCadastroEmpresaAcompBean.paginarPesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid id="botoeslinha" columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<f:verbatim><hr class="lin"> </f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
 	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup >
				<br:brCommandButton id="btnLimparTela" styleClass="bto1" style="margin-right:5px" value="#{msgs.label_botao_limpar}" action="#{manterCadastroEmpresaAcompBean.limpar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_incluir}"  action="#{manterCadastroEmpresaAcompBean.avancarIncluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterCadastroEmpresaAcompBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_detalhar}" action="#{manterCadastroEmpresaAcompBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty manterCadastroEmpresaAcompBean.itemSelecionadoLista}" style="margin-right:5px"   styleClass="bto1" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_excluir}"  action="#{manterCadastroEmpresaAcompBean.avancarExcluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" disabled="#{empty manterCadastroEmpresaAcompBean.itemSelecionadoLista}"  styleClass="bto1" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_alterar}"  action="#{manterCadastroEmpresaAcompBean.avancarAlterar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>	
	
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   
	
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
