<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incManterAssocRetornoLayArqProdServ" name="incManterAssocRetornoLayArqProdServ" >

<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina" >
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterAssocRetornoLayArqProdServ_tipoServico}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="filtroServicos" value="#{assRetLayArqProSerBean.cdTipoServico}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.incManterAssocRetornoLayArqProdServ_selecione}"/>	
				<f:selectItems value="#{assRetLayArqProSerBean.listaTipoServico}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.incManterAssocRetornoLayArqProdServ_tipoServico}" server="false" client="true"/>	
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterAssocRetornoLayArqProdServ_tipoLayoutArquivo}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="filtroLayoutArquivo" value="#{assRetLayArqProSerBean.cdTipoLayoutArquivo}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.incManterAssocRetornoLayArqProdServ_selecione}"/>	
				<f:selectItems value="#{assRetLayArqProSerBean.listaTipoLayoutArquivo}"/>		
				<brArq:commonsValidator type="required" arg="#{msgs.incManterAssocRetornoLayArqProdServ_tipoLayoutArquivo}" server="false" client="true"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid  columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterAssocRetornoLayArqProdServ_tipoRetorno}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="filtroRetorno" value="#{assRetLayArqProSerBean.cdTipoRetorno}" style="margin-top:5px" >
				<f:selectItem itemValue="" itemLabel="#{msgs.incManterAssocRetornoLayArqProdServ_selecione}"/>	
				<f:selectItems value="#{assRetLayArqProSerBean.listaTipoArquivoRetorno}"/>
				<brArq:commonsValidator type="required" arg="#{msgs.incManterAssocRetornoLayArqProdServ_tipoRetorno}" server="false" client="true"/>							
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.incManterAssocRetornoLayArqProdServ_btnVoltar}" action="#{assRetLayArqProSerBean.voltar}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton  id="btnConfirmar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.incManterAssocRetornoLayArqProdServ_btnAvancar}" action="#{assRetLayArqProSerBean.avancarIncluir}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>										

	
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
			
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>
