<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterAssocRetornoLayArqProdServ" name="conManterAssocRetornoLayArqProdServ" >
<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" styleClass="CorpoPagina" >
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid  styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_argumentosPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoServico}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboTipoServico" value="#{assRetLayArqProSerBean.filtroTipoServico}" disabled="#{assRetLayArqProSerBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conManterAssocRetornoLayArqProdServ_selecione}"/>	
				<f:selectItems value="#{assRetLayArqProSerBean.listaTipoServico}"/>	
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoLayoutArquivo}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboTipoLayoutArquivo" value="#{assRetLayArqProSerBean.filtroTipoLayoutArquivo}" disabled="#{assRetLayArqProSerBean.btoAcionado}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="#{msgs.conManterAssocRetornoLayArqProdServ_selecione}"/>	
				<f:selectItems value="#{assRetLayArqProSerBean.listaTipoLayoutArquivo}"/>						
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoRetorno}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup >
			<br:brSelectOneMenu id="cboTipoRetorno" value="#{assRetLayArqProSerBean.filtroTipoArquivoRetorno}" disabled="#{assRetLayArqProSerBean.btoAcionado}" style="margin-top:5px" >
				<f:selectItem itemValue="" itemLabel="#{msgs.conManterAssocRetornoLayArqProdServ_selecione}"/>	
				<f:selectItems value="#{assRetLayArqProSerBean.listaTipoArquivoRetorno}"/>						
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conManterAssocRetornoLayArqProdServ_btn_limparCampos}" action="#{assRetLayArqProSerBean.limparCampos}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conManterAssocRetornoLayArqProdServ_btn_consultar}" action="#{assRetLayArqProSerBean.carregaLista}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><br></f:verbatim> 

	<br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto;width:750px" >
		
			<app:scrollableDataTable id="dataTable" value="#{assRetLayArqProSerBean.listaGrid}" var="result" 
			rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="750px" height="170">

			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>		

				<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
					value="#{assRetLayArqProSerBean.itemSelecionadoLista}">
					<f:selectItems value="#{assRetLayArqProSerBean.listaControle}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sor" index="#{parametroKey}" />
			</app:scrollableColumn>

			<app:scrollableColumn width="200px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoServico}" style="text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoServico}"/>
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn width="150px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoLayoutArquivo}" style="text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoLayoutArquivo}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn width="260px" styleClass="colTabLeft" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterAssocRetornoLayArqProdServ_tipoRetorno}" style="text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoArquivoRetorno}"/>
			 </app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnDetalhar" disabled="#{empty assRetLayArqProSerBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterAssocRetornoLayArqProdServ_btn_detalhar}" action="#{assRetLayArqProSerBean.detalhar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir"  disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterAssocRetornoLayArqProdServ_btn_incluir}" action="#{assRetLayArqProSerBean.incluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir"  disabled="#{empty assRetLayArqProSerBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.conManterAssocRetornoLayArqProdServ_btn_excluir}" action="#{assRetLayArqProSerBean.excluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>
	
	<br:brPanelGrid columns="1" style="margin-top:150px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		




</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>