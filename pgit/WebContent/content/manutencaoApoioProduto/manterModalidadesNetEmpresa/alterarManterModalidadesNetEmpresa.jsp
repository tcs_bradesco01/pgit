<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="alterarManterModalidadesNetEmpresa" name="alterarManterModalidadesNetEmpresa" >
<br:brPanelGrid columns="1" width="955" cellpadding="0" cellspacing="0" style="margin-left:5px">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" />
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alterar_modalidade_horario_transmissao}: "/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" border="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detalhar_modalidade}: "/>
			<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterModalidadesNetEmpresaBean.entradaHorarioLimiteAlterar.dsModalidade}" size="50" maxlength="50" />
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" id="panelHrInicioConsulta">
		
			<br:brPanelGroup style="margin-bottom:5px;"  >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detalhar_modalidade_horario_autorizacao}:"/>
			</br:brPanelGroup>
			
		   <br:brPanelGroup>					
			    <br:brInputText onblur="validarHora(this,'#{msgs.label_hora_invalida}')" 
			    id="txtHorarioLimiteSaldoDiario" styleClass="HtmlInputTextBradesco" size="13" maxlength="10" 
			    value="#{manterModalidadesNetEmpresaBean.entradaHorarioLimiteAlterar.hrLimiteAutorizacao}" alt="horaMinSeg2" />
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detalhar_modalidade_horario_transmissao}:" />
			</br:brPanelGroup>
			
			<br:brPanelGroup>
	   			<br:brInputText onblur="validarHora(this,'#{msgs.label_hora_invalida}')" id="txtHorarioLimiteSaldoDiarioSalario"  size="13" maxlength="10" 
	   			value="#{manterModalidadesNetEmpresaBean.entradaHorarioLimiteAlterar.hrLimiteTransmissao}" alt="horaMinSeg2" styleClass="HtmlInputTextBradesco" />
	   		</br:brPanelGroup>
	   		
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.detalhar_modalidade_horario_manutencao}:" />
			</br:brPanelGroup>
	   		
	   		<br:brPanelGroup>
	   			<br:brInputText onblur="validarHora(this,'#{msgs.label_hora_invalida}')" id="txtHorarioLimiteManutencao"  size="13" maxlength="10" 
	   			value="#{manterModalidadesNetEmpresaBean.entradaHorarioLimiteAlterar.hrLimiteManutencao}" alt="horaMinSeg2" styleClass="HtmlInputTextBradesco" />
	   		</br:brPanelGroup>
		</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>

 	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup style="text-align:left;">
			<br:brCommandButton styleClass="bto1" value="#{msgs.altFormaLiquidacaoPagamentoIntegrado_btn_voltar}" action="#{manterModalidadesNetEmpresaBean.voltarInicio}">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:100%">
			<br:brCommandButton id="btnAvancar" onclick="desbloquearTela();return checaCamposObrigatoriosAlterar('#{msgs.label_ocampo}',
				'#{msgs.label_necessario}', '#{msgs.altFormaLiquidacaoPagamentoIntegrado_label_horariolimite}',
				'#{msgs.incFormaLiquidacaoPagamentoIntegrado_label_prioridadedebito}')" styleClass="bto1"
				value="#{msgs.altFormaLiquidacaoPagamentoIntegrado_btn_avancar}" action="#{manterModalidadesNetEmpresaBean.avancarAlterarLiqPagtoIntegrado}">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>		
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>