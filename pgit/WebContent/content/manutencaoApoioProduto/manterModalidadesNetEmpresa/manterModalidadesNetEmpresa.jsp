<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="ManterModalidadesNetEmpresa" name="manterModalidadesNetEmpresa" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterHorarioLimiteEmpresa_title_argumentosPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
				
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

			<app:scrollableDataTable id="dataTable" value="#{manterModalidadesNetEmpresaBean.listaGridPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco"  layout="spread" forceId="true" forceIdIndex="false" 
						value="#{manterModalidadesNetEmpresaBean.itemSelecionadoLista}">
						<f:selectItems value="#{manterModalidadesNetEmpresaBean.selecionaModalidadesNetEmpresa}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>

				<app:scrollableColumn  width="70px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conFormaLiquidacaoPagamentoIntegrado_label_grid_codigo}" style="width:100;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdModalidade}"  styleClass="tableFontStyle" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conManterHorarioLimiteEmpresa_label_descricao}" style="width:180; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsModalidade}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conManterHorarioLimiteEmpresa_label_horario_pagamento}" style="width:180; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.hrLimiteAutorizacao}" styleClass="tableFontStyle" />				  
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conManterHorarioLimiteEmpresa_label_horario_arquivo}"style="width:180; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.hrLimiteTransmissao}" styleClass="tableFontStyle" />
				</app:scrollableColumn>

			
				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conManterHorarioLimiteEmpresa_label_horario_manutencao}"style="width:180; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.hrLimiteManutencao}" styleClass="tableFontStyle" />
				</app:scrollableColumn>
			</app:scrollableDataTable>
						
		</br:brPanelGroup>
	</br:brPanelGrid>		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>			
		<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterModalidadesNetEmpresaBean.paginarModalidadeNetEmpresa}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			  </brArq:pdcDataScroller>		
		</br:brPanelGroup>
	</br:brPanelGrid>	
	

	<f:verbatim><hr class="lin"> </f:verbatim>

	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup >
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterModalidadesNetEmpresaBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conLancamentoPersonalizado_label_botao_detalhar}"  action="#{manterModalidadesNetEmpresaBean.detalharModalidadesNetEmpresa}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" disabled="#{empty manterModalidadesNetEmpresaBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conLancamentoPersonalizado_label_botao_alterar}"  action="#{manterModalidadesNetEmpresaBean.alterarModalidadesNetEmpresa}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty manterModalidadesNetEmpresaBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conLancamentoPersonalizado_label_botao_excluir}"  action="#{manterModalidadesNetEmpresaBean.excluirModalidadeNetEmpresa}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>