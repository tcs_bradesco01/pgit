<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterSolicitacaoReplicacaoIncluirForm" name="manterSolicitacaoReplicacaoIncluirForm" >
<h:inputHidden id="hiddenRadio" value=""/>
<h:inputHidden id="hiddenRadioRegra" value=""/>


<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.PGIC0110_label_title_solicitacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>  
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_nivel_estrutura_produto_replicado}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboNivelEstrutProdRepl" value="#{manterSolicitacaoReplicacaoBean.nivelEstruturaProdutoReplicadoFiltro}">
				<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
				<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaNivelEstrutProdRepl}"/-->			
			</br:brSelectOneMenu>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup></br:brPanelGroup>
			</br:brPanelGrid>  
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_acao_replicacao}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboAcaoReplicacao" value="#{manterSolicitacaoReplicacaoBean.acaoReplicacoFiltro}">
				<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
				<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaAcaoReplicacao}"/-->			
			</br:brSelectOneMenu>
		</br:brPanelGroup>							
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup></br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_produto}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="cboProduto" value="#{manterSolicitacaoReplicacaoBean.produtoFiltro}">
					<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
					<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaProduto}"-->	 		
				</br:brSelectOneMenu>		    
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>			
		
		<br:brPanelGroup style="width:20px" ></br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_atributo_produto}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="cboAtributoProduto" value="#{manterSolicitacaoReplicacaoBean.atributoProdutoFiltro}">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
						<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaAtributoProduto}"-->			
					</br:brSelectOneMenu>	    
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>						
		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_atributo_produto_substitutivo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="cboAtributoProdutoSubstitutivo" value="#{manterSolicitacaoReplicacaoBean.atributoProdutoSubstitutivoFiltro}">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
						<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaAtributoProdutoSubstitutivo}"-->			
					</br:brSelectOneMenu>	    
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	
    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_dominio_atributo_produto}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="cboDominioAtributoProduto" value="#{manterSolicitacaoReplicacaoBean.dominioAtributoProdutoFiltro}">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
						<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaDominioAtributoProduto}"/-->			
					</br:brSelectOneMenu>    
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>					
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_dominio_produto_substitutivo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="cboDominioProdutoSubstitutivo" value="#{manterSolicitacaoReplicacaoBean.dominioProdutoSubstitutivoFiltro}">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
						<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaDominioProdutoSubstitutivo}"/-->			
					</br:brSelectOneMenu>	    
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_operacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="cboOperacao" value="#{manterSolicitacaoReplicacaoBean.operacaoFiltro}">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
						<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaOperacao}"/-->			
					</br:brSelectOneMenu>		    
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>			
		
		<br:brPanelGroup style="width:20px" ></br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_atributo_operacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="cboAtributoOperacao" value="#{manterSolicitacaoReplicacaoBean.atributoOperacaoFiltro}">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
						<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaAtributoOperacao}"/-->			
					</br:brSelectOneMenu>	    
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>						
		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_atributo_operacao_substitutivo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="cboAtributoOperacaoSubstitutivo" value="#{manterSolicitacaoReplicacaoBean.atributoOperacaoSubstitutivoFiltro}">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
						<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaAtributoOperacaoSubstitutivo}"/-->			
					</br:brSelectOneMenu>	    
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	
    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_dominio_atributo_operacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="cboDominioAtributoOperacao" value="#{manterSolicitacaoReplicacaoBean.dominioAtributoOperacaoFiltro}">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
						<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaDominioAtributoOperacao}"/-->			
					</br:brSelectOneMenu>    
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>					
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_dominio_operacao_substitutivo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brSelectOneMenu id="cboDominioAtributoOperacaoSubstitutivo" value="#{manterSolicitacaoReplicacaoBean.dominioAtributoOperacaoSubstitutivoFiltro}">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
						<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaDominioAtributoOperacaoSubstitutivo}"/-->			
					</br:brSelectOneMenu>	    
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_data_programada_replicacao}:"/>
		</br:brPanelGroup>	
			
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>			
			
		<br:brPanelGroup>		
			<app:calendar id="calendarioDataProg" value="#{manterSolicitacaoReplicacaoBean.dataProgramadaReplicacaoFiltro}" >
				<brArq:commonsValidator type="required" arg="#{msgs.PGIC0110_check_dataProg}" server="false" client="true"/>
			</app:calendar>		
		</br:brPanelGroup>	 
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_perc_reajuste}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brPanelGroup>	
	    	<br:brInputText  size="15" maxlength="9" id="txtPercReajusteFiltro" value="#{manterSolicitacaoReplicacaoBean.percReajusteFiltro}">  		    	
		    </br:brInputText>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>	
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
			<t:selectOneRadio onclick="habilitarDesativarCampos(document.forms[1], this);" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="#{msgs.PGIC0110_label_todos_contratos_existentes}" />
				<f:selectItem itemValue="1" itemLabel="#{msgs.PGIC0110_label_Somente_Contr_Client_deter_Agen_Oper}" />
				<f:selectItem itemValue="2" itemLabel="#{msgs.PGIC0110_label_Somente_Contr_Client_deter_Segmento}" />
				<f:selectItem itemValue="3" itemLabel="#{msgs.PGIC0110_label_Somente_Contr_Client_deter_Regiao_Geo}" />
				<f:selectItem itemValue="4" itemLabel="#{msgs.PGIC0110_label_Somente_Contr_Client_deter_Atividade_Eco}" />
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>

 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"> 	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_nivel_enquadramento_contatos}:"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<t:radio for="sor" index="0" />
	    </br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup></br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<t:radio for="sor" index="1" />
	    </br:brPanelGroup>	    
	    
	    <br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brPanelGroup>	
	    	<br:brInputText  size="15" maxlength="9" id="txtContratosClientAgencOper" value="#{manterSolicitacaoReplicacaoBean.contratosClientAgencOperFiltro}">  		    	
		    </br:brInputText>	
		</br:brPanelGroup>	    
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup></br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<t:radio for="sor" index="2" />
	    </br:brPanelGroup>	    
	    
	    <br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboContratoClientDetermSegm" value="#{manterSolicitacaoReplicacaoBean.contratoClientDetermSegmFiltro}">
				<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
				<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaContratoClientDetermSegm}"/-->			
			</br:brSelectOneMenu>
		</br:brPanelGroup>		   
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup></br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<t:radio for="sor" index="3" />
	    </br:brPanelGroup>	    
	    
	    <br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup> 						
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboContratoClientDetermRegiao" value="#{manterSolicitacaoReplicacaoBean.contratoClientDetermRegiaoFiltro}">
				<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
				<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaContratoClientDetermRegiao}"/-->			
			</br:brSelectOneMenu>
		</br:brPanelGroup>		   
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup></br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<t:radio for="sor" index="4" />
	    </br:brPanelGroup>	    
	    
	    <br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboContratoClientDetermAtvEconomica" value="#{manterSolicitacaoReplicacaoBean.contratoClientDetermAtvEconomicaFiltro}">
				<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0110_label_selecione}"/>
				<!--f:selectItems value="#{manterSolicitacaoReplicacaoBean.listaContratoClientDetermAtvEconomica}"/-->			
			</br:brSelectOneMenu>
		</br:brPanelGroup>		   
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup></br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0110_label_periodo_solicitacao}:"/>
		</br:brPanelGroup>
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>			
			
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.PGIC0110_label_de}"/>
			<app:calendar id="calendarioDe" value="#{manterSolicitacaoReplicacaoBean.filtroDataDe}" >
				<brArq:commonsValidator type="required" arg="#{msgs.PGIC0110_check_periodo_solicitacao}" server="false" client="true"/>
			</app:calendar>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_ate}"/>
			<app:calendar id="calendarioAte" value="#{manterSolicitacaoReplicacaoBean.filtroDataAte}" >
				<brArq:commonsValidator type="required" arg="#{msgs.PGIC0110_check_periodo_solicitacao}" server="false" client="true"/>
			</app:calendar>			
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton  id="btnVoltar" styleClass="HtmlCommandButtonBradesco" value="#{msgs.PGIC0110_label_botao_voltar}" action="#{manterSolicitacaoReplicacaoBean.voltarConsultar}"> 	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px"> 
			<br:brCommandButton  id="btnAvancar" styleClass="HtmlCommandButtonBradesco" value="#{msgs.PGIC0110_label_botao_avancar}" action="#{manterSolicitacaoReplicacaoBean.avancarConfirmar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
