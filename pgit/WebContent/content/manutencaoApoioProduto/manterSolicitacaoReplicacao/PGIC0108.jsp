<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterSolicitacaoReplicacaoPesquisar" name="manterSolicitacaoReplicacaoPesquisar" >

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.PGIC0108_label_title_argumentoPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0108_label_periodo_solicitacao}:"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
			
	<br:brPanelGroup>
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.PGIC0108_label_de}"/>
		<app:calendar id="calendarioDe" value="#{manterSolicitacaoReplicacaoBean.filtroDataDe}" >
			<brArq:commonsValidator type="required" arg="#{msgs.PGIC0108_check_periodo_solicitacao}" server="false" client="true"/>
		</app:calendar>
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.PGIC0108_label_ate}"/>
		<app:calendar id="calendarioAte" value="#{manterSolicitacaoReplicacaoBean.filtroDataAte}" >
			<brArq:commonsValidator type="required" arg="#{msgs.PGIC0108_check_periodo_solicitacao}" server="false" client="true"/>
		</app:calendar>			
	</br:brPanelGroup>	
	
   
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0108_label_situacao_solicitacao}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboSituacaoSolicitacao" value="#{manterSolicitacaoReplicacaoBean.situacaoSolicitacaoFiltro}" style="margin-top:5px">
				<f:selectItem itemValue="" itemLabel="SELECIONE"/>				
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>								

	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="HtmlCommandButtonBradesco" value="#{msgs.PGIC0108_label_botao_limpar_campos}" action="#{manterSolicitacaoReplicacaoBean.limparCampos}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="HtmlCommandButtonBradesco" value="#{msgs.PGIC0108_label_botao_consultar}" action="" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
			<f:verbatim> <div id="rolagem" style="width:750px; overflow:scroll"></f:verbatim> 
			<t:dataTable id="dataTable" value="#{cmpi0001Bean.listaGridRemessa}" var="result" rows="5" 
			cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
			columnClasses="alinhamento_centro, alinhamento_direita, alinhamento_direita, alinhamento_esquerda, 
			alinhamento_direita, alinhamento_direita, alinhamento_esquerda, alinhamento_esquerda" 
			headerClass="tabela_celula_destaque_acentuado" width="1110px">
			<t:column width="30px">
				<f:facet name="header">
			      <br:brOutputText value="" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>		
				<t:selectOneRadio  onclick="javascript:habilitarBotaosLoteOcorrencia(document.forms[1], this);" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
					<f:selectItems value="#{cmpi0001Bean.listaControleRemessa}"/>
				</t:selectOneRadio>
		    	<t:radio for="sor" index="#{result.totalRegistros}" />
			</t:column>
			  <t:column width="250px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0108_label_grid_nivel_estrutura}" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.cpf_completo}" style="float:left;"/>
			  </t:column>
			  <t:column width="180px" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.PGIC0108_label_grid_acao_replicacao}" style="font-weight: bold;font-family: verdana;font-size: 10 px;margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.nome}" style="float:left;" />
			  </t:column>
			  <t:column width="250px" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.PGIC0108_label_grid_nivel_enquadramento}" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.nome}" style="float:left;"/>
			  </t:column>
			  <t:column width="250px" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.PGIC0108_label_grid_periodo_contratacao}" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.nome}" style="float:left;"/>
			  </t:column>			  
			  <t:column width="150px" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.PGIC0108_label_grid_situacao}" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.nome}" style="float:left;"/>
			  </t:column>			  
			</t:dataTable>	
			<f:verbatim> </div> </f:verbatim>				
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
 <br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterRegraSegregacaoAcessoDadosBean.pesquisarExcecao}" rendered="#{manterRegraSegregacaoAcessoDadosBean.listaGridExcecoes!= null && manterRegraSegregacaoAcessoDadosBean.mostraBotoes}">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="HtmlCommandButtonBradesco"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnDetalhar"  styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.PGIC0108_label_botao_detalhar}" action="#{manterSolicitacaoReplicacaoBean.avancarDetalhar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnIncluir" disabled="false" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.PGIC0108_label_botao_incluir}" action="#{manterSolicitacaoReplicacaoBean.avancarIncluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnExcluir" styleClass="HtmlCommandButtonBradesco" disabled="false" value="#{msgs.PGIC0108_label_botao_excluir}" action="#{manterSolicitacaoReplicacaoBean.avancarExcluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:150px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
			
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
