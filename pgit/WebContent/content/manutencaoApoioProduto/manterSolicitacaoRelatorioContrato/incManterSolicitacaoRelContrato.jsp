<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="manterSolicitacaoRelatorioContratoIncluir" name="manterSolicitacaoRelatorioContratoIncluir" >
	<h:inputHidden id="hiddenTipoRelatorio" value="#{manterSolicitacaoRelatorioContratoBean.tipoRelatorio}"/>
	<h:inputHidden id="hiddenCriterioSelecaoContratos" value="#{manterSolicitacaoRelatorioContratoBean.criterioSelecaoContratos}"/>
	<h:inputHidden id="hiddenNivelConsolidacao" value="#{manterSolicitacaoRelatorioContratoBean.nivelConsolidacao}"/>
	<h:inputHidden id="hiddenObrigatoriedade" value="#{manterSolicitacaoRelatorioContratoBean.hiddenObrigatoriedade}"/>
	<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_servico}:" />
			</br:brPanelGroup>		
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  >	
			<br:brPanelGroup>
				<br:brSelectOneRadio id="tipoRelatorio" styleClass="HtmlSelectOneRadioBradesco"  value="#{manterSolicitacaoRelatorioContratoBean.tipoRelatorio}" >
					<f:selectItem itemLabel="#{msgs.incManterSolicitacaoRelContrato_radio_analitico}" itemValue="1" />  
		            <f:selectItem itemLabel="#{msgs.incManterSolicitacaoRelContrato_radio_estatístico}" itemValue="2" />  
	  				<a4j:support event="onclick" reRender="hiddenTipoRelatorio"  />												
		    	</br:brSelectOneRadio>
		    </br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
				<t:selectOneRadio  id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterSolicitacaoRelatorioContratoBean.criterioSelecaoContratos}">  
					<f:selectItem itemValue="0" itemLabel="" />
					<f:selectItem itemValue="1" itemLabel="" />
					<a4j:support event="onclick" reRender="hiddenCriterioSelecaoContratos,tipoServico,modalidadeServico" action="#{manterSolicitacaoRelatorioContratoBean.limparCriterioSelecaoContratos}"/>												
		    	</t:selectOneRadio>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacaoRelContrato_title_solicitacao}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacaoRelContrato_title_criterio_selecao_contratos}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
	    	<br:brPanelGroup>
		  		<h:selectBooleanCheckbox id="chkTipoServico" value="#{manterSolicitacaoRelatorioContratoBean.itemCheckTipoServico}" >
					<a4j:support event="onclick" reRender="tipoServico,modalidadeServico,chkModalidadeServico" action="#{manterSolicitacaoRelatorioContratoBean.limparTipoServico}"/>
				</h:selectBooleanCheckbox>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_tipo_servico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>	
						<br:brSelectOneMenu id="tipoServico" value="#{manterSolicitacaoRelatorioContratoBean.tipoServico}" disabled="#{!manterSolicitacaoRelatorioContratoBean.itemCheckTipoServico}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.conManterSolicitacaoRelContrato_selecione}"/>
							<f:selectItems value="#{manterSolicitacaoRelatorioContratoBean.listaTipoServico}"/>
							<a4j:support event="onchange" reRender="modalidadeServico,chkModalidadeServico" action="#{manterSolicitacaoRelatorioContratoBean.preencheListaModalidade}" />												
						</br:brSelectOneMenu>			    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		
	   <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			  
		   	<br:brPanelGroup>
		  		<h:selectBooleanCheckbox id="chkModalidadeServico" value="#{manterSolicitacaoRelatorioContratoBean.itemCheckModalidadeServico}" disabled="#{manterSolicitacaoRelatorioContratoBean.tipoServico == null || manterSolicitacaoRelatorioContratoBean.tipoServico == 0}" >
					<a4j:support event="onclick" reRender="modalidadeServico" action="#{manterSolicitacaoRelatorioContratoBean.limparModalidadeServico}"/>
				</h:selectBooleanCheckbox>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_modalidade_servico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>	
						<br:brSelectOneMenu id="modalidadeServico" value="#{manterSolicitacaoRelatorioContratoBean.modalidadeServico}" disabled="#{!manterSolicitacaoRelatorioContratoBean.itemCheckModalidadeServico || manterSolicitacaoRelatorioContratoBean.tipoServico == null || manterSolicitacaoRelatorioContratoBean.tipoServico == 0 }"  >
							<f:selectItem itemValue="0" itemLabel="#{msgs.conManterSolicitacaoRelContrato_selecione}"/>
							<f:selectItems value="#{manterSolicitacaoRelatorioContratoBean.listaModalidadeServico}"/>
						</br:brSelectOneMenu>			    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
			
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
	    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacaoRelContrato_title_nivel_consolidade}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		
		
	  <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		  	<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_empresa}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>	
						<br:brSelectOneMenu id="empresaConglomerado" value="#{manterSolicitacaoRelatorioContratoBean.empresa}" >
							<f:selectItems value="#{manterSolicitacaoRelatorioContratoBean.listaEmpresaConglomerado}"/>
							<a4j:support event="onchange" reRender="diretoriaRegional,gerenciaRegional,chkDiretoriaRegional,chkGerenciaRegional,agenciaOperadora,chkAgenciaOperacao" action="#{manterSolicitacaoRelatorioContratoBean.preencheListaDiretoriaRegional}"/>
						</br:brSelectOneMenu>			    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
	  <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
		  		<h:selectBooleanCheckbox id="chkDiretoriaRegional" value="#{manterSolicitacaoRelatorioContratoBean.itemCheckDiretoriaRegional}"  disabled="#{manterSolicitacaoRelatorioContratoBean.empresa == null || manterSolicitacaoRelatorioContratoBean.empresa == 0}">
					<a4j:support event="onclick" reRender="diretoriaRegional,gerenciaRegional,chkGerenciaRegional,agenciaOperadora,chkAgenciaOperacao" action="#{manterSolicitacaoRelatorioContratoBean.preencheListaGerenciaRegional}"/>
				</h:selectBooleanCheckbox>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_diretoria_regional}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>	
						<br:brSelectOneMenu id="diretoriaRegional" value="#{manterSolicitacaoRelatorioContratoBean.diretoriaRegional}"  disabled="#{!manterSolicitacaoRelatorioContratoBean.itemCheckDiretoriaRegional || manterSolicitacaoRelatorioContratoBean.empresa == 0 || manterSolicitacaoRelatorioContratoBean.empresa == null}"   >
							<f:selectItem itemValue="0" itemLabel="#{msgs.conManterSolicitacaoRelContrato_selecione}"/>
							<f:selectItems value="#{manterSolicitacaoRelatorioContratoBean.listaDiretoriaRegional}"/>
							<a4j:support event="onchange" reRender="gerenciaRegional,chkGerenciaRegional" action="#{manterSolicitacaoRelatorioContratoBean.preencheListaGerenciaRegional}" />												
						</br:brSelectOneMenu>			    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
	  	
	  	
	  	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<h:selectBooleanCheckbox id="chkGerenciaRegional" value="#{manterSolicitacaoRelatorioContratoBean.itemCheckGerenciaRegional}"  disabled="#{manterSolicitacaoRelatorioContratoBean.diretoriaRegional == null || manterSolicitacaoRelatorioContratoBean.diretoriaRegional == 0}">
					<a4j:support event="onclick" reRender="gerenciaRegional,agenciaOperadora,chkAgenciaOperacao"/>
				</h:selectBooleanCheckbox>	
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" /> 
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_gerencia_regional}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>	
						<br:brSelectOneMenu id="gerenciaRegional" value="#{manterSolicitacaoRelatorioContratoBean.gerenciaRegional}"  disabled="#{!manterSolicitacaoRelatorioContratoBean.itemCheckGerenciaRegional || manterSolicitacaoRelatorioContratoBean.diretoriaRegional == null || manterSolicitacaoRelatorioContratoBean.diretoriaRegional == 0}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.conManterSolicitacaoRelContrato_selecione}"/>
							<f:selectItems value="#{manterSolicitacaoRelatorioContratoBean.listaGerenciaRegional}"/>
							<a4j:support event="onchange" reRender="agenciaOperadora,chkAgenciaOperacao"  />												
						</br:brSelectOneMenu>			    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
	  <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<h:selectBooleanCheckbox id="chkAgenciaOperacao" value="#{manterSolicitacaoRelatorioContratoBean.itemCheckAgenciaOperadora}"  disabled="#{manterSolicitacaoRelatorioContratoBean.gerenciaRegional == null || manterSolicitacaoRelatorioContratoBean.gerenciaRegional == 0}">
					<a4j:support event="onclick" reRender="agenciaOperadora, lblAgencia, btnLupa, hiddenAgencia" action="#{manterSolicitacaoRelatorioContratoBean.limparAgencia}"/>
				</h:selectBooleanCheckbox>	
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_unidade_organizacional_operadora}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  id="panelGridAgenciaOperadora">					
				    <br:brPanelGroup>	
				    	<br:brInputText maxlength="8" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" id="agenciaOperadora" value="#{manterSolicitacaoRelatorioContratoBean.agenciaOperadora}" size="12"  readonly="#{!manterSolicitacaoRelatorioContratoBean.itemCheckAgenciaOperadora}">		    
				    		<brArq:commonsValidator type="integer" arg="#{msgs.incManterSolicitacaoRelContrato_unidade_organizacional_operadora}" server="false" client="true" />
				    		<a4j:support event="onblur" reRender="lblAgencia" action="#{manterSolicitacaoRelatorioContratoBean.buscaAgOperadora}"  />		
				    	</br:brInputText>
					</br:brPanelGroup>	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_agencia}:"/> 
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="lblAgencia" value="#{manterSolicitacaoRelatorioContratoBean.dsAgencia}"/>
						<h:inputHidden id="hiddenAgencia" value="#{manterSolicitacaoRelatorioContratoBean.dsAgencia}"/>
					</br:brPanelGroup>				
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>		
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		
		
	    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacaoRelContrato_title_agrupamento_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<h:selectBooleanCheckbox id="chkSegmento" value="#{manterSolicitacaoRelatorioContratoBean.itemCheckSegmento}" >
					<a4j:support event="onclick" reRender="segmento" action="#{manterSolicitacaoRelatorioContratoBean.limparSegmento}"/>
				</h:selectBooleanCheckbox>	
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_segmento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>	
						<br:brSelectOneMenu id="segmento" value="#{manterSolicitacaoRelatorioContratoBean.segmento}"  disabled="#{!manterSolicitacaoRelatorioContratoBean.itemCheckSegmento}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.conManterSolicitacaoRelContrato_selecione}"/>
							<f:selectItems value="#{manterSolicitacaoRelatorioContratoBean.listaSegmento}"/>
						</br:brSelectOneMenu>			    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<h:selectBooleanCheckbox id="chkParticipanteGrupoEconomico" value="#{manterSolicitacaoRelatorioContratoBean.itemCheckParticipanteGrupoEconomico}" >
					<a4j:support event="onclick" reRender="participanteGrupoEconomico,btnLupaGrupo" action="#{manterSolicitacaoRelatorioContratoBean.limparParticipante}"/>
				</h:selectBooleanCheckbox>	
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_participante_grupo_economico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>	
						<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Long" size="15" maxlength="9" onkeypress="onlyNum();" readonly="#{!manterSolicitacaoRelatorioContratoBean.itemCheckParticipanteGrupoEconomico}" value="#{manterSolicitacaoRelatorioContratoBean.participante}"  id="participanteGrupoEconomico" >
							<a4j:support event="onblur" reRender="lblDescGrupoEconomico" action="#{manterSolicitacaoRelatorioContratoBean.buscaDescGrupoEconomico}"/>
						</br:brInputText>	    
					</br:brPanelGroup>		
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_grupo_economico}:"/> 
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="lblDescGrupoEconomico" value="#{manterSolicitacaoRelatorioContratoBean.saidaGrupoEconomico.dsGrupoEconomico}"/>
					</br:brPanelGroup>			
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		 
			<br:brPanelGroup>
				<h:selectBooleanCheckbox id="chkClasse" value="#{manterSolicitacaoRelatorioContratoBean.itemCheckClasse}" >			
					<a4j:support event="onclick" reRender="cboClasseRamo,cboSubRamoAtividade" action="#{manterSolicitacaoRelatorioContratoBean.limparClasse}"/>
				</h:selectBooleanCheckbox>	
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_classe_ramo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			  
			   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px;">			
						<br:brPanelGroup style="margin-right:20px;">								
							<br:brSelectOneMenu id="cboClasseRamo" value="#{manterSolicitacaoRelatorioContratoBean.classeRamo}" styleClass="HtmlSelectOneMenuBradesco"
								disabled="#{!manterSolicitacaoRelatorioContratoBean.itemCheckClasse}">							   
									<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
									<f:selectItems value="#{manterSolicitacaoRelatorioContratoBean.listaCboClasseRamo}" />	 
									<a4j:support event="onchange" reRender="cboSubRamoAtividade" action="#{manterSolicitacaoRelatorioContratoBean.carregaComboSubRamoAtividade}"/>											
							</br:brSelectOneMenu>						
						</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sub_ramo_atividade}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				 
		
				  <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px;">			
					<br:brPanelGroup>							
						
						<br:brSelectOneMenu id="cboSubRamoAtividade" value="#{manterSolicitacaoRelatorioContratoBean.subRamoAtividadeEconomica}" styleClass="HtmlSelectOneMenuBradesco"
							disabled="#{!manterSolicitacaoRelatorioContratoBean.itemCheckClasse || manterSolicitacaoRelatorioContratoBean.classeRamo == '0' || manterSolicitacaoRelatorioContratoBean.classeRamo == null}">							   
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{manterSolicitacaoRelatorioContratoBean.listaCboSubRamoAtividade}" />												
						</br:brSelectOneMenu>
						
					</br:brPanelGroup>					
				</br:brPanelGrid>
				</br:brPanelGroup>	
		
		</br:brPanelGrid>	
		
		
		<f:verbatim><hr class="lin"> </f:verbatim>	  	
		
		
	  	
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" border="0">	
			<br:brPanelGroup style="text-align:left;width:150px"  >
				<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.incManterSolicitacaoRelContrato_btn_voltar}" action="#{manterSolicitacaoRelatorioContratoBean.voltarPesquisar}">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
			
			<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.incManterSolicitacaoRelContrato_btn_limpar}" action="#{manterSolicitacaoRelatorioContratoBean.limparCamposIncluir}" style="margin-right:5px">				
					<brArq:submitCheckClient/>
				</br:brCommandButton>		
				<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.incManterSolicitacaoRelContrato_btn_avancar}" action="#{manterSolicitacaoRelatorioContratoBean.avancarIncluirConfirmar}" 		
				onclick="desbloquearTela();return validarCampo('#{msgs.label_descricao_agencia}')" >				
				 	
							<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>  	
	</br:brPanelGrid>
		
	<br:brPanelGroup style="position:absolute; width: 100%; text-align: center; top:145">
		<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   
	</br:brPanelGroup>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
