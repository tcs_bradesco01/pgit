<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>


<brArq:form id="manterSolicitacaoRelatorioContratoIncluirConfirmar" name="manterSolicitacaoRelatorioContratoIncluirConfirmar" >

	<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacaoRelContrato_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_radio_analitico}" rendered="#{manterSolicitacaoRelatorioContratoBean.tipoRelatorio == 1}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacaoRelContrato_radio_estat�stico}" rendered="#{manterSolicitacaoRelatorioContratoBean.tipoRelatorio == 2}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacaoRelContrato2_title_criterio_selecao_contratos}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacaoRelContrato_tipo_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioContratoBean.tipoServicoDesc}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacaoRelContrato_modalidade_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioContratoBean.modalidadeServiceDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacaoRelContrato_title_nivel_consolidade}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacaoRelContrato_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioContratoBean.empresaDesc}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacaoRelContrato_diretoria_regional}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioContratoBean.diretoriaRegionalDesc}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacaoRelContrato_gerencia_regional}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioContratoBean.gerenciaRegionalDesc}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacaoRelContrato_unidade_organizacional_operadora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{manterSolicitacaoRelatorioContratoBean.agenciaOperadora}"/>
			<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value=" - " rendered="#{manterSolicitacaoRelatorioContratoBean.agenciaOperadora != null && manterSolicitacaoRelatorioContratoBean.agenciaOperadora != ''}"/>
			<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{manterSolicitacaoRelatorioContratoBean.dsAgencia}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
		
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacaoRelContrato_title_agrupamento_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacaoRelContrato_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioContratoBean.segmentoDesc}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacaoRelContrato_participante_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioContratoBean.saidaGrupoEconomico.dsGrupoEconomicoFormatado}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_classe_ramo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioContratoBean.classeRamoDesc}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>


	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>


	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_sub_ramo_atividade}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicitacaoRelatorioContratoBean.subRamoAtividadeEconomicaDesc}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
		
	
		
	
	<f:verbatim><hr class="lin"> </f:verbatim>								
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.incManterSolicitacaoRelContrato2_btn_voltar}" action="#{manterSolicitacaoRelatorioContratoBean.voltarIncluir}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.incManterSolicitacaoRelContrato2_btn_confirmar}" action="#{manterSolicitacaoRelatorioContratoBean.confirmarIncluir}" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela();  return false; }">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
