<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>


<brArq:form id="conManterSolicitRelatCliOrgaoPublicoForm" name="conManterSolicitRelatCliOrgaoPublicoForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina"  cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conManterClientesOrgaoPublicoFederal_label_periodo_solicitacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup></br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="text-align:left;" >
			    <br:brPanelGroup>
					<app:calendar id="dtSolicInicio" value="#{manterSolicitRelatCliOrgaoPublicoFederalBean.dtSolicInicio}" />
				</br:brPanelGroup>
			    <br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value=" �: " style="margin-left:20px;"/>
				</br:brPanelGroup>
			    <br:brPanelGroup>
					<app:calendar id="dtSolicFinal" value="#{manterSolicitRelatCliOrgaoPublicoFederalBean.dtSolicFinal}" />
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.manterClientesOrgaoPublicoFederal_label_botao_limpar_campos}" action="#{manterSolicitRelatCliOrgaoPublicoFederalBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.manterClientesOrgaoPublicoFederal_label_botao_consultar}" action="#{manterSolicitRelatCliOrgaoPublicoFederalBean.carregarLista}" onclick="javascript:return validateForm(document.forms[1]);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

   <br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; width:750px;"> 	
			<app:scrollableDataTable  id="dataTable" value="#{manterSolicitRelatCliOrgaoPublicoFederalBean.listaGridPesquisa}" var="result" rows="10" rowIndexVar="parametroKey"
			rowClasses="tabela_celula_normal, tabela_celula_destaque"  width="100%">
			 <app:scrollableColumn width="30px" styleClass="colTabCenter">
				 <f:facet name="header">
			      <br:brOutputText value=""  escape="false"  styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>	
			    
				<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
				  value="#{manterSolicitRelatCliOrgaoPublicoFederalBean.itemSelecionadoLista}">  
					<f:selectItems value="#{manterSolicitRelatCliOrgaoPublicoFederalBean.listaControleRadio}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />					
				</t:selectOneRadio>
				<t:radio for="sor" index="#{parametroKey}" />
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="200px"  styleClass="colTabCenter">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.manterClientesOrgaoPublicoFederal_label_data_hora_solicitacao}" style="text-align:center;width:200px"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dataHoraFormatada}" />
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="180px"  styleClass="colTabCenter">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.manterClientesOrgaoPublicoFederal_label_situacao}" style="text-align:center;width:180px"/>
			    </f:facet>
			    <br:brOutputText value="#{result.cdSituacao}" />
			  </app:scrollableColumn>
			  			  
			  <app:scrollableColumn width="250px"  styleClass="colTabCenter">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.manterClientesOrgaoPublicoFederal_label_usuario_solicitante}" style="text-align:center;width:250px"/>
			    </f:facet>
			    <br:brOutputText value="#{result.cdUsuario}" />
			  </app:scrollableColumn>			  
			  
			</app:scrollableDataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" > 	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="" >
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			       styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
 	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup >
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterSolicitRelatCliOrgaoPublicoFederalBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_detalhar}" action="#{manterSolicitRelatCliOrgaoPublicoFederalBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_incluir}"  action="#{manterSolicitRelatCliOrgaoPublicoFederalBean.incluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty manterSolicitRelatCliOrgaoPublicoFederalBean.itemSelecionadoLista}"  styleClass="bto1" value="#{msgs.conManterPrioridadeCreditoTipoCompromisso_path_excluir}"  action="#{manterSolicitRelatCliOrgaoPublicoFederalBean.excluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>	
	
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
