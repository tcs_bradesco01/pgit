<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>

<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="pesqmanterCadastroEspecieBeneficiosForm" name="pesmanterCadastroEspecieBeneficiosForm" >
<h:inputHidden id="hiddenRadio" value=""/>

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.PGIC0076_label_title_argumentoPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>   
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0076_label_codigoEspecie}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brPanelGroup>	
	    	<br:brInputText  size="15" maxlength="9" id="txtCodigoEspecie" value="#{manterCadastroEspecieBeneficiosBean.codigoEspecie}">  		    	
		    </br:brInputText>	
		</br:brPanelGroup>					 
								
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="HtmlCommandButtonBradesco" value="#{msgs.PGIC0076_label_botao_limpar_campos}" action="#{manterCadastroEspecieBeneficiosBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="HtmlCommandButtonBradesco" value="#{msgs.PGIC0142_label_botao_consultar}" action="#{manterCadastroEspecieBeneficiosBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
		<f:verbatim> <div id="rolagem" style="width:750px;  overflow:scroll"></f:verbatim> 
			<t:dataTable id="dataTable" value="#{manterRegraSegregacaoAcessoDadosBean.listaGridExcecoes}" var="result" rows="5" 
			cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
			columnClasses="alinhamento_centro, alinhamento_direita, alinhamento_direita, alinhamento_esquerda, alinhamento_direita, alinhamento_direita, alinhamento_esquerda, alinhamento_esquerda"
			headerClass="tabela_celula_destaque_acentuado" width="750px" >			
			<t:column width="30px">
				<f:facet name="header" >
			      <br:brOutputText value="" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left;" escape="false" />
			    </f:facet>		
				<t:selectOneRadio  onclick="javascript:habilitarBotaosPesqExcecaoSegregacaoAcessoDados(document.forms[1], this);" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
					<f:selectItems value="#{manterRegraSegregacaoAcessoDadosBean.listaControleRadioExcecoes}"/>
				</t:selectOneRadio>
		    	<t:radio for="sor" index="#{result.linhaSelecionada}" />
			</t:column>

			<t:column width="120px" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.manterRegraSegregacaoAcessoDadosExcecao_grid_codigoExcecao}" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.codigoExcecao}" style="float:right;" />
			  </t:column>
			  
			  <t:column width="600px" >
			    <f:facet name="header">
			      <h:outputText value="#{msgs.manterRegraSegregacaoAcessoDadosExcecao_grid_tipoUnidadeOrganizacionalUsuario}" style="font-weight: bold;font-family: verdana;font-size: 10 px; margin-left:5 px;float:left;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.tipoUnidadeOrganizacionalUsuario}"  style="float:left;"/>
			  </t:column>
			</t:dataTable>		
			<f:verbatim> </div> </f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterRegraSegregacaoAcessoDadosBean.pesquisarExcecao}" rendered="#{manterRegraSegregacaoAcessoDadosBean.listaGridExcecoes!= null && manterRegraSegregacaoAcessoDadosBean.mostraBotoes}">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="HtmlCommandButtonBradesco"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>				
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
			<br:brPanelGroup style="width:750px">			
				<br:brCommandButton id="btnIncluir" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px;" value="#{msgs.PGIC0076_label_botao_incluir}" action="#{manterCadastroEspecieBeneficiosBean.avancarIncluir}">				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" styleClass="HtmlCommandButtonBradesco"  style="margin-right:5px;" value="#{msgs.PGIC0076_label_botao_alterar}" action="#{manterCadastroEspecieBeneficiosBean.avancarAlterar}">				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" styleClass="HtmlCommandButtonBradesco"  value="#{msgs.PGIC0076_label_botao_excluir}" action="#{manterCadastroEspecieBeneficiosBean.avancarExcluir}">				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:300px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
