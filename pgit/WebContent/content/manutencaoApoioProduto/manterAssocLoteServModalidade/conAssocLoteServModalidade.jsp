<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="conAssocLoteServModalidadeForm" name="conAssocLoteServModalidade" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conAssocLoteServModalidade_label_title_argumentoPesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conAssocLoteServModalidade_label_tipo_servico}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brSelectOneMenu id="cboTipoServico"  value="#{assocLoteServModalidadeBean.filtroTipoServico}" disabled="#{assocLoteServModalidadeBean.btoAcionado}">
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
			<f:selectItems value="#{assocLoteServModalidadeBean.listaTipoServico}"/>	
			<a4j:support event="onchange" reRender="cboModalidadeServico" action="#{assocLoteServModalidadeBean.carregaListaModalidadeServico}" />												
		</br:brSelectOneMenu>
		
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup></br:brPanelGroup>
			</br:brPanelGrid>  
		</br:brPanelGroup>
		
	</br:brPanelGrid>
	
	  <br:brPanelGrid  columns="1" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conAssocLoteServModalidade_label_servico_relacionado}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brSelectOneMenu id="cboModalidadeServico" value="#{assocLoteServModalidadeBean.filtroServRelacionado}" disabled = "#{assocLoteServModalidadeBean.filtroTipoServico == null || assocLoteServModalidadeBean.filtroTipoServico == 0 || assocLoteServModalidadeBean.btoAcionado}" >
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
			<f:selectItems value="#{assocLoteServModalidadeBean.listaModalidadeServico}"/>
		</br:brSelectOneMenu>
		
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup></br:brPanelGroup>
			</br:brPanelGrid>  
		</br:brPanelGroup>
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conAssocLoteServModalidade_label_tipo_lote}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brSelectOneMenu id="cboTipoLote" value="#{assocLoteServModalidadeBean.filtroTipoLote}" disabled="#{assocLoteServModalidadeBean.btoAcionado}">
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
			<f:selectItems value="#{assocLoteServModalidadeBean.listaTipoLote}"/>
		</br:brSelectOneMenu>
		
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup></br:brPanelGroup>
			</br:brPanelGrid>  
		</br:brPanelGroup>
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conAssocLoteServModalidade_label_tipo_layout_arquivo}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brSelectOneMenu id="cboTipoLayoutArquivo" value="#{assocLoteServModalidadeBean.filtroTipoLayoutArquivo}" disabled="#{assocLoteServModalidadeBean.btoAcionado}">
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
			<f:selectItems value="#{assocLoteServModalidadeBean.listaTipoLayoutArquivo}"/>
		</br:brSelectOneMenu>
		
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup></br:brPanelGroup>
			</br:brPanelGrid>  
		</br:brPanelGroup>
		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		
					
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conAssocLoteServModalidade_label_botao_limpar_campos}" action="#{assocLoteServModalidadeBean.limparDados}"  style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			
			<br:brCommandButton id="btnConsultar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.conAssocLoteServModalidade_label_botao_consultar}"  action="#{assocLoteServModalidadeBean.carregaLista}"  >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup  style="overflow-x:auto;width:750px" >	
			<app:scrollableDataTable height="170" id="dataTable" value="#{assocLoteServModalidadeBean.listaPesquisa}" var="result" rows="10" rowIndexVar="parametroKey"
			rowClasses="tabela_celula_normal, tabela_celula_destaque"  width="100%">
			 <app:scrollableColumn width="30px" styleClass="colTabCenter">
				 <f:facet name="header">
			      <br:brOutputText value=""  escape="false"  styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>	
			    
				<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
				  value="#{assocLoteServModalidadeBean.itemSelecionado}">  
					<f:selectItems value="#{assocLoteServModalidadeBean.listaControle}"/>
					<a4j:support event="onclick" reRender="btnDetalhar, btnExcluir" />					
				</t:selectOneRadio>
				<t:radio for="sor" index="#{parametroKey}" />
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="100px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conAssocLoteServModalidade_label_tipo_servico}" style="text-align:center;width:100"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoServico}" />
			  </app:scrollableColumn>
			  
			 <app:scrollableColumn width="250px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conAssocLoteServModalidade_label_servico_relacionado}" style="text-align:center;width:250"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsModalidadeServico}"/>
			  </app:scrollableColumn>
			  
		  <app:scrollableColumn width="140px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conAssocLoteServModalidade_label_tipo_lote}" style="text-align:center;width:140"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoLote}"/>
			  </app:scrollableColumn>				  			  
			  
			 <app:scrollableColumn width="160px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conAssocLoteServModalidade_label_tipo_layout_arquivo}" style="text-align:center;width:160"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoLayoutArquivo}"/>
			  </app:scrollableColumn>
	
			</app:scrollableDataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{assocLoteServModalidadeBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet> 
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>				
		
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGroup id="panelBotoes" style="width: 100%; text-align: right" >	
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnDetalhar" disabled="#{empty assocLoteServModalidadeBean.itemSelecionado}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conAssocLoteServModalidade_path_detalhar}" action="#{assocLoteServModalidadeBean.detalhar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" disabled="false" styleClass="bto1"  style="margin-right:5px" value="#{msgs.conAssocLoteServModalidade_path_incluir}" action="#{assocLoteServModalidadeBean.incluir}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{empty assocLoteServModalidadeBean.itemSelecionado}" styleClass="bto1" value="#{msgs.conAssocLoteServModalidade_path_excluir}" action="#{assocLoteServModalidadeBean.excluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGroup>
	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	