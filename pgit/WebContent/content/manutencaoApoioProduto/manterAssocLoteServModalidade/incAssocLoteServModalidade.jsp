<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si" %>

<brArq:form id="conAssocLoteServModalidadeForm" name="conAssocLoteServModalidade" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{assocLoteServModalidadeBean.hiddenObrigatoriedade}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incAssocLoteServModalidade_label_title_inclusao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   <br:brPanelGrid  columns="1" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incAssocLoteServModalidade_label_tipo_servico}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brSelectOneMenu id="cboTipoServico"  value="#{assocLoteServModalidadeBean.cboTipoServico}">
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
			<f:selectItems value="#{assocLoteServModalidadeBean.listaTipoServico}" />
			<a4j:support event="onchange" reRender="cboModalidadeServico" action="#{assocLoteServModalidadeBean.carregaListaModalidadeServicoIncluir}" />	
			<brArq:commonsValidator type="required" arg="#{msgs.incAssocLoteServModalidade_label_tipo_servico}" server="false" client="true"/>	
		</br:brSelectOneMenu>
		
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup></br:brPanelGroup>
			</br:brPanelGrid>  
		</br:brPanelGroup>
		
	</br:brPanelGrid>
	
	  <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incAssocLoteServModalidade_label_modalidade_servico}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brSelectOneMenu id="cboModalidadeServico" value="#{assocLoteServModalidadeBean.cboServRelacionado}" disabled = "#{assocLoteServModalidadeBean.cboTipoServico == null || assocLoteServModalidadeBean.cboTipoServico == 0}"  >
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
			<f:selectItems value="#{assocLoteServModalidadeBean.listaModalidadeServicoIncluir}" />
			<brArq:commonsValidator type="required" arg="#{msgs.incAssocLoteServModalidade_label_modalidade_servico}" server="false" client="true"/>
		</br:brSelectOneMenu>
		
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup></br:brPanelGroup>
			</br:brPanelGrid>  
		</br:brPanelGroup>
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incAssocLoteServModalidade_label_tipo_lote}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brSelectOneMenu id="cboTipoLote" value="#{assocLoteServModalidadeBean.cboTipoLote}">
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
			<f:selectItems value="#{assocLoteServModalidadeBean.listaTipoLote}"/>
			<brArq:commonsValidator type="required" arg="#{msgs.incAssocLoteServModalidade_label_tipo_lote}" server="false" client="true"/>
		</br:brSelectOneMenu>
		
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup></br:brPanelGroup>
			</br:brPanelGrid>  
		</br:brPanelGroup>
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incAssocLoteServModalidade_label_tipo_layout_arquivo}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
		</br:brPanelGroup>						
		
		<br:brSelectOneMenu id="cboTipoLayoutArquivo" value="#{assocLoteServModalidadeBean.cboTipoLayoutArquivo}">
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
			<f:selectItems value="#{assocLoteServModalidadeBean.listaTipoLayoutArquivo}" />
			<brArq:commonsValidator type="required" arg="#{msgs.incAssocLoteServModalidade_label_tipo_layout_arquivo}" server="false" client="true"/>
		</br:brSelectOneMenu>
		
	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>		

	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.incAssocLoteServModalidade_label_botao_voltar}" action="#{assocLoteServModalidadeBean.voltarIncluir}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar"  styleClass="bto1" value="#{msgs.incAssocLoteServModalidade_label_botao_avancar}" 
     			action="#{assocLoteServModalidadeBean.avancarIncluir}"  onclick="javascript: validar(document.forms[1],'#{msgs.label_ocampo}','#{msgs.label_necessario}', '#{msgs.incAssocLoteServModalidade_label_tipo_servico}', '#{msgs.incAssocLoteServModalidade_label_modalidade_servico}', '#{msgs.incAssocLoteServModalidade_label_tipo_lote}','#{msgs.incAssocLoteServModalidade_label_tipo_layout_arquivo}')">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	