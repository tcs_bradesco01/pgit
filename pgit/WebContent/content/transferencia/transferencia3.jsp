<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="brHtml"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<brHtml:brPanelGrid border="0" align="center" style="height:50">

	<brHtml:brOutputTextBold
		value="#{msgs.transferencia3_aviso1}" />
	<brHtml:brOutputText
		value="#{msgs.transferencia3_aviso2}" />

</brHtml:brPanelGrid>

<br>
<br>

<h:form>
<brHtml:brPanelGrid border="1" align="center">

	<brHtml:brPanelGrid border="0">

		<brHtml:brPanelGrid columns="2" border="0">
			<brHtml:brGraphicImage url="/images/logo.gif" style="border-width:0">
			</brHtml:brGraphicImage>
			<brHtml:brOutputTextBold value="#{msgs.transferencia_comprovante_transferencia}"
				style="font-size: 12pt" />
		</brHtml:brPanelGrid>

		<brHtml:brPanelGrid columns="3" border="0" width="100%">
			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_data}" />
				<brHtml:brOutputText
					value="#{transferencia.retornoTransferencia.data}" />
			</brHtml:brPanelGroup>

			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_hora} " />
				<brHtml:brOutputText
					value="#{transferencia.retornoTransferencia.hora}" />
			</brHtml:brPanelGroup>

			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_numero_documento} " />
				<brHtml:brOutputText
					value="#{transferencia.retornoTransferencia.numero_documento}" />
			</brHtml:brPanelGroup>
		</brHtml:brPanelGrid>

		<brHtml:brPanelGrid>
			<c:dataUser param="user" styleClassLabel="HtmlOutputTextBradesco"
				valueLabel="#{msgs.transferencia_debitado_conta}" />
		</brHtml:brPanelGrid>

		<brHtml:brPanelGrid columns="2" width="100%">

			<c:dataUser param="agencia" styleClassLabel="HtmlOutputTextBradesco"
				valueLabel="#{msgs.transferencia_agencia_conta_debito} " />

			<c:dataUser param="conta" styleClassLabel="HtmlOutputTextBradesco"
				valueLabel="#{msgs.transferencia_conta}" />

			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_agencia_conta_credito}" />
				<brHtml:brOutputText value="#{transferencia.agencia}" />
			</brHtml:brPanelGroup>

			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_conta}" />
				<brHtml:brOutputText value="#{transferencia.conta}" />
			</brHtml:brPanelGroup>

			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_nome_favorecido}  " />
				<brHtml:brOutputText value="#{transferencia.nome}" />
			</brHtml:brPanelGroup>

			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_cpf}  " />
				<brHtml:brOutputText value="#{transferencia.cpf}" />
			</brHtml:brPanelGroup>

			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_valor} " />
				<brHtml:brOutputText value="#{transferencia.valor}" />
			</brHtml:brPanelGroup>
			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_numero_controle}  " />
				<brHtml:brOutputText
					value="#{transferencia.retornoTransferencia.numero_controle}" />
			</brHtml:brPanelGroup>
		</brHtml:brPanelGrid>


	</brHtml:brPanelGrid>

</brHtml:brPanelGrid>

<brHtml:brPanelGrid align="center" style="height:50">
	<brHtml:brPanelGroup>
		<brHtml:brCommandButton alt="#{msgs.botao_sair}" image="/images/bt_encerrar_sessao.gif"
			action="logout" >
		<f:actionListener type="br.com.bradesco.web.aq.view.actionlisteners.LogoutController" />
		</brHtml:brCommandButton>	
		<brHtml:brCommandButton id="button_voltar" alt="#{msgs.botao_voltar}" image="/images/bt_voltar.gif"
			action="nav_transferencia" actionListener="#{transferencia.processarEventos}" >
		</brHtml:brCommandButton>	
			
	</brHtml:brPanelGroup>
</brHtml:brPanelGrid>
</h:form>
