<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="brHtml"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>

<h:form>
	<brHtml:brPanelGrid styleClass="HtmlPanelGridBradesco" width="100%">

		<brHtml:brPanelGroup>
			<brHtml:brPanelGrid bgcolor="#E9E9E9">
				<brHtml:brOutputText
					value="#{msgs.transferencia2_aviso1}" />
			</brHtml:brPanelGrid>
		</brHtml:brPanelGroup>

		<brHtml:brPanelGrid columns="2" border="0" width="95%">

			<brHtml:brOutputTextBold value="#{msgs.transferencia_Da} " />
			<brHtml:brPanelGrid>
				<brHtml:brOutputTextBold value="#{msgs.transferencia_Para} " />
			</brHtml:brPanelGrid>

			<brHtml:brPanelGroup>
				<c:dataUser param="agencia" styleClassLabel="HtmlOutputTextBradesco"
					valueLabel="#{msgs.transferencia_agencia} " />
				<c:dataUser param="conta" styleClassLabel="HtmlOutputTextBradesco"
					valueLabel="#{msgs.transferencia_conta} " />
			</brHtml:brPanelGroup>

			<brHtml:brPanelGrid columns="6" border="0" style="text-align:left">
				<brHtml:brOutputText value="#{msgs.transferencia_banco} " />
				<brHtml:brOutputText value="#{transferencia.banco}" />

				<brHtml:brOutputText value="#{msgs.transferencia_agencia} " />
				<brHtml:brOutputText value="#{transferencia.agencia}" />

				<brHtml:brOutputText value="#{transferencia.conta} " />
				<brHtml:brPanelGroup>
					<brHtml:brOutputText value="#{transferencia.conta} - " />
					<brHtml:brOutputText value="#{transferencia.digito}" />
				</brHtml:brPanelGroup>
			</brHtml:brPanelGrid>



			<c:dataUser param="user" styleClassLabel="HtmlOutputTextBradesco"
				valueLabel="#{msgs.transferencia_cliente} " />
			
			<brHtml:brPanelGrid>
			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_nome} " />
				<brHtml:brOutputText value="#{transferencia.nome}" />
			</brHtml:brPanelGroup>
			</brHtml:brPanelGrid>

			<brHtml:brOutputText value="" />
			
			<brHtml:brPanelGrid>
			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_cpf}" />
				<brHtml:brOutputText value="#{transferencia.cpf}" />
			</brHtml:brPanelGroup>
			</brHtml:brPanelGrid>
			<brHtml:brOutputText value="" />
			
			<brHtml:brPanelGrid>
			<brHtml:brPanelGroup>
				<brHtml:brOutputTextBold value="#{msgs.transferencia_dataagendamento_transferencia} " />
				<brHtml:brOutputText value="#{transferencia.data}" />
			</brHtml:brPanelGroup>
			</brHtml:brPanelGrid>
			<brHtml:brOutputText value="" />
			<brHtml:brPanelGrid>
			<brHtml:brPanelGroup>
				<brHtml:brOutputText value="#{msgs.transferencia_valor} " />
				<brHtml:brOutputText value=" #{transferencia.valor} " />
			</brHtml:brPanelGroup>
			</brHtml:brPanelGrid>

		</brHtml:brPanelGrid>

		<brHtml:brPanelGrid width="100%" columns="2" align="center"
			style="text-align:center">
			<brHtml:brOutputText value="" />
			<brHtml:brPanelGrid style="text-align:center">
				<brHtml:brOutputTextBold
					value="#{msgs.transferencia2_aviso2}" />
			</brHtml:brPanelGrid>
		</brHtml:brPanelGrid>
		
		<brHtml:brPanelGrid columns="2" cellspacing="0" border="0" width="100%" 
				columnClasses="loginTdHalfWidth,loginTdHalfWidth">
				<brHtml:brPanelGroup>
					<brHtml:brPanelGrid columns="1" cellspacing="5" border="0" width="100%">
						<brHtml:brPanelGroup>
							<brHtml:brOutputText styleClass="loginHeader" value="#{msgs.login_tancode_1} "/>
						</brHtml:brPanelGroup>
						<brHtml:brPanelGroup>
							<brHtml:brOutputFormat styleClass="loginHeader" value="#{msgs.login_tancode_2}">
								<f:param value="#{IDENTIFICATION_BACKING.tancodePosition}" />
							</brHtml:brOutputFormat>
							<brHtml:brInputSecret id="j_tancode" value="#{transferencia.tamCode}" size="3" maxlength="3"/>
							<brHtml:brOutputText styleClass="loginHeader" value=" #{msgs.login_tancode_3}"/>
						</brHtml:brPanelGroup>
					</brHtml:brPanelGrid>
				</brHtml:brPanelGroup>
				<brHtml:brPanelGroup>
					<brHtml:brGraphicImage alt="#{msgs.figura_cadeado_seguranca}" url="/images/chavesegurancabradesco.jpg" />
				</brHtml:brPanelGroup>
			</brHtml:brPanelGrid>

		<brHtml:brPanelGrid align="center">
			<brHtml:brPanelGroup>
				<brHtml:brCommandButton alt="#{msgs.botao_voltar}" image="/images/bt_voltar.gif" action="nav_transferencia"/>				
				<brHtml:brCommandButton alt="#{msgs.botao_confirmar}" id="button_transferencia" image="/images/bt_confirmar.gif"
					action="nav_transferencia2" actionListener="#{transferencia.processarEventos}">
				</brHtml:brCommandButton>	
			</brHtml:brPanelGroup>
		</brHtml:brPanelGrid>

	</brHtml:brPanelGrid>
</h:form>
