<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="brHtml"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="brApp" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>

<h:form id="clientValidateRequiredForm">
	<brHtml:brPanelGrid styleClass="HtmlPanelGridBradesco">
		<brHtml:brPanelGroup>
			<brHtml:brPanelGrid styleClass="HtmlPanelGridBradesco"
				bgcolor="#E9E9E9">
				<brHtml:brOutputText value="#{msgs.transferencia_aviso}" />
			</brHtml:brPanelGrid>
			<brHtml:brPanelGrid align="center">
				<brHtml:brOutputText value="#{msgs.transferencia_aviso1}" />
			</brHtml:brPanelGrid>
		</brHtml:brPanelGroup>

		<brHtml:brPanelGrid columns="3" border="0" align="left">
			<brHtml:brOutputText value="#{msgs.transferencia_banco}" />
			<brHtml:brInputText onkeyup="getQuery(this.value)" id="banco1" value="#{transferencia.banco}"
				maxlength="3" size="8"  >
				<arq:commonsValidator type="integer" arg="#{msgs.transferencia_banco}" server="false" client="true"/>
				<arq:commonsValidator type="required"
					arg="#{msgs.transferencia_banco}" server="false" client="true" />
			</brHtml:brInputText>

			<brHtml:brInputText readonly="true" id="nome_banco" maxlength="50" size="50" value="#{transferencia.nomeBanco}"  />
		</brHtml:brPanelGrid>

		<brHtml:brPanelGrid columns="6" border="0" align="left">
			<brApp:agenciaConta id="agenciaConta" 
				sizeAgencia="4" sizeConta="8" sizeDc="1"
				maxlengthAgencia="4" maxlengthConta="7" maxlengthDc="1" value="#{transferencia.agenciaConta}"/>
				<h:message for="agenciaConta"/>
		</brHtml:brPanelGrid>

		<brHtml:brPanelGrid columns="2" border="0" width="40%">
			<brHtml:brOutputText value="#{msgs.transferencia_nome} " />
			<brHtml:brInputText value="#{transferencia.nome}" maxlength="50"
				size="50">
				<arq:commonsValidator type="required"
					arg="#{msgs.transferencia_nome}" server="false" client="true" />
			</brHtml:brInputText>
		</brHtml:brPanelGrid>
		
		<brHtml:brPanelGrid columns="2" border="0"> 
			<brHtml:brOutputText value="#{msgs.transferencia_cpf} " />
			<brHtml:brPanelGroup >
			
			<brHtml:brInputText id="validatorCPF" value="#{transferencia.cpf}" maxlength="12"
				size="15">
				<f:validator validatorId="CPFValidator"/>
				<arq:commonsValidator type="required"
					arg="#{msgs.transferencia_cpf}" server="false" client="true" />
			</brHtml:brInputText>
			<brHtml:brMessageColor for="validatorCPF" id="cpfmensajes"/>	
			</brHtml:brPanelGroup>
		</brHtml:brPanelGrid>

		<brHtml:brPanelGrid columns="7" border="0" align="left">
			<brHtml:brPanelGroup>
				<brHtml:brOutputText 
					value="#{msgs.transferencia_dataagendamento_transferencia} " />
				<brHtml:brInputText onblur=" return fverDataPosterior(this.value)" value="#{transferencia.data}"  size="15">
					<arq:commonsValidator type="required"
						arg="#{msgs.transferencia_data}" server="false" client="true" />
					<arq:commonsValidator type="date" datePatternStrict="dd/MM/yyyy"
						arg="#{msgs.transferencia_dataagendamento_transferencia}"
						server="false" client="true" />
				</brHtml:brInputText>
				<brHtml:brOutputTextColor
					value=" #{msgs.transferencia_formato_data}" />
			</brHtml:brPanelGroup>
		</brHtml:brPanelGrid>

		<brHtml:brPanelGrid columns="3" border="0" align="left">
			<brHtml:brOutputText value="#{msgs.transferencia_valor}" />
			<brHtml:brInputText value="#{transferencia.valor}" maxlength="15"
				size="14">
				<arq:commonsValidator type="mask" mask="^[0-9]*,([0-9]{2})$"
					arg="#{msgs.transferencia_valor}" server="false" client="true" />
				<arq:commonsValidator type="required"
					arg="#{msgs.transferencia_valor}" server="false" client="true" />
			</brHtml:brInputText>
			<brHtml:brOutputTextColor
				value=" #{msgs.transferencia_formato_valor}" />
		</brHtml:brPanelGrid>

		<brHtml:brPanelGrid align="center" style="text-align:center">
			<brHtml:brOutputText value="#{msgs.transferencia_aviso2}" />
		</brHtml:brPanelGrid>

		<brHtml:brPanelGrid align="center">
			<brHtml:brPanelGroup>
				<brHtml:brCommandLink title="#{msgs.botao_voltar}" id="button_voltar" action="home_page2" actionListener="#{transferencia.processarEventos}">
					<brHtml:brGraphicImage alt="#{msgs.botao_voltar}"  url="/images/bt_voltar.gif" />
				</brHtml:brCommandLink>

				<brHtml:brCommandLink title="#{msgs.botao_limpar}" id="button_limpar" action="nav_transferencia" actionListener="#{transferencia.processarEventos}" >
					<brHtml:brGraphicImage  alt="#{msgs.botao_limpar}" url="/images/bt_limpar.gif" />
				</brHtml:brCommandLink>

				<brHtml:brCommandButton image="/images/bt_continuar.gif" id="button_enviar"
					action="nav_transferencia1" title="#{msgs.botao_continuar}" onclick="javascript: return validar();" actionListener="#{transferencia.processarEventos}"/>
			</brHtml:brPanelGroup>
		</brHtml:brPanelGrid>
	</brHtml:brPanelGrid>
<arq:validatorScript functionName="validateForm"/>
</h:form>