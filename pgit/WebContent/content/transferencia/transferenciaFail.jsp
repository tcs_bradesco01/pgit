<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="brHtml"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

<h:form>
<brHtml:brPanelGrid border="1" align="center">

	<brHtml:brPanelGrid border="0">

		<brHtml:brPanelGrid>
			<brHtml:brOutputTextTitleColor value="#{msgs.transferencia_failed}" />
			<h:messages globalOnly="true" showSummary="true" />
		</brHtml:brPanelGrid>
	</brHtml:brPanelGrid>

</brHtml:brPanelGrid>

<brHtml:brPanelGrid align="center" style="height:50">
	<brHtml:brPanelGroup>
		<brHtml:brCommandButton id="button_voltar" alt="#{msgs.botao_voltar}" image="/images/bt_voltar.gif"
			action="nav_transferencia" actionListener="#{transferencia.processarEventos}" >
		</brHtml:brCommandButton>	
	</brHtml:brPanelGroup>
</brHtml:brPanelGrid>
</h:form>