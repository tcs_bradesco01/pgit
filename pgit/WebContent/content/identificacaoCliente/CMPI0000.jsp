<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>

<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="identificacaoClienteForm" name="identificacaoClienteForm" >
<h:inputHidden id="hiddenRadioCliente" value="#{cmpi0000Bean.hiddenRadioCliente}"/>
<h:inputHidden id="hiddenRadioFiltroArgumento" value="#{cmpi0000Bean.hiddenRadioFiltroArgumento}"/>
<h:inputHidden id="hiddenRadioValidaCPFCliente" value="#{cmpi0000Bean.validaCpfCliente}"/>
<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0" >
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
			<t:dataTable id="dataTable" value="#{cmpi0000Bean.listaGridCliente}" var="result" rows="10" 
			cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
			columnClasses="alinhamento_centro, alinhamento_direita, alinhamento_esquerda, alinhamento_direita"
			headerClass="tabela_celula_destaque_acentuado" width="750px">
			  <t:column width="30px" style="padding-right:5px; padding-left:5px">
				<t:selectOneRadio onclick="javascript:habilitarBotaoConfirmarCliente(document.forms[1], this);" id="sor2" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
					<f:selectItems value="#{cmpi0000Bean.listaControleCliente}"/>
				</t:selectOneRadio>
				<t:radio for="sor2" index="#{result.totalRegistros}" />
			  </t:column>	
			  <t:column width="130px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.cmpi0000_label_cpf_cnpj}" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.cpf}" />
			  </t:column>
			  <t:column width="330px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_nome_razao_social}" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.nomeRazao}" />
			  </t:column>
			  <t:column width="200px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_data_nascimento_fundacao}" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.dataNascimentoFundacao}" />
			  </t:column>			 		 
			</t:dataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" rendered="#{cmpi0000Bean.listaGridCliente!= null && cmpi0000Bean.mostraBotoes0000}">	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{cmpi0000Bean.pesquisar}" >
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
		<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="2" width="750"  style="text-align:right" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>		
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brCommandButton id="btnConfirmar" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.botao_confirmar}" action="#{cmpi0000Bean.confirmarCliente}" disabled="true">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnVoltar" styleClass="HtmlCommandButtonBradesco" value="#{msgs.botao_voltar}" action="#{cmpi0000Bean.voltarIdentificacaoCliente}">
				<brArq:submitCheckClient/>
			</br:brCommandButton> 
		</br:brPanelGroup>

		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid>
			<br:brPanelGroup>
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGrid>	
			  
	</br:brPanelGrid>					
</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>
