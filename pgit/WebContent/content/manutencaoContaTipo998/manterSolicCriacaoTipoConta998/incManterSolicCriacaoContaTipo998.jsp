<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incManterSolicCriacaoContaTipo998Form" name="incManterSolicCriacaoContaTipo998Form">

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicCriacaoContaTipo998Bean.identificacaoClienteContratoBean.saidaListarClubesClientes.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px">
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicCriacaoContaTipo998Bean.identificacaoClienteContratoBean.saidaListarClubesClientes.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"> </f:verbatim>

	<a4j:outputPanel id="painelArgumentos" style="width: 100%; text-align: left" ajaxRendered="true">					
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_conta_conta_debito}:"/>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
			<a4j:outputPanel id="panelBancoContaDebitoCheck" ajaxRendered="true">
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{manterSolicCriacaoContaTipo998Bean.cdBancoContaDebitoInc}" size="4" maxlength="3" id="txtBancoContaDebitoCheck" onkeyup="proximoCampo(3,'incManterSolicCriacaoContaTipo998Form','incManterSolicCriacaoContaTipo998Form:txtBancoContaDebitoCheck','incManterSolicCriacaoContaTipo998Form:txtAgenciaContaDebitoCheck');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgenciaContaDebitoCheck" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{manterSolicCriacaoContaTipo998Bean.cdAgenciaBancariaContaDebitoInc}" size="6" maxlength="5" id="txtAgenciaContaDebitoCheck" onkeyup="proximoCampo(5,'incManterSolicCriacaoContaTipo998Form','incManterSolicCriacaoContaTipo998Form:txtAgenciaContaDebitoCheck','incManterSolicCriacaoContaTipo998Form:txtGrupoContabilCheck');"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelRazaoCheck" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_razao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>				

			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterSolicCriacaoContaTipo998Bean.cdGrupoContabilInc}" size="4" maxlength="3" id="txtGrupoContabilCheck" onkeyup="proximoCampo(3,'incManterSolicCriacaoContaTipo998Form','incManterSolicCriacaoContaTipo998Form:txtGrupoContabilCheck','incManterSolicCriacaoContaTipo998Form:txtSubGrupoContabilCheck');"/>
								
						<br:brInputText style="margin-left:5px;margin-right:20px" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterSolicCriacaoContaTipo998Bean.cdSubGrupoContabilInc}" size="4" maxlength="3" id="txtSubGrupoContabilCheck" onkeyup="proximoCampo(3,'incManterSolicCriacaoContaTipo998Form','incManterSolicCriacaoContaTipo998Form:txtSubGrupoContabilCheck','incManterSolicCriacaoContaTipo998Form:txtContaDebitoCheck');"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelContaDebitoCheck" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterSolicCriacaoContaTipo998Bean.cdContaBancariaContaDebitoInc}" size="17" maxlength="13" id="txtContaDebitoCheck" onkeyup="proximoCampo(13,'incManterSolicCriacaoContaTipo998Form','incManterSolicCriacaoContaTipo998Form:txtContaDebitoCheck','incManterSolicCriacaoContaTipo998Form:txtDigContaDebitoCheck');"/>
								
						<br:brInputText style="margin-left:5px" styleClass="HtmlInputTextBradescoUpperCase" value="#{manterSolicCriacaoContaTipo998Bean.cdDigitoContaContaDebitoInc}" size="2" maxlength="1" id="txtDigContaDebitoCheck" onkeypress="validarCampoNumerosLetras();" onfocus="cleanClipboard();"/>
					</br:brPanelGroup>
				</br:brPanelGrid>				
			</a4j:outputPanel>
	    </br:brPanelGrid>
	</a4j:outputPanel>
	
	<f:verbatim><hr class="lin"></f:verbatim>
		
	<a4j:outputPanel id="panelBotoes" style="width: 100%;" ajaxRendered="true">		
	 	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" border="0" width="100%">
		 	<br:brPanelGrid columns="1" style="text-align:left" cellpadding="0" cellspacing="0" border="0" width="100%">
				<br:brPanelGroup>
					<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.label_botao_voltar}" style="margin-right:5px" action="#{manterSolicCriacaoContaTipo998Bean.voltarMenu}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="text-align:right" cellpadding="0" cellspacing="0" border="0" width="100%">
				<br:brPanelGroup>
					<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.botao_avancar}" style="margin-right:5px" action="#{manterSolicCriacaoContaTipo998Bean.avancarIncluir}"
					onclick="javascript:desbloquearTela(); return validaCamposInclusao(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
					 '#{msgs.consultarPagamentosIndividual_agencia}',
					 '#{msgs.label_razao}',
					 '#{msgs.consultarPagamentosIndividual_conta}',
					 '#{msgs.label_deve_ser_diferente_de_zeros}');">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>		
	</a4j:outputPanel>

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
    
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />

</brArq:form>