<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detManterSolicCriacaoContaTipo998Form" name="detManterSolicCriacaoContaTipo998Form">

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.cpfCnpjFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.dsNomePssoa}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"> </f:verbatim>
    
    <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_solicitacao}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.pesq_emissaoaut_label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.dsSituacaoSolicitacao}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.conManterSolManutContratos_motivo_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.dsMotvoSolicit}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"> </f:verbatim>

	<a4j:outputPanel id="painelArgumentos" style="width: 100%; text-align: left" ajaxRendered="true">					
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_conta_conta_debito}:"/>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<a4j:outputPanel id="panelBancoContaDebitoCheck" ajaxRendered="true">
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.cdBanco} - #{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.dsBanco}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgenciaContaDebitoCheck" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.cdAgencia} - #{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.dsAgencia}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelRazaoCheck" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_razao}:"/>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.cdGrupoContabil} - #{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.cdSubGrupoContabil}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>				

			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelContaDebitoCheck" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.cdConta} - #{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.cdDigitoConta}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>		
			</a4j:outputPanel>
	    </br:brPanelGrid>
	</a4j:outputPanel>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_data_hora_inclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.dtHoraInclusao}" converter="timestampPdcConverter" />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.cdUsuarioInclusao}" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.dsTipoCanal}" />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicCriacaoContaTipo998Bean.saidaDetalharContaTipo.complementoInclusao}" />
		</br:brPanelGroup>
	</br:brPanelGrid>

    <f:verbatim><hr class="lin"> </f:verbatim>
		
		<a4j:outputPanel id="panelBotoes" style="width: 100%;" ajaxRendered="true">		
		 	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" border="0" width="100%">
			 	<br:brPanelGrid columns="1" style="text-align:left" cellpadding="0" cellspacing="0" border="0" width="100%">
					<br:brPanelGroup>
						<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.label_botao_voltar}" style="margin-right:5px" action="#{manterSolicCriacaoContaTipo998Bean.voltarMenu}">
							<brArq:submitCheckClient/>
						</br:brCommandButton>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>		
		</a4j:outputPanel>

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
    
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />

</brArq:form>