<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="incManterSolicGeracaoArqRetBaseFavForm" name="incManterSolicGeracaoArqRetBaseFavForm" >
<br:brPanelGrid columns="1" style="margin-top:9" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup> 
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicGeracaoArqRetBaseFavBean.nrCnpjCpf}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.dsRazaoSocial}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
  
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.dsEmpresa}"/>			
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_numero_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.nroContrato}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
   <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_descricao_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.dsContrato}"/>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.cdSituacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
 	   
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">	

  <br:brPanelGrid>
        <br:brPanelGroup>			
			  <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_solicitacao}:"/>
		</br:brPanelGroup>
   </br:brPanelGrid>
   	 
	 
	<br:brPanelGrid columns="3" style="margin-top:6px">		
		<br:brPanelGroup>
				<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_tipo_favorecido}: " />
				</br:brPanelGroup>	
			 
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTipoFavorecido"  value="#{manterSolicGeracaoArqRetBaseFavBean.cboTipoFavorecido}"  style="width: 160" styleClass="HtmlSelectOneMenuBradesco">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{manterSolicGeracaoArqRetBaseFavBean.listaTipoFavorecido}"/>
							 </br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
				<br:brPanelGroup style="margin-left:20px">
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_situacao_favorecido}: " />
				</br:brPanelGroup>	
			 
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup style="margin-left:20px">
							<br:brSelectOneMenu id="cboSituacaoFavorecido"  value="#{manterSolicGeracaoArqRetBaseFavBean.cboSituacaoFavorecido}" styleClass="HtmlSelectOneMenuBradesco" style="margin-right:20px">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItem itemValue="1" itemLabel="#{msgs.label_ativo}"/>
								<f:selectItem itemValue="2" itemLabel="#{msgs.label_bloqueado}"/>								
								<f:selectItem itemValue="3" itemLabel="#{msgs.label_inativo}"/>		
								<f:selectItem itemValue="4" itemLabel="#{msgs.label_excluido}"/>		
							 </br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
 				<br:brPanelGroup >
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_destino}: " />
				</br:brPanelGroup>			
			 
			 	<br:brPanelGroup>
					<t:selectOneRadio id="radioDestino" value="#{manterSolicGeracaoArqRetBaseFavBean.radioDestino}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false" >  
						<f:selectItem itemValue="1" itemLabel="#{msgs.label_cliente}" />
						<f:selectItem itemValue="2" itemLabel="#{msgs.label_departamento_gestor}" />
					</t:selectOneRadio>		
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>	
					
	</br:brPanelGrid>
     		 
   	<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%">
     		 
		<f:verbatim><hr class="lin"></f:verbatim>
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}: "/>
					<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.vlTarifaPadrao}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />			
				<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_desconto_tarifa}: "/>
		    	<br:brInputText size="9" id="txtDescontoTarifa" alt="percentual" value="#{manterSolicGeracaoArqRetBaseFavBean.vlDescontoTarifa}" converter="decimalBrazillianConverter" style="text-align:right; margin-right:3px" styleClass="HtmlInputTextBradesco" 
		    		onfocus="loadMasks();" onblur="validarCampoDescontoTarifa(this)" disabled="#{manterSolicGeracaoArqRetBaseFavBean.desabilitaCampoDescontoTarifa}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="%"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}: "/>
					<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.vlTarifaAtualizada}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>

	</a4j:outputPanel>	
		
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{manterSolicGeracaoArqRetBaseFavBean.voltarConsultar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_limpar}" action="#{manterSolicGeracaoArqRetBaseFavBean.limparRadiosPrincipaisIncluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnCalcular" styleClass="bto1" style="margin-right:5px;" value="#{msgs.btn_calcular}" action="#{manterSolicGeracaoArqRetBaseFavBean.calcularTarifaAtualizada}" disabled="#{manterSolicGeracaoArqRetBaseFavBean.indicadorDescontoBloqueio}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAvancarIncluir" styleClass="bto1" value="#{msgs.label_avan�ar}"  action="#{manterSolicGeracaoArqRetBaseFavBean.avancarIncluir}" onclick="javascript:desbloquearTela(); return validaCamposIncluir(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_destino}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>  
    </a4j:outputPanel>	 	 

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   	
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>