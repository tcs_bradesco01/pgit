<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%> 
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="detManterSolicGeracaoArqRetBaseFavForm" name="detManterSolicGeracaoArqRetBaseFavForm" >
<br:brPanelGrid columns="1" style="margin-top:9" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
 		
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup> 
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicGeracaoArqRetBaseFavBean.nrCnpjCpf}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.dsRazaoSocial}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
  
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.dsEmpresa}"/>			
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_numero_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.nroContrato}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
   <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_descricao_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.dsContrato}"/>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.cdSituacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_solicitacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_favorecido}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.tipoFavorecido}"/>
		</br:brPanelGroup> 	
		<br:brPanelGroup> 
			<br:brGraphicImage style="margin-left:20px" url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao_favorecido}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.situacaoFavorecido}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup> 
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_destino}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cliente}" rendered="#{manterSolicGeracaoArqRetBaseFavBean.destino == 1}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_departamento_gestor}" rendered="#{manterSolicGeracaoArqRetBaseFavBean.destino == 2}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>    

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_detalhes_solicitacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_solicitacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.numeroSolic}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_hora_solicitacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.horaSolicitacao}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.situacaoSolicitacao}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_hora_atendimento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.horaAtendimento}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_resultado_do_processamento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.resultadoProcessamento}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>   
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_qtde_abrev_registros}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.qtdeRegistros}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="R$ " rendered="#{manterSolicGeracaoArqRetBaseFavBean.vlTarifaPadrao != NULL}"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.vlTarifaPadrao}" converter="decimalBrazillianConverter"/>
		</br:brPanelGroup> 	
		<br:brPanelGroup> 
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_desconto_tarifa}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.vlDescontoTarifa}" converter="decimalBrazillianConverter" style="margin-right:0px"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="%" rendered="#{manterSolicGeracaoArqRetBaseFavBean.vlDescontoTarifa != NULL}"/>		
		</br:brPanelGroup>	
		<br:brPanelGroup> 
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="R$ " rendered="#{manterSolicGeracaoArqRetBaseFavBean.vlTarifaAtualizada != NULL}"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.vlTarifaAtualizada}" converter="decimalBrazillianConverter"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>    
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_hora_inclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.dataHoraInclusao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.usuarioInclusao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.tipoCanalInclusao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.complementoInclusao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.grid_data_hora_manutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.dataHoraManutencao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.usuarioManutencao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.tipoCanalManutencao}"/> 
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseFavBean.complementoManutencao}"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="text-align:left;width:750px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{manterSolicGeracaoArqRetBaseFavBean.voltarConsultar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   	
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>