<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incManterSolicGeracaoArqRetBasePagtos2Form" name="incManterSolicGeracaoArqRetBasePagtos2Form">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicGeracaoArqRetBasePagtosBean.nrCnpjCpf}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.dsRazaoSocial}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
  
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.dsEmpresa}"/>			
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_numero_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.nroContrato}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
   <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_descricao_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.dsContrato}"/>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.cdSituacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid> 	   

	
		<f:verbatim><hr class="lin"></f:verbatim>    

	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_solicitacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_de_pagamento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.dataFormatadaDe} "/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_a} "/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.dataFormatadaAte}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.tipoServico}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_modalidade}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.modalidade}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta_de_debito}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.contaDebitoFormatado}"/>			
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pagamentos_efetuados}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.indicadorPagosFormatado}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>   
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pagamentos_nao_efetuados}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.indicadorNaoPagosFormatado}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>   
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pagamentos_agendados}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.indicadorAgendadosFormatado}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>   
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_origem_recurso}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.origemDesc}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>   
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_destino}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.destinoDesc}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>   
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_favorecido}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.nmFavorecidoInc}"/>
		</br:brPanelGroup> 	
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cod_favorecido}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.cdFavorecidoInc}"/>
		</br:brPanelGroup> 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>   
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_num_inscricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.inscricaoFavorecidoInc}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.dsTipoFavorecidoInc}"/>
		</br:brPanelGroup> 	 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>  
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_mnemonico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.mnemonico}"/>
		</br:brPanelGroup>		 			
	</br:brPanelGrid>
		
	
	<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%">
		
		<f:verbatim><hr class="lin"></f:verbatim>
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:9px" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="R$ "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.vlTarifaPadrao}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup> 	
			<br:brPanelGroup> 
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_desconto_tarifa}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.vlDescontoTarifa}" converter="decimalBrazillianConverter"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="%" rendered="#{not empty manterSolicGeracaoArqRetBasePagtosBean.vlDescontoTarifa}"/>
			</br:brPanelGroup>
			<br:brPanelGroup> 
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="R$ " rendered="#{not empty manterSolicGeracaoArqRetBasePagtosBean.vlTarifaAtualizada}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.vlTarifaAtualizada}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>  
	</br:brPanelGrid>  
	 
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">		
	  	<br:brPanelGroup style="text-align:left;width:40px">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{manterSolicGeracaoArqRetBasePagtosBean.voltarIncluir}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGroup style="text-align:right;width:710px">
			<br:brCommandButton id="btnConfirmar" styleClass="bto1" value="#{msgs.btn_confirmar}" onclick=" javascript: if (!confirm('#{msgs.label_conf_inclusao}')) { desbloquearTela(); return false; }" action="#{manterSolicGeracaoArqRetBasePagtosBean.confirmarIncluir}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>  
		
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	