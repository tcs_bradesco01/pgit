<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="incManterSolicGeracaoArqRetBasePagtosForm" name="incManterSolicGeracaoArqRetBasePagtosForm" >
<h:inputHidden id="hiddenFoco" value="#{manterSolicGeracaoArqRetBasePagtosBean.foco}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">


	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicGeracaoArqRetBasePagtosBean.nrCnpjCpf}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.dsRazaoSocial}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
  
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.dsEmpresa}"/>			
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_numero_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.nroContrato}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
   <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_descricao_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.dsContrato}"/>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.cdSituacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid> 	   
		

    <f:verbatim><hr class="lin"></f:verbatim>
    
    <a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true"> 
	   <br:brPanelGrid>
	        <br:brPanelGroup>			
				  <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_solicitacao}:"/>
			</br:brPanelGroup>
	   </br:brPanelGrid>
    
       <br:brPanelGrid columns="1" style="margin-top:9px">
	    	<br:brPanelGroup>		
                <br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_data_de_pagamento}: "/>
				</br:brPanelGroup> 
	
            	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    		<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			
				<a4j:outputPanel id="calendarios" style="text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
							 <br:brPanelGroup>
									<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco"  value="#{msgs.label_de}:"/>
							 </br:brPanelGroup>	
							<br:brPanelGroup>
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoInicio');" id="txtPeriodoInicio" value="#{manterSolicGeracaoArqRetBasePagtosBean.dataPagamentoDe}" >
								</app:calendar>
							</br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_a}:"/>
							<br:brPanelGroup>
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoFim');" id="txtPeriodoFim" value="#{manterSolicGeracaoArqRetBasePagtosBean.dataPagamentoAte}" >
				 				</app:calendar>	
							</br:brPanelGroup>	
						</br:brPanelGroup>			    
					</br:brPanelGrid>
				</a4j:outputPanel>				
			</br:brPanelGroup>
    	</br:brPanelGrid>
    
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
       		
      	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">			
		    <br:brPanelGroup>	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
					<h:selectBooleanCheckbox id="chkTipoServico" value="#{manterSolicGeracaoArqRetBasePagtosBean.chkTipoServico}" style="margin-right:5px">
					<a4j:support oncomplete="javascript:foco(this);" event="onclick" action="#{manterSolicGeracaoArqRetBasePagtosBean.controleServicoModalidade}" reRender="cboTipoServico,cboModalidade" /> 
					</h:selectBooleanCheckbox>					
				</br:brPanelGrid>	
			</br:brPanelGroup>
	
			<br:brPanelGroup>			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_de_servico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
							
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="text-align:left;">					
				    <br:brPanelGroup> 
				    	<br:brSelectOneMenu id="cboTipoServico" value="#{manterSolicGeracaoArqRetBasePagtosBean.cboTipoServico}" styleClass="HtmlSelectOneMenuBradesco" disabled="#{!manterSolicGeracaoArqRetBasePagtosBean.chkTipoServico}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterSolicGeracaoArqRetBasePagtosBean.listaServicos}" />	
							<a4j:support oncomplete="javascript:foco(this);"  status="statusAguarde" event="onchange" reRender="cboModalidade" action="#{manterSolicGeracaoArqRetBasePagtosBean.listarModalidades}"/>												
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>			
		   </br:brPanelGroup>
		</br:brPanelGrid>
			
	    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
				</br:brPanelGrid>	
			</br:brPanelGroup>	
							
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;margin-left:25px">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_modalidade}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			  
				<br:brPanelGrid style="margin-left:25px" columns="1" cellpadding="0" cellspacing="0" >					
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboModalidade" value="#{manterSolicGeracaoArqRetBasePagtosBean.cboModalidade}" styleClass="HtmlSelectOneMenuBradesco"
						  disabled="#{manterSolicGeracaoArqRetBasePagtosBean.cboTipoServico == null || manterSolicGeracaoArqRetBasePagtosBean.cboTipoServico == 0 
						   || !manterSolicGeracaoArqRetBasePagtosBean.chkTipoServico}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterSolicGeracaoArqRetBasePagtosBean.listaModalidades}" />												
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
      		<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>				
						<h:selectBooleanCheckbox id="chkContaDebito" value="#{manterSolicGeracaoArqRetBasePagtosBean.chkContaDebito}" style="margin-right:5px" >
						<a4j:support oncomplete="javascript:foco(this);" event="onclick" action="#{manterSolicGeracaoArqRetBasePagtosBean.limparCheckContaDebito}" reRender="panelBancoContaDeb,panelAgenciaContaDeb,panelContaContaDeb, btnConsultarContaDebito, cboTipoConta"/> 
						</h:selectBooleanCheckbox>					
			        </br:brPanelGroup>
	
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_conta_de_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			  
				<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:20px">
					<a4j:outputPanel id="panelBancoContaDeb" ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						<br:brInputText onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{!manterSolicGeracaoArqRetBasePagtosBean.chkContaDebito}" value="#{manterSolicGeracaoArqRetBasePagtosBean.bancoContaDeb}" size="4" maxlength="3" id="txtBancoContaDeb" onkeyup="proximoCampo(3,'incManterSolicGeracaoArqRetBasePagtosForm','incManterSolicGeracaoArqRetBasePagtosForm:txtBancoContaDeb','incManterSolicGeracaoArqRetBasePagtosForm:txtAgenciaContaDeb');">
							<a4j:support  event="onblur" status="statusAguarde" reRender="btnConsultarContaDebito, cmbTipoConta" action="#{manterSolicGeracaoArqRetBasePagtosBean.limparTipoContaDebito}"  />			
						</br:brInputText>
					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelAgenciaContaDeb" ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agencia}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						<br:brInputText styleClass="HtmlInputTextBradesco" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{!manterSolicGeracaoArqRetBasePagtosBean.chkContaDebito}" value="#{manterSolicGeracaoArqRetBasePagtosBean.agenciaContaDeb}" size="6" maxlength="5" id="txtAgenciaContaDeb" onkeyup="proximoCampo(5,'incManterSolicGeracaoArqRetBasePagtosForm','incManterSolicGeracaoArqRetBasePagtosForm:txtAgenciaContaDeb','incManterSolicGeracaoArqRetBasePagtosForm:txtContaContaDeb');">
							<a4j:support  event="onblur" status="statusAguarde" reRender="btnConsultarContaDebito, cmbTipoConta" action="#{manterSolicGeracaoArqRetBasePagtosBean.limparTipoContaDebito}"  />			
						</br:brInputText>
					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelContaContaDeb" ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_conta}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >					
							    <br:brPanelGroup>
									<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" disabled="#{!manterSolicGeracaoArqRetBasePagtosBean.chkContaDebito}"
									 value="#{manterSolicGeracaoArqRetBasePagtosBean.contaContaDeb}" size="17" maxlength="13" id="txtContaContaDeb" style="margin-right:5px">
										<a4j:support  event="onblur" status="statusAguarde" reRender="btnConsultarContaDebito, cmbTipoConta" action="#{manterSolicGeracaoArqRetBasePagtosBean.limparTipoContaDebito}"  />						 
									</br:brInputText>
								</br:brPanelGroup>
						</br:brPanelGrid>				
					</a4j:outputPanel>
				</br:brPanelGrid>
			</br:brPanelGroup>		
		</br:brPanelGrid> 

        <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>

      	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
          	<br:brPanelGroup>	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
				    <br:brPanelGroup>	
					   <h:selectBooleanCheckbox id="chkCompromissosPagos" value="#{manterSolicGeracaoArqRetBasePagtosBean.chkCompromissosPagos}" style="margin-right:5px">
					   </h:selectBooleanCheckbox>	
					</br:brPanelGroup>
									
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_pagamentos_efetuados}"/>
					</br:brPanelGroup>				
				</br:brPanelGrid>				
		 	</br:brPanelGroup>	
	 	</br:brPanelGrid>	
	 
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
	 
	  	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	   		<br:brPanelGroup>	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
					<br:brPanelGroup>	
						<h:selectBooleanCheckbox id="chkCompromissosNaoPagos" value="#{manterSolicGeracaoArqRetBasePagtosBean.chkCompromissosNaoPagos}" style="margin-right:5px">
						</h:selectBooleanCheckbox>	
					</br:brPanelGroup>
					
							
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_pagamentos_nao_efetuados}"/>
					</br:brPanelGroup>				
				</br:brPanelGrid>				
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	 
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
	 
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	    	<br:brPanelGroup>	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
					<br:brPanelGroup>	
						<h:selectBooleanCheckbox id="chkCompromissosAgendados" value="#{manterSolicGeracaoArqRetBasePagtosBean.chkCompromissosAgendados}" style="margin-right:5px" >
						</h:selectBooleanCheckbox>	
					</br:brPanelGroup>
					
							
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_pagamentos_agendados}"/>
					</br:brPanelGroup>				
				</br:brPanelGrid>				
			 </br:brPanelGroup>	
		 </br:brPanelGrid>
	 
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
				
	 	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
			<br:brPanelGroup>				
				<h:selectBooleanCheckbox id="chkFavorecido" value="#{manterSolicGeracaoArqRetBasePagtosBean.chkFavorecido}" style="margin-right:5px" >
				<a4j:support oncomplete="javascript:foco(this);" event="onclick" action="#{manterSolicGeracaoArqRetBasePagtosBean.limparCheckFavorecido}" reRender="rdoFiltroFavorecido,panelFavCodigo,panelFavInscricao,panelFavMnemonico"/> 
				</h:selectBooleanCheckbox>					
	        </br:brPanelGroup>

			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_favorecido}:"/>
			</br:brPanelGroup>
    	</br:brPanelGrid>
			
	    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<a4j:region id="regionFavorecido">
			<t:selectOneRadio id="rdoFiltroFavorecido" value="#{manterSolicGeracaoArqRetBasePagtosBean.itemFiltroFavSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{!manterSolicGeracaoArqRetBasePagtosBean.chkFavorecido}">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />	
				<f:selectItem itemValue="2" itemLabel="" />		
				<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="panelFavCodigo,panelFavInscricao,panelFavMnemonico" ajaxSingle="true" action="#{manterSolicGeracaoArqRetBasePagtosBean.limparCamposFavorecido}"/>
			</t:selectOneRadio>
		</a4j:region>
			 
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">		
			<t:radio for="rdoFiltroFavorecido" index="0" />	 
		
		  	<a4j:outputPanel id="panelFavCodigo" ajaxRendered="true">
		    	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				  
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    		<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" disabled="#{manterSolicGeracaoArqRetBasePagtosBean.itemFiltroFavSelecionado != '0'}" value="#{manterSolicGeracaoArqRetBasePagtosBean.codigoFav}" size="18" maxlength="15" id="txtCodigo"/>		
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</a4j:outputPanel>	

	 		<t:radio for="rdoFiltroFavorecido" index="1" />
	 
			<a4j:outputPanel id="panelFavInscricao" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			    	<br:brPanelGroup>	
				    	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;margin-top:6px"  >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_inscricao}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						  
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						    <br:brPanelGroup>
						    		<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();"  disabled="#{manterSolicGeracaoArqRetBasePagtosBean.itemFiltroFavSelecionado != '1'}" value="#{manterSolicGeracaoArqRetBasePagtosBean.inscricaoFav}" size="18" maxlength="15" id="txtInscricao"/>		
							</br:brPanelGroup>					
						</br:brPanelGrid>
					</br:brPanelGroup>	
				
					<br:brPanelGroup>
					    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;margin-left:20px;">		
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
					    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
					  
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:20px" >	
				    	<br:brSelectOneMenu id="cboTipo" value="#{manterSolicGeracaoArqRetBasePagtosBean.cboTipo}" styleClass="HtmlSelectOneMenuBradesco" disabled="#{manterSolicGeracaoArqRetBasePagtosBean.itemFiltroFavSelecionado != '1'}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>	
							<f:selectItems value="#{manterSolicGeracaoArqRetBasePagtosBean.listaTipoInscricao}" />													
						</br:brSelectOneMenu>			
						</br:brPanelGrid>
					</br:brPanelGroup>
				</br:brPanelGrid>			
			</a4j:outputPanel>	
	 
	  		<t:radio for="rdoFiltroFavorecido" index="2" />
	 
		 	<a4j:outputPanel id="panelFavMnemonico" ajaxRendered="true">
		    	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;margin-top:6px" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_mnemonico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				  
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    		<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{manterSolicGeracaoArqRetBasePagtosBean.itemFiltroFavSelecionado != '2'}" value="#{manterSolicGeracaoArqRetBasePagtosBean.mnemonico}" size="18" maxlength="15" id="txtMnemonico"/>		
					</br:brPanelGroup>					
				</br:brPanelGrid>
		 	</a4j:outputPanel>	
		</br:brPanelGrid> 

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
	
	  <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>	
		    	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_origem_recurso}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			  
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			    	<br:brSelectOneMenu id="cboOrigem" value="#{manterSolicGeracaoArqRetBasePagtosBean.cboOrigem}" styleClass="HtmlSelectOneMenuBradesco" style="margin-right:20px">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>	
						<f:selectItem itemValue="1" itemLabel="#{msgs.label_proprio}"/>
						<f:selectItem itemValue="2" itemLabel="#{msgs.label_compror}"/>									
					</br:brSelectOneMenu>			
				</br:brPanelGrid>
			</br:brPanelGroup>	
		
			<br:brPanelGroup>
				<br:brPanelGroup style="text-align:left;">
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_destino}: " />
				</br:brPanelGroup>			
					
			 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="text-align:left;">		
			 		<br:brPanelGroup>
						<t:selectOneRadio id="radioDestinoFiltro" value="#{manterSolicGeracaoArqRetBasePagtosBean.radioDestino}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false" >  
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_cliente}" />
							<f:selectItem itemValue="2" itemLabel="#{msgs.label_departamento_gestor}" />
						</t:selectOneRadio>		
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>		  
		</br:brPanelGrid>			
		
		<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%">
			<f:verbatim><hr class="lin"></f:verbatim>
		
			<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
				
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="0">
					<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}: "/>
							<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.vlTarifaPadrao}" converter="decimalBrazillianConverter"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />			
						<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_desconto_tarifa}: "/>
				    	<br:brInputText size="9" id="txtDescontoTarifa" alt="percentual" value="#{manterSolicGeracaoArqRetBasePagtosBean.vlDescontoTarifa}" converter="decimalBrazillianConverter" style="text-align:right; margin-right:3px" styleClass="HtmlInputTextBradesco" 
				    		onfocus="loadMasks();" onblur="validarCampoDescontoTarifa(this)" disabled="#{manterSolicGeracaoArqRetBasePagtosBean.desabilitaCampoDescontoTarifa}"/>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="%"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}: "/>
							<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBasePagtosBean.vlTarifaAtualizada}" converter="decimalBrazillianConverter"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</br:brPanelGrid>
	</a4j:outputPanel>
     
  <a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">     
  <f:verbatim><hr class="lin"></f:verbatim>  
    	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">		
	  	<br:brPanelGroup style="text-align:left;width:40px">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{manterSolicGeracaoArqRetBasePagtosBean.voltarPrincipal}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGroup style="text-align:right;width:710px">
			<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.btn_limpar}" action="#{manterSolicGeracaoArqRetBasePagtosBean.limparRadiosPrincipaisIncluir}" styleClass="bto1" style="cursor:hand;margin-right:5px" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnCalcular" styleClass="bto1" style="margin-right:5px;" value="#{msgs.btn_calcular}" action="#{manterSolicGeracaoArqRetBasePagtosBean.calcularTarifaAtualizada}" disabled="#{manterSolicGeracaoArqRetBasePagtosBean.indicadorDescontoBloqueio}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.btn_avancar}" action="#{manterSolicGeracaoArqRetBasePagtosBean.avancarIncluir}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" 
			onclick="javascript:desbloquearTela(); return validaCamposIncluir(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_data_de_pagamento}',
			'#{msgs.label_situacao_pagto}','#{msgs.label_destino}', '#{msgs.label_tipo_de_servico}', '#{msgs.label_modalidade}', '#{msgs.label_conta_de_debito}',
			'#{msgs.label_banco}', '#{msgs.label_agencia}', '#{msgs.label_conta}', '#{msgs.label_digito_conta}','#{msgs.label_tipo_conta}','#{msgs.label_selecione_opcao}', '#{msgs.label_favorecido}', '#{msgs.label_codigo}', '#{msgs.label_inscricao}', 
			'#{msgs.label_tipo}', '#{msgs.label_mnemonico}');" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>  
    </a4j:outputPanel>	 
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   	
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>