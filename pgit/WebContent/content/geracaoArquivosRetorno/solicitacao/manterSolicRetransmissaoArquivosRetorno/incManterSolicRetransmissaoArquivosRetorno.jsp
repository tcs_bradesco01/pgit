<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="incManterSolicRetransmissaoArquivosRetornoForm" name="incManterSolicRetransmissaoArquivosRetornoForm" >
<a4j:jsFunction name="funcPaginacao" action="#{manterSolicRetransmissaoArquivosRetornoBean.pesquisar}" reRender="dataTable" onbeforedomupdate="true"/>

<br:brPanelGrid columns="1" style="margin-top:9" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
 	   
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup> 
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicRetransmissaoArquivosRetornoBean.nrCnpjCpf}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.dsRazaoSocial}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
  
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.dsEmpresa}"/>			
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_numero_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.nroContrato}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
   	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_descricao_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.dsContrato}"/>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.cdSituacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">	

  		<br:brPanelGrid>
        	<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
   		</br:brPanelGrid>

   		<br:brPanelGrid columns="2" style="margin-top:9px">
		    <br:brPanelGroup>		
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_data_geracao}: "/>
				</br:brPanelGroup> 
		
	          	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    		<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<a4j:outputPanel id="calendarios" style="text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
							 <br:brPanelGroup>
									<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco"  value="#{msgs.label_de}:"/>
							 </br:brPanelGroup>	
							<br:brPanelGroup rendered="#{!manterSolicRetransmissaoArquivosRetornoBean.disableArgumentosIncluir}">
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoInicio');" id="txtPeriodoInicio" value="#{manterSolicRetransmissaoArquivosRetornoBean.dataSolicitacaoDeRetorno}" >
								</app:calendar>
							</br:brPanelGroup>
							<br:brPanelGroup rendered="#{manterSolicRetransmissaoArquivosRetornoBean.disableArgumentosIncluir}">
								<app:calendar id="txtPeriodoInicio2" value="#{manterSolicRetransmissaoArquivosRetornoBean.dataSolicitacaoDeRetorno}" disabled="true" >
								</app:calendar>
							</br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_a}:"/>
							<br:brPanelGroup rendered="#{!manterSolicRetransmissaoArquivosRetornoBean.disableArgumentosIncluir}">
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoFim');" id="txtPeriodoFim" value="#{manterSolicRetransmissaoArquivosRetornoBean.dataSolicitacaoAteRetorno}" >
				 				</app:calendar>	
							</br:brPanelGroup>	
							<br:brPanelGroup rendered="#{manterSolicRetransmissaoArquivosRetornoBean.disableArgumentosIncluir}">
								<app:calendar id="txtPeriodoFim2" value="#{manterSolicRetransmissaoArquivosRetornoBean.dataSolicitacaoAteRetorno}" disabled="true">
				 				</app:calendar>	
							</br:brPanelGroup>	
						</br:brPanelGroup>			    
					</br:brPanelGrid>
				</a4j:outputPanel>			
			</br:brPanelGroup>
		
			<br:brPanelGroup>	 	
				<br:brPanelGrid cellpadding="0" cellspacing="0">
					<br:brPanelGroup style="margin-left:20px">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText  styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_sequencia_retorno}: "/>
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				 	<br:brPanelGroup style="margin-left:20px">
		     		 	<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{manterSolicRetransmissaoArquivosRetornoBean.disableArgumentosIncluir}" value="#{manterSolicRetransmissaoArquivosRetornoBean.sequenciaRetorno}" size="20" maxlength="9" id="txtSequenciaRetorno" />
					</br:brPanelGroup>
				</br:brPanelGrid>	
			</br:brPanelGroup>			
		</br:brPanelGrid>	
	 
		<br:brPanelGrid columns="2" style="margin-top:6px">		
			<br:brPanelGroup>
				<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_layout_arquivo}: " />
				</br:brPanelGroup>	
			 
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
							<br:brSelectOneMenu id="cboLayoutArquivo"  value="#{manterSolicRetransmissaoArquivosRetornoBean.cboLayoutArquivo}" disabled="#{manterSolicRetransmissaoArquivosRetornoBean.disableArgumentosIncluir}" style="width: 160" styleClass="HtmlSelectOneMenuBradesco">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{manterSolicRetransmissaoArquivosRetornoBean.listarTipoLayout}"/>							
							 </br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>	
			</br:brPanelGroup>			
	 	</br:brPanelGrid>
		 
		<f:verbatim><hr class="lin"></f:verbatim>

	 	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" style="margin-right:5px;" action="#{manterSolicRetransmissaoArquivosRetornoBean.limparTelaIncluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}" 
				disabled="#{manterSolicRetransmissaoArquivosRetornoBean.disableArgumentosIncluir}" action="#{manterSolicRetransmissaoArquivosRetornoBean.consultarIncluir}" styleClass="bto1" onclick="javascript:desbloquearTela(); return validaCamposIncluir(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_data_geracao}');">		
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim><br/></f:verbatim> 

	</a4j:outputPanel>	

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 

			<app:scrollableDataTable id="dataTable" value="#{manterSolicRetransmissaoArquivosRetornoBean.listaGridConsultarIncluir}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
							 
				 <app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  	<t:selectBooleanCheckbox disabled="#{empty manterSolicRetransmissaoArquivosRetornoBean.listaGridConsultarIncluir}"  
					  	 id="checkSelecionarTodos" styleClass="HtmlSelectOneRadioBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.opcaoChecarTodos}" onclick="javascritp:selecionarTodos(document.forms[1],this);">
						</t:selectBooleanCheckbox>
				    </f:facet>	
					<t:selectBooleanCheckbox  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
							value="#{result.check}">
						<f:selectItems value="#{manterSolicRetransmissaoArquivosRetornoBean.listaGridControleIncluir}"/>	
						<a4j:support event="onclick" reRender="panelBotoes" action="#{manterSolicRetransmissaoArquivosRetornoBean.habilitarPanelBotoes}"/>
					</t:selectBooleanCheckbox>
				</app:scrollableColumn>
										  
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_cpf_cnpj_cliente_pagador}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.corpoCpfCnpj}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_nome_razao}" style="width:250; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsPessoa}" />
				</app:scrollableColumn>	  
							  
				<app:scrollableColumn styleClass="colTabRight" width="130px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_cod_transmissao}" style="width:130; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdClienteTransferenciaArquivo}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="170px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_layout_arquivo}" style="width:170; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoLayoutArquivo}" />
				</app:scrollableColumn>
									
				<app:scrollableColumn styleClass="colTabLeft" width="170px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_tipo_retorno}" style="width:170; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsArquivoRetorno}" />
				</app:scrollableColumn>
														
				<app:scrollableColumn styleClass="colTabRight" width="170px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_sequencia_de_retorno}" style="width:170; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrArquivoRetorno}" />
				</app:scrollableColumn>
									
				<app:scrollableColumn styleClass="colTabCenter" width="170px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_data_hora_geracao}" style="width:170; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.hrInclusaoFormatada}" />
				</app:scrollableColumn>
									
				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_situacao}" style="width:250; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoProcessamentoRetorno}" />
				</app:scrollableColumn>
									
				<app:scrollableColumn styleClass="colTabLeft" width="170px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_operacao}" style="width:170; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsAmbiente}" />
				</app:scrollableColumn>
								
				<app:scrollableColumn styleClass="colTabRight" width="170px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_qtde_registros}" style="width:170; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.qtdeRegistroArquivoRetornoFormatada}" />
				</app:scrollableColumn>
								
				<app:scrollableColumn styleClass="colTabRight" width="170px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_valor}" style="width:170; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.vlrRegistroArquivoRetorno}" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>			   
	</br:brPanelGrid>
	     
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
            <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterSolicRetransmissaoArquivosRetornoBean.pesquisarIncluir}">
				<f:facet name="first">
		  			<brArq:pdcCommandButton id="primeira"
				    styleClass="HtmlCommandButtonBradesco"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="funcPaginacao();"/>
				</f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%">
		
	    <f:verbatim><hr class="lin"></f:verbatim>   
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
	

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="0">
				<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}: "/>
						<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.vlTarifaPadrao}" converter="decimalBrazillianConverter"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />			
					<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_desconto_tarifa}: "/>
			    	<br:brInputText size="9" id="txtDescontoTarifa" alt="percentual" value="#{manterSolicRetransmissaoArquivosRetornoBean.vlDescontoTarifa}" converter="decimalBrazillianConverter" style="text-align:right; margin-right:3px" styleClass="HtmlInputTextBradesco" 
			    		onfocus="loadMasks();" onblur="validarCampoDescontoTarifa(this)" disabled="#{manterSolicRetransmissaoArquivosRetornoBean.desabilitaCampoDescontoTarifa}"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="%"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}: "/>
						<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.vlTarifaAtualizada}" converter="decimalBrazillianConverter"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim> 
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{manterSolicRetransmissaoArquivosRetornoBean.voltarConsultar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >
				<br:brCommandButton id="btnLimparIncluir" styleClass="bto1" value="#{msgs.btn_limpar}" style="margin-right:5px;" action="#{manterSolicRetransmissaoArquivosRetornoBean.limparGridIncluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			<br:brCommandButton id="btnCalcular" styleClass="bto1" style="margin-right:5px;" value="#{msgs.btn_calcular}" action="#{manterSolicRetransmissaoArquivosRetornoBean.calcularTarifaAtualizada}" disabled="#{manterSolicRetransmissaoArquivosRetornoBean.indicadorDescontoBloqueio}"
				onfocus="loadMasks();" onblur="validarCampoDescontoTarifa(this)">
				<brArq:submitCheckClient/>
				<a4j:support event="onclick" reRender="btnAvancarIncluir"/>
			</br:brCommandButton>
				<br:brCommandButton id="btnAvancarIncluir" styleClass="bto1"  value="#{msgs.label_avan�ar}"  action="#{manterSolicRetransmissaoArquivosRetornoBean.avancarIncluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>   
		
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   	
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>