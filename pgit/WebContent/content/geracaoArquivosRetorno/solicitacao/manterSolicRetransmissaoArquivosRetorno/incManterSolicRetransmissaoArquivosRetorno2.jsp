<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%> 
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="incManterSolicRetransmissaoArquivosRetorno2Form" name="incManterSolicRetransmissaoArquivosRetorno2Form" >
<br:brPanelGrid columns="1" style="margin-top:9" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
 		
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup> 
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicRetransmissaoArquivosRetornoBean.nrCnpjCpf}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.dsRazaoSocial}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
  
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.dsEmpresa}"/>			
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_numero_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.nroContrato}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
   <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_descricao_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.dsContrato}"/>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.cdSituacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
				
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 
			
				<app:scrollableDataTable id="dataTable" value="#{manterSolicRetransmissaoArquivosRetornoBean.listaIncluirSelecionados}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_cpf_cnpj_cliente_pagador}" style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.corpoCpfCnpj}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_nome_razao}" style="width:250; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsPessoa}" />
					</app:scrollableColumn>	  
							  
				<app:scrollableColumn styleClass="colTabRight" width="130px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_cod_transmissao}" style="width:130; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdClienteTransferenciaArquivo}"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="170px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_layout_arquivo}" style="width:170; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsTipoLayoutArquivo}" />
					</app:scrollableColumn>
										
					<app:scrollableColumn styleClass="colTabLeft" width="170px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_tipo_retorno}" style="width:170; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsArquivoRetorno}" />
					</app:scrollableColumn>
															
					<app:scrollableColumn styleClass="colTabRight" width="170px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_sequencial_retorno}" style="width:170; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nrArquivoRetorno}" />
					</app:scrollableColumn>
										
					<app:scrollableColumn styleClass="colTabCenter" width="170px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_data_hora_geracao}" style="width:170; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.hrInclusaoFormatada}" />
					</app:scrollableColumn>
										
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_situacao}" style="width:250; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsSituacaoProcessamentoRetorno}" />
					</app:scrollableColumn>
										
					<app:scrollableColumn styleClass="colTabLeft" width="170px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_operacao}" style="width:170; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsAmbiente}" />
					</app:scrollableColumn>
									
					<app:scrollableColumn styleClass="colTabRight" width="170px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_qtde_registros}" style="width:170; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.qtdeRegistroArquivoRetornoFormatada}" />
					</app:scrollableColumn>
									
					<app:scrollableColumn styleClass="colTabRight" width="170px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_valor}" style="width:170; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.vlrRegistroArquivoRetorno}" converter="decimalBrazillianConverter"/>
					</app:scrollableColumn>
					
				</app:scrollableDataTable>					
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		
		<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%">
			
			<f:verbatim><hr class="lin"> </f:verbatim>
			
			<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}:"/>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="R$" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.vlTarifaPadrao}" converter="decimalBrazillianConverter"/>
				</br:brPanelGroup> 	
				<br:brPanelGroup> 
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_desconto_tarifa}:"/> 
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.vlDescontoTarifa}" converter="decimalBrazillianConverter"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="%" />
				</br:brPanelGroup>	
				<br:brPanelGroup> 
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}:"/>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="R$" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicRetransmissaoArquivosRetornoBean.vlTarifaAtualizada}" converter="decimalBrazillianConverter"/>
				</br:brPanelGroup>	
			</br:brPanelGrid>     
		</br:brPanelGrid>     
			
			<f:verbatim><hr class="lin"> </f:verbatim>
								
			<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
				<br:brPanelGroup style="text-align:left;width:150px" >
					<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{manterSolicRetransmissaoArquivosRetornoBean.voltarIncluir}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
				<br:brPanelGroup style="text-align:right;width:600px" >
					<br:brCommandButton id="btnIncluir2" styleClass="bto1" value="#{msgs.btn_confirmar}" action="#{manterSolicRetransmissaoArquivosRetornoBean.confirmarIncluir}" onclick="javascript: if (!confirm('#{msgs.label_conf_inclusao}')) { desbloquearTela(); return false; }">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>

		<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   	
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>