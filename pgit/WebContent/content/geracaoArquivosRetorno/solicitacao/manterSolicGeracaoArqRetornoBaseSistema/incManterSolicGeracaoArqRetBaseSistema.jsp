<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="incManterSolicGeracaoArqRetBaseSistemaForm" name="incManterSolicGeracaoArqRetBaseSistemaForm" >
<br:brPanelGrid columns="1" style="margin-top:9" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup> 
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicGeracaoArqRetBaseSistemaBean.nrCnpjCpf}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.dsRazaoSocial}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
  
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.dsEmpresa}"/>			
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_numero_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.nroContrato}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
   <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_descricao_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.dsContrato}"/>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.cdSituacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">	

   <br:brPanelGrid>
        <br:brPanelGroup>			
			  <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_solicitacao}:"/>
		</br:brPanelGroup>
   </br:brPanelGrid>
   
   <br:brPanelGrid columns="3" style="margin-top:6px">
   		<br:brPanelGroup>
   			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_base_sistema}: " />   		
   		</br:brPanelGroup>
   </br:brPanelGrid>
   
   <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brSelectOneMenu id="cboTipoSistema"  value="#{manterSolicGeracaoArqRetBaseSistemaBean.cboTipoSistema}" styleClass="HtmlSelectOneMenuBradesco">
				<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
				<f:selectItems value="#{manterSolicGeracaoArqRetBaseSistemaBean.listarTipoSistema}"/>
			 </br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%">
   	 
		<f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
					
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="0">
				<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}: "/>
						<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.vlTarifaPadrao}" converter="decimalBrazillianConverter"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />			
					<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_desconto_tarifa}: "/>
			    	<br:brInputText size="9" id="txtDescontoTarifa" alt="percentual" value="#{manterSolicGeracaoArqRetBaseSistemaBean.vlDescontoTarifa}" converter="decimalBrazillianConverter" style="text-align:right; margin-right:3px" styleClass="HtmlInputTextBradesco" 
			    		onfocus="loadMasks();" onblur="validarCampoDescontoTarifa(this)" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.desabilitaCampoDescontoTarifa}"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="%"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}: "/>
						<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.vlTarifaAtualizada}" converter="decimalBrazillianConverter"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
	</br:brPanelGrid>
	</a4j:outputPanel>
	
		
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	limparRadiosPrincipaisIncluir
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{manterSolicGeracaoArqRetBaseSistemaBean.voltarConsulta}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_limpar}" action="#{manterSolicGeracaoArqRetBaseSistemaBean.limparRadiosPrincipaisIncluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnCalcular" styleClass="bto1" style="margin-right:5px;" value="#{msgs.btn_calcular}" action="#{manterSolicGeracaoArqRetBaseSistemaBean.calcularTarifaAtualizada}" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.indicadorDescontoBloqueio}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAvancarIncluir" styleClass="bto1" value="#{msgs.label_avan�ar}"   action="#{manterSolicGeracaoArqRetBaseSistemaBean.avancarIncluir}" onclick="javascript:desbloquearTela(); return validaCampoInclusao(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_base_sistema}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>   
    </a4j:outputPanel>	 


<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>