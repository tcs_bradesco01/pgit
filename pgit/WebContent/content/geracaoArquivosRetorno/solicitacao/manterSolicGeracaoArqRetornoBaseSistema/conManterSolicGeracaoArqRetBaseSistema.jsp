<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="conManterSolicGeracaoArqRetBaseSistemaForm" name="conManterSolicGeracaoArqRetBaseSistemaForm" >
<br:brPanelGrid columns="1" style="margin-top:9" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
 	   
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio  id="radioTipoFiltro" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}">  
				<f:selectItem itemValue="0" itemLabel="#{msgs.label_filtrar_por_cliente}" />
				<f:selectItem itemValue="1" itemLabel="#{msgs.label_filtrar_por_contrato}" />
				<a4j:support oncomplete="javascript:foco(this);" event="onclick" status="statusAguarde" reRender="formulario,dataTable,panelBotoes,dataScroller" action="#{manterSolicGeracaoArqRetBaseSistemaBean.limparRadiosPrincipais}"/>			
			</t:selectOneRadio>
		</br:brPanelGrid>
	
		<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">	
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<t:radio for="radioTipoFiltro" index="0" />
			</br:brPanelGroup>		
		</br:brPanelGrid>		
	 
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_identificacao_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	

		<a4j:region id="regionFiltro">
		<t:selectOneRadio id="rdoFiltroCliente"    value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" onclick="submit();" layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '0' || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="" />
		</t:selectOneRadio>
		</a4j:region>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="rdoFiltroCliente" index="0" />
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				<br:brInputText id="txtCnpj"  onkeyup="proximoCampo(9,'conManterSolicGeracaoArqRetBaseSistemaForm','conManterSolicGeracaoArqRetBaseSistemaForm:txtCnpj','conManterSolicGeracaoArqRetBaseSistemaForm:txtFilial');" 
				    style="margin-right: 5"converter="javax.faces.Long" 
				    onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" size="11" maxlength="9" 
				    disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" 
				    styleClass="HtmlInputTextBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"
			     />
			    
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'conManterSolicGeracaoArqRetBaseSistemaForm','conManterSolicGeracaoArqRetBaseSistemaForm:txtFilial','conManterSolicGeracaoArqRetBaseSistemaForm:txtControle');"
				     style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" size="5" maxlength="4" 
				     disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" 
				     styleClass="HtmlInputTextBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" 
			     />
			     
				<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" 
					onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" size="3" maxlength="2" 
					disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" 
					styleClass="HtmlInputTextBradesco" 
					value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" 
				/>
			</br:brPanelGrid>
		</a4j:outputPanel>	 
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		<a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'conManterSolicGeracaoArqRetBaseSistemaForm','conManterSolicGeracaoArqRetBaseSistemaForm:txtCpf','conManterSolicGeracaoArqRetBaseSistemaForm:txtControleCpf');" />
			    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nome_razao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '2'|| manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3'|| manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'conManterSolicGeracaoArqRetBaseSistemaForm','conManterSolicGeracaoArqRetBaseSistemaForm:txtBanco','conManterSolicGeracaoArqRetBaseSistemaForm:txtAgencia');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3'|| manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'conManterSolicGeracaoArqRetBaseSistemaForm','conManterSolicGeracaoArqRetBaseSistemaForm:txtAgencia','conManterSolicGeracaoArqRetBaseSistemaForm:txtConta');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3'|| manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}"
						 value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" style="margin-right:5px"/>
					</br:brPanelGroup>					
			</br:brPanelGrid>				
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado|| manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" value="#{msgs.btn_consultar_cliente}" action="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
	   		 onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_banco}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');"
		   	    actionListener="#{manterSolicGeracaoArqRetBaseSistemaBean.limparArgsListaAposPesquisaClienteContratoConsultar}" >		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel> 
	</br:brPanelGrid>	 
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup> 
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
 
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top: 9" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" rendered="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 0}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescCliente}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	 
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" rendered="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 0}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.numeroDescCliente}"/>
		</br:brPanelGroup>	    
		<br:brPanelGroup>
			<br:brGraphicImage style="margin-left:20px" url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" rendered="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 0}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescCliente}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage style="margin-left:20px" url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" rendered="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 0}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.situacaoDescCliente}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>     
    
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
		<br:brPanelGroup>			
			<t:radio for="radioTipoFiltro" index="1" />
		</br:brPanelGroup>		
	</br:brPanelGrid>	     
    
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_identificacao_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	    
	 
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		        <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="empresaGestora" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px" >
						<f:selectItems value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.listaEmpresaGestora}" />								
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
	 
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoContrato" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
						<f:selectItems value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.listaTipoContrato}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
			
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_numero_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.numeroFiltro}" size="12" maxlength="10" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}">
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>	

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;">
		 <br:brCommandButton id="btoConsultarContrato" 
		   		disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" styleClass="bto1" value="#{msgs.btn_consultar_contrato}" 
		   		action="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.consultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" 
		   		onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],
		   		'#{msgs.label_ocampo}',
		   	    '#{msgs.label_necessario}',
		   	    '#{msgs.label_empresa_gestora_contrato}',
		   	    '#{msgs.label_tipo_contrato}',
		   	    '#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');"
		   	    actionListener="#{manterSolicGeracaoArqRetBaseSistemaBean.limparArgsListaAposPesquisaClienteContratoConsultar}" >		
			<brArq:submitCheckClient />
		</br:brCommandButton>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	  
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	 
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}"/>
		</br:brPanelGroup>	    
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	

    <f:verbatim><hr class="lin"></f:verbatim>

  <br:brPanelGrid>
        <br:brPanelGroup>			
			  <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
   </br:brPanelGrid>

   <br:brPanelGrid columns="2" style="margin-top:9px">
	      
	    <br:brPanelGroup>		
	                <br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_data_solicitacao}: "/>
					</br:brPanelGroup> 
		
	            	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    		<br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
			
			
				<a4j:outputPanel id="calendarios" style="text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
							 <br:brPanelGroup>
									<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco"  value="#{msgs.label_de}:"/>
							 </br:brPanelGroup>	
							<br:brPanelGroup rendered="#{(manterSolicGeracaoArqRetBaseSistemaBean.habilitaArgumentosPesquisa || manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) && !manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}">
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoInicio');" id="txtPeriodoInicio" value="#{manterSolicGeracaoArqRetBaseSistemaBean.dataSolicitacaoDe}" >
								</app:calendar>
							</br:brPanelGroup>
							<br:brPanelGroup rendered="#{(!manterSolicGeracaoArqRetBaseSistemaBean.habilitaArgumentosPesquisa && !manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}">
								<app:calendar id="txtPeriodoInicio2" value="#{manterSolicGeracaoArqRetBaseSistemaBean.dataSolicitacaoDe}" disabled="true" >
								</app:calendar>
							</br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_a}:"/>
							<br:brPanelGroup rendered="#{(manterSolicGeracaoArqRetBaseSistemaBean.habilitaArgumentosPesquisa || manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) && !manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}">
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoFim');" id="txtPeriodoFim" value="#{manterSolicGeracaoArqRetBaseSistemaBean.dataSolicitacaoAte}" >
				 				</app:calendar>	
							</br:brPanelGroup>	
							<br:brPanelGroup rendered="#{(!manterSolicGeracaoArqRetBaseSistemaBean.habilitaArgumentosPesquisa && !manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}">
								<app:calendar id="txtPeriodoFim2" value="#{manterSolicGeracaoArqRetBaseSistemaBean.dataSolicitacaoAte}" disabled="true">
				 				</app:calendar>	
							</br:brPanelGroup>	
						</br:brPanelGroup>			    
					</br:brPanelGrid>
				</a4j:outputPanel>	
			
			
		</br:brPanelGroup>
		
		
		
			
		<br:brPanelGroup>	 	
					<br:brPanelGrid cellpadding="0" cellspacing="0">
						<br:brPanelGroup style="margin-left:20px">
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText  styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_numero_solicitacao}: "/>
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					 	<br:brPanelGroup style="margin-left:20px">
			     		 	<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{(!manterSolicGeracaoArqRetBaseSistemaBean.habilitaArgumentosPesquisa && !manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" value="#{manterSolicGeracaoArqRetBaseSistemaBean.numeroSolicitacao}" size="10" maxlength="5" id="txtNumeroSolicitacao" />
						</br:brPanelGroup>
					</br:brPanelGrid>	
		</br:brPanelGroup>			
	 </br:brPanelGrid>	
	 
	<br:brPanelGrid columns="2" style="margin-top:6px">		
		<br:brPanelGroup>
				<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_situacao}: " />
				</br:brPanelGroup>	
			 
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
							<br:brSelectOneMenu id="cboSituacaoSolicitacao"  value="#{manterSolicGeracaoArqRetBaseSistemaBean.cboSituacaoSolicitacao}" disabled="#{(!manterSolicGeracaoArqRetBaseSistemaBean.habilitaArgumentosPesquisa && !manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" style="width: 160" styleClass="HtmlSelectOneMenuBradesco">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{manterSolicGeracaoArqRetBaseSistemaBean.listaSituacaoSolicitacao}"/>
							 </br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
				<br:brPanelGroup style="margin-left:20px">
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_origem_solicitacao}: " />
				</br:brPanelGroup>	
			 
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup style="margin-left:20px">
							<br:brSelectOneMenu id="cboOrigemSolicitacao"  value="#{manterSolicGeracaoArqRetBaseSistemaBean.cboOrigemSolicitacao}" disabled="#{(!manterSolicGeracaoArqRetBaseSistemaBean.habilitaArgumentosPesquisa && !manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" style="width: 160" styleClass="HtmlSelectOneMenuBradesco">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItem itemValue="1" itemLabel="#{msgs.label_cliente}"/>
								<f:selectItem itemValue="2" itemLabel="#{msgs.label_banco}"/>								
							 </br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>	
		</br:brPanelGroup>			
 </br:brPanelGrid>
     
		 
<f:verbatim><hr class="lin"></f:verbatim>

 <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" style="margin-right:5px" disabled="false" onclick="javascript:desbloquearTela();" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{manterSolicGeracaoArqRetBaseSistemaBean.limpar}">
				<brArq:submitCheckClient/> 
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}" 
			disabled="#{(!manterSolicGeracaoArqRetBaseSistemaBean.habilitaArgumentosPesquisa && !manterSolicGeracaoArqRetBaseSistemaBean.filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" action="#{manterSolicGeracaoArqRetBaseSistemaBean.consultar}" styleClass="bto1" onclick="javascript:desbloquearTela(); return validaCamposConsultar(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_data_solicitacao}');">		
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><br/></f:verbatim> 

	</a4j:outputPanel>	

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 

		  <app:scrollableDataTable id="dataTable" value="#{manterSolicGeracaoArqRetBaseSistemaBean.listaGridConsultar}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				
			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
							<br:brOutputText value="" styleClass="tableFontStyle" style="width:30; text-align:center" />
						</f:facet>	
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false"
						 value="#{manterSolicGeracaoArqRetBaseSistemaBean.itemSelecionadoListaConsultar}">
							<f:selectItems value="#{manterSolicGeracaoArqRetBaseSistemaBean.radioGridConsultar}"/>
						<a4j:support event="onclick"  reRender="btnLimparFinal,btnDetalhar,btnExcluir"  />
						</t:selectOneRadio>
						<t:radio for="sorLista" index="#{parametroKey}" />
			 </app:scrollableColumn>
									  
			<app:scrollableColumn styleClass="colTabRight" width="150px" >
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_numero_solicitacao}" style="text-align:center;width:150"/>
				</f:facet>
					<br:brOutputText value="#{result.nrSolicitacaoPagamento}"/>
			</app:scrollableColumn>	  
							  
			<app:scrollableColumn width="220" styleClass="colTabCenter">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.grid_data_hora_solicitacao}" styleClass="tableFontStyle"  style="width:220; text-align:center"  />
				</f:facet>
				<br:brOutputText value="#{result.hrSolicitacaoFormatada}" />
			</app:scrollableColumn>
						  
			<app:scrollableColumn width="150" styleClass="colTabLeft">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_situacao}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.dsSituacaoSolicitacao}" />
			</app:scrollableColumn>
			
			<app:scrollableColumn width="200" styleClass="colTabLeft">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_resultado_processamento}" styleClass="tableFontStyle"  style="width:200; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.resultadoProcessamento}" />
			</app:scrollableColumn>
			
			<app:scrollableColumn width="150" styleClass="colTabCenter">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_data_hora_atendimento}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.hrAtendimentoSolicitacao}" />
			</app:scrollableColumn>	
						  
			<app:scrollableColumn width="150" styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_qtde_registros}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.qtdeRegistroSolicitacao}" />
			</app:scrollableColumn>	
		
			
		</app:scrollableDataTable>
	  </br:brPanelGroup>			   
	 </br:brPanelGrid>
	     
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
	<br:brPanelGroup>
	            <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterSolicGeracaoArqRetBaseSistemaBean.pesquisar}">
						<f:facet name="first">
				  <brArq:pdcCommandButton id="primeira"
				    styleClass="HtmlCommandButtonBradesco"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
		</brArq:pdcDataScroller>
	</br:brPanelGroup>	
  </br:brPanelGrid>		
	
    <f:verbatim><hr class="lin"></f:verbatim> 

	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:right;width:750px" >		
			<br:brCommandButton id="btnLimparFinal" style="margin-right:5px" onclick="javascript:desbloquearTela();" styleClass="bto1" value="#{msgs.btn_limpar}" action="#{manterSolicGeracaoArqRetBaseSistemaBean.limparGridConsultar}" disabled="#{empty manterSolicGeracaoArqRetBaseSistemaBean.listaGridConsultar}">
				<brArq:submitCheckClient/> 
			</br:brCommandButton>
     		<br:brCommandButton id="btnDetalhar" style="margin-right:5px" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.itemSelecionadoListaConsultar == null}" onclick="javascript:desbloquearTela();" styleClass="bto1" value="#{msgs.btn_detalhar}" action="#{manterSolicGeracaoArqRetBaseSistemaBean.detalhar}">
				<brArq:submitCheckClient/> 
			</br:brCommandButton>							
			<br:brCommandButton id="btnIncluir" style="margin-right:5px" disabled="#{!manterSolicGeracaoArqRetBaseSistemaBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado}" onclick="javascript:desbloquearTela();" styleClass="bto1" value="#{msgs.btn_incluir}" action="#{manterSolicGeracaoArqRetBaseSistemaBean.incluir}">
				<brArq:submitCheckClient/> 
			</br:brCommandButton>	
			<br:brCommandButton id="btnExcluir" onclick="javascript:desbloquearTela();" disabled="#{manterSolicGeracaoArqRetBaseSistemaBean.itemSelecionadoListaConsultar == null}" styleClass="bto1" value="#{msgs.btn_excluir}" action="#{manterSolicGeracaoArqRetBaseSistemaBean.excluir}">
				<brArq:submitCheckClient/> 
			</br:brCommandButton>					
		</br:brPanelGroup>
	</br:brPanelGrid> 
    </a4j:outputPanel>	 	  

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   	
	
	<t:inputHidden value="#{manterSolicGeracaoArqRetBaseSistemaBean.disableArgumentosConsulta}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>