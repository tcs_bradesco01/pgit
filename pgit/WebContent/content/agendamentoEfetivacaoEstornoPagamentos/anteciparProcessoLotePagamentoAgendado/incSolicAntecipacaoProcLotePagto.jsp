<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incSolicAntecipacaoProcLotePagtoForm"
	name="incSolicAntecipacaoProcLotePagtoForm">
	<h:inputHidden id="hiddenFoco"
		value="#{manterSolicAntecipacaoProcLotePagamentoBean.pagamentosBean.foco}" />

	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"
		cellspacing="0">

		<jsp:include
			page="/content/agendamentoEfetivacaoEstornoPagamentos/filtroIdentificacaoClienteContrato.jsp"
			flush="false" />

		<a4j:outputPanel id="formulario" style="width: 100%; text-align: left"
			ajaxRendered="true">
			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.conPagamentosConsolidado_argumentos_pesquisa}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<a4j:outputPanel id="painelArgumentos"
				style="width: 100%; text-align: left" ajaxRendered="true">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText style="margin-left:3px"
							styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brInputText id="txtLoteInterno"
							value="#{manterSolicAntecipacaoProcLotePagamentoBean.pagamentosBean.loteInterno}"
							size="18" maxlength="9"
							onkeypress="onlyNum();"
							disabled="#{(!manterSolicAntecipacaoProcLotePagamentoBean.pagamentosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || 
					 manterSolicAntecipacaoProcLotePagamentoBean.pagamentosBean.disableArgumentosConsulta || manterSolicAntecipacaoProcLotePagamentoBean.gerenciaTxtLote}">
					 <a4j:support event="onblur" reRender="btnConsultar"  oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					 </br:brInputText>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</a4j:outputPanel>
		</a4j:outputPanel>
		
		<f:verbatim><hr class="lin"></f:verbatim>

		<br:brPanelGrid width="100%" cellpadding="3" cellspacing="3">
			<br:brPanelGroup style="float:right;">
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1"
					style="margin-right: 5px;"
					action="#{manterSolicAntecipacaoProcLotePagamentoBean.limparCamposTelaInclusao}"
					value="#{msgs.label_limpar_campos}"
					disabled="#{pagamentosBean.desabilitaBtnLimparCampos}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.label_consultar}"
					 action="#{manterSolicAntecipacaoProcLotePagamentoBean.consultaLotesInclusaoSolicitacao}"
					 onclick="desbloquearTela();return validaCampoLote(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
						 '#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}', '#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_tipo_layout_arquivo}', '#{msgs.manterSolicitacaoExclusaoLotePagamentos_situacao_solicitacao}',
						 '#{msgs.label_deve_ser_diferente_de_zeros}');" 
			          disabled="#{(!manterSolicAntecipacaoProcLotePagamentoBean.pagamentosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || 
			   		  manterSolicAntecipacaoProcLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}">
				 <brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll" >

				<app:scrollableDataTable id="dataTable" 	value="#{manterSolicAntecipacaoProcLotePagamentoBean.listaLote}" var="result" 
					rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterSolicAntecipacaoProcLotePagamentoBean.itemSelecao}">
							<f:selectItems 	value="#{manterSolicAntecipacaoProcLotePagamentoBean.listaItensSelecao}"/>
							<a4j:support event="onclick" reRender="btnAvancar"/>
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
					<app:scrollableColumn styleClass="colTabRight" width="150px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_lote_interno}"
								style="width:250; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.nrLoteInterno}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="150px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_layout_arquivo}"
								style="width:250; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsTipoLayoutArquivo}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="150px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_tipo_de_servico}"
								style="width:250; text-align:center" />							
						</f:facet>
						<br:brOutputText value="#{result.dsProdutoServicoOperacao}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabRight" width="150px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_quantidade_pagamentos_lote}"
								style="width:250; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.qtTotalPagamentoLote}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabRight" width="150px">
						<f:facet name="header">
							<br:brOutputText
								value="#{msgs.label_valor_total_pagamentos_lote}"
								style="width:250; text-align:center"
								/>
						</f:facet>
						<br:brOutputText value="#{result.vlTotalPagamentoLote}" 
						converter="decimalBrazillianConverter"/>
					</app:scrollableColumn>


				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterSolicAntecipacaoProcLotePagamentoBean.paginarListaLoteIncluir}" >
					 <f:facet name="first">
					    <brArq:pdcCommandButton id="primeira"
					      styleClass="bto1"
					      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					  </f:facet>
					  <f:facet name="fastrewind">
					    <brArq:pdcCommandButton id="retrocessoRapido"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					  </f:facet>
					  <f:facet name="previous">
					    <brArq:pdcCommandButton id="anterior"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					  </f:facet>
					  <f:facet name="next">
					    <brArq:pdcCommandButton id="proxima"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					  </f:facet>
					  <f:facet name="fastforward">
					    <brArq:pdcCommandButton id="avancoRapido"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					  </f:facet>
					  <f:facet name="last">
					    <brArq:pdcCommandButton id="ultima"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					  </f:facet>
					</brArq:pdcDataScroller>
				</br:brPanelGroup>
	      </br:brPanelGrid>
		</br:brPanelGrid>



		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="2" width="100%" style="text-align:right"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="text-align:left;width:100%">
				<br:brCommandButton id="btnVoltar" styleClass="bto1"
					style="align:left" value="#{msgs.botao_voltar}"
					action="#{manterSolicAntecipacaoProcLotePagamentoBean.voltarInclusaoConsulta}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brCommandButton id="btnLimpar" styleClass="bto1"
					value="#{msgs.label_limpar}"
					action="#{manterSolicAntecipacaoProcLotePagamentoBean.limpaLista}"
					style="margin-right:5px"
					disabled="#{empty manterSolicAntecipacaoProcLotePagamentoBean.listaLote}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnAvancar" styleClass="bto1"
					value="#{msgs.botao_avancar}"
					action="#{manterSolicAntecipacaoProcLotePagamentoBean.avancarIncluir}"
					disabled="#{empty manterSolicAntecipacaoProcLotePagamentoBean.itemSelecao}"
					onclick="desbloquearTela();return validaCamposInclusaoLotePagamento(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
					 '#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}', '#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_tipo_layout_arquivo}', '#{msgs.conPagamentosConsolidado_data_pagamento}',
					 '#{msgs.label_tipo_de_servico}', '#{msgs.label_modalidade}', '#{msgs.label_banco}', '#{msgs.label_agencia}', '#{msgs.label_conta}','#{msgs.label_deve_ser_diferente_de_zeros}');">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<a4j:status id="statusAguarde" onstart="bloquearTela()"
			onstop="desbloquearTela()" />

	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>