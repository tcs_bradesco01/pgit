<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="identificacaoClienteAgendamento">

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	

	<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 
	<app:scrollableDataTable id="tableClientes" value="#{filtroAgendamentoEfetivacaoEstornoBean2.listaConsultarListaClientePessoas}" width="100%" rowIndexVar="parametroKey" var="result" rows="10" >
			
		<app:scrollableColumn width="20" styleClass="colTabCenter">
			<f:facet name="header">
				<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			</f:facet>
			<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{filtroAgendamentoEfetivacaoEstornoBean2.itemClienteSelecionado}">
				<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean2.listaControleRadioClientes}"/>
				<a4j:support event="onclick" reRender="panelBotoes"/>
				<t:radio for="sorLista" index="#{parametroKey}" />
			</t:selectOneRadio>				
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="110" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.identificacaoClienteAgendamento_grid_club}" styleClass="tableFontStyle" style="width:110; text-align:center"/>
			</f:facet>
			<br:brOutputText value="#{result.cdClub}" />
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="210" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.identificacaoClienteAgendamento_grid_cpf_cnpj}" styleClass="tableFontStyle" style="width:210; text-align:center"/>
			</f:facet>
			<br:brOutputText value="#{result.cnpjOuCpfFormatado}" />
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="210" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.identificacaoClienteAgendamento_grid_nome_razao}" styleClass="tableFontStyle" style="width:210; text-align:center"/>
			</f:facet>
			<br:brOutputText value="#{result.dsNomeRazao}" />
		</app:scrollableColumn>
		
		<app:scrollableColumn width="230" styleClass="colTabCenter">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.identificacaoClienteAgendamento_grid_data_nascimento_fundacao}" styleClass="tableFontStyle" style="width:230; text-align:center"/>
			</f:facet>
			<br:brOutputText value="#{result.dtNascimentoFundacaoFormatado}" />
		</app:scrollableColumn>		
			  
	</app:scrollableDataTable>
	</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>

		<brArq:pdcDataScroller  id="dataScroller" for="tableClientes" actionListener="#{filtroAgendamentoEfetivacaoEstornoBean2.submitCliente}">
				<f:facet name="first">
				  <brArq:pdcCommandButton id="primeira"
				    styleClass="bto1"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="bto1" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="bto1" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="bto1" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="bto1" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="bto1" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
		</brArq:pdcDataScroller>
	</br:brPanelGroup>
	</br:brPanelGrid>	
    
    <f:verbatim><hr class="lin"></f:verbatim>
				
    
   	<a4j:outputPanel id="panelBotoes" style="width: 100%; " ajaxRendered="true">	
   		
   	   <br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
    	<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.identificacaoClienteAgendamento_btn_voltar}" action="#{filtroAgendamentoEfetivacaoEstornoBean2.voltarCliente}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
    
    	<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnSelecionar" disabled="#{empty filtroAgendamentoEfetivacaoEstornoBean2.itemClienteSelecionado}" action="#{filtroAgendamentoEfetivacaoEstornoBean2.efetuarPesquisarContrato}" styleClass="bto1" value="#{msgs.identificacaoClienteAgendamento_grid_selecionar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
       </br:brPanelGrid>	
       
	</a4j:outputPanel>   

</br:brPanelGrid>
</brArq:form>