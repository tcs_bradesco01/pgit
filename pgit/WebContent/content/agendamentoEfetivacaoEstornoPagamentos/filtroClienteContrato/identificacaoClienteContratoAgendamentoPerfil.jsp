<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="identificacaoClienteContratoAgendamentoPerfil">

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top: 9" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.cpf}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.nomeRazao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>	
	
<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
	<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 
	<app:scrollableDataTable id="tableClientes" value="#{filtroAgendamentoEfetivacaoEstornoBean.listaContratoClientePerfil}" width="100%" rowIndexVar="parametroKey" var="result" rows="10" >
			
		<app:scrollableColumn width="20" styleClass="colTabCenter">
			<f:facet name="header">
				<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			</f:facet>
			<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false"
				value="#{filtroAgendamentoEfetivacaoEstornoBean.itemSelecionadoListaContratoClientePerfil}">
				<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaControleRadioContratoClientePerfil}"/>
				<a4j:support event="onclick" reRender="panelBotoes"/>
				<t:radio for="sorLista" index="#{parametroKey}" />
			</t:selectOneRadio>				
		</app:scrollableColumn>
					  
		<app:scrollableColumn width="335" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_empresa_gestora_contrato}" styleClass="tableFontStyle" style="width:220; text-align:center"/>
			</f:facet>
			<br:brOutputText value="#{result.cdPessoaJuridica}" />
			<br:brOutputText value="-" />			
			<br:brOutputText value="#{result.dsPessoaJuridica}" />						
		</app:scrollableColumn>
		
		<app:scrollableColumn width="335" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_numero_contrato}" styleClass="tableFontStyle" style="width:220; text-align:center"/>
			</f:facet>
			<br:brOutputText value="#{result.nrSequenciaContrato}" />
		</app:scrollableColumn>		
	</app:scrollableDataTable>
	</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>

		<brArq:pdcDataScroller  id="dataScroller" for="tableClientes" actionListener="#{filtroAgendamentoEfetivacaoEstornoBean.submitPerfilContratoCliente}">
				<f:facet name="first">
				  <brArq:pdcCommandButton id="primeira"
				    styleClass="bto1"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="bto1" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="bto1" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="bto1" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="bto1" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="bto1" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
		</brArq:pdcDataScroller>
	</br:brPanelGroup>
	</br:brPanelGrid>	
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
   	<a4j:outputPanel id="panelBotoes" style="width: 100%; " ajaxRendered="true">	
   		
   	   <br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
    	<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.identificacaoClienteContratoAgendamento_btn_voltar}" action="#{filtroAgendamentoEfetivacaoEstornoBean.voltarContratoCliente}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
    
    	<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnSelecionar" disabled="#{empty filtroAgendamentoEfetivacaoEstornoBean.itemSelecionadoListaContratoClientePerfil}"
				 action="#{filtroAgendamentoEfetivacaoEstornoBean.perfilClienteContrato}" styleClass="bto1" value="#{msgs.identificacaoClienteContratoAgendamento_btn_selecionar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
       </br:brPanelGrid>	
       
	</a4j:outputPanel>   

</br:brPanelGrid>
</brArq:form>