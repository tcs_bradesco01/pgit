<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="identificacaoParticipanteForm" name="identificacaoParticipanteForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">


	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 			 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	CPF/CNPJ – cdCpfCnpjParticipante/ cdFilialCnpjParticipante - cdControleCpfParticipante
Nome/Razão Social - dsParticipante
Tipo de Participação - dsTipoParticipação
Situação da Participação - dsSituacaoParticipação
	

	<f:verbatim><br></f:verbatim> 
	
			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 
	
				<app:scrollableDataTable id="dataTable" value="#{filtroAgendamentoEfetivacaoEstornoBean.listaParticipantes}" var="result" 
					rows="10" rowIndexVar="parametroKey" 
					rowClasses="tabela_celula_normal, tabela_celula_destaque" 
					width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{filtroAgendamentoEfetivacaoEstornoBean.itemSelecionadoListaParcipantes}">
							<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaParticipantesControleRadio}"/>
							<a4j:support event="onclick" reRender="panelBotoes"/>
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
				
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_cpfCnpj}" style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cpfCnpj}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_nomeRazaoSocial}"  style="width:250; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsParticipante}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_tipo_participacao}" style="width:250; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsTipoParticipacao}" />
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_situacao_participacao}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsSituacaoParticipacao}" />
					</app:scrollableColumn>				
					
				</app:scrollableDataTable>
				
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{filtroAgendamentoEfetivacaoEstornoBean.pesquisarParticipantes}" >
				 <f:facet name="first">
				    <brArq:pdcCommandButton id="primeira"
				      styleClass="bto1"
				      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" />
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" />
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" />
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton id="proxima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" />
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" />
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton id="ultima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" />
				  </f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<f:verbatim><hr class="lin"> </f:verbatim>	

 
  	<a4j:outputPanel id="panelBotoes" style="width: 100%; " ajaxRendered="true">	
   		
   	   <br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
    	<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.label_voltar}" action="#{filtroAgendamentoEfetivacaoEstornoBean.voltarParticipante}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
    
    	<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnSelecionar" disabled="#{empty filtroAgendamentoEfetivacaoEstornoBean.itemSelecionadoListaParcipantes}" action="#{filtroAgendamentoEfetivacaoEstornoBean.selecionarParticipante}" styleClass="bto1" value="#{msgs.label_selecionar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
       </br:brPanelGrid>	
       
	</a4j:outputPanel>   
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	