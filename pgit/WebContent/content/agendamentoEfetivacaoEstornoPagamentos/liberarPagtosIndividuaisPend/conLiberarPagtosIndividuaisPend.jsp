<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="conLiberarPagtosIndividuaisPendForm" name="conLiberarPagtosIndividuaisPendForm">
<h:inputHidden id="hiddenFoco" value="#{liberarPagtosIndividuaisPendBean.foco}"/>
	<a4j:jsFunction name="funcPaginacao" action="#{liberarPagtosIndividuaisPendBean.limparSelecionados}" reRender="dataTable, checkSelecionarTodos" onbeforedomupdate="true"/>
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup> 
	</br:brPanelGrid>

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="radioTipoFiltro" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}">  
			<f:selectItem itemValue="0" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_cliente}" />
			<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_contrato}" />
			<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_conta_debito}" />
			<a4j:support oncomplete="javascript:foco(this);" event="onclick" status="statusAguarde" reRender="formulario,dataTable,panelBotoes,dataScroller" action="#{liberarPagtosIndividuaisPendBean.limparTelaPrincipal}"/>			
		</t:selectOneRadio>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">					
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
		<br:brPanelGroup>			
			<t:radio for="radioTipoFiltro" index="0" />
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
 
	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '0' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}">  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
	</t:selectOneRadio>
	</a4j:region>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtCnpj" onkeyup="proximoCampo(9,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:txtCnpj','conLiberarPagtosIndividuaisPendForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"/>
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtFilial" onkeyup="proximoCampo(4,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:txtFilial','conLiberarPagtosIndividuaisPendForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" />
				<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" />
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:txtCpf','conLiberarPagtosIndividuaisPendForm:txtControleCpf');" />
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:txtBanco','conLiberarPagtosIndividuaisPendForm:txtAgencia');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:txtAgencia','conLiberarPagtosIndividuaisPendForm:txtConta');"/>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}"
						 value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" style="margin-right:5px"/>
				</br:brPanelGroup>			
			</br:brPanelGrid>				
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{msgs.consultarPagamentosIndividual_btn_consultar_cliente}" action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
	   		 onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_banco}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');"
	   		  actionListener="#{liberarPagtosIndividuaisPendBean.limparArgsListaAposPesquisaClienteContrato}">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top: 9" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescCliente}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescCliente}"/>
		</br:brPanelGroup>	    
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescCliente}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescCliente}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>     
    
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
		<br:brPanelGroup>			
			<t:radio for="radioTipoFiltro" index="1" />
		</br:brPanelGroup>		
	</br:brPanelGrid>	    
    
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	    
	
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:20px">
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="empresaGestora" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px" >
						<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaEmpresaGestora}" />								
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoContrato" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
						
						<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaTipoContrato}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroFiltro}" size="12" maxlength="10" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}">
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>	
    
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;">
   		<br:brCommandButton id="btoConsultarContrato" actionListener="#{liberarPagtosIndividuaisPendBean.limparArgsListaAposPesquisaClienteContrato}" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" styleClass="bto1" value="#{msgs.consultarPagamentosIndividual_btn_consultar_contrato}" action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.consultarPagamentosIndividual_tipo_contrato}','#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');">		
			<brArq:submitCheckClient />
		</br:brCommandButton>
	</br:brPanelGrid>	  
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}"/>
		</br:brPanelGroup>	    
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>  
    
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
		<br:brPanelGroup>			
			<t:radio for="radioTipoFiltro" index="2" />
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_identificacao_conta_debito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<a4j:outputPanel id="panelBancoContaDebito" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco_conta_debito}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',1)" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" converter="javax.faces.Integer" value="#{filtroAgendamentoEfetivacaoEstornoBean.bancoContaDebitoFiltro}" size="4" maxlength="3" id="txtBancoContaDebito" onkeyup="proximoCampo(3,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:txtBancoContaDebito','conLiberarPagtosIndividuaisPendForm:txtAgenciaContaDebito');">
				<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{liberarPagtosIndividuaisPendBean.controlarArgumentosPesquisa}"/>				
			</br:brInputText>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgenciaContaDebito" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia_conta_debito}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',2)" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" converter="javax.faces.Integer" value="#{filtroAgendamentoEfetivacaoEstornoBean.agenciaContaDebitoBancariaFiltro}" size="6" maxlength="5" id="txtAgenciaContaDebito" onkeyup="proximoCampo(5,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:txtAgenciaContaDebito','conLiberarPagtosIndividuaisPendForm:txtContaContaDebito');">
				<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{liberarPagtosIndividuaisPendBean.controlarArgumentosPesquisa}"/>				
			</br:brInputText>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelContaContaDebito" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',3)" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" onkeypress="onlyNum();" value="#{filtroAgendamentoEfetivacaoEstornoBean.contaBancariaContaDebitoFiltro}" size="17" maxlength="13" id="txtContaContaDebito" >
						<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{liberarPagtosIndividuaisPendBean.controlarArgumentosPesquisa}"/>				
					</br:brInputText>
				</br:brPanelGroup>
			</br:brPanelGrid>				
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<a4j:outputPanel id="painelArgumentos" style="width: 100%; text-align: left" ajaxRendered="true">					
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<t:selectOneRadio id="radioAgendadosPagosNaoPagos" disabled="true" value="#{liberarPagtosIndividuaisPendBean.agendadosPagosNaoPagos}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false" >  
					<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_agendados}" />
				</t:selectOneRadio>		
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_data_pagamento}:" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	

	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGroup rendered="#{(liberarPagtosIndividuaisPendBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) && !liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}">
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialPagamento');" id="dataInicialPagamento" value="#{liberarPagtosIndividuaisPendBean.dataInicialPagamentoFiltro}" >
					</app:calendar>
				</br:brPanelGroup>
				<br:brPanelGroup rendered="#{(!liberarPagtosIndividuaisPendBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}">
					<app:calendar id="dataInicialPagamentoDes" value="#{liberarPagtosIndividuaisPendBean.dataInicialPagamentoFiltro}" disabled="true" >
					</app:calendar>
				</br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
				<br:brPanelGroup rendered="#{(liberarPagtosIndividuaisPendBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) && !liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}">
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFinalPagamento');" id="dataFinalPagamento" value="#{liberarPagtosIndividuaisPendBean.dataFinalPagamentoFiltro}" >
	 				</app:calendar>	
				</br:brPanelGroup>	
				<br:brPanelGroup rendered="#{(!liberarPagtosIndividuaisPendBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}">
					<app:calendar id="dataFinalPagamentoDes" value="#{liberarPagtosIndividuaisPendBean.dataFinalPagamentoFiltro}" disabled="true">
	 				</app:calendar>	
				</br:brPanelGroup>	
			</br:brPanelGroup>			    
		</br:brPanelGrid>
	</a4j:outputPanel>		
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>			

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
				<h:selectBooleanCheckbox id="chkParticipanteContrato" value="#{liberarPagtosIndividuaisPendBean.chkParticipanteContrato}" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2 || (!liberarPagtosIndividuaisPendBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" style="margin-right:5px">
					<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="btoBuscar,txtCodigoParticipante,txtNomeParticipante" action="#{filtroAgendamentoEfetivacaoEstornoBean.limparParticipanteContrato}"/>
				</h:selectBooleanCheckbox>		
				
				<br:brPanelGroup>	
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf_cnpj_participante}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.corpoCpfCnpjParticipanteFiltro}" converter="javax.faces.Long"  size="12" maxlength="9" onkeypress="onlyNum();" id="txtCorpoCpfCnpjParticipanteFiltro" disabled="#{!liberarPagtosIndividuaisPendBean.chkParticipanteContrato ||  liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>					
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.filialCpfCnpjParticipanteFiltro}" size="6" maxlength="4" converter="javax.faces.Integer"  onkeypress="onlyNum();" id="txtFilialCpfCnpjParticipanteFiltro" disabled="#{!liberarPagtosIndividuaisPendBean.chkParticipanteContrato ||  liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">

					</br:brInputText>
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.controleCpfCnpjParticipanteFiltro}" size="4" maxlength="2"  converter="javax.faces.Integer" onkeypress="onlyNum();" id="txtControleCpfCnpjParticipanteFiltro" disabled="#{!liberarPagtosIndividuaisPendBean.chkParticipanteContrato ||  liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">						
					</br:brInputText>				
				
					<h:inputHidden id="txtCodigoParticipante" value="#{filtroAgendamentoEfetivacaoEstornoBean.codigoParticipanteFiltro}"/>				
					<br:brCommandButton id="btoBuscar" styleClass="bto1" value="#{msgs.consultarPagamentosIndividual_btn_buscar}" action="#{filtroAgendamentoEfetivacaoEstornoBean.buscarParticipante}" style="cursor:hand;margin-left:20px" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
					 onclick="javascript:desbloquearTela(); return validaCampoParticipante(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_cpfCnpj}','#{msgs.label_base}','#{msgs.label_filial}','#{msgs.label_controle}','#{msgs.label_deve_ser_diferente_de_zeros}');"
					 disabled="#{!liberarPagtosIndividuaisPendBean.chkParticipanteContrato || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}">		
						<brArq:submitCheckClient />
					</br:brCommandButton>			
					
				</br:brPanelGroup>	
															
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
			
			<br:brPanelGrid columns="2"style="margin-left:20px" cellpadding="0" cellspacing="0">					
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpfCnpj}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.cpfCnpjParticipanteFiltro}"/>
				</br:brPanelGroup>				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nomeRazaoSocial}:"/> 
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.nomeParticipanteFiltro}"/>    
				</br:brPanelGroup>	
				 
			</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
			<h:selectBooleanCheckbox id="chkContaDebito" value="#{liberarPagtosIndividuaisPendBean.chkContaDebito}" disabled="#{(filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2 || (!liberarPagtosIndividuaisPendBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado)) || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" style="margin-right:5px">
				<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="txtBancoContaDebitoCheck,txtAgenciaContaDebitoCheck,txtContaDebitoCheck" action="#{liberarPagtosIndividuaisPendBean.limparContaDebito}"/>			
			</h:selectBooleanCheckbox>
			<br:brPanelGroup>	
			 	 <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_conta_debito}"/>
			 </br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:25px">
		<a4j:outputPanel id="panelBancoContaDebitoCheck" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{!liberarPagtosIndividuaisPendBean.chkContaDebito || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{liberarPagtosIndividuaisPendBean.cdBancoContaDebito}" size="4" maxlength="3" id="txtBancoContaDebitoCheck" onkeyup="proximoCampo(3,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:txtBancoContaDebitoCheck','conLiberarPagtosIndividuaisPendForm:txtAgenciaContaDebitoCheck');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgenciaContaDebitoCheck" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{!liberarPagtosIndividuaisPendBean.chkContaDebito || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{liberarPagtosIndividuaisPendBean.cdAgenciaBancariaContaDebito}" size="6" maxlength="5" id="txtAgenciaContaDebitoCheck" onkeyup="proximoCampo(5,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:txtAgenciaContaDebitoCheck','conLiberarPagtosIndividuaisPendForm:txtContaDebitoCheck');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelContaDebitoCheck" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{!liberarPagtosIndividuaisPendBean.chkContaDebito || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" converter="javax.faces.Long" 
							onkeypress="onlyNum();" value="#{liberarPagtosIndividuaisPendBean.cdContaBancariaContaDebito}" size="17" maxlength="13" id="txtContaDebitoCheck"/>
				</br:brPanelGroup>			
			</br:brPanelGrid>				
		</a4j:outputPanel>		
    </br:brPanelGrid>	
    
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>    	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-bottom:40px" >	
				<h:selectBooleanCheckbox id="chkServicoModalidade" value="#{liberarPagtosIndividuaisPendBean.chkServicoModalidade}" disabled="#{(!liberarPagtosIndividuaisPendBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}"style="margin-right:5px">
					<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="cboTipoServico,cboModalidade" action="#{liberarPagtosIndividuaisPendBean.controleServicoModalidade}"/>
				</h:selectBooleanCheckbox>					
			</br:brPanelGrid>	
		</br:brPanelGroup>

		<br:brPanelGroup>			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="cboTipoServico" value="#{liberarPagtosIndividuaisPendBean.tipoServicoFiltro}" styleClass="HtmlSelectOneMenuBradesco"
			    	disabled="#{!liberarPagtosIndividuaisPendBean.chkServicoModalidade || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{liberarPagtosIndividuaisPendBean.listaTipoServicoFiltro}" />						
						<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onchange" reRender="cboModalidade" action="#{liberarPagtosIndividuaisPendBean.listarModalidadesPagto}"/>						
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>				
			
		    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_modalidade}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="cboModalidade" value="#{liberarPagtosIndividuaisPendBean.modalidadeFiltro}" styleClass="HtmlSelectOneMenuBradesco"
			    	  disabled="#{(liberarPagtosIndividuaisPendBean.tipoServicoFiltro == null || liberarPagtosIndividuaisPendBean.tipoServicoFiltro == 0 
			    	   || !liberarPagtosIndividuaisPendBean.chkServicoModalidade) || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_todos}"/>
						<f:selectItems value="#{liberarPagtosIndividuaisPendBean.listaModalidadeFiltro}" />												
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>				
		</br:brPanelGroup>			
		
	</br:brPanelGrid>    					
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>   
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<h:selectBooleanCheckbox id="chkSituacaoMotivo" value="#{liberarPagtosIndividuaisPendBean.chkSituacaoMotivo}" disabled="#{(!liberarPagtosIndividuaisPendBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}"  style="margin-right:5px">
			<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="cboMotivoSituacao" action="#{liberarPagtosIndividuaisPendBean.limparSituacaoMotivo}"/>
		</h:selectBooleanCheckbox>					
		<br:brPanelGroup>	
		 	 <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao_motivo}"/>
		 </br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:25px" >	
		<br:brPanelGroup>			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_motivo_situacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="cboMotivoSituacao" disabled="#{!liberarPagtosIndividuaisPendBean.chkSituacaoMotivo || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.cboMotivoSituacaoFiltro}" styleClass="HtmlSelectOneMenuBradesco" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{liberarPagtosIndividuaisPendBean.listaConsultarMotivoSituacaoPagamentoFixoFiltro}" />							
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>				
		</br:brPanelGroup>					
	</br:brPanelGrid>	  					
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>   
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="radioPesquisasFiltroPrincipal" value="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal}"  disabled="#{(!liberarPagtosIndividuaisPendBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_remessa}" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="#{msgs.consultarPagamentosIndividual_favorecido}" />
			<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="panelRadioPrincipal" action="#{liberarPagtosIndividuaisPendBean.limparRemessaFavorecidoBeneficiarioLinha}"/>										
		</t:selectOneRadio>
	</br:brPanelGrid>	
	
	<a4j:outputPanel id="panelRadioPrincipal" ajaxRendered="true">		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-bottom:50px">			
				<t:radio for="radioPesquisasFiltroPrincipal" index="0" />				
			</br:brPanelGrid>				
		</br:brPanelGroup>

		<br:brPanelGroup>	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_pagamento}:"/>
				</br:brPanelGroup>		
			</br:brPanelGrid>					
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px;">			
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="text-align:left;">
					    <br:brPanelGroup>
							<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '0' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.numeroPagamentoDeFiltro}" size="50" maxlength="30" id="txtNumeroDePagamento" style="margin-right:5px" />					
					    </br:brPanelGroup>					
					</br:brPanelGrid>		
				</br:brPanelGroup>				
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-top:6px" >		
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>	
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_valor_pagamento}:"/>
				</br:brPanelGroup>		
			</br:brPanelGrid>					
			
			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-top:5px;">			
				<br:brPanelGroup>
					<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="text-align:left;">
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_de}:" style="margin-right:5px"/>
						</br:brPanelGroup>
					    <br:brPanelGroup>
							<br:brInputText onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" styleClass="HtmlInputTextBradesco" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '0' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.valorPagamentoDeFiltro}" id="txtValorDePagamento" style="margin-right:5px" converter="decimalBrazillianConverter" alt="decimalBr" maxlength="15" size="27" onfocus="loadMasks();"/>					
					    </br:brPanelGroup>					
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_ate}:" style="margin-right:5px"/>
						</br:brPanelGroup>
					    <br:brPanelGroup>
							<br:brInputText onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" styleClass="HtmlInputTextBradesco" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '0' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}"  value="#{liberarPagtosIndividuaisPendBean.valorPagamentoAteFiltro}" size="33" maxlength="30" id="txtValorAtePagamento" converter="decimalBrazillianConverter" alt="decimalBr" maxlength="15" size="27" onfocus="loadMasks();" />					
					    </br:brPanelGroup>					
					</br:brPanelGrid>		
				</br:brPanelGroup>				
			</br:brPanelGrid>			
		</br:brPanelGroup>		
		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioPesquisasFiltroPrincipal" index="1" />				
		</br:brPanelGroup>

		<br:brPanelGroup>	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px; margin-top:5px" >	
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero}:"/>
				</br:brPanelGroup>		
			</br:brPanelGrid>					
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px; margin-left:25px">			
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{liberarPagtosIndividuaisPendBean.numeroRemessaFiltro}" size="13" 
					  maxlength="9" id="numeroRemessaFiltro" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '1' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" style="margin-right:5px" onkeypress="onlyNum();"/>					
			    </br:brPanelGroup>					
			</br:brPanelGrid>		
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioPesquisasFiltroPrincipal" index="2" />				
		</br:brPanelGroup>

		<br:brPanelGroup>	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_linha_digitavel}:"/>
				</br:brPanelGroup>		
			</br:brPanelGrid>					
			
			<br:brPanelGrid columns="8" cellpadding="0" cellspacing="0" style="text-align:left; margin-top:5px;">
				<br:brPanelGroup>			
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.linhaDigitavel1}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel1" onkeyup="proximoCampo(5,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:linhaDigitavel1','conLiberarPagtosIndividuaisPendForm:linhaDigitavel2');" style="margin-right:5px" />					
				</br:brPanelGroup>
				<br:brPanelGroup>			
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.linhaDigitavel2}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel2" onkeyup="proximoCampo(5,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:linhaDigitavel2','conLiberarPagtosIndividuaisPendForm:linhaDigitavel3');"style="margin-right:5px" />					
				</br:brPanelGroup>
				<br:brPanelGroup>			
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.linhaDigitavel3}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel3" onkeyup="proximoCampo(5,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:linhaDigitavel3','conLiberarPagtosIndividuaisPendForm:linhaDigitavel4');"style="margin-right:5px" />					
				</br:brPanelGroup>
				<br:brPanelGroup>			
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.linhaDigitavel4}" onkeypress="onlyNum();" size="7" maxlength="6" id="linhaDigitavel4" onkeyup="proximoCampo(6,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:linhaDigitavel4','conLiberarPagtosIndividuaisPendForm:linhaDigitavel5');"style="margin-right:5px" />					
				</br:brPanelGroup>
				<br:brPanelGroup>			
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.linhaDigitavel5}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel5" onkeyup="proximoCampo(5,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:linhaDigitavel5','conLiberarPagtosIndividuaisPendForm:linhaDigitavel6');"style="margin-right:5px" />					
				</br:brPanelGroup>
				<br:brPanelGroup>			
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.linhaDigitavel6}" onkeypress="onlyNum();" size="7" maxlength="6" id="linhaDigitavel6" onkeyup="proximoCampo(6,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:linhaDigitavel6','conLiberarPagtosIndividuaisPendForm:linhaDigitavel7');"style="margin-right:5px" />					
				</br:brPanelGroup>
				<br:brPanelGroup>			
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.linhaDigitavel7}" onkeypress="onlyNum();" size="2" maxlength="1" id="linhaDigitavel7" onkeyup="proximoCampo(1,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:linhaDigitavel7','conLiberarPagtosIndividuaisPendForm:linhaDigitavel8');"style="margin-right:5px" />					
				</br:brPanelGroup>
				<br:brPanelGroup>			
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '2' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.linhaDigitavel8}" onkeypress="onlyNum();" size="18" maxlength="14" id="linhaDigitavel8" />					
				</br:brPanelGroup>
			</br:brPanelGrid>		
		</br:brPanelGroup>			
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="radioFavorecidoFiltro" disabled="#{liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '3' || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" value="#{liberarPagtosIndividuaisPendBean.radioFavorecidoFiltro}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_conta_credito}" />
			<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="panelRadioPrincipal" action="#{liberarPagtosIndividuaisPendBean.limparFavorecido}"/>										
		</t:selectOneRadio>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioPesquisasFiltroPrincipal" index="3" />				
		</br:brPanelGroup>
	</br:brPanelGrid>				

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="radioFavorecidoFiltro" index="0" />			
		
		 <a4j:outputPanel id="panelCodigoFavorecido" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;margin-top:6px;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_codigo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtCodigoFavorecido" size="20" disabled="#{(liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '3' || liberarPagtosIndividuaisPendBean.radioFavorecidoFiltro != '0')  || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" maxlength="15" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{liberarPagtosIndividuaisPendBean.codigoFavorecidoFiltro}"/>
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<br:brPanelGroup>	
			<t:radio for="radioFavorecidoFiltro" index="1" />			
		</br:brPanelGroup>	
		
		<br:brPanelGroup>			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;margin-top:6px;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_inscricao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtInscricao" disabled="#{(liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '3' || liberarPagtosIndividuaisPendBean.radioFavorecidoFiltro != '1')  || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" size="20" maxlength="15" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{liberarPagtosIndividuaisPendBean.inscricaoFiltro}" 
			       style="margin-right:20px"/>
			</br:brPanelGrid>
		</br:brPanelGroup>						
			
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left; margin-top:6px;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu disabled="#{(liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '3' || liberarPagtosIndividuaisPendBean.radioFavorecidoFiltro != '1') || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" id="tipoInscricao" value="#{liberarPagtosIndividuaisPendBean.tipoInscricaoFiltro}" styleClass="HtmlSelectOneMenuBradesco" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{liberarPagtosIndividuaisPendBean.listaInscricaoFavorecidoFiltro}" />						
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>	
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:20px">
	    <br:brPanelGroup>	
			<t:radio for="radioFavorecidoFiltro" index="2" />			
	    </br:brPanelGroup>			
	    
	    <br:brPanelGroup>		    
			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:25px;margin-top:6px">		
				<a4j:outputPanel id="panelBancoRadioFavorecido" ajaxRendered="true">
					
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{(liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '3' || liberarPagtosIndividuaisPendBean.radioFavorecidoFiltro != '2')  || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{liberarPagtosIndividuaisPendBean.cdBancoContaCredito}" size="4" maxlength="3" id="txtBancoContaCredito" onkeyup="proximoCampo(3,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:txtBancoContaCredito','conLiberarPagtosIndividuaisPendForm:txtAgenciaContaCredito');">
					</br:brInputText>	
					
				</a4j:outputPanel>
				
				<a4j:outputPanel id="panelAgenciaRadioFavorecido" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{(liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '3' || liberarPagtosIndividuaisPendBean.radioFavorecidoFiltro != '2') || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{liberarPagtosIndividuaisPendBean.cdAgenciaBancariaContaCredito}" size="6" maxlength="5" id="txtAgenciaContaCredito" onkeyup="proximoCampo(5,'conLiberarPagtosIndividuaisPendForm','conLiberarPagtosIndividuaisPendForm:txtAgenciaContaCredito','conLiberarPagtosIndividuaisPendForm:txtContaContaCredito');">
					</br:brInputText>	
				</a4j:outputPanel>
				
				<a4j:outputPanel id="panelContaRadioFavorecido" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{(liberarPagtosIndividuaisPendBean.radioPesquisaFiltroPrincipal != '3' || liberarPagtosIndividuaisPendBean.radioFavorecidoFiltro != '2') || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" 
								 styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" value="#{liberarPagtosIndividuaisPendBean.cdContaBancariaContaCredito}" size="17" maxlength="13" id="txtContaContaCredito">
							</br:brInputText>	
		 				</br:brPanelGroup>			
					</br:brPanelGrid>	
				</a4j:outputPanel>
			</br:brPanelGrid>	
	    </br:brPanelGroup>							
	</br:brPanelGrid>	
		
	
	</a4j:outputPanel>		
		
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left; margin-top:6px;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_classificacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="classificacao" value="#{liberarPagtosIndividuaisPendBean.classificacaoFiltro}" styleClass="HtmlSelectOneMenuBradesco" disabled="#{liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}">
						<f:selectItem itemValue="000000000" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_classificao_numero_pagamento}"/>
						<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_classificacao_favorecido}"/>
						<f:selectItem itemValue="3" itemLabel="#{msgs.consultarPagamentosIndividual_classificacao_valor_pagamento}"/>																		
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>		
	</br:brPanelGrid>			
	</a4j:outputPanel>
	
    <f:verbatim><hr class="lin"></f:verbatim>	
    
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{liberarPagtosIndividuaisPendBean.limpar}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);" value="#{msgs.consultarPagamentosIndividual_btn_consultar}" action="#{liberarPagtosIndividuaisPendBean.consultar}"
			onclick="javascript:desbloquearTela(); return validaLiberarPagamentosIndividuaisPendentes(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
				 '#{msgs.consultarPagamentosIndividual_ou}',			
				 '#{msgs.consultarPagamentosIndividual_data_pagamento}',
				 '#{msgs.consultarPagamentosIndividual_selecione_uma_opcao_consulta_argumento}',
				 '#{msgs.label_campos_participante_necessarios}',
				 '#{msgs.consultarPagamentosIndividual_conta_debito}',
				 '#{msgs.consultarPagamentosIndividual_banco}',
				 '#{msgs.consultarPagamentosIndividual_agencia}',
				 '#{msgs.consultarPagamentosIndividual_conta}',
				 '#{msgs.consultarPagamentosIndividual_tipo_servico}',
				 '#{msgs.consultarPagamentosIndividual_modalidade}',
				 '#{msgs.consultarPagamentosIndividual_situacao_pagamento}',
				 '#{msgs.consultarPagamentosIndividual_motivo_situacao}',
				 '#{msgs.consultarPagamentosIndividual_numero_pagamento}',
 				 '#{msgs.consultarPagamentosIndividual_valor_pagamento}',
 				 '#{msgs.consultarPagamentosIndividual_remessa}',
 				 '#{msgs.consultarPagamentosIndividual_numero}',
 				 '#{msgs.consultarPagamentosIndividual_linha_digitavel}',
 				 '#{msgs.consultarPagamentosIndividual_selecione_uma_opcao_consulta_para_favorecido}',
 				 '#{msgs.consultarPagamentosIndividual_codigo}',
 				 '#{msgs.consultarPagamentosIndividual_favorecido}',
 				 '#{msgs.consultarPagamentosIndividual_inscricao}',
 				 '#{msgs.consultarPagamentosIndividual_tipo}',
 				 '#{msgs.consultarPagamentosIndividual_conta_credito}',
 				 '#{msgs.consultarPagamentosIndividual_digito}', 
  				 '#{msgs.label_tipo_conta}',
  				 '#{msgs.label_deve_ser_diferente_de_zeros}');"
				disabled="#{(!liberarPagtosIndividuaisPendBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}"> 				 
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>    		
		
    </a4j:outputPanel>		
		
		<f:verbatim>
			<br>
		</f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 
			
				<app:scrollableDataTable id="dataTable" value="#{liberarPagtosIndividuaisPendBean.listaGridLiberarPagtos}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
						  	<t:selectBooleanCheckbox disabled="#{empty liberarPagtosIndividuaisPendBean.listaGridLiberarPagtos}" id="checkSelecionarTodos" styleClass="HtmlSelectOneRadioBradesco" value="#{liberarPagtosIndividuaisPendBean.opcaoChecarTodos}" onclick="javascritp:selecionarTodos(document.forms[1],this);">
							</t:selectBooleanCheckbox>
					    </f:facet>
						<t:selectBooleanCheckbox  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" value="#{result.check}">
							<f:selectItems value="#{liberarPagtosIndividuaisPendBean.listaControleLiberarPagtos}"/>
							<a4j:support event="onclick" reRender="panelBotoes" action="#{liberarPagtosIndividuaisPendBean.habilitarPanelBotoes}"/>
						</t:selectBooleanCheckbox>
					</app:scrollableColumn>
				
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					    	<br:brOutputText value="#{msgs.conAnteciparPostergarPagtosAgeInd_coluna_cnpj_cpf_cliente_pagador}" style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cpfCnpjFormatado}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					    	<h:outputText value="#{msgs.label_nome_razao}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsRazaoSocial}" />
					</app:scrollableColumn>
	
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					    	<h:outputText value="#{msgs.label_empresa_gestora_contrato}" style="width:250; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsEmpresa}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="150px" >			
					    <f:facet name="header">
					    	<h:outputText value="#{msgs.label_numero_contrato}"  style="width:150; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nrContrato}" />
					</app:scrollableColumn>
	
					<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
					    <f:facet name="header">
					    	<h:outputText value="#{msgs.label_tipo_servico}" style="width:300; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsResumoProdutoServico}" />
					</app:scrollableColumn>
	
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					    	<h:outputText value="#{msgs.label_modalidade}"  style="width:250; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsOperacaoProdutoServico}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
					    <f:facet name="header">
					    	<h:outputText value="#{msgs.label_numero_pagamento}" style="width:150; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.cdControlePagamento}" converter="spaceConverter" escape="false"/>
					</app:scrollableColumn>
	
					<app:scrollableColumn styleClass="colTabCenter" width="130px" >			
					    <f:facet name="header">
					    	<h:outputText value="#{msgs.label_data_pagamento}" style="width:130; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dtCreditoPagamento}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="150px" >			
					    <f:facet name="header">
					    	<h:outputText value="#{msgs.label_valor_pagamento}" style="width:150; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.vlEfetivoPagamento}" converter="decimalBrazillianConverter" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					    	<h:outputText value="#{msgs.label_favorecido_beneficiario}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsFavorecido}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="220px" >			
					    <f:facet name="header">
					    	<h:outputText value="#{msgs.label_bco_ag_conta_debito}" style="width:220; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.bancoAgenciaContaDebitoFormatado}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					    	<h:outputText value="#{msgs.label_motivo_situacao_pagto}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsMotivoSituacaoPagamento}" />
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{liberarPagtosIndividuaisPendBean.pesquisarTelaConsulta}" >
					<f:facet name="first">
						<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" onclick="funcPaginacao();"/>
					</f:facet>
					<f:facet name="fastrewind">
					    <brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" onclick="funcPaginacao();"/>
					</f:facet>
					<f:facet name="previous">
					    <brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" onclick="funcPaginacao();"/>
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" onclick="funcPaginacao();" />
					</f:facet>
					<f:facet name="fastforward">
					    <brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" onclick="funcPaginacao();"/>
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" onclick="funcPaginacao();"/> 
					</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>	

		
		<f:verbatim>
			<hr class="lin">
		</f:verbatim>
		
		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
			<br:brPanelGrid columns="4" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
				<br:brPanelGroup style="width:750px">
					<br:brCommandButton id="btnLimpar" disabled="#{empty liberarPagtosIndividuaisPendBean.listaGridLiberarPagtos}" styleClass="bto1" style="margin-right:5px;" value="#{msgs.label_botao_limpar}" action="#{liberarPagtosIndividuaisPendBean.limparInformacaoConsulta}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton disabled="#{liberarPagtosIndividuaisPendBean.disableDetalhar}" style="margin-right:5px" id="btnDetalhar" styleClass="bto1" value="#{msgs.label_detalhar}"  action="#{liberarPagtosIndividuaisPendBean.detalhar}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnLiberarPendencia" disabled="#{!liberarPagtosIndividuaisPendBean.panelBotoes}" styleClass="bto1" style="margin-right:5px;" value="#{msgs.btn_liberar_pendencia}" action="#{liberarPagtosIndividuaisPendBean.liberarPendencia}" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>										
					<br:brCommandButton id="btnLiberarProcessamento" disabled="#{!liberarPagtosIndividuaisPendBean.panelBotoes}"  styleClass="bto1" value="#{msgs.btn_liberar_processamento}" action="#{liberarPagtosIndividuaisPendBean.liberarProcessamento}" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>		
			</br:brPanelGrid>	
	    </a4j:outputPanel>
	</br:brPanelGrid>
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   
	
	<t:inputHidden value="#{liberarPagtosIndividuaisPendBean.disableArgumentosConsulta}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conLiberarPagtosIndividuaisPendForm:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>		
	
	<brArq:validatorScript functionName="validateForm"/>	
</brArq:form>