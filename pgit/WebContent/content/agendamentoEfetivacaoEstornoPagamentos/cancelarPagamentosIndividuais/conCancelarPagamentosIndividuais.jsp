<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conCancelarPagamentoForm" name="conCancelarPagamentoForm" >
<a4j:jsFunction name="funcPaginacao" action="#{cancelarPagamentosIndividuaisBean.limparSelecionados}" reRender="btnLimparSelecao, btnAlterarSelecionados, dataTable" onbeforedomupdate="true"/>
<h:inputHidden id="hiddenFoco" value="#{cancelarPagamentosIndividuaisBean.foco}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="radioTipoFiltro" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}">  
			<f:selectItem itemValue="0" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_cliente}" />
			<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_contrato}" />
			<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_conta_debito}" />
			<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="formulario,dataTable,panelBotoes,dataScroller" action="#{cancelarPagamentosIndividuaisBean.limparTelaPrincipal}" status="statusAguarde" />			
		</t:selectOneRadio>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">			
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<t:radio for="radioTipoFiltro" index="0" />
			</br:brPanelGroup>		
		</br:brPanelGrid>		
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
 
		<a4j:region id="regionFiltro">
		<t:selectOneRadio id="rdoFiltroCliente" value="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '0' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="" />
		</t:selectOneRadio>
		</a4j:region>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="0" />			
			
			 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cnpj}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'conCancelarPagamentoForm','conCancelarPagamentoForm:txtCnpj','conCancelarPagamentoForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'conCancelarPagamentoForm','conCancelarPagamentoForm:txtFilial','conCancelarPagamentoForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="1" />			
			
			 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cpf}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
				    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
				    onkeyup="proximoCampo(9,'conCancelarPagamentoForm','conCancelarPagamentoForm:txtCpf','conCancelarPagamentoForm:txtControleCpf');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
			<t:radio for="rdoFiltroCliente" index="2" />		
			
			 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
			</a4j:outputPanel>
			
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="3" />			
			
			<a4j:outputPanel id="panelBanco" ajaxRendered="true">
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'conCancelarPagamentoForm','conCancelarPagamentoForm:txtBanco','conCancelarPagamentoForm:txtAgencia');"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'conCancelarPagamentoForm','conCancelarPagamentoForm:txtAgencia','conCancelarPagamentoForm:txtConta');"/>
				
			</a4j:outputPanel>
		
			<a4j:outputPanel id="panelConta" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}"
							 value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" style="margin-right:5px"/>
					</br:brPanelGroup>
				    <br:brPanelGroup>						

					</br:brPanelGroup>						
				</br:brPanelGrid>				
			</a4j:outputPanel>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
		   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{msgs.consultarPagamentosIndividual_btn_consultar_cliente}" action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
		   		 onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_banco}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');"
					actionListener="#{cancelarPagamentosIndividuaisBean.limparArgsListaAposPesquisaClienteContrato}" >		
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</a4j:outputPanel>
		</br:brPanelGrid>	
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_cliente}:"/>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>	
			
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	    
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top: 9" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	 
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescCliente}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescCliente}"/>
			</br:brPanelGroup>	    
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescCliente}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescCliente}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	
	    <f:verbatim><hr class="lin"></f:verbatim>     
	    
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<t:radio for="radioTipoFiltro" index="1" />
			</br:brPanelGroup>		
		</br:brPanelGrid>	    
	    
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	    
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:20px">
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="empresaGestora" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px" >
							<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaEmpresaGestora}" />								
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
	
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="tipoContrato" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
							<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaTipoContrato}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="numero" styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();"  value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroFiltro}" size="12" maxlength="10" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}">
					    </br:brInputText>	
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
	    <f:verbatim><hr class="lin"></f:verbatim>	
    
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;">
	   		<br:brCommandButton id="btoConsultarContrato" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" styleClass="bto1" value="#{msgs.consultarPagamentosIndividual_btn_consultar_contrato}" action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.consultarPagamentosIndividual_tipo_contrato}','#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');"
				actionListener="#{cancelarPagamentosIndividuaisBean.limparArgsListaAposPesquisaClienteContrato}"   		>		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGrid>	  
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	 
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}"/>
			</br:brPanelGroup>	    
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
	    <f:verbatim><hr class="lin"></f:verbatim>  
	    
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<t:radio for="radioTipoFiltro" index="2" />
			</br:brPanelGroup>		
		</br:brPanelGrid>		
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_identificacao_conta_debito}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<a4j:outputPanel id="panelBancoContaDebito" ajaxRendered="true">
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco_conta_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',1)" styleClass="HtmlInputTextBradesco"  onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" converter="javax.faces.Integer" value="#{filtroAgendamentoEfetivacaoEstornoBean.bancoContaDebitoFiltro}" size="4" maxlength="3" id="txtBancoContaDebito" onkeyup="proximoCampo(3,'conCancelarPagamentoForm','conCancelarPagamentoForm:txtBancoContaDebito','conCancelarPagamentoForm:txtAgenciaContaDebito');">
					<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);"  status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{cancelarPagamentosIndividuaisBean.controlarArgumentosPesquisa}"/>				
				</br:brInputText>
				
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgenciaContaDebito" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia_conta_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',2)" styleClass="HtmlInputTextBradesco"  onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" converter="javax.faces.Integer" value="#{filtroAgendamentoEfetivacaoEstornoBean.agenciaContaDebitoBancariaFiltro}" size="6" maxlength="5" id="txtAgenciaContaDebito" onkeyup="proximoCampo(5,'conCancelarPagamentoForm','conCancelarPagamentoForm:txtAgenciaContaDebito','conCancelarPagamentoForm:txtContaContaDebito');">
					<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);"  status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{cancelarPagamentosIndividuaisBean.controlarArgumentosPesquisa}"/>				
				</br:brInputText>
				
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelContaContaDebito" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',3)" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" onkeypress="onlyNum();" value="#{filtroAgendamentoEfetivacaoEstornoBean.contaBancariaContaDebitoFiltro}" size="17" maxlength="13" id="txtContaContaDebito" >
							<a4j:support  oncomplete="javascript:focoBlur(document.forms[1]);"  status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{cancelarPagamentosIndividuaisBean.controlarArgumentosPesquisa}"/>				
						</br:brInputText>
					</br:brPanelGroup>						
		    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
							</br:brPanelGroup>
					</br:brPanelGrid>	
				    <br:brPanelGroup>
	
					</br:brPanelGroup>			
				</br:brPanelGrid>				
			</a4j:outputPanel>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
    
	    <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
			<br:brPanelGroup>
				<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<a4j:outputPanel id="painelArgumentos" style="width: 100%; text-align: left" ajaxRendered="true">					
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<t:selectOneRadio id="radioAgendadosPagosNaoPagos" disabled="true" value="#{cancelarPagamentosIndividuaisBean.agendadosPagosNaoPagos}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false" >  
					<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_agendados}" />
				</t:selectOneRadio>		
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_data_pagamento}:" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
	
		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brPanelGroup rendered="#{(cancelarPagamentosIndividuaisBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) && !cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}">
						<app:calendar id="dataInicialPagamento" value="#{cancelarPagamentosIndividuaisBean.dataInicialPagamentoFiltro}" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialPagamento');">
						</app:calendar>
					</br:brPanelGroup>
					<br:brPanelGroup rendered="#{(!cancelarPagamentosIndividuaisBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}">
						<app:calendar id="dataInicialPagamentoDes" value="#{cancelarPagamentosIndividuaisBean.dataInicialPagamentoFiltro}" disabled="true" >
						</app:calendar>
					</br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
					<br:brPanelGroup rendered="#{(cancelarPagamentosIndividuaisBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) && !cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}">
						<app:calendar id="dataFinalPagamento" value="#{cancelarPagamentosIndividuaisBean.dataFinalPagamentoFiltro}" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFinalPagamento');">
		 				</app:calendar>	
					</br:brPanelGroup>	
					<br:brPanelGroup rendered="#{(!cancelarPagamentosIndividuaisBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}">
						<app:calendar id="dataFinalPagamentoDes" value="#{cancelarPagamentosIndividuaisBean.dataFinalPagamentoFiltro}" disabled="true">
		 				</app:calendar>	
					</br:brPanelGroup>	
				</br:brPanelGroup>			    
			</br:brPanelGrid>
		</a4j:outputPanel>		
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>			
	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
				<h:selectBooleanCheckbox id="chkParticipanteContrato" value="#{cancelarPagamentosIndividuaisBean.chkParticipanteContrato}" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2 || (!cancelarPagamentosIndividuaisBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" style="margin-right:5px">
					<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="btoBuscar,txtCodigoParticipante,txtNomeParticipante" action="#{filtroAgendamentoEfetivacaoEstornoBean.limparParticipanteContrato}"/>
				</h:selectBooleanCheckbox>		
				
				<br:brPanelGroup>	
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf_cnpj_participante}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.corpoCpfCnpjParticipanteFiltro}" converter="javax.faces.Long"  size="12" maxlength="9" onkeypress="onlyNum();" id="txtCorpoCpfCnpjParticipanteFiltro" disabled="#{!cancelarPagamentosIndividuaisBean.chkParticipanteContrato ||  cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>					
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.filialCpfCnpjParticipanteFiltro}" size="6" maxlength="4" converter="javax.faces.Integer"  onkeypress="onlyNum();" id="txtFilialCpfCnpjParticipanteFiltro" disabled="#{!cancelarPagamentosIndividuaisBean.chkParticipanteContrato ||  cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">

					</br:brInputText>
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.controleCpfCnpjParticipanteFiltro}" size="4" maxlength="2"  converter="javax.faces.Integer" onkeypress="onlyNum();" id="txtControleCpfCnpjParticipanteFiltro" disabled="#{!cancelarPagamentosIndividuaisBean.chkParticipanteContrato ||  cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">						
					</br:brInputText>				
				
					<h:inputHidden id="txtCodigoParticipante" value="#{filtroAgendamentoEfetivacaoEstornoBean.codigoParticipanteFiltro}"/>				
					<br:brCommandButton id="btoBuscar" styleClass="bto1" value="#{msgs.consultarPagamentosIndividual_btn_buscar}" action="#{filtroAgendamentoEfetivacaoEstornoBean.buscarParticipante}" style="cursor:hand;margin-left:20px" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
					 onclick="javascript:desbloquearTela(); return validaCampoParticipante(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_cpfCnpj}','#{msgs.label_base}','#{msgs.label_filial}','#{msgs.label_controle}','#{msgs.label_deve_ser_diferente_de_zeros}');"
					 disabled="#{!cancelarPagamentosIndividuaisBean.chkParticipanteContrato || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}">		
						<brArq:submitCheckClient />
					</br:brCommandButton>			
					
				</br:brPanelGroup>	
															
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
			
			<br:brPanelGrid columns="2"style="margin-left:20px" cellpadding="0" cellspacing="0">					
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpfCnpj}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.cpfCnpjParticipanteFiltro}"/>
				</br:brPanelGroup>				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nomeRazaoSocial}:"/> 
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.nomeParticipanteFiltro}"/>    
				</br:brPanelGroup>	
				 
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>		
		
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
				<h:selectBooleanCheckbox id="chkContaDebito" value="#{cancelarPagamentosIndividuaisBean.chkContaDebito}" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2 || (!cancelarPagamentosIndividuaisBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" style="margin-right:5px">
					<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="txtBancoContaDebitoCheck,txtAgenciaContaDebitoCheck,txtContaDebitoCheck" action="#{cancelarPagamentosIndividuaisBean.limparContaDebito}"/>			
				</h:selectBooleanCheckbox>
				<br:brPanelGroup>	
				 	 <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_conta_debito}"/>
				 </br:brPanelGroup>
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:25px">
			<a4j:outputPanel id="panelBancoContaDebitoCheck" ajaxRendered="true">
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{!cancelarPagamentosIndividuaisBean.chkContaDebito || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{cancelarPagamentosIndividuaisBean.cdBancoContaDebito}" size="4" maxlength="3" id="txtBancoContaDebitoCheck" onkeyup="proximoCampo(3,'conCancelarPagamentoForm','conCancelarPagamentoForm:txtBancoContaDebitoCheck','conCancelarPagamentoForm:txtAgenciaContaDebitoCheck');"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgenciaContaDebitoCheck" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{!cancelarPagamentosIndividuaisBean.chkContaDebito || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{cancelarPagamentosIndividuaisBean.cdAgenciaBancariaContaDebito}" size="6" maxlength="5" id="txtAgenciaContaDebitoCheck" onkeyup="proximoCampo(5,'conCancelarPagamentoForm','conCancelarPagamentoForm:txtAgenciaContaDebitoCheck','conCancelarPagamentoForm:txtContaDebitoCheck');"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelContaDebitoCheck" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{!cancelarPagamentosIndividuaisBean.chkContaDebito || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" converter="javax.faces.Long" 
								onkeypress="onlyNum();" value="#{cancelarPagamentosIndividuaisBean.cdContaBancariaContaDebito}" size="17" maxlength="13" id="txtContaDebitoCheck"/>
					</br:brPanelGroup>						
		    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
							</br:brPanelGroup>
					</br:brPanelGrid>	
				    <br:brPanelGroup>
	
					</br:brPanelGroup>			
				</br:brPanelGrid>				
			</a4j:outputPanel>
	    </br:brPanelGrid>	
	    
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>    	
	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-bottom:40px" >	
					<h:selectBooleanCheckbox id="chkServicoModalidade" value="#{cancelarPagamentosIndividuaisBean.chkServicoModalidade}" disabled="#{(!cancelarPagamentosIndividuaisBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}"style="margin-right:5px">
						<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="cboTipoServico,cboModalidade" action="#{cancelarPagamentosIndividuaisBean.controleServicoModalidade}"/>
					</h:selectBooleanCheckbox>					
				</br:brPanelGrid>	
			</br:brPanelGroup>
	
			<br:brPanelGroup>			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_servico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboTipoServico" value="#{cancelarPagamentosIndividuaisBean.tipoServicoFiltro}" styleClass="HtmlSelectOneMenuBradesco"
				    	disabled="#{!cancelarPagamentosIndividuaisBean.chkServicoModalidade || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{cancelarPagamentosIndividuaisBean.listaTipoServicoFiltro}" />						
							<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onchange" reRender="cboModalidade" action="#{cancelarPagamentosIndividuaisBean.listarModalidadesPagto}"/>						
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>				
				
			    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_modalidade}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboModalidade" value="#{cancelarPagamentosIndividuaisBean.modalidadeFiltro}" styleClass="HtmlSelectOneMenuBradesco"
				    	  disabled="#{cancelarPagamentosIndividuaisBean.tipoServicoFiltro == null || cancelarPagamentosIndividuaisBean.tipoServicoFiltro == 0 
				    	   || !cancelarPagamentosIndividuaisBean.chkServicoModalidade || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_todos}"/>
							<f:selectItems value="#{cancelarPagamentosIndividuaisBean.listaModalidadeFiltro}" />												
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>				
			</br:brPanelGroup>			
			
		</br:brPanelGrid>    					
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>   
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
			<h:selectBooleanCheckbox id="chkSituacaoMotivo" value="#{cancelarPagamentosIndividuaisBean.chkSituacaoMotivo}" disabled="#{(!cancelarPagamentosIndividuaisBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}"  style="margin-right:5px">
				<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="cboSituacaoPagamento,cboMotivoSituacao" action="#{cancelarPagamentosIndividuaisBean.limparSituacaoMotivo}"/>		
			</h:selectBooleanCheckbox>					
			<br:brPanelGroup>	
			 	 <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao_motivo}"/>
			 </br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:25px" >	
			<br:brPanelGroup>			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_situacao_pagamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu disabled="#{!cancelarPagamentosIndividuaisBean.chkSituacaoMotivo || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" id="cboSituacaoPagamento" value="#{cancelarPagamentosIndividuaisBean.cboSituacaoPagamentoFiltro}" style="margin-right:20px" styleClass="HtmlSelectOneMenuBradesco" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_agendado}"/>
							<f:selectItem itemValue="2" itemLabel="#{msgs.label_pendente}"/>
							<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onchange" reRender="cboMotivoSituacao" action="#{cancelarPagamentosIndividuaisBean.listarConsultarMotivoSituacaoPagamento}"/>																			
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>				
			</br:brPanelGroup>
			
			<br:brPanelGroup>			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_motivo_situacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboMotivoSituacao" disabled="#{!cancelarPagamentosIndividuaisBean.chkSituacaoMotivo || cancelarPagamentosIndividuaisBean.cboSituacaoPagamentoFiltro == null || cancelarPagamentosIndividuaisBean.cboSituacaoPagamentoFiltro == 0 || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.cboMotivoSituacaoFiltro}" styleClass="HtmlSelectOneMenuBradesco" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{cancelarPagamentosIndividuaisBean.listaConsultarMotivoSituacaoPagamentoFiltro}" />							
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>				
			</br:brPanelGroup>					
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio id="radioPesquisasFiltroPrincipal" value="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal}"  disabled="#{(!cancelarPagamentosIndividuaisBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_remessa}" />
				<f:selectItem itemValue="2" itemLabel="" />
				<f:selectItem itemValue="3" itemLabel="#{msgs.consultarPagamentosIndividual_favorecido}" />
				<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="panelRadioPrincipal" action="#{cancelarPagamentosIndividuaisBean.limparRemessaFavorecidoBeneficiarioLinha}"/>										
			</t:selectOneRadio>
		</br:brPanelGrid>	
		
		<a4j:outputPanel id="panelRadioPrincipal" ajaxRendered="true">		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-bottom:50px">			
					<t:radio for="radioPesquisasFiltroPrincipal" index="0" />				
				</br:brPanelGrid>				
			</br:brPanelGroup>
	
			<br:brPanelGroup>	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_pagamento}:"/>
					</br:brPanelGroup>		
				</br:brPanelGrid>					
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px;">			
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="text-align:left;">
						    <br:brPanelGroup>
								<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '0' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.numeroPagamentoDeFiltro}" size="40" maxlength="30" id="txtNumeroDePagamento" style="margin-right:5px" />					
						    </br:brPanelGroup>					
						</br:brPanelGrid>		
					</br:brPanelGroup>				
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-top:6px" >		
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>	
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_valor_pagamento}:"/>
					</br:brPanelGroup>		
				</br:brPanelGrid>					
				
				<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-top:5px;">			
					<br:brPanelGroup>
						<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="text-align:left;">
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_de}:" style="margin-right:5px"/>
							</br:brPanelGroup>
						    <br:brPanelGroup>
								<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '0' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.valorPagamentoDeFiltro}" id="txtValorDePagamento" style="margin-right:5px" converter="decimalBrazillianConverter" alt="decimalBr" maxlength="15" size="27" onfocus="loadMasks();" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');"/>					
						    </br:brPanelGroup>					
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_ate}:" style="margin-right:5px"/>
							</br:brPanelGroup>
						    <br:brPanelGroup>
								<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '0' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}"  value="#{cancelarPagamentosIndividuaisBean.valorPagamentoAteFiltro}" size="33" maxlength="30" id="txtValorAtePagamento" converter="decimalBrazillianConverter" alt="decimalBr" maxlength="15" size="27" onfocus="loadMasks();" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');"/>					
						    </br:brPanelGroup>					
						</br:brPanelGrid>		
					</br:brPanelGroup>				
				</br:brPanelGrid>			
			</br:brPanelGroup>		
			
		</br:brPanelGrid>	
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<t:radio for="radioPesquisasFiltroPrincipal" index="1" />				
			</br:brPanelGroup>
	
			<br:brPanelGroup>	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px; margin-top:5px" >	
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero}:"/>
					</br:brPanelGroup>		
				</br:brPanelGrid>					
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px; margin-left:25px">			
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" value="#{cancelarPagamentosIndividuaisBean.numeroRemessaFiltro}" size="13" 
						  maxlength="9" id="numeroRemessaFiltro" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '1' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" style="margin-right:5px" onkeypress="onlyNum();"/>					
				    </br:brPanelGroup>					
				</br:brPanelGrid>		
			</br:brPanelGroup>
		</br:brPanelGrid>		
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<t:radio for="radioPesquisasFiltroPrincipal" index="2" />				
			</br:brPanelGroup>
	
			<br:brPanelGroup>	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_linha_digitavel}:"/>
					</br:brPanelGroup>		
				</br:brPanelGrid>					
				
				<br:brPanelGrid columns="8" cellpadding="0" cellspacing="0" style="text-align:left; margin-top:5px;">
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.linhaDigitavel1}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel1" onkeyup="proximoCampo(5,'conCancelarPagamentoForm','conCancelarPagamentoForm:linhaDigitavel1','conCancelarPagamentoForm:linhaDigitavel2');" style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.linhaDigitavel2}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel2" onkeyup="proximoCampo(5,'conCancelarPagamentoForm','conCancelarPagamentoForm:linhaDigitavel2','conCancelarPagamentoForm:linhaDigitavel3');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.linhaDigitavel3}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel3" onkeyup="proximoCampo(5,'conCancelarPagamentoForm','conCancelarPagamentoForm:linhaDigitavel3','conCancelarPagamentoForm:linhaDigitavel4');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.linhaDigitavel4}" onkeypress="onlyNum();" size="7" maxlength="6" id="linhaDigitavel4" onkeyup="proximoCampo(6,'conCancelarPagamentoForm','conCancelarPagamentoForm:linhaDigitavel4','conCancelarPagamentoForm:linhaDigitavel5');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.linhaDigitavel5}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel5" onkeyup="proximoCampo(5,'conCancelarPagamentoForm','conCancelarPagamentoForm:linhaDigitavel5','conCancelarPagamentoForm:linhaDigitavel6');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.linhaDigitavel6}" onkeypress="onlyNum();" size="7" maxlength="6" id="linhaDigitavel6" onkeyup="proximoCampo(6,'conCancelarPagamentoForm','conCancelarPagamentoForm:linhaDigitavel6','conCancelarPagamentoForm:linhaDigitavel7');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.linhaDigitavel7}" onkeypress="onlyNum();" size="2" maxlength="1" id="linhaDigitavel7" onkeyup="proximoCampo(1,'conCancelarPagamentoForm','conCancelarPagamentoForm:linhaDigitavel7','conCancelarPagamentoForm:linhaDigitavel8');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.linhaDigitavel8}" onkeypress="onlyNum();" size="18" maxlength="14" id="linhaDigitavel8" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
				</br:brPanelGrid>		
			</br:brPanelGroup>			
		</br:brPanelGrid>			
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio id="radioFavorecidoFiltro" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '3' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" value="#{cancelarPagamentosIndividuaisBean.radioFavorecidoFiltro}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_conta_credito}" />
				<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="panelRadioPrincipal" action="#{cancelarPagamentosIndividuaisBean.limparFavorecido}"/>										
			</t:selectOneRadio>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<t:radio for="radioPesquisasFiltroPrincipal" index="3" />				
			</br:brPanelGroup>
		</br:brPanelGrid>				
	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="radioFavorecidoFiltro" index="0" />			
			
			 <a4j:outputPanel id="panelCodigoFavorecido" ajaxRendered="true">	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;margin-top:6px;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_codigo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCodigoFavorecido" size="20" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '3' || cancelarPagamentosIndividuaisBean.radioFavorecidoFiltro != '0' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" maxlength="15" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{cancelarPagamentosIndividuaisBean.codigoFavorecidoFiltro}"/>
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<br:brPanelGroup>	
				<t:radio for="radioFavorecidoFiltro" index="1" />			
			</br:brPanelGroup>	
			
			<br:brPanelGroup>			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;margin-top:6px;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_inscricao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtInscricao" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '3' || cancelarPagamentosIndividuaisBean.radioFavorecidoFiltro != '1' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" size="20" maxlength="15" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{cancelarPagamentosIndividuaisBean.inscricaoFiltro}" 
				       style="margin-right:20px"/>
				</br:brPanelGrid>
			</br:brPanelGroup>						
				
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left; margin-top:6px;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '3' || cancelarPagamentosIndividuaisBean.radioFavorecidoFiltro != '1' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" id="tipoInscricao" value="#{cancelarPagamentosIndividuaisBean.tipoInscricaoFiltro}" styleClass="HtmlSelectOneMenuBradesco" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{cancelarPagamentosIndividuaisBean.listaInscricaoFavorecidoFiltro}" />						
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>			
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:20px">
		    <br:brPanelGroup>	
				<t:radio for="radioFavorecidoFiltro" index="2" />			
		    </br:brPanelGroup>			
		    
		    <br:brPanelGroup>		    
				<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:25px;margin-top:6px">		
					<a4j:outputPanel id="panelBancoRadioFavorecido" ajaxRendered="true">
						
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '3' || cancelarPagamentosIndividuaisBean.radioFavorecidoFiltro != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{cancelarPagamentosIndividuaisBean.cdBancoContaCredito}" size="4" maxlength="3" id="txtBancoContaCredito" onkeyup="proximoCampo(3,'conCancelarPagamentoForm','conCancelarPagamentoForm:txtBancoContaCredito','conCancelarPagamentoForm:txtAgenciaContaCredito');">
							
						</br:brInputText>	
						
					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelAgenciaRadioFavorecido" ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '3' || cancelarPagamentosIndividuaisBean.radioFavorecidoFiltro != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{cancelarPagamentosIndividuaisBean.cdAgenciaBancariaContaCredito}" size="6" maxlength="5" id="txtAgenciaContaCredito" onkeyup="proximoCampo(5,'conCancelarPagamentoForm','conCancelarPagamentoForm:txtAgenciaContaCredito','conCancelarPagamentoForm:txtContaContaCredito');">
						
						</br:brInputText>	
					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelContaRadioFavorecido" ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
					    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
						    <br:brPanelGroup>
								<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{cancelarPagamentosIndividuaisBean.radioPesquisaFiltroPrincipal != '3' || cancelarPagamentosIndividuaisBean.radioFavorecidoFiltro != '2' || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}"
									 styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" value="#{cancelarPagamentosIndividuaisBean.cdContaBancariaContaCredito}" size="17" maxlength="13" id="txtContaContaCredito" >
									
								</br:brInputText>	
									 
			 				</br:brPanelGroup>						
				    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>
									</br:brPanelGroup>
							</br:brPanelGrid>	
						    <br:brPanelGroup>
						    
							</br:brPanelGroup>			
						</br:brPanelGrid>	
					</a4j:outputPanel>
					
					
					
				</br:brPanelGrid>	
		    </br:brPanelGroup>							
		</br:brPanelGrid>	
			
		
		</a4j:outputPanel>		
			
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left; margin-top:6px;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_classificacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="classificacao" value="#{cancelarPagamentosIndividuaisBean.classificacaoFiltro}" styleClass="HtmlSelectOneMenuBradesco" disabled="#{cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}">
							<f:selectItem itemValue="000000000" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_classificao_numero_pagamento}"/>
							<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_classificacao_favorecido}"/>
							<f:selectItem itemValue="3" itemLabel="#{msgs.consultarPagamentosIndividual_classificacao_valor_pagamento}"/>																		
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>		
		</br:brPanelGrid>			
		</a4j:outputPanel>
	
	    <f:verbatim><hr class="lin"></f:verbatim>	
	    
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{cancelarPagamentosIndividuaisBean.limpar}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);" value="#{msgs.consultarPagamentosIndividual_btn_consultar}" action="#{cancelarPagamentosIndividuaisBean.carregaListaCancelarPagtosIndividuais}"
				onclick="javascript:desbloquearTela(); return validaPagamentoIndividual(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
					 '#{msgs.consultarPagamentosIndividual_agendados}',
					 '#{msgs.consultarPagamentosIndividual_ou}',
					 '#{msgs.consultarPagamentosIndividual_pagos_naopagos}',
					 '#{msgs.consultarPagamentosIndividual_data_pagamento}',
					 '#{msgs.consultarPagamentosIndividual_selecione_uma_opcao_consulta_argumento}',
					 '#{msgs.label_campos_participante_necessarios}',
					 '#{msgs.consultarPagamentosIndividual_conta_debito}',
					 '#{msgs.consultarPagamentosIndividual_banco}',
					 '#{msgs.consultarPagamentosIndividual_agencia}',
					 '#{msgs.consultarPagamentosIndividual_conta}',
					 '#{msgs.consultarPagamentosIndividual_tipo_servico}',
					 '#{msgs.consultarPagamentosIndividual_modalidade}',
					 '#{msgs.consultarPagamentosIndividual_situacao_pagamento}',
					 '#{msgs.consultarPagamentosIndividual_motivo_situacao}',
					 '#{msgs.consultarPagamentosIndividual_numero_pagamento}',
	 				 '#{msgs.consultarPagamentosIndividual_valor_pagamento}',
	 				 '#{msgs.consultarPagamentosIndividual_remessa}',
	 				 '#{msgs.consultarPagamentosIndividual_numero}',
	 				 '#{msgs.consultarPagamentosIndividual_linha_digitavel}',
	 				 '#{msgs.consultarPagamentosIndividual_selecione_uma_opcao_consulta_para_favorecido}',
	 				 '#{msgs.consultarPagamentosIndividual_codigo}',
	 				 '#{msgs.consultarPagamentosIndividual_favorecido}',
	 				 '#{msgs.consultarPagamentosIndividual_inscricao}',
	 				 '#{msgs.consultarPagamentosIndividual_tipo}',
	 				 '#{msgs.consultarPagamentosIndividual_conta_credito}',
	 				 '#{msgs.consultarPagamentosIndividual_digito}',
	  				 '#{msgs.label_tipo_conta}',
	  				 '#{msgs.label_deve_ser_diferente_de_zeros}');"
					disabled="#{(!cancelarPagamentosIndividuaisBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}"> 				 
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
	</a4j:outputPanel>				   
		
		<f:verbatim><br></f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">

			<app:scrollableDataTable id="dataTable"  value="#{cancelarPagamentosIndividuaisBean.listaGridCancelarPagtosIndividuais}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  	<t:selectBooleanCheckbox disabled="#{empty cancelarPagamentosIndividuaisBean.listaGridCancelarPagtosIndividuais}" id="checkSelecionarTodos" styleClass="HtmlSelectOneRadioBradesco" value="#{cancelarPagamentosIndividuaisBean.selecionarTodos}" onclick="javascritp:selecionarTodos(document.forms[1],this);">
						</t:selectBooleanCheckbox>
				    </f:facet>	
					<t:selectBooleanCheckbox  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
							value="#{result.check}">
						<f:selectItems value="#{cancelarPagamentosIndividuaisBean.listaControleCancelarPagtosIndividuais}"/>	
						<a4j:support event="onclick" reRender="panelBotoes" action="#{cancelarPagamentosIndividuaisBean.habilitarPanelBotoes}"/>
					</t:selectBooleanCheckbox>
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conPagamentosFavorecido_grid_cnpj_cpf_cliente_pagador}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cpfCnpjFormatado}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_nome_razao_social}"  style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsRazaoSocial}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_empresa_gestora_contrato}" style="width:250; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsEmpresa}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="125px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_numero_contrato}"  style="width:125; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrContrato }" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_tipo_servico}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsResumoProdutoServico }" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_modalidade}"  style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsOperacaoProdutoServico }" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_numero_pagamento}" style="width:150; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdControlePagamento }" converter="spaceConverter" escape="false"/>
				</app:scrollableColumn>	

				<app:scrollableColumn styleClass="colTabCenter" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_data_pagamento}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dtCreditoPagamento }" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_valor_pagamento}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.vlEfetivoPagamento }" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>		
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_favorecido_beneficiario}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.favorecidoBeneficiarioFormatado}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="220px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_bco_ag_conta_credito}" style="width:220; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.bancoAgenciaContaCreditoFormatado}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_bco_ag_conta_debito}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.bancoAgenciaContaDebitoFormatado}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_situacao_pagamento}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoOperacaoPagamento }" />
				</app:scrollableColumn>					
			</app:scrollableDataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
	<br:brPanelGroup>
	            <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{cancelarPagamentosIndividuaisBean.pesquisar}">
						<f:facet name="first">
				  <brArq:pdcCommandButton id="primeira"
				    styleClass="HtmlCommandButtonBradesco"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"  onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"  onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"  onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="funcPaginacao();" />
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="funcPaginacao();" />
				</f:facet>
		</brArq:pdcDataScroller>
	</br:brPanelGroup>	
  </br:brPanelGrid>	
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid id="panelBotoes" columns="4" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
		<br:brPanelGroup style="width:750px">
			<br:brCommandButton id="btnLimpar" disabled="#{cancelarPagamentosIndividuaisBean.listaGridCancelarPagtosIndividuais == null}" styleClass="bto1" style="margin-right:5px;" value="#{msgs.label_botao_limpar}" action="#{cancelarPagamentosIndividuaisBean.limparGrid}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnLimparSelecionados" disabled="#{!cancelarPagamentosIndividuaisBean.panelBotoes}" styleClass="bto1" style="margin-right:5px;" value="#{msgs.label_limpar_selecionados}" action="#{cancelarPagamentosIndividuaisBean.limparSelecionados}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton style="margin-right:5px" disabled="#{!cancelarPagamentosIndividuaisBean.panelBotoes || cancelarPagamentosIndividuaisBean.checkCont  != 1}" id="btnDetalhar" styleClass="bto1" value="#{msgs.label_detalhar}"  action="#{cancelarPagamentosIndividuaisBean.detalhar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnCancelarSelecionados" disabled="#{!cancelarPagamentosIndividuaisBean.panelBotoes}" styleClass="bto1"  value="#{msgs.label_excluir_selecionados}" action="#{cancelarPagamentosIndividuaisBean.cancelarSelecionados}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
		
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   
		
	<t:inputHidden value="#{cancelarPagamentosIndividuaisBean.disableArgumentosConsulta}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conCancelarPagamentoForm:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />

</brArq:form>