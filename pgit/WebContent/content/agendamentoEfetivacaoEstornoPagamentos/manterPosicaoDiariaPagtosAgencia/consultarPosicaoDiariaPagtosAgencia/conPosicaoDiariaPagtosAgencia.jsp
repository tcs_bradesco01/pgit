<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="conPosicaoDiariaPagtosAgenciaForm" name="conPosicaoDiariaPagtosAgenciaForm" >
<a4j:jsFunction name="funcPaginacao" action="#{conPosicaoDiariaPagtosAgenciaBean.paginacao}" reRender="dataTable,dataScrolle " onbeforedomupdate="true"/>
<br:brPanelGrid columns="1" style="margin-top:9" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">


		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio disabled="#{conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}"  id="radioTipoFiltro" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="#{msgs.label_filtrar_por_cliente}" />
				<f:selectItem itemValue="1" itemLabel="#{msgs.label_filtrar_por_contrato}" />
				<f:selectItem itemValue="2" itemLabel="#{msgs.label_filtrar_por_unidade_organizacional}" />				
				<a4j:support oncomplete="javascript:foco(this);" event="onclick" status="statusAguarde" reRender="formulario, dataTable, panelBotoes,dataScroller" action="#{conPosicaoDiariaPagtosAgenciaBean.limparTelaPrincipal}"/>			
			</t:selectOneRadio>
		</br:brPanelGrid>
	
		<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">	
	 
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<t:radio for="radioTipoFiltro" index="0" />
			</br:brPanelGroup>		
		</br:brPanelGrid>		
	 
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_identificacao_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
 
		<a4j:region id="regionFiltro">
		<t:selectOneRadio id="rdoFiltroCliente"    value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.itemFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoFiltroSelecionado != '0' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="" />
			<a4j:support event="onclick" status="statusAguarde" reRender="panelBotoes, panelCnpj, panelCpf, panelNomeRazao, panelBanco, panelAgencia, panelConta, panelBtoConsultarCliente, btnConsultar,panelBotoes,dataTable,dataScroller" ajaxSingle="true" oncomplete="validarProxCampoIdentClienteContrato(document.forms[1]);" action="#{conPosicaoDiariaPagtosAgenciaBean.limparAposAlterarOpcaoCliente}"/>
		</t:selectOneRadio>
		</a4j:region>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<!--<t:radio for="rdoFiltroCliente" index="0" />			-->
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtCnpj"  onkeyup="proximoCampo(9,'conPosicaoDiariaPagtosAgenciaForm','conPosicaoDiariaPagtosAgenciaForm:txtCnpj','conPosicaoDiariaPagtosAgenciaForm:txtFilial');" 
				    style="margin-right: 5"converter="javax.faces.Long" 
				    onkeypress="onlyNum();" size="11" maxlength="9" 
				    disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.itemFiltroSelecionado != '0' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" 
				    styleClass="HtmlInputTextBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"
			     />
			    
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtFilial" onkeyup="proximoCampo(4,'conPosicaoDiariaPagtosAgenciaForm','conPosicaoDiariaPagtosAgenciaForm:txtFilial','conPosicaoDiariaPagtosAgenciaForm:txtControle');"
				     style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" 
				     disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.itemFiltroSelecionado != '0' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" 
				     styleClass="HtmlInputTextBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" 
			     />
			     
				<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" 
					onkeypress="onlyNum();" size="3" maxlength="2" 
					disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.itemFiltroSelecionado != '0' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" 
					styleClass="HtmlInputTextBradesco" 
					value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.entradaConsultarListaClientePessoas.cdControleCnpj}" 
				/>
			</br:brPanelGrid>
		</a4j:outputPanel>	 
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		<a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.itemFiltroSelecionado != '1' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'conPosicaoDiariaPagtosAgenciaForm','conPosicaoDiariaPagtosAgenciaForm:txtCpf','conPosicaoDiariaPagtosAgenciaForm:txtControleCpf');" />
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.itemFiltroSelecionado != '1' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nome_razao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.itemFiltroSelecionado != '2' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.itemFiltroSelecionado != '3' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'conPosicaoDiariaPagtosAgenciaForm','conPosicaoDiariaPagtosAgenciaForm:txtBanco','conPosicaoDiariaPagtosAgenciaForm:txtAgencia');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.itemFiltroSelecionado != '3' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'conPosicaoDiariaPagtosAgenciaForm','conPosicaoDiariaPagtosAgenciaForm:txtAgencia','conPosicaoDiariaPagtosAgenciaForm:txtConta');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			 		
				    <br:brPanelGroup>
						<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.itemFiltroSelecionado != '3' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}"
							value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" style="margin-right:5px"/>
					</br:brPanelGroup>					
			</br:brPanelGrid>				
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.itemFiltroSelecionado || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" value="#{msgs.consultarPagamentosIndividual_btn_consultar_cliente}" action="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
	   		 onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_banco}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');"
	   		 actionListener="#{conPosicaoDiariaPagtosAgenciaBean.limparArgsListaAposPesquisaClienteContratoConsultar}" >		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel> 
	</br:brPanelGrid>	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/> 
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup> 
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top: 9" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" rendered="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoFiltroSelecionado == 0}" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.empresaGestoraDescCliente}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" rendered="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoFiltroSelecionado == 0}" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.numeroDescCliente}"/>
		</br:brPanelGroup>	    
		<br:brPanelGroup>
			<br:brGraphicImage style="margin-left:20px" url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" rendered="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoFiltroSelecionado == 0}" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.descricaoContratoDescCliente}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage style="margin-left:20px" url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" rendered="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoFiltroSelecionado == 0}" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.situacaoDescCliente}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	 
    <f:verbatim><hr class="lin"></f:verbatim>     
    
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
		<br:brPanelGroup>			
			<t:radio for="radioTipoFiltro" index="1" />
		</br:brPanelGroup>		
	</br:brPanelGrid>	     
    
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_identificacao_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	    
	 
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		        <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="empresaGestora" disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoFiltroSelecionado != '1' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px" >
						<f:selectItems value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.listaEmpresaGestora}" />								
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoContrato" disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoFiltroSelecionado != '1' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
						<f:selectItems value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.listaTipoContrato}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_numero_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.numeroFiltro}" size="12" maxlength="10" disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoFiltroSelecionado != '1' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}">
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>	

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;">
		 <br:brCommandButton id="btoConsultarContrato" 
		   		disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoFiltroSelecionado != '1' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" styleClass="bto1" 
		   		value="#{msgs.consultarPagamentosIndividual_btn_consultar_contrato}" 
		   		action="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.consultarContrato}" 
		   		style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" 
		   		onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],
		   		'#{msgs.label_ocampo}',
		   	    '#{msgs.label_necessario}',
		   	    '#{msgs.label_empresa_gestora_contrato}',
		   	    '#{msgs.label_tipo_contrato}',
		   	    '#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');"
		   	    actionListener="#{conPosicaoDiariaPagtosAgenciaBean.limparArgsListaAposPesquisaClienteContratoConsultar}" >		
			<brArq:submitCheckClient />
		</br:brCommandButton>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	  
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.empresaGestoraDescContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	 
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.numeroDescContrato}"/>
		</br:brPanelGroup>	    
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.descricaoContratoDescContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.situacaoDescContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	

    <f:verbatim><hr class="lin"></f:verbatim>
    
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" > 
		<br:brPanelGroup>			
			<t:radio for="radioTipoFiltro" index="2" />
		</br:brPanelGroup>		
	</br:brPanelGrid>	
    
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px;margin-left:20px">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_unidade_organizacional_operadora}:" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
	 
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:20px">	 				
		<br:brPanelGroup>
			<br:brInputText
				onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
				styleClass="HtmlInputTextBradesco" id="txtUniOrganizacional"
				value="#{conPosicaoDiariaPagtosAgenciaBean.cdUnidadeOrganizacional}"
				size="8" maxlength="5" onkeypress="onlyNum();"
				disabled="#{conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.tipoFiltroSelecionado != '2' || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta || conPosicaoDiariaPagtosAgenciaBean.cdIndicadorBloq == 'S'}">
				<a4j:support event="onblur"
					action="#{conPosicaoDiariaPagtosAgenciaBean.limparUnidadeOrganizacional}"
					reRender="btnConsultar" />
				</br:brInputText>
			</br:brPanelGroup>		 			
	</br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>

		<br:brPanelGrid columns="1">
            <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
        </br:brPanelGrid>  
     
        <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		   
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_de_servico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup> 
				    	<br:brSelectOneMenu id="cboTipoServico" value="#{conPosicaoDiariaPagtosAgenciaBean.cboTipoServico}" styleClass="HtmlSelectOneMenuBradesco"  disabled="#{(!conPosicaoDiariaPagtosAgenciaBean.habilitaArgumentosPesquisa && !conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.clienteContratoSelecionado) || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}"  >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{conPosicaoDiariaPagtosAgenciaBean.listaServicos}" />
							<a4j:support oncomplete="javascript:foco(this);" event="onchange" status="statusAguarde" action="#{conPosicaoDiariaPagtosAgenciaBean.carregaModalidades}" reRender="cboModalidade"/>	
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>			
				
			    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_modalidade}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			  
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboModalidade" value="#{conPosicaoDiariaPagtosAgenciaBean.cboModalidade}" styleClass="HtmlSelectOneMenuBradesco" disabled="#{conPosicaoDiariaPagtosAgenciaBean.cboTipoServico == 0 || conPosicaoDiariaPagtosAgenciaBean.cboTipoServico == null || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_todos}"/>
							<f:selectItems value="#{conPosicaoDiariaPagtosAgenciaBean.listaModalidades}" />												
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
				
			</br:brPanelGroup>		
		</br:brPanelGrid> 
		

	<f:verbatim><hr class="lin"></f:verbatim>

    <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>
		
			<br:brCommandButton id="btnLimpar" style="margin-right:5px" onclick="javascript:desbloquearTela();" disabled="false" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{conPosicaoDiariaPagtosAgenciaBean.limpar}" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
			disabled="#{((!conPosicaoDiariaPagtosAgenciaBean.habilitaArgumentosPesquisa && !conPosicaoDiariaPagtosAgenciaBean.filtroConPosicaoDiariaPagtosAgenciaBean.clienteContratoSelecionado) && conPosicaoDiariaPagtosAgenciaBean.cdUnidadeOrganizacional == null) || conPosicaoDiariaPagtosAgenciaBean.disableArgumentosConsulta}" action="#{conPosicaoDiariaPagtosAgenciaBean.populaGridConsultar}" styleClass="bto1"
			onclick="javascript:desbloquearTela(); return validaPosicaoDiariaPagAgencia(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',					 
					 '#{msgs.label_unidade_organizacional_maior_zero}','#{msgs.label_modalidade}');">
									
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><br/></f:verbatim> 

	</a4j:outputPanel>	

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">

		  <app:scrollableDataTable id="dataTable" value="#{conPosicaoDiariaPagtosAgenciaBean.listaGridConsultar}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				
			<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
							<br:brOutputText value="" styleClass="tableFontStyle" style="width:30; text-align:center" />
						</f:facet>	
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false"
						 value="#{conPosicaoDiariaPagtosAgenciaBean.itemSelecionadoListaConsultar}">
							<f:selectItems value="#{conPosicaoDiariaPagtosAgenciaBean.listaControleRadioConsultar}"/>
						<a4j:support event="onclick"  reRender="panelBotoes"  />
						</t:selectOneRadio>
						<t:radio for="sorLista" index="#{parametroKey}" />
			 </app:scrollableColumn>
			  
			<app:scrollableColumn  styleClass="colTabRight" width="200px" > 
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_cnpj_cpf_cliente_pagador}" style="text-align:center;width:200"/>
				</f:facet> 
					<br:brOutputText value="#{result.cpfCnpjFormatado}" style="text-align:right;width:200"/>
			</app:scrollableColumn>	  
							   
			<app:scrollableColumn width="200" styleClass="colTabLeft">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_nomeRazao}" style="width:200; text-align:center"  />
				</f:facet>
				<br:brOutputText value="#{result.dsRazaoSocial}"  />
			</app:scrollableColumn>
						  
			<app:scrollableColumn width="200" styleClass="colTabLeft">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_empresa_gestora_contrato}" style="width:200; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.dsEmpresa}" />
			</app:scrollableColumn>
			
			<app:scrollableColumn width="150" styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_numero_contrato}" style="width:150; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.cdContratoOrigemRec}" />
			</app:scrollableColumn>
			
			<app:scrollableColumn width="250" styleClass="colTabLeft">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_tipo_de_servico}" style="width:250; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.dsServico}" />
			</app:scrollableColumn>		
			
			<app:scrollableColumn width="250" styleClass="colTabLeft">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_modalidade}" style="width:250; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.dsModalidade}" />
			</app:scrollableColumn>	
				
			<app:scrollableColumn width="150" styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_pagamentos_agendados}" style="width:150; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.vlPagamentoAgendado}" converter="decimalBrazillianConverter" />
			</app:scrollableColumn>
			 
			<app:scrollableColumn width="150" styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_pagamentos_pendentes}" style="width:150; text-align:center"  />
				</f:facet>
				<br:brOutputText value="#{result.vlPagamentoPendente}" converter="decimalBrazillianConverter" />
			</app:scrollableColumn>
						  
			<app:scrollableColumn width="150" styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_pagamentos_sem_saldo}" style="width:150; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.vlPagamentoSemSaldo}" converter="decimalBrazillianConverter"/>
			</app:scrollableColumn>
					
			<app:scrollableColumn width="150" styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_pagamentos_efetivados}" style="width:150; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.vlPagamentoEfetivado}" converter="decimalBrazillianConverter"/>
			</app:scrollableColumn>		
			
			<app:scrollableColumn width="200" styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.label_total_pagamentos}" style="width:200; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{result.vlTotalPagamentos}" converter="decimalBrazillianConverter" />
			</app:scrollableColumn>	
			
		</app:scrollableDataTable>
	  </br:brPanelGroup>			   
	 </br:brPanelGrid>
	     
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid> 
		
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable"  actionListener="#{conPosicaoDiariaPagtosAgenciaBean.pesquisar}">
			 <f:facet name="first"> 
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="fastrewind"> 
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="previous"> 
			    <brArq:pdcCommandButton id="anterior" 
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima" 
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
 			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima" 
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <f:verbatim><hr class="lin"></f:verbatim> 

	<br:brPanelGrid id="panelBotoes" columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:right;width:750px" >		
			<br:brCommandButton id="btnLimparFinal" style="margin-right:5px" disabled="#{empty conPosicaoDiariaPagtosAgenciaBean.listaGridConsultar}" onclick="javascript:desbloquearTela();" styleClass="bto1" value="#{msgs.btn_limpar}" action="#{conPosicaoDiariaPagtosAgenciaBean.limparInformacaoConsulta}">
				<brArq:submitCheckClient/> 
			</br:brCommandButton> 
     		<br:brCommandButton id="btnDetalharContaDebito" style="margin-right:5px" disabled="#{empty conPosicaoDiariaPagtosAgenciaBean.itemSelecionadoListaConsultar}" onclick="javascript:desbloquearTela();" styleClass="bto1" value="#{msgs.btn_detalhar_conta_debito}" action="#{conPosicaoDiariaPagtosAgenciaBean.detalharContaDebito}">
				<brArq:submitCheckClient/>  
			</br:brCommandButton>				 			
			<br:brCommandButton id="btnImprimir" disabled="#{empty conPosicaoDiariaPagtosAgenciaBean.listaGridConsultar}" onclick="javascript:desbloquearTela();"  styleClass="bto1" value="#{msgs.btn_imprimir_lista}" action="#{conPosicaoDiariaPagtosAgenciaBean.imprimirConsultar}">
				<brArq:submitCheckClient/> 
			</br:brCommandButton>					 	
		</br:brPanelGroup> 
	</br:brPanelGrid>   
 
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   	
		
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>