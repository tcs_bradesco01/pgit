<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detPagamentosForm" name="detPagamentosForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
 	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.nrCnpjCpf}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsRazaoSocial}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.dsEmpresa}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_numeroContrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.nroContrato}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
   <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_descricao_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsContrato}"/>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.cdSituacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	   <f:verbatim><hr class="lin"></f:verbatim>
	   
	   <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_pagamentos_agendados}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.pagamentosAgendadosSelecionado}" converter="decimalBrazillianConverter"/>    
		</br:brPanelGroup>
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_pagamentos_pendentes}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.pagamentosPendentesSelecionado}" converter="decimalBrazillianConverter"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_pagamentos_sem_saldo}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.pagamentosSemSaldoSelecionado}" converter="decimalBrazillianConverter"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_pagamentos_efetivados}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.pagamentosEfetivadosSelecionado}" converter="decimalBrazillianConverter"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_total_pagamentos}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.valorTotalPagamentosSelecionado}" converter="decimalBrazillianConverter"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	    
   <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
     <br:brPanelGroup styleClass="EstiloTabelaSemScroll">
      <app:scrollableDataTable id="dataTable" value="#{conPosicaoDiariaPagtosAgenciaBean.listaGridPagamentos}" width="100%" rowIndexVar="parametroKey" var="result" rows="10" rowClasses="tabela_celula_normal, tabela_celula_destaque">
   			
   		<app:scrollableColumn styleClass="colTabCenter" width="30px" >
			<f:facet name="header">
		    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:30; text-align:center" />
		    </f:facet>	
			<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false"
			 value="#{conPosicaoDiariaPagtosAgenciaBean.itemSelecionadoListaPagamentos}">
				<f:selectItems value="#{conPosicaoDiariaPagtosAgenciaBean.listaControleRadioPagamentos}"/>
			<a4j:support event="onclick"  reRender="btnDetalhar"  />
			</t:selectOneRadio>
	    	<t:radio for="sorLista" index="#{parametroKey}" />
         </app:scrollableColumn>
   			
		<app:scrollableColumn styleClass="colTabRight" width="200px" >
			<f:facet name="header">
		  		<br:brOutputText value="#{msgs.label_cnpj_cpf_cliente_pagador}" style="text-align:center;width:200"/>
			</f:facet>
				<br:brOutputText value="#{result.cpfCnpjFormatado}" style="text-align:right;width:200"/>
		</app:scrollableColumn>	  
						  
		<app:scrollableColumn width="150" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_nomeRazao}" styleClass="tableFontStyle"  style="width:150; text-align:center"  />
			</f:facet>
			<br:brOutputText value="#{result.dsRazaoSocial}" style="text-align:left;width:150" />
		</app:scrollableColumn>
					  
		<app:scrollableColumn width="200px" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_empresa_gestora_contrato}" styleClass="tableFontStyle"  style="width:200px; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsEmpresa}" style="text-align:left;width:200px" />
		</app:scrollableColumn>
					  
		<app:scrollableColumn width="150" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_numeroContrato}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.cdNumeroContrato}" style="text-align:right;width:150" />
		</app:scrollableColumn>
		
		<app:scrollableColumn width="250" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_tipo_servico}" styleClass="tableFontStyle"  style="width:250; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsResumoProdutoServico}" style="text-align:left;width:150"/>
		</app:scrollableColumn>		
		
		<app:scrollableColumn width="250px" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_modalidade}" styleClass="tableFontStyle"  style="width:250px; text-align:center" />
			</f:facet> 
			<br:brOutputText value="#{result.dsOperacaoProdutoServico}" style="text-align:left;width:250px"/>
		</app:scrollableColumn>	
			
		<app:scrollableColumn width="150" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_numero_pagamento}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.cdControlePagamento}" converter="spaceConverter" escape="false"/>
		</app:scrollableColumn>	
		
		<app:scrollableColumn width="150" styleClass="colTabCenter">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_data_do_pagamento}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dtCreditoPagamento}" style="text-align:center;width:150"/>
		</app:scrollableColumn>	
		
		<app:scrollableColumn width="150" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_valorEfetivado}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.vlEfetivacaoPagamentoCliente}" converter="decimalBrazillianConverter"/>
		</app:scrollableColumn>	
		
		<app:scrollableColumn width="150" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_favorecido_beneficiario}" styleClass="tableFontStyle"  style="width:250; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.beneficiarioFavorecido}" style="text-align:left;width:250" />
		</app:scrollableColumn>	
		
		<app:scrollableColumn width="150" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_bco_ag_conta_debito}" styleClass="tableFontStyle"  style="width:250; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.contaDebitoFormatada}" style="text-align:right;width:250"/>
		</app:scrollableColumn>	
		
		<app:scrollableColumn width="150" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_bco_ag_conta_credito}" styleClass="tableFontStyle" style="width:250; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.contaCreditoFormatada}" style="text-align:right;width:250" />
		</app:scrollableColumn>	
		
		<app:scrollableColumn width="150" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_situacaoPagamento}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsSituacaoOperacaoPagamento}" style="text-align:left;width:150" />
		</app:scrollableColumn>		
	
	</app:scrollableDataTable>
  </br:brPanelGroup>	
 </br:brPanelGrid>	

	  <br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
	            <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{conPosicaoDiariaPagtosAgenciaBean.pesquisarPagamentos}">
						<f:facet name="first">
				  <brArq:pdcCommandButton id="primeira"
				    styleClass="HtmlCommandButtonBradesco"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>	 
	  </br:brPanelGrid>	
	
	    <f:verbatim><hr class="lin"></f:verbatim>	
	    
	  <br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">		
	  	<br:brPanelGroup style="text-align:left;width:40px">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{conPosicaoDiariaPagtosAgenciaBean.voltarDetPagamentos}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup> 

		<br:brPanelGroup style="text-align:right;width:710px">	
			<br:brCommandButton style="cursor:hand;" id="btnDetalhar" disabled="#{conPosicaoDiariaPagtosAgenciaBean.itemSelecionadoListaPagamentos == null}" value="#{msgs.btn_detalhar}" action="#{conPosicaoDiariaPagtosAgenciaBean.detalharFiltro}" styleClass="bto1" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<br:brCommandButton style="cursor:hand;margin-left:5px" id="btnImprimir"  value="#{msgs.btn_imprimir_lista}" action="#{conPosicaoDiariaPagtosAgenciaBean.imprimirDetalharPagamentos}" styleClass="bto1" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela();">
				<brArq:submitCheckClient/>																					
			</br:brCommandButton>			 
		</br:brPanelGroup>	
 	</br:brPanelGrid>	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>