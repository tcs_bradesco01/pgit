<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detPagamentosCredContaForm" name="detPagamentosCredContaForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.nrCnpjCpf}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsRazaoSocial}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsEmpresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numeroContrato}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.nroContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsContrato}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.cdSituacaoContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_contaDebito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsBancoDebito}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_agencia}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsAgenciaDebito}"/>    
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.contaDebito}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
				
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.tipoContaDebito}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_favorecido}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_num_inscricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.numeroInscricaoFavorecido}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.descTipoFavorecido}"/>				
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsFavorecido}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.cdFavorecido}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsBancoOriginal}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia}:"/>  
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsAgenciaOriginal}"/>    
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.cdDigitoContaOriginal}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsTipoContaOriginal}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
 	<f:verbatim><hr class="lin"> </f:verbatim>
 	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  rendered="#{conPosicaoDiariaPagtosAgenciaBean.exibeBeneficiario}">
		<br:brPanelGroup>
			<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_beneficiario}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_num_inscricao}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.numeroInscricaoBeneficiario}"/>
				</br:brPanelGroup>				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo}:"/> 
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsTipoBeneficiario}"/>
				</br:brPanelGroup>	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.nomeBeneficiario}"/>
				</br:brPanelGroup>							
			</br:brPanelGrid>
			
			<f:verbatim><hr class="lin"> </f:verbatim>
		</br:brPanelGroup>							
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentosConsolidado_pagamento}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoServico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsTipoServico}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_modalidade}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsIndicadorModalidade}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_remessa}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.nrSequenciaArquivoRemessa}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_lote}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.nrLoteArquivoRemessa}"/>    
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{conPosicaoDiariaPagtosAgenciaBean.numControleInternoLote != 0}">
		        <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
		        <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_lote_interno}:"/> 
		        <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.numControleInternoLote}"/>    
		    </br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_numeroPagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.numeroPagamento}" converter="spaceConverter" escape="false"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_listaDebito}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.cdListaDebito}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataAgendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dtAgendamento}"/>
			</br:brPanelGroup>							
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataVencimento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dtVencimento}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataPagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dtPagamento}"/>
			</br:brPanelGroup>							
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataDevolucaoEstorno}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dtDevolucaoEstorno}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataEmissaDocumento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dtEmissaoDocumento}"/>    
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_quantidadeMoeda}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.qtMoeda}" converter="decimalBrazillianConverter"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorAgendado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlrAgendamento}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorEfetivado}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlrEfetivacao}" converter="decimalBrazillianConverter"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorDocumento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlDocumento}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorDesconto}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlDesconto}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorAbatimento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlAbatimento}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorMulta}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlMulta}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorMora}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlMora}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorIr}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlImpostoRenda}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorIss}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlIss}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorIof}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlIof}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorInss}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlInss}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_outrasDeducoes}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlDeducao}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_outrosAcrescimos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlAcrescimo}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoMoeda}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.cdIndicadorEconomicoMoeda}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_situacaoPagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.cdSituacaoOperacaoPagamento}"/>
			</br:brPanelGroup>	
				
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
					
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_motivoSituacaoPagamento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsMotivoSituacao}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="EspacamentoLinhas"  columns="2" cellpadding="0" cellspacing="0" rendered="#{conPosicaoDiariaPagtosAgenciaBean.exibeDcom == true}">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_operacaoDCOM}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.cdOperacaoDcom}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_situacaoDCOM}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsSituacaoDcom}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="EspacamentoLinhas"  columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_descricaoPagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsPagamento}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao_transferencia_automatica}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.cdSituacaoTranferenciaAutomatica}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_numeroDocumento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.nrDocumento}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoDocumento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.tipoDocumento}"/>    
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_serieDocumento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.cdSerieDocumento}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_usoEmpresa}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsUsoEmpresa}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_msgPrimeiraLinhaExtrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsMensagemPrimeiraLinha}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_msgSegundaLinhaExtrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsMensagemSegundaLinha}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_bancoCredito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.cdBancoCredito}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia_credito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.agenciaDigitoCredito}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta_credito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.contaDigitoCredito}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoContaCredito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.tipoContaCredito}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_bancoCreditoDestino}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.cdBancoDestino}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia_credito_destino}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.agenciaDigitoDestino}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
			

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta_credito_destino}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.contaDigitoDestino}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoContaCreditoDestino}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.tipoContaDestino}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_dataHoraInclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dataHoraInclusao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.usuarioInclusao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipoCanal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.tipoCanalInclusao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.complementoInclusao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_dataHoraManutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dataHoraManutencao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_usuario}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.usuarioManutencao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipoCanal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.tipoCanalManutencao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.complementoManutencao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{conPosicaoDiariaPagtosAgenciaBean.voltarDetalhe}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>
	

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>