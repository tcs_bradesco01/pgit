<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="saldoAtualForm" name="saldoAtualForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_cpf_cnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.nrCnpjCpf}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nome_razao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsRazaoSocial}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.dsEmpresa}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_numeroContrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.nroContrato}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
   <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_descricao_contrato}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.dsContrato}"/>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.cdSituacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	

	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_pagamentos_efetuar_dia}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid  style="margin-top:40px" columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup> 
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_agendados}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
				<br:brPanelGroup>		 	
				</br:brPanelGroup> 	
			</br:brPanelGrid> 
			 
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_pendentes}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
				<br:brPanelGroup>		 	
				</br:brPanelGroup> 	
			</br:brPanelGrid> 
						
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_sem_saldo}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
				<br:brPanelGroup>		 	
				</br:brPanelGroup> 	
			</br:brPanelGrid> 
			 
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_efetivados}:" /> 
				</br:brPanelGroup>
			</br:brPanelGrid>			
		</br:brPanelGroup> 
		
		<br:brPanelGroup style="margin-left:50px">
		</br:brPanelGroup>

		<br:brPanelGroup>		
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup style="text-align:right;width:80px">
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_quantidade}" /> 
					<f:verbatim><hr class="lin"></f:verbatim>
					
					<br:brPanelGroup style="text-align:right;width:50px" >
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.qtAgendadoSemSaldo}"/> 
					</br:brPanelGroup>
					
					<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
						<br:brPanelGroup>		 	
						</br:brPanelGroup> 	
					</br:brPanelGrid>
					
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.qtPendenteSemSaldo}"/> 
					</br:brPanelGroup>
					
					<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
						<br:brPanelGroup>		 	
						</br:brPanelGroup> 	
					</br:brPanelGrid>
					
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.qtSemSaldo}"/> 
					</br:brPanelGroup>	
					
					<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
						<br:brPanelGroup>		 	
						</br:brPanelGroup> 	
					</br:brPanelGrid> 
					
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.qtEfetivadoSemSaldo}"/> 
					</br:brPanelGroup>						
				</br:brPanelGroup>
			
				<br:brPanelGroup style="text-align:right;width:80px">
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_valor}" /> 
					<f:verbatim><hr class="lin"></f:verbatim>
					
					<br:brPanelGroup style="text-align:right;width:50px" >
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{conPosicaoDiariaPagtosAgenciaBean.vlAgendadoSemSaldo}"  converter="decimalBrazillianConverter"/>    
					</br:brPanelGroup>
					
					<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
						<br:brPanelGroup>		 	
						</br:brPanelGroup> 	
					</br:brPanelGrid>
					
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.vlPendenteSemSaldo}" converter="decimalBrazillianConverter"/> 
					</br:brPanelGroup>
					
					<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
						<br:brPanelGroup>		 	
						</br:brPanelGroup> 	
					</br:brPanelGrid>
					
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.vlSemSaldo}" converter="decimalBrazillianConverter"/> 
					</br:brPanelGroup>	
					
					<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
						<br:brPanelGroup>		 	
						</br:brPanelGroup> 	
					</br:brPanelGrid> 
					
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.vlEfetivadoSemSaldo}" converter="decimalBrazillianConverter"/> 
					</br:brPanelGroup>						
				</br:brPanelGroup>		
			</br:brPanelGrid>
		</br:brPanelGroup> 
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"></f:verbatim>
	 
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_saldos}:"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid  style="margin-top:40px" columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_livre}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
				<br:brPanelGroup>		 	
				</br:brPanelGroup> 	
			</br:brPanelGrid> 
			 
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_reserva_dispónivel}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
				<br:brPanelGroup>		 	
				</br:brPanelGroup> 	
			</br:brPanelGrid> 
			 
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup> 
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_reserva_nao_disponivel}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
				<br:brPanelGroup>		 	
				</br:brPanelGroup> 	
			</br:brPanelGrid> 
			 
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_total}:" /> 
				</br:brPanelGroup>
			</br:brPanelGrid>			
		</br:brPanelGroup> 
		
		<br:brPanelGroup style="margin-left:20px">
		</br:brPanelGroup>

		<br:brPanelGroup>		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_valor}" /> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<f:verbatim><hr class="lin"></f:verbatim>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.vlSaldoLivre}" converter="decimalBrazillianConverter"/> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
				<br:brPanelGroup>		 	
				</br:brPanelGroup> 	
			</br:brPanelGrid> 
			 
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.vlReservaDisponivel}" converter="decimalBrazillianConverter"/> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
				<br:brPanelGroup>		 	
				</br:brPanelGroup> 	
			</br:brPanelGrid> 
			 
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup> 
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.vlReservaNaoDisponivel}" converter="decimalBrazillianConverter"/>   
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
				<br:brPanelGroup>		 	
				</br:brPanelGroup> 	
			</br:brPanelGrid> 
			 
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{conPosicaoDiariaPagtosAgenciaBean.vlTotal}" converter="decimalBrazillianConverter"/>  
				</br:brPanelGroup>
			</br:brPanelGrid>	
		</br:brPanelGroup> 
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
 	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">		
		<br:brPanelGroup style="text-align:left;width:40px">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{conPosicaoDiariaPagtosAgenciaBean.voltarSaldoAtual}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>