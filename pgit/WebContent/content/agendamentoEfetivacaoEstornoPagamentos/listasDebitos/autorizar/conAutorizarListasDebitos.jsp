<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conAutorizarListasDebitosForm" name="conAutorizarListasDebitosForm" >
<h:inputHidden id="hiddenFoco" value="#{autorizarListasDebitoBean.foco}"/>
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio id="radioTipoFiltro" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{autorizarListasDebitoBean.disableArgumentosConsulta}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_cliente}" />
				<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_contrato}" />
				<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_conta_debito}" />
				<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="formulario,panelBotoes,dataTable,dataScroller" action="#{autorizarListasDebitoBean.limparTelaPrincipal}" status="statusAguarde" />
			</t:selectOneRadio>
		</br:brPanelGrid>
		
		<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">			
		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<t:radio for="radioTipoFiltro" index="0" />
				</br:brPanelGroup>		
			</br:brPanelGrid>		
		
			<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_cliente}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
	 
			<a4j:region id="regionFiltro">
			<t:selectOneRadio id="rdoFiltroCliente" value="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '0' || autorizarListasDebitoBean.disableArgumentosConsulta}">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<f:selectItem itemValue="3" itemLabel="" />
			</t:selectOneRadio>
			</a4j:region>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="0" />			
				
				 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cnpj}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
					    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtCnpj" onkeyup="proximoCampo(9,'conAutorizarListasDebitosForm','conAutorizarListasDebitosForm:txtCnpj','conAutorizarListasDebitosForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || autorizarListasDebitoBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"/>
					    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtFilial" onkeyup="proximoCampo(4,'conAutorizarListasDebitosForm','conAutorizarListasDebitosForm:txtFilial','conAutorizarListasDebitosForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || autorizarListasDebitoBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" />
						<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || autorizarListasDebitoBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" />
					</br:brPanelGrid>
				</a4j:outputPanel>	
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="1" />			
				
				 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cpf}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
					    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || autorizarListasDebitoBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
					    onkeyup="proximoCampo(9,'conAutorizarListasDebitosForm','conAutorizarListasDebitosForm:txtCpf','conAutorizarListasDebitosForm:txtControleCpf');" />
					    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || autorizarListasDebitoBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
				<t:radio for="rdoFiltroCliente" index="2" />		
				
				 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '2' || autorizarListasDebitoBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
				</a4j:outputPanel>
				
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
				
			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="3" />			
				
				<a4j:outputPanel id="panelBanco" ajaxRendered="true">
					
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || autorizarListasDebitoBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'conAutorizarListasDebitosForm','conAutorizarListasDebitosForm:txtBanco','conAutorizarListasDebitosForm:txtAgencia');"/>
				</a4j:outputPanel>
				
				<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || autorizarListasDebitoBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'conAutorizarListasDebitosForm','conAutorizarListasDebitosForm:txtAgencia','conAutorizarListasDebitosForm:txtConta');"/>
					
				</a4j:outputPanel>
			
				<a4j:outputPanel id="panelConta" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || autorizarListasDebitoBean.disableArgumentosConsulta}"
								 value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" style="margin-right:5px"/>
						</br:brPanelGroup>
					</br:brPanelGrid>				
				</a4j:outputPanel>
		    </br:brPanelGrid>
		    
		    
		    
		    <f:verbatim><hr class="lin"></f:verbatim>
			
			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
		    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
			   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado || autorizarListasDebitoBean.disableArgumentosConsulta}" value="#{msgs.consultarPagamentosIndividual_btn_consultar_cliente}" action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
			   		 onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_banco}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');"
						actionListener="#{autorizarListasDebitoBean.limparArgsListaAposPesquisaClienteContrato}" >		
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</a4j:outputPanel>
			</br:brPanelGrid>	
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_cliente}:"/>
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		   		<br:brPanelGroup >
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
				</br:brPanelGroup>	
				
				<br:brPanelGroup >			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
				</br:brPanelGroup>
		    </br:brPanelGrid>
		    
			<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top: 9" >
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	 
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescCliente}"/>
				</br:brPanelGroup>	
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>		
			
		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescCliente}"/>
				</br:brPanelGroup>	    
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescCliente}"/>
				</br:brPanelGroup>	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescCliente}"/>
				</br:brPanelGroup>	
			</br:brPanelGrid>	
		
		    <f:verbatim><hr class="lin"></f:verbatim>     
		    
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<t:radio for="radioTipoFiltro" index="1" />
				</br:brPanelGroup>		
			</br:brPanelGrid>	    
		    
			<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	    
			
			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:20px">
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
					    	<br:brSelectOneMenu id="empresaGestora" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || autorizarListasDebitoBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px" >
								<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaEmpresaGestora}" />								
							</br:brSelectOneMenu>
						</br:brPanelGroup>					
					</br:brPanelGrid>
				</br:brPanelGroup>	
		
				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
				</br:brPanelGroup>		
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_contrato}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brSelectOneMenu id="tipoContrato" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || autorizarListasDebitoBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
								<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaTipoContrato}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>					
					</br:brPanelGrid>
				</br:brPanelGroup>	
				
				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
				</br:brPanelGroup>		
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_contrato}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroFiltro}" size="12" maxlength="10" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || autorizarListasDebitoBean.disableArgumentosConsulta}">
						    </br:brInputText>	
						</br:brPanelGroup>					
					</br:brPanelGrid>
				</br:brPanelGroup>	
			</br:brPanelGrid>	
			
		    <f:verbatim><hr class="lin"></f:verbatim>	
	    
			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;">
		   		<br:brCommandButton id="btoConsultarContrato" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || autorizarListasDebitoBean.disableArgumentosConsulta}" styleClass="bto1" value="#{msgs.consultarPagamentosIndividual_btn_consultar_contrato}" action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.consultarPagamentosIndividual_tipo_contrato}','#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');"
					actionListener="#{autorizarListasDebitoBean.limparArgsListaAposPesquisaClienteContrato}"   		>		
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGrid>	  
			
			<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	 
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}"/>
				</br:brPanelGroup>	
			</br:brPanelGrid>	
		
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>		
			
		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}"/>
				</br:brPanelGroup>	    
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}"/>
				</br:brPanelGroup>	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}"/>
				</br:brPanelGroup>	
			</br:brPanelGrid>	
			
		    <f:verbatim><hr class="lin"></f:verbatim>  
		    
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<t:radio for="radioTipoFiltro" index="2" />
				</br:brPanelGroup>		
			</br:brPanelGrid>		
		
			<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_identificacao_conta_debito}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-left:20px">
				<a4j:outputPanel id="panelBancoContaDebito" ajaxRendered="true">
					
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco_conta_debito}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || autorizarListasDebitoBean.disableArgumentosConsulta}" converter="javax.faces.Integer" value="#{filtroAgendamentoEfetivacaoEstornoBean.bancoContaDebitoFiltro}" size="4" maxlength="3" id="txtBancoContaDebito" onkeyup="proximoCampo(3,'conAutorizarListasDebitosForm','conAutorizarListasDebitosForm:txtBancoContaDebito','conAutorizarListasDebitosForm:txtAgenciaContaDebito');" onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',1)">
						<a4j:support  oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{autorizarListasDebitoBean.controlarArgumentosPesquisa}"/>				
					</br:brInputText>
					
				</a4j:outputPanel>
				
				<a4j:outputPanel id="panelAgenciaContaDebito" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia_conta_debito}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brInputText onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',2)" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || autorizarListasDebitoBean.disableArgumentosConsulta}" converter="javax.faces.Integer" value="#{filtroAgendamentoEfetivacaoEstornoBean.agenciaContaDebitoBancariaFiltro}" size="6" maxlength="5" id="txtAgenciaContaDebito" onkeyup="proximoCampo(5,'conAutorizarListasDebitosForm','conAutorizarListasDebitosForm:txtAgenciaContaDebito','conAutorizarListasDebitosForm:txtContaContaDebito');">
						<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{autorizarListasDebitoBean.controlarArgumentosPesquisa}"/>				
					</br:brInputText>
					
				</a4j:outputPanel>
				
				<a4j:outputPanel id="panelContaContaDebito" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brInputText onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',3)" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || autorizarListasDebitoBean.disableArgumentosConsulta}" onkeypress="onlyNum();" value="#{filtroAgendamentoEfetivacaoEstornoBean.contaBancariaContaDebitoFiltro}" size="17" maxlength="13" id="txtContaContaDebito" >
								<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{autorizarListasDebitoBean.controlarArgumentosPesquisa}"/>				
							</br:brInputText>
						</br:brPanelGroup>						
			    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>
								</br:brPanelGroup>
						</br:brPanelGrid>	
					</br:brPanelGrid>				
				</a4j:outputPanel>
		    </br:brPanelGrid>
		    
		    <f:verbatim><hr class="lin"></f:verbatim>
	    
		    <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
				<br:brPanelGroup>
					<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_argumentos_pesquisa}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
			<a4j:outputPanel id="painelArgumentos" style="width: 100%; text-align: left" ajaxRendered="true">		
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_data_pagamento}:" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
		
			<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brPanelGroup rendered="#{(autorizarListasDebitoBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) && !autorizarListasDebitoBean.disableArgumentosConsulta}">
							<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialPagamento');" id="dataInicialPagamento" value="#{autorizarListasDebitoBean.dataInicialPagamentoFiltro}" >
							</app:calendar>
						</br:brPanelGroup>
						<br:brPanelGroup rendered="#{(!autorizarListasDebitoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || autorizarListasDebitoBean.disableArgumentosConsulta}">
							<app:calendar id="dataInicialPagamentoDes" value="#{autorizarListasDebitoBean.dataInicialPagamentoFiltro}" disabled="true" >
							</app:calendar>
						</br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
						<br:brPanelGroup rendered="#{(autorizarListasDebitoBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) && !autorizarListasDebitoBean.disableArgumentosConsulta}">
							<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFinalPagamento');" id="dataFinalPagamento" value="#{autorizarListasDebitoBean.dataFinalPagamentoFiltro}" >
			 				</app:calendar>	
						</br:brPanelGroup>	
						<br:brPanelGroup rendered="#{(!autorizarListasDebitoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || autorizarListasDebitoBean.disableArgumentosConsulta}">
							<app:calendar id="dataFinalPagamentoDes" value="#{autorizarListasDebitoBean.dataFinalPagamentoFiltro}" disabled="true">
			 				</app:calendar>	
						</br:brPanelGroup>	
					</br:brPanelGroup>			    
				</br:brPanelGrid>
			</a4j:outputPanel>		
				
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>			
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
					<h:selectBooleanCheckbox id="chkNumeroListaDebito" value="#{autorizarListasDebitoBean.chkNumeroListaDebito}" disabled="#{(!autorizarListasDebitoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || autorizarListasDebitoBean.disableArgumentosConsulta}" style="margin-right:5px">
						<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="txtNumeroListaDebitoDe, txtNumeroListaDebitoAte" action="#{autorizarListasDebitoBean.limparNumeroListaDebito}"/>
					</h:selectBooleanCheckbox>										
					<br:brPanelGroup>	
					 	 <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_lista_debito}"/>
					 </br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:25px;">			
					<br:brPanelGroup>
						<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="text-align:left;">
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_de}:" style="margin-right:5px"/>
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{!autorizarListasDebitoBean.chkNumeroListaDebito || autorizarListasDebitoBean.disableArgumentosConsulta}" value="#{autorizarListasDebitoBean.numListaDebitoDeFiltro}" size="12" maxlength="9" id="txtNumeroListaDebitoDe" style="margin-right:5px" />					
							</br:brPanelGroup>					
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_ate}:" style="margin-right:5px"/>
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{!autorizarListasDebitoBean.chkNumeroListaDebito || autorizarListasDebitoBean.disableArgumentosConsulta}" value="#{autorizarListasDebitoBean.numListaDebitoAteFiltro}" size="12" maxlength="9" id="txtNumeroListaDebitoAte" />					
							</br:brPanelGroup>					
						</br:brPanelGrid>		
					</br:brPanelGroup>				
				</br:brPanelGrid>	
		
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
				<h:selectBooleanCheckbox id="chkParticipanteContrato" value="#{autorizarListasDebitoBean.chkParticipanteContrato}" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2 || (!autorizarListasDebitoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || autorizarListasDebitoBean.disableArgumentosConsulta}" style="margin-right:5px">
					<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="btoBuscar,txtCodigoParticipante,txtNomeParticipante" action="#{filtroAgendamentoEfetivacaoEstornoBean.limparParticipanteContrato}"/>
				</h:selectBooleanCheckbox>		
				
				<br:brPanelGroup>	
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf_cnpj_participante}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.corpoCpfCnpjParticipanteFiltro}" converter="javax.faces.Long"  size="12" maxlength="9" onkeypress="onlyNum();" id="txtCorpoCpfCnpjParticipanteFiltro" disabled="#{!autorizarListasDebitoBean.chkParticipanteContrato ||  autorizarListasDebitoBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>					
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.filialCpfCnpjParticipanteFiltro}" size="6" maxlength="4" converter="javax.faces.Integer"  onkeypress="onlyNum();" id="txtFilialCpfCnpjParticipanteFiltro" disabled="#{!autorizarListasDebitoBean.chkParticipanteContrato ||  autorizarListasDebitoBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">

					</br:brInputText>
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.controleCpfCnpjParticipanteFiltro}" size="4" maxlength="2"  converter="javax.faces.Integer" onkeypress="onlyNum();" id="txtControleCpfCnpjParticipanteFiltro" disabled="#{!autorizarListasDebitoBean.chkParticipanteContrato ||  autorizarListasDebitoBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">						
					</br:brInputText>				
				
					<h:inputHidden id="txtCodigoParticipante" value="#{filtroAgendamentoEfetivacaoEstornoBean.codigoParticipanteFiltro}"/>				
					<br:brCommandButton id="btoBuscar" styleClass="bto1" value="#{msgs.consultarPagamentosIndividual_btn_buscar}" action="#{filtroAgendamentoEfetivacaoEstornoBean.buscarParticipante}" style="cursor:hand;margin-left:20px" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
					 onclick="javascript:desbloquearTela(); return validaCampoParticipante(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_cpfCnpj}','#{msgs.label_base}','#{msgs.label_filial}','#{msgs.label_controle}','#{msgs.label_deve_ser_diferente_de_zeros}');"
					 disabled="#{!autorizarListasDebitoBean.chkParticipanteContrato || autorizarListasDebitoBean.disableArgumentosConsulta}">		
						<brArq:submitCheckClient />
					</br:brCommandButton>			
					
				</br:brPanelGroup>	
															
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
			
			<br:brPanelGrid columns="2"style="margin-left:20px" cellpadding="0" cellspacing="0">					
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpfCnpj}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.cpfCnpjParticipanteFiltro}"/>
				</br:brPanelGroup>				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nomeRazaoSocial}:"/> 
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.nomeParticipanteFiltro}"/>    
				</br:brPanelGroup>	
				 
			</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>		
			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
					<h:selectBooleanCheckbox id="chkContaDebito" value="#{autorizarListasDebitoBean.chkContaDebito}" disabled="#{(filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2 || (!autorizarListasDebitoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado)) || autorizarListasDebitoBean.disableArgumentosConsulta}" style="margin-right:5px">
						<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="txtBancoContaDebitoCheck,txtAgenciaContaDebitoCheck,txtContaDebitoCheck" action="#{autorizarListasDebitoBean.limparContaDebito}"/>			
					</h:selectBooleanCheckbox>
					<br:brPanelGroup>	
					 	 <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_conta_debito}"/>
					 </br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:25px">
				<a4j:outputPanel id="panelBancoContaDebitoCheck" ajaxRendered="true">
					
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{!autorizarListasDebitoBean.chkContaDebito || autorizarListasDebitoBean.disableArgumentosConsulta}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{autorizarListasDebitoBean.cdBancoContaDebito}" size="4" maxlength="3" id="txtBancoContaDebitoCheck" onkeyup="proximoCampo(3,'conAutorizarListasDebitosForm','conAutorizarListasDebitosForm:txtBancoContaDebitoCheck','conAutorizarListasDebitosForm:txtAgenciaContaDebitoCheck');"/>
				</a4j:outputPanel>
				
				<a4j:outputPanel id="panelAgenciaContaDebitoCheck" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{!autorizarListasDebitoBean.chkContaDebito || autorizarListasDebitoBean.disableArgumentosConsulta}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{autorizarListasDebitoBean.cdAgenciaBancariaContaDebito}" size="6" maxlength="5" id="txtAgenciaContaDebitoCheck" onkeyup="proximoCampo(5,'conAutorizarListasDebitosForm','conAutorizarListasDebitosForm:txtAgenciaContaDebitoCheck','conAutorizarListasDebitosForm:txtContaDebitoCheck');"/>
				</a4j:outputPanel>
				
				<a4j:outputPanel id="panelContaDebitoCheck" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" disabled="#{!autorizarListasDebitoBean.chkContaDebito || autorizarListasDebitoBean.disableArgumentosConsulta}" converter="javax.faces.Long"
									onkeypress="onlyNum();" value="#{autorizarListasDebitoBean.cdContaBancariaContaDebito}" size="17" maxlength="13" id="txtContaDebitoCheck"/>
						</br:brPanelGroup>						
			    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>
								</br:brPanelGroup>
						</br:brPanelGrid>	
					</br:brPanelGrid>				
				</a4j:outputPanel>
		    </br:brPanelGrid>	
		    
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>    	
		
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
				<br:brPanelGroup>	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-bottom:40px" >	
						<h:selectBooleanCheckbox id="chkServicoModalidade" value="#{autorizarListasDebitoBean.chkServicoModalidade}" disabled="#{(!autorizarListasDebitoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || autorizarListasDebitoBean.disableArgumentosConsulta}"style="margin-right:5px">
							<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="cboTipoServico,cboModalidade" action="#{autorizarListasDebitoBean.controleServicoModalidade}"/>
						</h:selectBooleanCheckbox>					
					</br:brPanelGrid>	
				</br:brPanelGroup>
		
				<br:brPanelGroup>			
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_servico}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
					    	<br:brSelectOneMenu id="cboTipoServico" value="#{autorizarListasDebitoBean.tipoServicoFiltro}" styleClass="HtmlSelectOneMenuBradesco"
					    	disabled="#{!autorizarListasDebitoBean.chkServicoModalidade || autorizarListasDebitoBean.disableArgumentosConsulta}" >
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{autorizarListasDebitoBean.listaTipoServicoFiltro}" />						
								<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onchange" reRender="cboModalidade" action="#{autorizarListasDebitoBean.listarModalidadesListaDebito}"/>						
							</br:brSelectOneMenu>
						</br:brPanelGroup>					
					</br:brPanelGrid>				
					
				    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_modalidade}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
					    	<br:brSelectOneMenu id="cboModalidade" value="#{autorizarListasDebitoBean.modalidadeFiltro}" styleClass="HtmlSelectOneMenuBradesco"
					    	  disabled="#{(autorizarListasDebitoBean.tipoServicoFiltro == null || autorizarListasDebitoBean.tipoServicoFiltro == 0 
					    	   || !autorizarListasDebitoBean.chkServicoModalidade) || autorizarListasDebitoBean.disableArgumentosConsulta}" >
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_todos}"/>
								<f:selectItems value="#{autorizarListasDebitoBean.listaModalidadeListaDebitoFiltro}" />												
							</br:brSelectOneMenu>
						</br:brPanelGroup>					
					</br:brPanelGrid>				
				</br:brPanelGroup>			
				
			</br:brPanelGrid>		
			</a4j:outputPanel>
		
		    <f:verbatim><hr class="lin"></f:verbatim>	
		    
			<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{autorizarListasDebitoBean.limpar}" style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnConsultar" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);" value="#{msgs.consultarPagamentosIndividual_btn_consultar}" action="#{autorizarListasDebitoBean.consultarListasDebitos}"				
					onclick="javascript:desbloquearTela(); return validaPagamentoIndividualAutorizar(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',					 
						 '#{msgs.consultarPagamentosIndividual_data_pagamento}',
						 '#{msgs.consultarPagamentosIndividual_selecione_uma_opcao_consulta_argumento}',
						 '#{msgs.label_campos_participante_necessarios}',
						 '#{msgs.consultarPagamentosIndividual_conta_debito}',
						 '#{msgs.consultarPagamentosIndividual_banco}',
						 '#{msgs.consultarPagamentosIndividual_agencia}',
						 '#{msgs.consultarPagamentosIndividual_conta}',
						 '#{msgs.consultarPagamentosIndividual_digito}',
						 '#{msgs.consultarPagamentosIndividual_tipo_servico}',
						 '#{msgs.consultarPagamentosIndividual_modalidade}',
						 '#{msgs.consultarPagamentosIndividual_de}',
						 '#{msgs.consultarPagamentosIndividual_ate}',
						 '#{msgs.label_numero_lista_debito}',
						 '#{msgs.label_deve_ser_diferente_de_zero}');"	
						disabled="#{(!autorizarListasDebitoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || autorizarListasDebitoBean.disableArgumentosConsulta}"> 				 
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid> 
			
		</a4j:outputPanel>		
		
		<f:verbatim><br></f:verbatim> 
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
	
				<app:scrollableDataTable id="dataTable" value="#{autorizarListasDebitoBean.listaGridAutorizarListasDebitos}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
						  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
						</f:facet>		
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false"	value="#{autorizarListasDebitoBean.itemSelecionadoAutorizarListasDebitos}" >
							<f:selectItems value="#{autorizarListasDebitoBean.listaControleAutorizarListasDebitos}"/>
							<a4j:support event="onclick" reRender="btnLimpar, btnAutorizarParcial, btnAutorizarIntegral" />
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_empresa_gestora_contrato}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsEmpresa}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_numero_contrato}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nrSequenciaContratoNegocio}" />
					</app:scrollableColumn>	
				
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					    	<br:brOutputText value="#{msgs.label_numero_lista_debito}"  style="width:200px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdListaDebitoPagamento}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.conPagamentosFavorecido_grid_cnpj_cpf_cliente_pagador}" style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nrCpfCnpjFormatado}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>	
	
					<app:scrollableColumn styleClass="colTabRight" width="220px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_bco_ag_conta_debito}" style="width:220; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.contaDebitoFormatado}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabCenter" width="200px" >			
					    <f:facet name="header">
					    	<br:brOutputText value="#{msgs.label_data_pagamento_lista_debito}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dtPrevistaPagamentoFormatada}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_tipo_servico}" style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsResumoProdutoServico}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_modalidade}"  style="width:150; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsProdutoServicoRelacionado}" />
					</app:scrollableColumn>		
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_situacao_lista_debito}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsSituacaoListaDebito}" />
					</app:scrollableColumn>	
	
					<app:scrollableColumn styleClass="colTabRight" width="240px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_qtde_pagtos_efetivados}" style="width:240; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.qtPagamentoEfetivado}" />
					</app:scrollableColumn>	
	
					<app:scrollableColumn styleClass="colTabRight" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_qtde_pagtos_nao_efetivados}" style="width:250; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.qtPagamentoNaoEfetivado}" />
					</app:scrollableColumn>	
	
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_qtde_total_pagamentos}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.qtPagamentoTotal}" />
					</app:scrollableColumn>	
	
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_valor_pagtos_efetivados}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.vlPagamentoEfetivado}" converter="decimalBrazillianConverter"/>
					</app:scrollableColumn>	
	
					<app:scrollableColumn styleClass="colTabRight" width="220px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_valor_pagtos_nao_efetivados}" style="width:220; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.vlPagamentoNaoEfetivado}" converter="decimalBrazillianConverter"/>
					</app:scrollableColumn>	
	
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_valor_total_pagamentos}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.vlPagamentoTotal}" converter="decimalBrazillianConverter"/>
					</app:scrollableColumn>					
				</app:scrollableDataTable>		
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{autorizarListasDebitoBean.pesquisar}" >
				 <f:facet name="first">
				    <brArq:pdcCommandButton actionListener="#{autorizarListasDebitoBean.pesquisar}" immediate="true" id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton actionListener="#{autorizarListasDebitoBean.pesquisar}" immediate="true" id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton actionListener="#{autorizarListasDebitoBean.pesquisar}" immediate="true" id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton actionListener="#{autorizarListasDebitoBean.pesquisar}" immediate="true" id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton actionListener="#{autorizarListasDebitoBean.pesquisar}" immediate="true" id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton actionListener="#{autorizarListasDebitoBean.pesquisar}" immediate="true" id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  </f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
	
		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
		<br:brPanelGrid columns="4" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
			<br:brPanelGroup style="width:750px">
				<br:brCommandButton id="btnLimpar" disabled="#{empty autorizarListasDebitoBean.listaGridAutorizarListasDebitos}" styleClass="bto1" style="margin-right:5px;" value="#{msgs.label_botao_limpar}" action="#{autorizarListasDebitoBean.limparInformacaoConsulta}">				
					<brArq:submitCheckClient/>
				</br:brCommandButton>	
				<br:brCommandButton id="btnAutorizarParcial" disabled="#{empty autorizarListasDebitoBean.itemSelecionadoAutorizarListasDebitos}" styleClass="bto1" style="margin-right:5px;" value="#{msgs.label_autorizar_parcial}" action="#{autorizarListasDebitoBean.autorizarParcial}">				
					<brArq:submitCheckClient/>
				</br:brCommandButton>	
				<br:brCommandButton id="btnAutorizarIntegral" disabled="#{empty autorizarListasDebitoBean.itemSelecionadoAutorizarListasDebitos}" styleClass="bto1"  value="#{msgs.label_autorizar_integral}" action="#{autorizarListasDebitoBean.autorizarIntegral}">				
					<brArq:submitCheckClient/>
				</br:brCommandButton>	
			</br:brPanelGroup>		
		</br:brPanelGrid>
	    </a4j:outputPanel>			
			
		<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/> 
		
		<t:inputHidden value="#{autorizarListasDebitoBean.disableArgumentosConsulta}" id="hiddenFlagPesquisa"></t:inputHidden>
	
		<f:verbatim>
		  	<script language="javascript">
		  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conAutorizarListasDebitosForm:hiddenFlagPesquisa').value);
		  	</script>
		</f:verbatim>  
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>