<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%> 
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j"%>

<brArq:form id="detConsultarListasDebitos" name="detConsultarListasDebitos" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarListasDebitosBean.nrCnpjCpf}" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nomeRazao}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarListasDebitosBean.dsRazaoSocial}" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarListasDebitosBean.dsEmpresa}" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarListasDebitosBean.nroContrato}" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarListasDebitosBean.dsContrato}" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarListasDebitosBean.cdSituacaoContrato}" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
		 <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0"> 
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_conta_debito}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_banco}:"/> 			 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{consultarListasDebitosBean.dsBancoDebito}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarListasDebitosBean.dsAgenciaDebito}"/>
			</br:brPanelGroup>			
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarListasDebitosBean.contaDebito}"/>
			</br:brPanelGroup>						
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarListasDebitosBean.tipoContaDebito}"/> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim> 
		
	    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0"> 
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detConsultaSaldo_label_lista_debito}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	

	     <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conAssociacaoLayoutArquivoProdutoServico_servicos}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{consultarListasDebitosBean.dsTipoServico}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolManutContratos_modalidade}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarListasDebitosBean.dsModalidade}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detConsultaSaldo_label_lista_debito}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{consultarListasDebitosBean.nrListaDebito}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_pagamento_lista_debito}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarListasDebitosBean.dtPagamentoListaDebito}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim> 
		
	    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0"> 
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_pagamentos_lista_debitos}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	

	     <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_qtde_pagto_autorizados}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{consultarListasDebitosBean.qtdPagtosAutorizados}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_pagtos_autorizados}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" converter="decimalBrazillianConverter" value="#{consultarListasDebitosBean.vlPagtosAutorizados}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_qtde_pagtos_desautorizados}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{consultarListasDebitosBean.qtdPagtosDesautorizados}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_pagtos_desautorizados}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" converter="decimalBrazillianConverter" value="#{consultarListasDebitosBean.vlPagtosDesautorizados}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_qtde_pagtos_efetivados}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{consultarListasDebitosBean.qtdPagtosEfetivados}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_pagtos_efetivados}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" converter="decimalBrazillianConverter" value="#{consultarListasDebitosBean.vlPagtosEfetivados}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_qtde_pagtos_nao_efetivados}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{consultarListasDebitosBean.qtdPagtosNaoEfetivados}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_pagtos_nao_efetivados}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" converter="decimalBrazillianConverter" value="#{consultarListasDebitosBean.vlPagtosNaoEfetivados}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_qtde_pagamentos}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{consultarListasDebitosBean.qtdPagtosTotal}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_total_pagamentos}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" converter="decimalBrazillianConverter" value="#{consultarListasDebitosBean.vlPagtosTotal}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim> 
	
	
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
		
			<app:scrollableDataTable id="Grid" value="#{consultarListasDebitosBean.listaGridDetalharDebitos}" var="varResult" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{consultarListasDebitosBean.itemSelecionadoDetalharDebitos}">
							<f:selectItems value="#{consultarListasDebitosBean.listaControleDetalharDebitos}"/>
							<a4j:support status="statusAguarde" event="onclick" reRender="panelBotoes"/>
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn  styleClass="colTabLeft" width="130px" >
					<f:facet name="header">
					<br:brOutputText value="#{msgs.label_numero_pagamento}" style="width:120; text-align:center"/>
						</f:facet>
					<br:brOutputText value="#{varResult.cdControlePagamento}" converter="spaceConverter" escape="false"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  styleClass="colTabRight" width="130px" >
					<f:facet name="header">
					<br:brOutputText value="#{msgs.label_valor_pagamento}" style="width:120; text-align:center"/>
						</f:facet>
					<br:brOutputText value="#{varResult.vlPagamento}" converter="decimalBrazillianConverter" />
				</app:scrollableColumn>
				
				<app:scrollableColumn  styleClass="colTabLeft" width="240px" >
					<f:facet name="header">
					<br:brOutputText value="#{msgs.label_favorecido_beneficiario}" style="width:240; text-align:center"/>
						</f:facet>
					<br:brOutputText value="#{varResult.favorecidoBeneficiario}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  styleClass="colTabRight" width="220px" >
					<f:facet name="header">
					<br:brOutputText value="#{msgs.label_bco_ag_conta_debito}" style="width:220; text-align:center"/>
						</f:facet>
					<br:brOutputText value="#{varResult.dsContaDebitoFormatado}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  styleClass="colTabLeft" width="200px" >
					<f:facet name="header">
					<br:brOutputText value="#{msgs.label_situacao_pagto}" style="width:200; text-align:center"/>
						</f:facet>
					<br:brOutputText value="#{varResult.dsSituacaoPagamento}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  styleClass="colTabLeft" width="200px" >
					<f:facet name="header">
					<br:brOutputText value="#{msgs.label_motivo_situacao_pagamento}" style="width:200; text-align:center"/>
						</f:facet>
					<br:brOutputText value="#{varResult.dsMotivoPagamento}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  styleClass="colTabLeft" width="200px" >
					<f:facet name="header">
					<br:brOutputText value="#{msgs.label_autorizacao_lista_debito}" style="width:200; text-align:center"/>
						</f:facet>
					<br:brOutputText value="#{varResult.dsSituacaoPagamentoLista}"/>
				</app:scrollableColumn>
						
			</app:scrollableDataTable>
						
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup id="Paginacao" >
		    <brArq:pdcDataScroller id="dataScroller" for="Grid"  rendered="true" actionListener="#{consultarListasDebitosBean.pesquisarDetalhar}">
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet> 
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima" 
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup> 
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim> 
	

	    <a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
			<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup style="text-align:left;width:150px" >
					<br:brCommandButton id="btnVoltar" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_voltar}" action="#{consultarListasDebitosBean.voltar}" >	
						<brArq:submitCheckClient/>
					</br:brCommandButton>					
				</br:brPanelGroup>
				<br:brPanelGroup style="text-align:right;width:500px" >			
					<br:brCommandButton style="margin-right:5px" style="margin-right:5px" disabled= "#{empty consultarListasDebitosBean.itemSelecionadoDetalharDebitos}"  id="btnDetalhar" styleClass="bto1" value="#{msgs.label_detalhar}"  action="#{consultarListasDebitosBean.detalharMododalidades}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>	
				<br:brPanelGroup >		
					<br:brCommandButton id="btnImprimir" onclick="javascript:desbloquearTela()" styleClass="bto1" value="#{msgs.btn_imprimir_lista}" action="#{consultarListasDebitosBean.imprimirRelatorioDetalhe}" >	
						<brArq:submitCheckClient/>
					</br:brCommandButton>	
				</br:brPanelGroup>
			 </br:brPanelGrid>	
	    </a4j:outputPanel>
	
	
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>	
