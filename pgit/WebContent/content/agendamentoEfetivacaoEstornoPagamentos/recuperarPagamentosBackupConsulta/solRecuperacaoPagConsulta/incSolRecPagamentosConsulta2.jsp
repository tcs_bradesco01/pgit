<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incSolRecPagamentosConsulta2Form" name="incSolRecPagamentosConsulta2Form" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.nrCnpjCpf}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.dsRazaoSocial}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.dsEmpresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.nroContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_descricaoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.dsContrato}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_situacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.cdSituacaoContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	
	
    <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_data_pagamento}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.dsPeriodoPagamento}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.dsTipoServico}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_modalidade}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.dsModalidade}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_remessa}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.numeroRemessaFiltro}" rendered="#{solRecPagamentosConsultaBean.chkRemessa}"/>    		
		</br:brPanelGroup>				
		<br:brPanelGroup style="margin-right:10px" >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_lote_interno}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.loteInterno}" rendered="#{solRecPagamentosConsultaBean.chkRemessa}"/>    		
		</br:brPanelGroup>				
	</br:brPanelGrid>
	<f:verbatim><hr class="lin"> </f:verbatim>	
	

	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_situacao_motivo}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao_pagamento}:" />
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.dsSituacaoPagamento}" rendered="#{solRecPagamentosConsultaBean.chkSituacaoMotivo}"/>    
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_motivo_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.dsMotivoSituacaoPagamento}" rendered="#{solRecPagamentosConsultaBean.chkSituacaoMotivo}"/>    
		</br:brPanelGroup>	
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_participante_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpfCnpj}:" />
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.cpfCnpjParticipanteFiltro}" rendered="#{solRecPagamentosConsultaBean.chkParticipanteContrato}"/>    
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nomeRazaoSocial}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.nomeParticipanteFiltro}" rendered="#{solRecPagamentosConsultaBean.chkParticipanteContrato}"/>    
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_conta_debito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.descricaoBancoDebito}" rendered="#{solRecPagamentosConsultaBean.chkContaDebito}"/>
		</br:brPanelGroup>		
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/> 
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.descricaoAgenciaDebito}" rendered="#{solRecPagamentosConsultaBean.chkContaDebito}"/>    
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.descricaoContaDebito}" rendered="#{solRecPagamentosConsultaBean.chkContaDebito}"/>
		</br:brPanelGroup>	
		
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_conta}:" />
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.descricaoTipoContaDebito}" rendered="#{solRecPagamentosConsultaBean.chkContaDebito}"/>    
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	
	
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_numero_pagamento}:" />
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.consultarPagamentosIndividual_de} #{solRecPagamentosConsultaBean.numeroPagamentoDeFiltro}  #{msgs.consultarPagamentosIndividual_ate}  #{solRecPagamentosConsultaBean.numeroPagamentoAteFiltro}" rendered="#{solRecPagamentosConsultaBean.chkNumeroPagamento}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_valor_pagamento}:" />
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value=" #{msgs.consultarPagamentosIndividual_de} " rendered="#{solRecPagamentosConsultaBean.chkValorPagamento}"/>    
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.valorPagamentoDeFiltro}" converter="decimalBrazillianConverter"  rendered="#{solRecPagamentosConsultaBean.chkValorPagamento}"/>    
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value=" #{msgs.consultarPagamentosIndividual_ate} " rendered="#{solRecPagamentosConsultaBean.chkValorPagamento}"/>    
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.valorPagamentoAteFiltro}" converter="decimalBrazillianConverter" rendered="#{solRecPagamentosConsultaBean.chkValorPagamento}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	

	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.incSolRecPagamentosConsulta_identificacao_recebedor}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_favorecido}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_codigo}:" />
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.codigoFavorecidoFiltro}" rendered="#{solRecPagamentosConsultaBean.chkIdentificacaoRecebedor &&  solRecPagamentosConsultaBean.radioPesquisaFiltroPrincipal == 3 &&  solRecPagamentosConsultaBean.radioFavorecidoFiltro == 0}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_inscricao}:" />
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.dsInscricaoFavorecio}" rendered="#{solRecPagamentosConsultaBean.chkIdentificacaoRecebedor &&  solRecPagamentosConsultaBean.radioPesquisaFiltroPrincipal == 3 &&  solRecPagamentosConsultaBean.radioFavorecidoFiltro == 1}"/>    
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_tipo}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solRecPagamentosConsultaBean.dsTipoInscricao}" rendered="#{solRecPagamentosConsultaBean.chkIdentificacaoRecebedor &&  solRecPagamentosConsultaBean.radioPesquisaFiltroPrincipal == 3 &&  solRecPagamentosConsultaBean.radioFavorecidoFiltro == 1}"/>    
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_conta_credito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="7" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.cdBancoContaCredito}" rendered="#{solRecPagamentosConsultaBean.chkHabilitaCredito}"/>
		</br:brPanelGroup>		
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/> 
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.cdAgenciaBancariaContaCredito}" rendered="#{solRecPagamentosConsultaBean.chkHabilitaCredito}"/>    
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_conta}: "/>
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.cdContaBancariaContaCredito}" rendered="#{solRecPagamentosConsultaBean.chkHabilitaCredito}"/>
		</br:brPanelGroup>	
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value=" Digito: "/>
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.cdDigitoContaCredito}" rendered="#{solRecPagamentosConsultaBean.chkHabilitaCredito}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_conta_salario}: "/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="7" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_banco}: "/>
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"  value="#{solRecPagamentosConsultaBean.bancoSalario}" rendered="#{solRecPagamentosConsultaBean.chkHabilitaSalario}"/>
		</br:brPanelGroup>		
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}: "/> 
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.agenciaSalario} " rendered="#{solRecPagamentosConsultaBean.chkHabilitaSalario}"/>    
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.contaSalario}" rendered="#{solRecPagamentosConsultaBean.chkHabilitaSalario}"/>
		</br:brPanelGroup>			
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="Digito: "/>
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.digitoSalario}" rendered="#{solRecPagamentosConsultaBean.chkHabilitaSalario}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_conta_pagamento}: "/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_banco}: "/>
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.bancoPgtoDestino}" rendered="#{solRecPagamentosConsultaBean.chkHabilitaPagamento}"/>
		</br:brPanelGroup>		
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_ispb}: "/> 
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.ispbPgtoDestino}" rendered="#{solRecPagamentosConsultaBean.chkHabilitaPagamento}"/>    
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_conta}: "/>
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.contaPgtoDestino}" rendered="#{solRecPagamentosConsultaBean.chkHabilitaPagamento}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="2"  >
		<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.destino}: "/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value=" #{solRecPagamentosConsultaBean.descDestinoPagamento}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<%--
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_programada_recuperacao}:"/>
			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solRecPagamentosConsultaBean.data2}" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	--%>
	
	<f:verbatim><hr class="lin"> </f:verbatim>		
		
	 <br:brPanelGrid columns="3" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.label_voltar}" action="#{solRecPagamentosConsultaBean.voltar}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.label_confirmar}" action="#{solRecPagamentosConsultaBean.confirmar}" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela(); return false; }">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>		

		
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />

</brArq:form>