<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incSolRecPagamentosConsultaForm"
	name="incSolRecPagamentosConsultaForm">
	<h:inputHidden id="hiddenFoco"
		value="#{solRecPagamentosConsultaBean.foco}" />


	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"
		cellspacing="0">

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<t:selectOneRadio id="radioTipoFiltro"
				value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado}"
				styleClass="HtmlSelectOneRadioBradesco" layout="spread"
				forceId="true" forceIdIndex="false"
				disabled="#{solRecPagamentosConsultaBean.disableArgumentosConsulta}">
				<f:selectItem itemValue="0"
					itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_cliente}" />
				<f:selectItem itemValue="1"
					itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_contrato}" />
				<a4j:support oncomplete="javascript:foco(this);" event="onclick"
					status="statusAguarde"
					reRender="formulario,tableFavorecido,panelBotoes,dataScroller"
					action="#{solRecPagamentosConsultaBean.limparFiltroPrincipal}" />
			</t:selectOneRadio>
		</br:brPanelGrid>

		<a4j:outputPanel id="formulario" style="width: 100%; text-align: left"
			ajaxRendered="true">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
				style="text-align:left;">
				<br:brPanelGroup>
					<t:radio for="radioTipoFiltro" index="0" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%"
				style="margin-top:6px;margin-left:20px">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.label_identificacao_cliente}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<a4j:region id="regionFiltro">
				<t:selectOneRadio id="rdoFiltroCliente"
					value="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado}"
					onclick="submit();" styleClass="HtmlSelectOneRadioBradesco"
					layout="spread" forceId="true" forceIdIndex="false"
					disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '0' || solRecPagamentosConsultaBean.disableArgumentosConsulta}">
					<f:selectItem itemValue="0" itemLabel="" />
					<f:selectItem itemValue="1" itemLabel="" />
					<f:selectItem itemValue="2" itemLabel="" />
					<f:selectItem itemValue="3" itemLabel="" />
				</t:selectOneRadio>
			</a4j:region>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="0" />

				<a4j:outputPanel id="panelCnpj" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_cnpj}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">


						<br:brInputText id="txtCnpj"
							onkeyup="proximoCampo(9,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtCnpj','incSolRecPagamentosConsultaForm:txtFilial');"
							style="margin-right: 5" converter="javax.faces.Long"
							onkeypress="onlyNum();" size="11" maxlength="9"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
							styleClass="HtmlInputTextBradesco"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" />

						<br:brInputText id="txtFilial"
							onkeyup="proximoCampo(4,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtFilial','incSolRecPagamentosConsultaForm:txtControle');"
							style="margin-right: 5" converter="javax.faces.Integer"
							onkeypress="onlyNum();" size="5" maxlength="4"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
							styleClass="HtmlInputTextBradesco"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" />

						<br:brInputText id="txtControle" style="margin-right: 5"
							converter="javax.faces.Integer" onkeypress="onlyNum();" size="3"
							maxlength="2"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
							styleClass="HtmlInputTextBradesco"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" />

					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="1" />

				<a4j:outputPanel id="panelCpf" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_cpf}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
						<br:brInputText
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							styleClass="HtmlInputTextBradesco" style="margin-right: 5"
							converter="javax.faces.Long" onkeypress="onlyNum();"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpf}"
							size="11" maxlength="9" id="txtCpf"
							onkeyup="proximoCampo(9,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtCpf','incSolRecPagamentosConsultaForm:txtControleCpf');" />
						<br:brInputText
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							styleClass="HtmlInputTextBradesco"
							converter="javax.faces.Integer" onkeypress="onlyNum();"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCpf}"
							maxlength="2" size="3" id="txtControleCpf" />
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="2" />

				<a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_nome_razao}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brInputText styleClass="HtmlInputTextBradesco"
						disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '2' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}"
						size="71" maxlength="70" id="txtNomeRazaoSocial" />
				</a4j:outputPanel>

			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="3" />

				<a4j:outputPanel id="panelBanco" ajaxRendered="true">

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_banco}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						converter="javax.faces.Integer"
						disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdBanco}"
						size="4" maxlength="3" id="txtBanco"
						onkeyup="proximoCampo(3,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtBanco','incSolRecPagamentosConsultaForm:txtAgencia');" />
				</a4j:outputPanel>

				<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_agencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						converter="javax.faces.Integer"
						disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}"
						size="6" maxlength="5" id="txtAgencia"
						onkeyup="proximoCampo(5,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtAgencia','incSolRecPagamentosConsultaForm:txtConta');" />

				</a4j:outputPanel>

				<a4j:outputPanel id="panelConta" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_conta}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"
								onkeypress="onlyNum();"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdContaBancaria}"
								size="17" maxlength="13" id="txtConta" style="margin-right:5px" />
						</br:brPanelGroup>
						<br:brPanelGroup>

						</br:brPanelGroup>
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>


			<f:verbatim>
				<hr class="lin">
			</f:verbatim>


			<br:brPanelGrid columns="1" width="100%" cellpadding="0"
				cellspacing="0">
				<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true"
					style="width:100%; text-align: right">
					<br:brCommandButton id="btoConsultarCliente" styleClass="bto1"
						disabled="#{empty filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
						value="#{msgs.consultarPagamentosIndividual_btn_consultar_cliente}"
						action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarClientes}"
						style="cursor:hand;"
						onmouseover="javascript:alteraBotao('visualizacao', this.id);"
						onmouseout="javascript:alteraBotao('normal', this.id);"
						onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_banco}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');"
						actionListener="#{solRecPagamentosConsultaBean.limparArgsListaAposPesquisaClienteContratoConsultar}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</a4j:outputPanel>
			</br:brPanelGrid>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
				value="#{msgs.label_cliente}:" />
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"
						style="margin-right: 5" />
					<br:brOutputText styleClass="HtmlOutputTextBradesco"
						value="#{msgs.label_cpf_cnpj}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}" />
				</br:brPanelGroup>

				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"
						style="margin-right: 5" />
					<br:brOutputText styleClass="HtmlOutputTextBradesco"
						value="#{msgs.label_nome_razao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%" style="margin-top: 9">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.label_contrato}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>


			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_empresa_gestora_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
						rendered="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 0}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescCliente}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_numero_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
						rendered="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 0}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescCliente}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage style="margin-left:20px"
						url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_descricao_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
						rendered="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 0}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescCliente}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage style="margin-left:20px"
						url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_situacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
						rendered="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 0}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescCliente}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
				style="text-align:left;">
				<br:brPanelGroup>
					<t:radio for="radioTipoFiltro" index="1" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%"
				style="margin-top:6px;margin-left:20px">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.label_identificacao_contrato}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0"
				style="margin-left:20px">

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_empresa_gestora_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="empresaGestora"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraFiltro}"
								styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
								<f:selectItems
									value="#{filtroAgendamentoEfetivacaoEstornoBean.listaEmpresaGestora}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_tipo_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="tipoContrato"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoContratoFiltro}"
								styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
								<f:selectItems
									value="#{filtroAgendamentoEfetivacaoEstornoBean.listaTipoContrato}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_numero_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								id="numero" styleClass="HtmlInputTextBradesco"
								onkeypress="onlyNum();"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroFiltro}"
								size="12" maxlength="10"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || solRecPagamentosConsultaBean.disableArgumentosConsulta}">
							</br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" width="100%" cellpadding="0"
				cellspacing="0" style="text-align:right;">
				<br:brCommandButton id="btoConsultarContrato"
					disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
					styleClass="bto1"
					value="#{msgs.consultarPagamentosIndividual_btn_consultar_contrato}"
					action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarContrato}"
					style="cursor:hand;"
					onmouseover="javascript:alteraBotao('visualizacao', this.id);"
					onmouseout="javascript:alteraBotao('normal', this.id);"
					onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],
		   		'#{msgs.label_ocampo}',
		   	    '#{msgs.label_necessario}',
		   	    '#{msgs.label_empresa_gestora_contrato}',
		   	    '#{msgs.label_tipo_contrato}',
		   	    '#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');"
					actionListener="#{solRecPagamentosConsultaBean.limparArgsListaAposPesquisaClienteContratoConsultar}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.label_contrato}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>


			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>


			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_empresa_gestora_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_numero_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_situacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.consultarPagamentosIndividual_argumentos_pesquisa}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>



			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
				style="margin-top:6px">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.consultarPagamentosIndividual_data_pagamento}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<a4j:outputPanel id="calendarios"
				style="width: 100%; text-align: left" ajaxRendered="true"
				onmousedown="javascript: cleanClipboard();">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brPanelGroup
							rendered="#{!solRecPagamentosConsultaBean.habilitaArgumentosPesquisa}">
							<app:calendar id="dataInicialPagamentoDes"
								value="#{solRecPagamentosConsultaBean.dataInicialPagamentoFiltro}"
								disabled="false">
							</app:calendar>
						</br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
							style="margin-right:5px;margin-left:5px"
							value="#{msgs.consultarPagamentosIndividual_a}" />
						<br:brPanelGroup
							rendered="#{!solRecPagamentosConsultaBean.habilitaArgumentosPesquisa}">
							<app:calendar id="dataFinalPagamentoDes"
								value="#{solRecPagamentosConsultaBean.dataFinalPagamentoFiltro}"
								disabled="false">
							</app:calendar>
						</br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</a4j:outputPanel>


			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_tipo_servico}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>



					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTipoServico"
								value="#{solRecPagamentosConsultaBean.tipoServicoFiltro}"
								styleClass="HtmlSelectOneMenuBradesco"
								disabled="#{solRecPagamentosConsultaBean.disableArgumentosConsulta}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{solRecPagamentosConsultaBean.listaTipoServicoFiltro}" />
								<a4j:support oncomplete="javascript:foco(this);"
									status="statusAguarde" event="onchange"
									reRender="cboModalidade"
									action="#{solRecPagamentosConsultaBean.listarModalidadesPagto}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>


				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_modalidade}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboModalidade"
								value="#{solRecPagamentosConsultaBean.modalidadeFiltro}"
								styleClass="HtmlSelectOneMenuBradesco"
								disabled="#{solRecPagamentosConsultaBean.tipoServicoFiltro == null || solRecPagamentosConsultaBean.tipoServicoFiltro == 0 
				    	    || solRecPagamentosConsultaBean.disableArgumentosConsulta}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{solRecPagamentosConsultaBean.listaModalidadeFiltro}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>


			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<h:selectBooleanCheckbox id="chkSituacaoMotivo"
					value="#{solRecPagamentosConsultaBean.chkSituacaoMotivo}"
					disabled="#{solRecPagamentosConsultaBean.disableArgumentosConsulta}"
					style="margin-right:5px">
					<a4j:support oncomplete="javascript:foco(this);"
						status="statusAguarde" event="onclick"
						reRender="cboSituacaoPagamento,cboMotivoSituacao"
						action="#{solRecPagamentosConsultaBean.limparSituacaoMotivo}" />
				</h:selectBooleanCheckbox>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_situacao_motivo}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="margin-left:25px">
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_situacao_pagamento}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu
								disabled="#{!solRecPagamentosConsultaBean.chkSituacaoMotivo || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
								id="cboSituacaoPagamento"
								value="#{solRecPagamentosConsultaBean.cboSituacaoPagamentoFiltro}"
								style="margin-right:20px" styleClass="HtmlSelectOneMenuBradesco">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{solRecPagamentosConsultaBean.listaConsultarSituacaoPagamentoFiltro}" />
								<a4j:support oncomplete="javascript:foco(this);"
									status="statusAguarde" event="onchange"
									reRender="cboMotivoSituacao"
									action="#{solRecPagamentosConsultaBean.listarConsultarMotivoSituacaoPagamento}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_motivo_situacao}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboMotivoSituacao"
								disabled="#{!solRecPagamentosConsultaBean.chkSituacaoMotivo || solRecPagamentosConsultaBean.cboSituacaoPagamentoFiltro == null || solRecPagamentosConsultaBean.cboSituacaoPagamentoFiltro == 0 || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
								value="#{solRecPagamentosConsultaBean.cboMotivoSituacaoFiltro}"
								styleClass="HtmlSelectOneMenuBradesco">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{solRecPagamentosConsultaBean.listaConsultarMotivoSituacaoPagamentoFiltro}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<h:selectBooleanCheckbox id="chkRemessa"
					disabled="#{solRecPagamentosConsultaBean.disableArgumentosConsulta}"
					value="#{solRecPagamentosConsultaBean.chkRemessa}">
					<a4j:support oncomplete="javascript:foco(this);"
						status="statusAguarde" event="onclick"
						reRender="numeroRemessaFiltro"
						action="#{solRecPagamentosConsultaBean.limparRemessa}" />
				</h:selectBooleanCheckbox>

				<br:brPanelGroup>
					<br:brOutputText style="margin-left:5px"
						styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conPagamentosConsolidado_remessa}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid style="margin-left:25px" columns="3" cellpadding="0" cellspacing="0" width="300px">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.conPagamentosConsolidado_numero}:" />
						</br:brPanelGroup>
						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGroup>
							<br:brInputText styleClass="HtmlInputTextBradesco"
								value="#{solRecPagamentosConsultaBean.numeroRemessaFiltro}"
								size="13" maxlength="9" id="numeroRemessaFiltro"
								disabled="#{solRecPagamentosConsultaBean.disableArgumentosConsulta || !solRecPagamentosConsultaBean.chkRemessa}"
								onkeypress="onlyNum();"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup style="margin-left:5px">
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.conPagamentosConsolidado_lote}:" />
						</br:brPanelGroup>
						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGroup>
							<br:brInputText styleClass="HtmlInputTextBradesco"
								style="margin-left:5px" 
								value="#{solRecPagamentosConsultaBean.loteInterno}"
								size="13" maxlength="9" id="numeroLoteInterno"
								disabled="#{solRecPagamentosConsultaBean.disableArgumentosConsulta || !solRecPagamentosConsultaBean.chkRemessa}"
								onkeypress="onlyNum();"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<h:selectBooleanCheckbox id="chkParticipanteContrato"
					value="#{solRecPagamentosConsultaBean.chkParticipanteContrato}"
					disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2 || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
					style="margin-right:5px">
					<a4j:support oncomplete="javascript:foco(this);"
						status="statusAguarde" event="onclick"
						reRender="btoBuscar,txtCodigoParticipante,txtNomeParticipante"
						action="#{filtroAgendamentoEfetivacaoEstornoBean.limparParticipanteContrato}" />
				</h:selectBooleanCheckbox>

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_cpf_cnpj_participante}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brInputText styleClass="HtmlInputTextBradesco"
						style="margin-right:5px"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.corpoCpfCnpjParticipanteFiltro}"
						converter="javax.faces.Long" size="12" maxlength="9"
						onkeypress="onlyNum();" id="txtCorpoCpfCnpjParticipanteFiltro"
						disabled="#{!solRecPagamentosConsultaBean.chkParticipanteContrato ||  solRecPagamentosConsultaBean.disableArgumentosConsulta}"
						onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>
					<br:brInputText styleClass="HtmlInputTextBradesco"
						style="margin-right:5px"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.filialCpfCnpjParticipanteFiltro}"
						size="6" maxlength="4" converter="javax.faces.Integer"
						onkeypress="onlyNum();" id="txtFilialCpfCnpjParticipanteFiltro"
						disabled="#{!solRecPagamentosConsultaBean.chkParticipanteContrato ||  solRecPagamentosConsultaBean.disableArgumentosConsulta}"
						onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">

					</br:brInputText>
					<br:brInputText styleClass="HtmlInputTextBradesco"
						style="margin-right:5px"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.controleCpfCnpjParticipanteFiltro}"
						size="4" maxlength="2" converter="javax.faces.Integer"
						onkeypress="onlyNum();" id="txtControleCpfCnpjParticipanteFiltro"
						disabled="#{!solRecPagamentosConsultaBean.chkParticipanteContrato ||  solRecPagamentosConsultaBean.disableArgumentosConsulta}"
						onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>

					<h:inputHidden id="txtCodigoParticipante"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.codigoParticipanteFiltro}" />
					<br:brCommandButton id="btoBuscar" styleClass="bto1"
						value="#{msgs.consultarPagamentosIndividual_btn_buscar}"
						action="#{filtroAgendamentoEfetivacaoEstornoBean.buscarParticipante}"
						style="cursor:hand;margin-left:20px"
						onmouseover="javascript:alteraBotao('visualizacao', this.id);"
						onmouseout="javascript:alteraBotao('normal', this.id);"
						onclick="javascript:desbloquearTela(); return validaCampoParticipante(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_cpfCnpj}','#{msgs.label_base}','#{msgs.label_filial}','#{msgs.label_controle}','#{msgs.label_deve_ser_diferente_de_zeros}');"
						disabled="#{!solRecPagamentosConsultaBean.chkParticipanteContrato || solRecPagamentosConsultaBean.disableArgumentosConsulta}">
						<brArq:submitCheckClient />
					</br:brCommandButton>

				</br:brPanelGroup>

			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>


			<br:brPanelGrid columns="2" style="margin-left:20px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_cpfCnpj}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.cpfCnpjParticipanteFiltro}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_nomeRazaoSocial}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.nomeParticipanteFiltro}" />
				</br:brPanelGroup>

			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<h:selectBooleanCheckbox id="chkContaDebito"
					value="#{solRecPagamentosConsultaBean.chkContaDebito}"
					disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2  || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
					style="margin-right:5px">
					<a4j:support oncomplete="javascript:foco(this);"
						status="statusAguarde" event="onclick"
						reRender="txtBancoContaDebitoCheck,txtAgenciaContaDebitoCheck,txtContaDebitoCheck,btnConsultarContaDebito,panelTipoContaDebito"
						action="#{solRecPagamentosConsultaBean.limparContaDebito}" />
				</h:selectBooleanCheckbox>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_conta_debito}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0"
				style="margin-left:25px">
				<a4j:outputPanel id="panelBancoContaDebitoCheck" ajaxRendered="true">

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_banco}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText styleClass="HtmlInputTextBradesco"
						disabled="#{!solRecPagamentosConsultaBean.chkContaDebito || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
						onkeypress="onlyNum();" converter="javax.faces.Integer"
						value="#{solRecPagamentosConsultaBean.cdBancoContaDebito}"
						size="4" maxlength="3" id="txtBancoContaDebitoCheck"
						onkeyup="proximoCampo(3,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtBancoContaDebitoCheck','incSolRecPagamentosConsultaForm:txtAgenciaContaDebitoCheck');"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>
				</a4j:outputPanel>

				<a4j:outputPanel id="panelAgenciaContaDebitoCheck"
					ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_agencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText styleClass="HtmlInputTextBradesco"
						disabled="#{!solRecPagamentosConsultaBean.chkContaDebito || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
						onkeypress="onlyNum();" converter="javax.faces.Integer"
						value="#{solRecPagamentosConsultaBean.cdAgenciaBancariaContaDebito}"
						size="6" maxlength="5" id="txtAgenciaContaDebitoCheck"
						onkeyup="proximoCampo(5,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtAgenciaContaDebitoCheck','incSolRecPagamentosConsultaForm:txtContaDebitoCheck');">
					</br:brInputText>
				</a4j:outputPanel>

				<a4j:outputPanel id="panelContaDebitoCheck" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_conta}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText styleClass="HtmlInputTextBradesco"
								disabled="#{!solRecPagamentosConsultaBean.chkContaDebito || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
								converter="javax.faces.Long" onkeypress="onlyNum();"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								value="#{solRecPagamentosConsultaBean.cdContaBancariaContaDebito}"
								size="17" maxlength="13" id="txtContaDebitoCheck">
							</br:brInputText>
						</br:brPanelGroup>
						<br:brPanelGrid columns="1" style="margin-right:5px"
							cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGroup>

						</br:brPanelGroup>
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<h:selectBooleanCheckbox id="chkNumeroPagamento"
					value="#{solRecPagamentosConsultaBean.chkNumeroPagamento}"
					disabled="#{solRecPagamentosConsultaBean.disableArgumentosConsulta}"
					style="margin-right:5px">
					<a4j:support oncomplete="javascript:foco(this);"
						status="statusAguarde" event="onclick"
						reRender="txtNumeroPagamentoDe, txtNumeroPagamentoAte"
						action="#{solRecPagamentosConsultaBean.limparNumeroPagamento}" />
				</h:selectBooleanCheckbox>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.consultarPagamentosIndividual_numero_pagamento}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"
				style="margin-top:5px;">
				<br:brPanelGroup>
					<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_de}:"
								style="margin-right:5px; margin-left:23px" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brInputText styleClass="HtmlInputTextBradesco"
								disabled="#{!solRecPagamentosConsultaBean.chkNumeroPagamento || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
								value="#{solRecPagamentosConsultaBean.numeroPagamentoDeFiltro}"
								size="40" maxlength="30" id="txtNumeroPagamentoDe"
								style="margin-right:5px" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_ate}:"
								style="margin-right:5px" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brInputText styleClass="HtmlInputTextBradesco"
								disabled="#{!solRecPagamentosConsultaBean.chkNumeroPagamento || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
								value="#{solRecPagamentosConsultaBean.numeroPagamentoAteFiltro}"
								size="40" maxlength="30" id="txtNumeroPagamentoAte" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>


			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<h:selectBooleanCheckbox id="chkValorPagamento"
					value="#{solRecPagamentosConsultaBean.chkValorPagamento}"
					disabled="#{solRecPagamentosConsultaBean.disableArgumentosConsulta}"
					style="margin-right:5px">
					<a4j:support oncomplete="javascript:foco(this);"
						status="statusAguarde" event="onclick"
						reRender="txtValorDePagamento, txtValorAtePagamento"
						action="#{solRecPagamentosConsultaBean.limparValorPagamento}" />
				</h:selectBooleanCheckbox>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.consultarPagamentosIndividual_valor_pagamento}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-top:5px;">
				<br:brPanelGroup>
					<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="text-align:left;">
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_de}:"
								style="margin-right:5px; margin-left:23px" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brInputText converter="decimalBrazillianConverter"
								alt="decimalBr" onfocus="loadMasks();"
								onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');"
								styleClass="HtmlInputTextBradesco"
								disabled="#{!solRecPagamentosConsultaBean.chkValorPagamento || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
								value="#{solRecPagamentosConsultaBean.valorPagamentoDeFiltro}"
								size="13" maxlength="9" id="txtValorDePagamento"
								style="margin-right:5px" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_ate}:"
								style="margin-right:5px" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brInputText converter="decimalBrazillianConverter"
								alt="decimalBr" onfocus="loadMasks();"
								onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');"
								styleClass="HtmlInputTextBradesco"
								disabled="#{!solRecPagamentosConsultaBean.chkValorPagamento || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
								value="#{solRecPagamentosConsultaBean.valorPagamentoAteFiltro}"
								size="13" maxlength="9" id="txtValorAtePagamento" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>




			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>



			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<t:selectOneRadio id="radioPesquisasFiltroPrincipal"
					value="#{solRecPagamentosConsultaBean.radioPesquisaFiltroPrincipal}"
					disabled="#{!solRecPagamentosConsultaBean.chkIdentificacaoRecebedor || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
					styleClass="HtmlSelectOneRadioBradesco" layout="spread"
					forceId="true" forceIdIndex="false">
					<f:selectItem itemValue="0"
						itemLabel="#{msgs.consultarPagamentosIndividual_favorecido}" />
					<f:selectItem itemValue="1"
						itemLabel="#{msgs.consultarPagamentosIndividual_remessa}" />
					<f:selectItem itemValue="2" itemLabel="#{msgs.label_beneficiario}" />
					<f:selectItem itemValue="3"
						itemLabel="#{msgs.consultarPagamentosIndividual_favorecido}" />
					<a4j:support oncomplete="javascript:foco(this);"
						status="statusAguarde" event="onclick"
						reRender="panelFavorecidoBeneficiario"
						action="#{solRecPagamentosConsultaBean.limparFavorecidoBeneficiario}" />
				</t:selectOneRadio>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<t:selectOneRadio id="radioFavorecidoFiltro"
					disabled="#{!solRecPagamentosConsultaBean.chkIdentificacaoRecebedor || solRecPagamentosConsultaBean.disableArgumentosConsulta || solRecPagamentosConsultaBean.radioPesquisaFiltroPrincipal != 3 }"
					value="#{solRecPagamentosConsultaBean.radioFavorecidoFiltro}"
					styleClass="HtmlSelectOneRadioBradesco" layout="spread"
					forceId="true" forceIdIndex="false">
					<f:selectItem itemValue="0" itemLabel="" />
					<f:selectItem itemValue="1" itemLabel="" />
					<a4j:support oncomplete="javascript:foco(this);"
						status="statusAguarde" event="onclick" reRender="panelFavorecido"
						action="#{solRecPagamentosConsultaBean.limparFavorecido}" />
				</t:selectOneRadio>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<h:selectBooleanCheckbox id="chkIdentificacaoRecebedor"
							disabled="#{solRecPagamentosConsultaBean.disableArgumentosConsulta}"
							value="#{solRecPagamentosConsultaBean.chkIdentificacaoRecebedor}"
							style="margin-right:5px">
							<a4j:support oncomplete="javascript:foco(this);"
								status="statusAguarde" event="onclick"
								reRender="panelFavorecidoBeneficiario"
								action="#{solRecPagamentosConsultaBean.limparIdentificadorRecebedor}" />
						</h:selectBooleanCheckbox>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
								value="#{msgs.incSolRecPagamentosConsulta_identificacao_recebedor}" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<a4j:outputPanel id="panelFavorecidoBeneficiario" ajaxRendered="true">
				<a4j:outputPanel id="panelFavorecido" ajaxRendered="true">
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
						style="margin-left:20px">
						<br:brPanelGroup>
							<t:radio for="radioPesquisasFiltroPrincipal" index="3" />
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
							style="margin-left:40px">
							<t:radio for="radioFavorecidoFiltro" index="0" />

							<a4j:outputPanel id="panelCodigoFavorecido" ajaxRendered="true">
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
									style="text-align:left;margin-top:6px;">
									<br:brPanelGroup>
										<br:brGraphicImage url="/images/bullet.jpg"
											styleClass="HtmlBullet" />
									</br:brPanelGroup>
									<br:brPanelGroup>
										<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
											value="#{msgs.consultarPagamentosIndividual_codigo}:" />
									</br:brPanelGroup>
								</br:brPanelGrid>

								<br:brPanelGrid columns="1" style="margin-top:5px"
									cellpadding="0" cellspacing="0">
									<br:brPanelGroup>
									</br:brPanelGroup>
								</br:brPanelGrid>

								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
									<br:brInputText id="txtCodigoFavorecido" size="20"
										disabled="#{solRecPagamentosConsultaBean.radioPesquisaFiltroPrincipal != '3' || solRecPagamentosConsultaBean.radioFavorecidoFiltro != '0' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
										maxlength="15" onkeypress="onlyNum();"
										styleClass="HtmlInputTextBradesco"
										value="#{solRecPagamentosConsultaBean.codigoFavorecidoFiltro}"
										onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
								</br:brPanelGrid>
							</a4j:outputPanel>
						</br:brPanelGrid>

						<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"
							style="margin-left:40px">
							<br:brPanelGroup>
								<t:radio for="radioFavorecidoFiltro" index="1" />
							</br:brPanelGroup>

							<br:brPanelGroup>
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
									style="text-align:left;margin-top:6px;">
									<br:brPanelGroup>
										<br:brGraphicImage url="/images/bullet.jpg"
											styleClass="HtmlBullet" />
									</br:brPanelGroup>
									<br:brPanelGroup>
										<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
											value="#{msgs.consultarPagamentosIndividual_inscricao}:" />
									</br:brPanelGroup>
								</br:brPanelGrid>

								<br:brPanelGrid columns="1" style="margin-top:5px"
									cellpadding="0" cellspacing="0">
									<br:brPanelGroup>
									</br:brPanelGroup>
								</br:brPanelGrid>

								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
									<br:brInputText id="txtInscricao"
										disabled="#{solRecPagamentosConsultaBean.radioPesquisaFiltroPrincipal != '3' || solRecPagamentosConsultaBean.radioFavorecidoFiltro != '1' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
										size="20" maxlength="15" onkeypress="onlyNum();"
										styleClass="HtmlInputTextBradesco"
										value="#{solRecPagamentosConsultaBean.inscricaoFiltro}"
										onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
										style="margin-right:20px" />
								</br:brPanelGrid>
							</br:brPanelGroup>

							<br:brPanelGroup>
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
									style="text-align:left; margin-top:6px;">
									<br:brPanelGroup>
										<br:brGraphicImage url="/images/bullet.jpg"
											styleClass="HtmlBullet" />
									</br:brPanelGroup>
									<br:brPanelGroup>
										<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
											value="#{msgs.consultarPagamentosIndividual_tipo}:" />
									</br:brPanelGroup>
								</br:brPanelGrid>

								<br:brPanelGrid columns="1" style="margin-top:5px"
									cellpadding="0" cellspacing="0">
									<br:brPanelGroup>
									</br:brPanelGroup>
								</br:brPanelGrid>

								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
									<br:brPanelGroup>
										<br:brSelectOneMenu
											disabled="#{solRecPagamentosConsultaBean.radioPesquisaFiltroPrincipal != '3' || solRecPagamentosConsultaBean.radioFavorecidoFiltro != '1' || solRecPagamentosConsultaBean.disableArgumentosConsulta}"
											id="tipoInscricao"
											value="#{solRecPagamentosConsultaBean.tipoInscricaoFiltro}"
											styleClass="HtmlSelectOneMenuBradesco">
											<f:selectItem itemValue="0"
												itemLabel="#{msgs.label_combo_selecione}" />
											<f:selectItems
												value="#{solRecPagamentosConsultaBean.listaInscricaoFavorecidoFiltro}" />
										</br:brSelectOneMenu>
									</br:brPanelGroup>
								</br:brPanelGrid>
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGrid>
				</a4j:outputPanel>

			</a4j:outputPanel>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="margin-top:6px">
				<h:selectBooleanCheckbox id="chkContaDestino"
					disabled="#{solRecPagamentosConsultaBean.disableArgumentosConsulta}"
					value="#{solRecPagamentosConsultaBean.chkinfoContas}"
					style="margin-right:5px">
					<a4j:support oncomplete="javascript:foco(this);"
						status="statusAguarde" event="onclick"
						action="#{solRecPagamentosConsultaBean.limparInformacao}"
						reRender="chkHabilitaCredito, chkHabilitaSalario, chkHabilitaPagamento" />
				</h:selectBooleanCheckbox>

				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="Informa��o Conta:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<t:selectOneRadio id="chkHabilitaCredito"
						disabled="#{ !solRecPagamentosConsultaBean.chkinfoContas}"
						value="#{solRecPagamentosConsultaBean.chkCntCredito}"
						styleClass="HtmlSelectOneRadioBradesco" forceId="true"
						forceIdIndex="false">
						<f:selectItem itemValue="1"
							itemLabel="#{msgs.label_conta_de_credito}" />
						<a4j:support oncomplete="javascript:foco(this);"
							status="statusAguarde" event="onclick"
							action="#{solRecPagamentosConsultaBean.limparIndcConta}"
							reRender="chkHabilitaCredito, chkHabilitaSalario, chkHabilitaPagamento" />
					</t:selectOneRadio>
				</br:brPanelGroup>
			</br:brPanelGrid>


			<br:brPanelGrid columns="5" cellpadding="0" style="margin-left:25px;"
				cellspacing="0" width="350px">
				<a4j:outputPanel id="panelBancoRadioFavorecido" ajaxRendered="true">

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_banco}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brInputText styleClass="HtmlInputTextBradesco"
						disabled="#{solRecPagamentosConsultaBean.chkCntCredito != 1}"
						onkeypress="onlyNum();" converter="javax.faces.Integer"
						value="#{solRecPagamentosConsultaBean.cdBancoContaCredito}"
						size="4" maxlength="3" id="txtBancoContaCredito"
						onkeyup="proximoCampo(3,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtBancoContaCredito','incSolRecPagamentosConsultaForm:txtAgenciaContaCredito');"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>

				</a4j:outputPanel>

				<a4j:outputPanel id="panelAgenciaRadioFavorecido"
					ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_agencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText
						disabled="#{solRecPagamentosConsultaBean.chkCntCredito != 1}"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						converter="javax.faces.Integer"
						value="#{solRecPagamentosConsultaBean.cdAgenciaBancariaContaCredito}"
						size="6" maxlength="5" id="txtAgenciaContaCredito"
						onkeyup="proximoCampo(5,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtAgenciaContaCredito','incSolRecPagamentosConsultaForm:txtContaContaCredito');"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>
				</a4j:outputPanel>

				<a4j:outputPanel id="panelContaRadioFavorecido" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_conta}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText
								disabled="#{solRecPagamentosConsultaBean.chkCntCredito != 1}"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"
								onkeypress="onlyNum();"
								onkeyup="proximoCampo(13,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtContaContaCredito','incSolRecPagamentosConsultaForm:txtDigitoContaCredito');"
								value="#{solRecPagamentosConsultaBean.cdContaBancariaContaCredito}"
								size="15" maxlength="13" id="txtContaContaCredito">
							</br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>

				</a4j:outputPanel>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<a4j:outputPanel id="paneldigitoRadioFavorecido" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="Digito:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText
						disabled="#{solRecPagamentosConsultaBean.chkCntCredito != 1}"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						 value="#{solRecPagamentosConsultaBean.cdDigitoContaCredito}" size="2" maxlength="1"
						id="txtDigitoContaCredito"
						onkeyup="proximoCampo(1,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtDigitoContaCredito','incSolRecPagamentosConsultaForm:txtBancoContaCredito');"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>
				</a4j:outputPanel>
			</br:brPanelGrid>


			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<t:selectOneRadio id="chkHabilitaSalario"
						value="#{solRecPagamentosConsultaBean.chkCntSalario}"
						styleClass="HtmlSelectOneRadioBradesco" forceId="true"
						disabled="#{ !solRecPagamentosConsultaBean.chkinfoContas}"
						forceIdIndex="false">
						<f:selectItem itemValue="2" itemLabel="Conta Sal�rio" />
						<a4j:support oncomplete="javascript:foco(this);"
							status="statusAguarde" event="onclick"
							action="#{solRecPagamentosConsultaBean.limparIndsalario}"
							reRender="chkHabilitaCredito, chkHabilitaSalario, chkHabilitaPagamento" />
					</t:selectOneRadio>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="5" cellpadding="0"
				style="margin-left:25px;margin-top:6px" cellspacing="0"
				width="350px">
				<a4j:outputPanel id="panelBancoRadioFavorecido2" ajaxRendered="true">

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_banco}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brInputText styleClass="HtmlInputTextBradesco"
						disabled="#{solRecPagamentosConsultaBean.chkCntSalario != 2}"
						onkeypress="onlyNum();" converter="javax.faces.Integer" 
						value="#{solRecPagamentosConsultaBean.bancoSalario}"
						size="4" maxlength="3" id="txtBancoContaSalario"
						onkeyup="proximoCampo(3,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtBancoContaSalario','incSolRecPagamentosConsultaForm:txtAgenciaContaSalario');"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>

				</a4j:outputPanel>

				<a4j:outputPanel id="panelAgenciaRadioFavorecido2"
					ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_agencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText
						disabled="#{solRecPagamentosConsultaBean.chkCntSalario != 2}"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						converter="javax.faces.Integer" 
						value="#{solRecPagamentosConsultaBean.agenciaSalario}" size="6" maxlength="5"
						id="txtAgenciaContaSalario"
						onkeyup="proximoCampo(5,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtAgenciaContaSalario','incSolRecPagamentosConsultaForm:txtContaContaSalario');"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>
				</a4j:outputPanel>

				<a4j:outputPanel id="panelContaRadioFavorecido2" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_conta}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText
								disabled="#{solRecPagamentosConsultaBean.chkCntSalario != 2}"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								styleClass="HtmlInputTextBradesco" 
								onkeyup="proximoCampo(13,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtContaContaSalario','incSolRecPagamentosConsultaForm:txtDigitoContaSalario');"
								onkeypress="onlyNum();" value="#{solRecPagamentosConsultaBean.contaSalario}" 
								size="15" maxlength="13"
								id="txtContaContaSalario">
							</br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>

				</a4j:outputPanel>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<a4j:outputPanel id="paneldigitoRadioFavorecido3"
					ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="Digito:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brInputText
						disabled="#{solRecPagamentosConsultaBean.chkCntSalario != 2}"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						value="#{solRecPagamentosConsultaBean.digitoSalario}" size="2" maxlength="1"
						id="txtDigitoContaSalario"
						onkeyup="proximoCampo(1,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtDigitoContaSalario','incSolRecPagamentosConsultaForm:txtBancoContaSalario');"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>

				</a4j:outputPanel>
			</br:brPanelGrid>


			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<t:selectOneRadio id="chkHabilitaPagamento"
						value="#{solRecPagamentosConsultaBean.chkCntPagamento}"
						styleClass="HtmlSelectOneRadioBradesco" forceId="true"
						disabled="#{ !solRecPagamentosConsultaBean.chkinfoContas}"
						forceIdIndex="false">
						<f:selectItem itemValue="3" itemLabel="Conta Pagamento" />
						<a4j:support oncomplete="javascript:foco(this);" event="onclick"
							action="#{solRecPagamentosConsultaBean.limparIndcpag}"
							reRender="chkHabilitaCredito, chkHabilitaSalario, chkHabilitaPagamento" />
					</t:selectOneRadio>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0"
				style="margin-left:25px;margin-top:6px" width="270px">
				<a4j:outputPanel id="panelBancoRadioFavorecido3" ajaxRendered="true">

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_banco}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brInputText styleClass="HtmlInputTextBradesco"
						disabled="#{  solRecPagamentosConsultaBean.chkCntPagamento != 3}"
						onkeypress="onlyNum();" 
						value="#{solRecPagamentosConsultaBean.bancoPgtoDestino}"
						size="4" maxlength="3" id="txtBancoContaPagto"
						onkeyup="proximoCampo(3,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtBancoContaPagto','incSolRecPagamentosConsultaForm:ispbContaPagto');"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>

				</a4j:outputPanel>

				<a4j:outputPanel id="panelAgenciaRadioFavorecido3"
					ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="ISPB:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText
						disabled="#{  solRecPagamentosConsultaBean.chkCntPagamento != 3}"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						 size="9" maxlength="8" onkeyup="proximoCampo(8,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:ispbContaPagto','incSolRecPagamentosConsultaForm:txtContaContaPagto');"
						id="ispbContaPagto" value="#{solRecPagamentosConsultaBean.ispbPgtoDestino}"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>
				</a4j:outputPanel>

				<a4j:outputPanel id="panelContaRadioFavorecido3" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_conta}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText
								disabled="#{ solRecPagamentosConsultaBean.chkCntPagamento != 3}"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								styleClass="HtmlInputTextBradesco" 
								onkeypress="onlyNum();" value="" size="15" maxlength="13"
								value="#{solRecPagamentosConsultaBean.contaPgtoDestino}"
								onkeyup="proximoCampo(13,'incSolRecPagamentosConsultaForm','incSolRecPagamentosConsultaForm:txtContaContaPagto','incSolRecPagamentosConsultaForm:txtBancoContaPagto');"
								id="txtContaContaPagto">
							</br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>

				</a4j:outputPanel>
			</br:brPanelGrid>



			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
								value="Destino:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<t:selectOneRadio id="radioCampoDestino1" value="#{solRecPagamentosConsultaBean.destinoPagamento}"
					styleClass="HtmlSelectOneRadioBradesco" forceId="true"
					forceIdIndex="false">
					<f:selectItem itemValue="1" itemLabel="#{msgs.relatorio}" />
					<f:selectItem itemValue="2" itemLabel="#{msgs.arquivo_ISD}" />
					<f:selectItem itemValue="3" itemLabel="#{msgs.base_online}" />
				</t:selectOneRadio>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

		</a4j:outputPanel>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<a4j:outputPanel id="panelBotoes"
			style="width: 100%; text-align: right" ajaxRendered="true">
			<br:brPanelGrid columns="3" style="text-align:right" cellpadding="0"
				cellspacing="0" border="0">
				<br:brPanelGroup>
					<br:brCommandButton id="btnLimpar" styleClass="bto1"
						style="margin-right:5px" value="#{msgs.label_limpar_campos}"
						action="#{solRecPagamentosConsultaBean.limparTelaPrincipal}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brCommandButton id="btnAvancar" styleClass="bto1"
						value="#{msgs.btn_avancar}"
						action="#{solRecPagamentosConsultaBean.avancar}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>


		<f:verbatim>
			<br>
		</f:verbatim>


		<a4j:status id="statusAguarde" onstart="bloquearTela()"
			onstop="desbloquearTela()" />

		<t:inputHidden
			value="#{solRecPagamentosConsultaBean.disableArgumentosConsulta}"
			id="hiddenFlagPesquisa"></t:inputHidden>

		<f:verbatim>
			<script language="javascript">
	validarProxCampoIdentClienteContratoFlag(
			document.forms[1],
			document
					.getElementById('incSolRecPagamentosConsultaForm:hiddenFlagPesquisa').value);
</script>
		</f:verbatim>

	</br:brPanelGrid>

	<brArq:validatorScript functionName="validateForm" />

</brArq:form>