<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="excManSolRecPagVencNaoPag" name="excManSolRecPagVencNaoPag">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.nrCnpjCpf}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.dsRazaoSocial}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.dsEmpresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.nroContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_descricaoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.dsContrato}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_situacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.cdSituacaoContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_conta_de_debito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.dsBancoDebito}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_agencia}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.dsAgenciaDebito}"/>    
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.contaDebito}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
				
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.tipoContaDebito}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_solicitacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.dsNumero}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao_solicitacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.dsSituacaoSolicitacao}"/>    
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_motivo_da_soliticacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.dsMotivoSolicitacao}"/>    
		</br:brPanelGroup>							
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_datasolicitacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.dsDataSolicitacao}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManSolRecuperacaoPagConsulta_data_programada_quitacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolRecPagVencNaoPagBean.dsDataProgramadaRecuperacao}"/>    
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 

			<app:scrollableDataTable id="dataTable" value="#{manterSolRecPagVencNaoPagBean.listaDetalhe}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
			
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_cpf_cnpj_cliente_pagador}" style="width:200; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{result.cpfCnpjFormatado}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="260px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.label_nome_razao}"  style="width:260; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{result.dsRazaoSocial}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.label_empresa_gestora_contrato}" style="width:250; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsEmpresa}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="220px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.label_numero_contrato}" style="width:220; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{result.nrSequenciaContratoNegocio}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.label_tipoServico}" style="width:200; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsOperacaoProdutoServico}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="180px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.label_modalidade}" style="width:180; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsOperacaoProdutoRelacionado}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.label_numero_pagamento}" style="width:200; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.cdControlePagamento}" />
				</app:scrollableColumn>											

				<app:scrollableColumn styleClass="colTabCenter" width="200px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.label_data_pagamento}"  style="width:200; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{result.dtCredito}" converter="datePdcConverter" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.label_valor_pagamento}" style="width:200; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.vlrEfetivacaoPagamento}" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.label_favorecido_beneficiario}" style="width:200; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.cdInscricaoFavorecido}" />
					<br:brOutputText value="-" />
					<br:brOutputText value="#{result.dsFavorecido}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.label_bco_ag_conta_debito}" style="width:200; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.contaDebito}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.label_situacaoPagamento}" style="width:200; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsSituacaoOperacaoPagamento}" />
				</app:scrollableColumn>
				
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterSolRecPagVencNaoPagBean.pesquisarDetExc}" >
			 <f:facet name="first">
				<brArq:pdcCommandButton id="primeira"
				  styleClass="bto1"
				  value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
				<brArq:pdcCommandButton id="retrocessoRapido"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
				<brArq:pdcCommandButton id="anterior"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
				<brArq:pdcCommandButton id="proxima"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
				<brArq:pdcCommandButton id="avancoRapido"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
				<brArq:pdcCommandButton id="ultima"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
						
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
   <br:brPanelGrid columns="3" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.btn_voltar}" action="#{manterSolRecPagVencNaoPagBean.voltarExcluir}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnConfirmar" styleClass="bto1"  value="#{msgs.btn_confirmar}" action="#{manterSolRecPagVencNaoPagBean.confirmarExcluir}"
			onclick="javascript: if (!confirm('Confirma Exclus�o?')) { desbloquearTela(); return false; }" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>