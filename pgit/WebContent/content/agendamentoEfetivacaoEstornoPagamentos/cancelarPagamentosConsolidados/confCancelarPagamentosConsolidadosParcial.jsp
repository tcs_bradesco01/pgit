<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="confCancelarPagamentosConsolidadosParcialForm" name="confCancelarPagamentosConsolidadosParcialForm">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{cancelarPagamentosConsolidadosBean.nrCnpjCpf}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cancelarPagamentosConsolidadosBean.dsRazaoSocial}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{cancelarPagamentosConsolidadosBean.dsEmpresa}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cancelarPagamentosConsolidadosBean.nroContrato}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{cancelarPagamentosConsolidadosBean.dsContrato}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cancelarPagamentosConsolidadosBean.cdSituacaoContrato}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim> 
		
		 <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0"> 
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_conta_debito}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_banco}:"/> 			 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{cancelarPagamentosConsolidadosBean.dsBancoDebito}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cancelarPagamentosConsolidadosBean.dsAgenciaDebito}"/>
			</br:brPanelGroup>			
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cancelarPagamentosConsolidadosBean.contaDebito}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>			
					
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cancelarPagamentosConsolidadosBean.tipoContaDebito}"/> 
			</br:brPanelGroup>			
		</br:brPanelGrid>
				
		<f:verbatim><hr class="lin"> </f:verbatim>
		
	    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0"> 
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_pagamentos}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_servico}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cancelarPagamentosConsolidadosBean.dsTipoServico}"/>
			</br:brPanelGroup>			

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_modalidade}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{cancelarPagamentosConsolidadosBean.dsIndicadorModalidade}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>			

	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_qtde_pagamentos}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{cancelarPagamentosConsolidadosBean.qtdePagamentos}" converter="inteiroMilharConverter" />
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_total_pagamentos}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cancelarPagamentosConsolidadosBean.vlTotalPagamentos}"  converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 	
		

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_dataPagamento}:"/> 			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{cancelarPagamentosConsolidadosBean.dtPagamento}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_remessa}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{cancelarPagamentosConsolidadosBean.nrRemessa}" />
		</br:brPanelGroup>			

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacaoPagamento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{cancelarPagamentosConsolidadosBean.situacaoPagamento}" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_qtde_pagamentos_serao_excluidos}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{cancelarPagamentosConsolidadosBean.qtdePagamentosAutorizados}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_pagamentos_serao_excluidos}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{cancelarPagamentosConsolidadosBean.vlTotalPagamentosAutorizados}"  converter="decimalBrazillianConverter"/> 
			</br:brPanelGroup>			
		</br:brPanelGrid>		
				
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
			
				<app:scrollableDataTable id="dataTable" value="#{cancelarPagamentosConsolidadosBean.listaGridDetCancelarPagamentosConsolidadosSelecionados}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabLeft" width="130px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_numero_pagamento}" style="width:130; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nrPagamento}" converter="spaceConverter" escape="false"/>
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabRight" width="130px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_valor_pagamento}" style="width:130; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.vlPagamento}" converter="decimalBrazillianConverter"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="240px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_favorecido_beneficiario}" style="width:240px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.favorecidoBeneficiario}" />
					</app:scrollableColumn>
										
					<app:scrollableColumn styleClass="colTabRight" width="220px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_bco_ag_conta_credito}" style="width:220; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.bancoAgenciaContaCreditoFormatado}" />
					</app:scrollableColumn>
				</app:scrollableDataTable>					
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{cancelarPagamentosConsolidadosBean.pesquisarParcialSelecionados}">
					<f:facet name="first">
						<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					</f:facet>
					<f:facet name="fastrewind">
					    <brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					</f:facet>
					<f:facet name="previous">
					    <brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					</f:facet>
					<f:facet name="fastforward">
					    <brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					</f:facet>
				</brArq:pdcDataScroller> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<f:verbatim>
			<hr class="lin">
		</f:verbatim>
		
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	 
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{cancelarPagamentosConsolidadosBean.voltarCancelarParcial}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup> 
			<br:brPanelGroup style="text-align:right;width:600px" >
				<br:brCommandButton id="btnImprimir" styleClass="bto1" value="#{msgs.btn_imprimir_lista}" action="#{cancelarPagamentosConsolidadosBean.imprimirParcial}" onclick="javascript: desbloquearTela()">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton style="margin-left:5px" id="btnConfirmar" styleClass="bto1" value="#{msgs.btn_confirmar}" action="#{cancelarPagamentosConsolidadosBean.confirmarCancelarParcial}" onclick="javascript: if (!confirm('#{msgs.label_conf_exclusao}')) { desbloquearTela(); return false; }">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>