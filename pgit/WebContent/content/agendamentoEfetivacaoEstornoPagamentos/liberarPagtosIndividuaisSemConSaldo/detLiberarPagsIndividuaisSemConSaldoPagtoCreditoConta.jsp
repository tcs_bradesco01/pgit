<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detLiberarPagsIndividuaisSemConSaldoCreditoConta" name="detLiberarPagsIndividuaisSemConSaldoCreditoConta">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.nrCnpjCpf}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsRazaoSocial}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsEmpresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.nroContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_descricaoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsContrato}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_situacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdSituacaoContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_contaDebito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsBancoDebito}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_agencia}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsAgenciaDebito}"/>    
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.contaDebito}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
				
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >							
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.tipoContaDebito}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_favorecido}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_numeroInscricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.numeroInscricaoFavorecido}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.descTipoFavorecido}"/>				
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_nome}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsFavorecido}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_codigo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdFavorecido}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsBancoOriginal}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_agencia}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsAgenciaOriginal}"/>    
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdDigitoContaOriginal}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>					
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >						
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsTipoContaOriginal}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  rendered="#{liberarPagsIndividuaisSemConSaldoBean.exibeBeneficiario}">
		<br:brPanelGroup>	
		
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_beneficiario}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_numeroInscricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.numeroInscricaoBeneficiario}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsTipoBeneficiario}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_nome}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.nomeBeneficiario}"/>
		</br:brPanelGroup>								
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
			</br:brPanelGroup>							
	</br:brPanelGrid>
	

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentosConsolidado_pagamento}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoServico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsTipoServico}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_modalidade}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsIndicadorModalidade}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_remessa}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.nrSequenciaArquivoRemessa}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_lote}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.nrLoteArquivoRemessa}"/>    
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{liberarPagsIndividuaisSemConSaldoBean.numControleInternoLote != 0}">
			    <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			    <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_lote_interno}:"/> 
			    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.numControleInternoLote}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_numeroPagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.numeroPagamento}" converter="spaceConverter" escape="false"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_listaDebito}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdListaDebito}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataAgendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dtAgendamento}"/>
			</br:brPanelGroup>							
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataVencimento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dtVencimento}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataPagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dtPagamento}"/>
			</br:brPanelGroup>							
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataDevolucaoEstorno}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dtDevolucaoEstorno}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataEmissaDocumento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dtEmissaoDocumento}"/>    
			</br:brPanelGroup>
		</br:brPanelGrid>	

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataPagamentoFloating}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dtFloatingPagamento}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataEfetivacaoFloating}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dtEfetivFloatPgto}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_quantidadeMoeda}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.qtMoeda}" converter="decimalBrazillianConverter"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorAgendado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlrAgendamento}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorEfetivado}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlrEfetivacao}" converter="decimalBrazillianConverter"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorDocumento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlDocumento}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorDesconto}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlDesconto}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorAbatimento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlAbatimento}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorMulta}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlMulta}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorMora}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlMora}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorIr}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlImpostoRenda}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorIss}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlIss}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorIof}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlIof}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorInss}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlInss}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorFloating}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlFloatingPagamento}" converter="decimalBrazillianConverter" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_outrasDeducoes}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlDeducao}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_outrosAcrescimos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.vlAcrescimo}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoMoeda}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdIndicadorEconomicoMoeda}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_situacaoPagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdSituacaoOperacaoPagamento}"/>
			</br:brPanelGroup>	
				
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
					
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_motivoSituacaoPagamento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsMotivoSituacao}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="EspacamentoLinhas"  columns="2" cellpadding="0" cellspacing="0" rendered="#{liberarPagsIndividuaisSemConSaldoBean.exibeDcom == true}">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_operacaoDCOM}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdOperacaoDcom}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_situacaoDCOM}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsSituacaoDcom}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="EspacamentoLinhas" columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_descricaoPagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsPagamento}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao_transferencia_automatica}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdSituacaoTranferenciaAutomatica}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_numeroDocumento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.nrDocumento}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoDocumento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.tipoDocumento}"/>    
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_serieDocumento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdSerieDocumento}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_usoEmpresa}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsUsoEmpresa}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_msgPrimeiraLinhaExtrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsMensagemPrimeiraLinha}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_msgSegundaLinhaExtrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsMensagemSegundaLinha}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_bancoCredito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdBancoCredito}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia_credito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.agenciaDigitoCredito}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta_credito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.contaDigitoCredito}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoContaCredito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.tipoContaCredito}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_bancoCreditoDestino}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdBancoDestino}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia_credito_destino}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.agenciaDigitoDestino}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
			

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta_credito_destino}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.contaDigitoDestino}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoContaCreditoDestino}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.tipoContaDestino}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataHoraInclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dataHoraInclusao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.usuarioInclusao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoCanal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.tipoCanalInclusao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.complementoInclusao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataHoraManutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dataHoraManutencao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.usuarioManutencao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoCanal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.tipoCanalManutencao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.complementoManutencao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.detPagamentoIndividual_btnVoltar}" action="#{liberarPagsIndividuaisSemConSaldoBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
