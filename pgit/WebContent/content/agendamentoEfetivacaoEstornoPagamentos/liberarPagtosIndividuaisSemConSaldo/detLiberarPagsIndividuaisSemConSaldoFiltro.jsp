<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="detLiberarPagtosConsolidadosSemConSaldoFiltroForm" name="detLiberarPagtosConsolidadosSemConSaldoFiltroForm">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{liberarPagsIndividuaisSemConSaldoBean.nrCnpjCpfInterm}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsRazaoSocialInterm}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{liberarPagsIndividuaisSemConSaldoBean.dsEmpresaInterm}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.nroContratoInterm}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/> 			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{liberarPagsIndividuaisSemConSaldoBean.dsContratoInterm}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.cdSituacaoContratoInterm}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
				
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		 <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0"> 
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_conta_debito}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_banco}:"/> 			 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{liberarPagsIndividuaisSemConSaldoBean.dsBancoDebitoInterm}"/>
			</br:brPanelGroup>	
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.dsAgenciaDebitoInterm}"/>
			</br:brPanelGroup>			
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.contaDebitoInterm}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" > 			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{liberarPagsIndividuaisSemConSaldoBean.tipoContaDebitoInterm}"/> 
			</br:brPanelGroup>			
		</br:brPanelGrid>		
				
		<f:verbatim>
			<hr class="lin">
		</f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
				<app:scrollableDataTable id="dataTable" value="#{liberarPagsIndividuaisSemConSaldoBean.listaGridLiberarPagsIndSelecionados}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{liberarPagsIndividuaisSemConSaldoBean.itemSelecionadoLista}">
							<f:selectItems value="#{liberarPagsIndividuaisSemConSaldoBean.listaControleRadioSelecionados}"/>
							<a4j:support event="onclick" reRender="btnDetalhar" />
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
				
					<app:scrollableColumn styleClass="colTabRight" width="180px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_cnpj_cpf_cliente_pagador}" style="width:180; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cpfCnpjFormatado}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_nome_razao}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsRazaoSocial}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_empresa_gestora_contrato}" style="width:250; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsEmpresa}" />
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabRight" width="150px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_numero_contrato}"  style="width:150; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nrContrato}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabLeft" width="230px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_tipo_de_servico}" style="width:230; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsResumoProdutoServico}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabLeft" width="230px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_modalidade}"  style="width:230; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsOperacaoProdutoServico}" />
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_numero_do_pagamento}" style="width:150; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.cdControlePagamento}" converter="spaceConverter" escape="false"/>
					</app:scrollableColumn>	
	
					<app:scrollableColumn styleClass="colTabCenter" width="170px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_data_do_pagamento}" style="width:170; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dtFormatada}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_valor_do_pagamento}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.vlEfetivoPagamento}" converter="decimalBrazillianConverter" />
					</app:scrollableColumn>		
					
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_favorecido_beneficiario}" style="width:250; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.favBeneficiarioFormatado}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabRight" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_bco_ag_conta_credito}" style="width:250; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.bancoAgenciaContaCreditoFormatado}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabRight" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_bco_ag_conta_debito}" style="width:250; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.bancoAgenciaContaDebitoFormatado}" />
					</app:scrollableColumn>									
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_situacao_pagto}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsSituacaoOperacaoPagamento}" />
					</app:scrollableColumn>			
				</app:scrollableDataTable>					
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{liberarPagsIndividuaisSemConSaldoBean.pesquisarSelecionados}">
					<f:facet name="first">
						<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					</f:facet>
					<f:facet name="fastrewind">
					    <brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					</f:facet>
					<f:facet name="previous">
					    <brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					</f:facet>
					<f:facet name="fastforward">
					    <brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					</f:facet>
				</brArq:pdcDataScroller> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<f:verbatim>
			<hr class="lin">
		</f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{liberarPagsIndividuaisSemConSaldoBean.voltarConsulta}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >
				<br:brCommandButton id="btnDetalhar" disabled="#{liberarPagsIndividuaisSemConSaldoBean.itemSelecionadoLista == null}" styleClass="bto1" value="#{msgs.btn_detalhar}" action="#{liberarPagsIndividuaisSemConSaldoBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>