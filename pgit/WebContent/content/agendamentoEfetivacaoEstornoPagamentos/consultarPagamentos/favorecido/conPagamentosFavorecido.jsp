<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conPagamentoFavorecidoForm" name="conPagamentoFavorecidoForm" >
<h:inputHidden id="hiddenFoco" value="#{consultarPagamentosFavorecidoBean.foco}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<t:selectOneRadio id="radioAgendadosPagosNaoPagos" disabled="#{consultarPagamentosFavorecidoBean.disableArgumentosConsulta}" value="#{consultarPagamentosFavorecidoBean.agendadosPagosNaoPagos}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false" >  
				<f:selectItem itemValue="1" itemLabel="#{msgs.conPagamentosFavorecido_radio_agendados}" />
				<f:selectItem itemValue="2" itemLabel="#{msgs.conPagamentosFavorecido_radio_pagos_nao_pagos}" />
			</t:selectOneRadio>	
	    </br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_data_pagamento}:" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGroup rendered="#{!consultarPagamentosFavorecidoBean.disableArgumentosConsulta}">
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataPagamentoInicial');" id="dataPagamentoInicial" value="#{consultarPagamentosFavorecidoBean.dataPagamentoInicial}" >
					</app:calendar>
				</br:brPanelGroup>
				<br:brPanelGroup rendered="#{consultarPagamentosFavorecidoBean.disableArgumentosConsulta}">
					<app:calendar id="dataPagamentoInicialDesc" value="#{consultarPagamentosFavorecidoBean.dataPagamentoInicial}" disabled="true" >
					</app:calendar>
				</br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
				<br:brPanelGroup rendered="#{!consultarPagamentosFavorecidoBean.disableArgumentosConsulta}">
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataPagamentoFinal');" id="dataPagamentoFinal" value="#{consultarPagamentosFavorecidoBean.dataPagamentoFinal}" >
	 				</app:calendar>	
				</br:brPanelGroup>	
				<br:brPanelGroup rendered="#{consultarPagamentosFavorecidoBean.disableArgumentosConsulta}">
					<app:calendar id="dataPagamentoFinalDesc" value="#{consultarPagamentosFavorecidoBean.dataPagamentoFinal}" disabled="true">
	 				</app:calendar>	
				</br:brPanelGroup>	
			</br:brPanelGroup>			    
		</br:brPanelGrid>
	</a4j:outputPanel>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" >	
		<h:selectBooleanCheckbox id="chkTipoServico" value="#{consultarPagamentosFavorecidoBean.chkTipoServico}" style="margin-bottom:18px" disabled="#{consultarPagamentosFavorecidoBean.disableArgumentosConsulta}">
			<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="cmbTipoServico,cmbModalidade" action="#{consultarPagamentosFavorecidoBean.controleServicoModalidade}"/>
		</h:selectBooleanCheckbox>										
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_tipo_servico}:" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
						<br:brSelectOneMenu id="cmbTipoServico" value="#{consultarPagamentosFavorecidoBean.cmbTipoServico}" disabled="#{!consultarPagamentosFavorecidoBean.chkTipoServico || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.conPagamentosFavorecido_label_tipo_selecione}"/>
							<f:selectItems value="#{consultarPagamentosFavorecidoBean.listaCmbTipoServico}"/>
							<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onchange" reRender="cmbModalidade" action="#{consultarPagamentosFavorecidoBean.listarCmbModalidade}"/>
						</br:brSelectOneMenu>	
					</br:brPanelGroup>
				</br:brPanelGrid>		
			</br:brPanelGrid>
		 </br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid style="margin-left:25px" columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_modalidade}:"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>	
				<br:brSelectOneMenu id="cmbModalidade" value="#{consultarPagamentosFavorecidoBean.cmbModalidade}" disabled="#{(consultarPagamentosFavorecidoBean.cmbTipoServico == 0 || consultarPagamentosFavorecidoBean.cmbTipoServico == null || !consultarPagamentosFavorecidoBean.chkTipoServico) || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}"
				styleClass="HtmlSelectOneMenuBradesco">
					<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_todos}"/>
					<f:selectItems value="#{consultarPagamentosFavorecidoBean.listaCmbModalidade}"/>
				</br:brSelectOneMenu>	
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<a4j:outputPanel id="panelRadios" style="width: 100%" ajaxRendered="true">
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="sorRadio"
				styleClass="HtmlSelectOneRadioBradesco" layout="spread"
				forceId="true" forceIdIndex="false"
				value="#{consultarPagamentosFavorecidoBean.rdoPesquisa}"
				disabled="#{consultarPagamentosFavorecidoBean.disableArgumentosConsulta}">
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<f:selectItem itemValue="3" itemLabel="" />
				<f:selectItem itemValue="4" itemLabel="" />
					<a4j:support oncomplete="javascript:foco(this);"
						status="statusAguarde" event="onclick"
						reRender="panelRadios,sorRadioFavorecido,banco,agencia,conta,contaDig,sorRadioBeneficiario"
						action="#{consultarPagamentosFavorecidoBean.limparRadioPesquisa}" />
				</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<t:radio for="sorRadio" index="0" />
			<br:brOutputText styleClass="HtmlSelectOneRadioBradesco" value="#{msgs.conPagamentosFavorecido_label_favorecido}" />
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGroup>		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<t:selectOneRadio id="sorRadioFavorecido" disabled="#{consultarPagamentosFavorecidoBean.rdoPesquisa != '0' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{consultarPagamentosFavorecidoBean.rdoFavorecido}">  
						<f:selectItem itemValue="0" itemLabel="" />
						<f:selectItem itemValue="1" itemLabel="" />	
						<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="codigoFavorecido,inscricaoFavorecido,tipoFavorecido" action="#{consultarPagamentosFavorecidoBean.limparRadioFavorecido}"/>
					</t:selectOneRadio>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid style="margin-left:20px" columns="2" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<t:radio for="sorRadioFavorecido" index="0" />					
				</br:brPanelGroup>

				<br:brPanelGroup>						
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_codigo}:" />
						</br:brPanelGroup>	
					</br:brPanelGrid>
							
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
							
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>				
							<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{consultarPagamentosFavorecidoBean.codigoFavorecido}" id="codigoFavorecido" size="20" maxlength="15" onkeypress="onlyNum();" disabled="#{consultarPagamentosFavorecidoBean.rdoFavorecido != '0' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}"/>							
						</br:brPanelGroup>
					</br:brPanelGrid>						
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid style="margin-left:20px" columns="2" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<t:radio for="sorRadioFavorecido" index="1" />					
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
								<br:brPanelGroup>
									<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_cpfcnpjoutros}:" />
								</br:brPanelGroup>	
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>
								</br:brPanelGroup>
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup style="margin-right:20px">				
									<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{consultarPagamentosFavorecidoBean.inscricaoFavorecido}" id="inscricaoFavorecido" size="20" maxlength="15" onkeypress="onlyNum();" disabled="#{consultarPagamentosFavorecidoBean.rdoFavorecido != '1' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}"/>							
								</br:brPanelGroup>
							</br:brPanelGrid>
						</br:brPanelGroup>
						
						<br:brPanelGroup>
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
								<br:brPanelGroup>
									<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_tipo}:" />
								</br:brPanelGroup>	
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>
								</br:brPanelGroup>
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>				
									<br:brSelectOneMenu id="tipoFavorecido" value="#{consultarPagamentosFavorecidoBean.cmbTipoFavorecido}" disabled="#{consultarPagamentosFavorecidoBean.rdoFavorecido != '1' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}">
										<f:selectItem itemValue="0" itemLabel="#{msgs.conPagamentosFavorecido_label_tipo_selecione}"/>
										<f:selectItems value="#{consultarPagamentosFavorecidoBean.listaCmbTipoFavorecido}"/>
									</br:brSelectOneMenu>	
								</br:brPanelGroup>
							</br:brPanelGrid>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<t:radio for="sorRadio" index="1" />
			<br:brOutputText styleClass="HtmlSelectOneRadioBradesco" value="#{msgs.conPagamentosFavorecido_label_conta_credito}" />
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid style="margin-left:20px" columns="5" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_banco}:" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup style="margin-right:20px">				
						<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{consultarPagamentosFavorecidoBean.banco}" id="banco" size="4" maxlength="3" onkeypress="onlyNum();" disabled="#{consultarPagamentosFavorecidoBean.rdoPesquisa != '1' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}" onkeyup="proximoCampo(3,'conPagamentoFavorecidoForm','conPagamentoFavorecidoForm:banco','conPagamentoFavorecidoForm:agencia');">
						</br:brInputText>					
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_agencia}:" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup style="margin-right:20px">				
						<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{consultarPagamentosFavorecidoBean.agencia}" id="agencia" size="6" maxlength="5" onkeypress="onlyNum();" disabled="#{consultarPagamentosFavorecidoBean.rdoPesquisa != '1' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}" onkeyup="proximoCampo(5,'conPagamentoFavorecidoForm','conPagamentoFavorecidoForm:agencia','conPagamentoFavorecidoForm:conta');">

						</br:brInputText>							
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_conta}:" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup style="margin-right:20px">				
						<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{consultarPagamentosFavorecidoBean.conta}" id="conta" size="17" maxlength="13" onkeypress="onlyNum();" disabled="#{consultarPagamentosFavorecidoBean.rdoPesquisa != '1' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}" >
						
						</br:brInputText>						

					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<t:radio for="sorRadio" index="2" />
			<br:brOutputText styleClass="HtmlSelectOneRadioBradesco" value="#{msgs.label_conta_salario}" />
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid style="margin-left:20px" columns="5" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.conPagamentosFavorecido_label_banco}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup style="margin-right:20px">
						<br:brInputText
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							styleClass="HtmlInputTextBradesco"
							value="#{consultarPagamentosFavorecidoBean.bancoSalario}" id="bancoSalario"
							size="4" maxlength="3" onkeypress="onlyNum();"
							disabled="#{consultarPagamentosFavorecidoBean.rdoPesquisa != '2' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}"
							onkeyup="proximoCampo(3,'conPagamentoFavorecidoForm','conPagamentoFavorecidoForm:bancoSalario','conPagamentoFavorecidoForm:agenciaSalario');" />
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_agencia}:" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup style="margin-right:20px">
						<br:brInputText
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							styleClass="HtmlInputTextBradesco"
							value="#{consultarPagamentosFavorecidoBean.agenciaSalario}"
							id="agenciaSalario" size="6" maxlength="5" onkeypress="onlyNum();"
							disabled="#{consultarPagamentosFavorecidoBean.rdoPesquisa != '2' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}"
							onkeyup="proximoCampo(5,'conPagamentoFavorecidoForm','conPagamentoFavorecidoForm:agenciaSalario','conPagamentoFavorecidoForm:contaSalario');"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_conta}:" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup style="margin-right:20px">
						<br:brInputText
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							styleClass="HtmlInputTextBradesco"
							value="#{consultarPagamentosFavorecidoBean.contaSalario}" id="contaSalario"
							size="17" maxlength="13" onkeypress="onlyNum();"
							disabled="#{consultarPagamentosFavorecidoBean.rdoPesquisa != '2' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}" />
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<t:radio for="sorRadio" index="3" />
			<br:brOutputText styleClass="HtmlSelectOneRadioBradesco" value="#{msgs.label_conta_pagamento}" />
		</br:brPanelGroup>			
		    
		    <br:brPanelGroup>		    
				<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:25px;margin-top:6px">		
					<a4j:outputPanel id="panelBancoPagamentoRadioFavorecido" ajaxRendered="true">
						
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>

						<br:brInputText styleClass="HtmlInputTextBradesco"
							disabled="#{consultarPagamentosFavorecidoBean.rdoPesquisa != '3' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}"
							onkeypress="onlyNum();"
							value="#{consultarPagamentosFavorecidoBean.cdBancoPagamento}"
							size="4" maxlength="3" id="txtBancoPagamento"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
						</br:brInputText>

					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelIspb" ajaxRendered="true" style="margin-right:15px;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />		
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_ispb}:"/>
						</br:brPanelGroup>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>

						<br:brInputText	
						disabled="#{consultarPagamentosFavorecidoBean.rdoPesquisa != '3' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}"
							styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						value="#{consultarPagamentosFavorecidoBean.ispb}"
						size="10" maxlength="8" id="txtISPB"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelContaPagamentoRadioFavorecido" ajaxRendered="true">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
						</br:brPanelGroup>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
					    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						    <br:brPanelGroup>
								<br:brInputText
									disabled="#{consultarPagamentosFavorecidoBean.rdoPesquisa != '3' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}"
									onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
									styleClass="HtmlInputTextBradesco"
									onkeypress="onlyNum();"
									value="#{consultarPagamentosFavorecidoBean.cdContaPagamento}"
									size="25" maxlength="20" id="txtContaPagamento"/>
							</br:brPanelGroup>						
						</br:brPanelGrid>	
					</a4j:outputPanel>
				</br:brPanelGrid>	
		    </br:brPanelGroup>							
		</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<t:radio for="sorRadio" index="4" />
			<br:brOutputText styleClass="HtmlSelectOneRadioBradesco" value="#{msgs.conPagamentosFavorecido_label_beneficiario}" />
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<t:selectOneRadio id="sorRadioBeneficiario" 
					disabled="#{consultarPagamentosFavorecidoBean.rdoPesquisa != '4' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{consultarPagamentosFavorecidoBean.rdoBeneficiario}">  
					<f:selectItem itemValue="0" itemLabel="" />
					<f:selectItem itemValue="1" itemLabel="" />	
					<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="codigoBeneficiario,tipoBeneficiario,cmbOrgaoPagador" action="#{consultarPagamentosFavorecidoBean.limparRadioBeneficiario}"/>
				</t:selectOneRadio>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid style="margin-left:20px" columns="2" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<t:radio for="sorRadioBeneficiario" index="0" />					
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>						
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_codigo}:" />
						</br:brPanelGroup>	
					</br:brPanelGrid>
							
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
							
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup style="margin-right:20px">				
							<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{consultarPagamentosFavorecidoBean.codigoBeneficiario}" id="codigoBeneficiario" size="20" maxlength="15" onkeypress="onlyNum();" disabled="#{consultarPagamentosFavorecidoBean.rdoBeneficiario != '0' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}"/>							
						</br:brPanelGroup>
					</br:brPanelGrid>						
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_tipo}:" />
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>	
							<br:brSelectOneMenu id="tipoBeneficiario" value="#{consultarPagamentosFavorecidoBean.cmbTipoBenefeciario}" 
								disabled="#{consultarPagamentosFavorecidoBean.rdoBeneficiario != '0' || 
									consultarPagamentosFavorecidoBean.disableArgumentosConsulta}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.conPagamentosFavorecido_label_tipo_selecione}"/>
								<f:selectItem itemValue="1" itemLabel="#{msgs.conPagamentosFavorecido_nit}"/>
								<f:selectItem itemValue="2" itemLabel="#{msgs.conPagamentosFavorecido_nib}"/>	
							</br:brSelectOneMenu>	
						</br:brPanelGroup>
					</br:brPanelGrid>	
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid style="margin-left:20px" columns="2" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<t:radio for="sorRadioBeneficiario" index="1" />					
			</br:brPanelGroup>
			
			<br:brPanelGroup>			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_orgao_pagador}:" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
						<br:brSelectOneMenu id="cmbOrgaoPagador" value="#{consultarPagamentosFavorecidoBean.cmbOrgaoPagador}" 
							disabled="#{consultarPagamentosFavorecidoBean.rdoBeneficiario != '1' || consultarPagamentosFavorecidoBean.disableArgumentosConsulta}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.conPagamentosFavorecido_label_tipo_selecione}"/>
							<f:selectItems value="#{consultarPagamentosFavorecidoBean.listaCmbOrgaoPagador}"/>
						</br:brSelectOneMenu>	
					</br:brPanelGroup>
				</br:brPanelGrid>	
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</br:brPanelGrid>
</a4j:outputPanel>	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btnLimpar2" style="margin-right:5px" disabled="false" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{consultarPagamentosFavorecidoBean.limparPesquisa}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" disabled="#{consultarPagamentosFavorecidoBean.disableArgumentosConsulta}" styleClass="bto1" value="#{msgs.conPagamentosFavorecido_btn_consultar}" action="#{consultarPagamentosFavorecidoBean.consultar}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
				onclick="javascript:desbloquearTela(); return validaCamposConsulta(document.forms[1],'#{msgs.label_ocampo}',
				 '#{msgs.label_necessario}',
				 '#{msgs.conPagamentosFavorecido_radio_validacao}',
				 '#{msgs.conPagamentosFavorecido_label_data_pagamento}',
				 '#{msgs.conPagamentosFavorecido_label_tipo_servico}',
				 '#{msgs.conPagamentosFavorecido_label_modalidade}',
				 '#{msgs.conPagamentosFavorecido_label_codigo}',
				 '#{msgs.conPagamentosFavorecido_label_inscricao}',
				 '#{msgs.conPagamentosFavorecido_label_tipo}',
				 '#{msgs.conPagamentosFavorecido_label_selecione}',
				 '#{msgs.conPagamentosFavorecido_label_banco}',
				 '#{msgs.conPagamentosFavorecido_label_agencia}',
				 '#{msgs.conPagamentosFavorecido_label_conta}',
				 '#{msgs.conPagamentosFavorecido_label_contadig}',
				 '#{msgs.conPagamentosFavorecido_label_codigo}',
				 '#{msgs.conPagamentosFavorecido_label_tipo}',
				 '#{msgs.conPagamentosFavorecido_label_orgao_pagador}',
				 '#{msgs.conPagamentosFavorecido_label_favorecido}',
				 '#{msgs.conPagamentosFavorecido_label_beneficiario}',
				 '#{msgs.consultarPagamentosIndividual_selecione_uma_opcao_consulta_argumento}',
				 '#{msgs.conPagamentosFavorecido_label_conta_credito}',
				 '#{msgs.consultarPagamentosIndividual_ou}', 
				 '#{msgs.label_tipo_conta}',
				 '#{msgs.label_deve_ser_diferente_de_zeros}',
				 '#{msgs.label_conta_salario}');">		
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>  	
	
	<f:verbatim><br> </f:verbatim>
	
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">

			<app:scrollableDataTable id="dataTable" value="#{consultarPagamentosFavorecidoBean.listaGridPagamentosFavorecido}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{consultarPagamentosFavorecidoBean.itemSelecionadoLista}">
						<f:selectItems value="#{consultarPagamentosFavorecidoBean.listaControlePagamentosFavorecido}"/>
						<a4j:support event="onclick" reRender="panelBotoes" action="#{consultarPagamentosFavorecidoBean.habilitaBotoes}"/>
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conPagamentosFavorecido_grid_cnpj_cpf_cliente_pagador}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cpfCnpjFormatado}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_nome_razao_social}"  style="width:200; text-align:center"/>
				    </f:facet>
			    	<br:brOutputText value="#{result.nmRazaoSocial}" />				    
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_empresa_gestora_contrato}" style="width:250; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsEmpresa}" />				    
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_numero_contrato}"  style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrContrato}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_tipo_servico}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdResumoProdutoServico}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_modalidade}"  style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsOperacaoProdutoServico}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_numero_pagamento}" style="width:150; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdControlePagamento}" converter="spaceConverter" escape="false"/>
				</app:scrollableColumn>	

				<app:scrollableColumn styleClass="colTabCenter" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_data_pagamento}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dtFormatada}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_valor_pagamento}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.vlPagamentoFormatado}" />
				</app:scrollableColumn>		
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_favorecido_beneficiario}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsAbrevFavorecido}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_bco_ag_conta_salario}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.bancoAgenciaContaSalarioFormatado}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="300px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_conta_de_credito_pagamento}" style="width:300; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.bancoAgenciaContaCreditoFormatado}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_bco_ag_conta_debito}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.bancoAgenciaContaDebitoFormatado}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_ambiente}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsIndicadorPagamento}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_situacao_pagamento}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdMotivoSituacaoPagamento}" />
				</app:scrollableColumn>					
			</app:scrollableDataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{consultarPagamentosFavorecidoBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right"
			ajaxRendered="true">
			<br:brPanelGrid style="text-align:right;" columns="6"
				cellpadding="0" cellspacing="0">

				<br:brCommandButton id="btnLimpar" style="margin-right:5px"
					disabled="#{consultarPagamentosFavorecidoBean.listaGridPagamentosFavorecido == null}"
					styleClass="bto1"
					value="#{msgs.conPagamentosFavorecido_btn_limpar}"
					action="#{consultarPagamentosFavorecidoBean.limparGrid}">
					<brArq:submitCheckClient />
				</br:brCommandButton>

				<br:brCommandButton id="btnDetalhar" style="margin-right:5px"
					disabled="#{empty consultarPagamentosFavorecidoBean.itemSelecionadoLista}"
					styleClass="bto1"
					value="#{msgs.conPagamentosFavorecido_btn_detalhar}"
					action="#{consultarPagamentosFavorecidoBean.detalhar}">
					<brArq:submitCheckClient />
				</br:brCommandButton>

				<br:brCommandButton id="btnConsultaManutencao"
					style="margin-right:5px"
					disabled="#{empty consultarPagamentosFavorecidoBean.itemSelecionadoLista}"
					styleClass="bto1"
					value="#{msgs.conPagamentosFavorecido_btn_consultar_manutencao}"
					action="#{consultarPagamentosFavorecidoBean.consultarManutencao}">
					<brArq:submitCheckClient />
				</br:brCommandButton>

				<br:brCommandButton id="btnConsultarAutorizantes" styleClass="bto1"
					style="margin-right:5px"
					value="#{msgs.btn_consultar_autorizantes_net_empresa}"
					action="#{consultarPagamentosFavorecidoBean.consultarAutorizantes}"
					disabled="#{consultarPagamentosFavorecidoBean.habilitaBotaoConsultarAutorizantes}">
					<brArq:submitCheckClient />
				</br:brCommandButton>

				<br:brCommandButton id="btnHistoricoConsultaSaldo"
					style="margin-right:5px"
					disabled="#{empty consultarPagamentosFavorecidoBean.itemSelecionadoLista}"
					styleClass="bto1"
					value="#{msgs.conPagamentosFavorecido_btn_historico_consulta_saldo}"
					action="#{consultarPagamentosFavorecidoBean.historicoConsultaSaldo}">
					<brArq:submitCheckClient />
				</br:brCommandButton>

				<br:brCommandButton id="btnImprimir"
					disabled="#{empty consultarPagamentosFavorecidoBean.listaGridPagamentosFavorecido}"
					styleClass="bto1" value="#{msgs.btn_imprimir_lista}"
					onclick="javascript:desbloquearTela()"
					action="#{consultarPagamentosFavorecidoBean.imprimir}">
					<brArq:submitCheckClient />
				</br:brCommandButton>

			</br:brPanelGrid>
		</a4j:outputPanel>

		<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/> 
		
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>