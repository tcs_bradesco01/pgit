<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detPagamentosConsolidadoFiltro" name="detPagamentosConsolidadoFiltro">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.nrCnpjCpf}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsRazaoSocial}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsEmpresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_numeroContrato}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.nroContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_descricaoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsContrato}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_situacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.cdSituacaoContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentosConsolidado_contaDebito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsBancoDebito}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_agencia}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsAgenciaDebito}"/>    
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.contaDebito}"/>
		</br:brPanelGroup>						
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.tipoContaDebito}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentosConsolidado_pagamentos}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_tipoServico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsTipoServico}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_modalidade}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsIndicadorModalidade}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_qtdePagamentos}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsQtdePagamentos}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_valorTotalPagamentos}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsVlrTotalPagamentos}" converter="decimalBrazillianConverter"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_dataPagamento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dtPagamentoFiltro}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_remessa}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.nrRemessaFiltro}" />    
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao_pagto}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsSituacaoPagamentoFiltro}" />    
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
			<app:scrollableDataTable id="dataTable" value="#{consultarPagamentosConsolidadoBean.listaGridDetPagamentosConsolidado}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{consultarPagamentosConsolidadoBean.itemSelecionadoListaDetPagamentosConsolidado}">
						<f:selectItems value="#{consultarPagamentosConsolidadoBean.listaRadiosDetPagamentosConsolidado}"/>
						<a4j:support event="onclick" reRender="botoes" action="#{consultarPagamentosConsolidadoBean.habilitaBotoes}"/>
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabLef" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.detPagamentosConsolidado_grid_numeroPagamento}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrPagamento}" converter="spaceConverter" escape="false"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.detPagamentosConsolidado_grid_valorPagamento}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.vlPagamento}" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLef" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.detPagamentosConsolidado_grid_favorecidoBeneficiario}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.favorecidoBeneficiarioFormatado}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="300px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_conta_de_credito_pagamento}" style="width:230px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.bancoAgenciaContaCreditoFormatado}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdLoteInterno}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="180px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_indicadorAutorizacao}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsIndicadorAutorizacao}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="180px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_ambiente}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsIndicadorPagamento}" />
				</app:scrollableColumn>
				
			</app:scrollableDataTable>			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{consultarPagamentosConsolidadoBean.pesquisarDetalhar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" id="botoes">
		<br:brPanelGrid style="text-align:left;" cellpadding="0" cellspacing="0">
			<br:brCommandButton id="btnVoltar" styleClass="bto1"
				value="#{msgs.btn_voltar}"
					action="#{consultarPagamentosConsolidadoBean.voltar}">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGrid>
		<br:brPanelGroup style="width: 100%; text-align:right;">
			<br:brPanelGrid cellpadding="0" cellspacing="0" columns="5">
				<br:brCommandButton id="btnDetalhar"
					disabled="#{consultarPagamentosConsolidadoBean.itemSelecionadoListaDetPagamentosConsolidado == null}"
					styleClass="bto1" style="margin-left:5px"
					value="#{msgs.detPagamentosConsolidado_btn_detalhar}"
					action="#{consultarPagamentosConsolidadoBean.detalhar}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultarManutencao"
					disabled="#{consultarPagamentosConsolidadoBean.itemSelecionadoListaDetPagamentosConsolidado == null}"
					styleClass="bto1" style="margin-left:5px;"
					value="#{msgs.consultarPagamentosIndividual_btn_consultar_manutencao}"
					action="#{consultarPagamentosConsolidadoBean.consultarManutencao}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultarAutorizantes" styleClass="bto1"
					disabled="#{consultarPagamentosConsolidadoBean.habilitaBotaoConsultarAutorizantes}"
					style="margin-left:5px"
					value="#{msgs.btn_consultar_autorizantes_net_empresa}"
					action="#{consultarPagamentosConsolidadoBean.consultarAutorizantes}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnHistoricoConsultaSaldo"
					disabled="#{consultarPagamentosConsolidadoBean.itemSelecionadoListaDetPagamentosConsolidado == null}"
						styleClass="bto1" style="margin-left:5px"
					value="#{msgs.consultarPagamentosIndividual_btn_historico_consulta_saldo}"
					action="#{consultarPagamentosConsolidadoBean.historicoConsultaSaldo}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnImprimir" styleClass="bto1"
						value="#{msgs.btn_imprimir_lista}" style="margin-left:5px"
					action="#{consultarPagamentosConsolidadoBean.imprimirDetalhe}"
					onclick="javascript:desbloquearTela();">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>