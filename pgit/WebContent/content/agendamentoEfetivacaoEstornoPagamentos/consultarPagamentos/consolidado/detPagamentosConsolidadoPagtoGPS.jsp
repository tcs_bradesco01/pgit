<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detPagamentosConsolidadoPagtoGPS" name="detPagamentosConsolidadoPagtoGPS">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentosConsolidado_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.nrCnpjCpf}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsRazaoSocial}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsEmpresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.nroContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_descricaoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsContrato}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_situacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.cdSituacaoContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid rendered="#{consultarPagamentosConsolidadoBean.manutencao}" columns="1" cellpadding="0" cellspacing="0" width="100%" >
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoManutencao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsTipoManutencao}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		<f:verbatim><hr class="lin"> </f:verbatim>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentosConsolidado_contaDebito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsBancoDebito}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_agencia}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsAgenciaDebito}"/>    
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.contaDebito}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.tipoContaDebito}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_favorecido}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_numeroInscricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.numeroInscricaoFavorecido}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.descTipoFavorecido}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_nome}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsFavorecido}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_codigo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.cdFavorecido}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsBancoFavorecido}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_agencia}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsAgenciaFavorecido}"/>    
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsContaFavorecido}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsTipoContaFavorecido}"/>    
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_pagamento}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoServico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsTipoServico}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_modalidade}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsIndicadorModalidade}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_remessa}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.nrSequenciaArquivoRemessa}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_lote}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.nrLoteArquivoRemessa}"/>    
			</br:brPanelGroup>	
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.numControleInternoLote}"/>    
			</br:brPanelGroup>
</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_numeroPagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.numeroPagamento}" converter="spaceConverter" escape="false"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_listaDebito}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.cdListaDebito}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataAgendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dtAgendamento}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataVencimento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dtVencimento}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataPagamento}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dtPagamento}" converter="datePdcConverter"/>    
			</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataDevolucaoEstorno}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dtDevolucaoEstorno}" converter="datePdcConverter"/>    
		</br:brPanelGroup>	
	</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataCompetencia}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dataCompetencia}"/>    
			</br:brPanelGroup>	
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataPagamentoFloating}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dtFloatingPagamento}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dataEfetivacaoFloating}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dtEfetivFloatPgto}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_quantidadeMoeda}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.qtMoeda}" converter="decimalBrazillianConverter"/>    
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorAgendado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.vlrAgendamento}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorEfetivado}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.vlrEfetivacao}" converter="decimalBrazillianConverter"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorPrincipal}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.vlPrincipal}" converter="decimalBrazillianConverter"/>    
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_vlOutrasEntidades}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.vlOutrasEntidades}" converter="decimalBrazillianConverter"/>    
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorAbatimento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.vlAbatimento}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorMulta}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.vlMulta}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorMora}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.vlMora}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorTotal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.vlTotal}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_valorFloating}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.vlFloatingPagamento}" converter="decimalBrazillianConverter" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_tipoMoeda}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.cdIndicadorEconomicoMoeda}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_situacaoPagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.cdSituacaoOperacaoPagamento}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_motivoSituacaoPagamento}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsMotivoSituacao}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_indicadorAutorizacao}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsIndicadorAutorizacao}"/>    
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_operacaoDCOM}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.cdOperacaoDcom}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_situacaoDCOM}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsSituacaoDcom}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_descricaoPagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsPagamento}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_usoEmpresa}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsUsoEmpresa}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_msgPrimeiraLinhaExtrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsMensagemPrimeiraLinha}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_msgSegundaLinhaExtrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dsMensagemSegundaLinha}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_dddContribuinte}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dddContribuinte}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentoIndividual_telefoneContribuinte}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.telefoneContribuinte}"/>    
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo_do_pagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.codigoInss}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_dataHoraInclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dataHoraInclusao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.usuarioInclusao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_tipoCanal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.tipoCanalInclusao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.complementoInclusao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_dataHoraManutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.dataHoraManutencao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.usuarioManutencao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_tipoCanal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.tipoCanalManutencao}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultarPagamentosConsolidadoBean.complementoManutencao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{consultarPagamentosConsolidadoBean.voltarDetalhe}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>