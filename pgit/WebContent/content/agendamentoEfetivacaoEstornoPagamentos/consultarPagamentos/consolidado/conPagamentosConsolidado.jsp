<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conPagamentosConsolidadoForm"
	name="conPagamentosConsolidadoForm">
	<h:inputHidden id="hiddenFoco"
		value="#{consultarPagamentosConsolidadoBean.foco}" />

	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"
		cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<t:selectOneRadio id="radioTipoFiltro"
				value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado}"
				styleClass="HtmlSelectOneRadioBradesco" layout="spread"
				forceId="true" forceIdIndex="false"
				disabled="#{consultarPagamentosConsolidadoBean.disableArgumentosConsulta}">
				<f:selectItem itemValue="0"
					itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_cliente}" />
				<f:selectItem itemValue="1"
					itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_contrato}" />
				<f:selectItem itemValue="2"
					itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_conta_debito}" />
				<a4j:support oncomplete="javascript:foco(this);" event="onclick"
					reRender="formulario,dataTable,panelBotoes,dataTable,dataScroller"
					action="#{consultarPagamentosConsolidadoBean.limparTelaPrincipal}"
					status="statusAguarde" />
			</t:selectOneRadio>
		</br:brPanelGrid>

		<a4j:outputPanel id="formulario" style="width: 100%; text-align: left"
			ajaxRendered="true">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
				style="text-align:left;">
				<br:brPanelGroup>
					<t:radio for="radioTipoFiltro" index="0" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%"
				style="margin-top:6px;margin-left:20px">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.consultarPagamentosIndividual_label_identificacao_cliente}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<a4j:region id="regionFiltro">
				<t:selectOneRadio id="rdoFiltroCliente"
					value="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado}"
					onclick="submit();" styleClass="HtmlSelectOneRadioBradesco"
					layout="spread" forceId="true" forceIdIndex="false"
					disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '0' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}">
					<f:selectItem itemValue="0" itemLabel="" />
					<f:selectItem itemValue="1" itemLabel="" />
					<f:selectItem itemValue="2" itemLabel="" />
					<f:selectItem itemValue="3" itemLabel="" />
				</t:selectOneRadio>
			</a4j:region>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="0" />

				<a4j:outputPanel id="panelCnpj" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_cnpj}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
						<br:brInputText id="txtCnpj"
							onkeyup="proximoCampo(9,'conPagamentosConsolidadoForm','conPagamentosConsolidadoForm:txtCnpj','conPagamentosConsolidadoForm:txtFilial');"
							style="margin-right: 5" converter="javax.faces.Long"
							onkeypress="onlyNum();" size="11" maxlength="9"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
							styleClass="HtmlInputTextBradesco"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
						<br:brInputText id="txtFilial"
							onkeyup="proximoCampo(4,'conPagamentosConsolidadoForm','conPagamentosConsolidadoForm:txtFilial','conPagamentosConsolidadoForm:txtControle');"
							style="margin-right: 5" converter="javax.faces.Integer"
							onkeypress="onlyNum();" size="5" maxlength="4"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
							styleClass="HtmlInputTextBradesco"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
						<br:brInputText id="txtControle" style="margin-right: 5"
							converter="javax.faces.Integer" onkeypress="onlyNum();" size="3"
							maxlength="2"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
							styleClass="HtmlInputTextBradesco"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCnpj}"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="1" />

				<a4j:outputPanel id="panelCpf" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_cpf}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
						<br:brInputText styleClass="HtmlInputTextBradesco"
							style="margin-right: 5" converter="javax.faces.Long"
							onkeypress="onlyNum();"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpf}"
							size="11" maxlength="9" id="txtCpf"
							onkeyup="proximoCampo(9,'conPagamentosConsolidadoForm','conPagamentosConsolidadoForm:txtCpf','conPagamentosConsolidadoForm:txtControleCpf');"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
						<br:brInputText styleClass="HtmlInputTextBradesco"
							converter="javax.faces.Integer" onkeypress="onlyNum();"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCpf}"
							maxlength="2" size="3" id="txtControleCpf"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="2" />

				<a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brInputText styleClass="HtmlInputTextBradesco"
						disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '2' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}"
						size="71" maxlength="70" id="txtNomeRazaoSocial" />
				</a4j:outputPanel>

			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="3" />

				<a4j:outputPanel id="panelBanco" ajaxRendered="true">

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_banco}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						converter="javax.faces.Integer"
						disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdBanco}"
						size="4" maxlength="3" id="txtBanco"
						onkeyup="proximoCampo(3,'conPagamentosConsolidadoForm','conPagamentosConsolidadoForm:txtBanco','conPagamentosConsolidadoForm:txtAgencia');" />
				</a4j:outputPanel>

				<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_agencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						converter="javax.faces.Integer"
						disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}"
						size="6" maxlength="5" id="txtAgencia"
						onkeyup="proximoCampo(5,'conPagamentosConsolidadoForm','conPagamentosConsolidadoForm:txtAgencia','conPagamentosConsolidadoForm:txtConta');" />

				</a4j:outputPanel>

				<a4j:outputPanel id="panelConta" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_conta}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText styleClass="HtmlInputTextBradesco"
								converter="javax.faces.Long"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								onkeypress="onlyNum();"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdContaBancaria}"
								size="17" maxlength="13" id="txtConta" style="margin-right:5px" />
						</br:brPanelGroup>
						<br:brPanelGroup>

						</br:brPanelGroup>
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" width="100%" cellpadding="0"
				cellspacing="0">
				<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true"
					style="width:100%; text-align: right">
					<br:brCommandButton id="btoConsultarCliente"
						actionListener="#{consultarPagamentosConsolidadoBean.limparArgsListaAposPesquisaClienteContrato}"
						styleClass="bto1"
						disabled="#{empty filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
						value="#{msgs.consultarPagamentosIndividual_btn_consultar_cliente}"
						action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarClientes}"
						style="cursor:hand;"
						onmouseover="javascript:alteraBotao('visualizacao', this.id);"
						onmouseout="javascript:alteraBotao('normal', this.id);"
						onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_banco}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</a4j:outputPanel>
			</br:brPanelGrid>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
				value="#{msgs.consultarPagamentosIndividual_label_cliente}:" />
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"
						style="margin-right: 5" />
					<br:brOutputText styleClass="HtmlOutputTextBradesco"
						value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}" />
				</br:brPanelGroup>

				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"
						style="margin-right: 5" />
					<br:brOutputText styleClass="HtmlOutputTextBradesco"
						value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%" style="margin-top: 9">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.consultarPagamentosIndividual_title_contrato}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescCliente}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_numero_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescCliente}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescCliente}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_situacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescCliente}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
				style="text-align:left;">
				<br:brPanelGroup>
					<t:radio for="radioTipoFiltro" index="1" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%"
				style="margin-top:6px;margin-left:20px">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.consultarPagamentosIndividual_label_identificacao_contrato}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0"
				style="margin-left:20px">

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_empresa_gestora_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="empresaGestora"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraFiltro}"
								styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
								<f:selectItems
									value="#{filtroAgendamentoEfetivacaoEstornoBean.listaEmpresaGestora}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_tipo_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="tipoContrato"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1'  || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoContratoFiltro}"
								styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
								<f:selectItems
									value="#{filtroAgendamentoEfetivacaoEstornoBean.listaTipoContrato}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_numero_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								id="numero" styleClass="HtmlInputTextBradesco"
								onkeypress="onlyNum();"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroFiltro}"
								size="12" maxlength="10"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1'  || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"></br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" width="100%" cellpadding="0"
				cellspacing="0" style="text-align:right;">
				<br:brCommandButton id="btoConsultarContrato"
					actionListener="#{consultarPagamentosConsolidadoBean.limparArgsListaAposPesquisaClienteContrato}"
					disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
					styleClass="bto1"
					value="#{msgs.consultarPagamentosIndividual_btn_consultar_contrato}"
					action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarContrato}"
					style="cursor:hand;"
					onmouseover="javascript:alteraBotao('visualizacao', this.id);"
					onmouseout="javascript:alteraBotao('normal', this.id);"
					onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.consultarPagamentosIndividual_tipo_contrato}','#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.consultarPagamentosIndividual_title_contrato}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_numero_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_situacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
				style="text-align:left;">
				<br:brPanelGroup>
					<t:radio for="radioTipoFiltro" index="2" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%"
				style="margin-top:6px;margin-left:20px">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.consultarPagamentosIndividual_title_identificacao_conta_debito}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<a4j:outputPanel id="panelBancoContaDebito" ajaxRendered="true">

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_banco_conta_debito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText
						onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',1)"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
						converter="javax.faces.Integer"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.bancoContaDebitoFiltro}"
						size="4" maxlength="3" id="txtBancoContaDebito"
						onkeyup="proximoCampo(3,'conPagamentosConsolidadoForm','conPagamentosConsolidadoForm:txtBancoContaDebito','conPagamentosConsolidadoForm:txtAgenciaContaDebito');">
						<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);"
							status="statusAguarde" event="onblur"
							reRender="btnConsultar,painelArgumentos"
							action="#{consultarPagamentosConsolidadoBean.controlarArgumentosPesquisa}" />
					</br:brInputText>

				</a4j:outputPanel>

				<a4j:outputPanel id="panelAgenciaContaDebito" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_agencia_conta_debito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText
						onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',2)"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' ||consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
						converter="javax.faces.Integer"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.agenciaContaDebitoBancariaFiltro}"
						size="6" maxlength="5" id="txtAgenciaContaDebito"
						onkeyup="proximoCampo(5,'conPagamentosConsolidadoForm','conPagamentosConsolidadoForm:txtAgenciaContaDebito','conPagamentosConsolidadoForm:txtContaContaDebito');">
						<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);"
							status="statusAguarde" event="onblur"
							reRender="btnConsultar,painelArgumentos"
							action="#{consultarPagamentosConsolidadoBean.controlarArgumentosPesquisa}" />
					</br:brInputText>

				</a4j:outputPanel>

				<a4j:outputPanel id="panelContaContaDebito" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_conta}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText
								onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',3)"
								styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
								onkeypress="onlyNum();"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.contaBancariaContaDebitoFiltro}"
								size="17" maxlength="13" id="txtContaContaDebito">
								<a4j:support
									oncomplete="javascript:focoBlur(document.forms[1]);"
									status="statusAguarde" event="onblur"
									reRender="btnConsultar,painelArgumentos"
									action="#{consultarPagamentosConsolidadoBean.controlarArgumentosPesquisa}" />
							</br:brInputText>
						</br:brPanelGroup>
						<br:brPanelGrid columns="1" style="margin-right:5px"
							cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGroup>

						</br:brPanelGroup>
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.conPagamentosConsolidado_argumentos_pesquisa}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<a4j:outputPanel id="painelArgumentos"
				style="width: 100%; text-align: left" ajaxRendered="true">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<t:selectOneRadio id="radioAgendadosPagosNaoPagos"
							disabled="#{(!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) ||  consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
							value="#{consultarPagamentosConsolidadoBean.agendadosPagosNaoPagos}"
							styleClass="HtmlSelectOneRadioBradesco" forceId="true"
							forceIdIndex="false">
							<f:selectItem itemValue="1"
								itemLabel="#{msgs.conPagamentosConsolidado_agendados}" />
							<f:selectItem itemValue="2"
								itemLabel="#{msgs.conPagamentosConsolidado_pagos_naopagos}" />
						</t:selectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
					style="margin-top:6px">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.conPagamentosConsolidado_data_pagamento}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<a4j:outputPanel id="calendarios"
					style="width: 100%; text-align: left" ajaxRendered="true"
					onmousedown="javascript: cleanClipboard();">
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brPanelGroup
								rendered="#{(consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) && !consultarPagamentosConsolidadoBean.disableArgumentosConsulta}">
								<app:calendar oninputkeydown="javascript: cleanClipboard();"
									oninputkeypress="onlyNum();"
									oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialPagamento');"
									id="dataInicialPagamento"
									value="#{consultarPagamentosConsolidadoBean.dataInicialPagamentoFiltro}">
								</app:calendar>
							</br:brPanelGroup>
							<br:brPanelGroup
								rendered="#{(!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}">
								<app:calendar id="dataInicialPagamentoDes"
									value="#{consultarPagamentosConsolidadoBean.dataInicialPagamentoFiltro}"
									disabled="true">
								</app:calendar>
							</br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
								style="margin-right:5px;margin-left:5px"
								value="#{msgs.consultarPagamentosIndividual_a}" />
							<br:brPanelGroup
								rendered="#{(consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) && !consultarPagamentosConsolidadoBean.disableArgumentosConsulta}">
								<app:calendar oninputkeydown="javascript: cleanClipboard();"
									oninputkeypress="onlyNum();"
									oninputchange="recuperarDataCalendario(document.forms[1],'dataFinalPagamento');"
									id="dataFinalPagamento"
									value="#{consultarPagamentosConsolidadoBean.dataFinalPagamentoFiltro}">
								</app:calendar>
							</br:brPanelGroup>
							<br:brPanelGroup
								rendered="#{(!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}">
								<app:calendar id="dataFinalPagamentoDes"
									value="#{consultarPagamentosConsolidadoBean.dataFinalPagamentoFiltro}"
									disabled="true">
								</app:calendar>
							</br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

				</a4j:outputPanel>

				<f:verbatim>
					<hr class="lin">
				</f:verbatim>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<h:selectBooleanCheckbox id="chkRemessa"
						disabled="#{(!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
						value="#{consultarPagamentosConsolidadoBean.chkRemessa}">
						<a4j:support oncomplete="javascript:foco(this);"
							status="statusAguarde" event="onclick"
							reRender="numeroRemessaFiltro"
							action="#{consultarPagamentosConsolidadoBean.limparRemessa}" />
					</h:selectBooleanCheckbox>

					<br:brPanelGroup>
						<br:brOutputText style="margin-left:5px"
							styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.conPagamentosConsolidado_remessa}" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid style="margin-left:20px" columns="1" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.conPagamentosConsolidado_numero}:" />
					</br:brPanelGroup>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGroup>
						<br:brInputText
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							styleClass="HtmlInputTextBradesco"
							value="#{consultarPagamentosConsolidadoBean.numeroRemessaFiltro}"
							size="13" maxlength="9" id="numeroRemessaFiltro"
							disabled="#{!consultarPagamentosConsolidadoBean.chkRemessa || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
							onkeypress="onlyNum();" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<h:selectBooleanCheckbox id="chkContaDebito"
						value="#{consultarPagamentosConsolidadoBean.chkContaDebito}"
						disabled="#{(filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2 || (!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado)) || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
						style="margin-right:5px">
						<a4j:support oncomplete="javascript:foco(this);"
							status="statusAguarde" event="onclick"
							reRender="txtBancoContaDebitoCheck,txtAgenciaContaDebitoCheck,txtContaDebitoCheck"
							action="#{consultarPagamentosConsolidadoBean.limparContaDebito}" />
					</h:selectBooleanCheckbox>
					<br:brPanelGroup>
						<br:brOutputText style="margin-left:5px"
							styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.conPagamentosConsolidado_conta_debito}" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"
					style="margin-left:25px">
					<a4j:outputPanel id="panelBancoContaDebitoCheck"
						ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
							style="text-align:left;">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
									value="#{msgs.conPagamentosConsolidado_banco}:" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brInputText
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							styleClass="HtmlInputTextBradesco"
							disabled="#{!consultarPagamentosConsolidadoBean.chkContaDebito || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
							onkeypress="onlyNum();" converter="javax.faces.Integer"
							value="#{consultarPagamentosConsolidadoBean.cdBancoContaDebito}"
							size="4" maxlength="3" id="txtBancoContaDebitoCheck"
							onkeyup="proximoCampo(3,'conPagamentosConsolidadoForm','conPagamentosConsolidadoForm:txtBancoContaDebitoCheck','conPagamentosConsolidadoForm:txtAgenciaContaDebitoCheck');" />
					</a4j:outputPanel>

					<a4j:outputPanel id="panelAgenciaContaDebitoCheck"
						ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
							style="text-align:left;">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
									value="#{msgs.conPagamentosConsolidado_agencia}:" />
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brInputText
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							styleClass="HtmlInputTextBradesco"
							disabled="#{!consultarPagamentosConsolidadoBean.chkContaDebito || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
							onkeypress="onlyNum();" converter="javax.faces.Integer"
							value="#{consultarPagamentosConsolidadoBean.cdAgenciaBancariaContaDebito}"
							size="6" maxlength="5" id="txtAgenciaContaDebitoCheck"
							onkeyup="proximoCampo(5,'conPagamentosConsolidadoForm','conPagamentosConsolidadoForm:txtAgenciaContaDebitoCheck','conPagamentosConsolidadoForm:txtContaDebitoCheck');" />
					</a4j:outputPanel>

					<a4j:outputPanel id="panelContaDebitoCheck" ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
							style="text-align:left;">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
									value="#{msgs.conPagamentosConsolidado_conta}:" />
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<br:brInputText styleClass="HtmlInputTextBradesco"
									disabled="#{!consultarPagamentosConsolidadoBean.chkContaDebito || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
									converter="javax.faces.Long"
									onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
									onkeypress="onlyNum();"
									value="#{consultarPagamentosConsolidadoBean.cdContaBancariaContaDebito}"
									size="17" maxlength="13" id="txtContaDebitoCheck" />
							</br:brPanelGroup>
							<br:brPanelGrid columns="1" style="margin-right:5px"
								cellpadding="0" cellspacing="0">
								<br:brPanelGroup>
								</br:brPanelGroup>
							</br:brPanelGrid>
							<br:brPanelGroup>

							</br:brPanelGroup>
						</br:brPanelGrid>
					</a4j:outputPanel>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
							style="margin-bottom:40px">
							<h:selectBooleanCheckbox id="chkServicoModalidade"
								value="#{consultarPagamentosConsolidadoBean.chkServicoModalidade}"
								disabled="#{(!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
								style="margin-right:5px">
								<a4j:support oncomplete="javascript:foco(this);"
									status="statusAguarde" event="onclick"
									reRender="cboTipoServico,cboModalidade"
									action="#{consultarPagamentosConsolidadoBean.controleServicoModalidade}" />
							</h:selectBooleanCheckbox>
						</br:brPanelGrid>
					</br:brPanelGroup>

					<br:brPanelGroup>
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
							style="text-align:left;">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
									value="#{msgs.consultarPagamentosIndividual_tipo_servico}:" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<br:brSelectOneMenu id="cboTipoServico"
									value="#{consultarPagamentosConsolidadoBean.tipoServicoFiltro}"
									styleClass="HtmlSelectOneMenuBradesco"
									disabled="#{!consultarPagamentosConsolidadoBean.chkServicoModalidade || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}">
									<f:selectItem itemValue="0"
										itemLabel="#{msgs.label_combo_selecione}" />
									<f:selectItems
										value="#{consultarPagamentosConsolidadoBean.listaTipoServicoFiltro}" />
									<a4j:support oncomplete="javascript:foco(this);"
										status="statusAguarde" event="onchange"
										reRender="cboModalidade"
										action="#{consultarPagamentosConsolidadoBean.listarModalidadesPagto}" />
								</br:brSelectOneMenu>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
							style="text-align:left;">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
									value="#{msgs.consultarPagamentosIndividual_modalidade}:" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<br:brSelectOneMenu id="cboModalidade"
									value="#{consultarPagamentosConsolidadoBean.modalidadeFiltro}"
									styleClass="HtmlSelectOneMenuBradesco"
									disabled="#{(consultarPagamentosConsolidadoBean.tipoServicoFiltro == null || consultarPagamentosConsolidadoBean.tipoServicoFiltro == 0 
			    	   || !consultarPagamentosConsolidadoBean.chkServicoModalidade) || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}">
									<f:selectItem itemValue="0"
										itemLabel="#{msgs.label_combo_todos}" />
									<f:selectItems
										value="#{consultarPagamentosConsolidadoBean.listaModalidadeFiltro}" />
								</br:brSelectOneMenu>
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
							style="margin-bottom:40px">
							<h:selectBooleanCheckbox id="chkRastreamentoTitulo"
								value="#{consultarPagamentosConsolidadoBean.chkRastreamentoTitulo}"
								disabled="#{(!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
								style="margin-right:5px">
								<a4j:support oncomplete="javascript:foco(this);"
									status="statusAguarde" event="onclick"
									reRender="radioRastreadosNaoRastreados"
									action="#{consultarPagamentosConsolidadoBean.controleRastreadosNaoRastreados}" />
							</h:selectBooleanCheckbox>
						</br:brPanelGrid>
					</br:brPanelGroup>

					<br:brPanelGroup>
						<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"
							style="text-align:left;">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
									value="#{msgs.manterServicoRelacionado_combo_rastreamento_titulos}:" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<t:selectOneRadio id="radioRastreadosNaoRastreados"
									value="#{consultarPagamentosConsolidadoBean.radioRastreados}"
									styleClass="HtmlSelectOneRadioBradesco" layout="spread"
									forceId="true" forceIdIndex="false"
									disabled="#{(!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosConsolidadoBean.disableArgumentosConsulta || !consultarPagamentosConsolidadoBean.chkRastreamentoTitulo}">
									<f:selectItem itemValue="1"
										itemLabel="#{msgs.consultarPagamentosIndividual_rastreados}" />
									<f:selectItem itemValue="2"
										itemLabel="#{msgs.consultarPagamentosIndividual_nao_rastreados}" />
									<f:selectItem itemValue="3"
										itemLabel="#{msgs.consultarPagamentosIndividual_todos}" />
										
										
								</t:selectOneRadio>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<t:radio for="radioRastreadosNaoRastreados" index="0" />
							</br:brPanelGroup>
							<br:brPanelGroup>
								<t:radio for="radioRastreadosNaoRastreados" index="1" />
							</br:brPanelGroup>
							<br:brPanelGroup>
								<t:radio for="radioRastreadosNaoRastreados" index="2" />
							</br:brPanelGroup>
							
						</br:brPanelGrid>
					</br:brPanelGroup>

				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<h:selectBooleanCheckbox id="chkSituacaoMotivo"
						value="#{consultarPagamentosConsolidadoBean.chkSituacaoMotivo}"
						disabled="#{(!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
						style="margin-right:5px">
						<a4j:support oncomplete="javascript:foco(this);"
							status="statusAguarde" event="onclick"
							reRender="cboSituacaoPagamento,cboMotivoSituacao"
							action="#{consultarPagamentosConsolidadoBean.limparSituacaoMotivo}" />
					</h:selectBooleanCheckbox>

					<br:brPanelGroup>
						<br:brOutputText style="margin-left:5px"
							styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.label_situacao}" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid style="margin-left:20px" columns="1" cellpadding="0"
					cellspacing="0">
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.conPagamentosConsolidado_situacao_pagamento}:" />
						</br:brPanelGroup>

						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboSituacaoPagamento"
								disabled="#{!consultarPagamentosConsolidadoBean.chkSituacaoMotivo || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
								value="#{consultarPagamentosConsolidadoBean.cboSituacaoPagamentoFiltro}"
								styleClass="HtmlSelectOneMenuBradesco">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{consultarPagamentosConsolidadoBean.listaConsultarSituacaoPagamentoFiltro}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
							style="margin-bottom:40px">
							<h:selectBooleanCheckbox id="chkIndicadorAutorizacao"
								value="#{consultarPagamentosConsolidadoBean.chkIndicadorAutorizacao}"
								disabled="#{(!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
								style="margin-right:5px">
								<a4j:support oncomplete="javascript:foco(this);"
									status="statusAguarde" event="onclick"
									reRender="radioIndicadorAutorizacao"
									action="#{consultarPagamentosConsolidadoBean.controleIndicadorAutorizacao}" />
							</h:selectBooleanCheckbox>
						</br:brPanelGrid>
					</br:brPanelGroup>

					<br:brPanelGroup>
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
							style="text-align:left;">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
							</br:brPanelGroup>
							<br:brPanelGroup>
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
									value="#{msgs.label_indicadorAutorizacao}:" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<t:selectOneRadio id="radioIndicadorAutorizacao"
									value="#{consultarPagamentosConsolidadoBean.radioIndicadorAutorizacao}"
									styleClass="HtmlSelectOneRadioBradesco" layout="spread"
									forceId="true" forceIdIndex="false"
									disabled="#{!consultarPagamentosConsolidadoBean.chkIndicadorAutorizacao}">
									<f:selectItem itemValue="1"
										itemLabel="#{msgs.label_autorizado}" />
									<f:selectItem itemValue="2"
										itemLabel="#{msgs.label_desautorizado}" />
								</t:selectOneRadio>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
							cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<t:radio for="radioIndicadorAutorizacao" index="0" />
							</br:brPanelGroup>
							<br:brPanelGroup>
								<t:radio for="radioIndicadorAutorizacao" index="1" />
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>

				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<h:selectBooleanCheckbox id="chkLoteInterno"
						value="#{consultarPagamentosConsolidadoBean.chkLoteInterno}"
						disabled="#{(!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || 
				consultarPagamentosConsolidadoBean.disableArgumentosConsulta}">
						<a4j:support oncomplete="javascript:foco(this);"
							status="statusAguarde" event="onclick"
							reRender="txtLoteInterno, cboTipoLayoutArquivo"
							action="#{consultarPagamentosConsolidadoBean.limparCamposLoteInterno}" />
					</h:selectBooleanCheckbox>

					<br:brPanelGroup>
						<br:brOutputText style="margin-left:3px"
							styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brPanelGroup style="margin-left:15px">
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.label_numero}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brInputText id="txtLoteInterno"
							value="#{consultarPagamentosConsolidadoBean.loteInterno}"
							style="margin-left:15px" size="18"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							onkeypress="onlyNum();"
							disabled="#{!consultarPagamentosConsolidadoBean.chkLoteInterno}" />
					</br:brPanelGroup>
				</br:brPanelGrid>

			</a4j:outputPanel>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" width="100%" style="text-align:right"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brCommandButton id="btnLimparCampos" styleClass="bto1"
						value="#{msgs.label_limpar_campos}"
						action="#{consultarPagamentosConsolidadoBean.limparTudo}"
						style="margin-right:5px">
						<brArq:submitCheckClient />
					</br:brCommandButton>
					<br:brCommandButton id="btnConsultar" styleClass="bto1"
						onmouseout="javascript:alteraBotao('normal', this.id);"
						value="#{msgs.consultarPagamentosIndividual_btn_consultar}"
						onclick="desbloquearTela();return validaCamposConsultarPagamentosConsolidado(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
				 '#{msgs.consultarPagamentosIndividual_agendados}','#{msgs.consultarPagamentosIndividual_ou}','#{msgs.consultarPagamentosIndividual_pagos_naopagos}', '#{msgs.consultarPagamentosIndividual_data_pagamento}',
				 '#{msgs.consultarPagamentosIndividual_selecione_uma_opcao_consulta_argumento}', '#{msgs.consultarPagamentosIndividual_conta_debito}', '#{msgs.consultarPagamentosIndividual_banco}',
				 '#{msgs.consultarPagamentosIndividual_agencia}', '#{msgs.consultarPagamentosIndividual_conta}', '#{msgs.consultarPagamentosIndividual_tipo_servico}', '#{msgs.consultarPagamentosIndividual_modalidade}',
				 '#{msgs.consultarPagamentosIndividual_situacao_pagamento}', '#{msgs.consultarPagamentosIndividual_remessa}','#{msgs.consultarPagamentosIndividual_numero}',
				 '#{msgs.consultarPagamentosIndividual_digito}', '#{msgs.label_deve_ser_diferente_de_zeros}', '#{msgs.manterServicoRelacionado_combo_rastreamento_titulos}',
				 '#{msgs.label_indicadorAutorizacao}','#{msgs.label_numeroLoteInterno}','#{msgs.label_layout}');"
						disabled="#{(!consultarPagamentosConsolidadoBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
						action="#{consultarPagamentosConsolidadoBean.carregaListaConsultarPagamentosConsolidado}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>

		<f:verbatim>
			<br>
		</f:verbatim>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll">

				<app:scrollableDataTable id="dataTable"
					value="#{consultarPagamentosConsolidadoBean.listaGridConPagamentosConsolidado}"
					var="result" rows="10" rowIndexVar="parametroKey"
					rowClasses="tabela_celula_normal, tabela_celula_destaque"
					width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px">
						<f:facet name="header">
							<br:brOutputText value="" styleClass="tableFontStyle"
								style="width:25; text-align:center" />
						</f:facet>
						<t:selectOneRadio id="sorLista"
							styleClass="HtmlSelectOneRadioBradesco" layout="spread"
							forceId="true" forceIdIndex="false"
							value="#{consultarPagamentosConsolidadoBean.itemSelecionadoListaConPagamentosConsolidado}">
							<f:selectItems
								value="#{consultarPagamentosConsolidadoBean.listaRadiosConPagamentosConsolidado}" />
							<a4j:support event="onclick" reRender="btnDetalhar" />
						</t:selectOneRadio>
						<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="200px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_empresa_gestora_contrato}"
								style="width:200; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsRazaoSocial}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="200px">
						<f:facet name="header">
							<h:outputText value="#{msgs.label_numero_contrato}"
								style="width:200; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.nrContratoOrigem}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="200px">
						<f:facet name="header">
							<h:outputText value="#{msgs.label_cnpj_cpf_cliente_pagador}"
								style="width:200; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.cnpjFormatado}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="200px">
						<f:facet name="header">
							<h:outputText value="#{msgs.label_remessa}"
								style="width:200; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.nrArquivoRemessaPagamento}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabCenter" width="150px">
						<f:facet name="header">
							<h:outputText value="#{msgs.label_data_pagamento}"
								style="width:150; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dtCreditoPagamento}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="200px">
						<f:facet name="header">
							<h:outputText value="#{msgs.label_bco_ag_conta_debito}"
								style="width:200; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.contaDebitoFormatado}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="200px">
						<f:facet name="header">
							<h:outputText value="#{msgs.label_tipo_servico}"
								style="width:200; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsResumoProdutoServico}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="200px">
						<f:facet name="header">
							<h:outputText value="#{msgs.label_modalidade}"
								style="width:200; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsOperacaoProdutoServico}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="200px">
						<f:facet name="header">
							<h:outputText value="#{msgs.label_situacao_pagto}"
								style="width:200; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsSituacaoOperacaoPagamento}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="180px">
						<f:facet name="header">
							<h:outputText value="#{msgs.label_qtde_pagto}"
								style="width:180; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.qtPagamento}" converter="inteiroMilharConverter"/>
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="200px">
						<f:facet name="header">
							<h:outputText value="#{msgs.label_valor_total_pagto}"
								style="width:200; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.vlEfetivoPagamentoCliente}"
							converter="decimalBrazillianConverter" />
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" style="text-align:center"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable"
					actionListener="#{consultarPagamentosConsolidadoBean.pesquisar}">
					<f:facet name="first">
						<brArq:pdcCommandButton id="primeira" styleClass="bto1"
							value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" />
					</f:facet>
					<f:facet name="fastrewind">
						<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_retrocesso}"
							title="#{msgs.label_retrocesso_msg}" />
					</f:facet>
					<f:facet name="previous">
						<brArq:pdcCommandButton id="anterior" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_anterior}"
							title="#{msgs.label_anterior_msg}" />
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton id="proxima" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_proxima}"
							title="#{msgs.label_proxima_msg}" />
					</f:facet>
					<f:facet name="fastforward">
						<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_avanco}"
							title="#{msgs.label_avanco_msg}" />
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton id="ultima" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_ultima}"
							title="#{msgs.label_ultima_msg}" />
					</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<a4j:outputPanel id="panelBotoes"
			style="width: 100%; text-align: right" ajaxRendered="true">
			<br:brPanelGrid columns="1" width="100%" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup style="text-align:right;width:750px">
					<br:brCommandButton id="btnLimpar" styleClass="bto1"
						disabled="#{empty consultarPagamentosConsolidadoBean.listaGridConPagamentosConsolidado}"
						value="#{msgs.conPagamentosConsolidado_btn_limpar}"
						action="#{consultarPagamentosConsolidadoBean.limparGrid}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
					<br:brCommandButton id="btnDetalhar" style="margin-left:5px"
						styleClass="bto1"
						disabled="#{empty consultarPagamentosConsolidadoBean.itemSelecionadoListaConPagamentosConsolidado}"
						value="#{msgs.conPagamentosConsolidado_btn_detalhar}"
						action="#{consultarPagamentosConsolidadoBean.detalharFiltro}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
					<br:brCommandButton id="btnImprimir" style="margin-left:5px"
						styleClass="bto1"
						disabled="#{empty consultarPagamentosConsolidadoBean.listaGridConPagamentosConsolidado}"
						value="#{msgs.btn_imprimir_lista}"
						action="#{consultarPagamentosConsolidadoBean.imprimir}"
						onclick="javascript:desbloquearTela();">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>

		<a4j:status id="statusAguarde" onstart="bloquearTela()"
			onstop="desbloquearTela()" />

		<t:inputHidden
			value="#{consultarPagamentosConsolidadoBean.disableArgumentosConsulta}"
			id="hiddenFlagPesquisa"></t:inputHidden>

		<f:verbatim>
			<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conPagamentosConsolidadoForm:hiddenFlagPesquisa').value);
	  	</script>
		</f:verbatim>
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>