<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conPagamentoIndividualForm" name="conPagamentoIndividualForm" >
<h:inputHidden id="hiddenFoco" value="#{consultarPagamentosIndividualBean.foco}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="radioTipoFiltro" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{consultarPagamentosIndividualBean.disableArgumentosConsulta}" >  
			<f:selectItem itemValue="0" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_cliente}" />
			<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_contrato}" />
			<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_conta_debito}" />
			<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="formulario,panelBotoes,dataTable,dataScroller" action="#{consultarPagamentosIndividualBean.limparTelaPrincipal}" status="statusAguarde" />				
		</t:selectOneRadio>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">			
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;"  >
			<br:brPanelGroup>			
				<t:radio for="radioTipoFiltro" index="0"  />
			</br:brPanelGroup>		
		</br:brPanelGrid>		
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
 
		<a4j:region id="regionFiltro">
		<t:selectOneRadio id="rdoFiltroCliente" value="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '0' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" >  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="" />
		</t:selectOneRadio>
		</a4j:region>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="0" />			
			
			 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cnpj}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtCnpj','conPagamentoIndividualForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0'  || consultarPagamentosIndividualBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtFilial','conPagamentoIndividualForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || consultarPagamentosIndividualBean.disableArgumentosConsulta }" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					<br:brInputText id="txtControle" style="margin-right: 5" converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0'  || consultarPagamentosIndividualBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCnpj}"onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="1" />			
			
			 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cpf}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
				    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || consultarPagamentosIndividualBean.disableArgumentosConsulta  }" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
				    onkeyup="proximoCampo(9,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtCpf','conPagamentoIndividualForm:txtControleCpf');" />
				    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
			<t:radio for="rdoFiltroCliente" index="2" />		
			
			 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta }" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial" />
			</a4j:outputPanel>
			
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="3" />			
			
			<a4j:outputPanel id="panelBanco" ajaxRendered="true">
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtBanco','conPagamentoIndividualForm:txtAgencia');" />
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtAgencia','conPagamentoIndividualForm:txtConta');" />
				
			</a4j:outputPanel>
		
			<a4j:outputPanel id="panelConta" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || consultarPagamentosIndividualBean.disableArgumentosConsulta}"
							 value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" style="margin-right:5px" />
					</br:brPanelGroup>
				</br:brPanelGrid>				
			</a4j:outputPanel>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
		   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado || consultarPagamentosIndividualBean.disableArgumentosConsulta }" value="#{msgs.consultarPagamentosIndividual_btn_consultar_cliente}" action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
		   		 onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_banco}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');"
					actionListener="#{consultarPagamentosIndividualBean.limparArgsListaAposPesquisaClienteContrato}"  >		
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</a4j:outputPanel>
		</br:brPanelGrid>	
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_cliente}:"/>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>	
			
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	    
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top: 9" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	 
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescCliente}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescCliente}"/>
			</br:brPanelGroup>	    
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescCliente}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescCliente}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	
	    <f:verbatim><hr class="lin"></f:verbatim>     
	    
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<t:radio for="radioTipoFiltro" index="1" />
			</br:brPanelGroup>		
		</br:brPanelGrid>	    
	    
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	    
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:20px">
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="empresaGestora" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px" >						
							<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaEmpresaGestora}" />								
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
	
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="tipoContrato" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
							<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaTipoContrato}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroFiltro}" size="12" maxlength="10" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || consultarPagamentosIndividualBean.disableArgumentosConsulta}">
					    </br:brInputText>	
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
	    <f:verbatim><hr class="lin"></f:verbatim>	
    
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;">
	   		<br:brCommandButton id="btoConsultarContrato" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" styleClass="bto1" value="#{msgs.consultarPagamentosIndividual_btn_consultar_contrato}" action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.consultarPagamentosIndividual_tipo_contrato}','#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');"
				actionListener="#{consultarPagamentosIndividualBean.limparArgsListaAposPesquisaClienteContrato}"   		>		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGrid>	  
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	 
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}"/>
			</br:brPanelGroup>	    
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
	    <f:verbatim><hr class="lin"></f:verbatim>  
	    
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<t:radio for="radioTipoFiltro" index="2" />
			</br:brPanelGroup>		
		</br:brPanelGrid>		
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_identificacao_conta_debito}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<a4j:outputPanel id="panelBancoContaDebito" ajaxRendered="true">
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco_conta_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" converter="javax.faces.Integer" value="#{filtroAgendamentoEfetivacaoEstornoBean.bancoContaDebitoFiltro}" size="4" maxlength="3" id="txtBancoContaDebito" onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',1)" onkeyup="proximoCampo(3,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtBancoContaDebito','conPagamentoIndividualForm:txtAgenciaContaDebito');"  >
					<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{consultarPagamentosIndividualBean.controlarArgumentosPesquisa}" />				
				</br:brInputText>
				
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgenciaContaDebito" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia_conta_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',2)" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" converter="javax.faces.Integer" value="#{filtroAgendamentoEfetivacaoEstornoBean.agenciaContaDebitoBancariaFiltro}" size="6" maxlength="5" id="txtAgenciaContaDebito" onkeyup="proximoCampo(5,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtAgenciaContaDebito','conPagamentoIndividualForm:txtContaContaDebito');">
					<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{consultarPagamentosIndividualBean.controlarArgumentosPesquisa}"  />				
				</br:brInputText>
				
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelContaContaDebito" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',3)"  styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" onkeypress="onlyNum();" value="#{filtroAgendamentoEfetivacaoEstornoBean.contaBancariaContaDebitoFiltro}" size="17" maxlength="13" id="txtContaContaDebito" >
							<a4j:support  oncomplete="javascript:focoBlur(document.forms[1]);"  status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{consultarPagamentosIndividualBean.controlarArgumentosPesquisa}" />				
						</br:brInputText>
					</br:brPanelGroup>
				</br:brPanelGrid>				
			</a4j:outputPanel>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
    
	    <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
			<br:brPanelGroup>
				<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<a4j:outputPanel id="painelArgumentos" style="width: 100%; text-align: left" ajaxRendered="true">					
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<t:selectOneRadio id="radioAgendadosPagosNaoPagos" disabled="#{(!consultarPagamentosIndividualBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.agendadosPagosNaoPagos}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false" >  
					<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_agendados}" />
					<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_pagos_naopagos}" />
				</t:selectOneRadio>		
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_data_pagamento}:" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
	
		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brPanelGroup rendered="#{(consultarPagamentosIndividualBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) &&  !consultarPagamentosIndividualBean.disableArgumentosConsulta}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialPagamento');" id="dataInicialPagamento" value="#{consultarPagamentosIndividualBean.dataInicialPagamentoFiltro}" >

						</app:calendar>
					</br:brPanelGroup>
					<br:brPanelGroup rendered="#{(!consultarPagamentosIndividualBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) ||   consultarPagamentosIndividualBean.disableArgumentosConsulta}">
						<app:calendar id="dataInicialPagamentoDes" value="#{consultarPagamentosIndividualBean.dataInicialPagamentoFiltro}" disabled="true" >
						</app:calendar>
					</br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
					<br:brPanelGroup rendered="#{(consultarPagamentosIndividualBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) &&  !consultarPagamentosIndividualBean.disableArgumentosConsulta}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFinalPagamento');" id="dataFinalPagamento" value="#{consultarPagamentosIndividualBean.dataFinalPagamentoFiltro}" >

		 				</app:calendar>	
					</br:brPanelGroup>	
					<br:brPanelGroup rendered="#{(!consultarPagamentosIndividualBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) ||   consultarPagamentosIndividualBean.disableArgumentosConsulta}">
						<app:calendar id="dataFinalPagamentoDes" value="#{consultarPagamentosIndividualBean.dataFinalPagamentoFiltro}" disabled="true">
		 				</app:calendar>	
					</br:brPanelGroup>	
				</br:brPanelGroup>			    
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>		
		</a4j:outputPanel>		
			
				
	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<h:selectBooleanCheckbox id="chkParticipanteContrato" value="#{consultarPagamentosIndividualBean.chkParticipanteContrato}" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2 || (!consultarPagamentosIndividualBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosIndividualBean.disableArgumentosConsulta}" style="margin-right:5px">
					<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="btoBuscar,txtCodigoParticipante,txtNomeParticipante" action="#{filtroAgendamentoEfetivacaoEstornoBean.limparParticipanteContrato}"/>
				</h:selectBooleanCheckbox>		
				
				<br:brPanelGroup>	
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf_cnpj_participante}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.corpoCpfCnpjParticipanteFiltro}" converter="javax.faces.Long"  size="12" maxlength="9" onkeypress="onlyNum();" id="txtCorpoCpfCnpjParticipanteFiltro" disabled="#{!consultarPagamentosIndividualBean.chkParticipanteContrato ||  consultarPagamentosIndividualBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					</br:brInputText>					
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.filialCpfCnpjParticipanteFiltro}" size="6" maxlength="4" converter="javax.faces.Integer"  onkeypress="onlyNum();" id="txtFilialCpfCnpjParticipanteFiltro" disabled="#{!consultarPagamentosIndividualBean.chkParticipanteContrato ||  consultarPagamentosIndividualBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">

					</br:brInputText>
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right:5px" value="#{filtroAgendamentoEfetivacaoEstornoBean.controleCpfCnpjParticipanteFiltro}" size="4" maxlength="2"  converter="javax.faces.Integer" onkeypress="onlyNum();" id="txtControleCpfCnpjParticipanteFiltro" disabled="#{!consultarPagamentosIndividualBean.chkParticipanteContrato ||  consultarPagamentosIndividualBean.disableArgumentosConsulta}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">						
					</br:brInputText>				
				
					<h:inputHidden id="txtCodigoParticipante" value="#{filtroAgendamentoEfetivacaoEstornoBean.codigoParticipanteFiltro}"/>				
					<br:brCommandButton id="btoBuscar" styleClass="bto1" value="#{msgs.consultarPagamentosIndividual_btn_buscar}" action="#{filtroAgendamentoEfetivacaoEstornoBean.buscarParticipante}" style="cursor:hand;margin-left:20px" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
					 onclick="javascript:desbloquearTela(); return validaCampoParticipante(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_cpfCnpj}','#{msgs.label_base}','#{msgs.label_filial}','#{msgs.label_controle}','#{msgs.label_deve_ser_diferente_de_zeros}');"
					 disabled="#{!consultarPagamentosIndividualBean.chkParticipanteContrato || consultarPagamentosIndividualBean.disableArgumentosConsulta}">		
						<brArq:submitCheckClient />
					</br:brCommandButton>			
					
				</br:brPanelGroup>	
															
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
			
			<br:brPanelGrid columns="2"style="margin-left:20px" cellpadding="0" cellspacing="0">					
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpfCnpj}:"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.cpfCnpjParticipanteFiltro}"/>
				</br:brPanelGroup>				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nomeRazaoSocial}:"/> 
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.nomeParticipanteFiltro}"/>    
				</br:brPanelGroup>	
				 
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>		
		
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
				<h:selectBooleanCheckbox id="chkContaDebito" value="#{consultarPagamentosIndividualBean.chkContaDebito}" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2 || (!consultarPagamentosIndividualBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosIndividualBean.disableArgumentosConsulta}" style="margin-right:5px">
					<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="txtBancoContaDebitoCheck,txtAgenciaContaDebitoCheck,txtContaDebitoCheck" action="#{consultarPagamentosIndividualBean.limparContaDebito}"/>			
				</h:selectBooleanCheckbox>
				<br:brPanelGroup>	
				 	 <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_conta_debito}"/>
				 </br:brPanelGroup>
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:25px">
			<a4j:outputPanel id="panelBancoContaDebitoCheck" ajaxRendered="true">
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{!consultarPagamentosIndividualBean.chkContaDebito || consultarPagamentosIndividualBean.disableArgumentosConsulta}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{consultarPagamentosIndividualBean.cdBancoContaDebito}" size="4" maxlength="3" id="txtBancoContaDebitoCheck" onkeyup="proximoCampo(3,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtBancoContaDebitoCheck','conPagamentoIndividualForm:txtAgenciaContaDebitoCheck');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgenciaContaDebitoCheck" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{!consultarPagamentosIndividualBean.chkContaDebito || consultarPagamentosIndividualBean.disableArgumentosConsulta}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{consultarPagamentosIndividualBean.cdAgenciaBancariaContaDebito}" size="6" maxlength="5" id="txtAgenciaContaDebitoCheck" onkeyup="proximoCampo(5,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtAgenciaContaDebitoCheck','conPagamentoIndividualForm:txtContaDebitoCheck');"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelContaDebitoCheck" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{!consultarPagamentosIndividualBean.chkContaDebito || consultarPagamentosIndividualBean.disableArgumentosConsulta}" converter="javax.faces.Long" 	onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{consultarPagamentosIndividualBean.cdContaBancariaContaDebito}" size="17" maxlength="13" id="txtContaDebitoCheck"/>
					</br:brPanelGroup>	
				</br:brPanelGrid>				
			</a4j:outputPanel>
	    </br:brPanelGrid>	
	    
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>    	
	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-bottom:40px" >	
					<h:selectBooleanCheckbox id="chkServicoModalidade" value="#{consultarPagamentosIndividualBean.chkServicoModalidade}" disabled="#{(!consultarPagamentosIndividualBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosIndividualBean.disableArgumentosConsulta}"style="margin-right:5px">
						<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="cboTipoServico,cboModalidade" action="#{consultarPagamentosIndividualBean.controleServicoModalidade}"/>
					</h:selectBooleanCheckbox>					
				</br:brPanelGrid>	
			</br:brPanelGroup>
	
			<br:brPanelGroup>			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_servico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboTipoServico" value="#{consultarPagamentosIndividualBean.tipoServicoFiltro}" styleClass="HtmlSelectOneMenuBradesco"
				    	disabled="#{!consultarPagamentosIndividualBean.chkServicoModalidade || consultarPagamentosIndividualBean.disableArgumentosConsulta}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{consultarPagamentosIndividualBean.listaTipoServicoFiltro}" />						
							<a4j:support oncomplete="javascript:foco(this);"  status="statusAguarde" event="onchange" reRender="cboModalidade" action="#{consultarPagamentosIndividualBean.listarModalidadesPagto}"/>						
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>				
				
			    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_modalidade}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboModalidade" value="#{consultarPagamentosIndividualBean.modalidadeFiltro}" styleClass="HtmlSelectOneMenuBradesco"
				    	  disabled="#{consultarPagamentosIndividualBean.tipoServicoFiltro == null || consultarPagamentosIndividualBean.tipoServicoFiltro == 0 
				    	   || !consultarPagamentosIndividualBean.chkServicoModalidade || consultarPagamentosIndividualBean.disableArgumentosConsulta}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_todos}"/>
							<f:selectItems value="#{consultarPagamentosIndividualBean.listaModalidadeFiltro}" />												
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>				
			</br:brPanelGroup>			
			
		</br:brPanelGrid>    					
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>   
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-bottom:40px" >	
					<h:selectBooleanCheckbox id="chkRastreamentoTitulo" value="#{consultarPagamentosIndividualBean.chkRastreamentoTitulo}" disabled="#{(!consultarPagamentosIndividualBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosIndividualBean.disableArgumentosConsulta}" style="margin-right:5px">
						<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="radioRastreadosNaoRastreados" action="#{consultarPagamentosIndividualBean.controleRastreadosNaoRastreados}"/>
					</h:selectBooleanCheckbox>					
				</br:brPanelGrid>	
			</br:brPanelGroup>
	
			<br:brPanelGroup>			
				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterServicoRelacionado_combo_rastreamento_titulos}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>

			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<t:selectOneRadio id="radioRastreadosNaoRastreados"
									value="#{consultarPagamentosIndividualBean.radioRastreados}"
									styleClass="HtmlSelectOneRadioBradesco" layout="spread"
									forceId="true" forceIdIndex="false"
									disabled="#{(!consultarPagamentosIndividualBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosIndividualBean.disableArgumentosConsulta || !consultarPagamentosIndividualBean.chkRastreamentoTitulo}">
									<f:selectItem itemValue="1"
										itemLabel="#{msgs.consultarPagamentosIndividual_rastreados}" />
									<f:selectItem itemValue="2"
										itemLabel="#{msgs.consultarPagamentosIndividual_nao_rastreados}" />
									<f:selectItem itemValue="3"
										itemLabel="#{msgs.consultarPagamentosIndividual_todos}" />
								</t:selectOneRadio>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<t:radio for="radioRastreadosNaoRastreados" index="0" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<t:radio for="radioRastreadosNaoRastreados" index="1" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<t:radio for="radioRastreadosNaoRastreados" index="2" />
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>

		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
			<h:selectBooleanCheckbox id="chkSituacaoMotivo" value="#{consultarPagamentosIndividualBean.chkSituacaoMotivo}" disabled="#{(!consultarPagamentosIndividualBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosIndividualBean.disableArgumentosConsulta}"  style="margin-right:5px">
				<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="cboSituacaoPagamento,cboMotivoSituacao" action="#{consultarPagamentosIndividualBean.limparSituacaoMotivo}"/>		
			</h:selectBooleanCheckbox>					
			<br:brPanelGroup>	
			 	 <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao_motivo}"/>
			 </br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:25px" >	
			<br:brPanelGroup>			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_situacao_pagamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu disabled="#{!consultarPagamentosIndividualBean.chkSituacaoMotivo || consultarPagamentosIndividualBean.disableArgumentosConsulta}" id="cboSituacaoPagamento" value="#{consultarPagamentosIndividualBean.cboSituacaoPagamentoFiltro}" style="margin-right:20px" styleClass="HtmlSelectOneMenuBradesco" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{consultarPagamentosIndividualBean.listaConsultarSituacaoPagamentoFiltro}" />
							<a4j:support oncomplete="javascript:foco(this);"  status="statusAguarde" event="onchange" reRender="cboMotivoSituacao" action="#{consultarPagamentosIndividualBean.listarConsultarMotivoSituacaoPagamento}"/>												
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>				
			</br:brPanelGroup>
			
			<br:brPanelGroup>			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_motivo_situacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboMotivoSituacao" disabled="#{!consultarPagamentosIndividualBean.chkSituacaoMotivo || consultarPagamentosIndividualBean.cboSituacaoPagamentoFiltro == null || consultarPagamentosIndividualBean.cboSituacaoPagamentoFiltro == 0 || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.cboMotivoSituacaoFiltro}" styleClass="HtmlSelectOneMenuBradesco" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{consultarPagamentosIndividualBean.listaConsultarMotivoSituacaoPagamentoFiltro}" />							
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>				
			</br:brPanelGroup>					
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio id="radioPesquisasFiltroPrincipal" value="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal}"  disabled="#{(!consultarPagamentosIndividualBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosIndividualBean.disableArgumentosConsulta}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_remessa}" />
				<f:selectItem itemValue="2" itemLabel="" />
				<f:selectItem itemValue="3" itemLabel="#{msgs.consultarPagamentosIndividual_favorecido}" />
				<a4j:support  oncomplete="javascript:foco(this);"  status="statusAguarde" event="onclick" reRender="panelRadioPrincipal" action="#{consultarPagamentosIndividualBean.limparRemessaFavorecidoBeneficiarioLinha}"/>										
			</t:selectOneRadio>
		</br:brPanelGrid>	
		
		<a4j:outputPanel id="panelRadioPrincipal" ajaxRendered="true">		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-bottom:50px">			
					<t:radio for="radioPesquisasFiltroPrincipal" index="0" />				
				</br:brPanelGrid>				
			</br:brPanelGroup>
	
			<br:brPanelGroup>	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_pagamento}:"/>
					</br:brPanelGroup>		
				</br:brPanelGrid>					
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px;">			
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="text-align:left;">
						    <br:brPanelGroup>
								<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '0' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.numeroPagamentoDeFiltro}" size="40" maxlength="30" id="txtNumeroDePagamento" style="margin-right:5px" />					
						    </br:brPanelGroup>					
						</br:brPanelGrid>		
					</br:brPanelGroup>				
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-top:6px" >		
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>	
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_valor_pagamento}:"/>
					</br:brPanelGroup>		
				</br:brPanelGrid>					
				
				<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-top:5px;">			
					<br:brPanelGroup>
						<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="text-align:left;">
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_de}:" style="margin-right:5px"/>
							</br:brPanelGroup>
						    <br:brPanelGroup>
								<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '0' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.valorPagamentoDeFiltro}" id="txtValorDePagamento" style="margin-right:5px" converter="decimalBrazillianConverter" alt="decimalBr" maxlength="15" size="27" onfocus="loadMasks();" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');"/>					
						    </br:brPanelGroup>					
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_ate}:" style="margin-right:5px"/>
							</br:brPanelGroup>
						    <br:brPanelGroup>
								<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '0' || consultarPagamentosIndividualBean.disableArgumentosConsulta}"  value="#{consultarPagamentosIndividualBean.valorPagamentoAteFiltro}"  id="txtValorAtePagamento" converter="decimalBrazillianConverter" alt="decimalBr" maxlength="15" size="27" onfocus="loadMasks();" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');"/>					
						    </br:brPanelGroup>					
						</br:brPanelGrid>		
					</br:brPanelGroup>				
				</br:brPanelGrid>			
			</br:brPanelGroup>		
			
		</br:brPanelGrid>	
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<t:radio for="radioPesquisasFiltroPrincipal" index="1" />				
			</br:brPanelGroup>
	
			<br:brPanelGroup>	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px; margin-top:5px" >	
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero}:"/>
					</br:brPanelGroup>		
				</br:brPanelGrid>					
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px; margin-left:25px">			
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" value="#{consultarPagamentosIndividualBean.numeroRemessaFiltro}" size="13" 
						   maxlength="9" id="numeroRemessaFiltro" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '1' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" style="margin-right:5px" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
				    </br:brPanelGroup>					
				</br:brPanelGrid>		
			</br:brPanelGroup>
		</br:brPanelGrid>		
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<t:radio for="radioPesquisasFiltroPrincipal" index="2" />				
			</br:brPanelGroup>
	
			<br:brPanelGroup>	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_linha_digitavel}:"/>
					</br:brPanelGroup>		
				</br:brPanelGrid>					
				
				<br:brPanelGrid columns="8" cellpadding="0" cellspacing="0" style="text-align:left; margin-top:5px;">
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.linhaDigitavel1}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel1" onkeyup="proximoCampo(5,'conPagamentoIndividualForm','conPagamentoIndividualForm:linhaDigitavel1','conPagamentoIndividualForm:linhaDigitavel2');" style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.linhaDigitavel2}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel2" onkeyup="proximoCampo(5,'conPagamentoIndividualForm','conPagamentoIndividualForm:linhaDigitavel2','conPagamentoIndividualForm:linhaDigitavel3');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.linhaDigitavel3}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel3" onkeyup="proximoCampo(5,'conPagamentoIndividualForm','conPagamentoIndividualForm:linhaDigitavel3','conPagamentoIndividualForm:linhaDigitavel4');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.linhaDigitavel4}" onkeypress="onlyNum();" size="7" maxlength="6" id="linhaDigitavel4" onkeyup="proximoCampo(6,'conPagamentoIndividualForm','conPagamentoIndividualForm:linhaDigitavel4','conPagamentoIndividualForm:linhaDigitavel5');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.linhaDigitavel5}" onkeypress="onlyNum();" size="6" maxlength="5" id="linhaDigitavel5" onkeyup="proximoCampo(5,'conPagamentoIndividualForm','conPagamentoIndividualForm:linhaDigitavel5','conPagamentoIndividualForm:linhaDigitavel6');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.linhaDigitavel6}" onkeypress="onlyNum();" size="7" maxlength="6" id="linhaDigitavel6" onkeyup="proximoCampo(6,'conPagamentoIndividualForm','conPagamentoIndividualForm:linhaDigitavel6','conPagamentoIndividualForm:linhaDigitavel7');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.linhaDigitavel7}" onkeypress="onlyNum();" size="2" maxlength="1" id="linhaDigitavel7" onkeyup="proximoCampo(1,'conPagamentoIndividualForm','conPagamentoIndividualForm:linhaDigitavel7','conPagamentoIndividualForm:linhaDigitavel8');"style="margin-right:5px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
					<br:brPanelGroup>			
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.linhaDigitavel8}" onkeypress="onlyNum();" size="18" maxlength="14" id="linhaDigitavel8" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>					
					</br:brPanelGroup>
				</br:brPanelGrid>		
			</br:brPanelGroup>			
		</br:brPanelGrid>			
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio id="radioFavorecidoFiltro" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" value="#{consultarPagamentosIndividualBean.radioFavorecidoFiltro}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_conta_credito}" />
				<f:selectItem itemValue="3" itemLabel="#{msgs.label_conta_salario}" />
				<f:selectItem itemValue="4" itemLabel="#{msgs.label_conta_pagamento}" />
				<a4j:support  oncomplete="javascript:foco(this);"  status="statusAguarde" event="onclick" 
				reRender="panelRadioPrincipal" action="#{consultarPagamentosIndividualBean.limparFavorecido}"/>										
			</t:selectOneRadio>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<t:radio for="radioPesquisasFiltroPrincipal" index="3" />				
			</br:brPanelGroup>
		</br:brPanelGrid>				
	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="radioFavorecidoFiltro" index="0" />			
			
			 <a4j:outputPanel id="panelCodigoFavorecido" ajaxRendered="true">	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;margin-top:6px;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_codigo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCodigoFavorecido" size="20" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || consultarPagamentosIndividualBean.radioFavorecidoFiltro != '0' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" maxlength="15" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{consultarPagamentosIndividualBean.codigoFavorecidoFiltro}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<br:brPanelGroup>	
				<t:radio for="radioFavorecidoFiltro" index="1" />			
			</br:brPanelGroup>	
			
			<br:brPanelGroup>			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;margin-top:6px;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cpfcnpjoutros}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtInscricao" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || consultarPagamentosIndividualBean.radioFavorecidoFiltro != '1' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" size="20" maxlength="15" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{consultarPagamentosIndividualBean.inscricaoFiltro}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
				       style="margin-right:20px"/>
				</br:brPanelGrid>
			</br:brPanelGroup>						
				
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left; margin-top:6px;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || consultarPagamentosIndividualBean.radioFavorecidoFiltro != '1' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" id="tipoInscricao" value="#{consultarPagamentosIndividualBean.tipoInscricaoFiltro}" styleClass="HtmlSelectOneMenuBradesco" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{consultarPagamentosIndividualBean.listaInscricaoFavorecidoFiltro}" />						
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>			
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:20px">
		    <br:brPanelGroup>	
				<t:radio for="radioFavorecidoFiltro" index="2" />			
		    </br:brPanelGroup>			
		    
		    <br:brPanelGroup>		    
				<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:25px;margin-top:6px">		
					<a4j:outputPanel id="panelBancoRadioFavorecido" ajaxRendered="true">
						
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || consultarPagamentosIndividualBean.radioFavorecidoFiltro != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{consultarPagamentosIndividualBean.cdBancoContaCredito}" size="4" maxlength="3" id="txtBancoContaCredito" onkeyup="proximoCampo(3,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtBancoContaCredito','conPagamentoIndividualForm:txtAgenciaContaCredito');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
						
						</br:brInputText>	
						
					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelAgenciaRadioFavorecido" ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						<br:brInputText disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || consultarPagamentosIndividualBean.radioFavorecidoFiltro != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{consultarPagamentosIndividualBean.cdAgenciaBancariaContaCredito}" size="6" maxlength="5" id="txtAgenciaContaCredito" onkeyup="proximoCampo(5,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtAgenciaContaCredito','conPagamentoIndividualForm:txtContaContaCredito');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
							
						</br:brInputText>	
					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelContaRadioFavorecido" ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
					    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						    <br:brPanelGroup>
								<br:brInputText disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || consultarPagamentosIndividualBean.radioFavorecidoFiltro != '2' || consultarPagamentosIndividualBean.disableArgumentosConsulta}"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
									 styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" value="#{consultarPagamentosIndividualBean.cdContaBancariaContaCredito}" size="17" maxlength="13" id="txtContaContaCredito">
									
								</br:brInputText>	
									 
			 				</br:brPanelGroup>						
				    				
						</br:brPanelGrid>	
					</a4j:outputPanel>
					
					
				</br:brPanelGrid>	
		    </br:brPanelGroup>							
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:20px">
		    <br:brPanelGroup>	
				<t:radio for="radioFavorecidoFiltro" index="3" />			
		    </br:brPanelGroup>			
		    
		    <br:brPanelGroup>		    
				<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:25px;margin-top:6px">		
					<a4j:outputPanel id="panelBancoSalarioRadioFavorecido" ajaxRendered="true">
						
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>

						<br:brInputText styleClass="HtmlInputTextBradesco"
							disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || consultarPagamentosIndividualBean.radioFavorecidoFiltro != '3' || consultarPagamentosIndividualBean.disableArgumentosConsulta}"
							onkeypress="onlyNum();" converter="javax.faces.Integer"
							value="#{consultarPagamentosIndividualBean.cdBancoContaSalario}"
							size="4" maxlength="3" id="txtBancoContaSalario"
							onkeyup="proximoCampo(3,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtBancoContaSalario','conPagamentoIndividualForm:txtAgenciaContaSalario');"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
						</br:brInputText>

					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelAgenciaSalarioRadioFavorecido" ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>

						<br:brInputText
						disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || consultarPagamentosIndividualBean.radioFavorecidoFiltro != '3' || consultarPagamentosIndividualBean.disableArgumentosConsulta}"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						converter="javax.faces.Integer"
						value="#{consultarPagamentosIndividualBean.cdAgenciaBancariaContaSalario}"
						size="6" maxlength="5" id="txtAgenciaContaSalario"
						onkeyup="proximoCampo(5,'conPagamentoIndividualForm','conPagamentoIndividualForm:txtAgenciaContaSalario','conPagamentoIndividualForm:txtContaContaSalario');"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelContaSalarioRadioFavorecido" ajaxRendered="true">
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
					    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						    <br:brPanelGroup>
								<br:brInputText
									disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || consultarPagamentosIndividualBean.radioFavorecidoFiltro != '3' || consultarPagamentosIndividualBean.disableArgumentosConsulta}"
									onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
									styleClass="HtmlInputTextBradesco"
									converter="javax.faces.Long" onkeypress="onlyNum();"
									value="#{consultarPagamentosIndividualBean.cdContaBancariaContaSalario}"
									size="17" maxlength="13" id="txtContaContaSalarioCredito"/>
							</br:brPanelGroup>						
						</br:brPanelGrid>	
					</a4j:outputPanel>
				</br:brPanelGrid>	
		    </br:brPanelGroup>							
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:20px">
		    <br:brPanelGroup>	
				<t:radio for="radioFavorecidoFiltro" index="4" />			
		    </br:brPanelGroup>			
		    
		    <br:brPanelGroup>		    
				<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:25px;margin-top:6px">		
					<a4j:outputPanel id="panelBancoPagamentoRadioFavorecido" ajaxRendered="true">
						
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>

						<br:brInputText styleClass="HtmlInputTextBradesco"
							disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || consultarPagamentosIndividualBean.radioFavorecidoFiltro != '4' || consultarPagamentosIndividualBean.disableArgumentosConsulta}"
							onkeypress="onlyNum();"
							value="#{consultarPagamentosIndividualBean.cdBancoPagamento}"
							size="4" maxlength="3" id="txtBancoPagamento"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
						</br:brInputText>

					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelIspb" ajaxRendered="true" style="margin-right:15px;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />		
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_ispb}:"/>
						</br:brPanelGroup>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>

						<br:brInputText	disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || 
							consultarPagamentosIndividualBean.radioFavorecidoFiltro != '4' || 
							consultarPagamentosIndividualBean.disableArgumentosConsulta}"
						styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
						value="#{consultarPagamentosIndividualBean.ispb}"
						size="10" maxlength="8" id="txtISPB"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					</a4j:outputPanel>
					
					<a4j:outputPanel id="panelContaPagamentoRadioFavorecido" ajaxRendered="true">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
						</br:brPanelGroup>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
					    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						    <br:brPanelGroup>
								<br:brInputText
									disabled="#{consultarPagamentosIndividualBean.radioPesquisaFiltroPrincipal != '3' || 
										consultarPagamentosIndividualBean.radioFavorecidoFiltro != '4' || 
										consultarPagamentosIndividualBean.disableArgumentosConsulta}"
									onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
									styleClass="HtmlInputTextBradesco"
									onkeypress="onlyNum();"
									value="#{consultarPagamentosIndividualBean.cdContaPagamento}"
									size="25" maxlength="20" id="txtContaPagamento"/>
							</br:brPanelGroup>						
						</br:brPanelGrid>	
					</a4j:outputPanel>
				</br:brPanelGrid>	
		    </br:brPanelGroup>							
		</br:brPanelGrid>	
			
		</a4j:outputPanel>		
			
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left; margin-top:6px;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_classificacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="classificacao" value="#{consultarPagamentosIndividualBean.classificacaoFiltro}" styleClass="HtmlSelectOneMenuBradesco" disabled="#{consultarPagamentosIndividualBean.disableArgumentosConsulta}">
							<f:selectItem itemValue="000000000" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_classificao_numero_pagamento}"/>
							<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_classificacao_favorecido}"/>
							<f:selectItem itemValue="3" itemLabel="#{msgs.consultarPagamentosIndividual_classificacao_valor_pagamento}"/>																		
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>		
		</br:brPanelGrid>			
		</a4j:outputPanel>
	
	    <f:verbatim><hr class="lin"></f:verbatim>	
	    
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{consultarPagamentosIndividualBean.limpar}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);" value="#{msgs.consultarPagamentosIndividual_btn_consultar}" action="#{consultarPagamentosIndividualBean.consultarPagamentosIndividuais}"
				onclick="javascript:desbloquearTela(); return validaPagamentoIndividual(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
					 '#{msgs.consultarPagamentosIndividual_agendados}',
					 '#{msgs.consultarPagamentosIndividual_ou}',
					 '#{msgs.consultarPagamentosIndividual_pagos_naopagos}',
					 '#{msgs.consultarPagamentosIndividual_data_pagamento}',
					 '#{msgs.consultarPagamentosIndividual_selecione_uma_opcao_consulta_argumento}',
					 '#{msgs.label_campos_participante_necessarios}',
					 '#{msgs.consultarPagamentosIndividual_conta_debito}',
					 '#{msgs.consultarPagamentosIndividual_banco}',
					 '#{msgs.consultarPagamentosIndividual_agencia}',
					 '#{msgs.consultarPagamentosIndividual_conta}',
					 '#{msgs.consultarPagamentosIndividual_tipo_servico}',
					 '#{msgs.consultarPagamentosIndividual_modalidade}',
					 '#{msgs.consultarPagamentosIndividual_situacao_pagamento}',
					 '#{msgs.consultarPagamentosIndividual_motivo_situacao}',
					 '#{msgs.consultarPagamentosIndividual_numero_pagamento}',
	 				 '#{msgs.consultarPagamentosIndividual_valor_pagamento}',
	 				 '#{msgs.consultarPagamentosIndividual_remessa}',
	 				 '#{msgs.consultarPagamentosIndividual_numero}',
	 				 '#{msgs.consultarPagamentosIndividual_linha_digitavel}',
	 				 '#{msgs.consultarPagamentosIndividual_selecione_uma_opcao_consulta_para_favorecido}',
	 				 '#{msgs.consultarPagamentosIndividual_codigo}',
	 				 '#{msgs.consultarPagamentosIndividual_favorecido}',
	 				 '#{msgs.consultarPagamentosIndividual_inscricao}',
	 				 '#{msgs.consultarPagamentosIndividual_tipo}',
	 				 '#{msgs.consultarPagamentosIndividual_conta_credito}',
	 				 '#{msgs.consultarPagamentosIndividual_digito}',
	  				 '#{msgs.label_tipo_conta}',
	  				 '#{msgs.label_deve_ser_diferente_de_zeros}',
	  				 '#{msgs.manterServicoRelacionado_combo_rastreamento_titulos}',
	  				 '#{msgs.label_conta_salario}}');"
					disabled="#{(!consultarPagamentosIndividualBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultarPagamentosIndividualBean.disableArgumentosConsulta}"> 				 
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
	</a4j:outputPanel>				   
		
		<f:verbatim><br></f:verbatim> 
	
			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 
	
				<app:scrollableDataTable id="dataTable" value="#{consultarPagamentosIndividualBean.listaGridPagamentosIndividuais}" var="result" 
					rows="10" rowIndexVar="parametroKey" 
					rowClasses="tabela_celula_normal, tabela_celula_destaque" 
					width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px">
						<f:facet name="header">
							<br:brOutputText value="" styleClass="tableFontStyle"
								style="width:25; text-align:center" />
						</f:facet>
						<t:selectOneRadio id="sorLista"
							styleClass="HtmlSelectOneRadioBradesco" layout="spread"
							forceId="true" forceIdIndex="false"
							value="#{consultarPagamentosIndividualBean.itemSelecionadoLista}">
							<f:selectItems
								value="#{consultarPagamentosIndividualBean.listaControleRadio}" />
							<a4j:support event="onclick" reRender="panelBotoes"
								action="#{consultarPagamentosIndividualBean.habilitaBotoes}"/>
						</t:selectOneRadio>
						<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>


					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.consultarPagamentosIndividual_grid_cnpj_cpf_cliente_pagador}" style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdCpfCnpjFormatado}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_nome_razao_social}"  style="width:250; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsRazaoSocial}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_empresa_gestora_contrato}" style="width:250; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsEmpresa}" />
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_numero_contrato}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nrContrato}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabLeft" width="220px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_tipo_servico}" style="width:220; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsResumoProdutoServico}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_modalidade}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsOperacaoProdutoServico}" />
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft" width="130px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_numero_pagamento}" style="width:130; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.cdControlePagamento}" converter="spaceConverter" escape="false"/>
					</app:scrollableColumn>	
	
					<app:scrollableColumn styleClass="colTabCenter" width="150px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_data_pagamento}" style="width:150; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dtCreditoPagamento}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="150px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_valor_pagamento}" style="width:150; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.vlEfetivoPagamentoFormatado}" />
					</app:scrollableColumn>		
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_favorecido_beneficiario}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsFavorecido}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabRight" width="280px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_bco_ag_conta_debito}" style="width:280; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.bancoAgenciaContaDebitoFormatado}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabRight" width="280px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_bco_ag_conta_salario}" style="width:280; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.bancoAgenciaContaSalarioFormatado}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabRight" width="280px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_conta_de_credito_pagamento}" style="width:280; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.bancoAgenciaContaCreditoFormatado}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_ambiente}" style="width:150; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsIndicadorPagamento}" />
					</app:scrollableColumn>						
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_situacao_pagamento}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsSituacaoOperacaoPagamento}" />
					</app:scrollableColumn>				
					
				</app:scrollableDataTable>
				
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{consultarPagamentosIndividualBean.pesquisarPagamentoIndividual}" >
				 <f:facet name="first">
				    <brArq:pdcCommandButton id="primeira"
				      styleClass="bto1"
				      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" />
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" />
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" />
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton id="proxima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" />
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" />
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton id="ultima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" />
				  </f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
			<br:brPanelGrid columns="9" style="text-align:right" cellpadding="0"
				cellspacing="0" border="0">
				<br:brPanelGroup>
					<br:brCommandButton
						disabled="#{consultarPagamentosIndividualBean.listaGridPagamentosIndividuais == null}"
						id="btnLimparGrid" styleClass="bto1"
						value="#{msgs.consultarPagamentosIndividual_btn_limpar}"
						style="margin-right:5px"
						action="#{consultarPagamentosIndividualBean.limparGrid}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brCommandButton id="btnDetalhar" styleClass="bto1"
						style="margin-right:5px"
						value="#{msgs.consultarPagamentosIndividual_btn_detalhar}"
						action="#{consultarPagamentosIndividualBean.detalhar}"
						disabled="#{consultarPagamentosIndividualBean.itemSelecionadoLista == null}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brCommandButton id="btnConsultarManutencao" styleClass="bto1"
						style="margin-right:5px"
						value="#{msgs.consultarPagamentosIndividual_btn_consultar_manutencao}"
						action="#{consultarPagamentosIndividualBean.consultarManutencao}"
						disabled="#{consultarPagamentosIndividualBean.itemSelecionadoLista == null}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brCommandButton id="btnConsultarAutorizantes" styleClass="bto1"
						style="margin-right:5px"
						value="#{msgs.btn_consultar_autorizantes_net_empresa}"
						action="#{consultarPagamentosIndividualBean.consultarAutorizantes}"
						disabled="#{consultarPagamentosIndividualBean.habilitaBotaoConsultarAutorizantes}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brCommandButton id="btnHistoricoConsultaSaldo"
						styleClass="bto1" style="margin-right:5px"
						value="#{msgs.consultarPagamentosIndividual_btn_historico_consulta_saldo}"
						action="#{consultarPagamentosIndividualBean.historicoConsultaSaldo}"
						disabled="#{consultarPagamentosIndividualBean.itemSelecionadoLista == null}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brCommandButton
						disabled="#{empty consultarPagamentosIndividualBean.listaGridPagamentosIndividuais}"
						id="btnImprimir" styleClass="bto1"
						value="#{msgs.btn_imprimir_lista}"
						action="#{consultarPagamentosIndividualBean.imprimirRelatorioPagInd}"
						onclick="javascript:desbloquearTela();">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>	
		
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   
	
	<t:inputHidden value="#{consultarPagamentosIndividualBean.disableArgumentosConsulta}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conPagamentoIndividualForm:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
		
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />

</brArq:form>