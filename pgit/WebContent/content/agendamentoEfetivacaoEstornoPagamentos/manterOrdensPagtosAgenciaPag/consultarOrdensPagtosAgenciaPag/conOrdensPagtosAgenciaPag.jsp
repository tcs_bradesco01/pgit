<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%> 
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j"%>

<brArq:form id="conOrdensPagtosAgenciaPagForm" name="conOrdensPagtosAgenciaPagForm">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brInputText disabled="#{consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{consultarOrdensPagtosAgenciaPagBean.agencia}" size="6" maxlength="5" id="txtAgencia" />
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrdensPagtosAgenciaPag_label_data_pagamento_op}:"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>			
	
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.label_de}:" />
			
			<br:brPanelGroup rendered="#{!consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta}">
				<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtDataCreditoOpDe');" id="txtDataCreditoOpDe" value="#{consultarOrdensPagtosAgenciaPagBean.dataCreditoOpDe}" >
				</app:calendar>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta}">
				<app:calendar id="txtDataCreditoOpDeDes" value="#{consultarOrdensPagtosAgenciaPagBean.dataCreditoOpDe}" disabled="true" >
				</app:calendar>
			</br:brPanelGroup>
			
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_ate}:"/>
			
			<br:brPanelGroup rendered="#{!consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta}">
				<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtDataCreditoOpAte');" id="txtDataCreditoOpAte" value="#{consultarOrdensPagtosAgenciaPagBean.dataCreditoOpAte}" >
 				</app:calendar>	
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta}">
				<app:calendar id="txtDataCreditoOpAteDes" value="#{consultarOrdensPagtosAgenciaPagBean.dataCreditoOpAte}" disabled="true">
 				</app:calendar>	
			</br:brPanelGroup>			
		</br:brPanelGroup>
	</a4j:outputPanel>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_do_pagamento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>				
			</br:brPanelGrid>
		    <br:brPanelGrid columns="7" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.label_de}:" />
				</br:brPanelGroup>		    
				<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>		    
			    <br:brPanelGroup>
					<br:brInputText disabled="#{consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta}" id="txtValorInicial" styleClass="HtmlOutputTextRotuloBradesco" value="#{consultarOrdensPagtosAgenciaPagBean.valorInicial}" converter="decimalBrazillianConverter" alt="decimalBr" size="30" 
						onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');"/>
				</br:brPanelGroup>								
				<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			    <br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_ate}:" />
				</br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>										
				<br:brPanelGroup>
					<br:brInputText disabled="#{consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta}" id="txtValorFinal" styleClass="HtmlOutputTextRotuloBradesco" value="#{consultarOrdensPagtosAgenciaPagBean.valorFinal}" converter="decimalBrazillianConverter" alt="decimalBr" size="30" 
						onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');"/>
				</br:brPanelGroup>				
			</br:brPanelGrid>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_situacaoPagamento}:" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>	
					<br:brSelectOneMenu id="cmbSituacaoPagamento" style="text-transform: uppercase" value="#{consultarOrdensPagtosAgenciaPagBean.cmbSituacaoPagamento}" disabled="#{consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta}" styleClass="HtmlSelectOneMenuBradesco">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conPagamentosFavorecido_label_tipo_selecione}"/>
						<f:selectItems value="#{consultarOrdensPagtosAgenciaPagBean.listaCmbSituacaoPagamento}"/>					
					</br:brSelectOneMenu>	
				</br:brPanelGroup>
			</br:brPanelGrid>		
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_forma_pagamento}:" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>	
					<br:brSelectOneMenu id="cmbFormaPagamento" style="text-transform: uppercase" value="#{consultarOrdensPagtosAgenciaPagBean.cmbFormaPagamento}" disabled="#{consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta}" styleClass="HtmlSelectOneMenuBradesco">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conPagamentosFavorecido_label_tipo_selecione}"/>
						<f:selectItem itemValue="1" itemLabel="1 - #{msgs.label_saque_em_dinheiro}"/>					
						<f:selectItem itemValue="2" itemLabel="2 - #{msgs.label_credito_em_conta}"/>					
						<f:selectItem itemValue="3" itemLabel="3 - #{msgs.label_emissao_cheque}"/>
						<f:selectItem itemValue="4" itemLabel="4 - #{msgs.label_diversos}"/>
					</br:brSelectOneMenu>	
				</br:brPanelGroup>
			</br:brPanelGrid>		
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conOrdensPagtosAgenciaPag_coluna_inscricao_favorecido}:" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup style="margin-right:20px">				
						<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{consultarOrdensPagtosAgenciaPagBean.inscricaoFavorecido}" id="inscricaoFavorecido" size="20" maxlength="15" onkeypress="onlyNum();" disabled="#{consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta || consultarOrdensPagtosAgenciaPagBean.exibeInscricaoFavorevido}"/>							
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_tipo}:" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>				
						<br:brSelectOneMenu id="tipoFavorecido" value="#{consultarOrdensPagtosAgenciaPagBean.cmbTipoFavorecido}" disabled="#{consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.conPagamentosFavorecido_label_tipo_selecione}"/>
							<f:selectItems value="#{consultarOrdensPagtosAgenciaPagBean.listaCmbTipoFavorecido}"/>
							<a4j:support oncomplete="javascript:foco(this);"  status="statusAguarde" event="onchange" reRender="inscricaoFavorecido" action="#{consultarOrdensPagtosAgenciaPagBean.verificaIsentoNaoInformado}"/>						
						</br:brSelectOneMenu>	
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<f:verbatim><hr class="lin"> </f:verbatim> 

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup style="text-align:right;width: 750px" >
				<br:brCommandButton id="btnLimpar" style="margin-right:5px" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{consultarOrdensPagtosAgenciaPagBean.limparArgumentos}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton disabled="#{consultarOrdensPagtosAgenciaPagBean.disableArgumentosConsulta}" id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{consultarOrdensPagtosAgenciaPagBean.consultar}" onclick="javascript:desbloquearTela(); return validaCampos(document.forms[1], '#{msgs.label_ocampo}', 
				'#{msgs.label_necessario}', 
				'#{msgs.conOrdensPagtosAgenciaPag_label_data_credito_op_inicial}', 
				'#{msgs.conOrdensPagtosAgenciaPag_label_data_credito_op_final}',
				'#{msgs.conPagamentosFavorecido_label_tipo}',
				'#{msgs.conOrdensPagtosAgenciaPag_coluna_inscricao_favorecido}',
				'#{msgs.label_deve_ser_diferente_de_zeros}');">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><br> </f:verbatim>
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
			
				<app:scrollableDataTable id="dataTable" value="#{consultarOrdensPagtosAgenciaPagBean.listaGridOrdemPagtoAgePag}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
						  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
						</f:facet>		
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
							layout="spread" forceId="true" forceIdIndex="false"
							value="#{consultarOrdensPagtosAgenciaPagBean.itemSelecionado}" >
							<f:selectItems value="#{consultarOrdensPagtosAgenciaPagBean.listaControleOrdemPagtoAgePag}"/>
							<a4j:support event="onclick" reRender="btnLimparConsulta, btnImprimir, btnDetalhar" />	
						</t:selectOneRadio>
						<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
					
					
					<app:scrollableColumn styleClass="colTabRight" width="150px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.conOrdensPagtosAgenciaPag_coluna_agencia_pagadora}" style="width:150; text-align:center"/>
						</f:facet>
						<br:brOutputText value="#{result.cdAgenciaPagadora}"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="150px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_codigo_favorecido}" style="width:150; text-align:center"/>
						</f:facet>
						<br:brOutputText value="#{result.cdFavorecido}"/>
					</app:scrollableColumn>			
					
					<app:scrollableColumn styleClass="colTabRight" width="150px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.conOrdensPagtosAgenciaPag_coluna_inscricao_favorecido}" style="width:150; text-align:center"/>
						</f:facet>
						<br:brOutputText value="#{result.inscricaoFavorecidoFormatado}"/>
					</app:scrollableColumn>	
									
					
					<app:scrollableColumn styleClass="colTabLeft" width="300px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_nome_favorecido}" style="width:300; text-align:center"/>
						</f:facet>
						<br:brOutputText value="#{result.dsAbrevFavorecido}"/>
					</app:scrollableColumn>					
					
					<app:scrollableColumn styleClass="colTabRight" width="200px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.conOrdensPagtosAgenciaPag_coluna_cliente_pagador}" style="width:200; text-align:center"/>
						</f:facet>
						<br:brOutputText value="#{result.cpfCnpjFormatado}"/>
					</app:scrollableColumn>		
					
					<app:scrollableColumn styleClass="colTabLeft" width="300px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_nomeRazaoSocial}" style="width:300; text-align:center"/>
						</f:facet>
						<br:brOutputText value="#{result.dsClientePagador}"/>
					</app:scrollableColumn>						
					
					<app:scrollableColumn styleClass="colTabCenter" width="120px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_data_agendamento}" style="width:120; text-align:center"/>
						</f:facet>
						<br:brOutputText value="#{result.dtCreditoPagamento}" converter="datePdcConverter"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_numero_pagamento}" style="width:200; text-align:center"/>
						</f:facet>
						<br:brOutputText value="#{result.cdControlePagamento}"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="100px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_valor}" style="width:100; text-align:center"/>
						</f:facet>
						<br:brOutputText value="#{result.vlPagamentoClientePagador}" converter="decimalBrazillianConverter" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabCenter" width="100px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.conOrdensPagtosAgenciaPag_coluna_disponivel_ate}" style="width:100; text-align:center"/>
						</f:facet>
						<br:brOutputText value="#{result.dtDisponivelAte}" converter="datePdcConverter"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="100px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.conOrdensPagtosAgenciaPag_coluna_situacao}" style="width:100; text-align:center"/>
						</f:facet>
						<br:brOutputText value="#{result.cdMotivoSituacaoPagamento}"/>
					</app:scrollableColumn>					
					
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_forma_pagamento}" style="width:200; text-align:center"/>
						</f:facet>						
						<br:brOutputText value="#{result.descTipoRetornoPagamento}"/>
					</app:scrollableColumn>					
					 
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{consultarOrdensPagtosAgenciaPagBean.pesquisar}"  > 
					<f:facet name="first">
						<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					</f:facet>
					<f:facet name="fastrewind">
						<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1"  style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					</f:facet>
					<f:facet name="previous">
						<brArq:pdcCommandButton id="anterior" styleClass="bto1"  style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton id="proxima" styleClass="bto1"  style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					</f:facet>
					<f:facet name="fastforward">
						<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1"  style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton id="ultima" styleClass="bto1"  style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					</f:facet>
				</brArq:pdcDataScroller>	
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim> 

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup style="text-align:right;width: 750px" >
				<br:brCommandButton disabled="#{empty consultarOrdensPagtosAgenciaPagBean.listaGridOrdemPagtoAgePag}" id="btnLimparConsulta" style="margin-right:5px" styleClass="bto1" value="#{msgs.btn_limpar}" action="#{consultarOrdensPagtosAgenciaPagBean.limparPesquisa}">
					<brArq:submitCheckClient/>				
				</br:brCommandButton>
				<br:brCommandButton disabled="#{empty consultarOrdensPagtosAgenciaPagBean.listaGridOrdemPagtoAgePag}" id="btnImprimir" style="margin-right:5px" styleClass="bto1" value="#{msgs.btn_imprimir_lista}" onclick="javascript: desbloquearTela()" action="#{consultarOrdensPagtosAgenciaPagBean.imprimirRelatorio}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton disabled="#{empty consultarOrdensPagtosAgenciaPagBean.itemSelecionado}" id="btnDetalhar" styleClass="bto1" value="#{msgs.btn_detalhar}" action="#{consultarOrdensPagtosAgenciaPagBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>