<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j"%>

<brArq:form id="validarAutenticacaoComprovantesForm" name="validarAutenticacaoComprovantesForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.validarAutenticacaoComprovantes_argumentosDePesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3"  cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.validarAutenticacaoComprovantes_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" value="#{validarAutenticacaoComprovantesBean.banco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'validarAutenticacaoComprovantesForm','validarAutenticacaoComprovantesForm:txtBanco','validarAutenticacaoComprovantesForm:txtAgencia');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}"/>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-left:20px"  cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.validarAutenticacaoComprovantes_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{validarAutenticacaoComprovantesBean.agencia}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'validarAutenticacaoComprovantesForm','validarAutenticacaoComprovantesForm:txtAgencia','validarAutenticacaoComprovantesForm:txtConta');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}"/>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" style="margin-left:20px"  cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.validarAutenticacaoComprovantes_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
					<br:brInputText styleClass="HtmlInputTextBradesco"   onkeypress="onlyNum();"  value="#{validarAutenticacaoComprovantesBean.conta}" size="17" maxlength="13" id="txtConta"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}"/>
				</br:brPanelGrid>
			</br:brPanelGrid>	
						
			
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.validarAutenticacaoComprovantes_valorPagamento}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brInputText styleClass="HtmlInputTextBradesco"  value="#{validarAutenticacaoComprovantesBean.valor}" size="30" maxlength="18" id="txtValor"  converter="decimalBrazillianConverter" alt="decimalBr" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" disabled="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}">
		</br:brInputText>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.validarAutenticacaoComprovantes_dataPagamento}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup rendered="#{!validarAutenticacaoComprovantesBean.disableArgumentosConsulta}" >				
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataPagamento');" id="dataPagamento" value="#{validarAutenticacaoComprovantesBean.dataPagamento}" />
				</br:brPanelGroup>	
				<br:brPanelGroup rendered="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}">				
					<app:calendar id="dataPagamentoDes" value="#{validarAutenticacaoComprovantesBean.dataPagamento}" disabled="true"  />
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.validarAutenticacaoComprovantes_servico}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brSelectOneMenu id="tipoServico" value="#{validarAutenticacaoComprovantesBean.cdServico}" disabled="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}">
			<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
			<f:selectItems value="#{validarAutenticacaoComprovantesBean.listaServico}"/>			
			<a4j:support event="onchange" status="statusAguarde" reRender="modalidade, codBarras, numeroPagamento, cdProdtPgit" action="#{validarAutenticacaoComprovantesBean.carregaListaModalidade}" />	
		</br:brSelectOneMenu>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.validarAutenticacaoComprovantes_modalidade}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brSelectOneMenu id="modalidade" value="#{validarAutenticacaoComprovantesBean.cdModalidade}"  disabled="#{validarAutenticacaoComprovantesBean.cdServico == null || validarAutenticacaoComprovantesBean.cdServico == 0 || validarAutenticacaoComprovantesBean.disableArgumentosConsulta}">
			<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
			<f:selectItems value="#{validarAutenticacaoComprovantesBean.listaModalidade}"/>
			<a4j:support event="onchange" status="statusAguarde" reRender="codBarras,numeroPagamento,cdProdtPgit" action="#{validarAutenticacaoComprovantesBean.consultarServicosCatalogo}" />
		</br:brSelectOneMenu>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid id="codBarras" columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">
		<br:brPanelGroup rendered="#{validarAutenticacaoComprovantesBean.cdModalidade != null && validarAutenticacaoComprovantesBean.cdModalidade != 0 && validarAutenticacaoComprovantesBean.saidaConsultarServicosCatalogo.cdProdtPgit == 25}">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.detPagamentoIndividual_codigoBarras}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
				<br:brInputText id="codBarras1" size="16" maxlength="12" onkeypress="onlyNum();" value="#{validarAutenticacaoComprovantesBean.codBarras1}" disabled="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}" onkeyup="proximoCampo(12,'validarAutenticacaoComprovantesForm','validarAutenticacaoComprovantesForm:codBarras1','validarAutenticacaoComprovantesForm:codBarras2');" />
				<br:brInputText id="codBarras2" size="16" maxlength="12" onkeypress="onlyNum();" style="margin-left: 10px;" value="#{validarAutenticacaoComprovantesBean.codBarras2}" disabled="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}" onkeyup="proximoCampo(12,'validarAutenticacaoComprovantesForm','validarAutenticacaoComprovantesForm:codBarras2','validarAutenticacaoComprovantesForm:codBarras3');" />
				<br:brInputText id="codBarras3" size="16" maxlength="12" onkeypress="onlyNum();" style="margin-left: 10px;" value="#{validarAutenticacaoComprovantesBean.codBarras3}" disabled="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}" onkeyup="proximoCampo(12,'validarAutenticacaoComprovantesForm','validarAutenticacaoComprovantesForm:codBarras3','validarAutenticacaoComprovantesForm:codBarras4');" />
				<br:brInputText id="codBarras4" size="16" maxlength="12" onkeypress="onlyNum();" style="margin-left: 10px;" value="#{validarAutenticacaoComprovantesBean.codBarras4}" disabled="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}" />
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid id="numeroPagamento" columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">
		<br:brPanelGroup rendered="#{validarAutenticacaoComprovantesBean.cdModalidade != null && validarAutenticacaoComprovantesBean.cdModalidade != 0 && validarAutenticacaoComprovantesBean.saidaConsultarServicosCatalogo.cdProdtPgit != 25}">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_numero_pagamento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText id="numPagamento" size="40" maxlength="30" value="#{validarAutenticacaoComprovantesBean.numeroPagamento}" disabled="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}" />
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	<f:verbatim><hr class="lin"> </f:verbatim> 
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="margin-left:530px;"> 
			<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.validarAutenticacaoComprovantes_limpar}" action="#{validarAutenticacaoComprovantesBean.limpar}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="text-align:right;float:right;">
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.validarAutenticacaoComprovantes_consultarAutenticacao}" action="#{validarAutenticacaoComprovantesBean.consultar}" onclick="javascript: desbloquearTela(); return validaCampos(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.validarAutenticacaoComprovantes_banco}','#{msgs.validarAutenticacaoComprovantes_agencia}','#{msgs.validarAutenticacaoComprovantes_conta}','#{msgs.validarAutenticacaoComprovantes_digitoConta}', '#{msgs.validarAutenticacaoComprovantes_valorPagamento}' ,'#{msgs.validarAutenticacaoComprovantes_dataPagamento}', '#{msgs.validarAutenticacaoComprovantes_servico}' , '#{msgs.validarAutenticacaoComprovantes_modalidade}' ,'#{msgs.label_deve_ser_diferente_de_zeros}' ,'#{msgs.detPagamentoIndividual_codigoBarras}' ,'#{msgs.consultarPagamentosIndividual_grid_numero_pagamento}' ); " disabled="#{validarAutenticacaoComprovantesBean.disableArgumentosConsulta}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><br><br></f:verbatim>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" rendered="#{validarAutenticacaoComprovantesBean.cdAutenticacaoEletronica != null && cdAutenticacaoEletronica != ''}" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.validarAutenticacaoComprovantes_autenticacaoEletronica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{validarAutenticacaoComprovantesBean.cdAutenticacaoEletronica}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>	
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>  
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>