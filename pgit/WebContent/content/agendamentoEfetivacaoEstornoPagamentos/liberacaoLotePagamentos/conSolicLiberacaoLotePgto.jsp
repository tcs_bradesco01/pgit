<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conSolicLiberacaoLotePgtoForm" name="conSolicLiberacaoLotePgtoForm">
<h:inputHidden id="hiddenFoco" value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.foco}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	 	
   	<jsp:include page="/content/agendamentoEfetivacaoEstornoPagamentos/filtroIdentificacaoClienteContrato.jsp" flush="false" />
   	
	<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">
	    <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
			<br:brPanelGroup>
				<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.conPagamentosConsolidado_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<a4j:outputPanel id="painelArgumentos" style="width: 100%; text-align: left" ajaxRendered="true">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_data_solicitacao}:" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
			<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brPanelGroup rendered="#{(manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) && !manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.disableArgumentosConsulta}">
							<app:calendar id="dataInicialPagamento" value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.dataInicialPagamentoFiltro}" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialPagamento');">
							</app:calendar>
						</br:brPanelGroup>
						<br:brPanelGroup rendered="#{(!manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.disableArgumentosConsulta}">
							<app:calendar id="dataInicialPagamentoDes" value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.dataInicialPagamentoFiltro}" disabled="true" >
							</app:calendar>
						</br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
						<br:brPanelGroup rendered="#{(manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado)  && !manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.disableArgumentosConsulta}">
							<app:calendar id="dataFinalPagamento" value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.dataFinalPagamentoFiltro}" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFinalPagamento');">
			 				</app:calendar>	
						</br:brPanelGroup>	
						<br:brPanelGroup rendered="#{(!manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.disableArgumentosConsulta}">
							<app:calendar id="dataFinalPagamentoDes" value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.dataFinalPagamentoFiltro}" disabled="true">
			 				</app:calendar>	
						</br:brPanelGroup>	
					</br:brPanelGroup>			    
				</br:brPanelGrid>
			</a4j:outputPanel>
			
			<br:brPanelGrid columns="1" style="margin-top:15px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >	
				<h:selectBooleanCheckbox id="chkLoteInterno" value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.chkLoteInterno}" disabled="#{(!manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || 
						 manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.disableArgumentosConsulta}">
					<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="txtLoteInterno, cboTipoLayoutArquivo" 
						action="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.limparCamposLoteInterno}"/>
				</h:selectBooleanCheckbox>		
					
				<br:brPanelGroup>
					<br:brOutputText style="margin-left:3px" styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}:"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup style="width:100px; margin-bottom:5px" >
				</br:brPanelGroup>	
				
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brInputText id="txtLoteInterno" value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.loteInterno}" style="margin-left:15px" size="18" maxlength="9" 
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" disabled="#{!manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.chkLoteInterno || manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.disableArgumentosConsulta}"/>
				</br:brPanelGroup>
				
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-top:15px">	
				<h:selectBooleanCheckbox id="chkSituacaoSolicitacao" value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.chkSituacaoSolicitacao}" disabled="#{(!manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || 
						 manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.disableArgumentosConsulta}" >
						 
					<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="cboSituacaoSolicitacao, cboMotivoSituacao" 
						action="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.limparSituacaoSolicitacao}"/>
				</h:selectBooleanCheckbox>		
					
				<br:brPanelGroup>
					<br:brOutputText style="margin-left:3px" styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_situacao_solicitacao}:"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup style="width:45px; margin-bottom:5px" >
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_motivo_solicitacao}:"/>
				</br:brPanelGroup>			
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>				
					<br:brSelectOneMenu id="cboSituacaoSolicitacao" value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.cboSituacaoSolicitacao}" style="margin-left:15px" styleClass="HtmlSelectOneMenuBradesco" 
						disabled="#{!manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.chkSituacaoSolicitacao || manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.disableArgumentosConsulta}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.listaSituacaoSolicitacaoFiltro}" />
						<a4j:support oncomplete="javascript:foco(this);"  status="statusAguarde" event="onchange" reRender="cboMotivoSituacao" action="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.listarConsultarMotivoSituacaoFiltro}"/>												
					</br:brSelectOneMenu>
				</br:brPanelGroup>
				
				<br:brPanelGroup style="width:65px; margin-bottom:5px" >
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brSelectOneMenu id="cboMotivoSituacao"  value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.cboMotivoSituacao}" 
						style="margin-left:5px" styleClass="HtmlSelectOneMenuBradesco" 
						disabled="#{!manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.chkSituacaoSolicitacao || manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.disableArgumentosConsulta}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.listaMotivoSituacaoFiltro}" />							
					</br:brSelectOneMenu>
				</br:brPanelGroup>
			</br:brPanelGrid>
		
			<f:verbatim><hr class="lin"> </f:verbatim>
			
			<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{pagamentosBean.limpaTelaInicialLote}" 
					actionListener="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.limpaListaLote}"  style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnConsultar" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);" 
						value="#{msgs.consultarPagamentosIndividual_btn_consultar}" onclick="desbloquearTela();return validaCamposConsultarExclusaoLotePagamento(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
						 '#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}', '#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_tipo_layout_arquivo}', '#{msgs.manterSolicitacaoExclusaoLotePagamentos_situacao_solicitacao}',
						 '#{msgs.label_deve_ser_diferente_de_zeros}');" 
						 disabled="#{(!manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || 
						 manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.disableArgumentosConsulta}" action="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.carregaListaLotePagamento}" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>
	</a4j:outputPanel>
	
	<f:verbatim><br></f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
		
			<app:scrollableDataTable id="dataTable" value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.listaGridLotePagto}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.itemSelecionadoLista}">
						<f:selectItems value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.listaControleRadios}"/>
						<a4j:support event="onclick" reRender="btnIncluir, btnDetalhar, btnExcluir"/>
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_empresa_gestora_contrato}" style="width:250; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsEmpresa}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_numero_contrato}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrSequenciaContratoNegocio}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_numero_solicitacao}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrSolicitacaoPagamentoIntegrado}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabCenter" width="130px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_data_solicitacao}"  style="width:130; text-align:center"/>
				    </f:facet>
				   <br:brOutputText value="#{result.dataHoraFormatada}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_numero_lote_interno}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrLoteInterno}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_tipo_layout}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoLayoutArquivo}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="150px">			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_tipo_de_servico}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsProdutoServicoOperacao}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_situacao_solicitacao}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoSolicitaoPgto}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_motivo_solicitacao}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsMotivoSolicitacao}" />
				</app:scrollableColumn>
				
			</app:scrollableDataTable>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pesquisarConsultar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
		<br:brPanelGrid columns="4" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
			<br:brPanelGroup style="width:750px">
				<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.conPagamentosConsolidado_btn_limpar}" disabled="#{empty manterSolicitacaoLiberacaoLoteSemConsultaBean.listaGridLotePagto}" action="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.limparInformacaoConsulta}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" styleClass="bto1" style="margin-left:5px;margin-right:5px;" value="#{msgs.btn_incluir}" action="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.incluir}"> 
					<brArq:submitCheckClient/>
				</br:brCommandButton>										
				<br:brCommandButton id="btnDetalhar" style="margin-left:5px;margin-right:5px;" styleClass="bto1" disabled="#{empty manterSolicitacaoLiberacaoLoteSemConsultaBean.itemSelecionadoLista}" value="#{msgs.conPagamentosConsolidado_btn_detalhar}" 
					action="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" disabled="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.desabilitaBtnExcluir}"  styleClass="bto1" value="#{msgs.btn_excluir}" action="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.excluir}"> 
					<brArq:submitCheckClient/> 
				</br:brCommandButton> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>	
			    	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
	
	<t:inputHidden value="#{manterSolicitacaoLiberacaoLoteSemConsultaBean.pagamentosBean.disableArgumentosConsulta}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conSolicLiberacaoLotePgtoForm:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>