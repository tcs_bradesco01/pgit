<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManSolEstornoPagto" name="conManSolEstornoPagto" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="radioTipoFiltro" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}">  
			<f:selectItem itemValue="0" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_cliente}" />
			<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_contrato}" />
			<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="formulario,panelBotoes,dataTable,dataScroller" action="#{consultaSolicitacaoEstornoOPBean.limparTelaPrincipal}" status="statusAguarde" />			
		</t:selectOneRadio>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">			
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<t:radio for="radioTipoFiltro" index="0" />
			</br:brPanelGroup>		
		</br:brPanelGrid>		
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
 
		<a4j:region id="regionFiltro">
		<t:selectOneRadio id="rdoFiltroCliente" value="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '0' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="" />
		</t:selectOneRadio>
		</a4j:region>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="0" />			
			
			 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cnpj}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'conManSolEstornoPagto','conManSolEstornoPagto:txtCnpj','conManSolEstornoPagto:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0'  || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'conManSolEstornoPagto','conManSolEstornoPagto:txtFilial','conManSolEstornoPagto:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta }" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0'  || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCnpj}"onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="1" />			
			
			 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cpf}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
				    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta  }" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
				    onkeyup="proximoCampo(9,'conManSolEstornoPagto','conManSolEstornoPagto:txtCpf','conManSolEstornoPagto:txtControleCpf');" />
				    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
			<t:radio for="rdoFiltroCliente" index="2" />		
			
			 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '2' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta }" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
			</a4j:outputPanel>
			
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="3" />			
			
			<a4j:outputPanel id="panelBanco" ajaxRendered="true">
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'conManSolEstornoPagto','conManSolEstornoPagto:txtBanco','conManSolEstornoPagto:txtAgencia');"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'conManSolEstornoPagto','conManSolEstornoPagto:txtAgencia','conManSolEstornoPagto:txtConta');"/>
				
			</a4j:outputPanel>
		
			<a4j:outputPanel id="panelConta" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}"
							 value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" style="margin-right:5px"/>
					</br:brPanelGroup>
				</br:brPanelGrid>				
			</a4j:outputPanel>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
		   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta }" value="#{msgs.consultarPagamentosIndividual_btn_consultar_cliente}" action="#{consultaSolicitacaoEstornoOPBean.btnConsultarCliente}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
		   		 onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_banco}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');"
					actionListener="#{consultaSolicitacaoEstornoOPBean.limparArgsListaAposPesquisaClienteContrato}" >		
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</a4j:outputPanel>
		</br:brPanelGrid>	
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_cliente}:"/>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>	
			
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	    
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top: 9" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	 
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescCliente}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescCliente}"/>
			</br:brPanelGroup>	    
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescCliente}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescCliente}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	
	    <f:verbatim><hr class="lin"></f:verbatim>     
	    
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
			<br:brPanelGroup>			
				<t:radio for="radioTipoFiltro" index="1" />
			</br:brPanelGroup>		
		</br:brPanelGrid>	    
	    
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	    
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:20px">
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="empresaGestora" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px" >
							<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaEmpresaGestora}" />								
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
	
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="tipoContrato" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
							<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaTipoContrato}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroFiltro}" size="12" maxlength="10" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}">
					    </br:brInputText>	
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
	    <f:verbatim><hr class="lin"></f:verbatim>	
    
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;">
	   		<br:brCommandButton id="btoConsultarContrato" disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}" styleClass="bto1" value="#{msgs.consultarPagamentosIndividual_btn_consultar_contrato}" action="#{consultaSolicitacaoEstornoOPBean.btnConsultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.consultarPagamentosIndividual_tipo_contrato}','#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');"
				actionListener="#{consultaSolicitacaoEstornoOPBean.limparArgsListaAposPesquisaClienteContrato}"   		>		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGrid>	  
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	 
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}"/>
			</br:brPanelGroup>	    
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
	    <f:verbatim><hr class="lin"></f:verbatim>  
	    
	    <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
			<br:brPanelGroup>
				<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<a4j:outputPanel id="painelArgumentos" style="width: 100%; text-align: left" ajaxRendered="true">	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
	
				<br:brPanelGroup>			
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_servico}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
					    	<br:brSelectOneMenu id="cboTipoServico" value="#{consultaSolicitacaoEstornoOPBean.tipoServicoFiltro}" styleClass="HtmlSelectOneMenuBradesco"
					    	disabled="#{(!consultaSolicitacaoEstornoOPBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || estornoOrdensPagamentosBean.disableArgumentosConsulta}" >
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{consultaSolicitacaoEstornoOPBean.listaTipoServicoFiltro}" />						
							</br:brSelectOneMenu>
						</br:brPanelGroup>					
					</br:brPanelGrid>	
				</br:brPanelGroup>	
					
			</br:brPanelGrid> 
								
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_de_solicitacao}:" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
		
			<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >									
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brPanelGroup rendered="#{(consultaSolicitacaoEstornoOPBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) &&  !consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}">
							<app:calendar id="dataInicialSolicitacao" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialSolicitacao');" value="#{consultaSolicitacaoEstornoOPBean.dataInicialSolicitacaoFiltro}" >
							</app:calendar>
						</br:brPanelGroup>
						<br:brPanelGroup rendered="#{(!consultaSolicitacaoEstornoOPBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) ||   consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}">
							<app:calendar id="dataInicialSolicitacaoDes" value="#{consultaSolicitacaoEstornoOPBean.dataInicialSolicitacaoFiltro}" disabled="true" >
							</app:calendar>
						</br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
						<br:brPanelGroup rendered="#{(consultaSolicitacaoEstornoOPBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) &&  !consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}">
							<app:calendar id="dataFinalSolicitacao" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFinalSolicitacao');" value="#{consultaSolicitacaoEstornoOPBean.dataFinalSolicitacaoFiltro}" >
							</app:calendar>	
						</br:brPanelGroup>	
						<br:brPanelGroup rendered="#{(!consultaSolicitacaoEstornoOPBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) ||   consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}">
							<app:calendar id="dataFinalSolicitacaoDes" value="#{consultaSolicitacaoEstornoOPBean.dataFinalSolicitacaoFiltro}" disabled="true">
							</app:calendar>	
						</br:brPanelGroup>	
					</br:brPanelGroup>			    
				</br:brPanelGrid>
			</a4j:outputPanel>		
				
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>		

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >			
				<br:brPanelGroup>	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
						<br:brSelectBooleanCheckbox id="chkSituacaoMotivo" value="#{consultaSolicitacaoEstornoOPBean.chkSituacaoMotivo}" disabled="#{(!consultaSolicitacaoEstornoOPBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}">
						<%-- <t:radio for="sorCliente" index="0" />	 --%>
						<a4j:support  event="onclick" reRender="cboSituacaoSolicitacao,cboMotivo" />						
						<%-- <t:radio for="sorCliente" index="1" />	--%>
						</br:brSelectBooleanCheckbox>
					</br:brPanelGrid>	
				</br:brPanelGroup>
		
				<br:brPanelGroup>			
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_situacao_solicitacao}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboSituacaoSolicitacao" value="#{consultaSolicitacaoEstornoOPBean.situacaoSolicitacaoFiltro}" styleClass="HtmlSelectOneMenuBradesco"
							disabled="#{!consultaSolicitacaoEstornoOPBean.chkSituacaoMotivo || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}" style="margin-right:20px">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{consultaSolicitacaoEstornoOPBean.listaSituacaoSolicitacaoFiltro}" />						
								<a4j:support  oncomplete="javascript:foco(this);" status="statusAguarde" event="onchange" reRender="cboMotivo" action="#{consultaSolicitacaoEstornoOPBean.listarConsultarMotivoSituacaoEstorno}" />						
							</br:brSelectOneMenu>
						</br:brPanelGroup>					
					</br:brPanelGrid>				
				</br:brPanelGroup>				
				
				<br:brPanelGroup>		
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_motivo}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboMotivo" value="#{consultaSolicitacaoEstornoOPBean.motivoFiltro}" styleClass="HtmlSelectOneMenuBradesco"
							  disabled="#{!consultaSolicitacaoEstornoOPBean.chkSituacaoMotivo || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta ||
							  consultaSolicitacaoEstornoOPBean.situacaoSolicitacaoFiltro == 0 || consultaSolicitacaoEstornoOPBean.situacaoSolicitacaoFiltro == null}" >
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{consultaSolicitacaoEstornoOPBean.listaMotivoSituacaoFiltro}" />												
							</br:brSelectOneMenu>
						</br:brPanelGroup>					
					</br:brPanelGrid>				
				</br:brPanelGroup>			
				
			</br:brPanelGrid>
			
			
			
			<f:verbatim><hr class="lin"></f:verbatim>	
			
			<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{consultaSolicitacaoEstornoOPBean.limpar}" style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnConsultar" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);" value="#{msgs.consultarPagamentosIndividual_btn_consultar}" action="#{consultaSolicitacaoEstornoOPBean.consultarSolicitacaoEstornoPagtos}"
					onclick="javascript:desbloquearTela(); return validarConsultar(document.forms[1],
						'#{msgs.label_ocampo}', 
						 '#{msgs.label_necessario}',
						 '#{msgs.consultarPagamentosIndividual_tipo_servico}',
						 '#{msgs.label_data_de_solicitacao}',
						 '#{msgs.label_situacao_solicitacao}',
						 '#{msgs.label_motivo}',
						 '#{msgs.label_tipo_de_servico}',
						 '#{msgs.label_necessario_selecionar_argumento_pesquisa}');"
						disabled="#{(!consultaSolicitacaoEstornoOPBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}"> 				 
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid> 
			
		</a4j:outputPanel>	
	</a4j:outputPanel>				   
		
		<f:verbatim><br></f:verbatim> 
	
			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
	
				<app:scrollableDataTable id="dataTable" value="#{consultaSolicitacaoEstornoOPBean.consultaSolicitacaoEstornoOPSaidaDTO.ocorrencias}" var="result" 
					rows="10" rowIndexVar="parametroKey" 
					rowClasses="tabela_celula_normal, tabela_celula_destaque" 
					width="100%" >
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
						value="#{consultaSolicitacaoEstornoOPBean.itemSelecionadoListaFiltro}">
							<f:selectItems value="#{consultaSolicitacaoEstornoOPBean.selecItemManterSolicEstPagtos}"/>
							<a4j:support event="onclick" reRender="panelBotoes"/>
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>
				
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_cpf_cnpj_cliente_pagador}" style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdCpfCnpjParticipante}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_nome_razao}"  style="width:250px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdPessoaCompleto}" /> 
					</app:scrollableColumn>		 		
	
					<app:scrollableColumn styleClass="colTabRight" width="220px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_numero_contrato}" style="width:220; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nrSequenciaContratoNegocio}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_numero_solicitacao}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nrSolicitacaoPagamentoIntegrado}" />
					</app:scrollableColumn>										
	
					<app:scrollableColumn styleClass="colTabCenter" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_datasolicitacao}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsDataSolicitacao} - #{result.dsHoraSolicitacao}" />
					</app:scrollableColumn>	
	
					<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_modalidade}" style="width:150; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsModalidade}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.PGIC0108_label_situacao_solicitacao}" style="width:150; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsSituacaoSolicitante}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_motivo}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsMotivoSituacaoSolicitante}" />
					</app:scrollableColumn>	
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{consultaSolicitacaoEstornoOPBean.pesquisarFiltro}" >
				 <f:facet name="first">
				    <brArq:pdcCommandButton id="primeira"
				      styleClass="bto1"
				      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton id="proxima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton id="ultima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  </f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		 	<br:brPanelGrid columns="4" style="text-align:right" cellpadding="0" cellspacing="0" border="0">	
				<br:brPanelGroup>
					<br:brCommandButton disabled="#{consultaSolicitacaoEstornoOPBean.consultaSolicitacaoEstornoOPSaidaDTO.ocorrencias == null}" id="btnLimparGrid" styleClass="bto1" value="#{msgs.btn_limpar}" style="margin-right:5px" action="#{consultaSolicitacaoEstornoOPBean.limparGrid}" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			
				<br:brPanelGroup>
					<br:brCommandButton id="btnDetalhar" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_detalhar}" action="#{consultaSolicitacaoEstornoOPBean.detalhar}" 
						disabled="#{consultaSolicitacaoEstornoOPBean.itemSelecionadoListaFiltro == null}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>

			</br:brPanelGrid>		
		</a4j:outputPanel>	
		
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   
		
	<t:inputHidden value="#{consultaSolicitacaoEstornoOPBean.disableArgumentosConsulta}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conManSolEstornoPagto:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />

</brArq:form>