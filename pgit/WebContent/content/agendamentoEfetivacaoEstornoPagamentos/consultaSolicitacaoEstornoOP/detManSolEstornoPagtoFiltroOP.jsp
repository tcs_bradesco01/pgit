<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detManSolEstornoPagtoFiltro" name="detManSolEstornoPagtoFiltro">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detPagamentoIndividual_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.nrCnpjCpf}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsRazaoSocial}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsEmpresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_numeroContrato}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.nroContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_descricaoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsContrato}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detPagamentosConsolidado_situacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.cdSituacaoContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_solicitacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsNumero}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.grid_situacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsSituacao}"/>    
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_motivo}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsMotivo}"/>    
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_datasolicitacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsDataSolicitacao}" />
		</br:brPanelGroup>				
		<br:brPanelGroup>
			
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManSolEstornoPagto_valor_programado_para_estorno}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsValorProgramadoEstorno}" converter="decimalBrazillianConverter" />    
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_observacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.observacao}" />    
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>		
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManSolEstornoPagto_conta_de_estorno}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsBanco}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsAgencia}"/>    
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsConta}"/>    
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>					

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{consultaSolicitacaoEstornoOPBean.dsTipo}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>	

	<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 

			<app:scrollableDataTable id="dataTable" value="#{consultaSolicitacaoEstornoOPBean.listaConsultarPagtosSolicitacaoEstornoFiltroDetalhar}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false"
					value="#{consultaSolicitacaoEstornoOPBean.itemSelecionadoListaDetalhar}">
						<f:selectItems value="#{consultaSolicitacaoEstornoOPBean.selecItemListaControleRadioDetalhar}"/>
						<a4j:support event="onclick" reRender="btnDetalhar"/>
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_cpf_cnpj_cliente_pagador}" style="width:200; text-align:center"/> 
				    </f:facet>
   				    <br:brOutputText value="#{result.cpfCnpjClientePagador}"  />
				</app:scrollableColumn>	

				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_nomeRazao}" style="width:250px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsRazaoSocial}"  />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabRight" width="230px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_numero_contrato}" style="width:230px; text-align:center"/>
				    </f:facet>
				  	<br:brOutputText value="#{result.nrSequenciaContratoNegocio}"  />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="230px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_tipoServico}" style="width:230px; text-align:center"/>
				    </f:facet>
   				  	<br:brOutputText value="#{result.dsOperacaoProdutoServico}"  />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="230px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_modalidade}" style="width:230px; text-align:center"/>
				    </f:facet>
   				  	<br:brOutputText value="#{result.dsOperacaoProdutoRelacionado}"  />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabRight" width="230px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_numero_pagamento}" style="width:230px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdControlePagamento}" converter="spaceConverter" escape="false"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabCenter" width="230px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_data_de_pagamento}" style="width:230px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dtCredito}" converter="datePdcConverter" />
				</app:scrollableColumn>		

				<app:scrollableColumn styleClass="colTabRight" width="230px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_valor_pagamento}" style="width:230px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.vlrEfetivacaoPagamento}" converter="decimalBrazillianConverter" />
				</app:scrollableColumn>		

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_favorecido_beneficiario}" style="width:200px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.favorecidoBeneficiarioFormatado}" />
				</app:scrollableColumn>		

				<app:scrollableColumn styleClass="colTabRight" width="230px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_bco_ag_conta_credito}" style="width:230px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.bancoAgenciaContaFavorecidoFormatada}" />
				</app:scrollableColumn>		

				<app:scrollableColumn styleClass="colTabLeft" width="230px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_situacao_pagto}" style="width:230px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoOperacaoPagamento}" />
				</app:scrollableColumn>		

			</app:scrollableDataTable>			
		</br:brPanelGroup>
	</br:brPanelGrid>	

 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{consultaSolicitacaoEstornoOPBean.pesquisarDetalhar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{consultaSolicitacaoEstornoOPBean.voltarConsultar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnDetalhar" disabled="#{empty consultaSolicitacaoEstornoOPBean.itemSelecionadoListaDetalhar}" styleClass="bto1" style="margin-left:5px" value="#{msgs.detPagamentosConsolidado_btn_detalhar}" action="#{consultaSolicitacaoEstornoOPBean.detalharFiltro}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>