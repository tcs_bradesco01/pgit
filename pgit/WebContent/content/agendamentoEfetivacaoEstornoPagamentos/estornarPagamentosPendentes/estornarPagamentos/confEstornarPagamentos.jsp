<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="confEstonarPagamentosForm" name="confEstonarPagamentosForm">
<h:messages showDetail="true"> </h:messages>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}: "/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{estornarPagamentosBean.cpfCnpj}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}: "/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{estornarPagamentosBean.nomeRazao}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}: "/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{estornarPagamentosBean.empresaConglomerado}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}: "/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{estornarPagamentosBean.numeroContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}: "/> 			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{estornarPagamentosBean.descricaoContrato}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}: "/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{estornarPagamentosBean.situacao}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_pagtos_serem_estornados}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{estornarPagamentosBean.qtdePagtosSeremRecuperados}" />
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_total_pagtos_serem_estornados}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{estornarPagamentosBean.vlrTotalPagtosSeremRecuperados}" converter="decimalBrazillianConverter" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_grid_tipo_servico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{estornarPagamentosBean.dsTipoServicoSelecionado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_modalidade_pagamento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{estornarPagamentosBean.dsModalidade}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" rendered="#{estornarPagamentosBean.opcaoCreditoConta}">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.detServicos_tipoConsultaSaldo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
				
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="setor" value="#{estornarPagamentosBean.codFormaEstornoPgto}" styleClass="HtmlSelectOneMenuBradesco">
						<f:selectItems  value="#{estornarPagamentosBean.listaSetor}" />								
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
	</br:brPanelGrid>	
		
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		</br:brPanelGroup>		
		<br:brPanelGroup>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_observacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
	    <br:brPanelGroup>
			<br:brInputTextarea rows="4" cols="100" value="#{estornarPagamentosBean.observacao}" id="observacao" onfocus="limitarTextArea(this,200);"  onmouseup="cleanClipboard()" onkeyup="limitarTextArea(this,200);">
				<brArq:commonsValidator type="required" arg="#{msgs.label_observacao}" server="false" client="true"/>
		    </br:brInputTextarea>	
		</br:brPanelGroup>					
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 
			
				<app:scrollableDataTable id="dataTable" value="#{estornarPagamentosBean.listaGridEstornoPagamentosSelecionados}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="750" >
					
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.consultarPagamentosIndividual_grid_cnpj_cpf_cliente_pagador}" style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdCpfCnpjFormatado}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_nome_razao_social}"  style="width:250px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nmRazaoSocial}" />
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabRight" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_numero_contrato}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nrContrato}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabLeft" width="220px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_tipo_servico}" style="width:220; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdResumoProdutoServico}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_modalidade}"  style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsOperacaoProdutoServico}" />
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabLeft" width="130px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_numero_pagamento}" style="width:130; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.cdControlePagamento}" converter="spaceConverter" escape="false"/>
					</app:scrollableColumn>	
	
					<app:scrollableColumn styleClass="colTabCenter" width="150px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_data_pagamento}" style="width:150; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dtCraditoPagamentoFormatada}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="150px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_valor_pagamento}" style="width:150; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.vlEfetivoPagamentoFormatado}" />
					</app:scrollableColumn>		
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_favorecido_beneficiario}" style="width:200px; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.favorecidoBeneficiarioFormatado}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabRight" width="220px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_bco_ag_conta_credito}" style="width:220; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.bancoAgenciaContaCreditoFormatado}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabRight" width="220px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.label_bco_ag_conta_debito}" style="width:220; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.bancoAgenciaContaDebitoFormatado}" />
					</app:scrollableColumn>	
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.consultarPagamentosIndividual_grid_situacao_pagamento}" style="width:200; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsSituacaoOperacaoPagamento}" />
					</app:scrollableColumn>		
					
			</app:scrollableDataTable>
		</br:brPanelGroup>
		
	</br:brPanelGrid>	
	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{estornarPagamentosBean.pesquisarSelecionados}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="width:100%; text-align:left" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{estornarPagamentosBean.voltarEstornar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup> 
		<br:brPanelGroup style="text-align:right; width:100%; " >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.btn_avancar}" action="#{estornarPagamentosBean.avancar}" onclick="javascript: return validateForm(document.forms[1]);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>  
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
