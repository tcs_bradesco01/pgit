<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incSolicAntecipacaoPostergarLotePagtoForm"
	name="incSolicAntecipacaoPostergarLotePagtoForm">
	<h:inputHidden id="hiddenFoco"
		value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.foco}" />

	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"
		cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<t:selectOneRadio id="radioTipoFiltro"
				value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado}"
				styleClass="HtmlSelectOneRadioBradesco" layout="spread"
				forceId="true" forceIdIndex="false"
				disabled="#{manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}">
				<f:selectItem itemValue="0"
					itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_cliente}" />
				<f:selectItem itemValue="1"
					itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_contrato}" />
				<f:selectItem itemValue="2"
					itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_conta_debito}" />
				<a4j:support oncomplete="javascript:foco(this);" event="onclick"
					reRender="formulario,dataTable,btnLimparCampos,btnConsultar"
					action="#{manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.limpaTelaPrincipalLote}"
					status="statusAguarde" actionListener="#{pagamentosBean.chamarAposDesabilitar}"/>
			</t:selectOneRadio>
		</br:brPanelGrid>

		<a4j:outputPanel id="formulario" style="width: 100%; text-align: left"
			ajaxRendered="true">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
				style="text-align:left;">
				<br:brPanelGroup>
					<t:radio for="radioTipoFiltro" index="0" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%"
				style="margin-top:6px;margin-left:20px">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.consultarPagamentosIndividual_label_identificacao_cliente}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<a4j:region id="regionFiltro">
				<t:selectOneRadio id="rdoFiltroCliente"
					value="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado}"
					onclick="submit();" styleClass="HtmlSelectOneRadioBradesco"
					layout="spread" forceId="true" forceIdIndex="false"
					disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '0' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}">
					<f:selectItem itemValue="0" itemLabel="" />
					<f:selectItem itemValue="1" itemLabel="" />
					<f:selectItem itemValue="2" itemLabel="" />
					<f:selectItem itemValue="3" itemLabel="" />
				</t:selectOneRadio>
			</a4j:region>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="0" />

				<a4j:outputPanel id="panelCnpj" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_cnpj}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
						<br:brInputText id="txtCnpj"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							onkeyup="proximoCampo(9,'incSolicAntecipacaoPostergarLotePagtoForm','incSolicAntecipacaoPostergarLotePagtoForm:txtCnpj','incSolicAntecipacaoPostergarLotePagtoForm:txtFilial');"
							style="margin-right: 5" converter="javax.faces.Long"
							onkeypress="onlyNum();" size="11" maxlength="9"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
							styleClass="HtmlInputTextBradesco"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" />
						<br:brInputText id="txtFilial"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							onkeyup="proximoCampo(4,'incSolicAntecipacaoPostergarLotePagtoForm','incSolicAntecipacaoPostergarLotePagtoForm:txtFilial','incSolicAntecipacaoPostergarLotePagtoForm:txtControle');"
							style="margin-right: 5" converter="javax.faces.Integer"
							onkeypress="onlyNum();" size="5" maxlength="4"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
							styleClass="HtmlInputTextBradesco"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" />
						<br:brInputText id="txtControle"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							style="margin-right: 5" converter="javax.faces.Integer"
							onkeypress="onlyNum();" size="3" maxlength="2"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
							styleClass="HtmlInputTextBradesco"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" />
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="1" />

				<a4j:outputPanel id="panelCpf" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_cpf}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
						<br:brInputText styleClass="HtmlInputTextBradesco"
							style="margin-right: 5" converter="javax.faces.Long"
							onkeypress="onlyNum();"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpf}"
							size="11" maxlength="9" id="txtCpf"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							onkeyup="proximoCampo(9,'incSolicAntecipacaoPostergarLotePagtoForm','incSolicAntecipacaoPostergarLotePagtoForm:txtCpf','incSolicAntecipacaoPostergarLotePagtoForm:txtControleCpf');" />
						<br:brInputText styleClass="HtmlInputTextBradesco"
							onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
							converter="javax.faces.Integer" onkeypress="onlyNum();"
							disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
							value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCpf}"
							maxlength="2" size="3" id="txtControleCpf" />
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="2" />

				<a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brInputText styleClass="HtmlInputTextBradesco"
						disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '2' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}"
						size="71" maxlength="70" id="txtNomeRazaoSocial" />
				</a4j:outputPanel>

			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<t:radio for="rdoFiltroCliente" index="3" />

				<a4j:outputPanel id="panelBanco" ajaxRendered="true">

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_banco}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText styleClass="HtmlInputTextBradesco"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
						onkeypress="onlyNum();" converter="javax.faces.Integer"
						disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdBanco}"
						size="4" maxlength="3" id="txtBanco"
						onkeyup="proximoCampo(3,'incSolicAntecipacaoPostergarLotePagtoForm','incSolicAntecipacaoPostergarLotePagtoForm:txtBanco','incSolicAntecipacaoPostergarLotePagtoForm:txtAgencia');" />
				</a4j:outputPanel>

				<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_agencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brInputText styleClass="HtmlInputTextBradesco"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
						onkeypress="onlyNum();" converter="javax.faces.Integer"
						disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}"
						size="6" maxlength="5" id="txtAgencia"
						onkeyup="proximoCampo(5,'incSolicAntecipacaoPostergarLotePagtoForm','incSolicAntecipacaoPostergarLotePagtoForm:txtAgencia','incSolicAntecipacaoPostergarLotePagtoForm:txtConta');" />

				</a4j:outputPanel>

				<a4j:outputPanel id="panelConta" ajaxRendered="true">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_conta}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText styleClass="HtmlInputTextBradesco"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								converter="javax.faces.Long" onkeypress="onlyNum();"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdContaBancaria}"
								size="17" maxlength="13" id="txtConta" style="margin-right:5px" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" width="100%" cellpadding="0"
				cellspacing="0">
				<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true"
					style="width:100%; text-align: right">
					<br:brCommandButton id="btoConsultarCliente"
						actionListener="#{manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.limparArgumentossListaAposPesquisaClienteContrato}"
						styleClass="bto1"
						disabled="#{empty filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado }"
						value="#{msgs.consultarPagamentosIndividual_btn_consultar_cliente}"
						action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarClientes}"
						style="cursor:hand;"
						onmouseover="javascript:alteraBotao('visualizacao', this.id);"
						onmouseout="javascript:alteraBotao('normal', this.id);"
						onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_banco}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</a4j:outputPanel>
			</br:brPanelGrid>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
				value="#{msgs.consultarPagamentosIndividual_label_cliente}:" />
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"
						style="margin-right: 5" />
					<br:brOutputText styleClass="HtmlOutputTextBradesco"
						value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}" />
				</br:brPanelGroup>

				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"
						style="margin-right: 5" />
					<br:brOutputText styleClass="HtmlOutputTextBradesco"
						value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%" style="margin-top: 9">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.consultarPagamentosIndividual_title_contrato}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_empresa_gestora_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescCliente}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_numero_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescCliente}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescCliente}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_situacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescCliente}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
				style="text-align:left;">
				<br:brPanelGroup>
					<t:radio for="radioTipoFiltro" index="1" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%"
				style="margin-top:6px;margin-left:20px">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.consultarPagamentosIndividual_label_identificacao_contrato}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0"
				style="margin-left:20px">

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.label_empresa_gestora_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="empresaGestora"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraFiltro}"
								styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
								<f:selectItems
									value="#{filtroAgendamentoEfetivacaoEstornoBean.listaEmpresaGestora}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_tipo_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="tipoContrato"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoContratoFiltro}"
								styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
								<f:selectItems
									value="#{filtroAgendamentoEfetivacaoEstornoBean.listaTipoContrato}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup style="width:20px; margin-bottom:5px">
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.consultarPagamentosIndividual_numero_contrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText id="numero" styleClass="HtmlInputTextBradesco"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								onkeypress="onlyNum();"
								value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroFiltro}"
								size="12" maxlength="10"
								disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"></br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" width="100%" cellpadding="0"
				cellspacing="0" style="text-align:right;">
				<br:brCommandButton id="btoConsultarContrato"
					actionListener="#{manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.limparArgumentossListaAposPesquisaClienteContrato}"
					disabled="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1' || manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}"
					styleClass="bto1"
					value="#{msgs.consultarPagamentosIndividual_btn_consultar_contrato}"
					action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarContrato}"
					style="cursor:hand;"
					onmouseover="javascript:alteraBotao('visualizacao', this.id);"
					onmouseout="javascript:alteraBotao('normal', this.id);"
					onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.consultarPagamentosIndividual_tipo_contrato}','#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGrid>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.consultarPagamentosIndividual_title_contrato}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_numero_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.consultarPagamentosIndividual_situacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.conPagamentosConsolidado_argumentos_pesquisa}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<a4j:outputPanel id="painelArgumentos"
				style="width: 100%; text-align: left" ajaxRendered="true">

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<t:selectOneRadio id="radioTipoAntecipacao"
							disabled="#{(!manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || 
					 		manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta || manterSolicAntecipacaoPostergarLotePagamentoBean.gerenciaTxtLote}"
							value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.tipoAgendamento}"
							styleClass="HtmlSelectOneRadioBradesco" forceId="true"
							forceIdIndex="false">
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_antecipar}" />
							<f:selectItem itemValue="2" itemLabel="#{msgs.label_postergar}" />
							<a4j:support oncomplete="javascript:foco(this);" event="onclick"
								status="statusAguarde" reRender="novaDataAgendamento,btnConsultar"
								action="#{manterSolicAntecipacaoPostergarLotePagamentoBean.habilitaCampoNovaDataAgendamento}" />
						</t:selectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText style="margin-left:3px"
							styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brInputText id="txtLoteInterno"
							value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.loteInterno}"
							size="18" maxlength="9"
							onkeypress="onlyNum();"
							disabled="#{(!manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || 
					 manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta || manterSolicAntecipacaoPostergarLotePagamentoBean.gerenciaTxtLote}">
					 	<a4j:support event="onblur" reRender="btnConsultar" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					 </br:brInputText>
					 	
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

			</a4j:outputPanel>
		</a4j:outputPanel>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid width="100%" cellpadding="3" cellspacing="3">
			<br:brPanelGroup style="float:right;">
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1"
					style="margin-right: 5px;"
					action="#{manterSolicAntecipacaoPostergarLotePagamentoBean.limparCamposTelaInclusao}"
					value="#{msgs.label_limpar_campos}"
					disabled="#{pagamentosBean.desabilitaBtnLimparCampos}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.label_consultar}"
					 action="#{manterSolicAntecipacaoPostergarLotePagamentoBean.consultaLotesInclusaoSolicitacao}"
					 onclick="desbloquearTela();return validaCampoLotePostergar(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
						 '#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}', '#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_tipo_layout_arquivo}', '#{msgs.manterSolicitacaoExclusaoLotePagamentos_situacao_solicitacao}',
						 '#{msgs.label_periodo_pesquisa_anteciparPostergar}', '#{msgs.label_deve_ser_diferente_de_zeros}');" 
			            disabled="#{(!manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado) || 
						manterSolicAntecipacaoPostergarLotePagamentoBean.pagamentosBean.disableArgumentosConsulta}">
				 <brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll">

				<app:scrollableDataTable id="dataTable"  value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.listaLote}"var="result" 
					rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.itemSelecao}">
							<f:selectItems 	value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.listaItensSelecao}" />
							<a4j:support event="onclick" reRender="btnAvancar"/>
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
			    	</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabRight" width="150px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_lote_interno}"
								style="width:250; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.nrLoteInterno}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="150px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_layout_arquivo}"
								style="width:250; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsTipoLayoutArquivo}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="150px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_tipo_de_servico}"
								style="width:250; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsProdutoServicoOperacao}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabRight" width="150px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_quantidade_pagamentos_lote}"
								style="width:250; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.qtTotalPagamentoLote}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabRight" width="150px">
						<f:facet name="header">
							<br:brOutputText
								value="#{msgs.label_valor_total_pagamentos_lote}"
								style="width:250; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.vlTotalPagamentoLote}"
							converter="decimalBrazillianConverter" />
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterSolicAntecipacaoPostergarLotePagamentoBean.paginarListaLoteIncluir}" >
					 <f:facet name="first">
					    <brArq:pdcCommandButton id="primeira"
					      styleClass="bto1"
					      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					  </f:facet>
					  <f:facet name="fastrewind">
					    <brArq:pdcCommandButton id="retrocessoRapido"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					  </f:facet>
					  <f:facet name="previous">
					    <brArq:pdcCommandButton id="anterior"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					  </f:facet>
					  <f:facet name="next">
					    <brArq:pdcCommandButton id="proxima"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					  </f:facet>
					  <f:facet name="fastforward">
					    <brArq:pdcCommandButton id="avancoRapido"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					  </f:facet>
					  <f:facet name="last">
					    <brArq:pdcCommandButton id="ultima"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					  </f:facet>
					</brArq:pdcDataScroller>
				</br:brPanelGroup>
	      </br:brPanelGrid>
		</br:brPanelGrid>
		

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" style="text-align:right"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="text-align:left;width:100%">
				<br:brCommandButton id="btnVoltar" styleClass="bto1"
					style="align:left" value="#{msgs.botao_voltar}"
					action="#{manterSolicAntecipacaoPostergarLotePagamentoBean.voltarInclusaoConsulta}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimpar" styleClass="bto1"
					value="#{msgs.label_limpar}"
					action="#{manterSolicAntecipacaoPostergarLotePagamentoBean.limpaLista}"
					style="margin-right:5px"
					disabled="#{empty manterSolicAntecipacaoPostergarLotePagamentoBean.listaLote}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnAvancar" styleClass="bto1"
					value="#{msgs.botao_avancar}"
					action="#{manterSolicAntecipacaoPostergarLotePagamentoBean.avancar}"
					disabled="#{empty manterSolicAntecipacaoPostergarLotePagamentoBean.itemSelecao}"
					onclick="desbloquearTela();return validaCamposInclusaoLotePagamento(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
					 '#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}', '#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_tipo_layout_arquivo}', '#{msgs.conPagamentosConsolidado_data_pagamento}',
					 '#{msgs.label_tipo_de_servico}', '#{msgs.label_modalidade}', '#{msgs.label_banco}', '#{msgs.label_agencia}', '#{msgs.label_conta}','#{msgs.label_deve_ser_diferente_de_zeros}');">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<a4j:status id="statusAguarde" onstart="bloquearTela()"
			onstop="desbloquearTela()" />

	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>