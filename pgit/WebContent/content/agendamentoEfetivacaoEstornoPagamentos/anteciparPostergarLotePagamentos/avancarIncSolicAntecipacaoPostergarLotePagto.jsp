<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="confIncSolicAntecipacaoPostergarLotePagtoForm"
	name="confIncSolicAntecipacaoPostergarLotePagtoForm">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"
		cellspacing="0">

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.detPagamentoIndividual_cliente}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detPagamentosConsolidado_cpfCnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.confirmaInclusaoBean.nrCnpjCpf}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detPagamentosConsolidado_nomeRazaoSocial}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_empresa_gestora_contrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detPagamentosConsolidado_numeroContrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detPagamentosConsolidado_descricaoContrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detPagamentosConsolidado_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.conPagamentosConsolidado_argumentos_pesquisa}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_tipo_solicitacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.descTipoAgendamento}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>


		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manterSolicitacaoExclusaoLotePagamentos_lote_interno}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.loteSelecionado.nrLoteInterno}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conAssociacaoLoteLayoutArquivoProdutoServico_tipo_layout_arquivo}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.loteSelecionado.dsTipoLayoutArquivo}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_tipo_de_servico}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.loteSelecionado.dsProdutoServicoOperacao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_quantidade_pagamentos_lote}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.loteSelecionado.qtTotalPagamentoLote}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_valor_total_dos_pagamentos_lote}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.loteSelecionado.vlTotalPagamentoLote}"
					converter="decimalBrazillianConverter" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_novaDataAgendamento}:" />
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup>
				<app:calendar
					id="novaDataAgendamento" value="#{manterSolicAntecipacaoPostergarLotePagamentoBean.novaDataAgendamentoConfirma}"></app:calendar>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="2" width="100%" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup style="text-align:left;width:150px">
				<br:brCommandButton id="btnVoltarInclusao" styleClass="bto1"
					value="#{msgs.btn_voltar}"
					action="#{manterSolicAntecipacaoPostergarLotePagamentoBean.voltarInclusao}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px">
				<br:brCommandButton id="btnConfirmar" styleClass="bto1"
					style="margin-left:5px" value="#{msgs.btn_avancar}"
					onclick="desbloquearTela();return  validarIncluir()"
					action="#{manterSolicAntecipacaoPostergarLotePagamentoBean.avancarIncluirOutro}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>