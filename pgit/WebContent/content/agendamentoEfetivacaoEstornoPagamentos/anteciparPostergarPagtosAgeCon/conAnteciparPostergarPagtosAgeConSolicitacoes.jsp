<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conAnteciparPostergarPagtosAgeConSolicitacoes" name="conAnteciparPostergarPagtosAgeConSolicitacoes">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{anteciparPostergarPagtosAgeConBean.nrCnpjCpf}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.dsRazaoSocial}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top: 9" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}" converter="spaceConverter" escape="false"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top: 9" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	
  	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">
  		<br:brPanelGroup>	
	  		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" style="margin-right:5px" value="#{msgs.label_data_solicitacao}:"/>
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup rendered="#{!anteciparPostergarPagtosAgeConBean.desabilitaArgumentoDePesquisa}">
				<br:brPanelGroup>
				 	<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" 
				 		oninputchange="recuperarDataCalendario(document.forms[1],'calendarioDe');" id="calendarioDe" 
				 		value="#{anteciparPostergarPagtosAgeConBean.dataInicialSolicitacaoFiltro}" disabled="false">
					</app:calendar>
				</br:brPanelGroup>					
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.histContasManterContrato_a}"/>
				<br:brPanelGroup >
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" 
						oninputchange="recuperarDataCalendario(document.forms[1],'calendarioAte');" id="calendarioAte" 
						value="#{anteciparPostergarPagtosAgeConBean.dataFinalSolicitacaoFiltro}" disabled="false">
					</app:calendar>		
				</br:brPanelGroup>							
			</br:brPanelGroup>			    
			    
			    
			<br:brPanelGroup rendered="#{anteciparPostergarPagtosAgeConBean.desabilitaArgumentoDePesquisa}">
				<br:brPanelGroup>
				 	<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" 
				 		oninputchange="recuperarDataCalendario(document.forms[1],'calendarioDe');" id="calendarioDeDes" 
				 		value="#{anteciparPostergarPagtosAgeConBean.dataInicialSolicitacaoFiltro}" disabled="true">
					</app:calendar>
				</br:brPanelGroup>					
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.histContasManterContrato_a}"/>
				<br:brPanelGroup >
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" 
						oninputchange="recuperarDataCalendario(document.forms[1],'calendarioAte');" id="calendarioAteDes" 
						value="#{anteciparPostergarPagtosAgeConBean.dataFinalSolicitacaoFiltro}" disabled="true">
					</app:calendar>		
				</br:brPanelGroup>							
			</br:brPanelGroup>
			
			
		</br:brPanelGrid>
	</a4j:outputPanel>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-top:10px;" >	
		<br:brPanelGroup >
	  		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" style="margin-right:15px" value="#{msgs.label_situacao_solicitacao}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" style="margin-right:5px" value="#{msgs.label_motivo_da_soliticacao}:"/>
		</br:brPanelGroup>
					
								
		<br:brPanelGroup style="margin-top:5px; margin-right:15px;">
		   	<br:brSelectOneMenu id="cboSituacao" value="#{anteciparPostergarPagtosAgeConBean.itemSelecionadoComboSolicitacao}" styleClass="HtmlSelectOneMenuBradesco"
		   		disabled="#{anteciparPostergarPagtosAgeConBean.desabilitaArgumentoDePesquisa}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
				<f:selectItems value="#{anteciparPostergarPagtosAgeConBean.listaConsultarSituacaoSolicitacaoEstorno}" />						
				<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onchange" reRender="cboMotivo" 
					action="#{anteciparPostergarPagtosAgeConBean.carregarComboMotivoEstorno}"/>						
			</br:brSelectOneMenu>
		</br:brPanelGroup>					
		
		<br:brPanelGroup >
		   	<br:brSelectOneMenu id="cboMotivo" value="#{anteciparPostergarPagtosAgeConBean.itemSelecionadoComboMotivo}" styleClass="HtmlSelectOneMenuBradesco"
		   		disabled="#{anteciparPostergarPagtosAgeConBean.desabilitaComboMotivo}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
				<f:selectItems value="#{anteciparPostergarPagtosAgeConBean.listaConsultarMotivoSituacaoEstorno}" />						
				<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onchange" />						
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<f:verbatim><hr class="lin"></f:verbatim>

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="text-align:right; width: 100%" >
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" style="align:left" value="#{msgs.btn_limpar_campos}" 
				action="#{anteciparPostergarPagtosAgeConBean.limparCamposSolicitacoes}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
			<br:brCommandButton style="margin-left:5px" id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}"
					disabled="#{anteciparPostergarPagtosAgeConBean.desabilitaArgumentoDePesquisa}"
					action="#{anteciparPostergarPagtosAgeConBean.consultaPostergarConsolidadoSolicitacao}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid> 

	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">

			<app:scrollableDataTable id="dataTable" value="#{anteciparPostergarPagtosAgeConBean.listaConsultaPostergarConsolidadoSolicitacao}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" 
						forceIdIndex="false" value="#{anteciparPostergarPagtosAgeConBean.itemSelecionadoConsultaPostergarConsolidado}">
						<f:selectItems value="#{anteciparPostergarPagtosAgeConBean.listaConsultaPostergarConsolidadoSolicitacaoControle}"/>
						<a4j:support event="onclick" reRender="botoes, btnExcluir" action="#{anteciparPostergarPagtosAgeConBean.habilitarBotaoExcluir}" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_empresa_gestora_contrato}" style="width:100%; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsPessoaJuridicaContrato}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_numeroContrato}"  style="width:100%; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrSequenciaContratoNegocio}" />
				</app:scrollableColumn>		
				
				<app:scrollableColumn styleClass="colTabRight" width="250px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_numero_solicitacao}"  style="width:100%; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrSolicitacaoPagamentoIntegrado}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabCenter" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_data_solicitacao}"  style="width:100%; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dtInclusaoSolicitacaoFormatada}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_tipo_solicitacao}"  style="width:100%; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoSolicitacao}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_remessa}"  style="width:100%; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrArquivoRemessa}" />
				</app:scrollableColumn>		
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_tipo_servico}"  style="width:100%; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsProdutoServicoOperacao}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_modalidade}"  style="width:100%; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsProdutoOperacaoRelacionado}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_situacao_da_solicitacao}"  style="width:100%; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoSolicitacaoPagamento}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_motivo_da_soliticacao}"  style="width:100%; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsMotivoSolicitacao}" />
				</app:scrollableColumn>	
				
			</app:scrollableDataTable>			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{anteciparPostergarPagtosAgeConBean.consultaPostergarConsolidadoSolicitacaoListener}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid id="botoes" columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{anteciparPostergarPagtosAgeConBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" style="align:left" value="#{msgs.btn_limpar}" 
				action="#{anteciparPostergarPagtosAgeConBean.limparSolicitacoes}"
				disabled="#{empty anteciparPostergarPagtosAgeConBean.listaConsultaPostergarConsolidadoSolicitacao}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
			<br:brCommandButton style="margin-left:5px" id="btnDetalhar" styleClass="bto1" value="#{msgs.btn_detalhar}" 
				disabled="#{empty anteciparPostergarPagtosAgeConBean.itemSelecionadoConsultaPostergarConsolidado}"
				action="#{anteciparPostergarPagtosAgeConBean.detalharSolicitacoes}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
			<br:brCommandButton style="margin-left:5px" id="btnExcluir" styleClass="bto1" value="#{msgs.btn_excluir}" 
				disabled="#{anteciparPostergarPagtosAgeConBean.desabilitaBotaoExcluir}"
				action="#{anteciparPostergarPagtosAgeConBean.excluirSolicitacoes}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid> 
</br:brPanelGrid> 
</brArq:form>