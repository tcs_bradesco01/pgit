<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detAnteciparPostergarPagtosAgeConSolicitacoes" name="detAnteciparPostergarPagtosAgeConSolicitacoes">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
		
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.cpfCnpjFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.dsComplementoPagador}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    	 
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="margin-right: 15">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}" converter="spaceConverter" escape="false"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="margin-right: 15">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_solicitacao}:"/>

	<br:brPanelGrid style="margin-top:9px" cellpadding="0" cellspacing="0">
	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
	   		<br:brPanelGroup style="margin-right: 15">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_numero}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.nrSolicitacaoPagamentoIntegrado}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao_da_solicitacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.dsSituacaoSolicitacaoPagamento}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_motivo_da_soliticacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.dsMotivoSolicitacao}"/>
		</br:brPanelGroup>	
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_tipo_solicitacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.dsTipoSolicitacao}"/>
		</br:brPanelGroup>
					
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_data_agendamento}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.dtAgendamentoPagamento}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_nova_data_agendamento}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.dtPrevistaAgendaPagamento}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
				
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_tipoServico}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.dsProdutoServicoOperacao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_modalidade}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.dsProdutoOperRelacionado}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
				
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_remessa}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.nrArquivoRemssaPagamento}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao_do_pagamento}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.dsSituacaoPagamentoCliente}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_banco_debito}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.bancoFormatado}"/>
			</br:brPanelGroup>
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_agencia_debito}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.agenciaFormatada}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_conta_debito}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.contaFormatada}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_tipo_conta_debito}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.dsTipoContaPagador}"/>
		</br:brPanelGroup>		
				
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_quantidade_total_pagamentos_solicitados}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.qtdPagamentoPrevtSolicitacao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_valor_total_Pagamentos_solicitados}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" converter="decimalBrazillianConverter" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.vlPagamentoPrevtSolicitacao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
				
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_quantidade_total_pagamentos_atendidos}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.qtdPagamentoEfetvSolicitacao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_valor_total_pagamentos_atendidos}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" converter="decimalBrazillianConverter" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.vlPagamentoEfetivoSolicitacao}"/>
			</br:brPanelGroup>
		
		</br:brPanelGrid>
				
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
				
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_quantidade_total_pagamentos_processados}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.qtdePagamentosProcessados}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_valor_total_pagamentos_processados}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" converter="decimalBrazillianConverter" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.vlrPagamentosProcessados}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
				
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_data_hora_de_processamento}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.dataHoraAtendimento}"/>
		</br:brPanelGroup>						
    </br:brPanelGrid>

    <f:verbatim><hr class="lin"></f:verbatim>
    
    
    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">

			<app:scrollableDataTable id="dataTable" value="#{anteciparPostergarPagtosAgeConBean.listaPostergarConsolidadoSolicitacao}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
			
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_numero_pagamento}" style="text-align:center; width: 100%"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdControlePagamento}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_valor_pagamento}"  style="text-align:center; width: 100%"/>
				    </f:facet>
				    <br:brOutputText value="#{result.vlAgendamentoPagamento}" converter="decimalBrazillianConverter" />
				</app:scrollableColumn>		
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_favorecido_beneficiario}"  style="text-align:center; width: 100%"/>
				    </f:facet>
				    <br:brOutputText value="#{result.complementoRecebedor}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_bco_ag_conta_credito}"  style="text-align:center; width: 100%"/>
				    </f:facet>
				    <br:brOutputText value="#{result.bcoAgContaFormatada}" />
				</app:scrollableColumn>	

			</app:scrollableDataTable>			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center;" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{anteciparPostergarPagtosAgeConBean.listaPostergarConsolidadoSolicitacaoListener}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>
    
    
	<f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_dataHoraInclusao2}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.hrInclusaoRegistro}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_usuario}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.cdAutenticacaoSegurancaInclusao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_tipo_canal}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.tipoCanalInclusao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_complemento}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.nrOperacaoFluxoInclusao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid cellpadding="0" cellspacing="0">
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_dataHoraManutencao2}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.hrManutencaoRegistro}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_usuario}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.cdAutenticacaoSegurancaManutencao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-right: 15">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_tipo_canal}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.tipoCanalManutencao}"/>
			</br:brPanelGroup>
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_complemento}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{anteciparPostergarPagtosAgeConBean.saidaDetalhePostergarConsolidadoSolicitacao.nmOperacaoFluxoManutencao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
            
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{anteciparPostergarPagtosAgeConBean.voltarSolicitacoes}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
	</br:brPanelGrid> 
</br:brPanelGrid> 
</brArq:form>