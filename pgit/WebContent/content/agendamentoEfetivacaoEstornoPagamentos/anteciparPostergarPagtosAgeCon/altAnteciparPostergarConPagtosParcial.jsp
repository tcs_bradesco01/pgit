<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="altAnteciparPostergarConPagtosParcial" name="altAnteciparPostergarConPagtosParcial">
	<a4j:jsFunction name="funcPaginacao" action="#{anteciparPostergarPagtosAgeConBean.limparSelecionados}" reRender="btnLimparSelecao, btnAlterarSelecionados, dataTable" onbeforedomupdate="true"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{anteciparPostergarPagtosAgeConBean.nrCnpjCpf}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{anteciparPostergarPagtosAgeConBean.dsRazaoSocial}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{anteciparPostergarPagtosAgeConBean.dsEmpresa}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{anteciparPostergarPagtosAgeConBean.nroContrato}" converter="spaceConverter" escape="false"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/> 			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{anteciparPostergarPagtosAgeConBean.dsContrato}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{anteciparPostergarPagtosAgeConBean.cdSituacaoContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim> 
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0"> 
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_conta_debito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_banco}:"/> 			 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{anteciparPostergarPagtosAgeConBean.dsBancoDebito}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{anteciparPostergarPagtosAgeConBean.dsAgenciaDebito}"/>
		</br:brPanelGroup>			
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{anteciparPostergarPagtosAgeConBean.contaDebito}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" > 				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{anteciparPostergarPagtosAgeConBean.tipoContaDebito}"/> 
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim> 
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0"> 
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_pagamentos}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_servico}:"/> 			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{anteciparPostergarPagtosAgeConBean.dsTipoServico}"/>
		</br:brPanelGroup>	

		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_modalidade}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{anteciparPostergarPagtosAgeConBean.dsIndicadorModalidade}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
	 	<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_qtde_pagamentos}:"/> 			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{anteciparPostergarPagtosAgeConBean.qtdePagamentos}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_total_pagamentos}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{anteciparPostergarPagtosAgeConBean.vlTotalPagamentos}" converter="decimalBrazillianConverter"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>	
   			<br:brSelectOneRadio layout="pageDirection" id="rdoTipoAgendamento" styleClass="HtmlSelectOneRadioBradesco" value="#{anteciparPostergarPagtosAgeConBean.tipoAgendamento}" disabled="true" >
				<f:selectItem  itemValue="1" itemLabel="#{msgs.label_antecipar_processamento}"/>  
       			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_antecipar_postergar_pagamento}"/>
				<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="calendarios" action="#{anteciparPostergarPagtosAgeConBean.verificarTipoAgendamento}"/>			
        		</br:brSelectOneRadio>
   		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left; margin-left: 25px" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >
		<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_novaDataAgendamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brPanelGroup rendered="#{anteciparPostergarPagtosAgeConBean.tipoAgendamento == '2'}">
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'novaDataAgendamentoHab');" id="novaDataAgendamentoHab" value="#{anteciparPostergarPagtosAgeConBean.novaDataAgendamento}"></app:calendar>
				</br:brPanelGroup>
				<br:brPanelGroup rendered="#{anteciparPostergarPagtosAgeConBean.tipoAgendamento != '2'}">
					<app:calendar disabled="true" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'novaDataAgendamentoDes');" id="novaDataAgendamentoDes" value="#{anteciparPostergarPagtosAgeConBean.novaDataAgendamento}"></app:calendar>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>		
	</a4j:outputPanel>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
		
				<app:scrollableDataTable id="dataTable" value="#{anteciparPostergarPagtosAgeConBean.listaGridDetalhar}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  	<t:selectBooleanCheckbox disabled="#{empty anteciparPostergarPagtosAgeConBean.listaGridDetalhar}" id="checkSelecionarTodos" styleClass="HtmlSelectOneRadioBradesco" value="#{anteciparPostergarPagtosAgeConBean.opcaoChecarTodos}" onclick="javascritp:selecionarTodos(document.forms[1],this);">
						</t:selectBooleanCheckbox>
				    </f:facet>
					<t:selectBooleanCheckbox  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" value="#{result.check}">
						<a4j:support event="onclick" reRender="dataTable, btnLimparSelecao, btnAlterarSelecionados" action="#{anteciparPostergarPagtosAgeConBean.habilitarPanelBotoes}"/>							
					</t:selectBooleanCheckbox>
				</app:scrollableColumn>					
					
				<app:scrollableColumn styleClass="colTabLeft" width="130px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_numero_pagamento}" style="width:130; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrPagamento}" converter="spaceConverter" escape="false"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="130px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_valor_pagamento}" style="width:130; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.vlPagamento}" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="180px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_favorecido_beneficiario}" style="width:180; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.favorecidoBeneficiario}" />
				</app:scrollableColumn>
									
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_bco_ag_conta_credito}" style="width:200px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.contaCreditoFormatada}" />
				</app:scrollableColumn>
				 
			</app:scrollableDataTable>
		</br:brPanelGroup>
		
	</br:brPanelGrid>	
	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{anteciparPostergarPagtosAgeConBean.pesquisarDetAnteciparPostergarPagtosAgeConFiltro}" >
			 	<f:facet name="first">
					<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="next">
					<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" onclick="funcPaginacao();" />
				</f:facet>
				<f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" onclick="funcPaginacao();"/>
				</f:facet>
				<f:facet name="last">
					<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" onclick="funcPaginacao();"/> 
				</f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
		<f:verbatim><hr class="lin"></f:verbatim>
	
		
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{anteciparPostergarPagtosAgeConBean.voltarConsultar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btnLimparSelecao" style="margin-left:5px" disabled="#{!anteciparPostergarPagtosAgeConBean.panelBotoes}" styleClass="bto1" value="#{msgs.label_limpar_selecionados}" action="#{anteciparPostergarPagtosAgeConBean.limparSelecionados}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnAlterarSelecionados" style="margin-left:5px" disabled="#{!anteciparPostergarPagtosAgeConBean.panelBotoes}" styleClass="bto1" value="#{msgs.label_alterarSelecionados}" action="#{anteciparPostergarPagtosAgeConBean.alterarSelecionados}" onclick="desbloquearTela(); return validaData(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.label_novaDataAgendamento}', '#{msgs.label_necessario_selecionar_tipo_agendamento}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>  
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>