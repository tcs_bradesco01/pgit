<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="desautorizarPagamentosConsolidadosForm" name="desautorizarPagamentosConsolidadosForm">
<h:inputHidden id="hiddenFoco" value="#{desautorizarPagamentosConsolidadosBean.foco}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta}" id="radioTipoFiltro" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
			<f:selectItem itemValue="0" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_cliente}" />
			<f:selectItem itemValue="1" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_contrato}" />
			<f:selectItem itemValue="2" itemLabel="#{msgs.consultarPagamentosIndividual_filtrar_por_conta_debito}" />
			<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="formulario,dataTable,panelBotoes,dataScroller" action="#{desautorizarPagamentosConsolidadosBean.limparTelaPrincipal}" status="statusAguarde" />			
		</t:selectOneRadio>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
		<br:brPanelGroup>			
			<t:radio for="radioTipoFiltro" index="0" />
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
 
	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '0'}">  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
	</t:selectOneRadio>
	</a4j:region>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'desautorizarPagamentosConsolidadosForm','desautorizarPagamentosConsolidadosForm:txtCnpj','desautorizarPagamentosConsolidadosForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'desautorizarPagamentosConsolidadosForm','desautorizarPagamentosConsolidadosForm:txtFilial','desautorizarPagamentosConsolidadosForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1' }" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    	onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(9,'desautorizarPagamentosConsolidadosForm','desautorizarPagamentosConsolidadosForm:txtCpf','desautorizarPagamentosConsolidadosForm:txtControleCpf');" />
			    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '1'}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" style="margin-left:20px" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '2'}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3'}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'desautorizarPagamentosConsolidadosForm','desautorizarPagamentosConsolidadosForm:txtBanco','desautorizarPagamentosConsolidadosForm:txtAgencia');"
				onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3'}" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'desautorizarPagamentosConsolidadosForm','desautorizarPagamentosConsolidadosForm:txtAgencia','desautorizarPagamentosConsolidadosForm:txtConta');"
				onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado != '3'}" 
						 onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{filtroAgendamentoEfetivacaoEstornoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" style="margin-right:5px"/>
					</br:brPanelGroup>
				    <br:brPanelGroup>						

					</br:brPanelGroup>		
			</br:brPanelGrid>				
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
	   		<br:brCommandButton id="btoConsultarCliente" actionListener="#{desautorizarPagamentosConsolidadosBean.limparArgsListaAposPesquisaClienteContrato}" styleClass="bto1" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || empty filtroAgendamentoEfetivacaoEstornoBean.itemFiltroSelecionado}" value="#{msgs.consultarPagamentosIndividual_btn_consultar_cliente}" action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
	   		 onclick="javascript:desbloquearTela(); return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}', '#{msgs.consultarPagamentosIndividual_label_nomeRazao}','#{msgs.consultarPagamentosIndividual_banco}','#{msgs.consultarPagamentosIndividual_agencia}','#{msgs.consultarPagamentosIndividual_conta}','#{msgs.label_digito_conta}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_deve_ser_diferente_de_zeros}');">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top: 9" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescCliente}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescCliente}"/>
		</br:brPanelGroup>	    
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescCliente}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescCliente}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>     
    
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
		<br:brPanelGroup>			
			<t:radio for="radioTipoFiltro" index="1" />
		</br:brPanelGroup>		
	</br:brPanelGrid>	    
    
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	    
	
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:20px">
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="empresaGestora" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1'}" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px" >
						<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaEmpresaGestora}" />								
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoContrato" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1'}" value="#{filtroAgendamentoEfetivacaoEstornoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width:250px">
						<f:selectItems value="#{filtroAgendamentoEfetivacaoEstornoBean.listaTipoContrato}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_numero_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroFiltro}" size="12" maxlength="10" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1'}"></br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>	
    
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;">
   		<br:brCommandButton id="btoConsultarContrato" actionListener="#{desautorizarPagamentosConsolidadosBean.limparArgsListaAposPesquisaClienteContrato}" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '1'}" styleClass="bto1" value="#{msgs.consultarPagamentosIndividual_btn_consultar_contrato}" action="#{filtroAgendamentoEfetivacaoEstornoBean.consultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCamposConsultaContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.consultarPagamentosIndividual_tipo_contrato}','#{msgs.label_numero_contrato}','#{msgs.label_deve_ser_diferente_de_zeros}');">		
			<brArq:submitCheckClient />
		</br:brCommandButton>
	</br:brPanelGrid>	  
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	 
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.empresaGestoraDescContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.numeroDescContrato}"/>
		</br:brPanelGroup>	    
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.descricaoContratoDescContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.consultarPagamentosIndividual_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{filtroAgendamentoEfetivacaoEstornoBean.situacaoDescContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
    <f:verbatim><hr class="lin"></f:verbatim>  
    
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  style="text-align:left;" >
		<br:brPanelGroup>			
			<t:radio for="radioTipoFiltro" index="2" />
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" style="margin-top:6px;margin-left:20px" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_title_identificacao_conta_debito}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-left:20px">
		<a4j:outputPanel id="panelBancoContaDebito" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco_conta_debito}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',1)" onkeypress="onlyNum();" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2'}" converter="javax.faces.Integer" value="#{filtroAgendamentoEfetivacaoEstornoBean.bancoContaDebitoFiltro}" size="4" maxlength="3" id="txtBancoContaDebito" onkeyup="proximoCampo(3,'desautorizarPagamentosConsolidadosForm','desautorizarPagamentosConsolidadosForm:txtBancoContaDebito','desautorizarPagamentosConsolidadosForm:txtAgenciaContaDebito');">
				<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{desautorizarPagamentosConsolidadosBean.controlarArgumentosPesquisa}"/>				
			</br:brInputText>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgenciaContaDebito" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia_conta_debito}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',2)" onkeypress="onlyNum();" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2'}" converter="javax.faces.Integer" value="#{filtroAgendamentoEfetivacaoEstornoBean.agenciaContaDebitoBancariaFiltro}" size="6" maxlength="5" id="txtAgenciaContaDebito" onkeyup="proximoCampo(5,'desautorizarPagamentosConsolidadosForm','desautorizarPagamentosConsolidadosForm:txtAgenciaContaDebito','desautorizarPagamentosConsolidadosForm:txtContaContaDebito');">
				<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{desautorizarPagamentosConsolidadosBean.controlarArgumentosPesquisa}"/>				
			</br:brInputText>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelContaContaDebito" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" onchange="javascript:validaCampoNumericoBlur(document.forms[1], this,'#{msgs.label_campo_contem_apenas_numeros}',3)" converter="javax.faces.Long" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado != '2'}" onkeypress="onlyNum();" value="#{filtroAgendamentoEfetivacaoEstornoBean.contaBancariaContaDebitoFiltro}" size="17" maxlength="13" id="txtContaContaDebito" >
						<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="btnConsultar,painelArgumentos" action="#{desautorizarPagamentosConsolidadosBean.controlarArgumentosPesquisa}"/>				
					</br:brInputText>
				</br:brPanelGroup>						
	    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
			    <br:brPanelGroup>

				</br:brPanelGroup>			
			</br:brPanelGrid>				
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.conPagamentosConsolidado_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<a4j:outputPanel id="painelArgumentos" style="width: 100%; text-align: left" ajaxRendered="true">
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="radioAgendadosPagosNaoPagos" disabled="true" value="#{desautorizarPagamentosConsolidadosBean.agendadosPagosNaoPagos}" styleClass="HtmlSelectOneRadioBradesco" forceId="true" forceIdIndex="false" >  
				<f:selectItem itemValue="1" itemLabel="#{msgs.conPagamentosConsolidado_agendados}" />
			</t:selectOneRadio>		
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosConsolidado_data_pagamento}:" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >					
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGroup rendered="#{!desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta && (desautorizarPagamentosConsolidadosBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado)}">
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialPagamento');"  id="dataInicialPagamento" value="#{desautorizarPagamentosConsolidadosBean.dataInicialPagamentoFiltro}" >
					</app:calendar>
				</br:brPanelGroup>
				<br:brPanelGroup rendered="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || (!desautorizarPagamentosConsolidadosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado)}">
					<app:calendar id="dataInicialPagamentoDes" value="#{desautorizarPagamentosConsolidadosBean.dataInicialPagamentoFiltro}" disabled="true" >
					</app:calendar>
				</br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
				<br:brPanelGroup rendered="#{!desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta && (desautorizarPagamentosConsolidadosBean.habilitaArgumentosPesquisa || filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado)}">
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialPagamento');"  id="dataFinalPagamento" value="#{desautorizarPagamentosConsolidadosBean.dataFinalPagamentoFiltro}" >
	 				</app:calendar>	
				</br:brPanelGroup>	
				<br:brPanelGroup rendered="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || (!desautorizarPagamentosConsolidadosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado)}">
					<app:calendar id="dataFinalPagamentoDes" value="#{desautorizarPagamentosConsolidadosBean.dataFinalPagamentoFiltro}" disabled="true">
	 				</app:calendar>	
				</br:brPanelGroup>	
			</br:brPanelGroup>			    
		</br:brPanelGrid>
	</a4j:outputPanel>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<h:selectBooleanCheckbox id="chkRemessa" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || (!desautorizarPagamentosConsolidadosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado)}" value="#{desautorizarPagamentosConsolidadosBean.chkRemessa}">		
			<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="numeroRemessaFiltro" action="#{desautorizarPagamentosConsolidadosBean.limparRemessa}"/>
		</h:selectBooleanCheckbox>
		
		<br:brPanelGroup>
			<br:brOutputText style="margin-left:5px" styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosConsolidado_remessa}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid style="margin-left:20px" columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosConsolidado_numero}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
	    <br:brPanelGroup>
			<br:brInputText styleClass="HtmlInputTextBradesco" value="#{desautorizarPagamentosConsolidadosBean.numeroRemessaFiltro}" size="13" maxlength="9" id="numeroRemessaFiltro" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || !desautorizarPagamentosConsolidadosBean.chkRemessa}" onkeypress="onlyNum();"
				onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<h:selectBooleanCheckbox id="chkContaDebito" value="#{desautorizarPagamentosConsolidadosBean.chkContaDebito}" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || (filtroAgendamentoEfetivacaoEstornoBean.tipoFiltroSelecionado == 2 || (!desautorizarPagamentosConsolidadosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado))}" style="margin-right:5px">
			<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="txtBancoContaDebitoCheck,txtAgenciaContaDebitoCheck,txtContaDebitoCheck" action="#{desautorizarPagamentosConsolidadosBean.limparContaDebito}"/>			
		</h:selectBooleanCheckbox>
		<br:brPanelGroup>
			<br:brOutputText style="margin-left:5px" styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosConsolidado_conta_debito}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" style="margin-left:25px">
		<a4j:outputPanel id="panelBancoContaDebitoCheck" ajaxRendered="true">			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosConsolidado_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || !desautorizarPagamentosConsolidadosBean.chkContaDebito}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{desautorizarPagamentosConsolidadosBean.cdBancoContaDebito}" size="4" maxlength="3" id="txtBancoContaDebitoCheck" onkeyup="proximoCampo(3,'desautorizarPagamentosConsolidadosForm','desautorizarPagamentosConsolidadosForm:txtBancoContaDebitoCheck','desautorizarPagamentosConsolidadosForm:txtAgenciaContaDebitoCheck');"
				onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgenciaContaDebitoCheck" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosConsolidado_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || !desautorizarPagamentosConsolidadosBean.chkContaDebito}" onkeypress="onlyNum();" converter="javax.faces.Integer" value="#{desautorizarPagamentosConsolidadosBean.cdAgenciaBancariaContaDebito}" size="6" maxlength="5" id="txtAgenciaContaDebitoCheck" onkeyup="proximoCampo(5,'desautorizarPagamentosConsolidadosForm','desautorizarPagamentosConsolidadosForm:txtAgenciaContaDebitoCheck','desautorizarPagamentosConsolidadosForm:txtContaDebitoCheck');"
				onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelContaDebitoCheck" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosConsolidado_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || !desautorizarPagamentosConsolidadosBean.chkContaDebito}" converter="javax.faces.Long" 
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" value="#{desautorizarPagamentosConsolidadosBean.cdContaBancariaContaDebito}" size="17" maxlength="13" id="txtContaDebitoCheck"/>
				</br:brPanelGroup>						
	    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
			    <br:brPanelGroup>

				</br:brPanelGroup>			
			</br:brPanelGrid>				
		</a4j:outputPanel>
    </br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-bottom:40px" >	
				<h:selectBooleanCheckbox id="chkServicoModalidade" value="#{desautorizarPagamentosConsolidadosBean.chkServicoModalidade}" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || (!desautorizarPagamentosConsolidadosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado)}"style="margin-right:5px">
					<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" reRender="cboTipoServico,cboModalidade" action="#{desautorizarPagamentosConsolidadosBean.controleServicoModalidade}"/>
				</h:selectBooleanCheckbox>					
			</br:brPanelGrid>	
		</br:brPanelGroup>

		<br:brPanelGroup>			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_tipo_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="cboTipoServico" value="#{desautorizarPagamentosConsolidadosBean.tipoServicoFiltro}" styleClass="HtmlSelectOneMenuBradesco"
			    	disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || !desautorizarPagamentosConsolidadosBean.chkServicoModalidade}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{desautorizarPagamentosConsolidadosBean.listaTipoServicoFiltro}" />						
						<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onchange" reRender="cboModalidade" action="#{desautorizarPagamentosConsolidadosBean.listarModalidadesPagto}"/>						
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>				
			
		    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_modalidade}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="cboModalidade" value="#{desautorizarPagamentosConsolidadosBean.modalidadeFiltro}" styleClass="HtmlSelectOneMenuBradesco"
			    	  disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || (desautorizarPagamentosConsolidadosBean.tipoServicoFiltro == null || desautorizarPagamentosConsolidadosBean.tipoServicoFiltro == 0 
			    	   || !desautorizarPagamentosConsolidadosBean.chkServicoModalidade)}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_todos}"/>
						<f:selectItems value="#{desautorizarPagamentosConsolidadosBean.listaModalidadeFiltro}" />												
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>				
		</br:brPanelGroup>
	</br:brPanelGrid>

	</a4j:outputPanel>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{desautorizarPagamentosConsolidadosBean.limparTelaToda}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);" value="#{msgs.consultarPagamentosIndividual_btn_consultar}" onclick="desbloquearTela();return validaCamposConsultarPagamentosConsolidadoSemSituacaoMotivo(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
				 '#{msgs.consultarPagamentosIndividual_agendados}','#{msgs.consultarPagamentosIndividual_ou}','#{msgs.consultarPagamentosIndividual_pagos_naopagos}', '#{msgs.consultarPagamentosIndividual_data_pagamento}',
				 '#{msgs.consultarPagamentosIndividual_selecione_uma_opcao_consulta_argumento}', '#{msgs.consultarPagamentosIndividual_conta_debito}', '#{msgs.consultarPagamentosIndividual_banco}',
				 '#{msgs.consultarPagamentosIndividual_agencia}', '#{msgs.consultarPagamentosIndividual_conta}', '#{msgs.consultarPagamentosIndividual_tipo_servico}', '#{msgs.consultarPagamentosIndividual_modalidade}',
				 '#{msgs.consultarPagamentosIndividual_situacao_pagamento}',  '#{msgs.consultarPagamentosIndividual_remessa}','#{msgs.consultarPagamentosIndividual_numero}','#{msgs.consultarPagamentosIndividual_digito}', '#{msgs.label_deve_ser_diferente_de_zeros}')" disabled="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta || (!desautorizarPagamentosConsolidadosBean.habilitaArgumentosPesquisa && !filtroAgendamentoEfetivacaoEstornoBean.clienteContratoSelecionado)}" action="#{desautorizarPagamentosConsolidadosBean.consultarPagtosConsAutorizados}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>
	
	<f:verbatim><br></f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">

			<app:scrollableDataTable id="dataTable" value="#{desautorizarPagamentosConsolidadosBean.listaGridDesautorizarPagto}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
	 					 <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco"  
						layout="spread" forceId="true" forceIdIndex="false"
						value="#{desautorizarPagamentosConsolidadosBean.itemSelecionadoGridDesautorizarPagto}" >
						<f:selectItems value="#{desautorizarPagamentosConsolidadosBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>	
					<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_empresa_gestora_contrato}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsRazaoSocial}" styleClass="tableFontStyle"/>				    
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_numero_contrato}"  style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrContratoOrigem}" />
				</app:scrollableColumn>				 

				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conPagamentosFavorecido_grid_cnpj_cpf_cliente_pagador}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cpfCnpjFormatado}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="100px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_remessa}"  style="width:100; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrArquivoRemessaPagamento}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabCenter" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conDesautorizarPagamentosConsolidado_dt_pagamento}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dtCreditoPagamento}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_bco_ag_conta_debito}"  style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.bancoAgenciaContaDebitoFormatado}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conDesautorizarPagamentosConsolidado_tipo_servi�o}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsResumoProdutoServico}" />
				</app:scrollableColumn>	

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conDesautorizarPagamentosConsolidado_modalidade}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsOperacaoProdutoServico}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conDesautorizarPagamentosConsolidado_sit_pagamento}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoOperacaoPagamento}" />
				</app:scrollableColumn>		
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conDesautorizarPagamentosConsolidado_qt_pagamento}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.qtPagamentoFormatado}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.conDesautorizarPagamentosConsolidado_vlr_total_pagamento}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.vlEfetivoPagamentoCliente}" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>	
				
			</app:scrollableDataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{desautorizarPagamentosConsolidadosBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
			<br:brPanelGrid columns="4" width="100%"  cellpadding="0" cellspacing="0" style="text-align:right">	
				<br:brPanelGroup style="width:750px">
					<br:brCommandButton id="btnLimpar" disabled="#{desautorizarPagamentosConsolidadosBean.listaGridDesautorizarPagto == null}" styleClass="bto1" style="margin-right:5px;" value="#{msgs.label_botao_limpar}"  action="#{desautorizarPagamentosConsolidadosBean.limparGrid}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnDetalhar"  styleClass="bto1" style="margin-right:5px;" value="#{msgs.btn_detalhar}" disabled="#{desautorizarPagamentosConsolidadosBean.itemSelecionadoGridDesautorizarPagto == null}" action="#{desautorizarPagamentosConsolidadosBean.detalhar}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnDesautorizarParcial" disabled="#{desautorizarPagamentosConsolidadosBean.itemSelecionadoGridDesautorizarPagto == null}" styleClass="bto1" style="margin-right:5px;" value="#{msgs.conDesautorizarPagamentosConsolidado_botao_desaut_parcial}" action="#{desautorizarPagamentosConsolidadosBean.desautorizarParcial}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>										
					<br:brCommandButton id="btnDesautorizarIntegral" disabled="#{desautorizarPagamentosConsolidadosBean.itemSelecionadoGridDesautorizarPagto == null}" styleClass="bto1" style="margin-right:0px;" value="#{msgs.conDesautorizarPagamentosConsolidados_botao_desautorizar_integral}" action="#{desautorizarPagamentosConsolidadosBean.desautorizarIntegral}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>		
			</br:brPanelGrid>	
	    </a4j:outputPanel>
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
	
	<t:inputHidden value="#{desautorizarPagamentosConsolidadosBean.disableArgumentosConsulta}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('desautorizarPagamentosConsolidadosForm:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>