 <%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="desautorizarPagConsolidadosParcialForm" name="desautorizarPagConsolidadosParcialForm">
<a4j:jsFunction name="funcPaginacao" action="#{desautorizarPagamentosConsolidadosBean.pesquisarParcial}" reRender="dataTable" onbeforedomupdate="true"/>
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup> 
		</br:brPanelGrid>   	
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.cpfCnpj}" />
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.nomeRazao}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>		
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.empresaConglomerado}" />
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.numeroContrato}" />
			</br:brPanelGroup>		
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.descricaoContrato}" />
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.situacao}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>		
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_conta_debito}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_banco}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.banco}" />
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.agencia}" />
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.conta}" />
			</br:brPanelGroup>		
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.tipo}" />
			</br:brPanelGroup>			
		</br:brPanelGrid>		
			
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_pagamentos}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_servico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.dsTipoServico}" />
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_modalidade}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.dsIndicadorModalidade}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_qtde_pagamentos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.quantPagamentos}" />
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_total_pagamentos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{desautorizarPagamentosConsolidadosBean.valorPagamentos}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
				<app:scrollableDataTable id="dataTable" value="#{desautorizarPagamentosConsolidadosBean.listaGridDesautorizarPagtoConsolidadosParcial}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
			
				<app:scrollableColumn styleClass="colTabCenter" width="30px">
					<f:facet name="header">
					  	<t:selectBooleanCheckbox style="margin-left:4px" disabled="#{empty desautorizarPagamentosConsolidadosBean.listaGridDesautorizarPagtoConsolidadosParcial}" id="checkSelecionarTodos" styleClass="HtmlSelectOneRadioBradesco" value="#{desautorizarPagamentosConsolidadosBean.btnChecaTodos}" onclick="javascritp:selecionarTodos(document.forms[1],this);" >
						</t:selectBooleanCheckbox>						
				    </f:facet>	
					<t:selectBooleanCheckbox  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" value="#{result.check}">
						<f:selectItems value="#{desautorizarPagamentosConsolidadosBean.listaControleDesautorizarPagtoConsolidadosParcial}"/>	
						<a4j:support event="onclick" reRender="panelBotoes" action="#{desautorizarPagamentosConsolidadosBean.habilitarPanelBotoes}"/>
					</t:selectBooleanCheckbox>
				</app:scrollableColumn>
					
				<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_numero_pagamento}" style="width:150px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrPagamento}" converter="spaceConverter" escape="false"/>				     
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_valor_pagamento}" style="width:150px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.vlPagamento}" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="180px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_favorecido_beneficiario}" style="width:180px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.favorecidoBeneficiario}" />
				</app:scrollableColumn>
									
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_bco_ag_conta_credito}" style="width:200px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.bancoAgenciaContaDebitoFormatado}" />
				</app:scrollableColumn>  
				 
			</app:scrollableDataTable>
		</br:brPanelGroup>
		
	</br:brPanelGrid>	
	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{desautorizarPagamentosConsolidadosBean.pesquisarDesPagtoConsolidadosParcial}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"  onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"	  styleClass="bto1" style="margin-left: 3px;"  value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"  onclick="funcPaginacao();"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
		<f:verbatim><hr class="lin"></f:verbatim>
		
		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
			<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup style="text-align:left;width:150px" >
					<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{desautorizarPagamentosConsolidadosBean.voltar}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>				
				</br:brPanelGroup>
				<br:brPanelGroup style="text-align:right;width:600px" >			
					<br:brCommandButton id="btnDesautorizarSelecionados" disabled="#{!desautorizarPagamentosConsolidadosBean.panelBotoes}" styleClass="bto1" value="#{msgs.btn_desautorizar_selecionados}"  action="#{desautorizarPagamentosConsolidadosBean.desautorizarSelecionados}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>		
				</br:brPanelGroup>
			 </br:brPanelGrid>	
	    </a4j:outputPanel>	
	    
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>	
</brArq:form>		