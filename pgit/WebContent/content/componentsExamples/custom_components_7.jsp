<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="x"%>

<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="cus"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>



<f:verbatim><br></f:verbatim>

<%-- MENSAJES DE ERROR --%>
<h:panelGrid columns="1">             
	<h:column>
	  	<h:panelGroup>
			<h:messages style="color: red" showDetail="false" />
		</h:panelGroup>	
	</h:column>	  	
</h:panelGrid>


<h:panelGrid columns="1" width="100%">

	<h:column>
		<h:panelGrid styleClass="mainPanel" columns="2" width="100%" columnClasses="panelGridColumHeader50,panelGridColumHeader50">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="Rendered result"/>
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="JSF Tag"/>
				</h:panelGroup>
			</h:column>			
		</h:panelGrid>
	</h:column>
	
	
	<%-- UIPanel  brPanelGrid  ****************************************************************************--%>			
	<h:column>	
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UIPanel  - (brPanelGrid)"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>
	
	<h:column>

		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">
			<h:column>	
			  	<h:panelGroup>		 	
			  	
					<cus:brPanelGrid border="1"  
									 columns="4" 
									 footerClass="HtmlOutputTextBoldBradesco" 
									 headerClass="HtmlOutputTextBoldBradesco" 
								     columnClasses="HtmlOutputTextBradesco, HtmlOutputTextBradesco">
					  <f:facet name="header">
					    <cus:brOutputText value="Table with numbers"/>
					  </f:facet>
					  <cus:brOutputText value="1" />
					  <cus:brOutputText value="2" />
					  <cus:brOutputText value="3" />
					  <cus:brOutputText value="4" />
					  <cus:brOutputText value="5" />
					  <cus:brOutputText value="6" />
					  <cus:brOutputText value="7" />					  
					  <f:facet name="footer">
					    <cus:brPanelGroup>
					      <cus:brOutputText value="one row"  />
					      <cus:brOutputText value=" "  />
					      <cus:brOutputText
					       value="grouped with panelGroup" />
					    </cus:brPanelGroup>
					  </f:facet>					  
					</cus:brPanelGrid>
					
				</h:panelGroup>
			</h:column>
			
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">

&lt;cus:brPanelGrid border="1"
  columns="4"
  footerClass="HtmlOutputTextBoldBradesco"
  headerClass="HtmlOutputTextBoldBradesco"
  columnClasses="HtmlOutputTextBradesco,
	HtmlOutputTextBradesco"&gt;
  &lt;f:facet name="header"&gt;
    &lt;cus:brOutputText value="Table with numbers"/&gt;
  &lt;/f:facet&gt;
  &lt;cus:brOutputText value="1" /&gt;
  &lt;cus:brOutputText value="2" /&gt;
  &lt;cus:brOutputText value="3" /&gt;
  &lt;cus:brOutputText value="4" /&gt;
  &lt;cus:brOutputText value="5" /&gt;
  &lt;cus:brOutputText value="6" /&gt;
  &lt;cus:brOutputText value="7" /&gt;
  &lt;f:facet name="footer"&gt;
    &lt;cus:brPanelGroup&gt;
      &lt;cus:brOutputText value="one row"  /&gt;
       &lt;cus:brOutputText value=" "  /&gt;
       &lt;cus:brOutputText
         value="grouped with panelGroup" /&gt;
    &lt;/cus:brPanelGroup&gt;
  &lt;/f:facet&gt;
&lt;/cus:brPanelGrid&gt;

</font>
</pre>	
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
		</h:panelGrid>
	</h:column>					
	
	
</h:panelGrid>










