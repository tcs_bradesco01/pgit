<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="x"%>

<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="cus"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>



<f:verbatim><br></f:verbatim>

<%-- MENSAJES DE ERROR --%>
<h:panelGrid columns="1">             
	<h:column>
	  	<h:panelGroup>
			<h:messages style="color: red" showDetail="false" />
		</h:panelGroup>	
	</h:column>	  	
</h:panelGrid>


<h:panelGrid columns="1" width="100%">

	<h:column>
		<h:panelGrid styleClass="mainPanel" columns="2" width="100%" columnClasses="panelGridColumHeader50,panelGridColumHeader50">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="Rendered result"/>
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="JSF Tag"/>
				</h:panelGroup>
			</h:column>			
		</h:panelGrid>
	</h:column>
	
	
	<%-- UISelectBoolean  brSelectBooleanCheckbox **************************************************************************--%>
	<%-- *******************************************************************************************************************--%>					
	<h:column>	
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UISelect"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>
	
	<%-- UISelectBoolean  brSelectBooleanCheckbox **************************--%>				
	<h:column>	
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UISelect  - Renderer (brSelectBooleanCheckbox)"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>
	<h:column>
		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">
			<h:column>			
				<h:panelGroup>		
					<cus:brSelectBooleanCheckbox title="emailUpdates" value="#{supportBean.car}"/>
					<cus:brOutputText value="Would you like email updates?"/>
				</h:panelGroup>
			</h:column>			
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brSelectBooleanCheckbox
 title="emailUpdates"
  value="#{jsfexample.wantsEmailUpdates}" &gt;
&lt;/cus:brSelectBooleanCheckbox&gt;
&lt;cus:brOutputText
  value="Would you like email updates?"/&gt;
</font>
</pre>	
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
		</h:panelGrid>
	</h:column>		


	<%-- UISelectMany brSelectManyCheckbox  **************************--%>			
	<h:column>	
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UISelectMany  - Renderer (brSelectManyCheckbox)"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>
	<h:column>
		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">
			<h:column>			
				<h:panelGroup>	
					<h:form id="selectManyCheckboxForm">	
						<cus:brSelectManyCheckbox layout="pageDirection" 
											  	  id="coches" 
											      value="#{supportBean.coches}" >
											  
							<f:selectItem itemLabel="Bmw" itemValue="Bmw"/>
							<f:selectItem itemLabel="Audi" itemValue="Audi"/>					
							<f:selectItem itemLabel="Porsche" itemValue="Porsche"/>									
						</cus:brSelectManyCheckbox>
					</h:form>						
				</h:panelGroup>
			</h:column>			
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brSelectManyCheckbox id="cars"
 value="#{carsBean.car}"&gt;
  &lt;f:selectItems
   value="#{carBean.carList}"/&gt;
&lt;/cus:brSelectManyCheckbox&gt;
</font>
</pre>	
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
		</h:panelGrid>
	</h:column>	
	
	
	<%-- UISelectMany brSelectManyMenu  **************************************************--%>			
	<h:column>	
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UISelectMany  - Renderer (brSelectManyMenu)"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>	
	<h:column>
		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">
			<h:column>			
				<h:panelGroup>	
					<h:form id="selectManyMenuForm">	
						<cus:brSelectManyMenu id="cars_selectManyMenu" value="#{supportBean.coches}" >
							<f:selectItem itemLabel="Bmw" itemValue="Bmw"/>
							<f:selectItem itemLabel="Audi" itemValue="Audi"/>					
							<f:selectItem itemLabel="Porsche" itemValue="Porsche"/>													   
						</cus:brSelectManyMenu>											  
					</h:form>						
				</h:panelGroup>
			</h:column>			
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brSelectManyMenu
 id="cars_selectManyMenu"
  value="#{carBean.car}"&gt;
   &lt;f:selectItems
    value="#{carBean.carList}"/&gt;
&lt;/cus:brSelectManyMenu&gt;
</font>
</pre>	
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
		</h:panelGrid>
	</h:column>		
	
	<%-- UISelectMany brSelectManyMenuColor  **************************************************--%>			
	<%-- UISelectMany brSelectOneListbox  **************************************************--%>			
	<h:column>	
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UISelectOne  - Renderer (brSelectOneListbox)"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>
	<h:column>
		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">
			<h:column>			
				<h:panelGroup>	
					<h:form id="selectManyListboxForm">	
						<cus:brSelectOneListbox id="cars_selectListMenu" value="#{supportBean.coches}">
							<f:selectItem itemLabel="Bmw" itemValue="Bmw"/>
							<f:selectItem itemLabel="Audi" itemValue="Audi"/>					
							<f:selectItem itemLabel="Porsche" itemValue="Porsche"/>													   
						</cus:brSelectOneListbox>
					</h:form>						
				</h:panelGroup>
			</h:column>			
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brSelectOneListbox
 id="cars_selectManyListbox"
  value="#{carBean.car}"&gt;
   &lt;f:selectItems
    value="#{carBean.carList}"&gt;
&lt;/cus:brSelectOneListbox&gt;
</font>
</pre>	
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
		</h:panelGrid>
	</h:column>			
	
	
	<%-- UISelectMany brSelectOneListboxColor  **************************************************--%>			

	<%-- UISelectMany brSelectOneRadio   **************************************************--%>			
	<h:column>	
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UISelectOne  - Renderer (brSelectOneRadio)"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>	
	<h:column>
		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">
			<h:column>			
				<h:panelGroup>	
					<h:form id="selectOneRadioForm">	
						<cus:brSelectOneRadio id="cars_selectOneRadio" value="#{supportBean.coches}">
							<f:selectItem itemLabel="Bmw" itemValue="Bmw"/>
							<f:selectItem itemLabel="Audi" itemValue="Audi"/>					
							<f:selectItem itemLabel="Porsche" itemValue="Porsche"/>													   
						</cus:brSelectOneRadio>
					</h:form>						
				</h:panelGroup>
			</h:column>			
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brSelectOneRadio
 value="#{carBean.currentCar}"&gt;
  &lt;f:selectItems
   value="#{carBean.carList}" /&gt;
&lt;/cus:brSelectOneRadio&gt;
</font>
</pre>	
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
		</h:panelGrid>
	</h:column>	
	
	<%-- UISelectMany brSelectOneRadioColor   **************************************************--%>			
	
	<%-- UISelectMany brSelectOneMenu    **************************************************--%>			
	<h:column>	
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UISelectOne  - Renderer (brSelectOneMenu)"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>	
	<h:column>
		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">
			<h:column>			
				<h:panelGroup>	
					<h:form id="selectOneMenuForm">	
						<cus:brSelectOneMenu id="cars_selectOneMenu" value="#{supportBean.coches}">
							<f:selectItem itemLabel="Bmw" itemValue="Bmw"/>
							<f:selectItem itemLabel="Audi" itemValue="Audi"/>					
							<f:selectItem itemLabel="Porsche" itemValue="Porsche"/>													   
						</cus:brSelectOneMenu>
					</h:form>						
				</h:panelGroup>
			</h:column>			
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brSelectOneMenu id="selectCar"
 value="#{carBean.currentCar}"&gt;
  &lt;f:selectItems
   value="#{carBean.carList}" /&gt;
&lt;/cus:brSelectOneMenu&gt;
</font>
</pre>	
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
		</h:panelGrid>
	</h:column>			
	
	
	<%-- UISelectMany brSelectOneMenuColor    **************************************************--%>			
	
</h:panelGrid>










