<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="x"%>

<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="cus"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>



<f:verbatim>
	<br>
</f:verbatim>

<%-- MENSAJES DE ERROR --%>
<h:panelGrid columns="1">
	<h:column>
		<h:panelGroup>
			<h:messages style="color: red" showDetail="false" />
		</h:panelGroup>
	</h:column>
</h:panelGrid>


<h:panelGrid columns="1" width="100%">

	<h:column>
		<h:panelGrid styleClass="mainPanel" columns="2" width="100%"
			columnClasses="panelGridColumHeader50,panelGridColumHeader50">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="Rendered result" />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="JSF Tag" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>


	<%-- UIOutput *****************************************************************************************--%>
	<%-- **************************************************************************************************--%>

	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIOutput" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>
	<%-- UIOutput Rótulo  *********************************--%>
	<h:column>
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIOutput - Rótulo" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>
		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>
				<h:panelGroup>
					<cus:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="outputText text..." />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brOutputText
	 styleClass="HtmlOutputTextRotuloBradesco"
 value="#{jsfexample.zipCode}"/&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<%-- UIOutput Bullet  *********************************--%>
	<h:column>
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIOutput - Bullet Regular" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>
		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>
				<h:panelGroup>
					<cus:brOutputText styleClass="HtmlOutputTextBulletRegularBradesco"
						value="outputText text..." />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brOutputText
	 styleClass="HtmlOutputTextBulletRegularBradesco"
 value="#{jsfexample.zipCode}"/&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>
	<%-- UIOutput Bullet Bold  *********************************--%>
	<h:column>
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIOutput - Bullet Bold" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>
		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>
				<h:panelGroup>
					<cus:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco"
						value="outputText text..." />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brOutputText
	 styleClass="HtmlOutputTextBulletBoldBradesco"
 value="#{jsfexample.zipCode}"/&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<%-- UIOutput Resposta  *********************************--%>
	<h:column>
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIOutput - Resposta" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>
		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>
				<h:panelGroup>
					<cus:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="outputText text..." />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brOutputText
	 styleClass="HtmlOutputTextRespostaBradesco"
 value="#{jsfexample.zipCode}"/&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>




</h:panelGrid>










