<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="x"%>

<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="cus"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>

<f:verbatim><br></f:verbatim>

<%-- MENSAJES DE ERROR --%>
<h:panelGrid columns="1">             
	<h:column>
	  	<h:panelGroup>
			<h:messages style="color: red" showDetail="false" />
		</h:panelGroup>	
	</h:column>	  	
</h:panelGrid>


<h:panelGrid columns="1" width="100%">

	<h:column>
		<h:panelGrid styleClass="mainPanel" columns="2" width="100%" columnClasses="panelGridColumHeader50,panelGridColumHeader50">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="Rendered result"/>
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="JSF Tag"/>
				</h:panelGroup>
			</h:column>			
		</h:panelGrid>
	</h:column>
	
	<%-- UIMessage and brMessage   **************************************************************************--%>
	<%-- ****************************************************************************************************--%>			
	<h:column>	
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UIMessage and UIMessages"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>
	
	<%-- brMessage   ******************************************--%>
	<h:column>	
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UIMessage and UIMessages  - Renderer (brMessage)"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>	
	<h:column>
		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">
			<h:column>			
				<h:panelGroup>		
					<h:form id="brMessageForm">			
						<h:panelGrid cellpadding="0" cellspacing="0" columns="1">	
							<h:panelGroup>	
								<cus:brOutputText value="Enter address:	"/>	
							</h:panelGroup>	
							<h:panelGroup>	
								<cus:brInputText id="mensaje" value="mensaje" required="true"/>	
								<cus:brMessage  for="mensaje" showSummary="false" showDetail="true"/>	
							</h:panelGroup>	
							<h:panelGroup>
								<cus:brCommandButton id="commandButton2" value="Command Button" action="#{supportBean.action}" />			
							</h:panelGroup>
						</h:panelGrid>
					</h:form>
				</h:panelGroup>
			</h:column>			
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
Enter address:
&lt;cus:brMessage for="useraddress" /&gt;
&lt;cus:brInputText id="useraddress"
 value="#{jsfexample.address}"
  required="true"/&gt;
&lt;cus:brCommandButton action="save" value="Save"/&gt;
</font>
</pre>	
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
		</h:panelGrid>
	</h:column>	
	
	<%-- brMessageColor   ******************************************--%>
	<h:column>	
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UIMessage and UIMessages  - Renderer (brMessageColor)"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>	
	<h:column>
		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">
			<h:column>			
				<h:panelGroup>		
					<h:form id="brMessageColorForm">			
						<h:panelGrid cellpadding="0" cellspacing="0" columns="1">	
							<h:panelGroup>	
								<cus:brOutputText value="Enter address:	"/>	
							</h:panelGroup>	
							<h:panelGroup>	
								<cus:brInputText id="brMessageColor" value="mensaje" required="true"/>						
								<cus:brMessageColor  for="brMessageColor" showSummary="false" showDetail="true"/>	
							</h:panelGroup>
							<h:panelGroup>					
								<cus:brCommandButton id="commandButton2" value="Command Button" action="#{supportBean.action}" />			
							</h:panelGroup>	
						</h:panelGrid>
					</h:form>
				</h:panelGroup>
			</h:column>			
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
Enter address:
&lt;cus:brMessageColor for="useraddress" /&gt;
&lt;cus:brInputText id="useraddress"
 value="#{jsfexample.address}"
  required="true"/&gt;
&lt;cus:brCommandButton action="save" value="Save"/&gt;
</font>
</pre>	
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
		</h:panelGrid>
	</h:column>		

</h:panelGrid>










