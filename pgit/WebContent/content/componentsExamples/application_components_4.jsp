<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="x"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>

<f:verbatim>
	<br>
</f:verbatim>

<%-- Mensagens --%>
<h:panelGrid columns="1">
	<h:column>
		<h:panelGroup>
			<h:messages showDetail="false" globalOnly="true" />
		</h:panelGroup>
	</h:column>
</h:panelGrid>

<h:panelGrid columns="1" width="100%">
	<h:column>
		<h:panelGrid styleClass="mainPanel" columns="2" width="100%"
			columnClasses="panelGridColumHeader50,panelGridColumHeader50">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="Rendered result" />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="JSF Tag" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<%-- UIModalMessages --%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIModalMessages" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIModalMessages" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>
		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>
				<h:panelGroup>
					<h:form id="brCommandButtonForm">
						<h:panelGrid columns="1">
							<h:commandButton action="#{supportBean.modalMessagesSucessoAction}" value="Mensagem de Sucesso" />
							<h:commandButton action="#{supportBean.modalMessagesViewException}" value="BradescoViewException" />		
							<h:commandButton action="#{supportBean.modalMessagesException}" value="Outras Exception" />	
							<h:commandButton action="#{supportBean.modalMessagesSucessoPath}" value="Action com Path" />
						</h:panelGrid>
					</h:form>
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
&lt;app:modalMessages
	id="modalMessages"
	buttonLabelForInfo ="OK"
	buttonLabelForError ="OK"
	titleError="ERROR"
	titleInfo="MENSAGEM"
	severityErrorImage="/images/errorImage.gif"
	severityInfoImage="/images/infoImage.gif"
	closeImage="/images/closeImage.gif"
	buttonImage="/images/buttonImage.gif"
	buttonClass="buttonCssClass"
	mainDivClass="mainDivCssClass"
	headerDivClass="headerDivCssClass"
	bodyDivClass="bodyDivCssClass"
	footerDivClass="footerDivCssClass"
	textClass="textCssClass"
	overlayColor="#000000" /&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>
</h:panelGrid>
