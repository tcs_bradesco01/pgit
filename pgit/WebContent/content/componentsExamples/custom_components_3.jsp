<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="b"%>
<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="x"%>

<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="cus"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>



<f:verbatim>
	<br>
</f:verbatim>

<%-- MENSAJES DE ERROR --%>
<h:panelGrid columns="1">
	<h:column>
		<h:panelGroup>
			<h:messages style="color: red" showDetail="false" />
		</h:panelGroup>
	</h:column>
</h:panelGrid>


<h:panelGrid columns="1" width="100%">

	<h:column>
		<h:panelGrid styleClass="mainPanel" columns="2" width="100%"
			columnClasses="panelGridColumHeader50,panelGridColumHeader50">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="Rendered result" />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="JSF Tag" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>


	<%-- UIInput ******************************************************************************************--%>
	<%-- **************************************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIInput" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<%-- brInputText **************************************************--%>
	<h:column>
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIInput - Renderer (brInputText)" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>

		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>
				<h:panelGroup>
					<cus:brInputText id="brInputText" value="brInputText" />
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brInputText id="address"
  value="#{jsfexample.address}" /&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<%-- brInputTextObrigatorio **************************************************--%>
	<h:column>
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIInput Obrigatório" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>


	<h:column>
		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>
				<h:panelGroup>
					<b:brGraphicImage styleClass="bullet" url="images/bullet.jpg" />
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;b:brGraphicImage styleClass="bullet" url="images/bullet.jpg" /&gt; 
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>





		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>
				<h:panelGroup>
					<cus:brInputText id="brInputTex2" value="brInputText"
						styleClass="HtmlInputTextObrigatorioBradesco" />
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brInputText id="address"
  value="#{jsfexample.address}"
  styleClass="HtmlInputTextObrigatorioBradesco" /&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	
</h:panelGrid>


	<%-- UIInput brInputTextarea Com Estilo  *********************************--%>
	<h:column>
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIInput com estilo" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>

		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>
				<h:panelGroup>
					<cus:brInputTextarea styleClass="HtmlInputTextareaEstiloBradesco" id="textArea2" rows="4" cols="20"
						value=" Teste ESTILO" />
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;cus:brInputTextarea 
	styleClass="HtmlInputTextareaEstiloBradesco" id="textArea"
   rows="4" cols="7"
   value="Text goes here.."/&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>


	