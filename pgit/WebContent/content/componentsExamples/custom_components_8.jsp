<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="x"%>
<%@taglib prefix="rich" uri="http://richfaces.org/rich"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="cus"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>

<h:form>

<h:panelGrid columns="1">
		<h:column>
			<h:panelGroup>
				<h:messages showDetail="false" globalOnly="true" />
			</h:panelGroup>
		</h:column>
	</h:panelGrid>

	<h:panelGrid columns="1" width="100%">
		<h:column>
			<h:panelGrid styleClass="mainPanel" columns="2" width="100%"
				columnClasses="panelGridColumHeader50,panelGridColumHeader50">
				<h:column>
					<h:panelGroup>
						<h:outputText styleClass="HtmlOutputTextTitleBradesco"
							value="Rendered result" />
					</h:panelGroup>
				</h:column>
				<h:column>
					<h:panelGroup>
						<h:outputText styleClass="HtmlOutputTextTitleBradesco"
							value="JSF Tag" />
					</h:panelGroup>
				</h:column>
			</h:panelGrid>
		</h:column>


	<%-- UIScrollableDataTable  ****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIScrollableDataTable" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>
		<h:panelGrid styleClass="elementPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIScrollableDataTable" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	

	<h:column>

		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>
			<app:scrollableDataTable id="dataTable" value="#{scrollableDataTablePag.dependencias}" var="dep" rows="8" width="340px">
				<app:scrollableColumn width="80px">
					<f:facet name="header">
						<h:outputText value="<b>Codigo</b>" styleClass="tableFontStyle"
							escape="false" />
					</f:facet>
					<h:outputText value="#{dep.cdConta}" />
				</app:scrollableColumn>
				<app:scrollableColumn width="80">
					<f:facet name="header">
						<h:outputText value="<b>Desc Conta</b>" styleClass="tableFontStyle"
							escape="false" />
					</f:facet>
					<h:outputText value="#{dep.descConta}" />

				</app:scrollableColumn>
				<app:scrollableColumn width="160px">
					<f:facet name="header">
						<h:outputText value="<b>Saldo</b>" styleClass="tableFontStyle"
							escape="false" />
					</f:facet>
					<h:outputText value="#{dep.vlSaldoContabil}"
						styleClass="tableFontStyle" />

				</app:scrollableColumn>
				<!--...//Set of columns and header/footer facets-->
			</app:scrollableDataTable>
			
			
			<arq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{scrollableDataTablePag.pesquisar}" >
			    <f:facet name="first">
			      <arq:pdcCommandButton id="primeira"
			        styleClass="button"
			        value="#{msgs.label_primeira}" title="#{msgs.label_primeira}"/>
			    </f:facet>
			    <f:facet name="fastrewind">
			      <arq:pdcCommandButton id="retrocessoRapido"
			        styleClass="button" style="margin-left: 3px;"
			        value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}"/>
			    </f:facet>
			    <f:facet name="previous">
			      <arq:pdcCommandButton id="anterior"
			        styleClass="button" style="margin-left: 3px;"
			        value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			    </f:facet>
			    <f:facet name="next">
			      <arq:pdcCommandButton id="proxima"
			        styleClass="button" style="margin-left: 3px;"
			        value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			    </f:facet>
			    <f:facet name="fastforward">
			      <arq:pdcCommandButton id="avancoRapido"
			        styleClass="button" style="margin-left: 3px;"
			        value="#{msgs.label_avanco}" title="#{msgs.label_avanco}"/>
			    </f:facet>
			    <f:facet name="last">
			      <arq:pdcCommandButton id="ultima"
			        styleClass="button" style="margin-left: 3px;"
			        value="#{msgs.label_ultima}" title="#{msgs.label_ultima}"/>
			    </f:facet>
			  </arq:pdcDataScroller>

			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">

&lt;app:scrollableDataTable id="dataTable" value="#{scrollableDataTable.allPessoas}" var="pessoas"
    rows="40" width="340px" height="396px"&gt;
  &lt;app:scrollableColumn width="80px"&gt;
    &lt;f:facet name="header"&gt;
      &lt;h:outputText value="<b>Código</b>" styleClass="tableFontStyle" escape="false" /&gt;
    &lt;/f:facet&gt;
    &lt;h:outputText value="#{pessoas.id}" /&gt;
  &lt;/app:scrollableColumn&gt;
  &lt;app:scrollableColumn width="80"&gt;
    &lt;f:facet name="header"&gt;
      &lt;h:outputText value="<b>Nome</b>" styleClass="tableFontStyle" escape="false" /&gt;
    &lt;/f:facet>
    &lt;h:outputText value="#{pessoas.nome}" /&gt;
  &lt;/app:scrollableColumn&gt;
  &lt;app:scrollableColumn width="160px"&gt;
    &lt;f:facet name="header"&gt;
      &lt;h:outputText value="<b>E-mail</b>" styleClass="tableFontStyle" escape="false" /&gt;
    &lt;/f:facet&gt;
    &lt;h:outputText value="#{pessoas.email}" styleClass="tableFontStyle" /&gt;
  &lt;/app:scrollableColumn&gt;
&lt;/app:scrollableDataTable&gt;

&lt;arq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{scrollableDataTable.pesquisar}"&gt;
  &lt;f:facet name="first"&gt;
    &lt;arq:pdcCommandButton id="primeira"
      styleClass="button"
      value="#{msgs.label_primeira}" title="#{msgs.label_primeira}"/&gt;
  &lt;/f:facet&gt;
  &lt;f:facet name="fastrewind"&gt;
    &lt;arq:pdcCommandButton id="retrocessoRapido"
      styleClass="button" style="margin-left: 3px;"
      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}"/&gt;
  &lt;/f:facet&gt;
  &lt;f:facet name="previous"&gt;
    &lt;arq:pdcCommandButton id="anterior"
      styleClass="button" style="margin-left: 3px;"
      value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/&gt;
  &lt;/f:facet&gt;
  &lt;f:facet name="next"&gt;
    &lt;arq:pdcCommandButton id="proxima"
      styleClass="button" style="margin-left: 3px;"
      value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/&gt;
  &lt;/f:facet&gt;
  &lt;f:facet name="fastforward"&gt;
    &lt;arq:pdcCommandButton id="avancoRapido"
      styleClass="button" style="margin-left: 3px;"
      value="#{msgs.label_avanco}" title="#{msgs.label_avanco}"/&gt;
  &lt;/f:facet&gt;
  &lt;f:facet name="last"&gt;
    &lt;arq:pdcCommandButton id="ultima"
      styleClass="button" style="margin-left: 3px;"
      value="#{msgs.label_ultima}" title="#{msgs.label_ultima}"/&gt;
  &lt;/f:facet&gt;
&lt;/arq:pdcDataScroller&gt;

</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>


</h:panelGrid>

</h:form>