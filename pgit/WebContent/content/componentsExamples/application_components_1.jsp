<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="x"%>

<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="cus"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>


<h:panelGrid columns="1" width="100%">

	<h:column>
		<h:panelGrid styleClass="mainPanel" columns="2" width="100%" columnClasses="panelGridColumHeader50,panelGridColumHeader50">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="Rendered result"/>
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="JSF Tag"/>
				</h:panelGroup>
			</h:column>			
		</h:panelGrid>
	</h:column>
	

	<%-- UIDeclaracaoConcordancia *********************************************************************--%>
	<h:column>	
		<h:panelGrid  styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UIDeclaracaoConcordancia"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>
	
	<h:column>

		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">
				
			<h:column>			
				<h:panelGroup>		
					<h:form id="declaracaoConcordanciaForm">
						<app:declaracaoConcordancia  id="declaracao"
												  rendered="true"
								  				  areaMessageCols="40"
								  				  areaMessageRows="5"
								  				  areaMessageValue="#{supportBean.textoArea}"
								  				  areaMessageStyle="HtmlOutputTextBradesco"
								  				  checkStyle="HtmlOutputTextBradesco"
								  				  labelCheckStyle="HtmlOutputTextBradesco"
								  				  labelCheckValue="#{msgs.label_check_value}"
								  				  labelInfoValue="#{msgs.label_info_value}"
								  				  labelInfoStyle="HtmlOutputTextBradesco"
								  				  tableStyleClass="HtmlOutputTextBradesco" 
								  				  tr1StyleClass="HtmlOutputTextBradesco"
								  				  tr2StyleClass="HtmlOutputTextBradesco"
								  				  tr3StyleClass="HtmlOutputTextBradesco"
								  				  titleTextArea = "Area Message"        
											      titleCheck = "Check"        
        									      titleLabelCheck = "Label Check"        
											      titleLabelInfo = "Label Info" />
								  				  
						<h:commandButton value="Concordo" action="#{supportBean.actionNext}" />	
						
					<h:panelGroup>						
						<h:message for="declaracao" 
								   showSummary="false" 
								   showDetail="true" 
								   styleClass="error" />
					</h:panelGroup>							  				  
					
					</h:form>						
				</h:panelGroup>
			</h:column>				

			
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
&lt;app:declaracaoConcordancia 
  id="declaracao"
  rendered="true"
  areaMessageCols="40"
  areaMessageRows="5"
  areaMessageValue="#{supportBean.textoArea}"
  areaMessageStyle="class"
  checkStyle="class"
  labelCheckStyle="class"
  labelCheckValue="#{msgs.label_check_value}"
  labelInfoValue="#{msgs.label_info_value}"
  labelInfoStyle="class"
  tableStyleClass="class" 
  tr1StyleClass="class"
  tr2StyleClass="class"
  tr3StyleClass="class"
  titleTextArea = "Area Message"        
  titleCheck = "Check"        
  titleLabelCheck = "Label Check"        
  titleLabelInfo = "Label Info"/&gt;
</font>
</pre>		
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
		</h:panelGrid>
	</h:column>
	
	
	<%-- UINumeroIdentificacao *****************************************************************************--%>
	<h:column>	
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UINumeroIdentificacao"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>
	
	<h:column>

		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">

			<h:form id="numIdenficForm">
				<h:column>			
					<h:panelGroup>		
						
						<app:numeroIdenticacao />
									 
							<f:verbatim>&nbsp;&nbsp;&nbsp;</f:verbatim>			 
							<h:commandButton value="Ok" 
											 styleClass="HtmlOutputTextBradesco" 
											 action="#{supportBean.action}" />					                                
	
					</h:panelGroup>					

				</h:column>
			</h:form>						
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
&lt;app:numeroIdenticacao /&gt;
</font>
</pre>		
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
			
		</h:panelGrid>
	</h:column>

	
	
	<%-- UISecretPhrase *****************************************************************************--%>
	<h:column>	
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" value="UISecretPhrase"/>	
				</h:panelGroup>
			</h:column>					
		</h:panelGrid>
	</h:column>
	
	<h:column>

		<h:panelGrid columns="2" width="100%" columnClasses="panelGridColumHeader50,color3">

			<h:form id="secretPhraseForm">
				<h:column>			
					<h:panelGroup>		

						<app:secretPhrase value="#{supportBean.phrase}"
						                id="secretPhrase"
										idPhrase="idPhrase"
										idCount="idCount"
						                minlength="12"
						                maxlength="40" 
						                size="45" 
						                titlePhrase="Secret Phrase"
						                titleCount="Count" />
						                
						<h:commandButton value="Ok" 
										 styleClass="HtmlOutputTextBradesco" 
										 action="#{supportBean.action}" />					                                
	
					</h:panelGroup>
					
					<h:panelGroup>						
						<h:message for="secretPhrase" 
								   showSummary="false" 
								   showDetail="true" 
								   styleClass="error" />
					</h:panelGroup>
				</h:column>
			</h:form>						
			<h:column>			
				<h:panelGroup>
					<f:verbatim>
<pre>
<font class="HtmlOutputTextBradesco">
&lt;app:secretPhrase
  value="#{supportBean.phrase}"
  id="secretPhrase"
  idPhrase="idPhrase"
  idCount="idCount"
  minlength="12"
  maxlength="40" 
  size="45" 
  titlePhrase="Secret Phrase"
  titleCount="Count"/&gt;
</font>
</pre>		
					</f:verbatim>		
				</h:panelGroup>										
			</h:column>			
			
		</h:panelGrid>
	</h:column>	
	
</h:panelGrid>