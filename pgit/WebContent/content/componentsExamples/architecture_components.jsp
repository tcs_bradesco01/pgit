<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="x"%>

<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="cus"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>


<h:panelGrid columns="1" width="100%">

	<h:column>
		<h:panelGrid styleClass="mainPanel" columns="2" width="100%"
			columnClasses="panelGridColumHeader50,panelGridColumHeader50">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="Rendered result" />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="JSF Tag" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<%-- UIApplet *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIApplet" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>

		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">

			<h:column>
				<h:panelGroup>

					<arq:applet codebase="./applets/" code="Clock3D.class" height="200"
						width="200">

						<arq:param name="fps" value="18" />
						<arq:param name="a1" value="12500" />
						<arq:param name="pixd" value="29" />
						<arq:param name="pixangle" value="5" />
						<arq:param name="radius" value="26" />
						<arq:param name="roty" value="-4" />
						<arq:param name="rotx" value="0" />
						<arq:param name="rotz" value="0.401" />
						<arq:param name="irotx" value="0" />
						<arq:param name="iroty" value="0" />
						<arq:param name="irotz" value="00" />
						<arq:param name="style" value="1" />
						<arq:param name="color" value="#{supportBean.appletColor}" />
						<arq:param name="bgcolor" value="#{supportBean.appletBgColor}" />
						<arq:param name="12hour" value="0" />
					</arq:applet>

				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:applet codebase="./applets/" 
  code="Clock3D.class"
  height="200" width="200" &gt;
  &lt;arq:param name="fps" value="18" /&gt;
  ...
  &lt;arq:param name="color" 
    value="#{supportBean.appletColor}" /&gt;
  &lt;arq:param name="bgcolor" 
    value="#{supportBean.appletBgColor}" /&gt;
  ...
&lt;/arq:applet&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>

		</h:panelGrid>
	</h:column>
	<%-- UIObject *****************************************************************************--%>

	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIObject (Objetos Media)" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>

		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">

			<h:column>
				<h:panelGroup>
					<arq:object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
						codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=4,0,2,0"
						width="65" height="52">
						<arq:param name="movie" value="./flash/pinkworm1.swf" />
						<arq:param name="quality" value="high" />
						<arq:param name="BGCOLOR" value="#00FFCC" />

						<arq:embed src="./flash/pinkworm1.swf"
							pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"
							width="65" height="52">
							<arq:param name="type" value="application/x-shockwave-flash" />
							<arq:param name="quality" value="high" />
							<arq:param name="BGCOLOR" value="#00FFCC" />
						</arq:embed>
					</arq:object>

				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:object 
  classid=
   "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" 
  codebase="http://download.macromedia.com/pub/
   shockwave/cabs/flash/swflash.cab#version=4,0,2,0" 
  width="65" height="52"&gt;
    
  &lt;arq:param name="movie" 
    value="./flash/pinkworm1.swf"/&gt;
  &lt;arq:param name="quality" value="high"/&gt;
  &lt;arq:param name="BGCOLOR" value="#00FFCC"/&gt;
  
  &lt;%-- Objeto embed para Netscape,Mozilla,etc --%&gt;
  &lt;arq:embed src="./flash/pinkworm1.swf"
   pluginspage="http://www.macromedia.com/shockwave/
    download/index.cgi?P1_Prod_Version=ShockwaveFlash"
   width="65" height="52"&gt;
   &lt;arq:param name="type" 
    value="application/x-shockwave-flash"/&gt;
   &lt;arq:param name="quality" value="high"/&gt;
   &lt;arq:param name="BGCOLOR" value="#00FFCC"/&gt;
  &lt;/arq:embed&gt;
  
&lt;/arq:object&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>

		</h:panelGrid>
	</h:column>

	<%-- UIDataUser *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIDataUser" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>

		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">

			<h:column>
				<h:panelGroup>
					<arq:dataUser id="dataUser" rendered="true"
						valueLabel="#{msgs.label_user}"
						styleClassLabel="HtmlOutputTextColorBradesco"
						styleClassValue="HtmlOutputTextBradesco" param="user"
						titleLabel="label" titleValue="value" />
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:dataUser id="dataUser"
  rendered="true"
  valueLabel="#{msgs.label_user}"
  styleClassLabel="HtmlOutputTextColorBradesco"
  styleClassValue="HtmlOutputTextBradesco"
  param="user"
  titleLabel="label"
  titleValue="value" /&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<arq:dataUser id="dataAgencia" rendered="true"
						valueLabel="#{msgs.label_agencia}:"
						styleClassLabel="HtmlOutputTextColorBradesco"
						styleClassValue="HtmlOutputTextBradesco" param="agencia"
						titleLabel="label" titleValue="value" />
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:dataUser id="dataAgencia"
  rendered="true"
  valueLabel="#{msgs.label_agencia}:"
  styleClassLabel="HtmlOutputTextColorBradesco"
  styleClassValue="HtmlOutputTextBradesco"
  param="agencia"
  titleLabel="label"
  titleValue="value" /&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>

		</h:panelGrid>
	</h:column>


	<%-- UIObjectManager *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIObjectManager" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>

		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>
				<h:panelGroup>
					<arq:objectManager id="objectManager1" rendered="true"
						valueLabel="#{msgs.label_retrieve_objeto}:"
						styleClassLabel="HtmlOutputTextColorBradesco"
						styleClassValue="HtmlOutputTextBradesco" param="paramName"
						scope="request" titleLabel="label" titleValue="value" />
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:objectManager id="objectManager1"
  rendered="true"
  valueLabel="#{msgs.label_retrieve_objeto}:"
  styleClassLabel="HtmlOutputTextColorBradesco"
  styleClassValue="HtmlOutputTextBradesco"
  param="paramName"
  scope="request" 
  titleLabel="label"
  titleValue="value" /&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<arq:objectManager id="objectManager2" rendered="true"
						valueLabel="#{msgs.label_retrieve_objeto}:"
						styleClassLabel="HtmlOutputTextColorBradesco"
						styleClassValue="HtmlOutputTextBradesco" param="test_object"
						scope="session" titleLabel="label" titleValue="value" />
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:objectManager id="objectManager2"
  rendered="true"
  valueLabel="#{msgs.label_retrieve_objeto}:"
  styleClassLabel="HtmlOutputTextColorBradesco"
  styleClassValue="HtmlOutputTextBradesco"
  param="test_object"
  scope="session"
  titleLabel="label"
  titleValue="value"/&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>


		</h:panelGrid>
	</h:column>



	<%-- UISubmitCheck *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UISubmitCheck" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>

		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">

			<h:column>
				<h:panelGroup>
					<h:outputText value="No Renderer"
						styleClass="HtmlOutputTextBradesco" />
				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:submitCheck id="submitCheck"/&gt;
</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>

		</h:panelGrid>
	</h:column>

	<%-- UISubmitCheckClient *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UISubmitCheckClient" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>

		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">

			<h:column>
				<h:panelGroup>
					<arq:form>
						<arq:validatorScript functionName="validateForm" />
						<h:commandButton value="Validar" >
							<arq:submitCheckClient />
						</h:commandButton>
					</arq:form>


				</h:panelGroup>
			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:form&gt;
	&lt;arq:validatorScript functionName="validateForm" /&gt;
	&lt;h:commandButton value="Validar" action="tela_1"
		actionListener="#{managedBean.cadastrar}"&gt;
		&lt;arq:submitCheckClient /&gt;
	&lt;/h:commandButton&gt;
&lt;/arq:form&gt;

</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>

		</h:panelGrid>
	</h:column>



	<%-- UIClientSideValidation *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIClientSideValidation" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>


	<h:form id="clientValidateRequiredForm"
		onsubmit="return validateForm(this);">
		<h:column>

			<h:panelGrid columns="2" width="100%"
				columnClasses="panelGridColumHeader50,color3">

				<h:column>
					<h:panelGrid columns="1">
						<h:column>
							<h:panelGroup>
								<h:outputText styleClass="HtmlOutputTextBradesco"
									value="#{msgs.pages_arq_exemplos_texto_validacao1}" />
							</h:panelGroup>
						</h:column>

						<h:column>
							<h:panelGroup>
								<h:inputText id="validationRequired"
									styleClass="HtmlInputTextBradesco" value="value">
									<arq:commonsValidator type="required"
										arg="#{msgs.label_campo_1}" server="false" client="true" />
								</h:inputText>
								<f:verbatim>&nbsp;</f:verbatim>
								<h:commandButton value="Validar"
									styleClass="HtmlOutputTextBradesco"
									action="#{supportBean.action}" />
							</h:panelGroup>
						</h:column>

						<h:column>
							<h:panelGroup>
								<h:message for="validationRequired" showSummary="true"
									showDetail="true" styleClass="error" />
							</h:panelGroup>
						</h:column>

					</h:panelGrid>
				</h:column>

				<h:column>
					<h:panelGroup>
						<f:verbatim>
							<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:commonsValidator type="required"
  arg="Campo 1"
  server="false"
  client="true"/&gt;
</font>
</pre>
						</f:verbatim>
					</h:panelGroup>
				</h:column>




				<h:column>
					<h:panelGrid columns="1">
						<h:column>
							<h:panelGroup>
								<h:outputText styleClass="HtmlOutputTextBradesco"
									value="#{msgs.pages_arq_exemplos_texto_validacao2}" />
							</h:panelGroup>
						</h:column>
						<h:column>
							<h:panelGroup>
								<h:inputText id="validationEmail"
									styleClass="HtmlInputTextBradesco">
									<arq:commonsValidator type="email" arg="#{msgs.label_campo_2}"
										server="false" client="true" />
								</h:inputText>
								<f:verbatim>&nbsp;</f:verbatim>
								<h:commandButton value="Validar"
									styleClass="HtmlOutputTextBradesco"
									action="#{supportBean.action}" />
							</h:panelGroup>
						</h:column>
						<h:column>
							<h:panelGroup>
								<h:message for="validationEmail" showSummary="true"
									showDetail="true" styleClass="error" />
							</h:panelGroup>
						</h:column>
					</h:panelGrid>
				</h:column>

				<h:column>
					<h:panelGroup>
						<f:verbatim>
							<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:commonsValidator type="email"
  arg="Campo 2"
  server="false"
  client="true"/&gt;
</font>
</pre>
						</f:verbatim>
					</h:panelGroup>
				</h:column>



				<h:column>
					<h:panelGrid columns="1">
						<h:column>
							<h:panelGroup>
								<h:outputText styleClass="HtmlOutputTextBradesco"
									value="#{msgs.pages_arq_exemplos_texto_validacao3}" />
							</h:panelGroup>
						</h:column>
						<h:column>
							<h:panelGroup>
								<h:inputText id="validateInteger"
									styleClass="HtmlInputTextBradesco">
									<arq:commonsValidator type="integer"
										arg="#{msgs.label_campo_3}" server="false" client="true" />
								</h:inputText>
								<f:verbatim>&nbsp;</f:verbatim>
								<h:commandButton value="Validar"
									styleClass="HtmlOutputTextBradesco"
									action="#{supportBean.action}" />
							</h:panelGroup>
						</h:column>
						<h:column>
							<h:panelGroup>
								<h:message for="validateInteger" showSummary="true"
									showDetail="true" styleClass="error" />
							</h:panelGroup>
						</h:column>
					</h:panelGrid>
				</h:column>

				<h:column>
					<h:panelGroup>
						<f:verbatim>
							<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:commonsValidator type="integer"
  arg="Campo 3"
  server="false"
  client="true"/&gt;
</font>
</pre>
						</f:verbatim>
					</h:panelGroup>
				</h:column>





				<h:column>
					<h:panelGrid columns="1">
						<h:column>
							<h:panelGroup>
								<h:outputText styleClass="HtmlOutputTextBradesco"
									value="#{msgs.pages_arq_exemplos_texto_validacao4}" />
							</h:panelGroup>
						</h:column>
						<h:column>
							<h:panelGroup>
								<h:inputText id="validateDate"
									styleClass="HtmlInputTextBradesco">
									<arq:commonsValidator type="date"
										datePatternStrict="dd/MM/yyyy" arg="#{msgs.label_campo_4}"
										server="false" client="true" />
								</h:inputText>
								<f:verbatim>&nbsp;</f:verbatim>
								<h:commandButton value="Validar"
									styleClass="HtmlOutputTextBradesco"
									action="#{supportBean.action}" />
							</h:panelGroup>
						</h:column>
						<h:column>
							<h:panelGroup>
								<h:message for="validateDate" showSummary="true"
									showDetail="true" styleClass="error" />
							</h:panelGroup>
						</h:column>
					</h:panelGrid>
				</h:column>

				<h:column>
					<h:panelGroup>
						<f:verbatim>
							<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:commonsValidator type="date"
  datePatternStrict="dd/MM/yyyy"
  arg="Campo 4"
  server="false"
  client="true"/&gt;
</font>
</pre>
						</f:verbatim>
					</h:panelGroup>
				</h:column>



				<h:column>
					<h:panelGrid columns="1" styleClass="Texte2">
						<h:column>
							<h:panelGroup>
								<h:outputText styleClass="HtmlOutputTextBradesco"
									value="#{msgs.pages_arq_exemplos_texto_validacao5}" />
							</h:panelGroup>
						</h:column>
						<h:column>
							<h:panelGroup>
								<h:inputText id="validateRegExp1"
									styleClass="HtmlInputTextBradesco">
									<arq:commonsValidator type="mask" arg="#{msgs.label_campo_5}"
										server="false" client="true" mask="[4-6].*" />
								</h:inputText>
								<f:verbatim>&nbsp;</f:verbatim>
								<h:commandButton value="Validar"
									styleClass="HtmlOutputTextBradesco"
									action="#{supportBean.action}" />
							</h:panelGroup>
						</h:column>
						<h:column>
							<h:panelGroup>
								<h:message for="validateRegExp1" showSummary="true"
									showDetail="true" styleClass="error" />
							</h:panelGroup>
						</h:column>
					</h:panelGrid>
				</h:column>


				<h:column>
					<h:panelGroup>
						<f:verbatim>
							<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:commonsValidator type="mask"
  arg="Campo 5"
  server="false"
  client="true"
  mask="[4-6].*" /&gt;
</font>
</pre>
						</f:verbatim>
					</h:panelGroup>
				</h:column>


			</h:panelGrid>
		</h:column>

		<arq:validatorScript functionName="validateForm" />
	</h:form>


	<%-- CPFValidator *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="CPFValidator" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:form id="validateCPFForm">
		<h:column>

			<h:panelGrid columns="2" width="100%"
				columnClasses="panelGridColumHeader50,color3">

				<h:column>
					<h:panelGroup>
						<h:inputText id="validatorCPF" styleClass="HtmlInputTextBradesco"
							maxlength="11">
							<f:validator validatorId="CPFValidator" />
						</h:inputText>
						<h:commandButton value="Validar"
							styleClass="HtmlCommandButtonBradesco"
							action="#{supportBean.action}" />
					</h:panelGroup>
				</h:column>

				<h:column>
					<h:panelGroup>
						<f:verbatim>
							<pre>
<font class="HtmlOutputTextBradesco">
&lt;f:validator validatorId="CPFValidator"/&gt;
</font>
</pre>
						</f:verbatim>
					</h:panelGroup>
				</h:column>
			</h:panelGrid>
		</h:column>

		<h:column>
			<h:panelGroup>
				<cus:brMessageColor for="validatorCPF" id="cpfmensajes" />
			</h:panelGroup>
		</h:column>
	</h:form>

	<%-- CNPJValidator *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="CNPJValidator" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>


	<h:form id="validateCNPJForm">
		<h:column>

			<h:panelGrid columns="2" width="100%"
				columnClasses="panelGridColumHeader50,color3">

				<h:column>
					<h:panelGroup>

						<h:inputText id="validatorCNPJ" styleClass="HtmlInputTextBradesco"
							maxlength="15">
							<f:validator validatorId="CNPJValidator" />
						</h:inputText>
						<h:commandButton value="Validar"
							styleClass="HtmlCommandButtonBradesco"
							action="#{supportBean.action}" />

					</h:panelGroup>
				</h:column>

				<h:column>
					<h:panelGroup>
						<f:verbatim>
							<pre>
<font class="HtmlOutputTextBradesco">
&lt;f:validator validatorId="CNPJValidator"/&gt;
</font>
</pre>
						</f:verbatim>
					</h:panelGroup>
				</h:column>
			</h:panelGrid>
		</h:column>

		<h:column>
			<h:panelGroup>
				<cus:brMessageColor for="validatorCNPJ" id="cnpjmensajes" />
			</h:panelGroup>
		</h:column>
	</h:form>

	<%-- CNPJConverter *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="CNPJConverter" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:form id="converterCNPJForm">
		<h:column>

			<h:panelGrid columns="2" width="100%"
				columnClasses="panelGridColumHeader50,color3">

				<h:column>
					<h:panelGroup>

						<h:inputText id="converterCNPJ" styleClass="HtmlInputTextBradesco">
							<f:converter converterId="CNPJConverter" />
						</h:inputText>
						<h:commandButton value="Validar"
							styleClass="HtmlCommandButtonBradesco"
							action="#{supportBean.action}" />

					</h:panelGroup>
				</h:column>

				<h:column>
					<h:panelGroup>
						<f:verbatim>
							<pre>
<font class="HtmlOutputTextBradesco">
&lt;f:converter converterId="CNPJConverter"/&gt;
</font>
</pre>
						</f:verbatim>
					</h:panelGroup>
				</h:column>

			</h:panelGrid>
		</h:column>

		<h:column>
			<h:panelGroup>
				<cus:brMessageColor for="converterCNPJ" id="cnpjconvmensajes" />
			</h:panelGroup>
		</h:column>
	</h:form>

	<%-- UILeftZeros *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UILeftZeros" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:form>
		<h:column>

			<h:panelGrid columns="2" width="100%"
				columnClasses="panelGridColumHeader50,color3">

				<h:column>
					<h:panelGroup>

						<h:inputText id="campo1" value="" size="10" maxlength="10">
							<arq:leftZeros quantCasas="10" />
						</h:inputText>


					</h:panelGroup>
				</h:column>

				<h:column>
					<h:panelGroup>
						<f:verbatim>
							<pre>
<font class="HtmlOutputTextBradesco">
&lt;h:inputText id="campo1" value="" size="10" maxlength="10"&gt;
&lt;brArq:leftZeros quantCasas="10" /&gt;
&lt;/h:inputText&gt;

</font>
</pre>
						</f:verbatim>
					</h:panelGroup>
				</h:column>

			</h:panelGrid>
		</h:column>


	</h:form>

	<%-- UINextFocus *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UINextFocus" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:form>
		<h:column>

			<h:panelGrid columns="2" width="100%"
				columnClasses="panelGridColumHeader50,color3">

				<h:column>
					<h:panelGroup>

						<h:inputText id="campo1" value="" size="10" maxlength="10">
							<arq:nextFocus forId="campo2" />

						</h:inputText>
						<h:inputText id="campo2" value="" size="10" maxlength="12">
							<arq:nextFocus forId="campo1" />
						</h:inputText>



					</h:panelGroup>
				</h:column>

				<h:column>
					<h:panelGroup>
						<f:verbatim>
							<pre>
<font class="HtmlOutputTextBradesco">
&lt;h:inputText id="campo1" value="" size="10" maxlength="10"&gt;
&lt;brArq:nextFocus forId="campo2" /&gt;
&lt;/h:inputText&gt;;
&lt;h:inputText id="campo2" value="" size="10" maxlength="12"&gt;
&lt;brArq:nextFocus forId="campo1" /&gt;
&lt;/h:inputText&gt;


</font>
</pre>
						</f:verbatim>
					</h:panelGroup>
				</h:column>

			</h:panelGrid>
		</h:column>


	</h:form>



	<%-- UIForm *****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="UIForm" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:form>
		<h:column>

			<h:panelGrid columns="2" width="100%"
				columnClasses="panelGridColumHeader50,color3">

				<h:column>
					<h:panelGroup>
						<arq:form id="form">
							<cus:brCommandButton id="consultar" value="Consultar"
								title="Consultar"
								actionListener="#{exemploBean.tratarCamposEPesquisar}"
								styleClass="button">
								<arq:submitCheckClient />
							</cus:brCommandButton>
						</arq:form>






					</h:panelGroup>
				</h:column>

				<h:column>
					<h:panelGroup>
						<f:verbatim>
							<pre>
<font class="HtmlOutputTextBradesco">
&lt;arq:form id="form"&gt;
	&lt;cus:brCommandButton id="consultar" value="Consultar"
		title="Consultar"
		actionListener="#{exemploBean.tratarCamposEPesquisar}"
		styleClass="button"&gt;
		&lt;arq:submitCheckClient /&gt;
	&lt;/cus:brCommandButton&gt;
&lt;/arq:form&gt;

</font>
</pre>
						</f:verbatim>
					</h:panelGroup>
				</h:column>

			</h:panelGrid>
		</h:column>


	</h:form>

</h:panelGrid>










