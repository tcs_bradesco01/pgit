<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="x"%>

<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="cus"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="arq"%>




<h:panelGrid columns="1" width="100%">

	<h:column>
		<h:panelGrid styleClass="mainPanel" columns="2" width="100%"
			columnClasses="panelGridColumHeader50,panelGridColumHeader50">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="Indicador de Passos 1" />
				</h:panelGroup>
			</h:column>
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="Tiles" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>


	<%-- UIPanel  brPanelGrid  ****************************************************************************--%>
	<h:column>
		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>

	<h:column>

		<h:panelGrid columns="2" width="100%"
			columnClasses="panelGridColumHeader50,color3">
			<h:column>

				<cus:brOutputLinkTitleColor style="align:center;" value="presIndicador_passos_2.jsf">
					<f:verbatim>Próxima Pagina </f:verbatim>
				</cus:brOutputLinkTitleColor>




			</h:column>

			<h:column>
				<h:panelGroup>
					<f:verbatim>
						<pre>
<font class="HtmlOutputTextBradesco">

	&lt;definition name="/presCustom_components_8.tiles"
		extends="custom_components.tiles"&gt;
		&lt;put name="body"
			value="/content/componentsExamples/custom_components_8.jsp" /&gt;
		&lt;put name="tela" value="1017" /&gt;
		&lt;put name="selection" value="path_customcomponents" /&gt;
		&lt;put name="passNumber" value="1" /&gt;
		&lt;putList name="path"&gt;
			&lt;item value="path_indicadorpassos" link="" /&gt;
			&lt;item value="path_indicadorpassos2" link="" /&gt;
			&lt;item value="path_indicadorpassos3" link="" /&gt;
		&lt;/putList&lt;
		&lt;putList name="specificJS"&gt;
			&lt;item value="partialValidations"
				link="/javascript/partialValidations.js" /&gt;
		&lt;/putList&gt;
	&lt;/definition&gt;

</font>
</pre>
					</f:verbatim>
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	</h:column>


</h:panelGrid>










