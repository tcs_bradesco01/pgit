<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detSimulacaoTarifasNegociacaoForm" name="detSimulacaoTarifasNegociacaoForm"  >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.cpfCnpjCliente}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.nomeRazaoSocial}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.grupoEconomico}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_atividade_economica}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.atividadeEconomica}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.segmento}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_subsegmento}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.subSegmento}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
		
	<f:verbatim><br></f:verbatim> 
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="1" width="280px" style="background-color:#F1F1F1">
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="100%">
			<br:brPanelGroup>
				<br:brOutputText style="margin-left:115px"  styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_simulado}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%" >		
		
		 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="130px">
				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
					<br:brPanelGroup> 
					</br:brPanelGroup>
				</br:brPanelGrid>	
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brOutputText style="margin-left:5px" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao_mes}:"/>				
					</br:brPanelGroup>	
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
					<br:brPanelGroup> 
					</br:brPanelGroup>
				</br:brPanelGrid>	
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brOutputText style="margin-left:5px" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_proposta_mes}:"/>
					</br:brPanelGroup>	
					
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
					<br:brPanelGroup> 
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brOutputText style="margin-left:5px" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_resultado}:"/>				
					</br:brPanelGroup>	
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
					<br:brPanelGroup> 
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brOutputText style="margin-left:5px" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_flexibilidade}:"/>					
					</br:brPanelGroup>	
				</br:brPanelGrid>	
			</br:brPanelGrid>	
			
				
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="130px" style="text-align:right">
			
				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
					<br:brPanelGroup> 
					</br:brPanelGroup>
				</br:brPanelGrid>	
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{simulacaoTarifasNegociacaoBean.tarifaPadraoMes}"  converter="decimalBrazillianConverter"/>    
					</br:brPanelGroup>	
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
					<br:brPanelGroup> 
					</br:brPanelGroup>
				</br:brPanelGrid>	
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="BORDER-BOTTOM: 2px solid #CBCBC6;width:100%;text-align:right " >
					<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"  value="#{simulacaoTarifasNegociacaoBean.tarifaPropostaMes}"  converter="decimalBrazillianConverter"/>    
					</br:brPanelGroup>					
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
					<br:brPanelGroup> 
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{simulacaoTarifasNegociacaoBean.resultado}"  converter="decimalBrazillianConverter"/>    
					</br:brPanelGroup>	
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
					<br:brPanelGroup> 
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{simulacaoTarifasNegociacaoBean.flexibilidade}" converter="decimalBrazillianConverter" /> 
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="%" rendered="#{simulacaoTarifasNegociacaoBean.flexibilidade != null}"/>   

					</br:brPanelGroup>	
				</br:brPanelGrid>				
					
			
		</br:brPanelGrid>
		</br:brPanelGrid>
		
		</br:brPanelGrid>
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="1" width="280px" style="background-color:#F1F1F1" >

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="165px">
					<br:brPanelGroup>
						<br:brOutputText style="margin-left:5px" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_receita_floating_cliente}:"/>								
					</br:brPanelGroup>	
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="text-align:right; margin-bottom:6px" width="100px">
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{simulacaoTarifasNegociacaoBean.receitaFloatingCliente}"  converter="decimalBrazillianConverter"/> 
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="(*)" rendered="#{simulacaoTarifasNegociacaoBean.receitaFloatingCliente != null}"/>      
					</br:brPanelGroup>	
				</br:brPanelGrid>	
			</br:brPanelGrid>		
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brOutputText style="margin-left:5px" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_obs_total_receita}"/>
			   </br:brPanelGroup>	
			</br:brPanelGrid>	
				
	
		</br:brPanelGrid>		
		
	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.label_voltar}" action="#{simulacaoTarifasNegociacaoBean.voltarDetalhe}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
