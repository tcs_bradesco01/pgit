<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="simulacaoTarifaNegociacao2Form" name="simulacaoTarifaNegociacao2Form"  >
<h:inputHidden id="hiddenTotal" value="#{simulacaoTarifasNegociacaoBean.total}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.cpfCnpjCliente}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.nomeRazaoSocial}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.grupoEconomico}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_atividade_economica}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.atividadeEconomica}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.segmento}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_subsegmento}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{simulacaoTarifasNegociacaoBean.subSegmento}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
		
	
	
	<f:verbatim><br></f:verbatim> 

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

			<app:scrollableDataTable id="dataTable" value="#{simulacaoTarifasNegociacaoBean.listaGrid}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>						
				    
					<t:selectBooleanCheckbox  id="check" styleClass="HtmlSelectOneRadioBradesco" value="#{result.check}" >
						<f:selectItems value="#{simulacaoTarifasNegociacaoBean.listaControleRadio}"/>		
						<a4j:support event="onclick" reRender="btoSimular,dataTable,hiddenTotal" action="#{simulacaoTarifasNegociacaoBean.habilitarPanelBotoes}"/>		
					</t:selectBooleanCheckbox>
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_tipoServico}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsProdutoServicoOperacao}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_modalidade}"  style="width:250; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsProdutoOperacaoRelacionado}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_operacao}" style="width:250; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsOperacaoProdutoServico}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="140px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_tarifa_individual_padrao}"  style="width:140; text-align:center"/>
				    </f:facet>
   				    <br:brOutputText value="R$ " />
				    <br:brOutputText value="#{result.vlrTarifaContrato}" converter="decimalBrazillianConverter" id="tarifaPadrao" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabRight" width="180px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_tarifa_individual_proposta}" style="width:180; text-align:center"/>
				    </f:facet>
			       <br:brOutputText value="R$ " />
				    <br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{!result.check}" value="#{result.vlrTarifaIndProposta}" id="txtVlrTarifaIndProposta" style="margin-right:5px;TEXT-ALIGN:right;" converter="decimalBrazillianConverter" alt="decimalBr" maxlength="15" size="27" onfocus="loadMasks();" onchange="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" >
				    
				    		<a4j:support event="onblur"  reRender="dataTable" action="#{simulacaoTarifasNegociacaoBean.calcularPercentual}"/>		
				    
				     </br:brInputText>									    
				    
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_tarifa_individual_percent}"  style="width:150; text-align:center"/>
				    </f:facet>
					<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{!result.check}" value="#{result.percentualTarifaIndividual}" id="txtPercentualTarifaIndividual" style="margin-right:5px;TEXT-ALIGN:right;" converter="decimalBrazillianConverter" alt="percSimTarifa" maxlength="9" size="12" onfocus="loadMasks();" onchange="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');">
				    
				    		<a4j:support event="onblur"  reRender="dataTable" action="#{simulacaoTarifasNegociacaoBean.calcularValorProposta}"/>		
				    
				     </br:brInputText>									    
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="100px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_qtde_estimada}" style="width:100; text-align:center" />
				    </f:facet>
					<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{!result.check}" value="#{result.qtdeEstimada}" id="txtQtdeEstimada" style="margin-right:5px;TEXT-ALIGN:right;"  maxlength="6" size="10" onkeypress="onlyNum();" >
				    
				    		<a4j:support event="onblur" reRender="dataTable"  action="#{simulacaoTarifasNegociacaoBean.calcular}"/>		
				    
				     </br:brInputText>									    								    
				</app:scrollableColumn>	

				<app:scrollableColumn styleClass="colTabRight" width="110px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_qtde_dias_floating}" style="width:110; text-align:center" />
				    </f:facet>
					<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{!result.check}" value="#{result.qtdeDiasFloating}" id="txtQtdeDiasFloating" style="margin-right:5px;TEXT-ALIGN:right;"  maxlength="3" size="5" onkeypress="onlyNum();"/>									    
				</app:scrollableColumn>
				
			
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_valor_medio}" style="width:200; text-align:center" />
				    </f:facet>
   			        <br:brOutputText value="R$ " />
         			<br:brOutputText value="#{result.vlrMedioFormatado}" id="txtVlrMedio" />
   				    <h:inputHidden id="hiddenVlrMedio" value="#{result.vlrMedio}" converter="decimalBrazillianConverter" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_valorTotal}" style="width:150; text-align:center" />
				    </f:facet>
   				    <br:brOutputText value="#{result.vlrTotalFormatado}" id="txtValorTotal" />
   				    <h:inputHidden id="hiddenVlrTotal" value="#{result.vlrTotal}" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>		
				
			</app:scrollableDataTable>
			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{consultarPagamentosIndividualBean.pesquisarPagamentoIndividual}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.label_voltar}" action="#{simulacaoTarifasNegociacaoBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btoSimular" disabled="#{!simulacaoTarifasNegociacaoBean.painelBotoes}" styleClass="bto1"  value="#{msgs.label_simular}" action="#{simulacaoTarifasNegociacaoBean.simular}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
