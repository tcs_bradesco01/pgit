<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %> 
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>


<brArq:form id="simulacaoTarifaNeg" name="simulacaoTarifaNeg"  >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup style="text-align:right;width:600px" >					
				<br:brCommandLink id="botaoInicial" style="visibility:hidden" styleClass="bto1" value="teste" action="#{simulacaoTarifasNegociacaoBean.iniciarPagina}" >
					<f:param name="paramCpfCnpjCliente" value="#{param['cpfCnpjCliente']}" />
					<f:param name="paramNomeRazaoSocial" value="#{param['nomeRazaoSocial']}" />
					<f:param name="paramGrupoEconomico" value="#{param['grupoEconomico']}" />
					<f:param name="paramAtividadeEconomica" value="#{param['atividadeEconomica']}" />
					<f:param name="paramSegmento" value="#{param['segmento']}" />
					<f:param name="paramSubSegmento" value="#{param['subSegmento']}" />
					<f:param name="paramNroOcorrencias" value="#{param['numeroOcorrencias']}" />					
					<f:param name="paramCdFluxoNegocio" value="#{param['cdFluxoNegocio']}" />					
					<f:param name="paramCdGrupoInfoFluxo" value="#{param['cdGrupoInfoFluxo']}" />										
					<f:param name="paramCdProdutoOperacaoDeflt" value="#{param['cdProdutoOperacaoDeflt']}" />														
					<f:param name="paramCdOperacaoProdutoServico" value="#{param['cdOperacaoProdutoServico']}" />														
					<f:param name="paramCdTipoCanal" value="#{param['cdTipoCanal']}" />														
					<f:param name="paramCdPessoaJuridicaContrato" value="#{param['cdPessoaJuridicaContrato']}" />																			
					<f:param name="paramCdTipoContratoNegocio" value="#{param['cdTipoContratoNegocio']}" />																			
					<f:param name="paramNrSequenciaContratoNegocio" value="#{param['nrSequenciaContratoNegocio']}" />																			
					<f:param name="paramNrOcorGrupoProposta" value="#{param['nrOcorGrupoProposta']}" />																			
					<f:param name="paramCdProdutoServicoOperacao" value="#{param['cdProdutoServicoOperacao']}" />																			
					<f:param name="paramCdProdutoOperacaoRelacionado" value="#{param['cdProdutoOperacaoRelacionado']}" />																			
					<f:param name="paramNumeroOcorrencias" value="#{param['numeroOcorrencias']}" />																			
					<brArq:submitCheckClient/>
				</br:brCommandLink>
			</br:brPanelGroup>
		</br:brPanelGrid>			
</br:brPanelGrid>
</brArq:form>
