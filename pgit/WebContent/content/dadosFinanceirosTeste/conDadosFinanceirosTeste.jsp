<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="dadosFinanceirosTesteForm" name="dadosFinanceirosTesteForm" >

	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" width="800px">

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<t:selectOneRadio id="selTipoPesquisa" layout="spread" value="#{dadosFinanceirosTesteBean.tipoPesquisaSelecionada}" >
					<f:selectItem itemValue="1" itemLabel="" />
					<f:selectItem itemValue="2" itemLabel="" />
					<a4j:support event="onclick" reRender="cnpjPanel,cpfPanel,btnConsultar" action="#{dadosFinanceirosTesteBean.limparCampos}"/>
		    	</t:selectOneRadio>
	    	</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cnpj}:" />
			</br:brPanelGroup>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup style="margin-top:2px;height:2px">
					<t:radio for=":dadosFinanceirosTesteForm:selTipoPesquisa" index="0" />
				</br:brPanelGroup>

				<br:brPanelGroup style="margin-top:2px;height:2px">
					<br:brPanelGrid id="cnpjPanel" columns="6" cellpadding="0" cellspacing="0" >
	 					<br:brPanelGroup style="margin-left:20px"> 
							<br:brInputText id="txtCnpj" styleClass="HtmlInputTextBradesco" value="#{dadosFinanceirosTesteBean.numBaseCnpj}"
								size="12" maxlength="9" onkeypress="onlyNum();" disabled="#{dadosFinanceirosTesteBean.tipoPesquisaSelecionada != 1}">
								<a4j:support event="onblur" reRender="btnConsultar" action="#{dadosFinanceirosTesteBean.checaCamposPreenchidos}"/>
							</br:brInputText>
						</br:brPanelGroup>
						<br:brPanelGroup style="width:5px" ></br:brPanelGroup>							
						<br:brPanelGroup> 
							<br:brInputText id="txtCnpj2" styleClass="HtmlInputTextBradesco" value="#{dadosFinanceirosTesteBean.numFilialCnpj}"
								size="5" maxlength="4" onkeypress="onlyNum();" disabled="#{dadosFinanceirosTesteBean.tipoPesquisaSelecionada != 1}" >
								<a4j:support event="onblur" reRender="btnConsultar" action="#{dadosFinanceirosTesteBean.checaCamposPreenchidos}"/>
							</br:brInputText>
						</br:brPanelGroup>	
						<br:brPanelGroup style="width:5px" ></br:brPanelGroup>							
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="-" style="margin-right:5px" />
						</br:brPanelGroup> 	
						<br:brPanelGroup> 
							<br:brInputText id="txtCnpj3" styleClass="HtmlInputTextBradesco" value="#{dadosFinanceirosTesteBean.numDigitoCnpj}"
								size="3" maxlength="2" onkeypress="onlyNum();" disabled="#{dadosFinanceirosTesteBean.tipoPesquisaSelecionada != 1}" >
								<a4j:support event="onblur" reRender="btnConsultar" action="#{dadosFinanceirosTesteBean.checaCamposPreenchidos}"/>
							</br:brInputText>
						</br:brPanelGroup>			
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGroup style="margin-top:10px;height:5px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf}:" />
			</br:brPanelGroup>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup style="margin-top:2px;height:2px">
					<t:radio for=":dadosFinanceirosTesteForm:selTipoPesquisa" index="1" />
				</br:brPanelGroup>

				<br:brPanelGroup style="margin-top:2px;height:2px">
					<br:brPanelGrid id="cpfPanel" columns="4" cellpadding="0" cellspacing="0" >
	 					<br:brPanelGroup style="margin-left:20px"> 
							<br:brInputText id="txtCpf" styleClass="HtmlInputTextBradesco" value="#{dadosFinanceirosTesteBean.numBaseCpf}"
								size="12" maxlength="9" onkeypress="onlyNum();" disabled="#{dadosFinanceirosTesteBean.tipoPesquisaSelecionada != 2}" >
								<a4j:support event="onblur" reRender="btnConsultar" action="#{dadosFinanceirosTesteBean.checaCamposPreenchidos}"/>
							</br:brInputText>
						</br:brPanelGroup>
						<br:brPanelGroup style="width:5px" ></br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="-" style="margin-right:5px" />
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brInputText id="txtCpf2" styleClass="HtmlInputTextBradesco" value="#{dadosFinanceirosTesteBean.numDigitoCpf}"
								size="3" maxlength="2" onkeypress="onlyNum();" disabled="#{dadosFinanceirosTesteBean.tipoPesquisaSelecionada != 2}" >
								<a4j:support event="onblur" reRender="btnConsultar" action="#{dadosFinanceirosTesteBean.checaCamposPreenchidos}"/>
							</br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup style="margin-top:10px;height:5px">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_dtAnoMes}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
				<br:brPanelGroup style="margin-left:10px;margin-top:2px;height:2px">
					<br:brInputText id="dtAno" styleClass="HtmlInputTextBradesco" value="#{dadosFinanceirosTesteBean.dtAno}" 
						size="6" maxlength="4" onkeypress="onlyNum();" />
				</br:brPanelGroup>

				<br:brPanelGroup style="margin-left:5px;margin-top:2px;height:2px">
					<br:brInputText id="dtMes" styleClass="HtmlInputTextBradesco" value="#{dadosFinanceirosTesteBean.dtMes}" 
						size="4" maxlength="2" onkeypress="onlyNum();" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim> <hr class="lin"> </f:verbatim>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brCommandButton id="btnLimparCampos" style="margin-left:550px;" styleClass="bto1" value="#{msgs.dadosFinanceiros_btn_limpar_campos}" action="#{dadosFinanceirosTesteBean.limparCampos}">
						<brArq:submitCheckClient />
					</br:brCommandButton>

					<br:brCommandButton id="btnConsultar" style="margin-left:5px" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{dadosFinanceirosTesteBean.consultar}" disabled="#{dadosFinanceirosTesteBean.habilitaBtnConsulta}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>

	    </br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_possuiPagamentoFornecedores}?" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-top:5px;height:5px">
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_quantidade}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_valor}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_creditoConta}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeCredConta}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorCredConta}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_doc}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeDoc}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorDoc}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_ted}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeTed}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorTed}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_titulosOutrosBancos}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeTitOutroBc}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorTitOutroBc}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_ordemPagamento}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.ordemPgto}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorOrdemPgto}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_debitoConta}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeDebConta}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorDebConta}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_titulosBradesco}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeTitBradesco}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorTitBradesco}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_depositoIdentificado}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeDeposIdent}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorDeposIdent}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_ordemCredito}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeOrdemCred}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorOrdemCred}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_resultado}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdePgtoFornecedor}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorPgtoFornecedor}" />
			</br:brPanelGroup>

		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_possuiPagamentoEletronicoTributos}?" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-top:5px;height:5px">
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px;margin-left:14px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_quantidade}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_valor}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_darf}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px;margin-left:14px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeDarf}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorDarf}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_gare}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px;margin-left:14px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeGare}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorGare}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_gps}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px;margin-left:14px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeGps}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorGps}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_codigoBarras}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px;margin-left:14px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeCodBarra}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorCodBarra}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_debitoVeiculos}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px;margin-left:14px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeDebVeiculo}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorDebVeiculo}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_resultado}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px;margin-left:14px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdePgtoEletronico}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorPgtoEletronico}" />
			</br:brPanelGroup>

		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_possuiCreditoFolhaPagamento}?" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="margin-top:5px;height:5px">
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_quantidade}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_valor}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_creditoConta}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeCredContaFolhaPgto}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorCredContaFolhaPgto}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_doc}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeDocFolhaPgto}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorDocFolhaPgto}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_ted}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeTedFolhaPgto}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorTedFolhaPgto}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_titulosOutrosBancos}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeTitOutroBc}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorTitOutroBc}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_ordemPagamento}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeOrdemPgtoFolhaPgto}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorOrdemPgtoFolhaPgto}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.dadosFinanceiros_resultado}:" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.qtdeFolhaPgto}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-top:5px;height:5px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{dadosFinanceirosTesteBean.dadosFinanceirosTesteSaida.valorFolhaPgto}" />
			</br:brPanelGroup>

		</br:brPanelGrid>

	</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />

</brArq:form>