<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="b" %>

<b:brPanelGrid columns="1" align="center" width="100%" style="height:170;text-align:center">
	<b:brPanelGroup>
		<b:brOutputText value="#{msgs.home2_txt}" />
	</b:brPanelGroup>
	<b:brPanelGroup>
		<b:brGraphicImage url="/images/fio_home_demonstrativos_pontilhado.gif"/>
	</b:brPanelGroup>
	<b:brPanelGroup>
		<b:brOutputText value="#{msgs.home2_description1_txt}" />
	</b:brPanelGroup>
	<b:brPanelGroup>
		<b:brOutputText value="#{msgs.home2_description2_txt}" />
	</b:brPanelGroup>
	<b:brPanelGroup>
		<b:brOutputText value="#{msgs.home2_description3_txt}" />
	</b:brPanelGroup>
</b:brPanelGrid>