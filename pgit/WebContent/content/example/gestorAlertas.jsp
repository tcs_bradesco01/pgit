<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %> 
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="brHtml" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 

<h:form>

<brHtml:brPanelGrid columns="1" align="center" width="100%" style="height:170">
<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="Gestor de Alertas" />
					
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	<brHtml:brPanelGroup>
		<brHtml:brOutputText value="#{msgs.pages_gestor_alertas_lanzamiento1}" />
		<brHtml:brCommandButton id="emailAlert" value="#{msgs.pages_gestor_alertas_button1}" action="nav_gestor_alertas">
			<f:actionListener type="br.com.bradesco.web.pgit.example.view.actionlisteners.AlertasActionListener" />
		</brHtml:brCommandButton>
	</brHtml:brPanelGroup>
	<brHtml:brPanelGroup>
		<brHtml:brOutputText value="#{msgs.pages_gestor_alertas_lanzamiento2}" />
		<brHtml:brCommandButton id="snmpAlert" value="#{msgs.pages_gestor_alertas_button2}" 
			action="nav_gestor_alertas">
			<f:actionListener type="br.com.bradesco.web.pgit.example.view.actionlisteners.AlertasActionListener" />
		</brHtml:brCommandButton>
	</brHtml:brPanelGroup>
	<brHtml:brPanelGroup>
		<brHtml:brOutputText value="#{msgs.pages_gestor_alertas_lanzamiento3}" />
		<brHtml:brCommandButton id="exceptionAlert" value="#{msgs.pages_gestor_alertas_button3}" 
			action="nav_gestor_alertas">
			<f:actionListener type="br.com.bradesco.web.pgit.example.view.actionlisteners.AlertasActionListener" />
		</brHtml:brCommandButton>
	</brHtml:brPanelGroup>
</brHtml:brPanelGrid>


</h:form>