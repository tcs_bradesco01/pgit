<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %> 
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="brHtml" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 

<h:form>

<brHtml:brPanelGrid columns="1" align="center" width="100%" style="height:170">
	<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="Lan�amento de Exce��es" />
					
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
</brHtml:brPanelGrid>
<brHtml:brPanelGrid>
	<brHtml:brPanelGroup>
		<brHtml:brSelectOneRadio id="options_selectOneRadio" value="#{exceptionMB.selectedOption}"
			layout="pageDirection">
			<f:selectItem itemLabel="#{msgs.pages_lanzamiento_excepciones1}" itemValue="option1"/>
			<f:selectItem itemLabel="#{msgs.pages_lanzamiento_excepciones2}" itemValue="option2"/>					
			<f:selectItem itemLabel="#{msgs.pages_lanzamiento_excepciones3}" itemValue="option3"/>
			<f:selectItem itemLabel="#{msgs.pages_lanzamiento_excepciones4}" itemValue="option4"/>													   
		</brHtml:brSelectOneRadio>
	</brHtml:brPanelGroup>
	
	<brHtml:brPanelGroup>
		<brHtml:brCommandButton action="nav_lanz_excepciones" value="#{msgs.pages_lanzamiento_excepciones_btn}" 
			actionListener="#{exceptionMB.throwException}" />
	</brHtml:brPanelGroup>
</brHtml:brPanelGrid>


</h:form>