<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="authz" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %> 


<tiles:useAttribute id="title" name="title" classname="java.lang.String" scope="request" ignore="true"/>

<tiles:useAttribute id="description" name="description" classname="java.lang.String" scope="request" ignore="true"/>
<tiles:useAttribute id="author" name="author" classname="java.lang.String" scope="request" ignore="true"/>
<tiles:useAttribute id="copyright" name="copyright" classname="java.lang.String" scope="request" ignore="true"/>
<tiles:useAttribute id="keywords" name="keywords" classname="java.lang.String" scope="request" ignore="true"/>

<tiles:useAttribute name="css" ignore="true"/>
<tiles:useAttribute name="especificCSS" ignore="true"/>
<tiles:useAttribute name="js" ignore="true"/>
<tiles:useAttribute name="especificJS" ignore="true"/>

<html>

<f:view>
<f:loadBundle basename="#{facesContext.application.messageBundle}" var="msgs" />


<head>
  <meta http-equiv="Pragma" content="no-cache">	
  <meta http-equiv="Content-Type" content="text/html;CHARSET=iso-8859-1" />
  <meta http-equiv="Page-Exit" content="progid:DXImageTransform.Microsoft.Fade(Duration=0.2,overlap=1)">
  
  <meta name="description" content='<h:outputText value="#{msgs[description]}"/>' />
  <meta name="author" content='<h:outputText value="#{msgs[author]}"/>' />
  <meta name="copyright" content='<h:outputText value="#{msgs[copyright]}"/>' />
  <meta name="keywords" content='<h:outputText value="#{msgs[keywords]}"/>' />
  
  <title><h:outputText value="#{msgs[title]}"/></title>
  <%-- br:brOutputText no puede ser usado, porque encapsula el texto entre <span></span> tags, no soportados por 
  		el tag html <title> --%>
  		
  	<%--  	#################################################################################
  			InclusiÃ³n de las StyleSheets 
  			#################################################################################
  	--%>
  	<c:if test="${css!=null}">
		<c:forEach items="${css}" var="item">
			<c:if test="${item.tooltip==null}">
				<link rel="stylesheet" type="text/css" href="<c:url value="${item.link}"/>">
			</c:if>
			<c:if test="${item.tooltip=='print'}">
				<link rel="stylesheet" type="text/css" media="print" href="<c:url value="${item.link}"/>">
			</c:if>
		</c:forEach>
	</c:if>
	<%-- Inclusion de CSS especifico de la pagina o grupo de paginas actual 
	(definido en su tiles-definition) --%>
	<c:if test="${especificCSS!=null}">
		<c:forEach items="${especificCSS}" var="item">
			<c:if test="${item.tooltip==null}">
				<link rel="stylesheet" type="text/css" href="<c:url value="${item.link}"/>">
			</c:if>
			<c:if test="${item.tooltip=='print'}">
				<link rel="stylesheet" type="text/css" media="print" href="<c:url value="${item.link}"/>">
			</c:if>
		</c:forEach>
	</c:if>
	
	<%--  	#################################################################################
  			InclusiÃ³n de los MÃ³dulos Javascript
  			#################################################################################
  	--%>
	<c:if test="${js!=null}">
		<c:forEach items="${js}" var="item">
			<script language="JavaScript" src="<c:url value="${item.link}"/>">
			</script>
		</c:forEach>
	</c:if>
	<%-- Inclusion de Javascript especifico de la pagina o grupo de paginas actual 
	(definido en su tiles-definition) --%>
	<c:if test="${especificJS!=null}">
		<c:forEach items="${especificJS}" var="item">
			<script language="JavaScript" src="<c:url value="${item.link}"/>">
			</script>
		</c:forEach>
	</c:if>
</head>


<body leftmargin="0" topmargin="0" onkeypress="  if(window.event){ fireDefault(event);}else{ return fireDefault(event);}">

<t:div id="div_brArq_form_wait" style="display:none;">
      <t:div id="wait"
                  style="{width: 100%; height: 100%; z-index: 10; position: absolute; cursor: wait; opacity:0.1; -moz-opacity: 0.1; filter: alpha(opacity=10); background-color: #ffffff}">
                  <t:panelGroup style="{width: 100%; height: 100%; z-index: 10}">
                        <br:brOutputText value=" " />
                  </t:panelGroup>
      </t:div>
</t:div>


<br:brPanelGrid id="mainLayout" border="0" columns="1" cellpadding="0" cellspacing="0"
	styleClass="tmainpage"  headerClass="theaderPanel" footerClass="tfooterPanel" 
	columnClasses="ccontext">

	<f:facet name="header">
		<f:subview id="header">
			<tiles:insert attribute="header" flush="false"/>
		</f:subview>
	</f:facet>
	<br:brPanelGroup id="pageContext">
		<br:brPanelGrid id="innerLayout" border="0" columns="1" cellpadding="0" cellspacing="0"
				styleClass="tmainpage"  headerClass="tsubHeaderPanel" footerClass="thelpPanel"
				columnClasses="ccontext">
			<f:facet name="header">
				<f:subview id="subHeader">
					<tiles:insert attribute="subHeader" flush="false" ignore="true"/>
				</f:subview>
			</f:facet>

		<br:brPanelGroup id="bodyContent">
				<br:brPanelGrid id="contentLayout" border="0" columns="1" cellpadding="0" cellspacing="0"
						styleClass="tmainpage"  headerClass="tmainMenuPanel">
					<f:facet name="header">
						<f:subview id="mainMenu">
							<tiles:useAttribute name="menuSelected" id="menuSelected" classname="java.lang.String" scope="request" ignore="true"/>
							<tiles:useAttribute id="menuOptions" name="menuOptions" classname="java.util.List" scope="request" ignore="true"/>
							<tiles:insert attribute="mainMenu" flush="false" ignore="true"/>
						</f:subview>
					</f:facet>
					<br:brPanelGroup id="pageContent">
						<tiles:useAttribute name="isHome" ignore="true"/>
						
						<c:if test="${isHome!=null}"> 
							<br:brPanelGrid id="pageContentLayout" border="0" columns="1" cellpadding="0" cellspacing="0"
								styleClass="mainBody"  columnClasses="cbodyContext">
								<br:brPanelGroup id="pageBody">
									<tiles:insert attribute="body" flush="false" ignore="true"/>
								</br:brPanelGroup>
							</br:brPanelGrid>
						</c:if>
						
						<c:if test="${isHome==null}"> 
							<br:brPanelGrid id="pageContentLayout" border="0" columns="1" cellpadding="0" cellspacing="0"
								styleClass="mainBody" headerClass="pathContext" columnClasses="cbodyContext textoAcima">
								<f:facet name="header">
									<f:subview id="pathView">
										<tiles:useAttribute id="selection" name="selection" scope="request" ignore="true" classname="java.lang.String" />
										<tiles:useAttribute id="path" name="path" scope="request" ignore="true" classname="java.util.List" />
										<tiles:useAttribute id="passNumber" name="passNumber" scope="request" ignore="true" classname="java.lang.String" />
										<br:brPanelGrid cellpadding="0" cellspacing="0" border="0" align="left">
											<br:brOutputText value="#{msgs[selection]}" styleClass="selectionMenu" escape="false"/>
										    <br:brPanelGroup>
										    	<t:dataList value="#{path}" var="item" rowIndexVar="index" rowCountVar="count">
												    <br:brPanelGroup style="white-space: nowrap;">
										    			<br:brOutputText value="#{index+1}" styleClass="passSelected" rendered="#{index+1==passNumber}"/>
										    			<br:brOutputText value="#{index+1}" styleClass="passNoSelected" rendered="#{index+1!=passNumber}"/>
											        	<br:brOutputText styleClass="textDefault" value="-" />
											        	<br:brOutputText styleClass="textPassSelected" value="#{msgs[item.value]}" rendered="#{index+1==passNumber}"/>
											        	<br:brOutputText styleClass="textPassNoSelected" value="#{msgs[item.value]}" rendered="#{index+1!=passNumber}"/>
													</br:brPanelGroup>
												</t:dataList>
										    </br:brPanelGroup>
										</br:brPanelGrid>
									</f:subview>
								</f:facet>
								<br:brPanelGroup id="pageBody">
									<tiles:insert attribute="body" flush="false" ignore="true"/>
								</br:brPanelGroup>
							</br:brPanelGrid>
						</c:if>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGroup>
	<f:facet name="footer">
		<f:subview id="footer">
			<tiles:insert attribute="footer" flush="false"/>
		</f:subview>
	</f:facet>
</br:brPanelGrid>
<script type="text/javascript">

function limparGrid(gridComRegistros) {
      if(gridComRegistros){
            document.forms[0].submit();
      }     
}

function fireDefault(e){
    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {  
     
     
      var elementType = e.target ? e.target.type : e.srcElement.type;
      var keyCode = e.keyCode;
     
      if(elementType != 'textarea' && keyCode == 13){
        if(window.event){//IE
          event.returnValue=false;
	      event.cancel = true; 
        } 
        var $btn = $("input[id*='default']")
		$btn.click();
        $btn.mouseout();
      }    
       return false;  
    } else {  
       return true;  
    }  
}

</script>


</body>
</f:view>
</html>
			
