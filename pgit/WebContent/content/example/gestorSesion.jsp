<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="b" %>

<%--<b:brOutputText value="#{tilesBacking.testProperty}" />--%>

<b:brPanelGrid columns="1" align="center" width="100%" style="height:170">
	<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="Gestor de Sess�o" />
					
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
	<b:brPanelGroup>
		<b:brOutputText value="Sesion Total Time: #{beanTest2.totalSesionTime}" />
	</b:brPanelGroup>
	<b:brPanelGroup>
		<b:brOutputText value="Current Sesion Creation Timestamp: #{beanTest2.sessionCreationTimestamp}" />
	</b:brPanelGroup>
	<b:brPanelGroup>
		<b:brOutputText value="Current Sesion Timestamp: #{beanTest2.currentSessionTimestamp}" />
	</b:brPanelGroup>
	<b:brPanelGroup>
		<b:brOutputText value="Is valid TTL?: #{beanTest2.isValidTime}" />
	</b:brPanelGroup>
</b:brPanelGrid>