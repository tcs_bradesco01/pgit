<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="b"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<h:form onsubmit="return validateForm(this);">
	<h:panelGrid columns="1" width="100%">

		<h:column>
			<h:panelGrid styleClass="mainPanel" columns="2" width="100%"
				columnClasses="panelGridColumHeader50,panelGridColumHeader50">
				<h:column>
					<h:panelGroup>
						<h:outputText styleClass="HtmlOutputTextTitleBradesco"
							value="#{msgs.page1_txt}" />
					</h:panelGroup>
				</h:column>
				<h:column>

				</h:column>
			</h:panelGrid>
		</h:column>


		<%-- UIGraphic ****************************************************************************************--%>
		<%-- **************************************************************************************************--%>
		<h:column>
			<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
				<h:column>
					<h:panelGroup>
						<h:outputText styleClass="HtmlOutputTextTitleBradesco"
							value="#{msgs.pages_gestor_objetos_texto1}" />
						<brArq:objectManager param="test_object" scope="session" />
					</h:panelGroup>
				</h:column>
			</h:panelGrid>
		</h:column>

		<h:column>

			<h:panelGrid columns="2" width="100%"
				columnClasses="panelGridColumHeader60">
				<h:column>
					<h:panelGroup>
						<b:brOutputText value="#{msgs.pages_gestor_objetos_texto2}" />
						<b:brInputText id="sessionInputText"
							value="#{beanTest1.objectValue}" size="40">
							<brArq:commonsValidator type="required" server="true" />
							<brArq:commonsValidator type="minlength" minlength="3"
								server="true" />
							<brArq:commonsValidator type="maxlength" maxlength="6"
								server="true" />
						</b:brInputText>
						<b:brMessage for="sessionInputText" />
						<b:brCommandButton value="Enviar" action="nav_gestor_objetos" />
					</h:panelGroup>
				</h:column>

			</h:panelGrid>
		</h:column>

	</h:panelGrid>

	<b:brPanelGrid columns="1" align="center" width="100%"
		style="height:170">

		<h:panelGrid styleClass="subMainPanel" columns="1" width="100%">
			<h:column>
				<h:panelGroup>
					<h:outputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.pages_gestor_objetos_texto3}" />
					<brArq:objectManager param="test_app_object" scope="application" />
				</h:panelGroup>
			</h:column>
		</h:panelGrid>
		<b:brPanelGroup>
			<b:brOutputText value="#{msgs.pages_gestor_objetos_texto4}" />
			<b:brInputText value="#{beanTest1.appObjectValue}" size="40" />
			<b:brCommandButton value="Enviar" action="nav_gestor_objetos" />
		</b:brPanelGroup>


	</b:brPanelGrid>

	<brArq:submitCheck />

	<brArq:validatorScript functionName="validateForm" />

</h:form>
