<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %> 
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="brApp" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="brHtml" %>

<h:form>

<brHtml:brPanelGrid columns="1" align="center" width="100%" style="height:170">
	<brHtml:brPanelGrid  styleClass="subMainPanel" columns="1" width="100%">
		<brHtml:brColumn>
			<brHtml:brPanelGroup>
				<brHtml:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.pages_criptografia_titulo}" />
			</brHtml:brPanelGroup>
		</brHtml:brColumn>
	</brHtml:brPanelGrid>

	<f:verbatim>
		<br/>
	</f:verbatim>

	<brHtml:brOutputTextBold value="#{msgs.pages_criptografia_informacoes}" />
	<brHtml:brPanelGrid columns="2">
		<brHtml:brOutputTextBold value="#{msgs.pages_criptografia_name}" />
		<brHtml:brOutputText value="#{exemploCriptografiaBean.providerName}" />
		
		<brHtml:brOutputTextBold value="#{msgs.pages_criptografia_version}" />
		<brHtml:brOutputText value="#{exemploCriptografiaBean.providerVersion}" />		

		<brHtml:brOutputTextBold value="#{msgs.pages_criptografia_info}" />
		<brHtml:brOutputText value="#{exemploCriptografiaBean.providerInfo}" />		

		<brHtml:brOutputTextBold value="#{msgs.pages_criptografia_algorithm}" />
		<brHtml:brOutputText value="#{exemploCriptografiaBean.algorithmInUse}" />		
	</brHtml:brPanelGrid>	

	<f:verbatim>
		<br/>
	</f:verbatim>
	
	<brHtml:brOutputTextBold value="#{msgs.pages_criptografia_explicao_criptografar}" />
	<brHtml:brPanelGrid columns="2">
		<brHtml:brOutputText value="#{msgs.pages_criptografia_textoPuro}" />
		<brHtml:brInputText value="#{exemploCriptografiaBean.criptografarTextoPuro}" size="120" />
		
		<brHtml:brOutputText value="#{msgs.pages_criptografia_textoCriptografado}" />
		<brHtml:brOutputText value="#{exemploCriptografiaBean.criptografarTextoCriptografado}" />		
	</brHtml:brPanelGrid>	
	<brHtml:brCommandButton value="#{msgs.pages_criptografia_botao_criptografar}" actionListener="#{exemploCriptografiaBean.criptografar}" />

	<f:verbatim>
		<br/>
	</f:verbatim>

	<brHtml:brOutputTextBold value="#{msgs.pages_criptografia_explicao_descriptografar}" />
	<brHtml:brPanelGrid columns="2">
		<brHtml:brOutputText value="#{msgs.pages_criptografia_textoCriptografado}" />
		<brHtml:brInputText value="#{exemploCriptografiaBean.descriptografarTextoCriptografado}" size="120" />		
	
		<brHtml:brOutputText value="#{msgs.pages_criptografia_textoPuro}" />
		<brHtml:brOutputText value="#{exemploCriptografiaBean.descriptografarTextoPuro}" />
	</brHtml:brPanelGrid>	
	<brHtml:brCommandButton value="#{msgs.pages_criptografia_botao_descriptografar}" actionListener="#{exemploCriptografiaBean.descriptografar}" />
	
</brHtml:brPanelGrid>

</h:form>