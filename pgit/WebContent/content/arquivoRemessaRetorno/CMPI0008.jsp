
<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="pesqConsultarLotesRetornoForm" name="pesqConsultarLotesRetornoForm">
<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0" border="0" >	
	<h:inputHidden id="hiddenRadio" value="#{arquivoRetornoBean.hdnCodListaLote}"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	 <br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.cmpi0008_cadastro}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_cpfcnpj}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.cpf}"/>	
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_nomerazaosocial}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.nomeRazao}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_servico}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.produto}"/>	
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_perfil}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.perfil}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="10" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_banco}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.banco}"/>	
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_agencia}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.agencia}"/>	
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_razao}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.razao}"/>	
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_conta}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.conta}"/>	
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_codigodolancamento}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.codigoLancamento}"/>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_canaldetransmissao}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.canalTransmissao}"/>	
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1" width="750">
		<br:brPanelGroup>
			<f:verbatim><hr class="lin"> </f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.cmpi0008_retorno}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="8" cellpadding="0" cellspacing="0" width="750px" border="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_sequencia}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.sequenciaRetorno}"/>	
		</br:brPanelGroup>					
		<br:brPanelGroup style="width: 122px;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_datahorageracao}:"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="width: 148px; margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.dataHoraGeracao}"/>	
		</br:brPanelGroup>		
		<br:brPanelGroup style="width: 87px;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_tipoderetorno}:"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="width: 204px; margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.tipoRetorno}" /> 
		</br:brPanelGroup>		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="6" cellpadding="0" cellspacing="0"  >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_situacao}:"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.situacaoProcessamentoRetorno}" /> 
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_layout}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.layout}"/>	
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_ambientedeprocessamento}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.ambienteProcessamento}" /> 
		</br:brPanelGroup>
		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_quantidadederegistros}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.qtdRegistrosRetorno}"/>	
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_valortotal}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.valorTotalRetorno}"/>	
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<f:verbatim><br></f:verbatim>		

	<br:brPanelGrid  columns="1" width="100%"  cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
		<t:dataTable id="dataTable" value="#{arquivoRetornoBean.listaLote}" var="result" rows="10" 
			cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
			columnClasses="alinhamento_centro, alinhamento_direita, alinhamento_direita, alinhamento_esquerda, alinhamento_direita, alinhamento_direita, alinhamento_esquerda, alinhamento_esquerda"
			headerClass="tabela_celula_destaque_acentuado" width="750x">
		       <t:column   width="30px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="" />
			    </f:facet>			    
				<t:selectOneRadio  onclick="javascript:habilitarBotaoEstatisticasLoteRetorno(document.forms[1], this);" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
					<f:selectItems value="#{arquivoRetornoBean.listaControleLote}"/>
		    	</t:selectOneRadio>
		    	<t:radio for="sor" index="#{result.sequencia}" />
			  </t:column>
			  <t:column  width="96px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header" >
			      <br:brOutputText value="#{msgs.cmpi0008_seqdelote}"  />
			    </f:facet>
			    <br:brOutputText value="#{result.seqLote}"   />
			  </t:column>
			  <t:column width="190px"  style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.cmpi0008_bancoagenciacontadebito}" />
			    </f:facet>
			    <br:brOutputText value="#{result.bcoAgeCtaDebito}" />
			  </t:column>			 
			   <t:column  width="210px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.cmpi0008_modalidadedepagamento}" />
			    </f:facet>
			    <br:brOutputText value="#{result.modalidadePagto}" />
			  </t:column>	
			   <t:column  width="100px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.cmpi0008_qtderegistros}" />
			    </f:facet>
			    <br:brOutputText value="#{result.qtdeRegistros}" />
			  </t:column>	
			    <t:column width="136px"  style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.cmpi0008_valortotal_grid}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.valorRegistros}" />
			  </t:column>	
			</t:dataTable>			
		</br:brPanelGroup>
	</br:brPanelGrid>		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" rendered="#{arquivoRetornoBean.listaLote!= null && arquivoRetornoBean.mostraBotoes0008}">	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{arquivoRetornoBean.pesquisarLostes}" >
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	
	<f:verbatim><br></f:verbatim>		
	

	<br:brPanelGrid columns="2" width="750" border="0" style="text-align:right" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>		
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brCommandButton id="btnEstatisticasLote"   style="margin-right:5px" styleClass="HtmlCommandButtonBradesco" value="#{msgs.cmpi0008_botaoestatisticaslote}" action="#{arquivoRetornoBean.consultarEstatisticasLote}" disabled="true" >				
				<brArq:submitCheckClient/>
			</br:brCommandButton> 
			<br:brCommandButton styleClass="HtmlCommandButtonBradesco" value="#{msgs.cmpi0008_botaovoltar}"  action="#{arquivoRetornoBean.voltarRemessa}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>

</brArq:form>
