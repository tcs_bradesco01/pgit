
<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="pesqArqRetornoForm" name="pesqArqRetornoForm" >
<h:inputHidden id="hiddenRadio" value="#{arquivoRetornoBean.codListaRetornoRadio}"/>
<h:inputHidden id="hiddenRadioFiltro" value="#{arquivoRetornoBean.filtro}"/>
<h:inputHidden id="hiddenRadioValidaCPF" value="#{arquivoRetornoBean.validaCpf}"/>
<h:inputHidden id="hiddenRadioValidaCnpj" value="#{arquivoRetornoBean.validaCnpj}"/>
<h:inputHidden id="hiddenRadioFiltroArgumento" value="#{arquivoRetornoBean.cmpi0000Bean.hiddenRadioFiltroArgumento}"/>
<h:inputHidden id="hiddenCPFCNPJ" value="#{arquivoRetornoBean.cmpi0000Bean.cpf}"/>
<h:inputHidden id="hiddenObrigatoriedade" value="#{arquivoRetornoBean.obrigatoriedade}"/>
<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0" border="0">
	
	<br:brPanelGrid styleClass="mainPanel" style="margin-top:9px" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
	    		<t:selectOneRadio value="#{arquivoRetornoBean.itemSelecionado}" onclick="javascript:habilitarCamposFiltroIdenticacaoCliente(document.forms[1], this);" id="radioFiltro" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<f:selectItem itemValue="3" itemLabel="" />
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

     <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="0" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" onkeyup="proximoCampo(9,'pesqArqRetornoForm','pesqArqRetornoForm:txtCnpj1','pesqArqRetornoForm:txtCnpj2');" onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtCnpj1',numeros);"  readonly="false" value="#{arquivoRetornoBean.cmpi0000Bean.cnpj1}" size="12" maxlength="9" id="txtCnpj1">
				    </br:brInputText>	
				</br:brPanelGroup>									    
  	
			    <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
	
			    <br:brPanelGroup>	
				    <br:brInputText styleClass="HtmlInputTextBradesco" onkeyup="proximoCampo(4,'pesqArqRetornoForm','pesqArqRetornoForm:txtCnpj2','pesqArqRetornoForm:txtCnpj3');"  onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtCnpj2',numeros);" readonly="false" value="#{arquivoRetornoBean.cmpi0000Bean.cnpj2}" size="6" maxlength="4" id="txtCnpj2">
				    </br:brInputText>	
   				</br:brPanelGroup>					

    		    <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGroup>						
				    <br:brInputText styleClass="HtmlInputTextBradesco"  onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtCnpj3',numeros);" readonly="false" value="#{arquivoRetornoBean.cmpi0000Bean.cnpj3}" size="4" maxlength="2" id="txtCnpj3">
				    </br:brInputText>					    
				</br:brPanelGroup>		
							
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="1" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brInputText styleClass="HtmlInputTextBradesco"  value="#{arquivoRetornoBean.cmpi0000Bean.cpf1}" size="12" maxlength="9" onkeyup="proximoCampo(9,'pesqArqRetornoForm','pesqArqRetornoForm:txtCpf1','pesqArqRetornoForm:txtCpf2');" onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtCpf1',numeros);" id="txtCpf1" readonly="true" />
				</br:brPanelGroup>								
				 <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>		
				<br:brPanelGroup>
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{arquivoRetornoBean.cmpi0000Bean.cpf2}" size="4"   maxlength="2" onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtCpf2',numeros);" id="txtCpf2" readonly="true" />
				</br:brPanelGroup>				
				
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="2" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nome_razao_social}:"/>
				</br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-right:20px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>				
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco"  readonly="true" value="#{arquivoRetornoBean.cmpi0000Bean.nomeRazaoIdentificaCliente}" size="50" maxlength="70" id="txtNomeRazao" style="margin-right:20px"/>  
				</br:brPanelGroup>					
			</br:brPanelGrid>			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				</br:brPanelGroup>	
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_nascimento_fundacao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>				
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		    
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>		    					

				<br:brPanelGroup>
					 <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" onkeyup="proximoCampo(2,'pesqArqRetornoForm','pesqArqRetornoForm:txtDiaNasc','pesqArqRetornoForm:txtMesNasc');" readonly="true" value="#{arquivoRetornoBean.cmpi0000Bean.diaNasc}"  size="4" maxlength="2"  onblur="validarData('pesqArqRetornoForm','pesqArqRetornoForm:txtDiaNasc', 'dia');" onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtDiaNasc',numeros);"  id="txtDiaNasc">
				    </br:brInputText>	
				</br:brPanelGroup>									    
  	
			    <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<f:verbatim>/</f:verbatim>
					</br:brPanelGroup>
				</br:brPanelGrid>	
	
			    <br:brPanelGroup>	
				    <br:brInputText styleClass="HtmlInputTextBradesco" onkeyup="proximoCampo(2,'pesqArqRetornoForm','pesqArqRetornoForm:txtMesNasc','pesqArqRetornoForm:txtAnosNasc');" readonly="true" value="#{arquivoRetornoBean.cmpi0000Bean.mesNasc}" size="4" maxlength="2"  onblur="validarData('pesqArqRetornoForm','pesqArqRetornoForm:txtMesNasc', 'mes');" onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtMesNasc',numeros);" id="txtMesNasc">
				    </br:brInputText>	
   				</br:brPanelGroup>					

    		    <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<f:verbatim>/</f:verbatim>
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGroup>						
				    <br:brInputText styleClass="HtmlInputTextBradesco" readonly="true" value="#{arquivoRetornoBean.cmpi0000Bean.anoNasc}" size="8" maxlength="4"  onblur="validarData('pesqArqRetornoForm','pesqArqRetornoForm:txtAnosNasc', 'ano');" onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtAnosNasc',numeros);" id="txtAnosNasc">
				    </br:brInputText>	
				</br:brPanelGroup>		
							
			</br:brPanelGrid>
				</br:brPanelGroup>					
			</br:brPanelGrid>			
			
		</br:brPanelGroup>		
		
	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
  <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="3" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0000_label_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" readonly="true" onkeyup="proximoCampo(3,'pesqArqRetornoForm','pesqArqRetornoForm:txtBanco','pesqArqRetornoForm:txtAgencia');" onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtBanco',numeros);" value="#{arquivoRetornoBean.cmpi0000Bean.bancoIdentificacaoCliente}" size="4" maxlength="3" id="txtBanco" style="margin-right:20px"/>  
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0000_label_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" readonly="true" onkeyup="proximoCampo(5,'pesqArqRetornoForm','pesqArqRetornoForm:txtAgencia','pesqArqRetornoForm:txtConta');"  onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtAgencia',numeros);"  value="#{arquivoRetornoBean.cmpi0000Bean.agenciaIdentificacaoCliente}" size="6" maxlength="5" id="txtAgencia" />  
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>				
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0000_label_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" readonly="true" value="#{arquivoRetornoBean.cmpi0000Bean.contaIdentificacaoCliente}" onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtConta',numeros);" size="14" maxlength="13" id="txtConta" />  
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>				
		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnConsultarClientes" styleClass="HtmlCommandButtonBradesco" onclick="javascript:validarCamposComValorZero(pesqArqRetornoForm);"  value="#{msgs.label_botao_consultar}" action="#{arquivoRetornoBean.consultarCliente}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="750" border="0">
		<br:brPanelGroup style="width:240px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{arquivoRetornoBean.cmpi0000Bean.tela == 'RETORNO'}" value="#{arquivoRetornoBean.cmpi0000Bean.cpf}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{arquivoRetornoBean.cmpi0000Bean.tela != 'RETORNO'}" value=""/>
		</br:brPanelGroup>		
		<br:brPanelGroup style="width:510px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{arquivoRetornoBean.cmpi0000Bean.tela == 'RETORNO'}" value="#{arquivoRetornoBean.cmpi0000Bean.nomeRazaoFundacao}"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" rendered="#{arquivoRetornoBean.cmpi0000Bean.tela != 'RETORNO'}" value=""/>
		</br:brPanelGroup>		
		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.cmpi0007_label_title_retorno}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0007_label_data_geracao}:" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0007_label_produto}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px" >
		</br:brPanelGroup>			
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0007_label_perfil}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.label_de}" />
			
			 <app:calendar id="calendarioDe" value="#{arquivoRetornoBean.dataDeFiltro}" styleClass="HtmlInputTextBradesco">
			 </app:calendar>
			
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_ate}"/>
			
			 <app:calendar id="calendarioAte" value="#{arquivoRetornoBean.dataAteFiltro}" styleClass="HtmlInputTextBradesco">
			 </app:calendar>
			
		</br:brPanelGroup>

		<br:brPanelGroup style="width:20px" >
		</br:brPanelGroup>

		<br:brPanelGroup>
			<br:brSelectOneMenu  id="produto"  value="#{arquivoRetornoBean.produtoFiltro}" disabled="#{arquivoRetornoBean.cmpi0000Bean.desabServicoRet}" onchange="javascript:habilitarCaixasDeTextoSequenciaCoEmpresaRetorno();">
				<f:selectItem itemValue="" itemLabel="#{msgs.cmpi0007_label_combo_selecione}"/>
				<f:selectItems value="#{arquivoRetornoBean.listaProduto}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px" >
		</br:brPanelGroup>			
		
	    <br:brPanelGroup>	
	    	<br:brInputText  size="15" maxlength="9" id="txtPerfil" value="#{arquivoRetornoBean.perfilFiltro}"  onkeyup="habilitaCaixaDePesquisaSequenciaRetorno()" onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtPerfil',numeros);" readonly="#{arquivoRetornoBean.desabPerfil}" onchange="javascript: habilitarSequencia(document.forms[1],this);">  
		    	<brArq:commonsValidator type="integer" arg="#{msgs.cmpi0007_label_perfil}" server="false" client="true" />
		    </br:brInputText>	
		</br:brPanelGroup>	
								
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0007_label_tipo_retorno}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:6px" >
		</br:brPanelGroup>		
			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0007_label_sequencia_retorno}:"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:6px" >
		</br:brPanelGroup>		
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.cmpi0007_label_situacao}:" />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brSelectOneMenu id="tipoRetorno"  value="#{arquivoRetornoBean.tipoRetornoFiltro}" disabled="#{arquivoRetornoBean.cmpi0000Bean.desabResTipo}">
				<f:selectItem itemValue="" itemLabel="#{msgs.cmpi0007_label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.cmpi0007_label_combo_tipo_confirmacao_agendamento}"/>
				<f:selectItem itemValue="2" itemLabel="#{msgs.cmpi0007_label_combo_tipo_confirmacao_pagamento}"/>
				<f:selectItem itemValue="3" itemLabel="#{msgs.cmpi0007_label_combo_tipo_confirmacao_pagamento_nao_efetuado}"/>
				<f:selectItem itemValue="4" itemLabel="#{msgs.cmpi0007_label_combo_tipo_doc_devolvido}"/>
				<f:selectItem itemValue="5" itemLabel="#{msgs.cmpi0007_label_combo_tipo_ordem_pagamento}"/>
				<f:selectItem itemValue="6" itemLabel="#{msgs.cmpi0007_label_combo_tipo_rastreamento_titulo}"/>

			</br:brSelectOneMenu>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px" >
		</br:brPanelGroup>			
		
		<br:brPanelGroup>
			<br:brInputText  size="15" maxlength="9" id="txtSeqRetorno" onkeypress="aplicamascara('pesqArqRetornoForm','pesqArqRetornoForm:txtSeqRetorno',numeros);" value="#{arquivoRetornoBean.sequenciaRetornoFiltro}" readonly="#{arquivoRetornoBean.desabRetorno}">  
				<brArq:commonsValidator type="integer" arg="#{msgs.cmpi0007_label_sequencia_retorno}" server="false" client="true" />
			</br:brInputText>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px" >
		</br:brPanelGroup>			
		
		<br:brPanelGroup>
			<br:brSelectOneMenu disabled="#{arquivoRetornoBean.disabledCampos}"  id="situacao" value="#{arquivoRetornoBean.situacaoFiltro}" disabled="#{arquivoRetornoBean.desabSitProcRet}">
				<f:selectItem itemValue="" itemLabel="#{msgs.cmpi0007_label_combo_selecione}"/>
				<f:selectItem itemValue="1" itemLabel="#{msgs.cmpi0007_label_combo_situacao_aguardando}"/>
				<f:selectItem itemValue="2" itemLabel="#{msgs.cmpi0007_label_combo_situacao_transmitido}"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>				
		
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnConsultar" disabled="#{arquivoRetornoBean.cmpi0000Bean.desabBtnConsultarRet}" onclick="javascript:validarCamposComValorZero2(pesqArqRetornoForm);" styleClass="HtmlCommandButtonBradesco" value="#{msgs.cmpi0007_label_botao_consultar}" action="#{arquivoRetornoBean.carregaLista}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
		<f:verbatim> <div id="rolagem" style="width:750px; overflow-x:scroll"></f:verbatim> 
			<t:dataTable id="dataTable" value="#{arquivoRetornoBean.listaGridRetorno}" var="result" rows="10" 
			cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
			columnClasses="alinhamento_centro, alinhamento_direita, alinhamento_esquerda, alinhamento_direita, alinhamento_esquerda, alinhamento_esquerda, alinhamento_direita, alinhamento_direita, alinhamento_esquerda, alinhamento_direita, alinhamento_direita"
			headerClass="tabela_celula_destaque_acentuado" width="1520px">
			<t:column width="30px" style="padding-right:5px; padding-left:5px">
				<f:facet name="header">
			      <br:brOutputText value="" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>		
				<t:selectOneRadio  onclick="javascript:habilitarBotaosLoteRetorno(document.forms[1], this);" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
					<f:selectItems value="#{arquivoRetornoBean.listaControleRetorno}"/>
				</t:selectOneRadio>
		    	<t:radio for="sor" index="#{result.totalRegistros}" />
			</t:column>
			  <t:column width="160px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.cmpi0007_label_cpf_cnpj}" />
			    </f:facet>
			    <br:brOutputText value="#{result.cpf_completo}" />
			  </t:column>
			  <t:column width="350px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.cmpi0007_label_nome_razao_social}" />
			    </f:facet>
			    <br:brOutputText value="#{result.nome}"/>
			  </t:column>
			  <t:column width="70px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.cmpi0007_label_perfil}" />
			    </f:facet>
			    <br:brOutputText value="#{result.perfil}" />
			  </t:column>
			  <t:column width="350px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.cmpi0007_label_produto}" />
			    </f:facet>
			    <br:brOutputText value="#{result.produto}"  />
			  </t:column>
			  <t:column width="330px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.cmpi0007_label_title_tipo_retorno}" />
			    </f:facet>
			    <br:brOutputText value="#{result.descricaoTipoArquivo}"/>
			   </t:column>
			  <t:column width="150px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.cmpi0007_label_sequencia_retorno}" />
			    </f:facet>			    
  			    <br:brOutputText value="#{result.numeroArquivoRetorno}" />			    
			  </t:column>
			  <t:column width="250px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.cmpi0007_label_data_hora_geracao}" />
			    </f:facet>
			    <br:brOutputText value="#{result.horaGeracaoRetorno}"  />
			  </t:column>
			  <t:column width="200px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.cmpi0007_label_situacao_retorno}" />
			    </f:facet>
			    <br:brOutputText value="#{result.descricaoSituacao}"/>
			  </t:column>
			  <t:column width="80px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.cmpi0007_label_quantidade_registros}" />
			    </f:facet>
			    <br:brOutputText value="#{result.qtdeOcorrencias}" />
			  </t:column>
			  <t:column width="80px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.cmpi0007_label_valor_total}" />
			    </f:facet>
			    <br:brOutputText value="#{result.valorRegistroArquivoRetorno}" />
			  </t:column>
			</t:dataTable>
			<f:verbatim> </div> </f:verbatim> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" rendered="#{arquivoRetornoBean.listaGridRetorno!= null && arquivoRetornoBean.mostraBotoes0007}">	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{arquivoRetornoBean.pesquisarArquivosLotes}" >
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		<f:verbatim><br></f:verbatim>		
	<br:brPanelGrid columns="2" width="750" border="0" style="text-align:right" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>		
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimpar" styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.cmpi0007_label_botao_limpar}" action="#{arquivoRetornoBean.limpar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnPesquisarLotes" styleClass="HtmlCommandButtonBradesco" disabled="true" value="#{msgs.cmpi0007_label_botao_consultar_lotes}" action="#{arquivoRetornoBean.consultarLotes}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
</br:brPanelGrid>


<f:verbatim>
		<script language="javascript">
				if(document.forms['pesqArqRetornoForm'] != null){	
					if(document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtCnpj1'] != null ){		
						document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtCnpj1'].focus();
					
					}	
				}
		</script>
</f:verbatim>

<brArq:validatorScript functionName="validateForm"/>

<f:verbatim>


	<script language="javascript">
	function validarCamposComValorZero(form){
		
			for(i=0; i<form.elements.length; i++){
				
				if(form.elements[i].name == 'radioFiltro'){
					
					
					if ((form.elements[i].value == 0) && (form.elements[i].checked)) {
					
						document.forms ['pesqArqRetornoForm'].elements ['pesqArqRetornoForm:hiddenRadioFiltro'].value = form.elements[i].value;
					
						if((validarValorVazio('pesqArqRetornoForm','pesqArqRetornoForm:txtCnpj1', 'CNPJ')) || (validarValorMenorZero('pesqArqRetornoForm','pesqArqRetornoForm:txtCnpj1', 'CNPJ'))){
							return;
						}
						
						if((validarValorVazio('pesqArqRetornoForm','pesqArqRetornoForm:txtCnpj2', 'FILIAL')) || (validarValorMenorZero('pesqArqRetornoForm','pesqArqRetornoForm:txtCnpj2', 'FILIAL'))){
							return;
						}
						
						if(validarValorVazio('pesqArqRetornoForm','pesqArqRetornoForm:txtCnpj3', 'CONTROLE DO CNPJ')){
							return;
						}
					}
					
					if ((form.elements[i].value == 1) && (form.elements[i].checked)) {
						
						document.forms ['pesqArqRetornoForm'].elements ['pesqArqRetornoForm:hiddenRadioFiltro'].value = form.elements[i].value;
						
						if((validarValorVazio('pesqArqRetornoForm','pesqArqRetornoForm:txtCpf1', 'CPF')) || (validarValorMenorZero('pesqArqRetornoForm','pesqArqRetornoForm:txtCpf1', 'CPF'))){
							return;
						}
						
						if(validarValorVazio('pesqArqRetornoForm','pesqArqRetornoForm:txtCpf2', 'CONTROLE DO CPF')){
							return;
						}	
					}
					
					if ((form.elements[i].value == 2) && (form.elements[i].checked)) {
						
						document.forms ['pesqArqRetornoForm'].elements ['pesqArqRetornoForm:hiddenRadioFiltro'].value = form.elements[i].value;
						
						if(validarValorVazio('pesqArqRetornoForm','pesqArqRetornoForm:txtNomeRazao', 'NOME / RAZAO SOCIAL')){
							return;
						}
					}
					
					if ((form.elements[i].value == 3) && (form.elements[i].checked)) {
						
						document.forms ['pesqArqRetornoForm'].elements ['pesqArqRetornoForm:hiddenRadioFiltro'].value = form.elements[i].value;
						
						if((validarValorVazio('pesqArqRetornoForm','pesqArqRetornoForm:txtBanco', 'BANCO')) || (validarValorMenorZero('pesqArqRetornoForm','pesqArqRetornoForm:txtBanco', 'BANCO'))){
							return;
						}

						if((validarValorVazio('pesqArqRetornoForm','pesqArqRetornoForm:txtAgencia', 'AGENCIA')) || (validarValorMenorZero('pesqArqRetornoForm','pesqArqRetornoForm:txtAgencia', 'AGENCIA'))){
							return;
						}
						
						if((validarValorVazio('pesqArqRetornoForm','pesqArqRetornoForm:txtConta', 'CONTA')) || (validarValorMenorZero('pesqArqRetornoForm','pesqArqRetornoForm:txtConta', 'CONTA'))){
							return;
						}
						
					}					
				}
			}
		}
		
		function validarCamposComValorZero2(){
			
			if((!document.forms ['pesqArqRetornoForm'].elements ['pesqArqRetornoForm:txtPerfil'].value == '') && (validarValorIgualAZero('pesqArqRetornoForm','pesqArqRetornoForm:txtPerfil', 'PERFIL'))){
				return;
			}
			
			if((!document.forms ['pesqArqRetornoForm'].elements ['pesqArqRetornoForm:txtSeqRetorno'].value == '') && validarValorIgualAZero('pesqArqRetornoForm','pesqArqRetornoForm:txtSeqRemessa', 'SEQUENCIA')){
				return;
			}	
				
		}

		function validarValorMenorZero(sform, scampo, fieldName){
					
			if(valor < 0){
				document.forms [sform].elements [scampo].value = '';
				document.forms [sform].elements [scampo].focus();
				alert(fieldName + ' DEVE SER INFORMADO.');
				return true;
			}
		}
		
		function validarValorVazio(sform, scampo, fieldName){
			
			var valor = document.forms [sform].elements [scampo].value;
			if(valor == null || valor == ''){
				document.forms [sform].elements [scampo].value = '';
				document.forms [sform].elements [scampo].focus();
				alert(fieldName + ' DEVE SER INFORMADO.');
				return true;
			}
		}
	</script>

</f:verbatim>


</brArq:form>




