<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>


<brArq:form id="pesqEstatisticasInconsistentesForm" name="pesqEstatisticasInconsistentesForm">
<h:inputHidden id="hiddenRadio" value="#{arquivoRetornoBean.codListaInconsistentesRadio}"/>

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0" border="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.cmpi0010_cadastro}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_cpf}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.cpf}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_nome_razao}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.nomeRazao}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_servico}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.produto}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_perfil}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.perfil}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
    <br:brPanelGrid columns="10" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_banco}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.banco}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_agencia}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.agencia}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_razao}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.razao}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_conta}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.conta}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_codigo_lancamento}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.codigoLancamento}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_canal_transmissao}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.canalTransmissao}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.cmpi0010_retorno}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="8" cellpadding="0" cellspacing="0" width="750px" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_sequencia}"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.sequenciaRetorno}"/>	
		</br:brPanelGroup>					
		<br:brPanelGroup style="width: 122px;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_datahorageracao}:"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="width: 148px; margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.dataHoraGeracao}"/>	
		</br:brPanelGroup>		
		<br:brPanelGroup style="width: 87px;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0008_tipoderetorno}:"/>	
		</br:brPanelGroup>		
	    <br:brPanelGroup style="width: 204px; margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.tipoRetorno}" /> 
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="6" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_situacao}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
		    <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.situacaoProcessamentoRetorno}" />
		</br:brPanelGroup>			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_layout}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.layout}"/>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_ambiente_processamento}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.ambienteProcessamento}" /> 			    			       		    
		</br:brPanelGroup>		
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_quantidade_registros}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.qtdRegistrosRetorno}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_valor_total}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.valorTotalRetorno}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.cmpi0010_lote}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_sequencia_lote}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.sequenciaLote}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_banco_agencia_conta}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.bancoAgenciaConta}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0010_modalidade_pagamento}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.modalidadePagamentoLote}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.cmpi0010_estatisticas_lote}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0009_quantidade_consistentes_pagos}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.qtdConsistentes}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0009_valor_consistentes_pagos}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.valorConsistentes}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0009_quantidade_inconsistentes_pagos}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.qtdInconsistentes}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0009_valor_inconsistentes_pagos}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.valorInconsistentes}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0009_quantidade_geral}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.qtdRegistrosLote}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.cmpi0009_valor_geral}:"/>
		</br:brPanelGroup>		
	    <br:brPanelGroup style="margin-right: 20px">	
			<f:verbatim>&nbsp;</f:verbatim>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{arquivoRetornoBean.valorTotalLote}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
 	
	<f:verbatim> <BR> </f:verbatim>	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
	<br:brPanelGroup >	
	
			<t:dataTable id="dataTable" value="#{arquivoRetornoBean.listaGridInconsistentes}" var="result" rows="10" 
			cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
			columnClasses="alinhamento_centro, alinhamento_direita, alinhamento_esquerda"
			headerClass="tabela_celula_destaque_acentuado" width="750px">

		 <t:column width="30px" style="padding-right:5px; padding-left:5px">
		    <f:facet name="header">
		      <br:brOutputText value="" />
		    </f:facet>	
		    <t:selectOneRadio onclick="javascript:radioGridInconsistentesCMPI0010(document.forms[1], this);" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
				<f:selectItems value="#{arquivoRetornoBean.listaControleInconsistentes}"/>
			</t:selectOneRadio>
			<t:radio for="sor" index="#{result.linhaSelecionada}" />
		  </t:column>		    			
		  <t:column width="360px" style="padding-right:5px; padding-left:5px">
		    <f:facet name="header">
		      <br:brOutputText value="#{msgs.cmpi0010_sequencia_registro}"/>
		    </f:facet>
		    <br:brOutputText value="#{result.numeroSequenciaRetorno}" />
		  </t:column>
		  <t:column width="360px" style="padding-right:5px; padding-left:5px">
		    <f:facet name="header">
		      <h:outputText value="#{msgs.cmpi0010_numero_pagamento}" />
		    </f:facet>
		    <br:brOutputText value="#{result.controlePagamento}" />
		  </t:column>
		</t:dataTable>
		
	</br:brPanelGroup>
	</br:brPanelGrid>		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" rendered="#{arquivoRetornoBean.listaGridInconsistentes != null && arquivoRetornoBean.mostraBotoes0010}">	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{arquivoRetornoBean.pesquisarInconsistencia}" >
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.cmpi0010_btn_anterior}" title="#{msgs.cmpi0010_btn_anterior}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.cmpi0010_btn_proximo}" title="#{msgs.cmpi0010_btn_proximo}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<f:verbatim><br></f:verbatim>		

	<br:brPanelGrid columns="2" width="750" border="0" style="text-align:right" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>		
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brCommandButton id="btnInconsistentes" style="margin-right:5px" styleClass="HtmlCommandButtonBradesco" value="#{msgs.cmpi0010_btn_ocorrencias}" action="#{arquivoRetornoBean.consultarOcorrenciasRetorno}" disabled="true">				
				<brArq:submitCheckClient/>
			</br:brCommandButton> 
			<br:brCommandButton id="btnVoltar" styleClass="HtmlCommandButtonBradesco"  value="#{msgs.cmpi0010_btn_voltar}" action="VOLTAR_ESTATISTICAS">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid>
		<br:brPanelGroup>
			<f:verbatim>&nbsp;</f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>

</brArq:form>
