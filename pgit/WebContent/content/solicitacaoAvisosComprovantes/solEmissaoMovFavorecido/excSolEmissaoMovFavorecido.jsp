<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detSolEmissaoMovFavorecidoForm" name="detSolEmissaoMovFavorecidoForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.nrCnpjCpf}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsRazaoSocial}"/>    
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsEmpresa}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.nroContrato}"/>    
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsContrato}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.cdSituacaoContrato}"/>    
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_solicitacao}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_solicitacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.nrSolicitacaoPagamentoIntegrado}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_inicio}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dtInicioPeriodoMovimentacao}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_fim}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dtFimPeriodoMovimentacao}"/>    
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_servico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsProdutoServicoOperacao}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_modalidade}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsProdutoOperacaoRelacionado}"/>    
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_favorecido}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo_favorecido}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.cdRecebedorCredito}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.cpfCnpjRecebedorFormatado}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_destino}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_logradouro}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsLogradouroPagador}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsNumeroLogradouroPagador}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_complemento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsComplementoLogradouroPagador}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_bairro}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsBairroClientePagador}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_municipio}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsMunicipioClientePagador}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_uf}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.cdSiglaUfPagador}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cep}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.cepFormatado}"/>
			</br:brPanelGroup> 				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>				
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia_operadora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsAgencia}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_departamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.departamento}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
			<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3"  cellpadding="0" cellspacing="0" >
			<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="R$ " rendered="#{solEmissaoMovFavorecidoBean.vlTarifaPadrao != NULL}"/>					
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.vlTarifaPadrao}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
			<br:brPanelGroup style="margin-left:50px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_desconto_tarifa}:"/>
				<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{solEmissaoMovFavorecidoBean.percentualDescTarifa}" converter="decimalBrazillianConverter"/>
				<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="%" rendered="#{solEmissaoMovFavorecidoBean.percentualDescTarifa != NULL}"/>
			</br:brPanelGroup>
			<br:brPanelGroup style="margin-left:50px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="R$ "  rendered="#{solEmissaoMovFavorecidoBean.vlrTarifaAtual != NULL}"/>		
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.vlrTarifaAtual}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<f:verbatim><hr class="lin"> </f:verbatim>
		
	
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{solEmissaoMovFavorecidoBean.voltarConsultar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >
				<br:brCommandButton id="btnExcluir" styleClass="bto1" style="align:left" value="#{msgs.btn_excluir}" action="#{solEmissaoMovFavorecidoBean.confirmarExcluir}" onclick="javascript: if (!confirm('#{msgs.label_conf_exclusao}')) { desbloquearTela(); return false; }">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>