<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incSolEmissaoMovFavorecidoFiltroForm" name="incSolEmissaoMovFavorecidoFiltroForm" >
<h:inputHidden id="hiddenTarifaPadrao" value="#{solEmissaoMovFavorecidoBean.vlTarifa}"/>
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.nrCnpjCpf}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsRazaoSocial}"/>    
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsEmpresa}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.nroContrato}"/>    
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.dsContrato}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.cdSituacaoContrato}"/>    
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"></f:verbatim>
			    
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
			<br:brPanelGroup>
				<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	    	<br:brPanelGroup>
	    		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass ="HtmlOutputTextRespostaBradesco" value="#{msgs.label_periodo_pagto}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >		
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicioSolicitacao');" id="dataInicioSolicitacao" value="#{solEmissaoMovFavorecidoBean.dtInicioPeriodoPagamento}"></app:calendar>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_a}"/>
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFimSolicitacao');" id="dataFimSolicitacao" value="#{solEmissaoMovFavorecidoBean.dtFimPeriodoPagamento}" ></app:calendar>
				</br:brPanelGroup>			    
			</br:brPanelGrid>
		</a4j:outputPanel>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_de_servico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboTipoServico" value="#{solEmissaoMovFavorecidoBean.tipoServicoFiltro}" styleClass="HtmlSelectOneMenuBradesco" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{solEmissaoMovFavorecidoBean.listaTipoServicoFiltro}" />						
							<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onchange" reRender="cboModalidade" action="#{solEmissaoMovFavorecidoBean.listarModalidades}"/>						
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>				
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_modalidade}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="cboModalidade" value="#{solEmissaoMovFavorecidoBean.modalidadeFiltro}" styleClass="HtmlSelectOneMenuBradesco"
				    	  disabled="#{solEmissaoMovFavorecidoBean.tipoServicoFiltro == null || solEmissaoMovFavorecidoBean.tipoServicoFiltro == 0}" >
							<f:selectItems value="#{solEmissaoMovFavorecidoBean.listaModalidadeFiltro}" />												
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>				
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio id="radioFavorecido" value="#{solEmissaoMovFavorecidoBean.radioIncluirFavorecido}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="favorecido" action="#{solEmissaoMovFavorecidoBean.limparArgumentosFavorecido}" status="statusAguarde" />			
			</t:selectOneRadio>
		</br:brPanelGrid>
			
		<a4j:outputPanel id="favorecido" style="width: 100%; text-align: left" ajaxRendered="true">				
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_favorecido}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>
					    	<t:radio for="radioFavorecido" index="0" />
					    </br:brPanelGroup>
					    <br:brPanelGroup>
					    	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>			
									<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								</br:brPanelGroup>		
								<br:brPanelGroup>			
									<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo}:"/>
								</br:brPanelGroup>
							</br:brPanelGrid>
							
							<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
							    <br:brPanelGroup>			
								</br:brPanelGroup>	
							</br:brPanelGrid>
							
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					    		<br:brPanelGroup>
					    			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{solEmissaoMovFavorecidoBean.radioIncluirFavorecido != '0'}" value="#{solEmissaoMovFavorecidoBean.cdFavorecido}" size="25" maxlength="15" id="txtCodigoFavorecido" />
							    </br:brPanelGroup>	
							</br:brPanelGrid>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="2" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>
					    	<t:radio for="radioFavorecido" index="1" />
					    </br:brPanelGroup>
				    	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
				    		<br:brPanelGroup>
					    		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
										<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									</br:brPanelGroup>		
									<br:brPanelGroup>			
										<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_inscricao}:"/>
									</br:brPanelGroup>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
								    <br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						    		<br:brPanelGroup>
								    	<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{solEmissaoMovFavorecidoBean.radioIncluirFavorecido != '1'}" value="#{solEmissaoMovFavorecidoBean.inscricaoFavorecido}" size="25" maxlength="15" id="txtInscricaoFavorecido" />
									</br:brPanelGroup>
								</br:brPanelGrid>
							</br:brPanelGroup>
							
							<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
					    		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
										<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									</br:brPanelGroup>		
									<br:brPanelGroup>			
										<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo}:"/>
									</br:brPanelGroup>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
								    <br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>
								    	<br:brSelectOneMenu id="cboTipoFavorecido" value="#{solEmissaoMovFavorecidoBean.tipoInscricaoFiltro}" styleClass="HtmlSelectOneMenuBradesco"
								    	  disabled="#{solEmissaoMovFavorecidoBean.radioIncluirFavorecido != '1'}" >
											<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
											<f:selectItems value="#{solEmissaoMovFavorecidoBean.listaTipoInscricaoFiltro}" />												
										</br:brSelectOneMenu>
									</br:brPanelGroup>
								</br:brPanelGrid>
							</br:brPanelGrid>
						</br:brPanelGrid>					
					</br:brPanelGrid>				
				</br:brPanelGroup>
			</br:brPanelGrid>	
		</a4j:outputPanel>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
				
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_info_emissao_destino_entrega}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio id="radioInfoAdicionais" value="#{solEmissaoMovFavorecidoBean.radioInfoAdicionais}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
				<f:selectItem itemValue="0" itemLabel="#{msgs.label_cliente}" />
				<f:selectItem itemValue="1" itemLabel="#{msgs.label_agencia_departamento}" />
				<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="destinoEntrega" action="#{solEmissaoMovFavorecidoBean.limparInfoAdicionais}" status="statusAguarde" />			
			</t:selectOneRadio>
		</br:brPanelGrid>
		
		<a4j:outputPanel id="destinoEntrega" style="width: 100%; text-align: left" ajaxRendered="true">
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					<t:radio for="radioInfoAdicionais" index="0" />
				</br:brPanelGrid>
		
				<br:brPanelGrid columns="1" style="margin-left:10px" cellpadding="0" cellspacing="0" >
					<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>	
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />		
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_endereco}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>
								<br:brPanelGrid columns="4" cellpadding="0" style="margin-left:20px" cellspacing="0">
									<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
										<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup>			
												<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
											</br:brPanelGroup>		
											<br:brPanelGroup>			
												<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_logradouro}:"/>
											</br:brPanelGroup>
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
										    <br:brPanelGroup>			
											</br:brPanelGroup>	
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
								    		<br:brPanelGroup>
								    			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="true" value="#{filtroEnderecoEmailBean.enderecoSelecionado.dsLogradouroPagador}" size="30" id="txtLogradouro" maxlength="70" />
										    </br:brPanelGroup>	
										</br:brPanelGrid>
									</br:brPanelGrid>
									
									<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
										<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-top:6px">
											<br:brPanelGroup>			
												<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
											</br:brPanelGroup>		
											<br:brPanelGroup>			
												<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_numero}:"/>
											</br:brPanelGroup>
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
										    <br:brPanelGroup>			
											</br:brPanelGroup>	
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
								    		<br:brPanelGroup>
								    			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="true" value="#{filtroEnderecoEmailBean.enderecoSelecionado.dsNumeroLogradouroPagador}" size="10" id="txtNumero" maxlength="7" />
										    </br:brPanelGroup>	
										</br:brPanelGrid>
									</br:brPanelGrid>
						
									<br:brPanelGrid columns="1" style="margin-left:20px;margin-top:6px" cellpadding="0" cellspacing="0" >
										<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup>			
												<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
											</br:brPanelGroup>		
											<br:brPanelGroup>			
												<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_complemento}:"/>
											</br:brPanelGroup>
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup>			
											</br:brPanelGroup>	
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup>
												<br:brInputText styleClass="HtmlInputTextBradesco" disabled="true" value="#{filtroEnderecoEmailBean.enderecoSelecionado.dsComplementoLogradouroPagador}" size="30" id="txtComplemento" maxlength="30" />
											</br:brPanelGroup>	
										</br:brPanelGrid>
									</br:brPanelGrid>
									
									<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
										<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-top:6px">
											<br:brPanelGroup>			
												<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
											</br:brPanelGroup>		
											<br:brPanelGroup>			
												<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_cep}:"/>
											</br:brPanelGroup>
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
										    <br:brPanelGroup>			
											</br:brPanelGroup>	
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
								    		<br:brPanelGroup>
								    			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="true" value="#{filtroEnderecoEmailBean.enderecoSelecionado.cdCepPagador}"  onkeyup="proximoCampo(5,'incSolEmissaoMovFavorecidoFiltroForm','incSolEmissaoMovFavorecidoFiltroForm:txtCep1','incSolEmissaoMovFavorecidoFiltroForm:txtCep2');" size="5" maxlength="5" id="txtCep1" onkeypress="onlyNum()"/>
										    	<br:brInputText style="margin-left:5px" styleClass="HtmlInputTextBradesco" disabled="true" value="#{filtroEnderecoEmailBean.enderecoSelecionado.cdCepComplementoPagador}" size="3" maxlength="3" id="txtCep2" onkeypress="onlyNum()" />
										    </br:brPanelGroup>	
										</br:brPanelGrid>
									</br:brPanelGrid>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="4" cellpadding="0" style="margin-left:20px" cellspacing="0">
									<br:brPanelGroup>
										<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup>			
												<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
											</br:brPanelGroup>		
											<br:brPanelGroup>			
												<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_bairro}:"/>
											</br:brPanelGroup>
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup>			
											</br:brPanelGroup>	
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup style="margin-right:20px">
												<br:brInputText styleClass="HtmlInputTextBradesco" disabled="true" value="#{filtroEnderecoEmailBean.enderecoSelecionado.dsBairroClientePagador}" size="30" id="txtBairro" maxlength="40" />
											</br:brPanelGroup>	
										</br:brPanelGrid>							
									</br:brPanelGroup>	

									<br:brPanelGroup>	
										<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup>			
												<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
											</br:brPanelGroup>		
											<br:brPanelGroup>			
												<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_municipio}:"/>
											</br:brPanelGroup>
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup>			
											</br:brPanelGroup>	
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup style="margin-right:20px">
												<br:brInputText styleClass="HtmlInputTextBradesco" disabled="true" value="#{filtroEnderecoEmailBean.enderecoSelecionado.dsMunicipioClientePagador}" size="30" id="txtMunicipio" maxlength="40" />
											</br:brPanelGroup>	
										</br:brPanelGrid>								
									</br:brPanelGroup>	
									
									<br:brPanelGroup>	
										<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup>			
												<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
											</br:brPanelGroup>		
											<br:brPanelGroup>			
												<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_uf}:"/>
											</br:brPanelGroup>
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup>			
											</br:brPanelGroup>	
										</br:brPanelGrid>
										
										<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
											<br:brPanelGroup>
												<br:brInputText styleClass="HtmlInputTextBradesco" disabled="true" value="#{filtroEnderecoEmailBean.enderecoSelecionado.cdSiglaUfPagador}" size="3" maxlength="2" id="txtUf" />
											</br:brPanelGroup>	
										</br:brPanelGrid>								
									</br:brPanelGroup>	
									
									<br:brPanelGroup>
										<br:brCommandButton  id="btnConsultarEndereco" disabled="#{solEmissaoMovFavorecidoBean.radioInfoAdicionais != '0'}" styleClass="bto1" style="margin-top:17px;margin-left:5px" value="#{msgs.btn_consultar_endereco}" action="#{filtroEnderecoEmailBean.consultarEnderecoEmail}">				
											<brArq:submitCheckClient/>
										</br:brCommandButton>	
									</br:brPanelGroup>
								</br:brPanelGrid>
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>
				</br:brPanelGrid>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
				<t:selectOneRadio id="radioAgenciaDepto" disabled="#{solEmissaoMovFavorecidoBean.radioInfoAdicionais != '1'}" value="#{solEmissaoMovFavorecidoBean.radioAgenciaDepto}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
					<f:selectItem itemValue="0" itemLabel="" />
					<f:selectItem itemValue="1" itemLabel="" itemDisabled="#{solEmissaoMovFavorecidoBean.indicadorDescontoBloqueio}"/>
					<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="destinoEntrega,btnCalcular" action="#{solEmissaoMovFavorecidoBean.selecionarAgenciaDepartamento}" status="statusAguarde" />			
				</t:selectOneRadio>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					<t:radio for="radioInfoAdicionais" index="1" />
				</br:brPanelGrid>
		
				<br:brPanelGrid columns="1" style="margin-left:20px; margin-top:6px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>
						    	<t:radio for="radioAgenciaDepto" index="0" />
						    </br:brPanelGroup>
						    
						    <br:brPanelGroup>
						    	<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
						    	<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.agenciaOperadora}"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="2" style="margin-top:6px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>
						    	<t:radio for="radioAgenciaDepto" index="1" />
						    </br:brPanelGroup>
					    	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					    		<br:brPanelGroup>
						    		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
										<br:brPanelGroup>			
											<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
										</br:brPanelGroup>		
										<br:brPanelGroup>			
											<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_departamento}:"/>
										</br:brPanelGroup>
									</br:brPanelGrid>
									
									<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
									    <br:brPanelGroup>			
										</br:brPanelGroup>	
									</br:brPanelGrid>
									
									<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
							    		<br:brPanelGroup>
									    	<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{solEmissaoMovFavorecidoBean.radioInfoAdicionais != '1' || solEmissaoMovFavorecidoBean.radioAgenciaDepto != '1'}" value="#{solEmissaoMovFavorecidoBean.departamentoFiltro}" size="10" id="txtDepartamento" maxlength="6"/>
										</br:brPanelGroup>
									</br:brPanelGrid>
								</br:brPanelGroup>
							</br:brPanelGrid>
						</br:brPanelGrid>				
					</br:brPanelGroup>
				</br:brPanelGrid>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="0">
			<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}: "/>
					<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.vlTarifa}" id="txtTarifaPadrao" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />			
				<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_desconto_tarifa}: "/>
		    	<br:brInputText size="9" id="txtDescontoTarifa" alt="percentual" value="#{solEmissaoMovFavorecidoBean.percentualDescTarifa}" converter="decimalBrazillianConverter" style="text-align:right; margin-right:3px" styleClass="HtmlInputTextBradesco" 
		    		onfocus="loadMasks();" onblur="validarCampoDescontoTarifa(this)" disabled="#{solEmissaoMovFavorecidoBean.disabledPercentualDesconto || solEmissaoMovFavorecidoBean.indicadorDescontoBloqueio}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="%"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}: "/>
					<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoMovFavorecidoBean.vlTarifaAtualizada}" id="txtTarifaAtualizada" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
		</a4j:outputPanel>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{solEmissaoMovFavorecidoBean.voltarConsultar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >
				<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.btn_limpar}" action="#{solEmissaoMovFavorecidoBean.limparIncluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnCalcular" styleClass="bto1" style="margin-left: 5px;" value="#{msgs.btn_calcular}" action="#{solEmissaoMovFavorecidoBean.calcularTarifaAtualizada}" disabled="#{solEmissaoMovFavorecidoBean.disabledBtnCalcular || solEmissaoMovFavorecidoBean.indicadorDescontoBloqueio}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAvancar" styleClass="bto1" style="margin-left: 5px;" value="#{msgs.label_avan�ar}" action="#{solEmissaoMovFavorecidoBean.abrirConfirmarIncluir}" 
						onclick="javascript:desbloquearTela(); 
						return validaIncluirMovimentacao(document.forms[1],					
						'#{msgs.label_ocampo}', 
						'#{msgs.label_necessario}',
						'#{msgs.label_periodo_pagto}', 
						'#{msgs.label_codigo_favorecido}', 
						'#{msgs.label_inscricao_favorecido}', 
						'#{msgs.label_tipo_favorecido}',						
						'#{msgs.label_tipo_postagem}', 
						'#{msgs.label_logradouro}', 
						'#{msgs.label_numero}', 
						'#{msgs.label_cep}' , 
						'#{msgs.label_email}', 
						'#{msgs.label_agencia_departamento}', 
						'#{msgs.label_departamento}', 
						'#{msgs.label_tipo_de_servico}',
						'#{msgs.label_modalidade}',
						'#{msgs.label_destino_entrega}',
						'#{msgs.label_codigo_favorecido_ou_inscricao_favorecido}',
						'#{msgs.label_complemento}',
						'#{msgs.label_bairro}',
						'#{msgs.label_municipio}',
						'#{msgs.label_uf}',
						'#{msgs.solEmissaoComprovantePagtoClientePag_percentual_maior_cem}',
						'#{msgs.label_tipo_de_servico}');">
					<brArq:submitCheckClient/>
				</br:brCommandButton>								
			</br:brPanelGroup>
		</br:brPanelGrid>
		<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>