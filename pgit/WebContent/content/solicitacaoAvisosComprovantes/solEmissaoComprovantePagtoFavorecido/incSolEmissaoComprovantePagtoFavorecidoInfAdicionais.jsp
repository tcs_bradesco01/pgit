<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incSolEmissaoComprovantePagtoFavorecidoInfAdicionaisForm" name="incSolEmissaoComprovantePagtoFavorecidoInfAdicionaisForm" >
<h:inputHidden id="hiddenTarifaPadrao" value="#{solEmissaoComprovantePagtoFavorecidoBean.vlTarifa}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoFavorecidoBean.nrCnpjCpf}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoFavorecidoBean.dsRazaoSocial}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoFavorecidoBean.dsEmpresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoFavorecidoBean.nroContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoFavorecidoBean.dsContrato}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoFavorecidoBean.cdSituacaoContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
		
			<app:scrollableDataTable id="dataTable" value="#{solEmissaoComprovantePagtoFavorecidoBean.listaIncluirComprovantesSelecionados}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_tipo_de_servico}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsResumoProdutoServico}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_modalidade}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsOperacaoProdutoServico}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_numero_pagamento}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdControlePagamento}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabCenter" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_data_do_pagamento}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dtCreditoPagamentoFormatada}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_valor_do_pagamento}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.vlEfetivoPagamento}" styleClass="tableFontStyle" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_favorecido}" style="width:200; text-align:center"/>
				    </f:facet>
				     <br:brOutputText value="#{result.favorecidoFormatado}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="230px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_conta_debito}" style="width:230px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.contaDebitoFormatada}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_situacao}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoOperacaoPagamento}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_info_emissao_destino_entrega}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="radioInfoAdicionais" value="#{solEmissaoComprovantePagtoFavorecidoBean.radioInfoAdicionais}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
			<f:selectItem itemValue="0" itemLabel="#{msgs.label_cliente}" />
			<f:selectItem itemValue="1" itemLabel="#{msgs.label_agencia_departamento}" />
			<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="infoAdicionais" action="#{solEmissaoComprovantePagtoFavorecidoBean.limparInfoAdicionais}" status="statusAguarde" />			
		</t:selectOneRadio>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="infoAdicionais" style="width: 100%; text-align: left" ajaxRendered="true">
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="radioTipoPostagem" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioInfoAdicionais != '0'}" value="#{solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
			<f:selectItem itemValue="0" itemLabel="#{msgs.label_correio}" />
			<f:selectItem itemValue="1" itemLabel="#{msgs.label_email}" />
			<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="infoAdicionais" action="#{solEmissaoComprovantePagtoFavorecidoBean.limparTipoPostagem}" status="statusAguarde" />			
		</t:selectOneRadio>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			<t:radio for="radioInfoAdicionais" index="0" />
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>	
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />		
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_postagem}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>
				    	<t:radio for="radioTipoPostagem" index="0" />
				    </br:brPanelGroup>
				    
				    <br:brPanelGroup>
						<br:brPanelGrid columns="4" cellpadding="0" style="margin-left:20px" cellspacing="0">
							<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
										<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									</br:brPanelGroup>		
									<br:brPanelGroup>			
										<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_logradouro}:"/>
									</br:brPanelGroup>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
								    <br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						    		<br:brPanelGroup>
						    			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem != '0' || !solEmissaoComprovantePagtoFavorecidoBean.habilitaCampoEndereco}" value="#{filtroEnderecoEmailBean.enderecoSelecionado.dsLogradouroPagador}" size="30" id="txtLogradouro" maxlength="70" />
								    </br:brPanelGroup>	
								</br:brPanelGrid>
							</br:brPanelGrid>
							
							<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-top:6px">
									<br:brPanelGroup>			
										<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									</br:brPanelGroup>		
									<br:brPanelGroup>			
										<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_numero}:"/>
									</br:brPanelGroup>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
								    <br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						    		<br:brPanelGroup>
						    			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem != '0' || !solEmissaoComprovantePagtoFavorecidoBean.habilitaCampoEndereco}" value="#{filtroEnderecoEmailBean.enderecoSelecionado.dsNumeroLogradouroPagador}" size="10" id="txtNumero" maxlength="7" />
								    </br:brPanelGroup>	
								</br:brPanelGrid>
							</br:brPanelGrid>
							
							<br:brPanelGrid columns="1" style="margin-left:20px;margin-top:6px" cellpadding="0" cellspacing="0" >
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
										<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									</br:brPanelGroup>		
									<br:brPanelGroup>			
										<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_complemento}:"/>
									</br:brPanelGroup>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
								    <br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						    		<br:brPanelGroup>
						    			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem != '0' || !solEmissaoComprovantePagtoFavorecidoBean.habilitaCampoEndereco}" value="#{filtroEnderecoEmailBean.enderecoSelecionado.dsComplementoLogradouroPagador}" size="30" id="txtComplemento" maxlength="30" />
								    </br:brPanelGroup>	
								</br:brPanelGrid>
							</br:brPanelGrid>
							
							<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-top:6px">
									<br:brPanelGroup>			
										<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									</br:brPanelGroup>		
									<br:brPanelGroup>			
										<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_cep}:"/>
									</br:brPanelGroup>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
								    <br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						    		<br:brPanelGroup>
						    			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem != '0' || !solEmissaoComprovantePagtoFavorecidoBean.habilitaCampoEndereco}" value="#{filtroEnderecoEmailBean.enderecoSelecionado.cdCepPagador}" onkeyup="proximoCampo(5,'incSolEmissaoComprovantePagtoFavorecidoInfAdicionaisForm','incSolEmissaoComprovantePagtoFavorecidoInfAdicionaisForm:txtCep1','incSolEmissaoComprovantePagtoFavorecidoInfAdicionaisForm:txtCep2');" size="5" maxlength="5" id="txtCep1" onkeypress="onlyNum();"/>
								    	<br:brInputText style="margin-left:5px" styleClass="HtmlInputTextBradesco" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem != '0' || !solEmissaoComprovantePagtoFavorecidoBean.habilitaCampoEndereco}" value="#{filtroEnderecoEmailBean.enderecoSelecionado.cdCepComplementoPagador}" size="3" maxlength="3" id="txtCep2" onkeypress="onlyNum();" />
								    </br:brPanelGroup>	
								</br:brPanelGrid>
							</br:brPanelGrid>
						</br:brPanelGrid>						
						
						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="4" style="margin-left:20px" cellpadding="0" cellspacing="0">	
							<br:brPanelGroup>
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
										<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									</br:brPanelGroup>		
									<br:brPanelGroup>			
										<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_bairro}:"/>
									</br:brPanelGroup>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
								    <br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						    		<br:brPanelGroup style="margin-right:20px">
						    			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem != '0' || !solEmissaoComprovantePagtoFavorecidoBean.habilitaCampoEndereco}" value="#{filtroEnderecoEmailBean.enderecoSelecionado.dsBairroClientePagador}" size="30" id="txtBairro" maxlength="40" />
								    </br:brPanelGroup>	
								</br:brPanelGrid>							
							</br:brPanelGroup>
							
							<br:brPanelGroup>	
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
										<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									</br:brPanelGroup>		
									<br:brPanelGroup>			
										<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_municipio}:"/>
									</br:brPanelGroup>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
								    <br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						    		<br:brPanelGroup style="margin-right:20px">
						    			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem != '0' || !solEmissaoComprovantePagtoFavorecidoBean.habilitaCampoEndereco}" value="#{filtroEnderecoEmailBean.enderecoSelecionado.dsMunicipioClientePagador}" size="30" id="txtMunicipio" maxlength="40" />
								    </br:brPanelGroup>	
								</br:brPanelGrid>								
							</br:brPanelGroup>	
							
							<br:brPanelGroup>	
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
										<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									</br:brPanelGroup>		
									<br:brPanelGroup>			
										<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_uf}:"/>
									</br:brPanelGroup>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
								    <br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						    		<br:brPanelGroup>
						    			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem != '0' || !solEmissaoComprovantePagtoFavorecidoBean.habilitaCampoEndereco}" value="#{filtroEnderecoEmailBean.enderecoSelecionado.cdSiglaUfPagador}" size="3" maxlength="2" id="txtUf" />
								    </br:brPanelGroup>	
								</br:brPanelGrid>								
							</br:brPanelGroup>	
							
							<br:brPanelGroup>
								<br:brCommandButton  id="btnConsultarEndereco" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioInfoAdicionais != '0' || solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem != '0'}" styleClass="bto1" style="margin-top:17px;margin-left:5px" value="#{msgs.btn_consultar_endereco}" action="#{filtroEnderecoEmailBean.consultarEnderecoEmail}">				
									<brArq:submitCheckClient/>
								</br:brCommandButton>	
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>
				    	<t:radio for="radioTipoPostagem" index="1" />
				    </br:brPanelGroup>
			    	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
			    		<br:brPanelGroup>
				    		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-top:6px">
								<br:brPanelGroup>			
									<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								</br:brPanelGroup>		
								<br:brPanelGroup>			
									<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_endereco}:"/>
								</br:brPanelGroup>
							</br:brPanelGrid>
							
							<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
							    <br:brPanelGroup>			
								</br:brPanelGroup>	
							</br:brPanelGrid>
							
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					    		<br:brPanelGroup>
							    	<br:brInputText id="txtEnderecoEmail" styleClass="HtmlInputTextBradesco" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioInfoAdicionais != '0' || solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem != '1'}" value="#{filtroEnderecoEmailBean.emailSelecionado.cdEnderecoEletronico}" size="80" maxlength="70" />
								</br:brPanelGroup>
							</br:brPanelGrid>
						</br:brPanelGroup>
						
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">	
							<br:brPanelGroup>
								<br:brCommandButton id="btnConsultarEmail" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioInfoAdicionais != '0' || solEmissaoComprovantePagtoFavorecidoBean.radioTipoPostagem != '1'}" styleClass="bto1" value="#{msgs.btn_consultar_email}" style="margin-top:17px;margin-left:5px" action="#{filtroEnderecoEmailBean.consultarEnderecoEmail}">
									<brArq:submitCheckClient/>
								</br:brCommandButton>	
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGrid>
				</br:brPanelGrid>				
			</br:brPanelGroup>
		</br:brPanelGrid>	
	</br:brPanelGrid>
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="radioAgenciaDepto" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioInfoAdicionais != '1'}" value="#{solEmissaoComprovantePagtoFavorecidoBean.radioAgenciaDepto}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="infoAdicionais" action="#{solEmissaoComprovantePagtoFavorecidoBean.limparAgenciaOperadora}" status="statusAguarde" />			
		</t:selectOneRadio>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			<t:radio for="radioInfoAdicionais" index="1" />
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-left:20px; margin-top:6px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>
				    	<t:radio for="radioAgenciaDepto" index="0" />
				    </br:brPanelGroup>
				    
				    <br:brPanelGroup>
				    	<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
				    	<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoFavorecidoBean.agenciaOperadora}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="2" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>
				    	<t:radio for="radioAgenciaDepto" index="1" />
				    </br:brPanelGroup>
				    
			    	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    		<br:brPanelGroup>
				    		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>			
									<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								</br:brPanelGroup>		
								<br:brPanelGroup>			
									<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_departamento}:"/>
								</br:brPanelGroup>
							</br:brPanelGrid>
							
							<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
							    <br:brPanelGroup>			
								</br:brPanelGroup>	
							</br:brPanelGrid>
							
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					    		<br:brPanelGroup>
							    	<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.radioInfoAdicionais != '1' || solEmissaoComprovantePagtoFavorecidoBean.radioAgenciaDepto != '1'}" value="#{solEmissaoComprovantePagtoFavorecidoBean.departamentoFiltro}" size="10" id="txtDepartamento" maxlength="6"/>
								</br:brPanelGroup>
							</br:brPanelGrid>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>				
			</br:brPanelGroup>
		</br:brPanelGrid>	
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>

	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" border="0" >
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoFavorecidoBean.vlTarifa}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-top:25px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
				
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_desconto_tarifa}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
							
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
							
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
				    	<br:brInputText style="margin-right:3px" styleClass="HtmlInputTextBradesco" disabled="#{solEmissaoComprovantePagtoFavorecidoBean.desabilitaDescontoTarifa}"  onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" value="#{solEmissaoComprovantePagtoFavorecidoBean.percentualDescTarifa}" converter="decimalBrazillianConverter" alt="percentual" onfocus="loadMasks();" size="9" id="txtDescontoTarifa"/>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="%"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	</a4j:outputPanel>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{solEmissaoComprovantePagtoFavorecidoBean.voltarIncluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" style="align:left" value="#{msgs.label_avan�ar}" action="#{solEmissaoComprovantePagtoFavorecidoBean.confirmarInclusao}"
			onclick="javascript:desbloquearTela(); return validaCamposIncluirInfoAdicionaisCliPagador(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.label_destino_entrega}',
					 '#{msgs.label_tipo_postagem}', '#{msgs.label_logradouro}', '#{msgs.label_numero}', '#{msgs.label_cep}' , '#{msgs.label_email}', '#{msgs.label_agencia_departamento}', '#{msgs.label_departamento}', '#{msgs.label_desconto_tarifa}', '#{msgs.label_complemento}', '#{msgs.label_bairro}', '#{msgs.label_municipio}', '#{msgs.label_uf}', '#{msgs.solEmissaoComprovantePagtoClientePag_percentual_maior_cem}');" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>