<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incSolEmissaoComprovantePagtoClientePagFiltroForm" name="incSolEmissaoComprovantePagtoClientePagFiltroForm" >
<a4j:jsFunction name="funcPaginacao" action="#{solEmissaoComprovantePagtoClientePagBean.limparSelecionados}" reRender="btnLimparSelecao, btnAlterarSelecionados, dataTable" onbeforedomupdate="true"/>
	
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoClientePagBean.nrCnpjCpf}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoClientePagBean.dsRazaoSocial}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoClientePagBean.dsEmpresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_numero_contrato}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoClientePagBean.nroContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoClientePagBean.dsContrato}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_situacao}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solEmissaoComprovantePagtoClientePagBean.cdSituacaoContrato}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
    	<br:brPanelGroup>
    		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass ="HtmlOutputTextRespostaBradesco" value="#{msgs.label_periodo_pagto}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	  <a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >	
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup rendered="#{!solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}">
				<app:calendar id="dataInicioSolicitacao" value="#{solEmissaoComprovantePagtoClientePagBean.dtInicioPeriodoPagamento}" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicioSolicitacao');"></app:calendar>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_a}"/>
				<app:calendar id="dataFimSolicitacao" value="#{solEmissaoComprovantePagtoClientePagBean.dtFimPeriodoPagamento}" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFimSolicitacao');"></app:calendar>
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}">
				<app:calendar id="dataInicioSolicitacaoDisable" value="#{solEmissaoComprovantePagtoClientePagBean.dtInicioPeriodoPagamento}" disabled="true"></app:calendar>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_a}"/>
				<app:calendar id="dataFimSolicitacaoDisable" value="#{solEmissaoComprovantePagtoClientePagBean.dtFimPeriodoPagamento}" disabled="true"></app:calendar>
			</br:brPanelGroup>			    
		</br:brPanelGrid>
	  </a4j:outputPanel>
	
	<br:brPanelGrid columns="2" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_de_servico}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="cboTipoServico" value="#{solEmissaoComprovantePagtoClientePagBean.tipoServicoFiltro}" styleClass="HtmlSelectOneMenuBradesco" disabled="#{solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{solEmissaoComprovantePagtoClientePagBean.listaTipoServicoFiltro}" />						
						<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onchange" reRender="cboModalidade" action="#{solEmissaoComprovantePagtoClientePagBean.listarModalidades}"/>						
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_modalidade}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="cboModalidade" value="#{solEmissaoComprovantePagtoClientePagBean.modalidadeFiltro}" styleClass="HtmlSelectOneMenuBradesco"
			    	  disabled="#{solEmissaoComprovantePagtoClientePagBean.tipoServicoFiltro == null || solEmissaoComprovantePagtoClientePagBean.tipoServicoFiltro == 0 || solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{solEmissaoComprovantePagtoClientePagBean.listaModalidadeFiltro}" />												
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>				
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="radioArgumentos" value="#{solEmissaoComprovantePagtoClientePagBean.radioIncluirSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="argumentos" action="#{solEmissaoComprovantePagtoClientePagBean.limparArgumentosIncluir}" status="statusAguarde" />			
		</t:selectOneRadio>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="argumentos" style="width: 100%; text-align: left" ajaxRendered="true" >		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>		
				<t:radio for="radioArgumentos" index="0" />					
			    <br:brPanelGrid columns="1" style="margin-bottom:70px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>	
			</br:brPanelGroup>
			
			<br:brPanelGroup>					
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_numero_pagamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.label_de}"/>
						<br:brInputText styleClass="HtmlInputTextBradesco"   disabled="#{solEmissaoComprovantePagtoClientePagBean.radioIncluirSelecionado != '0' || solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}"  value="#{solEmissaoComprovantePagtoClientePagBean.numeroPagamentoDe}" size="40" maxlength="30" id="txtNumeroPagamento1" />
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_ate}"/>
						<br:brInputText styleClass="HtmlInputTextBradesco"   disabled="#{solEmissaoComprovantePagtoClientePagBean.radioIncluirSelecionado != '0' || solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}"  value="#{solEmissaoComprovantePagtoClientePagBean.numeroPagamentoAte}" size="40" maxlength="30" id="txtNumeroPagamento2" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_valor_pagamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;" value="#{msgs.label_de}"/>
						<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" onkeypress="onlyNum();" disabled="#{solEmissaoComprovantePagtoClientePagBean.radioIncluirSelecionado != '0' || solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}" converter="decimalBrazillianConverter" alt="decimalBr15" onfocus="loadMasks();" value="#{solEmissaoComprovantePagtoClientePagBean.valorPagamentoDe}" maxlength="15" size="27" id="txtValorPagamento1" />
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_ate}"/>
						<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" onkeypress="onlyNum();" disabled="#{solEmissaoComprovantePagtoClientePagBean.radioIncluirSelecionado != '0' || solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}" converter="decimalBrazillianConverter" alt="decimalBr15" onfocus="loadMasks();" value="#{solEmissaoComprovantePagtoClientePagBean.valorPagamentoAte}" maxlength="15" size="27" id="txtValorPagamento2" />
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:top" >
			<t:radio for="radioArgumentos" index="1" />	
						
			<br:brOutputText style="margin-left:5px" styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_favorecido}:" />
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>				

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<t:selectOneRadio id="radioFavorecido" disabled="#{solEmissaoComprovantePagtoClientePagBean.radioIncluirSelecionado != '1' || solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}" value="#{solEmissaoComprovantePagtoClientePagBean.radioIncluirFavorecido}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
						<f:selectItem itemValue="0" itemLabel="" />
						<f:selectItem itemValue="1" itemLabel="" />
						<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="argumentos" action="#{solEmissaoComprovantePagtoClientePagBean.limparArgumentosFavorecido}" status="statusAguarde" />			
					</t:selectOneRadio>
				</br:brPanelGrid>
				
				<br:brPanelGrid style="margin-left:20px" columns="2" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<t:radio for="radioFavorecido" index="0" />
					</br:brPanelGroup>
					
					<br:brPanelGroup>
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
								<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" disabled="#{solEmissaoComprovantePagtoClientePagBean.radioIncluirSelecionado != '1' || solEmissaoComprovantePagtoClientePagBean.radioIncluirFavorecido != '0' || solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}" value="#{solEmissaoComprovantePagtoClientePagBean.cdFavorecido}" size="20" maxlength="15" id="txtCodigoFavorecido" />
							</br:brPanelGroup>	
						</br:brPanelGrid>
					</br:brPanelGroup>
				</br:brPanelGrid>
	
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
					
				<br:brPanelGrid columns="2" style="margin-left:20px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<t:radio for="radioFavorecido" index="1" />
					</br:brPanelGroup>
					
					<br:brPanelGroup>
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
										<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									</br:brPanelGroup>		
									<br:brPanelGroup>			
										<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_inscricao}:"/>
									</br:brPanelGroup>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>
										<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" disabled="#{solEmissaoComprovantePagtoClientePagBean.radioIncluirSelecionado != '1' || solEmissaoComprovantePagtoClientePagBean.radioIncluirFavorecido != '1' || solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}" value="#{solEmissaoComprovantePagtoClientePagBean.inscricaoFavorecido}" maxlength="15" size="20" id="txtInscricaoFavorecido" />
									</br:brPanelGroup>
								</br:brPanelGrid>
							</br:brPanelGroup>
							
							<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
								<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
										<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									</br:brPanelGroup>		
									<br:brPanelGroup>			
										<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo}:"/>
									</br:brPanelGroup>
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>			
									</br:brPanelGroup>	
								</br:brPanelGrid>
								
								<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>
										<br:brSelectOneMenu id="cboTipoFavorecido" value="#{solEmissaoComprovantePagtoClientePagBean.tipoInscricaoFiltro}" styleClass="HtmlSelectOneMenuBradesco"
										  disabled="#{solEmissaoComprovantePagtoClientePagBean.radioIncluirSelecionado != '1' || solEmissaoComprovantePagtoClientePagBean.radioIncluirFavorecido != '1' || solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}" >
											<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
											<f:selectItems value="#{solEmissaoComprovantePagtoClientePagBean.listaTipoInscricaoFiltro}" />												
										</br:brSelectOneMenu>
									</br:brPanelGroup>
								</br:brPanelGrid>
							</br:brPanelGrid>
						</br:brPanelGrid>
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
			</br:brPanelGroup>	
				
		</br:brPanelGrid>
	</a4j:outputPanel>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align:right">	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{solEmissaoComprovantePagtoClientePagBean.limparIncluir}" style="margin-right:5px;">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);" disabled="#{solEmissaoComprovantePagtoClientePagBean.disableArgumentosIncluir}" value="#{msgs.btn_consultar}" action="#{solEmissaoComprovantePagtoClientePagBean.listarIncluirSolEmiComprovante}"
			onclick="desbloquearTela();return validaIncluirFiltro(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}',
			 '#{msgs.label_numero_pagamento}', '#{msgs.label_valor_do_pagamento}', '#{msgs.label_codigo_favorecido_ou_inscricao_favorecido}', '#{msgs.label_codigo_favorecido}',
			 '#{msgs.label_inscricao_favorecido}', '#{msgs.label_tipo_favorecido}', '#{msgs.consultarPagamentosIndividual_ou}', '#{msgs.label_periodo_pagto}', '#{msgs.label_modalidade}', '#{msgs.label_obrigatorio_informar_um_dos_campos}'); ">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
		
			<app:scrollableDataTable id="dataTable" value="#{solEmissaoComprovantePagtoClientePagBean.listaIncluirComprovantes}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  	<t:selectBooleanCheckbox disabled="#{empty solEmissaoComprovantePagtoClientePagBean.listaIncluirComprovantes}" 
					  	 id="checkSelecionarTodos" styleClass="HtmlSelectOneRadioBradesco" value="#{solEmissaoComprovantePagtoClientePagBean.opcaoChecarTodos}" onclick="javascript:selecionarTodos(document.forms[1],this);">
						</t:selectBooleanCheckbox>
				    </f:facet>	
					<t:selectBooleanCheckbox  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
							value="#{result.check}">
						<f:selectItems value="#{solEmissaoComprovantePagtoClientePagBean.listaControleAutorizarPagto}"/>	
						<a4j:support event="onclick" reRender="btnLimpar, btnAvancar,btnLimparSelecionados" action="#{solEmissaoComprovantePagtoClientePagBean.habilitarPanelBotoes}"/>
					</t:selectBooleanCheckbox>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="220px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_tipo_de_servico}" style="width:220; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsResumoProdutoServico}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="220px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_modalidade}" style="width:220; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsOperacaoProdutoServico}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_numero_pagamento}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdControlePagamento}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabCenter" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_data_do_pagamento}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dtCreditoPagamentoFormatada}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_valor_do_pagamento}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.vlEfetivoPagamento}" styleClass="tableFontStyle" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_favorecido}" style="width:250; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.favorecidoFormatado}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="220px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_bco_ag_conta_debito}" style="width:220; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.contaDebitoFormatada}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_situacao}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoOperacaoPagamento}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{solEmissaoComprovantePagtoClientePagBean.pesquisarIncluir}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" onclick="funcPaginacao();"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{solEmissaoComprovantePagtoClientePagBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" disabled="#{empty solEmissaoComprovantePagtoClientePagBean.listaIncluirComprovantes}" value="#{msgs.btn_limpar}" action="#{solEmissaoComprovantePagtoClientePagBean.limparListaIncluir}" style="margin-right:5px;">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnLimparSelecionados" disabled="#{!solEmissaoComprovantePagtoClientePagBean.panelBotoes}" styleClass="bto1" value="#{msgs.label_limpar_selecionados}" action="#{solEmissaoComprovantePagtoClientePagBean.limparSelecionados}">				
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnAvancar" disabled="#{!solEmissaoComprovantePagtoClientePagBean.panelBotoes}" style="margin-left:5px" styleClass="bto1" value="#{msgs.label_avan�ar}" action="#{solEmissaoComprovantePagtoClientePagBean.infAdicionais}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>