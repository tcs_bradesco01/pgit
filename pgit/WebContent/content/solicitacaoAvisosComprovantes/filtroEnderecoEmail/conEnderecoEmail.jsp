<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conEnderecoEmailForm" name="conEnderecoEmailForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<t:selectOneRadio id="radioArgumentosPesquisa"  value="#{filtroEnderecoEmailBean.radioFiltroConsultaEnderecoEmail}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="formulario" action="#{filtroEnderecoEmailBean.limparArgumentosCliente}" status="statusAguarde" />			
			</t:selectOneRadio>
		</br:brPanelGrid>
		
		<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<t:radio for="radioArgumentosPesquisa" index="0" />
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cnpj}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
					    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'conEnderecoEmailForm','conEnderecoEmailForm:txtCnpj','conEnderecoEmailForm:txtFilial');" style="margin-right: 5" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  size="11" maxlength="9" disabled="#{filtroEnderecoEmailBean.radioFiltroConsultaEnderecoEmail != '0'}" styleClass="HtmlInputTextBradesco" value="#{filtroEnderecoEmailBean.entradaCliente.cdCpfCnpj}"/>
					    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'conEnderecoEmailForm','conEnderecoEmailForm:txtFilial','conEnderecoEmailForm:txtControle');" style="margin-right: 5" onkeypress="onlyNum();" size="5" maxlength="4" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  disabled="#{filtroEnderecoEmailBean.radioFiltroConsultaEnderecoEmail != '0'}" styleClass="HtmlInputTextBradesco" value="#{filtroEnderecoEmailBean.entradaCliente.cdFilialCnpj}" />
						<br:brInputText id="txtControle" style="margin-right: 5" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{filtroEnderecoEmailBean.radioFiltroConsultaEnderecoEmail != '0'}" styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{filtroEnderecoEmailBean.entradaCliente.cdControleCnpj}" />
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<t:radio for="radioArgumentosPesquisa" index="1" />
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				   <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">					
					    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{filtroEnderecoEmailBean.radioFiltroConsultaEnderecoEmail != '1'}" value="#{filtroEnderecoEmailBean.entradaCliente.cdCpf}" size="11" maxlength="9" id="txtCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(9,'conEnderecoEmailForm','conEnderecoEmailForm:txtCpf','conEnderecoEmailForm:txtControleCpf');" />
					    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{filtroEnderecoEmailBean.radioFiltroConsultaEnderecoEmail != '1'}" value="#{filtroEnderecoEmailBean.entradaCliente.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />				   
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<t:radio for="radioArgumentosPesquisa" index="2" />
				</br:brPanelGroup>		
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" style="margin-top:6px">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nome_razao_social}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
					    <br:brInputText id="txtNomeRazaoSocial" style="margin-right:5" size="71" maxlength="70" disabled="#{filtroEnderecoEmailBean.radioFiltroConsultaEnderecoEmail != '2'}" styleClass="HtmlInputTextBradesco" value="#{filtroEnderecoEmailBean.entradaCliente.dsNomeRazaoSocial}"/>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <f:verbatim><hr class="lin"></f:verbatim>	
		    
			<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<br:brCommandButton id="btnConsultar" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);" value="#{msgs.btn_consultar}" action="#{filtroEnderecoEmailBean.consultarClientes}"
						onclick="javascript:desbloquearTela(); return validaConsultaEnderecoEmail(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.consultarPagamentosIndividual_cnpj}','#{msgs.consultarPagamentosIndividual_cpf}','#{msgs.label_cnpj_incorreto}','#{msgs.label_cpf_incorreto}','#{msgs.label_nome_razao}');"
						disabled="#{empty filtroEnderecoEmailBean.radioFiltroConsultaEnderecoEmail}"> 				 
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>		
		</a4j:outputPanel>				   
			
		<f:verbatim><br></f:verbatim>
		
		<!-- CONSULTA POR EMAIL -->
		<br:brPanelGrid rendered="#{filtroEnderecoEmailBean.flagEnderecoEmail == 1}" columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 
				<app:scrollableDataTable id="dataTableEmail" value="#{filtroEnderecoEmailBean.listaGridEmail}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio id="sorListaEmail" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{filtroEnderecoEmailBean.itemSelecionadoEnderecoEmail}">
							<f:selectItems value="#{filtroEnderecoEmailBean.listaControleEnderecoEmail}"/>
							<a4j:support event="onclick" reRender="panelBotoes"/>
						</t:selectOneRadio>
				    	<t:radio for="sorListaEmail" index="#{parametroKey}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="700px">
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_email}" style="width:700px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdEnderecoEletronico }" styleClass="tableFontStyle"/>
					</app:scrollableColumn>	
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<!-- CONSULTA POR ENDERE�O -->
		<br:brPanelGrid rendered="#{filtroEnderecoEmailBean.flagEnderecoEmail == 2}" columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll"> 
				<app:scrollableDataTable id="dataTableEndereco" value="#{filtroEnderecoEmailBean.listaGridEndereco}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio id="sorListaEndereco" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{filtroEnderecoEmailBean.itemSelecionadoEnderecoEmail}">
							<f:selectItems value="#{filtroEnderecoEmailBean.listaControleEnderecoEmail}"/>
							<a4j:support event="onclick" reRender="panelBotoes"/>
						</t:selectOneRadio>
				    	<t:radio for="sorListaEndereco" index="#{parametroKey}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="300px">
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_logradouro}" style="width:300px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsLogradouroPagador}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="100px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_numero}" style="width:100px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsNumeroLogradouroPagador}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_complemento}" style="width:300px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsComplementoLogradouroPagador}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_bairro}" style="width:200px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsBairroClientePagador}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_municipio}" style="width:200px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsMunicipioClientePagador}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="50px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_uf}" style="width:50px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdSiglaUfPagador}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabRight" width="100px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.label_cep}" style="width:100px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cepFormatado}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<%-- CONSULTA POR EMAIL --%>
	 	<br:brPanelGrid rendered="#{filtroEnderecoEmailBean.flagEnderecoEmail == 1}" columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScrollerEmail" for="dataTableEmail" actionListener="#{filtroEnderecoEmailBean.pesquisarEmail}">
					<f:facet name="first">
				    	<brArq:pdcCommandButton id="primeiraEmail" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  	</f:facet>
				  	<f:facet name="fastrewind">
				    	<brArq:pdcCommandButton id="retrocessoRapidoEmail"styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  	</f:facet>
				  	<f:facet name="previous">
				    	<brArq:pdcCommandButton id="anteriorEmail" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  	</f:facet>
				  	<f:facet name="next">
						<brArq:pdcCommandButton id="proximaEmail" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  	</f:facet>
				  	<f:facet name="fastforward">
				    	<brArq:pdcCommandButton id="avancoRapidoEmail" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  	</f:facet>
				  	<f:facet name="last">
				    	<brArq:pdcCommandButton id="ultimaEmail" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  	</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<%-- CONSULTA POR ENDERE�O --%>
	 	<br:brPanelGrid rendered="#{filtroEnderecoEmailBean.flagEnderecoEmail == 2}" columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScrollerEndereco" for="dataTableEndereco" actionListener="#{filtroEnderecoEmailBean.pesquisarEndereco}">
					<f:facet name="first">
				    	<brArq:pdcCommandButton id="primeiraEndereco" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  	</f:facet>
				  	<f:facet name="fastrewind">
				    	<brArq:pdcCommandButton id="retrocessoRapidoEndereco"styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  	</f:facet>
				  	<f:facet name="previous">
				    	<brArq:pdcCommandButton id="anteriorEndereco" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  	</f:facet>
				  	<f:facet name="next">
						<brArq:pdcCommandButton id="proximaEndereco" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  	</f:facet>
				  	<f:facet name="fastforward">
				    	<brArq:pdcCommandButton id="avancoRapidoEndereco" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  	</f:facet>
				  	<f:facet name="last">
				    	<brArq:pdcCommandButton id="ultimaEndereco" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  	</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<a4j:outputPanel id="panelBotoes" style=" text-align: right" ajaxRendered="true">	
		 	<br:brPanelGrid columns="2" style="text-align:right" cellpadding="0" cellspacing="0" border="0">	
				<br:brPanelGroup>
					<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{filtroEnderecoEmailBean.voltar}" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
				<br:brPanelGroup style="margin-left:580px">
					<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.btn_limpar}" style="margin-right:5px" action="#{filtroEnderecoEmailBean.limparCampos}" disabled="#{empty filtroEnderecoEmailBean.listaGridEndereco && empty filtroEnderecoEmailBean.listaGridEmail}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<%-- CONSULTA POR E-MAIL --%>
					<br:brCommandButton rendered="#{filtroEnderecoEmailBean.flagEnderecoEmail == 1}" id="btnSelecionarEmail" styleClass="bto1" value="#{msgs.btn_selecionar}" action="#{filtroEnderecoEmailBean.selecionarEmail}" disabled="#{empty filtroEnderecoEmailBean.itemSelecionadoEnderecoEmail}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
										
					<%-- CONSULTA POR ENDERE�O --%>
					<br:brCommandButton rendered="#{filtroEnderecoEmailBean.flagEnderecoEmail == 2}" id="btnSelecionarEndereco" styleClass="bto1" value="#{msgs.btn_selecionar}" action="#{filtroEnderecoEmailBean.selecionarEndereco}" disabled="#{empty filtroEnderecoEmailBean.itemSelecionadoEnderecoEmail}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>
			
		<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>		
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>