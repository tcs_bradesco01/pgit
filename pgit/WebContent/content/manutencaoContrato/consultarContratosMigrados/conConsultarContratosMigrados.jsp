<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conContratosMigradosForm" name="conContratosMigradosForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <a4j:region id="regionFiltro">
		<t:selectOneRadio id="rdoPesquisa" value="#{conContratosMigradosBean.radioPesquisa}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
			<f:selectItem itemValue="0" itemLabel="#{msgs.label_pesquisar_dados_legado}" />
			<f:selectItem itemValue="1" itemLabel="#{msgs.label_pesquisar_contrato_pgit}" />
			<a4j:support event="onclick" reRender="painelDadosLegado, painelContratoPgit, btnLimparTela" action="#{conContratosMigradosBean.limpar}" />
		</t:selectOneRadio>
	</a4j:region>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<t:radio for="rdoPesquisa" index="0" />			
		
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		<a4j:outputPanel id="painelDadosLegado" style="width: 100%; text-align: left" ajaxRendered="true">	
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_c_custo_origem}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				   		<br:brSelectOneMenu id="cboCustoOrigem" disabled="#{conContratosMigradosBean.radioPesquisa != '0'}" value="#{conContratosMigradosBean.cboCustoOrigem}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{conContratosMigradosBean.listaCboCustoOrigem}" />
						</br:brSelectOneMenu>
				   </br:brPanelGrid>
			   </br:brPanelGroup>
			   
			   <br:brPanelGroup style="margin-left:20px">			
			   </br:brPanelGroup>
			   
			   <br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_situacao}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
						<br:brSelectOneMenu id="cboSituacao" disabled="#{conContratosMigradosBean.radioPesquisa != '0'}" value="#{conContratosMigradosBean.cboSituacao}" styleClass="HtmlSelectOneMenuBradesco" style="width: 200">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{conContratosMigradosBean.listaSituacao}" />
						</br:brSelectOneMenu>
				   </br:brPanelGrid>
				</br:brPanelGroup>
				
			 </br:brPanelGrid>
			 
			 
			 
			 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  style="margin-top:6px">
			   <br:brPanelGroup>	
			   		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_migracao}:"/>	
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value=" "/>	
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">										
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
							    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
									<br:brPanelGroup>
										<br:brPanelGroup rendered="#{conContratosMigradosBean.radioPesquisa == '0'}">
											<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicioMigracao');" id="dataInicioMigracao" value="#{conContratosMigradosBean.dataMigracaoDe}" >
											</app:calendar>
										</br:brPanelGroup>
										<br:brPanelGroup rendered="#{conContratosMigradosBean.radioPesquisa != '0'}">
											<app:calendar id="dataInicialPagamentoDes" value="#{conContratosMigradosBean.dataMigracaoDe}" disabled="true" >
											</app:calendar>
										</br:brPanelGroup>
										<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
										<br:brPanelGroup rendered="#{conContratosMigradosBean.radioPesquisa == '0'}">
											<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFimMigracao');" id="dataFimMigracao" value="#{conContratosMigradosBean.dataMigracaoA}" >
							 				</app:calendar>	
										</br:brPanelGroup>	
										<br:brPanelGroup rendered="#{conContratosMigradosBean.radioPesquisa != '0'}">
											<app:calendar id="dataFinalPagamentoDes" value="#{conContratosMigradosBean.dataMigracaoA}" disabled="true">
							 				</app:calendar>	
										</br:brPanelGroup>	
									</br:brPanelGroup>			    
								</br:brPanelGrid>
						</br:brPanelGrid>
					</a4j:outputPanel>										
			   </br:brPanelGroup>
			 </br:brPanelGrid>
		   
		   <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="corpoCpfCnpj" disabled="#{conContratosMigradosBean.radioPesquisa != '0'}" style="margin-right:5px" onkeypress="onlyNum();" size="11" maxlength="9" styleClass="HtmlInputTextBradesco" value="#{conContratosMigradosBean.corpoCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			    <br:brInputText id="filiaCnpj"  disabled="#{conContratosMigradosBean.radioPesquisa != '0'}" style="margin-right:5px" onkeypress="onlyNum();" size="5" maxlength="4" styleClass="HtmlInputTextBradesco" value="#{conContratosMigradosBean.filiaCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				<br:brInputText id="digitoCnpj" disabled="#{conContratosMigradosBean.radioPesquisa != '0'}" onkeypress="onlyNum();" size="3" maxlength="2" styleClass="HtmlInputTextBradesco" value="#{conContratosMigradosBean.digitoCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
		   </br:brPanelGrid>
			
		   <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"cellpadding="0" cellspacing="0" >
			   <br:brPanelGroup>			
			   </br:brPanelGroup>	
		   </br:brPanelGrid>
		   
		   <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_da_empresa_perfil}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="codigoEmpPerfil" disabled="#{conContratosMigradosBean.radioPesquisa != '0'}" maxlength="9" size="14" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{conContratosMigradosBean.codEmpresaPerfil}"
				    onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
		   </br:brPanelGrid>
			
		   <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			   <br:brPanelGroup>			
			   </br:brPanelGroup>	
		   </br:brPanelGrid>
		   
		   <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
	   			<br:brPanelGroup>
		   			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
							
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agencia}:"/>	
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px">	
							<br:brPanelGroup >			
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
							<br:brPanelGroup>
								 <br:brInputText id="agencia" size="8" maxlength="5" onkeypress="onlyNum();" disabled="#{conContratosMigradosBean.radioPesquisa != '0'}" styleClass="HtmlInputTextBradesco" value="#{conContratosMigradosBean.agencia}"
									 onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
							
					</br:brPanelGrid>
				</br:brPanelGroup>
				
				
				<br:brPanelGroup style="margin-left:20px">	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_conta}:"/>	
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px">	
							<br:brPanelGroup >			
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
							<br:brPanelGroup>
								 <br:brInputText id="conta" maxlength="7" size="11" onkeypress="onlyNum();" disabled="#{conContratosMigradosBean.radioPesquisa != '0'}" styleClass="HtmlInputTextBradesco" value="#{conContratosMigradosBean.conta}"
									 onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>
	   </a4j:outputPanel>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
	   <br:brPanelGroup>			
	   </br:brPanelGroup>	
   </br:brPanelGrid>
   
   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<t:radio for="rdoPesquisa" index="1" />			
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<a4j:outputPanel id="painelContratoPgit" style="width: 100%; text-align: left" ajaxRendered="true">	
			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
				   		<br:brSelectOneMenu id="cboEmpresaGestoraCont" disabled="#{conContratosMigradosBean.radioPesquisa != '1'}"  value="#{conContratosMigradosBean.cboEmpresaGestoraCont}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
							<f:selectItems  value="#{conContratosMigradosBean.listaEmpresaGestora}" />	
						</br:brSelectOneMenu>
				   </br:brPanelGrid>
			   </br:brPanelGroup>
			   
			   <br:brPanelGroup style="margin-left:20px">			
			   </br:brPanelGroup>
			   
			   <br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_contrato}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
				   		<br:brSelectOneMenu id="cboTpContrato" disabled="#{conContratosMigradosBean.radioPesquisa != '1'}" value="#{conContratosMigradosBean.cboTpContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
							<f:selectItems  value="#{conContratosMigradosBean.listaTipoContrato}" />
						</br:brSelectOneMenu>
				   </br:brPanelGrid>
			   </br:brPanelGroup>
			   
			    <br:brPanelGroup style="margin-left:20px">			
			   </br:brPanelGroup>
			   
			   <br:brPanelGroup>	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						   <br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_numero}"/>	
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>
								 <br:brInputText id="numero" disabled="#{conContratosMigradosBean.radioPesquisa != '1'}" onkeypress="onlyNum();" size="14" maxlength="10" styleClass="HtmlInputTextBradesco" value="#{conContratosMigradosBean.numero}"
								 	onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGrid>
				</br:brPanelGroup>
		   </br:brPanelGrid>
		</a4j:outputPanel>	   
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="750px" cellpadding="0" cellspacing="0" border="0">	
		<br:brPanelGroup style="float:right">
			<br:brCommandButton id="btnLimparTela" disabled="#{conContratosMigradosBean.radioPesquisa != '1' && conContratosMigradosBean.radioPesquisa != '0'}" styleClass="bto1" value="#{msgs.btn_limpar_dados}" style="margin-right:5px" action="#{conContratosMigradosBean.limparPaginaConsulta}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{conContratosMigradosBean.consultar}" 
			onclick="desbloquearTela(); return validaCamposFiltro(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.label_c_custo_origem}', '#{msgs.label_empresa_gestora_contrato}',
						'#{msgs.label_tipo_contrato}', '#{msgs.label_numero}', '#{msgs.label_pesquisar_dados_legado}', '#{msgs.label_pesquisar_contrato_pgit}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

			<app:scrollableDataTable id="dataTable" value="#{conContratosMigradosBean.listaContratosMigrados}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{conContratosMigradosBean.itemSelecionado}">
						<f:selectItems value="#{conContratosMigradosBean.listaControle}"/>
						<a4j:support event="onclick" reRender="btnDetalhar, btnLimparGrid, btnReverter, btnDesfazerReversao" action="#{conContratosMigradosBean.habilitaReverterDesfazer}" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
			 
				<app:scrollableColumn styleClass="colTabLeft" width="35px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_origem}" style="width:35px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.centroCustoOrigem}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="115px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_situacao}" style="width:115px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoContrato}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabCenter" width="100px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_data_migracao}"  style="width:100px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dtMigracao}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabRight" width="115px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_club_representante}" style="width:115px; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdClub}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="125px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_cpf_cnpj}"  style="width:125px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cpfCnpjFormatado}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="315px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_nome_razao_social}" style="width:315px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsNome}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabRight" width="120px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_perfil}"  style="width:120px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdPerfil}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_empresa_gestora}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsPessoaJuridicaContrato}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_tipo_contrato}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoContratoNegocio}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="100px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_numero}" style="width:100px; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.nrSequenciaContratoNegocio}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="45px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_agencia}" style="width:45px; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsAgencia}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="38px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_razao}" style="width:38px; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsRazao}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="55px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_conta}" style="width:55px; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.contaFormatada}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabRight" width="50px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.label_cod_lcto}" style="width:50px; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsLancamento}" />
				</app:scrollableColumn>				

			</app:scrollableDataTable>
			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{conContratosMigradosBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="750px" cellpadding="0" cellspacing="0" border="0">	
		<br:brPanelGroup style="float:right">
			<br:brCommandButton id="btnLimparGrid" styleClass="bto1" value="#{msgs.btn_limpar}" style="margin-right:5px" action="#{conContratosMigradosBean.limparGrid}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnDetalhar" styleClass="bto1"  value="#{msgs.btn_detalhar}" disabled="#{empty  conContratosMigradosBean.itemSelecionado}" style="margin-right:5px" action="#{conContratosMigradosBean.detalhar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnReverter" styleClass="bto1"  value="#{msgs.btn_reverter}" disabled="#{!conContratosMigradosBean.habilitaReverter}" style="margin-right:5px" action="#{conContratosMigradosBean.reverter}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnDesfazerReversao" styleClass="bto1"  value="#{msgs.btn_desfazer_reversao}" disabled="#{!conContratosMigradosBean.habilitaDesfazerReversao}" action="#{conContratosMigradosBean.desfazerReversao}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>

</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>
