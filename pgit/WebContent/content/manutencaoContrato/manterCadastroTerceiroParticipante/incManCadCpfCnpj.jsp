<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="incManCadCpfCnpjForm" name="incManCadCpfCnpjForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
<h:inputHidden id="dataHoje" value="#{manCadCpfCnpjTerParticipanteBean.dataHoje}" converter="dateBrazillianConverter"/>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_cliente_representante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_cpfcnpj}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manCadCpfCnpjTerParticipanteBean.cpfCnpjRepresentante}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_nomerazaosocial}:"/>						
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manCadCpfCnpjTerParticipanteBean.nomeRazaoRepresentante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_grupo_economico}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manCadCpfCnpjTerParticipanteBean.grupEconRepresentante}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_atividade_economica}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manCadCpfCnpjTerParticipanteBean.ativEconRepresentante}"/>
		</br:brPanelGroup>			
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_segmento}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manCadCpfCnpjTerParticipanteBean.segRepresentante}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_sub_segmento}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manCadCpfCnpjTerParticipanteBean.subSegRepresentante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manCadCpfCnpjTerParticipanteBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manCadCpfCnpjTerParticipanteBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_cliente_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manCadCpfCnpjTerParticipanteBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_cliente_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manCadCpfCnpjTerParticipanteBean.nomeRazaoParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

 	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_empresa}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manCadCpfCnpjTerParticipanteBean.empresaContrato}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manCadCpfCnpjTerParticipanteBean.tipoContrato}"/>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_numero}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manCadCpfCnpjTerParticipanteBean.numeroContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_descricao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manCadCpfCnpjTerParticipanteBean.descricaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_situacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manCadCpfCnpjTerParticipanteBean.situacaoContrato}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_motivo}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manCadCpfCnpjTerParticipanteBean.motivoContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo_participacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manCadCpfCnpjTerParticipanteBean.participacaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
<f:verbatim><hr class="lin"> </f:verbatim>

  <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cpf_cnpj_terceiro}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:region id="regionFiltro">
		<t:selectOneRadio id="rdoCpfCnpjIncluir" value="#{manCadCpfCnpjTerParticipanteBean.itemFiltroIncluir}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<a4j:support event="onclick" reRender="panelCnpj, panelCpf" ajaxSingle="true"  action="#{manCadCpfCnpjTerParticipanteBean.limparRdoIncluir}"/>
		</t:selectOneRadio>
	</a4j:region>
	
	
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="rdoCpfCnpjIncluir" index="0" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{manCadCpfCnpjTerParticipanteBean.itemFiltroIncluir != '1'}" value="#{manCadCpfCnpjTerParticipanteBean.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'incManCadCpfCnpjForm','incManCadCpfCnpjForm:txtCpf','incManCadCpfCnpjForm:txtControleCpf');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{manCadCpfCnpjTerParticipanteBean.itemFiltroIncluir != '1'}" value="#{manCadCpfCnpjTerParticipanteBean.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<t:radio for="rdoCpfCnpjIncluir" index="1" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'incManCadCpfCnpjForm','incManCadCpfCnpjForm:txtCnpj','incManCadCpfCnpjForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{manCadCpfCnpjTerParticipanteBean.itemFiltroIncluir != '2'}" styleClass="HtmlInputTextBradesco" value="#{manCadCpfCnpjTerParticipanteBean.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'incManCadCpfCnpjForm','incManCadCpfCnpjForm:txtFilial','incManCadCpfCnpjForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{manCadCpfCnpjTerParticipanteBean.itemFiltroIncluir != '2'}" styleClass="HtmlInputTextBradesco" value="#{manCadCpfCnpjTerParticipanteBean.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{manCadCpfCnpjTerParticipanteBean.itemFiltroIncluir != '2'}" styleClass="HtmlInputTextBradesco" value="#{manCadCpfCnpjTerParticipanteBean.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_inicio_rastreamento}:"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="calendarioRastreamento" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brPanelGroup >
						<app:calendar id="dataInicialRastreamento" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialRastreamento');" value="#{manCadCpfCnpjTerParticipanteBean.dataInicioRastreamento}" >
						</app:calendar>
					</br:brPanelGroup>
				</br:brPanelGroup>			    
			</br:brPanelGrid>
	</a4j:outputPanel>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agendar_titulos_rastreados}:"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<t:selectOneRadio id="radioAgendarTitRastreados"  styleClass="HtmlSelectOneRadioBradesco" value="#{manCadCpfCnpjTerParticipanteBean.rdoAgendarTitRastreados}" forceId="true" forceIdIndex="false" >
				<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
	            <f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	    	</t:selectOneRadio>
	    </br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_rastrear_notas_fiscais}:"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<t:selectOneRadio id="radioRastrearNotasFiscais" styleClass="HtmlSelectOneRadioBradesco" value="#{manCadCpfCnpjTerParticipanteBean.rdoRastrearNotasFiscais}" forceId="true" forceIdIndex="false" >
				<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
	            <f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	    	</t:selectOneRadio>
	    </br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_bloquear_emissao_papeleta}:"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<t:selectOneRadio id="radioBloquearEmissaoPapeleta" styleClass="HtmlSelectOneRadioBradesco" value="#{manCadCpfCnpjTerParticipanteBean.rdoBloqEmissaoPapeleta}" forceId="true" forceIdIndex="false" >
				<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
	            <f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	            <a4j:support event="onclick" reRender="calendarioInicioBloqEmPap" action="#{manCadCpfCnpjTerParticipanteBean.novaData}"  />
	    	</t:selectOneRadio>
	    </br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_inicio_bloqueio_emissao_papeleta}:" />
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="calendarioInicioBloqEmPap" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGroup rendered="#{manCadCpfCnpjTerParticipanteBean.rdoBloqEmissaoPapeleta == 1}" >
					<app:calendar id="dataInicioBloqEmissaoPapeleta" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicioBloqEmissaoPapeleta');" value="#{manCadCpfCnpjTerParticipanteBean.dataInicioBloqEmissaoPapeleta}" >
					</app:calendar>
				</br:brPanelGroup>
				<br:brPanelGroup rendered="#{!(manCadCpfCnpjTerParticipanteBean.rdoBloqEmissaoPapeleta == 1)}" >
					<app:calendar id="dataInicialPagamentoDes" value="#{manCadCpfCnpjTerParticipanteBean.dataInicioBloqEmissaoPapeleta}" disabled="true" >
					</app:calendar>
				</br:brPanelGroup>
			</br:brPanelGroup>			    
		</br:brPanelGrid>	
	</a4j:outputPanel>	

<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_voltar}" action="#{manCadCpfCnpjTerParticipanteBean.voltarTelaTerceiro}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>	
		<br:brPanelGrid columns="1" style="width:400px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:200px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.btn_avancar}" action="#{manCadCpfCnpjTerParticipanteBean.avancarIncluir}"  onclick="javascript: return validaCamposInclusao(document.forms[1], '#{msgs.label_ocampo}','#{msgs.label_necessario}', '#{msgs.label_cnpj}',
								 '#{msgs.label_cpf}', '#{msgs.label_data_inicio_rastreamento}' , '#{msgs.label_agendar_titulos_rastreados}' , '#{msgs.label_rastrear_notas_fiscais}' , '#{msgs.label_bloquear_emissao_papeleta}' , '#{msgs.label_data_inicio_bloqueio_emissao_papeleta}');" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	