<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinaturaForm" name="confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinaturaForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">


	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.formAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_cpf_cnpj}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.cpfCnpjClienteMaster}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_nome_razao_social}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.nomeRazaoSocialClienteMaster}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_grupo_economico}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.grupoEconomico}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_atividade_economica}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.atividadeEconomica}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.segmento}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_sub_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.subSegmento}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.dsAgenciaGestora}"/>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
	</br:brPanelGrid>

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.cpfCnpj}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.nomeRazaoSocial}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_empresa}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.empresa}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_tipo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.tipo}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_numero}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.numero}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.formAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_situacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.situacao}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_motivo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.motivo}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.formAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.participacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_aditivo}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_numero_aditivo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.numeroAditivo}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_situacao_aditivo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.situacaoAditivo}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_data_hora_cad_aditivo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.dataHoraCadastramentoAditivo}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_motivo_da_ativacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{ativarFormalizacaoManutencaoContratoBean.motivoAtivacao}"/>	
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_voltar}" action="#{ativarFormalizacaoManutencaoContratoBean.voltarConfirmar}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="text-align:right; margin-left:535px">
			<br:brCommandButton id="btnAvancar"
			onclick="javascript: if (!confirm('Confirma ativa��o?')) { desbloquearTela(); return false; }"
			 styleClass="bto1" value="#{msgs.confirmAtivarFormalizacaoAlteracaoContratoPendenteAssinatura_confirmar}" action="#{ativarFormalizacaoManutencaoContratoBean.confirmarFormalizacao}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>	

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
