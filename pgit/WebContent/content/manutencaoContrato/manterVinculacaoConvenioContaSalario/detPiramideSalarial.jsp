<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="detPiramideSalarialForm" name="detPiramideSalarialForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.cpfCnpjRepresentante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.nomeRazaoRepresentante}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.grupEconRepresentante}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.ativEconRepresentante}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.segRepresentante}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.subSegRepresentante}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>				

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.nomeRazaoParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_empresaGestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.empresaContrato}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_tipoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.tipoContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.numeroContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.descricaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.situacaoContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.motivoContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.participacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.dados_empresa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel value="#{msgs.valorFolhaPagamento}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.vlPagamentoMes}"
				styleClass="HtmlOutputTextRespostaBradesco"
				converter="currencyDecimalConverter" />
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel value="#{msgs.qtdeFuncionarios}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.qtdFuncionarioFlPgto}"
				styleClass="HtmlOutputTextRespostaBradesco"
				converter="inteiroMilharConverter" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel value="#{msgs.mediaSalarial}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.vlMediaSalarialMes}"
				styleClass="HtmlOutputTextRespostaBradesco"
				converter="currencyDecimalConverter" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel value="#{msgs.mesAnoCorrespondente}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.mesAnoCorrespondente}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputLabel value="#{msgs.pagamento}: "
					styleClass="HtmlOutputLabelBradesco" />
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialQzena == 1 && manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialMes != 1}">
				<br:brOutputText value="" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialMes == 1 && manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialQzena != 1}">
				<br:brOutputText value="" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialQzena == 1 && manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialMes == 1}">
				<br:brOutputText value="" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialQzena == 1 && manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialMes != 1}">
				<br:brOutputText value="Quinzenal" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialMes == 1 && manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialQzena != 1}">
				<br:brOutputText value="Mensal" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialQzena == 1 && manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalarialMes == 1}">
				<br:brOutputText value="Quinzenal e Mensal" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel value="#{msgs.aberturaContaSalario}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsLocCtaSalarial}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="pertenceGrupoEconimico"
				value="#{msgs.pertence_grupo_economico} "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="pertenceGrupoEconimico"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.indicadorGrupoEconomico}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
		<br:brPanelGroup style="margin-left:32px;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel id="codigoGrupo"
				value="#{msgs.codigo_grupo}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.codigoGrupoEconomico}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="distanciaAgenciaEmpresa"
				value="#{msgs.distancia_agencia_empresa}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="distanciaAgenciaEmpresa"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.distanciaAgEmpresa}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="endereco" value="#{msgs.endereco}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="endereco"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsLogradouroEmprQuest}"
				 styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
		<br:brPanelGroup style="margin-left:13px;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="bairro" value="#{msgs.bairro}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="bairro"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsBairroEmprQuest}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup style="margin-right:25px;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="cep" value="#{msgs.cep}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="cep"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cepEmpresa}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="uf" value="#{msgs.uf}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="uf"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsUfEmprQuest}"
				styleClass="HtmlOutputTextRespostaBradesco"
				style="margin-right:25px;" />
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="municipio" value="#{msgs.municipio}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="municipio"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsMuncEmpreQuest}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="ramoAtividade" value="#{msgs.ramo_atividade}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="ramoAtividade"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsRamoAtividadeQuest}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="telefoneEmpresa"
				value="#{msgs.telefone_empresa}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="telefoneEmpresa"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.telefoneEmpresa}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="siteEmpresa"
				value="#{msgs.site_empresa}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="siteEmpresa"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsSiteEmprQuest}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="gerenteRelacionamento"
				value="#{msgs.gerente_relacionamento}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="gerenteRelacionamento"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsGerRelacionamentoQuest}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="telefoneContato"
				value="#{msgs.telefone_contato}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="telefoneContato"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.telefoneContato}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="segmentoAtendeEmpresa"
				value="#{msgs.segmento_atende_empresa}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="segmentoAtendeEmpresa"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsAbrevSegmentoEmpr}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputLabelBold for="naoPossui" value="#{msgs.banco_atual_folha_percentual_participacao}: " />
			<br:brOutputText id="naoPossui"
				value="#{msgs.nao_possui}"
				styleClass="HtmlOutputTextRespostaBradesco"
				rendered="#{empty manterVinculacaoConvenioContaSalarioBean.dadosPiramide.ocorrencias5}" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup style="width:450px">
		<app:scrollableDataTable id="participacoesFolhaPagamento"
			value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.ocorrencias5}"
			var="bancoPercentual"
			rowIndexVar="rowIndex"
			rowClasses="tabela_celula_normal, tabela_celula_destaque">
			<app:scrollableColumn width="55">
				<f:facet name="header">
					<br:brOutputText value="" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{msgs.banco} #{rowIndex + 1}" />
			</app:scrollableColumn>
			<app:scrollableColumn width="300">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.banco}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{bancoPercentual.dsBancoFlQuest}" />
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.simbolo_percentual}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brPanelGroup styleClass="textRightDatatable">
					<br:brOutputText value="#{bancoPercentual.pctBancoFlQuest}" converter="decimalBrazillianConverter" />
					<br:brOutputText value="#{msgs.simbolo_percentual}" />
				</br:brPanelGroup>
			</app:scrollableColumn>
		</app:scrollableDataTable>
	</br:brPanelGroup>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="formaPagamento"
				value="#{msgs.forma_pagamento_funcionarios}" styleClass="HtmlOutputTextRotuloBradesco" />
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalCredt == 1}">
				<br:brOutputText value="" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalCreque == 1}">
				<br:brOutputText value="" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalDinheiro == 1}">
				<br:brOutputText value="" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsPagamentoSalDivers != null}">
				<br:brOutputText value="" style="margin-left:5px" styleClass="HtmlOutputTextRespostaBradesco" />
			</br:brPanelGroup>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalCredt == 1}">
				<br:brOutputText value="Cr�dito em conta" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalCreque == 1}">
				<br:brOutputText value="Cheque" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.cdPagamentoSalDinheiro == 1}">
				<br:brOutputText value="Dinheiro" styleClass="HtmlOutputTextRespostaBradesco" style="margin-left:5px"/>
				<f:verbatim><BR></f:verbatim>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsPagamentoSalDivers != null}">
				<br:brOutputText value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsPagamentoSalDivers}" style="margin-left:5px" styleClass="HtmlOutputTextRespostaBradesco" />
			</br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid cellpadding="0" cellspacing="0" width="100%">
		<br:brOutputLabelBold value="#{msgs.piramide_salarial_title}:" />

		<app:scrollableDataTable id="faixasRenda"
			value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.ocorrencias1}"
			var="faixaRenda"
			rowIndexVar="index"
			rowClasses="tabela_celula_normal, tabela_celula_destaque"
			width="100%">
			<app:scrollableColumn width="300px">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.faixa_renda}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{faixaRenda.faixa}" />
				<f:facet name="footer">
					<br:brOutputTextBold value="#{msgs.total}" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func_filial} 1" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{faixaRenda.qtdFuncionarioFilial01}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncionarioFilial01}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func_filial} 2" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{faixaRenda.qtdFuncionarioFilial02}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncionarioFilial02}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func_filial} 3" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{faixaRenda.qtdFuncionarioFilial03}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncionarioFilial03}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func_filial} 4" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{faixaRenda.qtdFuncionarioFilial04}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncionarioFilial04}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func_filial} 5" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{faixaRenda.qtdFuncionarioFilial05}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncionarioFilial05}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func_filial} 6" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{faixaRenda.qtdFuncionarioFilial06}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncionarioFilial06}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func_filial} 7" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{faixaRenda.qtdFuncionarioFilial07}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncionarioFilial07}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func_filial} 8" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{faixaRenda.qtdFuncionarioFilial08}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncionarioFilial08}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func_filial} 9" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{faixaRenda.qtdFuncionarioFilial09}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncionarioFilial09}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func_filial} 10" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{faixaRenda.qtdFuncionarioFilial10}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncionarioFilial10}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
		</app:scrollableDataTable>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="taxaRenovacaoAnual"
				value="#{msgs.rotatividade_funcionarios_anual_empresa}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="taxaRenovacaoAnual"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.porcentagemAdmissaoDemissao}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid id="painelRotatividade" columns="5" cellpadding="0" cellspacing="0">
		<br:brOutputLabelBold value="#{msgs.piramide_tempo_casa}: " />
		<br:brPanelGroup style="width: 10px;" />
		<br:brOutputLabelBold value="#{msgs.piramide_funcionarios_desligados}: " />
		<br:brPanelGroup style="width: 10px;" />
		<br:brPanelGroup />

		<app:scrollableDataTable id="rotatividadesTempoCasa"
			value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.ocorrencias2}"
			var="rotatividade" rows="5"
			rowIndexVar="index"
			rowClasses="tabela_celula_normal, tabela_celula_destaque"
			width="300px">
			<app:scrollableColumn>
				<f:facet name="header">
					<br:brOutputText value="#{msgs.faixa}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{rotatividade.dsFaixaRenovQuest}" />
				<f:facet name="footer">
					<br:brOutputTextBold value="#{msgs.total}" style="width:100%;text-align:left;" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{rotatividade.qtdFuncCasaQuest}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncCasaQuest}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
		</app:scrollableDataTable>
		<br:brPanelGroup style="width: 10px;" />
		<app:scrollableDataTable id="rotatividadesFuncionariosDesligados"
			value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.ocorrencias2}"
			var="rotatividade"
			rowIndexVar="index" rows="5"
			rowClasses="tabela_celula_normal, tabela_celula_destaque"
			width="300px">
			<app:scrollableColumn>
				<f:facet name="header">
					<br:brOutputText value="#{msgs.faixa}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{rotatividade.dsFaixaRenovQuest}" />
				<f:facet name="footer">
					<br:brOutputTextBold value="#{msgs.total}" style="width:100%;text-align:left;" />
				</f:facet>
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtd_func}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{rotatividade.qtdFuncDeslgQuest}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				<f:facet name="footer">
					<br:brOutputTextBold
						value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalQtdFuncDeslgQuest}"
						converter="inteiroMilharConverter"
						style="width:100%;text-align:right;" />
				</f:facet>
			</app:scrollableColumn>
		</app:scrollableDataTable>
		<br:brPanelGroup style="width: 10px;" />
		<app:scrollableDataTable id="percentuaisRotatividade"
			value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.ocorrencias2}"
			var="rotatividade"
			rowIndexVar="index" rows="5"
			rowClasses="tabela_celula_normal, tabela_celula_destaque"
			width="100%">
			<app:scrollableColumn width="60px">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.simbolo_percentual}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brPanelGroup style="width:100%;text-align:right;">
					<br:brOutputText value="#{rotatividade.ptcRenovFaixaQuest}" converter="decimalBrazillianConverter" />
					<br:brOutputText value="%" rendered="#{rotatividade.ptcRenovFaixaQuest != null}"/>
				</br:brPanelGroup>
				<f:facet name="footer">
					<br:brPanelGroup style="width:100%;text-align:right;">
						<br:brOutputTextBold
							value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.totalPtcRenovFaixaQuest}"
							converter="decimalBrazillianConverter" />
						<br:brOutputTextBold value="%" />
					</br:brPanelGroup>
				</f:facet>
			</app:scrollableColumn>
		</app:scrollableDataTable>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid cellpadding="0" cellspacing="0" columns="1">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="justificativa"
				value="#{msgs.beneficios_concedidos_colaboradores_custo_credito}:"
				styleClass="HtmlOutputLabelBradesco" />
		</br:brPanelGroup>
		<br:brOutputText id="justificativa"
			value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsTextoJustfConc}"
			styleClass="HtmlOutputTextRespostaBradesco"/>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="filiais"
				value="#{msgs.como_funcionarios_estao_distribuidos}" styleClass="HtmlOutputLabelBradesco" />
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGroup styleClass="divTabelaComponente">
			<app:scrollableDataTable id="filiais"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.ocorrencias3}"
				var="filial"
				rowIndexVar="index"
				rowClasses="tabela_celula_normal, tabela_celula_destaque"
				width="100%">
				<app:scrollableColumn width="50px">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.filiais}" style="width:100%; text-align:center" />
					</f:facet>
					<br:brOutputTextBold value="#{msgs.filial} #{index + 1}" />
				</app:scrollableColumn>
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.endereco}" style="width:100%; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{filial.dsLogradouroFilialQuest}" />
				</app:scrollableColumn>
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.uf}" style="width:100%; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{filial.cdSegmentoFilialQuest}" style="text-transform:uppercase;" />
				</app:scrollableColumn>
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.cidade}" style="width:100%; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{filial.dsMuncFilialQuest}" />
				</app:scrollableColumn>
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.cep}" style="width:100%; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{filial.cep}" />
				</app:scrollableColumn>
				<app:scrollableColumn styleClass="colTabRight">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.qtde_funcionarios}" style="width:100%; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{filial.qtdFuncFilalQuest}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brOutputLabelBold value="#{msgs.credito_consignado}" />
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel value="#{msgs.instituicoes_habilitadas_oferecer_creditos} " styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="naoPossuiCreditoConsignado"
				value="#{msgs.nao_possui}"
				styleClass="HtmlOutputTextRespostaBradesco"
				rendered="#{empty manterVinculacaoConvenioContaSalarioBean.dadosPiramide.ocorrencias4}" />
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup style="width:660px">
		<app:scrollableDataTable id="instituicoesCreditoConsigado"
			value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.ocorrencias4}"
			var="credito"
			rowIndexVar="index"
			rowClasses="tabela_celula_normal, tabela_celula_destaque"
			width="100%">
			<app:scrollableColumn width="55px">
				<f:facet name="header">
					<br:brOutputText value="" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{msgs.banco} #{index + 1}" />
			</app:scrollableColumn>
			<app:scrollableColumn width="300">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.banco}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{credito.dsBancoCredtCsignst}" />
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.valor_repasse_mensal}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{credito.vlRepasMesConsign}" styleClass="textRightDatatable" converter="decimalBrazillianConverter" />
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.valor_carteira}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{credito.vlCartCsign}" styleClass="textRightDatatable" converter="decimalBrazillianConverter" />
			</app:scrollableColumn>
		</app:scrollableDataTable>
	</br:brPanelGroup>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="haPreferenciaConcessaoCredito"
				value="#{msgs.preferencia_concessao_credito} "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="haPreferenciaConcessaoCredito"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.indicadorPreferenciaCreditoConsignado}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="painelAtendimentosConcorrentes"
				value="#{msgs.atendimento_concorrente_atuais_empresa}: " styleClass="HtmlOutputLabelBradesco" />
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brOutputText
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.atendimentoConcorrente}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup style="width:700px">
		<app:scrollableDataTable id="atendimentosConcorrentes"
			value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.ocorrencias6}"
			var="atendimento"
			rowIndexVar="index"
			rowClasses="tabela_celula_normal, tabela_celula_destaque">
			<app:scrollableColumn width="55px">
				<f:facet name="header">
					<br:brOutputText value="" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{msgs.banco} #{index + 1}" />
			</app:scrollableColumn>
			<app:scrollableColumn>
				<f:facet name="header">
					<br:brOutputText value="#{msgs.banco}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{atendimento.dsCrenAtendimentoQuest}" />
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.quantidade_atms}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{atendimento.qtdCrenQuest}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight" width="110px">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.qtdeFuncionarios}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{atendimento.qtdFuncCrenQuest}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
			</app:scrollableColumn>
			<app:scrollableColumn styleClass="colTabRight" width="110px">
				<f:facet name="header">
					<br:brOutputText value="#{msgs.distancia_empresa}" style="width:100%; text-align:center" />
				</f:facet>
				<br:brOutputText value="#{atendimento.cdDistcCrenQuest}" converter="inteiroMilharConverter" styleClass="textRightDatatable" />
			</app:scrollableColumn>
		</app:scrollableDataTable>
	</br:brPanelGroup>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="justificativaEmpresa"
				value="#{msgs.empresa_possui_relacionamento_pj_banco}"
				styleClass="HtmlOutputLabelBradesco" />
		</br:brPanelGroup>
		<br:brOutputText id="justificativaEmpresa"
			value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsTextoJustfRecip}"
			styleClass="HtmlOutputTextRespostaBradesco" />
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin" /></f:verbatim>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputLabelBold
				value="#{msgs.havera_instalacao_estrutura_pa_empresas_pae} "
				styleClass="HtmlOutputTextBoldBradesco" />
			<br:brOutputText
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.indicadorInstalacaoEstrutura}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="tipoEstruturaInstaladaEmpresa"
				value="#{msgs.tipo_estrutura_instalada_empresa} "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brPanelGroup style="margin-right:5px;">
				<br:brOutputText
					value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsEstruturaInstaEmpr}"
					styleClass="HtmlOutputTextRespostaBradesco" />
			</br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="agenciaVinculadaInstalacao"
				value="#{msgs.agencia_varejo_subordinado_pa_empresas_pae} "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="agenciaVinculadaInstalacao"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsAgenciaVincInsta}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="distanciaAgenciaLocalInstalacao"
				value="#{msgs.distancia_agencia_local_instalacao}: "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="distanciaAgenciaLocalInstalacao"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.distanciaAgInstalacao}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="houveVisitaLocal"
				value="#{msgs.houve_visita_local} "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="houveVisitaLocal"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsIndicadorVisitaInsta}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="nomeGerenteVisitante"
				value="#{msgs.quem_visitou_nome_gerente} "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="nomeGerenteVisitante"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsGerVisitanteInsta}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="areaInstalacaoPosto"
				value="#{msgs.area_instalacao_posta_m2} "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="areaInstalacaoPosto"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.areaInstalacao}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="tipoNecessidade"
				value="#{msgs.havera_necessidade}: " styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsTipoNecessInsta}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="haPerdaRelacionamento"
				value="#{msgs.banco_perder_folha_ha_perda_relacionamento} "
				styleClass="HtmlOutputLabelBradesco" />
			<br:brOutputText id="haPerdaRelacionamento"
				value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsIndicadorPerdarelacionamento}"
				styleClass="HtmlOutputTextRespostaBradesco" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputLabel for="justificativaInfoComplementares"
				value="#{msgs.info_complementares}:"
				styleClass="HtmlOutputLabelBradesco" />
		</br:brPanelGroup>
		<br:brOutputText id="justificativaInfoComplementares"
			value="#{manterVinculacaoConvenioContaSalarioBean.dadosPiramide.dsTextoJustfCompl}"
			styleClass="HtmlOutputTextRespostaBradesco"
			style="margin-top:10px;" />
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin" /></f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">
		<br:brCommandButton id="btnVoltar"
			value="#{msgs.btn_voltar}"
			action="#{manterVinculacaoConvenioContaSalarioBean.voltarConsultaContrato}">
			<brArq:submitCheckClient />
		</br:brCommandButton>
		<br:brPanelGroup style="width:100%;text-align:right;">
			<br:brCommandButton id="btnImprimirPiramide"
				value="#{msgs.btn_imprimir}"
				action="#{manterVinculacaoConvenioContaSalarioBean.imprimirPiramideSalarialPdf}"
				onclick="javascript:desbloquearTela();">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>
</brArq:form>