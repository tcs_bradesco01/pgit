<%@include file="/componentes/jsp/include.jsp"%>

<brArq:form id="formAlteraTpClassifParticContrato">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_cliente_representante}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_cpfcnpj}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVinculacaoConvenioContaSalarioBean.cpfCnpjRepresentante}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_nomerazaosocial}:"/>						
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.nomeRazaoRepresentante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_grupo_economico}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVinculacaoConvenioContaSalarioBean.grupEconRepresentante}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_atividade_economica}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.ativEconRepresentante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_segmento}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVinculacaoConvenioContaSalarioBean.segRepresentante}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_sub_segmento}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.subSegRepresentante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.dsAgenciaGestora}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.dsGerenteResponsavel}"/>
			</br:brPanelGroup>					
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_cliente_cpfCnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.cpfCnpjParticipante}"/>
			</br:brPanelGroup>				

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_cliente_nomeRazaoSocial}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.nomeRazaoParticipante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim>

	 	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_empresa}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVinculacaoConvenioContaSalarioBean.empresaContrato}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.tipoContrato}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_numero}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.numeroContrato}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_descricao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVinculacaoConvenioContaSalarioBean.descricaoContrato}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_situacao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVinculacaoConvenioContaSalarioBean.situacaoContrato}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_motivo}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.motivoContrato}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo_participacao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVinculacaoConvenioContaSalarioBean.participacaoContrato}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.manterVinculacaoConvenioContaSalario_dadosConvenio}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	<br:brPanelGrid width="100%">
	
		<app:scrollableDataTable id="dataTable" value="#{manterVinculacaoConvenioContaSalarioBean.listaDadosConvenio}" var="dados" 
			width="100%" rows="10" rowClasses="tabelaLinha">				
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="Conv�nio"/>
					</f:facet>
					<br:brOutputText value="#{dados.codConvenioContaSalario}" />					
				</app:scrollableColumn>
				
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="CNPJ"/>
					</f:facet>
					<br:brOutputText value="#{dados.cpfCnpjFormatado}" />					
				</app:scrollableColumn>
				
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="Banco"/>
					</f:facet>
					<br:brOutputText value="#{dados.banco}" />					
				</app:scrollableColumn>
				
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="Ag�ncia"/>
					</f:facet>
					<br:brOutputText value="#{dados.agenciaFormatado}" />					
				</app:scrollableColumn>
				
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="Conta"/>
					</f:facet>
					<br:brOutputText value="#{dados.contaFormatado}" />					
				</app:scrollableColumn>
				
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="Situa��o"/>
					</f:facet>
					<br:brOutputText value="#{dados.situacaoConvenio}" />					
				</app:scrollableColumn>
		</app:scrollableDataTable>
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterVinculacaoConvenioContaSalarioBean.carregarPaginacao}" >
			 <f:facet name="first">
				<brArq:pdcCommandButton id="primeira"
				  styleClass="bto1"
				  value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
				<brArq:pdcCommandButton id="retrocessoRapido"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
				<brArq:pdcCommandButton id="anterior"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
				<brArq:pdcCommandButton id="proxima"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
				<brArq:pdcCommandButton id="avancoRapido"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
				<brArq:pdcCommandButton id="ultima"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_data_hora_inclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.retornoSaidaDTO.horarioInclusao}"/>			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.retornoSaidaDTO.usuarioInclusao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.retornoSaidaDTO.tipoCanalInclusao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.retornoSaidaDTO.descCanalInclusao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_data_hora_manutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.retornoSaidaDTO.horarioManutencao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.retornoSaidaDTO.usuarioManutencao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.retornoSaidaDTO.tipoCanalManutencao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detMoedasOperacao_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.retornoSaidaDTO.descCanalManutencao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>


		<f:verbatim><hr class="lin"> </f:verbatim>

		<br:brPanelGrid columns="2" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="width:100%;text-align:left" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{manterVinculacaoConvenioContaSalarioBean.voltarConsultaContrato}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="width:100%;text-align:right" >
				<br:brCommandButton id="btnIncluir" styleClass="bto1" value="#{msgs.btn_incluir}" action="#{manterVinculacaoConvenioContaSalarioBean.incluir}" style="margin-right:5px" disabled="true" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAlterar" styleClass="bto1" value="#{msgs.btn_alterar}" action="#{manterVinculacaoConvenioContaSalarioBean.alterar}" style="margin-right:5px" disabled="#{manterVinculacaoConvenioContaSalarioBean.desabilitaBotoesIndicadorConvenioNovo}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" styleClass="bto1" value="#{msgs.btn_excluir}" action="#{manterVinculacaoConvenioContaSalarioBean.excluir}" disabled="#{manterVinculacaoConvenioContaSalarioBean.desabilitaBotoesIndicadorConvenioNovo}" onclick="javascript: if (!confirm('Confirma Exclus�o?')) { desbloquearTela(); return false; }" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

	</br:brPanelGrid>
</brArq:form>