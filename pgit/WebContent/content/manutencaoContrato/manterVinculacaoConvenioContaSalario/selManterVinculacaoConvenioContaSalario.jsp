<%@include file="/componentes/jsp/include.jsp"%>

<brArq:form id="formAlteraTpClassifParticContrato">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_cliente_representante}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVinculacaoConvenioContaSalarioBean.cpfCnpjRepresentante}"/>
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculacaoConvenioContaSalarioBean.nomeRazaoRepresentante}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.manterVinculacaoConvenioContaSalario_convenios}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

				<app:scrollableDataTable id="dataTable" value="#{manterVinculacaoConvenioContaSalarioBean.listaSelConvenio}" var="result" 
					rows="10" rowIndexVar="parametroKey" 
					rowClasses="tabela_celula_normal, tabela_celula_destaque" 
					width="100%">

					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterVinculacaoConvenioContaSalarioBean.itemSelecionadoListaConvenio}">
							<f:selectItems value="#{manterVinculacaoConvenioContaSalarioBean.listaControleSelConvenio}"/>
							<a4j:support event="onclick" reRender="btnConfirmarIncluir, btnConfirmarAlterar" />
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabCenter" width="250px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.manterVinculacaoConvenioContaSalario_banco_agencia_conta_convenio}" style="width:250; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.bancoFormatado}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>				

					<app:scrollableColumn styleClass="colTabCenter" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.manterVinculacaoConvenioContaSalario_numeroConvenio}"  style="width:250; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.numConvenio}" />
					</app:scrollableColumn>				

				</app:scrollableDataTable>

			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterVinculacaoConvenioContaSalarioBean.pesquisar}" >
				 <f:facet name="first">
					<brArq:pdcCommandButton id="primeira"
					  styleClass="bto1"
					  value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  </f:facet>
				  <f:facet name="fastrewind">
					<brArq:pdcCommandButton id="retrocessoRapido"
					  styleClass="bto1" style="margin-left: 3px;"
					  value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  </f:facet>
				  <f:facet name="previous">
					<brArq:pdcCommandButton id="anterior"
					  styleClass="bto1" style="margin-left: 3px;"
					  value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  </f:facet>
				  <f:facet name="next">
					<brArq:pdcCommandButton id="proxima"
					  styleClass="bto1" style="margin-left: 3px;"
					  value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  </f:facet>
				  <f:facet name="fastforward">
					<brArq:pdcCommandButton id="avancoRapido"
					  styleClass="bto1" style="margin-left: 3px;"
					  value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  </f:facet>
				  <f:facet name="last">
					<brArq:pdcCommandButton id="ultima"
					  styleClass="bto1" style="margin-left: 3px;"
					  value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  </f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim>

		<br:brPanelGrid columns="2" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="width:100%;text-align:left" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{manterVinculacaoConvenioContaSalarioBean.voltarVinculacao}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="width:100%;text-align:right" >
				<br:brCommandButton id="btnConfirmarIncluir" styleClass="bto1" value="#{msgs.btn_incluir}" action="#{manterVinculacaoConvenioContaSalarioBean.confirmarIncluir}" disabled="#{empty manterVinculacaoConvenioContaSalarioBean.itemSelecionadoListaConvenio}" rendered="#{manterVinculacaoConvenioContaSalarioBean.btoAcao == 'I'}" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela(); return false; }" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConfirmarAlterar" styleClass="bto1" value="#{msgs.btn_alterar}" action="#{manterVinculacaoConvenioContaSalarioBean.confirmarAlterar}" disabled="#{empty manterVinculacaoConvenioContaSalarioBean.itemSelecionadoListaConvenio}" rendered="#{manterVinculacaoConvenioContaSalarioBean.btoAcao == 'A'}" onclick="javascript: if (!confirm('Confirma Altera��o?')) { desbloquearTela(); return false; }" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

	</br:brPanelGrid>
</brArq:form>