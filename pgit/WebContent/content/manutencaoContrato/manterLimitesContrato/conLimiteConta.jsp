<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j"%>

<brArq:form id="conLimiteConta" name="conLimiteConta" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_cliente_master}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_nome_razao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerente_responsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.cpfCnpj}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.nomeRazaoSocial}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.empresa}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.numero}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.dsDescricaoContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterLimitesContratoBean.participacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<t:selectOneRadio id="filtroRadio" value="#{manterLimitesContratoBean.rdoLimiteConta}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />					
		<a4j:support event="onclick" action="#{manterLimitesContratoBean.limparArgumentosLimiteConta} "
				reRender="banco, agencia, conta, digito, tipoConta, btnConsultar,txtCnpj,txtFilial,txtControle,txtCpf,txtControleCpf" />
	</t:selectOneRadio>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="0" />			
		</br:brPanelGroup>
		
	    <br:brPanelGroup>				 		   
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(9,'conLimiteConta','conLimiteConta:txtCnpj','conLimiteConta:txtFilial');" style="margin-right: 5" onkeypress="onlyNum();" size="11" maxlength="9" styleClass="HtmlInputTextBradesco" value="#{manterLimitesContratoBean.cdCpfCnpjFiltro}" readonly="#{manterLimitesContratoBean.rdoLimiteConta != 0 || manterLimitesContratoBean.rdoLimiteConta == ''}"/>
				    <br:brInputText id="txtFilial" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(4,'conLimiteConta','conLimiteConta:txtFilial','conLimiteConta:txtControle');" style="margin-right: 5" onkeypress="onlyNum();" size="5" maxlength="4" styleClass="HtmlInputTextBradesco" value="#{manterLimitesContratoBean.cdFilialCnpjFiltro}"  readonly="#{manterLimitesContratoBean.rdoLimiteConta != 0 || manterLimitesContratoBean.rdoLimiteConta == ''}"/>
					<br:brInputText id="txtControle" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" size="3" maxlength="2" styleClass="HtmlInputTextBradesco" value="#{manterLimitesContratoBean.cdControleCnpjFiltro}" readonly="#{manterLimitesContratoBean.rdoLimiteConta != 0 || manterLimitesContratoBean.rdoLimiteConta == ''}"/>
			</br:brPanelGrid>
	    </br:brPanelGroup>				 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="filtroRadio" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" style="margin-right: 5" onkeypress="onlyNum();"  readonly="#{manterLimitesContratoBean.rdoLimiteConta != 1}" value="#{manterLimitesContratoBean.cdCpfFiltro}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'conLimiteConta','conLimiteConta:txtCpf','conLimiteConta:txtControleCpf');" />
			    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" readonly="#{manterLimitesContratoBean.rdoLimiteConta != 1}" value="#{manterLimitesContratoBean.cdControleCpfFiltro}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="8" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="2" />
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText readonly="#{manterLimitesContratoBean.rdoLimiteConta != 2}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="banco" styleClass="HtmlInputTextBradesco"  onkeypress="onlyNum();"
								value="#{manterLimitesContratoBean.bancoFiltro}" size="10" maxlength="3" >
					</br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" readonly="#{manterLimitesContratoBean.rdoLimiteConta != 2}" id="agencia" styleClass="HtmlInputTextBradesco"  onkeypress="onlyNum();"
										 value="#{manterLimitesContratoBean.agenciaFiltro}" size="12" maxlength="5" >
					</br:brInputText>	
				</br:brPanelGroup>									
			</br:brPanelGrid>
			
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" readonly="#{manterLimitesContratoBean.rdoLimiteConta != 2}" id="conta" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
								 value="#{manterLimitesContratoBean.contaFiltro}" size="16" maxlength="13" >
					</br:brInputText>	
				</br:brPanelGroup>
	    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
			    <br:brPanelGroup>
						
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.btn_limpar_dados}" action="#{manterLimitesContratoBean.limparDadosLimiteConta}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{manterLimitesContratoBean.consultarLimiteConta}"
			    onclick="javascript:desbloquearTela(); return validaPesquisarLimiteConta('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conContasManterContrato_cnpj}','#{msgs.conContasManterContrato_cpf}','#{msgs.conContasManterContrato_banco}', '#{msgs.conContasManterContrato_agencia}',
	   		'#{msgs.conContasManterContrato_conta}','#{msgs.conContasManterContrato_digito_conta}','#{msgs.conContasManterContrato_tipo_conta}');" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0"  >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
			<app:scrollableDataTable id="dataTable" value="#{manterLimitesContratoBean.listaGridLimiteConta}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
			
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>		
					<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
						value="#{manterLimitesContratoBean.itemSelecionadoLimiteConta}">
						<f:selectItems value="#{manterLimitesContratoBean.listaGridControleLimiteConta}"/>
						<a4j:support event="onclick" reRender="btnAlterar, btnLimiteUtilizado, btnDetalhar" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>			

				<app:scrollableColumn  width="250px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_cpf_cnpj_participante}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdCpfCnpj}"  styleClass="tableFontStyle" />
				</app:scrollableColumn>
	
				<app:scrollableColumn  width="250px" styleClass="colTabLeft">
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_nome_razaoSocial_participante}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.nmParticipante}"  styleClass="tableFontStyle" />
				</app:scrollableColumn>
	
				<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_banco}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.bancoFormatado}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_agencia}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.agenciaFormatada}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
	
				<app:scrollableColumn  width="250px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_conta}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.contaFormatada}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
	
				<app:scrollableColumn  width="250px" styleClass="colTabLeft" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_tipo_conta}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoConta}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="250px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_limite_conta}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.vlAdicionalContratoConta}" styleClass="tableFontStyle" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="250px" styleClass="colTabCenter" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_prazo_validade}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.prazoValidade}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="250px" styleClass="colTabRight" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_limite_adicional_ted}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.vlAdcContrCtaTed}" styleClass="tableFontStyle" converter="decimalBrazillianConverter"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn  width="250px" styleClass="colTabCenter" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_prazo_validade_ted}" style="width:250;text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.prazoValidadeTED}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>				 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterLimitesContratoBean.pesquisar}" > 
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{manterLimitesContratoBean.voltarPesquisa}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btnLimpar" disabled="#{manterLimitesContratoBean.listaGridLimiteConta == null}" style="margin-right:5px;" styleClass="bto1" value="#{msgs.btn_limpar}" action="#{manterLimitesContratoBean.limparLimiteConta}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnDetalhar" disabled="#{empty manterLimitesContratoBean.itemSelecionadoLimiteConta}" style="margin-right:5px;" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{manterLimitesContratoBean.detalharLimiteConta}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnAlterar" disabled="#{empty manterLimitesContratoBean.itemSelecionadoLimiteConta}" styleClass="bto1" value="#{msgs.btn_alterar}" action="#{manterLimitesContratoBean.alterarLimiteConta}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
		</br:brPanelGroup>
	</br:brPanelGrid> 	
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>