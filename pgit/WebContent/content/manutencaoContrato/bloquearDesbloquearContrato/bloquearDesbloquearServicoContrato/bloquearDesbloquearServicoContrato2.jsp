<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="bloquearDesbloquearServicoContrato2" name="bloquearDesbloquearServicoContrato2" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<h:inputHidden id="hiddenProsseguir" value="#{bloqDesbloqServicoContratoBean.hiddenProsseguir}" />
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_cliente_master}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_nome_razao_social}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.nomeRazaoMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.ativEconomicaMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
	 	<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
	   <br:brPanelGroup> 
	   </br:brPanelGroup>
    </br:brPanelGrid>

     <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>
		   <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		   <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerente_responsavel}:"/>
		   <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.gerenteResponsavel}"/>
	    </br:brPanelGroup>					
    </br:brPanelGrid>

  <f:verbatim><hr class="lin"> </f:verbatim>

     <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
	     <br:brPanelGroup>
		   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
	     </br:brPanelGroup>
     </br:brPanelGrid>

	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
	 	<br:brPanelGroup>
		</br:brPanelGroup>
	 </br:brPanelGrid>

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.cpfCnpj}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.nomeRazaoSocial}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	 <f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.empresaContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.tipoContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.numeroContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.situacaoContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.motivoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloqDesbloqServicoContratoBean.participacaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>		
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_dados_atuais}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170px">		
			<app:scrollableDataTable id="dataTable" value="#{bloqDesbloqServicoContratoBean.listaServicoSelecionado}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque">
				
				 
				<app:scrollableColumn styleClass="colTabLeft" width="375px"> 
					<f:facet name="header">
 					  <br:brOutputText value="#{msgs.bloquearDesbloquearServicoContrato_grid_tipo_servico}" styleClass="tableFontStyle" style="width:375px; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{result.dsModalidade}"/>
				 </app:scrollableColumn>
				 
				 <app:scrollableColumn styleClass="colTabLeft" width="350px"> 
					<f:facet name="header">
 					  <br:brOutputText value="#{msgs.bloquearDesbloquearServicoContrato_grid_situacao}" styleClass="tableFontStyle" style="width:350px; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{result.dsSituacaoServicoRelacionado}"/>
				 </app:scrollableColumn>
				 
			</app:scrollableDataTable>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_dados_alteracao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	

   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.bloquearDesbloquearContratos_condicao}:"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<h:inputHidden id="hiddenRadiobloquearDesbloquearFiltro" value="#{bloqDesbloqServicoContratoBean.bloquearDesbloquearFiltro}" />
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
		<br:brPanelGroup>
			<br:brSelectOneRadio id="radiobloquearDesbloquearFiltro" styleClass="HtmlSelectOneRadioBradesco" value="#{bloqDesbloqServicoContratoBean.bloquearDesbloquearFiltro}" >
				<f:selectItem  itemValue="1" itemLabel="#{msgs.bloquearDesbloquearServicoContrato2_radio_bloquear}"/>  
	            <f:selectItem  itemValue="2" itemLabel="#{msgs.bloquearDesbloquearServicoContrato2_radio_desbloquear}"/> 
	            <a4j:support event="onclick" reRender="hiddenRadiobloquearDesbloquearFiltro, cboMotivoBloqueioFiltro, radiobloquearDesbloquearFiltro, hiddenCboMotivoBloqueioFiltro" action="#{bloqDesbloqServicoContratoBean.listarMotivoSituacao}" />
	    	</br:brSelectOneRadio>
	    </br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px"  >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />		
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.bloquearDesbloquearServicoContrato2_motivo_bloqueio}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			  
             <h:inputHidden id="hiddenCboMotivoBloqueioFiltro" value="#{bloqDesbloqServicoContratoBean.motivoBloqueioFiltro}" />
			 <br:brPanelGrid style="margin-top:5px;"> 
				<br:brPanelGroup>	
					<br:brSelectOneMenu id="cboMotivoBloqueioFiltro" value="#{bloqDesbloqServicoContratoBean.motivoBloqueioFiltro}" disabled="#{bloqDesbloqServicoContratoBean.bloquearDesbloquearFiltro == 0 ||
					 bloqDesbloqServicoContratoBean.bloquearDesbloquearFiltro == null }"> 
						<f:selectItem itemValue="0" itemLabel="#{msgs.bloquearDesbloquearServicoContrato2_cbo_selecione}" />
						<f:selectItems value="#{bloqDesbloqServicoContratoBean.listaMotivoBloqueio}"/>						
					</br:brSelectOneMenu>							
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.bloquearDesbloquearServicoContrato2_btn_voltar}" action="#{bloqDesbloqServicoContratoBean.voltarServico}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="width:600px;" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" style="margin-right:5px;" styleClass="bto1" value="#{msgs.bloquearDesbloquearServicoContrato2_btn_limpar}" action="#{bloqDesbloqServicoContratoBean.limparFiltros}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.bloquearDesbloquearServicoContrato2_btn_avancar}" onclick="javascript:validarFiltrosServicoContrato(document.forms[1], '#{msgs.bloquearDesbloquearServicoContrato2_informe_bloquear_desbloquear}', '#{msgs.bloquearDesbloquearServicoContrato2_informe_motivo_bloqueio}');" action="#{bloqDesbloqServicoContratoBean.avancarBloquearDesbloquear}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>

	<brArq:validatorScript functionName="validateForm" />
</brArq:form>
