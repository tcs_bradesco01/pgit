<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%> 
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="bloquearDesbloquearContasContratoConta" name="bloquearDesbloquearContasContratoConta" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.bloquearDesbloquearContasContrato_cliente_Representante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_nome_razao_social}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.nomeRazaoMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.ativEconomicaMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
	   <br:brPanelGroup> 
	   </br:brPanelGroup>
    </br:brPanelGrid>

     <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>
		   <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		   <br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerente_responsavel}:"/>
		   <br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.gerenteResponsavel}"/>
	    </br:brPanelGroup>					
    </br:brPanelGrid>

  <f:verbatim><hr class="lin"> </f:verbatim>

     <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
	     <br:brPanelGroup>
		   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
	     </br:brPanelGroup>
     </br:brPanelGrid>

	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
	 	<br:brPanelGroup>
		</br:brPanelGroup>
	 </br:brPanelGrid>

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.cpfCnpj}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.nomeRazaoSocial}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.bloquearDesbloquearContasContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.empresaContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.tipoContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.numeroContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.situacaoContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.motivoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.bloquearDesbloquearContasContrato_tipo_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{bloquearDesbloquearContasContratoBean.participacaoContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.bloquearDesbloquearContasContrato_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="filtroRadio" value="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />	
				<f:selectItem itemValue="3" itemLabel="" />
				<a4j:support event="onclick" action="#{bloquearDesbloquearContasContratoBean.limparCamposFiltroConta}"
						reRender="txtCnpj,txtFilial,txtControle,txtCpf,txtControleCpf, banco, agencia, conta, digito, tipoConta, finalidade,agenciaDigito,btnConsultar" />
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="0" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conParticipantesManterContrato_label_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'bloquearDesbloquearContasContratoConta','bloquearDesbloquearContasContratoConta:txtCnpj','bloquearDesbloquearContasContratoConta:txtFilial');" style="margin-right: 5" onkeypress="onlyNum();" size="11" maxlength="9"  styleClass="HtmlInputTextBradesco" value="#{bloquearDesbloquearContasContratoBean.cnpjFiltro}" disabled="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != '0'}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'bloquearDesbloquearContasContratoConta','bloquearDesbloquearContasContratoConta:txtFilial','bloquearDesbloquearContasContratoConta:txtControle');" style="margin-right: 5"  onkeypress="onlyNum();" size="5" maxlength="4"  styleClass="HtmlInputTextBradesco" value="#{bloquearDesbloquearContasContratoBean.cnpjFiltro2}" disabled="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != '0'}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				<br:brInputText id="txtControle" style="margin-right: 5" onkeypress="onlyNum();" size="3" maxlength="2"  styleClass="HtmlInputTextBradesco" value="#{bloquearDesbloquearContasContratoBean.cnpjFiltro3}" disabled="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != '0'}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="1" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conParticipantesManterContrato_label_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" style="margin-right: 5" onkeypress="onlyNum();" value="#{bloquearDesbloquearContasContratoBean.cpfFiltro}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'bloquearDesbloquearContasContratoConta','bloquearDesbloquearContasContratoConta:txtCpf','bloquearDesbloquearContasContratoConta:txtControleCpf');" disabled="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != 1}"/>
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco"  onkeypress="onlyNum();" value="#{bloquearDesbloquearContasContratoBean.cpfFiltro2}" maxlength="2" size="3" id="txtControleCpf" disabled="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != 1}"/>
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="8" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="2" />
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" readonly="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != 2}" id="banco" styleClass="HtmlInputTextBradesco"  onkeypress="onlyNum();"
								value="#{bloquearDesbloquearContasContratoBean.bancoFiltro}" size="10" maxlength="3" >
					</br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" readonly="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != 2}" id="agencia" styleClass="HtmlInputTextBradesco"  onkeypress="onlyNum();"
										 value="#{bloquearDesbloquearContasContratoBean.agenciaFiltro}" size="12" maxlength="5" >
					</br:brInputText>	
				</br:brPanelGroup>					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" readonly="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != 2}" id="agenciaDigito" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" 
						style="margin-left:5px;"				 value="#{bloquearDesbloquearContasContratoBean.agenciaDigitoFiltro}" size="2" maxlength="1" >
					</br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
			
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" readonly="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != 2}" id="conta" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
								 value="#{bloquearDesbloquearContasContratoBean.contaFiltro}" size="15" maxlength="13" >
					</br:brInputText>	
				</br:brPanelGroup>
	    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" readonly="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != 2}" id="digito" styleClass="HtmlInputTextBradesco" 
								 value="#{bloquearDesbloquearContasContratoBean.digitoFiltro}" size="2" maxlength="2" >
					</br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_tipo_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu disabled="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != 2}" id="tipoConta" value="#{bloquearDesbloquearContasContratoBean.tipoContaFiltro}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conContasManterContrato_label_combo_selecione}"/>
						<f:selectItems value="#{bloquearDesbloquearContasContratoBean.listaTipoConta}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
	</br:brPanelGrid>
		
		
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="3" />
		</br:brPanelGroup>
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.bloquearDesbloquearContasContrato_finalidade_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="finalidade" disabled="#{bloquearDesbloquearContasContratoBean.radioPesquisaContas != 3}" value="#{bloquearDesbloquearContasContratoBean.finalidadeFiltro}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conContasManterContrato_label_combo_selecione}"/>
						<f:selectItems value="#{bloquearDesbloquearContasContratoBean.listaFinalidadeConta}" />						
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	 
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="width:583px;" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" style="margin-right:5px;" styleClass="bto1" value="#{msgs.bloquearDesbloquearContasContrato_btn_limpar_dados}" action="#{bloquearDesbloquearContasContratoBean.limparDados}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.bloquearDesbloquearContasContrato_btn_consultar}" action="#{bloquearDesbloquearContasContratoBean.carregaListaContas}" 			    
			onclick="javascript:desbloquearTela(); return validaPesquisarContas('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conParticipantesManterContrato_label_cnpj}','#{msgs.conParticipantesManterContrato_label_cpf}','#{msgs.conContasManterContrato_banco}', '#{msgs.conContasManterContrato_agencia}',
	   		'#{msgs.conContasManterContrato_conta}','#{msgs.conContasManterContrato_conta}','#{msgs.conContasManterContrato_tipo_conta}','#{msgs.conContasManterContrato_finalidade}');" >
	   		
<%--	   		
			*** btn desabilitado conf. posi��o. Acima vers�o sempre habilitado, pois a consulta padr�o retorna tudo **
	   		<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.bloquearDesbloquearContasContrato_btn_consultar}" action="#{bloquearDesbloquearContasContratoBean.carregaListaContas}" 			    
			onclick="javascript:desbloquearTela(); return validaPesquisarContas('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conParticipantesManterContrato_label_cnpj}','#{msgs.conParticipantesManterContrato_label_cpf}','#{msgs.conContasManterContrato_banco}', '#{msgs.conContasManterContrato_agencia}',
	   		'#{msgs.conContasManterContrato_conta}','#{msgs.conContasManterContrato_conta}','#{msgs.conContasManterContrato_tipo_conta}','#{msgs.conContasManterContrato_finalidade}');" disabled="#{ (bloquearDesbloquearContasContratoBean.radioPesquisaContas != 0 && 
	   		bloquearDesbloquearContasContratoBean.radioPesquisaContas != 1 && bloquearDesbloquearContasContratoBean.radioPesquisaContas != 2 && bloquearDesbloquearContasContratoBean.radioPesquisaContas != 3) || bloquearDesbloquearContasContratoBean.radioPesquisaContas == '' }">
--%>	   		
	   		
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim>
	  
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	 
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">		
			<app:scrollableDataTable id="dataTable" value="#{bloquearDesbloquearContasContratoBean.listaGrid}" var="result" rows="4" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque">
								
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false"
						value="#{bloquearDesbloquearContasContratoBean.itemSelecionadoGrid}" >
						<f:selectItems value="#{bloquearDesbloquearContasContratoBean.listaGridControle}"/>
						<a4j:support event="onclick" reRender="btnBloquearDesbloquear,btnLimpar" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" /> 
				</app:scrollableColumn>
				 
			  	<app:scrollableColumn styleClass="colTabRight" width="160px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conContasManterContrato_grid_cpf_cnpj}" style="text-align:center;width:160" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdCpfCnpj}" />
				</app:scrollableColumn>
				<app:scrollableColumn styleClass="colTabLeft" width="270px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conContasManterContrato_grid_nome_razao}" style="text-align:center;width:270" />
			    </f:facet>
			    <br:brOutputText value="#{result.nomeParticipante}" />
				</app:scrollableColumn>
				
				
				<app:scrollableColumn styleClass="colTabLeft" width="140px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conContasManterContrato_grid_banco}" style="text-align:center;width:140" />
			    </f:facet>
   			    <br:brOutputText value="#{result.cdBanco}" />
				<br:brOutputText value="-" />				
			    <br:brOutputText value="#{result.dsBanco}"/>
			  </app:scrollableColumn>
			  
				<app:scrollableColumn styleClass="colTabLeft" width="140px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conContasManterContrato_grid_agencia}" style="text-align:center;width:140" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdAgencia}" />
   				<br:brOutputText value="-" />				
			    <br:brOutputText value="#{result.dsAgencia}" />
			  </app:scrollableColumn>
			  
				<app:scrollableColumn styleClass="colTabRight" width="90px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conContasManterContrato_grid_conta}" style="text-align:center;width:90" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdConta}" />
				<br:brOutputText value="-" />				
				<br:brOutputText value="#{result.cdDigitoConta}" />			    
			  </app:scrollableColumn>			  			  
			  
			  
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conContasManterContrato_grid_tipo_conta}" style="text-align:center;width:200" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoConta}" />
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn styleClass="colTabLeft" width="330px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conContasManterContrato_grid_finalidade}" style="text-align:center;width:280"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsFinalidade}" />
			  </app:scrollableColumn>			  
			</app:scrollableDataTable>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
		    <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{bloquearDesbloquearContasContratoBean.pesquisar}" > 	
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet> 
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>			  
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.bloquearDesbloquearContasContrato_btn_voltar}" action="#{bloquearDesbloquearContasContratoBean.voltarContas}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" style="width:500px;" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimpar" disabled="false"  style="margin-right:5px;" styleClass="bto1" value="#{msgs.bloquearDesbloquearContasContrato_btn_limpar}" action="#{bloquearDesbloquearContasContratoBean.limpar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>			
			<br:brCommandButton disabled="#{empty bloquearDesbloquearContasContratoBean.itemSelecionadoGrid}" id="btnBloquearDesbloquear" styleClass="bto1" value="#{msgs.bloquearDesbloquearContasContrato_btn_bloquear_desbloquear}" action="#{bloquearDesbloquearContasContratoBean.bloquearDesbloquear}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>

	<brArq:validatorScript functionName="validateForm" />
</brArq:form>
