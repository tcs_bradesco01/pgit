<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterVincContaContratoDebitoTr" name="conManterVincContaContratoDebitoTr" >
<h:inputHidden id="hiddenFoco" value="#{manterVincContaContratoDebitoBean.foco}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<a4j:outputPanel id="formulario" style="width: 100%; text-align: left" ajaxRendered="true">				
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_identificacao_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<a4j:region id="regionFiltro">
		<t:selectOneRadio id="rdoFiltroCliente" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.bloqueiaRadio}">		 
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="" />
		</t:selectOneRadio>
		</a4j:region>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<t:radio for="rdoFiltroCliente" index="0" />			
			
			 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cnpj}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'conAlteraTpClassifParticContrato','conAlteraTpClassifParticContrato:txtCnpj','conAlteraTpClassifParticContrato:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'conAlteraTpClassifParticContrato','conAlteraTpClassifParticContrato:txtFilial','conAlteraTpClassifParticContrato:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" >
			<t:radio for="rdoFiltroCliente" index="1" />			
			
			 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_cpf}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
				    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente }" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
					    onkeyup="proximoCampo(9,'conAlteraTpClassifParticContrato','conAlteraTpClassifParticContrato:txtCpf','conAlteraTpClassifParticContrato:txtControleCpf');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" >
			<t:radio for="rdoFiltroCliente" index="2" />		
			
			 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_label_nomeRazao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '2' || manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>				
			</a4j:outputPanel>
			
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
			<t:radio for="rdoFiltroCliente" index="3" />			
			
			<a4j:outputPanel id="panelBanco" ajaxRendered="true">
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_banco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'conAlteraTpClassifParticContrato','conAlteraTpClassifParticContrato:txtBanco','conAlteraTpClassifParticContrato:txtAgencia');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>				
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'conAlteraTpClassifParticContrato','conAlteraTpClassifParticContrato:txtAgencia','conAlteraTpClassifParticContrato:txtConta');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				
			</a4j:outputPanel>
		
			<a4j:outputPanel id="panelConta" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.consultarPagamentosIndividual_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					</br:brPanelGroup>
				    				
				</br:brPanelGrid>				
			</a4j:outputPanel>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
	    		<br:brCommandButton id="btnLimparCampos2" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.limparDadosCliente}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
		   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemFiltroSelecionado || manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{msgs.identificacaoClienteContrato_label_consultar_cliente}" action="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.identificacaoClienteContrato_cnpj}','#{msgs.identificacaoClienteContrato_cpf}', '#{msgs.identificacaoClienteContrato_label_nomeRazao}','#{msgs.identificacaoClienteContrato_banco}','#{msgs.identificacaoClienteContrato_agencia}','#{msgs.identificacaoClienteContrato_conta}');">		
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</a4j:outputPanel>
		</br:brPanelGrid>	
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.consultarPagamentosIndividual_label_cliente}:"/>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>	
			
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	    
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolManutContratos_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="empresaGestora" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{!manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<f:selectItems  value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.listaEmpresaGestora}" />								
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_tipo_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoContrato" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{!manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<f:selectItems  value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.listaTipoContrato}" />
					</br:brSelectOneMenu>
					
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_numero}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.numeroFiltro}" size="12" maxlength="10" readonly="#{!manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
						<%--<a4j:support event="onblur" action="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.desabilitaCamposContrato}"	reRender="habilitaCampos,situacao, agenciaOperadora, gerenteId, gerenteNome" />--%>		
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.identificacaoClienteContrato_agencia_operadora}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="agenciaOperadora" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.agenciaOperadoraFiltro}" size="10" maxlength="6" 
					readonly="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaCampos || !manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
							<a4j:support event="onblur" reRender="gerenteId,gerenteNome,btnConsultarGerente" action="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.limparGerenteResponsavel}" oncomplete="limparGerente();" />
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_gerente_responsavel}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="gerenteId" readonly="#{!manterPendenciasContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}" onkeypress="javascript:limparNomeGerente(document.forms[1]);" onkeydown="javascript:limparNomeGerente(document.forms[1]);" styleClass="HtmlInputTextBradesco" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.codigoFunc}" size="15" maxlength="9">
						<brArq:commonsValidator type="integer" arg="#{msgs.identificacaoClienteContrato_gerente_responsavel}" server="false" client="true" />
						<a4j:support event="onblur" action="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.buscaFuncionarios}" reRender="gerenteNome,formModalMessages" onsubmit="loadModalJQuery()" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					</br:brInputText>	
				</br:brPanelGroup>
	    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
			    <br:brPanelGroup>
					<br:brInputText id="gerenteNome" styleClass="HtmlInputTextBradesco" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.funcionarioDTO.dsFuncionario}" size="50" maxlength="50" disabled="true"  />												
				</br:brPanelGroup>		
				<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
				<br:brPanelGroup>
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >							
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>				
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_situacao_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="situacao" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.situacaoFiltro}" disabled="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaCampos || !manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.identificacaoClienteContrato_selecione}"/>
						<f:selectItems  value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.listaSituacaoContrato}" />						
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>

    <f:verbatim><hr class="lin"></f:verbatim>
	</a4j:outputPanel>			
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);"
			 value="#{msgs.label_limpar_dados}" action="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.limparDadosPesquisaContrato}" style="margin-right:5px" disabled="false"> 				 
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" onclick="return validaCamposContratoSemSituacaoSemGerente(document.forms[1],'#{msgs.label_ocampo}','#{msgs.label_necessario}', '#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}', '#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}','#{msgs.identificacaoClienteContrato_tipo_contrato}', '#{msgs.identificacaoClienteContrato_mensagem_obrigatorio}');" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_consultar}" 
				action="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.conConsultarVinculoContratoDebitoTr}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><br></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

			<app:scrollableDataTable id="dataTable" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.listaGridPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
								
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemSelecionadoLista}">
						<f:selectItems value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="btnOperacoes" />						
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.identificacaoClienteContrato_club_representante}" style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdClubRepresentante}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="170px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_cpf_cnpj_representante}"  style="width:170; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cnpjOuCpfFormatado}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_nome_razao_social}" style="width:250; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.nmRazaoSocialRepresentante}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_empresa_gestora}"  style="width:300; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsPessoaJuridica}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_tipo_contrato}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoContrato}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_numero}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrSequenciaContrato}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_situacao_contrato}" style="width:150; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoContrato}" />
				</app:scrollableColumn>	

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_agencia_operadora}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsAgenciaOperadora}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_gerente_responsavel}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.descFuncionarioBradesco}" />
				</app:scrollableColumn>	

			</app:scrollableDataTable>
			
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.paginarConVincContrDebitoTr}" >
			 <f:facet name="first">
				<brArq:pdcCommandButton id="primeira"
				  styleClass="bto1"
				  value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
				<brArq:pdcCommandButton id="retrocessoRapido"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
				<brArq:pdcCommandButton id="anterior"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
				<brArq:pdcCommandButton id="proxima"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
				<brArq:pdcCommandButton id="avancoRapido"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
				<brArq:pdcCommandButton id="ultima"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimpar" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);"
					value="#{msgs.btn_limpar}" action="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.limparTela}" style="margin-right:5px" > 
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnOperacoes" styleClass="bto1" onmouseout="javascript:alteraBotao('normal', this.id);"
					value="#{msgs.detTarifasOperacoes2_operacao}" action="#{manterVincContaContratoDebitoBean.selecionar}" style="margin-right:5px" 
				 	disabled="#{empty manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.itemSelecionadoLista}"> 				 
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid> 
	</a4j:outputPanel>
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
	
	<t:inputHidden value="#{manterVincContaContratoDebitoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conManterVincContaContratoDebitoTr:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />

</brArq:form>