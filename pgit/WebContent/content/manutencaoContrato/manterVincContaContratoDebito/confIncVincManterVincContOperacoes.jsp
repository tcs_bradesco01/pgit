<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="confIncVincManterVincContOperacoes" name="confIncVincManterVincContOperacoes">

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	  <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_cpfcnpj}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.cpfCnpjRepresentante}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_nomerazaosocial}:"/>						
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.nomeRazaoRepresentante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_grupo_economico}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.grupEconRepresentante}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_atividade_economica}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.ativEconRepresentante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_segmento}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.segRepresentante}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_sub_segmento}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.subSegRepresentante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.dsAgenciaGestora}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.dsGerenteResponsavel}"/>
			</br:brPanelGroup>					
		</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
     <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_cliente_cpfCnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.cpfCnpjParticipante}"/>
			</br:brPanelGroup>				

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_cliente_nomeRazaoSocial}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.nomeRazaoParticipante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim>

	 	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_empresa}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.empresaContrato}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.tipoContrato}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_numero}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.numeroContrato}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_descricao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.descricaoContrato}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_situacao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.situacaoContrato}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_motivo}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.motivoContrato}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo_participacao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.participacaoContrato}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>

		
   <f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detTarifasOperacoes2_contaTarifa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detTarifasOperacoes2_banco}:"/>
		 	<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.bancoFiltroInc}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detTarifasOperacoes2_agencia}:"/>
		 	<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.agenciaFiltroInc}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detTarifasOperacoes2_conta}:"/>
		 	<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.contaFiltroInc}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detTarifasOperacoes2_tipo}:"/>
	 	<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.tipoContaFiltroInc}"/>
	</br:brPanelGroup>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.altDadosBasicos_clienteMaster_cpfCnpj}:"/>
		 	<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.cpfCnpj}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.altDadosBasicos_clienteMaster_nomeRazaoSocial}:"/>
		 	<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.nomeRazao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   <f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detTarifasOperacoes2_tarifa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

			<app:scrollableDataTable id="dataTable" value="#{manterVincContaContratoDebitoBean.listaGridIncluir}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.alteracaoTarifas_grid_tipo_servico}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdProdutoServicoOperacaoString}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>
								
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.manterVinc_grid_modalidade}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdProdutoOperacaoRelacionadoString}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.manterVinc__grid_operacao}"  style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdOperacaoProdutoServicoString}" />
				</app:scrollableColumn>				
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="" >
			 <f:facet name="first">
				<brArq:pdcCommandButton id="primeira"
				  styleClass="bto1"
				  value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
				<brArq:pdcCommandButton id="retrocessoRapido"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
				<brArq:pdcCommandButton id="anterior"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
				<brArq:pdcCommandButton id="proxima"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
				<brArq:pdcCommandButton id="avancoRapido"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
				<brArq:pdcCommandButton id="ultima"
				  styleClass="bto1" style="margin-left: 3px;"
				  value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>

<%-- TABELA --%>
	
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	 <f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid columns="2" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="width:100%;text-align:left" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{manterVincContaContratoDebitoBean.voltarConsultaOperacao}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:650px;" >
				<br:brCommandButton id="btnConfirmar" disabled="" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_confirmar}"  action="#{manterVincContaContratoDebitoBean.confirmarInclusao}" onclick="javascript: if (!confirm('Confirma Vinculação?')) { desbloquearTela(); return false; }" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>


</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
