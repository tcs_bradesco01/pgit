<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j"%>

<brArq:form id="incVincManterVincContOperacoes" name="incVincManterVincContOperacoes" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="filtroRadio" value="#{manterVincContaContratoDebitoBean.radioArgumentoVincular}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">
	    		<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />	
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	  <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_cpfcnpj}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.cpfCnpjRepresentante}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_nomerazaosocial}:"/>						
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.nomeRazaoRepresentante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_grupo_economico}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.grupEconRepresentante}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_atividade_economica}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.ativEconRepresentante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_segmento}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.segRepresentante}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_sub_segmento}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.subSegRepresentante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.dsAgenciaGestora}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.dsGerenteResponsavel}"/>
			</br:brPanelGroup>					
		</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
     <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_cliente_cpfCnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.cpfCnpjParticipante}"/>
			</br:brPanelGroup>				

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_cliente_nomeRazaoSocial}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.nomeRazaoParticipante}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim>

	 	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_empresa}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.empresaContrato}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.tipoContrato}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_numero}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.numeroContrato}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_descricao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.descricaoContrato}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_situacao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.situacaoContrato}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_motivo}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVincContaContratoDebitoBean.motivoContrato}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo_participacao}:"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterVincContaContratoDebitoBean.participacaoContrato}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>

		
   <f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="0" />			
		</br:brPanelGroup>
		
	    <br:brPanelGroup>				 		   
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(9,'incVincManterVincContOperacoes','incVincManterVincContOperacoes:txtCnpj','incVincManterVincContOperacoes:txtFilial');" style="margin-right: 5" onkeypress="onlyNum();" size="11" maxlength="9" styleClass="HtmlInputTextBradesco" value="#{manterVincContaContratoDebitoBean.cdCpfCnpjFiltro}" readonly="#{manterVincContaContratoDebitoBean.radioArgumentoVincular != 0 || manterVincContaContratoDebitoBean.radioArgumentoVincular == null}"/>
				    <br:brInputText id="txtFilial" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(4,'incVincManterVincContOperacoes','incVincManterVincContOperacoes:txtFilial','incVincManterVincContOperacoes:txtControle');" style="margin-right: 5" onkeypress="onlyNum();" size="5" maxlength="4" styleClass="HtmlInputTextBradesco" value="#{manterVincContaContratoDebitoBean.cdFilialCnpjFiltro}"  readonly="#{manterVincContaContratoDebitoBean.radioArgumentoVincular != 0 || manterVincContaContratoDebitoBean.radioArgumentoVincular == null}"/>
					<br:brInputText id="txtControle" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" size="3" maxlength="2" styleClass="HtmlInputTextBradesco" value="#{manterVincContaContratoDebitoBean.cdControleCnpjFiltro}" readonly="#{manterVincContaContratoDebitoBean.radioArgumentoVincular != 0 || manterVincContaContratoDebitoBean.radioArgumentoVincular == null}"/>
			</br:brPanelGrid>
	    </br:brPanelGroup>				 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="filtroRadio" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" style="margin-right: 5" onkeypress="onlyNum();"  readonly="#{manterVincContaContratoDebitoBean.radioArgumentoVincular != 1}" value="#{manterVincContaContratoDebitoBean.cdCpfFiltro}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'incVincManterVincContOperacoes','incVincManterVincContOperacoes:txtCpf','incVincManterVincContOperacoes:txtControleCpf');" />
			    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" readonly="#{manterVincContaContratoDebitoBean.radioArgumentoVincular != 1}" value="#{manterVincContaContratoDebitoBean.cdControleCpfFiltro}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
		
	<br:brPanelGrid columns="8" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="2" />
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText readonly="#{manterVincContaContratoDebitoBean.radioArgumentoVincular != 2}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtBanco" styleClass="HtmlInputTextBradesco"  onkeypress="onlyNum();"
								value="#{manterVincContaContratoDebitoBean.bancoFiltro}" size="10" maxlength="3" >
					</br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" readonly="#{manterVincContaContratoDebitoBean.radioArgumentoVincular != 2}" id="txtAgencia" styleClass="HtmlInputTextBradesco"  onkeypress="onlyNum();"
										 value="#{manterVincContaContratoDebitoBean.agenciaFiltro}" size="12" maxlength="5" >
					</br:brInputText>	
				</br:brPanelGroup>									
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" readonly="#{manterVincContaContratoDebitoBean.radioArgumentoVincular != 2}" id="txtConta" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"
								 value="#{manterVincContaContratoDebitoBean.contaFiltro}" size="16" maxlength="13" >
					</br:brInputText>	
				</br:brPanelGroup>
	    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
			    <br:brPanelGroup>
					<br:brInputText readonly="#{manterVincContaContratoDebitoBean.radioArgumentoVincular != 2}" id="digito" styleClass="HtmlInputTextBradesco" 
								 value="#{manterVincContaContratoDebitoBean.digitoFiltro}" size="3" maxlength="1" >
					</br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_tipo_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu disabled="#{manterVincContaContratoDebitoBean.radioArgumentoVincular != 2}" id="tipoConta" value="#{manterVincContaContratoDebitoBean.tipoContaFiltro}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conContasManterContrato_label_combo_selecione}"/>
						<f:selectItems value="#{manterVincContaContratoDebitoBean.listaTipoConta}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>
		
		
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.conContasManterContrato_limpar_dados}" action="#{manterVincContaContratoDebitoBean.limparDadosIncVinc}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conContasManterContrato_consultar}" action="#{manterVincContaContratoDebitoBean.listarContaDebTarifa}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
				<app:scrollableDataTable id="dataTable" value="#{manterVincContaContratoDebitoBean.listaContaDebTarifa}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">

				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
						<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>	
					<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterVincContaContratoDebitoBean.itemSelecListaContaDebTarifa}">
					<f:selectItems value="#{manterVincContaContratoDebitoBean.listaControleRadioGridListaContaDebTarifa}"/>
						<a4j:support event="onclick" reRender="btnVincular" />
					</t:selectOneRadio>
					<t:radio for="sor" index="#{parametroKey}" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabRight" width="100px" >
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_club}" style="text-align:center;width:100" />
					</f:facet>
					<br:brOutputText value="#{result.cdParticipante}" />
				</app:scrollableColumn>						

				<app:scrollableColumn styleClass="colTabRight" width="140px" >
					<f:facet name="header">
						<br:brOutputText value="#{msgs.conContasManterContrato_grid_cpf_cnpj}" style="text-align:center;width:140" />
					</f:facet>
					<br:brOutputText value="#{result.cdCpfCnpj}" style="text-align:center;width:140" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="400px" >
					<f:facet name="header">
						<br:brOutputText value="#{msgs.conContasManterContrato_grid_nome_razao}" style="text-align:center;width:400" />
					</f:facet>
					<br:brOutputText value="#{result.nmParticipante}" style="text-align:center;width:400" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
					<f:facet name="header">
						<br:brOutputText value="#{msgs.conContasManterContrato_grid_banco}" style="text-align:center;width:180" />
					</f:facet>
					<br:brOutputText value="#{result.cdBanco}-#{result.dsBanco}" style="text-align:center;width:180" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
					<f:facet name="header">
						<br:brOutputText value="#{msgs.conContasManterContrato_grid_agencia}" style="text-align:center;width:180" />
					</f:facet>
					<br:brOutputText value="#{result.cdAgencia}-#{result.dsAgencia}" style="text-align:center;width:180" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabRight" width="100px" >
					<f:facet name="header">
						<br:brOutputText value="#{msgs.conContasManterContrato_grid_conta}" style="text-align:center;width:100" />
					</f:facet>
					<br:brOutputText value="#{result.cdConta}" />
					<br:brOutputText value="-" />				
					<br:brOutputText value="#{result.cdDigitoConta}" />			    
				</app:scrollableColumn>			  			  

				<app:scrollableColumn styleClass="colTabLeft" width="120px" >
			    	<f:facet name="header">
			     	 	<br:brOutputText value="#{msgs.conContasManterContrato_grid_tipo_conta}" style="text-align:center;width:150" />
			    	</f:facet>
			    	<br:brOutputText value="#{result.dsTipoConta}" style="text-align:center;width:150" />
				</app:scrollableColumn>
			</app:scrollableDataTable>

		</br:brPanelGroup>
		
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
	 	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
	 	
			<br:brPanelGroup style="text-align:left;width:100px;"  >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.conContasManterContrato_voltar}" action="#{manterVincContaContratoDebitoBean.voltarConsultaOperacao}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:right;width:650px;" >
				<br:brCommandButton id="btnLimpar" styleClass="bto1" style="margin-right:5px" value="#{msgs.conContasManterContrato_limpar}" action="#{manterVincContaContratoDebitoBean.limparDadosIncVinc}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnVincular" disabled="#{empty manterVincContaContratoDebitoBean.itemSelecListaContaDebTarifa}" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_vincular}" action="#{manterVincContaContratoDebitoBean.confirmarIncluirVinculacao}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</a4j:outputPanel>
	
		<t:inputHidden value="#{manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  	validarProxCamp(document.forms[1], document.getElementById('incVincManterVincContOperacoes:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>