<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conRegistrarAssinaturaContrato" name="conRegistrarAssinaturaContrato" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.obrigatoriedade}"/>
<h:inputHidden id="habilitaCampos" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaCampos}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" onclick="submit();" layout="spread" forceId="true" forceIdIndex="false" disabled="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.bloqueiaRadio}">  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
	</t:selectOneRadio>
	</a4j:region>
	
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'conRegistrarAssinaturaContrato','conRegistrarAssinaturaContrato:txtCnpj','conRegistrarAssinaturaContrato:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'conRegistrarAssinaturaContrato','conRegistrarAssinaturaContrato:txtFilial','conRegistrarAssinaturaContrato:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente }" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'conRegistrarAssinaturaContrato','conRegistrarAssinaturaContrato:txtCpf','conRegistrarAssinaturaContrato:txtControleCpf');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '2' || confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  border="0">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'conRegistrarAssinaturaContrato','conRegistrarAssinaturaContrato:txtBanco','conRegistrarAssinaturaContrato:txtAgencia');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'conRegistrarAssinaturaContrato','conRegistrarAssinaturaContrato:txtAgencia','conRegistrarAssinaturaContrato:txtConta');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCampos2" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.limparDadosCliente}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
    		
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado || confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{msgs.identificacaoClienteContrato_label_consultar_cliente}" action="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
	   		
	   		onclick="javascript:desbloquearTela(); 
	   		return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.identificacaoClienteContrato_cnpj}','#{msgs.identificacaoClienteContrato_cpf}', '#{msgs.identificacaoClienteContrato_label_nomeRazao}',
	   		'#{msgs.identificacaoClienteContrato_banco}','#{msgs.identificacaoClienteContrato_agencia}','#{msgs.identificacaoClienteContrato_conta}');" >		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolManutContratos_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="empresaGestora" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{!confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<f:selectItems  value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.listaEmpresaGestora}" />								
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_tipo_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoContrato" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{!confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<f:selectItems  value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.listaTipoContrato}" />
					</br:brSelectOneMenu>
					
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_numero}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.numeroFiltro}" size="12" maxlength="10" readonly="#{!confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}" 
					onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.identificacaoClienteContrato_agencia_operadora}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="agenciaOperadora" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.agenciaOperadoraFiltro}" size="10" maxlength="6" 
					readonly="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaCampos || !confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					<a4j:support event="onkeyup" reRender="gerenteId,gerenteNome,btnConsultarGerente" action="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.limparGerenteResponsavel}" />
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_gerente_responsavel}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="gerenteId" onkeypress="javascript:limparNomeGerente(document.forms[1]);" onkeydown="javascript:limparNomeGerente(document.forms[1]);" styleClass="HtmlInputTextBradesco" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.codigoFunc}" size="15" maxlength="9" 
					readonly="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaCampos || !confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato || confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.agenciaOperadoraFiltro == ''}">
						<brArq:commonsValidator type="integer" arg="#{msgs.identificacaoClienteContrato_gerente_responsavel}" server="false" client="true" />
						<a4j:support event="onkeyup" reRender="btnConsultarGerente,gerenteNome"  />
						<a4j:support event="onblur" action="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.carregaListaFuncionarios}" reRender="gerenteNome,formModalMessages" onsubmit="loadModalJQuery()" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					</br:brInputText>	
				</br:brPanelGroup>
	    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
			    <br:brPanelGroup>
					<br:brInputText id="gerenteNome" styleClass="HtmlInputTextBradesco" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.funcionarioDTO.dsFuncionario}" size="50" maxlength="50" disabled="true"  />				
				</br:brPanelGroup>		
				<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.limparDadosPesquisaContrato}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" 
			onclick="javascript:desbloquearTela(); 
			return validaCamposContratoSemSituacao(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}', '#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}',
	   		'#{msgs.identificacaoClienteContrato_tipo_contrato}', '#{msgs.identificacaoClienteContrato_mensagem_obrigatorio_sem_situacao}', '#{msgs.identificacaoClienteContrato_obrigatorio_consulta_gerente}');" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_consultar}" 
	   		action="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.consultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
		
	<f:verbatim><br></f:verbatim> 

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

			<app:scrollableDataTable id="dataTable" value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.listaGridPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemSelecionadoLista}">
						<f:selectItems value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.identificacaoClienteContrato_club_representante}" style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdClubRepresentante}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="170px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_cpf_cnpj_representante}"  style="width:170; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cnpjOuCpfFormatado}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_nome_razao_social}" style="width:300; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.nmRazaoSocialRepresentante}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_empresa_gestora}"  style="width:300; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsPessoaJuridica}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_tipo_contrato}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoContrato}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_numero}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrSequenciaContrato}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_situacao_contrato}" style="width:150; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoContrato}" />
				</app:scrollableColumn>	
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_agencia_operadora}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsAgenciaOperadora}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					<f:facet name="header">
					  <h:outputText value="#{msgs.identificacaoClienteContrato_gerente_responsavel}" style="width:200; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.descFuncionarioBradesco}" />
				</app:scrollableColumn>			
				
			</app:scrollableDataTable>
			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup >
				<br:brCommandButton id="btnLimpar" style="margin-left:599px; margin-right:5px"  disabled="false"  styleClass="bto1" value="#{msgs.regAssinaturaContrato_btnLimpar}" action="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.limparTela}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnRegistrar" disabled="#{empty confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.itemSelecionadoLista}" styleClass="bto1" value="#{msgs.regAssinaturaContrato_btnRegistrar}" action="#{confirmarAssinaturaContratoContingenciaBean.registrar}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	</a4j:outputPanel>
  	   
	<t:inputHidden value="#{confirmarAssinaturaContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conRegistrarAssinaturaContrato:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>  	  
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
