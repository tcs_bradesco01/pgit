<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conListasContasForm" name="conListasContasForm" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
	 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup />
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" width="50%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco}:" />
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agencia}:" />
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_conta}:" />
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brInputText styleClass="HtmlInputTextBradesco"
				onkeypress="onlyNum();" converter="javax.faces.Integer"
				value="#{incluirContratoContingenciaBean.entradaContaDTO.cdBanco}"
				size="4" maxlength="3" id="txtBanco"
				onkeyup="proximoCampo(3,'conListasContasForm','conListasContasForm:txtBanco','conListasContasForm:txtAgencia');"
				onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
			</br:brInputText>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brInputText styleClass="HtmlInputTextBradesco"
				onkeypress="onlyNum();" converter="javax.faces.Integer"
				value="#{incluirContratoContingenciaBean.entradaContaDTO.cdAgencia}"
				size="6" maxlength="5" id="txtAgencia"
				onkeyup="proximoCampo(5,'conListasContasForm','conListasContasForm:txtAgencia','conListasContasForm:txtConta');"
				onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
			</br:brInputText>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brInputText styleClass="HtmlInputTextBradesco"
				converter="javax.faces.Long" onkeypress="onlyNum();"
				value="#{incluirContratoContingenciaBean.entradaContaDTO.cdConta}"
				size="17" maxlength="13" id="txtConta"
				onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
			</br:brInputText>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0"
			cellspacing="0">
			<a4j:outputPanel id="panelBtoConsultar"
				style="width:100%; text-align: right">
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1"
					value="#{msgs.label_botao_limpar_dados}"
					action="#{incluirContratoContingenciaBean.limparCamposConta}"
					style="margin-right:5px">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btoConsultar" styleClass="bto1"
					value="#{msgs.label_consultar}"
					action="#{incluirContratoContingenciaBean.consultarConta}"
					style="cursor:hand;"
					onmouseover="javascript:alteraBotao('visualizacao', this.id);"
					onmouseout="javascript:alteraBotao('normal', this.id);"
					onclick="if (!validarCamposObrigatoriosConta('conListasContasForm','#{msgs.label_ocampo}','#{msgs.label_necessario}','#{msgs.label_banco}','#{msgs.label_agencia}','#{msgs.label_conta}')) { desbloquearTela(); return false; };">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</a4j:outputPanel>
		</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:25px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
			<app:scrollableDataTable id="dataTable" value="#{incluirContratoContingenciaBean.listaGridListarContas}" 
			var="result" rows="50" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
						  	<t:selectBooleanCheckbox disabled="#{empty incluirContratoContingenciaBean.listaGridListarContas}" id="checkSelecionarTodos" styleClass="HtmlSelectOneRadioBradesco" value="#{incluirContratoContingenciaBean.opcaoChecarTodosContas}" onclick="selecionarTodasContas(document.forms[1],this);">
							</t:selectBooleanCheckbox>
					    </f:facet>
						<t:selectBooleanCheckbox  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" value="#{result.check}">
							<f:selectItems value="#{incluirContratoContingenciaBean.listaControleGridListarContas}"/>						
							<a4j:support event="onclick" reRender="panelBotoes" action="#{incluirContratoContingenciaBean.habilitarPanelBotoesContas}"/>
						</t:selectBooleanCheckbox>
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="150px" >
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_club}" style="text-align:center;width:150" />
						</f:facet>
						<br:brOutputText value="#{result.cdPessoa}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="200px" >
					    <f:facet name="header">
 				        	<br:brOutputText value="#{msgs.label_cpf_cnpj_participante}" style="width:200px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cpfCnpjFormatado}"/>
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >
					    <f:facet name="header">
 				        	<br:brOutputText value="#{msgs.label_nome_razaoSocial_participante}" style="width:200px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.nmParticipante}"/>
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >
					    <f:facet name="header">
 				        	<br:brOutputText value="#{msgs.label_banco}" style="width:250px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.bancoFormatado}"/>
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >
					    <f:facet name="header">
 				        	<br:brOutputText value="#{msgs.label_agencia}" style="width:200px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.agenciaFormatada}"/>
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabRight" width="200px" >
					    <f:facet name="header">
 				        	<br:brOutputText value="#{msgs.label_conta}" style="width:200px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.contaFormatada}"/>
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >
					    <f:facet name="header">
 				        	<br:brOutputText value="#{msgs.label_tipo_de_conta}" style="width:200px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsTipoConta}"/>
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" width="100%" style="text-align:center"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable"
					actionListener="#{incluirContratoContingenciaBean.paginarTelaListarContas}">
					<f:facet name="first">
						<brArq:pdcCommandButton id="primeira" styleClass="bto1"
							value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" />
					</f:facet>
					<f:facet name="fastrewind">
						<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_retrocesso}"
							title="#{msgs.label_retrocesso_msg}" />
					</f:facet>
					<f:facet name="previous">
						<brArq:pdcCommandButton id="anterior" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_anterior}"
							title="#{msgs.label_anterior_msg}" />
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton id="proxima" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_proxima}"
							title="#{msgs.label_proxima_msg}" />
					</f:facet>
					<f:facet name="fastforward">
						<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_avanco}"
							title="#{msgs.label_avanco_msg}" />
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton id="ultima" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_ultima}"
							title="#{msgs.label_ultima_msg}" />
					</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		
		<f:verbatim><hr class="lin"></f:verbatim>
		
		<a4j:outputPanel id="panelBotoes" style="width: 100%; " ajaxRendered="true">	
	   		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		    	<br:brPanelGroup style="text-align:left;width:150px"  >
					<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.label_voltar}" action="#{incluirContratoContingenciaBean.voltar}">	
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			
		    	<br:brPanelGroup style="text-align:right;width:600px" >
					<br:brCommandButton id="btnSelecionar" disabled="#{!incluirContratoContingenciaBean.panelBotoesContas}" action="#{incluirContratoContingenciaBean.selecionarConta}" styleClass="bto1" value="#{msgs.btn_selecionar}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
	       </br:brPanelGrid>	
	    </a4j:outputPanel>  	
	</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>