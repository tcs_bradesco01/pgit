<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<brArq:form id="incContratoPagamentosIntegrados3" name="incContratoPagamentosIntegrados3">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		 
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{incluirContratoContingenciaBean.dsEmpresaGestoraContrato}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{incluirContratoContingenciaBean.dsTipoContrato}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_identificacao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{incluirContratoContingenciaBean.dsIdentificacaoContrato}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.saidaListarClubesClientes.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.saidaListarClubesClientes.dsNomeRazao}"/>    
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_conta}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
				<app:scrollableDataTable id="dataTableContas" value="#{incluirContratoContingenciaBean.listaGridListarContasSelecionadas}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >
					    <f:facet name="header">
	 				        	<br:brOutputText value="#{msgs.label_banco}" style="width:250px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.bancoFormatado}"/>
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >
					    <f:facet name="header">
	 				        	<br:brOutputText value="#{msgs.label_agencia}" style="width:200px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.agenciaFormatada}"/>
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabRight" width="200px" >
					    <f:facet name="header">
	 				        	<br:brOutputText value="#{msgs.label_conta}" style="width:200px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.contaFormatada}"/>
					</app:scrollableColumn>	
					<app:scrollableColumn styleClass="colTabLeft" width="200px" >
					    <f:facet name="header">
	 				        	<br:brOutputText value="#{msgs.label_tipo_de_conta}" style="width:200px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsTipoConta}"/>
					</app:scrollableColumn>																							
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScrollerContas" for="dataTableContas" actionListener="#{incluirContratoContingenciaBean.pesquisarTelaListarContas}" >
					<f:facet name="first">
						<brArq:pdcCommandButton id="primeiraContas" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" />
					</f:facet>
					<f:facet name="fastrewind">
					    <brArq:pdcCommandButton id="retrocessoRapidoContas" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" />
					</f:facet>
					<f:facet name="previous">
					    <brArq:pdcCommandButton id="anteriorContas" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" />
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton id="proximaContas" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" />
					</f:facet>
					<f:facet name="fastforward">
					    <brArq:pdcCommandButton id="avancoRapidoContas" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" />
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton id="ultimaContas" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" /> 
					</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>		
    
		<f:verbatim><hr class="lin"> </f:verbatim>			

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
				<app:scrollableDataTable id="dataTable" value="#{incluirContratoContingenciaBean.listaGridTipoServicoSelecionados}" var="result" rows="15" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabLeft" width="700px" >
					    <f:facet name="header">
 				        	<br:brOutputText value="#{msgs.label_tipo_servico}" style="width:700px; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdProdutoServicoRelacionado}"/>
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{incluirContratoContingenciaBean.pesquisarTelaTipoServico}" >
					<f:facet name="first">
						<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" />
					</f:facet>
					<f:facet name="fastrewind">
					    <brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" />
					</f:facet>
					<f:facet name="previous">
					    <brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" />
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"  />
					</f:facet>
					<f:facet name="fastforward">
					    <brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" />
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" /> 
					</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{incluirContratoContingenciaBean.voltarConfirmacao}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px" >
				<br:brCommandButton id="btnConfirmar" styleClass="bto1" value="#{msgs.btn_confirmar}" 
				 
				action="#{incluirContratoContingenciaBean.confirmarIncluirContrato}" 
				onclick="javascript: if (!confirm('#{msgs.label_confirma_inclusao}')) { desbloquearTela(); return false; }">
					<brArq:submitCheckClient/>
				</br:brCommandButton>		
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>