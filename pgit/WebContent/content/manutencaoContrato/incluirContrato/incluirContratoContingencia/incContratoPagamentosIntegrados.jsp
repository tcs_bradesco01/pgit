<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incContratoPagamentosIntegradosForm"
	name="incContratoPagamentosIntegradosForm">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"
		cellspacing="0">

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.label_dados_contrato}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>

					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
							value="#{msgs.label_empresa_gestora_contrato}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="empresaGestoraContrato"
							value="#{incluirContratoContingenciaBean.cdEmpresaGestoraContrato}"
							styleClass="HtmlSelectOneMenuBradesco"
							disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}">
							<f:selectItems
								value="#{incluirContratoContingenciaBean.listaEmpresaGestora}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
							value="#{msgs.label_tipo_contrato}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="tipoContrato"
							value="#{incluirContratoContingenciaBean.cdTipoContrato}"
							styleClass="HtmlSelectOneMenuBradesco"
							disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}">
							<f:selectItems
								value="#{incluirContratoContingenciaBean.listaTipoContrato}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>

					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
							value="#{msgs.label_agencia_gestora_contrato}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brInputText id="txtAgenciaGestora"
							onkeypress="javascript:onlyNum();"
							onkeydown="javascript:limparAgenciaGestora(document.forms[1]);"
							styleClass="HtmlInputTextBradesco"
							value="#{incluirContratoContingenciaBean.cdAgenciaGestora}"
							size="6" maxlength="5"
							disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}">
							<a4j:support event="onblur"
								action="#{incluirContratoContingenciaBean.buscarAgenciaGestora}"
								reRender="txtDsAgenciaGestora, formModalMessages, gerenteId,gerenteNome,btnConsultarGerente"
								onsubmit="loadModalJQuery()"
								oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');focoGerenteResponsavel(document.forms[1]);"/>
						</br:brInputText>
					</br:brPanelGroup>

					<br:brPanelGrid columns="1" style="margin-right:5px"
						cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>	
				
				<<br:brPanelGroup>
						<br:brInputText disabled="true" id="txtDsAgenciaGestora"
							styleClass="HtmlInputTextBradesco"
							value="#{incluirContratoContingenciaBean.bancoAgenciaDTO.dsAgencia}"
							size="45" maxlength="45" />
					</br:brPanelGroup>


				</br:brPanelGrid>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>

					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
							value="#{msgs.label_gerente_responsavel}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brInputText id="gerenteId"
							disabled="#{ incluirContratoContingenciaBean.bancoAgenciaDTO.dsAgencia == null || incluirContratoContingenciaBean.cdAgenciaGestora == null || incluirContratoContingenciaBean.bancoAgenciaDTO.dsAgencia == '' || incluirContratoContingenciaBean.cdAgenciaGestora == '' || incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
							onkeypress="javascript:limparNomeGerente(document.forms[1]);onlyNum();"
							styleClass="HtmlInputTextBradesco"
							value="#{incluirContratoContingenciaBean.codigoFunc}" size="11"
							maxlength="9">
							<a4j:support event="onblur"
								action="#{incluirContratoContingenciaBean.buscaFuncionarios}"
								reRender="gerenteNome, formModalMessages"
								onsubmit="loadModalJQuery()"
								oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
						</br:brInputText>
					</br:brPanelGroup>

					<br:brPanelGrid columns="1" style="margin-right:5px"
						cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGroup>
						<br:brInputText disabled="true" id="gerenteNome"
							styleClass="HtmlInputTextBradesco"
							value="#{incluirContratoContingenciaBean.funcionarioDTO.dsFuncionario}"
							size="45" maxlength="45" />
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>

					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
							value="#{msgs.label_idioma}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="idioma"
							value="#{incluirContratoContingenciaBean.cdIdioma}"
							styleClass="HtmlSelectOneMenuBradesco"
							disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}">
							<f:selectItem itemValue="0"
								itemLabel="#{msgs.identificacaoClienteContrato_selecione}" />
							<f:selectItems
								value="#{incluirContratoContingenciaBean.listaIdioma}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
							value="#{msgs.label_moeda}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="moeda"
							value="#{incluirContratoContingenciaBean.cdMoeda}"
							styleClass="HtmlSelectOneMenuBradesco"
							disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}">
							<f:selectItem itemValue="0"
								itemLabel="#{msgs.identificacaoClienteContrato_selecione}" />
							<f:selectItems
								value="#{incluirContratoContingenciaBean.listaMoeda}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"
				style="margin-left:20px">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
							value="#{msgs.label_setor}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="setor"
							value="#{incluirContratoContingenciaBean.cdSetor}"
							styleClass="HtmlSelectOneMenuBradesco"
							disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}">
							<f:selectItem itemValue="0"
								itemLabel="#{msgs.identificacaoClienteContrato_selecione}" />
							<f:selectItems
								value="#{incluirContratoContingenciaBean.listaSetor}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>

					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
							value="#{msgs.label_identificacao_contrato}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco"
							value="#{incluirContratoContingenciaBean.dsIdentificacaoContrato}"
							size="55" maxlength="40" id="txtIdentificacaoContrato"
							disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" />
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.label_identificacao_cliente}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<t:selectOneRadio id="rdoFiltroCliente"
			value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado}"
			styleClass="HtmlSelectOneRadioBradesco" layout="spread"
			forceId="true" forceIdIndex="false"
			disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.bloqueiaRadio}">
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="" />
			<a4j:support event="onclick"
				reRender="panelBtoConsultarCliente,painelBcoAgCc"
				action="#{identificacaoClienteContratoBean.valorizaBanco}" />
		</t:selectOneRadio>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<t:radio for="rdoFiltroCliente" index="0" />

			<a4j:outputPanel id="panelCnpj" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.label_cnpj}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
					<br:brInputText id="txtCnpj"
						onkeyup="proximoCampo(9,'incContratoPagamentosIntegradosForm','incContratoPagamentosIntegradosForm:txtCnpj','incContratoPagamentosIntegradosForm:txtFilial');"
						style="margin-right: 5" converter="javax.faces.Long"
						onkeypress="onlyNum();" size="11" maxlength="9"
						disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
						styleClass="HtmlInputTextBradesco"
						value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					<br:brInputText id="txtFilial"
						onkeyup="proximoCampo(4,'incContratoPagamentosIntegradosForm','incContratoPagamentosIntegradosForm:txtFilial','incContratoPagamentosIntegradosForm:txtControle');"
						style="margin-right: 5" converter="javax.faces.Integer"
						onkeypress="onlyNum();" size="5" maxlength="4"
						disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
						styleClass="HtmlInputTextBradesco"
						value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					<br:brInputText id="txtControle" style="margin-right: 5"
						converter="javax.faces.Integer" onkeypress="onlyNum();" size="3"
						maxlength="2"
						disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
						styleClass="HtmlInputTextBradesco"
						value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCnpj}"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<t:radio for="rdoFiltroCliente" index="1" />

			<a4j:outputPanel id="panelCpf" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.label_cpf}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brInputText styleClass="HtmlInputTextBradesco"
						style="margin-right: 5" converter="javax.faces.Long"
						onkeypress="onlyNum();"
						disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente }"
						value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpf}"
						size="11" maxlength="9" id="txtCpf"
						onkeyup="proximoCampo(9,'incContratoPagamentosIntegradosForm','incContratoPagamentosIntegradosForm:txtCpf','incContratoPagamentosIntegradosForm:txtControleCpf');"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					<br:brInputText styleClass="HtmlInputTextBradesco"
						converter="javax.faces.Integer" onkeypress="onlyNum();"
						disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
						value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCpf}"
						maxlength="2" size="3" id="txtControleCpf"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
			<t:radio for="rdoFiltroCliente" index="2" />

			<a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.label_nomeRazaoSocial}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brInputText styleClass="HtmlInputTextBradesco"
					disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '2' || incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
					value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}"
					size="71" maxlength="70" id="txtNomeRazaoSocial" />
			</a4j:outputPanel>

		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="4" id="painelBcoAgCc" cellpadding="0"
			cellspacing="0" border="0">
			<t:radio for="rdoFiltroCliente" index="3" />

			<a4j:outputPanel id="panelBanco" ajaxRendered="true">

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.label_banco}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brInputText styleClass="HtmlInputTextBradesco"
					onkeypress="onlyNum();" converter="javax.faces.Integer"
					disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
					value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdBanco}"
					size="4" maxlength="3" id="txtBanco"
					onkeyup="proximoCampo(3,'incContratoPagamentosIntegradosForm','incContratoPagamentosIntegradosForm:txtBanco','incContratoPagamentosIntegradosForm:txtAgencia');"
					onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					
			</a4j:outputPanel>

			<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.label_agencia}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brInputText styleClass="HtmlInputTextBradesco"
					onkeypress="onlyNum();" converter="javax.faces.Integer"
					disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
					value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}"
					size="6" maxlength="5" id="txtAgencia"
					onkeyup="proximoCampo(5,'incContratoPagamentosIntegradosForm','incContratoPagamentosIntegradosForm:txtAgencia','incContratoPagamentosIntegradosForm:txtConta');"
					onblur="validaCampoNumerico(this\, '#{msgs.label_campo_contem_apenas_numeros}');">
					<a4j:support event="onblur" reRender="btoConsultarCliente" limitToList="true" />
				</br:brInputText>
			</a4j:outputPanel>

			<a4j:outputPanel id="panelConta" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.label_conta}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brInputText styleClass="HtmlInputTextBradesco"
					converter="javax.faces.Long" onkeypress="onlyNum();"
					disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
					value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdContaBancaria}"
					size="17" maxlength="13" id="txtConta"
					onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					<a4j:support event="onblur" reRender="btoConsultarCliente" limitToList="true" />
				</br:brInputText>
			</a4j:outputPanel>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0"
			cellspacing="0">
			<a4j:outputPanel id="panelBtoConsultarCliente"
				style="width:100%; text-align: right">
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1"
					disabled="#{empty incluirContratoContingenciaBean.identificacaoClienteContratoBean.itemFiltroSelecionado}"
					value="#{msgs.identificacaoClienteContrato_limpar_dados}"
					action="#{incluirContratoContingenciaBean.limparDados}"
					style="margin-right:5px">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btoConsultarCliente" styleClass="bto1"
					disabled="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.habBtoConsultarCliente}"
					value="#{msgs.identificacaoClienteContrato_label_consultar_cliente}"
					action="#{incluirContratoContingenciaBean.consultarCliente}"
					style="cursor:hand;"
					onmouseover="javascript:alteraBotao('visualizacao', this.id);"
					onmouseout="javascript:alteraBotao('normal', this.id);"
					onclick="javascript:desbloquearTela(); return 
	   											checarObrigatoriedade(document.forms[1], '#{msgs.label_ocampo}',
	    																						'#{msgs.label_necessario}',
	    																						'#{msgs.label_empresa_gestora_contrato}',
	    																						'#{msgs.label_tipo_contrato}',
	    																						'#{msgs.label_cod_agencia_gestora_contrato}',
	    																						'#{msgs.label_desc_agencia_gestora_contrato}',
	    																						'#{msgs.label_cod_gerente_responsavel}',
	    																						'#{msgs.label_desc_gerente_responsavel}',
	    																						'#{msgs.label_idioma}',
	    																						'#{msgs.label_moeda}',
	    																						'#{msgs.label_setor}',
		   																						'#{msgs.label_identificacao_contrato}',
	   												   											'#{msgs.identificacaoClienteContrato_cnpj}',
	   											                           						'#{msgs.identificacaoClienteContrato_cpf}',
							   											                        '#{msgs.identificacaoClienteContrato_label_nomeRazao}',
							   											                        '#{msgs.identificacaoClienteContrato_banco}',
							   											                        '#{msgs.identificacaoClienteContrato_agencia}',
							   											                        '#{msgs.identificacaoClienteContrato_conta}');">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</a4j:outputPanel>
		</br:brPanelGrid>


		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
			value="#{msgs.identificacaoClienteContrato_label_cliente}:" />
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"
					style="margin-right: 5" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco"
					value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
					value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.saidaListarClubesClientes.cnpjOuCpfFormatado}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="width:20px; margin-bottom:5px">
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"
					style="margin-right: 5" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco"
					value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
					value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.saidaListarClubesClientes.dsNomeRazao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>




		<br:brPanelGrid style="text-align:right" columns="1" cellpadding="0"
			cellspacing="0" width="100%">
			<br:brPanelGroup>

				<br:brCommandButton id="btoConsultarConta" styleClass="bto1"
					disabled="#{incluirContratoContingenciaBean.bloqueiaConta}"
					value="#{msgs.label_consultar_conta}"
					action="#{incluirContratoContingenciaBean.consultarConta}"
					style="cursor:hand;"
					onmouseover="javascript:alteraBotao('visualizacao', this.id);"
					onmouseout="javascript:alteraBotao('normal', this.id);">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>


		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
			value="#{msgs.label_conta}:" />

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup
				style="overflow-x:auto; overflow-y:auto; width:750px; height:170">
				<app:scrollableDataTable id="dataTable"
					value="#{incluirContratoContingenciaBean.listaGridListarContasSelecionadas}"
					var="result" rows="10" rowIndexVar="parametroKey"
					rowClasses="tabela_celula_normal, tabela_celula_destaque"
					width="100%">
					<app:scrollableColumn styleClass="colTabRight" width="150px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_club}"
								style="text-align:center;width:150" />
						</f:facet>
						<br:brOutputText value="#{result.cdPessoa}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="200px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_cpf_cnpj_participante}"
								style="width:200px; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.cpfCnpjFormatado}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="200px">
						<f:facet name="header">
							<br:brOutputText
								value="#{msgs.label_nome_razaoSocial_participante}"
								style="width:200px; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.nmParticipante}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="250px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_banco}"
								style="width:250px; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.bancoFormatado}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="200px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_agencia}"
								style="width:200px; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.agenciaFormatada}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabRight" width="200px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_conta}"
								style="width:200px; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.contaFormatada}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" width="200px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_tipo_de_conta}"
								style="width:200px; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsTipoConta}" />
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" style="text-align:center"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable"
					actionListener="#{incluirContratoContingenciaBean.pesquisarTelaListarContas}">
					<f:facet name="first">
						<brArq:pdcCommandButton id="primeira" styleClass="bto1"
							value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" />
					</f:facet>
					<f:facet name="fastrewind">
						<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_retrocesso}"
							title="#{msgs.label_retrocesso_msg}" />
					</f:facet>
					<f:facet name="previous">
						<brArq:pdcCommandButton id="anterior" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_anterior}"
							title="#{msgs.label_anterior_msg}" />
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton id="proxima" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_proxima}"
							title="#{msgs.label_proxima_msg}" />
					</f:facet>
					<f:facet name="fastforward">
						<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_avanco}"
							title="#{msgs.label_avanco_msg}" />
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton id="ultima" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_ultima}"
							title="#{msgs.label_ultima_msg}" />
					</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>



		<br:brPanelGrid style="text-align:right" columns="1" cellpadding="0"
			cellspacing="0" width="100%">
			<br:brPanelGroup>
				<br:brCommandButton id="btnAvancar" styleClass="bto1"
					value="#{msgs.label_avan�ar}"
					action="#{incluirContratoContingenciaBean.incluirContrato}"
					disabled="#{empty incluirContratoContingenciaBean.listaGridListarContasSelecionadas}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<t:inputHidden
			value="#{incluirContratoContingenciaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
			id="hiddenFlagPesquisa"></t:inputHidden>

		<f:verbatim>
			<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('incContratoPagamentosIntegradosForm:hiddenFlagPesquisa').value);
	  	</script>
		</f:verbatim>
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>