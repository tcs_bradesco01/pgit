<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="imprimirAdtivoContrato" name="imprimirAdtivoContrato" >

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid styleClass="colTabGeral" align="center" columns="1" width="50%" cellpadding="0" cellspacing="0">
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup style="text-align:center;">
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_inclusao_realizada_sucesso_contrato}"/>
		</br:brPanelGroup>			
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup style="margin-left: 100px"> 
		</br:brPanelGroup>
			
		<br:brPanelGroup style="text-align:left;" >

				<a4j:commandButton id="btnSim" styleClass="bto1" value="  #{msgs.label_sim}  " disabled="false" >
					<a4j:support event="onclick"
						action="#{manterFormalizacaoAlteracaoContratoBean.imprimirVersaoContrato}"
						oncomplete="document.getElementById('imprimirAdtivoContrato:btnImprimirDocumentoHidden').click()" />
				</a4j:commandButton>
				<br:brCommandButton id="btnImprimirDocumentoHidden"
						value=""
						action="#{manterFormalizacaoAlteracaoContratoBean.imprimirContratoFinal}"
						styleClass="HtmlCommandButtonBradesco"
						style="display: none"
						onclick="desbloquearTela()">
						<brArq:submitCheckClient />
				</br:brCommandButton>

				<br:brPanelGroup style="width: 30px;"> </br:brPanelGroup>
				
				<br:brCommandButton id="btnNao" styleClass="bto1"
					value="  #{msgs.label_nao}  "
					action="#{manterFormalizacaoAlteracaoContratoBean.voltarImpAditivo}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				
			</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	<br:brPanelGrid align="center" columns="1" width="40%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brCommandLink style="margin-left: 130px" action="#{manterFormalizacaoAlteracaoContratoBean.voltarImpAditivo}">
				<br:brGraphicImage url="/images/bt_voltar.gif" />
			</br:brCommandLink>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()"
		onstop="desbloquearTela()" />
	
</br:brPanelGrid>
</brArq:form>