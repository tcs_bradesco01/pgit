<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incConfManterForAltContrato" name="incConfManterForAltContrato" >

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_cliente_representante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_nome_razao_social}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.cpfCnpj}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.nomeRazaoSocial}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_empresa_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.empresaGestora}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.numero}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.descricao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.motivoDesc}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.participacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incConfManterForAltContrato_manutencao_pendentes_geracao_aditivo}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incConfManterForAltContrato_instrucao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_gerar_versao}"/>			
		</br:brPanelGroup>					
	</br:brPanelGrid>	
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0"
		rendered="#{manterFormalizacaoAlteracaoContratoBean.saidaConRelacaoConManutencoes.cdFormaAutorizacaoPagamento != 0
			|| manterFormalizacaoAlteracaoContratoBean.saidaConRelacaoConManutencoes.cdUnidadeOrganizacional != 0}">
		<f:verbatim><hr class="lin"></f:verbatim>
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_contrato}:"/>
		</br:brPanelGroup>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" 
			rendered="#{manterFormalizacaoAlteracaoContratoBean.saidaConRelacaoConManutencoes.cdFormaAutorizacaoPagamento != 0}"  >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_forma_autorizacao_pagamento}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
				value="#{manterFormalizacaoAlteracaoContratoBean.saidaConRelacaoConManutencoes.dsFormaAutorizacaoPagamento}" />
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0"
			rendered="#{manterFormalizacaoAlteracaoContratoBean.saidaConRelacaoConManutencoes.cdFormaAutorizacaoPagamento != 0
			&& manterFormalizacaoAlteracaoContratoBean.saidaConRelacaoConManutencoes.cdUnidadeOrganizacional != 0}">
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"
			rendered="#{manterFormalizacaoAlteracaoContratoBean.saidaConRelacaoConManutencoes.cdUnidadeOrganizacional != 0}" >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
				value="#{manterFormalizacaoAlteracaoContratoBean.saidaConRelacaoConManutencoes.unidadeOrganizacionalFormatada}" />
		</br:brPanelGrid>	
	</br:brPanelGrid>		
	
	<a4j:outputPanel id="painelParticipantes" style="width: 100%; text-align: right" ajaxRendered="true" rendered="#{manterFormalizacaoAlteracaoContratoBean.qtdeParticipantes != 0}">		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detManterForAltContrato_participantes}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		 <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
				
				<app:scrollableDataTable id="dataTableParticipantes" value="#{manterFormalizacaoAlteracaoContratoBean.listaGridParticipantesIncluir}" var="result" rows="#{manterFormalizacaoAlteracaoContratoBean.qtdeParticipantes}" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">			
				
				<app:scrollableColumn width="135px" styleClass="colTabRight" >
				    <f:facet name="header">
				       <br:brOutputText value="#{msgs.detManterForAltContrato_cnpj_cpf_titular}" style="width:135; text-align:center" />
				    </f:facet>
				     <br:brOutputText value="#{result.nrCpfCnpjParticipante}" />
				 </app:scrollableColumn>
				 
				 <app:scrollableColumn width="450px" styleClass="colTabLeft" >	
				    <f:facet name="header">
				       <br:brOutputText value="#{msgs.detManterForAltContrato_nome_razao_social}" style="width:450; text-align:center" />
				    </f:facet>
				     <br:brOutputText value="#{result.nmRazaoSocialParticipante}" />
				 </app:scrollableColumn>
				 
				 <app:scrollableColumn width="130px" styleClass="colTabLeft" >
				    <f:facet name="header">
				       <br:brOutputText value="#{msgs.detManterForAltContrato_tipo_participacao}" style="width:130; text-align:center" />
				    </f:facet>
				     <br:brOutputText value="#{result.dsTipoParticipacaoPessoa}" />
				 </app:scrollableColumn>
		
				<app:scrollableColumn width="80px" styleClass="colTabLeft" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.detManterForAltContrato_acao}" style="width:80; text-align:center"/>
				    </f:facet>
				   <br:brOutputText value="#{result.dsAcaoManutencaoParticipante}"/>
				 </app:scrollableColumn>
				</app:scrollableDataTable>				
			</br:brPanelGroup> 
		</br:brPanelGrid>	
	
	</a4j:outputPanel>			
	
	<a4j:outputPanel id="painelVinculos" style="width: 100%; text-align: right" ajaxRendered="true" rendered="#{manterFormalizacaoAlteracaoContratoBean.qtdeVinculos!= 0}">			
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detManterForAltContrato_contas}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		 
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
				
				<app:scrollableDataTable id="dataTableContas" value="#{manterFormalizacaoAlteracaoContratoBean.listaGridVinculosIncluir}" var="result" rows="#{manterFormalizacaoAlteracaoContratoBean.qtdeVinculos}" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">			
				
				<app:scrollableColumn width="165px" styleClass="colTabRight" >
					<f:facet name="header">
						<br:brOutputText value="#{msgs.detManterForAltContrato_cnpj_cpf_participantes}" style="width:165; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.nrCpfCnpjConta}" />
				</app:scrollableColumn>

				<app:scrollableColumn width="340px" styleClass="colTabLeft" >
					<f:facet name="header">
						<br:brOutputText value="#{msgs.detManterForAltContrato_nome_razao_social_participante}" style="width:340; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.nmRazaoSocialConta}" />
				</app:scrollableColumn>

				<app:scrollableColumn width="165px" styleClass="colTabLeft" >
					<f:facet name="header">
						<br:brOutputText value="#{msgs.detManterForAltContrato_banco}" style="width:165; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsBanco}" />
				</app:scrollableColumn>

				<app:scrollableColumn width="180px" styleClass="colTabLeft" >
					<f:facet name="header">
						<br:brOutputText value="#{msgs.detManterForAltContrato_agencia}" style="width:180; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsAgencia}" />
				</app:scrollableColumn>

				<app:scrollableColumn width="100px" styleClass="colTabLeft" >
					<f:facet name="header">
						<br:brOutputText value="#{msgs.detManterForAltContrato_conta}" style="width:100; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsConta}" />
				</app:scrollableColumn>

				<app:scrollableColumn width="100px" styleClass="colTabLeft" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.detManterForAltContrato_acao_2}" style="width:100; text-align:center"/>
				    </f:facet>
				   <br:brOutputText value="#{result.dsAcaoManutencaoConta}"/>
				 </app:scrollableColumn>
				</app:scrollableDataTable>				
			</br:brPanelGroup> 
		</br:brPanelGrid>
	
	</a4j:outputPanel>				
	
	<a4j:outputPanel id="painelServicos" style="width: 100%; text-align: right" ajaxRendered="true" rendered="#{manterFormalizacaoAlteracaoContratoBean.qtdeGridServicos != 0}">				
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detManterForAltContrato_servicos}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
				
				<app:scrollableDataTable id="dataTableServicos" value="#{manterFormalizacaoAlteracaoContratoBean.listaGridServicos}" var="result" rows="#{manterFormalizacaoAlteracaoContratoBean.qtdeGridServicos}" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">			
				
			<app:scrollableColumn  width="315px" styleClass="colTabLeft" >
				    <f:facet name="header">
				       <br:brOutputText value="#{msgs.detManterForAltContrato_produto_servico}" style="width:315; text-align:center" />
				    </f:facet>
				     <br:brOutputText value="#{result.dsProdutoServicoOperacao}" />
				 </app:scrollableColumn>
				 
				 <app:scrollableColumn  width="315px" styleClass="colTabLeft" >
				    <f:facet name="header">
				       <br:brOutputText value="#{msgs.detManterForAltContrato_operacoes}" style="width:315; text-align:center" />
				    </f:facet>
				     <br:brOutputText value="#{result.dsProdutoServicoRelacionado}" />
				 </app:scrollableColumn>
		
				<app:scrollableColumn  width="80px" styleClass="colTabLeft" >
				    <f:facet name="header">
				      <h:outputText value="#{msgs.detManterForAltContrato_acao_3}" style="width:80; text-align:center"/>
				    </f:facet>
				   <br:brOutputText value="#{result.dsAcaoManutencaoServico}"/>
				 </app:scrollableColumn>
				</app:scrollableDataTable>				
			</br:brPanelGroup> 
		</br:brPanelGrid>
	</a4j:outputPanel>					
	
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
	<f:verbatim><hr class="lin"> </f:verbatim>
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" disabled="false" styleClass="bto1" value="#{msgs.incConfManterForAltContrato_botao_voltar}" action="#{manterFormalizacaoAlteracaoContratoBean.voltarInc}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>		
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="width:350px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	 
			 
			<br:brPanelGroup style="text-align:right;width:250px">  
				 <br:brCommandButton id="btnConfirmar"  styleClass="bto1"  value="#{msgs.incConfManterForAltContrato_botao_confirmar}" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela(); return false; }" action="#{manterFormalizacaoAlteracaoContratoBean.confirmarIncluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>			
			</br:brPanelGroup>
			
		</br:brPanelGrid>	
	</a4j:outputPanel>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>