<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="forManterFormalizacaoAlteracaoContrato" name="forManterFormalizacaoAlteracaoContrato" >


<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_cliente_representante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_nome_razao_social}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.cpfCnpj}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.nomeRazaoSocial}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_empresa_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.empresaGestora}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_numero_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.numero}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.descricao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.motivoDesc}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterFormalizacaoAlteracaoContratoBean.participacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>		
		
			
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_label_argumento_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="radioSelecaoFiltro" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro}" >  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
	            <a4j:support event="onclick" action="#{manterFormalizacaoAlteracaoContratoBean.limparDados}"  reRender="numeroFiltro, cboSituacaoFiltro,panelDataInicioFiltro,panelDataFimFiltro, btnConsultar"/>				
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioSelecaoFiltro" index="0" />	
		</br:brPanelGroup>
		
	 	<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_numero_versao_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brInputText id="numeroFiltro" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" value="#{manterFormalizacaoAlteracaoContratoBean.numeroFiltro}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
						disabled="#{manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro != 0  || 
									manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro == '' || 
									manterFormalizacaoAlteracaoContratoBean.desabilitaConsulta }"
						maxlength="10" />

				</br:brPanelGrid>
			</br:brPanelGroup>	
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="2" style="margin-top:6px;" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioSelecaoFiltro" index="1" />	
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_situacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					<br:brSelectOneMenu id="cboSituacaoFiltro"  disabled="#{manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro != 1 || manterFormalizacaoAlteracaoContratoBean.desabilitaConsulta }" value="#{manterFormalizacaoAlteracaoContratoBean.codigoSituacaoFiltro}"> 
						<f:selectItem itemValue="0" itemLabel="#{msgs.forManterFormalizacaoAlteracaoContrato_cbo_selecione}" />
						<f:selectItems value="#{manterFormalizacaoAlteracaoContratoBean.listaSituacaoFiltro}"/> 
					</br:brSelectOneMenu>							
				</br:brPanelGrid>			
			</br:brPanelGrid>
			
		</br:brPanelGrid>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" style="margin-top:6px;" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioSelecaoFiltro" index="2" />	
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
			<br:brPanelGroup style="text-align:left;">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.forManterFormalizacaoAlteracaoContrato_data_inclusao}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" style="margin-top:6px;" cellpadding="0" cellspacing="0" >
		
		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brPanelGroup rendered="#{manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro != null &&  manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro == 2}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialPagamento');" id="dataInicialPagamento" value="#{manterFormalizacaoAlteracaoContratoBean.dataInicialPagamentoFiltro}" >

						</app:calendar>
					</br:brPanelGroup>
					<br:brPanelGroup rendered="#{empty manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro || manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro != 2}">
						<app:calendar id="dataInicialPagamentoDes" value="#{manterFormalizacaoAlteracaoContratoBean.dataInicialPagamentoFiltro}" disabled="true" >
						</app:calendar>
					</br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.forManterFormalizacaoAlteracaoContrato_label_a}"/>
					<br:brPanelGroup rendered="#{manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro != null &&  manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro == 2}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFinalPagamento');" id="dataFinalPagamento" value="#{manterFormalizacaoAlteracaoContratoBean.dataFinalPagamentoFiltro}" >

		 				</app:calendar>	
					</br:brPanelGroup>	
					<br:brPanelGroup rendered="#{empty manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro || manterFormalizacaoAlteracaoContratoBean.radioSelecaoFiltro != 2}">
						<app:calendar id="dataFinalPagamentoDes" value="#{manterFormalizacaoAlteracaoContratoBean.dataFinalPagamentoFiltro}" disabled="true">
		 				</app:calendar>	
					</br:brPanelGroup>	
				</br:brPanelGroup>			    
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>		
		</a4j:outputPanel>
	</br:brPanelGrid>						
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.forManterFormalizacaoAlteracaoContrato_botao_limpar_dados}" action="#{manterFormalizacaoAlteracaoContratoBean.limparCampos}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			 <br:brCommandButton id="btnConsultar" styleClass="bto1"  disabled="#{manterFormalizacaoAlteracaoContratoBean.desabilitaConsulta}" value="#{msgs.forManterFormalizacaoAlteracaoContrato_botao_consultar}" action="#{manterFormalizacaoAlteracaoContratoBean.consultar}"
			   		onclick="javascript: desbloquearTela(); return validaCamposFiltro(document.forms[1],'#{msgs.label_ocampo}','#{msgs.label_necessario}','#{msgs.forManterFormalizacaoAlteracaoContrato_numero}',	'#{msgs.forManterFormalizacaoAlteracaoContrato_situacao}', '#{msgs.forManterFormalizacaoAlteracaoContrato_data_inclusao}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim>  

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	

				<app:scrollableDataTable id="dataTable" value="#{manterFormalizacaoAlteracaoContratoBean.listaGridPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" 	width="100%">

				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value=" " styleClass="tableFontStyle" style="width:30; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{manterFormalizacaoAlteracaoContratoBean.itemSelecionadoLista}">
						<f:selectItems value="#{manterFormalizacaoAlteracaoContratoBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			    	<t:radio for="sor" index="#{parametroKey}" />
				</app:scrollableColumn>
				
			  	<app:scrollableColumn styleClass="colTabRight" width="230px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_numero_versao_contrato}" style="text-align:center;width:230"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrAditivoContratoNegocio}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="230px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.forManterFormalizacaoAlteracaoContrato_grid_data_inclusao}" style="text-align:center;width:230" />
				    </f:facet>
				    <br:brOutputText value="#{result.dtInclusaoAditivoContrato}" />
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="200x" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.forManterFormalizacaoAlteracaoContrato_grid_situacao}" style="text-align:center;width:200" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsSitucaoAditivoContrato}" />
  			  	</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
		
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterFormalizacaoAlteracaoContratoBean.consultarVersoesPaginacao}" >
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" disabled="false" styleClass="bto1" value="#{msgs.forManterFormalizacaoAlteracaoContrato_botao_voltar}" action="#{manterFormalizacaoAlteracaoContratoBean.voltarFormalizacao}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>		
			</br:brPanelGroup> 
			 
			<br:brPanelGroup style="text-align:right;width:700px"> 	
			
				<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.forManterFormalizacaoAlteracaoContrato_botao_limpar}" action="#{manterFormalizacaoAlteracaoContratoBean.limparDadosRadio}" style="margin-right:5px" disabled="#{ empty manterFormalizacaoAlteracaoContratoBean.listaGridPesquisa}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			
				<br:brCommandButton id="btnDetalhar" style="margin-right:5px;" disabled="#{empty manterFormalizacaoAlteracaoContratoBean.itemSelecionadoLista}" styleClass="bto1"  value="#{msgs.forManterFormalizacaoAlteracaoContrato_botao_detalhar}" action="#{manterFormalizacaoAlteracaoContratoBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				
				<br:brCommandButton id="btnIncluir" styleClass="bto2" style="margin-right:5px;" value="#{msgs.btn_gerar_versao_contrato}" action="#{manterFormalizacaoAlteracaoContratoBean.incluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>

				<br:brCommandButton id="btnExcluir" styleClass="bto1"
					value="#{msgs.forManterFormalizacaoAlteracaoContrato_botao_excluir}"
					action="#{manterFormalizacaoAlteracaoContratoBean.excluir}"
					disabled="#{manterFormalizacaoAlteracaoContratoBean.desabilitaBotaoImprimirVersaoContrato}">
					<brArq:submitCheckClient />
				</br:brCommandButton>

				<br:brCommandButton id="btnImprimir" styleClass="bto1"
					value="#{msgs.btn_imprimir}"
					action="#{manterFormalizacaoAlteracaoContratoBean.imprimirRelatorioFormalizacao}"
					disabled="#{empty manterFormalizacaoAlteracaoContratoBean.listaGridPesquisa}"
					onclick="javascript:desbloquearTela();" style="margin-left:5px">
					<brArq:submitCheckClient />
				</br:brCommandButton>

				<a4j:commandButton id="btnImprimirVersaoContrato" styleClass="bto2" value="#{msgs.btn_imprimir_versao_contrato}" style="margin-left:5px"
					disabled="#{manterFormalizacaoAlteracaoContratoBean.desabilitaBotaoImprimirVersaoContrato}">
				<a4j:support event="onclick"
					action="#{manterFormalizacaoAlteracaoContratoBean.imprimirContrato}"
					oncomplete="document.getElementById('forManterFormalizacaoAlteracaoContrato:btnImprimirDocumentoHidden').click()" />
				</a4j:commandButton>
				<br:brCommandButton id="btnImprimirDocumentoHidden"
					value=""
					action="#{manterFormalizacaoAlteracaoContratoBean.imprimirContratoFinal}"
					styleClass="HtmlCommandButtonBradesco"
					style="display: none"
					onclick="desbloquearTela()">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
			
		</br:brPanelGrid>	
	</a4j:outputPanel>
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()"
		onstop="desbloquearTela()" />
	

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>