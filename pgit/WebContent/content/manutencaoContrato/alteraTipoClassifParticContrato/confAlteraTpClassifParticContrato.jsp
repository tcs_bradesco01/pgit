<%@include file="/componentes/jsp/include.jsp"%>

<brArq:form id="formIncConfirma">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

		<jsp:include page="/content/manutencaoContrato/alteraTipoClassifParticContrato/cabecPadraoVincEndereco.jsp" flush="false" />

		<f:verbatim><hr class="lin"></f:verbatim>

		<br:brPanelGrid width="100%" cellpadding="0" cellspacing="0" rendered="#{alteraTipoClassifParticContratoBean.btoAcao == 'I'}">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tpServico}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.participante.dsTipoServico}"/>
			</br:brPanelGroup>

			<f:verbatim><hr class="lin"></f:verbatim>

			<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_participante}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup />
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}:" />	
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.participanteInclusao.cnpjOuCpfFormatado}"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup style="width:20px; margin-bottom:5px" />
				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.participanteInclusao.nmRazao}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup /> 
			</br:brPanelGrid>
			
			 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tipoParticipacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.participanteInclusao.cdDsTipoParticipacao}"/>
				</br:brPanelGroup>
	
				<br:brPanelGroup style="width:20px; margin-bottom:5px" />
	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_situacaoParticipacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.participanteInclusao.dsSituacaoParticipacao}"/>
				</br:brPanelGroup>						
			</br:brPanelGrid>

			<f:verbatim> <hr class="lin"> </f:verbatim>

		</br:brPanelGrid>
		
		<br:brPanelGrid width="100%" cellpadding="0" cellspacing="0" rendered="#{alteraTipoClassifParticContratoBean.btoAcao == 'A' || alteraTipoClassifParticContratoBean.btoAcao == 'E'}">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tpServico}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.dsTipoServico}"/>
			</br:brPanelGroup>

			<f:verbatim><hr class="lin"></f:verbatim>

			<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_participante}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup />
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}:" />	
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.cpfCnpjParticipante}"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup style="width:20px; margin-bottom:5px" />
				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.nomeRazaoParticipante}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup /> 
			</br:brPanelGrid>
			
			 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tipoParticipacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.dsTipoParticipacao}"/>
				</br:brPanelGroup>
	
				<br:brPanelGroup style="width:20px; margin-bottom:5px" />
	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_situacaoParticipacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.situacaoParticipacao}"/>
				</br:brPanelGroup>						
			</br:brPanelGrid>

			<f:verbatim> <hr class="lin"> </f:verbatim>

		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_endereco}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_logradouro}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.dsEndereco}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tipo}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_enderecoFixo}" rendered="#{alteraTipoClassifParticContratoBean.incEndereco.rdoEnderecoPessoa == '1'}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_enderecoTransitorio}" rendered="#{alteraTipoClassifParticContratoBean.incEndereco.rdoEnderecoPessoa == '2'}"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_qtdeDias}: " rendered="#{alteraTipoClassifParticContratoBean.incEndereco.rdoEnderecoPessoa == '2'}" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{alteraTipoClassifParticContratoBean.incEndereco.cdUsoPostal}" rendered="#{alteraTipoClassifParticContratoBean.incEndereco.rdoEnderecoPessoa == '2'}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"></f:verbatim>

		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup style="text-align:left;width:100%" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{alteraTipoClassifParticContratoBean.voltarConfirmar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:100%">
				<br:brCommandButton id="btnConfirmarInc" styleClass="bto1" value="#{msgs.botao_confirmar}" action="#{alteraTipoClassifParticContratoBean.confirmar}" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela(); return false; }" rendered="#{alteraTipoClassifParticContratoBean.btoAcao == 'I'}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConfirmarAlt" styleClass="bto1" value="#{msgs.botao_confirmar}" action="#{alteraTipoClassifParticContratoBean.confirmar}" onclick="javascript: if (!confirm('Confirma Altera��o?')) { desbloquearTela(); return false; }" rendered="#{alteraTipoClassifParticContratoBean.btoAcao == 'A'}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConfirmarExc" styleClass="bto1" value="#{msgs.botao_confirmar}" action="#{alteraTipoClassifParticContratoBean.confirmar}" onclick="javascript: if (!confirm('Confirma Exclus�o?')) { desbloquearTela(); return false; }" rendered="#{alteraTipoClassifParticContratoBean.btoAcao == 'E'}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
</brArq:form>