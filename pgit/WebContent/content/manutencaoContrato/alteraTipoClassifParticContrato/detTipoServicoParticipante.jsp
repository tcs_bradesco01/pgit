<%@include file="/componentes/jsp/include.jsp"%>

<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
	<br:brPanelGroup>
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tipoServico}:"/>
	</br:brPanelGroup>
</br:brPanelGrid>

<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
	<br:brPanelGroup />
</br:brPanelGrid>

<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
		<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tpServico}:" />
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.participante.dsTipoServico}"/>
	</br:brPanelGroup>
</br:brPanelGrid>

<f:verbatim><hr class="lin"></f:verbatim>

<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
	<br:brPanelGroup>
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_participante}:"/>
	</br:brPanelGroup>
</br:brPanelGrid>

<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup />
</br:brPanelGrid>	

<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}:" />	
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.participante.cpfCnpjFormatado}"/>
	</br:brPanelGroup>

	<br:brPanelGroup style="width:20px; margin-bottom:5px" />

	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}:" />
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.participante.dsRazaoSocial}"/>
	</br:brPanelGroup>
</br:brPanelGrid>

<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
	<br:brPanelGroup />
</br:brPanelGrid>

<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tipoParticipacao}:" />
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.participante.cdDsTipoParticipacao}"/>
	</br:brPanelGroup>

	<br:brPanelGroup style="width:20px; margin-bottom:5px" />

	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_situacaoParticipacao}:" />
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.participante.cdDescricaoSituacaoParticapante}"/>
	</br:brPanelGroup>						
</br:brPanelGrid>

<f:verbatim><hr class="lin"></f:verbatim>