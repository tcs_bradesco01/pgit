<%@include file="/componentes/jsp/include.jsp"%>

<brArq:form id="formAlteraTpClassifParticContrato">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

		<jsp:include page="/content/manutencaoContrato/alteraTipoClassifParticContrato/cabecPadraoVincEndereco.jsp" flush="false" />

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tpServico}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.dsTipoServico}"/>
			</br:brPanelGroup>

			<f:verbatim><hr class="lin"></f:verbatim>

			<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_participante}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup />
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}:" />	
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.cpfCnpjParticipante}"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup style="width:20px; margin-bottom:5px" />
				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.nomeRazaoParticipante}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup /> 
			</br:brPanelGrid>
			
			 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tipoParticipacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.dsTipoParticipacao}"/>
				</br:brPanelGroup>
	
				<br:brPanelGroup style="width:20px; margin-bottom:5px" />
	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_situacaoParticipacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.situacaoParticipacao}"/>
				</br:brPanelGroup>						
			</br:brPanelGrid>
		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_endereco}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup />
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_logradouro}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup />
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">					
					 <br:brPanelGroup>
						<br:brInputText value="#{alteraTipoClassifParticContratoBean.dsEndereco}" disabled="true" id="txtLogradouro" styleClass="HtmlInputTextBradesco" size="100" />
					</br:brPanelGroup>
					<br:brPanelGroup style="margin-left:170px">
			    		<br:brCommandButton id="btnPesquisarEndereco" styleClass="bto1" value="#{msgs.label_pesquisar}"
			    				action="#{alteraTipoClassifParticContratoBean.pesquisarEndereco}">
							<brArq:submitCheckClient/>
						</br:brCommandButton>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<t:selectOneRadio id="rdoEndereco" value="#{alteraTipoClassifParticContratoBean.incEndereco.rdoEnderecoPessoa}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<a4j:support event="onclick" reRender="panelQtdeDias" action="#{alteraTipoClassifParticContratoBean.limparQtdDias}" />
		</t:selectOneRadio>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<t:radio for="rdoEndereco" index="0" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_enderecoFixo}"/>
			</br:brPanelGroup>

			<br:brPanelGroup>			
				<t:radio for="rdoEndereco" index="1" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_enderecoTransitorio}"/>
			</br:brPanelGroup>

			<br:brPanelGroup />
			<br:brPanelGroup id="panelQtdeDias" style="margin-left:10px">
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_qtdeDias}" />
				<br:brInputText value="#{alteraTipoClassifParticContratoBean.incEndereco.cdUsoPostal}" disabled="#{!alteraTipoClassifParticContratoBean.validarSelecEndereco}" id="txtQtdeDias" styleClass="HtmlInputTextBradesco" size="4" maxlength="3" onkeypress="onlyNum();" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"></f:verbatim>

		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup style="text-align:left;width:100%" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{alteraTipoClassifParticContratoBean.voltarConsulta}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
			<br:brPanelGroup id="panelBotaoAvancar" style="text-align:right;width:100%">
				<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.botao_avancar}" action="#{alteraTipoClassifParticContratoBean.avancarAlterar}" disabled="#{!alteraTipoClassifParticContratoBean.pesquisaEnderecoInclusaoEfetuada}"
						onclick="return validaCpoEnderecoAvanca(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.alteraTipoClassificacaoParticipantesContrato_label_enderecoFixo}', '#{msgs.alteraTipoClassificacaoParticipantesContrato_label_enderecoTransitorio}', '#{msgs.alteraTipoClassificacaoParticipantesContrato_label_qtdeDias}');">
					<brArq:submitCheckClient/>
				</br:brCommandButton>		
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>