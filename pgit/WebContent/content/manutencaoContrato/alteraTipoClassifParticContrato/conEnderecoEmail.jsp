<%@include file="/componentes/jsp/include.jsp"%>

<brArq:form id="formEnderecoEmail">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
				<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" converter="javax.faces.Integer" value="#{alteraTipoClassifParticContratoBean.indiceEnderecoSelecionado}">
					<f:selectItems value="#{alteraTipoClassifParticContratoBean.listaEnderecoItems}"/>
					<a4j:support event="onclick" reRender="panelBotoes"/>
				</t:selectOneRadio>
				<app:scrollableDataTable id="dataTable" value="#{alteraTipoClassifParticContratoBean.listaEndereco}" var="result" 
					rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px">
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>	
				    	<t:radio for=":formEnderecoEmail:sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="150px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_clubRepresentante}" style="width:150; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdClub}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="170px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjRepresentante}"  style="width:170; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsCpfCnpjRecebedorFormatado}" />
					</app:scrollableColumn>				

					<app:scrollableColumn styleClass="colTabRight" width="170px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjRepresentante}"  style="width:170; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cpfCnpjRecebedorFormatado}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoRepresentante}" style="width:250; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.dsNomeRazao}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoRepresentante}" style="width:250; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.cdNomeRazao}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_email}" style="width:200; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdEnderecoEletronico}" />
					</app:scrollableColumn>
					
					<app:scrollableColumn styleClass="colTabLeft" width="500px" >
					    <f:facet name="header">
					      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_endereco}" style="width:500; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsEndereco}" />
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>	

	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{alteraTipoClassifParticContratoBean.pesquisarEnderecoPaginacao}">
				 <f:facet name="first">
				    <brArq:pdcCommandButton id="primeira"
				      styleClass="bto1"
				      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton id="proxima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton id="ultima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  </f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"></f:verbatim>

		<br:brPanelGrid id="panelBotoes" columns="2" width="100%" cellpadding="0" cellspacing="0">
	    	<br:brPanelGroup style="text-align:left; width:100%">
	    		<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.botao_voltar}" action="#{alteraTipoClassifParticContratoBean.voltarEndereco}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
	    	</br:brPanelGroup>
	    	<br:brPanelGroup style="width:100%; text-align:right">
	    		<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.botao_limpar}" action="#{alteraTipoClassifParticContratoBean.limparEndereco}" disabled="#{empty alteraTipoClassifParticContratoBean.indiceEnderecoSelecionado}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnSelecionar" styleClass="bto1" value="#{msgs.botao_selecionar}" action="#{alteraTipoClassifParticContratoBean.selecionarEndereco}" disabled="#{empty alteraTipoClassifParticContratoBean.indiceEnderecoSelecionado}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>