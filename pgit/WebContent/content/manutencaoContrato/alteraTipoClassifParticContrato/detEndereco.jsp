<%@include file="/componentes/jsp/include.jsp"%>

<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
	<br:brPanelGroup>
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_endereco}:"/>
	</br:brPanelGroup>
</br:brPanelGrid>

<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
	<br:brPanelGroup />
</br:brPanelGrid>

<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
		<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_logradouro}:" />
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.dsEndereco}"/>
	</br:brPanelGroup>
</br:brPanelGrid>

<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
	<br:brPanelGroup> 
	</br:brPanelGroup>
</br:brPanelGrid>

<br:brPanelGrid  columns="1" cellpadding="0" cellspacing="0">
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />
		<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tipo}:" />
		<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_enderecoFixo}" rendered="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdUsoPostal == 0}"/>
		<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_enderecoTransitorio}" rendered="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdUsoPostal > 0}"/>
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_qtdeDias}: " rendered="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdUsoPostal > 0}" />
		<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdUsoPostal}" rendered="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdUsoPostal > 0}" />
	</br:brPanelGroup>
</br:brPanelGrid>

<f:verbatim><hr class="lin"></f:verbatim>