<%@include file="/componentes/jsp/include.jsp"%>

<brArq:form id="formAlteraTpClassifParticContrato">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		
		<jsp:include page="/content/manutencaoContrato/alteraTipoClassifParticContrato/cabecPadraoVincEndereco.jsp" flush="false" />

		<f:verbatim><hr class="lin"> </f:verbatim>

		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<a4j:region>
			<t:selectOneRadio id="rdoArgPesquisa" value="#{alteraTipoClassifParticContratoBean.radioArgPesquisa}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<a4j:support event="onclick" status="statusAguarde" action="#{alteraTipoClassifParticContratoBean.selecionarRadioArgPesquisa}" reRender="panelBotoesPesquisa, panelFiltroTipoServico, panelFiltroParticipante, panelPesquisaParticipante"/>
			</t:selectOneRadio>
		</a4j:region>
		
		<br:brPanelGrid id="panelFiltroTipoServico" columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="text-align:left;">
					<br:brPanelGroup>
						<t:radio for="rdoArgPesquisa" index="0" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tpServico}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup />
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
					 <br:brPanelGroup>
						<br:brSelectOneMenu id="selTipoServico" value="#{alteraTipoClassifParticContratoBean.filtroHistPesquisaEndereco.cdProdutoOperacaoRelacionado}" disabled="#{alteraTipoClassifParticContratoBean.radioArgPesquisa != 1}" converter="javax.faces.Integer" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{alteraTipoClassifParticContratoBean.listaTipoServico}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>			
				<t:radio for="rdoArgPesquisa" index="1" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_participante}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid id="panelFiltroParticipante" columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.participantePesquisa.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.participantePesquisa.nmRazao}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid id="panelPesquisaParticipante" columns="1" width="100%" cellpadding="0" cellspacing="0">
		    	<br:brPanelGroup style="width:100%; text-align: right">
		    		<br:brCommandButton id="btnPesquisarArgPesquisa" styleClass="bto1" value="#{msgs.label_pesquisar}" action="#{alteraTipoClassifParticContratoBean.pesquisarParticipante}" disabled="#{alteraTipoClassifParticContratoBean.radioArgPesquisa != 2}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.histContasManterContrato_data_manutencao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>

				<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">	
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.PGIC0053_label_de}"/>
							<br:brPanelGroup >
							 	<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" id="calendarioDe" value="#{alteraTipoClassifParticContratoBean.dtManutencaoInicio}" >
								</app:calendar>
							</br:brPanelGroup>					
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.histContasManterContrato_a}"/>
							<br:brPanelGroup >
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" value="#{alteraTipoClassifParticContratoBean.dtManutencaoFim}" >
								</app:calendar>		
							</br:brPanelGroup>							
						</br:brPanelGroup>			    
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid id="panelBotoesPesquisa" columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<br:brPanelGroup style="width:100%; text-align: right">
	    		<br:brCommandButton id="btnLimparArgPesquisa" styleClass="bto1" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_limpar_dados}" action="#{alteraTipoClassifParticContratoBean.limparParticipante}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultarArgPesquisa" styleClass="bto1" value="#{msgs.label_botao_consultar}" action="#{alteraTipoClassifParticContratoBean.consultarHistoricoParticipante}" onclick="desbloquearTela();">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{alteraTipoClassifParticContratoBean.indicePesquisaHistSelecionado}">
						<f:selectItems value="#{alteraTipoClassifParticContratoBean.listaHistPesquisaSelectItems}"/>
						<a4j:support event="onclick" reRender="panelBotoes"/>
					</t:selectOneRadio>
						
					<app:scrollableDataTable id="dataTable" value="#{alteraTipoClassifParticContratoBean.listaHistoricoPesquisa}" var="result" 
						rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">

						<app:scrollableColumn styleClass="colTabCenter" width="30px">
							<f:facet name="header">
						    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
						    </f:facet>
					    	<t:radio for=":formAlteraTpClassifParticContrato:sorLista" index="#{parametroKey}" />
						</app:scrollableColumn>

						<app:scrollableColumn styleClass="colTabRight" width="170px" >
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}"  style="width:170; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.cpfCnpjFormatado}" />
						</app:scrollableColumn>

						<app:scrollableColumn styleClass="colTabLeft" width="250px" >
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}" style="width:250; text-align:center" />
						    </f:facet>
						    <br:brOutputText value="#{result.cdRazaoSocial}" />
						</app:scrollableColumn>

						<app:scrollableColumn styleClass="colTabLeft" width="200px" >
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tpServico}" style="width:380; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.dsModalidadeServico}" />
						</app:scrollableColumn>

						<app:scrollableColumn styleClass="colTabCenter" width="150px" >
						    <f:facet name="header">
						      <br:brOutputText value="#{msgs.trilhaAuditoria_data_hora_manutencao}" style="width:150; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.hrInclusaoRegistro}" converter="timestampPdcConverter"/>
						</app:scrollableColumn>

						<app:scrollableColumn styleClass="colTabRight" width="100px" >
						    <f:facet name="header">
						      <br:brOutputText value="#{msgs.trilhaAuditoria_usuario}" style="width:100; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.cdUsuarioManutencao}" styleClass="tableFontStyle"/>
						</app:scrollableColumn>

					</app:scrollableDataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{alteraTipoClassifParticContratoBean.consultarParticipantePaginacao}" >
					 <f:facet name="first">
					    <brArq:pdcCommandButton id="primeira"
					      styleClass="bto1"
					      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					  </f:facet>
					  <f:facet name="fastrewind">
					    <brArq:pdcCommandButton id="retrocessoRapido"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					  </f:facet>
					  <f:facet name="previous">
					    <brArq:pdcCommandButton id="anterior"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					  </f:facet>
					  <f:facet name="next">
					    <brArq:pdcCommandButton id="proxima"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					  </f:facet>
					  <f:facet name="fastforward">
					    <brArq:pdcCommandButton id="avancoRapido"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					  </f:facet>
					  <f:facet name="last">
					    <brArq:pdcCommandButton id="ultima"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					  </f:facet>
					</brArq:pdcDataScroller>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<f:verbatim><hr class="lin"></f:verbatim>

			<br:brPanelGrid id="panelBotoes" columns="2" width="100%" style="text-align:left" cellpadding="0" cellspacing="0">
		    	<br:brPanelGroup style="width:100%">
		    		<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.label_voltar}" action="#{alteraTipoClassifParticContratoBean.voltarConsulta}" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
		    	</br:brPanelGroup>
		    	<br:brPanelGroup style="width:100%; text-align: right">
		    		<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.label_botao_limpar}" action="#{alteraTipoClassifParticContratoBean.limparGrid}" style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnDetalhar" styleClass="bto1" value="#{msgs.label_detalhar}" action="#{alteraTipoClassifParticContratoBean.detalharHistorico}" disabled="#{empty alteraTipoClassifParticContratoBean.indicePesquisaHistSelecionado}" style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

	</br:brPanelGrid>

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>