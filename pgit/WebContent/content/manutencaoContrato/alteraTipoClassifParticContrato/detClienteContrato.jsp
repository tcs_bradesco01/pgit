<%@include file="/componentes/jsp/include.jsp"%>

<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
	<br:brPanelGroup />
</br:brPanelGrid>	

<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
	<br:brPanelGroup>
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cliente}:"/>
	</br:brPanelGroup>
</br:brPanelGrid>

<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
	<br:brPanelGroup />
</br:brPanelGrid>

<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
	<br:brPanelGroup >
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
		<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}:" />
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.cliente.cnpjOuCpfFormatado}"/>
	</br:brPanelGroup>

	<br:brPanelGroup style="width:20px; margin-bottom:5px" />

	<br:brPanelGroup >			
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
		<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}:" />
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.cliente.dsNomeRazao}"/>
	</br:brPanelGroup>
</br:brPanelGrid>

<f:verbatim><hr class="lin"></f:verbatim>

<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
	<br:brPanelGroup>
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_title_contrato}:"/>
	</br:brPanelGroup>
</br:brPanelGrid>

<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup />
</br:brPanelGrid>	

<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />	
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.contrato.dsPessoaJuridicaContrato}"/>
	</br:brPanelGroup>
	
	<br:brPanelGroup style="width:20px; margin-bottom:5px" />
	
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tipo}:" />
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.contrato.dsTipoContratoNegocio}"/>
	</br:brPanelGroup>
	
	<br:brPanelGroup style="width:20px; margin-bottom:5px" />
	
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_numero}:" />	
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.contrato.nrSeqContratoNegocio}"/>
	</br:brPanelGroup>
</br:brPanelGrid>

<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
	<br:brPanelGroup /> 
</br:brPanelGrid>

 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_descContrato}:" />
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.contrato.dsContrato}"/>
	</br:brPanelGroup>

	<br:brPanelGroup style="width:20px; margin-bottom:5px" />

	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
		<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_situacao}:" />
		<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.contrato.dsSituacaoContratoNegocio}"/>
	</br:brPanelGroup>						
</br:brPanelGrid>

<f:verbatim><hr class="lin"></f:verbatim>