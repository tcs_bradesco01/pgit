<%@include file="/componentes/jsp/include.jsp"%>

<brArq:form id="formAlteraTpClassifParticContrato">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

		<jsp:include page="/content/manutencaoContrato/alteraTipoClassifParticContrato/cabecPadraoVincEndereco.jsp" flush="false" />

		<f:verbatim> <hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tpServico}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.dsTipoServico}"/>
			</br:brPanelGroup>

			<f:verbatim><hr class="lin"></f:verbatim>

			<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_participante}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup />
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}:" />	
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.cpfCnpjParticipante}"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup style="width:20px; margin-bottom:5px" />
				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.nomeRazaoParticipante}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup /> 
			</br:brPanelGrid>
			
			 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tipoParticipacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.dsTipoParticipacao}"/>
				</br:brPanelGroup>
	
				<br:brPanelGroup style="width:20px; margin-bottom:5px" />
	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_situacaoParticipacao}:" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.situacaoParticipacao}"/>
				</br:brPanelGroup>						
			</br:brPanelGrid>
		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<jsp:include page="/content/manutencaoContrato/alteraTipoClassifParticContrato/detEndereco.jsp" flush="false" />

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup />
		</br:brPanelGrid>	

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.trilhaAuditoria_data_hora_inclusao}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.hrInclusaoRegistro}" converter="timestampPdcConverter"/>
			</br:brPanelGroup>
		
			<br:brPanelGroup style="width:20px" />
		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.trilhaAuditoria_usuario}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdUsuarioInclusao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.trilhaAuditoria_tipo_canal}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdTipoCanalInclusao}"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value=" - " rendered="#{not empty alteraTipoClassifParticContratoBean.detalhe.manutencao.dsTipoCanalInclusao}"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.dsTipoCanalInclusao}"/>
			</br:brPanelGroup>
		
			<br:brPanelGroup style="width:20px" />
		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.trilhaAuditoria_complemento}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdOperacaoCanalInclusao}" rendered="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdOperacaoCanalInclusao != '0'}"/>
			</br:brPanelGroup>						
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"></f:verbatim>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.trilhaAuditoria_data_hora_manutencao}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.hrManutencaoRegistro}"  converter="timestampPdcConverter"/>
			</br:brPanelGroup>
		
			<br:brPanelGroup style="width:20px" />
		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.trilhaAuditoria_usuario}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdUsuarioManutencao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.trilhaAuditoria_tipo_canal}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdTipoCanalManutencao}" rendered="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdTipoCanalManutencao != '0'}"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value=" - " rendered="#{not empty alteraTipoClassifParticContratoBean.detalhe.manutencao.dsTipoCanalManutencao}" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.dsTipoCanalManutencao}"/>
			</br:brPanelGroup>
		
			<br:brPanelGroup style="width:20px" />
		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.trilhaAuditoria_complemento}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.detalhe.manutencao.cdOperacaoCanalManutencao}"/>
			</br:brPanelGroup>						
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"></f:verbatim>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<br:brPanelGroup>
	    		<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.label_botao_voltar}" action="#{alteraTipoClassifParticContratoBean.voltarDetalhe}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
</brArq:form>