<%@include file="/componentes/jsp/include.jsp"%>

<brArq:form id="formParticipante">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

		<jsp:include page="/content/manutencaoContrato/alteraTipoClassifParticContrato/cabecPadraoVincEndereco.jsp" flush="false" />

		<f:verbatim><hr class="lin"></f:verbatim>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
				<a4j:region>
					<t:selectOneRadio id="sorLista" value="#{alteraTipoClassifParticContratoBean.indiceParticipanteSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">
						<f:selectItems value="#{alteraTipoClassifParticContratoBean.listaParticipantesItems}"/>
						<a4j:support event="onclick" reRender="panelBotao" status="statusAguarde" />
					</t:selectOneRadio>
				</a4j:region>

				<app:scrollableDataTable id="dataTable" value="#{alteraTipoClassifParticContratoBean.listaParticipantes}" var="result" 
					rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px">
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
					    </f:facet>
				    	<t:radio for=":formParticipante:sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="150px" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_clubParticipante}" style="width:150; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cdPessoa}" styleClass="tableFontStyle"/>
					</app:scrollableColumn>				
					
					<app:scrollableColumn styleClass="colTabRight" width="170px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}"  style="width:170; text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.cnpjOuCpfFormatado}" />
					</app:scrollableColumn>				
	
					<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
					    <f:facet name="header">
					      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}" style="width:250; text-align:center" />
					    </f:facet>
					    <br:brOutputText value="#{result.nmRazao}" />
					</app:scrollableColumn>				
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>	

	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{alteraTipoClassifParticContratoBean.pesquisarParticipantePaginacao}" >
				 <f:facet name="first">
				    <brArq:pdcCommandButton id="primeira"
				      styleClass="bto1"
				      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton id="proxima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton id="ultima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  </f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"></f:verbatim>

		<br:brPanelGrid id="panelBotao" columns="2" width="100%" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup style="text-align:left;width:100%" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{alteraTipoClassifParticContratoBean.voltarParticipante}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:100%">
				<br:brCommandButton id="btnSelecionar" styleClass="bto1" value="#{msgs.botao_selecionar}" action="#{alteraTipoClassifParticContratoBean.selecionarParticipante}" disabled="#{empty alteraTipoClassifParticContratoBean.indiceParticipanteSelecionado}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>		
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>