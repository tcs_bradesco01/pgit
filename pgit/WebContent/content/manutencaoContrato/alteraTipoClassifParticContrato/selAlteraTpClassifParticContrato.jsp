<%@include file="/componentes/jsp/include.jsp"%>

<brArq:form id="formAlteraTpClassifParticContrato">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_identificacao_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>	

		<a4j:region id="regionFiltro">
		<t:selectOneRadio id="rdoFiltroCliente" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.bloqueiaRadio}">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="" />
			<a4j:support event="onclick" status="statusAguarde" reRender="panelCnpj, panelCpf, panelNomeRazao, panelBanco, panelAgencia, panelConta, panelBtoConsultarCliente" ajaxSingle="true"  action="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.limparCampos}"/>
		</t:selectOneRadio>
		</a4j:region>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<t:radio for="rdoFiltroCliente" index="0" />			
			
			 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_cnpj}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup />
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
					<br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'formAlteraTpClassifParticContrato','formAlteraTpClassifParticContrato:txtCnpj','formAlteraTpClassifParticContrato:txtFilial');" style="margin-right: 5" converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					<br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'formAlteraTpClassifParticContrato','formAlteraTpClassifParticContrato:txtFilial','formAlteraTpClassifParticContrato:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
			<t:radio for="rdoFiltroCliente" index="1" />			
			
			 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_cpf}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
				    <br:brPanelGroup />
				</br:brPanelGrid>

			   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
					<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5" converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
					onkeyup="proximoCampo(9,'formAlteraTpClassifParticContrato','formAlteraTpClassifParticContrato:txtCpf','formAlteraTpClassifParticContrato:txtControleCpf');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" >
			<t:radio for="rdoFiltroCliente" index="2" />		
			
			 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '2' || alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="100" maxlength="70" id="txtNomeRazaoSocial"/>
			</a4j:outputPanel>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup />
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  border="0">
			<t:radio for="rdoFiltroCliente" index="3" />			
			
			<a4j:outputPanel id="panelBanco" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_banco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'formAlteraTpClassifParticContrato','formAlteraTpClassifParticContrato:txtBanco','formAlteraTpClassifParticContrato:txtAgencia');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="7" maxlength="6" id="txtAgencia" onkeyup="proximoCampo(6,'formAlteraTpClassifParticContrato','formAlteraTpClassifParticContrato:txtAgencia','formAlteraTpClassifParticContrato:txtConta');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelConta" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</a4j:outputPanel>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
	    		<br:brCommandButton id="btnLimparCampos2" styleClass="bto1" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_limpar_dados}" action="#{alteraTipoClassifParticContratoBean.limparDadosCliente}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
	    		
		   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado || alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_consultar_cliente}" action="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.consultarClientes}"
		   		onclick="javascript:desbloquearTela(); 
		   		return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.alteraTipoClassificacaoParticipantesContrato_cnpj}','#{msgs.alteraTipoClassificacaoParticipantesContrato_cpf}', '#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazao}',
		   		'#{msgs.alteraTipoClassificacaoParticipantesContrato_banco}','#{msgs.alteraTipoClassificacaoParticipantesContrato_agencia}','#{msgs.alteraTipoClassificacaoParticipantesContrato_conta}');">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</a4j:outputPanel>
		</br:brPanelGrid>
		
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cliente}:"/>
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>

			<br:brPanelGroup style="width:20px; margin-bottom:5px" />

			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazao}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	    
		<f:verbatim><hr class="lin"> </f:verbatim>

		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_title_identificacao_contrato}:"/>
		 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup />
		</br:brPanelGrid>
		
		 <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				 <br:brPanelGrid columns="5" style="margin-top:5px" cellpadding="0" cellspacing="0">	
		 			<br:brPanelGroup />
				</br:brPanelGrid>

				<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">					
					 <br:brPanelGroup>	
						<br:brSelectOneMenu id="selEmpresaGestoraContrada" converter="longConverter" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.cdPessoaJuridicaContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{alteraTipoClassifParticContratoBean.listaEmpresaGestora}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>		
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" />

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_contrato}:"/>
						</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="2" style="margin-top:5px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup />
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
						<br:brSelectOneMenu id="selTipoContrato" converter="inteiroConverter" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.cdTipoContratoNegocio}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{alteraTipoClassifParticContratoBean.listaTipoContrato}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>	
			</br:brPanelGroup>
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" />

			<br:brPanelGroup>	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_numero}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" style="margin-top:5px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup />
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>					
								<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								 value="#{alteraTipoClassifParticContratoBean.identificacaoClienteBean.entradaConsultarListaContratosPessoas.nrSeqContratoNegocio}" size="15" maxlength="10" id="txtNumeroContrato" >
							</br:brInputText>
						</br:brPanelGroup>		 	 
		 		</br:brPanelGrid>
	 		</br:brPanelGroup>
	 	</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<br:brPanelGroup style="width:100%; text-align: right">
	    		<br:brCommandButton id="btnLimparCamposContrato" styleClass="bto1" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_limpar_dados}" action="#{alteraTipoClassifParticContratoBean.limparContrato}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btoConsultarContrato" styleClass="bto1" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_btn_consultar_contrato}" action="#{alteraTipoClassifParticContratoBean.consultarContrato}" onclick="javascript:desbloquearTela(); 
		   		return validaCpoContrato('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_empresa_gestora_contrato}','#{msgs.label_tipo_contrato}','#{msgs.alteraTipoClassificacaoParticipantesContrato_label_numero}', '#{alteraTipoClassifParticContratoBean.identificacaoClienteBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}');">		
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_title_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsPessoaJuridicaContrato}"/>		
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_contrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>	
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup /> 
		</br:brPanelGrid>

		 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		 	<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_numero}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>	
			</br:brPanelGroup>

			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_descContrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsContrato}"/>			
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.identificacaoClienteBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>		
			</br:brPanelGroup>						
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim>

		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		 <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<a4j:region>
			<t:selectOneRadio id="rdoArgPesquisa" value="#{alteraTipoClassifParticContratoBean.radioArgPesquisa}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{!alteraTipoClassifParticContratoBean.consultaContratoEfetuada}">
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<a4j:support event="onclick" status="statusAguarde" action="#{alteraTipoClassifParticContratoBean.selecionarRadioArgPesquisa}" reRender="panelBotoesPesquisa, panelFiltroTipoServico, panelFiltroParticipante, panelPesquisaParticipante"/>
			</t:selectOneRadio>
		</a4j:region>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>			
				<t:radio for="rdoArgPesquisa" index="0" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tpServico}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid id="panelFiltroTipoServico" columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="width:20px; margin-bottom:5px" />

			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
					 <br:brPanelGroup>
						<br:brSelectOneMenu id="selTipoServico" value="#{alteraTipoClassifParticContratoBean.filtroPesquisaEndereco.cdProdutoServicoOperacao}" disabled="#{alteraTipoClassifParticContratoBean.pesquisaTipoServicoDesabilitada}" converter="javax.faces.Integer" style="width: 300" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{alteraTipoClassifParticContratoBean.listaTipoServico}" />
							<a4j:support event="onchange" action="#{alteraTipoClassifParticContratoBean.selecionarTipoServico}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>			
				<t:radio for="rdoArgPesquisa" index="1" />
			</br:brPanelGroup>		
			<br:brPanelGroup>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_participante}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid id="panelPesquisaParticipante" columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<br:brPanelGroup style="width:100%; text-align: right">
	    		<br:brCommandButton id="btnPesquisarArgPesquisa" styleClass="bto1" value="#{msgs.label_pesquisar}" action="#{alteraTipoClassifParticContratoBean.pesquisarParticipante}" disabled="#{alteraTipoClassifParticContratoBean.pesquisaParticipanteDesabilitado}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid id="panelFiltroParticipante" columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.participantePesquisa.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>	

			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{alteraTipoClassifParticContratoBean.participantePesquisa.nmRazao}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid id="panelBotoesPesquisa" columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<br:brPanelGroup style="width:100%; text-align: right">
	    		<br:brCommandButton id="btnLimparArgPesquisa" styleClass="bto1" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_limpar_dados}" action="#{alteraTipoClassifParticContratoBean.limparParticipante}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultarArgPesquisa" styleClass="bto1" value="#{msgs.label_botao_consultar}" action="#{alteraTipoClassifParticContratoBean.consultarParticipante}" rendered="#{!alteraTipoClassifParticContratoBean.acaoHistorico}" disabled="#{alteraTipoClassifParticContratoBean.consultaParticipanteDesabilitado}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultarHistArgPesquisa" styleClass="bto1" value="#{msgs.label_botao_consultar}" action="#{alteraTipoClassifParticContratoBean.consultarHistoricoParticipante}" rendered="#{alteraTipoClassifParticContratoBean.acaoHistorico}" disabled="#{alteraTipoClassifParticContratoBean.consultaParticipanteDesabilitado}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" rendered="#{alteraTipoClassifParticContratoBean.acaoHistorico}" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:6px">	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_manutencao}:" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_de}"/>
					<br:brPanelGroup>
						<app:calendar id="dataInicioManutencao" value="#{alteraTipoClassifParticContratoBean.filtroHistPesquisaEndereco.dtManutencaoInicio}" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" />
					</br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_a}"/>
					<br:brPanelGroup>
						<app:calendar id="dataFimManutencao" value="#{alteraTipoClassifParticContratoBean.filtroHistPesquisaEndereco.dtManutencaoFim}" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" />
					</br:brPanelGroup>	
				</br:brPanelGroup>			    
			</br:brPanelGrid>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup />
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" rendered="#{!alteraTipoClassifParticContratoBean.acaoHistorico}">
			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{alteraTipoClassifParticContratoBean.indicePesquisaSelecionado}">
						<f:selectItems value="#{alteraTipoClassifParticContratoBean.listaPesquisaSelectItems}"/>
						<a4j:support event="onclick" reRender="panelBotoes"/>
					</t:selectOneRadio>
					<app:scrollableDataTable id="dataTable" value="#{alteraTipoClassifParticContratoBean.listaPesquisa}" var="result" 
						rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
						<app:scrollableColumn styleClass="colTabCenter" width="30px">
							<f:facet name="header">
						    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
						    </f:facet>	
					    	<t:radio for=":formAlteraTpClassifParticContrato:sorLista" index="#{parametroKey}" />
						</app:scrollableColumn>
	
						<app:scrollableColumn styleClass="colTabRight" width="150px" >			
						    <f:facet name="header">
						      <br:brOutputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_clubParticipante}" style="width:150; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.cdClub}" styleClass="tableFontStyle"/>
						</app:scrollableColumn>				
						
						<app:scrollableColumn styleClass="colTabRight" width="170px" >			
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}"  style="width:170; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.cpfCnpjFormatado}" />
						</app:scrollableColumn>
	
						<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}" style="width:250; text-align:center" />
						    </f:facet>
						    <br:brOutputText value="#{result.dsRazaoSocial}" />
						</app:scrollableColumn>				
						
						<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tpServico}" style="width:200; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.dsTipoServico}" />
						</app:scrollableColumn>

						<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
							<f:facet name="header">
							  <h:outputText value="#{msgs.identificacaoClienteContrato_gerente_responsavel}" style="width:200; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.descFuncionarioBradesco}" />
						</app:scrollableColumn>
					</app:scrollableDataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{alteraTipoClassifParticContratoBean.consultarParticipantePaginacao}" >
					 <f:facet name="first">
					    <brArq:pdcCommandButton id="primeira"
					      styleClass="bto1"
					      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					  </f:facet>
					  <f:facet name="fastrewind">
					    <brArq:pdcCommandButton id="retrocessoRapido"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					  </f:facet>
					  <f:facet name="previous">
					    <brArq:pdcCommandButton id="anterior"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					  </f:facet>
					  <f:facet name="next">
					    <brArq:pdcCommandButton id="proxima"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					  </f:facet>
					  <f:facet name="fastforward">
					    <brArq:pdcCommandButton id="avancoRapido"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					  </f:facet>
					  <f:facet name="last">
					    <brArq:pdcCommandButton id="ultima"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					  </f:facet>
					</brArq:pdcDataScroller>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<f:verbatim><hr class="lin"></f:verbatim>

			<br:brPanelGrid id="panelBotoes" columns="1" width="100%" cellpadding="0" cellspacing="0">
		    	<br:brPanelGroup style="width:100%; text-align: right">
		    		<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.label_botao_limpar}" action="#{alteraTipoClassifParticContratoBean.limparGrid}" disabled="#{empty alteraTipoClassifParticContratoBean.indicePesquisaSelecionado}" style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnDetalhar" styleClass="bto1" value="#{msgs.label_detalhar}" action="#{alteraTipoClassifParticContratoBean.detalhar}" disabled="#{empty alteraTipoClassifParticContratoBean.indicePesquisaSelecionado}" style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnIncluir" styleClass="bto1" value="#{msgs.label_incluir}" action="#{alteraTipoClassifParticContratoBean.incluir}" disabled="#{!alteraTipoClassifParticContratoBean.consultaContratoEfetuada}" style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnAlterar" styleClass="bto1" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_alterar}" action="#{alteraTipoClassifParticContratoBean.alterar}" disabled="#{empty alteraTipoClassifParticContratoBean.indicePesquisaSelecionado}" style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnExcluir" styleClass="bto1" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_excluir}" action="#{alteraTipoClassifParticContratoBean.excluir}" disabled="#{empty alteraTipoClassifParticContratoBean.indicePesquisaSelecionado}" style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnHistorico" styleClass="bto1" value="#{msgs.label_historico}" action="#{alteraTipoClassifParticContratoBean.historico}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" rendered="#{alteraTipoClassifParticContratoBean.acaoHistorico}">	
			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
					<t:selectOneRadio id="sorListaHist" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{alteraTipoClassifParticContratoBean.indicePesquisaHistSelecionado}">
						<f:selectItems value="#{alteraTipoClassifParticContratoBean.listaHistPesquisaSelectItems}"/>
						<a4j:support event="onclick" reRender="panelBotoesHistorico"/>
					</t:selectOneRadio>
					<app:scrollableDataTable id="dataTableHist" value="#{alteraTipoClassifParticContratoBean.listaHistoricoPesquisa}" var="result" 
						rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
						<app:scrollableColumn styleClass="colTabCenter" width="30px">
							<f:facet name="header">
						    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
						    </f:facet>	
					    	<t:radio for=":formAlteraTpClassifParticContrato:sorListaHist" index="#{parametroKey}" />
						</app:scrollableColumn>
	
						<app:scrollableColumn styleClass="colTabRight" width="150px" >			
						    <f:facet name="header">
						      <br:brOutputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_clubParticipante}" style="width:150; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.cdClub}" styleClass="tableFontStyle"/>
						</app:scrollableColumn>				
						
						<app:scrollableColumn styleClass="colTabRight" width="170px" >			
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_cpfcnpjParticipante}"  style="width:170; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.cpfCnpjFormatado}" />
						</app:scrollableColumn>
	
						<app:scrollableColumn styleClass="colTabLeft" width="250px" >			
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_nomeRazaoParticipante}" style="width:250; text-align:center" />
						    </f:facet>
						    <br:brOutputText value="#{result.cdRazaoSocial}" />
						</app:scrollableColumn>				
						
						<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_tpServico}" style="width:200; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.dsTipoServico}" />
						</app:scrollableColumn>
	
						<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_modalidade}" style="width:200; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.dsModalidadeServico}" />
						</app:scrollableColumn>
	
						<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_operacao}" style="width:200; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.dsOperacaoCatalogo}" />
						</app:scrollableColumn>
						
						<app:scrollableColumn styleClass="colTabCenter" width="200px" >			
						    <f:facet name="header">
						      <h:outputText value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_data_hora_manu}" style="width:200; text-align:center"/>
						    </f:facet>
						    <br:brOutputText value="#{result.hrInclusaoRegistro}" />
						</app:scrollableColumn>
					</app:scrollableDataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<brArq:pdcDataScroller id="dataScrollerHist" for="dataTableHist" actionListener="#{alteraTipoClassifParticContratoBean.consultarHistoricoParticipantePaginacao}" >
					 <f:facet name="first">
					    <brArq:pdcCommandButton id="primeiraHist"
					      styleClass="bto1"
					      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					  </f:facet>
					  <f:facet name="fastrewind">
					    <brArq:pdcCommandButton id="retrocessoRapidoHist"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					  </f:facet>
					  <f:facet name="previous">
					    <brArq:pdcCommandButton id="anteriorHist"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					  </f:facet>
					  <f:facet name="next">
					    <brArq:pdcCommandButton id="proximaHist"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					  </f:facet>
					  <f:facet name="fastforward">
					    <brArq:pdcCommandButton id="avancoRapidoHist"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					  </f:facet>
					  <f:facet name="last">
					    <brArq:pdcCommandButton id="ultimaHist"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					  </f:facet>
					</brArq:pdcDataScroller>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<f:verbatim><hr class="lin"></f:verbatim>

			<br:brPanelGrid id="panelBotoesHistorico" columns="1" width="100%" cellpadding="0" cellspacing="0">
		    	<br:brPanelGroup style="width:100%; text-align: right">
					<br:brCommandButton id="btnDetalharHist" styleClass="bto1" value="#{msgs.label_detalhar}" action="#{alteraTipoClassifParticContratoBean.detalharHistorico}" disabled="#{empty alteraTipoClassifParticContratoBean.indicePesquisaHistSelecionado}" style="margin-right:5px">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
	 	</br:brPanelGrid>
	</br:brPanelGrid>

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>