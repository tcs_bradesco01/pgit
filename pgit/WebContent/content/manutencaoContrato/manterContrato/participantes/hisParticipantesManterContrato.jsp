<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%> 
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="hisParticipantesManterContrato" name="hisParticipantesManterContrato">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_cliente_representante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_cpfcnpj}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.cpfCnpjRepresentante}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_nomerazaosocial}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.nomeRazaoRepresentante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_grupo_economico}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.grupEconRepresentante}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_atividade_economica}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.ativEconRepresentante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_segmento}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.segRepresentante}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_sub_segmento}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.subSegRepresentante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.gerenteResponsavelFormatado}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.nomeRazaoParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>

 	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_empresa}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.empresaContrato}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.tipoContrato}"/>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_numero}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.numeroContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_descricao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.descricaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_situacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.situacaoContrato}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_motivo}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.motivoContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo_participacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.participacaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
<f:verbatim><hr class="lin"> </f:verbatim>

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_label_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<t:selectOneRadio id="radioHistorico" disabled="#{manterContratoBean.manterContratoParticipantesBean.itemSelecionadoGrid != null}" onclick="submit();" value="#{manterContratoBean.manterContratoParticipantesBean.itemSelecionado2}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
	</t:selectOneRadio>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<t:radio for="radioHistorico" index="1" />			
		
		 	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conParticipantesManterContrato_label_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(9,'hisParticipantesManterContrato','hisParticipantesManterContrato:txtCnpj','hisParticipantesManterContrato:txtFilial');" style="margin-right: 5" onkeypress="onlyNum();" size="11" maxlength="9"  styleClass="HtmlInputTextBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.cnpjHist}" disabled="#{manterContratoBean.manterContratoParticipantesBean.itemSelecionado2 == '0' || manterContratoBean.manterContratoParticipantesBean.itemSelecionado2 == '' || manterContratoBean.manterContratoParticipantesBean.bloquearCampos}" />
			    <br:brInputText id="txtFilial" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(4,'hisParticipantesManterContrato','hisParticipantesManterContrato:txtFilial','hisParticipantesManterContrato:txtControle');" style="margin-right: 5"  onkeypress="onlyNum();" size="5" maxlength="4"  styleClass="HtmlInputTextBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.cnpjHist2}" disabled="#{manterContratoBean.manterContratoParticipantesBean.itemSelecionado2 == '0' || manterContratoBean.manterContratoParticipantesBean.itemSelecionado2 == '' || manterContratoBean.manterContratoParticipantesBean.bloquearCampos}"/>
				<br:brInputText id="txtControle" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" style="margin-right: 5" onkeypress="onlyNum();" size="3" maxlength="2"  styleClass="HtmlInputTextBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.cnpjHist3}" disabled="#{manterContratoBean.manterContratoParticipantesBean.itemSelecionado2 == '0' || manterContratoBean.manterContratoParticipantesBean.itemSelecionado2 == '' || manterContratoBean.manterContratoParticipantesBean.bloquearCampos}"/>
			</br:brPanelGrid>
		
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="radioHistorico" index="0" />			
		
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conParticipantesManterContrato_label_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" style="margin-right: 5" onkeypress="onlyNum();" value="#{manterContratoBean.manterContratoParticipantesBean.cpfHist}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'hisParticipantesManterContrato','hisParticipantesManterContrato:txtCpf','hisParticipantesManterContrato:txtControleCpf');" disabled="#{manterContratoBean.manterContratoParticipantesBean.itemSelecionado2 == '1' || manterContratoBean.manterContratoParticipantesBean.itemSelecionado2 == '' || manterContratoBean.manterContratoParticipantesBean.bloquearCampos}"/>
			    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"  onkeypress="onlyNum();" value="#{manterContratoBean.manterContratoParticipantesBean.cpfHist2}" maxlength="2" size="3" id="txtControleCpf" disabled="#{manterContratoBean.manterContratoParticipantesBean.itemSelecionado2 == '1' || manterContratoBean.manterContratoParticipantesBean.itemSelecionado2 == ''|| manterContratoBean.manterContratoParticipantesBean.bloquearCampos}"/>
			</br:brPanelGrid>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conParticipantesManterContrato_label_data_manutencao}:" style="margin-top:5px"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>				
				<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataInicial');" id="dataInicial" value="#{manterContratoBean.manterContratoParticipantesBean.dataInicial}" >
				</app:calendar>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.hist_label_ate}:"/>
				<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dataFinal');" id="dataFinal" value="#{manterContratoBean.manterContratoParticipantesBean.dataFinal}" >
				</app:calendar>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	</a4j:outputPanel>		
		
	</br:brPanelGrid>
	
<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
	 		
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btnLimparDados" style="margin-left:5px" styleClass="bto1" value="#{msgs.conParticipantesManterContrato_btnLimparDadados}" action="#{manterContratoBean.manterContratoParticipantesBean.limparDadosHistorico}">							
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnConsultar" style="margin-left:5px" styleClass="bto1" value="#{msgs.conParticipantesManterContrato_btnConsultar}" action="#{manterContratoBean.manterContratoParticipantesBean.carregaListaHistorico}" disabled="#{manterContratoBean.manterContratoParticipantesBean.itemSelecionado2 == '' || manterContratoBean.manterContratoParticipantesBean.bloquearCampos}"
			onclick="javascript:desbloquearTela(); return checaCampoObrigatorioHist('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conParticipantesManterContrato_label_data_manutencao}','#{msgs.conParticipantesManterContrato_label_cpf}','#{msgs.conParticipantesManterContrato_label_cnpj}');">							
				<brArq:submitCheckClient/>
			</br:brCommandButton>					
		</br:brPanelGroup>
	</br:brPanelGrid>

<f:verbatim> <br> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">
 
			<app:scrollableDataTable id="dataTable" value="#{manterContratoBean.manterContratoParticipantesBean.listaPesquisa2}" var="varResult" 
				rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false"
						value="#{manterContratoBean.manterContratoParticipantesBean.itemSelecionadoPesquisa2}" >
						<f:selectItems value="#{manterContratoBean.manterContratoParticipantesBean.listaPesquisaControle2}"/>
						 <a4j:support event="onclick" reRender="btnDetalhar" /> 
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conParticipantesManterContrato_grid_cpfcnpj_participante}" style="text-align:center;width:200" />
					</f:facet>
					<br:brOutputText value="#{varResult.cdCpfCnpj}"/>
				</app:scrollableColumn>

				<app:scrollableColumn styleClass="colTabLeft" width="250px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conParticipantesManterContrato_grid_razao_social}" style="text-align:center;width:250"/>
					</f:facet>
					<br:brOutputText value="#{varResult.nomeParticipante}"/>
				</app:scrollableColumn>
 
				<app:scrollableColumn styleClass="colTabLeft" width="200px" > 
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.conParticipantesManterContrato_label_data_hora_manu}" style="text-align:center;width:200"/>
					</f:facet>
					<br:brOutputText value="#{varResult.hrInclusaoRegistroFormatada}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="200px">
			    	<f:facet name="header">
			      		<h:outputText value="#{msgs.label_usuario_manutencao}" style="text-align:center;width:100%"/>
			    	</f:facet>
			    	<br:brOutputText value="#{varResult.cdUsuarioManutencao}" />
			  	</app:scrollableColumn>
			  
			  	<app:scrollableColumn styleClass="colTabLeft" width="200px">
			    	<f:facet name="header">
			      		<h:outputText value="#{msgs.label_tipo_de_manutencao}" style="text-align:center;width:100%"/>
			    	</f:facet>
			    	<br:brOutputText value="#{varResult.cdIndicadorTipoManutencao}" />
			  	</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterContratoBean.manterContratoParticipantesBean.pesquisar}"  > 
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1" 
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1"  style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>	
		</br:brPanelGroup>
	</br:brPanelGrid>

<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.conParticipantesManterContrato_btnVoltar}" action="#{manterContratoBean.manterContratoParticipantesBean.voltarListaSemLoop}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		 		
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >		
			<br:brCommandButton id="btnLimparLista" style="margin-left:5px" styleClass="bto1" value="#{msgs.btn_limpar}" action="#{manterContratoBean.manterContratoParticipantesBean.limparLista}" disabled="#{empty manterContratoBean.manterContratoParticipantesBean.listaPesquisa2}">							
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnDetalhar" disabled="#{empty manterContratoBean.manterContratoParticipantesBean.itemSelecionadoPesquisa2}" style="margin-left:5px" styleClass="bto1" value="#{msgs.conParticipantesManterContrato_btnDetalhar}" action="#{manterContratoBean.manterContratoParticipantesBean.detalharHistorico}">							
				<brArq:submitCheckClient/>
			</br:brCommandButton>					
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<t:inputHidden value="#{manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
		<script language='javascript'>
			validarProxCampoIdentClienteContratoHistorico(document.forms[1], document.getElementById('hisParticipantesManterContrato:hiddenFlagPesquisa').value);
		
			function checaCampoObrigatorioHist(msgcampo, msgnecessario, msgdata,msgcpf,msgcnpj){
				var objRdoCliente = document.getElementsByName('radioHistorico');
					if (objRdoCliente[1].checked) {
						if (document.getElementById('hisParticipantesManterContrato:txtCpf').value == "") {
							alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
							
							return false;		
						} else {
							if(document.getElementById('hisParticipantesManterContrato:txtControleCpf').value == "") {
								alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
								
								return false;	
							}
						}
					}else if (objRdoCliente[0].checked) {
								if (document.getElementById('hisParticipantesManterContrato:txtCnpj').value == "") {
									alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
									
									return false;		
								} else {
									if (document.getElementById('hisParticipantesManterContrato:txtFilial').value == "") {
										alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
										
										return false;		
									} else {
										if (document.getElementById('hisParticipantesManterContrato:txtControle').value == ""){
											alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
											t.getElementById('hisParticipantesManterContrato:txtControle').focus();
											return false;		
										}
									}
								}
								
							}
					 
					if ( document.getElementById('hisParticipantesManterContrato:dataInicial.day').value == null ||
						 document.getElementById('hisParticipantesManterContrato:dataInicial.day').value == ''   ||
						 document.getElementById('hisParticipantesManterContrato:dataInicial.month').value == null ||
						 document.getElementById('hisParticipantesManterContrato:dataInicial.month').value == ''   ||
						 document.getElementById('hisParticipantesManterContrato:dataInicial.year').value == null ||
						 document.getElementById('hisParticipantesManterContrato:dataInicial.year').value == '' ) {
							alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
							return false;		
					}
					
					if ( document.getElementById('hisParticipantesManterContrato:dataFinal.day').value == null ||
						 document.getElementById('hisParticipantesManterContrato:dataFinal.day').value == ''   ||
						 document.getElementById('hisParticipantesManterContrato:dataFinal.month').value == null ||
						 document.getElementById('hisParticipantesManterContrato:dataFinal.month').value == ''   ||
						 document.getElementById('hisParticipantesManterContrato:dataFinal.year').value == null ||
						 document.getElementById('hisParticipantesManterContrato:dataFinal.year').value == '' ) {
							alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
							return false;		
					}
				
					return true;
				}
			
		</script>
	</f:verbatim>
	 
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	