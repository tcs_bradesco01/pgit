<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%> 

<brArq:form id="incParticipantesManterContrato2" name="incParticipantesManterContrato2">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">


	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_cliente_representante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_cpfcnpj}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.cpfCnpjRepresentante}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_nomerazaosocial}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.nomeRazaoRepresentante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_grupo_economico}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.grupEconRepresentante}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_atividade_economica}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.ativEconRepresentante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_segmento}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.segRepresentante}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_sub_segmento}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.subSegRepresentante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	  <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.gerenteResponsavelFormatado}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.nomeRazaoParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	 <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conParticipantesManterContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_empresa}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.empresaContrato}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.tipoContrato}"/>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_numero}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.numeroContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_descricao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.descricaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_situacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.situacaoContrato}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_motivo}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.motivoContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_tipo_participacao}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.participacaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.path_conParticipantesManterContrato_participantes}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_grid_cpfcnpj_participante}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterContratoBean.manterContratoParticipantesBean.participantesSelecionado.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>	
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conParticipantesManterContrato_label_nomerazaosocial_participante}:"/>			
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.participantesSelecionado.dsNomeRazao}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>

 
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_banco}:"/>			
			</br:brPanelGroup>		
		
			<br:brInputText styleClass="HtmlInputTextBradesco"
				value="#{manterContratoBean.manterContratoParticipantesBean.bancoFiltroContasParticipantes}"
				size="7" maxlength="3" id="txtBanco" converter="javax.faces.Integer"
				onkeyup="proximoCampo(3,'incParticipantesManterContrato2','incParticipantesManterContrato2:txtBanco','incParticipantesManterContrato2:txtAgencia');"/>
		
		</br:brPanelGrid>
		
		<br:brPanelGrid style="margin-left:15px">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia}:"/>			
			</br:brPanelGroup>		
		
			<br:brInputText styleClass="HtmlInputTextBradesco"
				value="#{manterContratoBean.manterContratoParticipantesBean.agenciaFiltroContasParticipantes}"
				size="12" maxlength="5" id="txtAgencia" converter="javax.faces.Integer"
				onkeyup="proximoCampo(4,'incParticipantesManterContrato2','incParticipantesManterContrato2:txtAgencia','incParticipantesManterContrato2:txtConta');"/>
		</br:brPanelGrid>
		
		<br:brPanelGrid style="margin-left:15px">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta}:"/>			
			</br:brPanelGroup>		
			
			<br:brInputText styleClass="HtmlInputTextBradesco"
				value="#{manterContratoBean.manterContratoParticipantesBean.contaFiltroContasParticipantes}"
				size="20" maxlength="13" id="txtConta" converter="javax.faces.Long" />
		</br:brPanelGrid>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid  width="100%" cellpadding="0" cellspacing="0" style="text-align:right;width:100%;" >	
		<br:brPanelGrid columns="2">
			<br:brCommandButton id="btnLimparCampos" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_limpar_campos}" 
				action="#{manterContratoBean.manterContratoParticipantesBean.limparCamposPerquisaContasParticipantes}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		 		
	
			<br:brCommandButton id="btnConsultar" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_consultar}" 
				action="#{manterContratoBean.manterContratoParticipantesBean.consultarContasParticipantes}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="margin-top:11px">	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">
 
			<app:scrollableDataTable id="dataTable" value="#{manterContratoBean.manterContratoParticipantesBean.listaContasPossiveisInclusao}" var="result" 
				rowIndexVar="parametroKey" rows="10" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
			
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">					
						<t:selectBooleanCheckbox disabled="#{empty manterContratoBean.manterContratoParticipantesBean.listaContasPossiveisInclusao}" id="todos" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.checkAll}" 
							onclick="javascritp:selecionarTodosParticipantes(document.forms[1],this);" >
						
						</t:selectBooleanCheckbox>  
					</f:facet>					 		
				
					<t:selectBooleanCheckbox  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						value="#{result.check}">
						<f:selectItems value="#{manterContratoBean.manterContratoParticipantesBean.listaContasPossiveisInclusaoControle}"/>	
						<a4j:support event="onclick" reRender="gridBtnSelecionar" action="#{manterContratoBean.manterContratoParticipantesBean.habilitarSelecionar}" />		
					</t:selectBooleanCheckbox>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="140px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_cpf_cnpj}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.cdCpfCnpj}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_nome_razao_social}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.nmParticipante}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_banco}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.bancoFormatado}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_agencia}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.agenciaFormatada}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="140px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_conta}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.contaFormatada}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_tipo_conta}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.dsTipoConta}"/>
				</app:scrollableColumn>
				
			</app:scrollableDataTable>
						
		</br:brPanelGroup>

		<br:brPanelGrid style="margin-top:11px; text-align: center; width: 100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup >
				<brArq:pdcDataScroller  id="dataScroller" for="dataTable" actionListener="#{manterContratoBean.manterContratoParticipantesBean.paginarListaontasParticipantes}" >
						<f:facet name="first">
						  <brArq:pdcCommandButton id="primeira"
						    styleClass="HtmlCommandButtonBradesco"
						    value="#{msgs.label_primeira}" title="#{msgs.label_primeira}" styleClass="bto1" style="cursor:hand;" />
						</f:facet>
						<f:facet name="fastrewind">
						  <brArq:pdcCommandButton id="retrocessoRapido"
						    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
						    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}" styleClass="bto1" style="cursor:hand;" />
						</f:facet>
						<f:facet name="previous">
						  <brArq:pdcCommandButton id="anterior"
						    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
						    value="#{msgs.label_anterior}" title="#{msgs.label_anterior}" styleClass="bto1" style="cursor:hand;" />
						</f:facet>
						<f:facet name="next">
						  <brArq:pdcCommandButton id="proxima"
						    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
						    value="#{msgs.label_proxima}" title="#{msgs.label_proxima}" styleClass="bto1" style="cursor:hand;" />
						</f:facet>
						<f:facet name="fastforward">
						   <brArq:pdcCommandButton id="avancoRapido"
						     styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
						     value="#{msgs.label_avanco}" title="#{msgs.label_avanco}" styleClass="bto1" style="cursor:hand;" />
						</f:facet>
						<f:facet name="last">
						  <brArq:pdcCommandButton id="ultima"
						    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
						    value="#{msgs.label_ultima}" title="#{msgs.label_ultima}" styleClass="bto1" style="cursor:hand;" />
						</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>	
		</br:brPanelGrid>
	</br:brPanelGrid>
	
			
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid id="gridBtnSelecionar" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;width:100%;" >	
			<br:brCommandButton id="btnSelecionar"  styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_selecionar}" 
				action="#{manterContratoBean.manterContratoParticipantesBean.adicionarContasParaInclusao}"
				disabled="#{!manterContratoBean.manterContratoParticipantesBean.habilitarBtnSelecionar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		 		
	</br:brPanelGrid>
	
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_contas_selecionadas}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">
 
			<app:scrollableDataTable id="dataTable2" value="#{manterContratoBean.manterContratoParticipantesBean.listaContasParaInclusao}" var="result" 
				rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
			
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">					
						  <t:selectBooleanCheckbox disabled="#{empty manterContratoBean.manterContratoParticipantesBean.listaContasParaInclusao}" 
						  style="width:30;text-align:center;margin-left:1px" id="todos2" 
						  styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoBean.manterContratoParticipantesBean.checkAll2}" >
								<a4j:support event="onclick" reRender="dataTable2, gridBtnRemover" action="#{manterContratoBean.manterContratoParticipantesBean.checaTodasContasParaInclusao}"/>
						</t:selectBooleanCheckbox >
					</f:facet>					 		
				
					<t:selectBooleanCheckbox  id="sorLista2" styleClass="HtmlSelectOneRadioBradesco" 
						value="#{result.check}">
						<f:selectItems value="#{manterContratoBean.manterContratoParticipantesBean.listaContasParaInclusaoControle}"/>	
						<a4j:support event="onclick" reRender="gridBtnRemover" action="#{manterContratoBean.manterContratoParticipantesBean.habilitarRemover}" />		
					</t:selectBooleanCheckbox>
				</app:scrollableColumn>
				
					
				<app:scrollableColumn styleClass="colTabRight" width="140px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_cpf_cnpj}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.cdCpfCnpj}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_nome_razao_social}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.nmParticipante}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_banco}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.bancoFormatado}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_agencia}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.agenciaFormatada}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="140px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_conta}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.contaFormatada}"/>
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >
					<f:facet name="header">
					  <br:brOutputText value="#{msgs.label_tipo_conta}" style="text-align:center;width:100%" />
					</f:facet>
					<br:brOutputText value="#{result.dsTipoConta}"/>
				</app:scrollableColumn>
				
			</app:scrollableDataTable>
						
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid id="gridBtnRemover" width="100%" cellpadding="0" cellspacing="0" style="text-align:right;width:100%;" >	
			<br:brCommandButton id="btnRemover" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_remover}" 
				action="#{manterContratoBean.manterContratoParticipantesBean.removerContasParaInclusao}" 
				disabled="#{!manterContratoBean.manterContratoParticipantesBean.habilitarBtnRemover}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		 		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.conParticipantesManterContrato_btnVoltar}" 
				action="#{manterContratoBean.manterContratoParticipantesBean.voltarIncluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		 		
		</br:brPanelGroup> 
		
		<br:brPanelGroup style="text-align:right;width:100%" >
			<br:brCommandButton id="btnAvancar" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_avancar}" 
					action="#{manterContratoBean.manterContratoParticipantesBean.avancarConcluirInclusao}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		 		
		</br:brPanelGroup> 

	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>	