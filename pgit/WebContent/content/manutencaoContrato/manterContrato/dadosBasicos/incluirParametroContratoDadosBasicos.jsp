<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conParametroContratoDadosBasicosForm" name="conParametroContratoDadosBasicosForm">

	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" id="clienteMaster"
					value="#{msgs.conDadosBasicos_clienteMaster}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="clienteMaster_cpfCnpj"
					value="#{msgs.conDadosBasicos_clienteMaster_cpfCnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="cpfCnpjClienteMaster"
					value="#{manterContratoDadosBasicosBean.cpfCnpjClienteMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="nomeRazaoSocial"
					value="#{msgs.conDadosBasicos_clienteMaster_nomeRazaoSocial}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="nomeRazaoClienteMaster"
					value="#{manterContratoDadosBasicosBean.nomeRazaoClienteMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="clienteMaster_grupoEconomico"
					value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="grupoEconomicoClienteMaster"
					value="#{manterContratoDadosBasicosBean.grupoEconomicoClienteMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="clienteMaster_atividadeEconomica"
					value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="atividadeEconomicaClienteMaster"
					value="#{manterContratoDadosBasicosBean.atividadeEconomicaClienteMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="clienteMaster_segmento"
					value="#{msgs.conDadosBasicos_clienteMaster_segmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="segmentoClienteMaster"
					value="#{manterContratoDadosBasicosBean.segmentoClienteMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="clienteMaster_subSegmento"
					value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="subSegmentoClienteMaster"
					value="#{manterContratoDadosBasicosBean.subSegmentoClienteMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="lebel_agencia_gestora"
					value="#{msgs.lebel_agencia_gestora}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="dsAgenciaGestora"
					value="#{manterContratoBean.dsAgenciaGestora}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="unidadesOrganizacionaisResponsaveis"
					value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="gerenteResponsavelUnidOrg"
					value="#{manterContratoDadosBasicosBean.gerenteResponsavelUnidOrg}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" id="clienteParticipante"
					value="#{msgs.conDadosBasicos_clienteParticipante}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="cpf_cnpj"
					value="#{msgs.label_cpf_cnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="cpfCnpj"
					value="#{manterContratoBean.cpfCnpj}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="nome_razao_social"
					value="#{msgs.label_nome_razao_social}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="nomeRazaoSocial1"
					value="#{manterContratoBean.nomeRazaoSocial}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" id="conDadosBasicos_contrato"
					value="#{msgs.conDadosBasicos_contrato}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="empresa_gestora_contrato"
					value="#{msgs.label_empresa_gestora_contrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="empresaGestoraContrato"
					value="#{manterContratoDadosBasicosBean.empresaGestoraContrato}" />
			</br:brPanelGroup>
		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="contrato_tipoContrato"
					value="#{msgs.conDadosBasicos_contrato_tipoContrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="tipoContrato"
					value="#{manterContratoDadosBasicosBean.tipoContrato}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="contrato_numero"
					value="#{msgs.conDadosBasicos_contrato_numero}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="numeroContrato"
					value="#{manterContratoDadosBasicosBean.numeroContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="identificacao_contrato"
					value="#{msgs.label_descricao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="descricaoContrato"
					value="#{manterContratoDadosBasicosBean.descricaoContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_situacao}:" id="contratoSituacao" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="situacaoContrato"
					value="#{manterContratoDadosBasicosBean.situacaoContrato}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="contratoMotivo"
					value="#{msgs.conDadosBasicos_contrato_motivo}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="motivoContrato"
					value="#{manterContratoDadosBasicosBean.motivoContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="tipo_participacao"
					value="#{msgs.label_tipo_participacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="participacaoContrato"
					value="#{manterContratoDadosBasicosBean.participacaoContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" id="parametroTitulo"
					value="#{msgs.label_parametro}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="lblParametro"
				value="#{msgs.label_parametro}:" />
		</br:brPanelGroup>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brSelectOneMenu id="comboParametro"
			value="#{manterContratoDadosBasicosBean.itemSelecionadoComboParametro}"
			styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::" />
			<f:selectItems value="#{manterContratoDadosBasicosBean.comboParametros}" />
			<a4j:support action="#{manterContratoDadosBasicosBean.verificaObrigatoriedadeDominio}" event="onblur" 
				reRender="txtDominio,botoes"/>
		</br:brSelectOneMenu>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" id="lblDominio"
				value="#{msgs.label_dominio}:" />
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brInputText id="txtDominio" maxlength="30" size="40" 
				value="#{manterContratoDadosBasicosBean.dsParametroContrato}" 
				disabled="#{manterContratoDadosBasicosBean.cdIndicadorUtilizaDescricao == 2}"/>
		</br:brPanelGroup>
		
		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid id="botoes" columns="2" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGrid align="left">
				<br:brCommandButton id="btnVoltar" styleClass="bto1"
					style="align:left" value="#{msgs.btn_voltar}"
					action="#{manterContratoDadosBasicosBean.voltarParametro}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGrid>
		
			<br:brPanelGrid align="right">
				<br:brCommandButton id="btnConfirmar" styleClass="bto1"
					value="#{msgs.btn_avancar}"
					action="#{manterContratoDadosBasicosBean.avancarIncluirParametro}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGrid>
		</br:brPanelGrid>
	</br:brPanelGrid>

</brArq:form>