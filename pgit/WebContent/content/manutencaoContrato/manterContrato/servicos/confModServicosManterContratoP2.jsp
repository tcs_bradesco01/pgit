<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<brArq:form id="confModServicosManterContratoP2" name="confModServicosManterContratoP2">
<h:inputHidden id="cdParametro" value="#{manterContratoServicosBean.cdParametroTela}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_empresaGestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_tipoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_tipoDeServico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoServicoSelecionado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolManutContratos_modalidade}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsModalidade}"/>
		</br:brPanelGroup>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_situacaoDaModalidade}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsSituacaoModalidade}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<!-- Pagamento Via D�bito em Conta -- CdParametro:21 -->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 21}" columns="1" cellpadding="0" cellspacing="0" width="100%">

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoConsultaSaldo21" value="#{manterContratoServicosBean.cboTipoConsultaSaldo}" 
							disabled="#{manterContratoServicosBean.desabilitarComboTipoConsultaSaldo}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoConsultaSaldo}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaTipoConsSaldoTituloValorSuperior}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.tipo_consulta_saldo_titulo_valor_superior}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" />

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboConsultaSaldoValorSuperior21" value="#{manterContratoServicosBean.cdConsultaSaldoValorSuperior}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}" />
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoConsultaSaldo}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>

			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaTipoConsSaldoTituloValorSuperior}"/>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_processamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboProcessamentoEfetivacaoPagamento21" value="#{manterContratoServicosBean.cboProcessamentoEfetivacaoPagamento}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaProcessamentoEfetivacaoPagamento}" />
							
							<a4j:support event="onchange" reRender="cboTratFevDataPag21, cboFeriadoLocal21" action="#{manterContratoServicosBean.recarregarCombomFeriado}" status="statusAguarde" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
						<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboTratFevDataPag21" value="#{manterContratoServicosBean.cboTratamentoFeriadosDataPagamento}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTratamentoFeriadosDataPagamento}" />
						</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_pgto_feriado_local}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px">
					<br:brSelectOneMenu id="cboFeriadoLocal21" value="#{manterContratoServicosBean.cdIndicadorFeriadoLocal}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterContratoServicosBean.feriadosLocais}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>

		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>
		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>
		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>

		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brSelectOneRadio id="rdoPermiteFavConPag21" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento}" >
						<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
	           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
   	        		</br:brSelectOneRadio>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicaoAgendamento21" value="#{manterContratoServicosBean.cboTipoRejeicaoAgendamento}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorRejeicaoAgendaLote == 'N'}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRejeicaoAgendamento}" />
							<a4j:support event="onchange" reRender="panelLote" action="#{manterContratoServicosBean.controlarCamposLote}" status="statusAguarde" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicaoEfetivacao21" value="#{manterContratoServicosBean.cboTipoRejeicaoEfetivacao}" 
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorRejeicaoEfetivacaoLote == 'N'}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRejeicaoEfetivacao}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" id="panelLote">
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup id="panelQtdeLote1">	
			   			<br:brInputText size="7" maxlength="5" id="txtQtddMaximaRegistroInconLote21" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.desabilitarCamposLote || manterContratoServicosBean.desabilitaIndicadorMaximaInconLote}">
			   				<a4j:support event="onblur" reRender="panelPercLote1" action="#{manterContratoServicosBean.controlarCampoPercLote}" status="statusAguarde" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />  
			   			</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup id="panelPercLote1">	
			   			<br:brInputText size="4" maxlength="3" id="txtPercentualMaximoRegistroInconsLote21" value="#{manterContratoServicosBean.percMaxRegInconsistente}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.desabilitarCamposLote || manterContratoServicosBean.desabilitaIndicadorPercentualMaximoInconLote}" >
			   				<a4j:support event="onblur" reRender="panelQtdeLote1" action="#{manterContratoServicosBean.controlarCampoQtdeLote}" status="statusAguarde" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />  
			   			</br:brInputText>	
			   			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="%"/>
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_prioridade_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboPrioridadeDebito21" value="#{manterContratoServicosBean.cboPrioridadeDebito}"
							disabled="#{manterContratoServicosBean.desabilitarComboPrioridadeDebito}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaPrioridadeDebito}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_debito_online}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoPermiteDebOnline21" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermiteDebitoOnline}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoUtilizaCadFavControlePag21" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
		           			<a4j:support event="onclick" reRender="panelValorMaximoFavNaoCad" action="#{manterContratoServicosBean.controlarCampoValorMaximoFavNaoCad}" status="statusAguarde"  oncomplete="loadMasks();"/>
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup id="panelValorMaximoFavNaoCad">	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorMaximoFavNaoCad21" 
			   				value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" disabled="#{manterContratoServicosBean.desabilitaValorMaximoFavNaoCad}" onkeypress="onlyNum();">  
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorLimitInd21" 
			   				value="#{manterContratoServicosBean.valorLimiteIndividual}" onkeypress="onlyNum();" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.vlIndicadorLimiteIndividualPagamento != 'S'}">  
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorLimDiario21" 
			   				value="#{manterContratoServicosBean.valorLimiteDiario}" onkeypress="onlyNum();" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.vlIndicadorLimiteIndividualPagamento != 'S'}">  
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="4" maxlength="3" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQtddDiasRepique21" value="#{manterContratoServicosBean.qtddDiasRepique}" onkeypress="onlyNum();"
			   			disabled="#{manterContratoServicosBean.saidaVerificarAtributo.qtIndicadorDiaRepiqConsulta != 'S'}">  
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="4" maxlength="3" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQtddDiasFloating21" value="#{manterContratoServicosBean.qtddDiasFloating}" onkeypress="onlyNum();"
			   			disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorDiaFloatPagamento != 'S'}">  
			   			</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoGerarLacnFutDeb21" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoGerarLancFutDeb}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_lancamento_futuro_credito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoGerarLacnFutCred21" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoGerarLancFutCred}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_demonstra_duas_linha_extrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brSelectOneRadio id="rdoDemonstra2LinhaExtrato17" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoIndicadorSegundaLinhaExtrato}" >
						<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
	           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
   	        		</br:brSelectOneRadio>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
		
	</br:brPanelGrid>
	
	<!-- Pagamento Via T�tulo Bradesco -- CdParametro:22-->
	<!-- Pagamento Via T�tulo Outros Bancos -- CdParametro:23-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 22 ||
								manterContratoServicosBean.cdParametroTela == 23}" columns="1" cellpadding="0" cellspacing="0" width="100%">

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoConsultaSaldo22" value="#{manterContratoServicosBean.cboTipoConsultaSaldo}" 
							disabled="#{manterContratoServicosBean.desabilitarComboTipoConsultaSaldo}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoConsultaSaldo}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup rendered="#{manterContratoServicosBean.cdParametroTela == 23}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.tipo_consulta_saldo_titulo_valor_superior}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" />

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboConsultaSaldoValorSuperior22" value="#{manterContratoServicosBean.cdConsultaSaldoValorSuperior}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorConsultaSaldoSuperior == 'N'}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}" />
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoConsultaSaldo}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>

			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaTipoConsSaldoTituloValorSuperior}"/>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_processamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboProcessamentoEfetivacaoPagamento22" value="#{manterContratoServicosBean.cboProcessamentoEfetivacaoPagamento}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaProcessamentoEfetivacaoPagamento}" />
							
							<a4j:support event="onchange" reRender="cboTratFevDataPag22, cboFeriadoLocal22" action="#{manterContratoServicosBean.recarregarCombomFeriado}" status="statusAguarde" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
						<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboTratFevDataPag22" value="#{manterContratoServicosBean.cboTratamentoFeriadosDataPagamento}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTratamentoFeriadosDataPagamento}" />
						</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_pgto_feriado_local}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px">
					<br:brSelectOneMenu id="cboFeriadoLocal22" value="#{manterContratoServicosBean.cdIndicadorFeriadoLocal}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterContratoServicosBean.feriadosLocais}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>

		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>
		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>
		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>

		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brSelectOneRadio id="rdoPermiteFavConPag22" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento}" >
						<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
	           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
   	        		</br:brSelectOneRadio>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicaoAgendamento22" value="#{manterContratoServicosBean.cboTipoRejeicaoAgendamento}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorRejeicaoAgendaLote == 'N'}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRejeicaoAgendamento}" />
							<a4j:support event="onchange" reRender="panelLote22" action="#{manterContratoServicosBean.controlarCamposLote}" status="statusAguarde" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicaoEfetivacao22" value="#{manterContratoServicosBean.cboTipoRejeicaoEfetivacao}" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorRejeicaoEfetivacaoLote == 'N'}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRejeicaoEfetivacao}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" id="panelLote22">
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup id="panelQtdeLote122">	
			   			<br:brInputText size="7" maxlength="5" id="txtQtddMaximaRegistroInconLote22" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.desabilitarCamposLote || manterContratoServicosBean.desabilitaIndicadorMaximaInconLote}" >
			   				<a4j:support event="onblur" reRender="panelPercLote122" action="#{manterContratoServicosBean.controlarCampoPercLote}" status="statusAguarde" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />  
			   			</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup id="panelPercLote122">	
			   			<br:brInputText size="4" maxlength="3" id="txtPercentualMaximoRegistroInconsLote22" value="#{manterContratoServicosBean.percMaxRegInconsistente}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.desabilitarCamposLote || manterContratoServicosBean.desabilitaIndicadorPercentualMaximoInconLote}" >
			   				<a4j:support event="onblur" reRender="panelQtdeLote122" action="#{manterContratoServicosBean.controlarCampoQtdeLote}" status="statusAguarde" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />  
			   			</br:brInputText>	
			   			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="%"/>
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_prioridade_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboPrioridadeDebito22" value="#{manterContratoServicosBean.cboPrioridadeDebito}" 
							disabled="#{manterContratoServicosBean.desabilitarComboPrioridadeDebito}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaPrioridadeDebito}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoGerarLancFutDebito22" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoGerarLancFutDeb}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoUtilizaCadFavControlePag22" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
		           			<a4j:support event="onclick" reRender="panelValorMaximoFavNaoCad22" action="#{manterContratoServicosBean.controlarCampoValorMaximoFavNaoCad}" status="statusAguarde"  oncomplete="loadMasks();"/>
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup id="panelValorMaximoFavNaoCad22">	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" 
			   				alt="decimalBr" id="txtValorMaximoFavNaoCad22" value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" disabled="#{manterContratoServicosBean.desabilitaValorMaximoFavNaoCad}" onkeypress="onlyNum();">  
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorLimitInd22" 
			   				value="#{manterContratoServicosBean.valorLimiteIndividual}" onkeypress="onlyNum();" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.vlIndicadorLimiteIndividualPagamento != 'S'}">  
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorLimDiario22" 
			   				value="#{manterContratoServicosBean.valorLimiteDiario}" onkeypress="onlyNum();" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.vlIndicadorLimiteDiaPagamento != 'S'}">  
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="4" maxlength="3" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQtddDiasRepique22" 
			   				value="#{manterContratoServicosBean.qtddDiasRepique}" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.qtIndicadorDiaRepiqConsulta != 'S'}" onkeypress="onlyNum();">  
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="4" maxlength="3" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQtddDiasFloating22" value="#{manterContratoServicosBean.qtddDiasFloating}"  
			   			disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorDiaFloatPagamento != 'S'}" onkeypress="onlyNum();">  
			   			</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_pagar_menor}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoPermitePagarMenor22" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermitePagarMenor}" 
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorAgendaValorMenor != 'S'}">
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_pagar_vencido}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoPermitePagarVencido22" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermitePagarVencido}" 
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorAgendaPagamentoVencido != 'S'}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
		           			<a4j:support event="onclick" reRender="panelQuantidadeLimiteDiasPagamentoVencido" action="#{manterContratoServicosBean.controlarCampoQtdeLimDiasPgtoVenc}" status="statusAguarde" />
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_limite_dias_para_pagamento_vencido}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup id="panelQuantidadeLimiteDiasPagamentoVencido">	
			   			<br:brInputText size="4" maxlength="3" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQuantidadeLimiteDiasPagamentoVencido22" 
			   				value="#{manterContratoServicosBean.quantidadeLimiteDiasPagamentoVencido}" disabled="#{manterContratoServicosBean.desabilitarCampoQtdeLimDiasPgtoVenc}" onkeypress="onlyNum();">  
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_demonstra_duas_linha_extrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoDemonstra2LinhaExtrato22" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoIndicadorSegundaLinhaExtrato}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_indicador_de_titulo_dda_no_retorno}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoIndicadorDdaRetorno" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoIndicadorDdaRetorno}">
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_consistencia_cpf_cnpj_benef_sacador_aval_npc}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboConsistenciaCpfCnpjBenefAvalNpc22" value="#{manterContratoServicosBean.cboConsistenciaCpfCnpjBenefAvalNpc}" 
							disabled="#{manterContratoServicosBean.desabilitarCboConsistenciaCpfCnpjBenefAvalNpc}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaConsistenciaCpfCnpjBenefAvalNpc}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>					
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_percentual_diferenca_tolerada}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brInputText maxlength="6" size="7" alt="percentual"
						onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');"
						id="txtPercentualDiferencaTolerada"
						converter="decimalBrazillianConverter"
						value="#{manterContratoServicosBean.vlPercentualDiferencaTolerada}">
					</br:brInputText>
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value=" #{msgs.label_porcentagem}" />
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		

		
	</br:brPanelGrid>
	
	<!-- Rastreabilidade de T�tulos -- CdParametro:26-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 26}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rastreamento_titulos}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboTipoRastreamentoTitulo26" value="#{manterContratoServicosBean.cboTipoRastreamentoTitulo}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRastreamentoTitulos}" />
							<a4j:support event="onclick" reRender="rdoAgendarTituloRastreadoFilial26" action="#{manterContratoServicosBean.permiteAlteracoesTitulo}"/>
						</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_rastrear_sacado_agregado}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoRastrearTituloTerceiros26" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoRastrearTituloTerceiros}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_rastrear_notas_fiscais}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoRastrearNotasFiscais26" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoRastrearNotasFiscais}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.confModalidades_rastreamentoTitulos_cliente_pagador}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoAgendarTituloRastProp26" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoAgendarTituloRastProp}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>		
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage  style="margin-left:15px" url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_Exige_Identificacao_Filial_Autorizacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
							<br:brSelectOneRadio style="margin-left:10px"
								id="rdoExigeIdentificacaoFilialAutorizacao"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoExigeIdentificacaoFilialAutorizacao}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_bloquear_emissao_papeleta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoBloqEmissaoPapeleta26" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoBloqEmissaoPapeleta}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
		           			<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="panelDtInicioBloqPapeleta26" action="#{manterContratoServicosBean.limparDataPapeleta}" />				
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
					
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_data_inicio_bloqueio_papeleta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:7px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
						
				<a4j:outputPanel id="panelDtInicioBloqPapeleta26" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >	 
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup rendered="#{manterContratoServicosBean.rdoBloqEmissaoPapeleta == '1'}">
							<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dtInicioBloqPapeleta26');" id="dtInicioBloqPapeleta26" value="#{manterContratoServicosBean.dtInicioBloqPapeleta}"/>							
						</br:brPanelGroup>
						<br:brPanelGroup rendered="#{manterContratoServicosBean.rdoBloqEmissaoPapeleta != '1'}">
							<app:calendar id="dtInicioBloqPapeleta26Disabled" disabled="true" value="#{manterContratoServicosBean.dtInicioBloqPapeleta}"/>						
						</br:brPanelGroup>
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="3"  cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_data_inicio_rastreamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:7px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
						
				<a4j:outputPanel id="panelDtInicioRastreamento26" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
							<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dtInicioRastreamento26');" id="dtInicioRastreamento26" value="#{manterContratoServicosBean.dtInicioRastreamento}">
						</app:calendar>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</a4j:outputPanel>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_rastrear_somente_boleto_bradesco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoAdesaoSacadoEletronico26" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoAdesaoSacadoEletronico}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.conServicosManterContrato_btnVoltar}" action="#{manterContratoServicosBean.voltarModalidadeConfigurar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" style="margin-right:5px" value="#{msgs.conServicosManterContrato_btnLimpar}"  action="#{manterContratoServicosBean.limparConfiguracaoMod}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnSalvar" styleClass="bto1" value="#{msgs.btn_avancar}" action="#{manterContratoServicosBean.salvarConfiguracaoMod}" onclick="if(!validaModalidadesConfiguracaoP2('#{msgs.label_ocampo}', 
				'#{msgs.label_necessario}','#{msgs.label_abertura_conta_banco_postal_bradesco_seguros}','#{msgs.label_acao_nao_comprovacao}','#{msgs.label_rastrear_somente_boleto_bradesco}','#{msgs.label_agendamento_debitos_pendentes_veiculos}'
				,'#{msgs.label_agendar_titulo_rastreado_filial}','#{msgs.label_agendar_titulo_rastreado_proprio}','#{msgs.label_agrupar_correspondencias}','#{msgs.label_autorizacao_complementar_agencia}'
				,'#{msgs.label_bloquear_emissao_papeleta}','#{msgs.label_cadastro_utilizado_recadastramento}','#{msgs.label_cadastro_consulta_endereco_envio}'
				,'#{msgs.label_cobrar_tarifas_funcionarios}','#{msgs.label_codigo_formulario}','#{msgs.label_condicao_enquadramento}','#{msgs.label_controle_expiracao_credito}'
				,'#{msgs.label_criterio_composto_enquadramento}','#{msgs.label_criterio_principal_enquadramento}','#{msgs.label_data_inicio_bloqueio_papeleta}','#{msgs.label_data_inicio_rastreamento}'
				,'#{msgs.label_data_fim_periodo_acerto_dados}','#{msgs.label_data_fim_recadastramento}','#{msgs.label_data_inicio_periodo_acerto_dados}','#{msgs.label_data_inicio_recadastramento}'
				,'#{msgs.label_data_limite_enquadramento_convenio_conta_salario}','#{msgs.label_data_limite_vinculo_cliente_beneficiario}','#{msgs.label_demonstra_informacoes_area_reservada}','#{msgs.label_destino_envio}'
				,'#{msgs.label_destino_envio_correspondencia}','#{msgs.label_efetua_consistencia_especie_beneficio}'
				,'#{msgs.label_emissao_antecipada_cartao_conta_salario}','#{msgs.label_emite_aviso_comprovacao_vida}','#{msgs.label_emite_msg_recadastramento}','#{msgs.label_exige_liberacao_lote_processado}'
				,'#{msgs.label_forma_autorizacao_pagamentos}','#{msgs.label_forma_envio_pagamentos}','#{msgs.label_forma_manutencao_cadastro_favorecidos}','#{msgs.label_forma_manutencao_cadastro_procuradores}'
				,'#{msgs.label_formulario_impressao}','#{msgs.label_gerar_lancamento_futuro_credito}','#{msgs.label_gerar_lancamento_futuro_debito}','#{msgs.label_gerar_lancamento_programado}'
				,'#{msgs.label_gerar_retorno_operacoes_realizadas_internet}','#{msgs.label_informa_agencia_conta_credito}','#{msgs.label_midia_disponibilizacao_correntista}','#{msgs.label_midia_mensagem_recadastramento}'
				,'#{msgs.label_momento_envio}','#{msgs.label_momento_indicacao_credito_efetivado}','#{msgs.label_ocorrencia_debito}','#{msgs.label_percentual_maximo_registros_inconsistentes_lote}'
				,'#{msgs.label_periodicidade_emissao}','#{msgs.label_periodicidade_manutencao_cadastro_procuradores}','#{msgs.label_periodicidade_envio_remessa_manutencao}','#{msgs.label_periodicidade_pesquisa_debitos_pendentes_veiculos}'
				,'#{msgs.label_permite_acertos_dados}','#{msgs.label_permite_antecipar_recadastramento}','#{msgs.label_permite_contigencia_pagamento}','#{msgs.label_permite_debito_online}'
				,'#{msgs.label_permite_envio_remessa_manutencao_cadastro}','#{msgs.label_permite_estorno_pagamento}','#{msgs.label_permite_favorecido_consultar_pagamento}','#{msgs.label_permite_pagar_menor}'
				,'#{msgs.label_permite_pagar_vencido}','#{msgs.label_permitir_agrupamento_correspondencia}','#{msgs.label_pesquisa_de}','#{msgs.label_possui_expiracao_credito}'
				,'#{msgs.label_pre_autorizacao_cliente}','#{msgs.label_prioridade_debito}','#{msgs.label_tipo_processamento}','#{msgs.label_quantidade_dias_floating}'
				,'#{msgs.label_quantidade_dias_repique}','#{msgs.label_quantidade_dias_emissao_aviso_antes_inicio_comprovacao}','#{msgs.label_quantidade_dias_emissao_aviso_antes_vencimento}','#{msgs.label_quantidade_dias_expiracao_credito}'
				,'#{msgs.label_quantidade_dias_aviso_antecipado}','#{msgs.label_quantidade_dias_envio_antecipado_formulario}','#{msgs.label_quantidade_dias_para_expiracao}','#{msgs.label_quantidade_dias_para_inativacao_favorecido}'
				,'#{msgs.label_quantidade_fases_etapa}','#{msgs.label_quantidade_linhas_comprovante}','#{msgs.label_quantidade_meses_emissao}','#{msgs.label_quantidade_meses_periodicidade_comprovacao}'
				,'#{msgs.label_quantidade_meses_etapa}','#{msgs.label_quantidade_meses_fase}','#{msgs.label_quantidade_vias_emitir}','#{msgs.label_quantidade_vias_pagas_registro}'
				,'#{msgs.label_quantidade_etapas_recadastramento}','#{msgs.label_quantidade_limite_cartao_conta_salario_solicitacao}','#{msgs.label_quantidade_limite_dias_para_pagamento_vencido}','#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}'
				,'#{msgs.label_rastrear_notas_fiscais}','#{msgs.label_rastrear_sacado_agregado}','#{msgs.label_texto_informacao_area_reservada}','#{msgs.label_tipo_cartao_conta_salario}'
				,'#{msgs.label_tipo_consistencia_cpf_cnpj_proprietario}','#{msgs.label_tipo_consistencia_identificacao_beneficiario}','#{msgs.label_tipo_carga_cadastro_beneficiario}','#{msgs.label_tipo_consistencia_inscricao_favorecido}'
				,'#{msgs.label_tipo_consistencia_cpf_cnpj_favorecido}','#{msgs.label_tipo_consolidacao_pagamentos_extrato}','#{msgs.label_tipo_data_controle_floating}','#{msgs.label_tipo_emissao}'
				,'#{msgs.label_tipo_formacao_lista_debito}','#{msgs.label_tipo_inscricao_favorecido}','#{msgs.label_tipo_rastreamento_titulos}','#{msgs.label_tipo_rejeicao_lote}'
				,'#{msgs.label_tipo_rejeicao_efetivacao}','#{msgs.label_tipo_rejeicao_agendamento}','#{msgs.label_tipo_tratamento_conta_transferida}','#{msgs.label_tipo_identificacao_beneficiario}'
				,'#{msgs.label_tratamento_pagamento_dia_nao_util}','#{msgs.label_tratamento_feriado_fim_vigencia}','#{msgs.label_tratamento_lista_debito_sem_numeracao}','#{msgs.label_tratamento_valor_divergente}'
				,'#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}','#{msgs.label_utiliza_cafastro_orgaos_pagadores}','#{msgs.label_utiliza_cadastro_procuradores}'
				,'#{msgs.label_utiliza_frases_pre_cadastradas}','#{msgs.label_utiliza_lista_debito}','#{msgs.label_utiliza_mensagem_personalizada_online}'
				,'#{msgs.label_valor_limite_agendamento}','#{msgs.label_valor_limite_agendamento_individual}','#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}'
				,'#{msgs.label_tipoContaCredito}','#{msgs.label_tipo_de_consulta_de_saldo}','#{msgs.label_quantidade_percentual_maximo_registros_inconsistentes_lote}', '#{msgs.label_pgto_feriado_local}', '#{msgs.label_indicador_de_titulo_dda_no_retorno}')){desbloquearTela(); return false;}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>