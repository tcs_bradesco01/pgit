<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="alterarPorTipoServicoManterContrato" name="alterarPorTipoServicoManterContrato">
	<h:inputHidden id="cdParametro" value="#{manterContratoServicosBean.cdParametroTela}" />
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_segmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" 
				value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.gerenteResponsavelFormatado}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpj}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocial}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_contrato}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_empresaGestora}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_tipoContrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_numero}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_descricao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.descricaoContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>


		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_motivo}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>
		</br:brPanelGrid>

		<!-- Pagamento de Tributos e Contas de Consumo -->
		<br:brPanelGrid  columns="1" cellpadding="0" cellspacing="0" width="100%">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_tipoDeServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoServicoSelecionado}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_situacaoDoServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsSituacaoServico}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_forma_envio_pagamentos}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboFormaEnvioPagamento" value="#{manterContratoServicosBean.cboFormaEnvioPagamento}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFormaEnvioPagamento != 'S'}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems value="#{manterContratoServicosBean.listaFormaEnvioPagamento}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_autorizacao_complementar_agencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoAutCompAg" styleClass="HtmlSelectOneRadioBradesco" 
								value="#{manterContratoServicosBean.rdoAutComplementarAg}" 
								disabled="#{manterContratoServicosBean.desabilitaIndicadorAutorizacaoComplemento}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid style="margin-left:20px;" columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_quantidade_dias_reutilização_controle_pagamento}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid style="margin-left:23px; margin-top:7px;" columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText size="5" maxlength="3" id="txtQtdDiasReutilizacaoControlePgto"
								value="#{manterContratoServicosBean.qtdDiasReutilizacaoControlePgto}" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_operacoes_realizadas}:" />
						</br:brPanelGroup>

						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGroup>
							<br:brSelectOneMenu id="comboRetornoOperacoesSelecionado" value="#{manterContratoServicosBean.comboRetornoOperacoesSelecionado}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems value="#{manterContratoServicosBean.listaRetornoOperacoes}" />
								<a4j:support event="onchange" reRender="comboRetornoTipoManutencao"
									action="#{manterContratoServicosBean.comboDisabledGerarRetornoTipoManutencao}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
				
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_tipo_manutencao}:" />
						</br:brPanelGroup>
			
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
	
						<br:brPanelGroup>
							<br:brSelectOneMenu disabled="#{manterContratoServicosBean.renderizaCombo == 0}" id="comboRetornoTipoManutencao" value="#{manterContratoServicosBean.comboCodIndicadorTipoRetornoInternet}">
								<f:selectItem itemValue="99" itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems value="#{manterContratoServicosBean.listaCodIndicadorTipoRetornoInternet}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_separado_canal}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoGerarRetornoSeparadoCanal" styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoGerarRetornoSeparadoCanal}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
				
				
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid rendered="#{manterContratoServicosBean.renderizaCamposDebitosPendentesDeVeiculos}"
				cellpadding="0" cellspacing="0" columns="1" width="100%">

				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid width="306" columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_utiliza_lista_debito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoUtilizaListaDebito" styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoUtilizaListaDebitos}"
								disabled="#{manterContratoServicosBean.desabilitaCdIndicadorListaDebito}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
								<a4j:support event="onclick" reRender="cboTipoFormacaoListaDebito, cboTratamentoListaDebitoSemNumeracao"
									action="#{manterContratoServicosBean.controlaRadioListaDebito}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_utiliza_Mora}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoUtilizaMora" styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoUtilizaMora}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
							value="#{msgs.label_tipo_formacao_lista_debito}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoFormacaoListaDebito" value="#{manterContratoServicosBean.cboTipoFormacaoListaDebito}"
							disabled="#{manterContratoServicosBean.rdoUtilizaListaDebitos == '2' || manterContratoServicosBean.desabilitaCdIndicadorTipoFormacaoLista}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}" />
							<f:selectItems value="#{manterContratoServicosBean.listaTipoFormacaoListaDebito}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
				
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tratamento_lista_debito_sem_numeracao}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTratamentoListaDebitoSemNumeracao"
								value="#{manterContratoServicosBean.cboTratamentoListaDebitoSemNumeracao}"
								disabled="#{manterContratoServicosBean.rdoUtilizaListaDebitos == '2' || manterContratoServicosBean.desabilitaCdIndicadorTipoConsistenciaLista}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems value="#{manterContratoServicosBean.listaTratamentoListaDebitoNumeracao}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_consolidacao_pagamentos_extrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu
								id="cboTipoConsolidacaoPagamentosComprovante" value="#{manterContratoServicosBean.cboTipoConsolidacaoPagamentosComprovante}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorTipoConsultaComprovante != 'S'}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems value="#{manterContratoServicosBean.listaTipoConsolidacaoPagamentosComprovante}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="text-align:left;width:150px">
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left"
					value="#{msgs.conServicosManterContrato_btnVoltar}" action="#{manterContratoServicosBean.voltar}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px">
				<br:brCommandButton id="btnLimpar" styleClass="bto1" style="margin-right:5px"
					value="#{msgs.conServicosManterContrato_btnLimpar}" action="#{manterContratoServicosBean.limparConfiguracao}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnSalvar" styleClass="bto1" value="#{msgs.btn_avancar}" action="#{manterContratoServicosBean.salvarConfiguracao}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>	
</brArq:form>