<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>

<brArq:form id="incServicosManterContratoOpcoesForm"
	name="incServicosManterContratoForm">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"
		cellspacing="0">

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.conServicosManterContrato_clienteMaster}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.cpfCnpjMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.nomeRazaoSocialMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.grupoEconomicoMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.atividadeEconomicaMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_clienteMaster_segmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.segmentoMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.subSegmentoMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.lebel_agencia_gestora}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.dsAgenciaGestora}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.gerenteResponsavelFormatado}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.conDadosBasicos_clienteParticipante}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
			width="100%">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_cpf_cnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.cpfCnpj}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_nome_razao_social}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.nomeRazaoSocial}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.conDadosBasicos_contrato}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_empresaGestora}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.empresa}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_tipoContrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.tipo}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_numero}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.numero}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_descricao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.descricaoContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>


		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.situacaoDesc}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_motivo}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.motivoDesc}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_participacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.participacao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.identidicacao_midia_de_Comprovante}:" />

				<br:brSelectOneMenu style="margin-left:5px"
					id="identificacaoMidiaComprov"
					value="#{manterContratoServicosBean.cdMidiaDisponComprovante}">
					<f:selectItem itemValue="0"
						itemLabel="#{msgs.identificacaoClienteContrato_selecione}" />
					<f:selectItem itemValue="1"
						itemLabel="#{msgs.identidicacao_midia_de_Comprovante_BDN}" />
					<f:selectItem itemValue="2"
						itemLabel="#{msgs.identidicacao_midia_de_Comprovante_Internet}" />
					<f:selectItem itemValue="3"
						itemLabel="#{msgs.identidicacao_midia_de_Comprovante_Ambos}" />
					<a4j:support event="onchange" reRender="btnIncluir" />
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="EspacamentoLinhas" />

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.identificacao_quantidade_funcionarios}:" />
				<br:brInputText id="txtQtdFuncionarios"
					style="margin-right: 5;width:90px;" maxlength="9"
					converter="javax.faces.Long" onkeypress="onlyNum();"
					styleClass="HtmlInputTextBradesco"
					value="#{manterContratoServicosBean.qtFuncionarios}"
					onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					<a4j:support event="onblur" reRender="btnIncluir" />
				</br:brInputText>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="EspacamentoLinhas" />

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.identificacao_de_meses_disponiveis}:" />
				<br:brInputText id="txtQtdMeses" style="margin-right: 5;width:30px;"
					maxlength="1" converter="javax.faces.Integer"
					onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco"
					onkeyup="return limitarDigitosRange(this, 1, 6);"
					value="#{manterContratoServicosBean.qtMesesEmissao}"
					onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
					<a4j:support event="onblur" reRender="btnIncluir" />
				</br:brInputText>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="EspacamentoLinhas" />

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.identificacao_de_emissoes_pagas_pela_empresa}:" />

				<br:brSelectOneMenu style="margin-left:5px" id="QtdEmissoesPagas"
					value="#{manterContratoServicosBean.qtEmissoesPagasEmpresa}">
					<f:selectItem itemValue="0"
						itemLabel="#{msgs.identificacaoClienteContrato_selecione}" />
					<f:selectItem itemValue="1"
						itemLabel="#{msgs.identificacao_de_emissoes_pagas_pela_empresa_Uma}" />
					<f:selectItem itemValue="2"
						itemLabel="#{msgs.identificacao_de_emissoes_pagas_pela_empresa_Duas}" />
					<f:selectItem itemValue="3"
						itemLabel="#{msgs.identificacao_de_emissoes_pagas_pela_empresa_Tres}" />
					<f:selectItem itemValue="99"
						itemLabel="#{msgs.identificacao_de_emissoes_pagas_pela_empresa_Todas}" />
					<a4j:support event="onchange" reRender="btnIncluir" />
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="EspacamentoLinhas" />

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.identificacao_contratacao}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brSelectOneRadio
						value="#{manterContratoServicosBean.cdFlagRepresentante}">
						<f:selectItem itemValue="S"
							itemLabel="#{msgs.identificacao_representante}" />
						<f:selectItem itemValue="N"
							itemLabel="#{msgs.identificacao_representante_e_participante}" />
						<a4j:support event="onclick"
							action="#{manterContratoServicosBean.modificaOpcaoRepresentante}"
							reRender="incServicosManterContratoOpcoesForm,btnIncluir" />
					</br:brSelectOneRadio>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<app:scrollableDataTable id="tableRepresentantes"
				value="#{manterContratoServicosBean.listaRepresentantes.ocorrencias}"
				width="100%" rowIndexVar="parametroKey" var="result"
				rowClasses="tabela_celula_normal, tabela_celula_destaque">
				<app:scrollableColumn styleClass="colTabCenter" width="7px">
					<f:facet name="header">
					</f:facet>
					<t:selectBooleanCheckbox id="sorLista"
						styleClass="HtmlSelectOneRadioBradesco"
						value="#{result.selecionado}">
						<f:selectItems value="" />
						<a4j:support event="onclick" reRender="tableRepresentantes"
							onsubmit="limparDtoRepresentante(this.id.replace('sorLista',''));" />
					</t:selectBooleanCheckbox>
				</app:scrollableColumn>

				<app:scrollableColumn width="120" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="CPF/CNPJ" styleClass="tableFontStyle"
							style="width:120; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.cpfCnpjFormatado}" />
				</app:scrollableColumn>

				<app:scrollableColumn width="250" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="Nome/Raz�o Social"
							styleClass="tableFontStyle" style="width:250; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dsNomeRazao}" />
				</app:scrollableColumn>

				<app:scrollableColumn width="100" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="Nome Reduzido" styleClass="tableFontStyle"
							style="width:100; text-align:center" />
					</f:facet>
					<br:brInputText value="#{result.dsnome}" maxlength="16"
						style="width: 100%;" disabled="#{!result.selecionado}"
						id="inputDsNome">
						<a4j:support event="onblur"
							action="#{manterContratoServicosBean.atualizaDadosBean}" />
					</br:brInputText>
				</app:scrollableColumn>

				<app:scrollableColumn width="60" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="Ag�ncia" styleClass="tableFontStyle"
							style="width:60; text-align:center" />
					</f:facet>
					<br:brInputText value="#{result.cdAgencia}" style="width: 100%;"
						maxlength="5" onkeypress="onlyNum();"
						converter="javax.faces.Integer" disabled="#{!result.selecionado}"
						id="inputCdAgencia">
						<a4j:support event="onblur"
							action="#{manterContratoServicosBean.atualizaDadosBean}" />
					</br:brInputText>
				</app:scrollableColumn>

				<app:scrollableColumn width="60" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="Conta" styleClass="tableFontStyle"
							style="width:60; text-align:center" />
					</f:facet>
					<br:brInputText value="#{result.cdConta}" style="width: 100%;"
						onkeypress="onlyNum();" maxlength="7"
						converter="javax.faces.Integer" disabled="#{!result.selecionado}"
						id="inputCdConta">
						<a4j:support event="onblur"
							action="#{manterContratoServicosBean.atualizaDadosBean}" />
					</br:brInputText>
				</app:scrollableColumn>

				<app:scrollableColumn width="30" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="Digito" styleClass="tableFontStyle"
							style="width:30; text-align:center" />
					</f:facet>
					<br:brInputText value="#{result.cdContaDigito}"
						style="width: 100%;" onkeypress="onlyNum();" maxlength="1"
						converter="javax.faces.Integer" disabled="#{!result.selecionado}"
						id="inputCdContaDigito">
						<a4j:support event="onblur"
							action="#{manterContratoServicosBean.atualizaDadosBean}" />
					</br:brInputText>
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>


		<a4j:outputPanel id="panelBotoes"
			style="width: 100%; text-align: right" ajaxRendered="true">
			<br:brPanelGrid columns="3" width="100%" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup style="text-align:left;width:150px">
					<br:brCommandButton id="btnVoltar" styleClass="bto1"
						style="align:left"
						value="#{msgs.conServicosManterContrato_btnVoltar}"
						action="#{manterContratoServicosBean.voltar}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>
				<br:brPanelGroup style="text-align:right;width:100%">
					<br:brCommandButton id="btnIncluir" styleClass="bto1"
						style="margin-right:5px"
						disabled="#{!manterContratoServicosBean.incluirRepresentantesHabilitado}"
						value="#{msgs.conServicosManterContrato_btnAvancar}"
						action="#{manterContratoServicosBean.incluirServicosRepresentantesConfirmar}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>

	</br:brPanelGrid>
</brArq:form>