<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="confServicosManterContrato"
	name="confServicosManterContrato">
	<h:inputHidden id="cdParametro"
		value="#{manterContratoServicosBean.cdParametroTela}" />
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"
		cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.conServicosManterContrato_clienteMaster}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.cpfCnpjMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.nomeRazaoSocialMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.grupoEconomicoMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.atividadeEconomicaMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_clienteMaster_segmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.segmentoMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.subSegmentoMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.lebel_agencia_gestora}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.dsAgenciaGestora}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.gerenteResponsavelFormatado}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.conDadosBasicos_clienteParticipante}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
			width="100%">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_cpf_cnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.cpfCnpj}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_nome_razao_social}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.nomeRazaoSocial}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.conDadosBasicos_contrato}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_empresaGestora}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.empresa}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_tipoContrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.tipo}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_numero}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.numero}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_descricao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.descricaoContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>


		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.situacaoDesc}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_motivo}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.motivoDesc}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_contrato_participacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoBean.participacao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<!--Pagamento de Fornecedores -->
		<br:brPanelGrid
			rendered="#{manterContratoServicosBean.cdParametroTela == 1}"
			columns="1" cellpadding="0" cellspacing="0" width="100%">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_tipoDeServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsTipoServicoSelecionado}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_situacaoDoServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsSituacaoServico}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_forma_envio_pagamentos}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboFormaEnvioPagamento"
								value="#{manterContratoServicosBean.cboFormaEnvioPagamento}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFormaEnvioPagamento != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaFormaEnvioPagamento}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_autorizacao_complementar_agencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoAutCompAg"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoAutComplementarAg}"
								disabled="#{manterContratoServicosBean.desabilitaIndicadorAutorizacaoComplemento}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid style="margin-left:20px;" columns="1"
						cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_quantidade_dias_reutiliza��o_controle_pagamento}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid style="margin-left:23px; margin-top:7px;"
						columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText size="5" maxlength="3"
								id="txtQtdDiasReutilizacaoControlePgto"
								value="#{manterContratoServicosBean.qtdDiasReutilizacaoControlePgto}" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_operacoes_realizadas}:" />
						</br:brPanelGroup>

						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGroup>
							<br:brSelectOneMenu id="comboRetornoOperacoesSelecionadoPagFornecedores" value="#{manterContratoServicosBean.comboRetornoOperacoesSelecionado}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems value="#{manterContratoServicosBean.listaRetornoOperacoes}" />
								<a4j:support event="onchange" reRender="comboRetornoTipoManutencaoPagFornecedores"
									action="#{manterContratoServicosBean.comboDisabledGerarRetornoTipoManutencao}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
				
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_tipo_manutencao}:" />
						</br:brPanelGroup>
			
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
	
						<br:brPanelGroup>
							<br:brSelectOneMenu disabled="#{manterContratoServicosBean.renderizaCombo == 0}" id="comboRetornoTipoManutencaoPagFornecedores" value="#{manterContratoServicosBean.comboCodIndicadorTipoRetornoInternet}">
								<f:selectItem itemValue="99" itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems value="#{manterContratoServicosBean.listaCodIndicadorTipoRetornoInternet}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_separado_canal}:" />
						</br:brPanelGroup>
						
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoGerarRetornoSeparadoCanalPagFornecedores" styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoGerarRetornoSeparadoCanal}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					
					</br:brPanelGrid>
				</br:brPanelGroup>
				
				
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_cadastro_float}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="radioCadastroFloating"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdtoCadastroFloating}"
								disabled="#{manterContratoServicosBean.permiteAlteracaoRadioFloating}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
								<a4j:support event="onclick"
									reRender="cboTipoDataControleFloating"
									action="#{manterContratoServicosBean.renderizaComboFloating}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tipo_data_controle_floating}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTipoDataControleFloating"
								value="#{manterContratoServicosBean.cboTipoDataControleFloating}"
								disabled="#{manterContratoServicosBean.permiteAlteracaoCombo}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTipoDataControleFloating}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_utiliza_lista_debito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoUtilizaListaDebito"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoUtilizaListaDebitos}"
								disabled="#{manterContratoServicosBean.desabilitaCdIndicadorListaDebito}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
								<a4j:support event="onclick"
									reRender="cboTipoFormacaoListaDebito, cboTratamentoListaDebitoSemNumeracao"
									action="#{manterContratoServicosBean.controlaRadioListaDebito}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tipo_formacao_lista_debito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTipoFormacaoListaDebito"
								value="#{manterContratoServicosBean.cboTipoFormacaoListaDebito}"
								disabled="#{manterContratoServicosBean.rdoUtilizaListaDebitos == '2' || manterContratoServicosBean.desabilitaCdIndicadorTipoFormacaoLista}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTipoFormacaoListaDebito}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tratamento_lista_debito_sem_numeracao}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTratamentoListaDebitoSemNumeracao"
								value="#{manterContratoServicosBean.cboTratamentoListaDebitoSemNumeracao}"
								disabled="#{manterContratoServicosBean.rdoUtilizaListaDebitos == '2' || manterContratoServicosBean.desabilitaCdIndicadorTipoConsistenciaLista}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTratamentoListaDebitoNumeracao}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tipo_consolidacao_pagamentos_extrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu
								id="cboTipoConsolidacaoPagamentosComprovante1"
								value="#{manterContratoServicosBean.cboTipoConsolidacaoPagamentosComprovante}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorTipoConsultaComprovante != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTipoConsolidacaoPagamentosComprovante}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<!--Pagamento de Sal�rios -->
		<br:brPanelGrid
			rendered="#{manterContratoServicosBean.cdParametroTela == 2}"
			columns="1" cellpadding="0" cellspacing="0" width="100%">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_tipoDeServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsTipoServicoSelecionado}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_situacaoDoServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsSituacaoServico}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_forma_envio_pagamentos}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboFormaEnvioPagamento2"
								value="#{manterContratoServicosBean.cboFormaEnvioPagamento}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFormaEnvioPagamento != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaFormaEnvioPagamento}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_autorizacao_complementar_agencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoAutCompAg2"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoAutComplementarAg}"
								disabled="#{manterContratoServicosBean.desabilitaIndicadorAutorizacaoComplemento}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid style="margin-left:20px;" columns="1"
						cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_quantidade_dias_reutiliza��o_controle_pagamento}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid style="margin-left:23px; margin-top:7px;"
						columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText size="5" maxlength="3"
								id="txtQtdDiasReutilizacaoControlePgto2"
								value="#{manterContratoServicosBean.qtdDiasReutilizacaoControlePgto}" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_operacoes_realizadas}:" />
						</br:brPanelGroup>

						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGroup>
							<br:brSelectOneMenu id="comboRetornoOperacoesSelecionado" value="#{manterContratoServicosBean.comboRetornoOperacoesSelecionado}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems value="#{manterContratoServicosBean.listaRetornoOperacoes}" />
								<a4j:support event="onchange" reRender="comboRetornoTipoManutencao"
									action="#{manterContratoServicosBean.comboDisabledGerarRetornoTipoManutencao}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
				
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_tipo_manutencao}:" />
						</br:brPanelGroup>
			
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
	
						<br:brPanelGroup>
							<br:brSelectOneMenu disabled="#{manterContratoServicosBean.renderizaCombo == 0}" id="comboRetornoTipoManutencao" value="#{manterContratoServicosBean.comboCodIndicadorTipoRetornoInternet}">
								<f:selectItem itemValue="99" itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems value="#{manterContratoServicosBean.listaCodIndicadorTipoRetornoInternet}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_separado_canal}:" />
						</br:brPanelGroup>
						
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoGerarRetornoSeparadoCanalPag" styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoGerarRetornoSeparadoCanal}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					
					</br:brPanelGrid>
				</br:brPanelGroup>
				
				
			</br:brPanelGrid>

			

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_cadastro_float}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="radioCadastroFloating5"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdtoCadastroFloating}"
								disabled="#{manterContratoServicosBean.permiteAlteracaoRadioFloating}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
								<a4j:support event="onclick"
									reRender="cboTipoDataControleFloating2"
									action="#{manterContratoServicosBean.renderizaComboFloating}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tipo_data_controle_floating}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTipoDataControleFloating2"
								value="#{manterContratoServicosBean.cboTipoDataControleFloating}"
								disabled="#{manterContratoServicosBean.permiteAlteracaoCombo}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTipoDataControleFloating}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"
				rendered="#{manterContratoServicosBean.flagExibeCampos == 'S'">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_emite_cartao_antecipado_conta_salario}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoEmissaoAntCartaoContSal"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoEmissaoAntCartaoContSal}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorCataoSalario != 'S'}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
								<a4j:support event="onclick"
									reRender="panelTipoCartaoContaSalario, panelqtddLimiteCartaoContaSalSol"
									action="#{manterContratoServicosBean.controlaTpCartaoQtdeLimite}"
									status="statusAguarde" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tipo_cartao}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup id="panelTipoCartaoContaSalario">
							<br:brSelectOneMenu id="cboTipoCartaoContaSalario"
								value="#{manterContratoServicosBean.cboTipoCartaoContaSalario}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorTipoCataoSalario != 'S' || manterContratoServicosBean.desabilitaTpCartaoQtdeLimite}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTipoCartaoContaSalario}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup
					rendered="#{manterContratoServicosBean.flagExibeCampos == 'S'">
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_quantidade_limite_solicitacao_cartao_conta_salario}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup id="panelqtddLimiteCartaoContaSalSol">
							<br:brInputText id="txtqtddLimiteCartaoContaSalSol" size="7"
								maxlength="5"
								value="#{manterContratoServicosBean.qtddLimiteCartaoContaSalarioSolicitacao}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.qtIndicadorLimiteSolicitacaoCatao != 'S' || manterContratoServicosBean.desabilitaTpCartaoQtdeLimite}"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								onkeypress="onlyNum();">
							</br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-left:20px"
						cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_data_limite_enquadramento_convenio_conta_salario}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:7px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<a4j:outputPanel id="calendario"
						style="width: 100%; text-align: left" ajaxRendered="true"
						onmousedown="javascript: cleanClipboard();">
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
								<app:calendar oninputkeydown="javascript: cleanClipboard();"
									oninputkeypress="onlyNum();"
									oninputchange="recuperarDataCalendario(document.forms[1],'dtLimiteEnqConvContaSal');"
									id="dtLimiteEnqConvContaSal"
									value="#{manterContratoServicosBean.dtLimiteEnqConvContaSal}"
									disabled="#{manterContratoServicosBean.saidaVerificarAtributo.dtIndicadorEnquaContaSalario != 'S'}" />
							</br:brPanelGroup>
						</br:brPanelGrid>
					</a4j:outputPanel>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_abertura_conta_bradesco_expresso}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoAbertContaBancoPostBradSeg"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoAbertContaBancoPostBradSeg}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorBancoPostal != 'S'}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_utiliza_lista_debito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoUtilizaListaDebito2"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoUtilizaListaDebitos}"
								disabled="#{manterContratoServicosBean.desabilitaCdIndicadorListaDebito}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
								<a4j:support event="onclick"
									reRender="cboTipoFormacaoListaDebito2, cboTratamentoListaDebitoSemNumeracao2"
									action="#{manterContratoServicosBean.controlaRadioListaDebito}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tipo_formacao_lista_debito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTipoFormacaoListaDebito2"
								value="#{manterContratoServicosBean.cboTipoFormacaoListaDebito}"
								disabled="#{manterContratoServicosBean.rdoUtilizaListaDebitos == '2' || manterContratoServicosBean.desabilitaCdIndicadorTipoFormacaoLista}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTipoFormacaoListaDebito}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tratamento_lista_debito_sem_numeracao}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTratamentoListaDebitoSemNumeracao2"
								value="#{manterContratoServicosBean.cboTratamentoListaDebitoSemNumeracao}"
								disabled="#{manterContratoServicosBean.rdoUtilizaListaDebitos == '2' || manterContratoServicosBean.desabilitaCdIndicadorTipoConsistenciaLista}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTratamentoListaDebitoNumeracao}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tipo_consolidacao_pagamentos_extrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu
								id="cboTipoConsolidacaoPagamentosComprovante2"
								value="#{manterContratoServicosBean.cboTipoConsolidacaoPagamentosComprovante}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorTipoConsultaComprovante != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTipoConsolidacaoPagamentosComprovante}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

		</br:brPanelGrid>

		<!-- Pagamento de Tributos e Contas de Consumo -->
		<br:brPanelGrid
			rendered="#{manterContratoServicosBean.cdParametroTela == 3}"
			columns="1" cellpadding="0" cellspacing="0" width="100%">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_tipoDeServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsTipoServicoSelecionado}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_situacaoDoServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsSituacaoServico}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_forma_envio_pagamentos}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboFormaEnvioPagamento3"
								value="#{manterContratoServicosBean.cboFormaEnvioPagamento}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFormaEnvioPagamento != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaFormaEnvioPagamento}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_autorizacao_complementar_agencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoAutCompAg3"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoAutComplementarAg}"
								disabled="#{manterContratoServicosBean.desabilitaIndicadorAutorizacaoComplemento}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brPanelGrid style="margin-left:20px;" columns="1"
						cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_quantidade_dias_reutiliza��o_controle_pagamento}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid style="margin-left:23px; margin-top:7px;"
						columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText size="5" maxlength="3"
								id="txtQtdDiasReutilizacaoControlePgto3"
								value="#{manterContratoServicosBean.qtdDiasReutilizacaoControlePgto}" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_operacoes_realizadas}:" />
						</br:brPanelGroup>

						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGroup>
							<br:brSelectOneMenu id="comboRetornoOperacoesSelecionadoTributos" value="#{manterContratoServicosBean.comboRetornoOperacoesSelecionado}">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems value="#{manterContratoServicosBean.listaRetornoOperacoes}" />
								<a4j:support event="onchange" reRender="comboRetornoTipoManutencaoTributos"
									action="#{manterContratoServicosBean.comboDisabledGerarRetornoTipoManutencao}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
				
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_tipo_manutencao}:" />
						</br:brPanelGroup>
			
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
	
						<br:brPanelGroup>
							<br:brSelectOneMenu disabled="#{manterContratoServicosBean.renderizaCombo == 0}" id="comboRetornoTipoManutencaoTributos" value="#{manterContratoServicosBean.comboCodIndicadorTipoRetornoInternet}">
								<f:selectItem itemValue="99" itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems value="#{manterContratoServicosBean.listaCodIndicadorTipoRetornoInternet}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_separado_canal}:" />
						</br:brPanelGroup>
						
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoGerarRetornoSeparadoCanalTributos" styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoGerarRetornoSeparadoCanal}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					
					</br:brPanelGrid>
				</br:brPanelGroup>
				
				
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>



			<br:brPanelGrid
				rendered="#{manterContratoServicosBean.renderizaCamposDebitosPendentesDeVeiculos}"
				cellpadding="0" cellspacing="0" columns="1" width="100%">

				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>



				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid width="306" columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_utiliza_lista_debito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoUtilizaListaDebito3"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoUtilizaListaDebitos}"
								disabled="#{manterContratoServicosBean.desabilitaCdIndicadorListaDebito}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
								<a4j:support event="onclick"
									reRender="cboTipoFormacaoListaDebito3, cboTratamentoListaDebitoSemNumeracao3"
									action="#{manterContratoServicosBean.controlaRadioListaDebito}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_utiliza_Mora}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoUtilizaMora"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoUtilizaMora}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
							value="#{msgs.label_tipo_formacao_lista_debito}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoFormacaoListaDebito3"
							value="#{manterContratoServicosBean.cboTipoFormacaoListaDebito}"
							disabled="#{manterContratoServicosBean.rdoUtilizaListaDebitos == '2' || manterContratoServicosBean.desabilitaCdIndicadorTipoFormacaoLista}">
							<f:selectItem itemValue="0"
								itemLabel="#{msgs.label_combo_selecione}" />
							<f:selectItems
									value="#{manterContratoServicosBean.listaTipoFormacaoListaDebito}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
				
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tratamento_lista_debito_sem_numeracao}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTratamentoListaDebitoSemNumeracao3"
								value="#{manterContratoServicosBean.cboTratamentoListaDebitoSemNumeracao}"
								disabled="#{manterContratoServicosBean.rdoUtilizaListaDebitos == '2' || manterContratoServicosBean.desabilitaCdIndicadorTipoConsistenciaLista}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTratamentoListaDebitoNumeracao}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tipo_consolidacao_pagamentos_extrato}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu
								id="cboTipoConsolidacaoPagamentosComprovante3"
								value="#{manterContratoServicosBean.cboTipoConsolidacaoPagamentosComprovante}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorTipoConsultaComprovante != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTipoConsolidacaoPagamentosComprovante}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<!-- Pagamento de Benef�cios -->
		<br:brPanelGrid
			rendered="#{manterContratoServicosBean.cdParametroTela == 4}"
			columns="1" cellpadding="0" cellspacing="0" width="100%">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_tipoDeServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsTipoServicoSelecionado}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_situacaoDoServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsSituacaoServico}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_forma_envio_pagamentos}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboFormaEnvioPagamento4"
								value="#{manterContratoServicosBean.cboFormaEnvioPagamento}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFormaEnvioPagamento != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaFormaEnvioPagamento}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_autorizacao_complementar_agencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoAutCompAg4"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoAutComplementarAg}"
								disabled="#{manterContratoServicosBean.desabilitaIndicadorAutorizacaoComplemento}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_gerar_retorno_operacoes_realizadas}:" />
						</br:brPanelGroup>
					
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
					
						<br:brPanelGroup>
							<br:brSelectOneMenu id="comboRetornoOperacoesSelecionado4"
								value="#{manterContratoServicosBean.comboRetornoOperacoesSelecionado}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaRetornoOperacoes}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_gerar_retorno_separado_canal}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoGerarRetornoSeparadoCanal4"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoGerarRetornoSeparadoCanal}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tipo_data_controle_floating}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTipoDataControleFloating4"
								value="#{manterContratoServicosBean.cboTipoDataControleFloating}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorTipoDataFloat != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTipoDataControleFloating}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_informa_agencia_conta_credito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoInformaAgenciaContaCred"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoInformaAgenciaContaCred}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorDispzContaCredito != 'S'}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tipo_identificacao_beneficiario}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTipoIdentificacaoBeneficiario"
								value="#{manterContratoServicosBean.cboTipoIdentificacaoBeneficiario}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorTipoIdBeneficio != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTipoIdentificacaoBeneficiario}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_utiliza_cafastro_orgaos_pagadores}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoUtilizaCadastroOrgaosPagadores"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoUtilizaCadastroOrgaosPagadores}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorCadastroOrg != 'S'}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_utiliza_cadastro_procuradores}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoUtilizaCadastroProcuradores"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoUtilizaCadastroProcuradores}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorCadastroProcd != 'S'}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
								<a4j:support event="onclick"
									reRender="rdoEfetuarManutencaoCadastroProcuradores4,cboPeriocidadeManCadProc,cboFormaManCadProcurador"
									action="#{manterContratoServicosBean.controlaRdoCadastroProcuradores}"
									status="statusAguarde" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_efetua_manutencao_cadastro_procuradores}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio
								id="rdoEfetuarManutencaoCadastroProcuradores4"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoEfetuarManutencaoCadastroProcuradores}"
								disabled="#{manterContratoServicosBean.desabilitaCamposProcuradores}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="4" itemLabel="#{msgs.combo_nao}" />
								<a4j:support event="onclick"
									reRender="cboPeriocidadeManCadProc,cboFormaManCadProcurador"
									action="#{manterContratoServicosBean.controlaRdoCadastroManCadProcuradores}"
									status="statusAguarde" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_periodicidade_manutencao_cadastro_procuradores}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboPeriocidadeManCadProc"
								value="#{manterContratoServicosBean.cboPeriocidadeManCadProc}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorPerdcManutencaoProcd != 'S' || manterContratoServicosBean.desabilitaCamposProcuradores || manterContratoServicosBean.desabilitaCamposManCadProcuradores}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaPeriodicidade}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_possui_expiracao_credito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoPossuiExpiracaoCredito"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoPossuiExpiracaoCredito}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFormaExpiracaoCredito != 'S'}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
								<a4j:support event="onclick"
									reRender="txtQtddDiasExpiracaoCredito, cboContExpCredConta"
									action="#{manterContratoServicosBean.controlaCamposExpiracaoCredito}"
									status="statusAguarde" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_forma_manutencao_cadastro_procuradores}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboFormaManCadProcurador"
								value="#{manterContratoServicosBean.cboFormaManCadProcurador}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFormaManutencao != 'S' || manterContratoServicosBean.desabilitaCamposProcuradores || manterContratoServicosBean.desabilitaCamposManCadProcuradores}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaFormaManutencaoCadastroProcuradores}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_quantidade_dias_expiracao_credito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText size="5" maxlength="3"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								id="txtQtddDiasExpiracaoCredito"
								value="#{manterContratoServicosBean.qtddDiasExpiracaoCredito}"
								onkeypress="onlyNum();"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.qtIndicadorDiaExpiracao != 'S' || manterContratoServicosBean.desabilitaCamposExpiracaoCredito}">
							</br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_forma_controle_expiracao_credito}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboContExpCredConta"
								value="#{manterContratoServicosBean.cboContExpCredConta}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFormaExpiracaoCredito != 'S' || manterContratoServicosBean.desabilitaCamposExpiracaoCredito}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaControleExpiracaoCredito}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_momento_indicacao_credito_efetivado}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboMomentoIndCredEfetivado"
								value="#{manterContratoServicosBean.cboMomentoIndCredEfetivado}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorMomentoCreditoEfetivacao != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaMomentoIndicacaoCreditoEfetivado}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tratamento_credito_dia_nao_util}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTratamentoFeriadoFimVigenciaCredito"
								value="#{manterContratoServicosBean.cboTratamentoFeriadoFimVigenciaCredito}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorCreditoNaoUtilizado != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTratamentoFeriadoFimVigenciaCredito}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

		</br:brPanelGrid>

		<!-- Cadastro de Favorecido -->
		<br:brPanelGrid
			rendered="#{manterContratoServicosBean.cdParametroTela == 5}"
			columns="1" cellpadding="0" cellspacing="0" width="100%">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_tipoDeServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsTipoServicoSelecionado}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_situacaoDoServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsSituacaoServico}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_forma_manutencao_cadastro}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
				
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboFormaManCadFav5"
								value="#{manterContratoServicosBean.cboFormaManCadFavorecido}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFormaManutencao != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaFormaManutencaoCadastroFavorecido}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>


				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_quantidade_dias_para_inativacao_favorecido}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText size="4" maxlength="3"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								id="txtQtddDiasInativFav5"
								value="#{manterContratoServicosBean.qtddDiasInativacaoFavorecido}"
								onkeypress="onlyNum();"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.qtIndicadorDiaInatividadeFavorecido != 'S'}">
							</br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_tipo_consistencia_inscricao_favorecido}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboTipoConInsFav5"
								value="#{manterContratoServicosBean.cboTipoConsistenciaInscricaoFavorecido}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorTipoContaFavorecidoT != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaTipoConsistenciaInscricaoFavorecido}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_gerar_retorno_operacoes_realizadas_internet}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoGerarRetOpReaInt5"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoGerarRetornoOperRealizadaInternet}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorRetornoInternet != 'S'}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<!-- Aviso de Movimenta��o de D�bito  - NAO TEM MAIS -->
		<br:brPanelGrid
			rendered="#{manterContratoServicosBean.cdParametroTela == 6}"
			columns="1" cellpadding="0" cellspacing="0" width="100%">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_tipoDeServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsTipoServicoSelecionado}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_situacaoDoServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsSituacaoServico}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.confServicosManterContrato_periodicidade}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboPeriodicidade"
								value="#{manterContratoServicosBean.cdPeriodicidade}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaPeriodicidade}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.confServicosManterContrato_destino}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboDestino"
								value="#{manterContratoServicosBean.cdDestino}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_correio}" />
								<f:selectItem itemValue="2"
									itemLabel="#{msgs.combo_agenciaOperadora}" />
								<f:selectItem itemValue="3" itemLabel="#{msgs.combo_email}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.confServicosManterContrato_permiteCorrespondenciaAberta}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboPermitCorrespAberta"
								value="#{manterContratoServicosBean.cdPermiteCorrespAberta}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.confServicosManterContrato_demonstraInfAreaReservada}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboDemonstInfAreaRes"
								value="#{manterContratoServicosBean.cdDemonsInfReservada}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.confServicosManterContrato_agruparCorrespondencia}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboAgruparCorrespondencia"
								value="#{manterContratoServicosBean.cdAgruparCorrespondencia}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItem itemValue="1" itemLabel="#{msgs.combo_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.combo_nao}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.confServicosManterContrato_quantidadeDeVias}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText size="3" maxlength="2"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								id="txtQuantVias"
								value="#{manterContratoServicosBean.quantidadeVias}"
								onkeypress="onlyNum();">
							</br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<!-- Aviso de Movimenta��o ao Pagador -->
		<!-- Aviso de Movimenta��o ao Favorecido -->
		<!-- Comprovante Pagamento ao Pagador -->
		<!-- Comprovante Pagamento ao Favorecido -->
		<br:brPanelGrid
			rendered="#{manterContratoServicosBean.cdParametroTela == 8 ||
							    manterContratoServicosBean.cdParametroTela == 9 ||
							    manterContratoServicosBean.cdParametroTela == 10 ||
							    manterContratoServicosBean.cdParametroTela == 11}"
			columns="1" cellpadding="0" cellspacing="0" width="100%">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_tipoDeServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsTipoServicoSelecionado}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conServicosManterContrato_situacaoDoServico}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsSituacaoServico}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<f:verbatim>
				<hr class="lin">
			</f:verbatim>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_periodicidade_emissao}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneMenu id="cboPeriodicidadeEmissao7"
								value="#{manterContratoServicosBean.cboPeriodicidadeEmissao}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorPeriodicidadeAviso != 'S'}">
								<f:selectItem itemValue="0"
									itemLabel="#{msgs.label_combo_selecione}" />
								<f:selectItems
									value="#{manterContratoServicosBean.listaPeriodicidade}" />
							</br:brSelectOneMenu>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
					cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.confServicosManterContrato_destino}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="cboDestinoEnvio7"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.cboDestinoEnvio}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorDestinoAviso != 'S'}">
								<f:selectItem itemValue="2"
									itemLabel="Agencia Gestora do Contrato" />
								<f:selectItem itemValue="3" itemLabel="E-mail" />
								<f:selectItem itemValue="1" itemLabel="Correspondencia" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_demonstra_informacoes_area_reservada}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brSelectOneRadio id="rdoDemonstraInfAreaRes7"
								styleClass="HtmlSelectOneRadioBradesco"
								value="#{manterContratoServicosBean.rdoDemonstraInfAreaRes}"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorAreaReservada != 'S'}">
								<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}" />
								<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}" />
							</br:brSelectOneRadio>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_quantidade_vias_emitir}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputText size="4" maxlength="2"
								onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
								id="qtddViasEmitir7"
								value="#{manterContratoServicosBean.qtddViasEmitir}"
								onkeypress="onlyNum();">
							</br:brInputText>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
				rendered="#{manterContratoServicosBean.cdParametroTela == 8 || manterContratoServicosBean.cdParametroTela == 10 || manterContratoServicosBean.cdParametroTela == 11}"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"
				rendered="#{manterContratoServicosBean.cdParametroTela == 8 || manterContratoServicosBean.cdParametroTela == 10 || manterContratoServicosBean.cdParametroTela == 11}">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup
							rendered="#{manterContratoServicosBean.cdParametroTela == 8}">
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_mensagem_externa}:" />
						</br:brPanelGroup>
						<br:brPanelGroup
							rendered="#{manterContratoServicosBean.cdParametroTela == 10}">
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_mensagem_negocio}:" />
						</br:brPanelGroup>
						<br:brPanelGroup
							rendered="#{manterContratoServicosBean.cdParametroTela == 11}">
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
								value="#{msgs.label_texto_informacao_area_reservada}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
							<br:brInputTextarea rows="3" cols="100"
								value="#{manterContratoServicosBean.textoInformacaoAreaReservada}"
								id="textoInformacaoAreaReservada7"
								onkeydown="javascript:maxLength(this,80);"
								onkeyup="javascript:maxLength(this,80);"
								disabled="#{manterContratoServicosBean.saidaVerificarAtributo.dsIndicadorAreaResrd != 'S'}">
							</br:brInputTextarea>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>


		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="2" width="100%" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup style="text-align:left;width:150px">
				<br:brCommandButton id="btnVoltar" styleClass="bto1"
					style="align:left"
					value="#{msgs.conServicosManterContrato_btnVoltar}"
					action="#{manterContratoServicosBean.voltar}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px">
				<br:brCommandButton id="btnLimpar" styleClass="bto1"
					style="margin-right:5px"
					value="#{msgs.conServicosManterContrato_btnLimpar}"
					action="#{manterContratoServicosBean.limparConfiguracao}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnSalvar" styleClass="bto1"
					value="#{msgs.btn_avancar}"
					action="#{manterContratoServicosBean.salvarConfiguracao}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>




	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>