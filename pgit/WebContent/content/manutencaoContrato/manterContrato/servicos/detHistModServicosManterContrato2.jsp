<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<brArq:form id="detHistModServicosManterContrato2" name="detHistModServicosManterContrato2">
<h:inputHidden id="cdParametro" value="#{manterContratoServicosBean.cdParametroTela}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>			
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_empresaGestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_tipoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_tipoDeServico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoServico}"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolManutContratos_modalidade}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsModalidade}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_situacaoDaModalidade}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsSituacaoServicoRelacionado}"/>						
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<!-- Rastreabilidade de T�tulos -- CdParametro:26-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 26}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rastreamento_titulos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRastreamentoTitulo}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_rastrear_sacado_agregado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_sim}" rendered = "#{manterContratoServicosBean.rdoRastrearTituloTerceiros == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_nao}" rendered = "#{manterContratoServicosBean.rdoRastrearTituloTerceiros == 2}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_rastrear_notas_fiscais}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_sim}" rendered = "#{manterContratoServicosBean.rdoRastrearNotasFiscais == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_nao}" rendered = "#{manterContratoServicosBean.rdoRastrearNotasFiscais == 2}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.confModalidades_rastreamentoTitulos_cliente_pagador}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_sim}" rendered = "#{manterContratoServicosBean.rdoAgendarTituloRastProp == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_nao}" rendered = "#{manterContratoServicosBean.rdoAgendarTituloRastProp == 2}"/>
			</br:brPanelGroup>					
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid cellpadding="0" cellspacing="0">
			<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_Exige_Identificacao_Filial_Autorizacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_sim}" rendered = "#{manterContratoServicosBean.rdoExigeIdentificacaoFilialAutorizacao == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_nao}" rendered = "#{manterContratoServicosBean.rdoExigeIdentificacaoFilialAutorizacao == 2}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_bloquear_emissao_papeleta}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_sim}" rendered = "#{manterContratoServicosBean.rdoBloqEmissaoPapeleta == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_nao}" rendered = "#{manterContratoServicosBean.rdoBloqEmissaoPapeleta == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_inicio_bloqueio_papeleta}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dtInicioBloqPapeleta}" converter="dateBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_data_inicio_rastreamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dtInicioRastreamento}" converter="dateBrazillianConverter" />
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_rastrear_somente_boleto_bradesco}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.rdoAdesaoSacadoEletronico}"/>
				<%-- 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_sim}" rendered = "#{manterContratoServicosBean.rdoAdesaoSacadoEletronico == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_nao}" rendered = "#{manterContratoServicosBean.rdoAdesaoSacadoEletronico == 2}"/>
				--%>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<!-- Pagamento Prova de Vida -- CdParametro:27 -->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 27}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_acao_nao_comprovacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsAcaoParaNaoComprovacao}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_meses_periodicidade_comprovacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddMesesPeriodicidadeComprovacao}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_emite_aviso_comprovacao_vida}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoEmiteAvitoCompVida == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoEmiteAvitoCompVida == 2}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_aviso_antes_inicio}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasEmissaoAvisoAntesInicioComprovacao}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_aviso_antes_vencimento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasEmissaoAvisoAntesVencimento}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_utiliza_mensagem_personalizada_online}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoUtilizaMensagemPersonalizada == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoUtilizaMensagemPersonalizada == 2}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_destino}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsDestinoEnvio}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cadastro_endereco_utilizado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsCadConsultaEndEnvio}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<!-- Pagamento DARF -- CdParametro:28 -->
	<!-- Pagamento GARE -- CdParametro:29 -->
	<!-- Pagamento GPS -- CdParametro:30 -->
	<!-- Pagamento C�gio de Barras -- CdParametro:31 -->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 28 ||
								manterContratoServicosBean.cdParametroTela == 29 ||
								manterContratoServicosBean.cdParametroTela == 30 ||
								manterContratoServicosBean.cdParametroTela == 31}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoConsSaldo}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_processamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsProcessamentoEfetivacaoPagamento}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
				<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTratFeriadosDataPagamento}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_local}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorFeriadoLocal}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup style="margin-top:5px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejAgendamento}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejEfetivacao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.percMaxRegInconsistente}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_prioridade_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsPrioridDebito}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 2}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 2}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteIndividual}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteDiario}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasRepique}"/>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterContratoServicosBean.flagExibeCampos == 'S'">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasFloating}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >

			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_demonstra_duas_linha_extrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorSegundaLinhaExtrato}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<!-- Pagamento D�bito Ve�culo -- CdParametro:32 -->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 32}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoConsSaldo}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_processamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsProcessamentoEfetivacaoPagamento}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
				<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTratFeriadosDataPagamento}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_local}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorFeriadoLocal}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup style="margin-top:5px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejAgendamento}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejEfetivacao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.percMaxRegInconsistente}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_prioridade_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsPrioridDebito}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 2}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteIndividual}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteDiario}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasRepique}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasFloating}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tratamento_valor_divergente}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTratamentoValorDivergente}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_consistencia_cpf_cnpj_proprietario}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoConsistenciaCpfCnpjProprietario}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_data_hora_inclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dataHoraInclusao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.usuarioInclusao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.tipoCanalInclusao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.complementoInclusao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_data_hora_manutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dataHoraManutencao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.usuarioManutencao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.tipoCanalManutencao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.complementoManutencao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:750px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.conServicosManterContrato_btnVoltar}" action="#{manterContratoServicosBean.voltar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>