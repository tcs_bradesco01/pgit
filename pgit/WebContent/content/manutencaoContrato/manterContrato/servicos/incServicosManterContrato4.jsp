<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incServicosManterContrato4Form" name="incServicosManterContrato4Form">

<h:inputHidden id="hiddenCountCheckedModalidade" value="#{manterContratoServicosBean.countCheckedModalidade}"/>
<h:inputHidden id="hiddenFoco" value="#{manterContratoServicosBean.foco}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>			

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.gerenteResponsavelFormatado}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpj}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocial}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_empresaGestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_tipoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	<%-- 
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="50%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_modalidade}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:70"> 
			<app:scrollableDataTable id="dataTableServico" value="#{manterContratoBean.dsServicoSelecionado}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" >
				
				<app:scrollableColumn styleClass="colTabLeft" width="350px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conServicosManterContrato_dataTable_Servico}" style="width:250px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsServico}"/>
				</app:scrollableColumn>
			  <app:scrollableColumn styleClass="colTabLeft" width="350px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conServicosManterContrato_dataTable_modalidade}" style="width:250px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{manterContratoServicosBean.descricaoSegundaModalidade}"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  rendered="#{!empty manterContratoServicosBean.listaGridModalidade}">
	<f:verbatim><hr class="lin"> </f:verbatim>
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_convenio_cc_salario}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" rendered="#{!empty manterContratoServicosBean.listaGridModalidade}">
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" rendered="#{!empty manterContratoServicosBean.listaGridModalidade}">	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">
				
				<app:scrollableDataTable id="dataTable" value="#{manterContratoServicosBean.saidaListarDadosCtaConvn.ocorrencias}"
					var="result" rows="10" rowIndexVar="parametroKey"
					rowClasses="tabela_celula_normal, tabela_celula_destaque"
					width="80%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px">
						<f:facet name="header">
							<br:brOutputText value="" styleClass="tableFontStyle"
								style="width:25; text-align:center" />
						</f:facet>
						<t:selectOneRadio id="sorLista"
							styleClass="HtmlSelectOneRadioBradesco" layout="spread"
							forceId="true" forceIdIndex="false"
							value="#{manterContratoServicosBean.itemSelecionadListaBanco}">
							<f:selectItems value="#{manterContratoServicosBean.listaControleRadioDadosConta}" />
						</t:selectOneRadio>
						<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="120px">
						<f:facet name="header">
							<br:brOutputText
								value="#{msgs.conDadosBasicos_banco_agencia_conta}"
								style="width:120; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.bancoAgConta}"
							styleClass="tableFontStyle" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="170px">
						<f:facet name="header">
							<h:outputText
								value="#{msgs.conDadosBasicos_numero_convenio}"
								style="width:170; text-align:center" />
						</f:facet>
						<br:brOutputText value="NOVO" rendered="#{result.cdConvnNovo == 1}"/>
						<br:brOutputText value="#{result.cdConveCtaSalarial}" rendered="#{result.cdConvnNovo == 2}"/>
					</app:scrollableColumn>

	
			</app:scrollableDataTable>			
			
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterContratoServicosBean.pesquisarServico}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>  --%> 
	
	

	<br:brPanelGrid columns="1" width="100%" cellpadding="0"
		cellspacing="0">
		<br:brOutputText styleClass="HtmlOutputFormatTitleBradesco"
			value="#{msgs.conDadosBasicos_dados_da_folha_de_pagamento}:" />

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0"
			rendered="#{!empty manterContratoServicosBean.listaGridModalidade}">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>

	<br:brPanelGrid columns="4" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{msgs.conDadosBasicos_valor_da_folha_pagamento}:"
					style="margin-right:5px" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterContratoServicosBean.txtValorFolhaPgamento}"
					id="txtValorFolhaPgamento" style="margin-right:5px"
					converter="decimalBrazillianConverter" alt="decimalBr" 
					maxlength="17" size="27" onfocus="loadMasks();" >
						<a4j:support  oncomplete="javascript:focoBlur(document.forms[1]);"  status="statusAguarde" event="onblur" reRender="txtMediaSalarial" action="#{manterContratoServicosBean.calcularMediaSalarial}" />				
				</br:brInputText>	
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{msgs.conDadosBasicos_quantidade_de_funcionarios}:" style="margin-right:5px" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterContratoServicosBean.txtQtdFuncionarios}"
					id="txtQtdFuncionarios"  style="margin-right:5px" alt="decimalBr9Ponto" >
						<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);" status="statusAguarde" event="onblur" reRender="txtMediaSalarial" action="#{manterContratoServicosBean.calcularMediaSalarial}" />
				</br:brInputText>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{msgs.conDadosBasicos_media_salarial}:" style="margin-right:5px" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterContratoServicosBean.txtMediaSalarial}"
					id="txtMediaSalarial" style="margin-right:5px" disabled="true"
					converter="decimalBrazillianConverter" alt="decimalBr"
					maxlength="17" size="27" onfocus="loadMasks();"					
					/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{msgs.conDadosBasicos_mes_ano}:" style="margin-right:5px" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterContratoServicosBean.txtMesAnoCorrespondente}"
					id="txtMesAnoCorrespondente" style="margin-right:5px" alt="mesAno" size="12" maxlength="6" 
					onblur="validarMesAno(this)" />
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="4" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDadosBasicos_pagamento}:"/> 
				</br:brPanelGroup>
			</br:brPanelGrid>		
			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >	
				<h:selectBooleanCheckbox id="chkLoteMensal" value="#{manterContratoServicosBean.chkLoteMensal}">
					<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick" />
				</h:selectBooleanCheckbox>		
					
				<br:brPanelGroup>
					<br:brOutputText style="margin-left:3px" styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDadosBasicos_Mensal}:"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup style="width:100px; margin-bottom:5px" >
				</br:brPanelGroup>
			</br:brPanelGrid>	
			<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >	
				<h:selectBooleanCheckbox id="chkLoteQuinzenal" value="#{manterContratoServicosBean.chkLoteQuinzenal}">
					<a4j:support oncomplete="javascript:foco(this);" status="statusAguarde" event="onclick"/>
				</h:selectBooleanCheckbox>		
					
				<br:brPanelGroup>
					<br:brOutputText style="margin-left:3px" styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDadosBasicos_Quinzenal}:"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup style="width:100px; margin-bottom:5px" >
				</br:brPanelGroup>
			</br:brPanelGrid>			
		</br:brPanelGrid>		
			
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_fideliza}: " style="margin-right:7px" /> 
			</br:brPanelGroup>

			<br:brPanelGroup id="painelFidelize">
				<t:selectOneRadio value="#{manterContratoServicosBean.radioFidelize}"
					id="radioFidelize" styleClass="HtmlSelectOneRadioBradesco"
					layout="spread" forceId="true" forceIdIndex="false">
					<f:selectItem itemValue="0" itemLabel="" />
					<f:selectItem itemValue="1" itemLabel="" />
					<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);"
						status="statusAguarde" event="onclick" reRender="btnAvancar, dsEmailEmpresa, dsEmailAgencia"/>							
				</t:selectOneRadio>
			</br:brPanelGroup>

			<br:brPanelGrid id="radiosFidelize" columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<t:radio for="radioFidelize" index="0"/>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_sim}"/> 
			    </br:brPanelGroup>
			    
			    <br:brPanelGroup>
					<t:radio for="radioFidelize" index="1"/>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nao}"/> 
			    </br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.email_empresa}: " style="margin-right:7px" />
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brInputTextarea rows="3" cols="30" id="dsEmailEmpresa"
						onblur="validaTamanho(this, 'Email Empresa', 1, 200)"
						onkeydown="validarTamanhoEDigitado(this)"
						value="#{manterContratoServicosBean.dsEmailEmpresa}">
					</br:brInputTextarea>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.email_agencia}: " style="margin-right:7px" />
				</br:brPanelGroup>

				<br:brPanelGroup>
					<br:brInputTextarea rows="3" cols="30" id="dsEmailAgencia"
						onblur="validaTamanho(this, 'Email Agencia', 1, 200)"
						onkeydown="validarTamanhoEDigitado(this)"
						value="#{manterContratoServicosBean.dsEmailAgencia}">
					</br:brInputTextarea>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conDadosBasicos_abertura_de_conta_salario}:"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0">
		<br:brPanelGroup id="painelManterPagtoAmbA"> 
			<t:selectOneRadio value="#{manterContratoServicosBean.aberturaContaSalario}" id="radioAberturaContaSalarioS"
				styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">
				<f:selectItem itemValue="0" itemLabel=""/>  
				<f:selectItem itemValue="1" itemLabel=""/> 
				<a4j:support event="onclick" reRender="btnAvancar"/>				 
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid id="radiosradioAberturaContaSalarioAgenciaExpress" columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<t:radio for="radioAberturaContaSalarioS" index="0"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_agencia}"/> 
	    </br:brPanelGroup>
	    <br:brPanelGroup>
			<t:radio for="radioAberturaContaSalarioS" index="1"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_bradesco_expresso}"/> 
	    </br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	

	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.alto_turnover}: " style="margin-right:7px" />  
			</br:brPanelGroup>
			</br:brPanelGrid>		
		</br:brPanelGrid>	
							
				<br:brPanelGroup id="painelTurnover">
					<t:selectOneRadio value="#{manterContratoServicosBean.altoTurnover}"
						id="turnover" styleClass="HtmlSelectOneRadioBradesco"
						layout="spread" forceId="true" forceIdIndex="false">
						<f:selectItem itemValue="1" itemLabel="" />
						<f:selectItem itemValue="2" itemLabel="" />
						<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);"
							status="statusAguarde" event="onclick" reRender="btnAvancar, painelOfertaCartao, painelPeloApp"/>							
					</t:selectOneRadio>
				</br:brPanelGroup>
				
			<br:brPanelGrid id="radiosFidelizeeq" columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<t:radio for="turnover" index="0"/>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_sim}"/> 
			    </br:brPanelGroup>
			    
			    <br:brPanelGroup>
					<t:radio for="turnover" index="1"/>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nao}"/> 
			    </br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.oferta_cartao_pre}: " style="margin-right:7px" /> 
			</br:brPanelGroup>
			
			<br:brPanelGroup id="painelOfertaCartao">
					<t:selectOneRadio value="#{manterContratoServicosBean.ofertaCartaoPre}"
						id="cartaoPre" styleClass="HtmlSelectOneRadioBradesco"
						layout="spread" forceId="true" forceIdIndex="false">
						<f:selectItem itemValue="1" itemLabel="" />
						<f:selectItem itemValue="2" itemLabel="" />
						<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);"
							status="statusAguarde" event="onclick" reRender="btnAvancar, dsEmailEmpresa, dsEmailAgencia"/>							
					</t:selectOneRadio>
				</br:brPanelGroup>

			<br:brPanelGrid id="cartaoPreA" columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<t:radio for="cartaoPre" index="0"/>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_sim}"/> 
			    </br:brPanelGroup>
			    
			    <br:brPanelGroup>
					<t:radio for="cartaoPre" index="1"/>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nao}"/> 
			    </br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
				
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.ativacao_app}: " style="margin-right:7px" /> 
			</br:brPanelGroup>
			
			<br:brPanelGroup id="painelPeloApp" >
					<t:selectOneRadio value="#{manterContratoServicosBean.peloApp}"
						id="peloApp" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false">
						<f:selectItem itemValue="1" itemLabel=""  />
						<f:selectItem itemValue="2" itemLabel="" />
						<a4j:support oncomplete="javascript:focoBlur(document.forms[1]);"
							status="statusAguarde" event="onclick" reRender="btnAvancar, dsEmailEmpresa, dsEmailAgencia"/>							
					</t:selectOneRadio>
				</br:brPanelGroup>

			<br:brPanelGrid id="dot" columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<t:radio for="peloApp" index="0"/>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_sim}"/> 
			    </br:brPanelGroup>
			    
			    <br:brPanelGroup>
					<t:radio for="peloApp" index="1"/>
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nao}"/> 
			    </br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid></br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:690px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.conServicosManterContrato_btnVoltar}" action="#{manterContratoServicosBean.voltarcontrato2}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>

			<br:brPanelGroup style="text-align:right;" >
					<br:brCommandButton id="btnIncluir"
						disabled="#{!manterContratoServicosBean.btnIncluir}"
						rendered="#{!empty manterContratoServicosBean.listaGridModalidade}"
						styleClass="bto1"
						onclick="javascript: validacamposincServicosManterContrato4(document.forms[1], '#{msgs.incServicosManterContrato_msgMaxModalidades}')"
						value="#{msgs.conServicosManterContrato_btnIncluir}"
						action="#{manterContratoServicosBean.incluirVlidarVincConvSalario}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>