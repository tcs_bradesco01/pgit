<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incServicosManterContrato5Form" name="incServicosManterContrato5Form">
<a4j:outputPanel id="messagesRetorno">
	<t:messages showDetail="true"></t:messages>
</a4j:outputPanel>
<h:inputHidden id="hiddenCountCheckedModalidade" value="#{manterContratoServicosBean.countCheckedModalidade}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>			

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.gerenteResponsavelFormatado}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpj}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocial}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_empresaGestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_tipoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="50%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_modalidade}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:70"> 
			<app:scrollableDataTable id="dataTableServico" value="#{manterContratoBean.dsServicoSelecionado}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" >
				
				<app:scrollableColumn styleClass="colTabLeft" width="350px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conServicosManterContrato_dataTable_tipoServico}" style="width:250px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsServico}"/>
				</app:scrollableColumn>
			  <app:scrollableColumn styleClass="colTabLeft" width="350px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conServicosManterContrato_dataTable_modalidade}" style="width:250px; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{manterContratoServicosBean.descricaoSegundaModalidade}"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<%-- 
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  rendered="#{!empty manterContratoServicosBean.listaGridModalidade}">
	<f:verbatim><hr class="lin"> </f:verbatim>
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_convenio_cc_salario}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" rendered="#{!empty manterContratoServicosBean.listaGridModalidade}">
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" rendered="#{!empty manterContratoServicosBean.listaGridModalidade}">	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">
				
				<app:scrollableDataTable id="dataTable" value="#{manterContratoServicosBean.listaSaidaListarDadosCtaConvn}"
					var="result" rows="10" rowIndexVar="parametroKey"
					rowClasses="tabela_celula_normal, tabela_celula_destaque"
					width="90%">
					<app:scrollableColumn styleClass="colTabRight" width="120px">
						<f:facet name="header">
							<br:brOutputText
								value="#{msgs.conDadosBasicos_banco_agencia_conta}"
								style="width:120; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.bancoAgenciaConta}"
							styleClass="tableFontStyle" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="170px">
						<f:facet name="header">
							<h:outputText
								value="#{msgs.conDadosBasicos_numero_convenio}"
								style="width:170; text-align:center" />
						</f:facet>
						<br:brOutputText value="NOVO" rendered="#{result.cdConvnNovo == 1}"/>
						<br:brOutputText value="#{result.cdConveCtaSalarial}" rendered="#{result.cdConvnNovo == 2}"/>
					</app:scrollableColumn>

	
			</app:scrollableDataTable>			
			
		</br:brPanelGroup>
	</br:brPanelGrid>  --%> 
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%" cellpadding="0"
		cellspacing="0">
		<br:brOutputText styleClass="HtmlOutputFormatTitleBradesco"
			value="#{msgs.conDadosBasicos_dados_da_folha_de_pagamento}:" />

	</br:brPanelGrid>

	<br:brPanelGrid columns="4" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_valor_da_folha_pagamento}:"
					style="margin-right:5px" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.txtValorFolhaPgamentoConf}"
					id="txtValorFolhaPgamento" style="margin-right:5px" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_quantidade_de_funcionarios}:" style="margin-right:5px" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.txtQtdFuncionariosConf}"
					id="txtQtdeFuncionarios" style="margin-right:5px"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_media_salarial}:" style="margin-right:5px" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" converter="decimalBrazillianConverter" value="#{manterContratoServicosBean.txtMediaSalarialConf}"
					id="txtMediaSalarial" style="margin-right:5px"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_mes_ano}:" style="margin-right:5px" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.txtMesAnoCorrespondenteConf}"
					id="txtMesAnoCorrespondente" style="margin-right:5px"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

		<br:brPanelGrid columns="3" width="100%" cellpadding="0"
			cellspacing="0">

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.conDadosBasicos_pagamento}:"
						style="margin-right:60px" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.tipoLotePagamentoConf}"
						id="chkPagamentoMensalQuinzenal"/>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.label_fideliza}:" style="margin-right:60px" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoServicosBean.fidelizeConf}" id="fideliza"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
							value="#{msgs.email_empresa}:" style="margin-right:5px"/>
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsEmailEmpresa}"
						id="dsEmailEmpresa"  />
					</br:brPanelGroup>
				</br:brPanelGrid>
	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
							value="#{msgs.email_agencia}:" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoServicosBean.dsEmailAgencia}"
						id="dsEmailAgencia"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_abertura_de_conta_salario}:"
					style="margin-right:5px" />
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
				value="#{manterContratoServicosBean.aberturaContaSalarioConf}"
				id="txtAberturaContaSalario" style="margin-right:5px" />
			</br:brPanelGroup>
		</br:brPanelGrid>

	<br:brPanelGrid columns="4" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.alto_turnover}:" style="margin-right:60px" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoServicosBean.altoTurnoverConf}" id="altoTurnover"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.oferta_cartao_pre}:" style="margin-right:60px" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoServicosBean.ofertaCartaoPreConf}" id="cartaoPre"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1">
			<br:brPanelGroup>
				<<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
						value="#{msgs.ativacao_app}:" style="margin-right:60px" />
				</br:brPanelGroup>
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{manterContratoServicosBean.peloAppConf}" id="peloApp"/>
			</br:brPanelGroup>
		</br:brPanelGrid></br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; " ajaxRendered="true">		
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:690px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.conServicosManterContrato_btnVoltar}" action="#{manterContratoServicosBean.voltarcontrato5}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brCommandButton id="btnConfirmar" styleClass="bto1" type="button"
					value="#{msgs.label_confirmar}"
					style="margin-right:5px"
					onclick="javascript: document.getElementById('incServicosManterContrato5Form:txtAgenciaGestora').click();">
				</br:brCommandButton>
			</br:brPanelGroup>
			
			<br:brInputText id="txtAgenciaGestora" style="display:none"
				styleClass="HtmlInputTextBradesco"
				value=""
				size="6" maxlength="5">
				<a4j:support event="onclick"
					action="#{manterContratoServicosBean.confirmarIncConvSalario}"
					reRender="formModalMessages"
					onsubmit="loadModalJQuery()"
					oncomplete="renegociarNovo('#{manterContratoServicosBean.urlRenegociacaoNovo}');"/>
			</br:brInputText>
		</br:brPanelGrid>
	</a4j:outputPanel>
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>