<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<brArq:form id="detModServicosManterContrato" name="detModServicosManterContrato">
<h:inputHidden id="cdParametro" value="#{manterContratoServicosBean.cdParametroTela}"/>
<br:brPanelGrid columns="1" width="900" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
 	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.gerenteResponsavelFormatado}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpj}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocial}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_empresaGestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_tipoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_tipoDeServico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoServicoSelecionado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolManutContratos_modalidade}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsModalidade}"/>
		</br:brPanelGroup>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_situacaoDaModalidade}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsSituacaoModalidade}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<!-- Aviso de Recadastramento -- CdParametro:15-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 15}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_momento_envio}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsMomentoEnvio}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_destino}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsDestinoEnvio}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cadastro_endereco_utilizado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsCadConsultaEndEnvio}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_aviso_antecipado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasAvisoAntecipado}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permitir_agrupamento_correspondencia}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_sim}" rendered = "#{manterContratoServicosBean.rdoPermitirAgrupamentoCorrespondencia == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_nao}" rendered = "#{manterContratoServicosBean.rdoPermitirAgrupamentoCorrespondencia == 2}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<!-- Formulario de Recadastramento -- CdParametro:16-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 16}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_momento_envio}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsMomentoEnvio}"/>
			</br:brPanelGroup>				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_destino}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsDestinoEnvio}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cadastro_endereco_utilizado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsCadConsultaEndEnvio}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_aviso_antecipado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasEnvioAntecipadoFormulario}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permitir_agrupamento_correspondencia}:"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_sim}" rendered = "#{manterContratoServicosBean.rdoPermitirAgrupamentoCorrespondencia == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.combo_nao}" rendered = "#{manterContratoServicosBean.rdoPermitirAgrupamentoCorrespondencia == 2}"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<!-- Pagamentos de Fornecedores - DOC -- CdParametro:17-->
	<!-- Pagamentos de Fornecedores - TED -- CdParametro:18-->
	<!-- Pagamento Via Dep�sito Identificado -- CdParametro:24-->	
	<!-- Ordem de Credito -- CdParametro:25-->	
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 17 ||
								manterContratoServicosBean.cdParametroTela == 18 ||
								manterContratoServicosBean.cdParametroTela == 24 ||
								manterContratoServicosBean.cdParametroTela == 25}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoConsSaldo}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_processamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsProcessamentoEfetivacaoPagamento}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
				<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTratFeriadosDataPagamento}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_local}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorFeriadoLocal}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterContratoServicosBean.cdParametroTela == 18}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permite_agendar_autorizar_com_grade_horaria_encerrada}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorAgendaGrade}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejAgendamento}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejEfetivacao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.percMaxRegInconsistente}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_prioridade_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsPrioridDebito}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 2}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteIndividual}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteDiario}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasRepique}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasFloating}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_demonstra_duas_linha_extrato}:" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoIndicadorSegundaLinhaExtrato == 1}"/>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoIndicadorSegundaLinhaExtrato == 2}"/>
			</br:brPanelGroup>
				
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid></br:brPanelGrid>
			
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alterarModContrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicador}"/>
			</br:brPanelGroup>
	
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<!-- Pagamentos Via Ordem de Pagamento -- CdParametro:19-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 19}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoConsSaldo}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_processamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsProcessamentoEfetivacaoPagamento}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
				<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTratFeriadosDataPagamento}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_local}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorFeriadoLocal}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup style="margin-top:5px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejAgendamento}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejEfetivacao}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.percMaxRegInconsistente}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_prioridade_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsPrioridDebito}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 2}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteIndividual}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteDiario}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasRepique}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasFloating}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_ocorrencia_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsOcorrenciaDebito}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_inscricao_favorecido}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoInscricaoFavorecido}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_local_emissao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsLocalEmissao}"/>
			</br:brPanelGroup>				
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_demonstra_duas_linha_extrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorSegundaLinhaExtrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_para_expiracao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.quantidadeDiasExpiracao}"/>
			</br:brPanelGroup>						
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<!-- Pagamento Via Cr�dito em Conta -- CdParametro:20 -->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 20}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoConsSaldo}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_processamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsProcessamentoEfetivacaoPagamento}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
				<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTratFeriadosDataPagamento}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_local}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorFeriadoLocal}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup style="margin-top:5px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejAgendamento}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejEfetivacao}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.percMaxRegInconsistente}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_prioridade_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsPrioridDebito}"/>
			</br:brPanelGroup>	
					
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteIndividual}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteDiario}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasRepique}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasFloating}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 2}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerar_lancamento_futuro_credito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoGerarLancFutCred == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoGerarLancFutCred == 2}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampo}">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerar_lancamento_programado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoGerarLancProgramado == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoGerarLancProgramado == 2}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
				
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_tratamento_conta_debito_transferida}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoTratamentoContaTransferida}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_consistencia_cpf_cnpj_favorecido}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoConsistenciaCpfCnpjFavorecido}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampo}">
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_validar_nome_favorecido_receita_federal}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoValidarNomeFavorecidoReceitaFederal == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoValidarNomeFavorecidoReceitaFederal == 2}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
			</br:brPanelGroup>				
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampo}">
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampo2}">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_meio_pagamento_enquadramento_conta_salario}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoContaCredito}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampo2}">
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
<%-- 			<br:brPanelGroup> --%>
<%-- 				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" /> --%>
<%-- 				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_demonstra_duas_linha_extrato}:" /> --%>
<%-- 				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorSegundaLinhaExtrato}"/> --%>
<%-- 			</br:brPanelGroup> --%>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_Origem_demonstra_duas_linha_extrato_portabilidade_bradesco}:"
					rendered="#{!manterContratoServicosBean.exibeOrigemSegundaViaExtrato}"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_Origem_demonstra_duas_linha_extrato}:"
					rendered="#{manterContratoServicosBean.exibeOrigemSegundaViaExtrato}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsRdoIndicadorSegundaLinhaExtrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_formatacao_primeira_linha_extrato_credito}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsPreenchimentoLancamentoPersonalizado}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	</br:brPanelGrid>
	
	<!-- Pagamento Via D�bito em Conta -- CdParametro:21 -->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 21}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoConsSaldo}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_processamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsProcessamentoEfetivacaoPagamento}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
				<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTratFeriadosDataPagamento}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_local}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorFeriadoLocal}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup style="margin-top:5px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejAgendamento}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejEfetivacao}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.percMaxRegInconsistente}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_prioridade_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsPrioridDebito}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permite_debito_online}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoPermiteDebitoOnline == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoPermiteDebitoOnline == 2}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteIndividual}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteDiario}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasRepique}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasFloating}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 2}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerar_lancamento_futuro_credito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoGerarLancFutCred == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoGerarLancFutCred == 2}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_demonstra_duas_linha_extrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoIndicadorSegundaLinhaExtrato == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoIndicadorSegundaLinhaExtrato == 2}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<!-- Pagamento Via T�tulo Bradesco -- CdParametro:22-->
	<!-- Pagamento Via T�tulo Outros Bancos -- CdParametro:23-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 22 ||
								manterContratoServicosBean.cdParametroTela == 23}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoConsSaldo}"/>
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterContratoServicosBean.cdParametroTela == 23}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.tipo_consulta_saldo_titulo_valor_superior}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsConsultaSaldoValorSuperior}" />
			</br:brPanelGroup>
			<br:brPanelGroup rendered="#{manterContratoServicosBean.cdParametroTela == 22}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_processamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsProcessamentoEfetivacaoPagamento}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.cdParametroTela == 23}">
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.cdParametroTela == 23}">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_processamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsProcessamentoEfetivacaoPagamento}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
				<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTratFeriadosDataPagamento}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_pgto_feriado_local}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorFeriadoLocal}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup style="margin-top:5px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejAgendamento}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoRejEfetivacao}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.percMaxRegInconsistente}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_prioridade_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsPrioridDebito}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoGerarLancFutDeb == 2}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteIndividual}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.valorLimiteDiario}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasRepique}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.qtddDiasFloating}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permite_pagar_menor}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoPermitePagarMenor == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoPermitePagarMenor == 2}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_permite_pagar_vencido}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_sim}" rendered="#{manterContratoServicosBean.rdoPermitePagarVencido == 1}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nao}" rendered="#{manterContratoServicosBean.rdoPermitePagarVencido == 2}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_quantidade_limite_dias_para_pagamento_vencido}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.quantidadeLimiteDiasPagamentoVencido}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_demonstra_duas_linha_extrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsIndicadorSegundaLinhaExtrato}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_indicador_de_titulo_dda_no_retorno}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTituloDdaRetorno}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_percentual_diferenca_tolerada}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
					value="#{manterContratoServicosBean.vlPercentualDiferencaTolerada}"
					converter="decimalBrazillianConverter" />
				<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco"
					value=" #{msgs.label_porcentagem}"
					rendered="#{not empty manterContratoServicosBean.vlPercentualDiferencaTolerada}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_consistencia_cpf_cnpj_benef_sacador_aval_npc}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsConsistenciaCpfCnpjBenefAvalNpc}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_data_hora_inclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dataHoraInclusao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.usuarioInclusao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.tipoCanalInclusao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.complementoInclusao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_data_hora_manutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dataHoraManutencao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.usuarioManutencao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.tipoCanalManutencao}"  />
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.complementoManutencao}"  />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	

	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:750px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.conServicosManterContrato_btnVoltar}" action="#{manterContratoServicosBean.voltarModalidadeConfigurar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
	
	
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>