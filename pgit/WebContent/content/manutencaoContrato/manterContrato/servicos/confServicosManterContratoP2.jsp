<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="confServicosManterContratoP2" name="confServicosManterContratoP2">
<h:inputHidden id="cdParametro" value="#{manterContratoServicosBean.cdParametroTela}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>			
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_empresaGestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_tipoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<!-- Emiss�o Comprovantes Diversos-->
	<!-- Emiss�o Comprovante Salarial-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 12 ||
							    manterContratoServicosBean.cdParametroTela == 13}" columns="1" cellpadding="0" cellspacing="0" width="100%" >
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_tipoDeServico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoServicoSelecionado}"/>						
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_situacaoDoServico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsSituacaoServico}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rejeicao_lote}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicao12" value="#{manterContratoServicosBean.cboTipoRejeicaoLote}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRejeicaoLote}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
				
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_meio_disponibilizacao_correntista_abrev}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboMeioDisponiCorren12" value="#{manterContratoServicosBean.cboMeioDisponibilizacaoCorrentista}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaMeioDisponibilizacaoCorrentistas}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_midia_disponibilizacao_correntista_abrev}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						<br:brSelectOneMenu id="cboMidiaDiposniCorre12" value="#{manterContratoServicosBean.cboMidiaDispCorrentista}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaMidiaDisponibilizacaoCorrentistas}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_meio_disponibilizacao_nao_correntista_abrev}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboMeioDisponiNaoCorren12" value="#{manterContratoServicosBean.cboMeioDisponibilizacaoNaoCorrentista}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaMeioDisponibilizacaoNaoCorrentistas}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_destino_envio_correspondencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						<br:brSelectOneMenu id="cboDestinoEnvioCorres12" value="#{manterContratoServicosBean.cboDestinoEnvioCorresp}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaDestinoEnvioCorrespondencia}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_cadastro_consulta_endereco_envio}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboCadConEndEnvio12" value="#{manterContratoServicosBean.cboCadConsultaEndEnvio}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaCadastroConsultaEnderecoEnvio}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_indicador_frases_cadastradas}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brSelectOneRadio id="rdoUtilizaFrasesPreCad12" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoUtilizaFrasesPreCadastradas}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_cobrar_tarifas_funcionarios}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brSelectOneRadio id="rdoCobrarTafFuncionarios12" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.cobrarTarifasFunc}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_meses_emissao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
			
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
						<br:brPanelGroup>	
				   			<br:brInputText size="4" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" maxlength="2" id="txtQtddMesesEmissao" value="#{manterContratoServicosBean.quantidadeMesesEmissao}" onkeypress="onlyNum();">  
				   			</br:brInputText>	
						</br:brPanelGroup>	
					</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_linhas_comprovante}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
			
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
						<br:brPanelGroup>	
				   			<br:brInputText size="4" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" maxlength="3" id="txtQtddLinhasComprovante12" value="#{manterContratoServicosBean.qtddLinhasPorComprovante}" onkeypress="onlyNum();">  
				   			</br:brInputText>	
						</br:brPanelGroup>	
					</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_vias_emitir}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
			
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
						<br:brPanelGroup>	
				   			<br:brInputText size="4" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" maxlength="2" id="txtQtddViaEmitir12" value="#{manterContratoServicosBean.qtddViasEmitir}" onkeypress="onlyNum();">  
				   			</br:brInputText>	
						</br:brPanelGroup>	
					</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_vias_cobradas_registro}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
			
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
						<br:brPanelGroup>	
				   			<br:brInputText size="4" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" maxlength="2" id="txtQtddViasPagasRegistro12" value="#{manterContratoServicosBean.qtddViasPagasRegistro}" onkeypress="onlyNum();">  
				   			</br:brInputText>	
						</br:brPanelGroup>	
					</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_operacoes_realizadas_netEmpresa}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brSelectOneRadio id="rdoGerarRetOpeReali12" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoGerarRetornoOperRealizadaInternet}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>		
		
	<!-- Recadastramento Benefici�rio -->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 14}"  columns="1" cellpadding="0" cellspacing="0" width="100%" >
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_tipoDeServico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoServicoSelecionado}"/>						
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_situacaoDoServico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsSituacaoServico}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim><hr class="lin"> </f:verbatim>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_codigo_identificador_beneficiario}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoIdentificacaoBeneficiario" value="#{manterContratoServicosBean.cboTipoIdentificacaoBeneficiario}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoIdentificacaoBeneficiario}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_consistencia_identificador_beneficiario}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoConsistenciaIdentificacaoBeneficiario" value="#{manterContratoServicosBean.cboTipoConsistenciaIdentificacaoBeneficiario}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoConsistenciaIdentificacaoBeneficiario}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_condicao_enquadramento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboCondicaoEnquadramento" value="#{manterContratoServicosBean.cboCondicaoEnquadramento}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaCondicaoEnquadramento}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_criterio_principal_enquadramento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						<br:brSelectOneMenu id="cboCritPrincEnquadramento" value="#{manterContratoServicosBean.cboCritPrincEnquadramento}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaCriterioPrincipalEnquadramento}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_criterio_composto_enquadramento}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboCritCompEnquadramento" value="#{manterContratoServicosBean.cboCritCompEnquadramento}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaCriterioCompostoEnquadramento}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_etapas_recadastramento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
			
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
						<br:brPanelGroup>	
				   			<br:brInputText size="4" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" maxlength="2" id="quantidadeEtapasRecadastramento" value="#{manterContratoServicosBean.quantidadeEtapasRecadastramento}" onkeypress="onlyNum();">  
				   			</br:brInputText>	
						</br:brPanelGroup>	
					</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_fases_etapa}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
		   				<br:brInputText size="5" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" maxlength="2" id="qtddFasesPorEtapa" value="#{manterContratoServicosBean.qtddFasesPorEtapa}" onkeypress="onlyNum();">  
		   				</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_meses_prazo_fase}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
		   				<br:brInputText size="5" maxlength="2" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="qtddMesesPorFase" value="#{manterContratoServicosBean.qtddMesesPorFase}" onkeypress="onlyNum();">  
		   				</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_meses_prazo_etapa}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
		   				<br:brInputText size="5" maxlength="2" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="qtddMesesPorEtapa" value="#{manterContratoServicosBean.qtddMesesPorEtapa}" onkeypress="onlyNum();">  
		   				</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_envio_remessa_manutencao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brSelectOneRadio id="rdoPermiteEnvioRemessaManutencaoCadastro" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermiteEnvioRemessaManutencaoCadastro}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/>
		           			<a4j:support event="onclick" reRender="cboPeriodicidadeEnvioRemessaManutencao" action="#{manterContratoServicosBean.controlaRdoCadastroManCadProcuradores}" status="statusAguarde" />
	   	        		</br:brSelectOneRadio>
			   		</br:brPanelGroup>	
				</br:brPanelGrid>	
				
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_periodicidade_envio_remessa_manutencao}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboPeriodicidadeEnvioRemessaManutencao" value="#{manterContratoServicosBean.cboPeriodicidadeEnvioRemessaManutencao}"
						disabled="#{manterContratoServicosBean.desabilitaCamposEnvioRemessaMan}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterContratoServicosBean.listaPeriodicidade}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_cadastro_utilizado_recadastramento_beneficiarios}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboCadUtilizadoRec" value="#{manterContratoServicosBean.cboCadUtilizadoRec}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaCadastroUtilizadoRecadastramento}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
					
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_carga_base_dados_utilizada}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoCargaCadastroBeneficiario" value="#{manterContratoServicosBean.cboTipoCargaCadastroBeneficiario}" >
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterContratoServicosBean.listaTipoCargaCadastroBeneficiarios}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_antecipar_recadastramento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brSelectOneRadio id="rdoPermiteAnteciparRecadastramento" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermiteAnteciparRecadastramento}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
			   		</br:brPanelGroup>	
				</br:brPanelGrid>	
				
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_data_limite_inicio_vinculo_carga_base}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<a4j:outputPanel id="calendar3" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >	
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
							<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dtLimiteVincClienteBenef');"  id="dtLimiteVincClienteBenef" value="#{manterContratoServicosBean.dtLimiteVincClienteBenef}">
						</app:calendar>
						</br:brPanelGroup>
					</br:brPanelGrid>
				</a4j:outputPanel>	
				
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_data_inicio_recadastramento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			<a4j:outputPanel id="calendario" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >		
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dtInicioRecadastramento');"  id="dtInicioRecadastramento" value="#{manterContratoServicosBean.dtInicioRecadastramento}">
					</app:calendar>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</a4j:outputPanel>		
				
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_data_fim_recadastramento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			<a4j:outputPanel id="calendario2" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dtFimRecadastramento');"  id="dtFimRecadastramento" value="#{manterContratoServicosBean.dtFimRecadastramento}">
					</app:calendar>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</a4j:outputPanel>	
				
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_acertos_dados}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brSelectOneRadio id="rdoPermiteAcertosDados" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermiteAcertosDados}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/>
		           			<a4j:support event="onclick" reRender="calendario4,calendario5" status="statusAguarde" />
	   	        		</br:brSelectOneRadio>
			   		</br:brPanelGroup>	
				</br:brPanelGrid>	
				
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_data_inicio_periodo_acerto_dados}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			<br:brPanelGroup id="calendario4" style="width: 100%; text-align: left">		
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup id="calendario4Habilitado" rendered="#{manterContratoServicosBean.rdoPermiteAcertosDados == 1}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dtInicioPerAcertoDados');"  id="dtInicioPerAcertoDados" value="#{manterContratoServicosBean.dtInicioPerAcertoDados}"/>
					</br:brPanelGroup>

					<br:brPanelGroup id="calendario4Desabilitado" rendered="#{manterContratoServicosBean.rdoPermiteAcertosDados == 2}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dtInicioPerAcertoDados');"  id="dtInicioPerAcertoDadosDes" value="" disabled="true"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>		
				
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_data_fim_periodo_acerto_dados}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			<a4j:outputPanel id="calendario5" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup id="calendario5Habilitado" rendered="#{manterContratoServicosBean.rdoPermiteAcertosDados == 1}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dtFimPerAcertoDados');" id="dtFimPerAcertoDados" value="#{manterContratoServicosBean.dtFimPerAcertoDados}"/>
					</br:brPanelGroup>

					<br:brPanelGroup id="calendario5Desabilitado" rendered="#{manterContratoServicosBean.rdoPermiteAcertosDados == 2}">
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'dtFimPerAcertoDados');" id="dtFimPerAcertoDadosDes" disabled="true"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</a4j:outputPanel>	
				
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_emite_msg_recadastramento_online}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brSelectOneRadio id="rdoEmiteMsgRecadMidia" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoEmiteMsgRecadMidia}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/>
		           			<a4j:support event="onclick" reRender="cboMidiaMsgRecad" action="#{manterContratoServicosBean.controlaRdoEmiteMsgRecadMidiaOnline}" status="statusAguarde" />
	   	        		</br:brSelectOneRadio>
			   		</br:brPanelGroup>	
				</br:brPanelGrid>	
				
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_midia_mensagem_recadastramento_online}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboMidiaMsgRecad" value="#{manterContratoServicosBean.cboMidiaMsgRecad}" disabled="#{manterContratoServicosBean.desabilitaCamposMsgOnline}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{manterContratoServicosBean.listaMidiaMensagemRecadastramento}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
			
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_retorno_operacoes_realizadas_netEmpresa}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brSelectOneRadio id="rdoGerarRetornoOperRealizadaInternet" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoGerarRetornoOperRealizadaInternet}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
			   		</br:brPanelGroup>	
				</br:brPanelGrid>	
				
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.conServicosManterContrato_btnVoltar}" action="#{manterContratoServicosBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" style="margin-right:5px" value="#{msgs.conServicosManterContrato_btnLimpar}"  action="#{manterContratoServicosBean.limparConfiguracao}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnSalvar" styleClass="bto1" value="#{msgs.btn_avancar}" action="#{manterContratoServicosBean.salvarConfiguracao}" onclick="javascript: desbloquearTela(); return validaServicosConfiguracaoP2('#{msgs.label_ocampo}', 
												'#{msgs.label_necessario}','#{msgs.label_abertura_conta_banco_postal_bradesco_seguros}','#{msgs.label_acao_nao_comprovacao}','#{msgs.label_rastrear_somente_boleto_bradesco}','#{msgs.label_agendamento_debitos_pendentes_veiculos}'
												,'#{msgs.label_agendar_titulo_rastreado_filial}','#{msgs.label_agendar_titulo_rastreado_proprio}','#{msgs.label_agrupar_correspondencias}','#{msgs.label_autorizacao_complementar_agencia}'
												,'#{msgs.label_bloquear_emissao_papeleta}','#{msgs.label_cadastro_utilizado_recadastramento}','#{msgs.label_cadastro_consulta_endereco_envio}','#{msgs.label_capturar_titulos_data_registro}'
												,'#{msgs.label_cobrar_tarifas_funcionarios}','#{msgs.label_codigo_formulario}','#{msgs.label_condicao_enquadramento}','#{msgs.label_controle_expiracao_credito}'
												,'#{msgs.label_criterio_composto_enquadramento}','#{msgs.label_criterio_principal_enquadramento}','#{msgs.label_data_inicio_bloqueio_papeleta}','#{msgs.label_data_inicio_rastreamento}'
												,'#{msgs.label_data_fim_periodo_acerto_dados}','#{msgs.label_data_fim_recadastramento}','#{msgs.label_data_inicio_periodo_acerto_dados}','#{msgs.label_data_inicio_recadastramento}'
												,'#{msgs.label_data_limite_enquadramento_convenio_conta_salario}','#{msgs.label_data_limite_vinculo_cliente_beneficiario}','#{msgs.label_demonstra_informacoes_area_reservada}','#{msgs.label_destino_envio}'
												,'#{msgs.label_destino_envio_correspondencia}','#{msgs.label_efetua_consistencia_especie_beneficio}'
												,'#{msgs.label_emissao_antecipada_cartao_conta_salario}','#{msgs.label_emite_aviso_comprovacao_vida}','#{msgs.label_emite_msg_recadastramento}','#{msgs.label_exige_liberacao_lote_processado}'
												,'#{msgs.label_forma_autorizacao_pagamentos}','#{msgs.label_forma_envio_pagamentos}','#{msgs.label_forma_manutencao_cadastro_favorecidos}','#{msgs.label_forma_manutencao_cadastro_procuradores}'
												,'#{msgs.label_formulario_impressao}','#{msgs.label_gerar_lancamento_futuro_credito}','#{msgs.label_gerar_lancamento_futuro_debito}','#{msgs.label_gerar_lancamento_programado}'
												,'#{msgs.label_gerar_retorno_operacoes_realizadas_internet}','#{msgs.label_informa_agencia_conta_credito}','#{msgs.label_midia_disponibilizacao_correntista}','#{msgs.label_midia_mensagem_recadastramento}'
												,'#{msgs.label_momento_envio}','#{msgs.label_momento_indicacao_credito_efetivado}','#{msgs.label_ocorrencia_debito}','#{msgs.label_percentual_maximo_registros_inconsistentes_lote}'
												,'#{msgs.label_periodicidade_emissao}','#{msgs.label_periodicidade_manutencao_cadastro_procuradores}','#{msgs.label_periodicidade_envio_remessa_manutencao}','#{msgs.label_periodicidade_pesquisa_debitos_pendentes_veiculos}'
												,'#{msgs.label_permite_acertos_dados}','#{msgs.label_permite_antecipar_recadastramento}','#{msgs.label_permite_contigencia_pagamento}','#{msgs.label_permite_debito_online}'
												,'#{msgs.label_permite_envio_remessa_manutencao_cadastro}','#{msgs.label_permite_estorno_pagamento}','#{msgs.label_permite_favorecido_consultar_pagamento}','#{msgs.label_permite_pagar_menor}'
												,'#{msgs.label_permite_pagar_vencido}','#{msgs.label_permitir_agrupamento_correspondencia}','#{msgs.label_pesquisa_de}','#{msgs.label_possui_expiracao_credito}'
												,'#{msgs.label_pre_autorizacao_cliente}','#{msgs.label_prioridade_debito}','#{msgs.label_processamento_efetivacao_pagamento}','#{msgs.label_quantidade_dias_floating}'
												,'#{msgs.label_quantidade_dias_repique}','#{msgs.label_quantidade_dias_emissao_aviso_antes_inicio_comprovacao}','#{msgs.label_quantidade_dias_emissao_aviso_antes_vencimento}','#{msgs.label_quantidade_dias_expiracao_credito}'
												,'#{msgs.label_quantidade_dias_aviso_antecipado}','#{msgs.label_quantidade_dias_envio_antecipado_formulario}','#{msgs.label_quantidade_dias_para_expiracao}','#{msgs.label_quantidade_dias_para_inativacao_favorecido}'
												,'#{msgs.label_quantidade_fases_etapa}','#{msgs.label_quantidade_linhas_comprovante}','#{msgs.label_quantidade_meses_emissao}','#{msgs.label_quantidade_meses_periodicidade_comprovacao}'
												,'#{msgs.label_quantidade_meses_etapa}','#{msgs.label_quantidade_meses_fase}','#{msgs.label_quantidade_vias_emitir}','#{msgs.label_quantidade_vias_pagas_registro}'
												,'#{msgs.label_quantidade_etapas_recadastramento}','#{msgs.label_quantidade_limite_cartao_conta_salario_solicitacao}','#{msgs.label_quantidade_limite_dias_para_pagamento_vencido}','#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}'
												,'#{msgs.label_rastrear_notas_fiscais}','#{msgs.label_rastrear_sacado_agregado}','#{msgs.label_texto_informacao_area_reservada}','#{msgs.label_tipo_cartao_conta_salario}'
												,'#{msgs.label_tipo_consistencia_cpf_cnpj_proprietario}','#{msgs.label_tipo_consistencia_identificacao_beneficiario}','#{msgs.label_tipo_carga_cadastro_beneficiario}','#{msgs.label_tipo_consistencia_inscricao_favorecido}'
												,'#{msgs.label_tipo_consistencia_cpf_cnpj_favorecido}','#{msgs.label_tipo_consolidacao_pagamentos_extrato}','#{msgs.label_tipo_data_controle_floating}','#{msgs.label_tipo_emissao}'
												,'#{msgs.label_tipo_formacao_lista_debito}','#{msgs.label_tipo_inscricao_favorecido}','#{msgs.label_tipo_rastreamento_titulos}','#{msgs.label_tipo_rejeicao_lote}'
												,'#{msgs.label_tipo_rejeicao_efetivacao}','#{msgs.label_tipo_rejeicao_agendamento}','#{msgs.label_tipo_tratamento_conta_transferida}','#{msgs.label_tipo_identificacao_beneficiario}'
												,'#{msgs.label_tratamento_feriados_data_pagamento}','#{msgs.label_tratamento_feriado_fim_vigencia}','#{msgs.label_tratamento_lista_debito_sem_numeracao}','#{msgs.label_tratamento_valor_divergente}'
												,'#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}','#{msgs.label_utiliza_cafastro_orgaos_pagadores}','#{msgs.label_utiliza_cadastro_procuradores}'
												,'#{msgs.label_utiliza_frases_pre_cadastradas}','#{msgs.label_utiliza_lista_debito}','#{msgs.label_utiliza_mensagem_personalizada_online}','#{msgs.label_validar_nome_favorecido_receita_federal}'
												,'#{msgs.label_valor_limite_diario}','#{msgs.label_valor_limite_individual}','#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}'
												,'#{msgs.label_tipoContaCredito}','#{msgs.label_tipo_de_consulta_de_saldo}','#{msgs.label_permite_correspondencia_aberta}'
												,'#{msgs.label_meio_disponibilizacao_correntista}','#{msgs.label_meio_disponibilizacao_nao_correntista}','#{msgs.label_tipo_formatacao_primeira_linha_extrato_credito}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>

	

	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>