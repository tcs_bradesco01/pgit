<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<brArq:form id="confModServicosManterContrato" name="confModServicosManterContrato">
<h:inputHidden id="cdParametro" value="#{manterContratoServicosBean.cdParametroTela}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	 
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.gerenteResponsavelFormatado}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpj}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocial}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_empresaGestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_tipoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_tipoDeServico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsTipoServicoSelecionado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolManutContratos_modalidade}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsModalidade}"/>
		</br:brPanelGroup>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_situacaoDaModalidade}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoServicosBean.dsSituacaoModalidade}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<!-- Aviso de Recadastramento -- CdParametro:15-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 15}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_momento_envio}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboMomentoEnvio15" value="#{manterContratoServicosBean.cboMomentoEnvio}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorMomentoAvisoRacadastro != 'S'}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaMomentoEnvio}" />
						</br:brSelectOneMenu>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:30px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_destino}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneRadio id="cboDestinoEnvio15" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.cboDestinoEnvio}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorDestinoAviso != 'S'}">
	           			<f:selectItem  itemValue="2" itemLabel="Agencia Gestora do Contrato"/>
	           			<f:selectItem  itemValue="3" itemLabel="E-mail"/>
						<f:selectItem  itemValue="1" itemLabel="Correspondencia"/>
   	        		</br:brSelectOneRadio>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brInputText size="70" maxlength="70" style="margin-right:10px;" value="" disabled="true"/>
						<br:brCommandButton id="btnBuscar2" styleClass="bto1" style="align:left" value="Buscar" disabled="true">
							<brArq:submitCheckClient/>
						</br:brCommandButton>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_cadastro_endereco_utilizado}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboCadConsultaEndEnvio15" value="#{manterContratoServicosBean.cboCadConsultaEndEnvio}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorConsEndereco != 'S'}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems  value="#{manterContratoServicosBean.listaCadastroConsultaEnderecoEnvio}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_dias_aviso_antecipado}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brInputText size="2" maxlength="3" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="qtddDiasAvisoAntecipado15" value="#{manterContratoServicosBean.qtddDiasAvisoAntecipado}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.saidaVerificarAtributo.qtIndicadorAntecedencia != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permitir_agrupamento_correspondencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brSelectOneRadio id="rdoPermitirAgrupamentoCorrespondencia15" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermitirAgrupamentoCorrespondencia}"
			   				disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorAgrupamentoAviso != 'S'}">
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<!-- Formulario de Recadastramento -- CdParametro:16-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 16}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_momento_envio}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboMomentoEnvio16" value="#{manterContratoServicosBean.cboMomentoEnvio}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorAgrupamentoAviso != 'S'}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems  value="#{manterContratoServicosBean.listaMomentoEnvio}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:30px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_destino}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneRadio id="cboDestinoEnvio16" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.cboDestinoEnvio}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorDestinoAviso != 'S'}">
	           			<f:selectItem  itemValue="2" itemLabel="Agencia Gestora do Contrato"/>
	           			<f:selectItem  itemValue="3" itemLabel="E-mail"/>
						<f:selectItem  itemValue="1" itemLabel="Correspondencia"/>
   	        		</br:brSelectOneRadio>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brInputText size="70" maxlength="70" style="margin-right:10px;" value="" disabled="true"/>
						<br:brCommandButton id="btnBuscar1" styleClass="bto1" style="align:left" value="Buscar" disabled="true">
							<brArq:submitCheckClient/>
						</br:brCommandButton>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_cadastro_endereco_utilizado}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboCadConsultaEndEnvio16" value="#{manterContratoServicosBean.cboCadConsultaEndEnvio}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorConsEndereco != 'S'}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems  value="#{manterContratoServicosBean.listaCadastroConsultaEnderecoEnvio}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_dias_aviso_antecipado}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brInputText size="2" maxlength="3" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="qtddDiasAvisoAntecipado16" value="#{manterContratoServicosBean.qtddDiasEnvioAntecipadoFormulario}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.saidaVerificarAtributo.qtIndicadorAntecedencia != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permitir_agrupamento_correspondencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brSelectOneRadio id="rdoPermitirAgrupamentoCorrespondencia16" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermitirAgrupamentoCorrespondencia}"
			   				disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFormularioContratoCliente != 'S'}">
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	dsa
	<!-- Pagamentos de Fornecedores - DOC -- CdParametro:17-->
	<!-- Pagamentos de Fornecedores - TED -- CdParametro:18-->
	<!-- Pagamento Via Dep�sito Identificado -- CdParametro:24-->	
	<!-- Ordem de Credito -- CdParametro:25-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 17 ||
								manterContratoServicosBean.cdParametroTela == 18 ||
								manterContratoServicosBean.cdParametroTela == 24 ||
								manterContratoServicosBean.cdParametroTela == 25}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoConsultaSaldo17" value="#{manterContratoServicosBean.cboTipoConsultaSaldo}"
							disabled="#{manterContratoServicosBean.desabilitarComboTipoConsultaSaldo}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoConsultaSaldo}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_processamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboProcessamentoEfetivacaoPagamento17" value="#{manterContratoServicosBean.cboProcessamentoEfetivacaoPagamento}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorMomentoProcessamentoPagamento != 'S'}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaProcessamentoEfetivacaoPagamento}" />
							
							<a4j:support event="onchange" reRender="cboTratFevDataPag17, cboFeriadoLocal17" action="#{manterContratoServicosBean.recarregarCombomFeriado}" status="statusAguarde" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
						<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px">
					<br:brSelectOneMenu id="cboTratFevDataPag17" value="#{manterContratoServicosBean.cboTratamentoFeriadosDataPagamento}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorPagamentoNaoUtilizado != 'S'}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems  value="#{manterContratoServicosBean.listaTratamentoFeriadosDataPagamento}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>

			<br:brPanelGrid style="margin-left:20px" />

			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_pgto_feriado_local}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px">
					<br:brSelectOneMenu id="cboFeriadoLocal17" value="#{manterContratoServicosBean.cdIndicadorFeriadoLocal}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterContratoServicosBean.feriadosLocais}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoPermiteFavConPag17" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFavorecidoConsPagamento != 'S'}">
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/>
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.cdParametroTela == 18}">
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup rendered="#{manterContratoServicosBean.cdParametroTela == 18}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_agendar_autorizar_com_grade_horaria_encerrada}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoPermiteAgAutGrade" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermiteAgAutGrade}">
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}"/>
		           			<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}"/>
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup rendered="#{!manterContratoServicosBean.desabilitaIndicadorRejeicaoAgendaLote}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicaoAgendamento17" value="#{manterContratoServicosBean.cboTipoRejeicaoAgendamento}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRejeicaoAgendamento}" />
							<a4j:support event="onchange" reRender="panelLote" action="#{manterContratoServicosBean.controlarCamposLote}" status="statusAguarde" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" rendered="#{!manterContratoServicosBean.desabilitaIndicadorRejeicaoAgendaLote}">
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		
			<br:brPanelGroup rendered="#{!manterContratoServicosBean.desabilitaIndicadorRejeicaoEfetivacaoLote}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicaoEfetivacao17" value="#{manterContratoServicosBean.cboTipoRejeicaoEfetivacao}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRejeicaoEfetivacao}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{!manterContratoServicosBean.desabilitaIndicadorRejeicaoEfetivacaoLote}">
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" id="panelLote">
			<br:brPanelGroup rendered="#{!manterContratoServicosBean.desabilitaIndicadorMaximaInconLote}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup id="panelQtdeLote1">	
			   			<br:brInputText size="7" maxlength="5" id="txtQtddMaximaRegistroInconLote17" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.desabilitarCamposLote || manterContratoServicosBean.desabilitaIndicadorMaximaInconLote}">
			   				<a4j:support event="onblur" reRender="panelPercLote1" action="#{manterContratoServicosBean.controlarCampoPercLote}" status="statusAguarde" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			   			</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" rendered="#{!manterContratoServicosBean.desabilitaIndicadorMaximaInconLote}">
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup rendered="#{!manterContratoServicosBean.desabilitaIndicadorPercentualMaximoInconLote}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup  id="panelPercLote1">	
			   			<br:brInputText size="4" maxlength="3" id="txtPercentualMaximoRegistroInconsLote17" value="#{manterContratoServicosBean.percMaxRegInconsistente}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.desabilitarCamposLote || manterContratoServicosBean.desabilitaIndicadorPercentualMaximoInconLote}">
			   				<a4j:support event="onblur" reRender="panelQtdeLote1" action="#{manterContratoServicosBean.controlarCampoQtdeLote}" status="statusAguarde" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			   			</br:brInputText>	
			   			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="%"/>
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" rendered="#{!manterContratoServicosBean.desabilitaIndicadorPercentualMaximoInconLote}">
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_prioridade_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboPrioridadeDebito17" value="#{manterContratoServicosBean.cboPrioridadeDebito}"
							disabled="#{manterContratoServicosBean.desabilitarComboPrioridadeDebito}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaPrioridadeDebito}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoGerarLancFutDebito17" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoGerarLancFutDeb}" 
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorLancamentoFuturoDebito != 'S'}">
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoUtilizaCadFavControlePag17" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorUtilizacaoFavorecidoControle != 'S'}">
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
		           			<a4j:support event="onclick" reRender="panelValorMaximoFavNaoCad" action="#{manterContratoServicosBean.controlarCampoValorMaximoFavNaoCad}" status="statusAguarde"  oncomplete="loadMasks();"/>
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup id="panelValorMaximoFavNaoCad">	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorMaximoFavNaoCad17"
			   				 value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" disabled="#{manterContratoServicosBean.desabilitaValorMaximoFavNaoCad}" onkeypress="onlyNum();">  
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorLimitInd17" 
			   			 	 value="#{manterContratoServicosBean.valorLimiteIndividual}" onkeypress="onlyNum();" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.vlIndicadorLimiteIndividualPagamento != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorLimDiario17"
			   			 value="#{manterContratoServicosBean.valorLimiteDiario}" onkeypress="onlyNum();" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.vlIndicadorLimiteDiaPagamento != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="4" maxlength="3" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQtddDiasRepique17" value="#{manterContratoServicosBean.qtddDiasRepique}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.saidaVerificarAtributo.qtIndicadorDiaRepiqConsulta != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="4" maxlength="2" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQtddDiasFloating17" value="#{manterContratoServicosBean.qtddDiasFloating}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorDiaFloatPagamento != 'S'}">
			   			</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="100%">
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_demonstra_duas_linha_extrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoDemonstra2LinhaExtrato17" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoIndicadorSegundaLinhaExtrato}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
	   	         	        			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
	        		
	   	      <br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.alterarModContrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
		
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				 
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="testando" value="#{manterContratoServicosBean.testeOrigem}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaOrigem}" />
					
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	

	<!-- Pagamentos Via Ordem de Pagamento -- CdParametro:19-->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 19}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoConsultaSaldo19" value="#{manterContratoServicosBean.cboTipoConsultaSaldo}" 
						disabled="#{manterContratoServicosBean.desabilitarComboTipoConsultaSaldo}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoConsultaSaldo}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_processamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboProcessamentoEfetivacaoPagamento19" value="#{manterContratoServicosBean.cboProcessamentoEfetivacaoPagamento}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorMomentoProcessamentoPagamento != 'S'}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaProcessamentoEfetivacaoPagamento}" />
							
							<a4j:support event="onchange" reRender="cboTratFevDataPag19, cboFeriadoLocal19" action="#{manterContratoServicosBean.recarregarCombomFeriado}" status="statusAguarde" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
						<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboTratFevDataPag19" value="#{manterContratoServicosBean.cboTratamentoFeriadosDataPagamento}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorPagamentoNaoUtilizado != 'S'}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems  value="#{manterContratoServicosBean.listaTratamentoFeriadosDataPagamento}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_pgto_feriado_local}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px">
					<br:brSelectOneMenu id="cboFeriadoLocal19" value="#{manterContratoServicosBean.cdIndicadorFeriadoLocal}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterContratoServicosBean.feriadosLocais}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>

		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>
		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>
		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brSelectOneRadio id="rdoPermiteFavConPag19" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFavorecidoConsPagamento != 'S'}">
						<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
	           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
   	        		</br:brSelectOneRadio>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicaoAgendamento19" value="#{manterContratoServicosBean.cboTipoRejeicaoAgendamento}"
							disabled="#{manterContratoServicosBean.desabilitaIndicadorRejeicaoAgendaLote}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRejeicaoAgendamento}" />
							<a4j:support event="onchange" reRender="panelLote19" action="#{manterContratoServicosBean.controlarCamposLote}" status="statusAguarde" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicaoEfetivacao19" value="#{manterContratoServicosBean.cboTipoRejeicaoEfetivacao}"
							disabled="#{manterContratoServicosBean.desabilitaIndicadorRejeicaoEfetivacaoLote}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRejeicaoEfetivacao}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" id="panelLote19">
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup id="panelQtdeLote119">	
			   			<br:brInputText size="7" maxlength="5" id="txtQtddMaximaRegistroInconLote19" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.desabilitarCamposLote || manterContratoServicosBean.desabilitaIndicadorRejeicaoEfetivacaoLote}">
			   				<a4j:support event="onblur" reRender="panelPercLote119" action="#{manterContratoServicosBean.controlarCampoPercLote}" status="statusAguarde" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			   			</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup  id="panelPercLote119">	
			   			<br:brInputText size="4" maxlength="3" id="txtPercentualMaximoRegistroInconsLote19" value="#{manterContratoServicosBean.percMaxRegInconsistente}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.desabilitarCamposLote || manterContratoServicosBean.desabilitaIndicadorPercentualMaximoInconLote}">
			   				<a4j:support event="onblur" reRender="panelQtdeLote119" action="#{manterContratoServicosBean.controlarCampoQtdeLote}" status="statusAguarde" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			   			</br:brInputText>	
			   			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="%"/>
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_prioridade_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboPrioridadeDebito19" value="#{manterContratoServicosBean.cboPrioridadeDebito}"
							disabled="#{manterContratoServicosBean.desabilitarComboPrioridadeDebito}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaPrioridadeDebito}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoGerarLancFutDebito19" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoGerarLancFutDeb}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorLancamentoFuturoDebito != 'S'}">
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoUtilizaCadFavControlePag19" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorUtilizacaoFavorecidoControle != 'S'}">
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
		           			<a4j:support event="onclick" reRender="panelValorMaximoFavNaoCad19" action="#{manterContratoServicosBean.controlarCampoValorMaximoFavNaoCad}" status="statusAguarde"  oncomplete="loadMasks();"/>
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup id="panelValorMaximoFavNaoCad19">	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorMaximoFavNaoCad19" value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.desabilitaValorMaximoFavNaoCad}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorLimitInd19" 
			   			value="#{manterContratoServicosBean.valorLimiteIndividual}" onkeypress="onlyNum();" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.vlIndicadorLimiteIndividualPagamento != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorLimDiario19"
			   				 value="#{manterContratoServicosBean.valorLimiteDiario}" onkeypress="onlyNum();" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.vlIndicadorLimiteIndividualPagamento != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="4" maxlength="3" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQtddDiasRepique19" value="#{manterContratoServicosBean.qtddDiasRepique}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.saidaVerificarAtributo.qtIndicadorDiaRepiqConsulta != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="4" maxlength="2" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQtddDiasFloating19" value="#{manterContratoServicosBean.qtddDiasFloating}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorDiaFloatPagamento != 'S'}">
			   			</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_ocorrencia_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" id="painelCombo">
					<br:brSelectOneMenu id="cboOcorrenciaDebito19" value="#{manterContratoServicosBean.cboOcorrenciaDebito}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorMomentoDebitoPagamento == 'N'}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}" />
						<f:selectItems  value="#{manterContratoServicosBean.listaOcorrenciaDebito}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_inscricao_favorecido}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboTipoInscFav19" value="#{manterContratoServicosBean.cboTipoInscricaoFavorecido}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorTipoIsncricaoFavorecido != 'S'}">
						<f:selectItem itemValue="-1" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems  value="#{manterContratoServicosBean.listaTipoInscricaoFavorecido}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_local_emissao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboLocalEmissao" value="#{manterContratoServicosBean.cdLocalEmissao}" converter="javax.faces.Integer">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItem itemValue="1" itemLabel="AGENCIA ESPECIFICA"/>
						<f:selectItem itemValue="2" itemLabel="QUALQUER AGENCIA"/>
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
				
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_demonstra_duas_linha_extrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoDemonstra2LinhaExtrato19" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoIndicadorSegundaLinhaExtrato}" >
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_dias_para_expiracao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="4" maxlength="3" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQtddDiasExpiracao19" value="#{manterContratoServicosBean.quantidadeDiasExpiracao}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.saidaVerificarAtributo.qtIndicadorDiaExpiracao != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<!-- Pagamento Via Cr�dito em Conta -- CdParametro:20 -->
	<br:brPanelGrid rendered="#{manterContratoServicosBean.cdParametroTela == 20}" columns="1" cellpadding="0" cellspacing="0" width="100%" >

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_de_consulta_de_saldo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoConsultaSaldo20" value="#{manterContratoServicosBean.cboTipoConsultaSaldo}"
							disabled="#{manterContratoServicosBean.desabilitarComboTipoConsultaSaldo}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoConsultaSaldo}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_processamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboProcessamentoEfetivacaoPagamento20" value="#{manterContratoServicosBean.cboProcessamentoEfetivacaoPagamento}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorMomentoProcessamentoPagamento != 'S'}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaProcessamentoEfetivacaoPagamento}" />
							<a4j:support event="onchange" reRender="cboTratFevDataPag20, cboFeriadoLocal20" action="#{manterContratoServicosBean.recarregarCombomFeriado}" status="statusAguarde" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_pgto_feriado_nacional_fim_semana}:"/>
						<br:brOutputText rendered="#{!manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}" styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tratamento_pagamento_dia_nao_util}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboTratFevDataPag20" value="#{manterContratoServicosBean.cboTratamentoFeriadosDataPagamento}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorPagamentoNaoUtilizado != 'S'}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems  value="#{manterContratoServicosBean.listaTratamentoFeriadosDataPagamento}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_pgto_feriado_local}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px">
					<br:brSelectOneMenu id="cboFeriadoLocal20" value="#{manterContratoServicosBean.cdIndicadorFeriadoLocal}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterContratoServicosBean.feriadosLocais}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>

		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>
		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>
		<br:brPanelGrid styleClass="EspacamentoLinhas" rendered="#{manterContratoServicosBean.habilitaCampoTipServicoForncSalTrib}"/>

		<br:brPanelGroup>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_permite_favorecido_consultar_pagamento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brSelectOneRadio id="rdoPermiteFavConPag20" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoPermiteFavorecidoConsultarPagamento}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorFavorecidoConsPagamento != 'S'}">
						<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
	           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
   	        		</br:brSelectOneRadio>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>

		</br:brPanelGrid>
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rejeicao_agendamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicaoAgendamento20" value="#{manterContratoServicosBean.cboTipoRejeicaoAgendamento}"
							disabled="#{manterContratoServicosBean.desabilitaIndicadorRejeicaoAgendaLote}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRejeicaoAgendamento}" />
							<a4j:support event="onchange" reRender="panelLote20" action="#{manterContratoServicosBean.controlarCamposLote}" status="statusAguarde" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_rejeicao_efetivacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoRejeicaoEfetivacao20" value="#{manterContratoServicosBean.cboTipoRejeicaoEfetivacao}"
							disabled="#{manterContratoServicosBean.desabilitaIndicadorRejeicaoEfetivacaoLote}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaTipoRejeicaoEfetivacao}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" id="panelLote20">
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup id="panelQtdeLote120">	
			   			<br:brInputText size="7" maxlength="5" id="txtQtddMaximaRegistroInconLote20" value="#{manterContratoServicosBean.qtddMaximaRegistrosInconsistentesLote}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.desabilitarCamposLote || manterContratoServicosBean.desabilitaIndicadorMaximaInconLote}">
			   				<a4j:support event="onblur" reRender="panelPercLote120" action="#{manterContratoServicosBean.controlarCampoPercLote}" status="statusAguarde" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			   			</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_percentual_maximo_registros_inconsistentes_lote}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup  id="panelPercLote120">	
			   			<br:brInputText size="4" maxlength="3" id="txtPercentualMaximoRegistroInconsLote20" value="#{manterContratoServicosBean.percMaxRegInconsistente}" 
			   			    disabled="#{manterContratoServicosBean.desabilitarCamposLote || manterContratoServicosBean.desabilitaIndicadorPercentualMaximoInconLote}" onkeypress="onlyNum();">
			   			    <a4j:support event="onblur" reRender="panelQtdeLote120" action="#{manterContratoServicosBean.controlarCampoQtdeLote}" status="statusAguarde" oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />  
			   			</br:brInputText>	
			   			<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="%"/>
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_prioridade_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneMenu id="cboPrioridadeDebito20" value="#{manterContratoServicosBean.cboPrioridadeDebito}"
							disabled="#{manterContratoServicosBean.desabilitarComboPrioridadeDebito}">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems  value="#{manterContratoServicosBean.listaPrioridadeDebito}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoUtilizaCadFavControlePag20" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoUtilizaCadastroFavorecidoControlePagamentos}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorUtilizacaoFavorecidoControle != 'S'}">
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
		           			<a4j:support event="onclick" reRender="panelValorMaximoFavNaoCad20" action="#{manterContratoServicosBean.controlarCampoValorMaximoFavNaoCad}" status="statusAguarde"  oncomplete="loadMasks();"/>
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup id="panelValorMaximoFavNaoCad20">	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorMaximoFavNaoCad20" value="#{manterContratoServicosBean.valorMaximoPagamentoFavorecidoNaoCadastrado}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.desabilitaValorMaximoFavNaoCad}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_limite_agendamento_individual}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorLimitInd20"
			   				 value="#{manterContratoServicosBean.valorLimiteIndividual}" onkeypress="onlyNum();" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.vlIndicadorLimiteIndividualPagamento != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_valor_limite_agendamento}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="30" onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" converter="decimalBrazillianConverter" alt="decimalBr" id="txtValorLimDiario20"
			   			 value="#{manterContratoServicosBean.valorLimiteDiario}" onkeypress="onlyNum();" disabled="#{manterContratoServicosBean.saidaVerificarAtributo.vlIndicadorLimiteDiaPagamento != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="4" maxlength="3" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQtddDiasRepique20" value="#{manterContratoServicosBean.qtddDiasRepique}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.saidaVerificarAtributo.qtIndicadorDiaRepiqConsulta != 'S'}">
			   			</br:brInputText>	
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_quantidade_dias_floating}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>	
			   			<br:brInputText size="4" maxlength="2" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtQtddDiasFloating20" value="#{manterContratoServicosBean.qtddDiasFloating}" onkeypress="onlyNum();"
			   				disabled="#{manterContratoServicosBean.habilitaQtdDiasFloating}" >
			   				<a4j:support event="onblur" reRender="txtQtddDiasFloating20, rdoGerarLancProg20" action="#{manterContratoServicosBean.habilitaRadioLancamento}" status="statusAguarde" />
			   			</br:brInputText>	
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_lancamento_futuro_debito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoGerarLancFutDebito20" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoGerarLancFutDeb}"
							disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorLancamentoFuturoDebito != 'S'}">
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_lancamento_futuro_credito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneRadio id="rdoGerarLancFutCredito20" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoGerarLancFutCred}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorLancamentoFuturoCredito != 'S'}">
						<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
	           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
   	        		</br:brSelectOneRadio>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
   			<br:brPanelGroup rendered="#{manterContratoServicosBean.habilitaCampo}">
	   			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >

					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_gerar_lancamento_programado}:"/>
					</br:brPanelGroup>
				
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoGerarLancProg20" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoGerarLancProgramado}"
							disabled="#{manterContratoServicosBean.habilitaRdoGerarLancamento || manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorLancamentoPagamento == 'N'}">
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem itemValue="2" itemLabel="#{msgs.label_nao}"/> 
		           			<a4j:support event="onclick" reRender="txtQtddDiasFloating20" action="#{manterContratoServicosBean.habilitaCamposFloatingLancamento}" status="statusAguarde"/>
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" rendered="#{manterContratoServicosBean.habilitaCampo}">
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>	

			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_tratamento_conta_debito_transferida}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboTratContaTrans20" value="#{manterContratoServicosBean.cboTipoTratamentoContaTransferida}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorContratoContaTransferencia != 'S'}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems  value="#{manterContratoServicosBean.listaTipoTratamentoContaTransferida}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>

			<br:brPanelGrid rendered="#{!manterContratoServicosBean.habilitaCampo}">
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGrid rendered="#{!manterContratoServicosBean.habilitaCampo}">
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_consistencia_cpf_cnpj_favorecido}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brSelectOneMenu id="cboTipoConsCpfCnpjFav20" value="#{manterContratoServicosBean.cboTipoConsistenciaCpfCnpjFavorecido}"
						disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorTipoContaFavorecidoT != 'S'}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems  value="#{manterContratoServicosBean.listaTipoConsistenciaCpfCnpjFavorecido}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_validar_nome_favorecido_receita_federal}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
			   			<br:brSelectOneRadio id="rdoValidarNomeFavReceitaFed20" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoValidarNomeFavorecidoReceitaFederal}"
			   				disabled="#{manterContratoServicosBean.saidaVerificarAtributo.cdIndicadorValidacaoNomeFavorecido != 'S'}">
							<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
		           			<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
	   	        		</br:brSelectOneRadio>
			   		</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="100%">
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_Origem_demonstra_duas_linha_extrato_portabilidade_bradesco}:"
							rendered="#{!manterContratoServicosBean.exibeOrigemSegundaViaExtrato}"/>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_Origem_demonstra_duas_linha_extrato}:"
							rendered="#{manterContratoServicosBean.exibeOrigemSegundaViaExtrato}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brSelectOneMenu id="cboDemonstra2LinhaExtrato20" value="#{manterContratoServicosBean.rdoIndicadorSegundaLinhaExtrato}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
						<f:selectItems value="#{manterContratoServicosBean.listaDemonstra2LinhaExtrato}" />
					</br:brSelectOneMenu>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" width="100%">
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.label_tipo_formatacao_primeira_linha_extrato_credito}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brSelectOneRadio id="rdoTipoFormatacaoPrimeiraLinhaExtratoCredito" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoServicosBean.rdoTipoFormatacaoPrimeiraLinha}">
							<f:selectItem itemValue="1" itemLabel="#{msgs.label_personalizado_por_cadastro}"/>  
		           			<f:selectItem itemValue="2" itemLabel="#{msgs.label_personalizado_por_movimento}"/> 
	   	        		</br:brSelectOneRadio>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.conServicosManterContrato_btnVoltar}" action="#{manterContratoServicosBean.voltarModalidadeConfigurar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" style="margin-right:5px" value="#{msgs.conServicosManterContrato_btnLimpar}"  action="#{manterContratoServicosBean.limparConfiguracaoMod}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnSalvar" styleClass="bto1" value="#{msgs.btn_avancar}" action="#{manterContratoServicosBean.salvarConfiguracaoMod}" 
				onclick="if(!validaModalidadesConfiguracao('#{msgs.label_ocampo}', 
				'#{msgs.label_necessario}','#{msgs.label_abertura_conta_banco_postal_bradesco_seguros}','#{msgs.label_acao_nao_comprovacao}','#{msgs.label_rastrear_somente_boleto_bradesco}','#{msgs.label_agendamento_debitos_pendentes_veiculos}'
				,'#{msgs.label_agendar_titulo_rastreado_filial}','#{msgs.label_agendar_titulo_rastreado_proprio}','#{msgs.label_agrupar_correspondencias}','#{msgs.label_autorizacao_complementar_agencia}'
				,'#{msgs.label_bloquear_emissao_papeleta}','#{msgs.label_cadastro_utilizado_recadastramento}','#{msgs.label_cadastro_endereco_utilizado}','#{msgs.label_capturar_titulos_data_registro}'
				,'#{msgs.label_cobrar_tarifas_funcionarios}','#{msgs.label_codigo_formulario}','#{msgs.label_condicao_enquadramento}','#{msgs.label_controle_expiracao_credito}'
				,'#{msgs.label_criterio_composto_enquadramento}','#{msgs.label_criterio_principal_enquadramento}','#{msgs.label_data_inicio_bloqueio_papeleta}','#{msgs.label_data_inicio_rastreamento}'
				,'#{msgs.label_data_fim_periodo_acerto_dados}','#{msgs.label_data_fim_recadastramento}','#{msgs.label_data_inicio_periodo_acerto_dados}','#{msgs.label_data_inicio_recadastramento}'
				,'#{msgs.label_data_limite_enquadramento_convenio_conta_salario}','#{msgs.label_data_limite_vinculo_cliente_beneficiario}','#{msgs.label_demonstra_informacoes_area_reservada}','#{msgs.label_destino_envio}'
				,'#{msgs.label_destino_envio_correspondencia}','#{msgs.label_efetua_consistencia_especie_beneficio}'
				,'#{msgs.label_emissao_antecipada_cartao_conta_salario}','#{msgs.label_emite_aviso_comprovacao_vida}','#{msgs.label_emite_msg_recadastramento}','#{msgs.label_exige_liberacao_lote_processado}'
				,'#{msgs.label_forma_autorizacao_pagamentos}','#{msgs.label_forma_envio_pagamentos}','#{msgs.label_forma_manutencao_cadastro_favorecidos}','#{msgs.label_forma_manutencao_cadastro_procuradores}'
				,'#{msgs.label_formulario_impressao}','#{msgs.label_gerar_lancamento_futuro_credito}','#{msgs.label_gerar_lancamento_futuro_debito}','#{msgs.label_gerar_lancamento_programado}'
				,'#{msgs.label_gerar_retorno_operacoes_realizadas_internet}','#{msgs.label_informa_agencia_conta_credito}','#{msgs.label_midia_disponibilizacao_correntista}','#{msgs.label_midia_mensagem_recadastramento}'
				,'#{msgs.label_momento_envio}','#{msgs.label_momento_indicacao_credito_efetivado}','#{msgs.label_ocorrencia_debito}','#{msgs.label_percentual_maximo_registros_inconsistentes_lote}'
				,'#{msgs.label_periodicidade_emissao}','#{msgs.label_periodicidade_manutencao_cadastro_procuradores}','#{msgs.label_periodicidade_envio_remessa_manutencao}','#{msgs.label_periodicidade_pesquisa_debitos_pendentes_veiculos}'
				,'#{msgs.label_permite_acertos_dados}','#{msgs.label_permite_antecipar_recadastramento}','#{msgs.label_permite_contigencia_pagamento}','#{msgs.label_permite_debito_online}'
				,'#{msgs.label_permite_envio_remessa_manutencao_cadastro}','#{msgs.label_permite_estorno_pagamento}','#{msgs.label_permite_favorecido_consultar_pagamento}','#{msgs.label_permite_pagar_menor}'
				,'#{msgs.label_permite_pagar_vencido}','#{msgs.label_permitir_agrupamento_correspondencia}','#{msgs.label_pesquisa_de}','#{msgs.label_possui_expiracao_credito}'
				,'#{msgs.label_pre_autorizacao_cliente}','#{msgs.label_prioridade_debito}','#{msgs.label_tipo_processamento}','#{msgs.label_quantidade_dias_floating}'
				,'#{msgs.label_quantidade_dias_repique}','#{msgs.label_quantidade_dias_emissao_aviso_antes_inicio_comprovacao}','#{msgs.label_quantidade_dias_emissao_aviso_antes_vencimento}','#{msgs.label_quantidade_dias_expiracao_credito}'
				,'#{msgs.label_quantidade_dias_aviso_antecipado}','#{msgs.label_quantidade_dias_envio_antecipado_formulario}','#{msgs.label_quantidade_dias_para_expiracao}','#{msgs.label_local_emissao}','#{msgs.label_quantidade_dias_para_inativacao_favorecido}'
				,'#{msgs.label_quantidade_fases_etapa}','#{msgs.label_quantidade_linhas_comprovante}','#{msgs.label_quantidade_meses_emissao}','#{msgs.label_quantidade_meses_periodicidade_comprovacao}'
				,'#{msgs.label_quantidade_meses_etapa}','#{msgs.label_quantidade_meses_fase}','#{msgs.label_quantidade_vias_emitir}','#{msgs.label_quantidade_vias_pagas_registro}'
				,'#{msgs.label_quantidade_etapas_recadastramento}','#{msgs.label_quantidade_limite_cartao_conta_salario_solicitacao}','#{msgs.label_quantidade_limite_dias_para_pagamento_vencido}','#{msgs.label_quantidade_maxima_registro_inconsistentes_lote}'
				,'#{msgs.label_rastrear_notas_fiscais}','#{msgs.label_rastrear_sacado_agregado}','#{msgs.label_texto_informacao_area_reservada}','#{msgs.label_tipo_cartao_conta_salario}'
				,'#{msgs.label_tipo_consistencia_cpf_cnpj_proprietario}','#{msgs.label_tipo_consistencia_identificacao_beneficiario}','#{msgs.label_tipo_carga_cadastro_beneficiario}','#{msgs.label_tipo_consistencia_inscricao_favorecido}'
				,'#{msgs.label_tipo_consistencia_cpf_cnpj_favorecido}','#{msgs.label_tipo_consolidacao_pagamentos_extrato}','#{msgs.label_tipo_data_controle_floating}','#{msgs.label_tipo_emissao}'
				,'#{msgs.label_tipo_formacao_lista_debito}','#{msgs.label_tipo_inscricao_favorecido}','#{msgs.label_tipo_rastreamento_titulos}','#{msgs.label_tipo_rejeicao_lote}'
				,'#{msgs.label_tipo_rejeicao_efetivacao}','#{msgs.label_tipo_rejeicao_agendamento}','#{msgs.label_tipo_tratamento_conta_transferida}','#{msgs.label_tipo_identificacao_beneficiario}'
				,'#{msgs.label_tratamento_pagamento_dia_nao_util}','#{msgs.label_tratamento_feriado_fim_vigencia}','#{msgs.label_tratamento_lista_debito_sem_numeracao}','#{msgs.label_tratamento_valor_divergente}'
				,'#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}','#{msgs.label_utiliza_cafastro_orgaos_pagadores}','#{msgs.label_utiliza_cadastro_procuradores}'
				,'#{msgs.label_utiliza_frases_pre_cadastradas}','#{msgs.label_utiliza_lista_debito}','#{msgs.label_utiliza_mensagem_personalizada_online}','#{msgs.label_validar_nome_favorecido_receita_federal}'
				,'#{msgs.label_valor_limite_agendamento}','#{msgs.label_valor_limite_agendamento_individual}','#{msgs.label_valor_maximo_pagamento_favorecido_nao_cadastrado}'
				,'#{msgs.label_tipo_de_consulta_de_saldo}','#{msgs.label_quantidade_percentual_maximo_registros_inconsistentes_lote}', '#{msgs.label_pgto_feriado_local}'
				, '#{msgs.label_tipo_formatacao_primeira_linha_extrato_credito}','#{msgs.alterarModContrato}','#{msgs.label_permite_agendar_autorizar_com_grade_horaria_encerrada}')){desbloquearTela(); return false;};">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>