<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="subLayoutsArquivos1" name="subLayoutsArquivos1" >
<h:inputHidden id="hiddenRadio" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.cdTpRejeicAcolimento}"/>
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.hiddenObrigatoriedade}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">


	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.retornoLayoutsArquivos_cliente_master}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_cpf_cnpj1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_nome_razao_social1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.gerenteResponsavelFormatado}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsNomeRazaoSocialParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.retornoLayoutsArquivos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.subLayoutsArquivos1_dados_atuais}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.subLayoutsArquivos1_layout1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.dsLayout}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.subLayoutsArquivos1_aplicativo_formatacao1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.dsAppFormatacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.subLayoutsArquivos1_dados_alterados}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
    	<br:brPanelGrid columns="1" style="margin-right:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_layout2}:"/>
			</br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>		
			<br:brPanelGroup>
		    	<br:brSelectOneMenu value="#{manterContratoBean.manterContratoLayoutsArquivosBean.substituirLayout}" style="width:180px" id="layout">
					<f:selectItem itemValue="0" itemLabel="#{msgs.subLayoutsArquivos1_label_combo_selecione}"/>
					<f:selectItems value="#{manterContratoBean.manterContratoLayoutsArquivosBean.listaTipoLayoutArquivo}" />								
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_aplicativo_formatacao2}:"/>
			</br:brPanelGroup>		
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			<br:brPanelGroup>
		    	<br:brSelectOneMenu value="#{manterContratoBean.manterContratoLayoutsArquivosBean.aplicativoFormatacao}" style="width:180px" id="aplicativoFormatacao">
					<f:selectItem itemValue="0" itemLabel="#{msgs.subLayoutsArquivos1_label_combo_selecione}"/>
					<f:selectItem itemValue="1" itemLabel="#{msgs.subLayoutsArquivos1_label_obb_convencional}"/>
					<f:selectItem itemValue="2" itemLabel="#{msgs.subLayoutsArquivos1_label_obb_plus}"/>
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_sistema_do_arquivo}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<a4j:region renderRegionOnly="true">
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
					<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.substituirCentroCusto}" size="5" maxlength="4" id="txtInicioCentroCusto"/>
					<f:verbatim>&nbsp;</f:verbatim>
					<br:brGraphicImage url="/images/lupa.gif" style="cursor: hand;">
						<a4j:support event="onclick" action="#{manterContratoBean.manterContratoLayoutsArquivosBean.buscarCentroCusto}" reRender="centroCusto" status="statusAguarde"/>
					</br:brGraphicImage>
					<f:verbatim>&nbsp;</f:verbatim>
					<f:verbatim>&nbsp;</f:verbatim>
					<br:brSelectOneMenu id="centroCusto" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.substituirCentroCustoSelecionado}"  disabled="#{empty manterContratoBean.manterContratoLayoutsArquivosBean.listaCentroCustoSubstituir}"
					  style="margin-right:20px" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.subLayoutsArquivos1_label_combo_selecione}"/>
						<f:selectItems value="#{manterContratoBean.manterContratoLayoutsArquivosBean.listaCentroCustoSubstituir}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
			</a4j:region>		
		</br:brPanelGroup>
			
		<br:brPanelGroup>		
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_codigo_serie_aplicativo}:"/>
				</br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGroup>
			    	<br:brInputText styleClass="HtmlInputTextBradesco" id="codigoSerieAplicativo" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.cdSerieAplicacaoTransmissaoSubstituir}" size="19" maxlength="15">
			    	</br:brInputText> 
				</br:brPanelGroup>
			</br:brPanelGrid>		
		</br:brPanelGroup>		
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.subLayoutsArquivos1_meio_transmissao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid styleClass="mainPanel" style="margin-bottom:9px" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.subLayoutsArquivos1_remessa1}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" style="margin-bottom:9px" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.subLayoutsArquivos1_retorno}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
    	<br:brPanelGrid columns="1" style="margin-right:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_principal_remessa}:"/>
			</br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>		
			<br:brPanelGroup>
		    	<br:brSelectOneMenu value="#{manterContratoBean.manterContratoLayoutsArquivosBean.principalRemessa}" style="width:180px" id="principalRemessa">
					<f:selectItem itemValue="0" itemLabel="#{msgs.subLayoutsArquivos1_label_combo_selecione}"/>
					<f:selectItems value="#{manterContratoBean.manterContratoLayoutsArquivosBean.listaMeioTransmissaoRemessaPrincipal}" />	
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_principal_retorno}:"/>
			</br:brPanelGroup>		
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			<br:brPanelGroup>
		    	<br:brSelectOneMenu value="#{manterContratoBean.manterContratoLayoutsArquivosBean.principalRetorno}"  style="width:180px" id="principalRetorno">
					<f:selectItem itemValue="0" itemLabel="#{msgs.subLayoutsArquivos1_label_combo_selecione}"/>
					<f:selectItems value="#{manterContratoBean.manterContratoLayoutsArquivosBean.listaMeioTransmissaoRemessaPrincipal}" />	
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid style="margin-top:6px" columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_alternativo_remessa}:"/>
			</br:brPanelGroup>		
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			<br:brPanelGroup>
		    	<br:brSelectOneMenu value="#{manterContratoBean.manterContratoLayoutsArquivosBean.alternativoRemessa}" style="width:180px" id="alternativoRemessa">
					<f:selectItem itemValue="0" itemLabel="#{msgs.subLayoutsArquivos1_label_combo_selecione}"/>
					<f:selectItems value="#{manterContratoBean.manterContratoLayoutsArquivosBean.listaMeioTransmissaoRemessaPrincipal}" />	
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid style="margin-top:6px" columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_alternativo_retorno}:"/>
			</br:brPanelGroup>		
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			<br:brPanelGroup>
		    	<br:brSelectOneMenu value="#{manterContratoBean.manterContratoLayoutsArquivosBean.alternativoRetorno}" style="width:180px" id="alternativoRetorno">
					<f:selectItem itemValue="0" itemLabel="#{msgs.subLayoutsArquivos1_label_combo_selecione}"/>
					<f:selectItems value="#{manterContratoBean.manterContratoLayoutsArquivosBean.listaMeioTransmissaoRemessaPrincipal}" />	
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid style="margin-top:6px" columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_username}:"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup>
		    	<br:brInputText styleClass="HtmlInputTextBradesco" id="username" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.username}" size="20"></br:brInputText>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid style="margin-top:6px" columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_empresa_responsavel_transmissao}:"/>
			</br:brPanelGroup>		
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			<br:brPanelGroup>
				<br:brInputText styleClass="HtmlInputTextBradesco" id="empresaResponsavelTransmissao" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.empresaResponsavelTransmissao}" size="20"></br:brInputText>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid style="margin-top:6px" columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid cellpadding="0" cellspacing="0">	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_nome_arquivo_remessa}:"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup>
		    	<br:brInputText styleClass="HtmlInputTextBradesco" id="nomeArquivoRemessa" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.dsNomeArquivoRemessa}" size="30"></br:brInputText>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_nome_arquivo_retorno}:"/>
			</br:brPanelGroup>		
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			<br:brPanelGroup>
				<br:brInputText styleClass="HtmlInputTextBradesco" id="nomeArquivoRetorno" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.dsNomeArquivoRetorno}" size="30"></br:brInputText>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.subLayoutsArquivos1_remessa2}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
    	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_nivel_controle}:"/>
			</br:brPanelGroup>	
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>		
			<br:brPanelGroup>
		    	<br:brSelectOneMenu id="cboNivelControle" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.cdNivelControle}"> 
					<f:selectItem itemValue="0" itemLabel="#{msgs.subLayoutsArquivos1_label_combo_selecione}"/>
					<f:selectItem itemValue="1" itemLabel="#{msgs.layoutsArquivos_label_combo_nivelControle_arquivo}"/>
					<f:selectItem itemValue="2" itemLabel="#{msgs.layoutsArquivos_label_combo_nivelControle_lote}"/>
					<f:selectItem itemValue="3" itemLabel="#{msgs.layoutsArquivos_label_combo_nivelControle_semControle}"/>
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_tipo_controle}:"/>
			</br:brPanelGroup>		
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>			
			<br:brPanelGroup>
		    	<br:brSelectOneMenu id="cboTipoControle" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.cdTipoControle}"> 
					<f:selectItem itemValue="0" itemLabel="#{msgs.subLayoutsArquivos1_label_combo_selecione}"/>
					<f:selectItem itemValue="1" itemLabel="#{msgs.layoutsArquivos_label_combo_tipoControle_sequencial}"/> 
					<f:selectItem itemValue="2" itemLabel="#{msgs.layoutsArquivos_label_combo_tipoControle_crescente}"/> 
					<f:selectItem itemValue="3" itemLabel="#{msgs.layoutsArquivos_label_combo_tipoControle_duplicidade}"/> 					
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_periodicidade_inicializacao_contagem}:"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup>
		    	<br:brSelectOneMenu id="cboPeriodicidade" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.cdPeriodicidade}"> 
					<f:selectItem itemValue="0" itemLabel="#{msgs.incRetornoLayoutsArquivos1_label_combo_selecione}"/>
					<f:selectItems value="#{manterContratoBean.manterContratoLayoutsArquivosBean.listaPeriodicidade}"/> 
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_numero_maximo_remessa}:"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup>
		    	<br:brInputText styleClass="HtmlInputTextBradesco" id="numeroMaximoRemessa" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.dsNrMaxRemessa}" size="20">
		    	<brArq:commonsValidator type="integer" arg="#{msgs.subLayoutsArquivos1_numero_maximo_remessa}" server="false" client="true" />
		    	</br:brInputText> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  >
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_tipo_rejeicao_acolhimento}:"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brSelectOneRadio id="radioFiltro" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.cdTpRejeicAcolimento}" >
					<f:selectItem  itemValue="1" itemLabel="#{msgs.altLayoutsArquivos1_radio_total}"/>  
		            <f:selectItem  itemValue="2" itemLabel="#{msgs.altLayoutsArquivos1_radio_parcial}"/> 
		            <a4j:support event="onclick" reRender="hiddenRadio"/>
		    	</br:brSelectOneRadio>
		    </br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.subLayoutsArquivos1_percentual_registros_inconsistentes}:"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup>
		    	<br:brInputText id="inpPercRegIncons" styleClass="HtmlInputTextBradesco" value="#{manterContratoBean.manterContratoLayoutsArquivosBean.subPercRegInconsistentes}" converter="decimalBrazillianConverter" alt="percentual" maxlength="6" size="7">
		    	</br:brInputText>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.subLayoutsArquivos1_btn_voltar}" action="#{manterContratoBean.manterContratoLayoutsArquivosBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.subLayoutsArquivos1_btn_avancar}" action="#{manterContratoBean.manterContratoLayoutsArquivosBean.avancarSubstituir}"
			onclick="javascript:desbloquearTela(); 
			return validarSubstituirLayoutArquivos('#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.subLayoutsArquivos1_layout2}',
	   		'#{msgs.subLayoutsArquivos1_aplicativo_formatacao1}', '#{msgs.subLayoutsArquivos1_principal_remessa}', '#{msgs.subLayoutsArquivos1_alternativo_remessa}','#{msgs.subLayoutsArquivos1_principal_retorno}',
	   		'#{msgs.subLayoutsArquivos1_alternativo_retorno}','#{msgs.subLayoutsArquivos1_empresa_responsavel_transmissao}','#{msgs.subLayoutsArquivos1_nivel_controle}', '#{msgs.subLayoutsArquivos1_tipo_controle}',
	   		'#{msgs.subLayoutsArquivos1_periodicidade_inicializacao_contagem}', '#{msgs.subLayoutsArquivos1_username}', '#{msgs.subLayoutsArquivos1_numero_maximo_remessa}', '#{msgs.subLayoutsArquivos1_percentual_registros_inconsistentes}',
	   		'#{msgs.subLayoutsArquivos1_tipo_rejeicao_acolhimento}','#{msgs.subLayoutsArquivos1_remessa1}','#{msgs.subLayoutsArquivos1_retorno}','#{msgs.subLayoutsArquivos1_sistema_do_arquivo}','#{msgs.subLayoutsArquivos1_codigo_serie_aplicativo}');" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>