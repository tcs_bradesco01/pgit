<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="incLayoutsArquivos1Form" name="incLayoutsArquivos1Form">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.retornoLayoutsArquivos_cliente_master}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_cpf_cnpj1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_nome_razao_social1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	  <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsNomeRazaoSocialParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.retornoLayoutsArquivos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputLabelBoldBradesco" value="#{msgs.label_layout}:"/>
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGroup>
		    	<br:brSelectOneMenu id="cboLayout" value="#{manterContratoLayoutsArquivosBean.layout}"> 
					<f:selectItem itemValue="0" itemLabel="#{msgs.altLayoutsArquivos1__label_combo_selecione}" />
					<f:selectItems value="#{manterContratoLayoutsArquivosBean.listaTipoLayoutArquivoHistorico}"/> 
					<a4j:support event="onchange" reRender="txtCodLacto, painelCodLancamento, painelNumVersLayoutArq" 
						action="#{manterContratoLayoutsArquivosBean.habilitaCamposIncluir}" />
				</br:brSelectOneMenu>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid id="painelCodLancamento" columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup rendered="#{!manterContratoLayoutsArquivosBean.habilitaNumVersLayoutArq}">
				<br:brPanelGroup style="margin-left:25px">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputLabelBoldBradesco" value="#{msgs.label_codigo_lancamento}:"/>
				</br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGroup style="margin-left:25px">
			    	<br:brInputText id="txtCodLacto" value="#{manterContratoLayoutsArquivosBean.codLancamento}" maxlength="5" onfocus="cleanClipboard();" 
			    		onkeypress="onlyNum();" disabled="#{manterContratoLayoutsArquivosBean.habilitarLancamento}" /> 
				</br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid id="painelNumVersLayoutArq" columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup rendered="#{manterContratoLayoutsArquivosBean.habilitaNumVersLayoutArq}">
				<br:brPanelGroup style="margin-left:25px">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputLabelBoldBradesco" value="#{msgs.label_num_vers_layout_arq}:"/>
				</br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGroup style="margin-left:25px">
			    	<br:brInputText id="txtNumVersLayoutArq" value="#{manterContratoLayoutsArquivosBean.numeroVersaoLayoutArquivo}" disabled="true"/> 
				</br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{manterContratoLayoutsArquivosBean.voltar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" style="margin-right:5px" value="#{msgs.btn_limpar}" action="#{manterContratoBean.manterContratoLayoutsArquivosBean.limparLayout}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="#{msgs.btn_avancar}" action="#{manterContratoBean.manterContratoLayoutsArquivosBean.avancarLayouts}"
								onclick="javascript: desbloquearTela(); return checarIncluirLayout(document.forms[1], '#{msgs.label_ocampo}',
	    																											  '#{msgs.label_necessario}',
	    																											  '#{msgs.label_layout}',
	    																											  '#{msgs.label_situacao}',
	    																											  '#{msgs.label_responsavel_custo_trans_arquivo}',
	    																											  '#{msgs.label_percentual_custo_trans_arquivo}');">
	    		<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>


</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>