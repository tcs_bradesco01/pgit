<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="resumoContratoForm" name="resumoContratoForm">
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" style="margin-top:9px">

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.label_cliente_representante}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid id="clienteReprensentante" cellpadding="0" cellspacing="0" columns="1" style="margin-top: 9px">
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.label_nome_razao_social}:" />
					<br:brOutputText id="nomeRazaoSocial" />
					<br:brOutputText
						value="#{manterContratoBean.saidaPrimeiroContrato.dsNomeRepresentante}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.label_cpf_cnpj}:" />
					<br:brOutputText id="cpfCnpj" />
					<br:brOutputText
						value="#{manterContratoBean.saidaPrimeiroContrato.cdCpfCnpjRepresentante}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.label_numero_Contrato}:" />
					<br:brOutputText id="numeroContrato" />
					<br:brOutputText
						value="#{manterContratoBean.saidaPrimeiroContrato.nrSequenciaContrato}" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.label_identificaco_Contrato}:" />
					<br:brOutputText id="identicacaoContrato" />
					<br:brOutputText
						value="#{manterContratoBean.saidaPrimeiroContrato.dsContrato}" />
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%" style="margin-top: 9px">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.label_dados_participante}:" />
			</br:brPanelGroup>

			<br:brPanelGrid style="margin-top: 9px" />


			<br:brPanelGrid cellpadding="0" cellspacing="0" width="100%"
				style="margin-top: 9px">
				<app:scrollableDataTable id="tabelaParticipantes"
					value="#{manterContratoBean.listaParticipantes}"
					var="result" width="100%" rowIndexVar="parameterKey4">
					<app:scrollableColumn>
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_nome_razao_social}" />
						</f:facet>
						<br:brOutputText value="#{result.nmParticipante}" />
					</app:scrollableColumn>
					<app:scrollableColumn>
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_cpf_cnpj}" />
						</f:facet>
						<br:brOutputText value="#{result.cdCpfCnpjParticipante}" />
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGrid>

		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%" style="margin-top: 9px">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.label_contas}:" />
			</br:brPanelGroup>

			<br:brPanelGrid style="margin-top: 9px" />

			<br:brPanelGrid cellpadding="0" cellspacing="0" width="100%"
				style="margin-top: 9px">
				<app:scrollableDataTable id="tabelaContas"
					value="#{manterContratoBean.listaContas}"
					var="result" width="100%" rowIndexVar="parameterKey4">
					<app:scrollableColumn>
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_nome_razao_social}" />
						</f:facet>
						<br:brOutputText value="#{result.cdNomeParticipanteConta}" />
					</app:scrollableColumn>
					<app:scrollableColumn>
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_cpf_cnpj}" />
						</f:facet>
						<br:brOutputText value="#{result.cdCpfCnpjConta}" />
					</app:scrollableColumn>
					<app:scrollableColumn>
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_agencia}" />
						</f:facet>
						<br:brOutputText value="#{result.agenciaFormatada}" />
					</app:scrollableColumn>
					<app:scrollableColumn>
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_conta}" />
						</f:facet>
						<br:brOutputText value="#{result.contaFormatada}" />
					</app:scrollableColumn>
					<app:scrollableColumn>
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_tipo_conta}" />
						</f:facet>
						<br:brOutputText value="#{result.dsTipoConta}" />
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>


		<br:brPanelGrid cellpadding="0" cellspacing="0" width="100%" id="gridServicosModalidades">
			<br:brPanelGrid columns="2" cellpadding="" cellspacing="0" width="100%">
				<br:brPanelGrid style="margin-top: 9px; width: 450px">
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_servicos}:" />
				</br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 9px; width: 450px">
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_modalidades}:" />
				</br:brPanelGrid>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%" style="margin-top: 20px" 
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.cdServicoFornecedor}">
				<br:brPanelGroup style="vertical-align: top; height: 100%; width: 450px">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.cdServicoFornecedor}" />
				</br:brPanelGroup>

				<br:brPanelGroup style="width: 450px">
					<h:dataTable
						id="tabelaServicosModPagamentoDeFornecedoresModalidades"
						var="result"
						value="#{manterContratoBean.saidaSegundoContrato.listModFornecedores}"
						width="100%">
						<h:column>
							<br:brPanelGrid columns="2"  rendered="#{not empty result.dsModalidadeFornecedor}">
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								<h:outputText styleClass="HtmlOutputTextBradesco"
									value="#{result.dsModalidadeFornecedor}" />
							</br:brPanelGrid>
						</h:column>
					</h:dataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 50px"
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.cdServicoTributos}">
				<br:brPanelGroup
					style="vertical-align: top; height: 100%; width: 450px;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.cdServicoTributos}" />
				</br:brPanelGroup>

				<br:brPanelGroup style="width: 450px">
					<h:dataTable id="tabelaServicosModPagamentoDeTributoModalidades"
						var="result"
						value="#{manterContratoBean.saidaSegundoContrato.listaModTributos}"
						width="100%">
						<h:column>
							<br:brPanelGrid columns="2"  rendered="#{not empty result.cdModalidadeTributos}"> 
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<h:outputText styleClass="HtmlOutputTextBradesco"
									value="#{result.cdModalidadeTributos}" />
							</br:brPanelGrid>
						</h:column>
					</h:dataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 50px"
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.cdServicoSalarial}">
				<br:brPanelGroup
					style="vertical-align: top; height: 100%; width: 450px;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.cdServicoSalarial}" />
				</br:brPanelGroup>

				<br:brPanelGroup style="width: 450px">
					<h:dataTable id="tabelaServicosModPagamentoDeSalariosModalidades"
						var="result"
						value="#{manterContratoBean.saidaSegundoContrato.listaModSalario}"
						width="100%">
						<h:column>
							<br:brPanelGrid columns="2"  rendered="#{not empty result.cdModalidadeSalarial}"> 
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<h:outputText styleClass="HtmlOutputTextBradesco"
									value="#{result.cdModalidadeSalarial}" />
							</br:brPanelGrid>
						</h:column>
					</h:dataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 50px"
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.cdServicoBeneficio}">
				<br:brPanelGroup
					style="vertical-align: top; height: 100%; width: 450px;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.cdServicoBeneficio}" />
				</br:brPanelGroup>

				<br:brPanelGroup style="width: 450px">
					<h:dataTable id="tabelaServicosModPagamentoDeBeneficiosModalidades"
						var="result"
						value="#{manterContratoBean.saidaSegundoContrato.listaModBeneficio}"
						width="100%">
						<h:column>
							<br:brPanelGrid columns="2"  rendered="#{not empty result.cdModalidadeBeneficio}"> 
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<h:outputText styleClass="HtmlOutputTextBradesco"
									value="#{result.cdModalidadeBeneficio}" />
							</br:brPanelGrid>
						</h:column>
					</h:dataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>


			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 50px"
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.dsServicoRecadastro}">
				<br:brPanelGroup
					style="vertical-align: top; height: 100%; width: 450px;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.dsServicoRecadastro}" />
				</br:brPanelGroup>

				<br:brPanelGroup style="width: 450px">
					<h:dataTable id="tabelaServicosModRecadastroModalidades"
						var="result"
						value="#{manterContratoBean.saidaSegundoContrato.listaModRecadastro}"
						width="100%">
						<h:column>
							<br:brPanelGrid columns="2"  rendered="#{not empty result.cdModalidadeRecadastro}"> 
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<h:outputText styleClass="HtmlOutputTextBradesco"
									value="#{result.cdModalidadeRecadastro}" />
							</br:brPanelGrid>
						</h:column>
					</h:dataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 50px"
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.cdIndicadorAvisoPagador}">
				<br:brPanelGroup
					style="vertical-align: top; height: 100%; width: 450px;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.cdIndicadorAvisoPagador}" />
				</br:brPanelGroup>
				
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 50px"
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.cdIndicadorAvisoFavorecido}">
				<br:brPanelGroup
					style="vertical-align: top; height: 100%; width: 450px;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.cdIndicadorAvisoFavorecido}" />
				</br:brPanelGroup>
				
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 50px"
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.cdIndicadorComplementoSalarial}">
				<br:brPanelGroup
					style="vertical-align: top; height: 100%; width: 450px;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.cdIndicadorComplementoSalarial}" />
				</br:brPanelGroup>
				
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 50px"
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.cdIndicadorComplementoPagador}">
				<br:brPanelGroup
					style="vertical-align: top; height: 100%; width: 450px;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.cdIndicadorComplementoPagador}" />
				</br:brPanelGroup>
				
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 50px"
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.cdIndicadorComplementoFavorecido}">
				<br:brPanelGroup
					style="vertical-align: top; height: 100%; width: 450px;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.cdIndicadorComplementoFavorecido}" />
				</br:brPanelGroup>
				
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 50px"
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.cdIndicadorComplementoDivergente}">
				<br:brPanelGroup
					style="vertical-align: top; height: 100%; width: 450px;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.cdIndicadorComplementoDivergente}" />
				</br:brPanelGroup>
				
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 50px"
				rendered="#{not empty manterContratoBean.saidaSegundoContrato.cdIndcadorCadastroFavorecido}">
				<br:brPanelGroup
					style="vertical-align: top; height: 100%; width: 450px;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<h:outputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{manterContratoBean.saidaSegundoContrato.cdIndcadorCadastroFavorecido}" />
				</br:brPanelGroup>
				
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

	
		</br:brPanelGrid>
		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%" style="margin-top: 9px">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.label_configuracoes_layout}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid style="margin-top: 9px" />

		<br:brPanelGrid cellpadding="0" cellspacing="0" width="100%"
			style="margin-top: 9px">
			<app:scrollableDataTable id="tabelaLayouts"
				value="#{manterContratoBean.meiosTransmissaoBean.listaMeioTransmissao.ocorrencia}"
				var="result"  width="100%" rowIndexVar="parameterKey4">
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.PGIC0010_label_produto}" />
					</f:facet>
					<br:brOutputText value="#{result.dsProdutoServico}" />
				</app:scrollableColumn>
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_layout}" />
					</f:facet>
					<br:brOutputText value="#{result.dsTipoLayoutArquivo}" />
				</app:scrollableColumn>
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_perfil_transmissao}" />
					</f:facet>
					<br:brOutputText value="#{result.cdPerfilTrocaArquivo}" />
				</app:scrollableColumn>
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_aplicativo_formatacao}" />
					</f:facet>
					<br:brOutputText value="#{result.dsAplicacaoTransmicaoPagamento}" />
				</app:scrollableColumn>
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_meio_transmissao}" />
					</f:facet>
					<br:brOutputText value="#{result.dsMeioPrincRemss}" />
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGrid>
	
		<f:verbatim >
			<hr class="lin">
		</f:verbatim>
				<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%" style="margin-top: 9px">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.label_configuracoes_servicos_modalidades_contratadas}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid cellpadding="0" cellspacing="0"
			style="margin-top: 9px" width="100%">


			<!-- Servico Pagamentos Fornecedores -->

			<t:dataList id="dataList" var="mensagem"
				value="#{manterContratoBean.listaPagamentosFornec}">

				<br:brPanelGrid>
					<br:brPanelGrid style="margin-top: 20px"
						rendered="#{mensagem.dsServicoTipoServico != 'N' && not empty mensagem.dsServicoTipoServico}">
						<br:brPanelGroup>
							<br:brOutputText
								value="#{msgs.label_servico}: #{mensagem.dsServicoTipoServico}"
								styleClass="HtmlOutputTextTitleBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" 
						width="100%" style="margin-top: 9px"
						rendered="#{mensagem.dsServicoTipoServico != 'N' && not empty mensagem.dsServicoTipoServico}">
						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_formaEnvioPagamento}:" />
								<br:brOutputText value="#{mensagem.dsFormaEnvioPagamento}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_forma_autorizacao_pagamentos}:" />
								<br:brOutputText
									value="#{mensagem.dsServicoFormaAutorizacaoPagamento}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_possui_floating}:" />
								<br:brOutputText value="#{mensagem.cdServicoFloating}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_tipo_data_controle_floating}:" />
								<br:brOutputText value="#{mensagem.dsServicoTipoDataFloat}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px" style="height:100%;">
							<br:brPanelGroup style="height:100%;">
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />
								<br:brOutputText value="#{msgs.label_utiliza_lista_debito}:" />
								<br:brOutputText value="#{mensagem.dsServicoListaDebito}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_tipo_consolidacao_pagamentos_extrato}:" />
								<br:brOutputText value="#{mensagem.cdServicoTipoConsolidado}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

					</br:brPanelGrid>
				</br:brPanelGrid>
			</t:dataList>

			<!-- Modalidade Pagamentos Fornecedores -->

			<t:dataList id="dataListModalidadePagamentoFornecedores"
				var="mensagem" value="#{manterContratoBean.listaPagamentosFornec}">
				<br:brPanelGrid>
					<br:brPanelGrid style="margin-top: 20px"
						rendered="#{mensagem.dsCretipoServico != 'N' && not empty mensagem.dsCretipoServico}">
						<br:brPanelGroup>
							<br:brOutputText value="#{mensagem.dsCretipoServico}"
								styleClass="HtmlOutputTextTitleBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						width="100%" style="margin-top: 9px"
						rendered="#{mensagem.dsCretipoServico != 'N' && not empty mensagem.dsCretipoServico}">
						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_tipo_de_consulta_de_saldo}:" />
								<br:brOutputText value="#{mensagem.dsCreTipoConsultaSaldo}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.detServicos_tipoProcessamento}:" />
								<br:brOutputText value="#{mensagem.dsCreTipoProcessamento}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_pgto_feriado_nacional_fim_semana}:" />
								<br:brOutputText value="#{mensagem.dsCreTipoTratamentoFeriado}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_pgto_feriado_local}:" />
								<br:brOutputText value="#{mensagem.cdCreCferiLoc}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_tipoRejeicaoAgendamento}:" />
								<br:brOutputText value="#{mensagem.dsCreTipoRejeicaoAgendada}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_tipoRejeicaoEfetivacao}:" />
								<br:brOutputText value="#{mensagem.dsCreTipoRejeicaoEfetivacao}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.detServicos_prioridadeDebito}:" />
								<br:brOutputText value="#{mensagem.dsCrePrioridadeDebito}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:" />
								<br:brOutputText
									value="#{mensagem.cdCreUtilizacaoCadastroFavorecido}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_valor_limite_agendamento_individual}:" />
								<br:brOutputText value="#{mensagem.vlCreLimiteIndividual}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_valor_limite_agendamento}:" />
								<br:brOutputText value="#{mensagem.vlCreLimiteDiario}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:" />
								<br:brOutputText value="#{mensagem.qtCreDiaRepique}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_quantidade_dias_floating}:" />
								<br:brOutputText value="#{mensagem.qtCreDiaFloating}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_tipo_tratamento_conta_debito_transferida}:" />
								<br:brOutputText value="#{mensagem.cdCreContratoTransf}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_tipo_consistencia_cpf_cnpj_favorecido}:" />
								<br:brOutputText
									value="#{mensagem.dsCreTipoConsistenciaFavorecido}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_gera_lancamento_programado}:" />
								<br:brOutputText value="#{mensagem.cdLctoPgmd}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</t:dataList>


			<!-- Modalidade Pagamentos Fornecedores Titulos -->

			<t:dataList id="dataListModalidadePagamentoFornecedoresTitulos"
				var="mensagem" value="#{manterContratoBean.listaTitulos}">
				<br:brPanelGrid>
					<br:brPanelGrid style="margin-top: 20px"
						rendered="#{mensagem.dsFtiTipoTitulo != 'N' && not empty mensagem.dsFtiTipoTitulo}">
						<br:brPanelGroup>
							<br:brOutputText value="#{mensagem.dsFtiTipoTitulo}"
								styleClass="HtmlOutputTextTitleBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						width="100%" style="margin-top: 9px"
						rendered="#{mensagem.dsFtiTipoTitulo != 'N' && not empty mensagem.dsFtiTipoTitulo}">
						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_tipo_de_consulta_de_saldo}:" />
								<br:brOutputText value="#{mensagem.dsFtiTipoConsultaSaldo}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.detServicos_tipoProcessamento}:" />
								<br:brOutputText value="#{mensagem.dsFtiTipoProcessamento}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_pgto_feriado_nacional_fim_semana}:" />
								<br:brOutputText value="#{mensagem.dsFtiTipoTratamentoFeriado}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_pgto_feriado_local}:" />
								<br:brOutputText value="#{mensagem.ftiCferiLoc}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_tipoRejeicaoAgendamento}:" />
								<br:brOutputText
									value="#{mensagem.dsFtiTipoRejeicaoAgendamento}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_tipoRejeicaoEfetivacao}:" />
								<br:brOutputText value="#{mensagem.dsFtiTipoRejeicaoEfetivacao}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.detServicos_prioridadeDebito}:" />
								<br:brOutputText value="#{mensagem.dsFtiPrioridadeDebito}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:" />
								<br:brOutputText value="#{mensagem.cdFtiUtilizadoFavorecido}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_valor_limite_agendamento_individual}:" />
								<br:brOutputText value="#{mensagem.vlFtiLimiteIndividual}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_valor_limite_agendamento}:" />
								<br:brOutputText value="#{mensagem.vlFtiLimiteDiario}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>


						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:" />
								<br:brOutputText value="#{mensagem.qtFtiDiaRepique}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_quantidade_dias_floating}:" />
								<br:brOutputText value="#{mensagem.qtFtiDiaFloating}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px"
							rendered="#{mensagem.exibeDsFtiTipoTitulo != 'S'}">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.tipo_consulta_saldo_titulo_valor_superior}:" />
								<br:brOutputText value="#{mensagem.dsTIpoConsultaSaldoSuperior}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_permite_pagar_menor}:" />
								<br:brOutputText value="#{mensagem.ftiPagamentoMenor}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_permite_pagar_vencido}:" />
								<br:brOutputText value="#{mensagem.ftiPagamentoVencido}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_quantidade_dias_para_pagar_vencimento}:" />
								<br:brOutputText value="#{mensagem.ftiQuantidadeMaxVencido}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</t:dataList>


			<!-- Modalidade Pagamentos Fornecedores outras modularidades -->

			<t:dataList
				id="dataListModalidadePagamentoFornecedoresOutrasModularidades"
				var="mensagem" value="#{manterContratoBean.listaFornModularidades}">
				<br:brPanelGrid>
					<br:brPanelGrid style="margin-top: 20px"
						rendered="#{mensagem.dsModTipoServico != 'N' &&  not empty mensagem.dsModTipoServico}">
						<br:brPanelGroup>
							<br:brOutputText value="#{mensagem.dsModTipoServico}"
								styleClass="HtmlOutputTextTitleBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						width="100%" style="margin-top: 9px"
						rendered="#{mensagem.dsModTipoServico != 'N' &&  not empty mensagem.dsModTipoServico}">
						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_tipo_de_consulta_de_saldo}:" />
								<br:brOutputText value="#{mensagem.dsModTipoConsultaSaldo}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.detServicos_tipoProcessamento}:" />
								<br:brOutputText value="#{mensagem.dsModTipoProcessamento}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_pgto_feriado_nacional_fim_semana}:" />
								<br:brOutputText value="#{mensagem.dsModTipoTratamentoFeriado}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_pgto_feriado_local}:" />
								<br:brOutputText value="#{mensagem.dsModalidadeCferiLoc}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_tipoRejeicaoAgendamento}:" />
								<br:brOutputText value="#{mensagem.dsModTipoRejeicaoAgendado}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_tipoRejeicaoEfetivacao}:" />
								<br:brOutputText value="#{mensagem.dsModTipoRejeicaoEfetivacao}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.detServicos_prioridadeDebito}:" />
								<br:brOutputText value="#{mensagem.dsModPrioridadeDebito}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:" />
								<br:brOutputText
									value="#{mensagem.cdModUtilizacaoCadastroFavorecido}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_valor_limite_agendamento_individual}:" />
								<br:brOutputText value="#{mensagem.vlModLimiteIndividual}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_valor_limite_agendamento}:" />
								<br:brOutputText value="#{mensagem.vlModLimiteDiario}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:" />
								<br:brOutputText value="#{mensagem.qtModDiaRepique}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_quantidade_dias_floating}:" />
								<br:brOutputText value="#{mensagem.qtModDiaFloating}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

					</br:brPanelGrid>
				</br:brPanelGrid>
			</t:dataList>


			<!-- Modalidade Pagamentos Fornecedores Via Ordem de Pagamento -->

			<t:dataList
				id="dataListModalidadePagamentoFornecedoresViaOrdemPagamento"
				var="mensagem" value="#{manterContratoBean.listaPagamentosFornec}">
				<br:brPanelGrid>
					<br:brPanelGrid style="margin-top: 20px"
						rendered="#{mensagem.dsOpgTipoServico != 'N' && not empty mensagem.dsOpgTipoServico}">
						<br:brPanelGroup>
							<br:brOutputText value="#{mensagem.dsOpgTipoServico}"
								styleClass="HtmlOutputTextTitleBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						width="100%" style="margin-top: 9px"
						rendered="#{mensagem.dsOpgTipoServico != 'N' && not empty mensagem.dsOpgTipoServico}">
						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_tipo_de_consulta_de_saldo}:" />
								<br:brOutputText value="#{mensagem.dsOpgTipoConsultaSaldo}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.detServicos_tipoProcessamento}:" />
								<br:brOutputText value="#{mensagem.dsOpgTipoProcessamento}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_pgto_feriado_nacional_fim_semana}:" />
								<br:brOutputText value="#{mensagem.dsOpgTipoTratamentoFeriado}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_pgto_feriado_local}:" />
								<br:brOutputText value="#{mensagem.cdFeriLoc}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_tipoRejeicaoAgendamento}:" />
								<br:brOutputText value="#{mensagem.dsOpgTiporejeicaoAgendado}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_tipoRejeicaoEfetivacao}:" />
								<br:brOutputText value="#{mensagem.dsOpgTipoRejeicaoEfetivacao}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.detServicos_prioridadeDebito}:" />
								<br:brOutputText value="#{mensagem.dsOpgPrioridadeDebito}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:" />
								<br:brOutputText
									value="#{mensagem.cdOpgUtilizacaoCadastroFavorecido}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_valor_limite_agendamento_individual}:" />
								<br:brOutputText value="#{mensagem.vlOpgLimiteIndividual}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_valor_limite_agendamento}:" />
								<br:brOutputText value="#{mensagem.vlOpgLimiteDiario}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px" style="height:100%;">
							<br:brPanelGroup style="height:100%;">  
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:" />
								<br:brOutputText value="#{mensagem.qtOpgDiaRepique}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>
						
						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_quantidade_dias_floating}:" />
								<br:brOutputText value="#{mensagem.qtOpgDiaFloating}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px" style="height:100%;">
							<br:brPanelGroup style="height:100%;">  
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_ocorrencia_debito}:" />
								<br:brOutputText value="#{mensagem.dsOpgOcorrenciasDebito}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_local_emissao}:" />
								<br:brOutputText value="#{mensagem.oPgEmissao}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_quantidade_dias_expiracao}:" />
								<br:brOutputText value="#{mensagem.qtOpgDiaExpiracao}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

					</br:brPanelGrid>
				</br:brPanelGrid>
			</t:dataList>


			<!-- Modalidade Rastreamento de T�tulos -->

			<t:dataList id="dataListRastreamentoTitulos" var="mensagem"
				value="#{manterContratoBean.listaPagamentosFornec}">
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{not empty mensagem.dsFraTipoRastreabilidade}">
					<br:brPanelGroup>
						<br:brOutputText
							value="#{msgs.manterServicoRelacionado_combo_rastreamento_boletos}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{not empty mensagem.dsFraTipoRastreabilidade}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_tipo_rastreamento_titulos}:" />
							<br:brOutputText value="#{mensagem.dsFraTipoRastreabilidade}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_rastrear_sacado_agregado}:" />
							<br:brOutputText value="#{mensagem.dsFraRastreabilidadeTerceiro}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_rastrear_notas_fiscais}:" />
							<br:brOutputText value="#{mensagem.dsFraRastreabilidadeNota}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_Exige_Identificacao_Filial_Autorizacao}:" />
							<br:brOutputText value="#{mensagem.dsFraCaptacaoTitulo}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.confModalidades_rastreamentoTitulos_cliente_pagador}:" />
							<br:brOutputText value="#{mensagem.dsFraAgendaCliente}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.confModalidades_rastreamentoTitulos_agendarTituloFilial}:" />
							<br:brOutputText value="#{mensagem.dsFraAgendaFilial}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_bloquear_emissao_papeleta}:" />
							<br:brOutputText value="#{mensagem.dsFraAgendaFilial}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_data_inicio_bloqueio_papeleta}:" />
							<br:brOutputText value="#{mensagem.dtFraInicioBloqueio}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_data_inicio_rastreamento}:" />
							<br:brOutputText value="#{mensagem.dtFraInicioRastreabilidade}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</t:dataList>


			<!-- Servico Pagamento de Tributos -->

			<t:dataList id="dataListServicoPagamentoTributos" var="mensagem"
				value="#{manterContratoBean.listaPagamentosTri}">
				<br:brPanelGrid>
					<br:brPanelGrid style="margin-top: 20px"
						rendered="#{not empty mensagem.dsServicoTipoServico}">
						<br:brPanelGroup>
							<br:brOutputText
								value="#{msgs.label_servico}: #{mensagem.dsServicoTipoServico}"
								styleClass="HtmlOutputTextTitleBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						width="100%" style="margin-top: 9px"
						rendered="#{not empty mensagem.dsFormaEnvioPagamento}">
						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.detServicos_formaEnvioPagamento}:" />
								<br:brOutputText value="#{mensagem.dsFormaEnvioPagamento}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_forma_autorizacao_pagamentos}:" />
								<br:brOutputText
									value="#{mensagem.dsServicoFormaAutorizacaoPagamento}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px" style="height:100%;">
							<br:brPanelGroup style="height:100%;"> 
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								<br:brOutputText value="#{msgs.label_utiliza_lista_debito}:" />
								<br:brOutputText value="#{mensagem.dsServicoListaDebito}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>

						<br:brPanelGrid width="450px">
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg"
									styleClass="HtmlBullet" />
								<br:brOutputText
									value="#{msgs.label_tipo_consolidacao_pagamentos_extrato}:" />
								<br:brOutputText value="#{mensagem.cdServicoTipoConsolidado}"
									styleClass="HtmlOutputTextRespostaBradesco" />
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</t:dataList>
		</br:brPanelGrid>

		<!-- Modalidade Pagamentos de Tributos -->

		<t:dataList id="dataListModalidadePagamentosTributos" var="mensagem"
			value="#{manterContratoBean.listaTri}" >

			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsTriTipoTributo != 'N' && not empty mensagem.dsTriTipoTributo}">
					<br:brPanelGroup>
						<br:brOutputText value="#{mensagem.dsTriTipoTributo}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{not empty mensagem.dsTriTipoConsultaSaldo}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_tipo_de_consulta_de_saldo}:" />
							<br:brOutputText value="#{mensagem.dsTriTipoConsultaSaldo}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_tipoProcessamento}:" />
							<br:brOutputText value="#{mensagem.dsTriTipoProcessamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_pgto_feriado_nacional_fim_semana}:" />
							<br:brOutputText value="#{mensagem.dsTriTipoTratamentoFeriado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_pgto_feriado_local}:" />
							<br:brOutputText value="#{mensagem.cdTriCferiLoc}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoAgendamento}:" />
							<br:brOutputText value="#{mensagem.dsTriTipoRejeicaoAgendado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoEfetivacao}:" />
							<br:brOutputText value="#{mensagem.dsTriTipoRejeicaoEfetivacao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_prioridadeDebito}:" />
							<br:brOutputText value="#{mensagem.dsTriPrioridadeDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:" />
							<br:brOutputText
								value="#{mensagem.cdTriUtilizacaoCadastroFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_valor_limite_agendamento_individual}:" />
							<br:brOutputText value="#{mensagem.vlTriLimiteIndividual}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_valor_limite_agendamento}:" />
							<br:brOutputText value="#{mensagem.vlTriLimiteDiario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:" />
							<br:brOutputText value="#{mensagem.qtTriDiaRepique}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>

		<!-- Servico Pagamentos de Salarios -->


		<t:dataList id="dataListServicoPagamentosSalarios" var="mensagem"
			value="#{manterContratoBean.listaPagamentosSalarios}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsServicoTipoServico != 'N' && not empty mensagem.dsServicoTipoServico}">
					<br:brPanelGroup>
						<br:brOutputText
							value="#{msgs.label_servico}: #{mensagem.dsServicoTipoServico}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>


				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsServicoTipoServico != 'N' && not empty mensagem.dsServicoTipoServico}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_formaEnvioPagamento}:" />
							<br:brOutputText value="#{mensagem.dsFormaEnvioPagamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_forma_autorizacao_pagamentos}:" />
							<br:brOutputText
								value="#{mensagem.dsServicoFormaAutorizacaoPagamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_possui_floating}:" />
							<br:brOutputText value="#{mensagem.cdServicoFloating}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_data_controle_floating}:" />
							<br:brOutputText value="#{mensagem.dsServicoTipoDataFloat}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px" style="height:100%;">
						<br:brPanelGroup style="height:100%;"> 
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_utiliza_lista_debito}:" />
							<br:brOutputText value="#{mensagem.dsServicoListaDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_consolidacao_pagamentos_extrato}:" />
							<br:brOutputText value="#{mensagem.cdServicoTipoConsolidado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>



		<!-- Modalidade Pagamentos de Salarios -->

		<t:dataList id="dataListModalidadePagamentosSalarios" var="mensagem"
			value="#{manterContratoBean.listaPagamentosSalarios}">
			<br:brPanelGrid >
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsCretipoServico != 'N' && not empty mensagem.dsCretipoServico}">
					<br:brPanelGroup>
						<br:brOutputText value="#{mensagem.dsCretipoServico}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsCretipoServico != 'N' && not empty mensagem.dsCretipoServico}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_tipo_de_consulta_de_saldo}:" />
							<br:brOutputText value="#{mensagem.dsCreTipoConsultaSaldo}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_tipoProcessamento}:" />
							<br:brOutputText value="#{mensagem.dsCreTipoProcessamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_pgto_feriado_nacional_fim_semana}:" />
							<br:brOutputText value="#{mensagem.dsCreTipoTratamentoFeriado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_pgto_feriado_local}:" />
							<br:brOutputText value="#{mensagem.cdCreCferiLoc}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoAgendamento}:" />
							<br:brOutputText value="#{mensagem.dsCreTipoRejeicaoAgendada}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoEfetivacao}:" />
							<br:brOutputText value="#{mensagem.dsCreTipoRejeicaoEfetivacao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_prioridadeDebito}:" />
							<br:brOutputText value="#{mensagem.dsCrePrioridadeDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:" />
							<br:brOutputText
								value="#{mensagem.cdCreUtilizacaoCadastroFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_valor_limite_agendamento_individual}:" />
							<br:brOutputText value="#{mensagem.vlCreLimiteIndividual}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_valor_limite_agendamento}:" />
							<br:brOutputText value="#{mensagem.vlCreLimiteDiario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:" />
							<br:brOutputText value="#{mensagem.qtCreDiaRepique}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_dias_floating}:" />
							<br:brOutputText value="#{mensagem.qtCreDiaFloating}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_tratamento_conta_debito_transferida}:" />
							<br:brOutputText value="#{mensagem.cdCreContratoTransf}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_consistencia_cpf_cnpj_favorecido}:" />
							<br:brOutputText
								value="#{mensagem.dsCreTipoConsistenciaFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_gera_lancamento_programado}:" />
							<br:brOutputText value="#{mensagem.cdLctoPgmd}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>


		<!-- Modalidade Pagamentos de Salarios Modularidades-->

		<t:dataList id="dataListModalidadePagamentosSalariosModularidades"
			var="mensagem" value="#{manterContratoBean.listaSalariosMod}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsModTipoServico != 'N' && not empty mensagem.dsModTipoServico}">
					<br:brPanelGroup>
						<br:brOutputText value="#{mensagem.dsModTipoServico}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsModTipoServico != 'N' && not empty mensagem.dsModTipoServico}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_tipo_de_consulta_de_saldo}:" />
							<br:brOutputText value="#{mensagem.dsModTipoConsultaSaldo}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_tipoProcessamento}:" />
							<br:brOutputText value="#{mensagem.dsModTipoProcessamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_pgto_feriado_nacional_fim_semana}:" />
							<br:brOutputText value="#{mensagem.dsModTipoTratamentoFeriado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_pgto_feriado_local}:" />
							<br:brOutputText value="#{mensagem.dsModalidadeCferiLoc}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoAgendamento}:" />
							<br:brOutputText value="#{mensagem.dsModTipoRejeicaoAgendado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoEfetivacao}:" />
							<br:brOutputText value="#{mensagem.dsModTipoRejeicaoEfetivacao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_prioridadeDebito}:" />
							<br:brOutputText value="#{mensagem.dsModPrioridadeDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:" />
							<br:brOutputText
								value="#{mensagem.cdModUtilizacaoCadastroFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_valor_limite_agendamento_individual}:" />
							<br:brOutputText value="#{mensagem.vlModLimiteIndividual}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_valor_limite_agendamento}:" />
							<br:brOutputText value="#{mensagem.vlModLimiteDiario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:" />
							<br:brOutputText value="#{mensagem.qtModDiaRepique}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_dias_floating}:" />
							<br:brOutputText value="#{mensagem.qtModDiaFloating}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>


		<!-- Modalidade Pagamentos de Salarios Dados Ordem Pagamento-->

		<t:dataList
			id="dataListModalidadePagamentosSalariosDadosOrdemPagamento"
			var="mensagem" value="#{manterContratoBean.listaPagamentosSalarios}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsOpgTipoServico != 'N' && not empty mensagem.dsOpgTipoServico}">
					<br:brPanelGroup>
						<br:brOutputText value="#{mensagem.dsOpgTipoServico}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsOpgTipoServico != 'N' && not empty mensagem.dsOpgTipoServico}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_tipo_de_consulta_de_saldo}:" />
							<br:brOutputText value="#{mensagem.dsOpgTipoConsultaSaldo}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_tipoProcessamento}:" />
							<br:brOutputText value="#{mensagem.dsOpgTipoProcessamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_pgto_feriado_nacional_fim_semana}:" />
							<br:brOutputText value="#{mensagem.dsOpgTipoTratamentoFeriado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_pgto_feriado_local}:" />
							<br:brOutputText value="#{mensagem.cdFeriLoc}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoAgendamento}:" />
							<br:brOutputText value="#{mensagem.dsOpgTiporejeicaoAgendado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoEfetivacao}:" />
							<br:brOutputText value="#{mensagem.dsOpgTipoRejeicaoEfetivacao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_prioridadeDebito}:" />
							<br:brOutputText value="#{mensagem.dsOpgPrioridadeDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:" />
							<br:brOutputText
								value="#{mensagem.cdOpgUtilizacaoCadastroFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_valor_limite_agendamento_individual}:" />
							<br:brOutputText value="#{mensagem.vlOpgLimiteIndividual}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_valor_limite_agendamento}:" />
							<br:brOutputText value="#{mensagem.vlOpgLimiteDiario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGrid width="450px" style="height:100%;">
							<br:brPanelGroup style="height:100%;">  
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:" />
							<br:brOutputText value="#{mensagem.qtOpgDiaRepique}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px" style="height:100%;">
							<br:brPanelGroup style="height:100%;">  
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_dias_floating}:" />
							<br:brOutputText value="#{mensagem.qtOpgDiaFloating}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
					
					
					<br:brPanelGrid width="450px" style="height:100%;">
							<br:brPanelGroup style="height:100%;">  
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_ocorrencia_debito}:" />
							<br:brOutputText value="#{mensagem.dsOpgOcorrenciasDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_local_emissao}:" />
							<br:brOutputText value="#{mensagem.oPgEmissao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_dias_expiracao}:" />
							<br:brOutputText value="#{mensagem.qtOpgDiaExpiracao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>
		<!-- Servicos Pagamentos de Beneficios -->


		<t:dataList id="dataListServicoPagamentosBeneficios" var="mensagem"
			value="#{manterContratoBean.listaPagamentosBeneficios}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsCretipoServico != 'N' && not empty mensagem.dsCretipoServico}">
					<br:brPanelGroup>
						<br:brOutputText
							value="#{msgs.label_servico}: #{mensagem.dsServicoTipoServico}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>


				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsServicoTipoServico != 'N' && not empty mensagem.dsServicoTipoServico}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_formaEnvioPagamento}:" />
							<br:brOutputText value="#{mensagem.dsFormaEnvioPagamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_forma_autorizacao_pagamentos}:" />
							<br:brOutputText
								value="#{mensagem.dsServicoFormaAutorizacaoPagamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_possui_floating}:" />
							<br:brOutputText value="#{mensagem.cdServicoFloating}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_data_controle_floating}:" />
							<br:brOutputText value="#{mensagem.dsServicoTipoDataFloat}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px" style="height:100%;">
						<br:brPanelGroup style="height:100%;"> 
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_utiliza_lista_debito}:" />
							<br:brOutputText value="#{mensagem.dsServicoListaDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_consolidacao_pagamentos_extrato}:" />
							<br:brOutputText value="#{mensagem.cdServicoTipoConsolidado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>

		<!-- Modalidade Pagamentos de Salarios Beneficios -->

		<t:dataList id="dataListModalidadePagamentosBeneficios" var="mensagem"
			value="#{manterContratoBean.listaPagamentosBeneficios}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsCretipoServico != 'N' && not empty mensagem.dsCretipoServico}">
					<br:brPanelGroup>
						<br:brOutputText value="#{mensagem.dsCretipoServico}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsCretipoServico != 'N' && not empty mensagem.dsCretipoServico}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_tipo_de_consulta_de_saldo}:" />
							<br:brOutputText value="#{mensagem.dsCreTipoConsultaSaldo}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_tipoProcessamento}:" />
							<br:brOutputText value="#{mensagem.dsCreTipoProcessamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_pgto_feriado_nacional_fim_semana}:" />
							<br:brOutputText value="#{mensagem.dsCreTipoTratamentoFeriado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_pgto_feriado_local}:" />
							<br:brOutputText value="#{mensagem.cdCreCferiLoc}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoAgendamento}:" />
							<br:brOutputText value="#{mensagem.dsCreTipoRejeicaoAgendada}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoEfetivacao}:" />
							<br:brOutputText value="#{mensagem.dsCreTipoRejeicaoEfetivacao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_prioridadeDebito}:" />
							<br:brOutputText value="#{mensagem.dsCrePrioridadeDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:" />
							<br:brOutputText
								value="#{mensagem.cdCreUtilizacaoCadastroFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_valor_limite_agendamento_individual}:" />
							<br:brOutputText value="#{mensagem.vlCreLimiteIndividual}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_valor_limite_agendamento}:" />
							<br:brOutputText value="#{mensagem.vlCreLimiteDiario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:" />
							<br:brOutputText value="#{mensagem.qtCreDiaRepique}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_dias_floating}:" />
							<br:brOutputText value="#{mensagem.qtCreDiaFloating}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_tratamento_conta_debito_transferida}:" />
							<br:brOutputText value="#{mensagem.cdCreContratoTransf}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_consistencia_cpf_cnpj_favorecido}:" />
							<br:brOutputText
								value="#{mensagem.dsCreTipoConsistenciaFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_gera_lancamento_programado}:" />
							<br:brOutputText value="#{mensagem.cdLctoPgmd}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>

		<!-- Servicos Pagamentos de Beneficios Modularidades -->

		<t:dataList id="dataListServicoPagamentosBeneficiosModularidades"
			var="mensagem"
			value="#{manterContratoBean.listaBeneficiosModularidades}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsServicoTipoServico != 'N' && not empty mensagem.dsServicoTipoServico}">
					<br:brPanelGroup>
						<br:brOutputText
							value="#{msgs.label_servico}: #{mensagem.dsServicoTipoServico}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsServicoTipoServico != 'N' && not empty mensagem.dsServicoTipoServico}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_formaEnvioPagamento}:" />
							<br:brOutputText value="#{mensagem.dsFormaEnvioPagamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_forma_autorizacao_pagamentos}:" />
							<br:brOutputText
								value="#{mensagem.dsServicoFormaAutorizacaoPagamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_possui_floating}:" />
							<br:brOutputText value="#{mensagem.cdServicoFloating}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_data_controle_floating}:" />
							<br:brOutputText value="#{mensagem.dsServicoTipoDataFloat}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px" style="height:100%;">
						<br:brPanelGroup style="height:100%;"> 
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_utiliza_lista_debito}:" />
							<br:brOutputText value="#{mensagem.dsServicoListaDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_consolidacao_pagamentos_extrato}:" />
							<br:brOutputText value="#{mensagem.cdServicoTipoConsolidado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>


		<!-- Modalidade Pagamentos Beneficios Modularidades -->

		<t:dataList id="dataListModalidadePagamentosBeneficiosModularidades"
			var="mensagem"
			value="#{manterContratoBean.listaBeneficiosModularidades}">

			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsModTipoServico != 'N' && not empty mensagem.dsModTipoServico}">
					<br:brPanelGroup>
						<br:brOutputText value="#{mensagem.dsModTipoServico}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsModTipoServico != 'N' && not empty mensagem.dsModTipoServico}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_tipo_de_consulta_de_saldo}:" />
							<br:brOutputText value="#{mensagem.dsModTipoConsultaSaldo}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_tipoProcessamento}:" />
							<br:brOutputText value="#{mensagem.dsModTipoProcessamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_pgto_feriado_nacional_fim_semana}:" />
							<br:brOutputText value="#{mensagem.dsModTipoTratamentoFeriado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_pgto_feriado_local}:" />
							<br:brOutputText value="#{mensagem.dsModalidadeCferiLoc}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoAgendamento}:" />
							<br:brOutputText value="#{mensagem.dsModTipoRejeicaoAgendado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoEfetivacao}:" />
							<br:brOutputText value="#{mensagem.dsModTipoRejeicaoEfetivacao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_prioridadeDebito}:" />
							<br:brOutputText value="#{mensagem.dsModPrioridadeDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:" />
							<br:brOutputText
								value="#{mensagem.cdModUtilizacaoCadastroFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_valor_limite_agendamento_individual}:" />
							<br:brOutputText value="#{mensagem.vlModLimiteIndividual}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_valor_limite_agendamento}:" />
							<br:brOutputText value="#{mensagem.vlModLimiteDiario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:" />
							<br:brOutputText value="#{mensagem.qtModDiaRepique}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_dias_floating}:" />
							<br:brOutputText value="#{mensagem.qtModDiaFloating}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_consistencia_cpf_cnpj_favorecido}:" />
							<br:brOutputText value="#{mensagem.dsModTipoConsFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>

		<!-- Modalidade Pagamentos de Beneficios Dados Ordem Pagamento-->

		<t:dataList
			id="dataListModalidadePagamentosBeneficiosDadosOrdemPagamento"
			var="mensagem"
			value="#{manterContratoBean.listaPagamentosBeneficios}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsOpgTipoServico != 'N' && not empty mensagem.dsOpgTipoServico}">
					<br:brPanelGroup>
						<br:brOutputText value="#{mensagem.dsOpgTipoServico}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsOpgTipoServico != 'N' &&  not empty mensagem.dsOpgTipoServico}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_tipo_de_consulta_de_saldo}:" />
							<br:brOutputText value="#{mensagem.dsOpgTipoConsultaSaldo}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_tipoProcessamento}:" />
							<br:brOutputText value="#{mensagem.dsOpgTipoProcessamento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_pgto_feriado_nacional_fim_semana}:" />
							<br:brOutputText value="#{mensagem.dsOpgTipoTratamentoFeriado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_pgto_feriado_local}:" />
							<br:brOutputText value="#{mensagem.cdFeriLoc}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoAgendamento}:" />
							<br:brOutputText value="#{mensagem.dsOpgTiporejeicaoAgendado}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_tipoRejeicaoEfetivacao}:" />
							<br:brOutputText value="#{mensagem.dsOpgTipoRejeicaoEfetivacao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.detServicos_prioridadeDebito}:" />
							<br:brOutputText value="#{mensagem.dsOpgPrioridadeDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_utiliza_cadastro_favorecido_controle_pagamentos}:" />
							<br:brOutputText
								value="#{mensagem.cdOpgUtilizacaoCadastroFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_valor_limite_agendamento_individual}:" />
							<br:brOutputText value="#{mensagem.vlOpgLimiteIndividual}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_valor_limite_agendamento}:" />
							<br:brOutputText value="#{mensagem.vlOpgLimiteDiario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.detServicos_quantidadeDiasRepique_consulta_saldo}:" />
							<br:brOutputText value="#{mensagem.qtOpgDiaRepique}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_ocorrencia_debito}:" />
							<br:brOutputText value="#{mensagem.dsOpgOcorrenciasDebito}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_local_emissao}:" />
							<br:brOutputText value="#{mensagem.oPgEmissao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_dias_expiracao}:" />
							<br:brOutputText value="#{mensagem.qtOpgDiaExpiracao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>


		<!--  ComprovacaoDeVida -->

		<t:dataList id="dataListServicoComprovacaoDeVida" var="mensagem"
			value="#{manterContratoBean.listaPagamentosBeneficios}">

			<br:brPanelGrid style="margin-top: 20px">
				<br:brPanelGroup>
					<br:brOutputText value="#{msgs.label_comprovacao_vida}:"
						styleClass="HtmlOutputTextTitleBradesco" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				width="100%" style="margin-top: 9px">
				<br:brPanelGrid width="450px">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText value="#{msgs.label_tipo_acao_nao_comprovacao}:" />
						<br:brOutputText value="#{mensagem.dsBprTipoAcao}"
							styleClass="HtmlOutputTextRespostaBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid width="450px">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText
							value="#{msgs.label_quantidade_meses_periodicidade_comprovacao}:" />
						<br:brOutputText value="#{mensagem.qtBprMesProvisorio}"
							styleClass="HtmlOutputTextRespostaBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid width="450px">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText
							value="#{msgs.label_emite_aviso_comprovacao_vida}:" />
						<br:brOutputText value="#{mensagem.cdBprIndicadorEmissaoAviso}"
							styleClass="HtmlOutputTextRespostaBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid width="450px">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText
							value="#{msgs.label_quantidade_dias_aviso_antes_inicio}:" />
						<br:brOutputText value="#{mensagem.qtBprDiaAnteriorAviso}"
							styleClass="HtmlOutputTextRespostaBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid width="450px">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText
							value="#{msgs.label_quantidade_dias_emissao_aviso_antes_vencimento}:" />
						<br:brOutputText value="#{mensagem.qtBprDiaAnteriorVencimento}"
							styleClass="HtmlOutputTextRespostaBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid width="450px">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText
							value="#{msgs.label_utiliza_mensagem_personalizada_online}:" />
						<br:brOutputText
							value="#{mensagem.cdBprUtilizadoMensagemPersonalizada}"
							styleClass="HtmlOutputTextRespostaBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid width="450px">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText value="#{msgs.label_destino}:" />
						<br:brOutputText value="#{mensagem.dsBprDestino}"
							styleClass="HtmlOutputTextRespostaBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid width="450px">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText
							value="#{msgs.label_cadastro_endereco_utilizado}:" />
						<br:brOutputText
							value="#{mensagem.dsBprCadastroEnderecoUtilizado}"
							styleClass="HtmlOutputTextRespostaBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid width="450px">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText
							value="#{msgs.label_cadastro_endereco_utilizado}:" />
						<br:brOutputText
							value="#{mensagem.dsBprCadastroEnderecoUtilizado}"
							styleClass="HtmlOutputTextRespostaBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>

		<!--  Servicos -->


		<t:dataList id="dataListServico1" var="mensagem"
			value="#{manterContratoBean.listaServicos}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsAvisoTipoAvisoFavorecido != 'N' && not empty mensagem.dsAvisoTipoAvisoFavorecido}">
					<br:brPanelGroup>
						<br:brOutputText value="#{mensagem.dsAvisoTipoAvisoFavorecido}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsAvisoTipoAvisoFavorecido != 'N' && not empty mensagem.dsAvisoTipoAvisoFavorecido}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_periodicidade_emissao}:" />
							<br:brOutputText
								value="#{mensagem.dsAvisoPeriodicidadeEmissaoFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_destino}:" />
							<br:brOutputText value="#{mensagem.dsAvisoDestinoFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_demonstra_informacoes_area_reservada}:" />
							<br:brOutputText
								value="#{mensagem.dsAvisoDemontracaoInformacaoReservadaFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_vias_emitir}:" />
							<br:brOutputText
								value="#{mensagem.cdAvisoQuantidadeViasEmitirFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>



		<!--  Servicos 2 -->


		<t:dataList id="dataListServico2" var="mensagem"
			value="#{manterContratoBean.listaServicos}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsAvisoTipoAvisoPagador != 'N' && not empty mensagem.dsAvisoTipoAvisoPagador}">
					<br:brPanelGroup>
						<br:brOutputText value="#{mensagem.dsAvisoTipoAvisoPagador}:"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsAvisoTipoAvisoPagador != 'N' && not empty mensagem.dsAvisoTipoAvisoPagador}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_periodicidade_emissao}:" />
							<br:brOutputText
								value="#{mensagem.dsAvisoPeriodicidadeEmissaoPagador}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_destino}:" />
							<br:brOutputText value="#{mensagem.dsAvisoDestinoPagador}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_demonstra_informacoes_area_reservada}:" />
							<br:brOutputText
								value="#{mensagem.dsAvisoDemontracaoInformacaoReservadaPagador}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_vias_emitir}:" />
							<br:brOutputText
								value="#{mensagem.cdAvisoQuantidadeViasEmitirPagador}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>

		<!--  Servicos 3 -->


		<t:dataList id="dataListServico3" var="mensagem"
			value="#{manterContratoBean.listaServicos}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.cdComprovanteTipoComprovanteFavorecido != 'N' && not empty mensagem.cdComprovanteTipoComprovanteFavorecido}">
					<br:brPanelGroup>
						<br:brOutputText
							value="#{mensagem.cdComprovanteTipoComprovanteFavorecido}:"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.cdComprovanteTipoComprovanteFavorecido != 'N' && not empty mensagem.cdComprovanteTipoComprovanteFavorecido}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_periodicidade_emissao}:" />
							<br:brOutputText
								value="#{mensagem.dsComprovantePeriodicidadeEmissaoFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_destino}:" />
							<br:brOutputText
								value="#{mensagem.dsComprovanteDestinoFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_permite_correspondencia_aberta}:" />
							<br:brOutputText value=""
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_demonstra_informacoes_area_reservada}:" />
							<br:brOutputText
								value="#{mensagem.dsDemonstrativoInformacaoReservadaFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_vias_emitir}:" />
							<br:brOutputText value="#{mensagem.qtViasEmitirFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_agrupar_correspondencias}:" />
							<br:brOutputText
								value="#{mensagem.dsAgrupamentoCorrespondenteFavorecido}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>

		<!--  Servicos 4 -->


		<t:dataList id="dataListServico4" var="mensagem"
			value="#{manterContratoBean.listaServicos}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.cdComprovanteTipoComprovantePagador != 'N' && not empty mensagem.cdComprovanteTipoComprovantePagador}">
					<br:brPanelGroup>
						<br:brOutputText
							value="#{mensagem.cdComprovanteTipoComprovantePagador}:"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.cdComprovanteTipoComprovantePagador != 'N' && not empty mensagem.cdComprovanteTipoComprovantePagador}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_periodicidade_emissao}:" />
							<br:brOutputText
								value="#{mensagem.dsComprovantePeriodicidadeEmissaoPagador}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_destino}:" />
							<br:brOutputText value="#{mensagem.dsComprovanteDestinoPagador}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_permite_correspondencia_aberta}:" />
							<br:brOutputText value=""
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_demonstra_informacoes_area_reservada}:" />
							<br:brOutputText
								value="#{mensagem.dsDemonstrativoInformacaoReservadaPagador}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_vias_emitir}:" />
							<br:brOutputText value="#{mensagem.qtViasEmitirPagador}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_agrupar_correspondencias}:" />
							<br:brOutputText
								value="#{mensagem.dsAgrupamentoCorrespondentePagador}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>

		<!--  Servicos 5 -->


		<t:dataList id="dataListServico6" var="mensagem"
			value="#{manterContratoBean.listaServicos}">

			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsCsdTipoComprovanteDiversos != 'N' && not empty mensagem.dsCsdTipoComprovanteDiversos}">
					<br:brPanelGroup>
						<br:brOutputText value="#{mensagem.dsCsdTipoComprovanteDiversos}:"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsCsdTipoComprovanteDiversos != 'N' &&  not empty mensagem.dsCsdTipoComprovanteDiversos}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_tipo_rejeicao_lote}:" />
							<br:brOutputText
								value="#{mensagem.dsCsdTipoRejeicaoLoteDiversos}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_meio_disponibilizacao_correntista}:" />
							<br:brOutputText
								value="#{mensagem.dsCsdMeioDisponibilizacaoCorrentistaDiversos}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_midia_disponibilizacao_correntista_abrev}:" />
							<br:brOutputText
								value="#{mensagem.dsCsdMediaDisponibilidadeCorrentistaDiversos}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_midia_disponibilizacao_correntista_abrev}:" />
							<br:brOutputText
								value="#{mensagem.dsCsdMediaDisponibilidadeNumeroCorrentistasDiversos}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_destino_envio_correspondencia}:" />
							<br:brOutputText value="#{mensagem.dsCsdDestinoDiversos}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_meses_emissao}:" />
							<br:brOutputText value="#{mensagem.qtCsdMesEmissaoDiversos}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_cadastro_consulta_endereco_envio}:" />
							<br:brOutputText
								value="#{mensagem.dsCsdCadastroConservadorEnderecoDiversos}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_vias_emitir}:" />
							<br:brOutputText
								value="#{mensagem.cdCsdQuantidadeViasEmitirDiversos}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>

		<!--Servico Cadastro de Favorecidos -->

		<t:dataList id="dataListServico6" var="mensagem"
			value="#{manterContratoBean.listaServicos}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.cdIndcadorCadastroFavorecido != 'N' && not empty mensagem.cdIndcadorCadastroFavorecido}">
					<br:brPanelGroup>
						<br:brOutputText value="#{msgs.label_cadastro_favorecidos}:"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.cdIndcadorCadastroFavorecido != 'N' && not empty mensagem.cdIndcadorCadastroFavorecido}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_forma_manutencao_cadastro}:" />
							<br:brOutputText value="#{mensagem.dsCadastroFormaManutencao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_quantidade_dias_para_inativacao_favorecido}:" />
							<br:brOutputText
								value="#{mensagem.cdCadastroQuantidadeDiasInativos}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_consistencia_inscricao_favorecido}:" />
							<br:brOutputText
								value="#{mensagem.dsCadastroTipoConsistenciaInscricao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>

		<!--Servico Recadastramento de Beneficiarios -->

		<t:dataList id="dataListServico7" var="mensagem"
			value="#{manterContratoBean.listaServicos}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsServicoRecadastro != 'N' && not empty mensagem.dsServicoRecadastro}">
					<br:brPanelGroup>
						<br:brOutputText
							value="#{msgs.servicos_configuracaoRecadastramentoBeneficiario}:"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsServicoRecadastro != 'N' && not empty mensagem.dsServicoRecadastro}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_identificacao_beneficiario}:" />
							<br:brOutputText
								value="#{mensagem.dsRecTipoIdentificacaoBeneficio}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_tipo_consistencia_identificacao_beneficiario}:" />
							<br:brOutputText
								value="#{mensagem.dsRecTipoConsistenciaIdentificacao}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_condicao_enquadramento}:" />
							<br:brOutputText value="#{mensagem.dsRecCondicaoEnquadramento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_criterio_principal_enquadramento}:" />
							<br:brOutputText
								value="#{mensagem.dsRecCriterioPrincipalEnquadramento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_criterio_composto_enquadramento}:" />
							<br:brOutputText
								value="#{mensagem.dsRecTipoCriterioCompostoEnquadramento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_quantidade_etapas_recadastramento}:" />
							<br:brOutputText value="#{mensagem.qtRecEtapaRecadastramento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_quantidade_fases_etapa}:" />
							<br:brOutputText
								value="#{mensagem.qtRecFaseEtapaRecadastramento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_data_inicio_recadastramento}:" />
							<br:brOutputText value="#{mensagem.dtRecInicioRecadastramento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_data_fim_recadastramento}:" />
							<br:brOutputText value="#{mensagem.dtRecFinalRecadastramento}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_gerar_retorno_operacoes_realizadas_netEmpresa}:" />
							<br:brOutputText value="#{mensagem.dsRecGeradorRetornoInternet}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>

		<!--Modalidade Recadastramento de Beneficiarios -->


		<t:dataList id="dataListModalidadeRecadastramentoBeneficios1"
			var="mensagem" value="#{manterContratoBean.listaServicos}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsServicoRecadastro != 'N' && not empty mensagem.dsRecTipoModalidadeAviso && mensagem.dsRecTipoModalidadeAviso != 'N'}">
					<br:brPanelGroup>
						<br:brOutputText value="#{mensagem.dsRecTipoModalidadeAviso}"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsServicoRecadastro != 'N' && not empty mensagem.dsRecTipoModalidadeAviso  && mensagem.dsRecTipoModalidadeAviso != 'N'}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_momento_envio}:" />
							<br:brOutputText value="#{mensagem.dsRecMomentoEnvioAviso}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_destino}:" />
							<br:brOutputText value="#{mensagem.dsRecDestinoAviso}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_cadastro_endereco_utilizado}:" />
							<br:brOutputText
								value="#{mensagem.dsRecCadastroEnderecoUtilizadoAviso}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_quantidade_dias_aviso_antecipado}:" />
							<br:brOutputText value="#{mensagem.qtRecDiaAvisoAntecipadoAviso}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_permitir_agrupamento_correspondencia}:" />
							<br:brOutputText
								value="#{mensagem.dsRecPermissaoAgrupamentoAviso}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>


		<t:dataList id="dataListModalidadeRecadastramentoBeneficios2"
			var="mensagem" value="#{manterContratoBean.listaServicos}">
			<br:brPanelGrid>
				<br:brPanelGrid style="margin-top: 20px"
					rendered="#{mensagem.dsServicoRecadastro != 'N' && not empty mensagem.dsRecTipoModalidadeFormulario && mensagem.dsRecTipoModalidadeFormulario != 'N'}">
					<br:brPanelGroup>
						<br:brOutputText
							value="#{mensagem.dsRecTipoModalidadeFormulario}j"
							styleClass="HtmlOutputTextTitleBradesco" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					width="100%" style="margin-top: 9px"
					rendered="#{mensagem.dsServicoRecadastro != 'N' && not empty mensagem.dsRecTipoModalidadeFormulario && mensagem.dsRecTipoModalidadeFormulario != 'N'}">
					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_momento_envio}:" />
							<br:brOutputText value="#{mensagem.dsRecMomentoEnvioFormulario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText value="#{msgs.label_destino}:" />
							<br:brOutputText value="#{mensagem.dsRecDestinoFormulario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_cadastro_endereco_utilizado}:" />
							<br:brOutputText
								value="#{mensagem.dsRecCadastroEnderecoUtilizadoFormulario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_quantidade_dias_aviso_antecipado}:" />
							<br:brOutputText value="#{mensagem.qtRecDiaAntecipadoFormulario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>


					<br:brPanelGrid width="450px">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText
								value="#{msgs.label_permitir_agrupamento_correspondencia}:" />
							<br:brOutputText
								value="#{mensagem.dsRecPermissaoAgrupamentoFormulario}"
								styleClass="HtmlOutputTextRespostaBradesco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</t:dataList>



		<f:verbatim>
			<hr class="lin">
		</f:verbatim>


		<br:brPanelGrid cellpadding="0" cellspacing="0" width="100%">
			<br:brPanelGrid styleClass="mainPanel" cellpadding="0"
				cellspacing="0" columns="1" width="100%" style="margin-top: 9px">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.label_tarifas}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid style="margin-top: 9px" />

			<br:brPanelGrid cellpadding="0" cellspacing="0" width="100%"
				style="margin-top: 9px">
				<app:scrollableDataTable id="listaTarifas"
					value="#{manterContratoBean.listaGridTarifasModalidade}"
					var="result"  width="100%" rowIndexVar="parameterKey4">
					<app:scrollableColumn>
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_servico}" />
						</f:facet>
						<br:brOutputText value="#{result.dsProduto}" />
					</app:scrollableColumn>
					<app:scrollableColumn>
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_modalidade}" />
						</f:facet>
						<br:brOutputText value="#{result.dsProdutoServicoRelacionado}" />
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="alinhamento_direita">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_tarifa_contratada}" />
						</f:facet>
						<br:brOutputText value="#{result.vlTarifa}" />
					</app:scrollableColumn>

				</app:scrollableDataTable>
			</br:brPanelGrid>
		
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%" style="margin-top: 9px" rendered="#{manterContratoBean.visualizaCestaTarifas}">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.label_conf_cestas_tarifas}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid style="margin-top: 9px" />

		<br:brPanelGrid cellpadding="0" cellspacing="0" width="100%" rendered="#{manterContratoBean.visualizaCestaTarifas}"
			style="margin-top: 9px">
			<app:scrollableDataTable id="tabelaTarifas"
				value="#{manterContratoBean.listaConfCestasTarifas}"
				var="listaTarifa"  width="100%" rowIndexVar="parameterKey42">
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_cestas_servi�os_contratado}" />
						<br:brOutputText value="#{msgs.label_servico_pagamento_salario}" />
					</f:facet>
					<br:brOutputText value="#{listaTarifa.dsCestaTariafaFlexbilizacao}" />
				</app:scrollableColumn>
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_tarifa_padrao}" />
					</f:facet>
					<br:brOutputText value="#{listaTarifa.vlTarifaPadrao}" />
				</app:scrollableColumn>
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_tarifa_contratada}" />
					</f:facet>
					<br:brOutputText value="#{listaTarifa.vlTariafaFlexbilizacaoContrato}" />
				</app:scrollableColumn>
				<app:scrollableColumn>
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_tarifa_contratada_porcentagem}" />
					</f:facet>
					<br:brOutputText value="#{listaTarifa.porcentagemTariafaFlexbilizacaoContrato}" />
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGrid>
	
		<br:brPanelGroup rendered="#{manterContratoBean.visualizaCestaTarifas}">	
			<f:verbatim>
				<hr class="lin">
			</f:verbatim>
		</br:brPanelGroup>

		<br:brPanelGrid id="painelBotoes" columns="2" cellpadding="0"
			cellspacing="0" width="100%">
			<br:brPanelGrid>
				<br:brCommandButton id="btnVoltar" value="#{msgs.label_voltar}"
					action="#{manterContratoBean.voltarResumo}" styleClass="bto1">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGrid>
			<br:brPanelGrid style="float: right">
				<br:brCommandButton id="btnImprimirResumo"
					value="#{msgs.btn_imprimir_resumo_contrato}"
					action="#{manterContratoBean.imprimirResumoContrato}"
					styleClass="bto1" onclick="desbloquearTela();">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGrid>
		</br:brPanelGrid>

	</br:brPanelGrid>
	<h:inputHidden id="nomeCampoFoco"
		value="#{manterContratoBean.nomeTabelaAtual}"></h:inputHidden>
</brArq:form>