<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="altMeiosTransmissaoForm" name="altMeiosTransmissaoForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.retornoLayoutsArquivos_cliente_master}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_cpf_cnpj1}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_nome_razao_social1}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.nmRazaoSocialRepresentante}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_grupo_economico}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.dsGrupoEconomico}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_atividade_economica}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.dsAtividadeEconomica}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_segmento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.dsSegmentoCliente}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_sub_segmento}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.dsSubSegmentoCliente}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.dsAgenciaOperadora}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerente_responsavel}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsGerenteResponsavel}"/>
			</br:brPanelGroup>				
		</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_cliente_participante}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterContratoBean.cpfCnpj}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>
					
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterContratoBean.nomeRazaoSocial}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.retornoLayoutsArquivos_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_empresa}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.dsPessoaJuridica}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_tipo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.dsTipoContrato}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_numero}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.nrSequenciaContrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_descricao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.dsContrato}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_situacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.dsSituacaoContrato}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_motivo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.dsMotivoSituacao}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_participacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.saidaMeioTransmissao.cdTipoParticipacao}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_perfil_troca_arquivos}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_layout_arquivo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.dsCodTipoLayout}"  />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_codigo}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdPerfilTrocaArq}"  />
			</br:brPanelGroup>			
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_utiliza_aplicativo_formatacao_proprio}:"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup >
				<t:selectOneRadio id="rdoAplicFormProprio"  styleClass="HtmlSelectOneRadioBradesco" value="#{meiosTransmissaoBean.rdoAplicFormProprio}" forceId="true" forceIdIndex="false" >
					<f:selectItem  itemValue="1" itemLabel="#{msgs.label_sim}"/>  
					<f:selectItem  itemValue="2" itemLabel="#{msgs.label_nao}"/> 
					<a4j:support oncomplete="javascript:foco(this);" event="onclick" reRender="cboAplicativoFormatacao,txtJustificativa" action="#{meiosTransmissaoBean.limpaCombosAplFormatProprio}" status="statusAguarde" />								
				</t:selectOneRadio>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_aplicativo_formatacao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
		
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					<br:brPanelGroup>
						<br:brSelectOneMenu disabled="#{meiosTransmissaoBean.rdoAplicFormProprio == '1'}" id="cboAplicativoFormatacao" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdAplicacaoTransmPagamento}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{meiosTransmissaoBean.listaAplicativoFormatacao}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_remessa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_meio_transmissao_principal}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboMeioTransmissaoPrincipalRemessa" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdMeioPrincipalRemessa}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{meiosTransmissaoBean.listarMeioTransmissao}" />
							<a4j:support event="onchange" reRender="cboMeioTransmissaoPrincipalRemessa,cboMeioTransmissaoAlternativoRemessa,cboNivelControleRetorno,cboTipoControleRetorno,cboEmpresaRespTransDadosControle,cboRespCustoTransArqDadosControle,txtPercentualCustoTransArq,txtJuncaoRespCustoTransm,txtVolumeMensalRegTraf,txtNomeContato,txtDDDContato,txtTelefoneContato,txtRamalContato,txtEmailContato,txtNomeSolictante,txtDDDSolictante,txtTelefoneSolictante,txtRamalSolictante,txtEmailSolictante,txtJustificativa,txtBanco,txtCliente"
								 action="#{meiosTransmissaoBean.habilitarFiltroMeioTransmissao}" status="statusAguarde" />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_meio_transmissao_alternativo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboMeioTransmissaoAlternativoRemessa" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdMeioAlternRemessa}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{meiosTransmissaoBean.listarMeioTransmissao}" />
							<a4j:support event="onchange" reRender="cboMeioTransmissaoPrincipalRemessa,cboMeioTransmissaoAlternativoRemessa,cboNivelControleRetorno,cboTipoControleRetorno,cboEmpresaRespTransDadosControle,cboRespCustoTransArqDadosControle,txtPercentualCustoTransArq,txtJuncaoRespCustoTransm,txtVolumeMensalRegTraf,txtNomeContato,txtDDDContato,txtTelefoneContato,txtRamalContato,txtEmailContato,txtNomeSolictante,txtDDDSolictante,txtTelefoneSolictante,txtRamalSolictante,txtEmailSolictante,txtJustificativa,txtBanco,txtCliente"
								 action="#{meiosTransmissaoBean.habilitarFiltroMeioTransmissao}" status="statusAguarde" />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_retorno}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>		
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_meio_transmissao_principal}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboNivelControleRetorno" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdMeioPrincipalRetorno}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{meiosTransmissaoBean.listarMeioTransmissao}" />
							<a4j:support event="onchange" reRender="cboMeioTransmissaoPrincipalRemessa,cboMeioTransmissaoAlternativoRemessa,cboNivelControleRetorno,cboTipoControleRetorno,cboEmpresaRespTransDadosControle,cboRespCustoTransArqDadosControle,txtPercentualCustoTransArq,txtJuncaoRespCustoTransm,txtVolumeMensalRegTraf,txtNomeContato,txtDDDContato,txtTelefoneContato,txtRamalContato,txtEmailContato,txtNomeSolictante,txtDDDSolictante,txtTelefoneSolictante,txtRamalSolictante,txtEmailSolictante,txtJustificativa,txtBanco,txtCliente"
								 action="#{meiosTransmissaoBean.habilitarFiltroMeioTransmissao}"  />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_meio_transmissao_alternativo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboTipoControleRetorno" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdMeioAltrnRetorno}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{meiosTransmissaoBean.listarMeioTransmissao}" />
							<a4j:support event="onchange" reRender="cboMeioTransmissaoPrincipalRemessa,cboMeioTransmissaoAlternativoRemessa,cboNivelControleRetorno,cboTipoControleRetorno,cboEmpresaRespTransDadosControle,cboRespCustoTransArqDadosControle,txtPercentualCustoTransArq,txtJuncaoRespCustoTransm,txtVolumeMensalRegTraf,txtNomeContato,txtDDDContato,txtTelefoneContato,txtRamalContato,txtEmailContato,txtNomeSolictante,txtDDDSolictante,txtTelefoneSolictante,txtRamalSolictante,txtEmailSolictante,txtJustificativa,txtBanco,txtCliente"
								 action="#{meiosTransmissaoBean.habilitarFiltroMeioTransmissao}" status="statusAguarde" />
						</br:brSelectOneMenu>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_contratacao}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa_responsavel_transmissao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboEmpresaRespTransDadosControle" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdPessoaJuridicaParceiro}"
							disabled="#{meiosTransmissaoBean.flagDadosControle}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{meiosTransmissaoBean.listaEmpresaResponsavelTransmissao}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_custo_transmissao_arquivo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>

			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>

		       <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="cboRespCustoTransArqDadosControle" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdResponsavelCustoEmpresa}"
							disabled="#{meiosTransmissaoBean.flagClienteSolicitante}" styleClass="HtmlSelectOneMenuBradesco">
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{meiosTransmissaoBean.listaResponsavelCustoTransArq}" />
							  <a4j:support oncomplete="javascript:foco(this);" event="onchange" reRender="cboMeioTransmissaoPrincipalRemessa,cboMeioTransmissaoAlternativoRemessa,
							  	cboNivelControleRetorno,cboTipoControleRetorno,cboEmpresaRespTransDadosControle,cboRespCustoTransArqDadosControle,txtPercentualCustoTransArq,
							  	txtJuncaoRespCustoTransm,txtVolumeMensalRegTraf,txtNomeContato,txtDDDContato,txtTelefoneContato,txtRamalContato,txtEmailContato,txtNomeSolictante,
							  	txtDDDSolictante,txtTelefoneSolictante,txtRamalSolictante,txtEmailSolictante,txtJustificativa, txtBanco, txtCliente"
							  	status="statusAguarde" action="#{meiosTransmissaoBean.atribuirPercentualBancoCliente}" />				
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="465">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco}:"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cliente}:"/>
			</br:brPanelGroup>
	
			<br:brPanelGroup>
				<br:brInputText id="txtBanco" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.pcCustoOrganizacaoTransmissao}" size="10" maxlength="5" styleClass="HtmlInputTextBradesco"
					alt="percentual" onfocus="loadMasks();" converter="decimalBrazillianConverter" disabled="#{meiosTransmissaoBean.desabilitaPercentualBanco}">
					<a4j:support event="onblur" reRender="txtCliente" action="#{meiosTransmissaoBean.calcularPercentualCliente}"
						onsubmit="validarPercentualBanco('altMeiosTransmissaoForm')" oncomplete="validaCampoDecimal(this, '#{msgs.label_numero_invalido}')" />
				</br:brInputText>
				<br:brOutputText  value="#{msgs.label_porcentagem}"  styleClass="HtmlOutputTextRespostaBradesco" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brInputText id="txtCliente" value="#{meiosTransmissaoBean.percentualCliente}" size="10" maxlength="5" styleClass="HtmlInputTextBradesco"
					onblur="validaCampoDecimal(this, '#{msgs.label_numero_invalido}');" alt="percentual" onfocus="loadMasks();" converter="decimalBrazillianConverter"
					disabled="true" />
				<br:brOutputText  value="#{msgs.label_porcentagem}"  styleClass="HtmlOutputTextRespostaBradesco" />
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_juncao_responsavel_custo}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
	       		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
					</br:brPanelGroup>	
					<br:brPanelGroup>
						<br:brInputText id="txtJuncaoRespCustoTransm" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdUnidadeOrganizacional}"
							disabled="#{meiosTransmissaoBean.flagClienteSolicitante}" size="12" maxlength="6" styleClass="HtmlInputTextBradesco"
							onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum()">
					    </br:brInputText>				    
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_volume_mensal_registros_trafegados}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtVolumeMensalRegTraf" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.qtMesRegistroTrafg}" size="12" maxlength="9" styleClass="HtmlInputTextBradesco"
							onkeypress="onlyNum()" alt="decimalBr9Ponto" onfocus="loadMasks()" disabled="#{meiosTransmissaoBean.flagClienteSolicitante}">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_contato_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nome}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtNomeContato" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.dsNomeContatoCliente}"
							size="100" maxlength="70" styleClass="HtmlInputTextBradesco" disabled="#{meiosTransmissaoBean.flagClienteSolicitante}">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_ddd}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtDDDContato" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdAreaFoneCliente}" size="10" maxlength="3" styleClass="HtmlInputTextBradesco"
							onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum()" disabled="#{meiosTransmissaoBean.flagClienteSolicitante}">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_telefone}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtTelefoneContato" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdFoneContatoCliente}" size="20" maxlength="9"
							styleClass="HtmlInputTextBradesco" alt="phoneNumber9" onkeypress="onlyNum()" onfocus="loadMasks()" disabled="#{meiosTransmissaoBean.flagClienteSolicitante}">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_email}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtEmailContato" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.dsEmailContatoCliente}"
							size="100" maxlength="70" styleClass="HtmlInputTextBradesco" disabled="#{meiosTransmissaoBean.flagClienteSolicitante}">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_solicitante_agencia_gerente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_nome}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtNomeSolictante" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.dsSolicitacaoPendente}"
							size="100" maxlength="70" styleClass="HtmlInputTextBradesco" disabled="#{meiosTransmissaoBean.flagClienteSolicitante}">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_ddd}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtDDDSolictante" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdAreaFonePend}" size="10" maxlength="3" styleClass="HtmlInputTextBradesco"
							onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum()" disabled="#{meiosTransmissaoBean.flagClienteSolicitante}">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			 <br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
			 
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_telefone}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtTelefoneSolictante" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.cdFoneSolicitacaoPend}" size="20" maxlength="11" 
							styleClass="HtmlInputTextBradesco" alt="phoneNumber9" onkeypress="onlyNum()" onfocus="loadMasks()" disabled="#{meiosTransmissaoBean.flagClienteSolicitante}">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="1" style="margin-left:20px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_email}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtEmailSolictante" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.dsEmailSolctPend}"
							size="100" maxlength="70" styleClass="HtmlInputTextBradesco" disabled="#{meiosTransmissaoBean.flagClienteSolicitante}">
					    </br:brInputText>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_justificativa_obs}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
		       	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputTextarea cols="75" id="txtJustificativa" value="#{meiosTransmissaoBean.saidaDetalhesPerfilTrocaArquivo.dsObsGeralPerfil}" rows="3"
							styleClass="HtmlInputTextBradesco" onkeydown="javascript:maxLength(this,250);" onkeyup="javascript:maxLength(this,250);"
							disabled="#{meiosTransmissaoBean.flagClienteSolicitante}">
					    </br:brInputTextarea>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton styleClass="bto1" value="#{msgs.label_botao_voltar}" action="#{meiosTransmissaoBean.voltar}" >	
					<brArq:submitCheckClient/>
				</br:brCommandButton>			
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px">
				<br:brCommandButton id="btnAvancar" styleClass="bto1" style="align:left" value="#{msgs.btn_avancar}" action="#{meiosTransmissaoBean.avancarAlterar}" 
					onclick="javascript: desbloquearTela(); return validaCamposAvancarAlterarMeios(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
					'#{msgs.lavel_nivel_controle}','#{msgs.label_tipo_de_controle}','#{msgs.label_periodicidade_inicializacao_contagem}','#{msgs.label_meio_transmissao_principal_remessa}',
					'#{msgs.label_meio_transmissao_principal_retorno}','#{msgs.label_empresa_responsavel_transmissao}','#{msgs.label_custo_transmissao_arquivo}','#{msgs.label_volume_mensal_registros_trafegados}',
					'#{msgs.label_nome_cliente}','#{msgs.label_ddd_cliente}','#{msgs.label_telefone_cliente}','#{msgs.label_ramal_cliente}','#{msgs.label_email_cliente}','#{msgs.label_nome_solicitante}',
					'#{msgs.label_ddd_solicitante}','#{msgs.label_telefone_solicitante}','#{msgs.label_ramal_solicitante}','#{msgs.label_email_solicitante}','#{msgs.label_justificativa_obs}',
					'#{msgs.label_juncao_responsavel_custo_transmissao}','#{msgs.label_percentual_custo_transmissao_arquivo}');" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
</brArq:form>