<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %> 
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterContratoParametros" name="conManterContratoParametros" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >				
			<br:brPanelGroup style="text-align:right;width:600px" >					
				<br:brCommandLink id="btnCarregarParametros" style="visibility:hidden" styleClass="bto1" action="#{manterContratoBean.carregarParametros}">
					<f:param name="empresaGestora" value="#{param['paramEmpresaGestora']}" />
					<f:param name="tipoContrato" value="#{param['paramTipoContrato']}" />
					<f:param name="numeroContrato" value="#{param['paramNumeroContrato']}" />										
					<brArq:submitCheckClient/>
				</br:brCommandLink>
			</br:brPanelGroup>
		</br:brPanelGrid>		

</br:brPanelGrid>
</brArq:form>