<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterSolManutContratoForm"
	name="conManterSolManutContratoForm">
	<h:inputHidden id="hiddenObrigatoriedade"
		value="#{manterContratoBean.identificacaoClienteContratoBean.obrigatoriedade}" />
	<h:inputHidden id="habilitaCampos"
		value="#{manterContratoBean.identificacaoClienteContratoBean.habilitaCampos}" />
	<h:inputHidden id="renegociavel"
		value="#{manterContratoBean.renegociavel}" />

	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0"
		cellspacing="0">

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.identificacaoClienteContrato_label_identificacao_cliente}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<t:selectOneRadio id="rdoFiltroCliente"
			value="#{manterContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado}"
			styleClass="HtmlSelectOneRadioBradesco" layout="spread"
			forceId="true" forceIdIndex="false" onclick="submit();"
			disabled="#{manterContratoBean.identificacaoClienteContratoBean.bloqueiaRadio}">
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="" />
		</t:selectOneRadio>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<t:radio for="rdoFiltroCliente" index="0" />

			<br:brPanelGroup id="panelCnpj">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.identificacaoClienteContrato_cnpj}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
					<br:brInputText id="txtCnpj"
						onkeyup="proximoCampo(9,'conManterSolManutContratoForm','conManterSolManutContratoForm:txtCnpj',
						'conManterSolManutContratoForm:txtFilial');" style="margin-right: 5" converter="javax.faces.Long"
						onkeypress="onlyNum();" size="11" maxlength="9"
						disabled="#{manterContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
						styleClass="HtmlInputTextBradesco"
						value="#{manterContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					<br:brInputText id="txtFilial"
						onkeyup="proximoCampo(4,'conManterSolManutContratoForm','conManterSolManutContratoForm:txtFilial',
						'conManterSolManutContratoForm:txtControle');" style="margin-right: 5" converter="javax.faces.Integer"
						onkeypress="onlyNum();" size="5" maxlength="4"
						disabled="#{manterContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
						styleClass="HtmlInputTextBradesco"
						value="#{manterContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					<br:brInputText id="txtControle" style="margin-right: 5"
						converter="javax.faces.Integer" onkeypress="onlyNum();" size="3"
						maxlength="2"
						disabled="#{manterContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
						styleClass="HtmlInputTextBradesco"
						value="#{manterContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCnpj}"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<t:radio for="rdoFiltroCliente" index="1" />

			<br:brPanelGroup id="panelCpf">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.identificacaoClienteContrato_cpf}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brInputText styleClass="HtmlInputTextBradesco"
						style="margin-right: 5" converter="javax.faces.Long"
						onkeypress="onlyNum();"
						disabled="#{manterContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente }"
						value="#{manterContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpf}"
						size="11" maxlength="9" id="txtCpf"
						onkeyup="proximoCampo(9,'conManterSolManutContratoForm','conManterSolManutContratoForm:txtCpf','conManterSolManutContratoForm:txtControleCpf');"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
					<br:brInputText styleClass="HtmlInputTextBradesco"
						converter="javax.faces.Integer" onkeypress="onlyNum();"
						disabled="#{manterContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
						value="#{manterContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCpf}"
						maxlength="2" size="3" id="txtControleCpf"
						onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
			<t:radio for="rdoFiltroCliente" index="2" />

			<br:brPanelGroup id="panelNomeRazao">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brInputText styleClass="HtmlInputTextBradesco"
					disabled="#{manterContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '2' || manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
					value="#{manterContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}"
					size="71" maxlength="70" id="txtNomeRazaoSocial" />
			</br:brPanelGroup>

		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" border="0">
			<t:radio for="rdoFiltroCliente" index="3" />

			<br:brPanelGroup id="panelBanco">

				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.identificacaoClienteContrato_banco}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brInputText styleClass="HtmlInputTextBradesco"
					onkeypress="onlyNum();" converter="javax.faces.Integer"
					disabled="#{manterContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
					value="#{manterContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdBanco}"
					size="4" maxlength="3" id="txtBanco"
					onkeyup="proximoCampo(3,'conManterSolManutContratoForm','conManterSolManutContratoForm:txtBanco','conManterSolManutContratoForm:txtAgencia');"
					onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />

			</br:brPanelGroup>

			<br:brPanelGroup id="panelAgencia">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.identificacaoClienteContrato_agencia}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brInputText styleClass="HtmlInputTextBradesco"
					onkeypress="onlyNum();" converter="javax.faces.Integer"
					disabled="#{manterContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
					value="#{manterContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}"
					size="6" maxlength="5" id="txtAgencia"
					onkeyup="proximoCampo(5,'conManterSolManutContratoForm','conManterSolManutContratoForm:txtAgencia','conManterSolManutContratoForm:txtConta');"
					onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			</br:brPanelGroup>

			<br:brPanelGroup id="panelConta">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.identificacaoClienteContrato_conta}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brInputText styleClass="HtmlInputTextBradesco"
					converter="javax.faces.Long" onkeypress="onlyNum();"
					disabled="#{manterContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
					value="#{manterContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdContaBancaria}"
					size="17" maxlength="13" id="txtConta"
					onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup id="panelBtoConsultarCliente"
				style="width:100%; text-align: right">
				<br:brCommandButton id="btnLimparCampos2" styleClass="bto1"
					value="#{msgs.identificacaoClienteContrato_limpar_dados}"
					action="#{manterContratoBean.identificacaoClienteContratoBean.limparDadosCliente}"
					style="margin-right:5px">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btoConsultarCliente" styleClass="bto1"
					disabled="#{empty manterContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado || manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
					value="#{msgs.identificacaoClienteContrato_label_consultar_cliente}"
					action="#{manterContratoBean.identificacaoClienteContratoBean.consultarClientes}"
					style="cursor:hand;"
					onmouseover="javascript:alteraBotao('visualizacao', this.id);"
					onmouseout="javascript:alteraBotao('normal', this.id);"
					onclick="return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.identificacaoClienteContrato_cnpj}','#{msgs.identificacaoClienteContrato_cpf}', '#{msgs.identificacaoClienteContrato_label_nomeRazao}','#{msgs.identificacaoClienteContrato_banco}','#{msgs.identificacaoClienteContrato_agencia}','#{msgs.identificacaoClienteContrato_conta}');">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
			value="#{msgs.identificacaoClienteContrato_label_cliente}:" />
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"
					style="margin-right: 5" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco"
					value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
					value="#{manterContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}" />
			</br:brPanelGroup>

			<br:brPanelGroup style="width:20px; margin-bottom:5px">
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"
					style="margin-right: 5" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco"
					value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"
					value="#{manterContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0"
			columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.conManterSolManutContratos_argumentos_pesquisa}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="empresaGestora"
							value="#{manterContratoBean.identificacaoClienteContratoBean.empresaGestoraFiltro}"
							styleClass="HtmlSelectOneMenuBradesco" style="width: 250"
							disabled="#{!manterContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
							<f:selectItems
								value="#{manterContratoBean.identificacaoClienteContratoBean.listaEmpresaGestora}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>

			<br:brPanelGroup style="width:20px; margin-bottom:5px">
			</br:brPanelGroup>


			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.identificacaoClienteContrato_tipo_contrato}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="tipoContrato"
							value="#{manterContratoBean.identificacaoClienteContratoBean.tipoContratoFiltro}"
							styleClass="HtmlSelectOneMenuBradesco" style="width: 250"
							disabled="#{!manterContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
							<f:selectItems
								value="#{manterContratoBean.identificacaoClienteContratoBean.listaTipoContrato}" />
						</br:brSelectOneMenu>

					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>

			<br:brPanelGroup style="width:20px; margin-bottom:5px">
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.identificacaoClienteContrato_numero}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brInputText id="numero" styleClass="HtmlInputTextBradesco"
							onkeypress="onlyNum();"
							value="#{manterContratoBean.identificacaoClienteContratoBean.numeroFiltro}"
							size="12" maxlength="10"
							readonly="#{!manterContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}"
							onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
							<%--<a4j:support event="onblur" action="#{manterContratoBean.identificacaoClienteContratoBean.desabilitaCamposContrato}"	reRender="habilitaCampos,situacao, agenciaOperadora, gerenteId, gerenteNome" />--%>
						</br:brInputText>
					</br:brPanelGroup>
				</br:brPanelGrid>

			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco"
							value="#{msgs.identificacaoClienteContrato_agencia_operadora}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brInputText id="agenciaOperadora" onkeypress="onlyNum();"
							styleClass="HtmlInputTextBradesco"
							value="#{manterContratoBean.identificacaoClienteContratoBean.agenciaOperadoraFiltro}"
							size="10" maxlength="6"
							readonly="#{manterContratoBean.identificacaoClienteContratoBean.habilitaCampos || !manterContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}"
							onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
							<a4j:support event="onblur"
								reRender="gerenteId,gerenteNome,btnConsultarGerente"
								action="#{manterContratoBean.identificacaoClienteContratoBean.limparGerenteResponsavel}"
								oncomplete="limparGerente();" />
						</br:brInputText>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>

			<br:brPanelGroup style="width:20px; margin-bottom:5px">
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.identificacaoClienteContrato_gerente_responsavel}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brInputText id="gerenteId"
							readonly="#{!manterPendenciasContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}"
							onkeypress="javascript:limparNomeGerente(document.forms[1]);"
							onkeydown="javascript:limparNomeGerente(document.forms[1]);"
							styleClass="HtmlInputTextBradesco"
							value="#{manterContratoBean.identificacaoClienteContratoBean.codigoFunc}"
							size="15" maxlength="9">
							<brArq:commonsValidator type="integer"
								arg="#{msgs.identificacaoClienteContrato_gerente_responsavel}"
								server="false" client="true" />
							<a4j:support event="onblur"
								action="#{manterContratoBean.identificacaoClienteContratoBean.buscaFuncionarios}"
								reRender="gerenteNome,formModalMessages"
								onsubmit="loadModalJQuery()"
								oncomplete="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
						</br:brInputText>
					</br:brPanelGroup>
					<br:brPanelGrid columns="1" style="margin-right:5px"
						cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGroup>
						<br:brInputText id="gerenteNome"
							styleClass="HtmlInputTextBradesco"
							value="#{manterContratoBean.identificacaoClienteContratoBean.funcionarioDTO.dsFuncionario}"
							size="50" maxlength="50" disabled="true" />
					</br:brPanelGroup>
					<br:brPanelGrid columns="1" style="margin-right:5px"
						cellpadding="0" cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>

		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.identificacaoClienteContrato_situacao_contrato}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brSelectOneMenu id="situacao"
							value="#{manterContratoBean.identificacaoClienteContratoBean.situacaoFiltro}"
							disabled="#{manterContratoBean.identificacaoClienteContratoBean.habilitaCampos || !manterContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
							<f:selectItem itemValue="0"
								itemLabel="#{msgs.identificacaoClienteContrato_selecione}" />
							<f:selectItems
								value="#{manterContratoBean.identificacaoClienteContratoBean.listaSituacaoContrato}" />
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="1" width="100%" style="text-align:right"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1"
					value="#{msgs.identificacaoClienteContrato_limpar_dados}"
					action="#{manterContratoBean.identificacaoClienteContratoBean.limparDadosPesquisaContrato}"
					style="margin-right:5px">
					<brArq:submitCheckClient />
				</br:brCommandButton>
<!-- PRIMEIRO BOTAO CONSULTAR				 -->
				<br:brCommandButton id="btnConsultar"
					onclick="return validaCamposContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{manterContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}', '#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}','#{msgs.identificacaoClienteContrato_tipo_contrato}', '#{msgs.identificacaoClienteContrato_mensagem_obrigatorio}', '#{msgs.identificacaoClienteContrato_obrigatorio_consulta_gerente}');"
					styleClass="bto1"
					value="#{msgs.identificacaoClienteContrato_consultar}"
					action="#{manterContratoBean.identificacaoClienteContratoBean.consultarContrato}"
					style="cursor:hand;"
					onmouseover="javascript:alteraBotao('visualizacao', this.id);"
					onmouseout="javascript:alteraBotao('normal', this.id);">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<br>
		</f:verbatim>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup
				style="overflow-x:auto; overflow-y:auto; width:750px; height:170">

				<app:scrollableDataTable id="dataTable"
					value="#{manterContratoBean.identificacaoClienteContratoBean.listaGridPesquisa}"
					var="result" rows="10" rowIndexVar="parametroKey"
					rowClasses="tabela_celula_normal, tabela_celula_destaque"
					width="100%">
					<app:scrollableColumn styleClass="colTabCenter" width="30px">
						<f:facet name="header">
							<br:brOutputText value="" styleClass="tableFontStyle"
								style="width:25; text-align:center" />
						</f:facet>
						<t:selectOneRadio id="sorLista"
							styleClass="HtmlSelectOneRadioBradesco" layout="spread"
							forceId="true" forceIdIndex="false"
							value="#{manterContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista}">
							<f:selectItems
								value="#{manterContratoBean.identificacaoClienteContratoBean.listaControleRadio}" />
							<a4j:support event="onclick"
								action="#{manterContratoBean.renegociar}" reRender="panelBotoes" />
						</t:selectOneRadio>
						<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="120px">
						<f:facet name="header">
							<br:brOutputText
								value="#{msgs.identificacaoClienteContrato_club_representante}"
								style="width:120; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.cdClubRepresentante}"
							styleClass="tableFontStyle" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="170px">
						<f:facet name="header">
							<h:outputText
								value="#{msgs.identificacaoClienteContrato_cpf_cnpj_representante}"
								style="width:170; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.cnpjOuCpfFormatado}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="400px">
						<f:facet name="header">
							<h:outputText
								value="#{msgs.identificacaoClienteContrato_nome_razao_social}"
								style="width:400; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.nmRazaoSocialRepresentante}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="175px">
						<f:facet name="header">
							<h:outputText
								value="#{msgs.identificacaoClienteContrato_empresa_gestora}"
								style="width:175; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsPessoaJuridica}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="150px">
						<f:facet name="header">
							<h:outputText
								value="#{msgs.identificacaoClienteContrato_tipo_contrato}"
								style="width:150; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsTipoContrato}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="150px">
						<f:facet name="header">
							<h:outputText value="#{msgs.identificacaoClienteContrato_numero}"
								style="width:150; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.nrSequenciaContrato}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="130px">
						<f:facet name="header">
							<h:outputText
								value="#{msgs.identificacaoClienteContrato_situacao_contrato}"
								style="width:130; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsSituacaoContrato}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="200px">
						<f:facet name="header">
							<h:outputText
								value="#{msgs.identificacaoClienteContrato_agencia_operadora}"
								style="width:200; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.dsAgenciaOperadora}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="150px">
						<f:facet name="header">
							<h:outputText
								value="#{msgs.identificacaoClienteContrato_gerente_responsavel}"
								style="width:150; text-align:center" />
						</f:facet>
						<br:brOutputText value="#{result.descFuncionarioBradesco}" />
					</app:scrollableColumn>

				</app:scrollableDataTable>

			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" style="text-align:center"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable"
					actionListener="#{manterContratoBean.identificacaoClienteContratoBean.pesquisar}">
					<f:facet name="first">
						<brArq:pdcCommandButton id="primeira" styleClass="bto1"
							value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" />
					</f:facet>
					<f:facet name="fastrewind">
						<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_retrocesso}"
							title="#{msgs.label_retrocesso_msg}" />
					</f:facet>
					<f:facet name="previous">
						<brArq:pdcCommandButton id="anterior" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_anterior}"
							title="#{msgs.label_anterior_msg}" />
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton id="proxima" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_proxima}"
							title="#{msgs.label_proxima_msg}" />
					</f:facet>
					<f:facet name="fastforward">
						<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_avanco}"
							title="#{msgs.label_avanco_msg}" />
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton id="ultima" styleClass="bto1"
							style="margin-left: 3px;" value="#{msgs.label_ultima}"
							title="#{msgs.label_ultima_msg}" />
					</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		            <br:brPanelGroup style="width:100%;text-align:right">
                  <br:brPanelGroup style="width:100px%">
                        <br:brCommandButton id="btnLimparTela" styleClass="bto1"
                              value="#{msgs.conManterContratos_limpar}"
                              action="#{manterContratoBean.identificacaoClienteContratoBean.limparTela}">
                             <brArq:submitCheckClient />
                        </br:brCommandButton>
                  </br:brPanelGroup>
            </br:brPanelGroup>

            <br:brPanelGroup id="panelBotoes" style="width:100%;text-align:right;margin-top:6px">
                  <br:brPanelGrid columns="4" style="text-align:right" cellpadding="0" cellspacing="0" border="0">
                        <br:brPanelGroup>
                             <br:brCommandButton id="btnDadosBasicos" styleClass="bto1"
                                   style="margin-right:5px"
                                   disabled="#{empty manterContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista}"
                                   value="#{msgs.conManterContratos_dados_basicos}"
                                   action="#{manterContratoBean.dadosBasicos}">
                                   <brArq:submitCheckClient />
                             </br:brCommandButton>
                        </br:brPanelGroup>
                        <br:brPanelGroup>
                             <br:brCommandButton id="btnAlterarRepresentante" styleClass="bto1"
                                   style="margin-right:5px"
                                   disabled="true"
                                   value="#{msgs.conManterContratos_alterar_representante}"
                                   action="#{manterContratoBean.alterarRepresentante}">
                                   <brArq:submitCheckClient />
                             </br:brCommandButton>
                        </br:brPanelGroup>
<!-- BOTAO EMPRESA/PESSOAS PARTICIPANTES                         -->
                        <br:brPanelGroup>
                             <br:brCommandButton id="btnParticipantes" styleClass="bto1"
                                   style="margin-right:5px"
                                   disabled="#{empty manterContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista}"
                                   value="#{msgs.conManterContratos_participantes}"
                                   action="#{manterContratoBean.participantes}">
                                   <brArq:submitCheckClient />
                             </br:brCommandButton>
                        </br:brPanelGroup>
                        <br:brPanelGroup>
                             <br:brCommandButton id="btnContas" styleClass="bto1"
                                   disabled="#{empty manterContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista}"
                                   value="#{msgs.conManterContratos_contas}"
                                   action="#{manterContratoBean.conta}">
                                   <brArq:submitCheckClient />
                             </br:brCommandButton>
                        </br:brPanelGroup>
                  </br:brPanelGrid>
                  <br:brPanelGrid columns="9" style="text-align:right;margin-top:6px"
                        cellpadding="0" cellspacing="0" border="0">
                        <br:brPanelGroup>
                             <br:brCommandButton id="btnServicos" styleClass="bto1"
                                   style="margin-right:5px"
                                   disabled="#{empty manterContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista}"
                                   value="#{msgs.conManterContratos_servicos}"
                                   action="#{manterContratoBean.servicos}">
                                   <brArq:submitCheckClient />
                             </br:brCommandButton>
                        </br:brPanelGroup>
                        <br:brPanelGroup>
                             <br:brCommandButton id="btnTarifas" styleClass="bto1"
                                   disabled="#{empty manterContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista}"
                                   style="margin-right:5px"
                                   value="#{msgs.conManterContratos_tarifas}"
                                   action="#{manterContratoBean.tarifas}">
                                   <brArq:submitCheckClient />
                             </br:brCommandButton>
                        </br:brPanelGroup>
                        <br:brPanelGroup>
                             <br:brCommandButton id="btnLayouts" styleClass="bto1"
                                   disabled="#{empty manterContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista}"
                                   style="margin-right:5px" action="#{manterContratoBean.layouts}"
                                   value="#{msgs.conManterContratos_layouts}">
                                   <brArq:submitCheckClient />
                             </br:brCommandButton>
                        </br:brPanelGroup>
                        <br:brPanelGroup>
                             <br:brCommandButton id="btnMeioTransmissao" styleClass="bto1"
                                   disabled="#{manterContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista == null}"
                                   style="margin-right:5px"
                                   action="#{manterContratoBean.meioTransmissao}"
                                   value="#{msgs.conManterSolManutContratos2_meios_transmissao}">
                                   <brArq:submitCheckClient />
                             </br:brCommandButton>
                        </br:brPanelGroup>
                        <br:brPanelGroup>
                             <br:brCommandButton id="btnResumoContrato" styleClass="bto1"
                                   disabled="#{empty manterContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista}"
                                   style="margin-right:5px"
                                   action="#{manterContratoBean.resumoContrato}"
                                   value="#{msgs.conManterSolManutContratos2_resumo_Contrato}">
                                   <brArq:submitCheckClient />
                             </br:brCommandButton>
                        </br:brPanelGroup>
                        <br:brPanelGroup>
	        				<a4j:commandButton id="btnImprimir" style="margin-right:5px;"
	        					disabled="#{manterContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista == null}"
								value="Imprimir Contrato"
								styleClass="bto1">
							<a4j:support event="onclick"
								action="#{manterContratoBean.imprimirContrato}"
								oncomplete="document.getElementById('conManterSolManutContratoForm:btnImprimirDocumentoHidden').click()" />
							</a4j:commandButton>
							<br:brCommandButton id="btnImprimirDocumentoHidden"
								value="Imprimir Contrato"
								action="#{manterContratoBean.imprimirContratoFinal}"
								styleClass="HtmlCommandButtonBradesco"
								style="display: none"
								onclick="desbloquearTela()">
								<brArq:submitCheckClient />
							</br:brCommandButton>
                        </br:brPanelGroup>
                        <br:brPanelGroup>
                             <a4j:commandButton id="btnRenegociarNovo" styleClass="bto1"
                                   value="#{msgs.label_renegociar}"
                                   reRender="renegociavel, formModalMessages, panelBotoes"
                                   disabled="#{empty manterContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista || manterContratoBean.identificacaoClienteContratoBean.bloquearRenegociar}"
                                   action="#{manterContratoBean.verificarRenegociavelNovo}"
                                   onclick="loadModalJQuery();"
                                   oncomplete="bloquearRenegociarNovo(); renegociarNovo('#{manterContratoBean.urlRenegociacaoNovo}');">
                             </a4j:commandButton>
                        </br:brPanelGroup>
                  </br:brPanelGrid>
            </br:brPanelGroup>


		<t:inputHidden
			value="#{manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}"
			id="hiddenFlagPesquisa"></t:inputHidden>
		<a4j:status id="statusAguarde" onstart="bloquearTela()"
			onstop="desbloquearTela()" />

		<f:verbatim>
			<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conManterSolManutContratoForm:hiddenFlagPesquisa').value);
	  		function limparGerente() {
	  			document.getElementById("conManterSolManutContratoForm:gerenteId").value = "";
	  			document.getElementById("conManterSolManutContratoForm:gerenteNome").value = "";
	  		}
	  	</script>
		</f:verbatim>
	</br:brPanelGrid>

	<brArq:validatorScript functionName="validateForm" />

</brArq:form>