<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j"%>

<brArq:form id="relHistContasManterContrato" name="relrelIncContasManterContrato" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="filtroRadio" value="#{manterContratoBean.manterContratoContasBean.radioPesquisa}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />				
				<a4j:support event="onclick" action="#{manterContratoBean.manterContratoContasBean.limparCamposFiltroRadio}" reRender="txtCnpj,txtFilial,txtControle,txtCpf,txtControleCpf, banco, agencia, conta, digito, btnConsultar" />
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_cliente_master}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_nome_razao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerente_responsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_cliente_participante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>
				
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.descricaoContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detContasManterContrato_title_conta_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:19px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.bancoDesc}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_agencia}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.agenciaDesc}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.contaDesc}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.tipoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_titular}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.cpf}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_razao_social}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.razao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_situacao_vinculo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.situacaoVinculoDesc}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_data_vinculacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.dataVinculacaoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_relacionamento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="tipoRelacionamento" value="#{manterContratoBean.manterContratoContasBean.cdTipoRelacionamentoHistorico}" >
						<f:selectItem itemValue="0" itemLabel="#{msgs.conContasManterContrato_label_combo_selecione}"/>
						<f:selectItems value="#{manterContratoBean.manterContratoContasBean.listaCboTipoRelacionamento}" />
						<a4j:support event="onchange" reRender="btnConsultar,btnIncluir" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
	    <br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_data_manutencao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brPanelGroup>
						 	<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioDe');" id="calendarioDe" value="#{manterContratoBean.manterContratoContasBean.dataManutencaoInicio}" >
							</app:calendar>
						</br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.label_a}"/>
						<br:brPanelGroup>
							<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'calendarioAte');" id="calendarioAte" value="#{manterContratoBean.manterContratoContasBean.dataManutencaoFim}" >
							</app:calendar>		
						</br:brPanelGroup>	
					</br:brPanelGroup>			    
				</br:brPanelGrid>
			</a4j:outputPanel>		
		</br:brPanelGroup>	
	</br:brPanelGrid>	
		
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.conContasManterContrato_limpar_dados}" action="#{manterContratoBean.manterContratoContasBean.limparDadosRelHistorico}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conContasManterContrato_consultar}" action="#{manterContratoBean.manterContratoContasBean.consultarRelHistorico}"
				onclick="javascript:desbloquearTela(); return validaPesquisarRelacionamentoHistorico(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conContasManterContrato_participante}','#{msgs.conContasManterContrato_banco}', '#{msgs.conContasManterContrato_agencia}',
				 	'#{msgs.conContasManterContrato_conta}','#{msgs.conContasManterContrato_digito_conta}','#{msgs.label_data_manutencao}', '#{msgs.conContasManterContrato_cnpj}', '#{msgs.conContasManterContrato_cpf}' );" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
			<app:scrollableDataTable id="dataTable" value="#{manterContratoBean.manterContratoContasBean.listaGridPesquisaContasRelHistorico}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
					  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>
					<t:selectOneRadio layout="spread" forceId="true" forceIdIndex="false" id="sorLista" styleClass="HtmlSelectOneRadioBradesco" value="#{manterContratoBean.manterContratoContasBean.itemSelecionadoListaContasRelHistorico}" >
						<f:selectItems value="#{manterContratoBean.manterContratoContasBean.listaControleRadioContasRelHistorico}"/>
						<a4j:support event="onclick" reRender="btnDetalhar" />
					</t:selectOneRadio>
					<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
					
			  	<app:scrollableColumn styleClass="colTabRight" width="140px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.subsContasManterContrato_grid_cpf_cnpj}" style="text-align:center;width:140" />
				    </f:facet>
				    <br:brOutputText value="#{result.cpfCnpjParticipanteFormatado}" style="width:140" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="400px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.subsContasManterContrato_grid_nome_razao}" style="text-align:center;width:400" />
				    </f:facet>
				    <br:brOutputText value="#{result.nmParticipante}" style="width:400" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.subsContasManterContrato_grid_banco}" style="text-align:center;width:180" />
				    </f:facet>
				    <br:brOutputText value="#{result.bancoFormatado}" style="width:180" />
			  </app:scrollableColumn>
			  
				<app:scrollableColumn styleClass="colTabLeft" width="180px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.subsContasManterContrato_grid_agencia}" style="text-align:center;width:180" />
				    </f:facet>
				    <br:brOutputText value="#{result.agenciaFormatada}" style="width:180" />
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn styleClass="colTabRight" width="100px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.subsContasManterContrato_grid_conta}" style="text-align:center;width:100" />
				    </f:facet> 
				    <br:brOutputText value="#{result.contaFormatada}" style="width:100" />
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn styleClass="colTabLeft" width="150px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_tipo_de_conta}" style="text-align:center;width:150" />
				    </f:facet> 
				    <br:brOutputText value="#{result.dsTipoConta}" style="width:150" />
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn styleClass="colTabLeft" width="150px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.label_tipo_relacionamento}" style="text-align:center;width:100%" />
				    </f:facet> 
				    <br:brOutputText value="#{result.dsTipoContaRelacionamentoConta}" style="width:100%" />
			  </app:scrollableColumn>
			  
				<app:scrollableColumn styleClass="colTabCenter" width="175px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.grid_data_hora_manutencao}" style="text-align:center;width:175" />
				    </f:facet>
				    <br:brOutputText value="#{result.dataHoraManutencaoFormatada}" style="width:175" />
			  </app:scrollableColumn>		
			  
			  <app:scrollableColumn styleClass="colTabLeft" width="100px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.grid_usuario}" style="text-align:center;width:100" />
				    </f:facet> 
				    <br:brOutputText value="#{result.cdUsuarioManutencao}" style="width:100" />
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn styleClass="colTabLeft" width="150px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.PGIC0142_label_grid_tipoManutencao}" style="text-align:center;width:100%" />
				    </f:facet> 
				    <br:brOutputText value="#{result.dsTipoManutencao}" style="width:100%" />
			  </app:scrollableColumn>	    
			</app:scrollableDataTable>
		</br:brPanelGroup>
		
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterContratoBean.manterContratoContasBean.pesquisarRelacionamentoHistorico}" >
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>		
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.botao_voltar}" action="#{manterContratoBean.manterContratoContasBean.voltarPesquisaRelacionamento}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>	
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterContratoBean.manterContratoContasBean.itemSelecionadoListaContasRelHistorico}"  value="#{msgs.label_detalhar}"  action="#{manterContratoBean.manterContratoContasBean.avancarRelHistoricoDetalhar}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>
	