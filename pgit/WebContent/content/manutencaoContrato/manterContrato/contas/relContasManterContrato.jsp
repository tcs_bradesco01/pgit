<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j"%>

<brArq:form id="relContasManterContrato" name="relContasManterContrato" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_cliente_master}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_nome_razao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerente_responsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_cliente_participante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>
				
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.descricaoContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detContasManterContrato_title_conta_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.codBanco}-#{manterContratoBean.manterContratoContasBean.bancoDesc}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_agencia}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.cdAgencia}-#{manterContratoBean.manterContratoContasBean.agenciaDesc}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.contaDesc}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="EspacamentoLinhas" />
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.tipoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid styleClass="EspacamentoLinhas" />
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_titular}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.cpf}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_razao_social}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.razao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_situacao_vinculo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.situacaoVinculoDesc}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detContasManterContrato_data_vinculacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.dataVinculacaoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
			
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_relacionamento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="tipoRelacionamento" value="#{manterContratoBean.manterContratoContasBean.tipoRelacionamento}" disabled="#{manterContratoBean.manterContratoContasBean.listaGridPesquisaContasRelacionamentos != null}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.conContasManterContrato_label_combo_selecione}"/>
						<f:selectItems value="#{manterContratoBean.manterContratoContasBean.listaCboTipoRelacionamento}" />
						<a4j:support event="onchange" reRender="btnConsultar,btnIncluir" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
			
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimpar" styleClass="bto1" value="#{msgs.relContasManterContrato_btn_limpar}" action="#{manterContratoBean.manterContratoContasBean.limparDadosRelacionamento}" style="margin-right:5px"
    		disabled="#{manterContratoBean.manterContratoContasBean.listaGridPesquisaContasRelacionamentos == null}"	>
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.relContasManterContrato_btn_consultar}" action="#{manterContratoBean.manterContratoContasBean.consultarRelacionamento}" 
				
				 disabled="#{manterContratoBean.manterContratoContasBean.listaGridPesquisaContasRelacionamentos != null}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
				<app:scrollableDataTable id="dataTable" value="#{manterContratoBean.manterContratoContasBean.listaGridPesquisaContasRelacionamentos}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{manterContratoBean.manterContratoContasBean.itemSelecionadoListaContasRelacionamentos}">
						<f:selectItems value="#{manterContratoBean.manterContratoContasBean.listaControleRadioContasRelacionamentos}"/>
						<a4j:support event="onclick" reRender="btnHistorico,btnIncluir,btnSubstituir,btnExcluir" />
					</t:selectOneRadio>
			    	<t:radio for="sor" index="#{parametroKey}" />
				</app:scrollableColumn>
				
			  	<app:scrollableColumn styleClass="colTabRight" width="160px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.relContasManterContrato_grid_cpf_cnpj}" style="text-align:center;width:160" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdCpfCnpj}" style="text-align:center;width:160" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="270px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.relContasManterContrato_grid_nome_razao}" style="text-align:center;width:270" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsPessoa}" style="text-align:center;width:270" />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabRight" width="200px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conContasManterContrato_grid_banco}" style="text-align:center;width:200" />
				    </f:facet>
				    <br:brOutputText value="#{result.bancoFormatado}" style="text-align:center;width:200" />			    				    			    
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn styleClass="colTabRight" width="200px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conContasManterContrato_grid_agencia}" style="text-align:center;width:200" />
				    </f:facet>
				    <br:brOutputText value="#{result.agenciaFormatada}" style="text-align:center;width:200" />					    			    
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn styleClass="colTabRight" width="150px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.conContasManterContrato_grid_conta}" style="text-align:center;width:150" />
				    </f:facet>
				    <br:brOutputText value="#{result.contaFormatada}" style="text-align:center;width:150" />				    			    			    			    				    			    
			  </app:scrollableColumn>
			  
				<app:scrollableColumn styleClass="colTabLeft" width="190px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.relContasManterContrato_grid_tipo_conta}" style="text-align:center;width:190" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoConta}" style="text-align:center;width:190" />
			  </app:scrollableColumn>
			  
				<app:scrollableColumn styleClass="colTabLeft" width="250px" >
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.relContasManterContrato_grid_tipo_relacionamento}" style="text-align:center;width:250" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoRelacionamento}" />
			  </app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>
		
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterContratoBean.manterContratoContasBean.pesquisarRelacionamento}" >
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterContratoBean.manterContratoContasBean.voltarPesquisarContas}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:600px" >
			<br:brCommandButton id="btnHistorico" styleClass="bto1" value="#{msgs.label_historico}" action="#{manterContratoBean.manterContratoContasBean.historicoRelacionamento}" style="margin-right:5px" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
			<br:brCommandButton id="btnIncluir" styleClass="bto1" disabled="false"  value="#{msgs.relContasManterContrato_incluir}"  action="#{manterContratoBean.manterContratoContasBean.incluirRelacionamento}" style="margin-right:5px" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnExcluir" styleClass="bto1" disabled="#{empty manterContratoBean.manterContratoContasBean.itemSelecionadoListaContasRelacionamentos}"  value="#{msgs.btn_excluir}"  action="#{manterContratoBean.manterContratoContasBean.excluirRelacionamento}" style="margin-right:5px" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnSubstituir" styleClass="bto1" disabled="#{empty manterContratoBean.manterContratoContasBean.itemSelecionadoListaContasRelacionamentos}"  value="#{msgs.relContasManterContrato_substituir}"  action="#{manterContratoBean.manterContratoContasBean.substituirRelacionamento}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>
	