<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j"%>

<brArq:form id="incContasManterContrato" name="incContasManterContrato" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="filtroRadio" value="#{manterContratoBean.manterContratoContasBean.radioPesquisa}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />	
				
				<a4j:support event="onclick" action="#{manterContratoBean.manterContratoContasBean.limparArgumentosPesquisa}" reRender="txtCnpj, txtFilial,txtControle, txtCpf, txtControleCpf, banco, agencia, conta, btnConsultar" />			
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_cliente_master}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_nome_razao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_gerente_responsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_cliente_participante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterContratoBean.dsNomeRazaoSocialParticipante}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>
				
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterContratoBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.empresa}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.numero}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.manterContratoContasBean.descricaoContrato}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conContasManterContrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterContratoBean.participacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conContasManterContrato_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="0" />			
		</br:brPanelGroup>
		
	    <br:brPanelGroup>				 		   
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(9,'incContasManterContrato','incContasManterContrato:txtCnpj','incContasManterContrato:txtFilial');" style="margin-right: 5" onkeypress="onlyNum();" size="11" maxlength="9" styleClass="HtmlInputTextBradesco" value="#{manterContratoBean.manterContratoContasBean.cdCpfCnpj}" disabled="#{manterContratoBean.manterContratoContasBean.radioPesquisa == null || manterContratoBean.manterContratoContasBean.radioPesquisa == '' || manterContratoBean.manterContratoContasBean.radioPesquisa != 0}"/>
				    <br:brInputText id="txtFilial" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeyup="proximoCampo(4,'incContasManterContrato','incContasManterContrato:txtFilial','incContasManterContrato:txtControle');" style="margin-right: 5" onkeypress="onlyNum();" size="5" maxlength="4" styleClass="HtmlInputTextBradesco" value="#{manterContratoBean.manterContratoContasBean.cdFilialCnpj}"  disabled="#{manterContratoBean.manterContratoContasBean.radioPesquisa == null || manterContratoBean.manterContratoContasBean.radioPesquisa == '' || manterContratoBean.manterContratoContasBean.radioPesquisa != 0}"/>
					<br:brInputText id="txtControle" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" size="3" maxlength="2" styleClass="HtmlInputTextBradesco" value="#{manterContratoBean.manterContratoContasBean.cdControleCnpj}" disabled="#{manterContratoBean.manterContratoContasBean.radioPesquisa == null || manterContratoBean.manterContratoContasBean.radioPesquisa == '' || manterContratoBean.manterContratoContasBean.radioPesquisa != 0}"/>
			</br:brPanelGrid>
	    </br:brPanelGroup>				 			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="filtroRadio" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" style="margin-right: 5" onkeypress="onlyNum();"  disabled="#{manterContratoBean.manterContratoContasBean.radioPesquisa == null || manterContratoBean.manterContratoContasBean.radioPesquisa == '' || manterContratoBean.manterContratoContasBean.radioPesquisa != 1}" value="#{manterContratoBean.manterContratoContasBean.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'incContasManterContrato','incContasManterContrato:txtCpf','incContasManterContrato:txtControleCpf');" />
			    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" disabled="#{manterContratoBean.manterContratoContasBean.radioPesquisa == null || manterContratoBean.manterContratoContasBean.radioPesquisa == '' || manterContratoBean.manterContratoContasBean.radioPesquisa != 1}" value="#{manterContratoBean.manterContratoContasBean.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="filtroRadio" index="2" />			
		</br:brPanelGroup>
		<br:brPanelGroup>								
			<br:brPanelGrid columns="7" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_banco}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brInputText id="banco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" 
								disabled="#{manterContratoBean.manterContratoContasBean.radioPesquisa == null || manterContratoBean.manterContratoContasBean.radioPesquisa == '' || manterContratoBean.manterContratoContasBean.radioPesquisa != 2}" 
								value="#{manterContratoBean.manterContratoContasBean.banco}" size="10" maxlength="3" >
							</br:brInputText>	
						</br:brPanelGroup>					
					</br:brPanelGrid>
				</br:brPanelGroup>	
		
				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
				</br:brPanelGroup>		
		
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_agencia}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brInputText id="agencia" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" 
								disabled="#{manterContratoBean.manterContratoContasBean.radioPesquisa == null || manterContratoBean.manterContratoContasBean.radioPesquisa == '' || manterContratoBean.manterContratoContasBean.radioPesquisa != 2}"
								value="#{manterContratoBean.manterContratoContasBean.agencia}" size="12" maxlength="5" >
							</br:brInputText>	
						</br:brPanelGroup>		
					    							
					</br:brPanelGrid>
				</br:brPanelGroup>	
		
				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
				</br:brPanelGroup>		
		
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conContasManterContrato_conta}:"/>
						</br:brPanelGroup>
					</br:brPanelGrid>
					
				    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
		
				    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
					    <br:brPanelGroup>
							<br:brInputText id="conta" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" 
								disabled="#{manterContratoBean.manterContratoContasBean.radioPesquisa == null || manterContratoBean.manterContratoContasBean.radioPesquisa == '' || manterContratoBean.manterContratoContasBean.radioPesquisa != 2}"
								value="#{manterContratoBean.manterContratoContasBean.conta}" size="17" maxlength="13" >
							</br:brInputText>	
						</br:brPanelGroup>
			    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>
								</br:brPanelGroup>
						</br:brPanelGrid>	
					   					
					</br:brPanelGrid>
				</br:brPanelGroup>	
		
				<br:brPanelGroup style="width:20px; margin-bottom:5px" >
				</br:brPanelGroup>		
		
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
		
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.conContasManterContrato_limpar_dados}" action="#{manterContratoBean.manterContratoContasBean.limparDadosIncluir}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conContasManterContrato_consultar}" action="#{manterContratoBean.manterContratoContasBean.consultarIncluir}" 
				onclick="javascript:desbloquearTela(); return validaPesquisarContasIncluir('#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conContasManterContrato_cnpj}','#{msgs.conContasManterContrato_cpf}','#{msgs.conContasManterContrato_banco}', '#{msgs.conContasManterContrato_agencia}',
	   		'#{msgs.conContasManterContrato_conta}','#{msgs.conContasManterContrato_digito_conta}','#{msgs.conContasManterContrato_tipo_conta}');" disabled="#{ (manterContratoBean.manterContratoContasBean.radioPesquisa != 0 && 
	   		manterContratoBean.manterContratoContasBean.radioPesquisa != 1 && manterContratoBean.manterContratoContasBean.radioPesquisa != 2 ) || manterContratoBean.manterContratoContasBean.radioPesquisa == '' }"			>
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim> <br> </f:verbatim> 

	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
				<app:scrollableDataTable id="dataTable" value="#{manterContratoBean.manterContratoContasBean.listaGridPesquisaContasIncluir}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{manterContratoBean.manterContratoContasBean.itemSelecionadoListaContasIncluir}">
						<f:selectItems value="#{manterContratoBean.manterContratoContasBean.listaControleRadioContasIncluir}"/>
						<a4j:support event="onclick" reRender="btnSelecionar" />
					</t:selectOneRadio>
			    	<t:radio for="sor" index="#{parametroKey}" />
				</app:scrollableColumn>
				
			  	<app:scrollableColumn styleClass="colTabRight" width="160px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.subsContasManterContrato_grid_cpf_cnpj}" style="width:160px; text-align:center" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdCpfCnpj}"  />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="270px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.subsContasManterContrato_grid_nome_razao}" style="width:270; text-align:center"  />
			    </f:facet>
			    <br:brOutputText value="#{result.nmParticipante}"  />
				</app:scrollableColumn>
				
				<app:scrollableColumn styleClass="colTabLeft" width="270px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.subsContasManterContrato_grid_banco}" style="width:270px; text-align:center"  />
			    </f:facet>
			    <br:brOutputText value="#{result.bancoFormatado}" />
			  </app:scrollableColumn>
			  
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.subsContasManterContrato_grid_agencia}" style="width:200; text-align:center"  />
			    </f:facet>
			    <br:brOutputText value="#{result.agenciaFormatada}"  />
			  </app:scrollableColumn>
			  
				<app:scrollableColumn styleClass="colTabRight" width="90px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.subsContasManterContrato_grid_conta}" style="width:90; text-align:center"  />
			    </f:facet>
			    <br:brOutputText value="#{result.contaFormatada}" />
			  </app:scrollableColumn>
			  
				<app:scrollableColumn styleClass="colTabLeft" width="200px" >
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.subsContasManterContrato_grid_tipo_conta}" style="width:200; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoConta}"  />
			  </app:scrollableColumn>	
			</app:scrollableDataTable>
		</br:brPanelGroup>
		
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterContratoBean.manterContratoContasBean.pesquisarIncluir}" >
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>		
	
	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterContratoBean.manterContratoContasBean.voltarPesquisarContas}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnLimpar" styleClass="bto1" disabled="#{manterContratoBean.manterContratoContasBean.listaGridPesquisaContasIncluir == null}"  value="#{msgs.subsContasManterContrato_btn_limpar}"  action="#{manterContratoBean.manterContratoContasBean.limparGridIncluir}" style="margin-right:5px" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			<br:brCommandButton id="btnSelecionar" styleClass="bto1" disabled="#{empty manterContratoBean.manterContratoContasBean.itemSelecionadoListaContasIncluir}" value="#{msgs.subsContasManterContrato_btn_selecionar}"  action="#{manterContratoBean.manterContratoContasBean.selecionarIncluir}" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<t:inputHidden value="#{manterContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoContasIncluir(document.forms[1], document.getElementById('incContasManterContrato:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>