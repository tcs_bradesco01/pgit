<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incEquiparacaoEntreContratosForm" name="incEquiparacaoEntreContratosForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.efetuarEquiparaçãoEntreContratos_title_contrato_a}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.empresaGestoraContratoA}"/>		
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_contrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.tipoContratoA}"/>	
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_numero}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.numeroContratoA}"/>	
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.efetuarEquiparaçãoEntreContratos_title_contrato_b}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.empresaGestoraContratoB}"/>		
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_contrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.tipoContratoB}"/>	
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_numero}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.numeroContratoB}"/>	
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
				<app:scrollableDataTable id="dataTable" value="#{efetuaEquiparacaoEntreContratosBean.gridListaInclusao}" var="result" 
					rows="10" rowIndexVar="parametroKey" 
					rowClasses="tabela_celula_normal, tabela_celula_destaque" 
					width="100%">
										
					<app:scrollableColumn styleClass="colTabLeft" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.efetuarEquiparaçãoEntreContratos_tipo_servico}" style="text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsProdutoServicoOperacao}"/>
					</app:scrollableColumn>
					<app:scrollableColumn styleClass="colTabLeft" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.efetuarEquiparaçãoEntreContratos_modalidade}" style="text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsProdutoOperRelacionado}"/>
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" >
				 <f:facet name="first">
				    <brArq:pdcCommandButton id="primeira"
				      styleClass="bto1"
				      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton id="proxima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton id="ultima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  </f:facet>
				</brArq:pdcDataScroller> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
			<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup style="text-align:left;width:150px" >
					<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.conServicosManterContrato_btnVoltar}" action="#{efetuaEquiparacaoEntreContratosBean.voltarModalidade}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>				
				</br:brPanelGroup>
				<br:brPanelGroup style="text-align:right;width:600px" >										
					<br:brCommandButton id="btnConfirmar" styleClass="bto1"  value="#{msgs.efetuarEquiparaçãoEntreContratos_btn_confirmar}" action="#{efetuaEquiparacaoEntreContratosBean.confirmar}" onclick="javascript: if (!confirm('Confirma Inclusão?')) { desbloquearTela(); return false; }" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>
		
		<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>