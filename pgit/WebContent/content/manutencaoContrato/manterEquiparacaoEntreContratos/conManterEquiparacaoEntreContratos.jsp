<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterEquiparacaoEntreContratosForm" name="conManterEquiparacaoEntreContratosForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_identificacao_contrato_a}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolManutContratos_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="empresaGestora" value="#{efetuaEquiparacaoEntreContratosBean.empresaGestoraFiltroA}" disabled="#{efetuaEquiparacaoEntreContratosBean.habilitaCamposContratoA}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" >
							<f:selectItems  value="#{efetuaEquiparacaoEntreContratosBean.identificacaoClienteContratoBean.listaEmpresaGestora}" />								
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
	
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
	
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_tipo_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="tipoContrato" value="#{efetuaEquiparacaoEntreContratosBean.tipoContratoFiltroA}"  disabled="#{efetuaEquiparacaoEntreContratosBean.habilitaCamposContratoA}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
							<f:selectItems  value="#{efetuaEquiparacaoEntreContratosBean.identificacaoClienteContratoBean.listaTipoContrato}" />
						</br:brSelectOneMenu>
						
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_numero}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{efetuaEquiparacaoEntreContratosBean.numeroFiltroA}"  disabled="#{efetuaEquiparacaoEntreContratosBean.habilitaCamposContratoA}"size="12" maxlength="10" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
							<%--<a4j:support event="onblur" action="#{manterContratoBean.identificacaoClienteContratoBean.desabilitaCamposContrato}"	reRender="habilitaCampos,situacao, agenciaOperadora, gerenteId, gerenteNome" />--%>		
					    </br:brInputText>	
					</br:brPanelGroup>					
				</br:brPanelGrid>										
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{efetuaEquiparacaoEntreContratosBean.limparDadosPesquisaContratoA}"  style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" onclick="return validaCamposContratoA(document.forms[1],'#{msgs.label_ocampo}', 
														'#{msgs.label_necessario}', '#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}',
														'#{msgs.identificacaoClienteContrato_tipo_contrato}', '#{msgs.identificacaoClienteContrato_numero}');" 
					styleClass="bto1" disabled="#{efetuaEquiparacaoEntreContratosBean.habilitaCamposContratoA}" value="#{msgs.btn_consultar_contrato}" action="#{efetuaEquiparacaoEntreContratosBean.consultarContratoA}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
					
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_identificacao_contrato_b}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolManutContratos_argumentos_pesquisa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="empresaGestoraB" value="#{efetuaEquiparacaoEntreContratosBean.empresaGestoraFiltroB}" disabled="#{efetuaEquiparacaoEntreContratosBean.habilitaCamposContratoB}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" >
							<f:selectItems  value="#{efetuaEquiparacaoEntreContratosBean.identificacaoClienteContratoBean.listaEmpresaGestora}" />								
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
	
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
	
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_tipo_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="tipoContratoB" value="#{efetuaEquiparacaoEntreContratosBean.tipoContratoFiltroB}" disabled="#{efetuaEquiparacaoEntreContratosBean.habilitaCamposContratoB}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
							<f:selectItems  value="#{efetuaEquiparacaoEntreContratosBean.identificacaoClienteContratoBean.listaTipoContrato}" />
						</br:brSelectOneMenu>
						
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_numero}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="numeroB" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{efetuaEquiparacaoEntreContratosBean.numeroFiltroB}"size="12" maxlength="10" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" disabled="#{efetuaEquiparacaoEntreContratosBean.habilitaCamposContratoB}">
							<%--<a4j:support event="onblur" action="#{manterContratoBean.identificacaoClienteContratoBean.desabilitaCamposContrato}"	reRender="habilitaCampos,situacao, agenciaOperadora, gerenteId, gerenteNome" />--%>		
					    </br:brInputText>	
					</br:brPanelGroup>					
				</br:brPanelGrid>										
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCamposB" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{efetuaEquiparacaoEntreContratosBean.limparDadosPesquisaContratoB}"  style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultarB" onclick="return validaCamposContratoB(document.forms[1],'#{msgs.label_ocampo}', 
														'#{msgs.label_necessario}', '#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}',
														'#{msgs.identificacaoClienteContrato_tipo_contrato}', '#{msgs.identificacaoClienteContrato_numero}');"  
					styleClass="bto1" value="#{msgs.btn_consultar_contrato}" disabled="#{efetuaEquiparacaoEntreContratosBean.habilitaCamposContratoB}" 
					action="#{efetuaEquiparacaoEntreContratosBean.consultarContratoB}" style="cursor:hand;" disabled="#{efetuaEquiparacaoEntreContratosBean.habilitaCamposContratoB}" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
					
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.efetuarEquiparaçãoEntreContratos_title_contrato_a}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.empresaGestoraContratoA}"/>		
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_contrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.tipoContratoA}"/>	
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_numero}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.numeroContratoA}"/>	
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.efetuarEquiparaçãoEntreContratos_title_contrato_b}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_empresa_gestora_contrato}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.empresaGestoraContratoB}"/>		
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tipo_contrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.tipoContratoB}"/>	
			</br:brPanelGroup>				
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup style="margin-right:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.alteraTipoClassificacaoParticipantesContrato_label_numero}:" />	
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{efetuaEquiparacaoEntreContratosBean.numeroContratoB}"/>	
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		

		<a4j:region id="regionFiltro">
			<t:selectOneRadio id="rdoServModal" value="#{efetuaEquiparacaoEntreContratosBean.itemFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{efetuaEquiparacaoEntreContratosBean.disabledCopiaModalidade}">  
				<f:selectItem itemValue="0" itemLabel="SIM" />
				<f:selectItem itemValue="1" itemLabel="NÃO" />	
				<a4j:support event="onclick" reRender="btnAvancar" ajaxSingle="true"  action="#{efetuaEquiparacaoEntreContratosBean.habilitaBotaoAvancar}"/>										
			</t:selectOneRadio>
		</a4j:region>

		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.etuarEquiparaçãoEntreContratos_label_confirmacao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<t:radio for="rdoServModal" index="0" />
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<t:radio for="rdoServModal" index="1" />
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.efetuarEquiparaçãoEntreContratos_title_tipo_servicos}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
				<app:scrollableDataTable id="dataTable" value="#{efetuaEquiparacaoEntreContratosBean.listarServicoEquiparacaoContratoSaida.ocorrencias}" var="result" 
					rows="10" rowIndexVar="parametroKey" 
					rowClasses="tabela_celula_normal, tabela_celula_destaque" 
					width="100%">
					
					<app:scrollableColumn width="25" styleClass="colTabCenter">
						<f:facet name="header">
							<t:selectBooleanCheckbox disabled="#{empty efetuaEquiparacaoEntreContratosBean.listarServicoEquiparacaoContratoSaida.ocorrencias}" 
								id="checkSelecionarTodos" styleClass="HtmlSelectOneRadioBradesco" value="#{efetuaEquiparacaoEntreContratosBean.opcaoChecarTodos}" 
								onclick="javascritp:selecionarTodos(document.forms[1],this);">
	  			        	</t:selectBooleanCheckbox>
						</f:facet>
					    <t:selectBooleanCheckbox  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" value="#{result.check}">
							<f:selectItems value="#{efetuaEquiparacaoEntreContratosBean.selectItemListaTipoServico}"/>						
							<a4j:support event="onclick" reRender="btnAvancar" action="#{efetuaEquiparacaoEntreContratosBean.habilitaBotaoAvancar}"/>						
						</t:selectBooleanCheckbox>
					</app:scrollableColumn>	
					<app:scrollableColumn styleClass="colTabLeft" >			
					    <f:facet name="header">
					      <br:brOutputText value="#{msgs.efetuarEquiparaçãoEntreContratos_tipo_servico}" style="text-align:center"/>
					    </f:facet>
					    <br:brOutputText value="#{result.dsProdutoOperRelacionado}"/>
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{efetuaEquiparacaoEntreContratosBean.pesquisar}" >
				 <f:facet name="first">
				    <brArq:pdcCommandButton id="primeira"
				      styleClass="bto1"
				      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton id="proxima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton id="ultima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  </f:facet>
				</brArq:pdcDataScroller> 
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<a4j:outputPanel id="panelListaServico" ajaxRendered="true">
			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" style="text-align: right">				
				<br:brPanelGroup>											
					<br:brCommandButton id="btnLimpar" styleClass="bto1" style="margin-right:5px" value="#{msgs.efetuarEquiparaçãoEntreContratos_btn_limpar}" action="#{efetuaEquiparacaoEntreContratosBean.limparListaServico}" onclick="tiraSelecionarTodos(document.forms[1]);">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnAvancar" disabled="#{efetuaEquiparacaoEntreContratosBean.habilitarServicoAvancar}" styleClass="bto1" style="align:right" value="#{msgs.efetuarEquiparaçãoEntreContratos_btn_avancar}"
								onclick="desbloquearTela();" onmouseout="javascript:alteraBotao('normal', this.id);" action="#{efetuaEquiparacaoEntreContratosBean.avancar}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>
				
		<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>