 <%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="pesquisarManterPendenciasContratoForm" name="pesquisarManterPendenciasContratoForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterPendenciasContrato_cliente_representante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_cpf_cnpj_master}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.cpfCnpjClienteMaster}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_nome_razao_social_master}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterPendenciasContratoBean.nomeRazaoClienteMaster}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_grupo_economico}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.grupoEconomicoClienteMaster}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_atividade_economica}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterPendenciasContratoBean.atividadeEconomicaClienteMaster}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.segmentoClienteMaster}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_sub_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.subSegmentoClienteMaster}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.dsAgenciaOperadora}"/>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.gerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>		
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterPendenciasContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_empresa_gestora}:"/>
			//<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.empresa}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_contrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_contrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.numeroContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_label_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.dsDescricaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_contrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_contrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterPendenciasContrato_contrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterPendenciasContratoBean.participacao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	

	<f:verbatim><hr class="lin"></f:verbatim>
	
	
	
  <br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 	
			<app:scrollableDataTable  id="dataTable" value="#{manterPendenciasContratoBean.listaGridPesquisa}" var="result" rows="10" rowIndexVar="parametroKey"
			rowClasses="tabela_celula_normal, tabela_celula_destaque"  width="750px">
			 <app:scrollableColumn width="30px" styleClass="colTabCenter">
				 <f:facet name="header">
			      <br:brOutputText value=""  escape="false"  styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>	
			    
				<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
				  value="#{manterPendenciasContratoBean.itemSelecionadoLista}">  
					<f:selectItems value="#{manterPendenciasContratoBean.listaControleRadio}"/>
					<a4j:support event="onclick" reRender="panelBotoes" action="#{manterPendenciasContratoBean.habilitaRegistrar}" />					
				</t:selectOneRadio>
				<t:radio for="sor" index="#{parametroKey}" />
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="250px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.conManterPendenciasContrato_tipo}" style="text-align:center;width:250"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipo}" />
			  </app:scrollableColumn>
			  
			 <app:scrollableColumn width="100px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conManterPendenciasContrato_tipo_baixa}" style="text-align:center;width:100"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoBaixa}"/>
			  </app:scrollableColumn>
			  
			 <app:scrollableColumn width="150px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conManterPendenciasContrato_data_geracao}" style="text-align:center;width:150"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dataHoraGeracaoFormatada}"/>
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="200px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conManterPendenciasContrato_situacao}" style="text-align:center;width:200"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsSituacaoPendContrato}"/>
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="90px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conManterPendenciasContrato_contrato_motivo}" style="text-align:center;width:90"/>
			    </f:facet>
			    <br:brOutputText value=""/>
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="90px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conManterPendenciasContrato_data_baixa}" style="text-align:center;width:90"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dataBaixaPendContrato}"/>
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="250px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conManterPendenciasContrato_tipo_unidade_organizacinal}" style="text-align:center;width:250"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsTipoUnidOrg}"/>
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="200px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conManterPendenciasContrato_contrato_empresa}" style="text-align:center;width:200"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsEmpresa}"/>
			  </app:scrollableColumn>
			  
			  <app:scrollableColumn width="200px"  styleClass="colTabLeft">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.conManterPendenciasContrato_unidade_organizacional}" style="text-align:center;width:200"/>
			    </f:facet>
			    <br:brOutputText value="#{result.dsUnidadeOrg}"/>
			  </app:scrollableColumn>
			  
			</app:scrollableDataTable>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" > 	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterPendenciasContratoBean.pesquisar}" >
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			       styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>

 	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="2" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >
		
		 	<br:brPanelGroup style="text-align:left;width:530px"  >
					<br:brCommandButton styleClass="bto1" value="#{msgs.conManterPendenciasContrato_btn_voltar}" action="#{manterPendenciasContratoBean.voltarConsulta}" >	
						<brArq:submitCheckClient/>
					</br:brCommandButton>
			</br:brPanelGroup>	
		
			<br:brPanelGroup >
				<br:brCommandButton id="btnReenviarEmail"  styleClass="bto1" style="margin-right:5px" value="#{msgs.label_reenviar_email}"  action="#{manterPendenciasContratoBean.reenviarEmail}" disabled="#{manterPendenciasContratoBean.desabilitaReenviarEmail}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnLimpar"  styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterPendenciasContrato_btn_limpar}"  action="#{manterPendenciasContratoBean.limparTela}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnDetalhar" disabled="#{empty manterPendenciasContratoBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterPendenciasContrato_btn_detalhar}"   action="#{manterPendenciasContratoBean.detalhar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnRegistrarBaixa" disabled="#{!(manterPendenciasContratoBean.habilitaRegistrar)}" styleClass="bto1" value="#{msgs.conManterPendenciasContrato_btn_registrar_baixa}"  action="#{manterPendenciasContratoBean.registrarBaixa}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			
		</br:brPanelGrid>
	</a4j:outputPanel>	


</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>