<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="conManterSolManutContratoForm" name="conManterSolManutContratoForm" >
<h:inputHidden id="hiddenObrigatoriedade" value="#{imprimirContratoBean.identificacaoClienteContratoBean.obrigatoriedade}"/>
<h:inputHidden id="habilitaCampos" value="#{imprimirContratoBean.identificacaoClienteContratoBean.habilitaCampos}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{imprimirContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{imprimirContratoBean.identificacaoClienteContratoBean.bloqueiaRadio}">  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
	</t:selectOneRadio>
	</a4j:region>
	
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_cnpj}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
			    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'conManterSolManutContratoForm','conManterSolManutContratoForm:txtCnpj','conManterSolManutContratoForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{imprimirContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || imprimirContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{imprimirContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"/>
			    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'conManterSolManutContratoForm','conManterSolManutContratoForm:txtFilial','conManterSolManutContratoForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{imprimirContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || imprimirContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{imprimirContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" />
				<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{imprimirContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || imprimirContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{imprimirContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" />
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0">
		<t:radio for="rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_cpf}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
			    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{imprimirContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || imprimirContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente }" value="#{imprimirContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
			    onkeyup="proximoCampo(9,'conManterSolManutContratoForm','conManterSolManutContratoForm:txtCpf','conManterSolManutContratoForm:txtControleCpf');" />
			    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{imprimirContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || imprimirContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{imprimirContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" >
		<t:radio for="rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{imprimirContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '2' || imprimirContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{imprimirContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
		
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  border="0">
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_banco}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{imprimirContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || imprimirContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{imprimirContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'conManterSolManutContratoForm','conManterSolManutContratoForm:txtBanco','conManterSolManutContratoForm:txtAgencia');"/>
			
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_agencia}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{imprimirContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || imprimirContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{imprimirContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'conManterSolManutContratoForm','conManterSolManutContratoForm:txtAgencia','conManterSolManutContratoForm:txtConta');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_conta}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{imprimirContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || imprimirContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{imprimirContratoBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" />
		</a4j:outputPanel>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
    		<br:brCommandButton id="btnLimparCampos2" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{imprimirContratoBean.identificacaoClienteContratoBean.limparDadosCliente}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
    		
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty imprimirContratoBean.identificacaoClienteContratoBean.itemFiltroSelecionado || imprimirContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{msgs.identificacaoClienteContrato_label_consultar_cliente}" action="#{imprimirContratoBean.identificacaoClienteContratoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"
	   		
	   		onclick="javascript:desbloquearTela(); 
	   		return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.identificacaoClienteContrato_cnpj}','#{msgs.identificacaoClienteContrato_cpf}', '#{msgs.identificacaoClienteContrato_label_nomeRazao}',
	   		'#{msgs.identificacaoClienteContrato_banco}','#{msgs.identificacaoClienteContrato_agencia}','#{msgs.identificacaoClienteContrato_conta}');"
	   		
	   		 >		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_cliente}:"/>
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{imprimirContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{imprimirContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolManutContratos_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
	<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="empresaGestora" value="#{imprimirContratoBean.identificacaoClienteContratoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{!imprimirContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.identificacaoClienteContrato_selecione}"/>
						<f:selectItems  value="#{imprimirContratoBean.identificacaoClienteContratoBean.listaEmpresaGestora}" />								
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_tipo_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brSelectOneMenu id="tipoContrato" value="#{imprimirContratoBean.identificacaoClienteContratoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{!imprimirContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.identificacaoClienteContrato_selecione}"/>
						<f:selectItems  value="#{imprimirContratoBean.identificacaoClienteContratoBean.listaTipoContrato}" />
					</br:brSelectOneMenu>
					
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_numero}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="numero" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{imprimirContratoBean.identificacaoClienteContratoBean.numeroFiltro}" size="10" maxlength="10" readonly="#{!imprimirContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
					<a4j:support event="onblur" action="#{imprimirContratoBean.identificacaoClienteContratoBean.desabilitaCamposContrato}"
									reRender="habilitaCampos,situacao, agenciaOperadora, gerenteId, btnConsultarGerente, gerenteNome "  />							
					<a4j:support event="onkeyup" action="#{imprimirContratoBean.identificacaoClienteContratoBean.desabilitaCamposContrato}"	reRender="habilitaCampos,situacao, agenciaOperadora, gerenteId, btnConsultarGerente, gerenteNome" />		
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{msgs.identificacaoClienteContrato_agencia_operadora}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="agenciaOperadora" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco" value="#{imprimirContratoBean.identificacaoClienteContratoBean.agenciaOperadoraFiltro}" size="30" maxlength="30" 
					readonly="#{imprimirContratoBean.identificacaoClienteContratoBean.habilitaCampos || !imprimirContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
					
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	

		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>		

		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_gerente_responsavel}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
					<br:brInputText id="gerenteId" onkeypress="javascript:limparNomeGerente(document.forms[1]);" onkeydown="javascript:limparNomeGerente(document.forms[1]);" styleClass="HtmlInputTextBradesco" value="#{imprimirContratoBean.identificacaoClienteContratoBean.codigoFunc}" size="15" maxlength="15" readonly="#{imprimirContratoBean.identificacaoClienteContratoBean.habilitaCampos || !imprimirContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<brArq:commonsValidator type="integer" arg="#{msgs.identificacaoClienteContrato_gerente_responsavel}" server="false" client="true" />
						<a4j:support event="onkeyup" reRender="btnConsultarGerente,gerenteNome"  />
					</br:brInputText>	
				</br:brPanelGroup>
	    		<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
			    <br:brPanelGroup>
					<br:brInputText id="gerenteNome" styleClass="HtmlInputTextBradesco" value="#{imprimirContratoBean.identificacaoClienteContratoBean.funcionarioDTO.dsFuncionario}" size="50" maxlength="50" readonly="true"  />				
				</br:brPanelGroup>		
				<br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>	
				<br:brPanelGroup>
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >							
						<br:brCommandButton id="btnConsultarGerente" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_buscar}" action="#{imprimirContratoBean.identificacaoClienteContratoBean.carregaListaFuncionarios}" onclick="javascript:return validateForm(document.forms[1]);" disabled="#{(!empty imprimirContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado && imprimirContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado!='' ) || imprimirContratoBean.identificacaoClienteContratoBean.habilitaCampos || (imprimirContratoBean.identificacaoClienteContratoBean.codigoFunc == '')}">
							<brArq:submitCheckClient/>
						</br:brCommandButton>
					</br:brPanelGrid>				
				</br:brPanelGroup>	
					
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_situacao_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>
			    	<br:brSelectOneMenu id="situacao" value="#{imprimirContratoBean.identificacaoClienteContratoBean.situacaoFiltro}" disabled="#{imprimirContratoBean.identificacaoClienteContratoBean.habilitaCampos || !imprimirContratoBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.identificacaoClienteContrato_selecione}"/>
						<f:selectItems  value="#{imprimirContratoBean.identificacaoClienteContratoBean.listaSituacaoContrato}" />						</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_limpar_dados}" action="#{imprimirContratoBean.identificacaoClienteContratoBean.limparDadosPesquisaContrato}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" 



			
			
			onclick="javascript:desbloquearTela(); 
			return validaCamposContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{imprimirContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}', '#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}',
	   		'#{msgs.identificacaoClienteContrato_tipo_contrato}', '#{msgs.identificacaoClienteContrato_mensagem_obrigatorio}', '#{msgs.identificacaoClienteContrato_obrigatorio_consulta_gerente}');" styleClass="bto1" value="#{msgs.identificacaoClienteContrato_consultar}" action="#{imprimirContratoBean.identificacaoClienteContratoBean.consultarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
		
	<f:verbatim><br></f:verbatim> 

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 

			<app:scrollableDataTable id="dataTable" value="#{imprimirContratoBean.identificacaoClienteContratoBean.listaGridPesquisa}" var="result" 
				rows="10" rowIndexVar="parametroKey" 
				rowClasses="tabela_celula_normal, tabela_celula_destaque" 
				width="100%">
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:25; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" 
						layout="spread" forceId="true" forceIdIndex="false" 
						value="#{imprimirContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista}">
						<f:selectItems value="#{imprimirContratoBean.identificacaoClienteContratoBean.listaControleRadio}"/>
						<a4j:support event="onclick" reRender="panelBotoes" />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
				</app:scrollableColumn>
			
				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <br:brOutputText value="#{msgs.identificacaoClienteContrato_club_representante}" style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cdClubRepresentante}" styleClass="tableFontStyle"/>
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_cpf_cnpj_representante}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.cnpjOuCpfFormatado}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_nome_razao_social}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.nmRazaoSocialRepresentante}" />
				</app:scrollableColumn>				
				
				<app:scrollableColumn styleClass="colTabLeft" width="300px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_empresa_gestora}"  style="width:300; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsPessoaJuridica}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_tipo_contrato}" style="width:200; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.dsTipoContrato}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabRight" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_numero}"  style="width:150; text-align:center"/>
				    </f:facet>
				    <br:brOutputText value="#{result.nrSequenciaContrato}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="200px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_agencia_operadora}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsAgenciaOperadora}" />
				</app:scrollableColumn>				

				<app:scrollableColumn styleClass="colTabLeft" width="150px" >			
				    <f:facet name="header">
				      <h:outputText value="#{msgs.identificacaoClienteContrato_situacao_contrato}" style="width:150; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dsSituacaoContrato}" />
				</app:scrollableColumn>				
				
			</app:scrollableDataTable>
			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{imprimirContratoBean.pesquisar}" >
			 <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
	 	<br:brPanelGrid columns="2" style="text-align:right" cellpadding="0" cellspacing="0" border="0">	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparTela" styleClass="bto1" value="#{msgs.conManterSolManutContratos_limpar}" style="margin-right:5px" action="#{imprimirContratoBean.identificacaoClienteContratoBean.limparTela}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brCommandButton id="btnDocumentos" styleClass="bto1" style="align:left" value="Documentos" action="#{imprimirContratoBean.selecionarDocumento}" disabled="#{empty imprimirContratoBean.identificacaoClienteContratoBean.itemSelecionadoLista}" >
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
		</br:brPanelGrid>		
	</a4j:outputPanel>
	
	<t:inputHidden value="#{imprimirContratoBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conManterSolManutContratoForm:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>
