<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="selecionarDocumentoImprimirContrato" name="selecionarDocumentoImprimirContrato" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="radioFiltro"  value="#{imprimirContratoBean.radioArgumentoPesquisaSelecionarDocumento}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<a4j:support event="onclick" action="#{imprimirContratoBean.limparDadosFiltro}"  reRender="numeroDocumento, panelDataGeracao, versao"/>				
	    	</t:selectOneRadio>	    	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_cliente_master}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_cpf_cnpj1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_nome_razao_social1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	
   		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	

	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.empresa}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.numero}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterLimitesContrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.dsDescricaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>



   <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.motivoDesc}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>			
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
  	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirContratoBean.participacao}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
		
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="0" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_numero_documento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brInputText id="numeroDocumento" disabled="#{imprimirContratoBean.radioArgumentoPesquisaSelecionarDocumento != 0}" styleClass="HtmlInputTextBradesco" value="#{imprimirContratoBean.numeroDocumentoFiltro}" size="30" maxlength="10" >
				    </br:brInputText>
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_versao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brInputText id="versao" disabled="#{imprimirContratoBean.radioArgumentoPesquisaSelecionarDocumento != 0}" styleClass="HtmlInputTextBradesco" value="#{imprimirContratoBean.versaoFiltro}" size="10" maxlength="10" >
				    </br:brInputText>
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="1" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.selecionarDocumentoImprimirContrato_data_geracao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		    	<br:brPanelGroup id="panelDataGeracao">
			    	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"  >					
					    <br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{imprimirContratoBean.radioArgumentoPesquisaSelecionarDocumento != 1}" value="#{imprimirContratoBean.diaFiltro}" style="margin-right: 5px" onkeypress="onlyNum();" size="3" maxlength="2" id="txtDia" onkeyup="proximoCampo(2,'selecionarDocumentoImprimirContrato','selecionarDocumentoImprimirContrato:txtDia','selecionarDocumentoImprimirContrato:txtMes');" onblur="validarData('selecionarDocumentoImprimirContrato','selecionarDocumentoImprimirContrato:txtDia', 'dia');"/>
					    <br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{imprimirContratoBean.radioArgumentoPesquisaSelecionarDocumento != 1}" value="#{imprimirContratoBean.mesFiltro}" style="margin-right: 5px" onkeypress="onlyNum();" size="3" maxlength="2" id="txtMes" onkeyup="proximoCampo(2,'selecionarDocumentoImprimirContrato','selecionarDocumentoImprimirContrato:txtMes','selecionarDocumentoImprimirContrato:txtAno');" onblur="validarData('selecionarDocumentoImprimirContrato','selecionarDocumentoImprimirContrato:txtMes', 'mes');"/>
						<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{imprimirContratoBean.radioArgumentoPesquisaSelecionarDocumento != 1}" value="#{imprimirContratoBean.anoFiltro}" onkeypress="onlyNum();" size="5" maxlength="4" id="txtAno" onblur="validarData('selecionarDocumentoImprimirContrato','selecionarDocumentoImprimirContrato:txtAno', 'ano');"  />
					</br:brPanelGrid>	
				</br:brPanelGroup>	
			</br:brPanelGrid>		 	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.selecionarDocumentoImprimirContrato_btn_limpar_dados}" action="#{imprimirContratoBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.selecionarDocumentoImprimirContrato_btn_consultar}" action="#{imprimirContratoBean.carregaListaDocumentos}"
					   		onclick="javascript: desbloquearTela(); return validaCamposFiltro(document.forms[1],'#{msgs.label_ocampo}','#{msgs.label_necessario}','#{msgs.selecionarDocumentoImprimirContrato_numero_documento}','#{msgs.selecionarDocumentoImprimirContrato_data_geracao}', '#{msgs.selecionarDocumentoImprimirContrato_versao}');">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
		
			<app:scrollableDataTable id="dataTable" value="#{imprimirContratoBean.listaDocumentos}" var="result" 
			rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >

			<app:scrollableColumn styleClass="colTabCenter" width="30px">
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>
				<t:selectOneRadio id="sor2" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
					value="#{imprimirContratoBean.itemSelecionadoListaDocumentos}">
					<f:selectItems value="#{imprimirContratoBean.listaControleDocumentos}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sor2" index="#{parametroKey}" />
			</app:scrollableColumn>
			
			<app:scrollableColumn styleClass="colTabRight" width="125px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarDocumentoImprimirContrato_grid_documento}" style="text-align:center;width:125px" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdDocumento}"/>
			 </app:scrollableColumn>
			
			<app:scrollableColumn styleClass="colTabRight" width="125px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarDocumentoImprimirContrato_grid_versao}" style="text-align:center;width:125px" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdVersao}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn styleClass="colTabLeft" width="125px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarDocumentoImprimirContrato_grid_data_geracao}" style="text-align:center;width:125px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsDataGeracao}"/>
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn styleClass="colTabLeft" width="125px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarDocumentoImprimirContrato_grid_idioma}" style="text-align:center;width:125px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsIdioma}"/>
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn styleClass="colTabLeft" width="125px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarDocumentoImprimirContrato_grid_formulario}" style="text-align:center;width:125px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsFormulario}"/>
			 </app:scrollableColumn>
			 
			</app:scrollableDataTable>				 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim> 
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
	<br:brPanelGrid columns="3" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >s
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.selecionarDocumentoImprimirContrato_btn_voltar}" action="#{imprimirContratoBean.voltarPesquisar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:350px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:250px" >
			<br:brCommandButton id="btnLimpar" disabled="#{empty imprimirContratoBean.listaDocumentos}" styleClass="bto1" style="margin-right:5px" value="#{msgs.selecionarDocumentoImprimirContrato_btn_limpar}" action="#{imprimirContratoBean.limparDados}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnVisualizar" disabled="#{empty imprimirContratoBean.itemSelecionadoListaDocumentos}" styleClass="bto1" style="margin-right:5px" value="#{msgs.selecionarDocumentoImprimirContrato_btn_visualizar}" action="">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnImprimir" disabled="#{empty imprimirContratoBean.itemSelecionadoListaDocumentos}" styleClass="bto1" value="#{msgs.selecionarDocumentoImprimirContrato_btn_imprimir}" action="">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>