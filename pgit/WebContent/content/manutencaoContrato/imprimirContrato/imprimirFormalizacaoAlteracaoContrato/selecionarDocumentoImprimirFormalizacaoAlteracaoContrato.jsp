<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="selecionarDocumentoImprimirFormalizacaoAlteracaoContrato" name="selecionarDocumentoImprimirFormalizacaoAlteracaoContrato" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio id="radioFiltro" value="#{imprimirFormAltContratoBean.radioArgumentoPesquisaSelecionarDocumento}"  styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<a4j:support event="onclick" action="#{imprimirFormAltContratoBean.limparDadosFiltro}"  reRender="numeroDocumento, panelDataInicio, panelDataFim, versao"/>				
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_cliente_master}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_cpf_cnpj1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_nome_razao_social1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>		

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.subSegmentoMaster}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_cpf_cnpj2}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.cpfCnpj}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_nome_razao_social2}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.nomeRazaoSocial}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>				
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.empresa}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.numero}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_situacao1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.motivoDesc}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.participacao}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_possui_aditivos}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.possuiAditivos}"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
		
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_data_hora_cad1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.dataHoraCadastramento}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_inicio_vigencia}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.inicioVigencia}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_aditivos}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value=""/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_siutacao2}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value=""/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_data_hora_cad2}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value=""/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="0" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_numero_documento}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brInputText id="numeroDocumento" disabled="#{imprimirFormAltContratoBean.radioArgumentoPesquisaSelecionarDocumento != 0}" styleClass="HtmlInputTextBradesco" value="#{imprimirFormAltContratoBean.numeroDocumentoFiltro}" size="30" maxlength="10" >
				    </br:brInputText>
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_versao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
					<br:brInputText id="versao" disabled="#{imprimirFormAltContratoBean.radioArgumentoPesquisaSelecionarDocumento != 0}" styleClass="HtmlInputTextBradesco" value="#{imprimirFormAltContratoBean.versaoFiltro}" size="10" maxlength="10" >
				    </br:brInputText>
				</br:brPanelGroup>					
			</br:brPanelGrid>										
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFiltro" index="1" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_data_geracao}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
			    	<br:brPanelGroup id="panelDataInicio">
						<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5px" onkeypress="onlyNum();" disabled="#{imprimirFormAltContratoBean.radioArgumentoPesquisaSelecionarDocumento != 1}" value="#{imprimirFormAltContratoBean.diaInicioFiltro}" size="3" maxlength="2" id="txtDiaInicio" onkeyup="proximoCampo(2,'selecionarDocumentoImprimirFormalizacaoAlteracaoContrato','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtDiaInicio','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtMesInicio');" onblur="validarData('selecionarDocumentoImprimirFormalizacaoAlteracaoContrato','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtDiaInicio', 'dia');"/>
						<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5px" onkeypress="onlyNum();" disabled="#{imprimirFormAltContratoBean.radioArgumentoPesquisaSelecionarDocumento != 1}" value="#{imprimirFormAltContratoBean.mesInicioFiltro}" size="3" maxlength="2" id="txtMesInicio" onkeyup="proximoCampo(2,'selecionarDocumentoImprimirFormalizacaoAlteracaoContrato','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtMesInicio','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtAnoInicio');" onblur="validarData('selecionarDocumentoImprimirFormalizacaoAlteracaoContrato','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtMesInicio', 'mes');"/>
						<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{imprimirFormAltContratoBean.radioArgumentoPesquisaSelecionarDocumento != 1}" value="#{imprimirFormAltContratoBean.anoInicioFiltro}" size="5" maxlength="4" id="txtAnoInicio" onblur="validarData('selecionarDocumentoImprimirFormalizacaoAlteracaoContrato','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtAnoInicio', 'ano');"  />	
					</br:brPanelGroup>	
					
				    <br:brPanelGrid>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_a}"/>
					</br:brPanelGrid>			
	
					 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    	<br:brPanelGroup id="panelDataFim">
							<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5px" onkeypress="onlyNum();" disabled="#{imprimirFormAltContratoBean.radioArgumentoPesquisaSelecionarDocumento != 1}" value="#{imprimirFormAltContratoBean.diaFimFiltro}" size="3" maxlength="2" id="txtDiaFim" onkeyup="proximoCampo(2,'selecionarDocumentoImprimirFormalizacaoAlteracaoContrato','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtDiaFim','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtMesFim');" onblur="validarData('selecionarDocumentoImprimirFormalizacaoAlteracaoContrato','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtDiaFim', 'dia');"/>
							<br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5px" onkeypress="onlyNum();" disabled="#{imprimirFormAltContratoBean.radioArgumentoPesquisaSelecionarDocumento != 1}" value="#{imprimirFormAltContratoBean.mesFimFiltro}" size="3" maxlength="2" id="txtMesFim" onkeyup="proximoCampo(2,'selecionarDocumentoImprimirFormalizacaoAlteracaoContrato','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtMesFim','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtAnoFim');" onblur="validarData('selecionarDocumentoImprimirFormalizacaoAlteracaoContrato','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtMesFim', 'mes');"/>
							<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{imprimirFormAltContratoBean.radioArgumentoPesquisaSelecionarDocumento != 1}" value="#{imprimirFormAltContratoBean.anoFimFiltro}" size="5" maxlength="4" id="txtAnoFim" onblur="validarData('selecionarDocumentoImprimirFormalizacaoAlteracaoContrato','selecionarDocumentoImprimirFormalizacaoAlteracaoContrato:txtAnoFim', 'ano');"  />	
						</br:brPanelGroup>					
					</br:brPanelGrid>										
									
				</br:brPanelGrid>										
			
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparDados" styleClass="bto1" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_btn_limpar_dados}" action="#{imprimirFormAltContratoBean.limparDados}" style="margin-right:5px">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnConsultar"  styleClass="bto1" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_btn_consultar}" action="#{imprimirFormAltContratoBean.carregaListaDocumentos}" 
				   		onclick="javascript: desbloquearTela(); return validaCamposFiltro(document.forms[1],'#{msgs.label_ocampo}','#{msgs.label_necessario}','#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_numero_documento}','#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_data_geracao}', '#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_versao}');"> 
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim> <br> </f:verbatim> 
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">	
		
			<app:scrollableDataTable id="dataTable" value="#{imprimirFormAltContratoBean.listaDocumentos}" var="result" 
			rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >

			<app:scrollableColumn styleClass="colTabCenter" width="30px">
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>
				<t:selectOneRadio id="sor2" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
					value="#{imprimirFormAltContratoBean.itemSelecionadoListaDocumentos}">
					<f:selectItems value="#{imprimirFormAltContratoBean.listaControleDocumentos}"/>
					<a4j:support event="onclick" reRender="panelBotoes" />
				</t:selectOneRadio>
		    	<t:radio for="sor2" index="#{parametroKey}" />
			</app:scrollableColumn>
			
			<app:scrollableColumn styleClass="colTabRight" width="125px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_grid_documento}" style="text-align:center;width:125px" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdDocumento}"/>
			 </app:scrollableColumn>
			
			<app:scrollableColumn styleClass="colTabRight" width="125px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_grid_versao}" style="text-align:center;width:125px" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdVersao}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn styleClass="colTabLeft" width="125px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_grid_data_geracao}" style="text-align:center;width:125px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsDataGeracao}"/>
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn styleClass="colTabLeft" width="125px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_grid_idioma}" style="text-align:center;width:125px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsIdioma}"/>
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn styleClass="colTabLeft" width="125px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_grid_formulario}" style="text-align:center;width:125px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsFormulario}"/>
			 </app:scrollableColumn>
			 
			</app:scrollableDataTable>				 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim> 
	
	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
	<br:brPanelGrid columns="3" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >s
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_btn_voltar}" action="#{imprimirFormAltContratoBean.voltarFormalizacao}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:350px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:250px" >
			<br:brCommandButton id="btnLimpar" disabled="false" styleClass="bto1" style="margin-right:5px" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_btn_limpar}" action="#{imprimirFormAltContratoBean.limparDados}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnVisualizar" disabled="#{empty imprimirFormAltContratoBean.itemSelecionadoListaDocumentos}" styleClass="bto1" style="margin-right:5px" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_btn_visualizar}" action="">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnImprimir" disabled="#{empty imprimirFormAltContratoBean.itemSelecionadoListaDocumentos}" styleClass="bto1" value="#{msgs.selecionarDocumentoImprimirFormalizacaoAlteracaoContrato_btn_imprimir}" action="">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	</a4j:outputPanel>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>