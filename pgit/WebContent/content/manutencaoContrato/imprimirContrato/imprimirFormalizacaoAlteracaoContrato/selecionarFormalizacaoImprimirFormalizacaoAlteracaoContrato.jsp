<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="selecionarFormalizacaoImprimirFormalizacaoAlteracaoContrato" name="selecionarFormalizacaoImprimirFormalizacaoAlteracaoContrato" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conServicosManterContrato_clienteMaster}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_cpfCnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.cpfCnpjRepresentante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conServicosManterContrato_clienteMaster_nomeRazaoSocial}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.nomeRazaoRepresentante}"/>    
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >  
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_grupoEconomico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.grupEconRepresentante}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_atividadeEconomica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.ativEconRepresentante}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.segRepresentante}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_clienteMaster_subSegmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.subSegRepresentante}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>			
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
		
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_empresaGestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.empresaContrato}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_tipoContrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.tipoContrato}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.numeroContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.situacaoContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.motivoContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_contrato_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{imprimirFormAltContratoBean.participacaoContrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 
		
			<app:scrollableDataTable id="dataTable" value="#{imprimirFormAltContratoBean.listaGridFormalizacao}" var="result" 
			rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%" >

			<app:scrollableColumn styleClass="colTabCenter" width="30px">
				<f:facet name="header">
			      <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			    </f:facet>
				<t:selectOneRadio id="sor2" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" 
					value="#{imprimirFormAltContratoBean.itemSelecionadoListaFormalizacao}">
					<f:selectItems value="#{imprimirFormAltContratoBean.listaControleFormalizacao}"/>
					<a4j:support event="onclick" reRender="btnDocumentos" />
				</t:selectOneRadio>
		    	<t:radio for="sor2" index="#{parametroKey}" />
			</app:scrollableColumn>
			
			<app:scrollableColumn styleClass="colTabRight" width="140px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarFormalizacaoImprimirFormalizacaoAlteracaoContrato_grid_aditivo}" style="text-align:center;width:140px" />
			    </f:facet>
			    <br:brOutputText value="#{result.nrAditivoContratoNegocio}"/>
			 </app:scrollableColumn>
			
			<app:scrollableColumn styleClass="colTabLeft" width="140px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarFormalizacaoImprimirFormalizacaoAlteracaoContrato_grid_data_hora_inclusao}" style="text-align:center;width:140px" />
			    </f:facet>
			    <br:brOutputText value="#{result.hrAssinaturaAditivoContrato}"/>
			 </app:scrollableColumn>

			<app:scrollableColumn styleClass="colTabLeft" width="140px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarFormalizacaoImprimirFormalizacaoAlteracaoContrato_grid_situacao}" style="text-align:center;width:140px" />
			    </f:facet>
			    <br:brOutputText value="#{result.dsSituacaoAditivoContrato}"/>
			 </app:scrollableColumn>
			 
			 <app:scrollableColumn styleClass="colTabLeft" width="230px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.selecionarFormalizacaoImprimirFormalizacaoAlteracaoContrato_grid_subst_contrato_comercial}" style="text-align:center;width:230px" />
			    </f:facet>
			    <br:brOutputText value="#{result.cdIndicadorContratoComl}"/>
			 </app:scrollableColumn>
			 
			</app:scrollableDataTable>				 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable">
			  <f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim> 
	
	<br:brPanelGrid columns="3" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >s
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.selecionarFormalizacaoImprimirFormalizacaoAlteracaoContrato_btn_voltar}" action="#{imprimirFormAltContratoBean.voltarPesquisar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:350px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
		<br:brPanelGroup style="text-align:right;width:250px" >
			<br:brCommandButton id="btnImprimir" disabled="#{empty imprimirFormAltContratoBean.itemSelecionadoListaFormalizacao}" styleClass="bto1" value="#{msgs.conImprimirFormalizacaoAlteracaoContrato_btn_imprimir}" action="#{imprimirFormAltContratoBean.selecionarDocumento}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>