<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>

<brArq:form id="incManterSolicitacoesMudancaAmbienteOperacaoPaticipanteForm" name="incManterSolicitacoesMudancaAmbienteOperacaoPaticipante">
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterAmbienteOperacaoContratoParticipanteBean.hiddenObrigatoriedade}"/>
<h:inputHidden id="hiddenParticipanteSelecionado" value="#{manterAmbienteOperacaoContratoParticipanteBean.nomeRazaoParticipanteSelecionado}"/>
<h:inputHidden id="hiddenCPF_CNPJ_ParticipanteSelecionado" value="#{manterAmbienteOperacaoContratoParticipanteBean.cpfCnpjParticipanteSelecionado}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
			<t:selectOneRadio value="#{manterAmbienteOperacaoContratoParticipanteBean.tipoMudancaSolicitacao}" id="radioTipoMudanca" styleClass="HtmlSelectOneRadioBradesco"
				layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterAmbienteOperacaoContratoParticipanteBean.habilitaMudancaSolicitacao}">
				<f:selectItem itemValue="0" itemLabel=""/>  
				<f:selectItem itemValue="1" itemLabel=""/>  
				<a4j:support event="onclick" reRender="btnAvancar, radiosManterPagamentosAgendProd" action="#{manterAmbienteOperacaoContratoParticipanteBean.desabilitarManterPagamentosAgendadosProducao}" />
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
			<t:selectOneRadio value="#{manterAmbienteOperacaoContratoParticipanteBean.formaEfetivacaoSolicitacao}" id="radioFormaEfetivacao" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterAmbienteOperacaoContratoParticipanteBean.desabilitaFormaEfetivacao}">
				<f:selectItem itemValue="0" itemLabel=""/>  
				<f:selectItem itemValue="1" itemLabel=""/> 
				<a4j:support event="onclick" reRender="btnAvancar, calendario" />				 
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0">
		<br:brPanelGroup id="painelManterPagtoAmb"> 
			<t:selectOneRadio value="#{manterAmbienteOperacaoContratoParticipanteBean.manterOperacoesAmbienteAtualSolicitacao}" id="radioManterOperacoes"
				styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterAmbienteOperacaoContratoParticipanteBean.habilitaRadioAmbiente}">
				<f:selectItem itemValue="0" itemLabel=""/>  
				<f:selectItem itemValue="1" itemLabel=""/> 
				<a4j:support event="onclick" reRender="btnAvancar"/>				 
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid> 

	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0">
		<br:brPanelGroup id="painelRadioTipoParticipante"> 
			<t:selectOneRadio value="#{manterAmbienteOperacaoContratoParticipanteBean.tipoParticipante}" 
				id="radioTipoParticipante"  styleClass="HtmlSelectOneRadioBradesco" layout="spread" 
				forceId="true" forceIdIndex="false" onclick="voltarSelecionar()">
				<f:selectItem itemValue="0" itemLabel=""/>  
				<f:selectItem itemValue="1" itemLabel=""/> 
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid> 

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_cpf_cnpj}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.cpfCnpjClienteMaster}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_nome_razao_social}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.nomeRazaoSocialClienteMaster}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_grupo_economico}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.grupoEconomico}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_atividade_economica}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.atividadeEconomica}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.segmento}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_sub_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.subSegmento}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"></f:verbatim>
	

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.nomeRazaoParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_empresa}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.empresa}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_tipo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.tipo}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_numero}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.numero}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.solManterSolicitacoesMudancaAmbienteOperacao_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.descricaoContrato}"/>
		</br:brPanelGroup>		
	
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_situacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.situacao}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_motivo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.motivo}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.solManterSolicitacoesMudancaAmbienteOperacao_participacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.participacao}"/>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_solicitacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
			<t:radio for="radioTipoParticipante" index="0"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_todos_partc}"/> 
		</br:brPanelGroup>
		<br:brPanelGroup> 
			<t:radio for="radioTipoParticipante" index="1"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_escolher_participante}"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:6px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%" id="panelParticipanteSelecionado">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="cpfCnpjParticipanteSelecionado" value="#{manterAmbienteOperacaoContratoParticipanteBean.cpfCnpjParticipanteSelecionado}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="nomeRazaoParticipanteSelecionado" value="#{manterAmbienteOperacaoContratoParticipanteBean.nomeRazaoParticipanteSelecionado}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:6px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup style="margin-left:650px">
		<br:brCommandButton id="btnPesquisar" action="#{manterAmbienteOperacaoContratoParticipanteBean.consultarParticipantes}" value="#{msgs.label_pesquisar}" disabled="#{manterAmbienteOperacaoContratoParticipanteBean.tipoParticipante != 1}"styleClass="bto1">
			<brArq:submitCheckClient/>
		</br:brCommandButton>
			
	</br:brPanelGroup>	
	
	
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">					
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_servico}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
		   	<br:brSelectOneMenu id="tipoServico" value="#{manterAmbienteOperacaoContratoParticipanteBean.servico}"  style="margin-top:5px" disabled="#{manterAmbienteOperacaoContratoParticipanteBean.disabledComboTipoServico}">
				<f:selectItem itemValue="0" itemLabel="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_selecione}"/>
				<f:selectItems value="#{manterAmbienteOperacaoContratoParticipanteBean.listaTipoServico}"/>
				<a4j:support event="onchange" reRender="tipoAmbiente,radiosManterPagamentosAgendProd,radioTipoMudanca" action="#{manterAmbienteOperacaoContratoParticipanteBean.valorizarTipoAmbiente}"/>										
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:6px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_forma_efetivacao}:"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid id="panelFormaEfetivacao"columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="radioFormaEfetivacao" index="0"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_batch}"/> 
	    </br:brPanelGroup>
	    <br:brPanelGroup>
	    	<t:radio for="radioFormaEfetivacao" index="1"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_on_line}"/> 
	    </br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:6px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_manter_agendados_producao}:"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid id="radiosManterPagamentosAgendProd" columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<t:radio for="radioManterOperacoes" index="0"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_sim}"/> 
	    </br:brPanelGroup>
	    <br:brPanelGroup>
			<t:radio for="radioManterOperacoes" index="1"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_nao}"/> 
	    </br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid>
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_informativo}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:6px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_data_programada}:"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="calendario" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >						
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup rendered="#{manterAmbienteOperacaoContratoParticipanteBean.formaEfetivacaoSolicitacao != '1'}">
		    	<app:calendar id="dataProgramada" value="#{manterAmbienteOperacaoContratoParticipanteBean.dataProgramadaSolicitacao}" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" 
		    		oninputchange="recuperarDataCalendario(document.forms[1],'dataProgramada');"></app:calendar>
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{manterAmbienteOperacaoContratoParticipanteBean.formaEfetivacaoSolicitacao == '1'}">
				<app:calendar id="dataProgramadaDes" value="#{manterAmbienteOperacaoContratoParticipanteBean.dataProgramadaSolicitacao}" disabled="true">
			</app:calendar>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
 
 	<f:verbatim><hr class="lin"></f:verbatim>
 	
 	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_voltar}" action="#{manterAmbienteOperacaoContratoParticipanteBean.voltarIncluir}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGroup style="margin-left:650px">
			<br:brCommandButton  id="btnAvancar"
				onclick="javascript:desbloquearTela(); return validaInclusaoParticipante(document.forms[1], '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_campo}', '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_necessario}',
				 '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_servico}', '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_mudanca}',
				 '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_forma_efetivacao}', '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_manter_operacoes}', '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_data}', '#{msgs.detDuplicacaoArquivoRetorno_participantes}');" 
				styleClass="bto1" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_avancar}" action="#{manterAmbienteOperacaoContratoParticipanteBean.avancarIncluir}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>

<br:brCommandButton id="btnVoltarSelecionar" value="" style="display:none;" 
	action="#{manterAmbienteOperacaoContratoParticipanteBean.validarTipoPartcSelecionado}">
		<brArq:submitCheckClient/>
</br:brCommandButton>

</brArq:form>