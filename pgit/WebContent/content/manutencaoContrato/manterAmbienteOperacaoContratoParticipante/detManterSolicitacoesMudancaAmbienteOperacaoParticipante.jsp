<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="detManterSolicitacoesMudancaAmbienteOperacaoForm" name="detManterSolicitacoesMudancaAmbienteOperacaoForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_cpf_cnpj}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.cpfCnpjClienteMaster}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_nome_razao_social}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.nomeRazaoSocialClienteMaster}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_grupo_economico}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.grupoEconomico}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_atividade_economica}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.atividadeEconomica}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.segmento}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_sub_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.subSegmento}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"></f:verbatim>
	

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.nomeRazaoParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_empresa}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.empresa}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_tipo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.tipo}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_numero}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.numero}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.solManterSolicitacoesMudancaAmbienteOperacao_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.descricaoContrato}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_situacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.situacao}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_motivo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.motivo}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.solManterSolicitacoesMudancaAmbienteOperacao_participacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.participacao}"/>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_solicitacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.cpfCnpjParticipanteSelecionado}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.nomeRazaoParticipanteSelecionado}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_servico}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.servicoDesc}"/>	
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_tipo_mudanca}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.tipoMudancaSolicitacao}" />	
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_forma_efetivacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterAmbienteOperacaoContratoParticipanteBean.formaEfetivacaoSolicitacao}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_data_programada}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.dataProgramadaSolicitacaoDesc}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_situacao_solicitacao}:"/>				
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.situacaoSolicitacao}"/>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_manter_agendados_producao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"    value="#{manterAmbienteOperacaoContratoParticipanteBean.manterOperacoesAmbienteAtualSolicitacao}"/>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_data_hora_inclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.dataHoraInclusao}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.usuarioInclusao}"  />			
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.tipoCanalInclusao}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.complementoInclusao}"  />			
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_data_hora_manutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.dataHoraManutencao}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.usuarioManutencao}"  />			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>				
		
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.tipoCanalManutencao}"  />			
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.complementoManutencao}"  />			
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.detManterSolicitacoesMudancaAmbienteOperacao_voltar}" action="#{manterAmbienteOperacaoContratoParticipanteBean.voltarDetalhar}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
