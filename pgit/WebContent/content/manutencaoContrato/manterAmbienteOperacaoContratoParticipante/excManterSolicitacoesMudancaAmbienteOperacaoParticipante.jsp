<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="excManterSolicitacoesMudancaAmbienteOperacaoForm" name="excManterSolicitacoesMudancaAmbienteOperacaoForm">
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_cpf_cnpj}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.cpfCnpjClienteMaster}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_nome_razao_social}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.nomeRazaoSocialClienteMaster}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_grupo_economico}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.grupoEconomico}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_atividade_economica}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.atividadeEconomica}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.segmento}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_sub_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.subSegmento}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.nomeRazaoParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>

	<f:verbatim><hr class="lin"></f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_empresa}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.empresa}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_tipo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.tipo}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_numero}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.numero}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.solManterSolicitacoesMudancaAmbienteOperacao_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.descricaoContrato}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"> 
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_situacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.situacao}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_motivo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.motivo}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.solManterSolicitacoesMudancaAmbienteOperacao_participacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.participacao}"/>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_solicitacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.cpfCnpjParticipanteSelecionado}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.nomeRazaoParticipanteSelecionado}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_servico}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.servicoDesc}"/>	
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_tipo_mudanca}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterAmbienteOperacaoContratoParticipanteBean.tipoMudancaSolicitacao }" />	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_forma_efetivacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterAmbienteOperacaoContratoParticipanteBean.formaEfetivacaoSolicitacao}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_data_programada}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.dataProgramadaSolicitacaoDesc}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_situacao_solicitacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoParticipanteBean.situacao}"/>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_manter_agendados_producao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"    value="#{manterAmbienteOperacaoContratoParticipanteBean.manterOperacoesAmbienteAtualSolicitacao}"/>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_voltar}" action="#{manterAmbienteOperacaoContratoParticipanteBean.voltarExcluir}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup style="margin-left:650px">
			<br:brCommandButton id="btnConfirmar" onclick="javascript: if (!confirm('Confirma Exclus�o?')) { desbloquearTela(); return false; }" styleClass="bto1"
			 value="#{msgs.excManterSolicitacoesMudancaAmbienteOperacao_confirmar}" action="#{manterAmbienteOperacaoContratoParticipanteBean.confirmarExcluir}" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>