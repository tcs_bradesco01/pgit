<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="registrarAssinaturaContrato" name="registrarAssinaturaContrato">
	<h:inputHidden id="hiddenObrigatoriedade" value="#{registrarAssinaturaContratoBean.obrigatoriedade}" />

	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_cliente_representante}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" 
					value="#{msgs.manutencaoContrato_registrarAssinatura_cpf_cnpj_clienteMaster}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
					value="#{registrarAssinaturaContratoBean.cnpjCpfMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_nome_razaoSocial_clienteMaster}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.nomeRazaoSocialMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_grupo_economico}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.grupoEconomico}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_atividade_economico}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.atividadeEconomico}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_segmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.segmento}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_sub_segmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.subSegmento}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.lebel_agencia_gestora}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.dsAgenciaGestora}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.gerenteResponsavel}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.conDadosBasicos_clienteParticipante}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_cpf_cnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_nome_razao_social}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_contrato}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_empresa}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.empresaGestora}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_tipo}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.tipo}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_numero}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.numero}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_descricao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.dsContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>


		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.situacao}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_motivo}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.motivo}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_participação}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.participacao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.forManterFormalizacaoAlteracaoContrato_label_argumento_pesquisa}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<t:selectOneRadio id="radioSelecaoFiltro" styleClass="HtmlSelectOneRadioBradesco" layout="spread"
					forceId="true" forceIdIndex="false" value="#{registrarAssinaturaContratoBean.radioSelecaoFiltro}">
					<f:selectItem itemValue="0" itemLabel="" />
					<f:selectItem itemValue="1" itemLabel="" />
					<f:selectItem itemValue="2" itemLabel="" />
					<a4j:support event="onclick"
						action="#{registrarAssinaturaContratoBean.limparDados}"
						reRender="numeroFiltro, cboSituacaoFiltro,panelDataInicioFiltro,panelDataFimFiltro, btnConsultar" />
				</t:selectOneRadio>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<t:radio for="radioSelecaoFiltro" index="0" />
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
					style="text-align:left;">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg"
							styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
							value="#{msgs.label_numero_versao_contrato}:" />
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>

				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brInputText id="numeroFiltro" onkeypress="onlyNum();" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"
						styleClass="HtmlInputTextBradesco"
						value="#{registrarAssinaturaContratoBean.numeroFiltro}"
						disabled="#{registrarAssinaturaContratoBean.radioSelecaoFiltro != 0 ||
							registrarAssinaturaContratoBean.radioSelecaoFiltro == '' || 
							registrarAssinaturaContratoBean.desabilitaConsulta}"
						maxlength="10" />

				</br:brPanelGrid>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" style="margin-top:6px;" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
				<t:radio for="radioSelecaoFiltro" index="1" />
			</br:brPanelGroup>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
						style="text-align:left;">
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg"
								styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
								value="#{msgs.forManterFormalizacaoAlteracaoContrato_situacao}:" />
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0"
						cellspacing="0">
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>

					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
						<br:brSelectOneMenu id="cboSituacaoFiltro"
							disabled="#{registrarAssinaturaContratoBean.radioSelecaoFiltro != 1 || 
								registrarAssinaturaContratoBean.desabilitaConsulta }"
							value="#{registrarAssinaturaContratoBean.codigoSituacaoFiltro}">
							<f:selectItem itemValue="0"
								itemLabel="#{msgs.label_combo_selecione}" />
							<f:selectItems
								value="#{registrarAssinaturaContratoBean.listaSituacaoFiltro}" />
						</br:brSelectOneMenu>
					</br:brPanelGrid>
				</br:brPanelGrid>

			</br:brPanelGrid>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" style="margin-top:6px;" cellpadding="0"
			cellspacing="0">
			<br:brPanelGroup>
				<t:radio for="radioSelecaoFiltro" index="2" />
			</br:brPanelGroup>

			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"
				style="text-align:left;">
				<br:brPanelGroup style="text-align:left;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
						value="#{msgs.forManterFormalizacaoAlteracaoContrato_data_inclusao}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" style="margin-top:6px;" cellpadding="0"
			cellspacing="0">

			<a4j:outputPanel id="calendarios"
				style="width: 100%; text-align: left" ajaxRendered="true"
				onmousedown="javascript: cleanClipboard();">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brPanelGroup
							rendered="#{registrarAssinaturaContratoBean.radioSelecaoFiltro != null &&  
								registrarAssinaturaContratoBean.radioSelecaoFiltro == 2}">
							<app:calendar oninputkeydown="javascript: cleanClipboard();"
								oninputkeypress="onlyNum();"
								oninputchange="recuperarDataCalendario(document.forms[1],'dataInicialPagamento');"
								id="dataInicialPagamento"
								value="#{registrarAssinaturaContratoBean.dataInicialPagamentoFiltro}">

							</app:calendar>
						</br:brPanelGroup>
						<br:brPanelGroup
							rendered="#{empty registrarAssinaturaContratoBean.radioSelecaoFiltro || 
							registrarAssinaturaContratoBean.radioSelecaoFiltro != 2}">
							<app:calendar id="dataInicialPagamentoDes"
								value="#{registrarAssinaturaContratoBean.dataInicialPagamentoFiltro}"
								disabled="true">
							</app:calendar>
						</br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
							style="margin-right:5px;margin-left:5px"
							value="#{msgs.forManterFormalizacaoAlteracaoContrato_label_a}" />
						<br:brPanelGroup
							rendered="#{registrarAssinaturaContratoBean.radioSelecaoFiltro != null &&  
							registrarAssinaturaContratoBean.radioSelecaoFiltro == 2}">
							<app:calendar oninputkeydown="javascript: cleanClipboard();"
								oninputkeypress="onlyNum();"
								oninputchange="recuperarDataCalendario(document.forms[1],'dataFinalPagamento');"
								id="dataFinalPagamento"
								value="#{registrarAssinaturaContratoBean.dataFinalPagamentoFiltro}">

							</app:calendar>
						</br:brPanelGroup>
						<br:brPanelGroup
							rendered="#{empty registrarAssinaturaContratoBean.radioSelecaoFiltro || 
							registrarAssinaturaContratoBean.radioSelecaoFiltro != 2}">
							<app:calendar id="dataFinalPagamentoDes"
								value="#{registrarAssinaturaContratoBean.dataFinalPagamentoFiltro}"
								disabled="true">
							</app:calendar>
						</br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0"
					cellspacing="0">
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="1" width="100%" style="text-align:right"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1"
					value="#{msgs.forManterFormalizacaoAlteracaoContrato_botao_limpar_dados}"
					action="#{registrarAssinaturaContratoBean.limparCampos}"
					style="margin-right:5px">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" styleClass="bto1"
					disabled="#{registrarAssinaturaContratoBean.desabilitaConsulta}"
					value="#{msgs.forManterFormalizacaoAlteracaoContrato_botao_consultar}"
					action="#{registrarAssinaturaContratoBean.consultar}"
					onclick="javascript: desbloquearTela(); return validaCamposFiltro(document.forms[1],'#{msgs.label_ocampo}','#{msgs.label_necessario}','#{msgs.label_numero_versao_contrato}', '#{msgs.forManterFormalizacaoAlteracaoContrato_situacao}', '#{msgs.forManterFormalizacaoAlteracaoContrato_data_inclusao}');">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<br>
		</f:verbatim>

		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" id="gridPesquisa">
			<br:brPanelGroup
				style="overflow-x:auto; overflow-y:auto; width:750px; height:170">

				<app:scrollableDataTable id="dataTable"
					value="#{registrarAssinaturaContratoBean.listaGridPesquisa}"
					var="result" rows="10" rowIndexVar="parametroKey"
					rowClasses="tabela_celula_normal, tabela_celula_destaque"
					width="100%">

					<app:scrollableColumn styleClass="colTabCenter" width="30px">
						<f:facet name="header">
							<br:brOutputText value=" " styleClass="tableFontStyle"
								style="width:30; text-align:center" />
						</f:facet>
						<t:selectOneRadio id="sor" styleClass="HtmlSelectOneRadioBradesco"
							layout="spread" forceId="true" forceIdIndex="false"
							value="#{registrarAssinaturaContratoBean.itemSelecionadoLista}">
							<f:selectItems
								value="#{registrarAssinaturaContratoBean.listaControleRadio}" />
							<a4j:support event="onclick" reRender="panelBotoes" />
						</t:selectOneRadio>
						<t:radio for="sor" index="#{parametroKey}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabRight" width="230px">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_numero_versao_contrato}"
								style="text-align:center;width:230" />
						</f:facet>
						<br:brOutputText value="#{result.nrAditivoContratoNegocio}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="230px">
						<f:facet name="header">
							<br:brOutputText
								value="#{msgs.forManterFormalizacaoAlteracaoContrato_grid_data_inclusao}"
								style="text-align:center;width:230" />
						</f:facet>
						<br:brOutputText value="#{result.dtInclusaoAditivoContrato}" />
					</app:scrollableColumn>

					<app:scrollableColumn styleClass="colTabLeft" width="200x">
						<f:facet name="header">
							<br:brOutputText
								value="#{msgs.forManterFormalizacaoAlteracaoContrato_grid_situacao}"
								style="text-align:center;width:200" />
						</f:facet>
						<br:brOutputText value="#{result.dsSitucaoAditivoContrato}" />
					</app:scrollableColumn>
				</app:scrollableDataTable>
			</br:brPanelGroup>

		</br:brPanelGrid>

		<br:brPanelGrid columns="1" width="100%" style="text-align:center"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable"
					actionListener="#{registrarAssinaturaContratoBean.paginacaoRegistrarAssinaturaContrato}" >
					<f:facet name="first">
						<brArq:pdcCommandButton 
							id="primeira" 
							styleClass="bto1"
							value="#{msgs.label_primeira}" 
							title="#{msgs.label_primeira_msg}"
							onclick="limpaItemSelecionado();" />
					</f:facet>
					<f:facet name="fastrewind">
						<brArq:pdcCommandButton 
							id="retrocessoRapido" 
							styleClass="bto1"
							style="margin-left: 3px;" 
							value="#{msgs.label_retrocesso}"
							title="#{msgs.label_retrocesso_msg}"
							onclick="limpaItemSelecionado();" />
					</f:facet>
					<f:facet name="previous">
						<brArq:pdcCommandButton
							id="anterior" 
							styleClass="bto1"
							style="margin-left: 3px;" 
							value="#{msgs.label_anterior}"
							title="#{msgs.label_anterior_msg}"
							onclick="limpaItemSelecionado();" />
					</f:facet>
					<f:facet name="next">
						<brArq:pdcCommandButton 
							id="proxima" 
							styleClass="bto1"
							style="margin-left: 3px;" 
							value="#{msgs.label_proxima}"
							title="#{msgs.label_proxima_msg}"
							onclick="limpaItemSelecionado();" /> 
					</f:facet>
					<f:facet name="fastforward">
						<brArq:pdcCommandButton
							id="avancoRapido" 
							styleClass="bto1"
							style="margin-left: 3px;" 
							value="#{msgs.label_avanco}"
							title="#{msgs.label_avanco_msg}"
							onclick="limpaItemSelecionado();" />
					</f:facet>
					<f:facet name="last">
						<brArq:pdcCommandButton
							id="ultima" 
							styleClass="bto1"
							style="margin-left: 3px;" 
							value="#{msgs.label_ultima}"
							title="#{msgs.label_ultima_msg}"
							onclick="limpaItemSelecionado();" />
					</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<a4j:jsFunction
			name="limpaItemSelecionado"
			actionListener="#{registrarAssinaturaContratoBean.limparItemSelecionado}"
			reRender="panelBotoes" />

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" id="panelBotoes">
			<br:brPanelGrid style="text-align:left">
				<br:brCommandButton id="btnVoltar" disabled="false" styleClass="bto1" 
					value="#{msgs.registrarAssinatura_contrato_btnVoltar}"
					action="#{registrarAssinaturaContratoBean.voltarConRegistrarAssinaturaContrato}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGrid>
			
			<br:brPanelGrid style="text-align:right" width="100%" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="4">
					<br:brCommandButton id="btnLimpar" style="margin-right:5px;"
						disabled="#{empty registrarAssinaturaContratoBean.itemSelecionadoLista}"
						styleClass="bto1" value="#{msgs.btn_limpar}"
						action="#{registrarAssinaturaContratoBean.limparGridPesquisar}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				
					<br:brCommandButton id="btnDetalhar" style="margin-right:5px;"
						disabled="#{empty registrarAssinaturaContratoBean.itemSelecionadoLista}"
						styleClass="bto1" value="#{msgs.btn_detalhar}"
						action="#{registrarAssinaturaContratoBean.detalhar}">
						<brArq:submitCheckClient />
					</br:brCommandButton>

					<br:brPanelGroup>
						<a4j:commandButton id="btnImprimir" style="margin-right:5px;"
							disabled="#{registrarAssinaturaContratoBean.habilitaBotaoRegistrar}"
							value="#{msgs.btn_imprimir_versao_contrato}" styleClass="bto1">
							<a4j:support event="onclick"
								action="#{registrarAssinaturaContratoBean.imprimirContrato}"
								oncomplete="document.getElementById('registrarAssinaturaContrato:btnImprimirDocumentoHidden').click()" />
						</a4j:commandButton>
						<br:brCommandButton id="btnImprimirDocumentoHidden"
							value="#{msgs.btn_imprimir_versao_contrato}"
							action="#{registrarAssinaturaContratoBean.imprimirContratoFinal}"
							styleClass="HtmlCommandButtonBradesco" style="display: none"
							onclick="desbloquearTela()">
							<brArq:submitCheckClient />
						</br:brCommandButton>
					</br:brPanelGroup>

					<br:brCommandButton id="btnRegistrar" style="margin-right:5px" 
						disabled="#{registrarAssinaturaContratoBean.habilitaBotaoRegistrar}"
						styleClass="bto1" value="#{msgs.btn_registrar}"
						action="#{registrarAssinaturaContratoBean.registrar}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</br:brPanelGrid>

	</br:brPanelGrid>
	
	<a4j:status id="statusAguarde" onstart="bloquearTela()"
			onstop="desbloquearTela()" />
	
	<brArq:validatorScript functionName="validateForm" />
	
</brArq:form>
