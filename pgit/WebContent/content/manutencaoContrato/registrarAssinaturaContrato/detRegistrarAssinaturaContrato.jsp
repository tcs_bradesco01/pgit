<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components"
	prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="registrarAssinaturaContrato" name="registrarAssinaturaContrato">
	<h:inputHidden id="hiddenObrigatoriedade" value="#{registrarAssinaturaContratoBean.obrigatoriedade}" />

	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_cliente_representante}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" 
					value="#{msgs.manutencaoContrato_registrarAssinatura_cpf_cnpj_clienteMaster}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
					value="#{registrarAssinaturaContratoBean.cnpjCpfMaster}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_nome_razaoSocial_clienteMaster}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.nomeRazaoSocialMaster}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_grupo_economico}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.grupoEconomico}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_atividade_economico}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.atividadeEconomico}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_segmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.segmento}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_sub_segmento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.subSegmento}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.lebel_agencia_gestora}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.dsAgenciaGestora}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.gerenteResponsavel}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.conDadosBasicos_clienteParticipante}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_cpf_cnpj}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_nome_razao_social}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_contrato}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_empresa}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.empresaGestora}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_tipo}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.tipo}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_numero}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.numero}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_descricao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.dsContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>


		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.situacao}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_motivo}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.motivo}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.manutencaoContrato_registrarAssinatura_participação}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.participacao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim> <hr class="lin"> </f:verbatim>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.detManterForAltContrato_numero_formalizacao}:" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_numero_versao_contrato}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.consultarManterFormAltContratoSaidaDTO.nrAditivoContratoNegocio}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detManterForAltContrato_data_hora_cadastramento_2}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.hrInclusaoRegistro}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detManterForAltContrato_situacao_2}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.consultarManterFormAltContratoSaidaDTO.dsSitucaoAditivoContrato}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detManterForAltContrato_data_hora_assinatura}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.hrManutencaoRegistro}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0"
			rendered="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdFormaAutorizacaoPagamento != 0
				|| registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdUnidadeOrganizacional != 0}">
			<f:verbatim><hr class="lin"></f:verbatim>
			
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_dados_contrato}:"/>
			</br:brPanelGroup>
	
			<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>		
	
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" 
				rendered="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdFormaAutorizacaoPagamento != 0}"  >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_forma_autorizacao_pagamento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.dsFormaAutorizacaoPagamento}" />
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" 
				rendered="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdFormaAutorizacaoPagamento != 0
				&& egistrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdUnidadeOrganizacional != 0}">
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"
				rendered="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdUnidadeOrganizacional != 0}" >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" 
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.unidadeOrganizacionalFormatada}" />
			</br:brPanelGrid>
			
			<f:verbatim><hr class="lin"> </f:verbatim>	
			
		</br:brPanelGrid>

		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%"
			cellpadding="0" cellspacing="0"
			rendered="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdFormaAutorizacaoPagamento != 0
			&& registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdUnidadeOrganizacional != 0}">
			<f:verbatim>
				<hr class="lin">
			</f:verbatim>
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
					value="#{msgs.label_dados_contrato}:" />
			</br:brPanelGroup>

			<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"
				rendered="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdFormaAutorizacaoPagamento != 0}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.label_forma_autorizacao_pagamento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.dsFormaAutorizacaoPagamento}" />
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0"
				rendered="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdUnidadeOrganizacional != 0}">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.lebel_agencia_gestora}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.unidadeOrganizacionalFormatada}" />
			</br:brPanelGrid>
		</br:brPanelGrid>

		<a4j:outputPanel id="painelParticipantes"
			style="width: 100%; text-align: right" ajaxRendered="true"
			rendered="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.numeroOcorrenciasParticipante != 0}">
			<f:verbatim> <hr class="lin"> </f:verbatim>

			<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.detManterForAltContrato_participantes}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
				<br:brPanelGroup
					style="overflow-x:auto; overflow-y:auto; width:750px; height:170">

					<app:scrollableDataTable id="dataTableParticipantes"
						value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.listaParticipantes}"
						var="result"
						rows="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.numeroOcorrenciasParticipante}"
						rowIndexVar="parametroKey"
						rowClasses="tabela_celula_normal, tabela_celula_destaque"
						width="100%">

						<app:scrollableColumn width="200px" styleClass="colTabRight">
							<f:facet name="header">
								<br:brOutputText
									value="#{msgs.detManterForAltContrato_cnpj_cpf_titular}"
									style="width:200; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.nrCpfCnpjParticipante}" />
						</app:scrollableColumn>

						<app:scrollableColumn width="210px" styleClass="colTabLeft">
							<f:facet name="header">
								<br:brOutputText
									value="#{msgs.detManterForAltContrato_nome_razao_social}"
									style="width:210; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.nmRazaoSocialParticipante}" />
						</app:scrollableColumn>

						<app:scrollableColumn width="200px" styleClass="colTabLeft">
							<f:facet name="header">
								<br:brOutputText
									value="#{msgs.detManterForAltContrato_tipo_participacao}"
									style="width:200; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.dsTipoParticipantePart}" />
						</app:scrollableColumn>

						<app:scrollableColumn width="150px" styleClass="colTabLeft">
							<f:facet name="header">
								<h:outputText value="#{msgs.detManterForAltContrato_acao}"
									style="width:150; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.dsAcaoManutencaoParticipante}" />
						</app:scrollableColumn>
					</app:scrollableDataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>

		<a4j:outputPanel id="painelVinculos"
			style="width: 100%; text-align: right" ajaxRendered="true"
			rendered="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.numeroOcorrenciasVinculacao != 0}">
			<f:verbatim> <hr class="lin"> </f:verbatim>

			<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%"
				cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.detManterForAltContrato_contas}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" width="100%" cellpadding="0"
				cellspacing="0">
				<br:brPanelGroup
					style="overflow-x:auto; overflow-y:auto; width:750px; height:170">

					<app:scrollableDataTable id="dataTableContas"
						value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.listaVinculos}"
						var="result"
						rows="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.numeroOcorrenciasVinculacao}"
						rowIndexVar="parametroKey"
						rowClasses="tabela_celula_normal, tabela_celula_destaque"
						width="100%">

						<app:scrollableColumn width="180px" styleClass="colTabRight">
							<f:facet name="header">
								<br:brOutputText
									value="#{msgs.detManterForAltContrato_cnpj_cpf_participantes}"
									style="width:180; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.nrCpfCnpjVinc}" />
						</app:scrollableColumn>

						<app:scrollableColumn width="210px" styleClass="colTabLeft">
							<f:facet name="header">
								<br:brOutputText
									value="#{msgs.detManterForAltContrato_nome_razao_social_participante}"
									style="width:210; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.nmRazaoSocialVinc}" />
						</app:scrollableColumn>

						<app:scrollableColumn width="150px" styleClass="colTabLeft">
							<f:facet name="header">
								<br:brOutputText value="#{msgs.detManterForAltContrato_banco}"
									style="width:150; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.dsBanco}" />
						</app:scrollableColumn>

						<app:scrollableColumn width="150px" styleClass="colTabLeft">
							<f:facet name="header">
								<br:brOutputText value="#{msgs.detManterForAltContrato_agencia}"
									style="width:150; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.dsAgenciaBancaria}" />
						</app:scrollableColumn>

						<app:scrollableColumn width="150px" styleClass="colTabRight">
							<f:facet name="header">
								<br:brOutputText value="#{msgs.detManterForAltContrato_conta}"
									style="width:150; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.cdContaBancaria}" />
							<br:brOutputText value="-" />
							<br:brOutputText value="#{result.cdDigitoConta}" />
						</app:scrollableColumn>

						<app:scrollableColumn width="150px" styleClass="colTabLeft">
							<f:facet name="header">
								<h:outputText value="#{msgs.detManterForAltContrato_acao_2}"
									style="width:150; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.dsAcaoManutencaoConta}" />
						</app:scrollableColumn>
					</app:scrollableDataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<f:verbatim> <hr class="lin"> </f:verbatim>
		</a4j:outputPanel>

		<a4j:outputPanel id="painelServicos"
			style="width: 100%; text-align: right" ajaxRendered="true"
			rendered="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.numeroOcorrenciasServicos != 0}">
			<f:verbatim> <hr class="lin"> </f:verbatim>

			<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextTitleBradesco"
						value="#{msgs.detManterForAltContrato_servicos}:" />
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0">
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
				<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170">

					<app:scrollableDataTable id="dataTableServicos"
						value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.listaServicos}"
						var="result"
						rows="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.numeroOcorrenciasServicos}"
						rowIndexVar="parametroKey"
						rowClasses="tabela_celula_normal, tabela_celula_destaque"
						width="100%">

						<app:scrollableColumn width="350px" styleClass="colTabLeft">
							<f:facet name="header">
								<br:brOutputText
									value="#{msgs.detManterForAltContrato_produto_servico}"
									style="width:350; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.dsProdutoServicoOperacao}" />
						</app:scrollableColumn>

						<app:scrollableColumn width="350px" styleClass="colTabLeft">
							<f:facet name="header">
								<br:brOutputText
									value="#{msgs.detManterForAltContrato_operacoes}"
									style="width:350; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.dsProdutoServicoRelacionado}" />
						</app:scrollableColumn>

						<app:scrollableColumn width="350px" styleClass="colTabLeft">
							<f:facet name="header">
								<h:outputText value="#{msgs.detManterForAltContrato_acao_3}"
									style="width:350; text-align:center" />
							</f:facet>
							<br:brOutputText value="#{result.dsAcaoManutencaoServico}" />
						</app:scrollableColumn>
					</app:scrollableDataTable>
				</br:brPanelGroup>
			</br:brPanelGrid>
				<f:verbatim>
			<hr class="lin">
		</f:verbatim>
			
		</a4j:outputPanel>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detManterForAltContrato_data_hora_inclusao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.hrInclusaoRegistroTrilha}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detManterForAltContrato_usuario}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdUsuarioInclusao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas"
			cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detManterForAltContrato_tipo_canal}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.tipoCanalInclusaoTrilhaFormatado}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detManterForAltContrato_complemento}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.dsTipoCanalInclusaoTrilha}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detManterForAltContrato_data_hora_manutencao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.hrManutencaoRegistro}" />
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detManterForAltContrato_usuario_2}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.cdUsuarioManutencao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>

		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detManterForAltContrato_tipo_canal_2}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.tipoCanalManutencaoFormatado}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco"
					value="#{msgs.detManterForAltContrato_complemento_2}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"
					value="#{registrarAssinaturaContratoBean.saidaDetalharManterFormAltContrato.dsTipoCanalManutencao}" />
			</br:brPanelGroup>
		</br:brPanelGrid>


		<f:verbatim>
			<hr class="lin">
		</f:verbatim>

		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
			<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0">

				<br:brPanelGroup style="text-align:left;width:150px">
					<br:brCommandButton id="btnVoltar" disabled="false"
						styleClass="bto1"
						value="#{msgs.detManterForAltContrato_botao_voltar}"
						action="#{registrarAssinaturaContratoBean.voltar}">
						<brArq:submitCheckClient />
					</br:brCommandButton>
				</br:brPanelGroup>

				<br:brPanelGrid columns="1" style="width:600px" cellpadding="0"
					cellspacing="0">
				</br:brPanelGrid>

			</br:brPanelGrid>
		</a4j:outputPanel>


	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>