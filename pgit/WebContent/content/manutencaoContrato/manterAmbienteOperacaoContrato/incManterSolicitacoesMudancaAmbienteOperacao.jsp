<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>

<brArq:form id="incManterSolicitacoesMudancaAmbienteOperacaoForm" name="incManterSolicitacoesMudancaAmbienteOperacaoForm">
<h:inputHidden id="hiddenObrigatoriedade" value="#{manterAmbienteOperacaoContratoBean.hiddenObrigatoriedade}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
			<t:selectOneRadio value="#{manterAmbienteOperacaoContratoBean.tipoMudancaSolicitacao}" id="radioTipoMudanca" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterAmbienteOperacaoContratoBean.habilitaMudancaSolicitacao}">
				<f:selectItem itemValue="0" itemLabel=""/>  
				<f:selectItem itemValue="1" itemLabel=""/>  
				<%--<a4j:support event="onclick" reRender="btnAvancar" oncomplete="document.getElementById('incManterSolicitacoesMudancaAmbienteOperacaoForm:btnRadio').click();"/>--%>
				<a4j:support event="onclick" reRender="btnAvancar"/>
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
			<t:selectOneRadio value="#{manterAmbienteOperacaoContratoBean.formaEfetivacaoSolicitacao}" id="radioFormaEfetivacao" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
				<f:selectItem itemValue="0" itemLabel=""/>  
				<f:selectItem itemValue="1" itemLabel=""/> 
				<a4j:support event="onclick" reRender="btnAvancar, calendario"/>				 
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1"  cellpadding="0" cellspacing="0">
		<br:brPanelGroup id="painelManterPagtoAmb"> 
			<t:selectOneRadio value="#{manterAmbienteOperacaoContratoBean.manterOperacoesAmbienteAtualSolicitacao}" id="radioManterOperacoes" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{manterAmbienteOperacaoContratoBean.habilitaRadioAmbiente}">
				<f:selectItem itemValue="0" itemLabel=""/>  
				<f:selectItem itemValue="1" itemLabel=""/> 
				<a4j:support event="onclick" reRender="btnAvancar"/>				 
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid> 

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_cpf_cnpj}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.cpfCnpjClienteMaster}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_nome_razao_social}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.nomeRazaoSocialClienteMaster}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_grupo_economico}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.grupoEconomico}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_atividade_economica}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.atividadeEconomica}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.segmento}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_sub_segmento}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.subSegmento}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
   	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"></f:verbatim>
	

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" width="100%">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.nomeRazaoParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_empresa}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.empresa}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_tipo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.tipo}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_numero}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.numero}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.solManterSolicitacoesMudancaAmbienteOperacao_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.descricaoContrato}"/>
		</br:brPanelGroup>		
	
	</br:brPanelGrid>
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_situacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.situacao}"/>	
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_motivo}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.motivo}"/>	
		</br:brPanelGroup>		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.solManterSolicitacoesMudancaAmbienteOperacao_participacao}:"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterAmbienteOperacaoContratoBean.participacao}"/>	
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_solicitacao}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_servico}:"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
		   	<br:brSelectOneMenu id="tipoServico" value="#{manterAmbienteOperacaoContratoBean.servico}"  style="margin-top:5px">
				<f:selectItem itemValue="0" itemLabel="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_selecione}"/>
				<f:selectItems value="#{manterAmbienteOperacaoContratoBean.listaTipoServico}"/>
				<a4j:support event="onchange" reRender="tipoAmbiente" action="#{manterAmbienteOperacaoContratoBean.valorizarTipoAmbiente}"/>										
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:6px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:6px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_tipo_mudanca}:"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" id="tipoAmbiente">
		<br:brPanelGroup> 
			<t:radio for="radioTipoMudanca" index="0"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_teste_para_producao}"/> 
		</br:brPanelGroup>
		<br:brPanelGroup> 
			<t:radio for="radioTipoMudanca" index="1"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_producao_para_teste}"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:6px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_forma_efetivacao}:"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<t:radio for="radioFormaEfetivacao" index="0"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_batch}"/> 
	    </br:brPanelGroup>
	    <br:brPanelGroup>
	    	<t:radio for="radioFormaEfetivacao" index="1"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_on_line}"/> 
	    </br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:6px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_manter_agendados_producao}:"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<t:radio for="radioManterOperacoes" index="0"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_sim}"/> 
	    </br:brPanelGroup>
	    <br:brPanelGroup>
			<t:radio for="radioManterOperacoes" index="1"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_nao}"/> 
	    </br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid>
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_informativo}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:6px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_data_programada}:"/> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1"  style="margin-top:5px" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="calendario" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >						
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		    <br:brPanelGroup rendered="#{manterAmbienteOperacaoContratoBean.formaEfetivacaoSolicitacao != '1'}">
		    	<app:calendar id="dataProgramada" value="#{manterAmbienteOperacaoContratoBean.dataProgramadaSolicitacao}" oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" 
		    		oninputchange="recuperarDataCalendario(document.forms[1],'dataProgramada');"></app:calendar>
			</br:brPanelGroup>	
			<br:brPanelGroup rendered="#{manterAmbienteOperacaoContratoBean.formaEfetivacaoSolicitacao == '1'}">
				<app:calendar id="dataProgramadaDes" value="#{manterAmbienteOperacaoContratoBean.dataProgramadaSolicitacao}" disabled="true">
			</app:calendar>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>
 
 	<f:verbatim><hr class="lin"></f:verbatim>
 	
 	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton  id="btnVoltar" styleClass="bto1" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_voltar}" action="#{manterAmbienteOperacaoContratoBean.voltarIncluir}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		<br:brPanelGroup style="margin-left:650px">
			<br:brCommandButton  id="btnAvancar"
				onclick="validaInclusao(document.forms[1], '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_campo}', '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_necessario}',
				 '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_servico}', '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_mudanca}',
				 '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_forma_efetivacao}', '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_manter_operacoes}', '#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_msgm_data}');" 
				styleClass="bto1" value="#{msgs.incManterSolicitacoesMudancaAmbienteOperacao_avancar}" action="#{manterAmbienteOperacaoContratoBean.avancarIncluir}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>
 	<br:brCommandButton  id="btnRadio" styleClass="bto1" value="" action="#{manterAmbienteOperacaoContratoBean.desabilitaAmbiente}" style="display:none">	
		<brArq:submitCheckClient/>
	</br:brCommandButton>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>