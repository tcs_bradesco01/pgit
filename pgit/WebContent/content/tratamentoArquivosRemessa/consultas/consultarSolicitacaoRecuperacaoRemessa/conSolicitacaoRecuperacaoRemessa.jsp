<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="conSolicitacaoRecuperacaoRemessaForm" name="conSolicitacaoRecuperacaoRemessaForm" >

<h:inputHidden id="hiddenObrigatoriedade" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.obrigatoriedade}"/>
<h:inputHidden id="habilitaCampos" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaCampos}"/>

<br:brPanelGrid columns="1" style="margin-top:9" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
     
    <br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid cellpadding="0" cellspacing="0">
		<t:selectOneRadio id="rdoFiltro" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemRadioFiltroSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<a4j:support event="onclick" reRender="painelFiltroCliente, painelFiltroContrato, panelRdoFiltroCliente, rdoFiltroCliente, panelCnpj, panelCpf, panelNomeRazao, panelBanco, panelAgencia, panelConta, painelDadosCliente,
			panelBtoConsultarCliente, panelBtoContrato, empresaGestora, tipoContrato, numero, agenciaOperadora, txtAgenciaOperadora, cboEmpresaGestora, cboTipoContrato, txtNumero, painelTxtNumero, painelTxtAgenciaOperadora, painelDadosClienteCnpj, painelDadosClienteNome,
			panelDadosContrato, painelDadosContratoCliente, calendarios, panelUsuario, cboSituacao, btnConsultarFiltroSituacao, btnConsultar" action="#{solicitacaoRecuperacaoRemessaBean.limparRadioFiltro}"/>
		</t:selectOneRadio>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
			<t:radio for="rdoFiltro" index="0" />
		</br:brPanelGroup>
		<br:brPanelGroup> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_radio_filtro_cliente}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  style="margin-left:20px">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_identificacao_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid id="panelRdoFiltroCliente" cellpadding="0" cellspacing="0" >
		<t:selectOneRadio id="rdoFiltroCliente" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.bloqueiaRadio}">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel=""/>
			<f:selectItem itemValue="3" itemLabel="" />
		</t:selectOneRadio>
	</br:brPanelGrid>	
	
	<br:brPanelGrid id="painelFiltroCliente" columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="0" />			
			
			 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_cnpj}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" onkeyup="proximoCampo(9,'conSolicitacaoRecuperacaoRemessaForm','conSolicitacaoRecuperacaoRemessaForm:txtCnpj','conSolicitacaoRecuperacaoRemessaForm:txtFilial');" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				    <br:brInputText id="txtFilial" onkeyup="proximoCampo(4,'conSolicitacaoRecuperacaoRemessaForm','conSolicitacaoRecuperacaoRemessaForm:txtFilial','conSolicitacaoRecuperacaoRemessaForm:txtControle');" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
					<br:brInputText id="txtControle" style="margin-right: 5"converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' || solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" styleClass="HtmlInputTextBradesco" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="1" />			
			
			 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_cpf}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			   <br:brPanelGrid columns="2"   cellpadding="0" cellspacing="0">					
				    <br:brInputText styleClass="HtmlInputTextBradesco" style="margin-right: 5"converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente }" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" 
				    onkeyup="proximoCampo(9,'conSolicitacaoRecuperacaoRemessaForm','conSolicitacaoRecuperacaoRemessaForm:txtCpf','conSolicitacaoRecuperacaoRemessaForm:txtControleCpf');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' || solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="4"  cellpadding="0" cellspacing="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="2" />		
			
			 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '2' || solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
			</a4j:outputPanel>
			
		</br:brPanelGrid>
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0"  border="0" style="margin-left:20px">
			<t:radio for="rdoFiltroCliente" index="3" />			
			
			<a4j:outputPanel id="panelBanco" ajaxRendered="true">
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_banco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onkeyup="proximoCampo(3,'conSolicitacaoRecuperacaoRemessaForm','conSolicitacaoRecuperacaoRemessaForm:txtBanco','conSolicitacaoRecuperacaoRemessaForm:txtAgencia');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onkeyup="proximoCampo(5,'conSolicitacaoRecuperacaoRemessaForm','conSolicitacaoRecuperacaoRemessaForm:txtAgencia','conSolicitacaoRecuperacaoRemessaForm:txtConta');" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelConta" ajaxRendered="true">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '3' || solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</a4j:outputPanel>
	    </br:brPanelGrid>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '0' && solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado != '1' && solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado !='2' && solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemFiltroSelecionado !='3' }" value="#{msgs.identificacaoClienteContrato_label_consultar_cliente}" 
	   			action="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.telaConsultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="return validaCamposConsultaCliente(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.identificacaoClienteContrato_cnpj}','#{msgs.identificacaoClienteContrato_cpf}', '#{msgs.identificacaoClienteContrato_label_nomeRazao}','#{msgs.identificacaoClienteContrato_banco}',
	   			'#{msgs.identificacaoClienteContrato_agencia}','#{msgs.identificacaoClienteContrato_conta}');">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>	
	
	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_cliente}:"/>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
   		<br:brPanelGroup id="painelDadosClienteCnpj">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup id="painelDadosClienteNome">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:15px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
    
    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_contrato}:"/>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid id="painelDadosContratoCliente" columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
	   		<br:brPanelGroup id="painelDadosContratoEmpGest">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco2px" value="#{msgs.identificacaoClienteContrato_label_empresa_gestora_contrato}:" style="margin-right: 2"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.empresaGestoraContratoCliente}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
			
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
			
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup id="painelDadosContratoNumContrato" style="margin-right:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco2px" value="#{msgs.identificacaoClienteContrato_label_numero_do_contrato}:" style="margin-right: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.numeroContratoCliente}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup id="painelDadosContratoDescContrato" style="margin-right:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco2px" value="#{msgs.identificacaoClienteContrato_label_descricaoContrato}:" style="margin-right: 2"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.descricaoContratoCliente}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup id="painelDadosContratoSituacao" style="margin-right:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco2px" value="#{msgs.identificacaoClienteContrato_label_situacao}:" style="margin-right: 2"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.situacaoContratoCliente}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
    </br:brPanelGrid>
    
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup> 
			<t:radio for="rdoFiltro" index="1" />
		</br:brPanelGroup>
		<br:brPanelGroup> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_radio_filtro_contrato}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  style="margin-left:20px">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_identificacao_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
						
	<br:brPanelGrid id="painelFiltroContrato" columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" style="margin-left:20px">
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid id="cboEmpresaGestora" columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
				    	<br:brSelectOneMenu id="empresaGestora" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.empresaGestoraFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
							<f:selectItems  value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.listaEmpresaGestora}" />								
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
	
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
	
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_tipo_contrato}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid id="cboTipoContrato" columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="tipoContrato" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.tipoContratoFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}">
							<f:selectItems  value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.listaTipoContrato}" />
							<a4j:support event="onchange" reRender="panelBtoContrato"/>
						</br:brSelectOneMenu>
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="width:20px; margin-bottom:5px" >
			</br:brPanelGroup>		
			
			<br:brPanelGroup id="painelTxtNumero">
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoClienteContrato_numero}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
	
			    <br:brPanelGrid id="txtNumero" columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText id="numero" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaEmpresaGestoraTipoContrato}" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.numeroFiltro}" size="12" maxlength="10" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');">
							<%--<a4j:support event="onblur" action="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.desabilitaCamposContrato}"	reRender="habilitaCampos,situacao, agenciaOperadora, gerenteId, gerenteNome" />--%>		
					    </br:brInputText>	
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>	
		</br:brPanelGrid>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:15px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
    
    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_contrato}:"/>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid id="panelDadosContrato" columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
	   		<br:brPanelGroup id="painelDadosContratoEmpGest2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco2px" value="#{msgs.identificacaoClienteContrato_label_empresa_gestora_contrato}:" style="margin-right: 2"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.empresaGestoraContrato}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
			
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
			
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup id="painelDadosContratoNumContrato2" style="margin-right:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco2px" value="#{msgs.identificacaoClienteContrato_label_numero_do_contrato}:" style="margin-right: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.numeroContrato}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup id="painelDadosContratoDescContrato2" style="margin-right:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco2px" value="#{msgs.identificacaoClienteContrato_label_descricaoContrato}:" style="margin-right: 2"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.descricaoContrato}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup id="painelDadosContratoSituacao2" style="margin-right:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco2px" value="#{msgs.identificacaoClienteContrato_label_situacao}:" style="margin-right: 2"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.situacaoContrato}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
    </br:brPanelGrid>

	<f:verbatim><hr class="lin"> </f:verbatim>

	<br:brPanelGrid id="panelBtoContrato" columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton id="btnConsultar" onclick="return validaCamposContrato(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}', 
				'#{msgs.identificacaoClienteContrato_empresa_gestora_contrato}','#{msgs.identificacaoClienteContrato_tipo_contrato}', '#{msgs.identificacaoClienteContrato_mensagem_obrigatorio}', '#{msgs.identificacaoClienteContrato_obrigatorio_consulta_gerente}');" styleClass="bto1" 
				value="#{msgs.identificacaoClienteContrato_consultar}" action="#{solicitacaoRecuperacaoRemessaBean.consultarContratoSolicitacaoRecuperacaoRemessa}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" 
				disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.desabilitaConsultarContrato}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid>
        <br:brPanelGroup>			
			  <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
   </br:brPanelGrid>

	<br:brPanelGroup id="panelArgPesquisa">
	  	<br:brPanelGrid columns="3" style="margin-top:9px">
		    <br:brPanelGroup>		
                <br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_data_solicitacao}: "/>
				</br:brPanelGroup> 
	
            	<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
		    		<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
							
				<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="0" width="310x">
						<br:brPanelGroup>
							 <br:brPanelGroup>
									<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco"  value="#{msgs.label_de}:"/>
							 </br:brPanelGroup>	
							<br:brPanelGroup rendered="#{!solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaArgumentosPesquisa}">
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoInicio');" id="txtPeriodoInicio" value="#{solicitacaoRecuperacaoRemessaBean.dataSolicitacaoDe}" >
								</app:calendar>
							</br:brPanelGroup>
							<br:brPanelGroup rendered="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaArgumentosPesquisa}">
								<app:calendar id="txtPeriodoInicio2"  value="#{solicitacaoRecuperacaoRemessaBean.dataSolicitacaoDe}" disabled="true" >
								</app:calendar>
							</br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
							<br:brPanelGroup rendered="#{!solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaArgumentosPesquisa}">
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoFim');" id="txtPeriodoFim" value="#{solicitacaoRecuperacaoRemessaBean.dataSolicitacaoAte}" >
				 				</app:calendar>	
							</br:brPanelGroup>	
							<br:brPanelGroup rendered="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaArgumentosPesquisa}">
								<app:calendar id="txtPeriodoFim2" value="#{solicitacaoRecuperacaoRemessaBean.dataSolicitacaoAte}" disabled="true">
				 				</app:calendar>	
							</br:brPanelGroup>	
						</br:brPanelGroup>			    
					</br:brPanelGrid>
				</a4j:outputPanel>	
			</br:brPanelGroup>
	
			<br:brPanelGroup id="panelUsuario">	 	
				<br:brPanelGrid cellpadding="0" cellspacing="0">
					<br:brPanelGroup style="margin-left:20px">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText  styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_usuario}: "/>
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				 	<br:brPanelGroup style="margin-left:20px">
		     		 	<br:brInputText styleClass="HtmlInputTextBradesco"  disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaArgumentosPesquisa}" value="#{solicitacaoRecuperacaoRemessaBean.usuario}" size="35" maxlength="30" id="txtUsuario" />
					</br:brPanelGroup>
				</br:brPanelGrid>	
			</br:brPanelGroup>
				
			<br:brPanelGroup id="cboSituacao">
				<br:brPanelGroup style="margin-left:20px">
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_situacao_solicitacao}: " />
				</br:brPanelGroup>	
			 
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup style="margin-left:20px">
							<br:brSelectOneMenu id="selSituacaoSolicitacao"  value="#{solicitacaoRecuperacaoRemessaBean.cboSituacaoSolicitacao}" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaArgumentosPesquisa}" style="width: 160" styleClass="HtmlSelectOneMenuBradesco">
								<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
								<f:selectItems value="#{solicitacaoRecuperacaoRemessaBean.listaSituacaoSolicitacao}"/>
							 </br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>	
			</br:brPanelGroup>		
	  	</br:brPanelGrid>
			 
		<f:verbatim><hr class="lin"></f:verbatim>
	
	 	<br:brPanelGrid columns="2" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimpar2" style="margin-right:5px"  styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{solicitacaoRecuperacaoRemessaBean.limparTudo}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultarFiltroSituacao" styleClass="bto1" value="#{msgs.btn_consultar}" 
					 action="#{solicitacaoRecuperacaoRemessaBean.consultarDadosGridConsultar}" styleClass="bto1" disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaArgumentosPesquisa}"
					 onclick="javascript:desbloquearTela(); return validaCamposConsultar(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_data_solicitacao}');">		
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><br/></f:verbatim> 
			
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup styleClass="EstiloTabelaSemScroll">
				  <app:scrollableDataTable id="tableFavorecido" value="#{solicitacaoRecuperacaoRemessaBean.listaGridContrato}" var="result" 
						rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
						
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
					    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:30; text-align:center" />
					    </f:facet>	
						<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false"
							 value="#{solicitacaoRecuperacaoRemessaBean.itemSelecionadoListaContrato}">
							<f:selectItems value="#{solicitacaoRecuperacaoRemessaBean.listaControleRadioConsultar}"/>
						<a4j:support event="onclick"  reRender="btnDetalhar,btnExcluir"  />
						</t:selectOneRadio>
				    	<t:radio for="sorLista" index="#{parametroKey}" />
			         </app:scrollableColumn>
											  
					<app:scrollableColumn width="180" styleClass="colTabCenter">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.grid_data_hora_solicitacao}"  style=" text-align:center;width:180"  />
						</f:facet>
						<br:brOutputText value="#{result.dataFormatada}" />
					</app:scrollableColumn>  
								  
					<app:scrollableColumn width="130" styleClass="colTabRight">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.grid_usuario}"  style=" text-align:center;width:130"  />
						</f:facet>
						<br:brOutputText value="#{result.cdUsuarioSolicitacao}" />
					</app:scrollableColumn>
								  
					<app:scrollableColumn width="165" styleClass="colTabLeft">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.label_situacao_solicitacao}"  style=" text-align:center;width:165"  />
						</f:facet>
						<br:brOutputText value="#{result.dsSituacaoSolicitacaoRecuperacao}" />
					</app:scrollableColumn>
								  
					<app:scrollableColumn width="300" styleClass="colTabLeft">
						<f:facet name="header">
							<br:brOutputText value="#{msgs.grid_destino}"   style="text-align:center;width:300" />
						</f:facet>
						<br:brOutputText value="#{result.dsDestinoRecuperacao }" />
					</app:scrollableColumn>
					
				</app:scrollableDataTable>
			</br:brPanelGroup>			   
		</br:brPanelGrid>
			     
	    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>			
			</br:brPanelGroup>	
		</br:brPanelGrid>
					
		<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller2" for="tableFavorecido"  actionListener="#{solicitacaoRecuperacaoRemessaBean.pesquisar}">
					 <f:facet name="first">
					    <brArq:pdcCommandButton id="primeira1"
					      styleClass="bto1"
					      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
					  </f:facet>
					  <f:facet name="fastrewind">
					    <brArq:pdcCommandButton id="retrocessoRapido1"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
					  </f:facet>
					  <f:facet name="previous">
					    <brArq:pdcCommandButton id="anterior1"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
					  </f:facet>
					  <f:facet name="next">
					    <brArq:pdcCommandButton id="proxima1"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
					  </f:facet>
					  <f:facet name="fastforward">
					    <brArq:pdcCommandButton id="avancoRapido1"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
					  </f:facet>
					  <f:facet name="last">
					    <brArq:pdcCommandButton id="ultima1"
					      styleClass="bto1" style="margin-left: 3px;"
					      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
					  </f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGroup>		

    <f:verbatim><hr class="lin"></f:verbatim>

	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">
			<br:brPanelGroup style="text-align:left;width:150px">
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:600px">
				<br:brCommandButton id="btnLimpar" style="margin-right:5px" onclick="javascript:desbloquearTela();"
					disabled="#{empty solicitacaoRecuperacaoRemessaBean.listaGridContrato}"
					styleClass="bto1" value="#{msgs.btn_limpar}" action="#{solicitacaoRecuperacaoRemessaBean.limparGrid}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnExcluir" style="margin-right:5px" onclick="javascript:desbloquearTela();"
					disabled="#{empty solicitacaoRecuperacaoRemessaBean.itemSelecionadoListaContrato}"
					styleClass="bto1" value="#{msgs.btn_excluir}" action="#{solicitacaoRecuperacaoRemessaBean.excluir}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnIncluir" style="margin-right:5px" onclick="javascript:desbloquearTela();"
					disabled="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaArgumentosPesquisa}"
					styleClass="bto1" value="#{msgs.btn_incluir}" action="#{solicitacaoRecuperacaoRemessaBean.incluir}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<br:brCommandButton id="btnDetalhar"  onclick="javascript:desbloquearTela();" styleClass="bto1"
					disabled="#{empty solicitacaoRecuperacaoRemessaBean.itemSelecionadoListaContrato}"
					value="#{msgs.btn_detalhar}" action="#{solicitacaoRecuperacaoRemessaBean.detalhar}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
	
	<t:inputHidden value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.habilitaFiltroDeCliente}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('conSolicitacaoRecuperacaoRemessaForm:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>