<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="listaConsultaClienteRecuperacaoRemessaForm" name="listaConsultaClienteRecuperacaoRemessaForm" >

<br:brPanelGrid columns="1" style="margin-top:9" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
 
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.identificacaoClienteContrato_label_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
   		<br:brPanelGroup >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_cpfcnpj}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.identificacaoClienteContrato_label_nomeRazao}:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>	

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
     
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<app:scrollableDataTable id="scrollTable" value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.listaSaidaConsultarContratoClienteFiltro}" var="result" 
					rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
				    	<br:brOutputText value="" styleClass="tableFontStyle" style="width:30; text-align:center" />
				    </f:facet>	
					<t:selectOneRadio id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false"
						 value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemSelecionadoListaCliente}">
						<f:selectItems value="#{solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.listaControleRadioConsultarCliente}"/>
					<a4j:support event="onclick"  reRender="btnDetalhar,btnExcluir, panelBotoes"  />
					</t:selectOneRadio>
			    	<t:radio for="sorLista" index="#{parametroKey}" />
		         </app:scrollableColumn>
										  
				<app:scrollableColumn width="300" styleClass="colTabCenter">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_empresa_gestora_contrato}"  style=" text-align:center;width:300"  />
					</f:facet>
					<br:brOutputText value="#{result.descricaoPessoaJuridicaFormatada}" />
				</app:scrollableColumn>  
							  
				<app:scrollableColumn width="130" styleClass="colTabRight">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_numero_contrato}"  style=" text-align:center;width:130"  />
					</f:facet>
					<br:brOutputText value="#{result.nrSequenciaContrato}" />
				</app:scrollableColumn>
							  
				<app:scrollableColumn width="220" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_descricao_contrato}"  style=" text-align:center;width:220"  />
					</f:facet>
					<br:brOutputText value="#{result.dsContrato}" />
				</app:scrollableColumn>
							  
				<app:scrollableColumn width="130" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_situacao_contrato}"   style="text-align:center;width:130" />
					</f:facet>
					<br:brOutputText value="#{result.dsSituacaoContrato}" />
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>			   
	</br:brPanelGrid>
		     
    <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
				
	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="scrollTable">
				 <f:facet name="first">
				    <brArq:pdcCommandButton id="primeira"
				      styleClass="bto1"
				      value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}"/>
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}"/>
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}"/>
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton id="proxima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}"/>
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}"/>
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton id="ultima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}"/>
				  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <f:verbatim><hr class="lin"></f:verbatim>

	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">		
		<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >			
				<br:brCommandButton id="btnVoltar" style="margin-right:5px" onclick="javascript:desbloquearTela();" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{identificacaoClienteContratoBean.voltarIdentCliente}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			<br:brPanelGroup style="text-align:right;width:750px">
				<br:brCommandButton id="btnSelecionar" onclick="desbloquearTela();" styleClass="bto1" value="#{msgs.btn_selecionar}"
					action="#{identificacaoClienteContratoBean.selecionarConsultaCliente}"
					disabled="#{empty solicitacaoRecuperacaoRemessaBean.identificacaoClienteContratoBean.itemSelecionadoListaCliente}">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</a4j:outputPanel>

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />

</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm" />
</brArq:form>