<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="confIncluirSolicitacaoRecuperacaoRemessaForm" name="confIncluirSolicitacaoRecuperacaoRemessaForm">

<h:inputHidden id="hiddenQtdeRegistrosSelecionados" value="#{solicitacaoRecuperacaoRemessaBean.qtdeRegistrosSelecionados}"/>

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.retornoLayoutsArquivos_cliente_master}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_cpf_cnpj1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_nome_razao_social1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	  <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin" align="left" width="750px"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.dsNomeRazaoSocialParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin" align="left" width="750px"> </f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.retornoLayoutsArquivos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.empresa}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.numero}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.descricaoContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.participacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	    

    <f:verbatim><hr class="lin"></f:verbatim>
	    
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_remessa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	 <br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
	   <br:brPanelGroup>
   			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right:5px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_destino}:" />
			<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_relatorio}" rendered="#{solicitacaoRecuperacaoRemessaBean.relatorio}"/>
			<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value=" - " rendered="#{(solicitacaoRecuperacaoRemessaBean.relatorio && solicitacaoRecuperacaoRemessaBean.arquivoISD) || (solicitacaoRecuperacaoRemessaBean.relatorio && solicitacaoRecuperacaoRemessaBean.arquivoRetorno)}"/>
			<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_arquivo_isd}"rendered="#{solicitacaoRecuperacaoRemessaBean.arquivoISD}"/>
			<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value=" - " rendered="#{(solicitacaoRecuperacaoRemessaBean.arquivoISD && solicitacaoRecuperacaoRemessaBean.arquivoRetorno)}"/>
			<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_arquivo_retorno}"rendered="#{solicitacaoRecuperacaoRemessaBean.arquivoRetorno}"/>
	   </br:brPanelGroup>
	</br:brPanelGrid>
    
      <f:verbatim><hr class="lin"></f:verbatim>
      
   <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
     <br:brPanelGroup styleClass="EstiloTabelaSemScroll">
      <app:scrollableDataTable id="dataTable" value="#{solicitacaoRecuperacaoRemessaBean.listaGridConfirmar}" width="100%"  var="result" rows="10" rowClasses="tabela_celula_normal, tabela_celula_destaque">
      
			
		<app:scrollableColumn styleClass="colTabRight" width="280px" >
			<f:facet name="header">
		  		<br:brOutputText value="#{msgs.label_identificacao_responsavel_transmissao}" style="text-align:center;width:280"/>
			</f:facet>
				<br:brOutputText value="#{result.cdCnpjCpfPessoa}"/>
		</app:scrollableColumn>	  
						  
		<app:scrollableColumn width="250" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_nome_responsavel_transmissao}" styleClass="tableFontStyle"  style="width:250; text-align:center"  />
			</f:facet>
			<br:brOutputText value="#{result.dsPessoa}" />
		</app:scrollableColumn>
					  
		<app:scrollableColumn width="120" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_layout_arquivo}" styleClass="tableFontStyle"  style="width:120; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsTipoLayoutArquivo }" />
		</app:scrollableColumn>
					  
		<app:scrollableColumn width="120" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_cod_transmissao}" styleClass="tableFontStyle"  style="width:120; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.cdClienteTransfArquivo}" />
		</app:scrollableColumn>
	
		<app:scrollableColumn width="180" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_sequencia_remessa}" styleClass="tableFontStyle"  style="width:180; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.nrArquivoRemessa}" />
		</app:scrollableColumn>	
				
		<app:scrollableColumn width="180" styleClass="colTabCenter">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_data_hora_recepcao}" styleClass="tableFontStyle"  style="width:180; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dataFormatada}" />
		</app:scrollableColumn>		
				
		<app:scrollableColumn width="200" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_situacao_processamento}" styleClass="tableFontStyle"  style="width:200; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsSituacaoProcessamentoRemessa }" />
		</app:scrollableColumn>		
				
		<app:scrollableColumn width="200" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_resultado_processamento}" styleClass="tableFontStyle"  style="width:200; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsCondicaoProcessamentoRemessa }" />
		</app:scrollableColumn>		
		
		<app:scrollableColumn width="150" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_meio_transmissao}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsMeioTransmissao }" />
		</app:scrollableColumn>		
		
		<app:scrollableColumn width="120" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_operacao}" styleClass="tableFontStyle"  style="width:120; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsAmbiente }" />
		</app:scrollableColumn>		
		
		<app:scrollableColumn width="150" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_qtde_registros}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.qtRegistroArqRemessa }" />
		</app:scrollableColumn>		
		
		<app:scrollableColumn width="120" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_valor}" styleClass="tableFontStyle"  style="width:120; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.vlRegistroArqRemessa }" converter="decimalBrazillianConverter"/>
		</app:scrollableColumn>			  
		
	</app:scrollableDataTable>
  </br:brPanelGroup>	
 </br:brPanelGrid>	
 
  <br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
	<br:brPanelGroup>
	            <brArq:pdcDataScroller id="dataScroller" for="dataTable">
						<f:facet name="first">
				  <brArq:pdcCommandButton id="primeira"
				    styleClass="HtmlCommandButtonBradesco"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
		</brArq:pdcDataScroller>
	</br:brPanelGroup>	
  </br:brPanelGrid>	
	    
	      <f:verbatim><hr class="lin"></f:verbatim>
	      
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">		
		
	  	<br:brPanelGroup style="text-align:left;width:40px">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" disabled="" action="#{solicitacaoRecuperacaoRemessaBean.voltarConfirmar}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="text-align:right;width:709px">
			<br:brCommandButton id="btnConfirmar" value="#{msgs.btn_confirmar}" onclick="javascript: if(!confirm('#{msgs.label_conf_inclusao}')) { desbloquearTela(); return false; } " action="#{solicitacaoRecuperacaoRemessaBean.confirmarIncluir}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>					
	</br:brPanelGroup>
	
	</br:brPanelGrid>
	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm" />
</brArq:form>	