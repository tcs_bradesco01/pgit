<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<brArq:form id="incSolicitacaoRecuperacaoRemessaForm" name="incSolicitacaoRecuperacaoRemessaForm">
<h:inputHidden id="hiddenQtdeRegistrosSelecionados" value="#{solicitacaoRecuperacaoRemessaBean.qtdeRegistrosSelecionados}"/>
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.retornoLayoutsArquivos_cliente_master}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_cpf_cnpj1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.cpfCnpjMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_nome_razao_social1}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.nomeRazaoSocialMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_grupo_economico}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.grupoEconomicoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_atividade_economica}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.atividadeEconomicaMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.segmentoMaster}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_sub_segmento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.subSegmentoMaster}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	  <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.lebel_agencia_gestora}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.dsAgenciaGestora}"/>
		</br:brPanelGroup>				
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conDadosBasicos_unidadesOrganizacionaisResponsaveis_gerenteResponsavel}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.dsGerenteResponsavel}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>

	<f:verbatim><hr class="lin" align="left" width="750px"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conDadosBasicos_clienteParticipante}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.cpfCnpjParticipante}"/>
		</br:brPanelGroup>				
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_nome_razao_social}:"/> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.dsNomeRazaoSocialParticipante}"/>
		</br:brPanelGroup>			
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin" align="left" width="750px"> </f:verbatim>

	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.retornoLayoutsArquivos_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>

    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_empresa}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.empresa}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_tipo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.tipo}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_numero}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.numero}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_descricao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.descricaoContrato}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.situacaoDesc}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_motivo}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.motivoDesc}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.retornoLayoutsArquivos_participacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{solicitacaoRecuperacaoRemessaBean.participacao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	    
	

    <f:verbatim><hr class="lin"></f:verbatim>

   	 <br:brPanelGrid columns="1">
        <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
 </br:brPanelGrid>         
		
<br:brPanelGrid columns="2" style="margin-top:9px" cellpadding="0" cellspacing="0">
	       
	     <br:brPanelGroup>		
	   
	                 <br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_data_recepcao}: "/>
					</br:brPanelGroup> 
		
	         <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
				
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
			      		       			
				<br:brPanelGroup>
					<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
							 <br:brPanelGroup>
									<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco"  value="#{msgs.label_de}:"/>
							 </br:brPanelGroup>	
							<br:brPanelGroup>
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoInicio');" id="txtPeriodoInicio" value="#{solicitacaoRecuperacaoRemessaBean.dataRecepcaoDe}" >
								</app:calendar>
							</br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
							<br:brPanelGroup>
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoFim');" id="txtPeriodoFim" value="#{solicitacaoRecuperacaoRemessaBean.dataRecepcaoAte}" >
				 				</app:calendar>	
							</br:brPanelGroup>	
						</br:brPanelGroup>			    
					</br:brPanelGrid>
				</a4j:outputPanel>	
								
								 	
				</br:brPanelGroup>
					
			</br:brPanelGrid>
		</br:brPanelGroup>
		
			
		<br:brPanelGroup>
		  <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:20px">
		
		 	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
	 			<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText  styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_sequencia_remessa}: "/>
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
				
			  <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
           		<br:brPanelGroup>
	     		 	<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();"  value="#{solicitacaoRecuperacaoRemessaBean.sequenciaRemessa}" size="15" maxlength="9" id="txtSequenciaRemessa" />
				</br:brPanelGroup>
			 </br:brPanelGrid>	
			 
		 </br:brPanelGrid>
		</br:brPanelGroup>
			
	 	
  </br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
	      
	      	 <br:brPanelGroup>			 
			  <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
			      
			        <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				        <br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_situacao_processamento}: "/>
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">				
					<br:brPanelGroup>
					    <br:brSelectOneMenu id="situacaoProcessamento" value="#{solicitacaoRecuperacaoRemessaBean.itemSituaProcessamento}"  styleClass="HtmlSelectOneMenuBradesco" >
			               <f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
			               <f:selectItems value="#{solicitacaoRecuperacaoRemessaBean.listaSituacaoProcessamento}" />
						   <a4j:support oncomplete="javascript:foco(this);" event="onchange" status="statusAguarde" action="#{solicitacaoRecuperacaoRemessaBean.limparFiltroResultado}" reRender="resultadoProcessamento"/>
			            </br:brSelectOneMenu>
				  	</br:brPanelGroup>
				</br:brPanelGrid>
				
			  </br:brPanelGrid>
			</br:brPanelGroup>
	
			<br:brPanelGroup>			 
			  <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:20px">
			      
			        <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				        <br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_resultado_processamento}: "/>
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">						
					<br:brPanelGroup>
					    <br:brSelectOneMenu id="resultadoProcessamento" value="#{solicitacaoRecuperacaoRemessaBean.itemResultProcessamento}"  styleClass="HtmlSelectOneMenuBradesco" style="width: 200">
			               <f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{solicitacaoRecuperacaoRemessaBean.listaResultadoProcessamento}" />
			            </br:brSelectOneMenu>
				  	</br:brPanelGroup>
				</br:brPanelGrid>
				
			 </br:brPanelGrid>
			</br:brPanelGroup>
			
			<br:brPanelGroup>			 
			  <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:20px">
			       
			        <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
				        <br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_layout_arquivo}: "/>
						</br:brPanelGroup>	
					</br:brPanelGrid>
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
					    <br:brPanelGroup>			
						</br:brPanelGroup>	
					</br:brPanelGrid>
					
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">				
					<br:brPanelGroup>
					    <br:brSelectOneMenu id="layoutArquivo" value="#{solicitacaoRecuperacaoRemessaBean.itemLayoutArquivo}"  styleClass="HtmlSelectOneMenuBradesco" style="width: 200">
			               <f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
			               <f:selectItems value="#{solicitacaoRecuperacaoRemessaBean.listarTipoLayoutArquivo}" />
			           </br:brSelectOneMenu>
				  	</br:brPanelGroup>
				</br:brPanelGrid>
					
				</br:brPanelGrid>
			</br:brPanelGroup>
		
		</br:brPanelGrid>
				
	
    <f:verbatim><hr class="lin"></f:verbatim>
 
 <br:brPanelGrid columns="2" width="100%"  style="text-align:right;width:750px" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>	
			<br:brCommandButton id="btnLimpar2" style="margin-right:5px"  styleClass="bto1" value="#{msgs.label_limpar_campos}" action="#{solicitacaoRecuperacaoRemessaBean.limparIncluir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		
			<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.btn_consultar}" action="#{solicitacaoRecuperacaoRemessaBean.populaGridIncluir}"   styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" 
			onclick="javascript:desbloquearTela(); return validaCamposConsultar(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_data_recepcao}');">		
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_remessa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="1" width="100%"  style="text-align:left" cellpadding="0" cellspacing="0">	
	    <br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_destino}:" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:6px" >
			<br:brPanelGroup>
			    <h:selectBooleanCheckbox  id="relatorio" styleClass="HtmlSelectOneRadioBradesco" value="#{solicitacaoRecuperacaoRemessaBean.relatorio}"> 

				</h:selectBooleanCheckbox>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" style="margin-right:20px" value="#{msgs.label_relatorio}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
			    <h:selectBooleanCheckbox  id="arquivoISD" styleClass="HtmlSelectOneRadioBradesco" value="#{solicitacaoRecuperacaoRemessaBean.arquivoISD}"  >

				</h:selectBooleanCheckbox>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" style="margin-right:20px" value="#{msgs.label_arquivo_isd}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
			    <h:selectBooleanCheckbox  id="arquivoRetorno" styleClass="HtmlSelectOneRadioBradesco" value="#{solicitacaoRecuperacaoRemessaBean.arquivoRetorno}"  >

				</h:selectBooleanCheckbox>	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" style="margin-right:20px" value="#{msgs.label_arquivo_retorno}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <f:verbatim><hr class="lin"></f:verbatim>
	    		


   <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
     <br:brPanelGroup styleClass="EstiloTabelaSemScroll">
      <app:scrollableDataTable id="dataTable" value="#{solicitacaoRecuperacaoRemessaBean.listaGridIncluir}" width="100%" rowIndexVar="parametroKey" var="result" rows="10" rowClasses="tabela_celula_normal, tabela_celula_destaque">
      
		<app:scrollableColumn styleClass="colTabCenter" width="30px">		
			<f:facet name="header">					
				  <t:selectBooleanCheckbox id="checkGrid" styleClass="HtmlSelectOneRadioBradesco" value="#{solicitacaoRecuperacaoRemessaBean.checkTudo}" disabled="#{empty solicitacaoRecuperacaoRemessaBean.listaGridIncluir}"   >
						<a4j:support oncomplete="javascript:foco(this);" event="onclick" action="#{solicitacaoRecuperacaoRemessaBean.checaTodos}" reRender="dataTable,hiddenQtdeRegistrosSelecionados" status="statusAguarde"/> 
				  </t:selectBooleanCheckbox>
			 </f:facet>					 		
					
			<t:selectBooleanCheckbox  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" value="#{result.check}" onclick="javascript:atualizaRegistrosSelecionado(this);">
				<f:selectItems value="#{solicitacaoRecuperacaoRemessaBean.listaGridIncluir}"/>							

			</t:selectBooleanCheckbox>						
		</app:scrollableColumn>
			
		<app:scrollableColumn styleClass="colTabRight" width="280px" >
			<f:facet name="header">
		  		<br:brOutputText value="#{msgs.label_identificacao_responsavel_transmissao}" style="text-align:center;width:280"/>
			</f:facet>
				<br:brOutputText value="#{result.cdCnpjCpfPessoa}"/>
		</app:scrollableColumn>	  
						  
		<app:scrollableColumn width="250" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_nome_responsavel_transmissao}" styleClass="tableFontStyle"  style="width:250; text-align:center"  />
			</f:facet>
			<br:brOutputText value="#{result.dsPessoa}" />
		</app:scrollableColumn>
					  
		<app:scrollableColumn width="120" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_layout_arquivo}" styleClass="tableFontStyle"  style="width:120; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsTipoLayoutArquivo }" />
		</app:scrollableColumn>
					  
		<app:scrollableColumn width="120" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_cod_transmissao}" styleClass="tableFontStyle"  style="width:120; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.cdClienteTransfArquivo}" />
		</app:scrollableColumn>
	
		
		<app:scrollableColumn width="180" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_sequencia_remessa}" styleClass="tableFontStyle"  style="width:180; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.nrArquivoRemessa}" />
		</app:scrollableColumn>	
				
		<app:scrollableColumn width="180" styleClass="colTabCenter">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_data_hora_recepcao}" styleClass="tableFontStyle"  style="width:180; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dataFormatada }" />
		</app:scrollableColumn>		
				
		<app:scrollableColumn width="200" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_situacao_processamento}" styleClass="tableFontStyle"  style="width:200; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsSituacaoProcessamentoRemessa }" />
		</app:scrollableColumn>		
				
		<app:scrollableColumn width="200" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_resultado_processamento}" styleClass="tableFontStyle"  style="width:200; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsCondicaoProcessamentoRemessa }" />
		</app:scrollableColumn>		
		
		<app:scrollableColumn width="150" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_meio_transmissao}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsMeioTransmissao }" />
		</app:scrollableColumn>		
		
		<app:scrollableColumn width="120" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_operacao}" styleClass="tableFontStyle"  style="width:120; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsAmbiente }" />
		</app:scrollableColumn>		
		
		<app:scrollableColumn width="150" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_qtde_registros}" styleClass="tableFontStyle"  style="width:150; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.qtRegistroArqRemessa }"/>
		</app:scrollableColumn>		
		
		<app:scrollableColumn width="120" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_valor}" styleClass="tableFontStyle"  style="width:120; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.vlRegistroArqRemessa }" converter="decimalBrazillianConverter"  />
		</app:scrollableColumn>			  
		
	</app:scrollableDataTable>
  </br:brPanelGroup>	
 </br:brPanelGrid>	
 
 <br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
	<br:brPanelGroup>
	            <brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{solicitacaoRecuperacaoRemessaBean.pesquisarIncluir}">
						<f:facet name="first">
				  <brArq:pdcCommandButton id="primeira"
				    styleClass="HtmlCommandButtonBradesco"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima_msg}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
		</brArq:pdcDataScroller>
	</br:brPanelGroup>	
  </br:brPanelGrid>	
		
    <f:verbatim><hr class="lin"></f:verbatim>

<a4j:outputPanel id="panelBotoes" ajaxRendered="true"> 
 <br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0">		
	  	<br:brPanelGroup style="text-align:left;width:40px">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="#{msgs.btn_voltar}" action="#{solicitacaoRecuperacaoRemessaBean.voltarIncluir}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>


	<br:brPanelGroup style="text-align:right;width:710px">
			<br:brCommandButton id="btnLimpar" value="#{msgs.btn_limpar}"  disabled="#{empty solicitacaoRecuperacaoRemessaBean.listaGridIncluir}" action="#{solicitacaoRecuperacaoRemessaBean.limparGridIncluir}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
					
			<br:brCommandButton style="cursor:hand;margin-left:5px" id="btnSolicitarRecuperacao" disabled="#{solicitacaoRecuperacaoRemessaBean.qtdeRegistrosSelecionados == null || solicitacaoRecuperacaoRemessaBean.qtdeRegistrosSelecionados <= 0}" value="#{msgs.btn_solicitar_recuperação}" action="#{solicitacaoRecuperacaoRemessaBean.confirmar}" styleClass="bto1" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);"  onclick = "javascript:desbloquearTela(); return validarSolicitacao(document.forms[1],'#{msgs.label_selecionar_pelo_menos_um_item_lista}','#{msgs.label_ocampo}', '#{msgs.label_necessario}', '#{msgs.label_destino}' ,'#{msgs.label_maximo_quarenta_registros_permitido}' );">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
	</br:brPanelGroup>
 </br:brPanelGrid>
 	</a4j:outputPanel>	
 
 	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()"/>   
	
</br:brPanelGrid> 
<brArq:validatorScript functionName="validateForm" />
</brArq:form>