<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterVincEmprPagContSalSubstituirForm" name="manterVincEmprPagContSalSubstituirForm" >

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" style="margin-left:5px; width:770">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_title_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_cpf_cnpj}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_nome_razao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_title_contrato}: "/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_empresa}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.dsEmpresaGestoraContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_tipo}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>	
	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
    	<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="N�mero do Contrato: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_descricao_contrato}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_situacao}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
		
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_title_conta_anterior}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_banco}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaVincContaSalarioEmpresa.bancoSalario}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_agencia}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaVincContaSalarioEmpresa.agenciaSalario}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_conta}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaVincContaSalarioEmpresa.contaDigito}"/>
		</br:brPanelGroup>						
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_situacao_conta}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaVincContaSalarioEmpresa.cdSituacaoConta}"/>
		</br:brPanelGroup>							
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_title_conta_atual}: "/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_banco_label}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>	
			    	<br:brInputText onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterVinculoEmpresaPagBean.entradaSubstituirVincCtaSalarioEmp.cdBancoAtualizado}" size="5" maxlength="3" id="txtBanco" 
			    	onkeyup="proximoCampo(3,'manterVincEmprPagContSalSubstituirForm','manterVincEmprPagContSalSubstituirForm:txtBanco','manterVincEmprPagContSalSubstituirForm:txtAgencia');" style="margin-right:20px">  
						<brArq:commonsValidator type="required" arg="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_banco_label}" server="false" client="true"/>
					</br:brInputText>	
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>	
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_agencia_label}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterVinculoEmpresaPagBean.entradaSubstituirVincCtaSalarioEmp.cdAgenciaAtualizada}" size="7" maxlength="5" id="txtAgencia" 
			    	onkeyup="proximoCampo(5,'manterVincEmprPagContSalSubstituirForm','manterVincEmprPagContSalSubstituirForm:txtAgencia','manterVincEmprPagContSalSubstituirForm:txtConta');">  
						<brArq:commonsValidator type="required" arg="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_agencia_label}" server="false" client="true"/>						
					</br:brInputText>	
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>				
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_conta_label}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >			
			    <br:brPanelGroup>	
			    	<br:brInputText onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterVinculoEmpresaPagBean.entradaSubstituirVincCtaSalarioEmp.cdContaAtualizada}" size="17" maxlength="13" id="txtConta" onkeyup="proximoCampo(13,'manterVincEmprPagContSalSubstituirForm','manterVincEmprPagContSalSubstituirForm:txtConta','manterVincEmprPagContSalSubstituirForm:txtDigito');">  
						<brArq:commonsValidator type="required" arg="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_conta_label}" server="false" client="true"/>										    	
					</br:brInputText>	
				</br:brPanelGroup>
				<br:brPanelGroup style="width:5px; margin-bottom:5px" >
				</br:brPanelGroup>
				<br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterVinculoEmpresaPagBean.entradaSubstituirVincCtaSalarioEmp.cdDigitoContaAtualizada}" size="3" maxlength="2" id="txtDigito" >  
 						<brArq:commonsValidator type="required" arg="D�gito Conta Sal�rio" server="false" client="true"/>
 					</br:brInputText>	
				</br:brPanelGroup>
				<br:brPanelGroup style="width:5px; margin-bottom:5px" >
				</br:brPanelGroup>				
				<br:brPanelGroup>
					<br:brMessageColor for="txtConta" id="longmessage" />
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>				
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"> </f:verbatim>		

 	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.botao_voltar}" action="#{manterVinculoEmpresaPagBean.voltarPesquisar}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>				
		</br:brPanelGroup>
		<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
		</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
			<br:brCommandButton id="btnAvancar" onclick="return validateForm(document.forms[1]);" styleClass="bto1" disabled="false"  value="#{msgs.PGIC0012_label_botao_avancar}"  action="#{manterVinculoEmpresaPagBean.avancarSubstituir}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>		
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
	<br:brPanelGrid columns="1" style="margin-top:30px" cellpadding="0" cellspacing="0" > 
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>   	
</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
