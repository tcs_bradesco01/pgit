<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterVincEmprPagContSalHistoricoForm" name="manterVincEmprPagContSalHistoricoForm" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" width="770" style="margin-left:5px">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.histManterVinculoEmpresaPagadoraContaSalario_title_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.histDetManterVinculoEmpresaPagadoraContaSalario_cpf_cnpj}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.histDetManterVinculoEmpresaPagadoraContaSalario_nome_razao}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.histManterVinculoEmpresaPagadoraContaSalario_title_contrato}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.histDetManterVinculoEmpresaPagadoraContaSalario_empresa}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.dsEmpresaGestoraContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.histDetManterVinculoEmpresaPagadoraContaSalario_tipo}: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>	
	
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >	
	 	<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="N�mero do Contrato: "/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.histDetManterVinculoEmpresaPagadoraContaSalario_descricao_contrato}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.histDetManterVinculoEmpresaPagadoraContaSalario_situacao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
		
	 <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
		<br:brPanelGroup>
			<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>
			<h:selectBooleanCheckbox id="chkDadosContaSalarioHistorico" value="#{manterVinculoEmpresaPagBean.chkDadosContaSalarioHistorico}" disabled="#{manterVinculoEmpresaPagBean.disableArgumentosConsultaHistorico}" style="margin-right:5px">
				<a4j:support status="statusAguarde" event="onclick" reRender="txtBanco,txtAgencia,txtConta,txtDigitoConta" action="#{manterVinculoEmpresaPagBean.limparDadosContaSalario}"/>
			</h:selectBooleanCheckbox>		
		</br:brPanelGroup>
		<br:brPanelGroup>
		    <br:brPanelGrid columns="4">
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco_conta_salario}:" />
						</br:brPanelGroup>	
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>				
							<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterVinculoEmpresaPagBean.disableArgumentosConsultaHistorico || !manterVinculoEmpresaPagBean.chkDadosContaSalarioHistorico}" value="#{manterVinculoEmpresaPagBean.bancoSalarioHistorico}" size="4" maxlength="3" id="txtBanco" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>     
		
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agencia_conta_salario}:" />
						</br:brPanelGroup>	
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>				
							<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterVinculoEmpresaPagBean.disableArgumentosConsultaHistorico || !manterVinculoEmpresaPagBean.chkDadosContaSalarioHistorico}" value="#{manterVinculoEmpresaPagBean.agenciaSalarioHistorico}" size="6" maxlength="5" id="txtAgencia" />
						</br:brPanelGroup>
					</br:brPanelGrid>
				</br:brPanelGroup>		
				
				<br:brPanelGroup>
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_conta_salario}:" />
						</br:brPanelGroup>	
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>				
							<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{manterVinculoEmpresaPagBean.disableArgumentosConsultaHistorico || !manterVinculoEmpresaPagBean.chkDadosContaSalarioHistorico}" value="#{manterVinculoEmpresaPagBean.contaSalarioHistorico}" size="17" maxlength="13" id="txtConta" />
							<br:brInputText style="margin-left: 5px" id="txtDigitoConta" size="3" maxlength="2" styleClass="HtmlInputTextBradesco" disabled="#{manterVinculoEmpresaPagBean.disableArgumentosConsultaHistorico || !manterVinculoEmpresaPagBean.chkDadosContaSalarioHistorico}" value="#{manterVinculoEmpresaPagBean.digitoContaSalarioHistorico}" />
						</br:brPanelGroup>					
					</br:brPanelGrid>
				</br:brPanelGroup>	
		    </br:brPanelGrid>
		</br:brPanelGroup>													
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
	    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_periodo_pesquisa_historico}:" />
				</br:brPanelGroup>	
			</br:brPanelGrid>
		
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>
						    
			<br:brPanelGroup>
				<br:brPanelGroup rendered="#{!manterVinculoEmpresaPagBean.disableArgumentosConsultaHistorico}">
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'periodoInicialHistorico');" id="periodoInicialHistorico" value="#{manterVinculoEmpresaPagBean.periodoInicialHistorico}" >
					</app:calendar>
				</br:brPanelGroup>
				<br:brPanelGroup rendered="#{manterVinculoEmpresaPagBean.disableArgumentosConsultaHistorico}">
					<app:calendar id="periodoInicialHistoricoDesc" value="#{manterVinculoEmpresaPagBean.periodoInicialHistorico}" disabled="true" >
					</app:calendar>
				</br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
				<br:brPanelGroup rendered="#{!manterVinculoEmpresaPagBean.disableArgumentosConsultaHistorico}">
					<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'periodoFinalHistorico');" id="periodoFinalHistorico" value="#{manterVinculoEmpresaPagBean.periodoFinalHistorico}" >
	 				</app:calendar>	
				</br:brPanelGroup>	
				<br:brPanelGroup rendered="#{manterVinculoEmpresaPagBean.disableArgumentosConsultaHistorico}">
					<app:calendar id="periodoFinalHistoricoDesc" value="#{manterVinculoEmpresaPagBean.periodoFinalHistorico}" disabled="true">
	 				</app:calendar>	
				</br:brPanelGroup>	
			</br:brPanelGroup>			    
		</br:brPanelGrid>
	</a4j:outputPanel>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
    <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="limpar campos" action="#{manterVinculoEmpresaPagBean.limparPaginaHistorico}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brCommandButton onclick="javascript:desbloquearTela(); return validarCampoHistoricoVinculoEmpresaPagadoraContaSalario(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_banco}','#{msgs.label_agencia}','#{msgs.label_conta}','#{msgs.label_digito_conta}');" id="btnConsultar" styleClass="bto1" value="consultar" disabled="#{manterVinculoEmpresaPagBean.disableArgumentosConsultaHistorico}" action="#{manterVinculoEmpresaPagBean.consultarHistorico}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >		
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><br></f:verbatim> 

	<br:brPanelGrid columns="1" width="750px" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170 ">	
			<a4j:region id="regionFiltroPesquisa">
				<t:selectOneRadio id="sor" value="#{manterVinculoEmpresaPagBean.itemSelecionadoListaHistorico}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
					<f:selectItems value="#{manterVinculoEmpresaPagBean.selectItemListaHitorico}"/>
					<a4j:support event="onclick" reRender="panelBotoes" limitToList="true" ajaxSingle="true"  />
				</t:selectOneRadio>
			</a4j:region>
		
			<app:scrollableDataTable id="dataTable" value="#{manterVinculoEmpresaPagBean.listaHistVincContaSalarioEmpresa}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">			
				<app:scrollableColumn styleClass="colTabCenter" width="30px">
					<f:facet name="header">
						<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:radio for=":manterVincEmprPagContSalHistoricoForm:sor" index="#{parametroKey}" />	
				</app:scrollableColumn> 

				<app:scrollableColumn width="200px" styleClass="colTabCenter" >			  
				    <f:facet name="header">
				   		<h:outputText value="#{msgs.histManterVinculoEmpresaPagadoraContaSalario_grid_banco}" style="width:200; text-align:center" /> 
				    </f:facet>
				    <br:brOutputText value="#{result.banco}" style="width:200; text-align:center"  />
				</app:scrollableColumn>			  
				  
				<app:scrollableColumn width="300px" styleClass="colTabRight" >			  
				    <f:facet name="header">
				    	<h:outputText value="#{msgs.histManterVinculoEmpresaPagadoraContaSalario_grid_agencia}" style="width:300; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.agenciaDigito}" style="width:300; text-align:center"  /> 
				</app:scrollableColumn>			  
				  
				<app:scrollableColumn width="200px" styleClass="colTabRight" >			  			  
				    <f:facet name="header">
				      <h:outputText value="#{msgs.histManterVinculoEmpresaPagadoraContaSalario_grid_conta_salario}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.contaDigitoSalario}" style="width:200; text-align:center"  /> 
				</app:scrollableColumn>
				
				<app:scrollableColumn width="200" styleClass="colTabLeft" >			  			  
				    <f:facet name="header">
				      <h:outputText value="#{msgs.histManterVinculoEmpresaPagadoraContaSalario_tipo_manutencao}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.tpManutencao}" style="width:200; text-align:left"  /> 
				</app:scrollableColumn>

				<app:scrollableColumn width="200px" styleClass="colTabCenter" >			  
				    <f:facet name="header">
				    	<h:outputText value="#{msgs.histManterVinculoEmpresaPagadoraContaSalario_grid_data_hora_manuntecao}" style="width:200; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.dataHoraManutencao}"  style="width:200; text-align:center" />
				</app:scrollableColumn>
				
				<app:scrollableColumn width="100px" styleClass="colTabLeft" >			  			  
				    <f:facet name="header">
				      <h:outputText value="#{msgs.histManterVinculoEmpresaPagadoraContaSalario_usuario}" style="width:100; text-align:center" />
				    </f:facet>
				    <br:brOutputText value="#{result.cdUsuario}" style="width:100; text-align:left"  /> 
				</app:scrollableColumn>			  
			</app:scrollableDataTable>	
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{vincEmprPagContSalBean.submitPesquisarHistorico}">
				<f:facet name="first">
			    	<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira}"/>
			  	</f:facet>
			  	<f:facet name="fastrewind">
			  		<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}"/>
			  	</f:facet>
			  	<f:facet name="previous">
			    	<brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			  	</f:facet>
			  	<f:facet name="next">
			    	<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			  	</f:facet>
			  	<f:facet name="fastforward">
			    	<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco}"/>
			  	</f:facet>
			  	<f:facet name="last">
			    	<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima}"/>
			  	</f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid columns="2" width="100%" style="text-align:left" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup style="width:100%; text-align:left">
			<br:brCommandButton id="btnVoltar" value="#{msgs.botao_voltar}" action="#{manterVinculoEmpresaPagBean.voltarPesquisar}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		
		<a4j:outputPanel id="panelBotoes" style="width:100%; text-align:right" ajaxRendered="true">
			<br:brCommandButton id="btnDetalhar" value="#{msgs.histManterVinculoEmpresaPagadoraContaSalario_btn_detalhar}" disabled="#{empty manterVinculoEmpresaPagBean.itemSelecionadoListaHistorico}" action="#{manterVinculoEmpresaPagBean.detalharHistorico}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</a4j:outputPanel>

	</br:brPanelGrid>		
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>

</brArq:form>
