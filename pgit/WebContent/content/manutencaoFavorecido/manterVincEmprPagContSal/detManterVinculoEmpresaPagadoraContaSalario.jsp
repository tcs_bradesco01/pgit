<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterVincEmprPagContSalDetalharForm" name="manterVincEmprPagContSalDetalharForm" >

	<br:brPanelGrid columns="1" styleClass="CorpoPagina"  width="770" style="margin-left:5px">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_title_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_cpf_cnpj}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_nome_razao}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
			
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_title_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_empresa}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.dsEmpresaGestoraContrato}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_tipo}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
			</br:brPanelGroup>	

		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	    
	    	<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="N�mero do Contrato: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
			</br:brPanelGroup>	
				
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"  />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_descricao_contrato}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_situacao}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<f:verbatim><hr class="lin"> </f:verbatim>
			
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_title_conta_salario}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_banco}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaVincContaSalarioEmpresa.bancoSalario}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_agencia}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaVincContaSalarioEmpresa.agenciaSalario}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_conta}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaVincContaSalarioEmpresa.contaDigito}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_transferencia_automatica}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarDetVinculoCtaSalarioEmpresa.cdTransmissaoAutomatica}"/>
			</br:brPanelGroup>		
		</br:brPanelGrid>
		
	
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		 <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_nome_conta}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaVincContaSalarioEmpresa.nmTitular}"/>
			</br:brPanelGroup>
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_cpf_conta}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarDetVinculoCtaSalarioEmpresa.cpfCnpj}"/>
			</br:brPanelGroup>								
		</br:brPanelGrid>				
		
		<f:verbatim><hr class="lin"></f:verbatim>
	
        <br:brOutputText styleClass="HtmlOutputTextBradesco" value="Trilha de auditoria - Inclus�o:"/>

        <br:brPanelGrid columns="2" style="margin-top: 6">
              <br:brPanelGroup style="text-align:left;" >
                    <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
                    <br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.excSolicitacaoRelatoriosFavorecidos_data_hora_solicitacao}: " />
                    <br:brOutputText id="dtPeriodoInc" styleClass="HtmlOutputTextBoldBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarDetVinculoCtaSalarioEmpresa.dataHoraInclusao}"/>
              </br:brPanelGroup>      

              <br:brPanelGroup style="text-align:left;">
                    <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>
                    <br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detSolicitacaoRelatoriosFavorecidos_usuario}: "  />
                    <br:brOutputText id="cdUsuarioInc" styleClass="HtmlOutputTextBoldBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarDetVinculoCtaSalarioEmpresa.cdUsuarioInclusao}"/>
              </br:brPanelGroup>      

              <br:brPanelGroup style="text-align:left;">
                    <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
                    <br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo Canal: " />
                    <br:brOutputText id="cdCanalInc" styleClass="HtmlOutputTextBoldBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarDetVinculoCtaSalarioEmpresa.canalInclusao}"/>
              </br:brPanelGroup>      

              <br:brPanelGroup style="text-align:left;">
                    <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />
                    <br:brOutputText styleClass="HtmlOutputTextBradesco" value="Complemento: " />
                    <br:brOutputText id="cdComplementoInc" styleClass="HtmlOutputTextBoldBradesco" value=""/>
              </br:brPanelGroup>      
        </br:brPanelGrid>
	
        <f:verbatim><hr class="lin"></f:verbatim>

        <br:brOutputText styleClass="HtmlOutputTextBradesco" value="Trilha de auditoria - Manuten��o:"/>

        <br:brPanelGrid columns="2" style="margin-top: 6">
	          <br:brPanelGroup style="text-align:left;" >
	                <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
	                <br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.excSolicitacaoRelatoriosFavorecidos_data_hora_solicitacao}: " />
	                <br:brOutputText id="dtPeriodoManu" styleClass="HtmlOutputTextBoldBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarDetVinculoCtaSalarioEmpresa.dataHoraManutencao}"/>
	          </br:brPanelGroup>      
	
	          <br:brPanelGroup style="text-align:left;">
	                <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />
	                <br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detSolicitacaoRelatoriosFavorecidos_usuario}: " />
	                <br:brOutputText id="cdUsuarioManu" styleClass="HtmlOutputTextBoldBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarDetVinculoCtaSalarioEmpresa.cdUsuarioManutencao}" />
	          </br:brPanelGroup>      
	
	          <br:brPanelGroup style="text-align:left;">
	                <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
	                <br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo Canal: " />
	                <br:brOutputText id="dtCanalManu" styleClass="HtmlOutputTextBoldBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarDetVinculoCtaSalarioEmpresa.canalManutencao}"/>
	          </br:brPanelGroup>
	
	          <br:brPanelGroup style="text-align:left;">
	                <br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />
	                <br:brOutputText styleClass="HtmlOutputTextBradesco" value="Complemento: "  />
	                <br:brOutputText id="cdComplementoManu" styleClass="HtmlOutputTextBoldBradesco" value="" style="margin-left:20px" />
	          </br:brPanelGroup>
        </br:brPanelGrid>	
			
		<f:verbatim><hr class="lin"> </f:verbatim>		
	
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.detManterVinculoEmpresaPagadoraContaSalario_btn_voltar}" action="#{manterVinculoEmpresaPagBean.voltarPesquisar}">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
	</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
