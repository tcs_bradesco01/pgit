<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterVincEmprPagContSalSubstConfForm" name="manterVincEmprPagContSalSubstConfForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.substConfManterVinculoEmpresaPagadoraContaSalario_title_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_nome_razao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
			
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.substConfManterVinculoEmpresaPagadoraContaSalario_title_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_empresa}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.dsEmpresaGestoraContrato}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_tipo}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
			</br:brPanelGroup>	
	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	    	<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="N�mero do Contrato: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_descricao_contrato}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_situacao}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_conta_salario}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_banco}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.entradaIncluirVinculoEmprPagContSal.banco}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_agencia}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.entradaIncluirVinculoEmprPagContSal.agencia}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_conta_salario}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.entradaIncluirVinculoEmprPagContSal.contaDigito}"/>
			</br:brPanelGroup>						
		</br:brPanelGrid>	
			
		<f:verbatim><hr class="lin"> </f:verbatim>		
	
	 	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{manterVinculoEmpresaPagBean.voltarIncluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
				<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnAvancar" styleClass="bto1" disabled="false"  value="#{msgs.btn_confirmar}"  action="#{manterVinculoEmpresaPagBean.confirmarInclusao}" onclick="javascript: if (!confirm('#{msgs.label_confirma_inclusao}')) { desbloquearTela(); return false; }">
					<brArq:submitCheckClient/>
				</br:brCommandButton>		
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid columns="1" style="margin-top:50px" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>   	
	</br:brPanelGrid>
	
	<brArq:validatorScript functionName="validateForm"/>
<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>
