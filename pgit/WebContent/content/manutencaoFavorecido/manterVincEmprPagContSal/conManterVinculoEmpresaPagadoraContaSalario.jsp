<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterVincEmprPagContSalForm" name="manterVincEmprPagContSalForm">
	<br:brPanelGrid columns="1" width="770" style="margin-top:9; margin-left:5px">
	    	
	 	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Identifica��o do Cliente:"/>
	 	
	 	<a4j:region id="regionFiltro">
			<t:selectOneRadio id="rdoFiltroCliente" value="#{manterVinculoEmpresaPagBean.itemFiltroSelecionado}" disabled="#{manterVinculoEmpresaPagBean.bloqueaRadio}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<f:selectItem itemValue="3" itemLabel="" />
				<a4j:support event="onclick" reRender="panelCnpj, panelCpf, panelNomeRazao, panelDataNascFunc, panelBanco, panelAgencia, panelConta, panelBtoConsultarCliente" action="#{manterVinculoEmpresaPagBean.limparCamposDoFiltro}" ajaxSingle="true" oncomplete="javascript: focoAgencias(document.forms[1])"  />
			</t:selectOneRadio>
			</a4j:region>
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<t:radio for=":manterVincEmprPagContSalForm:rdoFiltroCliente" index="0" />			
			
			 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
				<br:brPanelGrid columns="2">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="CNPJ: " />
				</br:brPanelGrid>	
				
			   <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0">					
				    <br:brInputText id="txtCnpj" converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{manterVinculoEmpresaPagBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
	  			 	<f:verbatim>&nbsp;</f:verbatim>
				    <br:brInputText id="txtFilial" converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{manterVinculoEmpresaPagBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
		   		 	<f:verbatim>&nbsp;</f:verbatim>
					<br:brInputText id="txtControle" converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{manterVinculoEmpresaPagBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaClientePessoas.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<t:radio for=":manterVincEmprPagContSalForm:rdoFiltroCliente" index="1" />			
			
			 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
				<br:brPanelGrid columns="2">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="CPF: " />
				</br:brPanelGrid>	
				
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
				    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{manterVinculoEmpresaPagBean.itemFiltroSelecionado != '1'}" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" />
	  			 	<f:verbatim>&nbsp;</f:verbatim>
				    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{manterVinculoEmpresaPagBean.itemFiltroSelecionado != '1'}" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
			<t:radio for=":manterVincEmprPagContSalForm:rdoFiltroCliente" index="2" />		
			
			 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
				<br:brPanelGrid columns="2" style="text-align:left;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Nome/Raz�o Social: "/>
				</br:brPanelGrid>	
				
			  	<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{manterVinculoEmpresaPagBean.itemFiltroSelecionado != '2'}" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
			</a4j:outputPanel>
	
		</br:brPanelGrid>
	    
	    <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0">
			<t:radio for="rdoFiltroCliente" index="3" />			
			
			<a4j:outputPanel id="panelBanco" ajaxRendered="true">
				<br:brPanelGrid columns="2">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Banco: " />
				</br:brPanelGrid>	
				
				<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterVinculoEmpresaPagBean.itemFiltroSelecionado != '3'}" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" />
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
				<br:brPanelGrid columns="2">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Ag�ncia: " />
				</br:brPanelGrid>	
				
				<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterVinculoEmpresaPagBean.itemFiltroSelecionado != '3'}" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" />
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelConta" ajaxRendered="true">
				<br:brPanelGrid columns="2">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Conta: " />
				</br:brPanelGrid>	
				
				<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{manterVinculoEmpresaPagBean.itemFiltroSelecionado != '3'}" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" />
			</a4j:outputPanel>
	    </br:brPanelGrid>
	    
	   	<f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
		   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty manterVinculoEmpresaPagBean.itemFiltroSelecionado}" value="consultar cliente" action="#{manterVinculoEmpresaPagBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCpoConsultaCliente(document.forms[1]);">		
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</a4j:outputPanel>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Cliente:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			    
	   	<br:brPanelGrid columns="2">
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>		
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Identifica��o do Contrato:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="5">
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2"/>
			</br:brPanelGroup>	
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Tipo de Contrato: " style="margin-left: 2"/>
			</br:brPanelGroup>	
					
			<f:verbatim>&nbsp;</f:verbatim>		
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="N�mero do Contrato: " style="margin-left: 2"/>
			</br:brPanelGroup>	
		
			<br:brSelectOneMenu id="selEmpresaGestoraContrada" converter="longConverter" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaContratosPessoas.cdPessoaJuridicaContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
				<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
				<f:selectItems  value="#{manterVinculoEmpresaPagBean.preencherEmpresaGestora}" />
			</br:brSelectOneMenu>
			
			<f:verbatim>&nbsp;</f:verbatim>
		
			<br:brSelectOneMenu id="selTipoContrato" converter="inteiroConverter" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaContratosPessoas.cdTipoContratoNegocio}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
				<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
				<f:selectItems  value="#{manterVinculoEmpresaPagBean.preencherTipoContrato}" />
			</br:brSelectOneMenu>
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" value="#{manterVinculoEmpresaPagBean.entradaConsultarListaContratosPessoas.nrSeqContratoNegocio}" size="15" maxlength="10" id="txtNumeroContrato" />
		</br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<br:brPanelGroup style="width:100%; text-align: right">
		   		<br:brCommandButton id="btoConsultarContrato" styleClass="bto1" value="consultar contrato" action="#{manterVinculoEmpresaPagBean.consultarContratos}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript: desbloquearTela(); return validaCpoContratoConsulta(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}','#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}', '#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}', '#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}');" >		
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Contrato:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="Empresa Gestora do Contrato: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.dsEmpresaGestoraContrato}"  />
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="Tipo de Contrato:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"  />
			</br:brPanelGroup>	
		</br:brPanelGrid>
				
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="N�mero do Contrato:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"  />
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_descricao_contrato}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsContrato}"  />
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_situacao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"  />
			</br:brPanelGroup>	
		</br:brPanelGrid> 
	    
	    <f:verbatim><hr class="lin"></f:verbatim>	 
		 
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.conManterSolicitacaoRastreamentoFavorecidosPesq_conta}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	   
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtBancoP" size="4" maxlength="3" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{manterVinculoEmpresaPagBean.desabilataFiltro}" value="#{manterVinculoEmpresaPagBean.entradaVincContaSalarioEmpresa.cdBancoSalario}" />
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agencia}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
				<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"   id="txtAgenciaP" size="6" maxlength="5" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{manterVinculoEmpresaPagBean.desabilataFiltro}" value="#{manterVinculoEmpresaPagBean.entradaVincContaSalarioEmpresa.cdAgenciaSalario}" />			
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_conta}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>
						<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" id="txtContaP" size="18" maxlength="13" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" disabled="#{manterVinculoEmpresaPagBean.desabilataFiltro}" value="#{manterVinculoEmpresaPagBean.entradaVincContaSalarioEmpresa.cdContaSalario}" />
					</br:brPanelGroup>
					<br:brPanelGroup>
						<br:brInputText  id="txtDigitoP"  size="3" maxlength="2" styleClass="HtmlInputTextBradesco"  disabled="#{manterVinculoEmpresaPagBean.desabilataFiltro}" value="#{manterVinculoEmpresaPagBean.entradaVincContaSalarioEmpresa.cdDigitoContaSalario}" style="margin-left:5px;"/>
					</br:brPanelGroup>
				</br:brPanelGrid>				
			</br:brPanelGroup>
	    </br:brPanelGrid>
			
		<f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" action="#{manterVinculoEmpresaPagBean.limparCampos}" styleClass="bto1" value="Limpar Campos">
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<f:verbatim>&nbsp;</f:verbatim>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" disabled="#{manterVinculoEmpresaPagBean.desabilataFiltro}" action="#{manterVinculoEmpresaPagBean.consultarSolicitacao}" value="Consultar" 
					onclick="javascript:desbloquearTela(); return validaFiltrosPesquisa(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}',
						'#{msgs.label_banco}',
						'#{msgs.label_agencia}',
						'#{msgs.label_conta}',
						'#{msgs.label_digito_conta}',
						'#{msgs.label_deve_ser_diferente_de_zeros}');">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	<f:verbatim><br> </f:verbatim>	
			
		<br:brPanelGrid columns="1" align="left" >
			<br:brPanelGroup style="width: 100%">
				<t:div id="divTable" style="overflow-x:auto; overflow-y:auto; width:770px; height:170; display:block" >
					<app:scrollableDataTable id="tableSolicitacao" value="#{manterVinculoEmpresaPagBean.listaVincContaSalarioEmpresa}" rowIndexVar="parametroKey" var="result" rows="10" width="100%">
						<app:scrollableColumn styleClass="colTabCenter" width="30px" >
							<f:facet name="header">
						    	<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
						    </f:facet>		
							<t:selectOneRadio  id="rdoSolicRastFav" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterVinculoEmpresaPagBean.itemSelecionadoLista}">
								<f:selectItems value="#{manterVinculoEmpresaPagBean.selectItemConSolicRastFav}"/>
								<a4j:support event="onclick" reRender="panelBotoes" />
							</t:selectOneRadio>
					    	<t:radio for="rdoSolicRastFav" index="#{parametroKey}" />
						</app:scrollableColumn>		
			      	  	
					  	<app:scrollableColumn width="200" styleClass="colTabLeft">
							<f:facet name="header">
						  		<br:brOutputText value="Banco" styleClass="tableFontStyle" style="width:200; text-align:center"/>
							</f:facet>
							<br:brOutputText value="#{result.bancoSalario}" />
					  	</app:scrollableColumn>
					  	
					  	<app:scrollableColumn width="200" styleClass="colTabLeft">
					    	<f:facet name="header">
					      		<br:brOutputText value="Ag�ncia" styleClass="tableFontStyle" style="width:200; text-align:center"/>
					    	</f:facet>
					    	<br:brOutputText value="#{result.agenciaSalario}" />
					  	</app:scrollableColumn>
					  	
					 	<app:scrollableColumn width="200" styleClass="colTabRight">
							<f:facet name="header">
						  		<br:brOutputText value="Conta Sal�rio" styleClass="tableFontStyle" style="width:200; text-align:center"/>
							</f:facet>
							<br:brOutputText value="#{result.contaDigito}" />
					  	</app:scrollableColumn>			  
					  	
					    <app:scrollableColumn width="400" styleClass="colTabLeft">
							<f:facet name="header">
						  		<br:brOutputText value="Nome" styleClass="tableFontStyle" style="width:360; text-align:center"/>
							</f:facet>
							<br:brOutputText value="#{result.nmTitular}" />
					  	</app:scrollableColumn>
					</app:scrollableDataTable>
				</t:div>
			</br:brPanelGroup>
			
		 	<br:brPanelGrid columns="1" width="770" style="text-align:center" cellpadding="0" cellspacing="0" >		
				<br:brPanelGroup>
					<brArq:pdcDataScroller id="dtFavorecido" for="tableSolicitacao" rendered="#{manterVinculoEmpresaPagBean.listManterDetVinculoCtaSalarioEmpty == false}" actionListener="#{manterVinculoEmpresaPagBean.listaVincContaSalarioEmpresa}">
						<f:facet name="first">
					    	<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira}"/>
					  	</f:facet>
					  	<f:facet name="fastrewind">
					  		<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}"/>
					  	</f:facet>
					  	<f:facet name="previous">
					    	<brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
					  	</f:facet>
					  	<f:facet name="next">
					    	<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
					  	</f:facet>
					  	<f:facet name="fastforward">
					    	<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco}"/>
					  	</f:facet>
					  	<f:facet name="last">
					    	<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_ultima}" title="#{msgs.label_ultima}"/>
					  	</f:facet>
					</brArq:pdcDataScroller> 
				</br:brPanelGroup>
			</br:brPanelGrid>		  
			<f:verbatim><HR class="lin"></f:verbatim>
			
			<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
				<br:brCommandButton id="btoLimpar" styleClass="bto1" value="Limpar" disabled="#{manterVinculoEmpresaPagBean.habilitaLimpa == false}" action="#{manterVinculoEmpresaPagBean.limparPagina}" style="cursor:hand;" onmouseout="javascript:alteraBotao('normal', this.id);">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				
				<br:brCommandButton id="btoIncluir" styleClass="bto1" value="#{msgs.btn_incluir}" disabled="#{!manterVinculoEmpresaPagBean.contratoSelecionado}" action="#{manterVinculoEmpresaPagBean.incluirVincContaEmpresaSalario}" style="cursor:hand; margin-left: 5" onmouseout="javascript:alteraBotao('normal', this.id);">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				
				<br:brCommandButton id="btoDetalhar" styleClass="bto1" value="Detalhar" disabled="#{empty manterVinculoEmpresaPagBean.itemSelecionadoLista}" action="#{manterVinculoEmpresaPagBean.avancarDetalhar}" style="cursor:hand; margin-left: 5" onmouseout="javascript:alteraBotao('normal', this.id);">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				
				<br:brCommandButton id="btoSubstituir" styleClass="bto1" value="Substituir" disabled="#{empty manterVinculoEmpresaPagBean.itemSelecionadoLista}" action="#{manterVinculoEmpresaPagBean.substituirVincContaEmpresaSalario}" style="cursor:hand; margin-left: 5" onmouseout="javascript:alteraBotao('normal', this.id);">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
							
				<br:brCommandButton id="btoExcluir" styleClass="bto1" value="Excluir" disabled="#{empty manterVinculoEmpresaPagBean.itemSelecionadoLista}" action="#{manterVinculoEmpresaPagBean.excluirVincContaEmpresaSalario}" style="cursor:hand; margin-left: 5" onmouseout="javascript:alteraBotao('normal', this.id);">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>	
								
				<br:brCommandButton id="btoHistorico" styleClass="bto1" value="Hist�rico" disabled="#{!manterVinculoEmpresaPagBean.contratoSelecionado}" action="#{manterVinculoEmpresaPagBean.avancarHistorico}" style="cursor:hand; margin-left: 5" onmouseout="javascript:alteraBotao('normal', this.id);">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			 </a4j:outputPanel>	
		</br:brPanelGrid>				    
	</br:brPanelGrid>
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>

