<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="manterVincEmprPagContSalIncluirForm" name="manterVincEmprPagContSalIncluirForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" style="margin-left:5px; width:770">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid> 
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_nome_razao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
			
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.label_contrato}: "/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_empresa}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.dsEmpresaGestoraContrato}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_tipo}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
			</br:brPanelGroup>	
		
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	    	<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="N�mero do Contrato: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_descricao_contrato}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
			</br:brPanelGroup>		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.substManterVinculoEmpresaPagadoraContaSalario_situacao}: "/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterVinculoEmpresaPagBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<f:verbatim><hr class="lin"> </f:verbatim>
			
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_conta_salario}: "/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>	
				    	<br:brInputText onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterVinculoEmpresaPagBean.entradaIncluirVinculoEmprPagContSal.cdBancoSalario}" size="5" maxlength="3" id="txtBanco" 
				    	onkeyup="proximoCampo(3,'manterVincEmprPagContSalIncluirForm','manterVincEmprPagContSalIncluirForm:txtBanco','manterVincEmprPagContSalIncluirForm:txtAgencia');" style="margin-right:20px">  
							<brArq:commonsValidator type="required" arg="#{msgs.label_banco}" server="false" client="true"/>
						</br:brInputText>	
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>	
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agencia}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				    <br:brPanelGroup>	
				    	<br:brInputText onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterVinculoEmpresaPagBean.entradaIncluirVinculoEmprPagContSal.cdAgenciaSalario}" size="7" maxlength="5" id="txtAgencia" 
				    	onkeyup="proximoCampo(5,'manterVincEmprPagContSalIncluirForm','manterVincEmprPagContSalIncluirForm:txtAgencia','manterVincEmprPagContSalIncluirForm:txtConta');">  
							<brArq:commonsValidator type="required" arg="#{msgs.label_agencia}" server="false" client="true"/>						
						</br:brInputText>	
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGroup>				
			
			<br:brPanelGroup>
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
					<br:brPanelGroup>			
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					</br:brPanelGroup>		
					<br:brPanelGroup>			
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_conta_salario}"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>
				
			    <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >			
				    <br:brPanelGroup>	
				    	<br:brInputText onkeypress="onlyNum();" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterVinculoEmpresaPagBean.entradaIncluirVinculoEmprPagContSal.cdContaSalario}" size="17" maxlength="13" id="txtConta" onkeyup="proximoCampo(13,'manterVincEmprPagContSalIncluirForm','manterVincEmprPagContSalIncluirForm:txtConta','manterVincEmprPagContSalIncluirForm:txtDigito');">  
							<brArq:commonsValidator type="required" arg="#{msgs.label_conta_salario}" server="false" client="true"/>										    	
						</br:brInputText>	
					</br:brPanelGroup>
					<br:brPanelGroup style="width:5px; margin-bottom:5px" >
					</br:brPanelGroup>
					<br:brPanelGroup>	
				    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterVinculoEmpresaPagBean.entradaIncluirVinculoEmprPagContSal.dsDigitoContaSalario}" size="3" maxlength="2" id="txtDigito" >  
	 						<brArq:commonsValidator type="required" arg="#{msgs.label_digito_conta_salario}" server="false" client="true"/>
	 					</br:brInputText>	
					</br:brPanelGroup>
					<br:brPanelGroup style="width:5px; margin-bottom:5px" >
					</br:brPanelGroup>				
					<br:brPanelGroup>
						<br:brMessageColor for="txtConta" id="longmessage" />
					</br:brPanelGroup>					
				</br:brPanelGrid>
			</br:brPanelGroup>				
		</br:brPanelGrid>
			
		<f:verbatim><hr class="lin"> </f:verbatim>		
	
	 	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px" >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" style="align:left" value="#{msgs.btn_voltar}" action="#{manterVinculoEmpresaPagBean.voltarPesquisar}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>				
			</br:brPanelGroup>
			<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
			</br:brPanelGrid>	
				<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnAvancar" onclick="return validateForm(document.forms[1]);" styleClass="bto1" disabled="false"  value="#{msgs.btn_avancar}"  action="#{manterVinculoEmpresaPagBean.avancarIncluir}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>		
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid columns="1" style="margin-top:30px" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>   	
	</br:brPanelGrid>

<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
