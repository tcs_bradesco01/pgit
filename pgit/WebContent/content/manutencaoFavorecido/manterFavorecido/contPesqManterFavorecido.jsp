<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmPesManterFavorecido">

<br:brPanelGrid columns="1" width="770" style="margin-top:9">

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Cliente:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 5">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brOutputText styleClass="HtmlOutputTextBradesco" value="Contrato:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.dsEmpresaGestoraContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Descri��o do Contrato: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	

	<f:verbatim><hr class="lin"></f:verbatim>

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Favorecido:"/>
	
	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.favorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsSituacaoFavorecido}"/>
		</br:brPanelGroup>	
    </br:brPanelGrid>
    
	<br:brPanelGrid columns="1">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>    

	<br:brPanelGrid columns="2">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Inscri��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsInscricaoFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de inscri��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoInscricaoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<a4j:region id="regionFiltroPesquisa">
		<t:selectOneRadio id="rdoConta" value="#{manterFavorecidoBean.itemContaSelecionada}" converter="javax.faces.Integer" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
			<f:selectItems value="#{manterFavorecidoBean.selectItemContasFavorido}"/>
			<a4j:support event="onclick" reRender="panelBotoes" limitToList="true" ajaxSingle="true" />
		</t:selectOneRadio>
	</a4j:region>
					   
	<app:scrollableDataTable id="tableConta" value="#{manterFavorecidoBean.listaConsultarListaContaFavorecido}" width="770" rowIndexVar="parametroKey" var="result" rows="10">
		<app:scrollableColumn styleClass="colTabCenter" width="30px" >
			<f:facet name="header">
				<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			</f:facet>		
			<t:radio for=":frmPesManterFavorecido:rdoConta" index="#{parametroKey}" />
		</app:scrollableColumn>  
			  
		<app:scrollableColumn width="200" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Banco" styleClass="tableFontStyle" style="width:200; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.cdBanco} #{result.dsBanco}"/>
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="200" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Ag�ncia" styleClass="tableFontStyle"  style="width:200; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.cdAgencia}-#{result.cdDigitoAgencia} #{result.nmAgenciaFavorecido}"/>
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="250" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Conta" styleClass="tableFontStyle" style="width:250; text-align:center"  />
			</f:facet>
			<br:brOutputText value="#{result.nrContaFavorecido}-#{result.nrDigitoConta}"/>
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="200" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Situa��o" styleClass="tableFontStyle" style="width:200; text-align:center"  />
			</f:facet>
			<br:brOutputText value="#{result.dsSituacaoConta}"/>
		</app:scrollableColumn>
			  
	</app:scrollableDataTable>

	<br:brPanelGroup style="text-align: center; width: 100%">
		<brArq:pdcDataScroller  id="dataScroller" for="tableConta" actionListener="#{manterFavorecidoBean.submitContaFavorecido}">
			<f:facet name="first">
				 <brArq:pdcCommandButton id="primeira"
				    styleClass="HtmlCommandButtonBradesco"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			</f:facet>
			<f:facet name="fastrewind">
				 <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			</f:facet>
			<f:facet name="previous">
				 <brArq:pdcCommandButton id="anterior"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			</f:facet>
			<f:facet name="next">
				 <brArq:pdcCommandButton id="proxima"
				   	styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			</f:facet>
			<f:facet name="fastforward">
				  <brArq:pdcCommandButton id="avancoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_avanco}" title="#{msgs.label_avanco}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			</f:facet>
			<f:facet name="last">
				 <brArq:pdcCommandButton id="ultima"
				   styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				   value="#{msgs.label_ultima}" title="#{msgs.label_ultima}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
			</f:facet>
		</brArq:pdcDataScroller>
	</br:brPanelGroup>	
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">

	 	<br:brPanelGroup style="width:100%; text-align:left">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="Voltar" action="#{manterFavorecidoBean.voltarConsultaFavorecido}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
		
		<a4j:outputPanel id="panelBotoes" ajaxRendered="true" style="width:100%; text-align:right">
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" value="Detalhar" disabled="#{manterFavorecidoBean.itemContaSelecionada == -1}" action="#{manterFavorecidoBean.detalharContaFavorecido}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brCommandButton id="btnBloqDesbloq" styleClass="bto1" value="Bloquear/Desbloquear" disabled="#{manterFavorecidoBean.itemContaSelecionada == -1}" action="#{manterFavorecidoBean.bloquearDesbloquearContaFavorecido}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brCommandButton id="btnHistorico" styleClass="bto1" value="Hist�rico" action="#{manterFavorecidoBean.abrirConsultarHistoricoDeContasFavorecido}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>


</br:brPanelGrid>

</brArq:form>
	