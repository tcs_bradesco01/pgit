<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmPesqManterFavorecido">
<br:brPanelGrid columns="1" width="770" style="margin-top:9">

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Cliente:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 5">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Contrato:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.dsEmpresaGestoraContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Descri��o do Contrato: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left: 20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<f:verbatim><hr class="lin"></f:verbatim>

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_dados_favorecido}:"/>
	
	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.favorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsSituacaoFavorecido}"/>
		</br:brPanelGroup>	
    </br:brPanelGrid>
    
	<br:brPanelGrid columns="1">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>    

	<br:brPanelGrid columns="2">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Inscri��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsInscricaoFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de inscri��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoInscricaoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>			
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid width="770px" columns="1" cellpadding="0" cellspacing="0" rendered="#{manterFavorecidoBean.cdPesquisaListaConta == 1}" >
		<br:brPanelGroup>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_conta}:"/>
			
			<br:brPanelGrid columns="3" style="margin-top: 6">	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_banco}: " style="margin-left: 2" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.bancoFormatado}"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_agencia}: " style="margin-left: 2" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.agenciaFormatada}"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_conta}: " style="margin-left: 2" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.contaFormatada}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1">	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_tipo}: " style="margin-left: 2" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.tipoContaFormatado}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>		
			
			<br:brPanelGrid columns="1">	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_situacao_cadastro}: " style="margin-left: 2" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.dsSituacaoConta}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>				
		
			<f:verbatim><hr class="lin"></f:verbatim>
		</br:brPanelGroup>
	</br:brPanelGrid>				
	
	<br:brPanelGrid width="770px" columns="1" cellpadding="0" cellspacing="0" rendered="#{manterFavorecidoBean.cdPesquisaListaConta == 2}" >
		<br:brPanelGroup>		
			 <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
				<br:brPanelGroup>
					<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
				
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
		     <br:brPanelGrid columns="4" >
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco}:" />
							</br:brPanelGroup>	
						</br:brPanelGrid>
					
						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
					
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>				
								<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterFavorecidoBean.disableArgumentosConsultaHistoricoConta}" value="#{manterFavorecidoBean.bancoHistoricoConta}" size="4" maxlength="3" id="txtBanco" />
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>     
		
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agencia}:" />
							</br:brPanelGroup>	
						</br:brPanelGrid>
					
						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
					
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>				
								<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterFavorecidoBean.disableArgumentosConsultaHistoricoConta}" value="#{manterFavorecidoBean.agenciaHistoricoConta}" size="6" maxlength="5" id="txtAgencia" />
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>		
					
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_conta}:" />
							</br:brPanelGroup>	
						</br:brPanelGrid>
					
						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
					
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>				
								<br:brInputText style="margin-right: 20px" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{manterFavorecidoBean.disableArgumentosConsultaHistoricoConta}" value="#{manterFavorecidoBean.contaHistoricoConta}" size="17" maxlength="13" id="txtConta" />
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>	
					
					<br:brPanelGroup>
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_tipo_conta}:" />
							</br:brPanelGroup>	
						</br:brPanelGrid>
					
						<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
					
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>				
						    	<br:brSelectOneMenu id="tipoConta" disabled="#{manterFavorecidoBean.disableArgumentosConsultaHistoricoConta}" value="#{manterFavorecidoBean.tipoContaHistoricoConta}" styleClass="HtmlSelectOneMenuBradesco">						
									<f:selectItem itemValue="0" itemLabel="#{msgs.conPagamentosFavorecido_label_tipo_selecione}"/>
									<f:selectItems value="#{manterFavorecidoBean.preencherTipoConta}"/>							
								</br:brSelectOneMenu>						
							</br:brPanelGroup>
						</br:brPanelGrid>
					</br:brPanelGroup>
		    </br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
			</br:brPanelGrid>
			
			<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_periodo_pesquisa_historico}:" />
						</br:brPanelGroup>	
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
								    
					<br:brPanelGroup>
						<br:brPanelGroup rendered="#{!manterFavorecidoBean.disableArgumentosConsultaHistoricoConta}">
							<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'periodoInicialHistoricoConta');" id="periodoInicialHistoricoConta" value="#{manterFavorecidoBean.periodoInicialHistoricoConta}" >
							</app:calendar>
						</br:brPanelGroup>
						<br:brPanelGroup rendered="#{manterFavorecidoBean.disableArgumentosConsultaHistoricoConta}">
							<app:calendar id="periodoInicialHistoricoContaDesc" value="#{manterFavorecidoBean.periodoInicialHistoricoConta}" disabled="true" >
							</app:calendar>
						</br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
						<br:brPanelGroup rendered="#{!manterFavorecidoBean.disableArgumentosConsultaHistoricoConta}">
							<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'periodoFinalHistoricoConta');" id="periodoFinalHistoricoConta" value="#{manterFavorecidoBean.periodoFinalHistoricoConta}" >
			 				</app:calendar>	
						</br:brPanelGroup>	
						<br:brPanelGroup rendered="#{manterFavorecidoBean.disableArgumentosConsultaHistoricoConta}">
							<app:calendar id="periodoFinalHistoricoContaDesc" value="#{manterFavorecidoBean.periodoFinalHistoricoConta}" disabled="true">
			 				</app:calendar>	
						</br:brPanelGroup>	
					</br:brPanelGroup>			    
				</br:brPanelGrid>
			</a4j:outputPanel>
			
			<f:verbatim><hr class="lin"></f:verbatim>

		    <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup>
					<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="limpar campos" action="#{manterFavorecidoBean.limparPaginaHistoricoConta}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					
					<f:verbatim>&nbsp;</f:verbatim>
					
					<br:brCommandButton onclick="javascript:desbloquearTela(); return validarCampoHistoricoConta(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_banco}','#{msgs.label_agencia}','#{msgs.label_conta}','#{msgs.label_tipo_conta}');" id="btnConsultar" styleClass="bto1" value="consultar" disabled="#{manterFavorecidoBean.disableArgumentosConsultaHistoricoConta}" action="#{manterFavorecidoBean.consultarHistoricoDeContasFavorecido}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >		
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<a4j:region id="regionFiltroContaHistorico">
		<t:selectOneRadio id="rdoHistoricoContaFavorecido" value="#{manterFavorecidoBean.itemContaHistorico}" converter="javax.faces.Integer" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
			<f:selectItems value="#{manterFavorecidoBean.selectItemHistoricoCotaFavorecido}"/>
			<a4j:support event="onclick" reRender="panelBotoes" limitToList="true" ajaxSingle="true" />
		</t:selectOneRadio>
	</a4j:region>
				
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:770px; height:170"> 		   
			<app:scrollableDataTable id="tableContaHistConta" value="#{manterFavorecidoBean.listaConsultarListaHistoricoContaFavorecido}" width="770" rowIndexVar="parametroKey" var="result" rows="10">
					
				<app:scrollableColumn styleClass="colTabCenter" width="30px" >
					<f:facet name="header">
						<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
					</f:facet>		
					<t:radio for=":frmPesqManterFavorecido:rdoHistoricoContaFavorecido" index="#{parametroKey}" />
				</app:scrollableColumn>  
				
				<app:scrollableColumn width="250" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_banco}" styleClass="tableFontStyle" style="width:250; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.bancoFormatado}"/>
				</app:scrollableColumn>
					  
				<app:scrollableColumn width="250" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_agencia}" styleClass="tableFontStyle"  style="width:250; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.agenciaFormatada}"/>
				</app:scrollableColumn>
					  
				<app:scrollableColumn width="250" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_conta}" styleClass="tableFontStyle" style="width:250; text-align:center"  />
					</f:facet>
					<br:brOutputText value="#{result.contaFormatada}"/>
				</app:scrollableColumn>
					  
				<app:scrollableColumn width="210" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.grid_data_hora_manutencao}" styleClass="tableFontStyle" style="width:210; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.dtManutencao} #{result.hrManutencao}" style="width:210; text-align:center" />
				</app:scrollableColumn>
					  
				<app:scrollableColumn width="120" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_usuario}" styleClass="tableFontStyle" style="width:120; text-align:center" />
					</f:facet>
					<br:brOutputText value="#{result.cdUsuarioManutencao}" />
				</app:scrollableColumn>
					  
				<app:scrollableColumn width="350" styleClass="colTabLeft">
					<f:facet name="header">
						<br:brOutputText value="#{msgs.label_tipo_manutencao}" styleClass="tableFontStyle" style="width:100%; text-align:center"  />
					</f:facet>
					<br:brOutputText value="#{result.tpManutencao}"/>
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</br:brPanelGroup>	
	</br:brPanelGrid> 	

	<br:brPanelGroup style="text-align: center; width: 100%">
		<brArq:pdcDataScroller  id="dataScroller" for="tableContaHistConta" actionListener="#{manterFavorecidoBean.submitListaHistContaDeFavorecidos}">
				<f:facet name="first">
				  <brArq:pdcCommandButton id="primeira"
				    styleClass="HtmlCommandButtonBradesco"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
		</brArq:pdcDataScroller>
	</br:brPanelGroup>	
    
    <f:verbatim><hr class="lin"></f:verbatim>
 	
 	 <br:brPanelGrid columns="2" width="100%" style="text-align:left" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup style="width:100%; text-align:left">
			<br:brCommandButton id="btnVoltar" value="voltar" action="#{manterFavorecidoBean.consultarContasDeFavorecido}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		
		<a4j:outputPanel id="panelBotoes" style="width:100%; text-align:right" ajaxRendered="true">
			<br:brCommandButton id="btnAvancar" value="detalhar" disabled="#{empty manterFavorecidoBean.itemContaHistorico || manterFavorecidoBean.itemContaHistorico == -1}" action="#{manterFavorecidoBean.detalharHistoricoDeContaFavorecido}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			</a4j:outputPanel>

	</br:brPanelGrid>

</br:brPanelGrid>

</brArq:form>