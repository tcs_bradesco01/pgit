<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmBloqDesManterFavorecido">
<br:brPanelGrid columns="1" width="770" style="margin-top:9">

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Cliente:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 5">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brOutputText styleClass="HtmlOutputTextBradesco" value="Contrato:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.dsEmpresaGestoraContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Descri��o do Contrato: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"></f:verbatim>

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Favorecido:"/>
	
	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.favorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsSituacaoFavorecido}"/>
		</br:brPanelGroup>	
    </br:brPanelGrid>
    
	<br:brPanelGrid columns="1">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>    

	<br:brPanelGrid columns="2">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Inscri��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsInscricaoFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de inscri��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoInscricaoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"></f:verbatim>

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Conta:"/>
	
	<br:brPanelGrid columns="3" style="margin-top: 6">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Banco:" style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.cdBanco} #{manterFavorecidoBean.saidaConsultarListaContaFavorecido.dsBanco}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Ag�ncia:" style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.cdAgencia}-#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.cdDigitoAgencia} #{manterFavorecidoBean.saidaConsultarListaContaFavorecido.nmAgenciaFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Conta:" style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.nrContaFavorecido}-#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.nrDigitoConta}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo:" style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.cdTipoConta} #{manterFavorecidoBean.saidaConsultarListaContaFavorecido.dsCdTipoConta}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>					
	
	<br:brPanelGrid columns="1">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o da Conta: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContaFavorecido.dsSituacaoConta}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<a4j:region id="regionBloqDesbloq">
	<t:selectOneRadio id="rdoDesBloq" style="vertical-align: middle" value="#{manterFavorecidoBean.entradaBloquearContaFavorecidoEntrada.cdSituacaoContrato}" converter="javax.faces.Integer" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
		<f:selectItems value="#{manterFavorecidoBean.preencherBloqueoDesbloqueo}" />
		<a4j:support event="onclick" reRender="selMotivoBloq, btnAvancar" action="#{manterFavorecidoBean.inicializaCamposContas}" ajaxSingle="true" />
	</t:selectOneRadio>
    </a4j:region>
    
	<br:brPanelGroup>
		<t:radio for="rdoDesBloq" index="0" />			
		<f:verbatim>&nbsp;</f:verbatim>
		<t:radio for="rdoDesBloq" index="1" />		
	</br:brPanelGroup>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGroup>			
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
		<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Motivo do Bloqueio:" style="font-weight:bold ;margin-left: 2" />
	</br:brPanelGroup>	
	
	<br:brSelectOneMenu id="selMotivoBloq" disabled="#{empty manterFavorecidoBean.entradaBloquearContaFavorecidoEntrada.cdSituacaoContrato || manterFavorecidoBean.entradaBloquearContaFavorecidoEntrada.cdSituacaoContrato == 1}" value="#{manterFavorecidoBean.entradaBloquearContaFavorecidoEntrada.cdMotivoBloqueioContrato}" converter="inteiroConverter" styleClass="HtmlSelectOneMenuBradesco" style="margin-top: 6;">
		<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
		<f:selectItems value="#{manterFavorecidoBean.preencherMotivoBloqueContaFavorecido}"/> 
	</br:brSelectOneMenu>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	 <br:brPanelGrid columns="2" width="100%" style="text-align:left" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup style="width:100%; text-align:left">
			<br:brCommandButton id="btnVoltar" value="voltar" action="#{manterFavorecidoBean.consultarContasDeFavorecido}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="width:100%; text-align:right">
			<br:brCommandButton id="btnAvancar" styleClass="bto1" value="avan�ar" disabled="#{empty manterFavorecidoBean.entradaBloquearContaFavorecidoEntrada.cdSituacaoContrato}" onclick="javascript: desbloquearTela(); return validarCampoBloqueDesbloqConta()" action="#{manterFavorecidoBean.avancarBloquearDesbloquearContaFavorecido}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>

</br:brPanelGrid>

</brArq:form>
