<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmDetqManterFavorecido">
<br:brPanelGrid columns="1" width="770" style="margin-top:9">

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Cliente:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 5">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brOutputText styleClass="HtmlOutputTextBradesco" value="Contrato:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.dsEmpresaGestoraContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Descri��o do Contrato: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	
	<f:verbatim><hr class="lin"></f:verbatim>

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Favorecido:"/>
	
	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.favorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsSituacaoFavorecido}"/>
		</br:brPanelGroup>	
    </br:brPanelGrid>
    
	<br:brPanelGrid columns="1">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>    

	<br:brPanelGrid columns="2">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Inscri��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsInscricaoFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de inscri��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoInscricaoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"></f:verbatim>

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Conta:"/>
	
	<br:brPanelGrid columns="3">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Banco: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaHistoricoContaFavorecido.bancoFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Ag�ncia: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaHistoricoContaFavorecido.agenciaFormatada}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Conta: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaHistoricoContaFavorecido.contaFormatada}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaHistoricoContaFavorecido.cdTipoConta}"/>
		</br:brPanelGroup>
		<br:brPanelGroup style="width: 550">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Observa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.dsObservacaoContaFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid columns="2">	
		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o do Cadastro: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.cdSituacaoContaFavorecido} #{manterFavorecidoBean.dsSituacao}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Motivo do Bloqueio: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.dsBloqueioConta}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>				
	
	<br:brPanelGrid columns="2">	
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Hora de Bloqueio da Conta: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.hrBloqueioContaFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="text-align:left;" >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_tipo_manutencao}: "  style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaHistoricoContaFavorecido.tpManutencao} #{manterFavorecidoBean.saidaDetalharHistoricoFavorecido.hrInclusao}"/>"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		

    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="2" style="margin-top: 6">
		<br:brPanelGroup style="text-align:left;" >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detManterHistoricoComplementar_label_datahorainclusao}: "  style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.dtInclusao} #{manterFavorecidoBean.saidaDetalharHistoricoFavorecido.hrInclusao}"/>"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detSolicitacaoRelatoriosFavorecidos_usuario}: "  style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.cdUsuarioInclusao}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo Canal: " style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.dsCanalInclusao}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Complemento: " style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.cdOperacaoCanalInclusao}"/>
		</br:brPanelGroup>	
			
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="2" style="margin-top: 6">
			
		<br:brPanelGroup style="text-align:left;" >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detManterHistoricoComplementar_label_datahoramanutencao}: "  style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.dtManutencao} #{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.hrManutencao}"/>
		</br:brPanelGroup>	
			
		<br:brPanelGroup style="text-align:left;" style="margin-right: 5; margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detSolicitacaoRelatoriosFavorecidos_usuario}: "  style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.cdUsuarioManutencao}"/>
		</br:brPanelGroup>	
			
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo Canal: " style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.dsCanalManutencao}"/>
		</br:brPanelGroup>	
			
		<br:brPanelGroup style="text-align:left;" style="margin-right: 5; margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Complemento: " style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheHistoricoContaFavorecido.cdOperacaoCanalManutencao}"/>
		</br:brPanelGroup>	
	</br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
 	<br:brPanelGrid columns="2" width="100%" style="text-align:left" cellpadding="0" cellspacing="0">	
		<br:brCommandButton id="btnVoltar" styleClass="bto1" value="voltar" action="#{manterFavorecidoBean.voltarContaHistorico}">
			<brArq:submitCheckClient/>
		</br:brCommandButton>
	</br:brPanelGrid>
	
</br:brPanelGrid>

</brArq:form>