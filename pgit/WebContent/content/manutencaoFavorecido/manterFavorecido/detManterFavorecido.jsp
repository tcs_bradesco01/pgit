<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmDetManterFavorecido">
<br:brPanelGrid columns="1" width="770" style="margin-top:9">

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Cliente:"/>
    
   	<br:brPanelGrid columns="2" width="750" style="margin-top: 5">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brOutputText styleClass="HtmlOutputTextBradesco" value="Contrato:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.dsEmpresaGestoraContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Descri��o do Contrato: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"   />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"></f:verbatim>

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Favorecido:"/>
	
	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.favorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsSituacaoFavorecido}"/>
		</br:brPanelGroup>	
    </br:brPanelGrid>
    
	<br:brPanelGrid columns="1">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>    

	<br:brPanelGrid columns="2">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Inscri��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsInscricaoFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de inscri��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoInscricaoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"></f:verbatim>

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Dados Complementares:"/>
	
	<br:brPanelGrid columns="2" style="margin-top: 6">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="�rg�o emissor do documento: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsOrganizacaoEmissorDocumento}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Data de Expedi��o do Documento: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dtExpedicaoDocumentoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="4">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>		
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Sexo: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.sexo}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Telefone Residencial: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.telefoneResidencial}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Fax: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.telefoneFax}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Ramal: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdRamalFaxFavorecido}" rendered="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdRamalFaxFavorecido != 0}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Telefone Comercial: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.telefoneComercial}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Ramal: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsRamalComercialFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Telefone Celular: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.telefoneCelular}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>		
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="E-mail particular: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.enEmailFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="E-mail comercial: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.enEmailComercialFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Sms: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdSmsFavorecidoCliente}" rendered="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdSmsFavorecidoCliente != '0000-0000'}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGroup style="margin-top: 5; margin-bottom: 5" >			
		<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Endere�o Residencial: "/>
	</br:brPanelGroup>			
	
	<br:brPanelGrid columns="3">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Data de �ltima atualiza��o do endere�o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dtAtualizarEnderecoResidencial}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Logradouro: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.enLogradouroFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.enNumeroLogradouroFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="5">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Complemento: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.enComplementoLogradouroFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Bairro: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.enBairroFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Estado: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsEstadoFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Sigla: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdSiglaEstadoFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />		
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CEP: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cepFavorecidoCliente}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>					
	
	<br:brPanelGrid columns="1">		
		<br:brPanelGroup>
			<<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Munic�pio: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsMunicipioFavorecidoCliente}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGroup style="margin-top: 5; margin-bottom: 5" >
		<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Endere�o Correspond�ncia:"/>
	</br:brPanelGroup>	
	
	<br:brPanelGrid columns="3">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Data da �ltima atualiza��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dtAtualizarEnderecoCorrespondencia}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Logradouro:" style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.enLogradrouroCorrespondenciaFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.enNumeroLogradouroCorrespondencia}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="5">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Complemento: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.enComplementoLogradouroCorrespondencia}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Bairro: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.enBairroCorrespondenciaFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Estado: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsEstadoCorrespondenciaFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />		
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Sigla: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdSiglaEstadoCorrespondencia}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CEP: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cepCorrespondenciaFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>					
	
	<br:brPanelGrid columns="3">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="C�digo do NIT: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsNitFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Inscri��o Estadual: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsInscricaoEstadoFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Sigla UF: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdSiglaUfInscricaoEstadual}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1">	
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Hora de bloqueio do favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.hrBloqueioFavorecidoCliente}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1">
		<br:brPanelGroup>	
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />			
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Observa��o: " style="margin-left: 2"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsObservacaoControleFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Indicador de retorno: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.indicadorRetorno}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Data da �ltima movimenta��o do favorecido: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dtUltimaMovimentacaoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Indicador de Favorecido Inativo: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.indicadorFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Estado civil: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.estadoCivil}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1">	
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Data de nascimento: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dtNascimentoFavorecidoCliente}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nacionalidade: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsNacionalidadeFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Munic�pio de Nascimento: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsMunicipioNascimentoFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />		
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome do c�njuge: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsConjugeFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome da m�e: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsMaeFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome do pai: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsPaiFavorecidoCliente}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Profiss�o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsProfissaoFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Cargo: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsCargoFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Valor da renda mensal: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.rendaMensal}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGroup style="margin-top: 5; margin-bottom: 5" >
		<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Endere�o da Empresa em que o favorecido atua:"/>
	</br:brPanelGroup>
	
	<br:brPanelGrid columns="4">	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome da empresa: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsEmpresaFavorecidoCliente}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Estado: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsUfEmpreFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Sigla: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdUfEmpresaFavorecido}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>		
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Munic�pio: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsMunicipioEmpresaFavorecido}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="2" style="margin-top: 6">
			
		<br:brPanelGroup style="text-align:left;" >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detManterHistoricoComplementar_label_datahorainclusao}: "  style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dtInclusao} #{manterFavorecidoBean.saidaConsultarDetalheFavorecido.hrInclusao}"/>"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup style="text-align:left;" style="margin-right: 5; margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detSolicitacaoRelatoriosFavorecidos_usuario}: "  style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdUsuarioInclusao}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo Canal: " style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsCanalinclusao}"/>
		</br:brPanelGroup>	
				
		<br:brPanelGroup style="text-align:left;" style="margin-right: 5; margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Complemento: " style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdOperacaoCanalInclusao}"/>
		</br:brPanelGroup>	
			
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="2" style="margin-top: 6">
			
		<br:brPanelGroup style="text-align:left;" >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detManterHistoricoComplementar_label_datahoramanutencao}: "  style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dtManutencao} #{manterFavorecidoBean.saidaConsultarDetalheFavorecido.hrManutencao}"/>
		</br:brPanelGroup>	
			
		<br:brPanelGroup style="text-align:left;" style="margin-right: 5; margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detSolicitacaoRelatoriosFavorecidos_usuario}: "  style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdUsuarioManutencao}"/>
		</br:brPanelGroup>	
			
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo Canal: " style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.dsCanalManutencao}"/>
		</br:brPanelGroup>	
			
		<br:brPanelGroup style="text-align:left;" style="margin-right: 5; margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Complemento: " style="margin-left: 5"/>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarDetalheFavorecido.cdOperacaoCanalManutencao}"/>
		</br:brPanelGroup>	
			
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"></f:verbatim>
		
	 <br:brPanelGrid columns="1" width="100%" style="text-align:left" cellpadding="0" cellspacing="0">	
		<br:brCommandButton id="btnVoltar" styleClass="bto1" action="#{manterFavorecidoBean.voltarConsultaFavorecido}" value="voltar">
			<brArq:submitCheckClient/>
		</br:brCommandButton>
	</br:brPanelGrid>

</br:brPanelGrid>

</brArq:form>
