<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmHisManterFavorecido">
<br:brPanelGrid columns="1" width="770" style="margin-top:9">

	<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Cliente:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 5">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
   <br:brOutputText styleClass="HtmlOutputTextBradesco" value="Contrato:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.dsEmpresaGestoraContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3">
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Descri��o do Contrato: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		
			<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	

	<f:verbatim><hr class="lin"></f:verbatim>

	<br:brPanelGrid width="770px" columns="1" cellpadding="0" cellspacing="0" rendered="#{manterFavorecidoBean.cdPesquisaLista == 1}" >	
		<br:brPanelGroup>	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Favorecido:"/>
			
			<br:brPanelGrid columns="2" style="margin-top: 6">
		   		<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Favorecido: " style="margin-left: 2" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.favorecido}"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px" />	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsSituacaoFavorecido}"/>
				</br:brPanelGroup>	
		    </br:brPanelGrid>
		    
			<br:brPanelGrid columns="1">	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Favorecido: " style="margin-left: 2" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoFavorecido}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>    
		
			<br:brPanelGrid columns="2">	
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Inscri��o: " style="margin-left: 2" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsInscricaoFavorecidoCliente}"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-right: 5; margin-left:20px"/>	
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de inscri��o: " style="margin-left: 2" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaListarFavorecido.dsTipoInscricaoFavorecido}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
			<f:verbatim><hr class="lin"></f:verbatim>
		</br:brPanelGroup>
    </br:brPanelGrid>			
	
	<br:brPanelGrid width="770px" columns="1" cellpadding="0" cellspacing="0" rendered="#{manterFavorecidoBean.cdPesquisaLista == 2}" >	
		<br:brPanelGroup>
			<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
				<br:brPanelGroup>
					<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
				
			<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>
				</br:brPanelGroup>
			</br:brPanelGrid>	
			
			<a4j:region id="regionFiltro">
				<t:selectOneRadio disabled="#{manterFavorecidoBean.disableArgumentosConsultaHistorico}" id="rdoFiltroHistorico" value="#{manterFavorecidoBean.itemFiltroHistoricoSelecionado}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >  
					<f:selectItem itemValue="0" itemLabel="" />
					<f:selectItem itemValue="1" itemLabel="" />		
					<a4j:support oncomplete="javascript:foco(this);" event="onclick"   reRender="panelCodigoFavorecido,panelInscricaoFavorecido" ajaxSingle="true"  action="#{manterFavorecidoBean.limparCamposPesquisaHistorico}"/>
				</t:selectOneRadio>
			</a4j:region>
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<t:radio for="rdoFiltroHistorico" index="0" />			
					
					 <a4j:outputPanel id="panelCodigoFavorecido" ajaxRendered="true">	
						<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;" >
							<br:brPanelGroup>			
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							</br:brPanelGroup>		
							<br:brPanelGroup>			
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_codigo_favorecido}:"/>
							</br:brPanelGroup>
						</br:brPanelGrid>
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						    <br:brPanelGroup>			
							</br:brPanelGroup>	
						</br:brPanelGrid>
						
					   <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>				
								<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterFavorecidoBean.cdFavorecidoFiltroHistorico}" id="codigoFavorecido" size="20" maxlength="15" onkeypress="onlyNum();" disabled="#{manterFavorecidoBean.itemFiltroHistoricoSelecionado != '0' || manterFavorecidoBean.disableArgumentosConsultaHistorico}"/>							
							</br:brPanelGroup>
						</br:brPanelGrid>	
					</a4j:outputPanel>	
				</br:brPanelGrid>
			
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
		
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>	
						<t:radio for="rdoFiltroHistorico" index="1" />		
					</br:brPanelGroup>
		
				 <a4j:outputPanel id="panelInscricaoFavorecido" ajaxRendered="true">	
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
								<br:brPanelGroup>
									<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_inscricao}:" />
								</br:brPanelGroup>	
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>
								</br:brPanelGroup>
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup style="margin-right:20px">				
									<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterFavorecidoBean.cdInscricaoFavorecidoFiltroHistorico}" id="inscricaoFavorecido" size="20" maxlength="15" onkeypress="onlyNum();" disabled="#{manterFavorecidoBean.itemFiltroHistoricoSelecionado != '1' || manterFavorecidoBean.disableArgumentosConsultaHistorico}"/>							
								</br:brPanelGroup>
							</br:brPanelGrid>
						</br:brPanelGroup>
						
						<br:brPanelGroup>
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
								<br:brPanelGroup>
									<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.conPagamentosFavorecido_label_tipo}:" />
								</br:brPanelGroup>	
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>
								</br:brPanelGroup>
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>				
									<br:brSelectOneMenu id="tipoFavorecido" value="#{manterFavorecidoBean.tipoInscricaoFavorecidoFiltroHistorico}" disabled="#{manterFavorecidoBean.itemFiltroHistoricoSelecionado != '1' || manterFavorecidoBean.disableArgumentosConsultaHistorico}">
										<f:selectItem itemValue="0" itemLabel="#{msgs.conPagamentosFavorecido_label_tipo_selecione}"/>
										<f:selectItems value="#{manterFavorecidoBean.preencherTipoInscricao}"/>
									</br:brSelectOneMenu>	
								</br:brPanelGroup>
							</br:brPanelGrid>
						</br:brPanelGroup>			
					</br:brPanelGrid>
				</br:brPanelGroup>
				</a4j:outputPanel>	
			</br:brPanelGrid>	
			
			<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
			</br:brPanelGrid>
			
			<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
			    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_periodo_pesquisa_historico}:" />
						</br:brPanelGroup>	
					</br:brPanelGrid>
				
					<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
					</br:brPanelGrid>
								    
					<br:brPanelGroup>
						<br:brPanelGroup rendered="#{!manterFavorecidoBean.disableArgumentosConsultaHistorico}">
							<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'periodoInicialHistorico');" id="periodoInicialHistorico" value="#{manterFavorecidoBean.periodoInicialHistorico}" >
							</app:calendar>
						</br:brPanelGroup>
						<br:brPanelGroup rendered="#{manterFavorecidoBean.disableArgumentosConsultaHistorico}">
							<app:calendar id="periodoInicialHistoricoDesc" value="#{manterFavorecidoBean.periodoInicialHistorico}" disabled="true" >
							</app:calendar>
						</br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
						<br:brPanelGroup rendered="#{!manterFavorecidoBean.disableArgumentosConsultaHistorico}">
							<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'periodoFinalHistorico');" id="periodoFinalHistorico" value="#{manterFavorecidoBean.periodoFinalHistorico}" >
			 				</app:calendar>	
						</br:brPanelGroup>	
						<br:brPanelGroup rendered="#{manterFavorecidoBean.disableArgumentosConsultaHistorico}">
							<app:calendar id="periodoFinalHistoricoDesc" value="#{manterFavorecidoBean.periodoFinalHistorico}" disabled="true">
			 				</app:calendar>	
						</br:brPanelGroup>	
					</br:brPanelGroup>			    
				</br:brPanelGrid>
			</a4j:outputPanel>
			
			<f:verbatim><hr class="lin"></f:verbatim>
				
		    <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup>
					<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="limpar campos" action="#{manterFavorecidoBean.limparPaginaHistorico}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					
					<f:verbatim>&nbsp;</f:verbatim>
					
					<br:brCommandButton onclick="javascript:desbloquearTela(); return validarCampoHistorico(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_codigo_favorecido}','#{msgs.conPagamentosFavorecido_label_inscricao}','#{msgs.conPagamentosFavorecido_label_tipo}');" id="btnConsultar" styleClass="bto1" value="consultar" disabled="#{manterFavorecidoBean.disableArgumentosConsultaHistorico}" action="#{manterFavorecidoBean.consultarHistoricoDeFavorecido}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >		
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>
    </br:brPanelGrid>		
	
	<a4j:region id="regionHistoricoPesquisa">
		<t:selectOneRadio id="rdoHistFavorecidos" value="#{manterFavorecidoBean.itemSelecionadoListaHistorico}" converter="javax.faces.Integer" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
			<f:selectItems value="#{manterFavorecidoBean.selectItemHistoricoFavorido}"/>
			<a4j:support event="onclick" reRender="panelBtoDetalhar" limitToList="true" ajaxSingle="true" />
		</t:selectOneRadio>
	</a4j:region>
					   
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:770px; height:170"> 					   
	<app:scrollableDataTable id="rdoFavorecidos" value="#{manterFavorecidoBean.listaConsultarHistoricoFavorecido}" width="770" rowIndexVar="parametroKey" var="result" rows="10">
			
		<app:scrollableColumn width="30px" styleClass="colTabCenter">
			<f:facet name="header">
				<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			</f:facet>
     	  	<t:radio for=":frmHisManterFavorecido:rdoHistFavorecidos" index="#{parametroKey}" />
		</app:scrollableColumn>
			
		<app:scrollableColumn width="250" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_favorecido}" styleClass="tableFontStyle" style="width:250; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.favorecidoFormatado}" style="width:250;" />
		</app:scrollableColumn>
			
		<app:scrollableColumn width="175" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_cpf_cnpj_favorecido}" styleClass="tableFontStyle" style="width:175; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.cpfCnpjFavorecidoFormatado}" style="width:175;" />
		</app:scrollableColumn>
		
		<app:scrollableColumn width="150" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_tipo_favorecido}" styleClass="tableFontStyle" style="width:150; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsTipoFavorecido}" style="width:150;" />
		</app:scrollableColumn>
		
		<app:scrollableColumn width="200" styleClass="colTabCenter">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.grid_data_hora_manutencao}" styleClass="tableFontStyle" style="width:200; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dtSistema} #{result.cdHora}" style="width:200;" />
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="200" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_usuario_manutencao}" styleClass="tableFontStyle" style="width:200; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.cdUsuario}"/>
		</app:scrollableColumn>
		
		<app:scrollableColumn width="350" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.label_tipo_manutencao}" styleClass="tableFontStyle" style="width:100%; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.tpManutencao}" />
		</app:scrollableColumn>
			  
	</app:scrollableDataTable>
	
		</br:brPanelGroup>	
	</br:brPanelGrid> 		

	<br:brPanelGroup style="text-align: center; width: 100%">
		<brArq:pdcDataScroller id="dtHistorico" for="rdoFavorecidos" actionListener="#{manterFavorecidoBean.submitListaHistoricos}">
			<f:facet name="first">
				  <brArq:pdcCommandButton id="primeira"
				    styleClass="HtmlCommandButtonBradesco"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
		</brArq:pdcDataScroller>
    </br:brPanelGroup>		

	<f:verbatim><hr class="lin"></f:verbatim>
	
	 <br:brPanelGrid columns="2" width="100%" style="text-align:left" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup style="width:100%; text-align:left">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" value="voltar" action="#{manterFavorecidoBean.voltarConsultaFavorecido}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		
		<a4j:outputPanel id="panelBtoDetalhar" ajaxRendered="true" style="width:100%; text-align:right">
			<br:brCommandButton id="btnDetalhar" styleClass="bto1" value="detalhar" disabled="#{empty manterFavorecidoBean.itemSelecionadoListaHistorico}" action="#{manterFavorecidoBean.detalharHistoricoFavorecido}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>

</br:brPanelGrid>

</brArq:form>
