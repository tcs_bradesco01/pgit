<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmConManterFavorecido" name="frmConManterFavorecido">

<br:brPanelGrid columns="1" width="770" style="margin-top:9">
    
 	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Identifica��o do Cliente:"/>
 	
 	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{manterFavorecidoBean.itemFiltroSelecionado}" disabled="#{manterFavorecidoBean.bloqueaRadio}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
	</t:selectOneRadio>
	</a4j:region>
	
    <br:brPanelGrid columns="2">
		<t:radio for=":frmConManterFavorecido:rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="CNPJ:" />
			</br:brPanelGrid>	
			
		   <br:brPanelGrid columns="5">					
			    <br:brInputText id="txtCnpj" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{manterFavorecidoBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterFavorecidoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"/>
  			 	<f:verbatim>&nbsp;</f:verbatim>
			    <br:brInputText id="txtFilial" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{manterFavorecidoBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterFavorecidoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" />
	   		 	<f:verbatim>&nbsp;</f:verbatim>
				<br:brInputText id="txtControle" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{manterFavorecidoBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterFavorecidoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" />
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" >
		<t:radio for=":frmConManterFavorecido:rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="CPF:" />
			</br:brPanelGrid>	
			
		   <br:brPanelGrid columns="3"  >					
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{manterFavorecidoBean.itemFiltroSelecionado != '1'}" value="#{manterFavorecidoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" />
  			 	<f:verbatim>&nbsp;</f:verbatim>
			    <br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{manterFavorecidoBean.itemFiltroSelecionado != '1'}" value="#{manterFavorecidoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" >
		<t:radio for=":frmConManterFavorecido:rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Nome/Raz�o Social:"/>
			</br:brPanelGrid>	
			
		  	<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{manterFavorecidoBean.itemFiltroSelecionado != '2'}" value="#{manterFavorecidoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
	</br:brPanelGrid>
    
     <br:brPanelGrid columns="4" >
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Banco:" />
			</br:brPanelGrid>	
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterFavorecidoBean.itemFiltroSelecionado != '3'}" value="#{manterFavorecidoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" />
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Ag�ncia: " />
			</br:brPanelGrid>	
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterFavorecidoBean.itemFiltroSelecionado != '3'}" value="#{manterFavorecidoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" />
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Conta: " />
			</br:brPanelGrid>	
			
			<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{manterFavorecidoBean.itemFiltroSelecionado != '3'}" value="#{manterFavorecidoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" />
		</a4j:outputPanel>
    </br:brPanelGrid>
    
   	<f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty manterFavorecidoBean.itemFiltroSelecionado}" value="consultar cliente" action="#{manterFavorecidoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCpoConsultaCliente(document.forms[1]);">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>
    
   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Cliente:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 5">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Identifica��o do Contrato:"/>
	
	<br:brPanelGrid columns="5" style="margin-top: 5">
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2"/>
		</br:brPanelGroup>	
		
		<f:verbatim>&nbsp;</f:verbatim>
		
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Tipo de Contrato: " style="margin-left: 2"/>
		</br:brPanelGroup>	
				
		<f:verbatim>&nbsp;</f:verbatim>		
		
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="N�mero do Contrato: " style="margin-left: 2"/>
		</br:brPanelGroup>	
				
		<br:brSelectOneMenu id="selEmpresaGestoraContrada" converter="longConverter" value="#{manterFavorecidoBean.entradaConsultarListaContratosPessoas.cdPessoaJuridicaContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
			<f:selectItems  value="#{manterFavorecidoBean.preencherEmpresaGestora}" />
		</br:brSelectOneMenu>
		<f:verbatim>&nbsp;</f:verbatim>
	
		<br:brSelectOneMenu id="selTipoContrato" converter="inteiroConverter" value="#{manterFavorecidoBean.entradaConsultarListaContratosPessoas.cdTipoContratoNegocio}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
			<f:selectItems  value="#{manterFavorecidoBean.preencherTipoContrato}" />
		</br:brSelectOneMenu>
		<f:verbatim>&nbsp;</f:verbatim>
		
		<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" value="#{manterFavorecidoBean.entradaConsultarListaContratosPessoas.nrSeqContratoNegocio}" size="12" maxlength="10" id="txtNumeroContrato" />
	</br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<br:brPanelGroup style="width:100%; text-align: right">
	   		<br:brCommandButton id="btoConsultarContrato" styleClass="bto1" value="consultar contrato" action="#{manterFavorecidoBean.consultarContratos}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript: desbloquearTela(); return validaCpoContratoConsulta(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}','#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}', '#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}', '#{manterFavorecidoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}');" >
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
      
    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Contrato:"/>
    
   	<br:brPanelGrid columns="3" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.dsEmpresaGestoraContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
		
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="3" style="margin-top: 6">
	
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
		</br:brPanelGroup>
		
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Descri��o do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterFavorecidoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
				
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
   
    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Argumentos de Pesquisa:"/>
    
    <a4j:region id="regionArgumentoPesquisa">
    <t:selectOneRadio id="rdoArgumentoPesquisa" disabled="#{manterFavorecidoBean.desabilataFiltro}" value="#{manterFavorecidoBean.itemArgumentoPesquisa}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">
		<f:selectItem itemValue="0" itemLabel=""/>  
        <f:selectItem itemValue="1" itemLabel=""/> 
        <a4j:support event="onclick" reRender=" panelcdFavorecido, panelInscricao, panelTipoInsc" action="#{manterFavorecidoBean.limparCamposFiltroFavorecidos}" ajaxSingle="true" />
    </t:selectOneRadio>
	</a4j:region>
	
	<%--<br:brPanelGrid columns="2">
		<t:radio for=":frmConManterFavorecido:rdoArgumentoPesquisa" index="0" />			
		
		<a4j:outputPanel id="panelFavorecido" ajaxRendered="true">
			<br:brPanelGrid columns="2" style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Nome do Favorecido: " style="margin-left: 2"/>
			</br:brPanelGrid>	
			
		  	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterFavorecidoBean.entradaListarFavorecido.nmFavorecido}" disabled="#{manterFavorecidoBean.itemArgumentoPesquisa != 0}" id="txtNomeFavorecido" size="50" />
		</a4j:outputPanel>
	</br:brPanelGrid>--%>
	
	<br:brPanelGrid columns="2">
		<t:radio for=":frmConManterFavorecido:rdoArgumentoPesquisa" index="0" />			
		
		<a4j:outputPanel id="panelcdFavorecido" ajaxRendered="true">
			<br:brPanelGrid columns="2" style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="C�digo do Favorecido: " style="margin-left: 2"/>
			</br:brPanelGrid>	
			
		  	<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{manterFavorecidoBean.desabilataFiltro  || manterFavorecidoBean.itemArgumentoPesquisa != 0}" converter="javax.faces.Long" value="#{manterFavorecidoBean.entradaListarFavorecido.cdFavorecidoClientePagador}" onchange="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum()" id="txtFavorecido" size="20" maxlength="15"/>
		</a4j:outputPanel>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="4">
		<t:radio for=":frmConManterFavorecido:rdoArgumentoPesquisa" index="1" />		
		
		<a4j:outputPanel id="panelTipoInsc" ajaxRendered="true"> 
			<br:brPanelGrid columns="2" style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Tipo de Inscri��o: " style="margin-left: 2"/>
			</br:brPanelGrid>	
			
		  	<br:brSelectOneMenu id="selTipoInscricao" converter="inteiroConverter" disabled="#{manterFavorecidoBean.itemArgumentoPesquisa != 1 || manterFavorecidoBean.desabilataFiltro}" value="#{manterFavorecidoBean.entradaListarFavorecido.cdTipoInscricaoFavorecido}" styleClass="HtmlSelectOneMenuBradesco" style="width: 200">
				<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
				<f:selectItems value="#{manterFavorecidoBean.preencherTipoInscricao}" />
				<a4j:support status="statusAguarde" event="onchange"  action="#{manterFavorecidoBean.limparInscricao}" reRender="panelInscricao"/>
			</br:brSelectOneMenu>
		</a4j:outputPanel>
		
		<f:verbatim>&nbsp;</f:verbatim>	

		<a4j:outputPanel id="panelInscricao" ajaxRendered="true">
			<br:brPanelGrid columns="2" style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Inscri��o: " style="margin-left: 2"/>
			</br:brPanelGrid>			  	
			
		   <br:brInputText styleClass="HtmlInputTextBradesco" size="22" value="#{manterFavorecidoBean.entradaListarFavorecido.cdInscricaoFavorecidoFormatado}" onblur="validaCPF(this,'#{msgs.valida_cpf}');" maxlength="14" onkeypress="formataCPF(this,event);"  onfocus="validaCampoNumericoColado(this);" id="txtInscricaoCpf"
		   disabled="#{manterFavorecidoBean.entradaListarFavorecido.cdTipoInscricaoFavorecido != 1 && manterFavorecidoBean.itemArgumentoPesquisa != 1 || manterFavorecidoBean.desabilataFiltro}"  rendered="#{manterFavorecidoBean.entradaListarFavorecido.cdTipoInscricaoFavorecido == 1}"/>	
		   
		   <br:brInputText styleClass="HtmlInputTextBradesco" size="22" value="#{manterFavorecidoBean.entradaListarFavorecido.cdInscricaoFavorecidoFormatado}" onkeypress="formataCNPJ(this,event);" onblur="validaCNPJ(this,'#{msgs.valida_cnpj}');"  onfocus="validaCampoNumericoColado(this);" maxlength="18" id="txtInscricaoCnpj"
		   disabled="#{manterFavorecidoBean.entradaListarFavorecido.cdTipoInscricaoFavorecido != 2 && manterFavorecidoBean.itemArgumentoPesquisa != 1 || manterFavorecidoBean.desabilataFiltro}"  rendered="#{manterFavorecidoBean.entradaListarFavorecido.cdTipoInscricaoFavorecido == 2}"/>
		   
		   <br:brInputText styleClass="HtmlInputTextBradesco" size="22" value="#{manterFavorecidoBean.entradaListarFavorecido.cdInscricaoFavorecidoFormatado}" maxlength="14" onkeypress="formataPIS(this,event);" onblur="validaPIS(this,'#{msgs.valida_pis_pasep}');"  onfocus="validaCampoNumericoColado(this);" id="txtInscricaoPis"
		   disabled="#{manterFavorecidoBean.entradaListarFavorecido.cdTipoInscricaoFavorecido != 3 && manterFavorecidoBean.itemArgumentoPesquisa != 1 || manterFavorecidoBean.desabilataFiltro}"  rendered="#{manterFavorecidoBean.entradaListarFavorecido.cdTipoInscricaoFavorecido == 3}"/>	
		   
		   <br:brInputText styleClass="HtmlInputTextBradesco" size="22" value="#{manterFavorecidoBean.entradaListarFavorecido.cdInscricaoFavorecidoFormatado}" onfocus="validaCampoNumericoColado(this);" onkeypress="onlyNum();" maxlength="15" id="txtInscricaoOutros"
		   disabled="#{manterFavorecidoBean.entradaListarFavorecido.cdTipoInscricaoFavorecido != 9 && manterFavorecidoBean.itemArgumentoPesquisa != 1 || manterFavorecidoBean.desabilataFiltro}"  rendered="#{manterFavorecidoBean.entradaListarFavorecido.cdTipoInscricaoFavorecido == 9 || manterFavorecidoBean.entradaListarFavorecido.cdTipoInscricaoFavorecido == null || manterFavorecidoBean.entradaListarFavorecido.cdTipoInscricaoFavorecido == 0}"/>

		</a4j:outputPanel>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="3">
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Tipo de Favorecido: " style="margin-left: 2"/>
		</br:brPanelGroup>	
		
		<f:verbatim>&nbsp;</f:verbatim>
		
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Situa��o do Favorecido: " style="margin-left: 2"/>
		</br:brPanelGroup>	
				
		<br:brSelectOneMenu id="selTipoFavorecido" converter="inteiroConverter" disabled="#{manterFavorecidoBean.desabilataFiltro}" value="#{manterFavorecidoBean.entradaListarFavorecido.cdTipoFavorecido}" styleClass="HtmlSelectOneMenuBradesco" style="width: 200">
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
			<f:selectItems value="#{manterFavorecidoBean.preencherTipoFavorecido}" />
		</br:brSelectOneMenu>
		
		<f:verbatim>&nbsp;</f:verbatim>
	
		<br:brSelectOneMenu id="selSituacaoFavorecido" disabled="#{manterFavorecidoBean.desabilataFiltro}" value="#{manterFavorecidoBean.entradaListarFavorecido.cdSituacaoFavorecidoCliente}" converter="inteiroConverter" styleClass="HtmlSelectOneMenuBradesco" style="width: 200">
			<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
			<f:selectItem itemValue="1" itemLabel="ATIVO"/>
			<f:selectItem itemValue="2" itemLabel="BLOQUEADO"/>
			<f:selectItem itemValue="3" itemLabel="INATIVO"/>
			<f:selectItem itemValue="4" itemLabel="EXCLUIDO"/>
		</br:brSelectOneMenu>
	</br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="limpar campos" action="#{manterFavorecidoBean.limparPagina}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brCommandButton id="btnConsultarFavorecidos" styleClass="bto1" value="consultar" disabled="#{manterFavorecidoBean.desabilataFiltro}" action="#{manterFavorecidoBean.consultarFavorecidos}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCpoConsultaArgumentos(document.forms[1]);">		
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<a4j:region id="regionFiltroPesquisa">
		<t:selectOneRadio id="rdoFavorecidos" value="#{manterFavorecidoBean.itemSelecionadoListaFavorecidos}" converter="javax.faces.Integer" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
			<f:selectItems value="#{manterFavorecidoBean.selectItemFavorecido}"/>
			<a4j:support event="onclick" reRender="panelBotoes" limitToList="true" ajaxSingle="true" />
		</t:selectOneRadio>
	</a4j:region>
		
	<app:scrollableDataTable id="tableFavorecido" value="#{manterFavorecidoBean.listaFavorecido}" width="100%" height="180" rowIndexVar="parametroKey" var="result" rows="10" rowClasses="tabela_celula_normal, tabela_celula_destaque">
		<app:scrollableColumn styleClass="colTabCenter" width="30px" >
			<f:facet name="header">
				<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			</f:facet>		
			<t:radio for=":frmConManterFavorecido:rdoFavorecidos" index="#{parametroKey}" />
		</app:scrollableColumn>  
							
		<app:scrollableColumn width="250" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="C�digo/Nome Favorecido" styleClass="tableFontStyle" style="width:250; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.favorecido}" />
		</app:scrollableColumn>
	
					  
		<app:scrollableColumn width="120" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Tipo de Favorecido" styleClass="tableFontStyle"  style="width:120; text-align:center"  />
			</f:facet>
			<br:brOutputText value="#{result.dsTipoFavorecido}" />
		</app:scrollableColumn>
					  
		<app:scrollableColumn width="200" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Inscri��o" styleClass="tableFontStyle"  style="width:200; text-align:center"  />
			</f:facet>
			<br:brOutputText value="#{result.dsInscricaoFavorecidoCliente}" />
		</app:scrollableColumn>
					  
		<app:scrollableColumn width="120" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Tipo de Inscri��o" styleClass="tableFontStyle"  style="width:120; text-align:center" />
			</f:facet>
			<br:brOutputText value="#{result.dsTipoInscricaoFavorecido}" />
		</app:scrollableColumn>
					  
		<app:scrollableColumn width="100" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Situa��o" styleClass="tableFontStyle"  style="width:100; text-align:center"  />
			</f:facet>
			<br:brOutputText value="#{result.dsSituacaoFavorecido}" />
		</app:scrollableColumn>
	</app:scrollableDataTable>
		
	<br:brPanelGroup style="text-align: center; width: 100%">
		<brArq:pdcDataScroller  id="dataScroller" for="tableFavorecido" actionListener="#{manterFavorecidoBean.submitListaDeFavorecidos}">
				<f:facet name="first">
				  <brArq:pdcCommandButton id="primeira"
				    styleClass="HtmlCommandButtonBradesco"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
		</brArq:pdcDataScroller>
	</br:brPanelGroup>	
	
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">		
		<a4j:outputPanel id="panelBotoes" ajaxRendered="true">
			<br:brCommandButton id="btnLimpar" styleClass="bto1" value="limpar" disabled="#{manterFavorecidoBean.desabilatalimpar}" action="#{manterFavorecidoBean.limparCampos}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brCommandButton id="btnDetalhar" disabled="#{empty manterFavorecidoBean.itemSelecionadoListaFavorecidos}" action="#{manterFavorecidoBean.detalharFavorecido}" value="detalhar" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brCommandButton id="btnBloqDesbloq" disabled="#{empty manterFavorecidoBean.itemSelecionadoListaFavorecidos}" value="Bloquear/Desbloquear" action="#{manterFavorecidoBean.bloquearDesbloquearFavorecido}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brCommandButton id="btnHistorico" value="Hist�rico" disabled="#{!manterFavorecidoBean.contratoSelecionado}" action="#{manterFavorecidoBean.abrirConsultarHistoricoDeFavorecido}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brCommandButton id="btnContas" disabled="#{empty manterFavorecidoBean.itemSelecionadoListaFavorecidos}" value="Contas" action="#{manterFavorecidoBean.consultarContasDeFavorecido}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>
	
	<t:inputHidden value="#{manterFavorecidoBean.bloqueaRadio}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('frmConManterFavorecido:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
</br:brPanelGrid> 
<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>