<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmIniSolRelFavConfirmacao">
	
	<br:brPanelGrid columns="1" width="100%" style="margin-top:9">
		
		<br:brPanelGroup style="width: 400">
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Solicita��o:" style="margin-left: 5" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.dsRelatorio}"/>
		</br:brPanelGroup>
		
	    <br:brPanelGroup style="width: 400;margin-top: 6">
	    	<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="N�vel de Consolida��o:" style="margin-left: 5" />
	    </br:brPanelGroup>
	    
	    
	    <br:brPanelGrid columns="1" width="750">
	   		<br:brPanelGroup style="width: 500">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa do Conglomerado: " style="margin-left: 5" />
				<%--<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.tpEmpresaConglomerada}"/>--%>				
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.dsEmpresaConglomerada}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="width: 400">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Diretoria Regional:" style="margin-left: 5" />
				<%--<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.tpDiretoriaRegional}"/>--%>				
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.dsUnidadeOrganizacionalStr}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="width: 400">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Ger�ncia Regional:" style="margin-left: 5" />
				<%--<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.tpGerenciaRegional}"/>--%>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.dsDependenciaStr}"/>
			</br:brPanelGroup>		
			
			<br:brPanelGroup style="width: 300">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Ag�ncia:" style="margin-left: 5" />
				<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.cdAgenciaOperadoraStr}"/>
				<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value=" - " rendered="#{relatorioFavorecidoBean.entradaIncRelFavorecido.cdAgenciaOperadoraStr != null && relatorioFavorecidoBean.entradaIncRelFavorecido.cdAgenciaOperadoraStr != ''}"/>
				<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.dsAgencia}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	    
	    <br:brPanelGroup style="width: 250;margin-top: 6">
	    	<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Agrupamentos de Clientes:" style="margin-left: 5" />
	    </br:brPanelGroup>
	    
	    <br:brPanelGrid columns="1" width="750">
	   		<br:brPanelGroup style="width: 400">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Segmento: " style="margin-left: 5" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.cdSegmento}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.dsSegmentoStr}"/>
			</br:brPanelGroup>		
			
			<br:brPanelGroup style="width: 600">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Grupo Econ�mico do Participante:" style="margin-left: 5" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaGrupoEconomico.dsGrupoEconomicoFormatado}"/>
			</br:brPanelGroup>		

	   		<br:brPanelGroup style="width: 100%">
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_classe_ramo}:" style="margin-left: 5px;"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.dsClasseRamo}"/>
			</br:brPanelGroup>		
			
			<br:brPanelGroup style="width: 100%">
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_sub_ramo_atividade}:" style="margin-left: 5px;"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.dsSubRamoAtividade}"/> 
			</br:brPanelGroup>	
		</br:brPanelGrid>	
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
			
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup style="width: 100%;">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" action="#{relatorioFavorecidoBean.voltarIncluirSolicRelatorioFavorecido}" value="voltar" style="cursor:hand; margin-left: 5" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="width: 100%;text-align:right">
			<br:brCommandButton id="btnConfirmar" styleClass="bto1" action="#{relatorioFavorecidoBean.confirmarInclusaoSolicRelatorio}" value="confirmar" onclick="javascript: if (!confirm('Confirma Inclus' +  String.fromCharCode(227) + 'o ?')) { desbloquearTela(); return false; }" style="cursor:hand; margin-left: 5" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
		
		
	</br:brPanelGrid>
</brArq:form>