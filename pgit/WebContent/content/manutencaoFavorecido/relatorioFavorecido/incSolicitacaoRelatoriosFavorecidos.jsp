<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmIncluirSolicitacaoRelatorioFavorecido">

<br:brPanelGrid columns="1" width="100%" style="margin-top:9">

 	<br:brPanelGrid columns="2" width="300">
		<br:brPanelGroup style="width:110;">
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Relat�rio:" />
		</br:brPanelGroup>
		
		<br:brPanelGroup id="panelRelatorio" style="width:190;">
			<t:selectOneRadio id="rdoTipoRelatorio" style="vertical-align:middle" converter="inteiroConverter" styleClass="HtmlSelectOneRadioBradesco" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.tpRelatorio}"  >
				<f:selectItem itemValue="1" itemLabel="Anal�tico"/>
				<f:selectItem itemValue="2" itemLabel="Estat�stico"/>
			</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="N�vel de Consolida��o: " style="margin-left: 5"/>
	</br:brPanelGroup>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid  columns="1" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_empresa_gestora_contrato}:"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
			<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
				<br:brPanelGroup>
					<br:brSelectOneMenu id="selOperacao" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.tpEmpresaConglomerada}" styleClass="HtmlSelectOneMenuBradesco" >
						<f:selectItems value="#{relatorioFavorecidoBean.preencherEmpresaGestora}"/>
						<a4j:support event="onchange" reRender="panelDiretoria,panelGerencia,panelAgencia,chkDiretoriaReg,chkAgOperadora" action="#{relatorioFavorecidoBean.preencheComboDiretoria}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid id="panelDiretoria" columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<h:selectBooleanCheckbox id="chkDiretoriaReg" value="#{relatorioFavorecidoBean.itemCheckDirReg}" disabled="#{relatorioFavorecidoBean.entradaIncRelFavorecido.tpEmpresaConglomerada == 0 || relatorioFavorecidoBean.entradaIncRelFavorecido.tpEmpresaConglomerada == null}">
				<a4j:support event="onclick" reRender="selDiretoria,panelGerencia,panelAgencia" action="#{relatorioFavorecidoBean.limparAgencia}" />
			</h:selectBooleanCheckbox>			
		</br:brPanelGroup>

		<br:brPanelGroup>	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:5px;" >	
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Diretoria Regional:"/>
				</br:brPanelGroup>		
			</br:brPanelGrid>					
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px;margin-left:5px;">			
				<br:brPanelGroup >
					<br:brSelectOneMenu id="selDiretoria" 
						 value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.tpDiretoriaRegional}" disabled="#{relatorioFavorecidoBean.itemCheckDirReg == false && relatorioFavorecidoBean.itemCheckGerReg == false && relatorioFavorecidoBean.itemCheckAgeOpe== false }" styleClass="HtmlSelectOneMenuBradesco " >
						<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
						<f:selectItems value="#{relatorioFavorecidoBean.listaDiretoria}"/>
						<a4j:support event="onchange" reRender="panelGerencia,panelAgencia" action="#{relatorioFavorecidoBean.preencheComboGerencia}" />
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid id="panelGerencia" columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<h:selectBooleanCheckbox id="chkGerenciaReg" value="#{relatorioFavorecidoBean.itemCheckGerReg}"  disabled="#{relatorioFavorecidoBean.entradaIncRelFavorecido.tpDiretoriaRegional == 0 || relatorioFavorecidoBean.entradaIncRelFavorecido.tpDiretoriaRegional == null}">
				<a4j:support event="onclick" reRender="panelGerencia,panelDiretoria,panelAgencia" action="#{relatorioFavorecidoBean.limparAgencia}" />
			</h:selectBooleanCheckbox>			
		</br:brPanelGroup>

		<br:brPanelGroup>	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:5px;" >	
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Ger�ncia Regional:"/>
				</br:brPanelGroup>		
			</br:brPanelGrid>					
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px;margin-left:5px;">			
				<br:brPanelGroup >
					<br:brSelectOneMenu id="selGerencia" converter="inteiroConverter"  value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.tpGerenciaRegional}" styleClass="HtmlSelectOneMenuBradesco "
						 disabled="#{relatorioFavorecidoBean.itemCheckGerReg == false && relatorioFavorecidoBean.itemCheckAgeOpe== false }" >
						<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
						<f:selectItems value="#{relatorioFavorecidoBean.listaGerencia}"/>
						<a4j:support event="onchange" reRender="panelAgencia" action="#{relatorioFavorecidoBean.limparAgenciaCombo}"/>
					</br:brSelectOneMenu>
				</br:brPanelGroup>					
			</br:brPanelGrid>		
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid id="panelAgencia" columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<h:selectBooleanCheckbox id="chkAgOperadora" value="#{relatorioFavorecidoBean.itemCheckAgeOpe}" disabled="#{relatorioFavorecidoBean.entradaIncRelFavorecido.tpGerenciaRegional == 0 || relatorioFavorecidoBean.entradaIncRelFavorecido.tpGerenciaRegional == null}">
				<a4j:support event="onclick" reRender="panelAgencia, selGerencia, panelDiretoria, hiddenAgencia" action="#{relatorioFavorecidoBean.limparAgencia}" />
			</h:selectBooleanCheckbox>					
		</br:brPanelGroup>

		<br:brPanelGroup>	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:5px;" >	
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Ag�ncia Operadora:" /> 	 
				</br:brPanelGroup>		
			</br:brPanelGrid>					
			
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:5px;margin-left:5px;">			
				<br:brPanelGroup>						
					<br:brInputText id="txtAgencia" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');desbloquearTela(); return validaAgenciaOperadora();" maxlength="8" size="10" disabled="#{!relatorioFavorecidoBean.itemCheckAgeOpe}" value="#{relatorioFavorecidoBean.agenciaFiltro}" alt="agencia" styleClass="HtmlInputTextBradesco" onkeypress="javascript: onlyNum();">
						<a4j:support event="onblur" reRender="lblAgencia" action="#{relatorioFavorecidoBean.buscaAgOperadora}"/>			
					</br:brInputText>
				</br:brPanelGroup>				
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px;"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_agencia}:"/> 
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="lblAgencia" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.dsAgencia}"/>
					
				</br:brPanelGroup>
			</br:brPanelGrid>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
		
	<br:brPanelGroup>
		<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
		<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Agrupamentos de Clientes: " style="margin-left: 5"/>
	</br:brPanelGroup>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<h:selectBooleanCheckbox id="chkSegmento" value="#{relatorioFavorecidoBean.itemCheckSeg}" >
				<a4j:support event="onclick" reRender="selSegmento" action="#{relatorioFavorecidoBean.limparAgencia}"/>
			</h:selectBooleanCheckbox>				
		</br:brPanelGroup>

		<br:brPanelGroup>	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:5px;" >	
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Segmento:"/>
				</br:brPanelGroup>		
			</br:brPanelGrid>					
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px;margin-left:5px;">			
				<br:brPanelGroup>
					<br:brSelectOneMenu id="selSegmento" disabled="#{relatorioFavorecidoBean.itemCheckSeg == false }"
						 value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.cdSegmento}" styleClass="HtmlSelectOneMenuBradesco " >
						<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
						<f:selectItems value="#{relatorioFavorecidoBean.preencheComboSegmento}"/>
					</br:brSelectOneMenu>												   					
				</br:brPanelGroup>					
			</br:brPanelGrid>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<h:selectBooleanCheckbox id="chkPartGrupoEco" value="#{relatorioFavorecidoBean.itemCheckParGrpEcp}"> 
				<a4j:support event="onclick" reRender="txtPartGrupoEco,btnLupaGrupo" action="#{relatorioFavorecidoBean.limparAgencia}"/>
			</h:selectBooleanCheckbox>				
		</br:brPanelGroup>

		<br:brPanelGroup>	
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-left:5px;" >	
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Grupo Econ�mico do Participante:"/>	
				</br:brPanelGroup>		
			</br:brPanelGrid>					
			
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" style="margin-top:5px; margin-left:5px">			
				<br:brPanelGroup>
					<br:brInputText id="txtPartGrupoEco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}'); desbloquearTela(); return validaGrupoEconomico();" maxlength="9" size="12" disabled="#{relatorioFavorecidoBean.itemCheckParGrpEcp== false}" value="#{relatorioFavorecidoBean.entradaIncRelFavorecido.cdParticipanteGrupoEconomico}" alt="grupo-economico" styleClass="HtmlInputTextBradesco" onkeypress="javascript: onlyNum()" >
						<a4j:support event="onblur" reRender="lblDescGrupoEconomico" action="#{relatorioFavorecidoBean.buscaDescGrupoEconomico}"/>
					</br:brInputText>					
				</br:brPanelGroup>			
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
					<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_descricao_grupo_economico}:"/> 
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" id="lblDescGrupoEconomico" value="#{relatorioFavorecidoBean.saidaGrupoEconomico.dsGrupoEconomico}"/>
				</br:brPanelGroup>		
			</br:brPanelGrid>		
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<h:selectBooleanCheckbox id="chkAtividadeEconomica" value="#{relatorioFavorecidoBean.itemCheckAtiEco}">
				<a4j:support event="onclick" reRender="cboClasseRamo, cboSubRamoAtividade" action="#{relatorioFavorecidoBean.limparAgencia}"/>
			</h:selectBooleanCheckbox>			
		</br:brPanelGroup>

		<br:brPanelGroup>	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:5px;">
				<br:brPanelGroup>
					<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
						<br:brPanelGroup>			
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						</br:brPanelGroup>		
						<br:brPanelGroup>			
							<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_classe_ramo}:"/>		
						</br:brPanelGroup>		
					</br:brPanelGrid>					
					
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px;">			
						<br:brPanelGroup style="margin-right:20px;">								
							<br:brSelectOneMenu id="cboClasseRamo" value="#{relatorioFavorecidoBean.cboClasseRamo}" styleClass="HtmlSelectOneMenuBradesco"
								disabled="#{!relatorioFavorecidoBean.itemCheckAtiEco}">							   
									<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
									<f:selectItems value="#{relatorioFavorecidoBean.listaCboClasseRamo}" />	 
									<a4j:support event="onchange" reRender="cboSubRamoAtividade" action="#{relatorioFavorecidoBean.carregaComboSubRamoAtividade}"/>											
							</br:brSelectOneMenu>						
						</br:brPanelGroup>					
					</br:brPanelGrid>
				</br:brPanelGroup>		
			</br:brPanelGrid>
		</br:brPanelGroup>
	</br:brPanelGrid> 
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-left:25px;">
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_sub_ramo_atividade}:"/>		
				</br:brPanelGroup>		
			</br:brPanelGrid>					
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px;">			
				<br:brPanelGroup>							
					
					<br:brSelectOneMenu id="cboSubRamoAtividade" value="#{relatorioFavorecidoBean.cboSubRamoAtividade}" styleClass="HtmlSelectOneMenuBradesco"
						disabled="#{!relatorioFavorecidoBean.itemCheckAtiEco|| relatorioFavorecidoBean.cboClasseRamo == '0' || relatorioFavorecidoBean.cboClasseRamo == null}">							   
							<f:selectItem itemValue="0" itemLabel="#{msgs.label_combo_selecione}"/>
							<f:selectItems value="#{relatorioFavorecidoBean.listaCboSubRamoAtividade}" />												
					</br:brSelectOneMenu>
					
				</br:brPanelGroup>					
			</br:brPanelGrid>
		</br:brPanelGroup>	
	</br:brPanelGrid>

		
	<f:verbatim><hr class="lin"></f:verbatim>
		
	<br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup style="width: 100%;">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" action="#{relatorioFavorecidoBean.voltarSolicitacaoRelatorioFavorecido}" value="voltar" style="cursor:hand; margin-left: 5" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="width: 100%;text-align:right">
			<br:brCommandButton id="btnAvancar" styleClass="bto1" action="#{relatorioFavorecidoBean.avancarSolicitacaoRelatorio}" value="avan�ar" style="cursor:hand; margin-left: 5" onclick="desbloquearTela();return valida('#{msgs.label_sub_ramo_atividade}', '#{msgs.label_classe_ramo}','#{msgs.label_descricao_agencia}')" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
</br:brPanelGrid>
<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>