<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmSolicitacaoRelatorioFavorecido">
<br:brPanelGrid columns="1" width="100%" style="margin-top:9">
    
    <br:brPanelGrid columns="1">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Tipo de Relat�rio: " style="margin-left: 2"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-top:9">
			<br:brSelectOneMenu id="selTipoFavorecido" value="#{relatorioFavorecidoBean.entradaConRelFavorecido.tpSolicitacao}" converter="inteiroConverter" styleClass="HtmlSelectOneMenuBradesco" style="width: 200">
				<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
				<f:selectItem itemValue="1" itemLabel="ANAL�TICO"/>
				<f:selectItem itemValue="2" itemLabel="ESTAT�STICO"/>
			</br:brSelectOneMenu>	
		</br:brPanelGroup>
	
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-top:15" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Per�odo de Solicita��o: " style="margin-left: 5; margin-top:15"/>
		</br:brPanelGroup>	

	<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >
		<br:brPanelGroup>	
			<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoInicio');" id="txtPeriodoInicio" value="#{relatorioFavorecidoBean.entradaConRelFavorecido.dtInicioSolicitacao}" disabled="false" >
				<f:converter converterId="dateBrazillianConverter"/>	
		 	</app:calendar> 
			<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco" value="�" style="margin-left: 10;margin-right: 10"/>
		   	
		   	<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoFim');" id="txtPeriodoFim" value="#{relatorioFavorecidoBean.entradaConRelFavorecido.dtFimSolicitacao}" disabled="false">
				<f:converter converterId="dateBrazillianConverter"/>
			</app:calendar>
		</br:brPanelGroup>
	</a4j:outputPanel>	
		
		
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"></f:verbatim>
	
	<br:brPanelGrid columns="2" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup>
			<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="Limpar Campos" style="cursor:hand;" action="#{relatorioFavorecidoBean.limparCampos}" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient />
			</br:brCommandButton>
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brCommandButton id="btnConsultar" styleClass="bto1" action="#{relatorioFavorecidoBean.consultarRelatorios}" value="Consultar" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>


	<b:brPanelGrid columns="1" align="left" width="100%">
		<b:brPanelGroup style="width: 100%">
			<a4j:region id="regionRadioSolicitacao">
			<t:selectOneRadio id="rdoSolcRelFavorecido" value="#{relatorioFavorecidoBean.itemSelecionadoListaRelatorio}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
				<f:selectItems value="#{relatorioFavorecidoBean.selectItemConRelFavorecido}"/>
				<a4j:support event="onclick" reRender="panelBotoes" limitToList="true" ajaxSingle="true" />
			</t:selectOneRadio>
			</a4j:region>
	    
			<app:scrollableDataTable id="tableSolicitacao" value="#{relatorioFavorecidoBean.listaConRelFavorecido}" rowIndexVar="solcRelFavorecidoKey" var="conRelFavorecido" rows="10" width="750" height="180">
				<app:scrollableColumn width="20" styleClass="colTabCenter">
					<f:facet name="header">
						<br:brOutputText value=" " styleClass="tableFontStyle" style="width:20; text-align:center"/>
					</f:facet>
					<t:radio for=":frmSolicitacaoRelatorioFavorecido:rdoSolcRelFavorecido" index="#{solcRelFavorecidoKey}" />
				</app:scrollableColumn>
				<app:scrollableColumn width="230" styleClass="colTabCenter">
					<f:facet name="header">
						<br:brOutputText value="Tipo de Relat�rio" styleClass="tableFontStyle" style="width:230; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{conRelFavorecido.dsTipoRelatorio}" style="width:230;" />
				</app:scrollableColumn>
				<app:scrollableColumn width="230" styleClass="colTabCenter">
					<f:facet name="header">
						<br:brOutputText value="Data/Hora Solicita��o" styleClass="tableFontStyle" style="width:230; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{conRelFavorecido.dsDataSolicitacao} - #{conRelFavorecido.dsHoraSolicitacao}" style="width:230" />
				</app:scrollableColumn>
				<app:scrollableColumn width="230" styleClass="colTabCenter">
					<f:facet name="header">
						<br:brOutputText value="Situa��o" styleClass="tableFontStyle" style="width:230; text-align:center"/>
					</f:facet>
					<br:brOutputText value="#{conRelFavorecido.dsSituacao}" style="width:230;" />
				</app:scrollableColumn>
			</app:scrollableDataTable>
		</b:brPanelGroup>
		<br:brPanelGroup style="text-align: center; width: 100%">
			<brArq:pdcDataScroller id="dtSolicitacao" for="tableSolicitacao" actionListener="{relatorioFavorecidoBean.submit}">
				<f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
	   	</br:brPanelGroup>	
	</b:brPanelGrid>	
		
	<f:verbatim><HR class="lin"></f:verbatim>
		
	 <a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	
		<br:brCommandButton id="btoDetalhar" styleClass="bto1" value="Detalhar" disabled="#{empty relatorioFavorecidoBean.itemSelecionadoListaRelatorio}"  action="#{relatorioFavorecidoBean.detalharRelatorio}" style="cursor:hand; margin-left: 5" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
			<brArq:submitCheckClient/>
		</br:brCommandButton>	
				
		<br:brCommandButton id="btoIncluir" styleClass="bto1" value="Incluir" action="#{relatorioFavorecidoBean.incluirSolitacaoRelatorio}" style="cursor:hand; margin-left: 5" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">	
			<brArq:submitCheckClient/>
		</br:brCommandButton>
				
		<br:brCommandButton id="btoExcluir" styleClass="bto1" value="Excluir" disabled="#{empty relatorioFavorecidoBean.itemSelecionadoListaRelatorio}" action="#{relatorioFavorecidoBean.excluirSolicRelatorioFavorecido}" style="cursor:hand; margin-left: 5" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">	
			<brArq:submitCheckClient/>
		</br:brCommandButton>		
	 </a4j:outputPanel>
	
</br:brPanelGrid>	
</brArq:form>