<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmExcSolicitacaoRelatoriosFavorecidos">

	<br:brPanelGrid columns="1" width="750" style="margin-top:9">
	     <br:brPanelGroup style="width: 250">
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Solicita��o:" style="margin-left: 5" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dsTipoRelatorio}"/>
		</br:brPanelGroup>
		
	    <br:brPanelGroup style="width: 250;margin-top: 6">
	    	<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="N�vel de Consolida��o:" style="margin-left: 5" />
	    </br:brPanelGroup>
	    
	    <br:brPanelGrid columns="1" width="750">
	   		<br:brPanelGroup style="width: 700">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa do Conglomerado: " style="margin-left: 5"  />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.cdPessoaJuridica}" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dsEmpresa}" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="width: 400">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Diretoria Regional:" style="margin-left: 5" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.nrSeqUnidadeOrganizacionalDirStr}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dsUnidadeOrganizacionalDir}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="width: 400">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Ger�ncia Regional:" style="margin-left: 5" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.nrUnidadeOrganizacionalGercStr}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dsOrganizacionalGerc}"/>
			</br:brPanelGroup>		
			
			<br:brPanelGroup style="width: 400">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Ag�ncia:" style="margin-left: 5" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.nrUnidadeOrganizacionalStr}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dsOrganizacaoOperacional}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	    
	    <br:brPanelGroup style="width: 400;margin-top: 6">
	    	<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Agrupamentos de Clientes:" style="margin-left: 5" />
	    </br:brPanelGroup>
	    
	    <br:brPanelGrid columns="1" width="750">
	   		<br:brPanelGroup style="width: 180">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Segmento: " style="margin-left: 5" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.cdSegmentoClienteStr}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dsSegmentoCliente}"/>
			</br:brPanelGroup>		
		
			<br:brPanelGroup style="width: 600">
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Grupo Econ�mico do Participante:" style="margin-left: 5" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.cdGrupoEconomicoStr}"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dsGrupoEconomico}"/>
			</br:brPanelGroup>		
			
	   		<br:brPanelGroup style="width: 100%">
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_classe_ramo}:" style="margin-left: 5px;"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dsClasseRamo}"/>
			</br:brPanelGroup>		
			
			<br:brPanelGroup style="width: 100%">
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_sub_ramo_atividade}:" style="margin-left: 5px;"/> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dsSubRamoAtividade}"/> 
			</br:brPanelGroup>	
		</br:brPanelGrid>
			    
	    <f:verbatim><hr class="lin"> </f:verbatim>	
	
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_data_hora_inclusao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dtInclusao}"  />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.hrInclusao}"  />			
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detSolicitacaoRelatoriosFavorecidos_usuario}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.cdUsuarioInclusao}"  />	
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.cdUsuarioInclusaoExter}"  />		
			</br:brPanelGroup>	
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>				
		
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" >		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="Tipo Canal:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.cdTipoCanalInclusao}"  />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dsCanalInclusao}"  />			
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="Complemento:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.cdOperCanalInclusao}"  />			
			</br:brPanelGroup>					
		</br:brPanelGrid>	

		<f:verbatim><hr class="lin"> </f:verbatim>	
	
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_data_hora_manutencao}:"/>
				<br:brOutputText  styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dtManutencao}"  />	
				<br:brOutputText  styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.hrManutencao}"  />		
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detSolicitacaoRelatoriosFavorecidos_usuario}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.cdUsuarioManutencao}"  />			
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
		<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">		
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="Tipo Canal:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.cdTipoCanalManutencaoStr}"  />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.dsCanalManutencao}"  />				
			</br:brPanelGroup>	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="Complemento:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{relatorioFavorecidoBean.saidaDetRelFavorecido.cdOperCanalManutencao}"  />			
			</br:brPanelGroup>					
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
			
	   <br:brPanelGrid columns="2" width="100%" cellpadding="0" cellspacing="0">	
		<br:brPanelGroup style="width: 100%;">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" action="#{relatorioFavorecidoBean.voltarExcluirSolicRelatorioFavorecido}" value="voltar" style="cursor:hand; margin-left: 5" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
			
		<br:brPanelGroup style="width: 100%;text-align:right">
			<br:brCommandButton id="btnConfirmar" styleClass="bto1" action="#{relatorioFavorecidoBean.confirmarExclusaoSolicRelatorio}" value="confirmar" style="cursor:hand; margin-left: 5" onclick="javascript: if (!confirm('Confirma Exclus�o?')) { desbloquearTela(); return false; }" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	</br:brPanelGrid>
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>