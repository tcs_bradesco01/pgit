<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si"%>

<brArq:form id="alterarCtaSalDestForm" name="alterarCtaSalDestForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" width="770" cellpadding="0" cellspacing="0" style="margin-left:5px">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Cliente:"/>
	    
	   	<br:brPanelGrid columns="2" style="margin-top: 6">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}" style="margin-left:5px" />
			</br:brPanelGroup>
			
			<br:brPanelGroup style="margin-left:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"  style="margin-left:5px" />
			</br:brPanelGroup>
	    </br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
	    <br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Contrato:"/>
	    
	   	<br:brPanelGrid columns="2" style="margin-top: 6">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.dsEmpresaGestoraContrato}" style="margin-left:5px" />
			</br:brPanelGroup>
			
			<br:brPanelGroup style="margin-left:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}" style="margin-left:5px" />
			</br:brPanelGroup>
			

		</br:brPanelGrid>
			
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_descricao_contrato}:" /> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsContrato}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"  style="margin-left:5px" />
			</br:brPanelGroup>						
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_salario}: "/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.bancoSalario}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.agenciaSalario}" style="margin-left:5px" /> 
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.contaDigitoSalario}" style="margin-left:5px" />
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_cpf}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cpfParticipante}" style="margin-left:5px" />
			</br:brPanelGroup>	
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_nome}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.nmParticipante}" style="margin-left:5px" />
			</br:brPanelGroup>		
		</br:brPanelGrid>	
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_destino}: "/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.bancoDestino}"style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.agenciaDestino}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.contaDigitoDestino}" style="margin-left:5px" />
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_tipo_conta}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cdTipoContaDestino}" style="margin-left:5px" />
			</br:brPanelGroup>	
				
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterCadastroContaSalarioContaDestino_contingencia_ted}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cdContingenciaTed}" style="margin-left:5px" />
			</br:brPanelGroup>	
		</br:brPanelGrid>		
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.altManterCadastroContaSalarioContaDestino_nova_conta}: "/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="10" cellpadding="0" cellspacing="0"  >
	    	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}: "/>
				</br:brPanelGroup>	
	
				 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtBanco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" size="5" maxlength="3" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.cdBanco}" converter="javax.faces.Integer" onkeypress="onlyNum();" >
						<%--onkeyup="proximoCampo(3, document.forms[1], 'alterarCtaSalDestForm:txtBanco','alterarCtaSalDestForm:txtAgencia');">--%>
							<brArq:commonsValidator type="required" arg="#{msgs.conManterCadastroContaSalarioContaDestino_banco}" server="false" client="true"/>
					    </br:brInputText>
					</br:brPanelGroup>
				</br:brPanelGrid>			
			</br:brPanelGrid>
	
			<br:brPanelGroup style="width:20px" />
	
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup >
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}: "/>
					</br:brPanelGroup>				
				
					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:5px"  >					
						<br:brPanelGroup>
							<br:brInputText id="txtAgencia" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" size="8" maxlength="5" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.cdAgencia}" converter="javax.faces.Integer" onkeypress="onlyNum();" >
							<%--onkeyup="proximoCampo(4,'alterarCtaSalDestForm','alterarCtaSalDestForm:txtAgencia','alterarCtaSalDestForm:txtDigitoAgencia');">--%>
								<brArq:commonsValidator type="required" arg="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}" server="false" client="true"/>
							</br:brInputText>	
						</br:brPanelGroup>			
						
						<br:brPanelGroup style="width:5px" />
		
						<br:brPanelGroup>
							<br:brInputText id="txtDigitoAgencia" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.cdDigitoAgencia}" size="3" maxlength="1" converter="javax.faces.Integer" onkeypress="onlyNum();" >
								<brArq:commonsValidator type="required" arg="D�gito Ag�ncia" server="false" client="true"/>
					    	</br:brInputText>
						</br:brPanelGroup>			
					</br:brPanelGrid>	
				</br:brPanelGrid>	
			</br:brPanelGrid>
	
			<br:brPanelGroup style="width:20px" >
			</br:brPanelGroup>	
	
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Conta: "/>
					</br:brPanelGroup>				
					 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:5px" >					
					    <br:brPanelGroup>
							<br:brInputText id="txtConta" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" size="18" maxlength="13" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.cdConta}" converter="javax.faces.Long" onkeypress="onlyNum();" >
							  <%--onkeyup="proximoCampo(13,'alterarCtaSalDestForm','alterarCtaSalDestForm:txtConta','alterarCtaSalDestForm:txtDigitoConta');" >--%>
								 <brArq:commonsValidator type="required" arg="Conta" server="false" client="true"/>								 
						    </br:brInputText>	
						</br:brPanelGroup>	
						
						<br:brPanelGroup style="width:5px" />
	
						<br:brPanelGroup>
							<br:brInputText id="txtDigitoConta" styleClass="HtmlInputTextBradesco" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.cdDigitoConta}" size="3" maxlength="2" onkeypress="onlyNum();" >
								<brArq:commonsValidator type="required" arg="Dig�to Conta" server="false" client="true"/>
					    	</br:brInputText>
						</br:brPanelGroup>					
					</br:brPanelGrid>			
				</br:brPanelGrid>			
			</br:brPanelGrid>
	
			<br:brPanelGroup style="width:20px" >
			</br:brPanelGroup>
	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_tipo_conta}: "/>
				</br:brPanelGroup>	
				
				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:5px">									
				    <br:brPanelGroup>
						<br:brSelectOneMenu id="tipoConta" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.cdTipoConta}" >
							<f:selectItem itemValue="" itemLabel="#{msgs.detManterCadastroContaSalarioContaDestino_label_combo_selecione}"/>
							<si:selectItems value="#{manterCadContaDestinoBean.listaTipoConta}" var="tipoConta" itemLabel="#{tipoConta.dsTipoConta}" itemValue="#{tipoConta.cdTipoConta}" />
							<brArq:commonsValidator type="required" arg="#{msgs.detManterCadastroContaSalarioContaDestino_tipo_conta}" server="false" client="true"/>								 								
						</br:brSelectOneMenu>						
						<br:brPanelGroup style="width:5px; margin-bottom:5px" />							
					</br:brPanelGroup>								
				</br:brPanelGrid>
			</br:brPanelGrid>
			
			<br:brPanelGroup style="width:20px" />	
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >			
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.incManterCadastroContaSalarioContaDestino_contingencia_ted}:"/>
				</br:brPanelGroup>	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px">		
					<br:brSelectOneMenu id="contigenciaTED" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.cdContingenciaTed}" >
						<f:selectItem itemValue="2" itemLabel="N�O"/>
						<f:selectItem itemValue="1" itemLabel="SIM"/>
					</br:brSelectOneMenu>		
				</br:brPanelGrid>
			</br:brPanelGrid>
			
		</br:brPanelGrid>	
			
		<f:verbatim><hr class="lin"></f:verbatim>
			
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px"  >
				<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.detManterCadastroContaSalarioContaDestino_btn_voltar}" action="#{manterCadContaDestinoBean.voltarPaginaCadContaSalario}">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
			
			<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnAvancar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.incManterCadastroContaSalarioContaDestino2_btn_avancar}" action="#{manterCadContaDestinoBean.avancarAlterar}" >				
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>	 
		
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
