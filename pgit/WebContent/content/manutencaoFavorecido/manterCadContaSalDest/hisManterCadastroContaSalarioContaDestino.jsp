<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="cadCtaSalDestForm" name="historicoCtaSalDestForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" style="margin-left: 5" width="770" cellpadding="0" cellspacing="0">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Cliente:"/>
	    
	    <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_cpf_cnpj}:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}" style="margin-left:5px" />
			</br:brPanelGroup>		
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_nome_razao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.dsNomeRazao}" style="margin-left:5px" />
			</br:brPanelGroup>		
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
	    <br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Contrato:"/>
	    
	   	<br:brPanelGrid columns="2"  cellpadding="0" cellspacing="0" style="margin-top: 6">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.dsEmpresaGestoraContrato}" style="margin-left:5px" />
			</br:brPanelGroup>
			
			<br:brPanelGroup style="margin-left:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}" style="margin-left:5px" />
			</br:brPanelGroup>
			
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3"  cellpadding="0" cellspacing="0">
		
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_descricao_contrato}:" /> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsContrato}"  style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}" style="margin-left:5px" />
			</br:brPanelGroup>						
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid width="750px" columns="1" cellpadding="0" cellspacing="0" rendered="#{manterCadContaDestinoBean.cdPesquisaLista == 1}" >
			<br:brPanelGroup>
				<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_salario}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:"/>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.bancoSalario}" style="margin-left:5px" />
					</br:brPanelGroup>	
					
					<br:brPanelGroup style="margin-left:20px">
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.agenciaSalario}" style="margin-left:5px" /> 
					</br:brPanelGroup>	
					
					<br:brPanelGroup style="margin-left:20px">
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta}:" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.contaDigitoSalario}" style="margin-left:5px" />
					</br:brPanelGroup>
				</br:brPanelGrid>	
			
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">	
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_cpf}:" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cpfParticipante}" style="margin-left:5px" />
					</br:brPanelGroup>	
					<br:brPanelGroup style="margin-left:20px">
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_nome}:" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.nmParticipante}" style="margin-left:5px" />
					</br:brPanelGroup>		
				</br:brPanelGrid>	
				
				<f:verbatim><hr class="lin"> </f:verbatim>
				
				<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_destino}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.bancoDestino}" style="margin-left:5px" />
					</br:brPanelGroup>	
					
					<br:brPanelGroup style="margin-left:20px">
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.agenciaDestino}" style="margin-left:5px" />
					</br:brPanelGroup>	
					
					<br:brPanelGroup style="margin-left:20px">
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta}:" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.contaDigitoDestino}" style="margin-left:5px" />
					</br:brPanelGroup>
				</br:brPanelGrid>		
				
				<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_tipo_conta}:"/>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cdTipoContaDestino}" style="margin-left:5px" /> 
					</br:brPanelGroup>	
					
					<br:brPanelGroup style="margin-left:20px">		
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
						<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterCadastroContaSalarioContaDestino_contingencia_ted}:" />
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cdContingenciaTed}" style="margin-left:5px" />
					</br:brPanelGroup>		
				</br:brPanelGrid>
					
				<f:verbatim><hr class="lin"> </f:verbatim>			
			</br:brPanelGroup>
		</br:brPanelGrid>			
		
		<br:brPanelGrid width="750px" columns="1" cellpadding="0" cellspacing="0" rendered="#{manterCadContaDestinoBean.cdPesquisaLista == 2}" >
			<br:brPanelGroup>
				 <br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
					<br:brPanelGroup>
						<br:brOutputText styleClass ="HtmlOutputTextTitleBradesco" value="#{msgs.label_argumentos_pesquisa}:"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
					
				<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
					</br:brPanelGroup>
				</br:brPanelGrid>	
				
			     <br:brPanelGrid columns="4" >
						<br:brPanelGroup>
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
								<br:brPanelGroup>
									<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco_conta_salario}:" />
								</br:brPanelGroup>	
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>
								</br:brPanelGroup>
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>				
									<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterCadContaDestinoBean.disableArgumentosConsultaHistorico}" value="#{manterCadContaDestinoBean.bancoSalarioHistorico}" size="4" maxlength="3" id="txtBanco" />
								</br:brPanelGroup>
							</br:brPanelGrid>
						</br:brPanelGroup>     
			
						<br:brPanelGroup>
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
								<br:brPanelGroup>
									<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agencia_conta_salario}:" />
								</br:brPanelGroup>	
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>
								</br:brPanelGroup>
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>				
									<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterCadContaDestinoBean.disableArgumentosConsultaHistorico}" value="#{manterCadContaDestinoBean.agenciaSalarioHistorico}" size="6" maxlength="5" id="txtAgencia" />
								</br:brPanelGroup>
							</br:brPanelGrid>
						</br:brPanelGroup>		
						
						<br:brPanelGroup>
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
								<br:brPanelGroup>
									<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
									<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_conta_salario}:" />
								</br:brPanelGroup>	
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>
								</br:brPanelGroup>
							</br:brPanelGrid>
						
							<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
								<br:brPanelGroup>				
									<br:brInputText onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{manterCadContaDestinoBean.disableArgumentosConsultaHistorico}" value="#{manterCadContaDestinoBean.contaSalarioHistorico}" size="17" maxlength="13" id="txtConta" />
									<br:brInputText style="margin-left: 5px" id="txtDigitoConta" size="3" maxlength="2" styleClass="HtmlInputTextBradesco" disabled="#{manterCadContaDestinoBean.disableArgumentosConsultaHistorico}" value="#{manterCadContaDestinoBean.digitoContaSalarioHistorico}" />
								</br:brPanelGroup>					
							</br:brPanelGrid>
						</br:brPanelGroup>	
			    </br:brPanelGrid>	
				
				<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
						<br:brPanelGroup>
						</br:brPanelGroup>
				</br:brPanelGrid>
				
				<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();">					
				    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
						<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >	
							<br:brPanelGroup>
								<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
								<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_periodo_pesquisa_historico}:" />
							</br:brPanelGroup>	
						</br:brPanelGrid>
					
						<br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
							<br:brPanelGroup>
							</br:brPanelGroup>
						</br:brPanelGrid>
									    
						<br:brPanelGroup>
							<br:brPanelGroup rendered="#{!manterCadContaDestinoBean.disableArgumentosConsultaHistorico}">
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'periodoInicialHistorico');" id="periodoInicialHistorico" value="#{manterCadContaDestinoBean.periodoInicialHistorico}" >
								</app:calendar>
							</br:brPanelGroup>
							<br:brPanelGroup rendered="#{manterCadContaDestinoBean.disableArgumentosConsultaHistorico}">
								<app:calendar id="periodoInicialHistoricoDesc" value="#{manterCadContaDestinoBean.periodoInicialHistorico}" disabled="true" >
								</app:calendar>
							</br:brPanelGroup>
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" style="margin-right:5px;margin-left:5px" value="#{msgs.consultarPagamentosIndividual_a}"/>
							<br:brPanelGroup rendered="#{!manterCadContaDestinoBean.disableArgumentosConsultaHistorico}">
								<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'periodoFinalHistorico');" id="periodoFinalHistorico" value="#{manterCadContaDestinoBean.periodoFinalHistorico}" >
				 				</app:calendar>	
							</br:brPanelGroup>	
							<br:brPanelGroup rendered="#{manterCadContaDestinoBean.disableArgumentosConsultaHistorico}">
								<app:calendar id="periodoFinalHistoricoDesc" value="#{manterCadContaDestinoBean.periodoFinalHistorico}" disabled="true">
				 				</app:calendar>	
							</br:brPanelGroup>	
						</br:brPanelGroup>			    
					</br:brPanelGrid>
				</a4j:outputPanel>
				
				<f:verbatim><hr class="lin"></f:verbatim>
				
			    <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
					<br:brPanelGroup>
						<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="limpar campos" action="#{manterCadContaDestinoBean.limparPaginaHistorico}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >
							<brArq:submitCheckClient/>
						</br:brCommandButton>
						
						<f:verbatim>&nbsp;</f:verbatim>
						
						<br:brCommandButton onclick="javascript:desbloquearTela(); return validarCampoHistoricoContaSalarioDestino(document.forms[1],'#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.label_banco}','#{msgs.label_agencia}','#{msgs.label_conta}','#{msgs.label_digito_conta}');" id="btnConsultar" styleClass="bto1" value="consultar" disabled="#{manterCadContaDestinoBean.disableArgumentosConsultaHistorico}" action="#{manterCadContaDestinoBean.consultarHistorico}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" >		
							<brArq:submitCheckClient/>
						</br:brCommandButton>
					</br:brPanelGroup>
				</br:brPanelGrid>	
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><br></f:verbatim> 			
		
		<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="overflow-x:auto; overflow-y:auto; width:750px; height:170"> 		   
				<app:scrollableDataTable id="dataTable" value="#{manterCadContaDestinoBean.listaHistoricoContaSalarioDest}" var="result" rows="10" rowIndexVar="parametroKey" rowClasses="tabela_celula_normal, tabela_celula_destaque" width="100%">
					
					<app:scrollableColumn styleClass="colTabCenter" width="30px" >
						<f:facet name="header">
						  <br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
						</f:facet>		
						<t:selectOneRadio  id="sorLista" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{manterCadContaDestinoBean.itemSelecionadoListaHist}">
							<f:selectItems value="#{manterCadContaDestinoBean.selectItemHistContaSalarioDest}"/>
							<a4j:support event="onclick" reRender="panelBotoes" />
						</t:selectOneRadio>
						<t:radio for="sorLista" index="#{parametroKey}" />
					</app:scrollableColumn>

					<app:scrollableColumn  width="250px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_nome_favorecido}"  style="text-align:center; width:250" />
						</f:facet>
						<br:brOutputText value="#{result.nmParticipante}"/>
					 </app:scrollableColumn>
					 
					<app:scrollableColumn  width="120px"  styleClass="colTabRight">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_cpf_favorecido}"  style="text-align:center; width:120" />
						</f:facet>
						<br:brOutputText value="#{result.cpfParticipante}"/>
					 </app:scrollableColumn>

					<app:scrollableColumn  width="200px"  styleClass="colTabCenter">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.hisManterCadastroContaSalarioContaDestino_grid_data_hora_manutencao}"  style="text-align:center; width:200" />
						</f:facet>
						<br:brOutputText value="#{result.dataHoraManutencao}"/>
					 </app:scrollableColumn>
					 
					<app:scrollableColumn  width="170px" styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.label_usuario_manutencao}"  style="text-align:center; width:170" />
						</f:facet>
						<br:brOutputText value="#{result.dsUsuario}"/>
					 </app:scrollableColumn>
					 
					 <app:scrollableColumn  width="350px"  styleClass="colTabLeft">
						<f:facet name="header">
						  <br:brOutputText value="#{msgs.hisManterCadastroContaSalarioContaDestino2_tipo_manutencao}"  style="text-align:center; width:100%" />
						</f:facet>
						<br:brOutputText value="#{result.tpManutencao}"/>
					 </app:scrollableColumn>
				</app:scrollableDataTable>	
			</br:brPanelGroup>		
		</br:brPanelGrid>
			
		
	 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterCadContaDestinoBean.listaHistoricoContaSalarioDest}" rendered="#{manterCadContaDestinoBean.listaHistContaSalDestinoEmpty == false}">
				 <f:facet name="first">
				    <brArq:pdcCommandButton id="primeira"
				      styleClass="bto1"
				      value="#{msgs.label_primeira}" title="#{msgs.label_primeira}"/>
				  </f:facet>
				  <f:facet name="fastrewind">
				    <brArq:pdcCommandButton id="retrocessoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}"/>
				  </f:facet>
				  <f:facet name="previous">
				    <brArq:pdcCommandButton id="anterior"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
				  </f:facet>
				  <f:facet name="next">
				    <brArq:pdcCommandButton id="proxima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
				  </f:facet>
				  <f:facet name="fastforward">
				    <brArq:pdcCommandButton id="avancoRapido"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_avanco}" title="#{msgs.label_avanco}"/>
				  </f:facet>
				  <f:facet name="last">
				    <brArq:pdcCommandButton id="ultima"
				      styleClass="bto1" style="margin-left: 3px;"
				      value="#{msgs.label_ultima}" title="#{msgs.label_ultima}"/>
				  </f:facet>
				</brArq:pdcDataScroller>	
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	 	
	 	<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px"  >
				<br:brCommandButton id="btnVoltar" styleClass="bto1" disabled="false" value="#{msgs.detManterCadastroContaSalarioContaDestino_btn_voltar}" action="#{manterCadContaDestinoBean.voltarPaginaCadContaSalario}">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>			
			<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnDetalhar" styleClass="bto1" disabled="#{empty manterCadContaDestinoBean.itemSelecionadoListaHist}" value="#{msgs.conManterCadastroContaSalarioContaDestino_btn_detalhar}" action="#{manterCadContaDestinoBean.detalheHistorico}" >				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>			

	</br:brPanelGrid>

</brArq:form>
