<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="historicoCtaSalDestForm" name="historicoCtaSalDestForm" > 

<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" width="770" style="margin-left:5px">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_title_cliente}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>

	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Contrato:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.dsEmpresaGestoraContrato}" style="margin-left:5px" />
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}" style="margin-left:5px" />
		</br:brPanelGroup>
		

	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
	
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_descricao_contrato}:" /> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsContrato}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}" style="margin-left:5px" />
		</br:brPanelGroup>						
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_salario}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaHistContaSalarioDest.bancoSalario}" style="margin-left:5px"/>
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterCadContaDestinoBean.saidaListaHistContaSalarioDest.agenciaSalario}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaHistContaSalarioDest.contaSalario}" style="margin-left:5px" />
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_cpf}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaHistContaSalarioDest.cpfParticipante}" style="margin-left:5px"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_nome}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaHistContaSalarioDest.nmParticipante}" style="margin-left:5px"/>
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_destino}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.bancoDestino}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.agenciaDestino}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.contaDigitoDestino}" style="margin-left:5px" />
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_tipo_conta}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.cdTipoContaDestino}" style="margin-left:5px" />
		</br:brPanelGroup>		
		
		<br:brPanelGroup  style="margin-left:20px">		
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterCadastroContaSalarioContaDestino_contingencia_ted}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaHistContaSalarioDest.cdContingenciaTed}" style="margin-left:5px" />
		</br:brPanelGroup>	
	</br:brPanelGrid>	
	
	<br:brPanelGrid  columns="1" cellpadding="0" cellspacing="0" style="text-align:left">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.hisManterCadastroContaSalarioContaDestino2_tipo_manutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaHistContaSalarioDest.tpManutencao}" style="margin-left:5px" />			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
		
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_data_hora_inclusao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.dataHoraInclusao}" style="margin-left:5px" />			
		</br:brPanelGroup>	
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.usuarioInclusao}" style="margin-left:5px" />			
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.tpDescricaoCanalInclusao}" style="margin-left:5px" />			
		</br:brPanelGroup>	
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.cdOperCanalInclusao}" style="margin-left:5px" />			
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_data_hora_manutencao}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.dataHoraManutencao}" style="margin-left:5px" />			
		</br:brPanelGroup>	
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_usuario}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.usuarioManutencao}" style="margin-left:5px" />			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>				
		
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_tipo_canal}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.tpDescricaoCanalManutencao}" style="margin-left:5px" />			
		</br:brPanelGroup>	
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_complemento}:"/>
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConHistVincContaSalarioDest.cdOperCanalManutencao}" style="margin-left:5px" />			
		</br:brPanelGroup>					
	</br:brPanelGrid>
		
	<f:verbatim><hr class="lin"></f:verbatim>

 	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px"  >
			<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.detManterCadastroContaSalarioContaDestino_btn_voltar}" action="#{manterCadContaDestinoBean.voltarHistorico}">	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>	
	</br:brPanelGrid>		
	
</br:brPanelGrid>
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
