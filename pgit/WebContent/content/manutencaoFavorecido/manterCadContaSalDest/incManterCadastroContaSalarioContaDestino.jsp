<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://sourceforge.net/projects/jsf-comp/easysi" prefix="si"%>

<brArq:form id="incluirCtaSalDestForm" name="incluirCtaSalDestForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" width="770" cellpadding="0" cellspacing="0" style="margin-left:5px">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Cliente:"/>
	    
	   	<br:brPanelGrid columns="2" style="margin-top: 6">
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}" style="margin-left:5px" />
			</br:brPanelGroup>
			
			<br:brPanelGroup style="margin-left:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.dsNomeRazao}" style="margin-left:5px" />
			</br:brPanelGroup>
	    </br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
	    <br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Contrato:"/>
	    
	   	<br:brPanelGrid columns="2" style="margin-top: 6">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.dsEmpresaGestoraContrato}" style="margin-left:5px" />
			</br:brPanelGroup>
			
			<br:brPanelGroup style="margin-left:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}" style="margin-left:5px" />
			</br:brPanelGroup>
			

		</br:brPanelGrid>
			
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_descricao_contrato}:" /> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsContrato}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}" style="margin-left:5px" />
			</br:brPanelGroup>						
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_salario}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="7" cellpadding="0" cellspacing="0" >
	
	    	 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:"/>
				</br:brPanelGroup>	
	
				 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px" >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdBancoSalario}" size="5" maxlength="3" id="txtBanco" onkeypress="onlyNum();" onkeyup="proximoCampo(3,'incluirCtaSalDestForm','incluirCtaSalDestForm:txtBanco','incluirCtaSalDestForm:txtAgencia');">
							<brArq:commonsValidator type="required" arg="Banco Sal�rio" server="false" client="true"/>
					    </br:brInputText>
					</br:brPanelGroup>
				</br:brPanelGrid>			
			</br:brPanelGrid>
			
			<br:brPanelGroup style="width:20px" >
			</br:brPanelGroup>	
	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup >
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:"/>
				</br:brPanelGroup>				
				 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px"  >					
				    <br:brPanelGroup>
						<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdAgenciaSalario}" size="8" maxlength="5" id="txtAgencia"  onkeypress="onlyNum();" onkeyup="proximoCampo(5,'incluirCtaSalDestForm','incluirCtaSalDestForm:txtAgencia','incluirCtaSalDestForm:txtConta');">
							<brArq:commonsValidator type="required" arg="Ag�ncia Sal�rio" server="false" client="true"/>
					    </br:brInputText>
					    						
						<br:brPanelGroup style="width:5px" />
		
						<br:brPanelGroup>
							<br:brInputText id="txtDigitoAgenciaS" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdDigitoAgenciaSalario}" size="3" maxlength="1" converter="javax.faces.Integer" onkeypress="onlyNum();" >
								<brArq:commonsValidator type="required" arg="D�gito Ag�ncia Sal�rio" server="false" client="true"/>
					    	</br:brInputText>
						</br:brPanelGroup>	
					</br:brPanelGroup>					
				</br:brPanelGrid>			
			</br:brPanelGrid>	
			
			<br:brPanelGroup style="width:20px" >
			</br:brPanelGroup>	
	
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_contasalario}:"/>
					</br:brPanelGroup>				
					 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:5px" >					
					    <br:brPanelGroup>
							<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdContaSalario}"  size="18" maxlength="13" id="txtConta" onkeypress="onlyNum();" onkeyup="proximoCampo(13,'incluirCtaSalDestForm','incluirCtaSalDestForm:txtConta','incluirCtaSalDestForm:txtDigito');" >
								<brArq:commonsValidator type="required" arg="Conta Sal�rio" server="false" client="true"/>								 
						    </br:brInputText>	
						</br:brPanelGroup>	
						<br:brPanelGroup style="width:5px" >
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdDigitoContaSalario}" size="3" maxlength="2" id="txtDigito" >
								<brArq:commonsValidator type="required" arg="D�gito Conta Sal�rio" server="false" client="true"/>
							</br:brInputText>	
						</br:brPanelGroup>					
					</br:brPanelGrid>			
				</br:brPanelGrid>			
			</br:brPanelGrid>
		</br:brPanelGrid>	
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_destino}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
	    <br:brPanelGrid columns="10" cellpadding="0" cellspacing="0"  >
	    	<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:"/>
				</br:brPanelGroup>	

				 <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px" >					
				    <br:brPanelGroup>
						<br:brInputText id="txtBanco2" size="5" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" maxlength="3" styleClass="HtmlInputTextBradesco" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdBanco}"  onkeypress="onlyNum()" >
							<brArq:commonsValidator type="required" arg="Banco Destino" server="false" client="true"/>		
					    </br:brInputText>
					</br:brPanelGroup>
				</br:brPanelGrid>			
			</br:brPanelGrid>
	
			<br:brPanelGroup style="width:20px" />
	
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup >
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}: "/>
					</br:brPanelGroup>				
				
					<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:5px"  >					
						<br:brPanelGroup>
							<br:brInputText id="txtAgenciaSalario" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" size="8" maxlength="5" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdAgencia}" converter="javax.faces.Integer" onkeypress="onlyNum();" >
								<brArq:commonsValidator type="required" arg="Ag�ncia Destino" server="false" client="true"/>
							</br:brInputText>	
						</br:brPanelGroup>			
						
						<br:brPanelGroup style="width:5px" />
		
						<br:brPanelGroup>
							<br:brInputText id="txtDigitoAgenciaD" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdDigitoAgencia}" size="3" maxlength="1" converter="javax.faces.Integer" onkeypress="onlyNum();" >
								<brArq:commonsValidator type="required" arg="D�gito Ag�ncia Destino" server="false" client="true"/>
					    	</br:brInputText>
						</br:brPanelGroup>			
					</br:brPanelGrid>	
				</br:brPanelGrid>
			</br:brPanelGrid>	
				
			<br:brPanelGroup style="width:20px" />
	
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
						<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Conta:"/>
					</br:brPanelGroup>				
					 <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:5px" >					
					    <br:brPanelGroup>
							<br:brInputText id="txtContaSalario" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdConta}" size="18" maxlength="13" onkeypress="onlyNum()" >
								<brArq:commonsValidator type="required" arg="Conta Destino" server="false" client="true"/>								 
						    </br:brInputText>	
						</br:brPanelGroup>	
						<br:brPanelGroup style="width:5px" >
						</br:brPanelGroup>
						<br:brPanelGroup>
							<br:brInputText styleClass="HtmlInputTextBradesco" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdDigitoConta}" size="3" maxlength="2" id="txtDigito2" onkeypress="onlyNum()" >
								<brArq:commonsValidator type="required" arg="D�gito Conta Destino" server="false" client="true"/>
							</br:brInputText>	
						</br:brPanelGroup>					
					</br:brPanelGrid>			
				</br:brPanelGrid>			
			</br:brPanelGrid>
	
			<br:brPanelGroup style="width:20px" />
	
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_tipo_conta}:"/>
				</br:brPanelGroup>	
				
				
				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:5px">					
				    <br:brPanelGroup>	
   				    	<br:brSelectOneMenu id="tipoConta" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdTipoConta}" >
							<f:selectItem itemValue="" itemLabel="#{msgs.detManterCadastroContaSalarioContaDestino_label_combo_selecione}"/>
							<si:selectItems value="#{manterCadContaDestinoBean.listaTipoConta}" var="tipoConta" itemLabel="#{tipoConta.dsTipoConta}" itemValue="#{tipoConta.cdTipoConta}" />
							<brArq:commonsValidator type="required" arg="Tipo Conta Destino" server="false" client="true"/>								 							
						</br:brSelectOneMenu>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>
			
			<br:brPanelGroup style="width:20px" />
			
			<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >			
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.incManterCadastroContaSalarioContaDestino_contingencia_ted}:" />
				</br:brPanelGroup>	
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" style="margin-top:5px">	
					<br:brPanelGroup>
						<br:brSelectOneMenu id="contigenciaTED" value="#{manterCadContaDestinoBean.entradaIncluirContaDestino.cdContingenciaTed}" >
							<f:selectItem itemValue="2" itemLabel="N�O"/>
							<f:selectItem itemValue="1" itemLabel="SIM"/>
						</br:brSelectOneMenu>	
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>	
		</br:brPanelGrid>	
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px"  >
				<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.detManterCadastroContaSalarioContaDestino_btn_voltar}" action="#{manterCadContaDestinoBean.voltarPaginaCadContaSalario}">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
			
			<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnAvancar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.incManterCadastroContaSalarioContaDestino2_btn_avancar}" action="#{manterCadContaDestinoBean.avancarIncluirCadContaSalario}" >				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup> 
		</br:brPanelGrid>	
	
	</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
