<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="detalharCtaSalDestForm" name="detalharCtaSalDestForm" >
<br:brPanelGrid columns="1" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" width="770" style="margin-left:5px" >
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
    <br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Cliente:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}" style="margin-left:5px" />
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social:" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.dsNomeRazao}" style="margin-left:5px" />
		</br:brPanelGroup>
    </br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
    <br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Contrato:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.dsEmpresaGestoraContrato}" style="margin-left:5px" />
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}" style="margin-left:5px" />
		</br:brPanelGroup>
		
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
	
		<br:brPanelGroup >			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px" >
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_descricao_contrato}:" /> 
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsContrato}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_situacao}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}" style="margin-left:5px" />
		</br:brPanelGroup>						
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_salario}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.bancoSalario}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco"  value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.agenciaSalario}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.contaDigitoSalario}" style="margin-left:5px" />
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_cpf}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cpfParticipante}" style="margin-left:5px" />
		</br:brPanelGroup>
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_nome}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.nmParticipante}" style="margin-left:5px" />
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_destino}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
	<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.bancoDestino}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.agenciaDestino}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.contaDigitoDestino}" style="margin-left:5px" />
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_tipo_conta}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cdTipoContaDestino}" style="margin-left:5px" />
		</br:brPanelGroup>	
		
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterCadastroContaSalarioContaDestino_contingencia_ted}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cdContingenciaTed}" style="margin-left:5px" />
		</br:brPanelGroup>		
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
		
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_data_hora_inclusao}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConVincContaSalarioDest.dataHoraInclusao}" style="margin-left:5px" />			
		</br:brPanelGroup>	
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_usuario}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConVincContaSalarioDest.cdUsuarioInclusao}" style="margin-left:5px" />			
		</br:brPanelGroup>
	</br:brPanelGrid>
			
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" style="text-align:left">		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_tipo_canal}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConVincContaSalarioDest.tpDescricaoCanalInclusao}" style="margin-left:5px" />			
		</br:brPanelGroup>	
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_complemento}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConVincContaSalarioDest.cdOperCanalInclusao}" style="margin-left:5px" />			
		</br:brPanelGroup>					
	</br:brPanelGrid>
	
	<f:verbatim><hr class="lin"> </f:verbatim>	
	
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0">
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_data_hora_manutencao}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConVincContaSalarioDest.dataHoraManutencao}" style="margin-left:5px" />			
		</br:brPanelGroup>	
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_usuario}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConVincContaSalarioDest.cdUsuarioManutencao}" style="margin-left:5px" />			
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>				
		
	<br:brPanelGrid  columns="2" cellpadding="0" cellspacing="0" >		
		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_tipo_canal}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConVincContaSalarioDest.tpDescricaoCanalManutencao}" style="margin-left:5px" />			
		</br:brPanelGroup>	
		<br:brPanelGroup style="margin-left:20px">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
			<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.ManterMoedasSistema_complemento}:" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConVincContaSalarioDest.cdOperCanalManutencao}" style="margin-left:5px" />			
		</br:brPanelGroup>					
	</br:brPanelGrid>	
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
    <br:brPanelGrid columns="2" width="100%"  cellpadding="0" cellspacing="0">	
		<br:brPanelGroup style="text-align:left">
			<br:brCommandButton id="btnVoltar" styleClass="bto1" action="#{manterCadContaDestinoBean.voltarPaginaCadContaSalario}"  value="#{msgs.label_voltar}">
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
</br:brPanelGrid>
	
<brArq:validatorScript functionName="validateForm"/>
</brArq:form>
