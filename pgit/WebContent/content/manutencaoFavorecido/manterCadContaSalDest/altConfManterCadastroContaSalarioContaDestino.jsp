<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="detalharCtaSalDestForm" name="detalharCtaSalDestForm" >
	<br:brPanelGrid columns="1" styleClass="CorpoPagina" width="770" cellpadding="0" cellspacing="0" style="margin-left:5px">
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_title_cliente}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
	   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}" style="margin-left:5px" />
			</br:brPanelGroup>
			
			<br:brPanelGroup style="margin-left:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social:" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.dsNomeRazao}" style="margin-left:5px" />
			</br:brPanelGroup>
	    </br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		  <br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_title_contrato}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	    
	   	<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.dsEmpresaGestoraContrato}" style="margin-left:5px" />
			</br:brPanelGroup>
			
			<br:brPanelGroup style="margin-left:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}" style="margin-left:5px" />
			</br:brPanelGroup>
			
	
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
		
			<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_descricao_contrato}:" /> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsContrato}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}" style="margin-left:5px" />
			</br:brPanelGroup>						
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_salario}: "/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.bancoSalario}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.agenciaSalario}" style="margin-left:5px" /> 
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.contaDigitoSalario}" style="margin-left:5px" />
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_cpf}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cpfParticipante}" style="margin-left:5px" />
			</br:brPanelGroup>	
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_nome}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.nmParticipante}" style="margin-left:5px" />
			</br:brPanelGroup>		
		</br:brPanelGrid>	
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta_destino}: "/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.bancoDestino}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.agenciaDestino}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.contaDigitoDestino}" style="margin-left:5px" />
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_tipo_conta}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cdTipoContaDestino}" style="margin-left:5px" />
			</br:brPanelGroup>	
				
			<br:brPanelGroup style="margin-left:20px">		
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterCadastroContaSalarioContaDestino_contingencia_ted}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaListaContaSalDestino.cdContingenciaTed}" style="margin-left:5px" />
			</br:brPanelGroup>	
		</br:brPanelGrid>		
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		<br:brPanelGrid styleClass="mainPanel" columns="1" width="100%" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{msgs.altManterCadastroContaSalarioContaDestino_nova_conta}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_banco}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.banco}" style="margin-left:5px" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup  style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_agencia}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.agenciaDigito}" style="margin-left:5px"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup  style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_conta}:" />				
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.contaDigito}" style="margin-left:5px"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0">
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.detManterCadastroContaSalarioContaDestino_tipo_conta}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.dsTipoConta}" style="margin-left:5px"/>
			</br:brPanelGroup>		
			
			<br:brPanelGroup  style="margin-left:20px">		
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.incManterCadastroContaSalarioContaDestino_contingencia_ted}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.entradaAlterarContaDestino.dsContingenciaTed}" style="margin-left:5px" />
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
			<br:brPanelGrid columns="3" width="100%" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup style="text-align:left;width:150px"  >
				<br:brCommandButton id="btnVoltar" styleClass="bto1"  value="#{msgs.detManterCadastroContaSalarioContaDestino_btn_voltar}" action="#{manterCadContaDestinoBean.voltarAlteracao}">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>	
			
			<br:brPanelGrid columns="1" style="width:450px" cellpadding="0" cellspacing="0" >
				<br:brPanelGroup> 
				</br:brPanelGroup>
			</br:brPanelGrid>	
			<br:brPanelGroup style="text-align:right;width:150px" >
				<br:brCommandButton id="btnConfirmar" onclick="javascript: return validateForm(document.forms[1]);" styleClass="bto1" value="#{msgs.incManterCadastroContaSalarioContaDestino2_btn_confirmar}" 
				onclick="javascript: if (!confirm('Confirma Altera��o?')) { desbloquearTela(); return false; }" action="#{manterCadContaDestinoBean.confirmarAlteracao}" >				
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup> 
		</br:brPanelGrid>	
		
	</br:brPanelGrid>
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>
