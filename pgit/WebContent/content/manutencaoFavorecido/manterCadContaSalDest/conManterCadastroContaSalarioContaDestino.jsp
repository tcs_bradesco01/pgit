<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>


<brArq:form id="frmCadCtaSalDestForm" name="frmCadCtaSalDestForm" > 
	<br:brPanelGrid columns="1" width="770" styleClass="CorpoPagina" cellpadding="0" cellspacing="0" style="margin-left:5px">
		<br:brPanelGrid columns="1" width="100%" style="margin-top:9px" cellpadding="0" cellspacing="0">
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	 	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Identifica��o do Cliente:"/>
	 	
	 	<a4j:region id="regionFiltro">
		<t:selectOneRadio id="rdoFiltroCliente" value="#{manterCadContaDestinoBean.itemFiltroSelecionado}" disabled="#{manterCadContaDestinoBean.bloqueaRadio}"  styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
			<f:selectItem itemValue="0" itemLabel="" />
			<f:selectItem itemValue="1" itemLabel="" />
			<f:selectItem itemValue="2" itemLabel="" />
			<f:selectItem itemValue="3" itemLabel="" />
			<a4j:support event="onclick" reRender="panelCnpj, panelCpf, panelNomeRazao, panelDataNascFunc, panelBanco, panelAgencia, panelConta, panelBtoConsultarCliente" action="#{manterCadContaDestinoBean.limparCamposDoFiltro}" ajaxSingle="true" oncomplete="javascript: focoAgencias(document.forms[1])" />
		</t:selectOneRadio>
		</a4j:region>
		
	    <br:brPanelGrid columns="2">
			<t:radio for=":frmCadCtaSalDestForm:rdoFiltroCliente" index="0" />			
			
			 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
				<br:brPanelGrid columns="2">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="CNPJ: " />
				</br:brPanelGrid>	
				
			   <br:brPanelGrid columns="5">					
				    <br:brInputText id="txtCnpj" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{manterCadContaDestinoBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterCadContaDestinoBean.entradaConsultarListaClientePessoas.cdCpfCnpj}"/>
	  			 	<f:verbatim>&nbsp;</f:verbatim>
				    <br:brInputText id="txtFilial" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{manterCadContaDestinoBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterCadContaDestinoBean.entradaConsultarListaClientePessoas.cdFilialCnpj}" />
		   		 	<f:verbatim>&nbsp;</f:verbatim>
					<br:brInputText id="txtControle" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{manterCadContaDestinoBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterCadContaDestinoBean.entradaConsultarListaClientePessoas.cdControleCnpj}" />
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2">
			<t:radio for=":frmCadCtaSalDestForm:rdoFiltroCliente" index="1" />			
			
			 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
				<br:brPanelGrid columns="2">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="CPF: " />
				</br:brPanelGrid>	
				
			   <br:brPanelGrid columns="3" >					
				    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{manterCadContaDestinoBean.itemFiltroSelecionado != '1'}" value="#{manterCadContaDestinoBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" />
	  			 	<f:verbatim>&nbsp;</f:verbatim>
				    <br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{manterCadContaDestinoBean.itemFiltroSelecionado != '1'}" value="#{manterCadContaDestinoBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" />
				</br:brPanelGrid>
			</a4j:outputPanel>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="4">
			<t:radio for=":frmCadCtaSalDestForm:rdoFiltroCliente" index="2" />		
			
			 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
				<br:brPanelGrid columns="2" style="text-align:left;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Nome/Raz�o Social: "/>
				</br:brPanelGrid>	
				
			  	<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{manterCadContaDestinoBean.itemFiltroSelecionado != '2'}" value="#{manterCadContaDestinoBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="75" maxlength="70" id="txtNomeRazaoSocial"/>
			</a4j:outputPanel>
			
		</br:brPanelGrid>
	    
	     <br:brPanelGrid columns="4">
			<t:radio for="rdoFiltroCliente" index="3" />			
			
			<a4j:outputPanel id="panelBanco" ajaxRendered="true">
				<br:brPanelGrid columns="2">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Banco: " />
				</br:brPanelGrid>	
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterCadContaDestinoBean.itemFiltroSelecionado != '3'}" value="#{manterCadContaDestinoBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" />
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
				<br:brPanelGrid columns="2">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Ag�ncia: " />
				</br:brPanelGrid>	
				
				<br:brInputText id="txtAgencia" size="6" maxlength="5" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterCadContaDestinoBean.itemFiltroSelecionado != '3'}" value="#{manterCadContaDestinoBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" />
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelConta" ajaxRendered="true">
				<br:brPanelGrid columns="2">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Conta: " />
				</br:brPanelGrid>	
				
				<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{manterCadContaDestinoBean.itemFiltroSelecionado != '3'}" value="#{manterCadContaDestinoBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" />
			</a4j:outputPanel>
	    </br:brPanelGrid>
	    
	   	<f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
		   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty manterCadContaDestinoBean.itemFiltroSelecionado}" value="consultar cliente" action="#{manterCadContaDestinoBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCpoConsultaCliente(document.forms[1]);">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</a4j:outputPanel>
		</br:brPanelGrid>
	    
	    <br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Cliente:"/>
	    
	   	<br:brPanelGrid columns="2" style="margin-top: 6">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
	    
		<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Identifica��o do Contrato:"/>
		
		<br:brPanelGrid columns="5" style="margin-top: 5">
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2"/>
			</br:brPanelGroup>	
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Tipo de Contrato: " style="margin-left: 2"/>
			</br:brPanelGroup>	
					
			<f:verbatim>&nbsp;</f:verbatim>		
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="N�mero do Contrato:" style="margin-left: 2"/>
			</br:brPanelGroup>	
		
			<br:brSelectOneMenu id="selEmpresaGestoraContrada" converter="longConverter" value="#{manterCadContaDestinoBean.entradaConsultarListaContratosPessoas.cdPessoaJuridicaContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
				<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
				<f:selectItems  value="#{manterCadContaDestinoBean.preencherEmpresaGestora}" />
			</br:brSelectOneMenu>
			
			<f:verbatim>&nbsp;</f:verbatim>
		
			<br:brSelectOneMenu id="selTipoContrato" converter="inteiroConverter" value="#{manterCadContaDestinoBean.entradaConsultarListaContratosPessoas.cdTipoContratoNegocio}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
				<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
				<f:selectItems  value="#{manterCadContaDestinoBean.preencherTipoContrato}" />
			</br:brSelectOneMenu>
			
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" converter="javax.faces.Long" onkeypress="onlyNum();" value="#{manterCadContaDestinoBean.entradaConsultarListaContratosPessoas.nrSeqContratoNegocio}" size="13" maxlength="10" id="txtNumeroContrato" />
		</br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
	    	<br:brPanelGroup style="width:100%; text-align: right">
		   		<br:brCommandButton id="btoConsultarContrato" styleClass="bto1" value="consultar contrato" action="#{manterCadContaDestinoBean.consultarContratos}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript: desbloquearTela(); return validaCpoContratoConsulta(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}','#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}','#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}', '#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}', '#{manterCadContaDestinoBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}');" >
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	      
	    <br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Contrato:"/>
	    
	   	<br:brPanelGrid columns="3" style="margin-top: 6">
	   		<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato:"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.dsEmpresaGestoraContrato}" style="margin-left:5" />
			</br:brPanelGroup>
			
			<br:brPanelGroup style="margin-left:20px">			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}" style="margin-left:5" />
			</br:brPanelGroup>
		
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1" styleClass="EspacamentoLinhas" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	    <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
	    
	    	<br:brPanelGroup >			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}" style="margin-left:5" />
			</br:brPanelGroup>
				
			<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_descricao_contrato}:" /> 
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsContrato}" style="margin-left:5" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="margin-left:20px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.conManterCadastroContaSalarioContaDestino_situacao}:" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterCadContaDestinoBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}" style="margin-left:5" />
			</br:brPanelGroup>						
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>	
		
		 <a4j:region id="regionArgumentoPesquisa">
			<t:selectOneRadio id="rdoFiltroPesquisa" disabled="#{manterCadContaDestinoBean.habilitaFiltroDeCadContaSalarioDet}" value="#{manterCadContaDestinoBean.itemSelecionadoFiltro}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<a4j:support event="onclick" reRender="panelCpf2, panelBanco2, panelAgencia2, panelConta2, panelBotoes" action="#{manterCadContaDestinoBean.limparCampoFiltro}" ajaxSingle="true" />
			</t:selectOneRadio>
		</a4j:region>
		
	    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Argumentos de Pesquisa:"/>
		
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" > 
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>	
	
	    <br:brPanelGrid columns="7">
			<t:radio for=":frmCadCtaSalDestForm:rdoFiltroPesquisa" index="0" />			
			
			<a4j:outputPanel id="panelBanco2" ajaxRendered="true">
				<br:brPanelGrid columns="2" style="text-align:left;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_banco_conta_salario}:" />
				</br:brPanelGrid>	
				
				<br:brInputText id="txtBancoP" size="4" maxlength="3" styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterCadContaDestinoBean.itemSelecionadoFiltro != 1}" value="#{manterCadContaDestinoBean.entradaListaContaSalDestino.cdBanco}" style="margin-left:5px" />
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelAgencia2" ajaxRendered="true">
				<br:brPanelGrid columns="2" style="text-align:left;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_agencia_conta_salario}:"/>
				</br:brPanelGrid>	
				
				<br:brInputText id="txtAgenciaP" size="6" maxlength="5" styleClass="HtmlInputTextBradesco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterCadContaDestinoBean.itemSelecionadoFiltro != 1}" value="#{manterCadContaDestinoBean.entradaListaContaSalDestino.cdAgencia}" style="margin-left:5px" />								
			</a4j:outputPanel>
			
			<a4j:outputPanel id="panelConta2" ajaxRendered="true">			
				<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
					<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0"  >
						<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_conta_salario}:"/>
						</br:brPanelGroup>				
						<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" style="margin-top:5px" >					
							<br:brPanelGroup>
								<br:brInputText id="txtContaP" size="18" maxlength="13" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Long" disabled="#{manterCadContaDestinoBean.itemSelecionadoFiltro != 1}" value="#{manterCadContaDestinoBean.entradaListaContaSalDestino.cdConta}" />
							</br:brPanelGroup>	
							
							<br:brPanelGroup style="width:5px" />
	
							<br:brPanelGroup>
								<br:brInputText id="txtDigitoContaP" size="3" maxlength="2" styleClass="HtmlInputTextBradesco" disabled="#{manterCadContaDestinoBean.itemSelecionadoFiltro != 1}" value="#{manterCadContaDestinoBean.entradaListaContaSalDestino.dgConta}" />
							</br:brPanelGroup>					
						</br:brPanelGrid>			
					</br:brPanelGrid>
				</br:brPanelGrid>			
			</a4j:outputPanel>
		</br:brPanelGrid>	
	
		<br:brPanelGrid columns="2">	
			<t:radio for=":frmCadCtaSalDestForm:rdoFiltroPesquisa" index="1" />			
			
			<a4j:outputPanel id="panelCpf2" ajaxRendered="true">
				<br:brPanelGrid columns="2" style="text-align:left;">
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_cpf_favorecido}:" />
				</br:brPanelGrid>	
				
			   <br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >					
				    <br:brInputText id="txtCpfP" size="12" maxlength="9" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Long" disabled="#{manterCadContaDestinoBean.itemSelecionadoFiltro != 2}" value="#{manterCadContaDestinoBean.entradaListaContaSalDestino.nrCpf}" />
	  			 	<f:verbatim>&nbsp;</f:verbatim>
				    <br:brInputText id="txtControleCpfP" size="4" maxlength="2" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterCadContaDestinoBean.itemSelecionadoFiltro != 2}" value="#{manterCadContaDestinoBean.entradaListaContaSalDestino.dgCpf}" />
				</br:brPanelGrid>
			</a4j:outputPanel>	
		</br:brPanelGrid>
			
		<f:verbatim><hr class="lin"> </f:verbatim>
	
		<br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" styleClass="bto1" value="#{msgs.conManterCadastroContaSalarioContaDestino_btn_limpar_campos}" disabled="#{manterCadContaDestinoBean.desabilataFiltro}" action="#{manterCadContaDestinoBean.limparCampos}" style="margin-right:5px">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" value="#{msgs.conManterCadastroContaSalarioContaDestino_btn_consultar}" disabled="#{manterCadContaDestinoBean.desabilataFiltro}" action="#{manterCadContaDestinoBean.consultarCadContaSalario}" onclick="javascript:desbloquearTela(); return validarFiltroConsContaSalarioDest();">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><br></f:verbatim> 
		
		<a4j:region id="regionRadioContaSalarioDest">
		<t:selectOneRadio id="rdoContaSalarioDest" value="#{manterCadContaDestinoBean.itemSelecionadoLista}" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
			<f:selectItems value="#{manterCadContaDestinoBean.selectItemManterCadContaDestino}"/>
			<a4j:support event="onclick" reRender="panelBotoes" limitToList="true" ajaxSingle="true" />
		</t:selectOneRadio>
		</a4j:region>
		
		<br:brPanelGrid columns="1" align="left" width="750">
			<br:brPanelGroup style="width: 100%">
				<t:div id="divTable" style="overflow-x:auto; overflow-y:auto; width:760px; height:170; display:block" >
					
					<app:scrollableDataTable id="dataTable" value="#{manterCadContaDestinoBean.listaContaSalDestino}"  rowIndexVar="parametroKey" rows="10" width="100%" var="result" >				
					    <app:scrollableColumn styleClass="colTabCenter" width="20px" >
						    <f:facet name="header">    
							    &nbsp;
						    </f:facet>		
						    <t:radio for=":frmCadCtaSalDestForm:rdoContaSalarioDest" index="#{parametroKey}" />
						</app:scrollableColumn>   
						<app:scrollableColumn width="250px" styleClass="colTabLeft">	 
							<f:facet name="header">
							    <br:brOutputText value="#{msgs.label_nome_favorecido}" style="text-align:center; width:250" />
							</f:facet>
							<br:brOutputText value="#{result.nmParticipante}"/>
						</app:scrollableColumn> 
						<app:scrollableColumn width="150px" styleClass="colTabRight">	 
							<f:facet name="header">
							    <br:brOutputText value="#{msgs.label_cpf_favorecido}" style="text-align:center; width:150" />
							</f:facet>
							<br:brOutputText value="#{result.cpfParticipante}"/>
						</app:scrollableColumn> 												
						<app:scrollableColumn width="200px" styleClass="colTabLeft">	 
							<f:facet name="header">
							    <br:brOutputText value="#{msgs.conManterCadastroContaSalarioContaDestino_grid_banco_conta_salario}" style="text-align:center; width:200" />
							</f:facet>
							<br:brOutputText value="#{result.bancoSalario}"/>
						</app:scrollableColumn> 
						<app:scrollableColumn  width="200px"  styleClass="colTabLeft">
							<f:facet name="header">
							    <br:brOutputText value="#{msgs.conManterCadastroContaSalarioContaDestino_grid_agencia_conta_salario}" style="text-align:center; width:200" />
							</f:facet>
							<br:brOutputText value="#{result.agenciaSalario}"/>
						</app:scrollableColumn>	
						
						<app:scrollableColumn  width="170px"  styleClass="colTabRight">
							<f:facet name="header">
							    <br:brOutputText value="#{msgs.conManterCadastroContaSalarioContaDestino_grid_conta_salario}" style="text-align:center; width:170" />
							</f:facet>
							<br:brOutputText value="#{result.contaDigitoSalario}"/>
						</app:scrollableColumn>
				
						<app:scrollableColumn  width="200px"  styleClass="colTabLeft">
							<f:facet name="header">
							  <br:brOutputText value="#{msgs.conManterCadastroContaSalarioContaDestino_grid_banco_conta_destino}" style="text-align:center; width:200" />
							</f:facet>
							<br:brOutputText value="#{result.bancoDestino}"/>
						</app:scrollableColumn>
						
						<app:scrollableColumn  width="200px"  styleClass="colTabLeft">
							<f:facet name="header">
							  <br:brOutputText value="#{msgs.conManterCadastroContaSalarioContaDestino_grid_agencia_conta_destino}" style="text-align:center; width:200" />
							</f:facet>
							<br:brOutputText value="#{result.agenciaDestino}"/>
						</app:scrollableColumn>
						
						<app:scrollableColumn  width="180px"  styleClass="colTabRight">
							<f:facet name="header">
							  <br:brOutputText value="#{msgs.conManterCadastroContaSalarioContaDestino_grid_conta_destino}" style="text-align:center; width:180" />
							</f:facet>
							<br:brOutputText value="#{result.contaDigitoDestino}"/>
						</app:scrollableColumn>
						
						<app:scrollableColumn  width="180px"  styleClass="colTabLeft">
							<f:facet name="header">
							  <br:brOutputText value="#{msgs.conManterCadastroContaSalarioContaDestino_grid_tipo_conta_destino}" style="text-align:center; width:180" />
							</f:facet>
							<br:brOutputText value="#{result.cdTipoContaDestino}"/>
						</app:scrollableColumn>
				
						<app:scrollableColumn  width="180px"  styleClass="colTabLeft">
							<f:facet name="header">
							  <br:brOutputText value="#{msgs.conManterCadastroContaSalarioContaDestino_grid_situacao}" style="text-align:center; width:180" />
							</f:facet>
							<br:brOutputText value="#{result.dsSituacaoContaDestino}"/>
						</app:scrollableColumn> 
					</app:scrollableDataTable>	
				</t:div>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
	
	 	<br:brPanelGrid columns="1" width="770" style="text-align:center" cellpadding="0" cellspacing="0" >	
			<br:brPanelGroup>
				<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{manterCadContaDestinoBean.listarContaSalDestino}" rendered="#{manterCadContaDestinoBean.listaContaSalDestinoEmpty == false}">
					<f:facet name="first">
				    	<brArq:pdcCommandButton id="primeira" styleClass="bto1" value="#{msgs.label_primeira}" title="#{msgs.label_primeira}"/>
					</f:facet>
				  	<f:facet name="fastrewind">
						<brArq:pdcCommandButton id="retrocessoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}"/>
					</f:facet>
				  	<f:facet name="previous"> 
				  		<brArq:pdcCommandButton id="anterior" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
				  	</f:facet>
				  	<f:facet name="next">
				    	<brArq:pdcCommandButton id="proxima" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
				  	</f:facet>
				  	<f:facet name="fastforward">
				    	<brArq:pdcCommandButton id="avancoRapido" styleClass="bto1" style="margin-left: 3px;" value="#{msgs.label_avanco}" title="#{msgs.label_avanco}"/>
				    </f:facet>
				  	<f:facet name="last">
				    	<brArq:pdcCommandButton id="ultima" styleClass="bto1" style="margin-left: 3px;"	value="#{msgs.label_ultima}" title="#{msgs.label_ultima}"/>
				  	</f:facet>
				</brArq:pdcDataScroller>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		<f:verbatim><hr class="lin"> </f:verbatim>
	 
	 	<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">	 	
		 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
				<br:brPanelGroup>
					<br:brCommandButton id="btnLimpar"  disabled="#{manterCadContaDestinoBean.habilitaFiltro}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterCadastroContaSalarioContaDestino_btn_limpar}" action="#{manterCadContaDestinoBean.limpar}" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>		
					<br:brCommandButton id="btnDetalhar" disabled="#{empty manterCadContaDestinoBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterCadastroContaSalarioContaDestino_btn_detalhar}" action="#{manterCadContaDestinoBean.detalharCadContaSalario}" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnIncluir" disabled="#{manterCadContaDestinoBean.desabilataFiltro}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterCadastroContaSalarioContaDestino_btn_incluir}" action="#{manterCadContaDestinoBean.incluirCadContaSalario}" >
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnAlterar" disabled="#{empty manterCadContaDestinoBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterCadastroContaSalarioContaDestino_btn_alterar}"  action="#{manterCadContaDestinoBean.alterarCadContaSalario}">
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnExcluir" disabled="#{empty manterCadContaDestinoBean.itemSelecionadoLista}" styleClass="bto1" style="margin-right:5px" value="#{msgs.conManterCadastroContaSalarioContaDestino_btn_excluir}" action="#{manterCadContaDestinoBean.excluirCadContaSalario}" >				
						<brArq:submitCheckClient/>
					</br:brCommandButton>
					<br:brCommandButton id="btnHistorico" disabled="#{manterCadContaDestinoBean.habilitaFiltro}" styleClass="bto1" value="#{msgs.conManterCadastroContaSalarioContaDestino_btn_historico}" action="#{manterCadContaDestinoBean.abrirConsultarHistorico}" >				
						<brArq:submitCheckClient/>
					</br:brCommandButton>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</a4j:outputPanel>					
	
	</br:brPanelGrid>
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />

</brArq:form>
