<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmIncSolicRastFavorecido" name="frmIncSolicRastFavorecido">

	<br:brPanelGrid columns="1" width="770" style="margin-top:9">
    	
	 	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Cliente:"/>
	    
	   	<br:brPanelGrid columns="2" style="margin-top: 6">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicRastFavBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
	 		    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Contrato:"/>
	    
	   	<br:brPanelGrid columns="2" style="margin-top: 6">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.dsEmpresaGestoraContrato}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato: " style="margin-left: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGrid columns="3" style="margin-top: 6">	
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato: " style="margin-left: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Descri��o do Contrato: "  />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
			</br:brPanelGroup>
			
				
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: "  />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

		
		 <f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
	    
	    <br:brPanelGrid columns="1" width="100%" style="margin-top: 6">
			<br:brPanelGroup style="text-align:left; width:100%">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Per�odo Rastreamento: " style="margin-left: 5"/>
			</br:brPanelGroup>	
			
			
		<br:brPanelGroup id="calendarios" style="width: 100%; text-align: left" onmousedown="javascript: cleanClipboard();" >		
			<br:brPanelGrid columns="2" width="100%" style="margin-top: 6">
				<br:brPanelGroup>	
					<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco" value="De" style="margin-right: 10"/>
						<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoInicio');"  id="txtPeriodoInicio" value="#{manterSolicRastFavBean.dataInicioRastreamento}" disabled="false">							 
			   				<f:converter converterId="dateBrazillianConverter"/>	
			   			</app:calendar>
			   			
			  		<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco" value="a" style="margin-left: 10;margin-right: 10"/>
				   		<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoFim');" id="txtPeriodoFim" value="#{manterSolicRastFavBean.dataFimRastreamento}" disabled="false">
							<f:converter converterId="dateBrazillianConverter"/>
			   			</app:calendar>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGroup>	
			
			
		</br:brPanelGrid>
				
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Servi�o: " style="margin-left: 5"/>
			</br:brPanelGroup>	
		
		<a4j:region>	
		<br:brPanelGrid columns="2">
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Tipo de Servi�o: " />	
				<br:brSelectOneMenu id="selTipoServico" styleClass="HtmlSelectOneMenuBradesco" value="#{manterSolicRastFavBean.filtroTipoServico}" style="margin-top:5px">
					<f:selectItem itemValue="0" itemLabel="::SELECIONE::" />
					<f:selectItems value="#{manterSolicRastFavBean.listaTipoServico}"/>
					<a4j:support event="onchange" reRender="selModalidadeServico" ajaxSingle="true" action="#{manterSolicRastFavBean.preencheListaModalidade}" />
				</br:brSelectOneMenu>
			</br:brPanelGroup>
							
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Modalidade: " style="margin-left: 5"/>
				<br:brSelectOneMenu id="selModalidadeServico" styleClass="HtmlSelectOneMenuBradesco" value="#{manterSolicRastFavBean.filtroModalidadeServico}" style="margin-top:5px">
					<f:selectItem itemValue="0" itemLabel="::SELECIONE::"/>
					<f:selectItems value="#{manterSolicRastFavBean.listaModalidadeServico}"/>
				</br:brSelectOneMenu>
			</br:brPanelGroup>		
		</br:brPanelGrid>	
		</a4j:region>
			
	<br:brPanelGrid columns="3" width="65%">
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Banco: " style="margin-left: 5" onkeypress="javascript: onlyNum()" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Ag�ncia: " style="margin-left: 5" onkeypress="javascript: onlyNum()" />
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Conta D�bito: " style="margin-left: 5" onkeypress="javascript: onlyNum()" />
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brInputText value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.cdBanco}" converter="javax.faces.Integer" maxlength="3" size="5" id="txtBanco" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco " onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			</br:brPanelGroup>
			
			<br:brPanelGroup>                                   
				<br:brInputText value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.cdAgencia}" converter="javax.faces.Integer" maxlength="5" size="7" id="txtAgencia" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco " onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');" />
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brInputText value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.cdConta}" converter="javax.faces.Long" maxlength="13" size="18" id="txtConta" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco " onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
				<br:brInputText value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.dgConta}" maxlength="2" size="4" id="txtDigito" onkeypress="onlyNum();" styleClass="HtmlInputTextBradesco " style="margin-left: 5" />
			</br:brPanelGroup>	
							
		</br:brPanelGrid>		
		
		<br:brPanelGrid columns="1" width="90%">	
			<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Inclus�o Autom�tica de Favorecidos: " style="margin-left: 5"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2" width="30%">	
			 <a4j:region id="regionBloqDesbloq">
				<t:selectOneRadio id="rdoDesBloq" style="vertical-align: middle" value="#{manterSolicRastFavBean.inclusaoAuto}" converter="javax.faces.Integer" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
					<f:selectItems value="#{manterSolicRastFavBean.preencherBloqueoDesbloqueo}" />
					<a4j:support event="onclick" reRender="panelFavorecido" action="#{manterSolicRastFavBean.iniciaFavorecido}" />
				</t:selectOneRadio>
		   	 </a4j:region>
		    
			<br:brPanelGroup>
				<t:radio for="rdoDesBloq" index="0" />			
				<f:verbatim>&nbsp;</f:verbatim>
				<t:radio for="rdoDesBloq" index="1" />		
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1">	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo_codigo_favorecido}: "  style="margin-left: 5"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup id="panelFavorecido">
				
				<a4j:region id="regionFavorecido">
					<t:selectOneRadio id="rdoFavorecido" style="vertical-align: middle" value="#{manterSolicRastFavBean.incluisaoFavorecido}" disabled="#{manterSolicRastFavBean.inclusaoAuto == 2}" converter="javax.faces.Integer" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
						<f:selectItems value="#{manterSolicRastFavBean.preencherFavorecido}" />
					</t:selectOneRadio>
			   	 </a4j:region>
			    
				<br:brPanelGroup>
					<t:radio for="rdoFavorecido" index="0" />			
					<f:verbatim>&nbsp;</f:verbatim>
					<t:radio for="rdoFavorecido" index="1" />		
				</br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
				
		<a4j:region id="regionDestino">
			<t:selectOneRadio id="radioDestino" style="vertical-align: middle" value="#{manterSolicRastFavBean.destinoAquivoRadio}" 
			converter="javax.faces.Integer" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItems value="#{manterSolicRastFavBean.preencherDestino}" />
			</t:selectOneRadio>
	   	 </a4j:region>
		
		<br:brPanelGrid columns="1" width="90%">	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Destino de Arquivo Retorno: " style="margin-left: 5"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<a4j:region>	
			<br:brPanelGrid columns="4" width="40%">	
				<t:radio for="radioDestino" index="0" />		
				<f:verbatim>&nbsp;</f:verbatim>
				<t:radio for="radioDestino" index="1" />		
				<f:verbatim>&nbsp;</f:verbatim>
			</br:brPanelGrid>
		</a4j:region>
		
		
		<f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<a4j:region id="regionTarifa">		
			<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0">
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" border="0">
					<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}: "/>
							<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.vlTarifaPadrao}" id="txtTarifaPadrao" converter="decimalBrazillianConverter"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
						<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />			
						<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="#{msgs.label_desconto_tarifa}: "/>
				    	<br:brInputText size="9" id="txtDescontoTarifa" alt="percentual" value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.numPercentualDescTarifa}" converter="decimalBrazillianConverter" style="text-align:right; margin-right:3px" styleClass="HtmlInputTextBradesco" 
				    		onfocus="loadMasks();" onblur="validarCampoDescontoTarifa(this)" disabled="#{manterSolicRastFavBean.indicadorDescontoBloqueio}"/>
						<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="%"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
				
				<br:brPanelGrid columns="1" cellpadding="0" cellspacing="0">
					<br:brPanelGroup>
							<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
							<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}: "/>
							<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
							<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.vlrTarifaAtual}" id="txtTarifaAtualizada" converter="decimalBrazillianConverter"/>
					</br:brPanelGroup>
				</br:brPanelGrid>
			</br:brPanelGrid>
		</a4j:region>
		
		<f:verbatim><hr class="lin"></f:verbatim>
		
	    <br:brPanelGrid columns="2" width="100%"  cellpadding="0" cellspacing="0">	
			<br:brPanelGroup style="text-align:left">
				<br:brCommandButton id="btnVoltar" styleClass="bto1" action="#{manterSolicRastFavBean.voltarPesquisa}" value="voltar">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="text-align:right; width: 750px;">
				<br:brCommandButton id="btnCalcular" styleClass="bto1" style="margin-right: 5px;" value="#{msgs.btn_calcular}" action="#{manterSolicRastFavBean.calcularTarifaAtualizada}"
					disabled="#{manterSolicRastFavBean.indicadorDescontoBloqueio}">
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				<br:brCommandButton id="btnAvancar" styleClass="bto1" onclick="javascript: if (!validarFiltroEstatistica('#{msgs.label_ocampo}', '#{msgs.label_necessario}')) { desbloquearTela(); return false; }" action="#{manterSolicRastFavBean.avancarInclusaoSolicRastFav}" value="avan�ar">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>

	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
	
</brArq:form>