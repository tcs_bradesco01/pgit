<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmDetSolicRastFavorecido">
	<br:brPanelGrid columns="1" width="770" style="margin-top:9">
    	
	    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Cliente:"/>
	    
	   	<br:brPanelGrid columns="2" style="margin-top: 6">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " style="margin-left: 5" />
				<br:brOutputText id="lblCnpjCpf" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " style="margin-left: 5" />
				<br:brOutputText id="lblNomeRazaoSocial" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Contrato:"/>
	    
	   	<br:brPanelGrid columns="2" style="margin-top: 6">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.dsEmpresaGestoraContrato}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato: " style="margin-left: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGrid columns="3" style="margin-top: 6">	
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato: " style="margin-left: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Descri��o do Contrato: "  />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
			</br:brPanelGroup>
			
			
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left: 20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
	    <f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
	    <f:verbatim>&nbsp;</f:verbatim>
			
	    <br:brPanelGrid columns="2">
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: "  style="margin-left: 5"/>
				<br:brOutputText id="cdSituacao" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.dsSituacaoSolicitacaoPagamento}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Motivo: "  style="margin-left: 5"/>
				<br:brOutputText id="cdMotivo" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.dsMotivoSituacaoSolicitacao}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
	    <br:brPanelGrid columns="1">
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Per�odo Rastreamento: " style="margin-left: 5"/>
				<br:brOutputText id="dtPeriodo" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.dtInicioRastreabilidadeFavorecido}  a  #{manterSolicRastFavBean.retornoDetalharSolicRastFav.dtFimRastreabilidadeFavorecido}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2">
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Servi�o: "  style="margin-left: 5"/>
				<br:brOutputText id="cdTipo" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.dsProdutoServicoOperacao}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Modalidade: "  style="margin-left: 5" />
				<br:brOutputText id="cdModalidade" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.dsProdutoOperacaoRelacionado}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3">
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.excManterSolicitacaoRastreamentoFavorecidos_banco}: "  style="margin-left: 5"/>
				<br:brOutputText id="cdBanco" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.banco}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.excManterSolicitacaoRastreamentoFavorecidos_agencia}: "  style="margin-left: 5"/>
				<br:brOutputText id="cdAgencia" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.agencia}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Conta D�bito: "  style="margin-left: 5"/>
				<br:brOutputText id="cdConta" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.digitoConta}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2">	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.excManterSolicitacaoRastreamentoFavorecidos_inclusao_automatica_favorecido}: "  style="margin-left: 5"/>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.dsIndicadorInclusaoFavorecido}" style="margin-left: 5"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2">	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_tipo_codigo_favorecido}: "  style="margin-left: 5"/>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.dsFormaCadastroFavorecidos}" style="margin-left: 5"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2">	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Destino de Arquivo Retorno: "  style="margin-left: 5"/>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.dsIdDestinoRetorno}" style="margin-left: 5"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3"  cellpadding="0" cellspacing="0" >
			<br:brPanelGroup >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="R$ " rendered="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.vlTarifaPadrao != NULL}"/>					
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.vlTarifaPadrao}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
			<br:brPanelGroup style="margin-left:50px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_desconto_tarifa}:"/>
				<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.numPercentualDescTarifa}" converter="decimalBrazillianConverter"/>
				<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="%" rendered="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.numPercentualDescTarifa != NULL}"/>
			</br:brPanelGroup>
			<br:brPanelGroup style="margin-left:50px">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="R$ " rendered="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.vlrTarifaAtual != NULL}"/>			
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.vlrTarifaAtual}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
			
		<f:verbatim><hr class="lin"></f:verbatim>
		<br:brPanelGrid columns="2" style="margin-top: 6">
			
			<br:brPanelGroup style="text-align:left;" >
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Data/Hora Inclus�o: "  style="margin-left: 5"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.dtInclusao} #{manterSolicRastFavBean.retornoDetalharSolicRastFav.hrInclusao}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detSolicitacaoRelatoriosFavorecidos_usuario}: "  style="margin-left: 5"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.cdUsuarioInclusao} #{manterSolicRastFavBean.retornoDetalharSolicRastFav.cdUsuarioInclusaoExter}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		<br:brPanelGrid columns="2">
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo Canal: " style="margin-left: 5"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.tipoCanalInclusaoFormatado}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"/>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Complemento: " style="margin-left: 5"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.cdOperCanalInclusao}"/>
			</br:brPanelGroup>	
			
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"></f:verbatim>
		<br:brPanelGrid columns="2" style="margin-top: 6">
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Data/Hora Manuten��o: "  style="margin-left: 5"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.dtManutencao} #{manterSolicRastFavBean.retornoDetalharSolicRastFav.hrManutencao}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.detSolicitacaoRelatoriosFavorecidos_usuario}: "  style="margin-left: 5"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.cdUsuarioManutencao} #{manterSolicRastFavBean.retornoDetalharSolicRastFav.cdUsuarioManutencaoExter}"/>
			</br:brPanelGroup>	
		</br:brPanelGrid>	
		<br:brPanelGrid columns="2">
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo Canal: " style="margin-left: 5"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.tipoCanalManutencaoFormatado}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Complemento: " style="margin-left: 5"/>
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoDetalharSolicRastFav.cdOperCanalManutencao}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"></f:verbatim>
		
	    <br:brPanelGrid columns="2" width="100%"  cellpadding="0" cellspacing="0">	
			<br:brPanelGroup style="text-align:left">
				<br:brCommandButton id="btnVoltar" styleClass="bto1" action="#{manterSolicRastFavBean.voltarPesquisa}"  value="voltar">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
	</br:brPanelGrid>
</brArq:form>