<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmIncSolicRastFavorecido">
	<br:brPanelGrid columns="1" width="770" style="margin-top:9">
	    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Cliente:"/>
	    
	   	<br:brPanelGrid columns="2" style="margin-top: 6">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ:" style="margin-left: 5" />
				<br:brOutputText id="lblCnpjCpf" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social:" style="margin-left: 5" />
				<br:brOutputText id="lblNomeRazaoSocial" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
			</br:brPanelGroup>
	    </br:brPanelGrid>
	    
	    <f:verbatim><hr class="lin"></f:verbatim>
	    
		    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Contrato:"/>
	    
	   	<br:brPanelGrid columns="2" style="margin-top: 6">
	   		<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.dsEmpresaGestoraContrato}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato: " style="margin-left: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>	
		<br:brPanelGrid columns="3" style="margin-top: 6">	
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato: " style="margin-left: 2" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Descri��o do Contrato: "  />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
			</br:brPanelGroup>
			
			<br:brPanelGroup>			
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left: 20px"/>	
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: "  />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>

	    <f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
	    
	    <br:brPanelGrid columns="1">
			<f:verbatim>&nbsp;</f:verbatim>
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Per�odo Rastreamento:" style="margin-left: 5"/>
				<br:brOutputText id="dtPeriodo" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.retornoIncluirSolicRastFav.dtInicioRastreamento} a #{manterSolicRastFavBean.retornoIncluirSolicRastFav.dtFimRastreamento}" />
			</br:brPanelGroup>	
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="1">
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Servi�o:" style="margin-left: 5"/>
			</br:brPanelGroup>	
			
			<br:brPanelGrid columns="2" width="100%">
				<br:brPanelGroup style="text-align:left;">
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Servi�o:"  style="margin-left: 5"/>
					<br:brOutputText id="cdTipo" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.tipoServicoDesc}"/>
				</br:brPanelGroup>
				
				<br:brPanelGroup style="text-align:left;">
					<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Modalidade:"  style="margin-left:20px"/>
					<br:brOutputText id="cdModalidade" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.tipoModalidadeDesc}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3">
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.excManterSolicitacaoRastreamentoFavorecidos_banco}:"  style="margin-left: 5"/>
				<br:brOutputText id="cdBanco" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.banco}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.excManterSolicitacaoRastreamentoFavorecidos_agencia}:"  style="margin-left: 5"/>
				<br:brOutputText id="cdAgencia" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.agencia}"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  style="margin-left:20px"/>
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Conta D�bito:"  style="margin-left: 5"/>
				<br:brOutputText id="cdConta" styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.contaDigitoAtualizado}"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2">	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.excManterSolicitacaoRastreamentoFavorecidos_inclusao_automatica_favorecido}:"  style="margin-left: 5"/>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.dsInclusaoAuto}" style="margin-left: 5"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2">	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="#{msgs.label_tipo_codigo_favorecido}:"  style="margin-left: 5"/>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.dsInclusaoFavorecido}" style="margin-left: 5"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="2">	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Destino de Arquivo Retorno:"  style="margin-left: 5"/>
					<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.dsDestinoRetorno}" style="margin-left: 5"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"> </f:verbatim>
		
		<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
			<br:brPanelGroup>
				<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_tarifa}:"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
	
		<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<br:brPanelGrid columns="3" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_tarifa_padrao}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.vlTarifaPadrao}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>

			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_desconto_tarifa}:"/>
				<br:brOutputText styleClass="HtmlOutputFormatBoldBradesco" value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.numPercentualDescTarifa}" converter="decimalBrazillianConverter"/>
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="%" />
			</br:brPanelGroup>
	
			<br:brPanelGroup>
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRotuloBradesco" value="#{msgs.label_valor_tarifa_atualizada}:"/>
				<br:brOutputText styleClass="HtmlOutputTextBulletBoldBradesco" value="R$ " />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{manterSolicRastFavBean.entradaIncluirSolicRastFav.vlrTarifaAtual}" converter="decimalBrazillianConverter"/>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"></f:verbatim>
		
		<br:brPanelGrid columns="2" width="100%"  cellpadding="0" cellspacing="0">	
			<br:brPanelGroup style="text-align:left">
				<br:brCommandButton id="btnVoltar" styleClass="bto1" action="incManterSolicitacaoRastreamentoFavorecidos"  value="voltar">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
			
			<br:brPanelGroup style="text-align:right; width: 750px">
				<br:brCommandButton id="btnConfirmar" styleClass="bto1" action="#{manterSolicRastFavBean.confirmarInclusaoSolicRastFav}" value="confirmar" onclick="javascript: if (!confirm('Confirma Inclus�o?')) { desbloquearTela(); return false; }">
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>		
		
	</br:brPanelGrid>
	<a4j:status id="statusAguarde" onstart="bloquearTela()" onstop="desbloquearTela()" />
</brArq:form>