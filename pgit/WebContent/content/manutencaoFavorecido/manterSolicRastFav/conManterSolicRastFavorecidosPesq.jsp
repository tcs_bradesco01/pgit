<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmManterSolicRastFavorecido">
	<br:brPanelGrid columns="1" width="770" style="margin-top:9">
    
 	<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Identifica��o do Cliente:"/>
 	
 	<a4j:region id="regionFiltro">
	<t:selectOneRadio id="rdoFiltroCliente" value="#{manterSolicRastFavBean.itemFiltroSelecionado}" disabled="#{manterSolicRastFavBean.bloqueaRadio}" onclick="submit();" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
		<f:selectItem itemValue="0" itemLabel="" />
		<f:selectItem itemValue="1" itemLabel="" />
		<f:selectItem itemValue="2" itemLabel="" />
		<f:selectItem itemValue="3" itemLabel="" />
	</t:selectOneRadio>
	</a4j:region>
	
    <br:brPanelGrid columns="2">
		<t:radio for=":frmManterSolicRastFavorecido:rdoFiltroCliente" index="0" />			
		
		 <a4j:outputPanel id="panelCnpj" ajaxRendered="true">	
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="CNPJ:" />
			</br:brPanelGrid>	
			
		   <br:brPanelGrid columns="5">					
			    <br:brInputText id="txtCnpj" converter="javax.faces.Long" onkeypress="onlyNum();" size="11" maxlength="9" disabled="#{manterSolicRastFavBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterSolicRastFavBean.entradaConsultarListaClientePessoas.cdCpfCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
  			 	<f:verbatim>&nbsp;</f:verbatim>
			    <br:brInputText id="txtFilial" converter="javax.faces.Integer" onkeypress="onlyNum();" size="5" maxlength="4" disabled="#{manterSolicRastFavBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterSolicRastFavBean.entradaConsultarListaClientePessoas.cdFilialCnpj}"  onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
	   		 	<f:verbatim>&nbsp;</f:verbatim>
				<br:brInputText id="txtControle" converter="javax.faces.Integer" onkeypress="onlyNum();" size="3" maxlength="2" disabled="#{manterSolicRastFavBean.itemFiltroSelecionado != '0'}" styleClass="HtmlInputTextBradesco" value="#{manterSolicRastFavBean.entradaConsultarListaClientePessoas.cdControleCnpj}" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</br:brPanelGrid>
		</a4j:outputPanel>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" >
		<t:radio for=":frmManterSolicRastFavorecido:rdoFiltroCliente" index="1" />			
		
		 <a4j:outputPanel id="panelCpf" ajaxRendered="true">
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="CPF:" />
			</br:brPanelGrid>	
			
		   <br:brPanelGrid columns="3"  >					
			    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" disabled="#{manterSolicRastFavBean.itemFiltroSelecionado != '1'}" value="#{manterSolicRastFavBean.entradaConsultarListaClientePessoas.cdCpf}" size="11" maxlength="9" id="txtCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
  			 	<f:verbatim>&nbsp;</f:verbatim>
			    <br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Integer" onkeypress="onlyNum();" disabled="#{manterSolicRastFavBean.itemFiltroSelecionado != '1'}" value="#{manterSolicRastFavBean.entradaConsultarListaClientePessoas.cdControleCpf}" maxlength="2" size="3" id="txtControleCpf" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
			</br:brPanelGrid>
		</a4j:outputPanel>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="2" >
		<t:radio for=":frmManterSolicRastFavorecido:rdoFiltroCliente" index="2" />		
		
		 <a4j:outputPanel id="panelNomeRazao" ajaxRendered="true">
			<br:brPanelGrid columns="2" style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Nome/Raz�o Social:"/>
			</br:brPanelGrid>	
			
		  	<br:brInputText styleClass="HtmlInputTextBradesco" disabled="#{manterSolicRastFavBean.itemFiltroSelecionado != '2'}" value="#{manterSolicRastFavBean.entradaConsultarListaClientePessoas.dsNomeRazaoSocial}" size="71" maxlength="70" id="txtNomeRazaoSocial"/>
		</a4j:outputPanel>
	</br:brPanelGrid>
    
     <br:brPanelGrid columns="4" >
		<t:radio for="rdoFiltroCliente" index="3" />			
		
		<a4j:outputPanel id="panelBanco" ajaxRendered="true">
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Banco:" />
			</br:brPanelGrid>	
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterSolicRastFavBean.itemFiltroSelecionado != '3'}" value="#{manterSolicRastFavBean.entradaConsultarListaClientePessoas.cdBanco}" size="4" maxlength="3" id="txtBanco" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelAgencia" ajaxRendered="true">
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Ag�ncia: " />
			</br:brPanelGrid>	
			
			<br:brInputText styleClass="HtmlInputTextBradesco" onkeypress="onlyNum();" converter="javax.faces.Integer" disabled="#{manterSolicRastFavBean.itemFiltroSelecionado != '3'}" value="#{manterSolicRastFavBean.entradaConsultarListaClientePessoas.cdAgenciaBancaria}" size="6" maxlength="5" id="txtAgencia" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
		</a4j:outputPanel>
		
		<a4j:outputPanel id="panelConta" ajaxRendered="true">
			<br:brPanelGrid columns="2">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Conta: " />
			</br:brPanelGrid>	
			
			<br:brInputText styleClass="HtmlInputTextBradesco" converter="javax.faces.Long"  onkeypress="onlyNum();" disabled="#{manterSolicRastFavBean.itemFiltroSelecionado != '3'}" value="#{manterSolicRastFavBean.entradaConsultarListaClientePessoas.cdContaBancaria}" size="17" maxlength="13" id="txtConta" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
		</a4j:outputPanel>
    </br:brPanelGrid>
    
   	<f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<a4j:outputPanel id="panelBtoConsultarCliente" ajaxRendered="true" style="width:100%; text-align: right">
	   		<br:brCommandButton id="btoConsultarCliente" styleClass="bto1" disabled="#{empty manterSolicRastFavBean.itemFiltroSelecionado}" value="consultar cliente" action="#{manterSolicRastFavBean.consultarClientes}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" onclick="javascript:desbloquearTela(); return validaCpoConsultaCliente(document.forms[1]);">		
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</a4j:outputPanel>
	</br:brPanelGrid>
    
   <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Cliente:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 5">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="CPF/CNPJ: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco"  value="#{manterSolicRastFavBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Nome/Raz�o Social: " />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaClientePessoas.dsNomeRazao}"/>
		</br:brPanelGroup>
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Identifica��o do Contrato:"/>
	
	<br:brPanelGrid columns="5" id="painelDetalharContrato" style="margin-top: 5">
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2"/>
		</br:brPanelGroup>	
		
		<f:verbatim>&nbsp;</f:verbatim>
		
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Tipo de Contrato: " style="margin-left: 2"/>
		</br:brPanelGroup>	
				
		<f:verbatim>&nbsp;</f:verbatim>		
		
		<br:brPanelGroup style="text-align:left;">
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
			<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="N�mero do Contrato: " style="margin-left: 2"/>
		</br:brPanelGroup>	
				
		<br:brSelectOneMenu disabled="#{manterSolicRastFavBean.habilitaCamposAposConsContrato}" id="selEmpresaGestoraContrada" converter="longConverter" value="#{manterSolicRastFavBean.entradaConsultarListaContratosPessoas.cdPessoaJuridicaContrato}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
			<f:selectItems  value="#{manterSolicRastFavBean.preencherEmpresaGestora}" />
		</br:brSelectOneMenu>
		<f:verbatim>&nbsp;</f:verbatim>
	
		<br:brSelectOneMenu disabled="#{manterSolicRastFavBean.habilitaCamposAposConsContrato}" id="selTipoContrato" converter="inteiroConverter" value="#{manterSolicRastFavBean.entradaConsultarListaContratosPessoas.cdTipoContratoNegocio}" styleClass="HtmlSelectOneMenuBradesco" style="width: 250">
			<f:selectItems  value="#{manterSolicRastFavBean.preencherTipoContrato}" />
		</br:brSelectOneMenu>
		<f:verbatim>&nbsp;</f:verbatim>
		
		<br:brInputText disabled="#{manterSolicRastFavBean.habilitaCamposAposConsContrato}" styleClass="HtmlInputTextBradesco" converter="javax.faces.Long" onkeypress="onlyNum();" value="#{manterSolicRastFavBean.entradaConsultarListaContratosPessoas.nrSeqContratoNegocio}" size="13" maxlength="10" id="txtNumeroContrato" onblur="validaCampoNumerico(this, '#{msgs.label_campo_contem_apenas_numeros}');"/>
	</br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0">
    	<br:brPanelGroup style="width:100%; text-align: right">
	   		<br:brCommandButton id="btoConsultarContrato" styleClass="bto1" value="consultar contrato" action="#{manterSolicRastFavBean.consultarIdentificacaoContratos}" style="cursor:hand;" 
	   		onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" 
	   		onclick="javascript: desbloquearTela(); return validaCpoContratoConsulta(document.forms[1], '#{msgs.label_ocampo}', '#{msgs.label_necessario}',
	   		'#{msgs.conDuplicacaoArquivoRetorno_empresa_gestora_contrato}','#{msgs.conDuplicacaoArquivoRetorno_tipo_contrato}', '#{msgs.conDuplicacaoArquivoRetorno_numero_contrato}', 
	   		'#{manterSolicRastFavBean.saidaConsultarListaClientePessoas.cnpjOuCpfFormatado}');" >
	   			<a4j:support event="onclick" reRender="painelDetalharContrato, rdoFiltroCliente, panelCnpj, panelCpf, panelNomeRazao, panelBanco, panelAgencia, panelConta, btnLimparCampos"/>
				<brArq:submitCheckClient />
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>
      
    <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="Contrato:"/>
    
   	<br:brPanelGrid columns="2" style="margin-top: 6">
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Empresa Gestora do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.dsEmpresaGestoraContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Tipo de Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsTipoContratoNegocio}"/>
		</br:brPanelGroup>
		
		
	</br:brPanelGrid>
		
	<br:brPanelGrid columns="3" style="margin-top: 6">
	
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="N�mero do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.nrSeqContratoNegocio}" />
		</br:brPanelGroup>
		
   		<br:brPanelGroup>
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Descri��o do Contrato: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsContrato}"/>
		</br:brPanelGroup>
		
		<br:brPanelGroup>			
			<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" style="margin-left:20px"  />	
			<br:brOutputText styleClass="HtmlOutputTextBradesco" value="Situa��o: " style="margin-left: 2" />
			<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="#{manterSolicRastFavBean.saidaConsultarListaContratosPessoas.dsSituacaoContratoNegocio}"/>
		</br:brPanelGroup>
		
		
				
    </br:brPanelGrid>
    
    <f:verbatim><hr class="lin"></f:verbatim>
		
        <br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="ARGUMENTOS DE PESQUISA:"/>
	    
	    <br:brPanelGrid columns="2" width="80%" id="painelArgumentosPesquisa">
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="Situa��o: " style="margin-left: 5"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup style="text-align:left;">
				<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				<br:brOutputText styleClass="HtmlOutputTextBoldBradesco" value="Per�odo Solicita��o: " style="margin-left: 5"/>
			</br:brPanelGroup>	
			
			<br:brPanelGroup>	
				<br:brSelectOneMenu id="selSituacaoFavorecido" value="#{manterSolicRastFavBean.entradaManterSolicRastFavorecido.cdSituacaoSolicitacaoPagamento}" disabled="#{manterSolicRastFavBean.desabilataFiltro}" styleClass="HtmlSelectOneMenuBradesco" style="width: 200">
					<f:selectItem itemValue="0" itemLabel="Selecione"/>
					<f:selectItems value="#{manterSolicRastFavBean.listaSolicitacao}" />
				</br:brSelectOneMenu>
			</br:brPanelGroup>	
			
		<a4j:outputPanel id="calendarios" style="width: 100%; text-align: left" ajaxRendered="true" onmousedown="javascript: cleanClipboard();" >							
			<br:brPanelGroup rendered="#{manterSolicRastFavBean.desabilataFiltro == false}" >	
				<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco" value="De" style="margin-right: 10"/>
		   			<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoInicio');" id="txtPeriodoInicio" value="#{manterSolicRastFavBean.entradaManterSolicRastFavorecido.dtInicioRastreabilidade}" disabled="#{manterSolicRastFavBean.desabilataFiltro}">
						<f:converter converterId="dateBrazillianConverter"/>
		 	 		</app:calendar>
		  		<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco" value="a" style="margin-left: 10;margin-right: 10"/>
			   		<app:calendar oninputkeydown="javascript: cleanClipboard();" oninputkeypress="onlyNum();" oninputchange="recuperarDataCalendario(document.forms[1],'txtPeriodoFim');" id="txtPeriodoFim" value="#{manterSolicRastFavBean.entradaManterSolicRastFavorecido.dtFimRastreabilidade}" disabled="#{manterSolicRastFavBean.desabilataFiltro}">
						<f:converter converterId="dateBrazillianConverter"/>
		 		 	</app:calendar>
			</br:brPanelGroup>
			
			<br:brPanelGroup rendered="#{manterSolicRastFavBean.desabilataFiltro}" >	
				<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco" value="De" style="margin-right: 10"/>
		   			<app:calendar id="txtPeriodoInicioD" disabled="true" value="#{manterSolicRastFavBean.entradaManterSolicRastFavorecido.dtInicioRastreabilidade}"/>
		  		<br:brOutputTextBold styleClass="HtmlOutputTextBoldBradesco" value="a" style="margin-left: 10;margin-right: 10"/>
			   		<app:calendar id="txtPeriodoFimD" disabled="true" value="#{manterSolicRastFavBean.entradaManterSolicRastFavorecido.dtFimRastreabilidade}"/>
			</br:brPanelGroup>
		</a4j:outputPanel>	
			
		</br:brPanelGrid>
		
		<f:verbatim><hr class="lin"></f:verbatim>
	    
	    <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
			<br:brPanelGroup>
				<br:brCommandButton id="btnLimparCampos" disabled="#{manterSolicRastFavBean.limpaCamp}"  action="#{manterSolicRastFavBean.limparCampos}" styleClass="bto1" value="Limpar Campos">
					<a4j:support event="onclick" reRender="painelDetalharContrato, btnLimparCampos"/>
					<brArq:submitCheckClient />
				</br:brCommandButton>
				<f:verbatim>&nbsp;</f:verbatim>
				<br:brCommandButton id="btnConsultar" styleClass="bto1" disabled="#{manterSolicRastFavBean.desabilataFiltro}"  action="#{manterSolicRastFavBean.consultarSolicitacao}" value="Consultar">
					<a4j:support event="onclick" reRender="painelArgumentosPesquisa, btnConsultar, btnLimparCampos"/>
					<brArq:submitCheckClient />
				</br:brCommandButton>
			</br:brPanelGroup>
		</br:brPanelGrid>
		
		<a4j:region id="regionRadioSolicitacao">
			<t:selectOneRadio value="#{manterSolicRastFavBean.itemSelecionadoListaSolicitacao}" id="rdoSolicRastFav" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
				<f:selectItems value="#{manterSolicRastFavBean.selectItemConSolicRastFav}"/>
				<a4j:support event="onclick" reRender="panelBotoes" limitToList="true" ajaxSingle="true" />
			</t:selectOneRadio>
		</a4j:region>
		
		<br:brPanelGrid columns="1" align="left" width="100%">
		    <app:scrollableDataTable id="tableSolicitacao" value="#{manterSolicRastFavBean.listManterSolicRastFav}" width="100%" height="180" rowIndexVar="solcRastFavorecidoKey" var="listConsultaSituacao" rows="10" rowClasses="tabela_celula_normal, tabela_celula_destaque">
			  <app:scrollableColumn width="30px" styleClass="colTabCenter">
				  <f:facet name="header">
				 	<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			   	  </f:facet>
			   	  <t:radio for=":frmManterSolicRastFavorecido:rdoSolicRastFav" index="#{solcRastFavorecidoKey}" />	
	      	  </app:scrollableColumn>
			  <app:scrollableColumn width="200" styleClass="colTabCenter">
				<f:facet name="header">
				  <br:brOutputText value="N�mero Solicita��o" styleClass="tableFontStyle" style="width:200; text-align:center"/>
				</f:facet>
				<br:brOutputText value="#{listConsultaSituacao.nrSolicitacaoPagamento}" style="width:200;" />
			  </app:scrollableColumn>
			  <app:scrollableColumn width="230" styleClass="colTabCenter">
			    <f:facet name="header">
			      <br:brOutputText value="Data/Hora Solicita��o" styleClass="tableFontStyle" style="width:220; text-align:center"/>
			    </f:facet>
			    <br:brOutputText value="#{listConsultaSituacao.dtSolicitacaoFormatado} - #{listConsultaSituacao.hrSolicitacao}" style="width:220" />
			  </app:scrollableColumn>
			  <app:scrollableColumn width="320" styleClass="colTabLeftcolTabCenter">
				<f:facet name="header">
				  <br:brOutputText value="Situa��o" styleClass="tableFontStyle" style="width:220; text-align:center"/>
				</f:facet>
				<br:brOutputText value="#{listConsultaSituacao.dsSituacao}" style="width:220;" />
			  </app:scrollableColumn>
			</app:scrollableDataTable>
			
		  <br:brPanelGroup style="text-align: center; width: 100%">
		<brArq:pdcDataScroller id="dtFavorecido" for="tableSolicitacao" rendered="#{manterSolicRastFavBean.listManterSolicRastFavEmpty == false}" actionListener="#{manterSolicRastFavBean.submit}">
			<f:facet name="first">
			    <brArq:pdcCommandButton id="primeira"
			      styleClass="bto1"
			      value="#{msgs.label_primeira}" title="#{msgs.label_primeira}"/>
			  </f:facet>
			  <f:facet name="fastrewind">
			    <brArq:pdcCommandButton id="retrocessoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}"/>
			  </f:facet>
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			  </f:facet>
			  <f:facet name="fastforward">
			    <brArq:pdcCommandButton id="avancoRapido"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_avanco}" title="#{msgs.label_avanco}"/>
			  </f:facet>
			  <f:facet name="last">
			    <brArq:pdcCommandButton id="ultima"
			      styleClass="bto1" style="margin-left: 3px;"
			      value="#{msgs.label_ultima}" title="#{msgs.label_ultima}"/>
			  </f:facet>
		</brArq:pdcDataScroller>
    </br:brPanelGroup>	
		  
			<f:verbatim><HR class="lin"></f:verbatim>
			
			<a4j:outputPanel id="panelBotoes" style="width: 100%; text-align: right" ajaxRendered="true">
				<br:brCommandButton id="btoLimpar" styleClass="bto1" value="Limpar" disabled="#{manterSolicRastFavBean.habilitaLimparTabela}" action="#{manterSolicRastFavBean.limparPagina}" style="cursor:hand; margin-left: 5;" onmouseout="javascript:alteraBotao('normal', this.id);">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
				
				<br:brCommandButton id="btoDetalhar" styleClass="bto1" value="Detalhar" disabled="#{empty manterSolicRastFavBean.itemSelecionadoListaSolicitacao}" action="#{manterSolicRastFavBean.detalharSolicRastFav}" style="cursor:hand; margin-left: 5" onmouseout="javascript:alteraBotao('normal', this.id);">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>	
				
				<br:brCommandButton id="btoIncluir" styleClass="bto1" value="Incluir" disabled="#{manterSolicRastFavBean.desabilataFiltro}" action="#{manterSolicRastFavBean.incluirSolicRastFav}" style="cursor:hand; margin-left: 5" onmouseout="javascript:alteraBotao('normal', this.id);">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>
			
				<br:brCommandButton id="btoExcluir" styleClass="bto1" value="Excluir" disabled="#{empty manterSolicRastFavBean.itemSelecionadoListaSolicitacao}" action="#{manterSolicRastFavBean.excluirSolicRastFav}" style="cursor:hand; margin-left: 5" onmouseout="javascript:alteraBotao('normal', this.id);">	
					<brArq:submitCheckClient/>
				</br:brCommandButton>	
			 </a4j:outputPanel>

		</br:brPanelGrid>		
    
    	<t:inputHidden value="#{manterSolicRastFavBean.bloqueaRadio}" id="hiddenFlagPesquisa"></t:inputHidden>
	
	<f:verbatim>
	  	<script language="javascript">
	  		validarProxCampoIdentClienteContratoFlag(document.forms[1], document.getElementById('frmManterSolicRastFavorecido:hiddenFlagPesquisa').value);
	  	</script>
	</f:verbatim>
	</br:brPanelGrid>
</brArq:form>

