<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>

<br:brDataTable id="tabela" var="extratoVar" value="#{extratoBean.listaExtrato}" width="100%"
				styleClass="standardTable" headerClass="standardTable_Header" footerClass="standardTable_Header"
                rowClasses="standardTable_Row1,standardTable_Row2" columnClasses="standardTable_Column">
	<br:brColumn>
		<f:facet name="header">
			<br:brOutputTextBold value="#{msgs.label_header_data}"/>
		</f:facet>
		<br:brOutputText value="#{extratoVar.data}"/>
	</br:brColumn>
	<br:brColumn>
		<f:facet name="header">
			<br:brOutputTextBold value="#{msgs.label_header_historico}"/>
		</f:facet>
		<br:brOutputText value="#{extratoVar.historico}"/>
	</br:brColumn>
	<br:brColumn>
		<f:facet name="header">
			<br:brOutputTextBold value="#{msgs.label_header_docto}"/>
		</f:facet>
		<br:brOutputText value="#{extratoVar.documento}"/>
	</br:brColumn>
	<br:brColumn>
		<f:facet name="header">
			<br:brOutputTextBold value="#{msgs.label_header_cpmf}"/>
		</f:facet>
		<br:brOutputText value="#{extratoVar.cpmf}">
			<f:convertNumber type="number" maxFractionDigits="2" maxIntegerDigits="7"/>
		</br:brOutputText>
	</br:brColumn>	
	<br:brColumn>
		<f:facet name="header">
			<br:brOutputTextBold value="#{msgs.label_header_valor}"/>
		</f:facet>
		<br:brOutputText value="#{extratoVar.valor}">
			<f:convertNumber type="number" maxFractionDigits="2" maxIntegerDigits="15"/>
		</br:brOutputText>
	</br:brColumn>
</br:brDataTable>
<h:form id="extratoForm">
	<br:brPanelGrid columns="1">
		<br:brPanelGroup>
			<br:brOutputTextColor value="#{msgs.label_month_choice}"/>
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brSelectOneMenu id="mesSelected" value="#{extratoBean.mesSelecionado}" onchange="submit();">
				<f:selectItem itemLabel="#{msgs.label_mes_1}" itemValue="1"/>
				<f:selectItem itemLabel="#{msgs.label_mes_2}" itemValue="2"/>
				<f:selectItem itemLabel="#{msgs.label_mes_3}" itemValue="3"/>
				<f:selectItem itemLabel="#{msgs.label_mes_4}" itemValue="4"/>
				<f:selectItem itemLabel="#{msgs.label_mes_5}" itemValue="5"/>
				<f:selectItem itemLabel="#{msgs.label_mes_6}" itemValue="6"/>
				<f:selectItem itemLabel="#{msgs.label_mes_7}" itemValue="7"/>
				<f:selectItem itemLabel="#{msgs.label_mes_8}" itemValue="8"/>
				<f:selectItem itemLabel="#{msgs.label_mes_9}" itemValue="9"/>
				<f:selectItem itemLabel="#{msgs.label_mes_10}" itemValue="10"/>
				<f:selectItem itemLabel="#{msgs.label_mes_11}" itemValue="11"/>
				<f:selectItem itemLabel="#{msgs.label_mes_12}" itemValue="12"/>
			</br:brSelectOneMenu>
		</br:brPanelGroup>
	</br:brPanelGrid>
</h:form>