<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmIdentificacaoContrato">

<br:brPanelGrid columns="1" width="100%" style="text-align:center; margin-top:9">

	<a4j:region id="regionFiltroPesquisa">
		<t:selectOneRadio id="rdoFavorecidos" value="#{manterFavorecidoBean.selectItemContrato}" converter="javax.faces.Integer" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
			<f:selectItems value="#{manterFavorecidoBean.selectItemContratos}"/>
			<a4j:support event="onclick" reRender="panelBotoes" limitToList="true" ajaxSingle="true" />
		</t:selectOneRadio>
	</a4j:region>

	<t:div style="width: 770; overflow-x:scroll">
	<app:scrollableDataTable id="tableContrato" value="#{manterFavorecidoBean.listaConsultarListaContratosPessoas}" width="1310" rowIndexVar="parametroKey" var="result" rows="10">
			
		<app:scrollableColumn width="20" styleClass="colTabCenter">
			<f:facet name="header">
				<br:brOutputText value=" " styleClass="tableFontStyle" style="width:25; text-align:center" />
			</f:facet>		
     	  	<t:radio for="frmIdentificacaoContrato:rdoFavorecidos" index="#{parametroKey}" />	
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="210" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Empresa Gestora do Contrato" styleClass="tableFontStyle"/>
			</f:facet>
			<br:brOutputText value="#{result.dsPessoaJuridicaContrato}" />
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="210" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Tipo de Contrato" styleClass="tableFontStyle" />
			</f:facet>
			<br:brOutputText value="#{result.dsTipoContratoNegocio}" />
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="210" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="N�mero Seq. Contrato" styleClass="tableFontStyle" />
			</f:facet>
			<br:brOutputText value="#{result.nrSeqContratoNegocio}" />
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="300" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.identificacaoClienteContrato_club_representante}" styleClass="tableFontStyle" style="width:300; text-align:center"/>
			</f:facet>
			<br:brOutputText value="#{result.cdPessoaParticipante}" />
		</app:scrollableColumn>
		
		<app:scrollableColumn width="220" styleClass="colTabRight">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.identificacaoClienteContrato_cpf_cnpj_representante}" styleClass="tableFontStyle" style="width:220; text-align:center"/>
			</f:facet>
			<br:brOutputText value="#{result.cnpjOuCpfFormatado}" />
		</app:scrollableColumn>	
		
		<app:scrollableColumn width="300" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.identificacaoClienteContrato_nome_razao_social}" styleClass="tableFontStyle" style="width:300; text-align:center"/>
			</f:facet>
			<br:brOutputText value="#{result.dsPessoaParticipante}" />
		</app:scrollableColumn>		
		
		<app:scrollableColumn width="300" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="#{msgs.identificacaoClienteContratoAgendamento_grid_tipo_participacao_contrato}" styleClass="tableFontStyle" style="width:300; text-align:center"/>
			</f:facet>
			<br:brOutputText value="#{result.dsTipoParticipacaoPessoa}" />
		</app:scrollableColumn>	

	</app:scrollableDataTable>
	</t:div>
	
	<br:brPanelGroup style="text-align: center; width: 100%">
		<brArq:pdcDataScroller  id="dataScroller" for="tableContrato" actionListener="#{manterFavorecidoBean.submitCotratos}">
				<f:facet name="first">
				  <brArq:pdcCommandButton id="primeira"
				    styleClass="HtmlCommandButtonBradesco"
				    value="#{msgs.label_primeira}" title="#{msgs.label_primeira}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastrewind">
				  <brArq:pdcCommandButton id="retrocessoRapido"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_retrocesso}" title="#{msgs.label_retrocesso}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="previous">
				  <brArq:pdcCommandButton id="anterior"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_anterior}" title="#{msgs.label_anterior}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="next">
				  <brArq:pdcCommandButton id="proxima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_proxima}" title="#{msgs.label_proxima}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="fastforward">
				   <brArq:pdcCommandButton id="avancoRapido"
				     styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				     value="#{msgs.label_avanco}" title="#{msgs.label_avanco}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
				<f:facet name="last">
				  <brArq:pdcCommandButton id="ultima"
				    styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
				    value="#{msgs.label_ultima}" title="#{msgs.label_ultima}" styleClass="bto1" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);" />
				</f:facet>
		</brArq:pdcDataScroller>
	</br:brPanelGroup>		
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
		<br:brCommandButton id="btnSelecionar" styleClass="bto1" value="Selecionar" value="#{manterFavorecidoBean.selecionarContrato}" style="cursor:hand;" onmouseover="javascript:alteraBotao('visualizacao', this.id);" onmouseout="javascript:alteraBotao('normal', this.id);">
			<brArq:submitCheckClient/>
		</br:brCommandButton>
	</br:brPanelGrid>
	
</br:brPanelGrid>
</brArq:form>