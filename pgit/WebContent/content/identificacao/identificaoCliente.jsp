<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="frmIdentificacaoCliente">

<br:brPanelGrid columns="1" width="100%" style="text-align:center; margin-top:9">

	<t:selectOneRadio id="rdoFavorecidos" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" >
		<%--  <f:selectItems value="#{cmpi0001Bean.listaControleRemessa}"/> --%>
		<f:selectItem itemValue="nome"       itemLabel=""/>  
	</t:selectOneRadio>
					   
	<app:scrollableDataTable id="tableFavorecido" value="#{conCadastramentoBloqueioBean.listaCadastramentoBloqueio}" width="770" rowIndexVar="parametroKey" var="listConsultaSituacao" rows="10">
			
		<app:scrollableColumn width="20" styleClass="colTabCenter">
			<f:facet name="header">
				&nbsp;
			</f:facet>
     	  	<t:radio for="rdoFavorecidos" index="0" />	
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="110" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Club" styleClass="tableFontStyle"/>
			</f:facet>
			<br:brOutputText value="" />
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="210" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="CPF/CNPJ" styleClass="tableFontStyle" />
			</f:facet>
			<br:brOutputText value="" />
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="210" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Nome/Raz�o Social" styleClass="tableFontStyle" />
			</f:facet>
			<br:brOutputText value="" />
		</app:scrollableColumn>
			  
		<app:scrollableColumn width="220" styleClass="colTabLeft">
			<f:facet name="header">
				<br:brOutputText value="Data Nascimento/Funda��o" styleClass="tableFontStyle" />
			</f:facet>
			<br:brOutputText value="" />
		</app:scrollableColumn>
	</app:scrollableDataTable>

	<br:brPanelGroup style="text-align: center; width: 100%">
		<brArq:pdcDataScroller id="dtFavorecido" for="tableFavorecido" actionListener="">
			<f:facet name="previous">
				<brArq:pdcCommandButton id="anterior" styleClass="bto1"  style="margin-left: 3px;" value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			</f:facet>
			<f:facet name="next">
				<brArq:pdcCommandButton id="proxima" styleClass="bto1" value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			</f:facet>
		</brArq:pdcDataScroller>
    </br:brPanelGroup>		
    
    <f:verbatim><hr class="lin"></f:verbatim>
    
    <br:brPanelGrid columns="1" width="100%"  style="text-align:right" cellpadding="0" cellspacing="0">	
		<br:brCommandButton id="btnSelecionar" action="#{identificacaoClienteBean.selecionarCliente}" styleClass="bto1" value="Selecionar">
			<brArq:submitCheckClient/>
		</br:brCommandButton>
	</br:brPanelGrid>
	
</br:brPanelGrid>
</brArq:form>