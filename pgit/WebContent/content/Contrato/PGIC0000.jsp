<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>

<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="identificacaoContratoForm" name="identificacaoContratoForm" >
<h:inputHidden id="hiddenRadioContrato" value="#{pgic0000Bean.hiddenRadioContrato}"/>
<h:inputHidden id="hiddenRadioFiltroArgumento" value="#{pgic0000Bean.hiddenRadioFiltroArgumento}"/>

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:11px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio onclick="javascript:habilitarCamposFiltroContrato(document.forms[1], this);" id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<f:selectItem itemValue="3" itemLabel="" />
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
    <br:brPanelGrid columns="6" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="0" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    <br:brSelectOneMenu id="empresa" value="#{pgic0000Bean.empresaFiltro}" disabled="true">
					<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0000_label_combo_selecione}"/>
					<f:selectItems value="#{pgic0000Bean.listaEmpresa}"/>
				</br:brSelectOneMenu>

				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brSelectOneMenu id="tipo" value="#{pgic0000Bean.tipoFiltro}" disabled="true">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0000_label_combo_selecione}"/>
						<f:selectItems value="#{pgic0000Bean.listaTipo}"/>
					</br:brSelectOneMenu>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0000_label_numero}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{pgic0000Bean.numeroFiltro}" size="13" maxlength="10" id="numero" disabled="true">
						<brArq:commonsValidator type="integer" arg="#{msgs.label_numero}" server="false" client="true" />
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="1" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0000_label_agencia_operadora}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{pgic0000Bean.agenciaOperadoraFiltro}" size="20"  id="txtAgenciaOperadora" disabled="true"/>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0000_label_gerente_responsavel}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brSelectOneMenu id="gerenteResponsavel" value="#{pgic0000Bean.gerenteResponsavelFiltro}" disabled="true">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0000_label_combo_selecione}"/>
						<f:selectItems value="#{pgic0000Bean.listaGerenteResponsavel}"/>
					</br:brSelectOneMenu>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="2" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				</br:brPanelGroup>	
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0000_label_data_cadastramento}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>				
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		    
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>		    					

				<br:brPanelGroup>
					<app:calendar id="calendarioData" value="#{pgic0000Bean.dataCadastramentoFiltro}" />
				</br:brPanelGroup>					
			</br:brPanelGrid>			
			
		</br:brPanelGroup>		
		
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="3" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				</br:brPanelGroup>	
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.PGIC0000_label_situacao_contrato}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>				
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		    
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>		    					

				<br:brPanelGroup>
					<br:brSelectOneMenu id="situacaoContrato" value="#{pgic0000Bean.situacaoContratoFiltro}" disabled="true">
						<f:selectItem itemValue="" itemLabel="#{msgs.PGIC0000_label_combo_selecione}"/>
						<f:selectItems value="#{pgic0000Bean.listaSituacaoContrato}"/>
					</br:brSelectOneMenu>	
				</br:brPanelGroup>					
			</br:brPanelGrid>			
			
		</br:brPanelGroup>		
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	 	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.label_botao_limpar_dados}" disabled="true" action="#{pgic0000Bean.limparDadosContrato}" id="btnLimparDados" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
						<br:brCommandButton styleClass="HtmlCommandButtonBradesco" value="#{msgs.label_botao_consultar}" action="#{pgic0000Bean.carregaLista}" id="btnConsultar" disabled="true" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<f:verbatim><br/> </f:verbatim>	
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
			<f:verbatim> <div id="rolagem" style="width:750px; overflow:scroll"></f:verbatim> 
			<t:dataTable id="dataTable" value="#{pgic0000Bean.listaGridContrato}" var="result" rows="5" 
			cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
			columnClasses="alinhamento_centro, alinhamento_direita, alinhamento_direita, alinhamento_esquerda, alinhamento_direita, alinhamento_direita, alinhamento_esquerda, alinhamento_esquerda"
			headerClass="tabela_celula_destaque_acentuado" width="750px">		
			  <t:column width="30px">
				<t:selectOneRadio onclick="javascript:habilitarBotaoConfirmarContrato(document.forms[1], this);" id="sor2" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
					<f:selectItems value="#{pgic0000Bean.listaControle}"/>
				</t:selectOneRadio>
				<t:radio for="sor2" index="#{result.totalRegistros}" />
			  </t:column>
			  <t:column width="100px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_club}" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.club}"style="float:right;" />
			  </t:column>
			  <t:column width="140px">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.cmpi0000_label_cpf_cnpj}" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.cpf}" style="float:right;"/>
			  </t:column>
			  <t:column width="200px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_nome_razao_social}" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.nomeRazao}" style="float:left;"/>
			  </t:column>
			  <t:column width="200px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_empresa}" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.dataNascimentoFundacao}" style="float:left;"/>
			  </t:column>
			  <t:column width="150px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_tipo}" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.bcoAgeConta}" style="float:left;"/>
			  </t:column>		
			  <t:column width="150px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0000_label_numero}" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.bcoAgeConta}" style="float:left;"/>
			  </t:column>	
			  <t:column width="150px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0000_label_situacao}" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.bcoAgeConta}" style="float:left;"/>
			  </t:column>		 
			  <t:column width="150px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.PGIC0000_label_unidade_operadora}" style="font-weight: bold;font-family: verdana;font-size: 10 px;" escape="false" />
			    </f:facet>
			    <br:brOutputText value="#{result.bcoAgeConta}" style="float:left;"/>
			  </t:column>	
			</t:dataTable>		
			<f:verbatim> </div> </f:verbatim>	
			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{pgic0000Bean.pesquisar}" rendered="#{pgic0000Bean.listaGridContrato!= null && pgic0000Bean.mostraBotoes0000}">
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			  </f:facet>
			</brArq:pdcDataScroller> 
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	
	 <br:brPanelGrid columns="4" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="HtmlCommandButtonBradesco" style="align:left" value="#{msgs.botao_voltar}" action="#{pgic0000Bean.voltarContrato}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			
		</br:brPanelGroup>	
		 
		<br:brPanelGrid columns="1" style="width:440px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGroup style="text-align:right;width:80px;" >
			<br:brCommandButton id="btnLimpar" styleClass="HtmlCommandButtonBradesco" style="align:right; margin-right:5px;" value="#{msgs.botao_limpar}" rendered="#{pgic0000Bean.listaGridContrato == null}" action="#{pgic0000Bean.limparListaContratos}" disabled="true">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnLimpar1" styleClass="HtmlCommandButtonBradesco" style="align:right; margin-right:5px;" value="#{msgs.botao_limpar}" rendered="#{pgic0000Bean.listaGridContrato != null}" action="#{pgic0000Bean.limparListaContratos}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:80px" >
			<br:brCommandButton id="btnConfirmar" styleClass="HtmlCommandButtonBradesco" style="align:right" value="#{msgs.botao_selecionar}" action="#{pgic0000Bean.confirmarContrato}" disabled="true">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>		
	</br:brPanelGrid>	  
						
</br:brPanelGrid>
	<brArq:validatorScript functionName="validateForm" />
</brArq:form>
