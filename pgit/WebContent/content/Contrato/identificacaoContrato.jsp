<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@taglib  uri="http://richfaces.org/rich" prefix="rich" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://bradesco.com.br/arq_components" prefix="brArq"%>

<brArq:form id="identificacaoContratoForm" name="identificacaoContratoForm" >

<h:inputHidden id="hiddenRadioContrato" value="#{identificacaoContratoBean.hiddenRadioContrato}"/>
<h:inputHidden id="hiddenRadioFiltroArgumento" value="#{identificacaoContratoBean.hiddenRadioFiltroArgumento}"/>
<h:inputHidden id="hiddenEmpresa" value="#{identificacaoContratoBean.hiddenEmpresa}"/>
<h:inputHidden id="hiddenTipo" value="#{identificacaoContratoBean.hiddenTipo}"/>
<h:inputHidden id="hiddenGerente" value="#{identificacaoContratoBean.hiddenGerente}"/>
<h:inputHidden id="hiddenSituacao" value="#{identificacaoContratoBean.hiddenSituacao}"/>

<br:brPanelGrid columns="1" width="750" cellpadding="0" cellspacing="0">
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup> 
		</br:brPanelGroup>
	</br:brPanelGrid>
	
<br:brPanelGrid styleClass="mainPanel" cellpadding="0" cellspacing="0" columns="1" width="100%"  >
		<br:brPanelGroup>
			<br:brOutputText styleClass="HtmlOutputTextTitleBradesco" value="#{msgs.label_title_argumentos_pesquisa}:"/>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:selectOneRadio  id="sor" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false" value="#{identificacaoContratoBean.itemSelecionadoPesquisa}">  
				<f:selectItem itemValue="0" itemLabel="" />
				<f:selectItem itemValue="1" itemLabel="" />
				<f:selectItem itemValue="2" itemLabel="" />
				<f:selectItem itemValue="3" itemLabel="" />
  				<a4j:support event="onclick" reRender="empresa,tipo, numero,txtAgenciaOperadora,gerenteResponsavel,txtDia,txtMes,txtAno,situacaoContrato" action="#{identificacaoContratoBean.limparCamposPesquisa}"/>				
	    	</t:selectOneRadio>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
    <br:brPanelGrid columns="6" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="0" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_empresa}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    <br:brSelectOneMenu id="empresa" value="#{identificacaoContratoBean.empresaFiltro}" disabled="#{identificacaoContratoBean.itemSelecionadoPesquisa != 0}" >
					<f:selectItem itemValue="0" itemLabel="#{msgs.identificacaoContrato_label_combo_selecione}"/>
					<f:selectItems value="#{identificacaoContratoBean.listaEmpresa}"/>
				</br:brSelectOneMenu>

				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.label_tipo}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brSelectOneMenu id="tipo" value="#{identificacaoContratoBean.tipoFiltro}" disabled="#{identificacaoContratoBean.itemSelecionadoPesquisa != 0}" onchange="javascript:selecaoTipo(document.forms[1], this);">
						<f:selectItem itemValue="0" itemLabel="#{msgs.identificacaoContrato_label_combo_selecione}"/>
						<f:selectItems value="#{identificacaoContratoBean.listaTipo}"/>
					</br:brSelectOneMenu>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		<br:brPanelGroup style="width:20px; margin-bottom:5px" >
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoContrato_label_numero}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{identificacaoContratoBean.numeroFiltro}" size="13" maxlength="10" id="numero" readonly="#{identificacaoContratoBean.itemSelecionadoPesquisa != 0}">
						<brArq:commonsValidator type="integer" arg="#{msgs.identificacaoContrato_label_numero}" server="false" client="true" />
				    </br:brInputText>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	

    <br:brPanelGrid columns="4" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="1" />			
		</br:brPanelGroup>
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoContrato_label_agencia_operadora}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" value="#{identificacaoContratoBean.agenciaOperadoraFiltro}" size="20"  id="txtAgenciaOperadora" readonly="#{identificacaoContratoBean.itemSelecionadoPesquisa != 1}"/>
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>			
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet" />
				</br:brPanelGroup>		
				<br:brPanelGroup>			
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoContrato_label_gerente_responsavel}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>
			
		    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
			    <br:brPanelGroup>			
				</br:brPanelGroup>	
			</br:brPanelGrid>

		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >					
			    <br:brPanelGroup>	
			    	<br:brSelectOneMenu id="gerenteResponsavel" value="#{identificacaoContratoBean.gerenteResponsavelFiltro}" disabled="#{identificacaoContratoBean.itemSelecionadoPesquisa != 1}" onchange="javascript:selecaoGerente(document.forms[1], this);">
						<f:selectItem itemValue="0" itemLabel="#{msgs.identificacaoContrato_label_combo_selecione}"/>
						<f:selectItems value="#{identificacaoContratoBean.listaGerenteResponsavel}"/>
					</br:brSelectOneMenu>	
				</br:brPanelGroup>					
			</br:brPanelGrid>
										
		</br:brPanelGroup>	
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="2" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				</br:brPanelGroup>	
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoContrato_label_data_cadastramento}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>				
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		    
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>		    					

				  <br:brPanelGrid columns="5" cellpadding="0" cellspacing="0" >
 

			    <br:brPanelGroup>	
			    	<br:brInputText styleClass="HtmlInputTextBradesco" readonly="#{identificacaoContratoBean.itemSelecionadoPesquisa != 2}" 
			    		onkeyup="proximoCampo(2,'identificacaoContratoForm','identificacaoContratoForm:txtDia','identificacaoContratoForm:txtMes');" 
			    		value="#{identificacaoContratoBean.diaFiltro}"  size="4" maxlength="2"  
			    		onblur="validarData('identificacaoContratoForm','identificacaoContratoForm:txtDia', 'dia');" 
			    		onkeypress="aplicamascara('identificacaoContratoForm','identificacaoContratoForm:txtDia',numeros);"  
			    		id="txtDia">
			    		<brArq:commonsValidator type="integer" arg="#{msgs.identificacaoContrato_label_data_cadastramento}" server="false" client="true" />			  				      
				    </br:brInputText>	
				</br:brPanelGroup>									    
  	
			    <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<f:verbatim>/</f:verbatim>
					</br:brPanelGroup>
				</br:brPanelGrid>	
	
			    <br:brPanelGroup>	
				    <br:brInputText styleClass="HtmlInputTextBradesco" readonly="#{identificacaoContratoBean.itemSelecionadoPesquisa != 2}"
				     onkeyup="proximoCampo(2,'identificacaoContratoForm','identificacaoContratoForm:txtMes','identificacaoContratoForm:txtAno');" 
				     value="#{identificacaoContratoBean.mesFiltro}" size="4" maxlength="2" 
				      onblur="validarData('identificacaoContratoForm','identificacaoContratoForm:txtMes', 'mes');"
				       onkeypress="aplicamascara('identificacaoContratoForm','identificacaoContratoForm:txtMes',numeros);" 
				       id="txtMes">
			    		<brArq:commonsValidator type="integer" arg="#{msgs.identificacaoContrato_label_data_cadastramento}" server="false" client="true" />			  				      				       
				    </br:brInputText>	
   				</br:brPanelGroup>					

    		    <br:brPanelGrid columns="1" style="margin-right:5px" cellpadding="0" cellspacing="0" >
					<br:brPanelGroup>
						<f:verbatim>/</f:verbatim>
					</br:brPanelGroup> 
				</br:brPanelGrid>	
				
				<br:brPanelGroup>						
				    <br:brInputText styleClass="HtmlInputTextBradesco" readonly="#{identificacaoContratoBean.itemSelecionadoPesquisa != 2}" value="#{identificacaoContratoBean.anoFiltro}" 
				      size="8" maxlength="4"  onblur="validarData('identificacaoContratoForm','identificacaoContratoForm:txtAno', 'ano');"
				     onkeypress="aplicamascara('identificacaoContratoForm','identificacaoContratoForm:txtAno',numeros);"
				      id="txtAno">
			    		<brArq:commonsValidator type="integer" arg="#{msgs.identificacaoContrato_label_data_cadastramento}" server="false" client="true" />			  				      				      
				    </br:brInputText>	
				</br:brPanelGroup>	

			</br:brPanelGrid>					
			</br:brPanelGrid>			
			
		</br:brPanelGroup>		
		
	</br:brPanelGrid>
	
	
	<br:brPanelGrid columns="1" style="margin-top:6px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
   <br:brPanelGrid columns="2" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
			<t:radio for="sor" index="3" />			
		</br:brPanelGroup>
		
		<br:brPanelGroup>
			<br:brPanelGrid columns="2" cellpadding="0" cellspacing="0"  style="text-align:left;">
				<br:brPanelGroup>
					<br:brGraphicImage url="/images/bullet.jpg" styleClass="HtmlBullet"/>	
				</br:brPanelGroup>	
				<br:brPanelGroup>
					<br:brOutputText styleClass="HtmlOutputTextRespostaBradesco" value="#{msgs.identificacaoContrato_label_situacao_contrato}"/>
				</br:brPanelGroup>
			</br:brPanelGrid>				
			
		    <br:brPanelGrid columns="1" cellpadding="0" cellspacing="0" >
		    
			    <br:brPanelGrid columns="1" style="margin-top:5px" cellpadding="0" cellspacing="0" >
				    <br:brPanelGroup>			
					</br:brPanelGroup>	
				</br:brPanelGrid>		    					

				<br:brPanelGroup>
					<br:brSelectOneMenu id="situacaoContrato" value="#{identificacaoContratoBean.situacaoContratoFiltro}" disabled="#{identificacaoContratoBean.itemSelecionadoPesquisa != 3}" onchange="javascript:selecaoSituacao(document.forms[1], this);">
						<f:selectItem itemValue="0" itemLabel="#{msgs.identificacaoContrato_label_combo_selecione}"/>
						<f:selectItems value="#{identificacaoContratoBean.listaSituacaoContrato}"/>
					</br:brSelectOneMenu>	
				</br:brPanelGroup>					
			</br:brPanelGrid>			
			
		</br:brPanelGroup>		
		
	</br:brPanelGrid>
	
	<br:brPanelGrid columns="1" style="margin-top:9px" cellpadding="0" cellspacing="0" >
		<br:brPanelGroup>
		</br:brPanelGroup>
	</br:brPanelGrid>
	
	
	<f:verbatim><hr class="lin"> </f:verbatim>	 	
	
 	<br:brPanelGrid columns="1" width="100%" style="text-align:right" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<br:brCommandButton styleClass="HtmlCommandButtonBradesco" style="margin-right:5px" value="#{msgs.label_botao_limpar_dados}" disabled="false" action="#{identificacaoContratoBean.limparDadosContrato}" id="btnLimparDados" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
						<br:brCommandButton styleClass="HtmlCommandButtonBradesco" value="#{msgs.label_botao_consultar}" action="#{identificacaoContratoBean.carregaLista}" id="btnConsultar" disabled="false" onclick="javascript: return validateForm(document.forms[1]);" >	
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
	</br:brPanelGrid>	
	<f:verbatim><br/> </f:verbatim>	
	
	<br:brPanelGrid columns="1" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup >	
			<f:verbatim> <div id="rolagem" style="width:750px; overflow-x:scroll"></f:verbatim> 
			<t:dataTable id="dataTable" value="#{identificacaoContratoBean.lista}" var="result" rows="10" 
			cellspacing="1" cellpadding="0" rowClasses="tabela_celula_normal, tabela_celula_destaque"
			columnClasses="alinhamento_centro, alinhamento_direita, alinhamento_direita, 
			alinhamento_esquerda, alinhamento_esquerda, alinhamento_esquerda, alinhamento_esquerda, 
			alinhamento_esquerda, alinhamento_esquerda"
			headerClass="tabela_celula_destaque_acentuado" width="1270px">		
			  <t:column width="30px" style="padding-right:5px; padding-left:5px">
				<t:selectOneRadio onclick="javascript:habilitarBotaoConfirmarContrato(document.forms[1], this);" id="sor2" styleClass="HtmlSelectOneRadioBradesco" layout="spread" forceId="true" forceIdIndex="false">  
					<f:selectItems value="#{identificacaoContratoBean.listaControle}"/>
				</t:selectOneRadio>
				<t:radio for="sor2" index="#{result.numeroRegistro}" />
			  </t:column>
			  <t:column width="100px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_club}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.club}" />
			  </t:column>
			  <t:column width="140px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <h:outputText value="#{msgs.identificacaoCliente_label_cpf_cnpj}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.cpfCnpj}"/>
			  </t:column>
			  <t:column width="200px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_nome_razao_social}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.nomeRazao}"/>
			  </t:column>
			  <t:column width="200px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_empresa}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.empresa}"/>
			  </t:column>
			  <t:column width="150px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.label_tipo}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.tipo}" />
			  </t:column>		
			  <t:column width="150px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.identificacaoContrato_label_numero}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.numero}"/>
			  </t:column>	
			  <t:column width="150px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.identificacaoContrato_label_situacao}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.situacao}"/>
			  </t:column>		 
			  <t:column width="150px" style="padding-right:5px; padding-left:5px">
			    <f:facet name="header">
			      <br:brOutputText value="#{msgs.identificacaoContrato_label_unidade_operadora}"/>
			    </f:facet>
			    <br:brOutputText value="#{result.unidadeOperadora}"/>
			  </t:column>	
			</t:dataTable>		
			<f:verbatim> </div> </f:verbatim>	
			
		</br:brPanelGroup>
	</br:brPanelGrid>	
	
		
 	<br:brPanelGrid columns="1" width="100%" style="text-align:center" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup>
			<brArq:pdcDataScroller id="dataScroller" for="dataTable" actionListener="#{identificacaoContratoBean.pesquisar}" rendered="#{identificacaoContratoBean.lista != null && identificacaoContratoBean.mostraBotoes0000}">
			  <f:facet name="previous">
			    <brArq:pdcCommandButton id="anterior"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_anterior}" title="#{msgs.label_anterior}"/>
			  </f:facet>
			  <f:facet name="next">
			    <brArq:pdcCommandButton id="proxima"
			      styleClass="HtmlCommandButtonBradesco" style="margin-left: 3px;"
			      value="#{msgs.label_proxima}" title="#{msgs.label_proxima}"/>
			  </f:facet>
			</brArq:pdcDataScroller>
		</br:brPanelGroup>
	</br:brPanelGrid>		
	
	<f:verbatim><hr class="lin"> </f:verbatim>
	
	
	<br:brPanelGrid columns="4" width="100%" cellpadding="0" cellspacing="0" >	
		<br:brPanelGroup style="text-align:left;width:150px" >
			<br:brCommandButton id="btnVoltar" styleClass="HtmlCommandButtonBradesco" style="align:left" value="#{msgs.botao_voltar}" action="#{identificacaoContratoBean.voltarContrato}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>	
			
		</br:brPanelGroup>	
		 
		<br:brPanelGrid columns="1" style="width:440px" cellpadding="0" cellspacing="0" >
			<br:brPanelGroup> 
			</br:brPanelGroup>
		</br:brPanelGrid>
			
		<br:brPanelGroup style="text-align:right;width:80px;" >
			<br:brCommandButton id="btnLimpar" styleClass="HtmlCommandButtonBradesco" style="align:right; margin-right:5px;" value="#{msgs.botao_limpar}" rendered="#{identificacaoContratoBean.lista == null}" action="#{identificacaoContratoBean.limparListaContratos}" disabled="true">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
			<br:brCommandButton id="btnLimpar1" styleClass="HtmlCommandButtonBradesco" style="align:right; margin-right:5px;" value="#{msgs.botao_limpar}" rendered="#{identificacaoContratoBean.lista != null}" action="#{identificacaoContratoBean.limparListaContratos}">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>
		<br:brPanelGroup style="text-align:right;width:80px" >
			<br:brCommandButton id="btnConfirmar" styleClass="HtmlCommandButtonBradesco" style="align:right" value="#{msgs.botao_selecionar}" action="#{identificacaoContratoBean.confirmarContrato}"  disabled="true">
				<brArq:submitCheckClient/>
			</br:brCommandButton>
		</br:brPanelGroup>		
	</br:brPanelGrid>	  
						
</br:brPanelGrid>

<f:verbatim> 

<script language="javascript">


	function selecaoEmpresa(form,combo){
	
		for(i=0; i<form.elements.length; i++){		
	
			if (form.elements[i].id == 'identificacaoContratoForm:hiddenEmpresa'){			
				form.elements[i].value = combo.value;
			}	
		}
	}
	
	function selecaoTipo(form,combo){
	
		for(i=0; i<form.elements.length; i++){		
	
			if (form.elements[i].id == 'identificacaoContratoForm:hiddenTipo'){			
				form.elements[i].value = combo.value;
			}	
		}
	}
	
	
	function selecaoGerente(form,combo){
	
		for(i=0; i<form.elements.length; i++){		
	
			if (form.elements[i].id == 'identificacaoContratoForm:hiddenGerente'){			
				form.elements[i].value = combo.value;
			}	
		}
	}
	
	function selecaoSituacao(form,combo){
	
		for(i=0; i<form.elements.length; i++){		
	
			if (form.elements[i].id == 'identificacaoContratoForm:hiddenSituacao'){			
				form.elements[i].value = combo.value;
			}	
		}
	}

	

</script>	
	
</f:verbatim>

	<brArq:validatorScript functionName="validateForm" />
</brArq:form>
