function validaIncluirFiltro(form, campo, necessario, datapagamento, codigo, inscricao, tipoInscricao,tipoPostagem,logradouro,numero,cep,endereco,agenciaDepartamento,departamento,destinoEntrega,modalidade,complemento,bairro,municipio,uf,percentualInvalido,tipoServico){
	var msgValidacao = '';
	var email ='';
	var erro=0;
	
	radioDestino = document.getElementsByName('radioInfoAdicionais');
	radioTipoPostagem = document.getElementsByName('radioTipoPostagem');
	radioAgenciaDepto = document.getElementsByName('radioAgenciaDepto');	
	radioFavorecido = document.getElementsByName('radioFavorecido');
	
	
	if(document.getElementById(form.id + ':cboTipoServico').value == null ||document.getElementById(form.id + ':cboTipoServico').value == '0' ){
		msgValidacao = msgValidacao + campo + ' ' + tipoServico + ' ' + necessario + '!' + '\n';	
	}
	
	if(document.getElementById(form.id + ':dataInicioSolicitacao.day').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.day').value == ''   ||
		document.getElementById(form.id + ':dataInicioSolicitacao.month').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.month').value == ''   ||
		document.getElementById(form.id + ':dataInicioSolicitacao.year').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.year').value == '' ||
		document.getElementById(form.id + ':dataFimSolicitacao.day').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.day').value == ''   ||
		document.getElementById(form.id + ':dataFimSolicitacao.month').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.month').value == ''   ||
		document.getElementById(form.id + ':dataFimSolicitacao.year').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.year').value == '') {
			msgValidacao = msgValidacao + campo + ' ' + datapagamento + ' ' + necessario + '!' + '\n';	
	}
	
	if(!radioDestino[0].checked && !radioDestino[1].checked){
		msgValidacao = msgValidacao + campo + ' ' + destinoEntrega + ' ' + necessario + '!' + '\n';	
	}
	
	if(radioFavorecido[0].checked){
		if (document.getElementById(form.id + ':txtCodigoFavorecido').value == '') {
			msgValidacao = msgValidacao + campo + ' ' + codigo + ' ' + necessario + '!\n';			
		}else{
			if(parseInt(document.getElementById(form.id + ':txtCodigoFavorecido').value,10) == '0'){
				msgValidacao = msgValidacao + campo + ' ' + codigo + ' ' + necessario + '!\n';		
			}
		}
	}
	else{
		if(radioFavorecido[1].checked){
			if(document.getElementById(form.id + ':txtInscricaoFavorecido').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + inscricao + ' ' + necessario + '!\n';			
			}else{
				if(parseInt(document.getElementById(form.id + ':txtInscricaoFavorecido').value,10) == '0'){
					msgValidacao = msgValidacao + campo + ' ' + inscricao + ' ' + necessario + '!\n';		
				}
			}				
			
			if ( document.getElementById(form.id +':cboTipoFavorecido').value == null || document.getElementById(form.id +':cboTipoFavorecido').value == '0' ){
				msgValidacao = msgValidacao + campo + ' ' + tipoInscricao + ' ' + necessario + '!\n';		
			}
					
		}
	}
	
	if(radioDestino[0].checked){
		if(radioTipoPostagem[0].checked){
			if (document.getElementById(form.id + ':txtLogradouro').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + logradouro + ' ' + necessario + '!\n';			
			}
			if (document.getElementById(form.id + ':txtNumero').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + numero + ' ' + necessario + '!\n';			
			}/* 
			if (document.getElementById(form.id + ':txtComplemento').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + complemento + ' ' + necessario + '!\n';			
			} */
			if ((document.getElementById(form.id + ':txtCep1').value == '')||(document.getElementById(form.id + ':txtCep2').value == '')) {
				msgValidacao = msgValidacao + campo + ' ' + cep + ' ' + necessario + '!\n';			
			}
			if (document.getElementById(form.id + ':txtBairro').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + bairro + ' ' + necessario + '!\n';			
			} 
			
			if (document.getElementById(form.id + ':txtMunicipio').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + municipio + ' ' + necessario + '!\n';			
			} 
			
			if (document.getElementById(form.id + ':txtUf').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + uf + ' ' + necessario + '!\n';			
			} 
		}else if(radioTipoPostagem[1].checked){
				if (document.getElementById(form.id + ':txtEmail').value == '') {
					msgValidacao = msgValidacao + campo + ' ' + 'Email' + ' ' + necessario + '!\n';			
				}else{
					email = document.getElementById(form.id + ':txtEmail').value
					var iArroba = 0;
					var subEmail = '';
					var erro = 0;

					if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
						iArroba = email.indexOf('@');
						proxArroba = email.substring(iArroba + 1, iArroba + 2);
						if (iArroba < 1)
						erro = 1;
						if (proxArroba == '.')
						erro = 1;
						if (email.substring(email.length - 1, email.length) == '.')
						erro = 1;
					} else {
						erro = 1;
					}

				    if (erro == 1){
				    	msgValidacao = msgValidacao + 'Endereço de email inválido';
				    }
				}
			}
		else{
			msgValidacao = msgValidacao + campo + ' ' + tipoPostagem + ' ' + necessario + '!\n';
		}
	}
	else if(radioDestino[1].checked){
			if(radioAgenciaDepto[1].checked){
				if (allTrim(document.getElementById(form.id + ':txtDepartamento')) == '' || parseInt(document.getElementById(form.id + ':txtDepartamento').value,10) == 0) {
					msgValidacao = msgValidacao + campo + ' ' + departamento + ' ' + necessario + '!\n';			
				}
			}
			else{
				if(!radioAgenciaDepto[0].checked){
					msgValidacao = msgValidacao + campo + ' ' + agenciaDepartamento + ' ' + necessario + '!\n';
				}
			}
	}
	
//	if((document.getElementById(form.id + ':cboTipoServico').value != 0 &&
//	   document.getElementById(form.id + ':cboTipoServico').value != null) &&
//	   (document.getElementById(form.id + ':cboModalidade').value == 0 ||
//	   document.getElementById(form.id + ':cboModalidade').value == null)){
//		
//			msgValidacao = msgValidacao + campo + ' ' + modalidade + ' ' + necessario + '!\n';						
//	}
	
	/*if(document.getElementById(form.id + ':txtDescontoTarifa').value != 0.00){
		var percentual = parseFloat(document.getElementById(form.id + ':txtDescontoTarifa').value.replace(",","."));
		if(percentual > 100.00){
			msgValidacao = msgValidacao + percentualInvalido;
		}	
	}*/
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;	
}

function validarCampoDescontoTarifa(object) {
	var percentual = object.value.replace(",",".");

	if(percentual > 100.00){
		//object.value = object.value.substring(0, object.value.length-1) 
		object.focus();
		alert("Desconto permitido na faixa de 0,00 a 100,00");
	}
}