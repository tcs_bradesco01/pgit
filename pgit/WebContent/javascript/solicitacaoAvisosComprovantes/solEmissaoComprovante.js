function validaSolEmissaoComprovante(form, msgcampo, msgnecessario, msgdatapagamento){
	var msgValidacao = '';
	
	if(document.getElementById(form.id + ':dataInicioSolicitacao.day').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.day').value == ''   ||
		document.getElementById(form.id + ':dataInicioSolicitacao.month').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.month').value == ''   ||
		document.getElementById(form.id + ':dataInicioSolicitacao.year').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.year').value == '' ||
		document.getElementById(form.id + ':dataFimSolicitacao.day').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.day').value == ''   ||
		document.getElementById(form.id + ':dataFimSolicitacao.month').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.month').value == ''   ||
		document.getElementById(form.id + ':dataFimSolicitacao.year').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.year').value == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	
	if(msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaConsultaEnderecoEmail(form, msgcampo, msgnecessario, msgcnpj, msgcpf, msgcnpjincorreto, msgcpfincorreto, msgnomerazao){
	var msgValidacao = '';
	var radio; 
	
	radio = document.getElementsByName('radioArgumentosPesquisa');
	
	if(radio[0].checked){
		if(allTrim(document.getElementById(form.id + ':txtCnpj')) == "" &&
		   allTrim(document.getElementById(form.id +':txtFilial')) == "" &&
		   allTrim(document.getElementById(form.id + ':txtControle'))  == ""){
				msgValidacao = msgValidacao +   msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';	
		}else{
			if(allTrim(document.getElementById(form.id + ':txtCnpj')) == "" ||
			   allTrim(document.getElementById(form.id +':txtFilial')) == "" ||
			   allTrim(document.getElementById(form.id + ':txtControle'))  == ""){
			   
					msgValidacao = msgValidacao + msgcnpjincorreto + '!\n';	
			}
		}
	} 
	else if (radio[1].checked) {

		if(allTrim(document.getElementById(form.id +':txtCpf')) == "" &&
		   allTrim(document.getElementById(form.id +':txtControleCpf')) == ""){
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
		}else{		
			if(allTrim(document.getElementById(form.id +':txtCpf')) == "" ||
			   allTrim(document.getElementById(form.id +':txtControleCpf')) == ""){
					msgValidacao = msgValidacao + msgcpfincorreto + '!\n';	
			}
		}	
	}
	else if(radio[2].checked){
		if (allTrim(document.getElementById(form.id + ':txtNomeRazaoSocial')) == '') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnomerazao + ' ' + msgnecessario + '!\n';			
		}
	}
	
	if(msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}
function validaCamposIncluirInfoAdicionais(form, campo, necessario, tipoDestino, tipoPostagem, logradouro, numero, cep, endereco, agenciaDepartamento, departamento, tarifa){
	var msgValidacao = '';
	var radioDestino;
	var radioTipoPostagem;
	var radioAgenciaDepto;
	
	radioDestino = document.getElementsByName('radioInfoAdicionais');
	radioTipoPostagem = document.getElementsByName('radioTipoPostagem');
	radioAgenciaDepto = document.getElementsByName('radioAgenciaDepto');
	
	if(!radioDestino[0].checked && !radioDestino[1].checked){
		msgValidacao = msgValidacao + campo + ' ' + tipoDestino + ' ' + necessario + '!\n';	
	}
	
	if(radioDestino[0].checked){
		if(radioTipoPostagem[0].checked){
			if (document.getElementById(form.id + ':txtLogradouro').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + logradouro + ' ' + necessario + '!\n';			
			}
			if (document.getElementById(form.id + ':txtNumero').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + numero + ' ' + necessario + '!\n';			
			} 
			if ((document.getElementById(form.id + ':txtCep1').value == '')||(document.getElementById(form.id + ':txtCep2').value == '')) {
				msgValidacao = msgValidacao + campo + ' ' + cep + ' ' + necessario + '!\n';			
			}
		}else if(radioTipoPostagem[1].checked){
				if (document.getElementById(form.id + ':txtEnderecoEmail').value == '') {
					msgValidacao = msgValidacao + campo + ' ' + endereco + ' ' + necessario + '!\n';			
				}
			}
		else{
			msgValidacao = msgValidacao + campo + ' ' + tipoPostagem + ' ' + necessario + '!\n';
		}
	}
	else if(radioDestino[1].checked){
			if(radioAgenciaDepto[1].checked){
				if (allTrim(document.getElementById(form.id + ':txtDepartamento')) == '' || parseInt(document.getElementById(form.id + ':txtDepartamento').value,10) == 0) {
					msgValidacao = msgValidacao + campo + ' ' + departamento + ' ' + necessario + '!\n';			
				}
			}
		
			else if(!radioAgenciaDepto[0].checked){
				msgValidacao = msgValidacao + campo + ' ' + agenciaDepartamento + ' ' + necessario + '!\n';
			}
	}
	
	if(document.getElementById(form.id + ':txtDescontoTarifa').value == '') {
		msgValidacao = msgValidacao + campo + ' ' + tarifa + ' ' + necessario + '!\n';			
	}
	
	if(msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	return true;
}

function validaFavorecidoFiltro(form, campo, necessario, favorecido, codigo, inscricao, tipoInscricao){
	var msgValidacao = '';
	var radioFavorecido;

	radioFavorecido = document.getElementsByName('radioFavorecido');
	
	if(radioFavorecido[0].checked){
		if (allTrim(document.getElementById(form.id + ':txtCodigoFavorecido')) == '') {
			msgValidacao = msgValidacao + campo + ' ' + codigo + ' ' + necessario + '!\n';			
		}else{
			if(parseInt(document.getElementById(form.id + ':txtCodigoFavorecido').value,10) == '0'){
				msgValidacao = msgValidacao + campo + ' ' + codigo + ' ' + necessario + '!\n';		
			}
		}

	}
	else{
		if(radioFavorecido[1].checked){
			if(allTrim(document.getElementById(form.id + ':txtInscricaoFavorecido')) == '') {
				msgValidacao = msgValidacao + campo + ' ' + inscricao + ' ' + necessario + '!\n';			
			}else{
				if(parseInt(document.getElementById(form.id + ':txtInscricaoFavorecido').value,10) == '0'){
					msgValidacao = msgValidacao + campo + ' ' + inscricao + ' ' + necessario + '!\n';		
				}
			}

			if(document.getElementById(form.id + ':cboTipoFavorecido').value == 0 || document.getElementById(form.id + ':cboTipoFavorecido').value == null){
				msgValidacao = msgValidacao + campo + ' ' + tipoInscricao + ' ' + necessario + '!\n';		
			}

		}
		else{
			msgValidacao = msgValidacao + campo + ' ' + favorecido + ' ' + necessario + '!';
		}
	}
	
	return msgValidacao;
}

function validaIncluirFiltro(form, campo, necessario, numeroPagamento, valorPagamento, favorecido, codigo, inscricao, tipoInscricao, msgou, msgdata, modalidade, msgObrigatorio){
	var msgValidacao = '';
	
	var radioArgumentos;
	radioArgumentos = document.getElementsByName('radioArgumentos');
	
	if(document.getElementById(form.id + ':dataInicioSolicitacao.day').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.day').value == ''   ||
		document.getElementById(form.id + ':dataInicioSolicitacao.month').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.month').value == ''   ||
		document.getElementById(form.id + ':dataInicioSolicitacao.year').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.year').value == '' ||
		document.getElementById(form.id + ':dataFimSolicitacao.day').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.day').value == ''   ||
		document.getElementById(form.id + ':dataFimSolicitacao.month').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.month').value == ''   ||
		document.getElementById(form.id + ':dataFimSolicitacao.year').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.year').value == '') {
		
			msgValidacao = msgValidacao + campo + ' ' + msgdata + ' ' + necessario + '!' + '\n';	
	}
	
	if((document.getElementById(form.id + ':cboTipoServico').value != 0 &&
	   document.getElementById(form.id + ':cboTipoServico').value != null) &&
	   (document.getElementById(form.id + ':cboModalidade').value == 0 ||
	   document.getElementById(form.id + ':cboModalidade').value == null)){
		
			msgValidacao = msgValidacao + campo + ' ' + modalidade + ' ' + necessario + '!\n';						
	}	
	
	if ( radioArgumentos[0].checked ){
		var flagNumeroValor = 0;
		var valorPagamentoDe = replaceAll(document.getElementById(form.id +':txtValorPagamento1').value);
		var valorPagamentoAte = replaceAll(document.getElementById(form.id +':txtValorPagamento2').value);
		
		var valorPreenchido= false;
		var numeroPreenchido = false;
		
		if( (allTrim(document.getElementById(form.id +':txtNumeroPagamento1')) == '') &&
   		    (allTrim(document.getElementById(form.id +':txtNumeroPagamento2')) == '') ){
			flagNumeroValor++;
		}else{
			numeroPreenchido = true;
		}
			


		if( (((allTrim(document.getElementById(form.id +':txtValorPagamento1')) == '') ||
			(valorPagamentoDe == '0')) &&
   		    ((allTrim(document.getElementById(form.id +':txtValorPagamento2')) == '') || 
   		    (valorPagamentoAte == '0')))){
			flagNumeroValor++;
		}else{
			valorPreenchido = true;
		}
			
		if(flagNumeroValor == 2){
			msgValidacao = msgValidacao + campo + ' ' + numeroPagamento + ' ' + msgou + ' ' + valorPagamento + ' ' + necessario + '!' + '\n';			
		}else{

			if(numeroPreenchido && ((allTrim(document.getElementById(form.id +':txtNumeroPagamento1')) == '') && 
				                    (allTrim(document.getElementById(form.id +':txtNumeroPagamento2')) != '')) ||
				                   ((allTrim(document.getElementById(form.id +':txtNumeroPagamento1')) != '') && 
				                    (allTrim(document.getElementById(form.id +':txtNumeroPagamento2')) == ''))){
				msgValidacao = msgValidacao + campo + ' ' + numeroPagamento + ' ' + necessario + '!' + '\n';		
			}
			if(valorPreenchido && ((allTrim(document.getElementById(form.id +':txtValorPagamento1')) == '') || (valorPagamentoDe == '0') || 
				                   (allTrim(document.getElementById(form.id +':txtValorPagamento2')) == '') || (valorPagamentoAte == '0'))){
			
   		     	msgValidacao = msgValidacao + campo + ' ' + valorPagamento + ' ' + necessario + '!' + '\n';	
   		     }
   		   
			
		}
	}	

	if(radioArgumentos[1].checked){
		msgValidacao += validaFavorecidoFiltro(form, campo, necessario, favorecido, codigo, inscricao, tipoInscricao);
	}
	
	if(msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	return true;	
}

function confirmaExclusao(confirmacao, sucesso){
	if(confirm(confirmacao)){
		alert(sucesso);
	}
}

function atualizaRegistroSelecionado(form){

	var existeRegistroSelecionado = false;
	for(i=0; i<form.elements.length && !existeRegistroSelecionado; i++)
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			if (form.elements[i].checked){
				existeRegistroSelecionado = true;
			}

		}	
		
	}	

	document.getElementById(form.id + ':btnAvancar').disabled = !existeRegistroSelecionado;		
}




function limparSelecionados(form){

	for(i=0; i<form.elements.length ; i++)
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = false;
		}	
		
	}	
		
	document.getElementById(form.id + ':btnAvancar').disabled = true;	
	document.getElementById(form.id + ':dataTable:checkSelecionarTodos').checked = false;	
	return true;
	
}

function validaIncluirMovimentacao(form, campo, necessario, datapagamento, codigo, inscricao, tipoInscricao, tipoPostagem, logradouro, numero, cep, endereco, agenciaDepartamento, departamento, tipoServico, modalidade,destinoEntrega,msgfavorecido,complemento,bairro,municipio,uf,percentualInvalido,tipoServico){
	var msgValidacao = '';
	var radioFavorecido;
	var radioDestino;
	var radioTipoPostagem;
	var radioAgenciaDepto;	
	var email ='';
	var erro=0;

	radioFavorecido = document.getElementsByName('radioFavorecido');
	radioDestino = document.getElementsByName('radioInfoAdicionais');
	radioTipoPostagem = document.getElementsByName('radioTipoPostagem');
	radioAgenciaDepto = document.getElementsByName('radioAgenciaDepto');	
	
	if(document.getElementById(form.id + ':cboTipoServico').value == null || document.getElementById(form.id + ':cboTipoServico').value =='0'){
		msgValidacao = msgValidacao + campo + ' ' + tipoServico + ' ' + necessario + '!' + '\n';	
	} 
	
	if(document.getElementById(form.id + ':dataInicioSolicitacao.day').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.day').value == ''   ||
		document.getElementById(form.id + ':dataInicioSolicitacao.month').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.month').value == ''   ||
		document.getElementById(form.id + ':dataInicioSolicitacao.year').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.year').value == '' ||
		document.getElementById(form.id + ':dataFimSolicitacao.day').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.day').value == ''   ||
		document.getElementById(form.id + ':dataFimSolicitacao.month').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.month').value == ''   ||
		document.getElementById(form.id + ':dataFimSolicitacao.year').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.year').value == '') {
			msgValidacao = msgValidacao + campo + ' ' + datapagamento + ' ' + necessario + '!' + '\n';	
	}
	
	if(!radioFavorecido[0].checked && !radioFavorecido[1].checked){
		msgValidacao = msgValidacao + campo + ' ' + msgfavorecido + ' ' + necessario + '!' + '\n';	
	}
	
	if(!radioDestino[0].checked && !radioDestino[1].checked){
		msgValidacao = msgValidacao + campo + ' ' + destinoEntrega + ' ' + necessario + '!' + '\n';	
	}

	if(radioFavorecido[0].checked){
		if (allTrim(document.getElementById(form.id + ':txtCodigoFavorecido')) == '') {
			msgValidacao = msgValidacao + campo + ' ' + codigo + ' ' + necessario + '!\n';			
		}else{
			if(parseInt(document.getElementById(form.id + ':txtCodigoFavorecido').value,10) == '0'){
				msgValidacao = msgValidacao + campo + ' ' + codigo + ' ' + necessario + '!\n';
			}
		}
	}
	else{
		if(radioFavorecido[1].checked){
			if(allTrim(document.getElementById(form.id + ':txtInscricaoFavorecido')) == '') {
				msgValidacao = msgValidacao + campo + ' ' + inscricao + ' ' + necessario + '!\n';			
			}else{
				if(parseInt(document.getElementById(form.id + ':txtInscricaoFavorecido').value,10) == '0'){
					msgValidacao = msgValidacao + campo + ' ' + inscricao + ' ' + necessario + '!\n';
				}
			}
			
			if(document.getElementById(form.id + ':cboTipoFavorecido').value == 0){
					msgValidacao = msgValidacao + campo + ' ' + tipoInscricao + ' ' + necessario + '!\n';		
			}
			
		}
	}
	
	if(radioDestino[0].checked){
		if(radioTipoPostagem[0].checked){
			if (document.getElementById(form.id + ':txtLogradouro').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + logradouro + ' ' + necessario + '!\n';			
			}
			if (document.getElementById(form.id + ':txtNumero').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + numero + ' ' + necessario + '!\n';			
			}/* 
			if (document.getElementById(form.id + ':txtComplemento').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + complemento + ' ' + necessario + '!\n';			
			} */
			if ((document.getElementById(form.id + ':txtCep1').value == '')||(document.getElementById(form.id + ':txtCep2').value == '')) {
				msgValidacao = msgValidacao + campo + ' ' + cep + ' ' + necessario + '!\n';			
			}
			if (document.getElementById(form.id + ':txtBairro').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + bairro + ' ' + necessario + '!\n';			
			}			
			if (document.getElementById(form.id + ':txtMunicipio').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + municipio + ' ' + necessario + '!\n';			
			} 			
			if (document.getElementById(form.id + ':txtUf').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + uf + ' ' + necessario + '!\n';			
			} 			
		}else if(radioTipoPostagem[1].checked){
				if (document.getElementById(form.id + ':txtEmail').value == '') {
					msgValidacao = msgValidacao + campo + ' ' + endereco + ' ' + necessario + '!\n';			
				}
				else{
					email = document.getElementById(form.id + ':txtEmail').value
					var iArroba = 0;
					var subEmail = '';
					var erro = 0;

					if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
						iArroba = email.indexOf('@');
						proxArroba = email.substring(iArroba + 1, iArroba + 2);
						if (iArroba < 1)
						erro = 1;
						if (proxArroba == '.')
						erro = 1;
						if (email.substring(email.length - 1, email.length) == '.')
						erro = 1;
					} else {
						erro = 1;
					}

				    if (erro == 1){
				    	msgValidacao = msgValidacao + 'Endereço de email inválido';
				    }
				}
			}
		else{
			msgValidacao = msgValidacao + campo + ' ' + tipoPostagem + ' ' + necessario + '!\n';
		}
	}
	else if(radioDestino[1].checked){
			if(radioAgenciaDepto[1].checked){
				if (allTrim(document.getElementById(form.id + ':txtDepartamento')) == '' || parseInt(document.getElementById(form.id + ':txtDepartamento').value,10) == 0) {
					msgValidacao = msgValidacao + campo + ' ' + departamento + ' ' + necessario + '!\n';			
				}
			}
		
			else if(!radioAgenciaDepto[0].checked){
				msgValidacao = msgValidacao + campo + ' ' + agenciaDepartamento + ' ' + necessario + '!\n';
			}
	}

//	if((document.getElementById(form.id + ':cboTipoServico').value != 0 &&
//	   document.getElementById(form.id + ':cboTipoServico').value != null) &&
//	   (document.getElementById(form.id + ':cboModalidade').value == 0 ||
//	   document.getElementById(form.id + ':cboModalidade').value == null)){
//		
//			msgValidacao = msgValidacao + campo + ' ' + modalidade + ' ' + necessario + '!\n';						
//	}
	
	/*if(document.getElementById(form.id + ':txtDescontoTarifa').value != 0.00){
		var percentual = parseFloat(document.getElementById(form.id + ':txtDescontoTarifa').value.replace(",","."));
		if(percentual > 100.00){
			msgValidacao = msgValidacao + percentualInvalido;
		}
	}*/
	
	
	

	if(msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return msgValidacao;
}

function selecionarTodos(form, check){
	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
		
	}	
	document.getElementById(form.id + ':btnLimparSelecionados').disabled = !check.checked;
	document.getElementById(form.id + ':btnAvancar').disabled = !check.checked;
}

function validaCamposIncluirInfoAdicionaisCliPagador(form, campo, necessario, tipoDestino, tipoPostagem, logradouro, numero, cep, endereco, agenciaDepartamento, departamento, tarifa,complemento,bairro,municipio,uf, percentualInvalido){
	var msgValidacao = '';
	var radioDestino;
	var radioTipoPostagem;
	var radioAgenciaDepto;
	
	radioDestino = document.getElementsByName('radioInfoAdicionais');
	radioTipoPostagem = document.getElementsByName('radioTipoPostagem');
	radioAgenciaDepto = document.getElementsByName('radioAgenciaDepto');
	
	if(!radioDestino[0].checked && !radioDestino[1].checked){
		msgValidacao = msgValidacao + campo + ' ' + tipoDestino + ' ' + necessario + '!\n';	
	}
	
	if(radioDestino[0].checked){
		if(radioTipoPostagem[0].checked){
			if (document.getElementById(form.id + ':txtLogradouro').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + logradouro + ' ' + necessario + '!\n';			
			}
			if (document.getElementById(form.id + ':txtNumero').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + numero + ' ' + necessario + '!\n';			
			}
			/*
			if (document.getElementById(form.id + ':txtComplemento').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + complemento + ' ' + necessario + '!\n';			
			} */
			
			if ((document.getElementById(form.id + ':txtCep1').value == '')||(document.getElementById(form.id + ':txtCep2').value == '')) {
				msgValidacao = msgValidacao + campo + ' ' + cep + ' ' + necessario + '!\n';			
			}
			
			if (document.getElementById(form.id + ':txtBairro').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + bairro + ' ' + necessario + '!\n';			
			} 
			
			if (document.getElementById(form.id + ':txtMunicipio').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + municipio + ' ' + necessario + '!\n';			
			} 
			
			if (document.getElementById(form.id + ':txtUf').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + uf + ' ' + necessario + '!\n';			
			} 
		}else if(radioTipoPostagem[1].checked){
			if (document.getElementById(form.id + ':txtEnderecoEmail').value == '') {
				msgValidacao = msgValidacao + campo + ' ' + 'Email' + ' ' + necessario + '!\n';			
			}else{
				email = document.getElementById(form.id + ':txtEnderecoEmail').value
				var iArroba = 0;
				var subEmail = '';
				var erro = 0;

				if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
					iArroba = email.indexOf('@');
					proxArroba = email.substring(iArroba + 1, iArroba + 2);
					if (iArroba < 1)
					erro = 1;
					if (proxArroba == '.')
					erro = 1;
					if (email.substring(email.length - 1, email.length) == '.')
					erro = 1;
				} else {
					erro = 1;
				}

			    if (erro == 1){
			    	msgValidacao = msgValidacao + 'Endereço de email inválido';
			    }
			}
		} else{
			msgValidacao = msgValidacao + campo + ' ' + tipoPostagem + ' ' + necessario + '!\n';
		}
	}
	else if(radioDestino[1].checked){
			if(radioAgenciaDepto[1].checked){
		
				if (allTrim(document.getElementById(form.id + ':txtDepartamento')) == '' || parseInt(document.getElementById(form.id + ':txtDepartamento').value,10) == 0) {
					msgValidacao = msgValidacao + campo + ' ' + departamento + ' ' + necessario + '!\n';			
				}
			}
		
			else if(!radioAgenciaDepto[0].checked){
				msgValidacao = msgValidacao + campo + ' ' + agenciaDepartamento + ' ' + necessario + '!\n';
			}
	}
	
	if(document.getElementById(form.id + ':hiddenTarifaPadrao').value != 0.00){
		if(document.getElementById(form.id + ':txtDescontoTarifa').value == '') {
			msgValidacao = msgValidacao + campo + ' ' + tarifa + ' ' + necessario + '!\n';			
		}
			
		//var percentual = parseFloat(document.getElementById(form.id + ':txtDescontoTarifa').value.replace(",","."));
		//if(percentual > 100.00){
			//msgValidacao = msgValidacao + percentualInvalido;
		//}
	}
	
	
	if(msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	return true;
}

function validaIncluirFiltroFavorecido(form, campo, necessario, msgdata, modalidade, favorecido, codigo, inscricao, tipoInscricao, numeroPagamento,valorPagamento){
	var msgValidacao = ''; 
	


	if(document.getElementById(form.id + ':dataInicioSolicitacao.day').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.day').value == ''   ||
		document.getElementById(form.id + ':dataInicioSolicitacao.month').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.month').value == ''   ||
		document.getElementById(form.id + ':dataInicioSolicitacao.year').value == null ||
		document.getElementById(form.id + ':dataInicioSolicitacao.year').value == '' ||
		document.getElementById(form.id + ':dataFimSolicitacao.day').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.day').value == ''   ||
		document.getElementById(form.id + ':dataFimSolicitacao.month').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.month').value == ''   ||
		document.getElementById(form.id + ':dataFimSolicitacao.year').value == null ||
		document.getElementById(form.id + ':dataFimSolicitacao.year').value == '') {
		
			msgValidacao = msgValidacao + campo + ' ' + msgdata + ' ' + necessario + '!' + '\n';	
	}

	if((document.getElementById(form.id + ':cboTipoServico').value != 0 &&
	   document.getElementById(form.id + ':cboTipoServico').value != null) &&
	   (document.getElementById(form.id + ':cboModalidade').value == 0 ||
	   document.getElementById(form.id + ':cboModalidade').value == null)){
		
			msgValidacao = msgValidacao + campo + ' ' + modalidade + ' ' + necessario + '!\n';						
	}
	
	
	if ( (allTrim(document.getElementById(form.id + ':txtNumeroPagamento1')) == '' &&  allTrim(document.getElementById(form.id + ':txtNumeroPagamento2')) != '') ||
		(allTrim(document.getElementById(form.id + ':txtNumeroPagamento1')) != '' &&  allTrim(document.getElementById(form.id + ':txtNumeroPagamento2')) == '')){
		
		
    	msgValidacao = msgValidacao + campo + ' '  + numeroPagamento + ' ' +  necessario + '!' + '\n';			
    	
    	
	}

	var valorPagamentoDe = parseFloat(document.getElementById(form.id +':txtValorPagamento1').value.replace(',','.'));
	var valorPagamentoAte = parseFloat(document.getElementById(form.id +':txtValorPagamento2').value.replace(',','.'));
	
		
	if(((allTrim(document.getElementById(form.id +':txtValorPagamento1')) == '') && (allTrim(document.getElementById(form.id +':txtValorPagamento2')) != '')) ||
	   ((allTrim(document.getElementById(form.id +':txtValorPagamento1')) != '') && (allTrim(document.getElementById(form.id +':txtValorPagamento2')) == ''))){
	
		msgValidacao = msgValidacao + campo + ' ' + valorPagamento + ' ' + necessario + '!' + '\n';	
   	}
	
	
	
	var radioFavorecido;
	radioFavorecido = document.getElementsByName('radioFavorecido');
	
	if(!radioFavorecido[0].checked && !radioFavorecido[1].checked){
		msgValidacao = msgValidacao + campo + ' ' + favorecido + ' ' + necessario + '!\n';	
	}
	
	if(radioFavorecido[0].checked){
		if (allTrim(document.getElementById(form.id + ':txtCodigoFavorecido')) == '') { 
			msgValidacao = msgValidacao + campo + ' ' + codigo + ' ' + necessario + '!\n';	 		
		}else{
			if(parseInt(document.getElementById(form.id + ':txtCodigoFavorecido').value,10) == '0'){
				msgValidacao = msgValidacao + campo + ' ' + codigo + ' ' + necessario + '!\n';		
			}
		}
		 
	}
	else{
		if(radioFavorecido[1].checked){
			if(allTrim(document.getElementById(form.id + ':txtInscricaoFavorecido')) == '') {
				msgValidacao = msgValidacao + campo + ' ' + inscricao + ' ' + necessario + '!\n';			
			}else{ 
				if(parseInt(document.getElementById(form.id + ':txtInscricaoFavorecido').value,10) == '0'){
					msgValidacao = msgValidacao + campo + ' ' + inscricao + ' ' + necessario + '!\n';		
				}
			}

			if(document.getElementById(form.id + ':cboTipoFavorecido').value == 0 || document.getElementById(form.id + ':cboTipoFavorecido').value == null){
				msgValidacao = msgValidacao + campo + ' ' + tipoInscricao + ' ' + necessario + '!\n';		
			}

		}
	}

	if(msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	return true;	
}

function replaceAll(string){
	while (string.indexOf(".") > 0){
		string = string.replace(".","");
	}
	string = parseFloat(string.replace(",", ""));
	return string;
}

function validaCampoPorcentagem(form, percentualInvalido, msgConfirma) {
	var msgValidacao = '';
	
	//if(document.getElementById(form.id + ':hiddenDescontoTarifa').value != 0.00){
		//var percentual = parseFloat(document.getElementById(form.id + ':hiddenDescontoTarifa').value.replace(",","."));
		//if(percentual > 100.00){
			//msgValidacao = msgValidacao + percentualInvalido;
		//}	
	//}

	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	if (!confirm(msgConfirma)) {
		desbloquearTela();
		return false;
	}

	return true;
}

function validarCampoDescontoTarifa(object) {
	var percentual = object.value.replace(",",".");

	if(percentual > 100.00){
		//object.value = object.value.substring(0, object.value.length-1) 
		object.focus();
		alert("Desconto permitido na faixa de 0,00 a 100,00");
	}
}