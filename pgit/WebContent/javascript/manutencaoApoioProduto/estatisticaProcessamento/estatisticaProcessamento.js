function validarFiltroEstatistica(msgcampo, msgnecessario, msgCalendarioDe, msgCalendarioAte, msghorariofiltro, msgcentrocusto, msgprocessamento, msgHoraInvalida, msg, msgHoraInicialFinal) {
	var campos = '';
	var objFocus = new Array();

	var objCentroCusto = document.getElementById('estatisticaProcessamentoForm:centroCusto');
	if (objCentroCusto != null) {
		if (objCentroCusto.disabled || isEmpty(objCentroCusto.value)) {
			campos = campos + msgcampo + ' ' + msgcentrocusto + ' ' + msgnecessario + '\n';
			if (!objCentroCusto.disabled) {
				objFocus.push(objCentroCusto);
			}
		}
	}

	if (!validarCampoData('estatisticaProcessamentoForm:calendarioDe')) {
		campos = campos + msgcampo + ' ' + msgCalendarioDe + ' ' + msgnecessario + '\n';
		objFocus.push(document.getElementById('estatisticaProcessamentoForm:calendarioDe.day'));
	}

	if (!validarCampoData('estatisticaProcessamentoForm:calendarioAte', '')) {
		campos = campos + msgcampo + ' ' + msgCalendarioAte + ' ' + msgnecessario + '\n';
		objFocus.push(document.getElementById('estatisticaProcessamentoForm:calendarioAte.day'));
	}
	
	var horaInicial = document.getElementById('estatisticaProcessamentoForm:horarioFiltro').value.replace(':','');
	var horaFinal = document.getElementById('estatisticaProcessamentoForm:horarioFiltroFim').value.replace(':','');
	
	if((horaInicial != '' && horaFinal == '') || (horaInicial == '' && horaFinal != '')){
		campos = campos + msgcampo + ' ' + msgHoraInicialFinal + ' ' + msgnecessario + '\n';
	}else{
		if(horaFinal < horaInicial){
			campos = campos + msgHoraInvalida + '\n';
		}
	}
	
	if(document.getElementById('estatisticaProcessamentoForm:calendarioDe.year').value > document.getElementById('estatisticaProcessamentoForm:calendarioAte.year').value){
		campos = msg + '!' + '\n';	
	}else{
		if(document.getElementById('estatisticaProcessamentoForm:calendarioDe.year').value == document.getElementById('estatisticaProcessamentoForm:calendarioAte.year').value){
			if(document.getElementById('estatisticaProcessamentoForm:calendarioDe.month').value > document.getElementById('estatisticaProcessamentoForm:calendarioAte.month').value){
				campos = msg + '!' + '\n';	
			}else{
				if(document.getElementById('estatisticaProcessamentoForm:calendarioDe.month').value == document.getElementById('estatisticaProcessamentoForm:calendarioAte.month').value){
					if(document.getElementById('estatisticaProcessamentoForm:calendarioDe.day').value > document.getElementById('estatisticaProcessamentoForm:calendarioAte.day').value){
						campos = msg + '!' + '\n';	
					}
				}
			}
		}
	}

	if (campos != '') {
		alert(campos);
		if (objFocus != null && objFocus.length > 0) {
			objFocus[0].focus();
		}
		return false
	}
	return true;
}

function validarHora(campo, msgHorarioInvalido){
		var msgValidacao = '';
		
		if(campo.value != ''){
			hora = (allTrim(campo)).split(":");
			if (hora.length != 2){
				msgValidacao = msgValidacao + msgHorarioInvalido + '\n';
			}else{
				if(isNaN(hora[0]) || hora[0] > 23){
					msgValidacao = msgValidacao + msgHorarioInvalido + '\n';
				}
				else if(isNaN(hora[1]) || hora[1] > 59){
					msgValidacao = msgValidacao + msgHorarioInvalido + '\n';
				}
			}
		}
	
		if (msgValidacao != ''){
			alert(msgValidacao);
			campo.value = '';
			campo.focus();
			return false;
		}
	
		return true;
	}