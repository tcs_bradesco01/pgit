

function validaCamposIncluir(form, msgcampo, msgnecessario, msgrestricao, msgnivelmensagem, msgcodtipomsg, msgdescricao, msgtipocomprovante){

	
	var campos = '';
	
	if (form.elements[form.id + ':codigoTipoMensagem'].value == ''){
		
	 	campos = campos + msgcampo + ' ' + msgcodtipomsg + ' ' + msgnecessario + '\n';
	 	

	}
	
	
	if (allTrim(form.elements[form.id + ':descricao']) == ''){	
	
		campos = campos + msgcampo + ' ' + msgdescricao + ' ' + msgnecessario + '\n';
		
		
	}

	
	if (form.elements[form.id + ':hiddenNivelMensagem'].value != 1 &&  form.elements[form.id + ':hiddenNivelMensagem'].value != 2){
		campos = campos + msgcampo + ' ' + msgnivelmensagem + ' ' + msgnecessario + '\n';
		
	}else{
	 	if ( form.elements[form.id + ':hiddenNivelMensagem'].value == 2){
		
			if (form.elements[form.id + ':tipoComprovante'].value == '0'){	
				campos = campos + msgcampo + ' ' + msgtipocomprovante + ' ' + msgnecessario + '\n';
				
			}
			if (form.elements[form.id + ':hiddenRestricao'].value != 1 && form.elements[form.id + ':hiddenRestricao'].value != 2 ){	
				campos = campos + msgcampo + ' ' + msgrestricao + ' ' + msgnecessario + '\n';
				
			}
		}
	}


	if(campos != ''){
		alert(campos);
		form.elements[form.id + ':hiddenObrigatoriedade'].value = "F";	
		
		
	}else{
		form.elements[form.id + ':hiddenObrigatoriedade'].value = "T";
					
	}	
	
	 
	
}

function validaCamposAlterar(form, msgcampo, msgnecessario, msgrestricao, msgnivelmensagem, msgcodtipomsg, msgdescricao, msgtipocomprovante){

	
	var campos = '';

	

	if (allTrim(form.elements[form.id + ':descricao']) == ''){	
	
		campos = campos + msgcampo + ' ' + msgdescricao + ' ' + msgnecessario + '\n';
	}
	
	if (form.elements[form.id + ':hiddenNivelMensagem'].value != 1 &&  form.elements[form.id + ':hiddenNivelMensagem'].value != 2){
		campos = campos + msgcampo + ' ' + msgnivelmensagem + ' ' + msgnecessario + '\n';

	}else{
	 	if ( form.elements[form.id + ':hiddenNivelMensagem'].value == 2){
		
			if (form.elements[form.id + ':tipoComprovante'].value == '0'){	
				campos = campos + msgcampo + ' ' + msgtipocomprovante + ' ' + msgnecessario + '\n';
				
			}
			if (form.elements[form.id + ':hiddenRestricao'].value != 1 && form.elements[form.id + ':hiddenRestricao'].value != 2 ){	
				campos = campos + msgcampo + ' ' + msgrestricao + ' ' + msgnecessario + '\n';
				
			}
		}
	}


	if(campos != ''){
		alert(campos);
		form.elements[form.id + ':hiddenObrigatoriedade'].value = "F";	
				
	}else{
		form.elements[form.id + ':hiddenObrigatoriedade'].value = "T";			
	}	
	
	

	
}


function limparCamposPesquisa (form){
	
		form.elements[form.id + ':tipoMensagem'][0].checked = false;
		form.elements[form.id + ':tipoMensagem'][1].checked = false;
//		form.elements[form.id + ':tipoMensagem'].value = null;
		form.elements[form.id + ':tipoComprovante'].value = 0;
		form.elements[form.id + ':radioRestricao'][0].checked = false;
		form.elements[form.id + ':radioRestricao'][1].checked = false;
	//	form.elements[form.id + ':radioRestricao'].value = null;
}