	function checaCentroCusto(form, msgcampo, msgnecessario,msgcentrocusto){
		if (allTrim(document.getElementById(form.id + ':centroCustoFiltro')) == ''){
			alert(msgcampo + ' ' + msgcentrocusto + ' ' + msgnecessario + '\n');
			return false;
		}
		else{
			return true;
		}
	}

function checaCamposObrigatoriosIncluir(msgcampo, msgnecessario, msgformaliquidacao, msghorariolimite, msgprioridadedebito) {
	var campos = '';
	var comboFormaLiquidacao = document.getElementById("incFormaLiquidacaoPagamentoIntegradoForm:formaLiquidacao");
	
	if (comboFormaLiquidacao.value == '0') {
		campos += msgcampo + ' ' + msgformaliquidacao + ' ' + msgnecessario + '\n';
	}

	if (allTrim(document.getElementById("incFormaLiquidacaoPagamentoIntegradoForm:txtHorarioLimite")) == '') {
		campos += msgcampo + ' ' + msghorariolimite + ' ' + msgnecessario + '\n';
	}	

	if (allTrim(document.getElementById("incFormaLiquidacaoPagamentoIntegradoForm:centroCustoFiltro")) == '') {
		campos += msgcampo + ' Filtro Centro de Custo ' + msgnecessario + '\n';
	}

	if (document.getElementById("incFormaLiquidacaoPagamentoIntegradoForm:centroCusto").value == '') {
		campos += msgcampo + ' Centro de Custo ' + msgnecessario + '\n';
	}

	if (document.getElementById("incFormaLiquidacaoPagamentoIntegradoForm:txtMargemSeguranca").value == '') {
		campos += msgcampo + ' Margem de Seguranca em Minutos ' + msgnecessario + '\n';
	}

	if (comboFormaLiquidacao.value == "24") {
		if (document.getElementById("incFormaLiquidacaoPagamentoIntegradoForm:hrLimiteValorSuperior").value == '') {
			campos += msgcampo + ' Hor\u00e1rio Limite para Agendamento Valor Superior ' + msgnecessario + '\n';
		}

		if (document.getElementById("incFormaLiquidacaoPagamentoIntegradoForm:qtMinutosValorSuperior").value == '') {
			campos += msgcampo + ' Margem de Seguran\u00e7a em Minutos Valor Superior ' + msgnecessario + '\n';
		}
	}

	if (campos == '') {
		return true;
	} else {
		alert(campos);
		return false;
	}
}

	function checaCamposObrigatoriosAlterar(msgcampo, msgnecessario, msghorariolimite, msgprioridadedebito){
		
		var campos = ''
		
		if (allTrim(document.getElementById("altFormaLiquidacaoPagamentoIntegradoForm:txtHorarioLimite")) == ''){
			campos += msgcampo + ' ' + msghorariolimite + ' ' + msgnecessario + '\n';
		}	
		
		if (document.getElementById("altFormaLiquidacaoPagamentoIntegradoForm:centroCusto").value == ''){
			campos += msgcampo + ' Centro de Custo ' + msgnecessario + '\n';
		}
		
		if (document.getElementById("altFormaLiquidacaoPagamentoIntegradoForm:txtMargemSeguranca").value == ''){
			campos += msgcampo + ' Margem de Seguranca em Minutos ' + msgnecessario + '\n';
		}
		
//		if (allTrim(document.getElementById("altFormaLiquidacaoPagamentoIntegradoForm:txtPrioridadeDebito"))== ''){
//			campos += msgcampo + ' ' + msgprioridadedebito + ' '+ msgnecessario + '\n';
//		}

		if (campos == '')
			return true;
		else {
			alert (campos);
			return false;
		}	
	}
	
	function validarData(form, msgDataInicialMenorFinal, msgPeriodoManutencao, msgcampo, msgnecessario){
		var campos = '';
	
		if (document.getElementById(form.id + ':calendarioDe.day').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioDe.day')) == ''   ||
			document.getElementById(form.id + ':calendarioDe.month').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioDe.month')) == ''   ||
			document.getElementById(form.id + ':calendarioDe.year').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioDe.year')) == '' ||
			document.getElementById(form.id + ':calendarioAte.day').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioAte.day')) == ''   ||
			document.getElementById(form.id + ':calendarioAte.month').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioAte.month')) == ''   ||
			document.getElementById(form.id + ':calendarioAte.year').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioAte.year')) == '') {
			
			campos = campos + msgcampo + ' ' + msgPeriodoManutencao + ' ' + msgnecessario + '!' + '\n';	
		}
		else{
			if(document.getElementById(form.id + ':calendarioDe.year').value > document.getElementById(form.id + ':calendarioAte.year').value){
				campos = msgDataInicialMenorFinal + '!' + '\n';	
			}else{
				if(document.getElementById(form.id + ':calendarioDe.year').value == document.getElementById(form.id + ':calendarioAte.year').value){
					if(document.getElementById(form.id + ':calendarioDe.month').value > document.getElementById(form.id + ':calendarioAte.month').value){
						campos = msgDataInicialMenorFinal + '!' + '\n';	
					}else{
						if(document.getElementById(form.id + ':calendarioDe.month').value == document.getElementById(form.id + ':calendarioAte.month').value){
							if(document.getElementById(form.id + ':calendarioDe.day').value > document.getElementById(form.id + ':calendarioAte.day').value){
								campos = msgDataInicialMenorFinal + '!' + '\n';	
							}
						}
					}
				}
			}		
		}	
		 
		if (campos == ''){
			return true;
		}else {
			alert(campos);
			return false;
		}	
	}
	
	
	function validarHora(campo, msgHorarioInvalido){
		var msgValidacao = '';
		
		if(campo.value != ''){
			hora = (allTrim(campo)).split(":");
			if (hora.length != 2){
				msgValidacao = msgValidacao + msgHorarioInvalido + '\n';
			}else{
				if(isNaN(hora[0]) || hora[0] > 23){
					msgValidacao = msgValidacao + msgHorarioInvalido + '\n';
				}
				else if(isNaN(hora[1]) || hora[1] > 59){
					msgValidacao = msgValidacao + msgHorarioInvalido + '\n';
				}
			}
		}
	
		if (msgValidacao != ''){
			alert(msgValidacao);
			campo.value = '';
			campo.focus();
			return false;
		}
	
		return true;
	}