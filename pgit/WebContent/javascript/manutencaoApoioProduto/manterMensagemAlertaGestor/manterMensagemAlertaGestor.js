
function checaCamposObrigatoriosIncluir(form, msgcampo, msgnecessario, msgCentroCusto, msgTipoProcesso, msgTipoMensagem,  msgPrefixo, msgMeioTransmissao, msgRecurso, msgIdioma,msgfuncionario){
	var campos = '';
	var hidden;
		
	for(i=0; i<form.elements.length; i++){
	
		if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:hiddenObrigatoriedade'){
			hidden = form.elements[i];			
		}
		
		if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:centroCusto'){
			if ( (form.elements[i].value == '0')  || (form.elements[i].value == '')){
				campos = campos + msgcampo + ' ' + msgCentroCusto + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:tipoProcesso'){
			if ( (form.elements[i].value == '0') ){
				campos = campos + msgcampo + ' ' + msgTipoProcesso + ' ' + msgnecessario + '\n';
			}
		}
	
		if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:tipoMensagem'){
			if ( allTrim(form.elements[i]) == '' ) {
				campos = campos + msgcampo + ' ' + msgTipoMensagem + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:hiddenRadioMeioTransmissao'){
			if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
				campos = campos + msgcampo + ' ' + msgMeioTransmissao + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:recurso'){
			if ( (form.elements[i].value == '0') ){
				campos = campos + msgcampo + ' ' + msgRecurso + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:idioma'){
			if ( (form.elements[i].value == '0') ){
				campos = campos + msgcampo + ' ' + msgIdioma + ' ' + msgnecessario + '\n';
			}
		}	
		
		if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:txtCodigoFunc'){
			if ( allTrim(form.elements[i]) == '')
				campos = campos  + msgfuncionario +  '\n';			
		}		
	}

	
	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
		desbloquearTela();
		return false;
	}else{
		hidden.value  ='T';
		desbloquearTela();
		return true;
	}
}

function confirmar(form, msgConfirma){
	
		var hiddenConfirm;
		
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id + ':hiddenConfirma'){	
				hiddenConfirm = form.elements[i];
				break;
			}
		}
		
		hiddenConfirm.value=confirm(msgConfirma);
	
}


function checaCamposObrigatoriosIncluirFuncionario(form, msgcampo, msgnecessario, codigoFuncionario){
	var campos = '';
	var hidden;
	var hiddenObrigatoriedade;
		
	for(i=0; i<form.elements.length; i++){
	
		if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:hiddenObrigatoriedadeFuncionario'){
			hiddenObrigatoriedade = form.elements[i];
		}
		
	
		if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:txtCodigoFunc'){
			if ( allTrim(form.elements[i]) == '') {
				campos = campos + msgcampo + ' ' + codigoFuncionario + ' ' + msgnecessario + '\n';
			}
		}
	}
	
	if (campos != ''){
		hiddenObrigatoriedade.value  ='F';
		alert(campos);
	}else{
		hiddenObrigatoriedade.value  ='T';
	}
}


function checaCamposObrigatoriosConsultar(form, msgcampo, msgnecessario, msgCentroCusto, msgTipoProcesso){
	var campos = '';
	var hidden;

		
	for(i=0; i<form.elements.length; i++){
	
		if (form.elements[i].id == 'pesqManterMensagemAlertaGestorForm:hiddenObrigatoriedadeConsultar'){
			hidden = form.elements[i];			
		}

		if (form.elements[i].id == 'pesqManterMensagemAlertaGestorForm:centroCusto'){
			if ( (form.elements[i].value == '0') || (form.elements[i].value == '') ){
				campos = campos + msgcampo + ' ' + msgCentroCusto + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'pesqManterMensagemAlertaGestorForm:tipoProcesso'){
			if ( (form.elements[i].value == '0') || (form.elements[i].value == '0') ){
				campos = campos + msgcampo + ' ' + msgTipoProcesso + ' ' + msgnecessario + '\n';
			}
		}
	}

	
	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
	}else{
		hidden.value  ='T';
	}
}

function checaCamposObrigatoriosAlterarFuncionario(form, msgcampo, msgnecessario, codigoFuncionario){
	var campos = '';
	var hidden;
	var hiddenObrigatoriedade;
		
	for(i=0; i<form.elements.length; i++){
	
		if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:hiddenObrigatoriedadeFuncionario'){
			hiddenObrigatoriedade = form.elements[i];
		}
		
	
		if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:txtCodigoFunc'){
			if ( allTrim(form.elements[i]) == '' ){
				campos = campos + msgcampo + ' ' + codigoFuncionario + ' ' + msgnecessario + '\n';
			}
		}
	}
	
	if (campos != ''){
		hiddenObrigatoriedade.value  ='F';
		alert(campos);
	}else{
		hiddenObrigatoriedade.value  ='T';
	}
}


function checaCamposObrigatoriosAlterar(form, msgcampo, msgnecessario, msgTipoMensagem,  msgMeioTransmissao, msgfuncionario, msgRecurso, msgIdioma){
	var campos = '';
	var hidden;
		
	for(i=0; i<form.elements.length; i++){
	
		if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:hiddenObrigatoriedade'){
			hidden = form.elements[i];			
		}
		
		if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:txtCodigoFunc'){
			if ( allTrim(form.elements[i]) == '')
				campos = campos +  msgfuncionario + '\n';			
		}		
		
	
		if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:tipoMensagem'){
			if ( allTrim(form.elements[i]) == '' ){
				campos = campos + msgcampo + ' ' + msgTipoMensagem + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:hiddenRadioMeioTransmissao'){
			if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
				campos = campos + msgcampo + ' ' + msgMeioTransmissao + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:recurso'){
			if ( (form.elements[i].value == '0') ){
				campos = campos + msgcampo + ' ' + msgRecurso + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:idioma'){
			if ( (form.elements[i].value == '0') ){
				campos = campos + msgcampo + ' ' + msgIdioma + ' ' + msgnecessario + '\n';
			}
		}		
	}

	
	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
		desbloquearTela();
		return false;
	}else{
		hidden.value  ='T';
		desbloquearTela();
		return true;
	}
}