


function validaCampos(form, msgCampo, msgNecessario, msgTipoUnidadeOrganizacionalUsuario,msgTipoUnidadeOrganizacionalProprietario, msgTipoAcao, msgNivelPermissao, msgTipoExcecao){

var hidden;
var mensagem = "";

	for(i=0; i<form.elements.length; i++){
	
		if(form.elements[i].id == form.id + ':hiddenObrigatoriedade'){
					hidden = form.elements[i];
			}
	
		if(form.elements[i].id == form.id + ':tipoUnidadeOrganizacionalUsuario'){
				if(form.elements[i].value == 0){
					mensagem += msgCampo + ' ' + msgTipoUnidadeOrganizacionalUsuario + ' ' + msgNecessario+  '\n';
				}
		}
		if(form.elements[i].id == form.id + ':tipoUnidadeOrganizacionalProprietarioDados'){
				if(form.elements[i].value == 0){
					mensagem += msgCampo + ' ' + msgTipoUnidadeOrganizacionalProprietario + ' ' + msgNecessario+  '\n';
				}
		}
		
		if(form.elements[i].id == form.id + ':hiddenTipoManutencao'){
			if(form.elements[i].value == 0){
				mensagem += msgCampo + ' ' + msgTipoAcao + ' ' + msgNecessario+  '\n';
			}
		}
		
		if(form.elements[i].id == form.id + ':nivelPermissaoAcessoDados'){
				if(form.elements[i].value == 0){
					mensagem += msgCampo + ' ' + msgNivelPermissao + ' ' + msgNecessario+  '\n';
				}
		}
	}
	
	//for(i=0; i<form.elements.length; i++){
		//if(form.elements[i].id == form.id + ':hiddenTipoManutencao'){
			//if(form.elements[i].value == 0){
				//mensagem += msgCampo + ' ' + msgTipoAcao + ' ' + msgNecessario+  '\n';
			//}
		//}
	//}
		
	if(mensagem != ''){
		alert(mensagem);
		hidden.value = "F";
		
	}else
		hidden.value = "V";
	
}



function validaCamposExcecao(form, msgCampo, msgNecessario, msgTipoAbrangencia,msgTipoUnidadeOrgUsuario, msgEmpresaCongUsuario, msgUnidadeOrgUsuario,
	msgTipoUnidadeOrgProprietario, msgEmpresaCongProprietario, msgUnidadeOrgProprietario, msgTipoExcecao){


var hidden;
var mensagem = "";

		if (form.elements[form.id + ':hiddenTipoAbrangencia'].value == 0){
			mensagem += msgCampo + ' ' + msgTipoAbrangencia + ' ' + msgNecessario+  '\n';				
		}else{
		
			if (form.elements[form.id + ':hiddenTipoAbrangencia'].value == 1){
				
				
				if (form.elements[form.id + ':empresaConglomeradoUsuario'].value == 0){
						mensagem += msgCampo + ' ' + msgEmpresaCongUsuario+ ' ' + msgNecessario+  '\n';				
				}
				
				
				
				if (allTrim(form.elements[form.id + ':txtUnidadeOrganizacionalUsuario']) == ''){
					mensagem += msgCampo + ' ' + msgUnidadeOrgUsuario+ ' ' + msgNecessario+  '\n';				
				}
			}	
				
		}
	
	
		
		
		
		
		if (form.elements[form.id + ':empresaConglomeradoProprietario'].value == 0){
					mensagem += msgCampo + ' ' + msgEmpresaCongProprietario+ ' ' + msgNecessario+  '\n';				
		}

		if (allTrim(form.elements[form.id + ':txtUnidadeOrganizacionalProprietarioUsuario']) == ''){
				mensagem += msgCampo + ' ' + msgUnidadeOrgProprietario+ ' ' + msgNecessario+  '\n';				
		}
	
		if(form.elements[form.id + ':hiddenTipoExcecao'].value != 1 && form.elements[form.id + ':hiddenTipoExcecao'].value != 2){
			mensagem += msgCampo + ' ' + msgTipoExcecao + ' ' + msgNecessario+  '\n';				
	
		}
		

	
	if(mensagem != ''){
		alert(mensagem);
		form.elements[form.id + ':hiddenObrigatoriedade'].value = "F";

		
	}else{
		form.elements[form.id + ':hiddenObrigatoriedade'].value = "V";
	}
	
}


function limpaDadosConsulta(){

			document.forms[1].elements[document.forms[1].id + ':tipoManutencao'].value = 0;
			document.forms[1].elements[document.forms[1].id + ':tipoUnidadeOrganizacionalUsuario'].value = 0;
			document.forms[1].elements[document.forms[1].id + ':tipoNivelPermissaoAcessoDados'].value = 0;

}