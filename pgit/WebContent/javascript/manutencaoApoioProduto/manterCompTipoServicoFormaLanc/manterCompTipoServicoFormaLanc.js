function validaCamposIncluir(form, msgcampo, msgnecessario, msgtiposervicocnab, msgformalanc, msgtiposervico, msgmodalidade, msgformaliquidacao){

	var msgValidacao = '';
	
	if ( document.getElementById(form.id +':cboTipoServicoCnab').value == null || document.getElementById(form.id +':cboTipoServicoCnab').value == 0 ){		
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervicocnab + ' ' + msgnecessario + '!' + '\n'
	}
	if ( document.getElementById(form.id +':cboFormaLancCnab').value == null || document.getElementById(form.id +':cboFormaLancCnab').value == 0 ){		
		msgValidacao = msgValidacao + msgcampo + ' ' + msgformalanc + ' ' + msgnecessario + '!' + '\n'
	}
	if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == 0 ){		
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n'
	}		
	/*if ( document.getElementById(form.id +':cboModalidadeServico').value == null || document.getElementById(form.id +':cboModalidadeServico').value == 0 ){		
		msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n'
	}
	if ( document.getElementById(form.id +':cboFormaLiquidacao').value == null || document.getElementById(form.id +':cboFormaLiquidacao').value == 0 ){		
		msgValidacao = msgValidacao + msgcampo + ' ' + msgformaliquidacao + ' ' + msgnecessario + '!' + '\n'
	}*/
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaCamposConsulta(form, msgcampo, msgnecessario, msgtiposervicocnab, msgformalanc, msgtiposervico, msgmodalidade, msgformaliquidacao){
	
	var objRdoCliente = document.getElementsByName('sorRadio');
	var msgValidacao = '';
	
	if (objRdoCliente[0].checked ){
		if ( document.getElementById(form.id +':cboTipoServicoCnab').value == null || document.getElementById(form.id +':cboTipoServicoCnab').value == 0 ){		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervicocnab + ' ' + msgnecessario + '!' + '\n'
		}
		
		if ( document.getElementById(form.id +':cboFormaLancCnab').value == null || document.getElementById(form.id +':cboFormaLancCnab').value == 0 ){		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgformalanc + ' ' + msgnecessario + '!' + '\n'
		}
		
	}
	
	if (objRdoCliente[1].checked){
		if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == 0 ){		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n'
		}		
		
		if ( document.getElementById(form.id +':cboModalidadeServico').value == null || document.getElementById(form.id +':cboModalidadeServico').value == 0 ){		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n'
		}
		
	}
	
	if (objRdoCliente[2].checked){
		if ( document.getElementById(form.id +':cboFormaLiquidacao').value == null || document.getElementById(form.id +':cboFormaLiquidacao').value == 0 ){		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgformaliquidacao + ' ' + msgnecessario + '!' + '\n'
		}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}