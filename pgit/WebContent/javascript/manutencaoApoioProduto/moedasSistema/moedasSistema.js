
function confirmar(form, msgConfirma){
	
		var hiddenConfirm;
		
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id + ':hiddenConfirma'){	
				hiddenConfirm = form.elements[i];
				break;
			}
		}
		
		hiddenConfirm.value=confirm(msgConfirma);
	
}

function habilitaBotoes(){
	document.getElementById('pesqmanterMoedasSistemaForm:btnDetalhar').disabled = "";
	document.getElementById('pesqmanterMoedasSistemaForm:btnExcluir').disabled = "";	
}

function desabilitaBotoes(){
	document.getElementById('pesqmanterMoedasSistemaForm:btnDetalhar').disabled = "disabled";
	document.getElementById('pesqmanterMoedasSistemaForm:btnExcluir').disabled = "disabled";	
}