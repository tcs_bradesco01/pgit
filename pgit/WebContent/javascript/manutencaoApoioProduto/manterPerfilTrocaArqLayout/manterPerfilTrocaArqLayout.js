function validarSistemaOrigemArquivo(form, msgCampo, msgNecessario, msgSistemaOrigemArquivo){
	if (allTrim(document.getElementById(form.id + ':txtSistemaOrigemArquivoFiltro')) == ''){
		alert(msgCampo + ' ' + msgSistemaOrigemArquivo + ' ' + msgNecessario + '!' + '\n');
		return false;
	}
	else{
		return true;
	}
}

function validarInclusao(form, msgCampo,msgNecessario,msgOu,msgCodigo,radioApliFormPropio,msgAplicativoFormatacao,
				msgTipoLayoutArquivo,msgSistemaOrigemArquivo,msgEmpresaResponsavelTransmissao,msgUsername,
				msgNomeArquivoRemessa,msgNomeArquivoRetorno,msgTipoRejeicaoAcolhimento,msgQuantidadeRegistrosInconsistentes,
				msgPercentualRegistrosInconsistentes,msgNivelControle,msgTipoDeControle,msgPeriodicidadeInicializacaoContagem,
				msgNumeroMaximoRemessa,msgMeioTransmissaoPrincipal,msgMeioTransmissaoAlternativo,msgNumeroMaximoRetorno,msgRemessa,msgRetorno, msgRdoApliFormProprio, msgRespCustoTransArqDadosControle, 
				rdoVan, txtPercent,txtJuncao,txtVolume,msgsNomeCliente,msgsDDDCliente,msgsTelefoneCliente,msgsRamalCliente,msgsEmailCliente,msgsNomeSolicitante,msgsDDDSolicitante,msgsTelefoneSolicitante,
				msgsRamalSolicitante,msgsEmailSolicitante,msgsJustificativa, msgExigeCpfCnpj, msgCamaraCompensacao, msgPadraoSap){
	
	var campos = '';

	var objRadioApliFormPropio = document.getElementsByName('rdoApliFormProprio');
	var flagMeioTransmissaoPrincipalRemessa = true;
	var flagMeioTransmissaoAlternativoRemessa = true;
	var flagMeioTransmissaoAlternativoRetorno = true;
	var flagMeioTransmissaoPrincipalRetorno= true;
	var flagMeioTransmissao14 = true ;
	var flagCustoTransmissao = true;
	
	if(objRadioApliFormPropio.length > 0){
		if(objRadioApliFormPropio[1].checked == false && objRadioApliFormPropio[2].checked == false ){
			campos = campos + msgCampo + ' ' + radioApliFormPropio + ' ' + msgNecessario + '!' + '\n';		
		}
		else if(objRadioApliFormPropio[1].checked == true){		
			if((document.getElementById(form.id + ':cboTipoLayoutArquivo').value == null || document.getElementById(form.id + ':cboTipoLayoutArquivo').value == 0) ) {
			    campos = campos + msgCampo + ' ' + msgTipoLayoutArquivo + ' ' + msgNecessario + '!' + '\n';
			}
			
		}
		else{		
			if((document.getElementById(form.id + ':cboAplicativoFormatacao').value == null || document.getElementById(form.id + ':cboAplicativoFormatacao').value == 0) ) {
			    campos = campos + msgCampo + ' ' + msgAplicativoFormatacao + ' ' + msgNecessario + '!' + '\n';
			} else {
				if((document.getElementById(form.id + ':cboTipoLayoutArquivo').value == null || document.getElementById(form.id + ':cboTipoLayoutArquivo').value == 0) ) {
				    campos = campos + msgCampo + ' ' + msgTipoLayoutArquivo + ' ' + msgNecessario + '!' + '\n';
				}
			}
		}
	}
	
	if(document.getElementById(form.id + ':cboTipoLayoutArquivo').value != 0){
		if(document.getElementById(form.id + ':cboTipoLayoutArquivo').value == 14 || document.getElementById(form.id + ':cboTipoLayoutArquivo').value == 15 || document.getElementById(form.id + ':cboTipoLayoutArquivo').value == 16){
			var tabelaRepresentantes = document.getElementById(form.id + ':tableRepresentantes');
			var validaItemSelecionado = false; 
			if (tabelaRepresentantes.rows.length > 0) {
	
				for ( var i = 0; i < tabelaRepresentantes.rows.length; i++) {
	
					var check = document.getElementById(form.id + ':tableRepresentantes_' + (i) + ':sorLista');
					
					if (check != null || check != undefined) {
						// Verifica se o combo Utiliza Layout Proprio est� preenchido
						if(document.getElementById(form.id + ':tableRepresentantes_' + (i) + ':cboUtilizaLayoutProprio').value != 0){
							validaItemSelecionado = true;
							// Se o layout for 2 (NAO), valida se o layout de formata��o est� preenchido
							if(document.getElementById(form.id + ':tableRepresentantes_' + (i) + ':cboUtilizaLayoutProprio').value == 2 &&
									document.getElementById(form.id + ':tableRepresentantes_' + (i) + ':cboAplicativoFotmatacao').value == 0){
								validaItemSelecionado = false;
							}
						}
					}
				}
				
				if (!validaItemSelecionado){
					campos = campos + msgCampo + ' ' + msgTipoLayoutArquivo + ' ' + msgNecessario + '!' + '\n';
				}
			}
	
		}
	}else{
		campos = campos + msgCampo + ' ' + msgTipoLayoutArquivo + ' ' + msgNecessario + '!' + '\n';
	}

	
	
	var objRdoApliFormProprio = document.getElementsByName('rdoApliFormProprio');
	
	//verificando se o radio "Utiliza Aplicativo de Formata��o Pr�prio" foi selecionado
	if(objRdoApliFormProprio.length > 0){
		if(objRdoApliFormProprio[0].checked == false && objRdoApliFormProprio[1].checked == false){
			campos = campos + msgCampo + ' ' + msgRdoApliFormProprio + ' ' + msgNecessario + '!' + '\n';
		}
	}
	
	if(document.getElementById(form.id + ':cboTipoRejeicaoAcolhimento').value == null || document.getElementById(form.id + ':cboTipoRejeicaoAcolhimento').value == 0) {
	    campos = campos + msgCampo + ' ' + msgTipoRejeicaoAcolhimento + ' ' + msgNecessario + '!' + '\n';
	}
	
	var objRadioFiltro = document.getElementsByName('radioFiltro');
	
	if(document.getElementById(form.id + ':cboTipoRejeicaoAcolhimento').value == 3 && !objRadioFiltro[0].checked && !objRadioFiltro[1].checked){
		campos = campos + msgCampo + ' ' + msgQuantidadeRegistrosInconsistentes  + ' '  + msgOu  + ' '  + msgPercentualRegistrosInconsistentes  +  ' ' + msgNecessario + '!' + '\n';
	}else{
		if(document.getElementById(form.id + ':cboTipoRejeicaoAcolhimento').value == 3 && objRadioFiltro[0].checked){
			if (allTrim(document.getElementById(form.id + ':txtQuantidadeRegistrosInconsistentes')) == '') {
			    campos = campos + msgCampo + ' ' + msgQuantidadeRegistrosInconsistentes + ' ' + msgNecessario + '!' + '\n';
			}
		}else{
			if(document.getElementById(form.id + ':cboTipoRejeicaoAcolhimento').value == 3 && objRadioFiltro[1].checked){
				if (allTrim(document.getElementById(form.id + ':txtPercentualRegistrosInconsistentes')) == '') {
				    campos = campos + msgCampo + ' ' + msgPercentualRegistrosInconsistentes + ' ' + msgNecessario + '!' + '\n';
				}else{
				    valorTemp = parseFloat(document.getElementById(form.id + ':txtPercentualRegistrosInconsistentes').value.replace(',','.'));
				    if (valorTemp == '0.00'){
					    campos = campos + msgCampo + ' ' + msgPercentualRegistrosInconsistentes + ' ' + msgNecessario + '!' + '\n';
				    }
				}
			}
		}
	}
		
	if ( document.getElementById(form.id +':cboNivelControleRemessa').value == null || document.getElementById(form.id +':cboNivelControleRemessa').value < 1 ) {
		campos = campos + msgCampo + ' ' + msgNivelControle + ' (' + msgRemessa + ') ' + msgNecessario + '!' + '\n';
	} else if ( document.getElementById(form.id +':cboNivelControleRemessa').value == 1 || document.getElementById(form.id +':cboNivelControleRemessa').value == 2 ) {
		if ( document.getElementById(form.id +':cboTipoControleRemessa').value == null || document.getElementById(form.id +':cboTipoControleRemessa').value < 1 ) {
			campos = campos + msgCampo + ' ' + msgTipoDeControle + ' (' + msgRemessa + ') ' + msgNecessario + '!' + '\n';
		}
	}

	if ( document.getElementById(form.id +':cboNivelControleRetorno').value == null || document.getElementById(form.id +':cboNivelControleRetorno').value < 1 ) {
		campos = campos + msgCampo + ' ' + msgNivelControle + ' (' + msgRetorno + ') ' + msgNecessario + '!' + '\n';
	} else if ( document.getElementById(form.id +':cboNivelControleRetorno').value == 1 || document.getElementById(form.id +':cboNivelControleRetorno').value == 2 ) {
		if ( document.getElementById(form.id +':cboTipoControleRetorno').value == null || document.getElementById(form.id +':cboTipoControleRetorno').value < 1 ) {
			campos = campos + msgCampo + ' ' + msgTipoDeControle + ' (' + msgRetorno + ') ' + msgNecessario + '!' + '\n';
		}
	}

	if ( document.getElementById(form.id +':cboPeriodicidadeInicializacaoContagemRemessa').value == null || document.getElementById(form.id +':cboPeriodicidadeInicializacaoContagemRemessa').value == '0' ) {
		campos = campos + msgCampo + ' ' + ' ' + msgPeriodicidadeInicializacaoContagem + ' (' + msgRemessa + ') ' + ' ' + msgNecessario + '!\n';
	}

	if ( document.getElementById(form.id +':cboPeriodicidadeInicializacaoContagemRetorno').value == null || document.getElementById(form.id +':cboPeriodicidadeInicializacaoContagemRetorno').value == '0' ) {
		campos = campos + msgCampo + ' '+ ' ' + msgPeriodicidadeInicializacaoContagem + ' (' + msgRetorno + ') ' + ' ' + msgNecessario + '!\n';
	}

	if ( document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value == null || document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value < 1 ) {
		campos = campos + msgCampo + ' ' + msgMeioTransmissaoPrincipal + ' ' + '(' + msgRemessa + ')' +  ' ' + msgNecessario + '!\n';
	}

	if ( document.getElementById(form.id +':cboMeioTransmissaoPrincipalRetorno').value == null || document.getElementById(form.id +':cboMeioTransmissaoPrincipalRetorno').value < 1 ) {
		campos = campos + msgCampo + ' ' + msgMeioTransmissaoPrincipal + ' '  + '(' + msgRetorno + ')' + ' ' + msgNecessario + '!\n';
	}
	
	if(document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value  != '0'){
		switch(document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value ){
			case '7':flagMeioTransmissaoPrincipalRemessa = false;break;
			case '8':flagMeioTransmissaoPrincipalRemessa = false;break;
			case '9':flagMeioTransmissaoPrincipalRemessa = false;break;
			default:flagMeioTransmissaoPrincipalRemessa =true;
		}
	}
	
	if(document.getElementById(form.id +':cboMeioTransmissaoAlternativoRemessa').value  != '0'){
		switch(document.getElementById(form.id +':cboMeioTransmissaoAlternativoRemessa').value){
			case '7': flagMeioTransmissaoAlternativoRemessa = false;break;
			case '8': flagMeioTransmissaoAlternativoRemessa = false;break;
			case '9': flagMeioTransmissaoAlternativoRemessa = false;break;
			default:flagMeioTransmissaoAlternativoRemessa =true;
		}
	}
	
	
	if(document.getElementById(form.id +':cboMeioTransmissaoAlternativoRetorno').value != '0'){
		switch(document.getElementById(form.id +':cboMeioTransmissaoAlternativoRetorno').value){
			case '7': flagMeioTransmissaoAlternativoRetorno = false;break;
			case '8':flagMeioTransmissaoAlternativoRetorno = false;break;
			case '9':flagMeioTransmissaoAlternativoRetorno = false;break;
			default:flagMeioTransmissaoAlternativoRetorno =true;
		}
	}
	
	if(document.getElementById(form.id +':cboMeioTransmissaoPrincipalRetorno').value != '0'){
		switch(document.getElementById(form.id +':cboMeioTransmissaoPrincipalRetorno').value){
			case '7': flagMeioTransmissaoPrincipalRetorno = false;break;
			case '8': flagMeioTransmissaoPrincipalRetorno = false;break;
			case '9': flagMeioTransmissaoPrincipalRetorno = false;break;
			default:flagMeioTransmissaoPrincipalRetorno =true;
		}
	}
	
	if(document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value == '14' 
		|| document.getElementById(form.id +':cboMeioTransmissaoAlternativoRemessa').value == '14' 
		|| document.getElementById(form.id +':cboMeioTransmissaoAlternativoRetorno').value == '14' 
		|| document.getElementById(form.id +':cboMeioTransmissaoPrincipalRetorno').value == '14')
	{
		 flagMeioTransmissao14 = false;
		 flagCustoMeioTransmissao = false;
	}
	
	if(document.getElementById(form.id + ':cboTipoLayoutArquivo').value == 14 || document.getElementById(form.id + ':cboTipoLayoutArquivo').value == 15 || document.getElementById(form.id + ':cboTipoLayoutArquivo').value == 16){
		
		var objRdoCdIndicadorCpfLayout = document.getElementsByName('cdIndicadorCpfLayout');

		if(objRdoCdIndicadorCpfLayout.length > 0){
			if(objRdoCdIndicadorCpfLayout[1].checked == false && objRdoCdIndicadorCpfLayout[2].checked == false){
				campos = campos + msgCampo + ' ' + msgExigeCpfCnpj + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		var objRdoCdIndicadorContaComplementar = document.getElementsByName('cdIndicadorContaComplementar');
		
		if(objRdoCdIndicadorContaComplementar.length > 0){
			if(objRdoCdIndicadorContaComplementar[1].checked == false && objRdoCdIndicadorContaComplementar[2].checked == false){
				campos = campos + msgCampo + ' ' + msgCamaraCompensacao + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		var objRdoCdIndicadorAssociacaoLayout = document.getElementsByName('cdIndicadorAssociacaoLayout');

		if(objRdoCdIndicadorAssociacaoLayout.length > 0){
			if(objRdoCdIndicadorAssociacaoLayout[1].checked == false && objRdoCdIndicadorAssociacaoLayout[2].checked == false){
				campos = campos + msgCampo + ' ' + msgPadraoSap + ' ' + msgNecessario + '!' + '\n';
			}
		}
	}
	
	
	if(flagMeioTransmissao14 == false) {
		
		if(document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '1' || document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '3'){
			if(document.getElementById(form.id +':juncaoRespCustoTrans').value == null || document.getElementById(form.id +':juncaoRespCustoTrans').value == ''){
				campos = campos + msgCampo + ' ' + txtJuncao + ' ' + msgNecessario + '!\n';
			}
		}	
			
		if(document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '3'){
			if(document.getElementById(form.id +':txtPercentualCustoTransArq').value == null || document.getElementById(form.id +':txtPercentualCustoTransArq').value == ''){
				campos = campos + msgCampo + ' ' + txtPercent + ' ' + msgNecessario + '!\n';
			}
		}
		
		if (document.getElementById(form.id +':cboEmpresaRespTransDadosControle').value == null || document.getElementById(form.id +':cboEmpresaRespTransDadosControle').value == '0' ) {
			campos = campos + msgCampo + ' ' + msgEmpresaResponsavelTransmissao + ' ' + msgNecessario + '!\n';
		}

		if (document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == null || document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value < 1 ) {
			campos = campos + msgCampo + ' ' + msgRespCustoTransArqDadosControle + ' ' + msgNecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':cboEmpresaRespTransDadosControle').value == null || document.getElementById(form.id +':cboEmpresaRespTransDadosControle').value == '0' ) {
			campos = campos + msgCampo + ' ' + msgEmpresaResponsavelTransmissao + ' ' + msgNecessario + '!\n';
		}

		if (document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == null || document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '0'  ) {
			campos = campos + msgCampo + ' ' + msgRespCustoTransArqDadosControle + ' ' + msgNecessario + '!\n';
		}

		if (document.getElementById(form.id +':volumeMensalTraf').value == null || document.getElementById(form.id +':volumeMensalTraf').value == '' ) {
			campos = campos + msgCampo + ' ' + txtVolume + ' ' + msgNecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':nomeContatoCliente').value == null || document.getElementById(form.id +':nomeContatoCliente').value == '' ) {
			campos = campos + msgCampo + ' ' + msgsNomeCliente + ' ' + msgNecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':dddContatoCliente').value == null || document.getElementById(form.id +':dddContatoCliente').value == '' ) {
			campos = campos + msgCampo + ' ' + msgsDDDCliente + ' ' + msgNecessario + '!\n';
		}

		if (document.getElementById(form.id +':telefoneContatoCliente').value == null || document.getElementById(form.id +':telefoneContatoCliente').value == '' ) {
			campos = campos + msgCampo + ' ' + msgsTelefoneCliente + ' ' + msgNecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':emailContatoCliente').value == null || document.getElementById(form.id +':emailContatoCliente').value == '' ) {
			campos = campos + msgCampo + ' ' + msgsEmailCliente + ' ' + msgNecessario + '!\n';
		}else{
		    email = document.getElementById(form.id + ':emailContatoCliente').value
		    var iArroba = 0;
		    var subEmail = '';
		    var erro = 0;

		    if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
		        iArroba = email.indexOf('@');
		        proxArroba = email.substring(iArroba + 1, iArroba + 2);
		        if (iArroba < 1)
		        erro = 1;
		        if (proxArroba == '.')
		        erro = 1;
		        if (email.substring(email.length - 1, email.length) == '.')
		        erro = 1;
		    } else {
		        erro = 1;
		    }

		    if (erro == 1){
		    	campos = campos + msgCampo + ' ' + msgsEmailCliente + ' ' +' inv�lido' + '!\n';
		    }
		}

		if (document.getElementById(form.id +':nomeContatoSolicitante').value == null || document.getElementById(form.id +':nomeContatoSolicitante').value == '' ) {
			campos = campos + msgCampo + ' ' + msgsNomeSolicitante + ' ' + msgNecessario + '!\n';
		}

		if (document.getElementById(form.id +':dddContatoSolicitante').value == null || document.getElementById(form.id +':dddContatoSolicitante').value == '' ) {
			campos = campos + msgCampo + ' ' + msgsDDDSolicitante + ' ' + msgNecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':telefoneContatoSolicitante').value == null || document.getElementById(form.id +':telefoneContatoSolicitante').value == '' ) {
			campos = campos + msgCampo + ' ' + msgsTelefoneSolicitante + ' ' + msgNecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':emailContatoSolicitante').value == null || document.getElementById(form.id +':emailContatoSolicitante').value == '' ) {
			campos = campos + msgCampo + ' ' + msgsEmailSolicitante + ' ' + msgNecessario + '!\n';
		}else{
		    email = document.getElementById(form.id + ':emailContatoSolicitante').value
		    var iArroba = 0;
		    var subEmail = '';
		    var erro = 0;

		    if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
		        iArroba = email.indexOf('@');
		        proxArroba = email.substring(iArroba + 1, iArroba + 2);
		        if (iArroba < 1)
		        erro = 1;
		        if (proxArroba == '.')
		        erro = 1;
		        if (email.substring(email.length - 1, email.length) == '.')
		        erro = 1;
		    } else {
		        erro = 1;
		    }

		    if (erro == 1){
		    	campos =   campos + msgCampo + ' ' + msgsEmailSolicitante + ' '+ 'inv�lido' + '!\n';
		    }
		}
		
		if (document.getElementById(form.id +':justificativaSolicitante').value == null || document.getElementById(form.id +':justificativaSolicitante').value == '' ) {
			campos = campos + msgCampo + ' ' + msgsJustificativa + ' ' + msgNecessario + '!\n';
			}
	}else if(flagMeioTransmissaoPrincipalRemessa == false 
			 ||flagMeioTransmissaoAlternativoRemessa == false 
			 || flagMeioTransmissaoAlternativoRetorno == false 
			 || flagMeioTransmissaoPrincipalRetorno == false )	
		{
		
		if(document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '1' || document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '3'){
			if(document.getElementById(form.id +':juncaoRespCustoTrans').value == null || document.getElementById(form.id +':juncaoRespCustoTrans').value == ''){
				campos = campos + msgCampo + ' ' + txtJuncao + ' ' + msgNecessario + '!\n';
			}
		}
		
		if (document.getElementById(form.id +':volumeMensalTraf').value == null || document.getElementById(form.id +':volumeMensalTraf').value == '' ) {
			campos = campos + msgCampo + ' ' + txtVolume + ' ' + msgNecessario + '!\n';
		}
		
			if (document.getElementById(form.id +':nomeContatoCliente').value == null || document.getElementById(form.id +':nomeContatoCliente').value == '' ) {
				campos = campos + msgCampo + ' ' + msgsNomeCliente + ' ' + msgNecessario + '!\n';
			}
			
			if (document.getElementById(form.id +':dddContatoCliente').value == null || document.getElementById(form.id +':dddContatoCliente').value == '' ) {
				campos = campos + msgCampo + ' ' + msgsDDDCliente + ' ' + msgNecessario + '!\n';
			}
	
			if (document.getElementById(form.id +':telefoneContatoCliente').value == null || document.getElementById(form.id +':telefoneContatoCliente').value == '' ) {
				campos = campos + msgCampo + ' ' + msgsTelefoneCliente + ' ' + msgNecessario + '!\n';
			}
	
			if (document.getElementById(form.id +':emailContatoCliente').value == null || document.getElementById(form.id +':emailContatoCliente').value == '' ) {
				campos = campos + msgCampo + ' ' + msgsEmailCliente + ' ' + msgNecessario + '!\n';
			}else{
			    email = document.getElementById(form.id + ':emailContatoCliente').value
			    var iArroba = 0;
			    var subEmail = '';
			    var erro = 0;

			    if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
			        iArroba = email.indexOf('@');
			        proxArroba = email.substring(iArroba + 1, iArroba + 2);
			        if (iArroba < 1)
			        erro = 1;
			        if (proxArroba == '.')
			        erro = 1;
			        if (email.substring(email.length - 1, email.length) == '.')
			        erro = 1;
			    } else {
			        erro = 1;
			    }

			    if (erro == 1){
			    	campos = campos + msgCampo + ' ' + msgsEmailCliente + ' ' +'inv�lido' + '!\n';
			    }
			}
	
			if (document.getElementById(form.id +':nomeContatoSolicitante').value == null || document.getElementById(form.id +':nomeContatoSolicitante').value == '' ) {
				campos = campos + msgCampo + ' ' + msgsNomeSolicitante + ' ' + msgNecessario + '!\n';
			}
	
			if (document.getElementById(form.id +':dddContatoSolicitante').value == null || document.getElementById(form.id +':dddContatoSolicitante').value == '' ) {
				campos = campos + msgCampo + ' ' + msgsDDDSolicitante + ' ' + msgNecessario + '!\n';
			}
			
			if (document.getElementById(form.id +':telefoneContatoSolicitante').value == null || document.getElementById(form.id +':telefoneContatoSolicitante').value == '' ) {
				campos = campos + msgCampo + ' ' + msgsTelefoneSolicitante + ' ' + msgNecessario + '!\n';
			}
	
			if (document.getElementById(form.id +':emailContatoSolicitante').value == null || document.getElementById(form.id +':emailContatoSolicitante').value == '' ) {
				campos = campos + msgCampo + ' ' + msgsEmailSolicitante + ' ' + msgNecessario + '!\n';
			}else{
			    email = document.getElementById(form.id + ':emailContatoSolicitante').value
			    var iArroba = 0;
			    var subEmail = '';
			    var erro = 0;

			    if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
			        iArroba = email.indexOf('@');
			        proxArroba = email.substring(iArroba + 1, iArroba + 2);
			        if (iArroba < 1)
			        erro = 1;
			        if (proxArroba == '.')
			        erro = 1;
			        if (email.substring(email.length - 1, email.length) == '.')
			        erro = 1;
			    } else {
			        erro = 1;
			    }

			    if (erro == 1){
			    	campos =  campos + msgCampo + ' ' + msgsEmailSolicitante + ' ' + 'inv�lido' + '!\n';
			    }
			}
			
			if (document.getElementById(form.id +':justificativaSolicitante').value == null || document.getElementById(form.id +':justificativaSolicitante').value == '' ) {
				campos = campos + msgCampo + ' ' + msgsJustificativa + ' ' + msgNecessario + '!\n';
				}
	}

	if (campos != ''){
		alert(campos);
		return false;
	}

	return true;	
}

function validarAlteracao(form, msgCampo,msgNecessario,msgOu,msgAplicativoFormatacao,
				msgSistemaOrigemArquivo,msgEmpresaResponsavelTransmissao,msgUsername,
				msgNomeArquivoRemessa,msgNomeArquivoRetorno,msgTipoRejeicaoAcolhimento,msgQuantidadeRegistrosInconsistentes,
				msgPercentualRegistrosInconsistentes,msgNivelControle,msgTipoDeControle,msgPeriodicidadeInicializacaoContagem,
				msgNumeroMaximoRemessa,msgMeioTransmissaoPrincipal,msgMeioTransmissaoAlternativo,msgNumeroMaximoRetorno,msgRemessa,msgRetorno, msgRdoApliFormProprio, msgRespCustoTransArqDadosControle, rdoVan, txtPercent){

	var campos = '';
	
	if(document.getElementById(form.id + ':cboAplicativoFormatacao').value == null || document.getElementById(form.id + ':cboAplicativoFormatacao').value == 0) {
	    campos = campos + msgCampo + ' ' + msgAplicativoFormatacao + ' ' + msgNecessario + '!' + '\n';
	}
	
	
	if(document.getElementById(form.id + ':cboTipoRejeicaoAcolhimento').value == null || document.getElementById(form.id + ':cboTipoRejeicaoAcolhimento').value == 0) {
	    campos = campos + msgCampo + ' ' + msgTipoRejeicaoAcolhimento + ' ' + msgNecessario + '!' + '\n';
	}
	
	var objRadioFiltro = document.getElementsByName('radioFiltro');
	if(document.getElementById(form.id + ':cboTipoRejeicaoAcolhimento').value == 3 && !objRadioFiltro[0].checked && !objRadioFiltro[1].checked){
		campos = campos + msgCampo + ' ' + msgQuantidadeRegistrosInconsistentes  + ' '  + msgOu  + ' '  + msgPercentualRegistrosInconsistentes  +  ' ' + msgNecessario + '!' + '\n';
	}else{
		if(document.getElementById(form.id + ':cboTipoRejeicaoAcolhimento').value == 3 && objRadioFiltro[0].checked){
			if (allTrim(document.getElementById(form.id + ':txtQuantidadeRegistrosInconsistentes')) == '') {
			    campos = campos + msgCampo + ' ' + msgQuantidadeRegistrosInconsistentes + ' ' + msgNecessario + '!' + '\n';
			}
		}else{
			if(document.getElementById(form.id + ':cboTipoRejeicaoAcolhimento').value == 3 && objRadioFiltro[1].checked){
				if (allTrim(document.getElementById(form.id + ':txtPercentualRegistrosInconsistentes')) == '') {
				    campos = campos + msgCampo + ' ' + msgPercentualRegistrosInconsistentes + ' ' + msgNecessario + '!' + '\n';
				}else{
				    valorTemp = parseFloat(document.getElementById(form.id + ':txtPercentualRegistrosInconsistentes').value.replace(',','.'));
				    if (valorTemp == '0.00'){
					    campos = campos + msgCampo + ' ' + msgPercentualRegistrosInconsistentes + ' ' + msgNecessario + '!' + '\n';
				    }
				}
			}
		}
	}
	
	if(document.getElementById(form.id + ':cboNivelControleRemessa').value == null || document.getElementById(form.id + ':cboNivelControleRemessa').value == 0) {
	    campos = campos + msgCampo + ' ' + msgNivelControle + ' (' + msgRemessa + ') ' + msgNecessario + '!' + '\n';
	}
	
	
	if(document.getElementById(form.id + ':cboPeriodicidadeInicializacaoContagemRemessa').value == null || document.getElementById(form.id + ':cboPeriodicidadeInicializacaoContagemRemessa').value == 0) {
	    campos = campos + msgCampo + ' ' + msgPeriodicidadeInicializacaoContagem + ' (' + msgRemessa + ') ' + msgNecessario + '!' + '\n';
	}
	
	
	if(document.getElementById(form.id + ':cboMeioTransmissaoPrincipalRemessa').value == null || document.getElementById(form.id + ':cboMeioTransmissaoPrincipalRemessa').value == 0) {
	    campos = campos + msgCampo + ' ' + msgMeioTransmissaoPrincipal + ' (' + msgRemessa + ') ' + msgNecessario + '!' + '\n';
	}
	
	
	if(document.getElementById(form.id + ':cboNivelControleRetorno').value == null || document.getElementById(form.id + ':cboNivelControleRetorno').value == 0) {
	    campos = campos + msgCampo + ' ' + msgNivelControle + ' (' + msgRetorno + ') ' + msgNecessario + '!' + '\n';
	}
	
	
	if(document.getElementById(form.id + ':cboPeriodicidadeInicializacaoContagemRetorno').value == null || document.getElementById(form.id + ':cboPeriodicidadeInicializacaoContagemRetorno').value == 0) {
	    campos = campos + msgCampo + ' ' + msgPeriodicidadeInicializacaoContagem + ' (' + msgRetorno + ') ' + msgNecessario + '!' + '\n';
	}
	
	
	if(document.getElementById(form.id + ':cboMeioTransmissaoPrincipalRetorno').value == null || document.getElementById(form.id + ':cboMeioTransmissaoPrincipalRetorno').value == 0) {
	    campos = campos + msgCampo + ' ' + msgMeioTransmissaoPrincipal + ' (' + msgRetorno + ') ' + msgNecessario + '!' + '\n';
	}
	
	//validando combo Tipo Controle (se a op��o selecionada for diferente de "Sem controle", � obrigat�rio)
	if(document.getElementById(form.id + ':cboNivelControleRetorno').value != 3){
	
		if(document.getElementById(form.id + ':cboTipoControleRetorno').value == null || document.getElementById(form.id + ':cboTipoControleRetorno').value == 0) {
		    campos = campos + msgCampo + ' ' + msgTipoDeControle + ' (' + msgRetorno + ') ' + msgNecessario + '!' + '\n';
		}
	}

	//validando combo Tipo Controle (se a op��o selecionada for diferente de "Sem controle", � obrigat�rio)
	if(document.getElementById(form.id + ':cboNivelControleRemessa').value != 3){
	
		if(document.getElementById(form.id + ':cboTipoControleRemessa').value == null || document.getElementById(form.id + ':cboTipoControleRemessa').value == 0) {
		    campos = campos + msgCampo + ' ' + msgTipoDeControle + ' (' + msgRemessa + ') ' + msgNecessario + '!' + '\n';
		}
	}

	if (campos != ''){
		alert(campos);
		return false;
	}

	return true;	
}

function validaCamposAvancarAlterar(form, msgcampo, msgnecessario, msgnivelcontrole, msgtipocontrole, msgperiodicidade,msgMeioTransmissaoPrincipalRemessa,
		msgMeioTransmissaoPrincipalRetorno,msgsEmpresaResponsavelTransmissao,msgsResponsavelCustoTransmissaoArquivo,msgsVolumeMensalRegistrosTrafegados,
		msgsNomeCliente,msgsDDDCliente,msgsTelefoneCliente,msgsRamalCliente,msgsEmailCliente,msgsNomeSolicitante,msgsDDDSolicitante,msgsTelefoneSolicitante,msgsRamalSolicitante,msgsEmailSolicitante,
		msgsJustificativa,msgsJuncaoRespCustoTransm,msgsPercentualCustoTransmissaoArquivo){

	var msgValidacao = '';
	
	if ( document.getElementById(form.id +':cboNivelControleRemessa').value == null || document.getElementById(form.id +':cboNivelControleRemessa').value < 1 ) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgnivelcontrole + ' ( Remessa  ) ' + msgnecessario + '!\n';
	} else if ( document.getElementById(form.id +':cboNivelControleRemessa').value == 1 || document.getElementById(form.id +':cboNivelControleRemessa').value == 2 ) {
		if ( document.getElementById(form.id +':cboTipoControleRemessa').value == null || document.getElementById(form.id +':cboTipoControleRemessa').value < 1 ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipocontrole  + ' ( Remessa  ) ' +  msgnecessario + '!\n';
		}
	}

	if ( document.getElementById(form.id +':cboNivelControleRetorno').value == null || document.getElementById(form.id +':cboNivelControleRetorno').value < 1 ) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgnivelcontrole  + ' ( Retorno  ) ' +  msgnecessario + '!\n';
	} else if ( document.getElementById(form.id +':cboNivelControleRetorno').value == 1 || document.getElementById(form.id +':cboNivelControleRetorno').value == 2 ) {
		if ( document.getElementById(form.id +':cboTipoControleRetorno').value == null || document.getElementById(form.id +':cboTipoControleRetorno').value < 1 ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipocontrole  + ' ( Retorno  ) ' +  msgnecessario + '!\n';
		}
	}

	if ( document.getElementById(form.id +':cboPeriodicidadeInicializacaoContagemRemessa').value == null || document.getElementById(form.id +':cboPeriodicidadeInicializacaoContagemRemessa').value == '0' ) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgperiodicidade + ' ' + msgnecessario + '!\n';
	}

	if ( document.getElementById(form.id +':cboPeriodicidadeInicializacaoContagemRetorno').value == null || document.getElementById(form.id +':cboPeriodicidadeInicializacaoContagemRetorno').value == '0' ) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgperiodicidade + ' ' + msgnecessario + '!\n';
	}

	if ( document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value == null || document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value < 1 ) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgMeioTransmissaoPrincipalRemessa + ' ' + msgnecessario + '!\n';
	}

	if ( document.getElementById(form.id +':cboMeioTransmissaoPrincipalRetorno').value == null || document.getElementById(form.id +':cboMeioTransmissaoPrincipalRetorno').value < 1 ) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgMeioTransmissaoPrincipalRetorno + ' ' + msgnecessario + '!\n';
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validarPercentualBanco(form) {
	var banco = document.getElementById(form +':txtBanco');
	var cliente = document.getElementById(form +':txtCliente');
	var percentual = banco.value.replace(",", ".");

	if(parseFloat(percentual) > 100) {
		alert("O campo Banco n\u00e3o pode ser maior que 100!");
		banco.value = "";
		cliente.value = "";
		banco.focus();
		return false;
	}
}

function selecionarTodos(form, check){
	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('tableRepresentantes_') != -1 ){		
			form.elements[i].checked = check.checked;
			
		}	
	}	
}
