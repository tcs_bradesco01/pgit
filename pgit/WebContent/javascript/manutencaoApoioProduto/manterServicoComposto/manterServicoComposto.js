function validaCampos(form, msgcampo, msgnecessario, txtServicoComposto, txtDescServicoComposto, txtQtdeDias, rdoLancFuturo){
	
	var msgValidacao = '';
	var objRdoLancFuturo = document.getElementsByName('rdoLancFuturo');
	
	if(document.getElementById(form.id +':txtServicoComposto').value== "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + txtServicoComposto + ' ' + msgnecessario + '!\n';
	}		
	if(document.getElementById(form.id +':txtDescServicoComposto').value== "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + txtDescServicoComposto + ' ' + msgnecessario + '!\n';
	}
	
	var qtdeDias = document.getElementById(form.id +':txtQtdeDias');
	
	if(!qtdeDias.disabled){
		if(qtdeDias.value == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + txtQtdeDias + ' ' + msgnecessario + '!\n';
		}		
		if(!objRdoLancFuturo[0].checked && !objRdoLancFuturo[1].checked ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + rdoLancFuturo + ' ' + msgnecessario + '!\n';
		}		
		
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaCamposAlteracao(form, msgcampo, msgnecessario, txtServicoComposto, txtDescServicoComposto, txtQtdeDias, rdoLancFuturo){
	
	var msgValidacao = '';
	var objRdoLancFuturo = document.getElementsByName('rdoLancFuturo');
	
	if(document.getElementById(form.id +':txtServicoComposto').value== "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + txtServicoComposto + ' ' + msgnecessario + '!\n';
	}		
	if(document.getElementById(form.id +':txtDescServicoComposto').value== "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + txtDescServicoComposto + ' ' + msgnecessario + '!\n';
	}
	
	var qtdeDias = document.getElementById(form.id +':txtQtdeDias');
	
	if(qtdeDias != null){
		if(qtdeDias.value == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + txtQtdeDias + ' ' + msgnecessario + '!\n';
		}		
		if(!objRdoLancFuturo[0].checked && !objRdoLancFuturo[1].checked ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + rdoLancFuturo + ' ' + msgnecessario + '!\n';
		}		
		
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaValorDias(obj, msgValidacao) {
	
	if (allTrim(obj) != ''){
		if(parseInt(obj.value, 10) > 365){
			alert(msgValidacao);
			
			obj.value = '';
			
			return false;
		}
	}
	
	
	return true;
}