function validaCpoConsultaCliente(msgcampo, msgnecessario, msgcnpj, msgcpf, msgnome, msgbanco, msgagencia, msgconta){

var objRdoCliente = document.getElementsByName('rdoFiltroCliente');

	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtCnpj')) == "") {
			alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
			document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtCnpj').focus();
			return false;		
		} else {
			if (allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtFilial')) == "") {
				alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
				document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtFilial').focus();
				return false;		
			} else {
				if (allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtControle'))== ""){
					alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
					document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtControle').focus();
					return false;		
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtCpf'))== "") {
			alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
			document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtCpf').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtControleCpf')) == "") {
				alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
				document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtControleCpf').focus();
				return false;	
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtNomeRazaoSocial')) == "") {
			alert(msgcampo + ' ' + msgnome + ' ' + msgnecessario);
			document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtNomeRazaoSocial').focus();
			return false;	
		}
	} else if (objRdoCliente[3].checked) {
		
		if (allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtBanco')) == "") {
			campo = msgcampo + ' ' + msgbanco + ' ' + msgnecessario;
			if(allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtAgencia')) == "") {
				campo += "\n" + msgcampo + ' ' + msgagencia + ' ' + msgnecessario
				if(allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtConta')) == "") {	
					campo += "\n" + msgcampo + ' ' + msgconta + ' ' + msgnecessario;
				}			
			}
			alert (campo);
			document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtBanco').focus();
			return false;		
		} else { 
			if(allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtAgencia')) == "") {
				campo = msgcampo + ' ' + msgagencia + ' ' + msgnecessario;
				if(allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtConta')) == "") {	
					campo += "\n" + msgcampo + ' ' + msgconta + ' ' + msgnecessario;
				}	
				alert (campo);
				document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtAgencia').focus();
				return false;		
			} else {
				if(allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtConta')) == "") {
					alert(msgcampo + ' ' + msgconta + ' ' + msgnecessario);
					document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtConta').focus();
					return false;		
				}
			}
		}		
	} 
	
	return true;						
}

function validaCpoContrato(msgcampo, msgnecessario,msgempresa,msgtipo, msgnumero, cliente){
	var msgValidacao = '';

	if ((cliente == null) || (cliente ==  '')){
		if (document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
		
		if (allTrim(document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:txtNumeroContrato'))== "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' ' + msgnecessario + '\n';
		} 
	
	}else{
		if (document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('manterVincMsgHistoricoComplementarOperacaoContratadaForm:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
	}	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}

function confirmar(form, msgConfirma){
	
		var hiddenConfirm;
		
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id + ':hiddenConfirma'){	
				hiddenConfirm = form.elements[i];
				break;
			}
		}
		
		hiddenConfirm.value=confirm(msgConfirma);
	
}