	function validarHora(campo, msgHorarioInvalido){
		var msgValidacao = '';
		
		if(campo.value != ''){
			hora = (allTrim(campo)).split(":");
			if (hora.length != 3){
				msgValidacao = msgValidacao + msgHorarioInvalido + '\n';
			}else{
				if(isNaN(hora[0]) || hora[0] > 23){
					msgValidacao = msgValidacao + msgHorarioInvalido + '\n';
				}
				else if(isNaN(hora[1]) || hora[1] > 59){
					msgValidacao = msgValidacao + msgHorarioInvalido + '\n';
				}
				else if(isNaN(hora[2]) || hora[2] > 59){
					msgValidacao = msgValidacao + msgHorarioInvalido + '\n';
				}
			}
		}
	
		if (msgValidacao != ''){
			alert(msgValidacao);
			campo.value = '';
			campo.focus();
			return false;
		}
	
		return true;
	}