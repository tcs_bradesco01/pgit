function validaManterOpServRelacionadoInc(form, msgcampo, msgnecessario, msgservico, msgmodalidade, msgoperacao, msgcodigooperacao, msgcodnatureza,
		msgvalor, msgpercentual){
	
	var msgValidacao = '';
	
	if(document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0'){	
		msgValidacao = msgValidacao + msgcampo + ' ' + msgservico + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if(document.getElementById(form.id +':cboOperacao').value == null || document.getElementById(form.id +':cboOperacao').value == '0'){	
		msgValidacao = msgValidacao + msgcampo + ' ' + msgoperacao + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if((allTrim(document.getElementById(form.id +':txtCodigoOperacao')) == '')){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigooperacao + ' ' + msgnecessario + '!' + '\n';	
	}
	
	var objRdoCliente = document.getElementsByName('rdoTarifavelContabil');
	
	if ( !objRdoCliente[1].checked && !objRdoCliente[2].checked ){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgcodnatureza + ' ' + msgnecessario + '!' + '\n';
	}
	
	var objRdoTarifaAlcadaAgencia = document.getElementsByName('rdoTarifaAlcadaAgencia');
	if ( objRdoTarifaAlcadaAgencia != null && objRdoTarifaAlcadaAgencia[0].checked){		
		if((allTrim(document.getElementById(form.id +':txtValorTarifa')) == '')){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgvalor + ' ' + msgnecessario + '!' + '\n';	
		}
	}
	
	if ( objRdoTarifaAlcadaAgencia != null && objRdoTarifaAlcadaAgencia[1].checked){
		if((allTrim(document.getElementById(form.id +':txtPercentualTarifa')) == '')){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgpercentual + ' ' + msgnecessario + '!' + '\n';	
		}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;	
	
}

function validaManterOpServRelacionadoAlt(form, msgcampo, msgnecessario, msgcodigooperacao, msgcodnatureza,
		msgvalor, msgpercentual){
	
	var msgValidacao = '';
	
	var objRdoCliente = document.getElementsByName('rdoTarifavelContabil');
	
	if ( !objRdoCliente[1].checked && !objRdoCliente[2].checked ){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgcodnatureza + ' ' + msgnecessario + '!' + '\n';
	}
	
	var objRdoTarifaAlcadaAgencia = document.getElementsByName('rdoTarifaAlcadaAgencia');
	if ( objRdoTarifaAlcadaAgencia != null && objRdoTarifaAlcadaAgencia[0].checked){		
		if((allTrim(document.getElementById(form.id +':txtValorTarifa')) == '')){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgvalor + ' ' + msgnecessario + '!' + '\n';	
		}
	}
	
	if ( objRdoTarifaAlcadaAgencia != null && objRdoTarifaAlcadaAgencia[1].checked){
		if((allTrim(document.getElementById(form.id +':txtPercentualTarifa')) == '')){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgpercentual + ' ' + msgnecessario + '!' + '\n';	
		}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;	
	
}