function validaCamposConsulta(form, msgcampo, msgnecessario, msgcnpj, msgnome, msgagencia, msgconta, msgdigito,msgcnpjincorreto,msgDiferenteZeros){

	var msgValidacao = '';

	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	if (objRdoCliente[0].checked) {
		if(allTrim(document.getElementById(form.id + ':txtCnpj')) == "" && allTrim(document.getElementById(form.id +':txtFilial')) == "" && allTrim(document.getElementById(form.id + ':txtControle'))  == ""){
				msgValidacao = msgValidacao +   msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';	
		}else{
			if(allTrim(document.getElementById(form.id + ':txtCnpj')) == "" || allTrim(document.getElementById(form.id +':txtFilial')) == "" || allTrim(document.getElementById(form.id + ':txtControle'))  == ""){
				msgValidacao = msgValidacao + msgcnpjincorreto + '!\n';	
			}else{
				if((parseInt(allTrim(document.getElementById(form.id +':txtCnpj')),10) == 0) || (parseInt(allTrim(document.getElementById(form.id +':txtFilial')),10) == 0)){
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' ' + msgDiferenteZeros + '!' + '\n';				
				}			
			}
		}	
		
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById(form.id +':txtNomeRazaoSocial')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnome + ' ' + msgnecessario + '!\n';
			
		}
	} else if (objRdoCliente[2].checked) {
		if(allTrim(document.getElementById(form.id +':txtAgencia'))== "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '!\n';
		}else{
			if(parseInt(allTrim(document.getElementById(form.id +':txtAgencia')),10) == 0){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgDiferenteZeros + '!' + '\n';				
			}			
		}			
		if(allTrim(document.getElementById(form.id +':txtConta')) == "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgconta + ' ' + msgnecessario + '!\n';					
		}else{
			if(parseInt(allTrim(document.getElementById(form.id +':txtConta')),10) == 0){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgDiferenteZeros + '!' + '\n';				
			}			
		}		
		if(allTrim(document.getElementById(form.id +':txtDigConta')) == "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgdigito + ' ' + msgnecessario + '!\n';					
		}
	} 
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}
function validaCamposManutencao(form, msgcampo, msgnecessario, msgcnpj, msgnome, msgagencia, msgconta, msgdigito,msgcnpjincorreto,msgDiferenteZeros, tela, msgConvenioSalario, msgRadio){
	
	var msgValidacao = '';

	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	
	if(tela=="Incluir"){
		if(allTrim(document.getElementById(form.id + ':txtCnpj')) == "" && allTrim(document.getElementById(form.id +':txtFilial')) == "" && allTrim(document.getElementById(form.id + ':txtControle'))  == ""){
			msgValidacao = msgValidacao +   msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';	
		}else{
			if(allTrim(document.getElementById(form.id + ':txtCnpj')) == "" || allTrim(document.getElementById(form.id +':txtFilial')) == "" || allTrim(document.getElementById(form.id + ':txtControle'))  == ""){
				msgValidacao = msgValidacao + msgcnpjincorreto + '!\n';	
			}else{
				if((parseInt(allTrim(document.getElementById(form.id +':txtCnpj')),10) == 0) || (parseInt(allTrim(document.getElementById(form.id +':txtFilial')),10) == 0)){
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' ' + msgDiferenteZeros + '!' + '\n';				
				}			
			}
		}	
	}
	if (allTrim(document.getElementById(form.id +':txtNomeRazaoSocial')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgnome + ' ' + msgnecessario + '!\n';
		
	}
	if(allTrim(document.getElementById(form.id +':txtAgencia'))== "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '!\n';
	}else{
		if(parseInt(allTrim(document.getElementById(form.id +':txtAgencia')),10) == 0){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgDiferenteZeros + '!' + '\n';				
		}			
	}			
	if(allTrim(document.getElementById(form.id +':txtConta')) == "") {
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgconta + ' ' + msgnecessario + '!\n';					
	}else{
		if(parseInt(allTrim(document.getElementById(form.id +':txtConta')),10) == 0){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgDiferenteZeros + '!' + '\n';				
		}			
	}		
	if(allTrim(document.getElementById(form.id +':txtDigConta')) == "") {
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgdigito + ' ' + msgnecessario + '!\n';					
	}

	if(allTrim(document.getElementById(form.id +':txtConvenioSalario')) == "") {
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgConvenioSalario + ' ' + msgnecessario + '!\n';					
	}else{
		if(parseInt(allTrim(document.getElementById(form.id +':txtConvenioSalario')),10) == 0){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgConvenioSalario + ' ' + msgDiferenteZeros + '!' + '\n';				
		}			
	}		
	
	
	if (!objRdoCliente[0].checked && !objRdoCliente[1].checked) {
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgRadio + ' ' + msgnecessario + '!\n';
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;						
}

function validarProxCampoIdentClienteContrato(form){
	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoNomeRazao = document.getElementById(form.id + ':txtNomeRazaoSocial');
	var campoBancoAgConta = document.getElementById(form.id + ':txtAgencia');
	
	if(objRdoCliente[0].checked){
		campoCnpj.focus();
		return true;
	}else if(objRdoCliente[1].checked){
		campoNomeRazao.focus();
		return true;
	}else if(objRdoCliente[2].checked){
		campoBancoAgConta.focus();
		return true;
	}else{
		return false;
	}
}