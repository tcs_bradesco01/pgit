function verificaIncluir(form, msgcampo, msgnecessario, campo1){
		var campos = '';
		var hidden;
		
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id + ':hiddenObrigatoriedade'){
				hidden = form.elements[i];
			}		

			if (form.elements[i].id == form.id  + ':selTipoMsg'){
				if (form.elements[i].value == '0'){
					campos = campos + msgcampo + " " +  campo1 + " " + msgnecessario;
				}
				
			}
			
		}	
		
		if (campos != ''){
			hidden.value  ='F';
			alert(campos);
		}else{
			hidden.value  ='T';
		}
	
	}
	
function validaCpoConsultaCliente(msgcampo, msgnecessario, msgcnpj, msgcpf, msgnome, msgbanco, msgagencia, msgconta){

var objRdoCliente = document.getElementsByName('rdoFiltroCliente');

	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtCnpj')) == "") {
			alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
			document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtCnpj').focus();
			return false;		
		} else {
			if (allTrim(document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtFilial')) == "") {
				alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
				document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtFilial').focus();
				return false;		
			} else {
				if (allTrim(document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtControle')) == ""){
					alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
					document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtControle').focus();
					return false;		
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtCpf')) == "") {
			alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
			document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtCpf').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtControleCpf')) == "") {
				alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
				document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtControleCpf').focus();
				return false;	
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtNomeRazaoSocial')) == "") {
			alert(msgcampo + ' ' + msgnome + ' ' + msgnecessario);
			document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtNomeRazaoSocial').focus();
			return false;	
		}
	} else if (objRdoCliente[3].checked) {
		if (allTrim(document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtBanco')) == "") {
			alert(msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '\n' + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n' + msgcampo + ' ' + msgconta + ' ' + msgnecessario);
			document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtBanco').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtAgencia')) == "") {
				alert(msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '\n' + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n' + msgcampo + ' ' + msgconta + ' ' + msgnecessario);
				document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtAgencia').focus();
				return false;		
			} else {
				if(allTrim(document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtConta')) == "") {
					alert(msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '\n' + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n' + msgcampo + ' ' + msgconta + ' ' + msgnecessario);
					document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtConta').focus();
					return false;		
				}
			}
		}
	} 
	
	return true;						
}

function validaCpoContrato(msgcampo, msgnecessario,msgempresa,msgtipo, msgnumero, cliente){
	var msgValidacao = '';

	if ((cliente == null) || (cliente ==  '')){
		if (document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
		
		if (allTrim(document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtNumeroContrato')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' ' + msgnecessario + '\n';
		} 
	
	}else{
		if (document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
	}	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}

function focoAgencias(){
	var objRdoAgencia = document.getElementsByName('rdoFiltroCliente');
	
	
	if (objRdoAgencia[0].checked == true) {
		document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtCnpj').focus();
	}	
	if (objRdoAgencia[1].checked == true) {
		document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtCpf').focus();
	}	
	if (objRdoAgencia[2].checked == true) {
		document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtNomeRazaoSocial').focus();
	}	
	if (objRdoAgencia[3].checked == true) {
		document.getElementById('pesqManterAssociacaoMensagemComprovanteSalarial:txtAgencia').focus();
	}	
	return true;						
}