

function checaCampoObrigatorioAlterar(form, msgcampo, msgnecessario, msgdescobjetivo, msgtipocomprovante, msgtipomensagem, msgrestricao){
	campos='';
	var hidden;	
	
	for(i=0; i<form.elements.length; i++){		
							
			if (form.elements[i].id == 'alterarManterMensagemComprovanteSalarialForm:hiddenObrigatoriedade'){ 
					hidden = form.elements[i];
			}						

			
			if (form.elements[i].id == 'alterarManterMensagemComprovanteSalarialForm:descricao'){			
					if (form.elements[i].value == ''){					
						campos = campos + msgcampo + ' ' + msgdescobjetivo + ' ' + msgnecessario + '\n';						
					}
			}
			
			
			if (form.elements[i].id == 'alterarManterMensagemComprovanteSalarialForm:acao'){			
					if (form.elements[i].value == ''){					
						campos = campos + msgcampo + ' ' + msgtipocomprovante + ' ' + msgnecessario + '\n';						
					}
			}
			
			if (form.elements[i].name == 'alterarManterMensagemComprovanteSalarialForm:hiddenTipoMensagem'){						
					
					if (form.elements[i].value != 1 && form.elements[i].value != 2){						
						campos = campos + msgcampo + ' ' + msgtipomensagem + ' ' + msgnecessario + '\n';						
					}
			}								

			if (form.elements[i].id == 'alterarManterMensagemComprovanteSalarialForm:indicadorRestricao'){						
					if (form.elements[i].value == ''){					
						campos = campos + msgcampo + ' ' + msgrestricao + ' ' + msgnecessario;						
					}
			}
	}	
						
	if (campos != ''){
		hidden.value  = 'F';
		alert(campos);
	}
	else
		hidden.value  = 'T';				
			
}



function checaCampoObrigatorioIncluir(form, msgcampo, msgnecessario,msgCodigoTipoMensagem, msgdescobjetivo, msgtipocomprovante, msgtipomensagem, msgrestricao){
	var campos='';
	var hidden;
	for(i=0; i<form.elements.length; i++){		

			if (form.elements[i].id == 'incluirManterMensagemComprovanteSalarialForm:hiddenObrigatoriedade'){ 
					hidden = form.elements[i];
			}						

			
			if (form.elements[i].id == 'incluirManterMensagemComprovanteSalarialForm:codigoTipoMensagem'){			
					if (form.elements[i].value == ''){					
						campos = campos + msgcampo + ' ' + msgCodigoTipoMensagem + ' ' + msgnecessario + '\n';						
					}
			}
			
			if (form.elements[i].id == 'incluirManterMensagemComprovanteSalarialForm:descricao'){			
					if (form.elements[i].value == ''){					
						campos = campos + msgcampo + ' ' + msgdescobjetivo + ' ' + msgnecessario + '\n';						
					}
			}
			
			
			if (form.elements[i].id == 'incluirManterMensagemComprovanteSalarialForm:acao'){			
					if (form.elements[i].value == ''){					
						campos = campos + msgcampo + ' ' + msgtipocomprovante + ' ' + msgnecessario + '\n';						
					}
			}
			
			if (form.elements[i].name == 'incluirManterMensagemComprovanteSalarialForm:hiddenTipoMensagem'){						
					
					if (form.elements[i].value != 1 && form.elements[i].value != 2){						
						campos = campos + msgcampo + ' ' + msgtipomensagem + ' ' + msgnecessario + '\n';						
					}
			}								

			if (form.elements[i].id == 'incluirManterMensagemComprovanteSalarialForm:indicadorRestricao'){						
					if (form.elements[i].value == ''){					
						campos = campos + msgcampo + ' ' + msgrestricao + ' ' + msgnecessario;						
					}
			}
	}	
						
	if (campos != ''){
		hidden.value  = 'F';
		alert(campos);
	}
	else
		hidden.value  = 'T';				
			
}