
function validarInclusao(form,msgCampo, msgNecessario, msgTipoRelatorio, msgTipoServico, msgModalidadeServico, msgEmpresa, msgDiretoriaRegional, msgGerenciaRegional,
	msgAgenciaOperadora, msgSegmento, msgParticipante, msgClasse, msgRamo, msgSubRamo, msgAtividadeEconomica, msgCheck, msgCriterioSelecaoContratos, msgNivelConsolidacao,	msgAgrupamentoClientes ){

	var campos = '';
	var flagCriterioSelecaoContrato = false;
	var flagNivelConsolidacao = false;
	var flagAgrupamentoClientes = false;
	
	
	
	if ((document.forms[form.id].elements[form.id + ':tipoRelatorio'][0].checked == false) && (document.forms[form.id].elements[form.id + ':tipoRelatorio'][1].checked == false)){
		campos = campos + msgCampo + ' ' + msgTipoRelatorio + ' ' + msgNecessario + '\n';
	}
	
	

	
	if (form.elements[form.id + ':chkTipoServico'].checked == true){
		
		flagCriterioSelecaoContrato = true;
	
		if (form.elements[form.id + ':tipoServico'].value == 0){
		
			campos = campos + msgCampo + ' ' + msgTipoServico + ' ' + msgNecessario + '\n';
		}
		

	}
	
	if (form.elements[form.id + ':chkModalidadeServico'].checked == true){
	
		flagCriterioSelecaoContrato = true;
		
		if (form.elements[form.id + ':modalidadeServico'].value == 0){
		
			campos = campos + msgCampo + ' ' + msgModalidadeServico + ' ' + msgNecessario + '\n';
		}
		

	}
	
	if (form.elements[form.id + ':chkEmpresaConglomerado'].checked == true){
	
		flagNivelConsolidacao = true;
		
		if (form.elements[form.id + ':empresaConglomerado'].value == 0){
		
			campos = campos + msgCampo + ' ' + msgEmpresa + ' ' + msgNecessario + '\n';
		}
		

	}
	
	
	if (form.elements[form.id + ':chkDiretoriaRegional'].checked == true){
	
		flagNivelConsolidacao = true;
		
		if (form.elements[form.id + ':diretoriaRegional'].value == 0){
		
			campos = campos + msgCampo + ' ' + msgDiretoriaRegional + ' ' + msgNecessario + '\n';
		}
		

	}
	
	
	if (form.elements[form.id + ':chkGerenciaRegional'].checked == true){
	
		flagNivelConsolidacao = true;
		
		if (form.elements[form.id + ':gerenciaRegional'].value == 0){
		
			campos = campos + msgCampo + ' ' + msgGerenciaRegional + ' ' + msgNecessario + '\n';
		}
		

	}
	
	
	
	if (form.elements[form.id + ':chkAgenciaOperacao'].checked == true){
	
		flagNivelConsolidacao = true;
		
		if (form.elements[form.id + ':agenciaOperadora'].value == ''){
		
			campos = campos + msgCampo + ' ' + msgAgenciaOperadora + ' ' + msgNecessario + '\n';
		}
		

	}
	
	if (form.elements[form.id + ':chkSegmento'].checked == true){
	
		flagAgrupamentoClientes = true;
		
		if (form.elements[form.id + ':segmento'].value == 0){
		
			campos = campos + msgCampo + ' ' + msgSegmento + ' ' + msgNecessario + '\n';
		}
		

	}
	
	if (form.elements[form.id + ':chkParticipanteGrupoEconomico'].checked == true){
	
		flagAgrupamentoClientes = true;
		
		if (form.elements[form.id + ':participanteGrupoEconomico'].value ==  null  || form.elements[form.id + ':participanteGrupoEconomico'].value == ''){
		
			campos = campos + msgCampo + ' ' + msgParticipante+ ' ' + msgNecessario + '\n';
		}
		

	}
	
	
	if (form.elements[form.id + ':chkClasse'].checked == true){
	
		flagAgrupamentoClientes = true;
		
		if (form.elements[form.id + ':classe'].value == ''){
		
			campos = campos + msgCampo + ' ' + msgClasse + ' ' + msgNecessario + '\n';
		}
		
		if (form.elements[form.id + ':ramo'].value == ''){
		
			campos = campos + msgCampo + ' ' + msgRamo + ' ' + msgNecessario + '\n';
		}
		if (form.elements[form.id + ':subRamo'].value == ''){
		
			campos = campos + msgCampo + ' ' + msgSubRamo + ' ' + msgNecessario + '\n';
		}

		if (form.elements[form.id + ':atividadeEconomica'].value == ''){
		
			campos = campos + msgCampo + ' ' + msgAtividadeEconomica + ' ' + msgNecessario + '\n';
		}
		
	}
	
	
	
	if (!flagCriterioSelecaoContrato){	
		campos = campos + msgCheck + ' ' + msgCriterioSelecaoContratos + '.' +  '\n';	
	}
	if (!flagNivelConsolidacao){	
		campos = campos + msgCheck +  ' ' + msgNivelConsolidacao + '.' +  '\n';	
	}
	if (!flagAgrupamentoClientes){	
		campos = campos + msgCheck +   ' ' + msgAgrupamentoClientes + '.' + '\n';	
	}
	

	if (campos != ''){		
		alert(campos);
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'F';
	}else{
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'T';
	}
	
}

function validarCampo(msgdsagencia){ 

var conglomerado = document.getElementById('manterSolicitacaoRelatorioContratoIncluir:empresaConglomerado').value;
var diretoria = document.getElementById('manterSolicitacaoRelatorioContratoIncluir:diretoriaRegional').value;
var gerencia = document.getElementById('manterSolicitacaoRelatorioContratoIncluir:gerenciaRegional').value;

	if(document.getElementById('manterSolicitacaoRelatorioContratoIncluir:chkEmpresaConglomerado').checked == false ){
		alert('Necessario informar pelo menos 1 (um) Nivel de Consolidac\u00e3o!');
		return false; 
	}

	if(document.getElementById('manterSolicitacaoRelatorioContratoIncluir:chkEmpresaConglomerado').checked == true && conglomerado == 0){
		alert('Selecione a Empresa Conglomerado!');
		return false; 
	}
		
	if(document.getElementById('manterSolicitacaoRelatorioContratoIncluir:chkDiretoriaRegional').checked == true && diretoria == 0){
		alert('Selecione a Diretoria Regional!');
		return false; 
	}
		
	if(document.getElementById('manterSolicitacaoRelatorioContratoIncluir:chkGerenciaRegional').checked == true && gerencia == 0){
		alert('Selecione a Gerencia Regional!');
		return false; 
	}
		
	if(document.getElementById('manterSolicitacaoRelatorioContratoIncluir:chkAgenciaOperacao').checked == true && document.getElementById('manterSolicitacaoRelatorioContratoIncluir:agenciaOperadora').value == ""){
		alert('Preencha o campo Ag\u00EAncia Operadora!');
		return false;
	}	
	
	if(document.getElementById('manterSolicitacaoRelatorioContratoIncluir:chkAgenciaOperacao').checked == true && document.getElementById('manterSolicitacaoRelatorioContratoIncluir:hiddenAgencia').value == "" || document.getElementById('manterSolicitacaoRelatorioContratoIncluir:hiddenAgencia').value == null){
		alert('Preencha o campo ' + msgdsagencia + '!');
		return false;
	}		
}

function validaAgenciaOperadora(){ 

	if(document.getElementById('manterSolicitacaoRelatorioContratoIncluir:agenciaOperadora').value == ""){
		alert('Preencha o campo Ag\u00EAncia Operadora!');
		return false;
	}	
	
	return true;
}

function validaGrupoEconomico(){ 

	if(document.getElementById('manterSolicitacaoRelatorioContratoIncluir:participanteGrupoEconomico').value == ""){
		alert('Preencha o campo Grupo Economico!');
		return false;
	}	
	
	return true;
}