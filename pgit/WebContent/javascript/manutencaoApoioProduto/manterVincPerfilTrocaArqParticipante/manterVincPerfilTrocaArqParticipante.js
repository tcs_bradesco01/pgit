function validarConsulta(form, msgCampo, msgNecessario, participante, msgCodigoPerfil, msgTipoLayout, msgParticipante, msgArgumentos) {
	var objRdo = document.getElementsByName('rdoFiltro');
	var campos = '';

	if (objRdo[0].checked) {
		var codPerfil = document.getElementById(form.id + ':txtCodigoPerfil');
		var tipoLayout = document.getElementById(form.id + ':cboTipoLayout');

		if(codPerfil.value == '' && tipoLayout.value == '0') {
			campos = 'Preencha ao menos um campo!\n';
		}
	} else if (objRdo[1].checked) {
		if (participante == '') {
		    campos = campos + msgCampo + ' ' + msgParticipante + ' ' + msgNecessario + '!' + '\n';
		}
	}

	if (campos != '') {
		alert(campos);
		return false;
	}

	return true;	
}

function validarInclusao(msgCampo, msgNecessario, perfil, participante, msgPerfil, msgParticipante, msgContratoMae, campoContratoMae1, campoContratoMae2, campoContratoMae3, campoContratoMae4){
	var campos = '';
	
	var contratoMae = document.getElementById('incManterVincPerfilTrocaArqParticipante:contratoMae').value;
	
	if (participante == '') {
	    campos = campos + msgCampo + ' ' + msgParticipante + ' ' + msgNecessario + '!' + '\n';
	}
	if (perfil == '') {
	    campos = campos + msgCampo + ' ' + msgPerfil + ' ' + msgNecessario + '!' + '\n';
	}
	
	if (contratoMae != '') {
		if(campoContratoMae1 == null || campoContratoMae2 == '' || campoContratoMae3 == null || campoContratoMae4 == ''){
			campos = campos + msgCampo + ' ' + msgParticipante + ' ' + msgNecessario + '!' + '\n';
		}
	}
	
	if (campos != ''){
		alert(campos);
		return false;
	}
	
	return true;	

}
 
function validaCpoConsultaCliente(msgcampo, msgnecessario, msgcnpj, msgcpf, msgnome, msgbanco, msgagencia, msgconta){

var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
var msgValidacao = '';

	if (objRdoCliente[0].checked) {
		if ( allTrim(document.getElementById('pesqVincPerfilTrocaArqPartForm:txtCnpj')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';	
		} else {
			if (allTrim(document.getElementById('pesqVincPerfilTrocaArqPartForm:txtFilial')) == "") {
				alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
				document.getElementById('pesqVincPerfilTrocaArqPartForm:txtFilial').focus();
				return false;		
			} else {
				if (allTrim(document.getElementById('pesqVincPerfilTrocaArqPartForm:txtControle')) == ""){
					alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
					document.getElementById('pesqVincPerfilTrocaArqPartForm:txtControle').focus();
					return false;		
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById('pesqVincPerfilTrocaArqPartForm:txtCpf')) == "") {
			alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
			document.getElementById('pesqVincPerfilTrocaArqPartForm:txtCpf').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById('pesqVincPerfilTrocaArqPartForm:txtControleCpf')) == "") {
				alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
				document.getElementById('pesqVincPerfilTrocaArqPartForm:txtControleCpf').focus();
				return false;	
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById('pesqVincPerfilTrocaArqPartForm:txtNomeRazaoSocial')) == "") {
			alert(msgcampo + ' ' + msgnome + ' ' + msgnecessario);
			document.getElementById('pesqVincPerfilTrocaArqPartForm:txtNomeRazaoSocial').focus();
			return false;	
		}
	} else if (objRdoCliente[3].checked){
		
		if (allTrim(document.getElementById('pesqVincPerfilTrocaArqPartForm:txtBanco')) == ""){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '!\n';
		}
		if  (allTrim(document.getElementById('pesqVincPerfilTrocaArqPartForm:txtAgencia')) == ""){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '!\n';
		}
		if (allTrim(document.getElementById('pesqVincPerfilTrocaArqPartForm:txtConta')) == ""){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgnecessario + '!\n';
					
		}
				
	}
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;						
}

function validaCpoContrato(msgcampo, msgnecessario,msgempresa,msgtipo, msgnumero, cliente){
	var msgValidacao = '';

	if ((cliente == null) || (cliente ==  '')){
		if (document.getElementById('pesqVincPerfilTrocaArqPartForm:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('pesqVincPerfilTrocaArqPartForm:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
		
		if ( allTrim(document.getElementById('pesqVincPerfilTrocaArqPartForm:txtNumeroContrato')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' ' + msgnecessario + '\n';
		} 
	
	}else{
		if (document.getElementById('pesqVincPerfilTrocaArqPartForm:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('pesqVincPerfilTrocaArqPartForm:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
	}	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}
 
function validaConsultaHistorico(form, msgcampo, msgnecessario, msgargumentopesquisa, msgdatapagamento, msgcodperfil, msgtipolayout, msgparticipante, participante){

	var msgValidacao = ''; 

	var objRdoCliente = document.getElementsByName('rdoFiltro');
	
	if ( !objRdoCliente[0].checked && !objRdoCliente[1].checked ){
		msgValidacao = msgargumentopesquisa + '!';	
	}
	
	if (document.getElementById(form.id + ':dataInicioManutencao.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicioManutencao.day')) == ''   ||
		document.getElementById(form.id + ':dataInicioManutencao.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicioManutencao.month')) == ''   ||
		document.getElementById(form.id + ':dataInicioManutencao.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicioManutencao.year')) == '' ||
		document.getElementById(form.id + ':dataFimManutencao.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataFimManutencao.day')) == ''   ||
		document.getElementById(form.id + ':dataFimManutencao.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataFimManutencao.month')) == ''   ||
		document.getElementById(form.id + ':dataFimManutencao.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataFimManutencao.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if(objRdoCliente[0].checked){
		if(allTrim(document.getElementById(form.id +':txtCodigoPerfil')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcodperfil + ' ' + msgnecessario + '!' + '\n';	
		}
		
		if ( document.getElementById(form.id +':cboTipoLayout').value == null || document.getElementById(form.id +':cboTipoLayout').value == '0' ){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipolayout + ' ' + msgnecessario + '!' + '\n';
		} 
	}
	
	if(objRdoCliente[1].checked){
		if (participante == '' || participante == null) {
		    msgValidacao = msgValidacao + msgcampo + ' ' + msgparticipante + ' ' + msgnecessario + '!' + '\n';
		}
	}

	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

