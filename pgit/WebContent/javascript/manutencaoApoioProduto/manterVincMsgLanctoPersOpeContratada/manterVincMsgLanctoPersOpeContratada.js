function validaCpoConsultaCliente(form){

var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
var campo = '';

	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById(form.id + ':txtCnpj')) == "") {
			alert("O campo CNPJ esta incompleto ou nao esta preenchido.");
			document.getElementById(form.id + ':txtCnpj').focus();
			return false;		
		} else {
			if (allTrim(document.getElementById(form.id + ':txtFilial')) == "") {
				alert("O campo CNPJ esta incompleto ou nao esta preenchido.");
				document.getElementById(form.id + ':txtFilial').focus();
				return false;		
			} else {
				if (allTrim(document.getElementById(form.id + ':txtControle')) == ""){
					alert("O campo CNPJ esta incompleto ou nao esta preenchido.");
					document.getElementById(form.id + ':txtControle').focus();
					return false;		
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById(form.id + ':txtCpf')) == "") {
			alert("O campo CPF esta incompleto ou nao esta preenchido.");
			document.getElementById(form.id + ':txtCpf').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById(form.id + ':txtControleCpf')) == "") {
				alert("O campo CPF esta incompleto ou nao esta preenchido.");
				document.getElementById(form.id + ':txtControleCpf').focus();
				return false;	
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById(form.id + ':txtNomeRazaoSocial')) == "") {
			alert("O campo Nome/Razao Social nao esta preenchido.");
			document.getElementById(form.id + ':txtNomeRazaoSocial').focus();
			return false;	
		}
	} else if (objRdoCliente[3].checked) {
		if (allTrim(document.getElementById('manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtBanco')) == "") {
			campo = "O campo BANCO nao esta preenchido.";
			if(allTrim(document.getElementById('manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtAgencia')) == "") {
				campo += "\nO campo AGENCIA nao esta preenchido."
				if(allTrim(document.getElementById('manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:txtConta')) == "") {
					campo += "\nO campo CONTA nao esta preenchido.";
				}
			}
			alert (campo);
			document.getElementById(form.id + ':txtBanco').focus();
			return false;	
		} else { 
			if(allTrim(document.getElementById(form.id + ':txtAgencia'))== "") {
				campo = "O campo AGENCIA nao esta preenchido.";
				if(allTrim(document.getElementById(form.id + ':txtConta')) == "") {	
					campo += "\nO campo CONTA nao esta preenchido.";
				}	
				alert (campo);
				document.getElementById(form.id + ':txtAgencia').focus();
				return false;		
			} else {
				if(allTrim(document.getElementById(form.id + ':txtConta')) == "") {
					alert("O campo CONTA nao esta preenchido.");
					document.getElementById(form.id + ':txtConta').focus();
					return false;		
				}
			}
		}
	} 
	
	return true;						
}

function validaCpoContrato(msgcampo, msgnecessario,msgempresa,msgtipo, msgnumero, cliente){
	var msgValidacao = '';

	if ((cliente == null) || (cliente ==  '')){
		if (document.getElementById('manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
		
			
	}else{
		if (document.getElementById('manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
	}	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}