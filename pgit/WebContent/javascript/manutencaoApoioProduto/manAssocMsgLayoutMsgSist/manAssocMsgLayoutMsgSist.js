function validarCentroCusto(form, id, msgCampo, msgNecessario, msgCentroCusto){
	if (allTrim(document.getElementById(form.id + ':' + id)) == ''){
		alert(msgCampo + ' ' + msgCentroCusto + ' ' + msgNecessario + '!' + '\n');
		return false;
	}
	else{
		return true;
	}
}

function validarPesquisa(form, msgCampo,msgNecessario,msgTipoLayoutArquivo,msgCodigoDaMensagem,msgCentroCusto,msgRecurso,msgIdioma,msgCodigoDoEvento,msgNecessarioSelecionarFiltroPesquisa){

	var campos = '';

	var radioTipoConsultaFiltro = document.getElementsByName('radioTipoConsultaFiltro');

	if(!radioTipoConsultaFiltro[0].checked && !radioTipoConsultaFiltro[1].checked){
	    campos = msgNecessarioSelecionarFiltroPesquisa + '!' + '\n';	
	}
	else{
		if(radioTipoConsultaFiltro[0].checked){
			if(document.getElementById(form.id + ':cboTipoLayoutArquivoFiltro').value == null || document.getElementById(form.id + ':cboTipoLayoutArquivoFiltro').value == 0) {
			    campos = campos + msgCampo + ' ' + msgTipoLayoutArquivo + ' ' + msgNecessario + '!' + '\n';
			}
			
			if (allTrim(document.getElementById(form.id + ':txtCodigoMensagemFiltro')) == '') {
			    campos = campos + msgCampo + ' ' + msgCodigoDaMensagem + ' ' + msgNecessario + '!' + '\n';
			}	
		}
		else{
			if(radioTipoConsultaFiltro[1].checked){
				if(document.getElementById(form.id + ':cboCentroCustoFiltro').value == null || document.getElementById(form.id + ':cboCentroCustoFiltro').value == 0) {
				    campos = campos + msgCampo + ' ' + msgCentroCusto + ' ' + msgNecessario + '!' + '\n';
				}
				
				if(document.getElementById(form.id + ':cboRecursoFiltro').value == null || document.getElementById(form.id + ':cboRecursoFiltro').value == 0) {
				    campos = campos + msgCampo + ' ' + msgRecurso + ' ' + msgNecessario + '!' + '\n';
				}
				
				if(document.getElementById(form.id + ':cboIdiomaFiltro').value == null || document.getElementById(form.id + ':cboIdiomaFiltro').value == 0) {
				    campos = campos + msgCampo + ' ' + msgIdioma + ' ' + msgNecessario + '!' + '\n';
				}
				
				if (allTrim(document.getElementById(form.id + ':txtCodigoEventoFiltro')) == '') {	
				    campos = campos + msgCampo + ' ' + msgCodigoDoEvento + ' ' + msgNecessario + '!' + '\n';
				}		
			}
		}	
	}

	if (campos != ''){
		alert(campos);
		return false;
	}

	return true;
}

function validarInclusao(form, msgCampo,msgNecessario,msgTipoLayoutArquivo,msgCodigoDaMensagem,msgCentroCusto,msgRecurso,msgIdioma,msgCodigoDoEvento){

	var campos = '';

	if(document.getElementById(form.id + ':cboTipoLayoutArquivo').value == null || document.getElementById(form.id + ':cboTipoLayoutArquivo').value == 0) {
	    campos = campos + msgCampo + ' ' + msgTipoLayoutArquivo + ' ' + msgNecessario + '!' + '\n';
	}
	
	if (allTrim(document.getElementById(form.id + ':txtCodigoMensagem')) == '') {
	    campos = campos + msgCampo + ' ' + msgCodigoDaMensagem + ' ' + msgNecessario + '!' + '\n';
	}
	
	if(document.getElementById(form.id + ':cboCentroCusto').value == null || document.getElementById(form.id + ':cboCentroCusto').value == 0) {
	    campos = campos + msgCampo + ' ' + msgCentroCusto + ' ' + msgNecessario + '!' + '\n';
	}
	
	if(document.getElementById(form.id + ':cboRecurso').value == null || document.getElementById(form.id + ':cboRecurso').value == 0) {
	    campos = campos + msgCampo + ' ' + msgRecurso + ' ' + msgNecessario + '!' + '\n';
	}
	
	if(document.getElementById(form.id + ':cboIdioma').value == null || document.getElementById(form.id + ':cboIdioma').value == 0) {
	    campos = campos + msgCampo + ' ' + msgIdioma + ' ' + msgNecessario + '!' + '\n';
	}
	
	if (allTrim(document.getElementById(form.id + ':txtCodigoEvento')) == '') {	
	    campos = campos + msgCampo + ' ' + msgCodigoDoEvento + ' ' + msgNecessario + '!' + '\n';
	}

	if (campos != ''){
		alert(campos);
		return false;
	}

	return true;
}