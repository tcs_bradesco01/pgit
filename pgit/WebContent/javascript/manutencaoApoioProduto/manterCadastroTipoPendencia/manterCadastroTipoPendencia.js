	function checaCamposObrigatorios(form, msgcampo, msgnecessario, msgcodigo, msgdescricao, msgobservacoes, msgtpuoresponsavel, msgtipobaixa, msgindicadorresponsabilidade, msguo, msgemail,  msconglo){
	
		var campos, hidden;
		campos = '';
	
		for(i=0; i<form.elements.length; i++){
		
			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:hiddenObrigatoriedade'){
				hidden = form.elements[i];
			}
		
			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:txtCodigo'){
				if (form.elements[i].value == ''){
					campos = campos + msgcampo + ' ' + msgcodigo + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:txtDescricao'){
				if (allTrim(form.elements[i]) == ''){
					campos = campos + msgcampo + ' ' + msgdescricao + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:txtObservacoes'){
				if (allTrim(form.elements[i]) == ''){
					campos = campos + msgcampo + ' ' + msgobservacoes + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:tipoUnidadeOrganizacional'){
				if (form.elements[i].value == '' && form.elements['pesqManterCadastroTipoPendenciaAlterar:hiddenIndicadorResponsabilidade'].value == 2){
					campos = campos + msgcampo + ' ' + msgtpuoresponsavel + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:hiddenTipoBaixa'){
				if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
					campos = campos + msgcampo + ' ' + msgtipobaixa + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:hiddenIndicadorResponsabilidade'){
				if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
					campos = campos + msgcampo + ' ' + msgindicadorresponsabilidade + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:txtUnidadeOrganizacional'){
				if (allTrim(form.elements[i]) == '' && form.elements['pesqManterCadastroTipoPendenciaAlterar:hiddenIndicadorResponsabilidade'].value == 2){
					campos = campos + msgcampo + ' ' + msguo + ' ' + msgnecessario + '\n';
				}
			}
			
			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:txtEmail'){
				var codigo = form.elements['pesqManterCadastroTipoPendenciaAlterar:txtCodigo'].value;
				
				if(codigo == '8' || codigo == '9' || codigo == '10' || codigo == '11'){
					if (form.elements[i].value == ''){
						campos = campos + msgcampo + ' ' + msgemail + ' ' + msgnecessario + '\n';
					}else{
						email = form.elements[i].value;
						var iArroba = 0;
						var subEmail = '';
						var erro = 0;

						if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
							iArroba = email.indexOf('@');
							proxArroba = email.substring(iArroba + 1, iArroba + 2);
							if (iArroba < 1)
							erro = 1;
							if (proxArroba == '.')
							erro = 1;
							if (email.substring(email.length - 1, email.length) == '.')
							erro = 1;
						} else {
							erro = 1;
						}

					    if (erro == 1){
					    	campos = campos + msgcampo + ' ' + msgemail + ' ' + '\u00e9 inválido' + '\n';
					    }
					}
				}
			}
			
			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:empresaConglomerado'){
				if (form.elements[i].value == '' && form.elements['pesqManterCadastroTipoPendenciaAlterar:hiddenIndicadorResponsabilidade'].value == 2){
					campos = campos + msgcampo + ' ' + msconglo + ' ' + msgnecessario + '\n';
				}
			}
			
		}
		if (campos != ''){
			hidden.value  ='F';
			alert(campos);
			campos = '';
		}else{
			hidden.value  ='T';
		}
	}
	
	
	function checaCamposObrigatoriosIncluir(form, msgcampo, msgnecessario, msgcodigo, msgdescricao, msgobservacoes, msgtpuoresponsavel, msgtipobaixa, msgindicadorresponsabilidade, msguo, msgemail, mconglo){
	
		var campos, hidden;
		campos = '';
	
		for(i=0; i<form.elements.length; i++){
		
			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:hiddenObrigatoriedade'){
				hidden = form.elements[i];
			}
		
			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:txtCodigo'){
				if (allTrim(form.elements[i]) == ''){
					campos = campos + msgcampo + ' ' + msgcodigo + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:txtDescricao'){
				if (allTrim(form.elements[i]) == ''){
					campos = campos + msgcampo + ' ' + msgdescricao + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:txtObservacoes'){
				if (allTrim(form.elements[i]) == ''){
					campos = campos + msgcampo + ' ' + msgobservacoes + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:tipoUnidadeOrganizacional'){
				if (form.elements[i].value == '' && form.elements['pesqManterCadastroTipoPendenciaIncluir:hiddenIndicadorResponsabilidade'].value == 2){
					campos = campos + msgcampo + ' ' + msgtpuoresponsavel + ' ' + msgnecessario + '\n';
				}
			}
			
			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:inputTipoUnidadeOrganizacional'){
				if (form.elements[i].value == '' && form.elements['pesqManterCadastroTipoPendenciaIncluir:hiddenIndicadorResponsabilidade'].value == 2){
					campos = campos + msgcampo + ' ' + msgtpuoresponsavel + ' ' + msgnecessario + '\n';
				}
			}
			

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:hiddenTipoBaixa'){
				if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
					campos = campos + msgcampo + ' ' + msgtipobaixa + ' ' + msgnecessario + '\n';
				}
			}
			
			

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:hiddenIndicadorResponsabilidade'){
				if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
					campos = campos + msgcampo + ' ' + msgindicadorresponsabilidade + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:txtUnidadeOrganizacional'){
				if (allTrim(form.elements[i]) == '' && form.elements['pesqManterCadastroTipoPendenciaIncluir:hiddenIndicadorResponsabilidade'].value == 2){
					campos = campos + msgcampo + ' ' + msguo + ' ' + msgnecessario + '\n';
				}
			}
						
			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:txtEmail'){
				var codigo = form.elements['pesqManterCadastroTipoPendenciaIncluir:txtCodigo'].value;
				
				if(codigo == '8' || codigo == '9' || codigo == '10' || codigo == '11'){
					if (form.elements[i].value == ''){
						campos = campos + msgcampo + ' ' + msgemail + ' ' + msgnecessario + '\n';
					}else{
						email = form.elements[i].value;
						var iArroba = 0;
						var subEmail = '';
						var erro = 0;

						if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
							iArroba = email.indexOf('@');
							proxArroba = email.substring(iArroba + 1, iArroba + 2);
							if (iArroba < 1)
							erro = 1;
							if (proxArroba == '.')
							erro = 1;
							if (email.substring(email.length - 1, email.length) == '.')
							erro = 1;
						} else {
							erro = 1;
						}

					    if (erro == 1){
					    	campos = campos + msgcampo + ' ' + msgemail + ' ' + '\u00e9 inválido' + '\n';
					    }
					}
				}
			}
			
			if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:empresaConglomerado'){
				if (form.elements[i].value == '' && form.elements['pesqManterCadastroTipoPendenciaIncluir:hiddenIndicadorResponsabilidade'].value == 2){
					campos = campos + msgcampo + ' ' + mconglo + ' ' + msgnecessario + '\n';
				}
			}
			
		}
		if (campos != ''){
			hidden.value  ='F';
			alert(campos);
			campos = '';
		}else{
			hidden.value  ='T';
		}
	}
	
	
	function validarConsultar(form, msgcampo, msgnecessario, msgEmpresaConglomerado, msgUnidadeOrganizacional){

		var campos, hidden, tipoUnidadeOrganizacional ;
		campos = '';

		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id + ':hiddenObrigatoriedade'){				
				hidden = form.elements[i];
			}
			
			if (form.elements[i].id == form.id + ':filtroTipoUnidadeOrganizacional'){				
				tipoUnidadeOrganizacional = form.elements[i];
				
			}
		
		
			if (form.elements[i].id == form.id + ':empresaConglomerado'){
				if  (form.elements[i].value == ''){

					campos = campos + msgcampo + ' ' + msgEmpresaConglomerado + ' ' + msgnecessario + '\n';					
				}
			}
			
			

			if (form.elements[i].id == form.id + ':txtUnidadeOrganizacional'){

				if  (form.elements[i].value == ''){
					campos = campos + msgcampo + ' ' + msgUnidadeOrganizacional + ' ' + msgnecessario + '\n';
				}
			}

		}

		if (tipoUnidadeOrganizacional.value != ''){
			if (campos != ''){
				hidden.value  ='F';
				alert(campos);
				campos = '';
			}else{
				hidden.value  ='T';
			}
		}else{
			hidden.value  ='T';
		}
	
	}
	
	function confirmar(form, msgConfirma){
	
		var hiddenConfirm;
		
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id + ':hiddenConfirma'){	
				hiddenConfirm = form.elements[i];
				break;
			}
		}
		
		hiddenConfirm.value=confirm(msgConfirma);
	
	}
	
	function validaConsulta(){
		if(document.getElementById('pesqManterCadastroTipoPendencia:filtroTipoUnidadeOrganizacional').value != 0){
			if(document.getElementById('pesqManterCadastroTipoPendencia:empresaConglomerado').value == 0){
				alert('O campo Empresa do Conglomerado \u00E9 necess\u00E1rio');
				return false;
			}
			if(document.getElementById('pesqManterCadastroTipoPendencia:txtUnidadeOrganizacional').value == ""){
				alert('O campo Unidade Organizacional \u00E9 necess\u00E1rio');
				return false;
			}
		}
		
		if(document.getElementById('pesqManterCadastroTipoPendencia:empresaConglomerado').value != 0){
			if(document.getElementById('pesqManterCadastroTipoPendencia:filtroTipoUnidadeOrganizacional').value == 0){
				alert('O campo Tipo de Unidade Organizacional Respons\u00E1vel \u00E9 necess\u00E1rio');
				return false;
			}
			if(document.getElementById('pesqManterCadastroTipoPendencia:txtUnidadeOrganizacional').value == ""){
				alert('O campo Unidade Organizacional \u00E9 necess\u00E1rio');
				return false;
			}
		}
		
		if(document.getElementById('pesqManterCadastroTipoPendencia:txtUnidadeOrganizacional').value != ""){
			if(document.getElementById('pesqManterCadastroTipoPendencia:filtroTipoUnidadeOrganizacional').value == 0){
				alert('O campo Tipo de Unidade Organizacional Respons\u00E1vel \u00E9 necess\u00E1rio');
				return false;
			}
			if(document.getElementById('pesqManterCadastroTipoPendencia:empresaConglomerado').value == 0){
				alert('O campo Empresa do Conglomerado \u00E9 necess\u00E1rio');
				return false;
			}
		}
		return true;
	}
	
	function habilitaBotoes(){
		document.getElementById('pesqManterCadastroTipoPendencia:btnDetalhar').disabled = "";
		document.getElementById('pesqManterCadastroTipoPendencia:btnExcluir').disabled = "";
		document.getElementById('pesqManterCadastroTipoPendencia:btnAlterar').disabled = "";
	}

	function desabilitaBotoes(){
		document.getElementById('pesqManterCadastroTipoPendencia:btnDetalhar').disabled = "disabled";
		document.getElementById('pesqManterCadastroTipoPendencia:btnExcluir').disabled = "disabled";
		document.getElementById('pesqManterCadastroTipoPendencia:btnAlterar').disabled = "disabled";
	}

	function habilitaFiltros(){
	
		if( allTrim( document.getElementById('pesqManterCadastroTipoPendencia:txtTipoPendencia')) != ""){
			document.getElementById('pesqManterCadastroTipoPendencia:empresaConglomerado').value = 0;
			document.getElementById('pesqManterCadastroTipoPendencia:filtroTipoUnidadeOrganizacional').value = 0;
			document.getElementById('pesqManterCadastroTipoPendencia:txtUnidadeOrganizacional').value = "";
			document.getElementById('pesqManterCadastroTipoPendencia:selTipoBaixa').value = 0;
			document.getElementById('pesqManterCadastroTipoPendencia:selResponsabilidade').value = 0;
			
			document.getElementById('pesqManterCadastroTipoPendencia:empresaConglomerado').disabled = "disabled";
			document.getElementById('pesqManterCadastroTipoPendencia:filtroTipoUnidadeOrganizacional').disabled = "disabled";
			document.getElementById('pesqManterCadastroTipoPendencia:txtUnidadeOrganizacional').disabled = "disabled";
			document.getElementById('pesqManterCadastroTipoPendencia:selTipoBaixa').disabled = "disabled";
			document.getElementById('pesqManterCadastroTipoPendencia:selResponsabilidade').disabled = "disabled";
		}else{
			document.getElementById('pesqManterCadastroTipoPendencia:empresaConglomerado').value = 0;
			document.getElementById('pesqManterCadastroTipoPendencia:filtroTipoUnidadeOrganizacional').value = 0;
			document.getElementById('pesqManterCadastroTipoPendencia:txtUnidadeOrganizacional').value = "";
			document.getElementById('pesqManterCadastroTipoPendencia:selTipoBaixa').value = 0;
			document.getElementById('pesqManterCadastroTipoPendencia:selResponsabilidade').value = 0;
			
			document.getElementById('pesqManterCadastroTipoPendencia:selTipoBaixa').disabled = "";
			document.getElementById('pesqManterCadastroTipoPendencia:selResponsabilidade').disabled = "";
		}
	
	}