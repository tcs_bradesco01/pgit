function checaCamposObrigatorios(form, msgcampo, msgnecessario, msgregra, msgproduto, msgoperacao, msgradio, msgRegraAlcada, msgTratamentoAlcada, msgcentroCustoRetorno, msgcodigoAplicacaoRetorno){
	var campos = '';
	var hidden;

	for(i=0; i<form.elements.length; i++){
	
		if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:hiddenObrigatoriedade'){
			hidden = form.elements[i];
		}

		if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:hiddenRadioRegra'){
		
			if (form.elements[i].value == '1'){
				for(j=0; j<form.elements.length; j++){
					if (form.elements[j].id == 'alterarManterFuncoesAlcadaForm:regra'){
						if (form.elements[j].value == ''){
							campos = campos + msgcampo + ' ' + msgregra + ' ' + msgnecessario + '\n'; 
						}
					}
				}
			}
			
			if (form.elements[i].value == '2'){
				for(j=0; j<form.elements.length; j++){
					if (form.elements[j].id == 'alterarManterFuncoesAlcadaForm:servico'){
						if (form.elements[j].value == 0){
							campos = campos + msgcampo + ' ' + msgproduto + ' ' + msgnecessario + '\n';
						}	
					}
					if (form.elements[j].id == 'alterarManterFuncoesAlcadaForm:servicoRelacionado'){
						if (form.elements[j].value == 0){
							campos = campos + msgcampo + ' ' + msgoperacao + ' ' + msgnecessario + '\n';
						}
					}
				}
			}
		}
		
		if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:hiddenRadioTratamentoAlcada'){
			if (form.elements[i].value == ''){
				campos = campos + msgcampo + ' ' + msgTratamentoAlcada + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:hiddenRadioRegraAlcada'){
			if (form.elements[i].value == ''){
				campos = campos + msgcampo + ' ' + msgRegraAlcada + ' ' + msgnecessario + '\n';
			}

			if (form.elements[i].value == '2'){
				for(j=0; j<form.elements.length; j++){
						if (form.elements[j].id == 'alterarManterFuncoesAlcadaForm:centroCustoRetorno'){
							if (form.elements[j].value == ''){
								campos = campos + msgcampo + ' ' + msgcentroCustoRetorno + ' ' + msgnecessario + '\n';
							}
						}
						if (form.elements[j].id == 'alterarManterFuncoesAlcadaForm:codigoAplicacaoRetorno'){
							if (form.elements[j].value == ''){
								campos = campos + msgcampo + ' ' + msgcodigoAplicacaoRetorno + ' ' + msgnecessario + '\n';
							}
						}
				}
			}
			
			
		}
		
	}
	
	if (campos != ''){
		hidden.value = 'F';
		alert(campos);
		campos = '';
	}else{
		hidden.value = 'T';
	}

}  

function checaCamposObrigatoriosIncluir(form, msgcampo, msgnecessario, msgcentrocusto, msgcodigoaplicacao, msgacao, msgregra, msgproduto, msgoperacao, msgradio, msgRegraAlcada, msgTratamentoAlcada, msgcentroCustoRetorno, msgcodigoAplicacaoRetorno){

	var campos = '';
	var hidden;
		
	for(i=0; i<form.elements.length; i++){
			
		if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:hiddenObrigatoriedade'){
			hidden = form.elements[i];
		}
			
		if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:centroCusto'){
			if (form.elements[i].value == ''){
				campos = campos + msgcampo + ' ' + msgcentrocusto + ' ' + msgnecessario + '\n';
			}
		}

		if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:codigoAplicacao'){
			if (form.elements[i].value == ''){
				campos = campos + msgcampo + ' ' + msgcodigoaplicacao + ' ' + msgnecessario + '\n';
			}
		}

		if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:acao'){
			if (form.elements[i].value == 0){
				campos = campos + msgcampo + ' ' + msgacao + ' ' + msgnecessario + '\n';
			}
		}

		if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:hiddenRadioRegra'){
			
			if (form.elements[i].value != '1' && form.elements[i].value != '2'){
				campos = campos + msgcampo + ' ' + msgradio + ' ' + msgnecessario + '\n';
			}
			if (form.elements[i].value == '1'){
				for(j=0; j<form.elements.length; j++){
					if (form.elements[j].id == 'incluirManterFuncoesAlcadaForm:regra'){
						if (form.elements[j].value == ''){
							campos = campos + msgcampo + ' ' + msgregra + ' ' + msgnecessario + '\n';
						}
					}
				}
			}
					
			if (form.elements[i].value == '2'){
				for(j=0; j<form.elements.length; j++){
						if (form.elements[j].id == 'incluirManterFuncoesAlcadaForm:servico'){
							if (form.elements[j].value == 0){
								campos = campos + msgcampo + ' ' + msgproduto + ' ' + msgnecessario + '\n';
							}
						}
						if (form.elements[j].id == 'incluirManterFuncoesAlcadaForm:servicoRelacionado'){
							if (form.elements[j].value == 0){
								campos = campos + msgcampo + ' ' + msgoperacao + ' ' + msgnecessario + '\n';
							}
						}
				}
			}
					
		}
		
		if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:hiddenRadioTratamentoAlcada'){
			if (form.elements[i].value == ''){
				campos = campos + msgcampo + ' ' + msgTratamentoAlcada + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:hiddenRadioRegraAlcada'){
			if (form.elements[i].value == ''){
				campos = campos + msgcampo + ' ' + msgRegraAlcada + ' ' + msgnecessario + '\n';
			}

			if (form.elements[i].value == '2'){
				for(j=0; j<form.elements.length; j++){
						if (form.elements[j].id == 'incluirManterFuncoesAlcadaForm:centroCustoRetorno'){
							if (form.elements[j].value == ''){
								campos = campos + msgcampo + ' ' + msgcentroCustoRetorno + ' ' + msgnecessario + '\n';
							}
						}
						if (form.elements[j].id == 'incluirManterFuncoesAlcadaForm:codigoAplicacaoRetorno'){
							if (form.elements[j].value == ''){
								campos = campos + msgcampo + ' ' + msgcodigoAplicacaoRetorno + ' ' + msgnecessario + '\n';
							}
						}
				}
			}
			
			
		}
		
		
		
		

   	}
			
	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
		campos = '';
	}else{
		hidden.value  ='T';
	}
}

