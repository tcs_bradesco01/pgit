function checaCamposObrigatoriosIncluir(form, msgcampo, msgnecessario, msgformaliq, msghorariolimite, msgcentrocusto, msgmargemseguranca, msgprioridadedebito, msgradio){

	var campos = '';
	var hidden;
		
	for(i=0; i<form.elements.length; i++){
			
		if (form.elements[i].id == 'incFormaLiquidacaoPagamentoIntegradoForm:hiddenObrigatoriedade'){
			hidden = form.elements[i];
		}
			
		if (form.elements[i].id == 'incFormaLiquidacaoPagamentoIntegradoForm:formaLiquidacao'){
			if (form.elements[i].value == ''){
				campos = campos + msgcampo + ' ' + msgformaliq + ' ' + msgnecessario + '\n';
			}
		}
				
		if (form.elements[i].id == 'incFormaLiquidacaoPagamentoIntegradoForm:hiddenRadio'){
			if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
				campos = campos + msgcampo + ' ' + msgradio + ' ' + msgnecessario + '\n';
			}
			
			if (form.elements[i].value == '1'){					
				for(j=0; j<form.elements.length; j++){
					if (form.elements[j].id == 'incFormaLiquidacaoPagamentoIntegradoForm:txtHorarioLimite'){
						if (form.elements[j].value == ''){
							campos = campos + msgcampo + ' ' + msghorariolimite + ' ' + msgnecessario + '\n';
						}
					}
				}
			}
					
			if (form.elements[i].value == '2'){
				for(j=0; j<form.elements.length; j++){
					if (form.elements[j].id == 'incFormaLiquidacaoPagamentoIntegradoForm:txtCentroCusto'){
						if (form.elements[j].value == ''){
							campos = campos + msgcampo + ' ' + msgcentrocusto + ' ' + msgnecessario + '\n';
						}
					}
					if (form.elements[j].id == 'incFormaLiquidacaoPagamentoIntegradoForm:txtMargemSeguranca'){
						if (form.elements[j].value == ''){
							campos = campos + msgcampo + ' ' + msgmargemseguranca + ' ' + msgnecessario + '\n';
						}
					}
					if (form.elements[j].id == 'incFormaLiquidacaoPagamentoIntegradoForm:txtPrioridadeDebito'){
						if (form.elements[j].value == ''){
							campos = campos + msgcampo + ' ' + msgprioridadedebito + ' ' + msgnecessario + '\n';
						}
					}							
				}
			}					
		}
	}

	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
	}else{
		hidden.value  ='T';
	}
}
	
function checaCamposObrigatoriosAlterar(form, msgcampo, msgnecessario, msghorariolimite, msgcentrocusto, msgmargemseguranca, msgprioridadedebito, msgradio){

	var campos='';
	var hidden;
		
	for(i=0; i<form.elements.length; i++){
	
		if (form.elements[i].id == 'altFormaLiquidacaoPagamentoIntegradoForm:hiddenObrigatoriedade'){
			hidden = form.elements[i];
		}
			
		if (form.elements[i].id == 'altFormaLiquidacaoPagamentoIntegradoForm:hiddenRadio'){
			if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
				campos = campos + msgcampo + ' ' + msgradio + ' ' + msgnecessario + '\n';
			}

			if (form.elements[i].value == '1'){					
				for(j=0; j<form.elements.length; j++){
					if (form.elements[j].id == 'altFormaLiquidacaoPagamentoIntegradoForm:txtHorarioLimite'){
						if (form.elements[j].value == ''){
							campos = campos + msgcampo + ' ' + msghorariolimite + ' ' + msgnecessario + '\n';
						}
					}
				}
			}
			
			if (form.elements[i].value == '2'){
				for(j=0; j<form.elements.length; j++){
					if (form.elements[j].id == 'altFormaLiquidacaoPagamentoIntegradoForm:txtCentroCusto'){
						if (form.elements[j].value == ''){
							campos = campos + msgcampo + ' ' + msgcentrocusto + ' ' + msgnecessario + '\n';
						}
					}
					if (form.elements[j].id == 'altFormaLiquidacaoPagamentoIntegradoForm:txtMargemSeguranca'){
						if (form.elements[j].value == ''){
							campos = campos + msgcampo + ' ' + msgmargemseguranca + ' ' + msgnecessario + '\n';
						}
					}
					if (form.elements[j].id == 'altFormaLiquidacaoPagamentoIntegradoForm:txtPrioridadeDebito'){
						if (form.elements[j].value == ''){
							campos = campos + msgcampo + ' ' + msgprioridadedebito + ' ' + msgnecessario + '\n';
						}
					}							
				}
			}					
		}
	}
	
	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
	}else{
		hidden.value  ='T';
	}
}
