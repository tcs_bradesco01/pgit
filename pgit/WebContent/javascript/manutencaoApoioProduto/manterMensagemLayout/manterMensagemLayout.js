function validaIncluir(form, msgcampo, msgnecessario, msgtipolayoutarquivo, msgcodigomensagem, msgtipomensagem, msgnivelmensagem, msgposicaoinicialcampo,
					   msgposicaofinalcampo, msgdescricaomensagemcliente){
	
	var msgValidacao = '';
	
	if(document.getElementById(form.id +':cboTipoLayoutArquivo').value == null || document.getElementById(form.id +':cboTipoLayoutArquivo').value == '0'){	
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtipolayoutarquivo + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if((allTrim(document.getElementById(form.id +':txtCodigoMensagem')) == '')){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigomensagem + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if(document.getElementById(form.id +':cboTipoMensagem').value == null || document.getElementById(form.id +':cboTipoMensagem').value == '0'){	
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtipomensagem + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if(document.getElementById(form.id +':cboNivelMensagem').value == null || document.getElementById(form.id +':cboNivelMensagem').value == '0'){	
		msgValidacao = msgValidacao + msgcampo + ' ' + msgnivelmensagem + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if((allTrim(document.getElementById(form.id +':txtDescricaoMensagemCliente')) == '')){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgdescricaomensagemcliente + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;	
	
}

function validaAlterar(form, msgcampo, msgnecessario, msgtipomensagem, msgnivelmensagem, msgposicaoinicialcampo,
					   msgposicaofinalcampo, msgdescricaomensagemcliente){
	
	var msgValidacao = '';
	
	if(document.getElementById(form.id +':cboTipoMensagem').value == null || document.getElementById(form.id +':cboTipoMensagem').value == '0'){	
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtipomensagem + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if(document.getElementById(form.id +':cboNivelMensagem').value == null || document.getElementById(form.id +':cboNivelMensagem').value == '0'){	
		msgValidacao = msgValidacao + msgcampo + ' ' + msgnivelmensagem + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if((allTrim(document.getElementById(form.id +':txtDescricaoMensagemCliente')) == '')){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgdescricaomensagemcliente + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;	
	
}