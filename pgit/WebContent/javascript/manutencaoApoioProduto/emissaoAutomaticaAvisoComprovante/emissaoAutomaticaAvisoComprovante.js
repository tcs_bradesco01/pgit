function validaCampos(form, msgCampo, msgNecessario, servicoEmissao, tipoServico, modalidadeServico){

var hidden;
var mensagem = "";

	for(i=0; i<form.elements.length; i++){
	
		if(form.elements[i].id == form.id + ':hiddenObrigatoriedade'){
					hidden = form.elements[i];
			}
	
		if(form.elements[i].id == form.id + ':servicoEmissao'){
				if(form.elements[i].value == 0){
					mensagem += msgCampo + ' ' + servicoEmissao + ' ' + msgNecessario+  '\n';
				}
		}
		/*if(form.elements[i].id == form.id + ':tipoServico'){
				if(form.elements[i].value == 0){
					mensagem += msgCampo + ' ' + tipoServico + ' ' + msgNecessario+  '\n';
				}
		}
		if(form.elements[i].id == form.id + ':modalidadeServico'){
				if(form.elements[i].value == 0){
					mensagem += msgCampo + ' ' + modalidadeServico + ' ' + msgNecessario+  '\n';
				}
		}*/
		
	}
	if(mensagem != ''){
		alert(mensagem);
		hidden.value = "F";
		
	}else
		hidden.value = "V";
	
}

function validaCampoPesquisar(form, msgCampo, msgNecessario, servicoEmissao){

var hidden;
var mensagem = "";

	for(i=0; i<form.elements.length; i++){
	
		if(form.elements[i].id == form.id + ':hiddenObrigatoriedade'){
					hidden = form.elements[i];
			}
	
		if(form.elements[i].id == form.id + ':servicoEmissao'){
				if(form.elements[i].value == 0){
					mensagem += msgCampo + ' ' + servicoEmissao + ' ' + msgNecessario+  '\n';
				}
		}		
	}
	if(mensagem != ''){
		alert(mensagem);
		hidden.value = "F";
		
	}else
		hidden.value = "V";
	
}
 
function validaCpoConsultaCliente(msgcampo, msgnecessario, msgcnpj, msgcpf, msgnome, msgbanco, msgagencia, msgconta){

var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
var msgValidacao = '';

	if (objRdoCliente[0].checked) {
		if ( allTrim(document.getElementById('pesqEmissaoAutomaticaForm:txtCnpj')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';	
		} else {
			if (allTrim(document.getElementById('pesqEmissaoAutomaticaForm:txtFilial')) == "") {
				alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
				document.getElementById('pesqEmissaoAutomaticaForm:txtFilial').focus();
				return false;		
			} else {
				if (allTrim(document.getElementById('pesqEmissaoAutomaticaForm:txtControle')) == ""){
					alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
					document.getElementById('pesqEmissaoAutomaticaForm:txtControle').focus();
					return false;		
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById('pesqEmissaoAutomaticaForm:txtCpf')) == "") {
			alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
			document.getElementById('pesqEmissaoAutomaticaForm:txtCpf').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById('pesqEmissaoAutomaticaForm:txtControleCpf')) == "") {
				alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
				document.getElementById('pesqEmissaoAutomaticaForm:txtControleCpf').focus();
				return false;	
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById('pesqEmissaoAutomaticaForm:txtNomeRazaoSocial')) == "") {
			alert(msgcampo + ' ' + msgnome + ' ' + msgnecessario);
			document.getElementById('pesqEmissaoAutomaticaForm:txtNomeRazaoSocial').focus();
			return false;	
		}
	} else if (objRdoCliente[3].checked){
		
		if (allTrim(document.getElementById('pesqEmissaoAutomaticaForm:txtBanco')) == ""){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '!\n';
		}
		if  (allTrim(document.getElementById('pesqEmissaoAutomaticaForm:txtAgencia')) == ""){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '!\n';
		}
		if (allTrim(document.getElementById('pesqEmissaoAutomaticaForm:txtConta')) == ""){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgnecessario + '!\n';
					
		}
				
	}
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;						
}

function validaCpoContrato(msgcampo, msgnecessario,msgempresa,msgtipo, msgnumero, cliente){
	var msgValidacao = '';

	if ((cliente == null) || (cliente ==  '')){
		if (document.getElementById('pesqEmissaoAutomaticaForm:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('pesqEmissaoAutomaticaForm:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
		
		if ( allTrim(document.getElementById('pesqEmissaoAutomaticaForm:txtNumeroContrato')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' ' + msgnecessario + '\n';
		} 
	
	}else{
		if (document.getElementById('pesqEmissaoAutomaticaForm:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('pesqEmissaoAutomaticaForm:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
	}	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}

function habilitaBotao(){
	for(i=0;i<=dataTable;i++){ 
 		var scampo = document.getElementById('incluirEmissaoAutomaticaForm:dataTable'+i+':sorLista');                                   
           if(scampo.disabled == true){                     
               document.getElementById('incluirEmissaoAutomaticaForm:dataTable:btnAvancar').disabled = false;
               break;
            }else{
               document.getElementById('incluirEmissaoAutomaticaForm:dataTable:btnAvancar').disabled = true;
            }
 	}

}

function validaCamposBloqDesbloq(form, msgcampo, msgnecessario, msgsituacao){

	var msgValidacao = '';
	var bloqueio = '';
	
	var objRdoBloqDesbloq = document.getElementsByName('rdoBloqDesblo');
	
	if (!objRdoBloqDesbloq[0].checked && !objRdoBloqDesbloq[1].checked ){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgsituacao + ' ' + msgnecessario + '!';	
	}

	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	if (objRdoBloqDesbloq[0].checked){
		bloqueio = 'Confirma Libera\u00E7\u00E3o?';	
	}
	
	if (objRdoBloqDesbloq[1].checked){
		bloqueio = 'Confirma Bloqueio?';
	}
	
	return confirm(bloqueio);						
}

