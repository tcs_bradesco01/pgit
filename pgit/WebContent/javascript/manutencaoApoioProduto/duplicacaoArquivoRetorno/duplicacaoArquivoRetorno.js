function checaCamposObrigatorios(form, msgcampo, msgnecessario, msgtipolayout, msgtipoarquivo, msgduplicacao,  msgtipoduplicacao,
								msginstrucao, msgparticipante, msgcontrato){

	var campos = '';
	var hidden;
		
	for(i=0; i<form.elements.length; i++){
	
		if (form.elements[i].id == 'pesqDuplicacaoArquivoRetornoIncluir:hiddenObrigatoriedade'){
			hidden = form.elements[i];
		}
		
		if (form.elements[i].id == 'pesqDuplicacaoArquivoRetornoIncluir:tipoLayout'){
			if ( (form.elements[i].value == '0') ){
				campos = campos + msgcampo + ' ' + msgtipolayout + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'pesqDuplicacaoArquivoRetornoIncluir:tipoArquivo'){
			if ( (form.elements[i].value == '0') ){
				campos = campos + msgcampo + ' ' + msgtipoarquivo + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'pesqDuplicacaoArquivoRetornoIncluir:hiddenRadioDuplicacao'){
			if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
				campos = campos + msgcampo + ' ' + msgduplicacao + ' ' + msgnecessario + '\n';
			}
		}
		
		if (form.elements[i].id == 'pesqDuplicacaoArquivoRetornoIncluir:hiddenRadioTipoDuplicacao'){
			if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
				campos = campos + msgcampo + ' ' + msgtipoduplicacao + ' ' + msgnecessario + '\n';
			}
		}
			
		
		if (form.elements[i].id == 'pesqDuplicacaoArquivoRetornoIncluir:hiddenRadioFiltroInstrucao'){
			if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
				campos = campos + msgcampo + ' ' + msginstrucao + ' ' + msgnecessario + '\n';
			}
		}
	
	}
	
	if ((document.getElementById(form.id +':hiddenRadioDuplicacao').value == 1) && 
		(document.getElementById(form.id +':hiddenCpfCnpjParticipante').value == '')){
			campos = campos + msgcampo + ' ' + msgparticipante + ' ' + msgnecessario + '\n';
	}
	
	
	if (document.getElementById(form.id +':empresaGestora').value == ''){
		alert('entrou');
		campos = campos + msgcampo + ' ' + msgcontrato + ' ' + msgnecessario + '\n';
	}		
	
	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
	}else{
		hidden.value  ='T';
	}
}

function checaCamposObrigatoriosAlterar(form, msgcampo, msgnecessario, msgtipoduplicacao,  msginstrucao, msgparticipante, msgDuplicacao, msgparticipante){
		
	var campos = '';
	var hidden;

	for(i=0; i<form.elements.length; i++){
	
		if (form.elements[i].id == 'pesqDuplicacaoArquivoRetornoAlterar:hiddenObrigatoriedade'){
			hidden = form.elements[i];
		}
		

		if (form.elements[i].id == 'pesqDuplicacaoArquivoRetornoAlterar:hiddenRadioDuplicacao'){
		
			if ((form.elements[i].value != '2') && (form.elements[i].value != '1')){				
				campos = campos + msgcampo + ' ' + msgDuplicacao + ' ' + msgnecessario + '\n';
			}
		}
			
		if (form.elements[i].id == 'pesqDuplicacaoArquivoRetornoAlterar:hiddenRadioTipoDuplicacao'){
			if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
				campos = campos + msgcampo + ' ' + msgtipoduplicacao + ' ' + msgnecessario + '\n';
			}
		}
		

		if (form.elements[i].id == 'pesqDuplicacaoArquivoRetornoAlterar:hiddenRadioFiltroInstrucao'){
			if ( (form.elements[i].value != '1') && (form.elements[i].value != '2')){
				campos = campos + msgcampo + ' ' + msginstrucao + ' ' + msgnecessario + '\n';
			}
		}
	}
	
	if ((document.getElementById(form.id +':hiddenRadioDuplicacao').value == 1) && 
		(document.getElementById(form.id +':hiddenClubParticipante').value < 1)){
			campos = campos + msgcampo + ' ' + msgparticipante + ' ' + msgnecessario + '\n';
	}

	
	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
	}else{
		hidden.value  ='T';
	}
}

function validaCpoConsultaCliente(msgcampo, msgnecessario, msgcnpj, msgcpf, msgnome, msgbanco, msgagencia, msgconta){

var objRdoCliente = document.getElementsByName('rdoFiltroCliente');

	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetorno:txtCnpj')) == "") {
			alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
			document.getElementById('pesqDuplicacaoArquivoRetorno:txtCnpj').focus();
			return false;		
		} else {
			if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetorno:txtFilial')) == "") {
				alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
				document.getElementById('pesqDuplicacaoArquivoRetorno:txtFilial').focus();
				return false;		
			} else {
				if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetorno:txtControle')) == ""){
					alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
					document.getElementById('pesqDuplicacaoArquivoRetorno:txtControle').focus();
					return false;		
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetorno:txtCpf')) == "") {
			alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
			document.getElementById('pesqDuplicacaoArquivoRetorno:txtCpf').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById('pesqDuplicacaoArquivoRetorno:txtControleCpf')) == "") {
				alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
				document.getElementById('pesqDuplicacaoArquivoRetorno:txtControleCpf').focus();
				return false;	
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetorno:txtNomeRazaoSocial')) == "") {
			alert(msgcampo + ' ' + msgnome + ' ' + msgnecessario);
			document.getElementById('pesqDuplicacaoArquivoRetorno:txtNomeRazaoSocial').focus();
			return false;	
		}
	} else if (objRdoCliente[3].checked) {
		var campos = '';
		if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetorno:txtBanco')) == "") {
			campos = msgcampo + ' ' + msgbanco + ' ' + msgnecessario;			
		} 
		if(allTrim(document.getElementById('pesqDuplicacaoArquivoRetorno:txtAgencia')) == "") {
			campos = campos + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n';
		} 
		if(allTrim(document.getElementById('pesqDuplicacaoArquivoRetorno:txtConta')) == "") {
			campos =campos +   msgcampo + ' ' + msgconta + ' ' + msgnecessario + '\n';
		}
		
		if (campos != ''){
			alert(campos);
			return false;
		}
	} 
	
	return true;						
}


function validaCpoContrato(msgcampo, msgnecessario,msgempresa,msgtipo, msgnumero, cliente){
	var msgValidacao = '';

	if ((cliente == null) || (cliente ==  '')){
		if (document.getElementById('pesqDuplicacaoArquivoRetorno:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('pesqDuplicacaoArquivoRetorno:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
		
		if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetorno:txtNumeroContrato')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' ' + msgnecessario + '\n';
		} 
	
	}else{
		if (document.getElementById('pesqDuplicacaoArquivoRetorno:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById('pesqDuplicacaoArquivoRetorno:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
	}	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}

function validaCpoContratoIncluir(msgcampo, msgnecessario,msgempresa,msgtipo, msgnumero){
	
	var campos = '';
	if (document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:selEmpresaGestoraContrada').value == 0) {
		campos = campos + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
				
	} 
	if (document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:selTipoContrato').value == 0) {
		campos = campos + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
	
	} 
	if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtNumeroContrato'))== "") {
		campos = campos + msgcampo + ' ' + msgnumero + ' ' + msgnecessario +  '\n';
		
	} 		
	
	if (campos != ''){
		alert(campos);
		return false;
	}
		
	
	
	return true;
}

function validaCpoConsultaClienteIncluir(msgcampo, msgnecessario, msgcnpj, msgcpf, msgnome, msgbanco, msgagencia, msgconta){

var objRdoCliente = document.getElementsByName('rdoFiltroCliente');

	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtCnpj')) == "") {
			alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
			document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtCnpj').focus();
			return false;		
		} else {
			if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtFilial')) == "") {
				alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
				document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtFilial').focus();
				return false;		
			} else {
				if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtControle')) == ""){
					alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
					document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtControle').focus();
					return false;		
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtCpf')) == "") {
			alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
			document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtCpf').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtControleCpf')) == "") {
				alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
				document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtControleCpf').focus();
				return false;	
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtNomeRazaoSocial')) == "") {
			alert(msgcampo + ' ' + msgnome + ' ' + msgnecessario);
			document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtNomeRazaoSocial').focus();
			return false;	
		}
	} else if (objRdoCliente[3].checked) {
		var campos = '';
		if (allTrim(document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtBanco')) == "") {
			campos = campos + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '\n';
					
		} 
		if(allTrim(document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtAgencia')) == "") {
			campos = campos + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n';
				
		} 
		if(allTrim(document.getElementById('pesqDuplicacaoArquivoRetornoIncluir:txtConta')) == "") {
			campos = campos + msgcampo + ' ' + msgconta + ' ' + msgnecessario + '\n';
				
		}
		
		if (campos != ''){		
			alert(campos);
			return false;
		}
			
} 
	
	return true;						
}