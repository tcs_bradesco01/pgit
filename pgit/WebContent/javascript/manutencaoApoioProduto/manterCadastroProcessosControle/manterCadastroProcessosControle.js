function checaCampoObrigatorioInclusao(form, msgcampo, msgnecessario, msgcentrocusto, msgtipoprocesso, msgdescricao, msgnet, msgjob, msgperiodicidade) {
	var campos = '';
	var objFocus = new Array();

	var objCentroCusto = document.getElementById(form.name + ':centroCusto');
	if (objCentroCusto != null) {
		if (objCentroCusto.disabled || isEmpty(allTrim(objCentroCusto))) {
			campos = campos + msgcampo + ' ' + msgcentrocusto + ' ' + msgnecessario + '\n';
			if (!objCentroCusto.disabled) {
				objFocus.push(objCentroCusto);
			}
		}
	}

	var objTpProcesso = document.getElementById(form.name + ':tipoProcesso');
	if (objTpProcesso != null) {
		if (isEmpty(objTpProcesso.value)){
			campos = campos + msgcampo + ' ' + msgtipoprocesso + ' ' + msgnecessario + '\n';
			objFocus.push(objTpProcesso);
		}
	}

	var objDescricao = document.getElementById(form.name + ':descricao');
	if (objDescricao != null) {
		if (isEmpty(allTrim(objDescricao))){
			campos = campos + msgcampo + ' ' + msgdescricao + ' ' + msgnecessario + '\n';
			objFocus.push(objDescricao);
		}
	}

	var objNet = document.getElementById(form.name + ':net');
	if (objNet != null) {
		if (isEmpty(allTrim(objNet))){
			campos = campos + msgcampo + ' ' + msgnet + ' ' + msgnecessario + '\n';
			objFocus.push(objNet);
		}
	}
	
	var objJob = document.getElementById(form.name + ':job');
	if (objJob != null) {
		if (isEmpty(allTrim(objJob))){
			campos = campos + msgcampo + ' ' + msgjob + ' ' + msgnecessario + '\n';
			objFocus.push(objJob);
		}
	}
	
	var objPeriodicidade = document.getElementById(form.name + ':periodicidade');
	if (objPeriodicidade != null) {
		if (isEmpty(objPeriodicidade.value)){
			campos = campos + msgcampo + ' ' + msgperiodicidade + ' ' + msgnecessario + '\n';
			objFocus.push(objPeriodicidade);
		}
	}

	if (campos != '') {
		alert(campos);
		if (objFocus != null && objFocus.length > 0) {
			objFocus[0].focus();
		}
		return false
	}
	return true;
}

function checaCampoObrigatorio(form, msgcampo, msgnecessario, msgsituacao, msgmotivobloqueio){
	var campos = '';
	var objFocus = new Array();

	var objRdoSituacao = document.getElementsByName('situacao')
	if (objRdoSituacao != null) {
		if (!validaRadioSelecionado('situacao')) {
			campos = campos + msgcampo + ' ' + msgsituacao + ' ' + msgnecessario + '\n';
		} else {
			if (objRdoSituacao[1].checked) {
				var objMotivo = document.getElementById(form.name + ':motivo');
				if (objMotivo != null) {
					if (isEmpty(objMotivo.value)) {
						campos = campos + msgcampo + ' ' + msgmotivobloqueio + ' ' + msgnecessario + '\n';
						objFocus.push(objMotivo);
					}
				}
			}
		}
	}

	if (campos != '') {
		alert(campos);
		if (objFocus != null && objFocus.length > 0) {
			objFocus[0].focus();
		}
		return false
	}
	return true;
}

function validarData(form, msg){
	
	var campos = '';
	
	if(document.getElementById(form.id + ':calendarioDe.year').value > document.getElementById(form.id + ':calendarioAte.year').value){
		campos = msg + '!' + '\n';	
	}else{
		if(document.getElementById(form.id + ':calendarioDe.year').value == document.getElementById(form.id + ':calendarioAte.year').value){
			if(document.getElementById(form.id + ':calendarioDe.month').value > document.getElementById(form.id + ':calendarioAte.month').value){
				campos = msg + '!' + '\n';	
			}else{
				if(document.getElementById(form.id + ':calendarioDe.month').value == document.getElementById(form.id + ':calendarioAte.month').value){
					if(document.getElementById(form.id + ':calendarioDe.day').value > document.getElementById(form.id + ':calendarioAte.day').value){
						campos = msg + '!' + '\n';	
					}
				}
			}
		}
	}
		   
	if (campos == ''){
		return true;
	}else {
		alert(campos);
		return false;
	}	
}

function checaCentroCusto(form, msgcampo, msgnecessario,msgcentrocusto){
		if (allTrim(document.getElementById(form.id + ':centroCustoFiltro')) == ''){
			alert(msgcampo + ' ' + msgcentrocusto + ' ' + msgnecessario + '\n');
			return false;
		}
		else{
			return true;
		}
}

function validarCamposConsultar(form, msgCampo, msgNecessario, msgCentroCusto){

	if ( allTrim(document.getElementById(form.id +':centroCusto')) == '') {
		alert(msgCampo + ' ' + msgCentroCusto + ' ' + msgNecessario + '!');
		return false;
	
	}	
	
	return true;	
}