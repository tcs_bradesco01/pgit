function checaCampoObrigatorio(form, msgcampo, msgnecessario, msgcodigoorgaopagador, msgtipounidadeorganizacional, msgpempresagrupobradesco, msgagenciaresponsavel, msgempresaparceira, msgloja, msgcategoria, msgselecaoradio){
	var campos = '';
	var objFocus = new Array();

	var objCodOrgaoPagador = document.getElementById(form.name + ':codigoOrgaoPagador');
	if (objCodOrgaoPagador != null) {
		if (isEmpty(objCodOrgaoPagador.value)) {
			campos = campos + msgcampo + ' ' + msgcodigoorgaopagador + ' ' + msgnecessario + '\n';
			objFocus.push(objCodOrgaoPagador);
		}
	}

	var objTpUnidade = document.getElementById(form.name + ':tipoUnidadeOrganizacional');
	if (objTpUnidade != null) {
		if (isEmpty(objTpUnidade.value)){
			campos = campos + msgcampo + ' ' + msgtipounidadeorganizacional + ' ' + msgnecessario + '\n';
			objFocus.push(objTpUnidade);
		}
	}

	var objRdoEmpresa = document.getElementsByName('sor')
	if (objRdoEmpresa != null) {
		if (!validaRadioSelecionado('sor')) {
			campos = campos + msgcampo + ' ' + msgselecaoradio + ' ' + msgnecessario + '\n';
		} else {
			if (objRdoEmpresa[0].checked) {
				var objEmpresaGrupo = document.getElementById(form.name + ':empresaGrupoBradesco');
				if (objEmpresaGrupo != null) {
					if (trim(objEmpresaGrupo.value) == '0') {
						campos = campos + msgcampo + ' ' + msgpempresagrupobradesco + ' ' + msgnecessario + '\n';
						objFocus.push(objEmpresaGrupo);
					}
				}
				var objAgencia = document.getElementById(form.name + ':agenciaResponsavel');
				if (objAgencia != null) {
					if (isEmpty(objAgencia.value)) {
						campos = campos + msgcampo + ' ' + msgagenciaresponsavel + ' ' + msgnecessario + '\n';
						objFocus.push(objAgencia);
					}
				}
			} else {
				if (objRdoEmpresa[1].checked) {
					var objEmpresaParceira = document.getElementById(form.name + ':empresaParceira');
					if (objEmpresaParceira != null) {
						if (isEmpty(objEmpresaParceira.value)) {
							campos = campos + msgcampo + ' ' + msgempresaparceira + ' ' + msgnecessario + '\n';
							objFocus.push(objEmpresaParceira);
						}
					}
					var objLoja = document.getElementById(form.name + ':loja');
					if (objLoja != null) {
						if (isEmpty(objLoja.value)) {
							campos = campos + msgcampo + ' ' + msgloja + ' ' + msgnecessario + '\n';
							objFocus.push(objLoja);
						}
					}
				}
			}
		}
	}

	if (!validaRadioSelecionado('categoriaRadio')) {
		campos = campos + msgcampo + ' ' + msgcategoria + ' ' + msgnecessario + '\n';
	}

	if (campos != '') {
		alert(campos);
		if (objFocus != null && objFocus.length > 0) {
			objFocus[0].focus();
		}
		return false
	}
	return true;
}

function verifica(form, msgcampo, msgnecessario, campo1, campo2){
	var campos, hidden;
	campos = '';

	for(i=0; i<form.elements.length; i++){

		if (form.elements[i].id == 'bloqOrgaoPagadorForm:hiddenObrigatoriedade'){
			hidden = form.elements[i];
		}

		if (form.elements[i].id == 'bloqOrgaoPagadorForm:hiddenRadio'){
			if (form.elements[i].value=="0"){
				campos = campos + msgcampo + " " +  campo1 + " " + msgnecessario + "\n";
			}
		}
		
		if (form.elements[i].id == 'bloqOrgaoPagadorForm:motivo'){
			if (form.elements[i].value==""){
				campos = campos + msgcampo + " " +  campo2 + " " + msgnecessario ;
			}
		}
	}	
	
	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
	}else{
		hidden.value  ='T';
	}
}
	
function receberRadio(form, radio){	
	var campos, hidden;
	campos = '';

	for(i=0; i<form.elements.length; i++) {
		if (form.elements[i].id == 'bloqOrgaoPagadorForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
	}		
}

function validarFiltroHistoricoManutencao(msgcampo, msgnecessario, msgCalendarioDe, msgCalendarioAte) {
	var campos = '';
	var objFocus = new Array();

	if (!validarCampoData('pesqhistoricoManutencaoOrgaoPagadorForm:calendarioDe')) {
		campos = campos + msgcampo + ' ' + msgCalendarioDe + ' ' + msgnecessario + '\n';
		objFocus.push(document.getElementById('pesqhistoricoManutencaoOrgaoPagadorForm:calendarioDe.day'));
	}

	if (!validarCampoData('pesqhistoricoManutencaoOrgaoPagadorForm:calendarioAte', '')) {
		campos = campos + msgcampo + ' ' + msgCalendarioAte + ' ' + msgnecessario + '\n';
		objFocus.push(document.getElementById('pesqhistoricoManutencaoOrgaoPagadorForm:calendarioAte.day'));
	}
	
	if (campos != '') {
		alert(campos);
		if (objFocus != null && objFocus.length > 0) {
			objFocus[0].focus();
		}
		return false
	}
	return true;
}