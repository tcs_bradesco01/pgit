function validar(form, msgcampo, msgnecessario, msgTipoMensagem, msgCentroCusto, msgRecurso, msgIdioma, msgCodigoMensagem, msgOrdemLinhaMensagem){

	var campos = '';

	if (form.elements[form.id + ':mensagem'].value == ''){
		campos = campos + msgcampo + ' ' + msgTipoMensagem + ' ' + msgnecessario + '\n';
	}

	if (form.elements[form.id + ':centroCusto'].value == ''){
		campos = campos + msgcampo + ' ' + msgCentroCusto + ' ' + msgnecessario + '\n';
	}	
	
	if (form.elements[form.id + ':recurso'].value == 0){
		campos = campos + msgcampo + ' ' + msgRecurso + ' ' + msgnecessario + '\n';
	}

	if (form.elements[form.id + ':idioma'].value == 0){
		campos = campos + msgcampo + ' ' + msgIdioma + ' ' + msgnecessario + '\n';
	}

	if (allTrim(form.elements[form.id + ':codigoMensagem']) == ''){
		campos = campos + msgcampo + ' ' + msgCodigoMensagem + ' ' + msgnecessario + '\n';
	}

	if (allTrim(form.elements[form.id + ':ordemLinhaMensagem']) == ''){
		campos = campos + msgcampo + ' ' + msgOrdemLinhaMensagem + ' ' + msgnecessario + '\n';
	}

	if (campos !== ''){
		alert(campos);
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'F';
	} else {
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'T';	
	}

}