function validar(form,msgCampo, msgNecessario, msgModalidadeServico ){

	
	var campos = '';
	
	if (form.elements[form.id + ':tipoServico'].value != 0){
		if (form.elements[form.id + ':modalidadeServico'].value == 0){
		
			campos = campos + msgCampo + ' ' + msgModalidadeServico + ' ' + msgNecessario + '\n';
		}
	}
		
	if (campos !== ''){		
		alert(campos);
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'F';
		return false;
	}else{
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'T';
		return true;S
	}
	
}

function checaCamposObrigatoriosIncluir(form,msgCampo, msgNecessario, msgCodigoLancamento, msgTipoServico, msgModalidadeServico, msgLancCredito, msgLancDebito, msgRestricao, msgTipoLancamento ){

	
	var campos = '';

	if (form.elements[form.id + ':tipoServico'].value == 0){
		campos = campos + msgCampo + ' ' + msgTipoServico + ' ' + msgNecessario + '\n';
	}

	if (form.elements[form.id + ':modalidadeServico'].value == 0){
	
		campos = campos + msgCampo + ' ' + msgModalidadeServico + ' ' + msgNecessario + '\n';
	}
	
	if ((!form.elements[form.id + ':tipoLancamentoCredito'].checked) && (!form.elements[form.id + ':tipoLancamentoDebito'].checked)){
		campos = campos + msgCampo + ' ' + msgTipoLancamento + ' ' + msgNecessario + '\n';
	}
	
	if (form.elements[form.id + ':tipoLancamentoCredito'].checked){
		if (form.elements[form.id + ':codLancCredito'].value == 0){
		
			campos = campos + msgCampo + ' ' + msgLancCredito + ' ' + msgNecessario + '\n';
		}
	}
			
	if (form.elements[form.id + ':tipoLancamentoDebito'].checked){
		if (form.elements[form.id + ':codLancDebito'].value == 0){
		
			campos = campos + msgCampo + ' ' + msgLancDebito + ' ' + msgNecessario + '\n';
		}
	}
	
	if (form.elements[form.id + ':hiddenRestricao'].value == 0){
		campos = campos + msgCampo + ' ' + msgRestricao + ' ' + msgNecessario + '\n';
	}

	if (campos !== ''){		
		alert(campos);
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'F';
		return false;
	}else{
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'T';
		return true;S
	}
	
}

function checaCamposObrigatoriosAlterar(form,msgCampo, msgNecessario, msgLancCredito, msgLancDebito, msgRestricao, msgTipoLancamento ){

	
	var campos = '';


	if ((!form.elements[form.id + ':tipoLancamentoCredito'].checked) && (!form.elements[form.id + ':tipoLancamentoDebito'].checked)){
		campos = campos + msgCampo + ' ' + msgTipoLancamento + ' ' + msgNecessario + '\n';
	}
	
	if (form.elements[form.id + ':tipoLancamentoCredito'].checked){
		if (form.elements[form.id + ':codLancCredito'].value == 0){
		
			campos = campos + msgCampo + ' ' + msgLancCredito + ' ' + msgNecessario + '\n';
		}
	}
			
	if (form.elements[form.id + ':tipoLancamentoDebito'].checked){
		if (form.elements[form.id + ':codLancDebito'].value == 0){
		
			campos = campos + msgCampo + ' ' + msgLancDebito + ' ' + msgNecessario + '\n';
		}
	}
	
	if (form.elements[form.id + ':hiddenRestricao'].value == 0){
		campos = campos + msgCampo + ' ' + msgRestricao + ' ' + msgNecessario + '\n';
	}

	if (campos !== ''){		
		alert(campos);
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'F';
		return false;
	}else{
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'T';
		return true;S
	}
	
}
