function validaCampos(form, msgCampo, msgNecessario, tipoServico, modalidadeServico, naturezaServico, servicoComposto, indicadorOrdenacao, servicoRelacionadoNegociavel, 
						negociacao, contingencia, manutencao){

		
	var campos = '';
	
	if (form.elements[form.id + ':tipoServico'].value == 0){
	
		campos = campos + msgCampo + ' ' + tipoServico + ' ' + msgNecessario + '\n';
	}

	
	//if (form.elements[form.id + ':modalidadeServico'].value == 0){
	
		//campos = campos + msgCampo + ' ' + modalidadeServico + ' ' + msgNecessario + '\n';
	//}	
	
	if (form.elements[form.id + ':servicoComposto'].value == 0){
	
		campos = campos + msgCampo + ' ' + servicoComposto + ' ' + msgNecessario + '\n';
	}
	
	if (form.elements[form.id + ':naturezaServico'].value == 0){
	
		campos = campos + msgCampo + ' ' + naturezaServico + ' ' + msgNecessario + '\n';
	}
	
	if (form.elements[form.id + ':txtIndicadorOrdenacao'].value == null || form.elements[form.id + ':txtIndicadorOrdenacao'].value == 0){
		
		campos = campos + msgCampo + ' ' + indicadorOrdenacao + ' ' + msgNecessario + '\n';
	}
	
	var objRdoServRelacNegoc = document.getElementsByName('rdoServicoRelacionadoNegociavel');
	
	if(!objRdoServRelacNegoc[1].checked && !objRdoServRelacNegoc[2].checked){
		campos = campos + msgCampo + ' ' + servicoRelacionadoNegociavel + ' ' + msgNecessario + '!' + '\n';
	}
	
	if(objRdoServRelacNegoc[1].checked){
		if( !document.getElementById(form.id + ':chkNegociacao').checked &&
			!document.getElementById(form.id + ':chkContingencia').checked &&
			!document.getElementById(form.id + ':chkManutencao').checked){
			
			campos = campos +  ' Pelo menos um dos campos ' + negociacao + ' / ' + contingencia + ' / ' + manutencao + ' ' + msgNecessario + '!' + '\n';
			
		}
	}
	
	if (campos !== ''){
		alert(campos);
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'F';
		
	}else{
		form.elements[form.id + ':hiddenObrigatoriedade'].value = 'T';
		
	}
	
}

function validaCamposAlteracao(form, msgCampo, msgNecessario, indicadorOrdenacao, servicoRelacionadoNegociavel, negociacao, contingencia, manutencao){
	var campos = '';
	
	if (form.elements[form.id + ':txtIndicadorOrdenacao'].value == null || form.elements[form.id + ':txtIndicadorOrdenacao'].value == 0){
		
		campos = campos + msgCampo + ' ' + indicadorOrdenacao + ' ' + msgNecessario + '\n';
	}
	
	var objRdoServRelacNegoc = document.getElementsByName('rdoServicoRelacionadoNegociavel');
	
	if(!objRdoServRelacNegoc[1].checked && !objRdoServRelacNegoc[2].checked){
		campos = campos + msgCampo + ' ' + servicoRelacionadoNegociavel + ' ' + msgNecessario + '!' + '\n';
	}
	
	if(objRdoServRelacNegoc[1].checked){
		if( !document.getElementById(form.id + ':chkNegociacao').checked &&
				!document.getElementById(form.id + ':chkContingencia').checked &&
				!document.getElementById(form.id + ':chkManutencao').checked){
			
			campos = campos +  ' Pelo menos um dos campos ' + negociacao + ' / ' + contingencia + ' / ' + manutencao + ' ' + msgNecessario + '!' + '\n';
			
		}
	}
	
	
	if (campos !== ''){
		alert(campos);
		
		return false;
	}
	
	return true;
}
