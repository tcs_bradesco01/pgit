function checaCamposObrigatoriosIncluir(form, msgcampo, msgnecessario, msgFormaLiquidacao, msgSequenciaEnvio, msgHorario, msgHorarioInvalido){
	var msgValidacao = '';
		
			
	if ( document.getElementById(form.id +':formaLiquidacao').value == null || document.getElementById(form.id +':formaLiquidacao').value == '0' ){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgFormaLiquidacao + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if(allTrim(document.getElementById(form.id +':txtSequenciaEnvio')) == ''){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgSequenciaEnvio + ' ' + msgnecessario + '!' + '\n';			
	}
	
	
	
	if (allTrim(document.getElementById(form.id + ':txtHorario')) == ""){
		msgValidacao = msgValidacao +   msgcampo + ' ' + msgHorario + ' ' + msgnecessario + '!\n';				
	}else{
		hora = (allTrim(document.getElementById(form.id + ':txtHorario'))).split(":");
		if (hora.length != 2){
			msgValidacao = msgValidacao + msgHorarioInvalido + '!\n';
		}else{
			if(hora[0] > 23){
				msgValidacao = msgValidacao + msgHorarioInvalido + '!\n';
			}
		}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
	
}

function validaHora(tempo, msg){

  horario = tempo.value.split(":");
  
  if (horario == null) 
  var horas = horario[0];
  var minutos = horario[1];
  var segundos = horario[2];
  
  if(horas > 24){ 
  alert(msgHora); event.returnValue=false; tempo.focus() }
  if(minutos > 59){
  alert(msgMinuto); event.returnValue=false; tempo.focus()}
  if(segundos > 59){
  alert(msgSegundo); event.returnValue=false; tempo.focus()}
}

function validaHoraOnblur(form, msgHorarioInvalido ){

	var msgValidacao = '';
	
	var campo = document.getElementById(form.id + ':txtHorario');

	if(campo.value != ''){
		hora = (allTrim(campo)).split(":");
		if (hora.length != 2){
			msgValidacao = msgValidacao + msgHorarioInvalido + '!\n';
		}else{
			if(isNaN(hora[0]) || hora[0] > 23){
				msgValidacao = msgValidacao + msgHorarioInvalido + '!\n';
			}
			else if(isNaN(hora[1]) || hora[1] > 59){
				msgValidacao = msgValidacao + msgHorarioInvalido + '!\n';
			}
			//else if(isNaN(hora[2]) || hora[2] > 59){
				//msgValidacao = msgValidacao + msgHorarioInvalido + '!\n';
			//}
		}
	}

	if (msgValidacao != ''){
		alert(msgValidacao);
		campo.value = '';
		campo.focus();
		return false;
	}

	return true;

}

