function checaCampoObrigatorioIncluir(form, msgcampo, msgnecessario, msgproduto, msgprioridade, msgdatainicio, msgcentrocusto){
		var campos, hidden;	
		campos = '';
	    var dataValida = true;
	    
		for(i=0; i<form.elements.length; i++){
		
			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:hiddenObrigatoriedade'){
				hidden = form.elements[i];
			}
		
			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:tipoCompromisso'){
				if (allTrim(form.elements[i]) == ''|| allTrim(form.elements[i]) == null){
					campos = campos + msgcampo + ' ' + msgproduto + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:prioridade'){
				if (allTrim(form.elements[i]) == '' || allTrim(form.elements[i]) == null){
					campos = campos + msgcampo + ' ' + msgprioridade + ' ' + msgnecessario + '\n';
				}
			}
			
			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:centroCusto'){
				if (form.elements[i].value == null|| form.elements[i].value == 0)	
					campos = campos + msgcampo + ' ' + msgcentrocusto + ' ' + msgnecessario + '\n';
			}	
			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:dataVigencia.day'){
				if (form.elements[i].value == '' || form.elements[i].value == null)	
					dataValida = false;
			}		
			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:dataVigencia.month'){
				if (form.elements[i].value == '' || form.elements[i].value == null)	
					dataValida = false;
			}		
			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:dataVigenciacia.year'){
				if (form.elements[i].value == '' || form.elements[i].value == null)	
					dataValida = false;
			}					
			
			


		}
		
		if (dataValida == false) 
			campos = campos + msgcampo + ' ' + msgdatainicio + ' ' + msgnecessario + '\n';
		
		
		
		if (campos != ''){
			alert(campos);
			hidden.value  ='F';
			return false;
		}else{
			hidden.value  ='T';
			return true;
		}
}

function checaCampoObrigatorioAlterar(form, msgcampo, msgnecessario, msgdatafim, msgtipocompromisso){
		var campos, hidden;	
		campos = '';
	    var dataValidaFim = true;	    
	    
		for(i=0; i<form.elements.length; i++){
		
			if (form.elements[i].id == 'altManterPrioridadeCreditoTipoCompromissoForm:hiddenObrigatoriedade'){
				hidden = form.elements[i];
			}
			
			if (form.elements[i].id == 'altManterPrioridadeCreditoTipoCompromissoForm:prioridade'){
				if (form.elements[i].value == ''|| form.elements[i].value == null){
					campos = campos + msgcampo + ' ' + msgtipocompromisso + ' ' + msgnecessario + '\n';
				}
			}			
		}

		if (dataValidaFim == false) 
			campos = campos + msgcampo + ' ' + msgdatafim + ' ' + msgnecessario + '\n';			

		if (campos != ''){
			alert(campos);
			hidden.value  ='F';
			return false;
		}else{
			hidden.value  ='T';
			return true;
		}
}

function limpaCampos (form){

	if (form.elements[form.id + ':tipoCompromisso'].value != ""){
		form.elements[form.id + ':prioridade'].value = "";
			
	}
}

function checaCentroCusto(form, msgcampo, msgnecessario,msgcentrocusto){
		if (allTrim(document.getElementById(form.id + ':centroCustoFiltro')) == ''){
			alert(msgcampo + ' ' + msgcentrocusto + ' ' + msgnecessario + '\n');
			return false;
		}
		else{
			return true;
		}
	}