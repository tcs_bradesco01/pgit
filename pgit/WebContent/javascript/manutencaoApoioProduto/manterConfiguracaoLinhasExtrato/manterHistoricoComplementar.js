function checaCamposMantHistComp(form, msgCampo, msgNecessario, msgCod, msgProduto, msgOperacao, msgCodLan, msgInicioCentroCustoCredito, msgCentroCustoCredito,  msgIdiomaCre, 
									msgRecursoCre, msgEventoMsgCre, msgInicioCentroCustoDebito, msgIdiomaDeb,  msgRecursoDeb, msgEventoMsgDeb, msgRestricao){

		var campos, hidden, flag=0, deb=0, cred=0, centroCustroCredito=0;
		campos = '';
		
		for(i=0; i<form.elements.length; i++){
		
		
			if (form.elements[i].id == form.id + ':hiddenObrigatoriedade'){
				hidden = form.elements[i];
			}
			
//			if (form.elements[i].id == form.id + ':txtCodigoHistoricoComplementar'){
//				if (form.elements[i].value == ''){
//					campos = campos + msgCampo + ' ' + msgCod + ' ' + msgNecessario + '\n';
//				}
//			}
//			
			if (form.elements[i].id == form.id + ':tipoServico'){
				if (form.elements[i].value == '0'){
					campos = campos + msgCampo + ' ' + msgProduto + ' ' + msgNecessario + '\n';
				}
			}
			
			if (form.elements[i].id == form.id + ':modalidadeServico'){
				if (form.elements[i].value == '0'){
					campos = campos + msgCampo + ' ' + msgOperacao + ' ' + msgNecessario + '\n';
				}
			}
			
			if (form.elements[i].id == form.id + ':tipoLancamentoCredito'){
				if (form.elements[i].checked == false){
					flag = 1;
				}
				if (form.elements[i].checked == true){
					cred = 1;
				}
				
			}
			
			if (form.elements[i].id == form.id + ':tipoLancamentoDebito'){
				if (form.elements[i].checked == false && flag == 1){
					campos = campos + msgCampo + ' ' + msgCodLan + ' ' + msgNecessario + '\n';
				}
				if (form.elements[i].checked == true){
					deb = 1;
				}
			}
			
			if(cred == 1){
			
				if (form.elements[i].id == form.id + ':txDsSegundaLinhaExtratoCredito'){
					
					
					if ( form.elements[i].value.search( /[^ a-z0-9]/i ) != -1 )
					{
						campos += "N\u00E3o \u00E9 permitido caracteres especiais no campo descri\u00E7\u00E3o da 2\u00AA de extrato" ;
						
					}
					
					if (form.elements[i].value == ''){
						campos = campos + 'Descri\u00E7\u00E3o da 2\u00AA de extrato para lan\u00E7amento a cr\u00E9dito \u00E9 obrigat\u00F3rio' + '\n';
					}
				}
			}

			
			if (form.elements[i].id == form.id + ':hiddenRestricao'){
				if (form.elements[i].value != '1' && form.elements[i].value != '2'){
					campos = campos + msgCampo + ' ' + msgRestricao + ' ' + msgNecessario + '\n';
					}
			}
			
		}
		
		if (campos != ''){
			hidden.value  ='F';
			alert(campos);
			campos = '';
		}else{
			hidden.value  ='T';
		}
}

function checaCamposAlterar(form, msgCampo, msgNecessario, msgCodLan, msgInicioCentroCustoCredito, msgIdiomaCre, msgRecursoCre,
									msgEventoMsgCre, msgInicioCentroCustoDebito, msgIdiomaDeb,  msgRecursoDeb, msgEventoMsgDeb, msgRestricao){

		var campos, hidden, flag=0, deb=0, cred=0;
		campos = '';
		
		for(i=0; i<form.elements.length; i++){
		
		
			if (form.elements[i].id == form.id + ':hiddenObrigatoriedade'){
				hidden = form.elements[i];
			}
			
			
			if (form.elements[i].id == form.id + ':tipoLancamentoCredito'){
				if (form.elements[i].checked == false){
					flag = 1;
				}
				if (form.elements[i].checked == true){
					cred = 1;
				}
				
			}
			
			if (form.elements[i].id == form.id + ':tipoLancamentoDebito'){
				if (form.elements[i].checked == false && flag == 1){
					campos = campos + msgCampo + ' ' + msgCodLan + ' ' + msgNecessario + '\n';
				}
				if (form.elements[i].checked == true){
					deb = 1;
				}
			}
			
			if(cred == 1){
				if (form.elements[i].id == form.id + ':txDsSegundaLinhaExtratoCredito'){
					
					
					if ( form.elements[i].value.search( /[^ a-z0-9]/i ) != -1 )
					{
						campos += "N\u00E3o \u00E9 permitido caracteres especiais no campo descri\u00E7\u00E3o da 2\u00AA de extrato" ;
						
					}
					
					if (form.elements[i].value == ''){
						campos = campos + 'Descri\u00E7\u00E3o da 2\u00AA de extrato para lan\u00E7amento a cr\u00E9dito \u00E9 obrigat\u00F3rio' + '\n';
					}
				}
				
			}
			
			if (form.elements[i].id == form.id + ':hiddenRestricao'){
				if (form.elements[i].value != '1' && form.elements[i].value != '2'){
						campos = campos + msgCampo + ' ' + msgRestricao + ' ' + msgNecessario + '\n';
					}
			}
			
		}
		
		if (campos != ''){
			hidden.value  ='F';
			alert(campos);
			campos = '';
		}else{
			hidden.value  ='T';
		}
		
		

}