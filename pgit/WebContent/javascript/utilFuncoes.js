function exibirMensagemRetornoServlet(mensagem) {

   if ((mensagem != "null") && (mensagem != "") && (mensagem != null)) {       
       alert(mensagem);
   }
}



function mostrarCalendario(nomeCampo) {

    var campo1 = 'document.frm1.' + nomeCampo + '.disabled';
    var campo2 = 'showCalendar(document.frm1.' + nomeCampo  + ')';
        
	if (!eval(campo1)) {
   	    setDateField2(document.frm1.Fecha_reg); 
   	    eval(campo2);
	}
}	


function consistirData(objeto) {

  var bRet     = false;
  var bProbl   = true;
  var sData    = eval('document.' + objeto + '.value');
  var vDiasMes = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

  if(sData != '') {
      var sValidChars = '0123456789/.-';
      var sValidDivs  = '/.-';
      var sNewData    = '';
      var nCountDiv   = 0;
      for(var i=0; i < sData.length; i++)
        {
          var sChar = sData.charAt(i);
          if(sValidChars.indexOf(sChar) == -1){ sChar = ''; }
          if(sValidDivs.indexOf(sChar) != -1)
            { sChar = '/'; 
              nCountDiv++;
            }
          sNewData = sNewData + sChar;
        }
      if(nCountDiv < 2)
       { sNewData += '/';
         nCountDiv++;
       }
      if(nCountDiv != 2)
        { alert('Formato da data inv\u00E1lido \n Utilize o formato dd/mm/aaaa \n (d = Dia, m = M\u00EAs e a = Ano)!');
          bProbl = false;
        }
      else
        {
          var nDivDay = sNewData.indexOf('/');
          if(nDivDay != 0)
            {
              var sDay         = sNewData.substring(0, nDivDay);
              while(sDay.charAt(0) == '0')
                { sDay = sDay.substring(1, sDay.length); }

              var sNewDataTemp = sNewData.substring(nDivDay+1, sNewData.length);

              nDivMonth = sNewDataTemp.indexOf('/');
              if(nDivMonth != 0)
                {
                  var sMonth = sNewDataTemp.substring(0, nDivMonth);
                  while(sMonth.charAt(0) == '0')
                    { sMonth = sMonth.substring(1, sMonth.length);
                    }

                  var sYear = parseInt(sNewDataTemp.substring(nDivMonth+1, sNewDataTemp.length));
                  sYear = parseInt(sYear);
                  var xData = new Date();
                  if(isNaN(sYear)) sYear = xData.getYear();

                  if(sYear  < 10) {sYear = '200' + sYear;}
                  else if(sYear  < 50) {sYear = '20' + sYear;}
                  else if(sYear  < 100) {sYear = '19' + sYear;}
                  else if(sYear  < 1000) {sYear = '2' + sYear;}
                  else if(sYear  > 9999){
                    alert('Formato da data inv\u00E1lido \nAno inv\u00E1lido!');
                    bProbl = false;
                  }

                  if( parseInt(sDay) == 0  && bProbl ){
                    alert('Formato da data inv\u00E1lido \nDia inv\u00E1lido!');
                    bProbl = false;
                  }

                  if( parseInt(sMonth) == 0  && bProbl ){
                    alert('Formato da data inv\u00E1lido \nM\u00EAs inv\u00E1lido!');
                    bProbl = false;
                  }

                  if( sYear > 2099 && bProbl ){
                    alert('Formato da data inv\u00E1lido \nS\u00E9culo inv\u00E1lido!');
                    bProbl = false;
                  }

                  if( bProbl ){
                    if(((parseInt(sYear) % 4 == 0) && (parseInt(sYear) % 100 != 0)) || (parseInt(sYear) % 400 == 0))
                    { vDiasMes[1] = 29;
                    }

                    if((parseInt(sDay) > vDiasMes[parseInt(sMonth)-1])||(parseInt(sDay) < 1)){
                      alert('Formato da data inv\u00E1lido \nDia inv\u00E1lido \nUtilize n�meros de 1 a ' + vDiasMes[parseInt(sMonth)-1] + '!');
                      bProbl = false;
                    }
                    else
                    {
                      if((parseInt(sMonth) > 12)||(parseInt(sMonth) < 1)) {
                        alert('Formato da data inv\u00E1lido \nM\u00EAs inv\u00E1lido \nUtilize n�meros de 1 a 12!');
                        bProbl = false;
                      }else{
                        sDay   = parseInt(sDay);
                        sMonth = parseInt(sMonth);
                        sYear  = parseInt(sYear);
                        if(sDay   < 10) {sDay = '0' + sDay;}
                        if(sMonth < 10) {sMonth = '0' + sMonth;}
                        eval('document.' + objeto + '.value = "' + sDay + '/' + sMonth + '/' + sYear + '";');
                        bRet = true;
                      }
                    }
                  }
                }
              else
               { alert('Formato da data inv\u00E1lido \nUtilize o formato dd/mm/aaaa \n (d = Dia, m = M\u00EAs e a = Ano)!');
                 bProbl = false;
               }
            }
          else
            { alert('Formato da data inv\u00E1lido \nUtilize o formato dd/mm/aaaa \n (d = Dia, m = M\u00EAs e a = Ano)!');
              bProbl = false;
            }
        }
    }

  if(!bProbl)
    { eval('document.' + objeto + '.focus()');
    }
  return bRet;
}



//-- verifica se segunda data � maior que a primeira

function validarPeriodo(oData1, oData2) {

   var data1 = eval('document.frm1.' + oData1 + '.value');
   var data2 = eval('document.frm1.' + oData2 + '.value');
      
   if(data1 != "" && data2 != "" ) {
      data1 = new String(data1);
      data2 = new String(data2);

      data1 = data1.split('/');
      data2 = data2.split('/');

      data1 = parseInt( data1[2] + data1[1] + data1[0] );
      data2 = parseInt( data2[2] + data2[1] + data2[0] );
      
      if (data1 > data2 ) {
          alert('Per\u00EDodo inicial deve ser menor ou igual ao per\u00EDodo Final!');
          eval('document.frm1.' + oData1 + '.focus()');
      } 
   }
}


function validarData(data1) {
  
   var oData = new Date();
   
   dia = oData.getDate();
   dia = '' + dia;
   mes = oData.getMonth() + 1;
   mes = '' + mes;
   ano = oData.getFullYear();

   if (mes.length == 1) {
   	  mes = "0" + mes;
   }
   
   if (dia.length == 1) {
   	  dia = "0" + dia;
   }
   data2 = dia + "/" + mes + "/" + ano;   

   if(data1 != "" && data2 != "" ) {

      data1 = new String(data1);
      data2 = new String(data2);
			
      data1 = data1.split('/');
      data2 = data2.split('/');

      data1 = parseInt( data1[2] + data1[1] + data1[0] );
      data2 = parseInt( data2[2] + data2[1] + data2[0] );
      
      if (data1 < data2 ) {
          return false;
          //eval('document.frm1.' + campo + '.focus()');
      } 
   }
	
}	

//-- se data igua � data do dia verifica se hor�rio � maior que o hor�rio corrente

function validarHora(data,hora) {

   var oData = new Date();	
	
   dia = oData.getDate();
   dia = '' + dia;
   mes = oData.getMonth() + 1;
   mes = '' + mes;
   ano = oData.getFullYear();

   if (mes.length == 1) {
   	  mes = "0" + mes;
   }
   
   if (dia.length == 1) {
   	  dia = "0" + dia;
   }

   dataAtual = dia + "/" + mes + "/" + ano;   

   if (dataAtual == data) {   
       
	   hatual = oData.getHours() ;
	   matual = oData.getMinutes() ;
	
	   hinf = hora.substr(0,2);
	   minf = hora.substr(3,4);
	   
	   if (hinf < hatual) {
	      return false;
	      
	   } else {
	   	  if (hatual == hinf) {
	   	  	 if (minf < matual) {	
	   	  	     return false;
	   	     }
	   	  }		
	   }	
	
	return true;	 
   
    } else {    
   		if (validarData(data) == false){   
   			return false;  
   			 			
  	 	} else {  	
		    return true;	
	    }  		   
	 }    

	return true;     
}



//-- Efetua a valida��o e ajuste de string para ficar no formato hora (hh:mm) */

function validaHora(object) {

   var sHora = object.value; 

   if (sHora != '') {
     var sCharValid   = '0123456789:';
     var sChangeChars = ',.-+/\\';
     var sNewHora   = '';
     var nCountDiv = 0;
     for(i=0;i < sHora.length;i++)
       { var sChar = sHora.charAt(i);
         if(sCharValid.indexOf(sChar) == -1)
           { if(sChangeChars.indexOf(sChar) != -1)
              sChar = ':';
             else
              sChar = '';
           }
         if(sChar == ':'){nCountDiv++;}
         sNewHora = sNewHora + sChar;
       }
     if((nCountDiv == 0)&&(sNewHora.length < 3))
       {  sNewHora  = sNewHora + ':';
          nCountDiv = 1;
       }
     if((nCountDiv != 1)||(sNewHora.length == 1))
       { alert('Formato da hora inv\u00E1lido !!!\n    Utilize o formato hh:mm\n    (h = hora, m = minuto)');
         object.value = "";
       }
     else
       { nPosDiv = sNewHora.indexOf(':');
         if(nPosDiv == 0)
           { alert('Formato da hora inv\u00E1lido !!!\n    Utilize o formato hh:mm\n    (h = hora, m = minuto)');
             object.value = "";
           }
         else
           {
             if(nPosDiv == (sNewHora.length - 1)){ sNewHora = sNewHora + '0'; }
             var nHora   = sNewHora.substring(0, nPosDiv);
             while(nHora.charAt(0) == '0')
                nHora = nHora.substring(1, nHora.length);
             if(nHora == ''){nHora = '0'}

             var nMinuto = sNewHora.substring(nPosDiv + 1, sNewHora.length);
             while(nMinuto.charAt(0) == '0')
                nMinuto = nMinuto.substring(1, nMinuto.length);
             if(nMinuto == ''){nMinuto = '0'}

             if(nHora > 23)
               { alert('Hora inv\u00E1lida !!!\n Coloque um valor v\u00E1lido entre 0 e 23.');
                 object.focus();
               }
             else
               {
                 if(nMinuto > 59)
                   { alert('Minuto inv\u00E1lida !!!\n Coloque um valor v\u00E1lido entre 0 e 59.');
                     object.focus();
                   }
                 else
                   { if((parseInt(nHora)   < 10)&&(nHora.length   = 1)){ nHora   = '0' + parseInt(nHora); }
                     if((parseInt(nMinuto) < 10)&&(nMinuto.length = 1)){ nMinuto = '0' + parseInt(nMinuto); }
                     object.value = nHora + ':' + nMinuto;
                   }
               }
           }
       }
   }
}


//-- Verifica se o hor�rio inicial � menor que o final

function validarHorarios(hora1,hora2) {

	h1 = hora1.substr(0,2);
	m1 = hora1.substr(3,4);
	h2 = hora2.substr(0,2);
	m2 = hora2.substr(3,4);
	
	if (hora1 == hora2) {
		return false;	
	}	

	if (h1 > h2) {	    
		return false;
	}else{
	    if (h1 == h2) {
			if (m2 < m1) {
 				return false;
			}
 	    }	
    }
    return true;
}	

function formataReais(fld, milSep, decSep, limite, e) {
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = e.keyCode;
    
    //if (whichCode == 8 ) return true; //backspace - estamos tratando disso em outra funcao no keydown
    if (whichCode == 0 ) return true;
    if (whichCode == 9 ) return true; //tecla tab
    if (whichCode == 13) return true; //tecla enter
    if (whichCode == 16) return true; //shift internet explorer
    if (whichCode == 17) return true; //control no internet explorer
    if (whichCode == 27 ) return true; //tecla esc
    if (whichCode == 34 ) return true; //tecla end
    if (whichCode == 35 ) return true;//tecla end
    if (whichCode == 36 ) return true; //tecla home
    
    /*
    O trecho abaixo previne a padrao nos navegadores. Nao estamos inserindo o caractere normalmente, mas via script
    */
    
    if (e.preventDefault){ //standart browsers
                                   e.preventDefault()
                   }else{ // internet explorer
                                   e.returnValue = false
    }
    
    var key = String.fromCharCode(whichCode);  // Valor para o codigo da Chave
    if (strCheck.indexOf(key) == -1) return false;  // Chave invalida
    
    if (fld.value.length > limite) {
                   return;
    }
    /*
    Concatenamos ao value o keycode de key, se esse for um numero
    */
    fld.value += key;
    
    var len = fld.value.length;
    var bodeaux = demaskvalue(fld.value,true).formatCurrency();
    fld.value=bodeaux;
    
    /*
    Essa parte da funcao tao somente move o cursor para o final no opera. Atualmente nao existe como move-lo no konqueror.
    */
      if (fld.createTextRange) {
                   var range = fld.createTextRange();
                   range.collapse(false);
                   range.select();
      }
      else if (fld.setSelectionRange) {
                   fld.focus();
                   var length = fld.value.length;
                   fld.setSelectionRange(length, length);
      }
    
      return false;
}

function reais(obj, limite, event){

    var whichCode = (window.Event) ? event.which : event.keyCode;
    /*
    Executa a formatacao apos o backspace nos navegadores !document.all
    */
    if (whichCode == 8 && !documentall) {
    
       if (event.preventDefault){ //standart browsers
                                       event.preventDefault();
                       }else{ // internet explorer
                                       event.returnValue = false;
       }
       var valor = obj.value;
       var x = valor.substring(0,valor.length-1);
       obj.value= demaskvalue(x,true).formatCurrency();
       return false;
    }
    /*
    Executa o Formata Reais e faz o format currency novamente apos o backspace
    */
    
    formataReais(obj,'.',',', limite, event);

} // end reais

function demaskvalue(valor, currency){
	/*
	* Se currency  false, retorna o valor apenas com os numeros. Se true, os dois ultimos caracteres sao considerados as
	* casas decimais
	*/
	var val2 = '';
	var strCheck = '0123456789';
	var len = valor.length;
    if (len== 0){
                   return 0.00;
    }
    
    if (currency ==true){
       /* Elimina os zeros a esquerda
       * a vari?vel  <i> passa a ser a localiza??o do primeiro caractere apos os zeros e
       * val2 contem os caracteres (descontando os zeros a esquerda)
       */
	
       for(var i = 0; i < len; i++)
                       if ((valor.charAt(i) != '0') && (valor.charAt(i) != ',')) break;

       for(; i < len; i++){
                       if (strCheck.indexOf(valor.charAt(i))!=-1) val2+= valor.charAt(i);
       }

       if(val2.length==0) return "0.00";
       if (val2.length==1)return "0.0" + val2;
       if (val2.length==2)return "0." + val2;

       var parte1 = val2.substring(0,val2.length-2);
       var parte2 = val2.substring(val2.length-2);
       var returnvalue = parte1 + "." + parte2;
       return returnvalue;

    }
    else{
       /* currency  false: retornamos os valores COM os zeros a esquerda,
       * sem considerar os ultimos 2 algarismos como casas decimais
       */
       val3 ="";
       for(var k=0; k < len; k++){
                       if (strCheck.indexOf(valor.charAt(k))!=-1) val3+= valor.charAt(k);
       }
    return val3;
    }
}

function formatamoney(c) {
    var t = this; if(c == undefined) c = 2;
    var p, d = (t=t.split("."))[1].substr(0, c);
    for(p = (t=t[0]).length; (p-=3) >= 1;) {
                        t = t.substr(0,p) + "." + t.substr(p);
    }
    return t+","+d+Array(c+1-d.length).join(0);
}

String.prototype.formatCurrency=formatamoney

function Mascara_Hora(obj){ 
	var hora01 = ''; 
	hora01 = hora01 + obj.value; 
	if (hora01.length == 2 || hora01.length == 5){ 
		hora01 = hora01 + ':';
		obj.value = hora01; 
	} 
	//if (hora01.length == 8){ 
		//Verifica_Hora(obj); 
	//} 
} 
           
function Verifica_Hora(obj){ 
	hrs = (obj.value.substring(0,2)); 
	min = (obj.value.substring(3,5)); 
	seg = (obj.value.substring(6,8));

    if(obj.value.length > 0){     
		estado = ""; 
		if ((hrs < 00 ) || (hrs > 23) || ( min < 00) ||( min > 59) || ( seg < 00) ||( seg > 59)){ 
			estado = "errada"; 
		} 
		
		if(obj.value.length < 8){
			estado = "errada";
		}
		
		if(obj.value.substring(2,3) != ":" || obj.value.substring(5,6) != ":"){
			estado = "errada";
		}
	               
		if (obj.value == "") { 
			estado = "errada"; 
		} 
	
		if (estado == "errada") { 
			alert("Hora inv\u00E1lida!"); 
			obj.focus(); 
		} 
	}
} 

function alteraCor(classe, item){
	var classeAtual = document.getElementById(item).className;
	if ((classe == 'visualizacao') && (classeAtual == 'ttltab1')){
		document.getElementById(item).className = 'ttlTab1Visualizado';
	}else{
		if ((classe == 'normal') && (classeAtual == 'ttlTab1Visualizado')){
			document.getElementById(item).className = 'ttltab1';
		}else{
			var lista = document.getElementById(item).parentNode.parentNode.getElementsByTagName("td");
			
			for (i = 0; i<= lista.length-1; i++){
				lista[i].firstChild.className = 'identificacao';
			}
			document.getElementById(item).className = 'itemNivel2Selecionado';
		}
	}
}

function alteraBotao(classe, item){
	var classeAtual = document.getElementById(item).className;
	if ((classe == 'visualizacao') && (classeAtual == 'bto1')){
		document.getElementById(item).className = 'bto1Visualizado';
	}else{
		if ((classe == 'normal') && (classeAtual == 'bto1Visualizado')){
			document.getElementById(item).className = 'bto1';
		}
	}
}

function alteraSetaTabelaOver(label,objetoExiste,existe,naoExiste){
	document.getElementById(label).className = 'imageOver';
	if(document.getElementById(objetoExiste)){
		document.getElementById(existe).src = '/pgit/images/setaDescBranco.gif';
	}else{
		document.getElementById(naoExiste).src = '/pgit/images/setaAscBranco.gif';
	}
}

function alteraSetaTabelaOut(label,objetoExiste,existe,naoExiste){
	document.getElementById(label).className = 'imageOut';
	if(document.getElementById(objetoExiste)){
		document.getElementById(existe).src = '/pgit/images/setaDesc.gif';
	}else{
		document.getElementById(naoExiste).src = '/pgit/images/setaAsc.gif';
	}
}

function loadMasks() {
	
	includeJavaScript('/pgit/javascript/jquery.meio.mask.js', loadMasksJQuery);
}

function loadMasksJQuery() {
	jQuery.mask.masks = jQuery.extend(jQuery.mask.masks,{
	 decimalBr:{ mask : '99,999.999.999.999.999', type : 'reverse' },
     decimalBrDefault:{ mask : '99,999.999.999.999.999', type : 'reverse', defaultValue: '000' },
     decimalBr15:{ mask : '99,999.999.999.999.9', type : 'reverse' },     
     decimalBr4:{ mask : '99,99', type : 'reverse' },
     decimalBr9:{ mask : '999,999,999', type : 'reverse' },
     decimalBr9Ponto:{ mask : '999.999.999', type : 'reverse' },
     indice:{ mask : '99999999,999999999', type : 'reverse', defaultValue: '000000000'},
	 numerico13:{ mask : '9999999999999' },
     numerico11:{ mask : '99999999999' },
     numerico10:{ mask : '9999999999' },
     numerico9:{ mask : '999999999' },
     numerico5:{ mask : '99999' },
     numerico4:{ mask : '9999' },
     numerico3:{ mask : '999' },
     numerico2:{ mask : '99' },
     phoneNumber9:{ mask : '9999-99999', type : 'reverse' },
     percentual:{ mask : '99,999', type : 'reverse' },
     percentualDefault:{ mask : '99,999', type : 'reverse', defaultValue: '000' },
     percSimTarifa:{ mask : '99,999.99', type : 'reverse' },
     hora: { mask : '29:59' },
     horaMinSeg:{ mask : '99:99', type : 'reverse' },
     mesAno:{ mask : '9999/99', type : 'reverse' },
     horaMinSeg2:{ mask : '29:59:59' }
  });

  	setMasks();
}

function setMasks() {
	
	(function(jQuery){jQuery(function(){jQuery('input:text').setMask();});})(jQuery);
}

function includeJavaScript(jsFile, loadFunction) {
	var domScript = document.createElement("script");
	domScript.type = "text/javascript";
	domScript.src = jsFile;
	document.body.appendChild(domScript);

	domScript.onreadystatechange = function () {
		if (domScript.readyState == 'loaded' || domScript.readyState == 'complete') {
		
        	loadFunction();
        }
    }
}

function desbloquearTela() {
	showDiv(false);
	FormSubmit_bloquerTeclado(false);
}

function bloquearTela() {
	showDiv(true);
	FormSubmit_bloquerTeclado(true);
}

function esconderCombosModal() {
	var objModal = document.getElementById('uiModalMessagesBodyDetailsDiv');
	if (objModal != null && objModal.style.display == 'block') {
		$('select').css({visibility:"hidden"});
	}
}

function trim(entrada) {
 	if(entrada == null) return '';
	var expressao = /(^\s*)|(\s*$)/;
	return entrada.replace (expressao, "");
}

function isEmpty(entrada) {
 	return (entrada == null || trim(entrada) == '');
}

// Verifica se campo numerico contem apenas numeros
// Em caso de digitacao, ser� validado pelo OnlyNum()
// Em caso de colar (CTRL+V) valida-se aqui disparado pelo onblur.

function validaCampoNumerico(campo, msgValidacao) {
	var campoTrim = campo.value.replace(/^\s+|\s+$/g,"");

	// se nao for 100% numerico ou conter pontos
	if (isNaN(campo.value) || (campo.value.indexOf('.') != -1 ))  {
		alert(msgValidacao);
		campo.value = '';
		campo.focus();
		return false;
	}	
	else if(campoTrim != campo.value){
		alert(msgValidacao);
		campo.value = '';
		campo.focus();
		return false;	
	}	
	else{
		return true;
	}	
}

function validaCampoDecimal(campo, msgValidacao) {
	var campoTrim = campo.value.replace(/^\s+|\s+$/g,"");
	
	// se nao for 100% numerico ou contiver pontos
	var valor = campo.value;
	
    while (valor.indexOf(".") > 0){
        valor = valor.replace(".","");	
    }
	valor = valor.replace(",", ".");
	
	if (isNaN(valor))  {
		alert(msgValidacao);
		campo.value = '';
		campo.focus();
		return false;
	}
	else if(campoTrim != campo.value){
		alert(msgValidacao);
		campo.value = '';
		campo.focus();
		return false;	
	}	
	else{
//		campo.value = valor;
		campo.value = $.mask.string( valor, {mask : '99,999.999.999.999.999', type : 'reverse'});
		return true;
	}	
}

function validaDigitoConta(campo, msgValidacao){
	
	var permitido="ABCDEFGHIJKLMNOPQRSTUVXZWY0123456789";
	if (campo.value != null){
		campo.value = campo.value.toUpperCase();
		for (var i=0;i<campo.value.length;i++){   
	        campo_temp=campo.value.substring(i,i+1)   
	        if (permitido.indexOf(campo_temp)==-1){   
	           alert(msgValidacao);
			   campo.value = '';
   		       campo.focus();
   		       return false;
	        }   
	    }   
	}

 	return true;

}

function loadModalJQuery() {
	includeJavaScript('/pgit/faces/jquery-1.2.6.pack.js', loadModalJQuerySimple);
}

function loadModalJQuerySimple() {
	includeJavaScript('/pgit/faces/jquery.simplemodal-1.2.2.pack.js', function(){});
}


function validaCampoNumericoBlur(form, campo, msgValidacao, flag){


	var objeto = document.getElementById(form.id + ':hiddenFoco');	
	var valida = validaCampoNumerico(campo, msgValidacao);
	

	//vindo de banco
	if (flag == 1 && valida){
		objeto.value= 'txtAgenciaContaDebito';

	}
	
	if (flag == 1 && !valida){
		objeto.value='txtBancoContaDebito';

	}
	
	//vindo de agencia
	if (flag == 2 && valida){
		objeto.value= 'txtContaContaDebito';
	}
	
	if (flag == 2 && !valida){
		objeto.value='txtAgenciaContaDebito';
	}
	
	//vindo de conta
	if (flag == 3 && valida){
		objeto.value= 'txtDigitoContaDebito';	
	}
	
	if (flag == 3 && !valida){
		objeto.value='txtContaContaDebito';
	}
		
}

function focoBlur(form){


	var objeto = document.getElementById(form.id + ':hiddenFoco');

	if (objeto.value != ''){
		var objetoFoco = document.getElementById(form.id + ':' + objeto.value);
		objetoFoco.focus();
		objeto.value = '';
	}
}






function validaDigitoContaBlur(form, campo, msgValidacao, id){


	var objeto = document.getElementById(form.id + ':hiddenFoco');
	var valida = validaDigitoConta(campo, msgValidacao);

	if (!valida){		
		objeto.value = id;
	}else{
		objeto.value = '';
	}
	
}

function cleanClipboard() {
	clipboardData.clearData();
}


function desabilitaPaginacaoDefault(form, radio, botoes){
	var radios = document.getElementsByName(radio);

	habilitaDesablitaBotoes(form, botoes, true);
	for(var i = 0; i < radios.length; i++){
		radios[i].checked = false;
	}
}

function habilitaDesablitaBotoes(form, botoes, estado){
	for(var i = 0; i < botoes.length; i++){
		if (document.getElementById(form.id + ':' + botoes[i]) != null) {
			document.getElementById(form.id + ':' + botoes[i]).disabled = estado;
		}
	}
}

function renegociar(nseqContrNegoc, cpssoaJuridContr, ctpoContrNegoc){
	if (document.getElementById("conManterSolManutContratoForm:renegociavel").value == 'S') {
		document.location.href = '/pgit_negociacao/proposta.renegociacao.jsf?nseqContrNegoc=' + nseqContrNegoc + '&cpssoaJuridContr=' + cpssoaJuridContr + '&ctpoContrNegoc=' + ctpoContrNegoc;
	} else {
		desbloquearRenegociar();
		return false;
	}
}

function renegociarNovo(url){
	if (url != null && url != '') {
		document.location.href = url;
	} else {
		desbloquearRenegociarNovo();
		return false;
	}
}

//Limita quantidade de caracteres no textArea
function limitarTextArea(campo, limite) {
	cleanClipboard();
	if(campo.value.length > limite){
		alert('O campo Observa\u00E7\u00E3o s\u00F3 permite a digita\u00E7\u00E3o de 200 caracteres.');
		campo.value = campo.value.substring(0,limite);
		if(campo.value.length == limite + 1){
			campo.value = campo.value.substring(0,limite - 1);
		}
	}
}