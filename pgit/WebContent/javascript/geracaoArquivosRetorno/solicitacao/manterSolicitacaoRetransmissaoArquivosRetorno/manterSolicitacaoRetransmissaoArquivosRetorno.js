function validaCamposConsultar(form, msgcampo, msgnecessario, msgdata){


	var msgValidacao = '';

	
	if ( document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.day').value == null ||
		 document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.day').value == ''   ||
		 document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.month').value == null ||
		 document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.month').value == ''   ||
		 document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.year').value == null ||
		 document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if ( document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.day').value == null ||
		 document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.day').value == ''   ||
		 document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.month').value == null ||
		 document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.month').value == ''   ||
		 document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.year').value == null ||
		 document.getElementById('conManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	

	return true;						
}

function validaCamposIncluir(form, msgcampo, msgnecessario, msgdata){


	var msgValidacao = '';

	
	if ( document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.day').value == null ||
		 document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.day').value == ''   ||
		 document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.month').value == null ||
		 document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.month').value == ''   ||
		 document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.year').value == null ||
		 document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoInicio.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if ( document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.day').value == null ||
		 document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.day').value == ''   ||
		 document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.month').value == null ||
		 document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.month').value == ''   ||
		 document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.year').value == null ||
		 document.getElementById('incManterSolicRetransmissaoArquivosRetornoForm:txtPeriodoFim.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	

	return true;						
}

function selecionarTodos(form, check){

	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}			
	}	

	document.getElementById(form.id + ':btnAvancarIncluir').disabled = !check.checked;		
}


function validarCampoDescontoTarifa(object) {
	var percentual = object.value.replace(",",".");

	if(percentual > 100.00){
		//object.value = object.value.substring(0, object.value.length-1) 
		object.focus();
		alert("Desconto permitido na faixa de 0,00 a 100,00");
	}
}