function validaCamposConsultar(form, msgcampo, msgnecessario, msgdata){


	var msgValidacao = '';

	
	if ( document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoInicio.day').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoInicio.day').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoInicio.month').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoInicio.month').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoInicio.year').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoInicio.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if ( document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoFim.day').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoFim.day').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoFim.month').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoFim.month').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoFim.year').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseFavForm:txtPeriodoFim.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	

	return true;						
}

function validaCamposIncluir(form, msgcampo, msgnecessario, msgdestino){

	var objRdoDestino = document.getElementsByName('radioDestino');
	
	if ( !objRdoDestino[1].checked && !objRdoDestino[2].checked ){
		alert(msgcampo + ' ' + msgdestino + ' ' + msgnecessario);
		return false;	
	}

	return true;						
}

function validarProxCampoIdentClienteContratoFlag(form, flagPesquisa){
	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCnpjFilial = document.getElementById(form.id + ':txtFilial');
	var campoCnpjControle = document.getElementById(form.id + ':txtControle');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoCpfControle = document.getElementById(form.id + ':txtControleCpf');
	var campoNomeRazao = document.getElementById(form.id + ':txtNomeRazaoSocial');
	var campoBanco = document.getElementById(form.id + ':txtBanco');
	var campoAgencia = document.getElementById(form.id + ':txtAgencia');
	var campoConta = document.getElementById(form.id + ':txtConta');
	
	if(objRdoCliente[0].checked){
		campoCnpj.disabled = false;
		campoCnpj.focus();
		campoCnpjFilial.disabled = false;
		campoCnpjControle.disabled = false;
		
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCpf.value = '';
		campoCpfControle.value = '';
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[1].checked){
		campoCpf.disabled = false;
		campoCpf.focus();
		campoCpfControle.disabled = false;
	
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[2].checked){
		campoNomeRazao.disabled = false;
		campoNomeRazao.focus();
			
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[3].checked){
		campoBanco.disabled = false;
		campoAgencia.disabled = false;
		campoConta.disabled = false;
		campoBanco.value = 237;
		campoAgencia.focus();
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else{
		document.getElementById(form.id + ':btoConsultarCliente').disabled = true;
		return false;
	}
}

function validarCampoDescontoTarifa(object) {
	var percentual = object.value.replace(",",".");

	if(percentual > 100.00){
		//object.value = object.value.substring(0, object.value.length-1) 
		object.focus();
		alert("Desconto permitido na faixa de 0,00 a 100,00");
	}
}