function confirmar(msgConfirma){
	confirm(msgConfirma);	
}

function validaCamposConsultar(form, msgCampo, msgNecessario, msgData){ 

	var msgValidacao = '';

	
	if ( document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.day').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.day').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.month').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.month').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.year').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.year').value == '' ) {
			alert(msgCampo + ' ' + msgData + ' ' + msgNecessario);
			return false;		
	}
	
	if ( document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.day').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.day').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.month').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.month').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.year').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.year').value == '' ) {
			alert(msgCampo + ' ' + msgData + ' ' + msgNecessario);
			return false;		
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;						
}


function validaCamposIncluir(form, msgCampo, msgNecessario, msgData, msgCompromisso, msgDestino, msgTipoServico, msgModalidade, msgContaDebito, 
msgBanco, msgAgencia, msgConta, msgDigitoConta, msgTipoConta, msgRdoFiltroFavorecido, msgFavorecido, msgCodigo, msgInscricao, msgTipo, msgMnemonico){

	var msgValidacao = '';
	var objChkCompromissosPagos = document.getElementById(form.id + ':chkCompromissosPagos').checked;
	var objChkCompromissosNaoPagos = document.getElementById(form.id + ':chkCompromissosNaoPagos').checked;
	var objChkCompromissosAgendados = document.getElementById(form.id + ':chkCompromissosAgendados').checked;
	var objRdoDestino = document.getElementsByName('radioDestinoFiltro');
	var objChkTipoServico = document.getElementById(form.id + ':chkTipoServico').checked;
	var objChkContaDebito = document.getElementById(form.id + ':chkContaDebito').checked;
	var objChkFavorecido = document.getElementById(form.id + ':chkFavorecido').checked;

	
	if (( document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.day').value == null ||
		 document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.day').value == ''   ||
		 document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.month').value == null ||
		 document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.month').value == ''   ||
		 document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.year').value == null ||
		 document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoInicio.year').value == '' )|| 
		 ( document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.day').value == null ||
		 document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.day').value == ''   ||
		 document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.month').value == null ||
		 document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.month').value == ''   ||
		 document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.year').value == null ||
		 document.getElementById('incManterSolicGeracaoArqRetBasePagtosForm:txtPeriodoFim.year').value == '' )) 
	{
		msgValidacao = msgValidacao + msgCampo + ' ' + msgData + ' ' + msgNecessario + '!\n';		
				
	}
	
	if ( !objChkCompromissosPagos && !objChkCompromissosNaoPagos && !objChkCompromissosAgendados){		  
		msgValidacao = msgValidacao + msgCampo + ' ' + msgCompromisso + ' ' + msgNecessario + '!' + '\n';
	}
	
	if ( !objRdoDestino[1].checked && !objRdoDestino[2].checked ){
		msgValidacao = msgValidacao + msgCampo + ' ' + msgDestino + ' ' + msgNecessario + '!' + '\n';
	}	
	
	
    if (document.getElementById(form.id + ':chkTipoServico').checked){
		if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
			msgValidacao = msgValidacao + msgCampo + ' ' + msgTipoServico + ' ' + msgNecessario + '!' + '\n';				
	}	
	
	
	//Verifica preenchimento de "Conta de D�bito" 
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDeb')) == '')
			msgValidacao = msgValidacao + msgCampo + ' ' + msgBanco + ' (' + msgContaDebito + ') ' + msgNecessario + '!' + '\n';		

		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDeb')) == '')
			msgValidacao = msgValidacao + msgCampo + ' ' + msgAgencia + ' (' + msgContaDebito + ') ' + msgNecessario + '!' + '\n';		

		if(allTrim(document.getElementById(form.id +':txtContaContaDeb')) == '')
			msgValidacao = msgValidacao + msgCampo + ' ' + msgConta + ' (' + msgContaDebito + ') ' + msgNecessario + '!' + '\n';
			
		/*if(allTrim(document.getElementById(form.id +':txtdigitoContaDeb')) == '')
			msgValidacao = msgValidacao + msgCampo + ' ' + msgDigitoConta + ' (' + msgContaDebito + ') ' + msgNecessario + '!' + '\n';					
			
		if(document.getElementById(form.id +':cmbTipoConta').value == null || document.getElementById(form.id +':cmbTipoConta').value == '0')
			msgValidacao = msgValidacao + msgCampo + ' ' + msgTipoConta + ' (' + msgContaDebito + ') ' + msgNecessario + '!' + '\n';	*/							
	}
	//Favorecido
	
	var objRadioFiltroFavorecido = document.getElementsByName('rdoFiltroFavorecido');
	
	if (document.getElementById(form.id + ':chkFavorecido').checked){
	
	    if (!objRadioFiltroFavorecido[0].checked && !objRadioFiltroFavorecido[1].checked &&	!objRadioFiltroFavorecido[2].checked ){		
			msgValidacao = msgValidacao + msgRdoFiltroFavorecido + ' (' + msgFavorecido + ') ' + '!' + '\n';	}
			
			if (objRadioFiltroFavorecido[0].checked){
					if(allTrim(document.getElementById(form.id +':txtCodigo')) == '')
						msgValidacao = msgValidacao + msgCampo + ' ' + msgCodigo + ' (' + msgFavorecido + ') ' + msgNecessario + '!' + '\n';				
			}
			else if(objRadioFiltroFavorecido[1].checked){
					if(allTrim(document.getElementById(form.id +':txtInscricao')) == '' || parseInt(document.getElementById(form.id +':txtInscricao').value,10) == 0)
						msgValidacao = msgValidacao + msgCampo + ' ' + msgInscricao + ' (' + msgFavorecido + ') ' + msgNecessario + '!' + '\n';	
						
					if (document.getElementById(form.id +':cboTipo').value == null || document.getElementById(form.id +':cboTipo').value == '0' )							
						msgValidacao = msgValidacao + msgCampo + ' ' + msgTipo + ' (' + msgFavorecido +') ' + msgNecessario + '!' + '\n';				
			}					
			else if(objRadioFiltroFavorecido[2].checked){
					if(allTrim(document.getElementById(form.id +':txtMnemonico')) == '')
							msgValidacao = msgValidacao + msgCampo + ' ' + msgMnemonico + ' (' + msgFavorecido + ') ' +  msgNecessario + '!' + '\n';				
			}
	}
	
	

	if ( msgValidacao != '' ){
		alert(msgValidacao);
		return false;
	}
    else{
	    return true;
    }
	
}

function validaConsultarContaDebito(form, msgcampo, msgnecessario,msgbanco,msgagencia,msgconta,msgcontadig, msgcontadebito){
	var msgValidacao = '';
	
	if(allTrim(document.getElementById(form.id +':txtBancoContaDeb')) == ''){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';	
	}			
	if(allTrim(document.getElementById(form.id +':txtAgenciaContaDeb')) == ''){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';			
	}
	if(allTrim(document.getElementById(form.id +':txtContaContaDeb')) == ''){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
	}
	if (allTrim(document.getElementById(form.id + ':txtdigitoContaDeb')) == '') {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgcontadig + ' (' + msgcontadebito + ') ' + msgnecessario + '!\n';			
	}	
		
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}

function validarCampoDescontoTarifa(object) {
	var percentual = object.value.replace(",",".");

	if(percentual > 100.00){
		//object.value = object.value.substring(0, object.value.length-1) 
		object.focus();
		alert("Desconto permitido na faixa de 0,00 a 100,00");
	}
}