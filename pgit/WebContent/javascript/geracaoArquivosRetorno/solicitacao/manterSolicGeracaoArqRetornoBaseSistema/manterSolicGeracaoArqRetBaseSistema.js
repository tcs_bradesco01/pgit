function validaCamposConsultar(form, msgcampo, msgnecessario, msgdata){


	var msgValidacao = '';

	
	if ( document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoInicio.day').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoInicio.day').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoInicio.month').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoInicio.month').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoInicio.year').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoInicio.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if ( document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoFim.day').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoFim.day').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoFim.month').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoFim.month').value == ''   ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoFim.year').value == null ||
		 document.getElementById('conManterSolicGeracaoArqRetBaseSistemaForm:txtPeriodoFim.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	

	return true;						
}


function validaCampoInclusao(form, msgcampo, msgnecessario, msg){

var msgValidacao = '';

	
	if ( document.getElementById(form.id + ':cboTipoSistema').value == 0 ) {
			alert(msgcampo + ' ' + msg + ' ' + msgnecessario + '!');
			return false;		
	}
	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	

	return true;						
}

function validarCampoDescontoTarifa(object) {
	var percentual = object.value.replace(",",".");

	if(percentual > 100.00){
		//object.value = object.value.substring(0, object.value.length-1) 
		object.focus();
		alert("Desconto permitido na faixa de 0,00 a 100,00");
	}
}