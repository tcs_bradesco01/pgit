function validaSituacaoAlterar(form) {
	var combo = document.getElementById('altManterClientesOrgaoPublicoFederalForm:situacaoCliente');
	var situacao = document.getElementById('altManterClientesOrgaoPublicoFederalForm:descricaoSituacao');
	var itemSelecionado = combo[combo.selectedIndex];
	
	var itemSelecionadoCombo = itemSelecionado.innerHTML
	itemSelecionadoCombo = itemSelecionadoCombo.toLowerCase();

	var situacaoTela = situacao.innerText
	situacaoTela = situacaoTela.toLowerCase();
	
	if (itemSelecionado.value == "0") {
		alert('Campo Situa\u00e7\u00e3o Obrigat\u00f3rio');
		return false;
	}

	if (trim(itemSelecionadoCombo) == trim(situacaoTela)) {
		alert('Registro J\u00e1 se encontra nesta situa\u00e7\u00e3o');
		return false;
	}

	return confirm('Confirma Altera\u00e7\u00e3o?');
}