function validaCampoParticipante(form, msgcampo, msgnecessario, msgCpfCnpj, msgBase, msgFilial, msgControle, msgDiferenteZeros){

	var msgValidacao = '';
	
	
	if ( allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) == '') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgControle+ ')' + ' ' +  msgnecessario + '!';
	}else{
		if ( allTrim(document.getElementById(form.id +':txtFilialCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) == '') {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' ' +  msgnecessario + '!';
		}else{
		
			if ( allTrim(document.getElementById(form.id +':txtFilialCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) == '' &&  allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) == ''  ) {
					msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' '  + msgnecessario + '!\n';
					msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgControle+ ')' + ' '  + msgnecessario + '!';
			}else{
			
				if ( allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) == '' && allTrim(document.getElementById(form.id +':txtFilialCpfCnpjParticipanteFiltro')) == '' ) {
						msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' ' + msgnecessario + '!';						
				}else{
				
					if ( parseInt(allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')),10) == 0 ) {
							msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' ' + msgDiferenteZeros + '!';						
					}
				}
				
			}
		}
	}

	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaCamposConsultaSolicitacao(form, msgcampo, msgnecessario, msgagendados, msgou, msgpagosnaopagos, msgdatapagamento, msgargumentopesquisa,
		msgparticipante, msgcontadebito, msgbanco, msgagencia, msgconta, msgcontadig, msgtiposervico, msgmodalidade, msgsituacaopagamento,
		msgmotivosituacao, msgnumeropagamento, msgvalorpagamento, msgremessa, msgnumero, msglinhadigitavel, msgradiofavorecido, msgcodigo, msgfavorecido,
		msginscricao, msgtipo, msgcontacredito, msgdigitoConta,msgtipoconta, msgradiofavorecidobeneficiario, msgBeneficiario,msgDiferenteZeros){


	var msgValidacao = '';
	
	var objRdoFavorecidoBeneficiario = document.getElementsByName('radioPesquisasFiltroPrincipal');
	
	//valida data inicial pagamento
	if (allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
		
	//Valida Tipo Servi�o e Modalidade
	if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' ){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';
	}
	if(document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' ){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
	}
	
	//N�mero da Remessa
	if (document.getElementById(form.id + ':chkRemessa').checked){
		if (allTrim(document.getElementById(form.id +':numeroRemessaFiltro')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgnecessario + '!' + '\n';	
		else{
			if (parseInt(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgDiferenteZeros + '!' + '\n';	
		}					
	}
	
	//Verifica preenchimento de "Participante do Contrato"	
	if (document.getElementById(form.id + ':chkParticipanteContrato').checked){
		if(allTrim(document.getElementById(form.id +':txtCodigoParticipante')) == ''){
			msgValidacao = msgValidacao + msgparticipante + '!' + '\n';		
		}
	}
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';		
		}
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';		
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';		
		}		
		/*
		if(allTrim(document.getElementById(form.id +':txtDigitoContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoConta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		*/
		/*if ( document.getElementById(form.id +':cmbTipoContaDebito').value == null || document.getElementById(form.id +':cmbTipoContaDebito').value == '0' ){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipoconta + ' (' + msgcontadebito + ') ' +  msgnecessario + '!' + '\n';	
		}*/
	}	
	
	//Valida Numero Pagamento
	if (document.getElementById(form.id + ':chkNumeroPagamento').checked){
		if(
			(allTrim(document.getElementById(form.id +':txtNumeroPagamentoDe')) == '') ||
			(allTrim(document.getElementById(form.id +':txtNumeroPagamentoAte')) == '')
			){
		    msgValidacao = msgValidacao + msgcampo + ' '  + msgnumeropagamento + ' ' +  msgnecessario + '!' + '\n';			
	    } 
	}
	
	//Valida Valor Pagamento
	if (document.getElementById(form.id + ':chkValorPagamento').checked){
		if(
			(allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') ||
			(allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == '')
		  ){
			msgValidacao = msgValidacao + msgcampo +  ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';				
	     }
	     else{
	 		if(
				(replaceAll(document.getElementById(form.id +':txtValorDePagamento').value) == 0) ||
				(replaceAll(document.getElementById(form.id +':txtValorAtePagamento').value) == 0)
			  ){
				msgValidacao = msgValidacao + msgcampo +  ' ' + msgvalorpagamento + ' ' + msgDiferenteZeros + '!' + '\n';				
		     }
	     }
	}
	
	//Valida Situa��o e Motivo da Situa��o do Pagamento
	if (document.getElementById(form.id + ':chkSituacaoMotivo').checked){
		if ( document.getElementById(form.id +':cboSituacaoPagamento').value == null || document.getElementById(form.id +':cboSituacaoPagamento').value == '0' ){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsituacaopagamento + ' ' + msgnecessario + '!' + '\n';	
		}
	}	
	
	//Valida Linha Digitavel
	if (document.getElementById(form.id + ':chkLinhasDigitaveis').checked){
	
		if ((allTrim(document.getElementById(form.id +':linhaDigitavel1')) == '') ||
						(allTrim(document.getElementById(form.id +':linhaDigitavel2')) == '') ||
						(allTrim(document.getElementById(form.id +':linhaDigitavel3')) == '') ||
						(allTrim(document.getElementById(form.id +':linhaDigitavel4')) == '') ||
						(allTrim(document.getElementById(form.id +':linhaDigitavel5')) == '') ||
						(allTrim(document.getElementById(form.id +':linhaDigitavel6')) == '') ||
						(allTrim(document.getElementById(form.id +':linhaDigitavel7')) == '') ||
						(allTrim(document.getElementById(form.id +':linhaDigitavel8')) == '')	){	 
							msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgnecessario + '!' + '\n';	
		}
		else{
			if( parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel1')),10) == 0 &&
				parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel2')),10) == 0 &&
				parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel3')),10) == 0 &&
				parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel4')),10) == 0 &&
				parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel5')),10) == 0 &&
				parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel6')),10) == 0 &&
				parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel7')),10) == 0 &&
				parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel8')),10) == 0 )
					msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgDiferenteZeros + '!' + '\n';
		}		
	}	
	
	//Valida Favorecido/Beneficiario
	if (document.getElementById(form.id + ':chkIdentificacaoRecebedor').checked){
		if (!objRdoFavorecidoBeneficiario[0].checked && !objRdoFavorecidoBeneficiario[1].checked){
			msgValidacao = msgValidacao + msgradiofavorecidobeneficiario + '!' + '\n';
		}
		
		if (objRdoFavorecidoBeneficiario[0].checked){
			var objRadioFavorecido = document.getElementsByName('radioFavorecidoFiltro');				
							
			if (!objRadioFavorecido[0].checked && !objRadioFavorecido[1].checked ){
				msgValidacao = msgValidacao + msgradiofavorecido + '!' + '\n';					
			}
						
			if (objRadioFavorecido[0].checked){
				if(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')) == '')
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';				
				else{
					if (parseInt(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')),10) == 0){
						msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!' + '\n';				
					}				
				}	
			}
				else if(objRadioFavorecido[1].checked){
					if(allTrim(document.getElementById(form.id +':txtInscricao')) == ''){
						msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';	
					}
					else{
						if(parseInt(document.getElementById(form.id +':txtInscricao').value,10) == 0){
							msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!' + '\n';	
						}					
					}
								
					if (document.getElementById(form.id +':tipoInscricao').value == null || allTrim(document.getElementById(form.id +':tipoInscricao')) == '0' )							
								msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' (' + msgfavorecido +') ' + msgnecessario + '!' + '\n';				
					}					
						
		}
		
		if (objRdoFavorecidoBeneficiario[1].checked){
				if(allTrim(document.getElementById(form.id +':txtInscricaoBeneficiario')) == '')
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgBeneficiario + ') ' + msgnecessario + '!' + '\n';				
			
				if ( document.getElementById(form.id +':tipoInscricaoBeneficiario').value == null || document.getElementById(form.id +':tipoInscricaoBeneficiario').value == '0' )
					msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' (' + msgBeneficiario +')  ' + msgnecessario + '!' + '\n';
		}		
	}	
	
	//Valida Conta Credito
	if (document.getElementById(form.id + ':chkBancoCredito').checked){
		if (allTrim(document.getElementById(form.id + ':txtBancoContaCredito')) == '') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';	
		}else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';	
		}
		if (allTrim(document.getElementById(form.id + ':txtAgenciaContaCredito')) == '') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';	
		}else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';	
		}
		if (allTrim(document.getElementById(form.id + ':txtContaContaCredito')) == '') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';	
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaContaCredito')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';	
		}
		
	}
	
	//Valida Data Programada Recupera��o
	/*
	if (allTrim(document.getElementById(form.id + ':dataRecuperacao1.day')) == ''   ||
		allTrim(document.getElementById(form.id + ':dataRecuperacao1.month')) == ''   ||
		allTrim(document.getElementById(form.id + ':dataRecuperacao1.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}*/
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;	
	
}	