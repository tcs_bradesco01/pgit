function validarConsultar(form, msgcampo, msgnecessario,msgdatasolicitacao,msgsituacaosolicitacao,msgmotivo,msgtiposervico, msgmodalidade  ){

	var msgValidacao = '';
 	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.day').value == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatasolicitacao + ' ' + msgnecessario + '!' + '\n';	
	}	
	
	if(document.getElementById(form.id + ':chkSituacaoSolicitacao').checked){
		if ( document.getElementById(form.id +':cboSituacaoSolicitacao').value == null || document.getElementById(form.id +':cboSituacaoSolicitacao').value == '0' ){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsituacaosolicitacao + ' ' + msgnecessario + '!' + '\n';	
		}
	}
	
	if(document.getElementById(form.id + ':chkServicoModalidade').checked){
		if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' ){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
		}
		
		if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' ){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
		}	
	}	
						
	if (msgValidacao != ''){
			alert(msgValidacao);
			return false;
	}
	
	return true;		

}