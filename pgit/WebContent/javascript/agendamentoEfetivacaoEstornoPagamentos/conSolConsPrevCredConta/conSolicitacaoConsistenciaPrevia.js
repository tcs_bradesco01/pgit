function validaCamposConsulta(form, msgcampo, msgnecessario, msgPeriodoSol ){
	var msgValidacao = '';
	
	
	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.day').value == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgPeriodoSol + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}	