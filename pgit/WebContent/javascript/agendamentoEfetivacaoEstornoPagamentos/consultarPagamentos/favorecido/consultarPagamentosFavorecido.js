function validaCamposConsulta(form, msgcampo, msgnecessario,msgradio,msgdatapagamento,msgtiposervico,msgmodalidade,msgcodfavorecido,msginscfavorecido,msgtipofavorecido,msgselecione,msgbanco,msgagencia,msgconta,msgcontadig,msgcodbeneficiario,msgtipobenefeciario,msgorgao,msgfavorecido,msgbenefeciario,
msgargumentopesquisa, msgcontacredito, msgou,msgtipoconta, msgDiferenteZeros, msgContaSalario){

	var msgValidacao = '';
	
	var objRdo = document.getElementsByName('radioAgendadosPagosNaoPagos');
	var objRdoFavorecido = document.getElementsByName('sorRadioFavorecido');
	var objRdoSor = document.getElementsByName('sorRadio');
	var objRdoBeneficiario = document.getElementsByName('sorRadioBeneficiario');
		
	if ( !objRdo[1].checked && !objRdo[2].checked )
		msgValidacao = msgValidacao + msgcampo + ' ' + msgradio + ' ' + msgnecessario + '!\n';		
				
	if (document.getElementById(form.id + ':dataPagamentoInicial.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataPagamentoInicial.day')) == ''   ||
		document.getElementById(form.id + ':dataPagamentoInicial.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataPagamentoInicial.month')) == ''   ||
		document.getElementById(form.id + ':dataPagamentoInicial.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataPagamentoInicial.year')) == '' ||
		document.getElementById(form.id + ':dataPagamentoFinal.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataPagamentoFinal.day')) == ''   ||
		document.getElementById(form.id + ':dataPagamentoFinal.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataPagamentoFinal.month')) == ''   ||
		document.getElementById(form.id + ':dataPagamentoFinal.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataPagamentoFinal.year')) == '') {
	
		msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if(document.getElementById(form.id + ':chkTipoServico').checked){			
		var tipoServicoInvalido = false;
		if (document.getElementById(form.id +':cmbTipoServico').value == null || document.getElementById(form.id +':cmbTipoServico').value == 0) {
			tipoServicoInvalido = true;
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!\n';			
		} 
		//if (document.getElementById(form.id +':cmbModalidade').value == null || document.getElementById(form.id +':cmbModalidade').value == 0) {
		//	tipoServicoInvalido = true;		
		//	msgValidacao = msgValidacao +  msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!\n';					
		//}
		
		if (tipoServicoInvalido){
			alert(msgValidacao);
			return false;
		}		
	}
	
		//Ao menos um argumento de pesquisa deve ser selecionado	
	if(	!objRdoSor[0].checked && !objRdoSor[1].checked &&
		!objRdoSor[2].checked && !objRdoSor[3].checked 
		&& !objRdoSor[4].checked){		
			msgValidacao = msgValidacao + msgargumentopesquisa + '!\n' + 
				'           (' + msgfavorecido + ', ' +  msgcontacredito + ', ' + msgContaSalario + ' ' + msgou + ' ' + msgbenefeciario + ')';	
			alert(msgValidacao);
			return false;
	}
	
	if (objRdoSor[0].checked) {
		if (!objRdoFavorecido[0].checked && !objRdoFavorecido[1].checked) {
			msgValidacao = msgValidacao + msgselecione + ' ' + msgfavorecido +'!\n';			
		}else if (objRdoFavorecido[0].checked) {
			if (allTrim(document.getElementById(form.id + ':codigoFavorecido')) == "" ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgcodfavorecido + ' (' + msgfavorecido + ') ' + msgnecessario + '!\n';			
			}else{
				if (parseInt(allTrim(document.getElementById(form.id + ':codigoFavorecido')),10) == 0)
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcodfavorecido + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!\n';		
			}
		}else if (objRdoFavorecido[1].checked){
			if (allTrim(document.getElementById(form.id + ':inscricaoFavorecido')) == ""  || parseInt(document.getElementById(form.id +':inscricaoFavorecido').value,10) == 0) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msginscfavorecido +  ' (' + msgfavorecido + ') ' + msgnecessario + '!\n';			
			}
			if(document.getElementById(form.id +':tipoFavorecido').value == null || document.getElementById(form.id +':tipoFavorecido').value == 0){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgtipofavorecido + ' (' + msgfavorecido + ') ' + msgnecessario + '!\n';			
			}
		}		
	}
	if (objRdoSor[1].checked) {
		if (allTrim(document.getElementById(form.id + ':banco')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' + msgnecessario + '!' + '\n';	
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':banco')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' + msgDiferenteZeros + '!' + '\n';
		}			
		if (allTrim(document.getElementById(form.id + ':agencia')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' + msgnecessario + '!' + '\n';			
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':agencia')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' + msgDiferenteZeros + '!' + '\n';
		}	
		if (allTrim(document.getElementById(form.id + ':conta')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' + msgnecessario + '!' + '\n';			
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':conta')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' + msgDiferenteZeros + '!' + '\n';
		}	
		
	}
	if (objRdoSor[2].checked) {
		if (allTrim(document.getElementById(form.id + ':bancoSalario')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgContaSalario + ') ' + msgnecessario + '!' + '\n';	
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':bancoSalario')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgContaSalario + ') ' + msgDiferenteZeros + '!' + '\n';
		}			
		if (allTrim(document.getElementById(form.id + ':agenciaSalario')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgContaSalario + ') ' + msgnecessario + '!' + '\n';			
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':agenciaSalario')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgContaSalario + ') ' + msgDiferenteZeros + '!' + '\n';
		}	
		if (allTrim(document.getElementById(form.id + ':contaSalario')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgContaSalario + ') ' + msgnecessario + '!' + '\n';			
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':contaSalario')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgContaSalario + ') ' + msgDiferenteZeros + '!' + '\n';
		}	
		
	}
	if (objRdoSor[3].checked) {

		var bancoPagamento = allTrim(document.getElementById(form.id +':txtBancoPagamento'));
		var ispb = allTrim(document.getElementById(form.id +':txtISPB'));
		var contaPagamento = allTrim(document.getElementById(form.id +':txtContaPagamento'));
		
		
		if(bancoPagamento == '' && ispb == ''){
			msgValidacao = msgValidacao + msgcampo + ' Banco Pagamento ou ISPB ' +  msgnecessario + '!' + '\n';				
		
		}else if (bancoPagamento == 0 && ispb == ''){
			msgValidacao = msgValidacao + msgcampo + ' Banco Pagamento ' +  msgDiferenteZeros + '!' + '\n';
		
	//	}else if (bancoPagamento == '' && ispb == 0){
	//		msgValidacao = msgValidacao + msgcampo + ' ISPB ' +  msgDiferenteZeros + '!' + '\n';
		
	//	}else if (bancoPagamento == 0 && ispb == 0){
	//		msgValidacao = msgValidacao + msgcampo + ' Banco Pagamento ou ISPB ' +  msgnecessario + '!' + '\n';
		
	//	}else if (parseInt(bancoPagamento) > 0 && parseInt(ispb) > 0){
	//		msgValidacao = msgValidacao + 'Somente o campo Banco Pagamento ou o campo ISPB deve ser preenchido!' ;
		}
		

		if(contaPagamento == ''){
			msgValidacao = msgValidacao + msgcampo + ' Conta Pagamento ' +  msgnecessario + '!' + '\n';				
		}else{
			if (parseInt(contaPagamento,20) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' Conta Pagamento ' +  msgDiferenteZeros + '!' + '\n';				
		}
	}
	
	if(objRdoSor[4].checked){
		if (!objRdoBeneficiario[0].checked && !objRdoBeneficiario[1].checked) {
			msgValidacao = msgValidacao + msgselecione + ' ' + msgbenefeciario + '!\n';			
		}else if (objRdoBeneficiario[0].checked) {
			if (allTrim(document.getElementById(form.id + ':codigoBeneficiario'))== "" ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgcodbeneficiario + ' ('+ msgbenefeciario + ') ' + msgnecessario + '!\n';			
			}else{
				if ( parseInt(allTrim(document.getElementById(form.id + ':codigoBeneficiario')),10) == 0  )
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcodbeneficiario + ' ('+ msgbenefeciario + ') ' + msgDiferenteZeros + '!\n';						
			}
			if(document.getElementById(form.id +':tipoBeneficiario').value == null || document.getElementById(form.id +':tipoBeneficiario').value == 0){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgtipobenefeciario + ' (' + msgbenefeciario + ') ' + msgnecessario + '!\n';			
			}
		}else if (objRdoBeneficiario[1].checked){
			if(document.getElementById(form.id +':cmbOrgaoPagador').value == null || document.getElementById(form.id +':cmbOrgaoPagador').value == 0){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgorgao + ' (' +  msgbenefeciario + ') ' + msgnecessario + '!\n';			
			}
		}	
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;	
}

function validaConsultarContaCredito(form, msgcampo, msgnecessario,msgbanco,msgagencia,msgconta,msgcontadig,msgcontacredito){
	var msgValidacao = '';
	
	if (allTrim(document.getElementById(form.id + ':banco')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' + msgnecessario + '!' + '\n';
	}else{
		if (parseInt(allTrim(document.getElementById(form.id +':banco')),10) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' + msgDiferenteZeros + '!' + '\n';
	}				
	if (allTrim(document.getElementById(form.id + ':agencia')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' + msgnecessario + '!' + '\n';
	}else{
		if (parseInt(allTrim(document.getElementById(form.id +':agencia')),10) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' + msgDiferenteZeros + '!' + '\n';
	}
	if (allTrim(document.getElementById(form.id + ':conta')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' + msgnecessario + '!' + '\n';
	}else{
		if (parseInt(allTrim(document.getElementById(form.id +':conta')),10) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' + msgDiferenteZeros + '!' + '\n';
	}
	if (allTrim(document.getElementById(form.id + ':contaDig')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgcontadig + ' (' + msgcontacredito + ') ' + msgnecessario + '!' + '\n';			
	}
		
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}