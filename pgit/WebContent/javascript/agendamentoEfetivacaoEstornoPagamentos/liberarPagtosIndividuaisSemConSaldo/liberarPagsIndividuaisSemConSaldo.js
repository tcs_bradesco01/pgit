function selecionarTodos(form, check){
	var checkCont = 0;
	
	for(i=0; i<form.elements.length; i++) {
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
			checkCont++;
		}	
		
	}	
	
	document.getElementById(form.id + ':btnLimparSelecionados').disabled = !check.checked;	
	document.getElementById(form.id + ':btnLiberarSelecionados').disabled = !check.checked;	
	
	if(checkCont == 1){
		document.getElementById(form.id + ':btnDetalhar').disabled = !check.checked;	
	}else{
		document.getElementById(form.id + ':btnDetalhar').disabled = true;	
	}
}

function desabilitarCheckPaginacao(){
		
		if (document.getElementById("liberarPagsIndividuaisSemConSaldoForm:dataTable").rows.length == 1) {
			
			return;
		}
	
		var count = 0;
		
		for ( var i = 0; i < 10; i++) {
			
			while (i) {		
				
				var check = document.getElementById('liberarPagsIndividuaisSemConSaldoForm:dataTable_' + (i + count) + ':sorLista');
				if (check != null || check != undefined) {
					break;
				} else {
					count += 10;
				}
			}
			check.checked = false;
		}		
	
	
}