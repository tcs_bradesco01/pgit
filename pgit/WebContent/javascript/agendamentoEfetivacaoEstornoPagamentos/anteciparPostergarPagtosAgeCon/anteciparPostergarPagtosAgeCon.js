function validaData(form, msgcampo, msgnecessario, novaDataAgendamento1, msgTipoAgendamento){

	var campos, hoje, dataAgendamento;
	campos = '';
	
	var radio = document.getElementsByName(form.id + ':rdoTipoAgendamento');

	if(!radio[1].checked && !radio[2].checked){
		campos = msgTipoAgendamento + '!\n';
	}
	else{
		// SE O RADIO SELECIONADO FOR ANTECIPAR-POSTERGAR PAGAMENTO ENT�O A DATA � LIBERADA PARA PREENCHIMENTO
		if(radio[2].checked){
		    if (allTrim(document.getElementById(form.id + ':novaDataAgendamentoHab.day')) == '' ||
			    allTrim(document.getElementById(form.id + ':novaDataAgendamentoHab.month')) == '' ||
			    allTrim(document.getElementById(form.id + ':novaDataAgendamentoHab.year')) == ''){
				campos = campos + msgcampo + ' ' + novaDataAgendamento1 + ' ' + msgnecessario + '!' + '\n';
		    }	
		}	
	}
	
	if (campos != ''){
		alert(campos);
		return false;
	}

	return true;
}


function selecionarTodos(form, check){
	for(i=0; i<form.elements.length; i++) {
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
		
	}	
	
	document.getElementById(form.id + ':btnLimparSelecao').disabled = !check.checked;
	document.getElementById(form.id + ':btnAlterarSelecionados').disabled = !check.checked;
}