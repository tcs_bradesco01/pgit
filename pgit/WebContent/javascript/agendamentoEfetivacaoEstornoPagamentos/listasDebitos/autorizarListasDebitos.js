function validaPagamentoIndividualAutorizar(form, msgcampo, msgnecessario, msgdatapagamento, msgargumentopesquisa,msgparticipante , msgcontadebito, msgbanco, msgagencia,
msgconta, msgdigitoConta, msgtiposervico, msgmodalidade, msgnumerode, msgnumeroate, msgnumerolistadebito,msgDiferenteZeros){
	var msgValidacao = '';
	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.day').value == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	//Verifica preenchimento de "Participante do Contrato"	
	if (document.getElementById(form.id + ':chkParticipanteContrato').checked){
		if(allTrim(document.getElementById(form.id +':txtCodigoParticipante')) == ''){
			msgValidacao = msgValidacao + msgparticipante + '!' + '\n';		
		}
	}
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';		
		}
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';		
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';		
		}			
		/*if(allTrim(document.getElementById(form.id +':txtDigitoContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoConta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';*/					
	}
	
	//Verifica preenchimento de "Tipo Servico e Modalidade" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkServicoModalidade').checked){
		if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
			
		//if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )		
		//	msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
	}	
	
	//Verifica preenchimento de "Numero Lista de Debito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkNumeroListaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtNumeroListaDebitoDe')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumerode + ' (' + msgnumerolistadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{	
			if(allTrim(document.getElementById(form.id +':txtNumeroListaDebitoDe')) == 0)
				msgValidacao = msgValidacao + msgcampo + ' ' + msgnumerode + ' (' + msgnumerolistadebito + ') ' + msgDiferenteZeros + '!' + '\n';		
		}

		/*if(allTrim(document.getElementById(form.id +':txtNumeroListaDebitoAte')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeroate + ' (' + msgnumerolistadebito + ') ' + msgnecessario + '!' + '\n';*/
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function selecionarTodos(form, check){

	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
		
	}	
	
	document.getElementById(form.id + ':btnLimparSelecionados').disabled = !check.checked;		
	document.getElementById(form.id + ':btnAutorizarSelecionados').disabled = !check.checked;	
}

function atualizaRegistroSelecionado(form){

	var existeRegistroSelecionado = false;
	for(i=0; i<form.elements.length && !existeRegistroSelecionado; i++)
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			if (form.elements[i].checked){
				existeRegistroSelecionado = true;
			}

		}	
		
	}	

	document.getElementById(form.id + ':btnLimparSelecionados').disabled = !existeRegistroSelecionado;	
	document.getElementById(form.id + ':btnAutorizarSelecionados').disabled = !existeRegistroSelecionado;	
}

function limparSelecionados(form){

	for(i=0; i<form.elements.length ; i++)
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = false;
		}	
		
	}	
	document.getElementById(form.id + ':btnLimparSelecionados').disabled = true;	
	document.getElementById(form.id + ':btnAutorizarSelecionados').disabled = true;	
	document.getElementById(form.id + ':dataTable:checkSelecionarTodos').checked = false;	
	return true;
	
}