function validaCampoParticipante(form, msgcampo, msgnecessario, msgCpfCnpj, msgBase, msgFilial, msgControle, msgDiferenteZeros){

	var msgValidacao = '';
	
	
	if ( allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) == '') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgControle+ ')' + ' ' +  msgnecessario + '!';
	}else{
		if ( allTrim(document.getElementById(form.id +':txtFilialCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) == '') {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' ' +  msgnecessario + '!';
		}else{
		
			if ( allTrim(document.getElementById(form.id +':txtFilialCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) == '' &&  allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) == ''  ) {
					msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' '  + msgnecessario + '!\n';
					msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgControle+ ')' + ' '  + msgnecessario + '!';
			}else{
			
				if ( allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) == '' && allTrim(document.getElementById(form.id +':txtFilialCpfCnpjParticipanteFiltro')) == '' ) {
						msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' ' + msgnecessario + '!';						
				}else{
				
					if ( parseInt(allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')),10) == 0 ) {
							msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' ' + msgDiferenteZeros + '!';						
					}
				}
				
			}
		}
	}

	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaConsultarListasDebitos(form, msgcampo, msgnecessario, msgdatapagamento, msgargumentopesquisa, msgparticipante, msgcontadebito, msgbanco, msgagencia,
msgconta, msgdigitoConta, msgtiposervico, msgmodalidade, msgnumerode, msgnumeroate, msgnumerolistadebito, msgsituacaolistadebito,msgDiferenteZeros){
	var msgValidacao = '';
	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.day').value == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	//Verifica preenchimento de "Numero Lista de Debito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkNumeroListaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtNumeroListaDebitoDe')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumerode + ' (' + msgnumerolistadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if(allTrim(document.getElementById(form.id +':txtNumeroListaDebitoDe')) == '0')
				msgValidacao = msgValidacao + msgcampo + ' ' + msgnumerode + ' (' + msgnumerolistadebito + ') ' + msgDiferenteZeros + '!' + '\n';		
	    }
	

		//retirado a pedido da kelli
		/*if(allTrim(document.getElementById(form.id +':txtNumeroListaDebitoAte')).value == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeroate + ' (' + msgnumerolistadebito + ') ' + msgnecessario + '!' + '\n';*/
	}
	
	//Verifica preenchimento de "Numero Lista de Debito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkSituacaoListaDebito').checked){
		if (document.getElementById(form.id +':cboSituacaoListaDebito').value == null || document.getElementById(form.id +':cboSituacaoListaDebito').value == '0' )	
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsituacaolistadebito + ' ' + msgnecessario + '!' + '\n';		
	}	
	
	
	//Verifica preenchimento de "Participante do Contrato"	
	if (document.getElementById(form.id + ':chkParticipanteContrato').checked){
		if(allTrim(document.getElementById(form.id +':txtCodigoParticipante')) == ''){
			msgValidacao = msgValidacao + msgparticipante + '!' + '\n';		
		}
	}
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';	
		}else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}
		/*if(allTrim(document.getElementById(form.id +':txtDigitoContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoConta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';	*/				
	}
	
	
	//Verifica preenchimento de "Tipo Servico e Modalidade" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkServicoModalidade').checked){
		if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
			
		//if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )		
		//	msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
	}	
	
	
	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}