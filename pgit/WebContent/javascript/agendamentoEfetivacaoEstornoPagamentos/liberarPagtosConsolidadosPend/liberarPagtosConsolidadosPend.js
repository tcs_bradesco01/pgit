function selecionarTodos(form, check){

	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
		
	}	
	
	document.getElementById(form.id + ':btnLiberarSelecionados').disabled = !check.checked;	
}

function validaLiberarPagamentosConsolidadosPendentes(form, msgcampo, msgnecessario, msgdatapagamento, msgRemessa, msgbanco, msgcontadebito, msgagencia, msgconta,
													  msgdigitoConta, msgtiposervico, msgmodalidade, msgmotivosituacao,msgDiferenteZeros){

	var msgValidacao = '';
	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	//Verifica preenchimento de "Remessa" - Argumentos de Pesquisa  
	if (document.getElementById(form.id + ':chkRemessa').checked){
		if(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgRemessa + ' ' + msgnecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgRemessa + ' ' + msgDiferenteZeros + '!' + '\n';
		}
	}	
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';		
		}
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == '') 
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 ) 
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';		
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';		
		}					
	}	
	
	//Verifica preenchimento de "Tipo de Servico e Modalidade" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkServicoModalidade').checked){
		if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
			
		//if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )		
		//	msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
	}
	
	//Verifica preenchimento de "Motivo" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkSituacaoMotivo').checked){
		if ( document.getElementById(form.id +':cboMotivoSituacao').value == null || document.getElementById(form.id +':cboMotivoSituacao').value == '0' )		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgmotivosituacao + ' ' + msgnecessario + '!' + '\n';	
	}			
		
	
		
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}
