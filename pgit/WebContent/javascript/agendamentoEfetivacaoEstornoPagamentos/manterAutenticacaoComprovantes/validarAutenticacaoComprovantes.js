

function validaCampos(form, msgcampo, msgnecessario,msgBanco,msgAgencia,msgConta,msgDigitoConta,msgValorPagamento ,msgDataPagamento,msgServico ,msgModalidade, msgDiferenteZeros, msgCodBarras, msgNumPagamento){

	var msgValidacao = '';

	if (allTrim(document.getElementById(form.id + ':txtBanco')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgBanco + ' ' + msgnecessario + '!\n';		
	}else{
		if (parseInt(allTrim(document.getElementById(form.id +':txtBanco')),10) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgBanco + ' ' + msgDiferenteZeros + '!\n';
	}
	if (allTrim(document.getElementById(form.id +':txtAgencia')) == "") {
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgAgencia + ' ' + msgnecessario + '!\n';					
	}else{
		if (parseInt(allTrim(document.getElementById(form.id +':txtAgencia')),10) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgAgencia + ' ' + msgDiferenteZeros + '!\n';
	}
	if (allTrim(document.getElementById(form.id + ':txtConta')) == ""){
		msgValidacao = msgValidacao +   msgcampo + ' ' + msgConta + ' ' + msgnecessario + '!\n';				
	}else{
		if (parseInt(allTrim(document.getElementById(form.id +':txtConta')),10) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgConta + ' ' + msgDiferenteZeros + '!\n';
	}	
	/*if (allTrim(document.getElementById(form.id + ':txtDigitoConta')) == ""){
		msgValidacao = msgValidacao +   msgcampo + ' ' + msgDigitoConta + ' ' + msgnecessario + '!\n';				
	}*/
	
	if (allTrim(document.getElementById(form.id + ':txtValor')) == ""){
		msgValidacao = msgValidacao +   msgcampo + ' ' + msgValorPagamento + ' ' + msgnecessario + '!\n';				
	}
	
	if (allTrim(document.getElementById(form.id + ':dataPagamento.day')) == ""){
		msgValidacao = msgValidacao +   msgcampo + ' ' + msgDataPagamento + ' ' + msgnecessario + '!\n';				
	}else{
		if (allTrim(document.getElementById(form.id + ':dataPagamento.month')) == ""){
			msgValidacao = msgValidacao +   msgcampo + ' ' + msgDataPagamento + ' ' + msgnecessario + '!\n';				
		}else{
			if (allTrim(document.getElementById(form.id + ':dataPagamento.year')) == ""){
				msgValidacao = msgValidacao +   msgcampo + ' ' + msgDataPagamento + ' ' + msgnecessario + '!\n';				
			}
		}
	}
	
	if (document.getElementById(form.id + ':tipoServico').value == 0){
		msgValidacao = msgValidacao +   msgcampo + ' ' + msgServico + ' ' + msgnecessario + '!\n';				
	}
	
	if (document.getElementById(form.id + ':modalidade').value == 0){
		msgValidacao = msgValidacao +   msgcampo + ' ' + msgModalidade + ' ' + msgnecessario + '!\n';				
	}
	
	if(document.getElementById(form.id + ':codBarras1') != undefined){
		if (allTrim(document.getElementById(form.id + ':codBarras1')).length < 12 || allTrim(document.getElementById(form.id + ':codBarras2')).length < 12 || allTrim(document.getElementById(form.id + ':codBarras3')).length < 12 || allTrim(document.getElementById(form.id + ':codBarras4')).length < 12){
			msgValidacao = msgValidacao +   msgcampo + ' ' + msgCodBarras + ' ' + msgnecessario + '!\n';				
		}
	}else if(document.getElementById(form.id + ':numPagamento') != undefined){
		if (allTrim(document.getElementById(form.id + ':numPagamento')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgNumPagamento + ' ' + msgnecessario + '!\n';		
		}
	}

	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}


function validaHora(tempo, msg){

  horario = tempo.value.split(":");
  
  if (horario == null) 
  var horas = horario[0];
  var minutos = horario[1];
  var segundos = horario[2];
  
  if(horas > 24){ 
  alert(msgHora); event.returnValue=false; tempo.focus() }
  if(minutos > 59){
  alert(msgMinuto); event.returnValue=false; tempo.focus()}
  if(segundos > 59){
  alert(msgSegundo); event.returnValue=false; tempo.focus()}
}

function validaHoraOnblur(form, msgHorarioInvalido ){

	var msgValidacao = '';
	
	var campo = document.getElementById(form.id + ':txtHoraPagamento');

	if(campo.value != ''){
		hora = (allTrim(campo)).split(":");
		if (hora.length != 3){
			msgValidacao = msgValidacao + msgHorarioInvalido + '!\n';
		}else{
			if(isNaN(hora[0]) || hora[0] > 23){
				msgValidacao = msgValidacao + msgHorarioInvalido + '!\n';
			}
			else if(isNaN(hora[1]) || hora[1] > 59){
				msgValidacao = msgValidacao + msgHorarioInvalido + '!\n';
			}
			else if(isNaN(hora[2]) || hora[2] > 59){
				msgValidacao = msgValidacao + msgHorarioInvalido + '!\n';
			}
		}
	}

	if (msgValidacao != ''){
		alert(msgValidacao);
		campo.value = '';
		campo.focus();
		return false;
	}

	return true;						
}