function confirmar(form, msgConfirma){
	
		var hiddenConfirm;
		
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id + ':hiddenConfirma'){	
				hiddenConfirm = form.elements[i];
				break;
			}
		}		
		hiddenConfirm.value=confirm(msgConfirma);
	
}


function selecionarTodos(form, check){

	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
		
	}	
	
	
	document.getElementById(form.id + ':btnAutorizarSelecionados').disabled = !check.checked;	
}


function atualizaRegistroSelecionado(form){

	var existeRegistroSelecionado = false;
	for(i=0; i<form.elements.length && !existeRegistroSelecionado; i++)
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			if (form.elements[i].checked){
				existeRegistroSelecionado = true;
			}

		}	
		
	}	

	document.getElementById(form.id + ':btnAutorizarSelecionados').disabled = !existeRegistroSelecionado;	
}




function limparSelecionados(form){

	for(i=0; i<form.elements.length ; i++)
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = false;
		}	
		
	}	
	document.getElementById(form.id + ':btnLimparSelecionados').disabled = true;	
	document.getElementById(form.id + ':btnAutorizarSelecionados').disabled = true;	
	document.getElementById(form.id + ':dataTable:checkSelecionarTodos').checked = false;	
	alert(document.getElementById(form.id + ':dataTable:checkSelecionarTodos').checked);
	return true;
	
}