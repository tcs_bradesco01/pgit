function validaCamposConsultar(form, msgcampo, msgnecessario, msgdata, msgTipoAgendamento){
	var msgValidacao = '';

	var radio = document.getElementsByName(form.id + ':rdoTipoAgendamento');

	if(!radio[1].checked && !radio[2].checked){
		msgValidacao = msgTipoAgendamento + '!\n';
	}
	else{
		// SE O RADIO SELECIONADO FOR ANTECIPAR-POSTERGAR PAGAMENTO ENT�O A DATA � LIBERADA PARA PREENCHIMENTO
		if(radio[2].checked){
		    if (allTrim(document.getElementById(form.id + ':txtPeriodoNovoAgendamentoHab.day')) == '' ||
			    allTrim(document.getElementById(form.id + ':txtPeriodoNovoAgendamentoHab.month')) == '' ||
			    allTrim(document.getElementById(form.id + ':txtPeriodoNovoAgendamentoHab.year')) == ''){
				msgValidacao = msgcampo + ' ' + msgdata + ' ' + msgnecessario + '!\n';
		    }	
		}	
	}
	
	if (msgValidacao != '')
	{
		alert(msgValidacao);
		return false;
	}
    else
    {
		return true;	
	}					
}

function selecionarTodos(form, check){

	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
		
	}	
	
	document.getElementById(form.id + ':btnDetalhar').disabled = true;
	document.getElementById(form.id + ':btnLimparSelecionados').disabled = !check.checked;	
	document.getElementById(form.id + ':btnAnteciparSelecionados').disabled = !check.checked;	
}