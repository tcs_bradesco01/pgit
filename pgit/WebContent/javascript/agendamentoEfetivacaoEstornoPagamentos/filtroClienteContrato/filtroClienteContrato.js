function validaCamposConsultaCliente(form, msgcampo, msgnecessario, msgcnpj,
		msgcpf, msgnome, msgbanco, msgagencia, msgconta, msgdigito,
		msgcnpjincorreto, msgcpfincorreto, msgDiferenteZeros) {

	var msgValidacao = '';

	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');

	if (objRdoCliente[0].checked) {

		if (allTrim(document.getElementById(form.id + ':txtCnpj')) == ""
				&& allTrim(document.getElementById(form.id + ':txtFilial')) == ""
				&& allTrim(document.getElementById(form.id + ':txtControle')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' '
					+ msgnecessario + '!\n';
		} else {
			if (allTrim(document.getElementById(form.id + ':txtCnpj')) == ""
					|| allTrim(document.getElementById(form.id + ':txtFilial')) == ""
					|| allTrim(document
							.getElementById(form.id + ':txtControle')) == "") {

				msgValidacao = msgValidacao + msgcnpjincorreto + '!\n';
			} else {
				if ((parseInt(allTrim(document
						.getElementById(form.id + ':txtCnpj')), 10) == 0)
						|| (parseInt(allTrim(document
								.getElementById(form.id + ':txtFilial')), 10) == 0)) {
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj
							+ ' ' + msgDiferenteZeros + '!' + '\n';
				}
			}
		}

	} else if (objRdoCliente[1].checked) {

		if (allTrim(document.getElementById(form.id + ':txtCpf')) == ""
				&& allTrim(document.getElementById(form.id + ':txtControleCpf')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcpf + ' '
					+ msgnecessario + '!\n';
		} else {
			if (allTrim(document.getElementById(form.id + ':txtCpf')) == ""
					|| allTrim(document
							.getElementById(form.id + ':txtControleCpf')) == "") {
				msgValidacao = msgValidacao + msgcpfincorreto + '!\n';
			} else {
				if (parseInt(allTrim(document
						.getElementById(form.id + ':txtCpf')), 10) == 0) {
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcpf + ' '
							+ msgDiferenteZeros + '!' + '\n';
				}
			}
		}

	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById(form.id + ':txtNomeRazaoSocial')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnome + ' '
					+ msgnecessario + '!\n';

		}
	} else if (objRdoCliente[3].checked) {
		if (allTrim(document.getElementById(form.id + ':txtBanco')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' '
					+ msgnecessario + '!\n';

		} else {
			if (parseInt(
					allTrim(document.getElementById(form.id + ':txtBanco')), 10) == 0) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' '
						+ msgDiferenteZeros + '!' + '\n';
			}
		}
		if (allTrim(document.getElementById(form.id + ':txtAgencia')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' '
					+ msgnecessario + '!\n';

		} else {
			if (parseInt(allTrim(document
					.getElementById(form.id + ':txtAgencia')), 10) == 0) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' '
						+ msgDiferenteZeros + '!' + '\n';
			}
		}
		if (allTrim(document.getElementById(form.id + ':txtConta')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' '
					+ msgnecessario + '!\n';

		} else {
			if (parseInt(
					allTrim(document.getElementById(form.id + ':txtConta')), 10) == 0) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' '
						+ msgDiferenteZeros + '!' + '\n';
			}
		}
		/*
		 * if(allTrim(document.getElementById(form.id +':txtdigito')) == "") {
		 * msgValidacao = msgValidacao + msgcampo + ' ' + msgdigito + ' ' +
		 * msgnecessario + '!\n';
		 *  }
		 */
	}

	if (msgValidacao != '') {
		alert(msgValidacao);
		return false;
	}

	return true;
}

function validaCamposConsultaContrato(form, msgcampo, msgnecessario, msgempresa, msgtipocontrato, msgnumero,msgDiferenteZeros){

	var msgValidacao = '';

	if ( document.getElementById(form.id +':empresaGestora').value == null || document.getElementById(form.id +':empresaGestora').value == '0' ) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '!\n';
	}
	
	if ( document.getElementById(form.id +':tipoContrato').value == null || document.getElementById(form.id +':tipoContrato').value == '0' ) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtipocontrato + ' ' + msgnecessario + '!\n';
	}
	
	if ( document.getElementById(form.id +':numero').value == null || allTrim(document.getElementById(form.id +':numero')) == "" ) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' ' + msgnecessario + '!\n';
	}else{
		if(allTrim(document.getElementById(form.id +':numero')) == 0){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' ' + msgDiferenteZeros + '!\n';
		}
	}
		
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaCamposCNPJDePara(form, msgcampo, msgnecessario,
		msgempresa, msgtipocontrato, msgnumero, msgconvenioPagFor,
		msgcnpj, msgDiferenteZeros) {

	var msgValidacao = '';

	var objRdoFiltroCNPJ = document.getElementsByName('rdoFiltroCNPJ');

	if (objRdoFiltroCNPJ[0].checked) {
		
		if (document.getElementById(form.id + ':txtCodConvenio').value == null
				|| allTrim(document.getElementById(form.id + ':txtCodConvenio')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconvenioPagFor + ' '
					+ msgnecessario + '!\n';
		} else {
			if (allTrim(document.getElementById(form.id + ':txtCodConvenio')) == 0) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconvenioPagFor + ' '
						+ msgDiferenteZeros + '!\n';
			}
		}
		
	} else if (objRdoFiltroCNPJ[1].checked) {

		if (allTrim(document.getElementById(form.id + ':txtCnpj')) == ""
				&& allTrim(document.getElementById(form.id + ':txtFilial')) == ""
				&& allTrim(document.getElementById(form.id + ':txtControle')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' '
					+ msgnecessario + '!\n';
		} else {
			if (allTrim(document.getElementById(form.id + ':txtCnpj')) == ""
					|| allTrim(document.getElementById(form.id + ':txtFilial')) == ""
					|| allTrim(document
							.getElementById(form.id + ':txtControle')) == "") {

				msgValidacao = msgValidacao + msgcnpjincorreto + '!\n';
			} else {
				if ((parseInt(allTrim(document
						.getElementById(form.id + ':txtCnpj')), 10) == 0)
						|| (parseInt(allTrim(document
								.getElementById(form.id + ':txtFilial')), 10) == 0)) {
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj
							+ ' ' + msgDiferenteZeros + '!' + '\n';
				}
			}
		}

	} else if (objRdoFiltroCNPJ[2].checked) {

		if (document.getElementById(form.id + ':empresaGestora').value == null
				|| document.getElementById(form.id + ':empresaGestora').value == '0') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' '
					+ msgnecessario + '!\n';
		}

		if (document.getElementById(form.id + ':tipoContrato').value == null
				|| document.getElementById(form.id + ':tipoContrato').value == '0') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipocontrato
					+ ' ' + msgnecessario + '!\n';
		}

		if (document.getElementById(form.id + ':numero').value == null
				|| allTrim(document.getElementById(form.id + ':numero')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' '
					+ msgnecessario + '!\n';
		} else {
			if (allTrim(document.getElementById(form.id + ':numero')) == 0) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' '
						+ msgDiferenteZeros + '!\n';
			}
		}
	}

	if (msgValidacao != '') {
		alert(msgValidacao);
		return false;
	}

	return true;
}

function validaCamposConsultaClienteInclusao(form, msgcampo, msgnecessario,
		msgcnpj, msgcpf, msgcnpjincorreto, msgcpfincorreto, msgDiferenteZeros) {
	var msgValidacao = '';

	var rdoFiltro = document.getElementsByName('rdoFiltro');

	if (rdoFiltro[1].checked) {

		if (allTrim(document.getElementById(form.id + ':txtCnpj')) == ""
				&& allTrim(document.getElementById(form.id + ':txtFilial')) == ""
				&& allTrim(document
						.getElementById(form.id + ':txtControleCNPJ')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' '
					+ msgnecessario + '!\n';
		} else {
			if (allTrim(document.getElementById(form.id + ':txtCnpj')) == ""
					|| allTrim(document.getElementById(form.id + ':txtFilial')) == ""
					|| allTrim(document
							.getElementById(form.id + ':txtControleCNPJ')) == "") {

				msgValidacao = msgValidacao + msgcnpjincorreto + '!\n';
			} else {
				if ((parseInt(allTrim(document
						.getElementById(form.id + ':txtCnpj')), 10) == 0)
						|| (parseInt(allTrim(document
								.getElementById(form.id + ':txtFilial')), 10) == 0)) {
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj
							+ ' ' + msgDiferenteZeros + '!' + '\n';
				}
			}
		}

	} else {

		if (allTrim(document.getElementById(form.id + ':txtCpf')) == ""
				&& allTrim(document.getElementById(form.id + ':txtControleCpf')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcpf + ' '
					+ msgnecessario + '!\n';
		} else {
			if (allTrim(document.getElementById(form.id + ':txtCpf')) == ""
					|| allTrim(document
							.getElementById(form.id + ':txtControleCpf')) == "") {
				msgValidacao = msgValidacao + msgcpfincorreto + '!\n';
			} else {
				if (parseInt(allTrim(document
						.getElementById(form.id + ':txtCpf')), 10) == 0) {
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcpf + ' '
							+ msgDiferenteZeros + '!' + '\n';
				}
			}
		}

	}

	if (msgValidacao != '') {
		alert(msgValidacao);
		return false;
	}

	return true;
}

function selecionarTodosCPFCNPJBloqueados(form, check) {
	for (i = 0; i < form.elements.length; i++) {
		if (form.elements[i].id.indexOf('dataTable_') != -1) {
			form.elements[i].checked = check.checked;
		}

	}
}

function validaPesquisaCpf() {
	var chkContrato = document.getElementById('incManterSolicitacaoPagamentos:chkContrato');
	var chk = document.getElementById('incManterSolicitacaoPagamentos:chkParticipanteContrato');
	var chkAgencia = document.getElementById('incManterSolicitacaoPagamentos:chkContaDebito');
	var chkCombo = document.getElementById('incManterSolicitacaoPagamentos:chkServicoModalidade');
	var busca = document.getElementById('incManterSolicitacaoPagamentos:cpfCnpjDescricao');
	var msgValidacao = '';
	var combo = document
			.getElementById('altManterClientesOrgaoPublicoFederalForm:situacaoCliente');

	if (chk.checked) {
		if (allTrim(document
				.getElementById('incManterSolicitacaoPagamentos:txtCorpoCpfCnpjParticipanteFiltro')) == ""
				&& allTrim(document
						.getElementById('incManterSolicitacaoPagamentos:txtFilialCpfCnpjParticipanteFiltro')) == ""
				&& allTrim(document
						.getElementById('incManterSolicitacaoPagamentos:txtControleCpfCnpjParticipanteFiltro')) == "") {
			msgValidacao = msgValidacao + 'Os campos CPF/CNPJ s\u00e3o necess\u00e1rios' + '!\n';
		}
		if (busca.innerText == "") {
			msgValidacao = msgValidacao + 'Obrigat\u00f3rio clicar no bot\u00e3o Buscar' + '!\n';
		}
	}
	
	if (chkAgencia.checked) {
		if (allTrim(document
				.getElementById('incManterSolicitacaoPagamentos:txtContaDebitoCheck')) == ""
					&& allTrim(document
							.getElementById('incManterSolicitacaoPagamentos:txtAgenciaContaDebitoCheck')) == "") {
			msgValidacao = msgValidacao + 'Os campos Ag\u00eancia/Conta s\u00e3o necess\u00e1rios' + '!\n';
		}
	}

	if (chkContrato.checked) {
		
		var numContrato = allTrim(document.getElementById('incManterSolicitacaoPagamentos:numero'));
		
		if (numContrato == "") {
			msgValidacao = msgValidacao + 'O campo n\u00famero do contrato \u00e9  necess\u00e1rio' + '!\n';
		}
	}

	if (chkCombo.checked) {
		var combo = document
				.getElementById('incManterSolicitacaoPagamentos:cboTipoServico');
		var itemSelecionado = combo[combo.selectedIndex];

		if (itemSelecionado.value == "0") {
			msgValidacao = msgValidacao + 'O campo Tipo Servi\u00e7o \u00e9 necess\u00e1rio' + '!\n';
		}
	}

	if (msgValidacao != '') {
		alert(msgValidacao);
		return false;
	}

	return true;
}

function validarProxCampoIdentClienteContrato(form) {
	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoNomeRazao = document
			.getElementById(form.id + ':txtNomeRazaoSocial');
	var campoBancoAgConta = document.getElementById(form.id + ':txtAgencia');

	if (objRdoCliente[0].checked) {
		campoCnpj.focus();
		return true;
	} else if (objRdoCliente[1].checked) {
		campoCpf.focus();
		return true;
	} else if (objRdoCliente[2].checked) {
		campoNomeRazao.focus();
		return true;
	} else if (objRdoCliente[3].checked) {
		campoBancoAgConta.focus();
		return true;
	} else {
		return false;
	}
}

function validarProxCampoIdentClienteContratoFlag(form, flagPesquisa) {
	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCnpjFilial = document.getElementById(form.id + ':txtFilial');
	var campoCnpjControle = document.getElementById(form.id + ':txtControle');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoCpfControle = document.getElementById(form.id + ':txtControleCpf');
	var campoNomeRazao = document
			.getElementById(form.id + ':txtNomeRazaoSocial');
	var campoBanco = document.getElementById(form.id + ':txtBanco');
	var campoAgencia = document.getElementById(form.id + ':txtAgencia');
	var campoConta = document.getElementById(form.id + ':txtConta');

	if (objRdoCliente[0].checked) {
		campoCnpj.disabled = false;
		campoCnpj.focus();
		campoCnpjFilial.disabled = false;
		campoCnpjControle.disabled = false;

		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';

		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';

		campoCpf.value = '';
		campoCpfControle.value = '';
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;

		if (flagPesquisa == 'true') {
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}

		return true;
	} else if (objRdoCliente[1].checked) {
		campoCpf.disabled = false;
		campoCpf.focus();
		campoCpfControle.disabled = false;

		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';

		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';

		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';

		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;

		if (flagPesquisa == 'true') {
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}

		return true;
	} else if (objRdoCliente[2].checked) {
		campoNomeRazao.disabled = false;
		campoNomeRazao.focus();

		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';

		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';

		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';

		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;

		if (flagPesquisa == 'true') {
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}

		return true;
	} else if (objRdoCliente[3].checked) {
		campoBanco.disabled = false;
		campoAgencia.disabled = false;
		campoConta.disabled = false;
		campoBanco.value = 237;
		campoAgencia.focus();

		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';

		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';

		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';

		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;

		if (flagPesquisa == 'true') {
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}

		return true;
	} else {
		document.getElementById(form.id + ':btoConsultarCliente').disabled = true;
		return false;
	}
}

function validarIncluir() {
	if (document.getElementById('confIncSolicAntecipacaoPostergarLotePagtoForm:novaDataAgendamento.day').value == ""
		|| document.getElementById('confIncSolicAntecipacaoPostergarLotePagtoForm:novaDataAgendamento.month').value == ""
		|| document.getElementById('confIncSolicAntecipacaoPostergarLotePagtoForm:novaDataAgendamento.year').value == "")
	{
		alert("O campo Nova Data de Agendamento \u00e9 necess\u00e1rio!");
		return false;
	}
	return true;
}
