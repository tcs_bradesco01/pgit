function selecionarTodos(form, check){

	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
		
	}	
	
	document.getElementById(form.id + ':btnRecuperarSelecionados').disabled = !check.checked;	
}

function validaSolRecPagVenNaoPagos(form, msgcampo, msgnecessario, msgagendados, msgou, msgpagosnaopagos, msgdatapagamento, msgargumentopesquisa,
		msgparticipante, msgcontadebito, msgbanco, msgagencia, msgconta, msgtiposervico, msgmodalidade, msgsituacaopagamento,
		msgmotivosituacao, msgnumeropagamento, msgvalorpagamento, msgremessa, msgnumero, msglinhadigitavel, msgradiofavorecido, msgcodigo, msgfavorecido,
		msginscricao, msgtipo, msgcontacredito, msgdigitoConta,msgTipoConta, msgDiferenteZeros ){
	var msgValidacao = '';
	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
		
	if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )		
		msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
	
	//Ao menos um argumento de pesquisa deve ser selecionado
	
	var objRadioArgumentosPesquisa = document.getElementsByName('radioPesquisasFiltroPrincipal');
	
	if ( document.getElementById(form.id +':cboTipoServico').value != null && document.getElementById(form.id +':cboTipoServico').value != '0' &&
		 document.getElementById(form.id +':cboModalidade').value != null && document.getElementById(form.id +':cboModalidade').value != '0'){
		
		if( !document.getElementById(form.id + ':chkParticipanteContrato').checked &&
			!document.getElementById(form.id + ':chkContaDebito').checked &&
			!document.getElementById(form.id + ':chkSituacaoMotivo').checked &&
			!objRadioArgumentosPesquisa[0].checked && !objRadioArgumentosPesquisa[1].checked &&		
			!objRadioArgumentosPesquisa[2].checked && !objRadioArgumentosPesquisa[3].checked
			/*&& !objRadioArgumentosPesquisa[4].checked */ ){
	
				msgValidacao = msgValidacao + msgargumentopesquisa + '!';	
		}
	}
	
	//Verifica preenchimento de "Participante do Contrato"	
	if (document.getElementById(form.id + ':chkParticipanteContrato').checked){
		if(allTrim(document.getElementById(form.id +':txtCodigoParticipante')) == ''){
			msgValidacao = msgValidacao + msgparticipante + '!' + '\n';		
		}
	}
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';				
		}	
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';				
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';				
		}					
	}	
	
	if (document.getElementById(form.id + ':chkSituacaoMotivo').checked){
		if ( document.getElementById(form.id +':cboMotivoSituacao').value == null || document.getElementById(form.id +':cboMotivoSituacao').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgmotivosituacao + ' ' + msgnecessario + '!' + '\n';	
	}	
	
	// Valida Numero e valor do Pagamento
	if (objRadioArgumentosPesquisa[0].checked){
		var flagNumeroValor = 0;
		var valorPagamentoDe = replaceAll(document.getElementById(form.id +':txtValorDePagamento').value);
		var valorPagamentoAte = replaceAll(document.getElementById(form.id +':txtValorAtePagamento').value);
		
		var valorPreenchido= false;
		var numeroPreenchido = false;
		
		if( (allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == '')){
			flagNumeroValor++;
		}else{
			numeroPreenchido = true;
		}
			


		if( (((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') ||
			(valorPagamentoDe == '0')) &&
   		    ((allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == '') || 
   		    (valorPagamentoAte == '0')))){
			flagNumeroValor++;
		}else{
			valorPreenchido = true;
		}
			
		if(flagNumeroValor == 2){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgou + ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';			
		}else{
			if(numeroPreenchido && ((allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == ''))){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgnecessario + '!' + '\n';		
			}
			if(valorPreenchido && ((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') || 
				                   (allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == ''))){
			
   		     	msgValidacao = msgValidacao + msgcampo + ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';	
   		    }else{
	   		    if(valorPreenchido && (valorPagamentoDe == '0' || valorPagamentoAte == '0')){
				
	   		     	msgValidacao = msgValidacao + msgcampo + ' ' + msgvalorpagamento + ' ' + msgDiferenteZeros + '!' + '\n';	
	   		    }
   		    	
   		    }
   		   
			
		}
		
	}else{
		//N�mero da Remessa
		if (objRadioArgumentosPesquisa[1].checked){
			if (allTrim(document.getElementById(form.id +':numeroRemessaFiltro')) == ''){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgnecessario + '!' + '\n';
			}else{ 
				if (parseInt(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')),10) == 0 )
					msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgDiferenteZeros + '!' + '\n';
			}	
		}
		else{
			//Valida Linha Digit�vel
			if (objRadioArgumentosPesquisa[2].checked){
				if ((allTrim(document.getElementById(form.id +':linhaDigitavel1')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel2')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel3')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel4')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel5')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel6')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel7')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel8')) == ''))	 
						msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgnecessario + '!' + '\n';	
				else{
					if( parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel1')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel2')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel3')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel4')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel5')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel6')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel7')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel8')),10) == 0 )
							msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgDiferenteZeros + '!' + '\n';
				}						
			}	
			else{
				//Favorecido
				if (objRadioArgumentosPesquisa[3].checked){
					var objRadioFavorecido = document.getElementsByName('radioFavorecidoFiltro');				
					
					if (!objRadioFavorecido[0].checked && !objRadioFavorecido[1].checked && !objRadioFavorecido[2].checked){
						msgValidacao = msgValidacao + msgradiofavorecido + '!' + '\n';	
						alert(msgValidacao);
						return false;						
					}
					
					if (objRadioFavorecido[0].checked){
						if(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';
						else{
							if(parseInt(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')),10) == 0){
								msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!' + '\n';
							}
						}					
					}
					else if(objRadioFavorecido[1].checked){
						if(allTrim(document.getElementById(form.id +':txtInscricao')) == '' ){
							msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';	
						}else{
						
							if (parseInt(document.getElementById(form.id +':txtInscricao').value,10) == 0){
								msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!' + '\n';	
							}
						}
							
						if ( document.getElementById(form.id +':tipoInscricao')== null || allTrim(document.getElementById(form.id +':tipoInscricao')) == '0' )							
							msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' (' + msgfavorecido +') ' + msgnecessario + '!' + '\n';				
					}					
					else if(objRadioFavorecido[2].checked){
						if(allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if (allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';				
						}	
						if(allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == ''){ 
							msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if (allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';				
						}	
						if(allTrim(document.getElementById(form.id +':txtContaContaCredito')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if(parseInt(allTrim(document.getElementById(form.id +':txtContaContaCredito')),10) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';				
						}						
					}
			  }
		}
	}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function replaceAll(string){
	while (string.indexOf(".") > 0){
		string = string.replace(".","");
	}
	string = parseFloat(string.replace(",", ""));
	return string;
}	