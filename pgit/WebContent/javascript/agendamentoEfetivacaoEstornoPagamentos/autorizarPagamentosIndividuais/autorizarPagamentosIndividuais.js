function selecionarTodos(form, check){
	var checkCont = 0;
	
	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
			checkCont++;
		}	
		
	}	
	
	document.getElementById(form.id + ':btnLimparSelecionados').disabled = !check.checked;	
	document.getElementById(form.id + ':btnAutorizarSelecionados').disabled = !check.checked;	
	
	if(checkCont == 1){
		document.getElementById(form.id + ':btnDetalhar').disabled = !check.checked;
	}else{
		document.getElementById(form.id + ':btnDetalhar').disabled = true;
	}	
}


function atualizaRegistroSelecionado(form){

	var existeRegistroSelecionado = false;
	for(i=0; i<form.elements.length && !existeRegistroSelecionado; i++)
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			if (form.elements[i].checked){
				existeRegistroSelecionado = true;
			}

		}	
		
	}	

	document.getElementById(form.id + ':btnLimparSelecionados').disabled = !existeRegistroSelecionado;	
	document.getElementById(form.id + ':btnAutorizarSelecionados').disabled = !existeRegistroSelecionado;	
	document.getElementById(form.id + ':btnDetalhar').disabled = !existeRegistroSelecionado;		
}




function limparSelecionados(form){

	for(i=0; i<form.elements.length ; i++)
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = false;
		}	
		
	}	
	document.getElementById(form.id + ':btnDetalhar').disabled = true;
	document.getElementById(form.id + ':btnLimparSelecionados').disabled = true;	
	document.getElementById(form.id + ':btnAutorizarSelecionados').disabled = true;	
	document.getElementById(form.id + ':dataTable:checkSelecionarTodos').checked = false;	
	return true;
	
}