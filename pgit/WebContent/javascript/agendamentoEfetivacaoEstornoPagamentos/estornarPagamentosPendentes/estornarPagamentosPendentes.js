
function validarDetalhar(form, msgcheck ){

	var campos, hidden;
	campos = '';
	
	if (document.getElementById(form.id + ':hiddenObrigatoriedade').value > 1){
			campos = campos + msgcheck + '!' + '\n';	
		}
			
	if (campos != ''){
			alert(campos);
			campos = '';
		
	}
}

function selecionarTodos(form, check){

	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
		
	}	
	
	document.getElementById(form.id + ':btnEstornarSelecionados').disabled = !check.checked;
	 document.getElementById(form.id + ':btnLimparSelecao').disabled = !check.checked;		
}

function atualizaRegistroSelecionado(form){

	var existeRegistroSelecionado = false;
	for(i=0; i<form.elements.length && !existeRegistroSelecionado; i++)
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			if (form.elements[i].checked){
				existeRegistroSelecionado = true;
			}

		}	
		
	}	

	document.getElementById(form.id + ':btnDesautorizarSelecionados').disabled = !existeRegistroSelecionado;		
}


function validaEstornarPagamentos(form, msgcampo, msgnecessario, msgagendados, msgou, msgpagosnaopagos, msgdatapagamento, msgargumentopesquisa,
			msgparticipante, msgcontadebito, msgbanco, msgagencia, msgconta, msgtiposervico, msgmodalidade, msgsituacaopagamento,
			msgmotivosituacao, msgnumeropagamento, msgvalorpagamento, msgremessa, msgnumero, msglinhadigitavel, msgradiofavorecido, msgcodigo, msgfavorecido,
			msginscricao, msgtipo, msgcontacredito, msgdigitoConta,msgTipoConta, msgDiferenteZeros  ){

	var msgValidacao = '';
	var tipoInscricao = document.getElementById(form.id +':tipoInscricao').value;
	var objRadioArgumentosPesquisa = document.getElementsByName('radioPesquisasFiltroPrincipal');
	
	//Verifica preenchimento do periodo
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
			allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
			document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
			allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
			document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
			allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
			document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
			allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
			document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
			allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
			document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
			allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
			
				msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
		}

	//Verifica preenchimento de "Participante do Contrato"	
	if (document.getElementById(form.id + ':chkParticipanteContrato').checked){
		if(allTrim(document.getElementById(form.id +':txtCodigoParticipante')) == ''){
			msgValidacao = msgValidacao + msgparticipante + '!' + '\n';		
		}
	}

	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa

		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}

		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}

		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}

		/*if(allTrim(document.getElementById(form.id +':txtDigitoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoConta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';					
		}*/

	//Valida Tipo Servi�o/Modalidade
	
	if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' ){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
	}	
	if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )	{	
			msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';
	}	
	
	// Valida Numero e valor do Pagamento
	if (objRadioArgumentosPesquisa[0].checked){
		var flagNumeroValor = 0;
		var valorPagamentoDe = replaceAll(document.getElementById(form.id +':txtValorDePagamento').value);
		var valorPagamentoAte = replaceAll(document.getElementById(form.id +':txtValorAtePagamento').value);
		
		var valorPreenchido= false;
		var numeroPreenchido = false;
		
		if( (allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == '') ){
			flagNumeroValor++;
		}else{
			numeroPreenchido = true;
		}

		if( (((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') ||
			(valorPagamentoDe == '0')) &&
   		    ((allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == '') || 
   		    (valorPagamentoAte == '0')))){
			flagNumeroValor++;
		}else{
			valorPreenchido = true;
		}
			
		if(flagNumeroValor == 2){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgou + ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';			
		}else{

			if(numeroPreenchido && ((allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == ''))){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgnecessario + '!' + '\n';		
			}
			if(valorPreenchido && ((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') ||  
				                   (allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == ''))){
			
   		     	msgValidacao = msgValidacao + msgcampo + ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';	
   		    }else{
	   		    if(valorPreenchido && (valorPagamentoDe == '0' || valorPagamentoAte == '0')){
				
	   		     	msgValidacao = msgValidacao + msgcampo + ' ' + msgvalorpagamento + ' ' + msgDiferenteZeros + '!' + '\n';	
	   		    }
   		    }
   		   
			
		}	
		
				
	}
	
	//N�mero da Remessa
	if (objRadioArgumentosPesquisa[1].checked){
		if (allTrim(document.getElementById(form.id +':numeroRemessaFiltro')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgnecessario + '!' + '\n';				
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgDiferenteZeros + '!' + '\n';
		}
	}
	
	

	//Favorecido
	if (objRadioArgumentosPesquisa[2].checked){
		var objRadioFavorecido = document.getElementsByName('radioFavorecidoFiltro');	
			
		
		if (!objRadioFavorecido[0].checked && !objRadioFavorecido[1].checked){
			msgValidacao = msgValidacao + msgradiofavorecido + '!' + '\n';	
			alert(msgValidacao);
			return false;						
		}
		
		if (objRadioFavorecido[0].checked){
			if(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')) == '')
				msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';				
			else{
				if (parseInt(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')),10) == 0 )
					msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!' + '\n';
			}	
		}
		else if(objRadioFavorecido[1].checked){
			if ( document.getElementById(form.id +':tipoInscricao')== null || allTrim(document.getElementById(form.id +':tipoInscricao')) == '0' ){							
				msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' (' + msgfavorecido +') ' + msgnecessario + '!' + '\n';
				if(allTrim(document.getElementById(form.id +':txtInscricaoOutros')) == '')
						msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';
		 	 }			
			else{
				if(tipoInscricao == 1){
					if(allTrim(document.getElementById(form.id +':txtInscricaoCpf')) == '')
						msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';
					
				}else if(tipoInscricao == 2){
					if(allTrim(document.getElementById(form.id +':txtInscricaoCnpj')) == '')
						msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';
				}else{
					if(allTrim(document.getElementById(form.id +':txtInscricaoOutros')) == '')
						msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';
				}
			}
		}	
	}  
	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						

}

function limitarQtdCaracter() {
	if (document.all) {// Internet Explorer
		var tecla = event.keyCode;
	} else if(document.layers) {// Nestcape
		var tecla = event.which;
	}

	// Check if the control key is pressed.   
	// If the Netscape way won't work (event.modifiers is undefined),   
	// try the IE way (event.ctrlKey)   
	var ctrl = typeof event.modifiers == 'undefined' ?  event.ctrlKey : event.modifiers & Event.CONTROL_MASK;   
  
	// Check if the 'V' key is pressed.   
	// If the Netscape way won't work (event.which is undefined),   
	// try the IE way (event.keyCode)   
	var v = typeof event.which == 'undefined' ?  event.keyCode == 86 : event.which == 86;   
  
	// If the control and 'V' keys are pressed at the same time   
	if ( ctrl && v ) {   
		// ... discard the keystroke and clear the text box   
		return false;   
	}  
	
	
	if (document.getElementById('confEstonarPagamentosForm:observacao').value.length > 199 && tecla != 8 && tecla != 9 && tecla != 46 && tecla != 37 && tecla != 38 && tecla != 39 && tecla != 40){
		alert('O campo Observa\u00E7\u00E3o s\u00F3 permite a digita\u00E7\u00E3o de 200 caracteres.');
       	return false;
   	}
}

function validaObrigatoriedadeObservacao(form, msgcampo, msgnecessario, msgobservacao){

	 var msgValidacao = '';
	 var tipoInscricao = document.getElementById(form.id +':tipoInscricao').value;

	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
	
}

function replaceAll(string){
	while (string.indexOf(".") > 0){
		string = string.replace(".","");
	}
	string = parseFloat(string.replace(",", ""));
	return string;
}