function validaManterLibPreviaSemConSaldos(form, msgcampo, msgnecessario, msgDataLiberacao, msgcodigoparticipante, msgnomeparticipante, msgcontadebito, msgbanco,
msgagencia, msgconta, msgdigitoConta, msgDiferenteZeros){

	var msgValidacao = '';
	
	if (allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == '' ||
  	 	allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == '' ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == '' ||
  	 	allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == '' ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgDataLiberacao + ' ' + msgnecessario + '!' + '\n';	
	}
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}	
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}						
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}

function validaCamposIncluir(form, msgcampo, msgnecessario, contaDebito, dataprogramada, msgconta){

	var msgValidacao = '';			

		if (allTrim(document.getElementById(form.id + ':dataProgramada.day')) == '' ||
			allTrim(document.getElementById(form.id + ':dataProgramada.month')) == '' ||
			allTrim(document.getElementById(form.id + ':dataProgramada.year')) == ''){
				msgValidacao = msgValidacao + msgcampo + ' ' + dataprogramada + ' ' + msgnecessario + '!' + '\n';	
		}

		if (document.getElementById(form.id +':hiddenContaSelecionada').value == 'false'){
			msgValidacao = msgValidacao + msgconta + '!' + '\n';	
		}

	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;

}