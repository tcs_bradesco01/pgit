function validaCampos(form, msgCampo, msgNecessario,msgDataInicio,msgDataFim, msgtipo, msgInscricaoFavorecido, msgDiferenteZeros){
	var msgValidacao = '';

	if (allTrim(document.getElementById(form.id + ':txtDataCreditoOpDe.day')) == ""){
		msgValidacao = msgValidacao +   msgCampo + ' ' + msgDataInicio + ' ' + msgNecessario + '!\n';				
	}else{
		if (allTrim(document.getElementById(form.id + ':txtDataCreditoOpDe.month')) == ""){
			msgValidacao = msgValidacao +   msgCampo + ' ' + msgDataInicio + ' ' + msgNecessario + '!\n';				
		}else{
			if (allTrim(document.getElementById(form.id + ':txtDataCreditoOpDe.year')) == ""){
				msgValidacao = msgValidacao +   msgCampo + ' ' + msgDataInicio + ' ' + msgNecessario + '!\n';				
			}
		}
	}

	if (allTrim(document.getElementById(form.id + ':txtDataCreditoOpAte.day')) == ""){
		msgValidacao = msgValidacao +   msgCampo + ' ' + msgDataFim + ' ' + msgNecessario + '!\n';				
	}else{
		if (allTrim(document.getElementById(form.id + ':txtDataCreditoOpAte.month')) == ""){
			msgValidacao = msgValidacao +   msgCampo + ' ' + msgDataFim + ' ' + msgNecessario + '!\n';				
		}else{
			if (allTrim(document.getElementById(form.id + ':txtDataCreditoOpAte.year')) == ""){
				msgValidacao = msgValidacao +   msgCampo + ' ' + msgDataFim + ' ' + msgNecessario + '!\n';				
			}
		}
	}
	
	if(allTrim(document.getElementById(form.id +':inscricaoFavorecido')) != ''){
		if ( document.getElementById(form.id +':tipoFavorecido').value == null || document.getElementById(form.id +':tipoFavorecido').value == '0' ){
			msgValidacao = msgValidacao +   msgCampo + ' ' + msgtipo + ' ' + msgNecessario + '!\n';
		}
	}
	
	if(document.getElementById(form.id +':tipoFavorecido').value == '1' || document.getElementById(form.id +':tipoFavorecido').value == '2'){
		if (allTrim(document.getElementById(form.id +':inscricaoFavorecido')) == ''){
				msgValidacao = msgValidacao + msgCampo + ' ' + msgInscricaoFavorecido + ' ' + msgNecessario + '!' + '\n';				
			}else{
				if (parseInt(allTrim(document.getElementById(form.id +':inscricaoFavorecido')),10) == 0 ){
						msgValidacao = msgValidacao + msgCampo + ' ' + msgInscricaoFavorecido + ' ' + msgDiferenteZeros + '!' + '\n';
					}
			}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}