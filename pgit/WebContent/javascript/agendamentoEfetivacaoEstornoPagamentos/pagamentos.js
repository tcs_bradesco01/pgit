function validaCampoParticipante(form, msgcampo, msgnecessario, msgCpfCnpj, msgBase, msgFilial, msgControle, msgDiferenteZeros){

	var msgValidacao = '';
	
	
	if ( allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) == '') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgControle+ ')' + ' ' +  msgnecessario + '!';
	}else{
		if ( allTrim(document.getElementById(form.id +':txtFilialCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) == '') {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' ' +  msgnecessario + '!';
		}else{
		
			if ( allTrim(document.getElementById(form.id +':txtFilialCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) == '' &&  allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) == ''  ) {
					msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' '  + msgnecessario + '!\n';
					msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgControle+ ')' + ' '  + msgnecessario + '!';
			}else{
			
				if ( allTrim(document.getElementById(form.id +':txtControleCpfCnpjParticipanteFiltro')) != '' &&  allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')) == '' && allTrim(document.getElementById(form.id +':txtFilialCpfCnpjParticipanteFiltro')) == '' ) {
						msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' ' + msgnecessario + '!';						
				}else{
				
					if ( parseInt(allTrim(document.getElementById(form.id +':txtCorpoCpfCnpjParticipanteFiltro')),10) == 0 ) {
							msgValidacao = msgValidacao + msgcampo + ' ' + msgCpfCnpj + ' (' + msgBase+ ')' + ' ' + msgDiferenteZeros + '!';						
					}
				}
				
			}
		}
	}

	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaPagamentoIndividual(form, msgcampo, msgnecessario, msgagendados, msgou, msgpagosnaopagos, msgdatapagamento, msgargumentopesquisa,
		msgparticipante, msgcontadebito, msgbanco, msgagencia, msgconta, msgtiposervico, msgmodalidade, msgsituacaopagamento,
		msgmotivosituacao, msgnumeropagamento, msgvalorpagamento, msgremessa, msgnumero, msglinhadigitavel, msgradiofavorecido, msgcodigo, msgfavorecido,
		msginscricao, msgtipo, msgcontacredito, msgdigitoConta,msgTipoConta, msgDiferenteZeros, msgrastreamentotitulos, msgcontasalario){
	var msgValidacao = '';
	
	var objRdoCliente = document.getElementsByName('radioAgendadosPagosNaoPagos');
	
	if ( !objRdoCliente[1].checked && !objRdoCliente[2].checked )
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagendados + ' ' + msgou + ' ' + msgpagosnaopagos + ' ' + msgnecessario + '!' + '\n';	
	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	//Ao menos um argumento de pesquisa deve ser selecionado
	var objRadioArgumentosPesquisa = document.getElementsByName('radioPesquisasFiltroPrincipal');
	if( !document.getElementById(form.id + ':chkParticipanteContrato').checked &&
		!document.getElementById(form.id + ':chkContaDebito').checked &&
		!document.getElementById(form.id + ':chkServicoModalidade').checked &&
		!document.getElementById(form.id + ':chkSituacaoMotivo').checked &&
		!document.getElementById(form.id + ':chkRastreamentoTitulo').checked &&
		!objRadioArgumentosPesquisa[0].checked && !objRadioArgumentosPesquisa[1].checked &&		
		!objRadioArgumentosPesquisa[2].checked && !objRadioArgumentosPesquisa[3].checked /*&&				
		!objRadioArgumentosPesquisa[4].checked */){		
			msgValidacao = msgValidacao + msgargumentopesquisa + '!';	
			alert(msgValidacao);
			return false;
	}
	
	//Verifica preenchimento de "Participante do Contrato"	
	if (document.getElementById(form.id + ':chkParticipanteContrato').checked){
		if(allTrim(document.getElementById(form.id +':txtCodigoParticipante')) == ''){
			msgValidacao = msgValidacao + msgparticipante + '!' + '\n';		
		}
	}
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}	
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}
		
		
	}	
	
	if (document.getElementById(form.id + ':chkServicoModalidade').checked){
		if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
			
		//if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )		
			//msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
	}

	var objRadioRastreamentoTitulo = document.getElementsByName('radioRastreadosNaoRastreados');
	if (document.getElementById(form.id + ':chkRastreamentoTitulo').checked){
		if (!objRadioRastreamentoTitulo[0].checked && !objRadioRastreamentoTitulo[1].checked && !objRadioRastreamentoTitulo[2].checked)
			msgValidacao = msgValidacao + msgcampo + ' ' + msgrastreamentotitulos + ' ' + msgnecessario + '!' + '\n';
	}

	if (document.getElementById(form.id + ':chkSituacaoMotivo').checked){
		if ( document.getElementById(form.id +':cboSituacaoPagamento').value == null || document.getElementById(form.id +':cboSituacaoPagamento').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsituacaopagamento + ' ' + msgnecessario + '!' + '\n';	
			
		/* Campo motivo da situa��o n�o � obrigat�rio - Altera��o Solicitada por F�bio Corre_Web2.doc
		if ( document.getElementById(form.id +':cboMotivoSituacao').value == null || document.getElementById(form.id +':cboMotivoSituacao').value == '0' )		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgmotivosituacao + ' ' + msgnecessario + '!' + '\n';	
		*/
	}	
	
	// Valida Numero e valor do Pagamento
	if (objRadioArgumentosPesquisa[0].checked){
		var flagNumeroValor = 0;
		
		var valorPagamentoDe = replaceAll(document.getElementById(form.id +':txtValorDePagamento').value);
		var valorPagamentoAte = replaceAll(document.getElementById(form.id +':txtValorAtePagamento').value);
		
		var valorPreenchido= false;
		var numeroPreenchido = false;
		
		if( (allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == '') ){
			flagNumeroValor++;
		}else{
			numeroPreenchido = true;
		}
			


		if( (((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') ||
			(valorPagamentoDe == '0')) &&
   		    ((allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == '') || 
   		    (valorPagamentoAte == '0')))){
			flagNumeroValor++;
		}else{
			valorPreenchido = true;
		}
			
		if(flagNumeroValor == 2){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgou + ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';			
		}else{
			if(numeroPreenchido && ((allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == ''))){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgnecessario + '!' + '\n';		
			}
			if(valorPreenchido && ((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') || 
				                   (allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == ''))){
			
   		     	msgValidacao = msgValidacao + msgcampo + ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';	
   		    }else{
	   		    if(valorPreenchido && (valorPagamentoDe == '0' || valorPagamentoAte == '0')){
				
	   		     	msgValidacao = msgValidacao + msgcampo + ' ' + msgvalorpagamento + ' ' + msgDiferenteZeros + '!' + '\n';	
	   		    }
   		    	
   		    }
   		   
			
		}
		
				
		
		
	}else{
		//N�mero da Remessa
		if (objRadioArgumentosPesquisa[1].checked){
			if (allTrim(document.getElementById(form.id +':numeroRemessaFiltro')) == ''){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgnecessario + '!' + '\n';				
			}else{
				if (parseInt(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')),10) == 0 )
					msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgDiferenteZeros + '!' + '\n';
			}
		}
		else{
			//Valida Linha Digit�vel
			if (objRadioArgumentosPesquisa[2].checked){
				if ((allTrim(document.getElementById(form.id +':linhaDigitavel1')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel2')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel3')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel4')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel5')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel6')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel7')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel8')) == ''))	 
						msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgnecessario + '!' + '\n';
				else{
					if( parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel1')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel2')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel3')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel4')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel5')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel6')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel7')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel8')),10) == 0 )
							msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgDiferenteZeros + '!' + '\n';
				}		
			}	
			else{
				//Favorecido
				if (objRadioArgumentosPesquisa[3].checked){
					var objRadioFavorecido = document.getElementsByName('radioFavorecidoFiltro');				
					
					if (!objRadioFavorecido[0].checked && !objRadioFavorecido[1].checked && 
							!objRadioFavorecido[2].checked && !objRadioFavorecido[3].checked
							&& !objRadioFavorecido[4].checked){
						msgValidacao = msgValidacao + msgradiofavorecido + '!' + '\n';	
						alert(msgValidacao);
						return false;						
					}
					
					if (objRadioFavorecido[0].checked){
						if(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';											
						}else{
							if (parseInt(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')),10) == 0 ){								
								msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!' + '\n';
							}
						}	
					}
					else 
					if(objRadioFavorecido[1].checked){
					
						if(allTrim(document.getElementById(form.id +':txtInscricao')) == '' ){
							msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';	
						}else{
						
							if (parseInt(document.getElementById(form.id +':txtInscricao').value,10) == 0){
								msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!' + '\n';	
							}
						}
						if ( document.getElementById(form.id +':tipoInscricao')== null || allTrim(document.getElementById(form.id +':tipoInscricao')) == '0' )							
							msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' (' + msgfavorecido +') ' + msgnecessario + '!' + '\n';
					}					
					else if(objRadioFavorecido[2].checked){
						if(allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if (allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';				
						}
						if(allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if (allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';				
						}
						if(allTrim(document.getElementById(form.id +':txtContaContaCredito')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if (parseInt(allTrim(document.getElementById(form.id +':txtContaContaCredito')),10) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';				
						}
						/*if(allTrim(document.getElementById(form.id +':txtDigitoContaCredito')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoConta + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						}*/
						
					}else if(objRadioFavorecido[3].checked){
						if(allTrim(document.getElementById(form.id +':txtBancoContaSalario')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontasalario + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if (allTrim(document.getElementById(form.id +':txtBancoContaSalario')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontasalario + ') ' +  msgDiferenteZeros + '!' + '\n';				
						}
						if(allTrim(document.getElementById(form.id +':txtAgenciaContaSalario')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontasalario + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if (allTrim(document.getElementById(form.id +':txtAgenciaContaSalario')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontasalario + ') ' +  msgDiferenteZeros + '!' + '\n';				
						}
						if(allTrim(document.getElementById(form.id +':txtContaContaSalario')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontasalario + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if (parseInt(allTrim(document.getElementById(form.id +':txtContaContaSalario')),10) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontasalario + ') ' +  msgDiferenteZeros + '!' + '\n';				
						}
						
					}else if(objRadioFavorecido[4].checked){
						
						var bancoPagamento = allTrim(document.getElementById(form.id +':txtBancoPagamento'));
						var ispb = allTrim(document.getElementById(form.id +':txtISPB'));
						var contaPagamento = allTrim(document.getElementById(form.id +':txtContaPagamento'));
						
						
						if(bancoPagamento == '' && ispb == ''){
							msgValidacao = msgValidacao + msgcampo + ' Banco Pagamento ou ISPB ' +  msgnecessario + '!' + '\n';				
						
						}else if (bancoPagamento == 0 && ispb == ''){
							msgValidacao = msgValidacao + msgcampo + ' Banco Pagamento ' +  msgDiferenteZeros + '!' + '\n';
						
					//	}else if (bancoPagamento == '' && ispb == 0){
					//		msgValidacao = msgValidacao + msgcampo + ' ISPB ' +  msgDiferenteZeros + '!' + '\n';
						
					//	}else if (bancoPagamento == 0 && ispb == 0){
					//		msgValidacao = msgValidacao + msgcampo + ' Banco Pagamento ou ISPB ' +  msgnecessario + '!' + '\n';
						
					//	}else if (parseInt(bancoPagamento) > 0 && parseInt(ispb) > 0){
					//		msgValidacao = msgValidacao + 'Somente o campo Banco Pagamento ou o campo ISPB deve ser preenchido!' ;
						}
						

						if(contaPagamento == ''){
							msgValidacao = msgValidacao + msgcampo + ' Conta Pagamento ' +  msgnecessario + '!' + '\n';				
						}else{
							if (parseInt(contaPagamento,20) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' Conta Pagamento ' +  msgDiferenteZeros + '!' + '\n';				
						}
						
					}
			  }
			
			}
		}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaPagamentoIndividualSemSituacaoOperPagamento(form, msgcampo, msgnecessario, msgagendados, msgou, msgpagosnaopagos, msgdatapagamento, msgargumentopesquisa,
		msgparticipante, msgcontadebito, msgbanco, msgagencia, msgconta, msgtiposervico, msgmodalidade, msgsituacaopagamento,
		msgmotivosituacao, msgnumeropagamento, msgvalorpagamento, msgremessa, msgnumero, msglinhadigitavel, msgradiofavorecido, msgcodigo, msgfavorecido,
		msginscricao, msgtipo, msgcontacredito, msgdigitoConta, msgtipoconta, msgDiferenteZeros ){
	var msgValidacao = '';

	var objRdoCliente = document.getElementsByName('radioAgendadosPagosNaoPagos');
	
	if(objRdoCliente != null && objRdoCliente.length > 0){
		if (!objRdoCliente[1].checked && !objRdoCliente[2].checked )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagendados + ' ' + msgou + ' ' + msgpagosnaopagos + ' ' + msgnecessario + '!' + '\n';		
	}
	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	//Ao menos um argumento de pesquisa deve ser selecionado
	var objRadioArgumentosPesquisa = document.getElementsByName('radioPesquisasFiltroPrincipal');
	if( !document.getElementById(form.id + ':chkParticipanteContrato').checked &&
		!document.getElementById(form.id + ':chkContaDebito').checked &&
		!document.getElementById(form.id + ':chkServicoModalidade').checked &&
		!objRadioArgumentosPesquisa[0].checked && !objRadioArgumentosPesquisa[1].checked &&		
		!objRadioArgumentosPesquisa[2].checked && !objRadioArgumentosPesquisa[3].checked){		
			msgValidacao = msgValidacao + msgargumentopesquisa + '!';	
			alert(msgValidacao);
			return false;
	}
	
	//Verifica preenchimento de "Participante do Contrato"	
	if (document.getElementById(form.id + ':chkParticipanteContrato').checked){
		if(allTrim(document.getElementById(form.id +':txtCodigoParticipante')) == ''){
			msgValidacao = msgValidacao + msgparticipante + '!' + '\n';		
		}
	}
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' +  msgDiferenteZeros + '!' + '\n';				
		}
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' +  msgDiferenteZeros + '!' + '\n';				
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' +  msgDiferenteZeros + '!' + '\n';				
		}			
		/*if(allTrim(document.getElementById(form.id +':txtDigitoContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoConta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';	*/				
	}	
	
	if (document.getElementById(form.id + ':chkServicoModalidade').checked){
		if (document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
			
		//if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )		
		//	msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
	}	
		
	// Valida Numero e valor do Pagamento
	if (objRadioArgumentosPesquisa[0].checked){
		var flagNumeroValor = 0;
		var valorPagamentoDe = replaceAll(document.getElementById(form.id +':txtValorDePagamento').value);
		var valorPagamentoAte = replaceAll(document.getElementById(form.id +':txtValorAtePagamento').value);
		
		var valorPreenchido= false;
		var numeroPreenchido = false;
		
		if( (allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == '')){
			flagNumeroValor++;
		}else{
			numeroPreenchido = true;
		}
			


		if( (((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') ||
			(valorPagamentoDe == '0')) &&
   		    ((allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == '') || 
   		    (valorPagamentoAte == '0')))){
			flagNumeroValor++;
		}else{
			valorPreenchido = true;
		}
			
		if(flagNumeroValor == 2){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgou + ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';			
		}else{

			if(numeroPreenchido && ((allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == ''))){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgnecessario + '!' + '\n';		
			}
			if(valorPreenchido && ((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') ||  
				                   (allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == ''))){
			
   		     	msgValidacao = msgValidacao + msgcampo + ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';	
   		    }else{
	   		    if(valorPreenchido && (valorPagamentoDe == '0' || valorPagamentoAte == '0')){
				
	   		     	msgValidacao = msgValidacao + msgcampo + ' ' + msgvalorpagamento + ' ' + msgDiferenteZeros + '!' + '\n';	
	   		    }
   		    }
   		   
			
		}	
	}else{
		//N�mero da Remessa
		if (objRadioArgumentosPesquisa[1].checked){
			if (allTrim(document.getElementById(form.id +':numeroRemessaFiltro')) == '')
				msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgnecessario + '!' + '\n';
			else{
				if (parseInt(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')),10) == 0 )
					msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgDiferenteZeros + '!' + '\n';
			}				
		}
		else{
			//Valida Linha Digit�vel
			if (objRadioArgumentosPesquisa[2].checked){
				if ((allTrim(document.getElementById(form.id +':linhaDigitavel1')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel2')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel3')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel4')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel5')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel6')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel7')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel8')) == '')	)	 
						msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgnecessario + '!' + '\n';	
				else{
					if( parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel1')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel2')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel3')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel4')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel5')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel6')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel7')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel8')),10) == 0 )
							msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgDiferenteZeros + '!' + '\n';
				}		
			}	
			else{
				//Favorecido
				if (objRadioArgumentosPesquisa[3].checked){
					var objRadioFavorecido = document.getElementsByName('radioFavorecidoFiltro');				
					
					if (!objRadioFavorecido[0].checked && !objRadioFavorecido[1].checked && !objRadioFavorecido[2].checked){
						msgValidacao = msgValidacao + msgradiofavorecido + '!' + '\n';	
						alert(msgValidacao);
						return false;						
					}
					
					if (objRadioFavorecido[0].checked){
						if(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';				
						else{
							if ( parseInt(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')),10) == 0 ){
								msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!' + '\n';												
							}
						}	
					}
					else if(objRadioFavorecido[1].checked){
						if(allTrim(document.getElementById(form.id +':txtInscricao')) == '' ){
							msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';	
						}else{
						
							if (parseInt(document.getElementById(form.id +':txtInscricao').value,10) == 0){
								msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!' + '\n';	
							}
						}	
						if (document.getElementById(form.id +':tipoInscricao').value == null || allTrim(document.getElementById(form.id +':tipoInscricao')) == '0' )							
							msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' (' + msgfavorecido +') ' + msgnecessario + '!' + '\n';				
					}					
					else if(objRadioFavorecido[2].checked){
						if(allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						else{
							if (allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';
						}	
						if(allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						else{
							if (allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';
						}
						if(allTrim(document.getElementById(form.id +':txtContaContaCredito')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						else{
							if (parseInt(allTrim(document.getElementById(form.id +':txtContaContaCredito')),10) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';
						}							
						/*if(allTrim(document.getElementById(form.id +':txtDigitoContaCredito')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoConta + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';*/				
							
						
					}
			  }
			
		}
	}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}


function validaPagamentoIndividualComTipoServicoModalidade(form, msgcampo, msgnecessario, msgagendados, msgou, msgpagosnaopagos, msgdatapagamento, msgargumentopesquisa,
		msgcodigoparticipante,  msgcontadebito, msgbanco, msgagencia, msgconta, msgtiposervico, msgmodalidade, msgsituacaopagamento,
		msgmotivosituacao, msgnumeropagamento, msgvalorpagamento, msgremessa, msgnumero, msglinhadigitavel, msgradiofavorecido, msgcodigo, msgfavorecido,
		msginscricao, msgtipo, msgcontacredito, msgdigitoConta, msgtipoconta, msgDiferenteZeros, msgProcessamentoPagamento ){
	var msgValidacao = '';

	var radio = document.getElementsByName('radioTipoAntecipacao');

	var objRdoCliente = document.getElementsByName('radioAgendadosPagosNaoPagos');
	
	if(objRdoCliente != null && objRdoCliente.length > 0){
		if (!objRdoCliente[1].checked && !objRdoCliente[2].checked )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagendados + ' ' + msgou + ' ' + msgpagosnaopagos + ' ' + msgnecessario + '!' + '\n';		
	}

	if(!radio[1].checked && !radio[2].checked){
		msgValidacao = msgValidacao + 'Os campos' + ' ' + msgProcessamentoPagamento + ' ' + 's\u00e3o necess\u00e1rios' + '!' + '\n';
	}

	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if (document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
		
	/*if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )		
		msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';*/		
	
	var objRadioArgumentosPesquisa = document.getElementsByName('radioPesquisasFiltroPrincipal');
	
	//Verifica preenchimento de "Participante do Contrato"	
	if (document.getElementById(form.id + ':chkParticipanteContrato').checked){
		if(allTrim(document.getElementById(form.id +':txtCodigoParticipante')) == ''){
			msgValidacao = msgValidacao + msgcodigoparticipante + '!' + '\n';		
		}
	}
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' +  msgDiferenteZeros + '!' + '\n';				
		}
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' +  msgDiferenteZeros + '!' + '\n';				
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' +  msgDiferenteZeros + '!' + '\n';				
		}			
		/*if(allTrim(document.getElementById(form.id +':txtDigitoContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoConta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';	*/				
	}	
	
	// Valida Numero e valor do Pagamento
	if (objRadioArgumentosPesquisa[0].checked){
		var flagNumeroValor = 0;
		var valorPagamentoDe = replaceAll(document.getElementById(form.id +':txtValorDePagamento').value);
		var valorPagamentoAte = replaceAll(document.getElementById(form.id +':txtValorAtePagamento').value);
		
		var valorPreenchido= false;
		var numeroPreenchido = false;
		
		if( (allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == '')){
			flagNumeroValor++;
		}else{
			numeroPreenchido = true;
		}
			


		if( (((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') ||
			(valorPagamentoDe == '0')) &&
   		    ((allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == '') || 
   		    (valorPagamentoAte == '0')))){
			flagNumeroValor++;
		}else{
			valorPreenchido = true;
		}
			
		if(flagNumeroValor == 2){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgou + ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';			
		}else{

			if(numeroPreenchido && ((allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == ''))){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgnecessario + '!' + '\n';		
			}
			if(valorPreenchido && ((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') || (valorPagamentoDe == '0') || 
				                   (allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == '') || (valorPagamentoAte == '0'))){
			
   		     	msgValidacao = msgValidacao + msgcampo + ' ' + msgvalorpagamento + ' ' + msgDiferenteZeros + '!' + '\n';	
   		     }
   		   
			
		}	
	}else{
		//N�mero da Remessa
		if (objRadioArgumentosPesquisa[1].checked){
			if (allTrim(document.getElementById(form.id +':numeroRemessaFiltro')) == '')
				msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgnecessario + '!' + '\n';
			else{
				if (parseInt(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')),10) == 0 )
					msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgDiferenteZeros + '!' + '\n';
			}				
		}
		else{
			//Valida Linha Digit�vel
			if (objRadioArgumentosPesquisa[2].checked){
				if ((allTrim(document.getElementById(form.id +':linhaDigitavel1')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel2')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel3')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel4')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel5')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel6')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel7')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel8')) == '')	)	 
						msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgnecessario + '!' + '\n';	
				else{
					if( parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel1')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel2')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel3')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel4')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel5')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel6')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel7')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel8')),10) == 0 )
							msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgDiferenteZeros + '!' + '\n';
				}		
			}	
			else{
				//Favorecido
				if (objRadioArgumentosPesquisa[3].checked){
					var objRadioFavorecido = document.getElementsByName('radioFavorecidoFiltro');				
					
					if (!objRadioFavorecido[0].checked && !objRadioFavorecido[1].checked && !objRadioFavorecido[2].checked){
						msgValidacao = msgValidacao + msgradiofavorecido + '!' + '\n';	
						alert(msgValidacao);
						return false;						
					}
					
					if (objRadioFavorecido[0].checked){
						if(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';				
						else{
							if ( parseInt(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')),10) == 0 ){
								msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!' + '\n';												
							}
						}	
					}
					else if(objRadioFavorecido[1].checked){
						if(allTrim(document.getElementById(form.id +':txtInscricao')) == '' ){
							msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';	
						}else{
						
							if (parseInt(document.getElementById(form.id +':txtInscricao').value,10) == 0){
								msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgDiferenteZeros + '!' + '\n';	
							}
						}	
						if (document.getElementById(form.id +':tipoInscricao').value == null || allTrim(document.getElementById(form.id +':tipoInscricao')) == '0' )							
							msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' (' + msgfavorecido +') ' + msgnecessario + '!' + '\n';				
					}					
					else if(objRadioFavorecido[2].checked){
						if(allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						else{
							if (allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';
						}	
						if(allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						else{
							if (allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';
						}
						if(allTrim(document.getElementById(form.id +':txtContaContaCredito')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						else{
							if (parseInt(allTrim(document.getElementById(form.id +':txtContaContaCredito')),10) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgDiferenteZeros + '!' + '\n';
						}							
						/*if(allTrim(document.getElementById(form.id +':txtDigitoContaCredito')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoConta + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';*/				
							
						
					}
			  }
			
		}
	}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaCamposConsultarPagamentosConsolidado(form, msgcampo, msgnecessario, msgagendados, msgou, msgpagosnaopagos, msgdatapagamento, msgargumentopesquisa,
		msgcontadebito, msgbanco, msgagencia, msgconta, msgtiposervico, msgmodalidade, msgsituacaopagamento, msgremessa, msgnumero,digito, msgDiferenteZeros, msgrastreamentotitulos,
		msgindicadorautorizacao, msgloteinterno, msgtipolayoutarquivo){
	var msgValidacao = '';
	
	var objRdoCliente = document.getElementsByName('radioAgendadosPagosNaoPagos');
	
	if ( !objRdoCliente[1].checked && !objRdoCliente[2].checked )
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagendados + ' ' + msgou + ' ' + msgpagosnaopagos + ' ' + msgnecessario + '!' + '\n';	
	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	//Ao menos um argumento de pesquisa deve ser selecionado
/*	if(	!document.getElementById(form.id + ':chkRemessa').checked &&
		!document.getElementById(form.id + ':chkContaDebito').checked &&
		!document.getElementById(form.id + ':chkServicoModalidade').checked &&
		!document.getElementById(form.id + ':chkSituacaoMotivo').checked){		
			msgValidacao = msgValidacao + msgargumentopesquisa + '!';	
			alert(msgValidacao);
			return false;
	}*/
	
	//Valida Remessa
	if (document.getElementById(form.id + ':chkRemessa').checked){
		if (document.getElementById(form.id +':numeroRemessaFiltro').value == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgnecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgDiferenteZeros + '!' + '\n';
		}
	}
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';	
		}else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}			
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';			
		}else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}	
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';
		}	
		/*if(allTrim(document.getElementById(form.id +':txtDigitoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + digito + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}	*/	
	}
	
	//Valida Tipo de Servi�o e Modalidade
	if (document.getElementById(form.id + ':chkServicoModalidade').checked){
		if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
			
		//if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )		
			//msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
	}

	var objRadioRastreamentoTitulo = document.getElementsByName('radioRastreadosNaoRastreados');
	if (document.getElementById(form.id + ':chkRastreamentoTitulo').checked){
		if (!objRadioRastreamentoTitulo[0].checked && !objRadioRastreamentoTitulo[1].checked && !objRadioRastreamentoTitulo[2].checked)
			msgValidacao = msgValidacao + msgcampo + ' ' + msgrastreamentotitulos + ' ' + msgnecessario + '!' + '\n';
	}
	
	//Valida Situa��o e Motivo da Situa��o do Pagamento
	if (document.getElementById(form.id + ':chkSituacaoMotivo') != null && document.getElementById(form.id + ':chkSituacaoMotivo').checked){
		if ( document.getElementById(form.id +':cboSituacaoPagamento').value == null || document.getElementById(form.id +':cboSituacaoPagamento').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsituacaopagamento + ' ' + msgnecessario + '!' + '\n';	
			
	}	
	
	var objIndicadorAutorizacao = document.getElementsByName('radioIndicadorAutorizacao');
	if (document.getElementById(form.id + ':chkIndicadorAutorizacao').checked){
		if (!objIndicadorAutorizacao[0].checked && !objIndicadorAutorizacao[1].checked)
			msgValidacao = msgValidacao + msgcampo + ' ' + msgindicadorautorizacao + ' ' + msgnecessario + '!' + '\n';
	}
	
	if (document.getElementById(form.id + ':chkLoteInterno').checked){
		if(allTrim(document.getElementById(form.id +':txtLoteInterno')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgloteinterno + ' ' + msgnecessario + '!' + '\n';		
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtLoteInterno')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgloteinterno + ' ' + msgnecessario + '!' + '\n';
		}
		
		if ( document.getElementById(form.id +':cboTipoLayoutArquivo').value == null || document.getElementById(form.id +':cboTipoLayoutArquivo').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipolayoutarquivo + ' ' + msgnecessario + '!' + '\n';	
			
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validarCancelarPagamentosConsolidado(form, msgcampo, msgnecessario, msgagendados, msgou, msgpagosnaopagos, msgdatapagamento, 
		msgremessa, msgnumero, msgcontadebito, msgbanco, msgagencia, msgconta, msgtiposervico, msgmodalidade, msgdigito, msgDiferenteZeros, msgProcessamentoPagamento){
		
	var msgValidacao = '';

	var radio = document.getElementsByName('radioTipoAntecipacao');

	var objRdoCliente = document.getElementsByName('radioAgendadosPagosNaoPagos');
	
	if ( !objRdoCliente[1].checked && !objRdoCliente[2].checked ){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagendados + ' ' + msgou + ' ' + msgpagosnaopagos + ' ' + msgnecessario + '!' + '\n';	
	}
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}

	if(!radio[1].checked && !radio[2].checked){
		msgValidacao = msgValidacao + 'Os campos' + ' ' + msgProcessamentoPagamento + ' ' + 's\u00e3o necess\u00e1rios' + '!' + '\n';
	}

	//Valida Remessa
	if (document.getElementById(form.id + ':chkRemessa').checked){
		if (allTrim(document.getElementById(form.id +':numeroRemessaFiltro')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgnecessario + '!' + '\n';
		else{
			if (parseInt(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgDiferenteZeros + '!' + '\n';
		}			
	}	
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';	
		}else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';	
		}				
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';			
		}else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';	
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZeros + '!' + '\n';	
		}
		/*if(allTrim(document.getElementById(form.id +':txtDigitoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdigito + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}*/		
	}
	
	//Valida Tipo de Servi�o e Modalidade
	if (document.getElementById(form.id + ':chkServicoModalidade').checked){
		if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
			
		//if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )		
		//	msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
	}	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaSolEmissaoComprovanteFavorecido(form, msgcampo, msgnecessario, msgagendados, msgou, msgpagosnaopagos, msgdatapagamento){
	var msgValidacao = '';	
	var objRdoCliente = document.getElementsByName('radioAgendadosPagosNaoPagos');
	
	if ( !objRdoCliente[1].checked && !objRdoCliente[2].checked )
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagendados + ' ' + msgou + ' ' + msgpagosnaopagos + ' ' + msgnecessario + '!' + '\n';	
	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaConsultarContaDebito(form, msgcampo, msgnecessario,msgbanco,msgagencia,msgconta,msgcontadig, msgcontadebito){
	var msgValidacao = '';
	
	
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';	
		}			
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';			
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}
		/*if (allTrim(document.getElementById(form.id + ':txtDigitoContaDebitoCheck')) == '') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcontadig + ' (' + msgcontadebito + ') ' + msgnecessario + '!\n';			
		}	*/
		
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}

function validaConsultarContaCredito(form, msgcampo, msgnecessario,msgbanco,msgagencia,msgconta,msgcontadig,msgcontacredito){
	var msgValidacao = '';
	
	
	if (allTrim(document.getElementById(form.id + ':txtBancoContaCredito')) == "" || parseInt(allTrim(document.getElementById(form.id + ':txtBancoContaCredito')),10) == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgnecessario + '!\n';			
	}/*else{
		if(allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' + msgnecessario + '!' + '\n';
	}*/			
	if (allTrim(document.getElementById(form.id + ':txtAgenciaContaCredito')) == "" || parseInt(allTrim(document.getElementById(form.id + ':txtAgenciaContaCredito')),10) == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia  + ' (' + msgcontacredito + ') ' +  msgnecessario + '!\n';			
	}/*else{
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' + msgnecessario + '!' + '\n';
	}*/
	if (allTrim(document.getElementById(form.id + ':txtContaContaCredito')) == "" || parseInt(allTrim(document.getElementById(form.id +':txtContaContaCredito')),10) == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgnecessario + '!\n';			
	}/*else{
		if (parseInt(allTrim(document.getElementById(form.id +':txtContaContaCredito')),10) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' + msgnecessario + '!' + '\n';
	}*/
	/*if (allTrim(document.getElementById(form.id + ':txtDigitoContaCredito')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgcontadig + ' (' + msgcontacredito + ') ' +  msgnecessario + '!\n';			
	}*/
		
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}

function validaPosicaoDiariaPagAgencia(form, msgcampo, msgnecessario,msgunidade,msgmodalidade){ 
	var msgValidacao = '';
	var objRdoFiltro = document.getElementsByName('radioTipoFiltro');
	
	//Valida Tipo de Servi�o e Modalidade 
	
	//if ((document.getElementById(form.id +':cboTipoServico').value != null && 
	//	document.getElementById(form.id +':cboTipoServico').value != '0') &&
	//   (document.getElementById(form.id +':cboModalidade').value == null || 
	//   	document.getElementById(form.id +':cboModalidade').value == '0' )){
	   		
	//	msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';		
	//}
	
	if(objRdoFiltro[2].checked){
		if(document.getElementById(form.id +':txtUniOrganizacional').value == 0){
			msgValidacao = msgValidacao + ' ' + msgunidade + ' ' + '!' + '\n';
		}
	}
		
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}

function atualizaRegistroSelecionado(form){

	var existeRegistroSelecionado = false;
	for(i=0; i<form.elements.length && !existeRegistroSelecionado; i++)
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			if (form.elements[i].checked){
				existeRegistroSelecionado = true;
			}

		}	
		
	}	

	document.getElementById(form.id + ':btnDesautorizarSelecionados').disabled = !existeRegistroSelecionado;		
}

function validaPagamentoIndividualSemSituacaoMotivo(form, msgcampo, msgnecessario, msgagendados, msgou, msgpagosnaopagos, msgdatapagamento, msgargumentopesquisa,
		msgparticipante, msgcontadebito, msgbanco, msgagencia, msgconta, msgtiposervico, msgmodalidade, msgsituacaopagamento,
		msgmotivosituacao, msgnumeropagamento, msgvalorpagamento, msgremessa, msgnumero, msglinhadigitavel, msgradiofavorecido, msgcodigo, msgfavorecido,
		msginscricao, msgtipo, msgcontacredito, msgdigitoConta,msgTipoConta, msgDiferenteZero ){
	var msgValidacao = '';
	
	var objRdoCliente = document.getElementsByName('radioAgendadosPagosNaoPagos');
	
	if ( !objRdoCliente[1].checked && !objRdoCliente[2].checked )
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagendados + ' ' + msgou + ' ' + msgpagosnaopagos + ' ' + msgnecessario + '!' + '\n';	
	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	//Ao menos um argumento de pesquisa deve ser selecionado
	var objRadioArgumentosPesquisa = document.getElementsByName('radioPesquisasFiltroPrincipal');
	if( !document.getElementById(form.id + ':chkParticipanteContrato').checked &&
		!document.getElementById(form.id + ':chkContaDebito').checked &&
		!document.getElementById(form.id + ':chkServicoModalidade').checked &&
		!objRadioArgumentosPesquisa[0].checked && !objRadioArgumentosPesquisa[1].checked &&		
		!objRadioArgumentosPesquisa[2].checked && !objRadioArgumentosPesquisa[3].checked /*&&				
		!objRadioArgumentosPesquisa[4].checked */){		
			msgValidacao = msgValidacao + msgargumentopesquisa + '!';	
			alert(msgValidacao);
			return false;
	}
	
	//Verifica preenchimento de "Participante do Contrato"	
	if (document.getElementById(form.id + ':chkParticipanteContrato').checked){
		if(allTrim(document.getElementById(form.id +':txtCodigoParticipante')) == ''){
			msgValidacao = msgValidacao + msgparticipante + '!' + '\n';		
		}
	}
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZero + '!' + '\n';
		}
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZero + '!' + '\n';
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';
		}else{
			if (allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZero + '!' + '\n';
		}	
		/*if(allTrim(document.getElementById(form.id +':txtDigitoContaDebitoCheck')) == '')
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoConta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		*/			
	}	
	
	if (document.getElementById(form.id + ':chkServicoModalidade').checked){
		if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
			
		//if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )		
		//	msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
	}	
	
	// Valida Numero e valor do Pagamento
	if (objRadioArgumentosPesquisa[0].checked){
		var flagNumeroValor = 0;
		var valorPagamentoDe = replaceAll(document.getElementById(form.id +':txtValorDePagamento').value);
		var valorPagamentoAte = replaceAll(document.getElementById(form.id +':txtValorAtePagamento').value);
		
		var valorPreenchido= false;
		var numeroPreenchido = false;
		
		if( (allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == '') ){
			flagNumeroValor++;
		}else{
			numeroPreenchido = true;
		}
			


		if( (((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') ||
			(valorPagamentoDe == '0')) &&
   		    ((allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == '') || 
   		    (valorPagamentoAte == '0')))){
			flagNumeroValor++;
		}else{
			valorPreenchido = true;
		}
			
		if(flagNumeroValor == 2){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgou + ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';			
		}else{

			if(numeroPreenchido && ((allTrim(document.getElementById(form.id +':txtNumeroDePagamento')) == ''))){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgnumeropagamento + ' ' + msgnecessario + '!' + '\n';		
			}
			if(valorPreenchido && ((allTrim(document.getElementById(form.id +':txtValorDePagamento')) == '') ||
				                   (allTrim(document.getElementById(form.id +':txtValorAtePagamento')) == '') )){
			
   		     	msgValidacao = msgValidacao + msgcampo + ' ' + msgvalorpagamento + ' ' + msgnecessario + '!' + '\n';	
   		     }else{
   		     
   		     	if(valorPreenchido && (valorPagamentoDe == '0' || valorPagamentoAte == '0')){			
   		     		msgValidacao = msgValidacao + msgcampo + ' ' + msgvalorpagamento + ' ' + msgDiferenteZero + '!' + '\n';	
   		    	 }
   		     }
   		   
			
		}
		
				
				
	}else{
		//N�mero da Remessa		
		if (objRadioArgumentosPesquisa[1].checked){
			if (allTrim(document.getElementById(form.id +':numeroRemessaFiltro')) == ''){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgnecessario + '!' + '\n';
			}else{
				if (parseInt(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')),10) == 0 )
					msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgDiferenteZero + '!' + '\n';
			}
		}
		else{
			//Valida Linha Digit�vel
			if (objRadioArgumentosPesquisa[2].checked){
				if ((allTrim(document.getElementById(form.id +':linhaDigitavel1')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel2')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel3')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel4')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel5')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel6')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel7')) == '') ||
					(allTrim(document.getElementById(form.id +':linhaDigitavel8')) == ''))	 
						msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgnecessario + '!' + '\n';
				else{
					if( parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel1')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel2')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel3')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel4')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel5')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel6')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel7')),10) == 0 &&
						parseInt(allTrim(document.getElementById(form.id +':linhaDigitavel8')),10) == 0 )
							msgValidacao = msgValidacao + msgcampo + ' ' + msglinhadigitavel + ' ' + msgDiferenteZero + '!' + '\n';
				}		
			}	
			else{
				//Favorecido
				if (objRadioArgumentosPesquisa[3].checked){
					var objRadioFavorecido = document.getElementsByName('radioFavorecidoFiltro');				
					
					if (!objRadioFavorecido[0].checked && !objRadioFavorecido[1].checked && !objRadioFavorecido[2].checked){
						msgValidacao = msgValidacao + msgradiofavorecido + '!' + '\n';	
						alert(msgValidacao);
						return false;						
					}
					
					if (objRadioFavorecido[0].checked){
						if(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';				
						else{
							if (parseInt(allTrim(document.getElementById(form.id +':txtCodigoFavorecido')),10) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgfavorecido + ') ' + msgDiferenteZero + '!' + '\n';
						}	
					}
					else if(objRadioFavorecido[1].checked){
						if(allTrim(document.getElementById(form.id +':txtInscricao')) == '' ){
							msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgnecessario + '!' + '\n';	
						}else{
						
							if (parseInt(document.getElementById(form.id +':txtInscricao').value,10) == 0){
								msgValidacao = msgValidacao + msgcampo + ' ' + msginscricao + ' (' + msgfavorecido + ') ' + msgDiferenteZero + '!' + '\n';	
							}
						}
						if ( document.getElementById(form.id +':tipoInscricao')== null || allTrim(document.getElementById(form.id +':tipoInscricao')) == '0' )							
							msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' (' + msgfavorecido +') ' + msgnecessario + '!' + '\n';
					}					
					else if(objRadioFavorecido[2].checked){
						if(allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if (allTrim(document.getElementById(form.id +':txtBancoContaCredito')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontacredito + ') ' +  msgDiferenteZero + '!' + '\n';			
						}
						if(allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if (allTrim(document.getElementById(form.id +':txtAgenciaContaCredito')) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontacredito + ') ' +  msgDiferenteZero + '!' + '\n';			
						}
						if(allTrim(document.getElementById(form.id +':txtContaContaCredito')) == ''){
							msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';				
						}else{
							if (parseInt(allTrim(document.getElementById(form.id +':txtContaContaCredito')),10) == 0 )
								msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontacredito + ') ' +  msgDiferenteZero + '!' + '\n';			
						}	
					/*	if(allTrim(document.getElementById(form.id +':txtDigitoContaCredito')) == '')
							msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoConta + ' (' + msgcontacredito + ') ' +  msgnecessario + '!' + '\n';*/				
							
						
					}
			  }
			 /* else if (objRadioArgumentosPesquisa[4].checked){
		  		var objRadioBeneficiario = document.getElementsByName('radioBeneficiarioFiltro');				
	
				//Verifica se alguma op��o de consulta foi selecionada para Beneficiario		  		
				if (!objRadioBeneficiario[0].checked && !objRadioBeneficiario[1].checked){
					msgValidacao = msgValidacao + msgradiobeneficiario + '!' + '\n';	
					alert(msgValidacao);
					return false;						
				}
		  		
		  		
				if(objRadioBeneficiario[0].checked){		  		
					if(document.getElementById(form.id +':txtCodigoBeneficiario').value == '')
						msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigo + ' (' + msgbeneficiario + ') ' + msgnecessario + '!' + '\n';	
						
					if ( document.getElementById(form.id +':tipoBeneficiario').value == null || document.getElementById(form.id +':tipoBeneficiario').value == '0' )							
						msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' (' + msgbeneficiario + ') ' + msgnecessario + '!' + '\n';				
		  		}
		  		else if(objRadioBeneficiario[1].checked){
					if ( document.getElementById(form.id +':orgaoPagador').value == null || document.getElementById(form.id +':orgaoPagador').value == '0' )							
						msgValidacao = msgValidacao + msgcampo + ' ' + msgorgaopagador + ' (' + msgbeneficiario + ') ' + msgnecessario + '!' + '\n';				
		  		
		  		}
		  	}*/
		}
	}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaCamposConsultarPagamentosConsolidadoSemSituacaoMotivo(form, msgcampo, msgnecessario, msgagendados, msgou, msgpagosnaopagos, msgdatapagamento, msgargumentopesquisa,
		msgcontadebito, msgbanco, msgagencia, msgconta, msgtiposervico, msgmodalidade, msgsituacaopagamento, msgremessa, msgnumero,digito, msgDiferenteZero){
	var msgValidacao = '';
	
	var objRdoCliente = document.getElementsByName('radioAgendadosPagosNaoPagos');
	
	if ( !objRdoCliente[1].checked && !objRdoCliente[2].checked )
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagendados + ' ' + msgou + ' ' + msgpagosnaopagos + ' ' + msgnecessario + '!' + '\n';	
	
	if (document.getElementById(form.id + ':dataInicialPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataInicialPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataInicialPagamento.year')) == '' ||
		document.getElementById(form.id + ':dataFinalPagamento.day').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.day')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.month').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.month')) == ''   ||
		document.getElementById(form.id + ':dataFinalPagamento.year').value == null ||
		allTrim(document.getElementById(form.id + ':dataFinalPagamento.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatapagamento + ' ' + msgnecessario + '!' + '\n';	
	}
	
	//Ao menos um argumento de pesquisa deve ser selecionado
/*	if(	!document.getElementById(form.id + ':chkRemessa').checked &&
		!document.getElementById(form.id + ':chkContaDebito').checked &&
		!document.getElementById(form.id + ':chkServicoModalidade').checked){		
			msgValidacao = msgValidacao + msgargumentopesquisa + '!';	
			alert(msgValidacao);
			return false;
	}*/
	
	//Valida Remessa
	if (document.getElementById(form.id + ':chkRemessa').checked){
		if (document.getElementById(form.id +':numeroRemessaFiltro').value == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgnecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':numeroRemessaFiltro')),10) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgremessa + ' ' + msgDiferenteZero + '!' + '\n';
		}
	}
	
	//Verifica preenchimento de "Conta de D�bito" - Argumentos de Pesquisa
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == ''){ 
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';	
		}else{
			if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' (' + msgcontadebito + ') ' + msgDiferenteZero + '!' + '\n';
		}			
		if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';			
		}else{
			if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' (' + msgcontadebito + ') ' + msgDiferenteZero + '!' + '\n';
		}
		if(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}else{
			if (allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == 0 )
				msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' (' + msgcontadebito + ') ' + msgDiferenteZero + '!' + '\n';		
		}
		/*if(allTrim(document.getElementById(form.id +':txtDigitoContaDebitoCheck')) == ''){
			msgValidacao = msgValidacao + msgcampo + ' ' + digito + ' (' + msgcontadebito + ') ' + msgnecessario + '!' + '\n';		
		}		*/
	}
	
	//Valida Tipo de Servi�o e Modalidade
	if (document.getElementById(form.id + ':chkServicoModalidade').checked){
		if ( document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0' )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtiposervico + ' ' + msgnecessario + '!' + '\n';	
			
		//if ( document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0' )		
		//	msgValidacao = msgValidacao + msgcampo + ' ' + msgmodalidade + ' ' + msgnecessario + '!' + '\n';	
	}	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function replaceAll(string){
	while (string.indexOf(".") > 0){
		string = string.replace(".","");
	}
	string = parseFloat(string.replace(",", ""));
	return string;
}	

function validarProxCampoIdentClienteContratoFlag(form, flagPesquisa){
	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCnpjFilial = document.getElementById(form.id + ':txtFilial');
	var campoCnpjControle = document.getElementById(form.id + ':txtControle');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoCpfControle = document.getElementById(form.id + ':txtControleCpf');
	var campoNomeRazao = document.getElementById(form.id + ':txtNomeRazaoSocial');
	var campoBanco = document.getElementById(form.id + ':txtBanco');
	var campoAgencia = document.getElementById(form.id + ':txtAgencia');
	var campoConta = document.getElementById(form.id + ':txtConta');
	
	if(objRdoCliente[0].checked){
		campoCnpj.disabled = false;
		campoCnpj.focus();
		campoCnpjFilial.disabled = false;
		campoCnpjControle.disabled = false;
		
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCpf.value = '';
		campoCpfControle.value = '';
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[1].checked){
		campoCpf.disabled = false;
		campoCpf.focus();
		campoCpfControle.disabled = false;
	
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[2].checked){
		campoNomeRazao.disabled = false;
		campoNomeRazao.focus();
			
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[3].checked){
		campoBanco.disabled = false;
		campoAgencia.disabled = false;
		campoConta.disabled = false;
		campoBanco.value = 237;
		campoAgencia.focus();
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else{
		document.getElementById(form.id + ':btoConsultarCliente').disabled = true;
		return false;
	}
}

function validaCamposConsultarExclusaoLotePagamento(form, msgCampo, msgNecessario, msgLoteInterno, msgTipoLayoutArquivo, msgSituacaoSolicitacao, msgDiferenteZeros){
	var msgValidacao = '';
	
	if (document.getElementById(form.id + ':chkLoteInterno').checked){
		if (document.getElementById(form.id +':txtLoteInterno').value == ''){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgLoteInterno + ' ' + msgNecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtLoteInterno')),10) == 0 )
				msgValidacao = msgValidacao + msgCampo + ' ' + msgLoteInterno + ' ' + msgDiferenteZeros + '!' + '\n';
		}
			
	}
	
	if (document.getElementById(form.id + ':chkSituacaoSolicitacao').checked){
		
		if(document.getElementById(form.id +':cboSituacaoSolicitacao').value == null || document.getElementById(form.id +':cboSituacaoSolicitacao').value == '0'){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgSituacaoSolicitacao + ' ' + msgNecessario + '!' + '\n';
		}
			
	}
	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;
}	

function validaCampoLote(form, msgCampo, msgNecessario, msgLoteInterno, msgTipoLayoutArquivo, msgSituacaoSolicitacao, msgDiferenteZeros){
	var msgValidacao = '';
	
		if (document.getElementById(form.id +':txtLoteInterno').value == ''){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgLoteInterno + ' ' + msgNecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtLoteInterno')),10) == 0 )
				msgValidacao = msgValidacao + msgCampo + ' ' + msgLoteInterno + ' ' + msgDiferenteZeros + '!' + '\n';
		}
		
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;
}	

function validaCampoLotePostergar(form, msgCampo, msgNecessario, msgLoteInterno, msgTipoLayoutArquivo, msgSituacaoSolicitacao, msgAnteciparPostergar ,msgDiferenteZeros){
	var msgValidacao = '';
	
	if (document.getElementById(form.id +':txtLoteInterno').value == ''){
		msgValidacao = msgValidacao + msgCampo + ' ' + msgLoteInterno + ' ' + msgNecessario + '!' + '\n';
	}else{
		if (parseInt(allTrim(document.getElementById(form.id +':txtLoteInterno')),10) == 0 )
			msgValidacao = msgValidacao + msgCampo + ' ' + msgLoteInterno + ' ' + msgDiferenteZeros + '!' + '\n';
	}
	
	var objRadioTipoAntecipacao = document.getElementsByName('radioTipoAntecipacao');
	
	if(!objRadioTipoAntecipacao[1].checked && !objRadioTipoAntecipacao[2].checked){
		msgValidacao = msgValidacao + msgCampo + ' ' + msgAnteciparPostergar + ' ' + msgNecessario + '!' + '\n';
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;
}	

function validaCamposInclusaoLotePagamento(form, msgCampo, msgNecessario, msgLoteInterno, msgTipoLayoutArquivo, msgDataPagamento, msgTitpoServico, msgModalidade, msgBanco, msgAgencia, msgConta, msgDiferenteZeros){
	var msgValidacao = '';
	
	if (document.getElementById(form.id +':txtLoteInterno').value == ''){
		msgValidacao = msgValidacao + msgCampo + ' ' + msgLoteInterno + ' ' + msgNecessario + '!' + '\n';
	}else{
		if (parseInt(allTrim(document.getElementById(form.id +':txtLoteInterno')),10) == 0 )
			msgValidacao = msgValidacao + msgCampo + ' ' + msgLoteInterno + ' ' + msgDiferenteZeros + '!' + '\n';
	}
	
	if(document.getElementById(form.id +':cboTipoLayoutArquivo').value == null || document.getElementById(form.id +':cboTipoLayoutArquivo').value == '0'){
		msgValidacao = msgValidacao + msgCampo + ' ' + msgTipoLayoutArquivo + ' ' + msgNecessario + '!' + '\n';
	}
	
	if (document.getElementById(form.id + ':chkDataPagamento').checked){
		if(document.getElementById(form.id +':dataInicialPagamento').value == null){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgDataPagamento + 'In�cio ' + + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById(form.id +':dataFinalPagamento').value == null){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgDataPagamento + 'Fim ' + + ' ' + msgNecessario + '!' + '\n';
		}
			
	}
	
	if (document.getElementById(form.id + ':chkTipoServico').checked){
		
		if(document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0'){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgTitpoServico + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0'){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgModalidade + ' ' + msgNecessario + '!' + '\n';
		}
			
	}
	
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		
		if (document.getElementById(form.id +':txtBancoPesquisa').value == ''){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgBanco + ' ' + msgNecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtBancoPesquisa')),10) == 0 )
				msgValidacao = msgValidacao + msgCampo + ' ' + msgBanco + ' ' + msgDiferenteZeros + '!' + '\n';
		}
		
		if (document.getElementById(form.id +':txtAgenciaPesquisa').value == ''){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgAgencia + ' ' + msgNecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtAgenciaPesquisa')),10) == 0 )
				msgValidacao = msgValidacao + msgCampo + ' ' + msgAgencia + ' ' + msgDiferenteZeros + '!' + '\n';
		}
		
		if (document.getElementById(form.id +':txtContaPesquisa').value == ''){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgConta + ' ' + msgNecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaPesquisa')),10) == 0 )
				msgValidacao = msgValidacao + msgCampo + ' ' + msgConta + ' ' + msgDiferenteZeros + '!' + '\n';
		}
			
	}
	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;
}	

function validaCamposInclusaoLotePagamentoAntecipa(form, msgCampo, msgNecessario, msgTipoAntecipacao, msgNovaDataAgendamento, msgLoteInterno, msgTipoLayoutArquivo, msgDataPagamento, msgTitpoServico, msgModalidade, msgBanco, msgAgencia, msgConta, msgDiferenteZeros){
	var msgValidacao = '';
	
	var objRadioTipoAntecipacao = document.getElementsByName('radioTipoAntecipacao');
	
	if(!objRadioTipoAntecipacao[1].checked && !objRadioTipoAntecipacao[2].checked){
		msgValidacao = msgValidacao + msgCampo + ' ' + msgTipoAntecipacao + ' ' + msgNecessario + '!' + '\n';
	}else{
		if(document.getElementById(form.id +':novaDataAgendamento').value == null){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgNovaDataAgendamento + ' ' + msgDiferenteZeros + '!' + '\n';
		}
	}
	
	if (document.getElementById(form.id +':txtLoteInterno').value == ''){
		msgValidacao = msgValidacao + msgCampo + ' ' + msgLoteInterno + ' ' + msgNecessario + '!' + '\n';
	}else{
		if (parseInt(allTrim(document.getElementById(form.id +':txtLoteInterno')),10) == 0 )
			msgValidacao = msgValidacao + msgCampo + ' ' + msgLoteInterno + ' ' + msgDiferenteZeros + '!' + '\n';
	}
	
	if(document.getElementById(form.id +':cboTipoLayoutArquivo').value == null || document.getElementById(form.id +':cboTipoLayoutArquivo').value == '0'){
		msgValidacao = msgValidacao + msgCampo + ' ' + msgTipoLayoutArquivo + ' ' + msgNecessario + '!' + '\n';
	}
	
	if (document.getElementById(form.id + ':chkDataPagamento').checked){
		if(document.getElementById(form.id +':dataInicialPagamento').value == null){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgDataPagamento + 'In�cio ' + + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById(form.id +':dataFinalPagamento').value == null){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgDataPagamento + 'Fim ' + + ' ' + msgNecessario + '!' + '\n';
		}
			
	}
	
	if (document.getElementById(form.id + ':chkTipoServico').checked){
		
		if(document.getElementById(form.id +':cboTipoServico').value == null || document.getElementById(form.id +':cboTipoServico').value == '0'){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgTitpoServico + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById(form.id +':cboModalidade').value == null || document.getElementById(form.id +':cboModalidade').value == '0'){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgModalidade + ' ' + msgNecessario + '!' + '\n';
		}
			
	}
	
	if (document.getElementById(form.id + ':chkContaDebito').checked){
		
		if (document.getElementById(form.id +':txtBancoPesquisa').value == ''){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgBanco + ' ' + msgNecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtBancoPesquisa')),10) == 0 )
				msgValidacao = msgValidacao + msgCampo + ' ' + msgBanco + ' ' + msgDiferenteZeros + '!' + '\n';
		}
		
		if (document.getElementById(form.id +':txtAgenciaPesquisa').value == ''){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgAgencia + ' ' + msgNecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtAgenciaPesquisa')),10) == 0 )
				msgValidacao = msgValidacao + msgCampo + ' ' + msgAgencia + ' ' + msgDiferenteZeros + '!' + '\n';
		}
		
		if (document.getElementById(form.id +':txtContaPesquisa').value == ''){
			msgValidacao = msgValidacao + msgCampo + ' ' + msgConta + ' ' + msgNecessario + '!' + '\n';
		}else{
			if (parseInt(allTrim(document.getElementById(form.id +':txtContaPesquisa')),10) == 0 )
				msgValidacao = msgValidacao + msgCampo + ' ' + msgConta + ' ' + msgDiferenteZeros + '!' + '\n';
		}
			
	}
	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;
}	

