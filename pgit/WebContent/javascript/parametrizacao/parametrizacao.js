function setColorBad(el) {
  if (el.style) el.style.backgroundColor = "#ffaaaa";
}

function setColorGood(el) {
  if (el.style) el.style.backgroundColor = "#fffbde";
}

function validaConsulta(){
	var tipoParametro = document.getElementById('formParam:selCodigoTipoParametro').value;

	if(tipoParametro == 0){
		alert('Selecione um par\xE2metro para consulta!');
		return false;
	}
	return true;
}

function validarIncluirParametro(){
	if(trim(document.getElementById('formParam:txtCodigoParametro').value) == ""){
		alert("O campo C\u00F3digo n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtCodigoParametro').focus();
		return false;
	}else if(document.getElementById('formParam:txtTextoMinimoData.day') != null
		&& (trim(document.getElementById('formParam:txtTextoMinimoData.day').value) == ""
			 || trim(document.getElementById('formParam:txtTextoMinimoData.month').value) == ""
			 || trim(document.getElementById('formParam:txtTextoMinimoData.year').value) == "")){
		alert("O campo Valor n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtTextoMinimoData.day').focus();
		return false;
	}else if(document.getElementById('formParam:txtTextoMinimoTexto') != null
			&& trim(document.getElementById('formParam:txtTextoMinimoTexto').value) == ""){
		alert("O campo Valor n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtTextoMinimoTexto').focus();
		return false;
	}else if(document.getElementById('formParam:txtTextoMinimoInteiro') != null
			&& trim(document.getElementById('formParam:txtTextoMinimoInteiro').value) == ""){
		alert("O campo Valor n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtTextoMinimoInteiro').focus();
		return false;
	}else if(document.getElementById('formParam:txtTextoMinimoValor') != null
			&& trim(document.getElementById('formParam:txtTextoMinimoValor').value) == ""){
		alert("O campo Valor n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtTextoMinimoValor').focus();
		return false;
	}else if(document.getElementById('formParam:txtTextoMinimoIndice') != null
			&& trim(document.getElementById('formParam:txtTextoMinimoIndice').value) == ""){
		alert("O campo Valor n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtTextoMinimoIndice').focus();
		return false;
	}else if(trim(document.getElementById('formParam:txtDataVigenciaParametro.day').value) == ""
			 || trim(document.getElementById('formParam:txtDataVigenciaParametro.month').value) == ""
			 || trim(document.getElementById('formParam:txtDataVigenciaParametro.year').value) == ""){
		alert("O campo Data de Vig\u00EAncia n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtDataVigenciaParametro.day').focus();
		return false;
	}else{
		return true;
	}
}

function validarIncluirTipoParametro(){
	if(trim(document.getElementById('formParam:txtCodigoTipoParametro').value) == ""){
		alert("O campo C\u00F3digo n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtCodigoTipoParametro').focus();
		return false;
	}else if(trim(document.getElementById('formParam:txtDescricaoTipoParametro').value) == ""){
		alert("O campo Descri\u00E7\u00E3o n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtDescricaoTipoParametro').focus();
		return false;
	}else if(trim(document.getElementById('formParam:txtInicioVigencia.day').value) == ""
			 || trim(document.getElementById('formParam:txtInicioVigencia.month').value) == ""
			 || trim(document.getElementById('formParam:txtInicioVigencia.year').value) == ""){
		alert("O campo Data de Vig\u00EAncia n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtInicioVigencia.day').focus();
		return false;	
	}else if(trim(document.getElementById('formParam:selCodigoTipoValor').value) == ""){
		alert("Selecione um Tipo de Valor!");
		document.getElementById('formParam:selCodigoTipoValor').focus();
		return false;
	}else{
		return true;
	}
}

function validarAlterarTipoParametro(){
	if(document.getElementById('formParam:txtDescricaoTipoParametro').value == ""){
		alert("O campo Descri\u00E7\u00E3o n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtDescricaoTipoParametro').focus();
		return false;
	}else if(trim(document.getElementById('formParam:txtInicioVigencia.day').value) == ""
			 || trim(document.getElementById('formParam:txtInicioVigencia.month').value) == ""
			 || trim(document.getElementById('formParam:txtInicioVigencia.year').value) == ""){
		alert("O campo Data de Vig\u00EAncia n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtInicioVigencia.day').focus();
		return false;
	}else{
		return true;
	}
}

function validarAlterarParametro(){
	if(document.getElementById('formParam:txtTextoMinimoData.day') != null
		&& (trim(document.getElementById('formParam:txtTextoMinimoData.day').value) == ""
			 || trim(document.getElementById('formParam:txtTextoMinimoData.month').value) == ""
			 || trim(document.getElementById('formParam:txtTextoMinimoData.year').value) == "")){
		alert("O campo Valor n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtTextoMinimoData.day').focus();
		return false;
	}else if(document.getElementById('formParam:txtTextoMinimoTexto') != null
			&& trim(document.getElementById('formParam:txtTextoMinimoTexto').value) == ""){
		alert("O campo Valor n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtTextoMinimoTexto').focus();
		return false;
	}else if(document.getElementById('formParam:txtTextoMinimoInteiro') != null
			&& trim(document.getElementById('formParam:txtTextoMinimoInteiro').value) == ""){
		alert("O campo Valor n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtTextoMinimoInteiro').focus();
		return false;
	}else if(document.getElementById('formParam:txtTextoMinimoValor') != null
			&& trim(document.getElementById('formParam:txtTextoMinimoValor').value) == ""){
		alert("O campo Valor n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtTextoMinimoValor').focus();
		return false;
	}else if(document.getElementById('formParam:txtTextoMinimoIndice') != null
			&& trim(document.getElementById('formParam:txtTextoMinimoIndice').value) == ""){
		alert("O campo Valor n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtTextoMinimoIndice').focus();
		return false;
	}else if(trim(document.getElementById('formParam:txtDataVigenciaParametro.day').value) == ""
			 || trim(document.getElementById('formParam:txtDataVigenciaParametro.month').value) == ""
			 || trim(document.getElementById('formParam:txtDataVigenciaParametro.year').value) == ""){
		alert("O campo Data de Vig\u00EAncia n\u00E3o pode ser branco!");
		document.getElementById('formParam:txtDataVigenciaParametro.day').focus();
		return false;
	}else{
		return true;
	}
}