function validaCamposConsultaCliente(form, msgcampo, msgnecessario, msgcnpj, msgcpf, msgnome, msgbanco, msgagencia, msgconta){

	var msgValidacao = '';

	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');

	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById(form.id + ':txtCnpj')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';
			
		} else {
			if (allTrim(document.getElementById(form.id +':txtFilial'))== "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';					
			} else {
				if (allTrim(document.getElementById(form.id + ':txtControle')) == ""){
					msgValidacao = msgValidacao +   msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';				
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById(form.id +':txtCpf')) == "") {
			msgValidacao = msgValidacao +   msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
			
		} else {
			if(allTrim(document.getElementById(form.id +':txtControleCpf')) == "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
				
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById(form.id +':txtNomeRazaoSocial')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnome + ' ' + msgnecessario + '!\n';
			
		}
	} else if (objRdoCliente[3].checked) {
		if (allTrim(document.getElementById(form.id +':txtBanco')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '!\n';
				
		} 
		if(allTrim(document.getElementById(form.id +':txtAgencia')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '!\n';
					
		}	
		if(allTrim(document.getElementById(form.id +':txtConta')) == "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgconta + ' ' + msgnecessario + '!\n';					
		
		}
		
	} 
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;						
}

function validaCamposConsultaParticipante(form, msgcampo, msgnecessario, msgcnpj, msgcpf, msgbanco, msgagencia, msgconta){

	var msgValidacao = '';

	var objRdoCliente = document.getElementsByName('radioIncluir');

	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById(form.id + ':txtCnpj')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';
			
		} else {
			if (allTrim(document.getElementById(form.id +':txtFilial'))== "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';					
			} else {
				if (allTrim(document.getElementById(form.id + ':txtControle')) == ""){
					msgValidacao = msgValidacao +   msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';				
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById(form.id +':txtCpf')) == "") {
			msgValidacao = msgValidacao +   msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
			
		} else {
			if(allTrim(document.getElementById(form.id +':txtControleCpf')) == "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
				
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById(form.id +':txtBanco')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '!\n';
				
		} 
		if(allTrim(document.getElementById(form.id +':txtAgencia')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '!\n';
					
		}	
		if(allTrim(document.getElementById(form.id +':txtConta')) == "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgconta + ' ' + msgnecessario + '!\n';					
		
		}
	} 
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;						
}

function validaCamposConsultaIncParticipante(form, msgcampo, msgnecessario, msgpointer, msgcpf, msgfornec, msgbanco, msgagencia, msgconta) {
	var msgValidacao = '';
	
	if (allTrim(document.getElementById(form.id + ':txtContaPointer')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgpointer + ' ' + msgnecessario + '!\n';
	} 
	if (allTrim(document.getElementById(form.id + ':txtCpf')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
	}
	if (allTrim(document.getElementById(form.id + ':txtRazao')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgfornec + ' ' + msgnecessario + '!\n';
	}
	if (allTrim(document.getElementById(form.id + ':txtBanco')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '!\n';
	}
	if (allTrim(document.getElementById(form.id + ':txtAgencia')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '!\n';
	}
	if (allTrim(document.getElementById(form.id + ':txtConta')) == "") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgnecessario + '!\n';
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}
	return true;
}