/*
   File: funcoesValida.js

   Fun��es gen�ricas de valida��o.
   Todas as fun��es deste arquivo mostram um aviso caso a valida��o falhe.
*/

/*
   Function: setColorBad

   Altera a cor de fundo de algum componente Utilizado pelas outras fun��es.

   Par�metros:

      el - Componente a ser alterado.

   Retorno:

      nada.
*/
function setColorBad(el) {
  if (el.style) el.style.backgroundColor = "#ffaaaa";
}

/*
   Function: setColorGood

   Altera a cor de fundo de algum componente. Utilizado pelas outras fun��es.

   Par�metros:

      el - Componente a ser alterado.

   Retorno:

      nada.
*/
function setColorGood(el) {
  if (el.style) el.style.backgroundColor = "white";
}


/*
   Function: fverEmail(obj)
   Verifica o campo EMAIL sea v�lida.
   Par�metros:
      obj - Nome a ser apresentado caso o valor n�o passe na valida��o.
   Retorno:
      *true* caso o campo esteja corretamente formatado.
*/
function fverEmail(obj){
	a=obj.value;
	var err=0;
	if (a.indexOf('@')!=-1){
		if (a.substring(0,1)=='@')
			err=1;	
		if (a.indexOf(' ')!=-1)
			err=1;	
		if (a.indexOf('.')==-1)		
			err=1;
	} else {	
		err=1;
	}//se erro 
		
	if (err==1){
		alert('O e-mail est� incorreto');
		return false;
	}
}


/*
   Function: isNumber

   Verifica se o campo est� preenchido com um n�mero.
   Pode ter v�rgula e sinal de menos.

   Par�metros:

      campo - Ponteiro para um campo de formul�rio.
      nome - Nome a ser apresentado caso o valor n�o passe na valida��o.

   Retorno:

      *true* caso o campo contenha somente d�gitos, v�rgula e sinal de menos.

   Veja tamb�m:

      <isStrictNumber>
*/
function isNumber(campo, nome)
{
	valor = campo.value.toLowerCase();
	RefString = "0123456789,-";

	if (valor.length < 1)
	{
       alert("O campo " + nome + " deve ser preenchido.");
       setColorBad(campo);
       return (false);
	
	}	
	for (var i = 0; i < valor.length; i++)
	{
		var ch = valor.substr(i, 1)
		var a = RefString.indexOf(ch, 0)
		if (a == -1)		
		{			
			alert ("Digite somente n�meros, v�rgula ou sinal de menos.");	
			campo.focus();	
            setColorBad(campo);
            return (false);
		}
	}
    setColorGood(campo);
    return(true);
}

/*
   Function: isStrictNumber

   Verifica se o campo est� preenchido com um n�mero.

   Par�metros:

      campo - Ponteiro para um campo de formul�rio.
      nome - Nome a ser apresentado caso o valor n�o passe na valida��o.

   Retorno:

      *true* caso o campo contenha somente d�gitos.

   Veja tamb�m:

      <isNumber>
*/
function isStrictNumber(campo, nome)
{
    valor = campo.value.toLowerCase();
	RefString = "0123456789";

	if (valor.length < 1)
	{
        alert("O campo " + nome + " deve ser preenchido.");
        campo.focus();	
        setColorBad(campo);
        return (false);
	} 
	for (var i = 0; i < valor.length; i++)
	{
		var ch = valor.substr(i, 1);
		var a = RefString.indexOf(ch, 0);
		if (a == -1)		
		{			
        alert ("O campo " + nome + " deve ter somente n�meros.");	
        campo.focus();	
        setColorBad(campo);
        return (false);
		}
	}
    setColorGood(campo);
    return(true);
}

/*
   Function: isAlpha

   Verifica se o campo est� preenchido somente com caracteres alfanum�ricos.
   Isto �: 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ

   Par�metros:

      campo - Ponteiro para um campo de formul�rio.
      nome - Nome a ser apresentado caso o valor n�o passe na valida��o.

   Retorno:

      *true* caso o campo contenha somente alfanum�ricos
*/
function isAlpha(campo, nome)
{
    valor = campo.value.toLowerCase();
	RefString = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";

	if (valor.length < 1)
	{
       alert("O campo " + nome + " deve ser preenchido.");
       setColorBad(campo);
       return (false);
	} 
	for (var i = 0; i < valor.length; i++)
	{
		var ch = valor.substr(i, 1);
		var a = RefString.indexOf(ch, 0);
		if (a == -1)		
		{			
			alert ("O campo " + nome + " deve ser alfanum�rico.");	
			campo.focus();	
             setColorBad(campo);
            return (false);
		}
	}	
    setColorGood(campo);
    return(true);
}

/*
   Function: isAlphaConta

   Verifica se o campo est� preenchido somente com caracteres alfanum�ricos.
   Isto �: 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ

   Par�metros:

      campo - Ponteiro para um campo de formul�rio.
      nome - Nome a ser apresentado caso o valor n�o passe na valida��o.

   Retorno:

      *true* caso o campo contenha somente alfanum�ricos
*/
function isAlphaConta(campo, nome)
{
    valor = campo.value.toLowerCase();
	RefString = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	if (valor.length < 1)
	{
       alert("O campo " + nome + " deve ser preenchido.");
       setColorBad(campo);
       return (false);
	} 
	for (var i = 0; i < valor.length; i++)
	{
		var ch = valor.substr(i, 1);
		var a = RefString.indexOf(ch, 0);
		if (a == -1)		
		{			
			alert ("O campo " + nome + " deve ser alfanum�rico.");	
			campo.focus();	
             setColorBad(campo);
            return (false);
		}
	}	
    setColorGood(campo);
    return(true);
}


/*
   Function: validaTamanho

   Verifica se o comprimento do campo est� dentro de um determinado limite.

   Par�metros:

      campo - Ponteiro para um campo de formul�rio.
      nome - Nome a ser apresentado caso o valor n�o passe na valida��o.
      comprimentoMinimo - limite inferior do comprimento.
      comprimentoMaximo - limite superior do comprimento.

   Retorno:

      *true* caso o comprimento do campo esteja dentro dos limites estipulados.
*/
function validaTamanho(campo, nome, comprimentoMinimo, comprimentoMaximo)
{
    valor = campo.value.toLowerCase();  
    if (valor.length < 1) {
       alert("O campo " + nome + " deve ser preenchido.");
        setColorBad(campo);
        return false;
    }
    if (valor.length < comprimentoMinimo)
    {
        alert ("O campo " + nome + " deve ter no m�nimo " + comprimentoMinimo + " caracteres."); 
        campo.focus();          
        setColorBad(campo);
        return false;
    }
    if (valor.length > comprimentoMaximo) {
        alert ("O campo " + nome + " deve ter no m�ximo " + comprimentoMaximo + " caracteres."); 
        campo.focus();          
        setColorBad(campo);
        return false;
    }
    setColorGood(campo);
    return true;
}

/*
   Function: validaValor

   Verifica se o valor digitado est� na forma ####,## (obriga a ter v�rgula e dois d�gitos de centavos).

   Par�metros:

      campo - Ponteiro para um campo de formul�rio.
      nome - Nome a ser apresentado caso o valor n�o passe na valida��o.

   Retorno:

      *true* caso o formato esteja correto.
*/
function validaValor(campo, nome)
{
    valor = campo.value;
    posV = valor.indexOf(',');
    RefString = "0123456789,.";
    if ((valor.length < 4) || (posV != (valor.length - 3))) {
       alert("O campo " + nome + " deve estar na forma 999,99 (verifique os centavos).");
       campo.focus();
       setColorBad(campo);
       return false;
    }
    
    for (var i = 0; i < valor.length; i++)
    {
        if (i == posV) {
            i++;
        }
        var ch = valor.substr(i, 1)
        var a = RefString.indexOf(ch, 0)
        if (a == -1)        
        {           
            alert ("O campo " + nome + " deve ser um valor v�lido.");
            campo.focus();  
            setColorBad(campo);
            return (false);
        }
    }
    setColorGood(campo);
    return true;
}

/*
   Function: validaTaxa

   Verifica se a taxa digitada est� na forma #####,#### (obriga a ter v�rgula e 4 d�gitos de centavos).

   Par�metros:

      campo - Ponteiro para um campo de formul�rio.
      nome - Nome a ser apresentado caso o valor n�o passe na valida��o.

   Retorno:

      *true* caso o formato esteja correto.
*/
function validaTaxa(campo, nome)
{
    valor = campo.value;
    posV = valor.indexOf(',');
    if ((valor.length < 6) || (posV != (valor.length - 5))) {
        alert("O campo " + nome + " deve estar na forma 9999,9999 (4 d�gitos depois da v�rgula).");
        campo.focus();
        setColorBad(campo);
        return false;
    }
    
    for (var i = 0; i < valor.length; i++)
    {
        if (i == posV) {
            i++;
        }
        var ch = valor.substr(i, 1)
        var a = RefString.indexOf(ch, 0)
        if (a == -1)        
        {           
            alert ("O campo " + nome + " deve ser um valor v�lido de 4 casas depois da v�rgula.");
            campo.focus();
            setColorBad(campo);
            return (false);
        }
    }
    setColorGood(campo);
    return true;
}

/**
* Funcao responsavel por restringir qualquer caracter que n�o seja
* um numero
*
* Parameters
*
**/
function onlyNum() {
	if (document.all) {// Internet Explorer
		var tecla = event.keyCode;
	} else if(document.layers) {// Nestcape
		var tecla = event.which;
	}
	if (tecla > 47 && tecla < 58) {// numeros de 0 a 9
		return true;
	}	else {
		event.keyCode = 0;
		return false;
	}
}

function onlyNumValor() {
	if (document.all) {// Internet Explorer
		var tecla = event.keyCode;
	} else if(document.layers) {// Nestcape
		var tecla = event.which;
	}
	if (tecla > 47 && tecla < 58) {// numeros de 0 a 9
		return true;
	}	else {
		if (tecla != 8 && tecla != 44){ // backspace ou virgula
			event.keyCode = 0;
			return false;
		} else {
			return true;
		}
	}
}

function cleanClipboard() {
	clipboardData.clearData();
}

function validaRadioSelecionado(rdoName) {
	var objRdo = document.getElementsByName(rdoName);
	if (objRdo != null) {
		for (var i = 0; i < objRdo.length; i++) {
			if (objRdo[i].checked) {
				return true;
			}
		}
	}
	return false;
}

function validarCampoData(data) {
	var objDataDay = document.getElementById(data + '.day');
	var objDataMonth = document.getElementById(data + '.month');
	var objDataYear = document.getElementById(data + '.year');
	return !(isEmpty(objDataDay.value) || isEmpty(objDataMonth.value) || isEmpty(objDataYear.value));
}

function validarCampoNumerosLetras(){
	var tecla = event.keyCode;
	var retorno = false;

	var codigosValidos = new Array("48","49","50","51","52","53","54","55","56","57",
								   "65","66","67","68","69","70","71","72","73","74",
								   "75","76","77","78","79","80","81","82","83","84",
								   "85","86","87","88","89","90","97","98","99","100",
								   "101","102","103","104","105","106","107","108","109",
								   "110","111","112","113","114","115","116","117","118",
								   "119","120","121","122","199","231");

	for (var i = 0; i < codigosValidos.length; i++){
		if (tecla == codigosValidos[i]) {
			retorno = true;
			break;
		}
	}

	if(retorno == false){
		window.event.cancelBubble = true;
		window.event.returnValue = false;
		return false;
	}

	return true;
}

function verificaEmailValido(campo) {
	email=campo.value;

	if(email == ''){
		return false;
	}

	var erro=0;

	if (email.indexOf('@')!=-1){
		if (email.substring(0,1)=='@')
			erro=1;     
		if (email.indexOf(' ')!=-1)
			erro=1;     
		if (email.indexOf('.')==-1)        
			erro=1;
	} else {    
		erro=1;
	}//se erro 

	if (erro==1){
		alert('Endereço de email inválido');
		campo.focus();
		return false;
	}
}

/**
 * Valida o preenchimento do mes e do ano.
 * @param campoMesAno
 * @return
 */
function validarMesAno(campoMesAno) {
	var regexMesAno = /^([1][0-2]|[0][1-9]|[1-9])\/(19|20)\d{2}$/;
	if (campoMesAno.value != '' && !regexMesAno.test(campoMesAno.value)) {
		alert('Data inv\u00E1lida.');
		campoMesAno.value = '';
		campoMesAno.focus();
	}
}