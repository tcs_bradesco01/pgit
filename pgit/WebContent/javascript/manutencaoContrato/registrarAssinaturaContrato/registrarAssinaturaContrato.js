function mensagemBtnConfirma(msg1, msg2) {
	if(confirm(msg1)){
		alert(msg2);
	}
}

function checaCampoObrigatorioIncluir(form, msgcampo, msgnecessario, msgproduto, msgprioridade, msgdatainicio){
		var campos, hidden;	
		campos = '';
	    var dataValida = true;
	    
		for(i=0; i<form.elements.length; i++){
		
			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:hiddenObrigatoriedade'){
				hidden = form.elements[i];
			}
		
			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:tipoCompromisso'){
				if (form.elements[i].value == ''|| form.elements[i].value == null){
					campos = campos + msgcampo + ' ' + msgproduto + ' ' + msgnecessario + '\n';
				}
			}

			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:prioridade'){
				if (form.elements[i].value == '' || form.elements[i].value == null){
					campos = campos + msgcampo + ' ' + msgprioridade + ' ' + msgnecessario + '\n';
				}
			}
			
			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:dataVigencia.day'){
				if (form.elements[i].value == '' || form.elements[i].value == null)	
					dataValida = false;
			}		
			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:dataVigencia.month'){
				if (form.elements[i].value == '' || form.elements[i].value == null)	
					dataValida = false;
			}		
			if (form.elements[i].id == 'incluirManterPrioridadeCreditoTipoCompromissoForm:dataVigenciacia.year'){
				if (form.elements[i].value == '' || form.elements[i].value == null)	
					dataValida = false;
			}					


		}
		
		if (dataValida == false) 
			campos = campos + msgcampo + ' ' + msgdatainicio + ' ' + msgnecessario + '\n';
		
		if (campos != ''){
			alert(campos);
			hidden.value  ='F';
			return false;
		}else{
			hidden.value  ='T';
			return true;
		}
}

function checaCampoObrigatorio(form, msgcampo, msgnecessario, msgDataAssinatura, msgMotivoAtivacao){
		var campos, hidden;	
		campos = '';
	    var dataValidaFim = true;	    
	    
		for(i=0; i<form.elements.length; i++){
		
			if (form.elements[i].id == 'registrarAssinaturaContrato:hiddenObrigatoriedade'){
				hidden = form.elements[i];
			}
			
//			if (form.elements[i].id == 'registrarAssinaturaContrato:numContratoFisico'){
//				if (form.elements[i].value == null || allTrim(form.elements[i]) == 0){
//					campos = campos + msgcampo + ' ' + msgMotivoAtivacao + ' ' + msgnecessario + '\n';
//				}
//			}
//			
			
		    //Valida��o Data de Assinatura		
			
			if (form.elements[i].id == 'registrarAssinaturaContrato:dataAssinatura.day'){
				if (form.elements[i].value == null || allTrim(form.elements[i]) == ''){	
			
					dataValidaFim = false;
				}
			}		
			if (form.elements[i].id == 'registrarAssinaturaContrato:dataAssinatura.month'){
				if (form.elements[i].value == null || allTrim(form.elements[i]) == '')	
					dataValidaFim = false;
			}		
			if (form.elements[i].id == 'registrarAssinaturaContrato:dataAssinatura.year'){
				if (form.elements[i].value == null || allTrim(form.elements[i]) == '')	
					dataValidaFim = false;
			}									


		}
		
			
		if (dataValidaFim == false) 
			campos = campos + msgcampo + ' ' + msgDataAssinatura + ' ' + msgnecessario + '\n';			
		
		if (campos != ''){
			alert(campos);
			hidden.value  ='F';
			return false;
		}else{
			hidden.value  ='T';
			return true;
		}
}

function validaCamposFiltro(form, msgcampo, msgnecessario, msgnumero, msgsituacao, msgdata){


	var objRdoCliente = document.getElementsByName('radioSelecaoFiltro');
	
	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById(form.id + ':numeroFiltro'))== "") {
			alert(msgcampo + ' ' + msgnumero + ' ' + msgnecessario);
			document.getElementById(form.id +':numeroFiltro').focus();
			return false;		
			}
	} 
	else if (objRdoCliente[1].checked) {
		if (document.getElementById(form.id +':cboSituacaoFiltro').value == 0) {
			alert(msgcampo + ' ' + msgsituacao + ' ' + msgnecessario);
			document.getElementById(form.id +':cboSituacaoFiltro').focus();
			return false;		
		}
	} 
	else if (objRdoCliente[2].checked) {
			if (allTrim(document.getElementById(form.id +':txtDiaInicio'))== "" || allTrim(document.getElementById(form.id +':txtMesInicio')) == "" || allTrim(document.getElementById(form.id +':txtAnoInicio')) == "" 
			|| allTrim(document.getElementById(form.id +':txtDiaFim')) == "" || allTrim(document.getElementById(form.id +':txtMesFim')) == "" || allTrim(document.getElementById(form.id +':txtAnoFim')) == "") {
				alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
				document.getElementById(form.id +':txtDiaInicio').focus();
				return false;	
			}
		} 
	
	return true;						
}

function desabilitarBotoesConformeRadio(formId) {
	var arrRadio = document.getElementsByName('sor');
	var existeRadioCheckado = false;
	if (arrRadio != null) {
		for (var i = 0; i < arrRadio.length; i++) {
			if (arrRadio[i].checked) {
				existeRadioCheckado = true;
			}
		}
	}
	
	if (!existeRadioCheckado) {
		document.getElementById(formId + ':btnLimpar').disabled = true;
		document.getElementById(formId + ':btnDetalhar').disabled = true;
		document.getElementById(formId + ':btnImprimir').disabled = true;
		document.getElementById(formId + ':btnRegistrar').disabled = true;
	}
}