function msgmConfirmacao(perguntaConfirmacao, msgmConfirmacao){
	if(confirm(perguntaConfirmacao)){
		alert(msgmConfirmacao);
	}
}

function validaInclusao(form, msgmCampo, msgmNecessario, campoServico, campoRadioMudanca, campoFormaEfetivacao, campoOperacoes, campoData){

	var campos ='';
	var hidden;
	var flagData = false;
	var radioMudanca = document.getElementsByName('radioTipoMudanca');
	var radioFormaEfetivacao = document.getElementsByName('radioFormaEfetivacao');
	var radioManterOperacoes = document.getElementsByName('radioManterOperacoes');
	
	
// Modificado para apresentar msg na ordem de apresentacao dos campos na tela.
	
	if(document.getElementById(form.id + ':tipoServico').value == 0) {
		campos = campos + msgmCampo + ' ' + campoServico + ' ' + msgmNecessario +'\n';
	}
	
	//if(!(radioMudanca[0].checked || radioMudanca[1].checked)){	
	//	campos = campos + msgmCampo + ' ' + campoRadioMudanca + ' ' + msgmNecessario +'\n';
	//}
	
	if(!(radioFormaEfetivacao[0].checked || radioFormaEfetivacao[1].checked)){
		campos = campos + msgmCampo + ' ' + campoFormaEfetivacao + ' ' + msgmNecessario +'\n';
	}else{
		if(radioFormaEfetivacao[0].checked){
			if(document.getElementById(form.id + ':dataProgramada.day').value == '' && !flagData) {
				campos = campos + msgmCampo + ' ' + campoData + ' ' + msgmNecessario +'\n';
				flagData = true;
			}

			if(document.getElementById(form.id + ':dataProgramada.month').value == '' && !flagData) {
				campos = campos + msgmCampo + ' ' + campoData + ' ' + msgmNecessario +'\n';
				flagData = true;
			}

			if(document.getElementById(form.id + ':dataProgramada.year').value == '' && !flagData) {
				campos = campos + msgmCampo + ' ' + campoData + ' ' + msgmNecessario +'\n';
				flagData = true;
			}
				
	
			if(!(radioManterOperacoes[0].checked || radioManterOperacoes[1].checked)){
				campos = campos + msgmCampo + ' ' + campoOperacoes + ' ' + msgmNecessario +'\n';
			}
		}
	}

	if(campos != ''){
		document.getElementById(form.id + ':hiddenObrigatoriedade').value = 'F';
		alert(campos);
		campos = '';
		flagData = false;
	}else{
		document.getElementById(form.id + ':hiddenObrigatoriedade').value = 'T';
		flagData = false;
	}
}
function validaInclusaoParticipante(form, msgmCampo, msgmNecessario, campoServico, campoRadioMudanca, campoFormaEfetivacao, campoOperacoes, campoData, tipoParticipante){
	
	var campos ='';
	var hidden;
	var flagData = false;
	var radioMudanca = document.getElementsByName('radioTipoMudanca');
	var radioFormaEfetivacao = document.getElementsByName('radioFormaEfetivacao');
	var radioManterOperacoes = document.getElementsByName('radioManterOperacoes');
	var radioTipoParticipante = document.getElementsByName('radioTipoParticipante')
	
// Modificado para apresentar msg na ordem de apresentacao dos campos na tela.
	if(!(radioTipoParticipante[0].checked || radioTipoParticipante[1].checked)){
		campos = campos + msgmCampo + ' ' + tipoParticipante + ' ' + msgmNecessario +'\n';
	}else{
		if(radioTipoParticipante[1].checked){
			if(document.getElementById(form.id + ':hiddenParticipanteSelecionado').value == '' || document.getElementById(form.id + ':hiddenCPF_CNPJ_ParticipanteSelecionado').value == ''){
				campos = campos + msgmCampo + ' ' + tipoParticipante + ' ' + msgmNecessario +'\n';
			}
		}
	}
	
	if(document.getElementById(form.id + ':tipoServico').value == 0) {
		campos = campos + msgmCampo + ' ' + campoServico + ' ' + msgmNecessario +'\n';
	}
	
	//if(!(radioMudanca[0].checked || radioMudanca[1].checked)){	
	//	campos = campos + msgmCampo + ' ' + campoRadioMudanca + ' ' + msgmNecessario +'\n';
	//}
	
	if(!(radioFormaEfetivacao[0].checked || radioFormaEfetivacao[1].checked)){
		campos = campos + msgmCampo + ' ' + campoFormaEfetivacao + ' ' + msgmNecessario +'\n';
	}else{
		if(radioFormaEfetivacao[0].checked){
			if(document.getElementById(form.id + ':dataProgramada.day').value == '' && !flagData) {
				campos = campos + msgmCampo + ' ' + campoData + ' ' + msgmNecessario +'\n';
				flagData = true;
			}
			
			if(document.getElementById(form.id + ':dataProgramada.month').value == '' && !flagData) {
				campos = campos + msgmCampo + ' ' + campoData + ' ' + msgmNecessario +'\n';
				flagData = true;
			}
			
			if(document.getElementById(form.id + ':dataProgramada.year').value == '' && !flagData) {
				campos = campos + msgmCampo + ' ' + campoData + ' ' + msgmNecessario +'\n';
				flagData = true;
			}
			
			
			if(!(radioManterOperacoes[0].checked || radioManterOperacoes[1].checked)){
				campos = campos + msgmCampo + ' ' + campoOperacoes + ' ' + msgmNecessario +'\n';
			}
		}
	}
	
	if(campos != ''){
		document.getElementById(form.id + ':hiddenObrigatoriedade').value = 'F';
		alert(campos);
		campos = '';
		flagData = false;
		return false;
	}else{
		document.getElementById(form.id + ':hiddenObrigatoriedade').value = 'T';
		flagData = false;
		return true;
	}
}


function desabilitaRadioManutencao(){
	var radioMudanca = document.getElementsByName('radioTipoMudanca');
	var radioManterOperacoes = document.getElementsByName('radioManterOperacoes');
	
	if(radioMudanca[1].checked){
		radioManterOperacoes[0].checked = true;
		radioManterOperacoes[0].disabled = true; 
		radioManterOperacoes[1].disabled = true;
	}else{
		radioManterOperacoes[0].checked = false;
		radioManterOperacoes[1].checked = false;
		radioManterOperacoes[0].disabled = ""; 
		radioManterOperacoes[1].disabled = "";
	}
	
}

function checaCampoObrigatorio(msgcampo, msgnecessario, msgdata){
	    
	if ( document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioDe.day').value == null ||
		 document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioDe.day').value == ''   ||
		 document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioDe.month').value == null ||
		 document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioDe.month').value == ''   ||
		 document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioDe.year').value == null ||
		 document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioDe.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if ( document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioAte.day').value == null ||
		 document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioAte.day').value == ''   ||
		 document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioAte.month').value == null ||
		 document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioAte.month').value == ''   ||
		 document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioAte.year').value == null ||
		 document.getElementById('solManterSolicitacoesMudancaAmbienteOperacaoForm:calendarioAte.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}

}

function voltarSelecionar(){
	document.getElementById('incManterSolicitacoesMudancaAmbienteOperacaoPaticipanteForm:btnVoltarSelecionar').click();
}