function validaCamposFiltro(form, msgcampo, msgnecessario, msgnumero, msgsituacao, msgdata){


	var objRdoCliente = document.getElementsByName('radioSelecaoFiltro');
	
	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById(form.id + ':numeroFiltro'))== "") {
			alert(msgcampo + ' ' + msgnumero + ' ' + msgnecessario);
			document.getElementById(form.id +':numeroFiltro').focus();
			return false;		
			}
	} 
	else if (objRdoCliente[1].checked) {
		if (document.getElementById(form.id +':cboSituacaoFiltro').value == 0) {
			alert(msgcampo + ' ' + msgsituacao + ' ' + msgnecessario);
			document.getElementById(form.id +':cboSituacaoFiltro').focus();
			return false;		
		}
	} 
	else if (objRdoCliente[2].checked) {
			if (allTrim(document.getElementById(form.id +':txtDiaInicio'))== "" || allTrim(document.getElementById(form.id +':txtMesInicio')) == "" || allTrim(document.getElementById(form.id +':txtAnoInicio')) == "" 
			|| allTrim(document.getElementById(form.id +':txtDiaFim')) == "" || allTrim(document.getElementById(form.id +':txtMesFim')) == "" || allTrim(document.getElementById(form.id +':txtAnoFim')) == "") {
				alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
				document.getElementById(form.id +':txtDiaInicio').focus();
				return false;	
			}
		} 
	
	return true;						
}
