
function validaMotivoAtivacao(form, msgCampo, msgNomeCampo, msgE, msgNecessario){

	var campos, hidden;
	campos = '';
	
	for(i = 0; i < form.elements.length; i++){
		
		if(form.elements[i].id == form.id + ':hiddenObrigatoriedade'){
			hidden = form.elements[i];
		}
		if(form.elements[i].id == form.id + ':motivoAtivacao'){
			if(form.elements[i].value == '0'){
				campos = campos + msgCampo + ' ' + msgNomeCampo + ' ' + msgE + ' ' + msgNecessario + '\n'; 
			}
		}
	}
	
	if(campos != ''){
		hidden.value = 'F';
		document.getElementById(form.id + ':hiddenObrigatoriedade').value =  'F';
		alert(campos);
		campos = '';
	}else{
		document.getElementById(form.id + ':hiddenObrigatoriedade').value =  'T';
		hidden.value = 'T';
	}

}

function mensagemConfirmacao(msgConfirmacao, msgLiberada){
	
	if(confirm(msgConfirmacao)){
		alert(msgLiberada);
	}
}