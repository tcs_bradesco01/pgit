
function validaCamposTerceiro(form, msgcampo, msgnecessario, msgcnpj, msgcpf){

	var msgValidacao = '';

	var objRdoCliente = document.getElementsByName('rdoCpfCnpjTerceiros');

	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById(form.id + ':txtCnpj')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';
			
		} else {
			if (allTrim(document.getElementById(form.id +':txtFilial'))== "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';					
			} else {
				if (allTrim(document.getElementById(form.id + ':txtControle')) == ""){
					msgValidacao = msgValidacao +   msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';				
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById(form.id +':txtCpf')) == "") {
			msgValidacao = msgValidacao +   msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
			
		} else {
			if(allTrim(document.getElementById(form.id +':txtControleCpf')) == "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
				
			}
		}
	} 
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;						
}



function validaCamposInclusao(form, msgcampo, msgnecessario, msgcnpj, msgcpf, msgdatainiciorastr, msgagendartitrastre, 
								msgrastrearnf, msgbloqemissaopapel, msgdatabloqemissaopapel){

	var msgValidacao = '';

	var objRdoCpfCnpj = document.getElementsByName('rdoCpfCnpjIncluir');
	
	var objRdoAgendarTitRas = document.getElementsByName('radioAgendarTitRastreados');
	var objRdoRastrearNF = document.getElementsByName('radioRastrearNotasFiscais');
	var objRdoBloqEmissaoPapel = document.getElementsByName('radioBloquearEmissaoPapeleta');
		
	
	
	//validando campos cpf/cnpj
	if (objRdoCpfCnpj[0].checked) {
		if (allTrim(document.getElementById(form.id +':txtCpf')) == "") {
			msgValidacao = msgValidacao +   msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
			
		} else {
			if(allTrim(document.getElementById(form.id +':txtControleCpf')) == "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
				
			}
		}
	} else if (objRdoCpfCnpj[1].checked) {
		if (allTrim(document.getElementById(form.id + ':txtCnpj')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';
			
		} else {
			if (allTrim(document.getElementById(form.id +':txtFilial'))== "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';					
			} else {
				if (allTrim(document.getElementById(form.id + ':txtControle')) == ""){
					msgValidacao = msgValidacao +   msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';				
				}
			}
		}
	} 
	
	//verificando se cpf ou cnpj foi preenchido
	if(objRdoCpfCnpj[0].checked == false && objRdoCpfCnpj[1].checked == false){
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgcpf + ' ' + '/' + ' ' + msgcnpj + ' ' +  msgnecessario + '!\n';
	}
	
	//verificando se "Agendar T�tulos Rastreados" foi preenchido
	if(objRdoAgendarTitRas[1].checked == false && objRdoAgendarTitRas[2].checked == false){
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgagendartitrastre +  ' ' +  msgnecessario + '!\n';
	}
	
	//verificando se "Rastrear Notas Fiscais" foi preenchido
	if(objRdoRastrearNF[1].checked == false && objRdoRastrearNF[2].checked == false){
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgrastrearnf +  ' ' +  msgnecessario + '!\n';
	}
	
	//verificando se "Bloquear Emiss�o de Parcelas" foi preenchido
	if(objRdoBloqEmissaoPapel[1].checked == false && objRdoBloqEmissaoPapel[2].checked == false){
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgbloqemissaopapel + ' ' +  msgnecessario + '!\n';
	}

//	var dia = new Date().getDate();
//	var mes = new Date().getMonth() + 1;
//	var ano = new Date().getYear();
//	var dataRastreamento = 0;
//
//	if (document.getElementById(form.id + ':dataInicialRastreamento.year').value < ano) {
//		dataRastreamento = dataRastreamento + 1;
//	} else {
//		if (document.getElementById(form.id + ':dataInicialRastreamento.month').value < mes) {
//			dataRastreamento = dataRastreamento + 1;
//		} else {
//			if (document.getElementById(form.id + ':dataInicialRastreamento.day').value < dia) {
//				dataRastreamento = dataRastreamento + 1;
//			}
//		}
//	}
	
	
	var data1Rastreamento = document.getElementById(form.id + ':dataInicialRastreamento').value;
	var data2Rastreamento = document.getElementById(form.id + ':dataHoje').value;
	
	data1Rastreamento = new String(data1Rastreamento);
	data2Rastreamento = new String(data2Rastreamento);

	data1Rastreamento = data1Rastreamento.split('/');
	data2Rastreamento = data2Rastreamento.split('/');

	data1Rastreamento = parseInt( data1Rastreamento[2] + data1Rastreamento[1] + data1Rastreamento[0] );
	data2Rastreamento = parseInt( data2Rastreamento[2] + data2Rastreamento[1] + data2Rastreamento[0] );


	if (data1Rastreamento < data2Rastreamento) {
		msgValidacao = msgValidacao + 'A data informada no campo Data de Inicio do Rastreamento nao pode ser menor que a data atual' + '!\n';
	}

	var dataPapeleta = 0;

	if (objRdoBloqEmissaoPapel[1].checked == true) {
		
		var data1 = document.getElementById(form.id + ':dataInicioBloqEmissaoPapeleta').value;
		var data2 = document.getElementById(form.id + ':dataHoje').value;
		
		data1 = new String(data1);
		data2 = new String(data2);

		data1 = data1.split('/');
		data2 = data2.split('/');

		data1 = parseInt( data1[2] + data1[1] + data1[0] );
		data2 = parseInt( data2[2] + data2[1] + data2[0] );

	
		if (data1 < data2) {
			msgValidacao = msgValidacao + 'A data informada no campo Data de Inicio do Bloqueio de Emissao de Papeleta nao pode ser menor que a data atual' + '!\n';
		}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;						
}

function validaCamposAlteracao(form, msgcampo, msgnecessario, msgdatainiciorastr, msgagendartitrastre, 
								msgrastrearnf, msgbloqemissaopapel, msgdatabloqemissaopapel){

	var msgValidacao = '';

		
	var objRdoAgendarTitRas = document.getElementsByName('radioAgendarTitRastreados');
	var objRdoRastrearNF = document.getElementsByName('radioRastrearNotasFiscais');
	var objRdoBloqEmissaoPapel = document.getElementsByName('radioBloquearEmissaoPapeleta');
		
	
	//verificando se "Agendar T�tulos Rastreados" foi preenchido
	if (objRdoAgendarTitRas[1].checked == false && objRdoAgendarTitRas[2].checked == false) {
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgagendartitrastre + ' ' + msgnecessario + '!\n';
	}
	
	//verificando se "Rastrear Notas Fiscais" foi preenchido
	if (objRdoRastrearNF[1].checked == false && objRdoRastrearNF[2].checked == false) {
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgrastrearnf + ' ' + msgnecessario + '!\n';
	}
	
	//verificando se "Bloquear Emiss�o de Parcelas" foi preenchido
	if (objRdoBloqEmissaoPapel[1].checked == false && objRdoBloqEmissaoPapel[2].checked == false) {
		msgValidacao = msgValidacao +  msgcampo + ' ' + msgbloqemissaopapel + ' ' + msgnecessario + '!\n';
	}
	
	
		
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;						
}

