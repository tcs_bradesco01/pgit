function validaCpoContrato(msgcampo, msgnecessario,msgempresa,msgtipo, msgnumero, cliente){
	var msgValidacao = '';

	if ((cliente == null) || (cliente ==  '')) {
		if (document.getElementById('formAlteraTpClassifParticContrato:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		if (document.getElementById('formAlteraTpClassifParticContrato:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
		if ( allTrim(document.getElementById('formAlteraTpClassifParticContrato:txtNumeroContrato')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' ' + msgnecessario + '\n';
		} 
	} else {
		if (document.getElementById('formAlteraTpClassifParticContrato:selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		if (document.getElementById('formAlteraTpClassifParticContrato:selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
	}	

	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}

function validaCpoEnderecoAvanca(form, msgcampo, msgnecessario, msgEnderecoFixo, msgEnderecoTransitorio, msgQtdeDias) {
	var objRdoEndereco = document.getElementsByName('rdoEndereco');
	var msgValidacao = '';
	
	if(!objRdoEndereco[0].checked && !objRdoEndereco[1].checked){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgEnderecoFixo + " ou " + msgEnderecoTransitorio + ' ' + msgnecessario + '\n';
	}
	else if(objRdoEndereco[1].checked && allTrim(document.getElementById('formAlteraTpClassifParticContrato:txtQtdeDias')) == ""){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgQtdeDias + ' ' + msgnecessario + '\n';
	}
	else if (objRdoEndereco[1].checked && allTrim(document.getElementById('formAlteraTpClassifParticContrato:txtQtdeDias')) == "0") {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgQtdeDias + ' tem que ser diferente de 0\n';
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;
	
}

function validaCpoEndereco(msgcampo, msgnecessario, msgendereco) {
	var objEndereco = document.getElementById('formAlteraTpClassifParticContrato:selEndereco');
	if (objEndereco.value == 0) {
		alert(msgcampo + ' ' + msgendereco + ' ' + msgnecessario);
		objEndereco.focus();
		return false;
	}

	return true;
}

function validarTipoConsulta(){
	if (!document.getElementsByName('rdoArgPesquisa')[0].checked && !document.getElementsByName('rdoArgPesquisa')[1].checked) {
		alert("Selecionar pelo menos um argumento de pesquisa.");
		return false;
	}
}