function validarFiltros(form){

	var radioBloqDesbloqFiltro, radioContratoEspelhoFiltro, cboMotivoBloqueioFiltro, hiddenProsseguir;
	
	radioBloqDesbloqFiltro = document.getElementById(form.id + ':hiddenRadioBloqDesbloqFiltro').value;
	radioContratoEspelhoFiltro = document.getElementById(form.id + ':hiddenRadioContratoEspelhoFiltro').value;
	cboMotivoBloqueioFiltro = document.getElementById(form.id + ':hiddenCboMotivoBloqueioFiltro').value;
	hiddenProsseguir = document.getElementById(form.id + ':hiddenProsseguir').value;
	
	//Bloquear
	if(radioBloqDesbloqFiltro == 1){
		if(radioContratoEspelhoFiltro == 0 || radioContratoEspelhoFiltro == null){
			alert("Informar Desbloquear/Bloquear Contratos Espelhados");
			hiddenProsseguir = 'F';
			document.getElementById(form.id + ':hiddenProsseguir').value =  hiddenProsseguir;
			return;
		}
		
		if(cboMotivoBloqueioFiltro == 0 || cboMotivoBloqueioFiltro == null){
			alert("Informar o Motivo do Bloqueio");
			hiddenProsseguir = 'F';
			document.getElementById(form.id + ':hiddenProsseguir').value =  hiddenProsseguir;
			return;
		}
		
		hiddenProsseguir = 'T';
		document.getElementById(form.id + ':hiddenProsseguir').value =  hiddenProsseguir;
		return;
		
	}else{
		 if(radioBloqDesbloqFiltro == 2){
		 	if(radioContratoEspelhoFiltro == 0 || radioContratoEspelhoFiltro == null){
				alert("Informar Desbloquear/Bloquear Contratos Espelhados");
				hiddenProsseguir = 'F';
				document.getElementById(form.id + ':hiddenProsseguir').value =  hiddenProsseguir;
				return;
			}
			
			hiddenProsseguir = 'T';
			document.getElementById(form.id + ':hiddenProsseguir').value =  hiddenProsseguir;
			return;
		 }
	
	}
		
	if(radioBloqDesbloqFiltro == 0 || radioBloqDesbloqFiltro == null){
		alert("Informar Bloquear ou Desbloquear");
		hiddenProsseguir = 'F';
		document.getElementById(form.id + ':hiddenProsseguir').value =  hiddenProsseguir;
		return;
	}
	
	if(radioContratoEspelhoFiltro == 0 || radioContratoEspelhoFiltro == null){
		alert("Informar Desbloquear/Bloquear Contratos Espelhados");
		hiddenProsseguir = 'F';
		document.getElementById(form.id + ':hiddenProsseguir').value =  hiddenProsseguir;
		return;
	}
	
	if(cboMotivoBloqueioFiltro == 0 || cboMotivoBloqueioFiltro == null){
		alert("Informar o Motivo do Bloqueio");
		hiddenProsseguir = 'F';
		document.getElementById(form.id + ':hiddenProsseguir').value =  hiddenProsseguir;
		return;
	}
	
	hiddenProsseguir = 'T';
	document.getElementById(form.id + ':hiddenProsseguir').value =  hiddenProsseguir;
	return;
}

function validarFiltrosServicoContrato(form,informebloqueardesbloquear, informemotivobloqueio ){
	var radiobloquearDesbloquearFiltro, cboMotivoBloqueioFiltro;
	
	radiobloquearDesbloquearFiltro = document.getElementById(form.id + ':hiddenRadiobloquearDesbloquearFiltro').value;
	cboMotivoBloqueioFiltro = document.getElementById(form.id + ':cboMotivoBloqueioFiltro').value;	
	
	if(radiobloquearDesbloquearFiltro == 0 || radiobloquearDesbloquearFiltro == null)
	{
		alert(informebloqueardesbloquear);
		document.getElementById(form.id + ':hiddenProsseguir').value = 'F';
		return;
	}

	if(cboMotivoBloqueioFiltro == 0 || cboMotivoBloqueioFiltro == null){
		alert(informemotivobloqueio);
		document.getElementById(form.id + ':hiddenProsseguir').value = 'F';
		return;
	}

	document.getElementById(form.id + ':hiddenProsseguir').value = 'T';
	return;
	
}

function validarFiltrosContasContrato(form){
	var radiobloquearDesbloquearFiltro, cboMotivoBloqueioFiltro;
	
	radiobloquearDesbloquearFiltro = document.getElementById(form.id + ':hiddenRadioBloqDesbloqFiltro').value;
	cboMotivoBloqueioFiltro = document.getElementById(form.id + ':hiddenCboMotivoBloqueioFiltro').value;	
	
	 
	if(radiobloquearDesbloquearFiltro == 1){
		if(cboMotivoBloqueioFiltro == 0 || cboMotivoBloqueioFiltro == null){
			alert("Informe o motivo do Bloqueio");
			document.getElementById(form.id + ':hiddenProsseguir').value = 'F';
			return;
		}
	}else{
		if(radiobloquearDesbloquearFiltro == 0 || radiobloquearDesbloquearFiltro == null)
		{
			alert("Informe Bloquear ou Desbloquear");
			document.getElementById(form.id + ':hiddenProsseguir').value = 'F';
			return;
		}
	}
	document.getElementById(form.id + ':hiddenProsseguir').value = 'T';
	return;
	
}

function selecionarTodos(form, check){

	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
		
	}	
	
	document.getElementById(form.id + ':btoBloquearDesbloquear').disabled = !check.checked;	
	
	
}
