function validaCamposFiltro(form, msgcampo, msgnecessario, msgCCustoOrigem, msgCboEmpresaGestoraCont, msgCboTpContrato, msgNumero, msgRadio, msgRadioCo){

	var objRdoPesquisa = document.getElementsByName('rdoPesquisa');
	var mensagen = '';
	var espaco = ' ';
	
	if (!objRdoPesquisa[0].checked && !objRdoPesquisa[1].checked) {
		mensagen = msgcampo + espaco + msgRadio + espaco  + 'ou' + espaco + msgRadioCo + espaco + msgnecessario + '!\n';
	}else if (objRdoPesquisa[0].checked) {
		if (document.getElementById(form.id + ':cboCustoOrigem').value == "0") {
				mensagen = msgcampo + espaco + msgCCustoOrigem + espaco + msgnecessario + '!\n';
		}
	} 
	else if (objRdoPesquisa[1].checked) {
		
		if (document.getElementById(form.id +':cboEmpresaGestoraCont').value == "0"){
			mensagen = mensagen + msgcampo + espaco + msgCboEmpresaGestoraCont + espaco + msgnecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':cboTpContrato').value == "0"){
			mensagen = mensagen + msgcampo + espaco + msgCboTpContrato + espaco + msgnecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':numero').value == ''){
			mensagen = mensagen + msgcampo + espaco + msgNumero + espaco + msgnecessario + '!\n';
		}
			 
	} 
	
	if(mensagen != ''){
		alert(mensagen);
		return false;
	}else{
	return true;
	}
	
}
