function confirmar(form, msgConfirma){
	
		var hiddenConfirm;
		
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id + ':hiddenConfirma'){	
				hiddenConfirm = form.elements[i];
				break;
			}
		}
		
		hiddenConfirm.value=confirm(msgConfirma);
	
}

function validaCpoConsultaCliente(msgcampo, msgnecessario, msgcnpj, msgcpf, msgnome, msgbanco, msgagencia, msgconta){

var objRdoCliente = document.getElementsByName('radioIncluir');

	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById('incParticipantesManterContrato:txtCnpj')) == "") {
			alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
			document.getElementById('incParticipantesManterContrato:txtCnpj').focus();
			return false;		
		} /*else {
			if (document.getElementById('incParticipantesManterContrato:txtFilial').value == "") {
				alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
				document.getElementById('incParticipantesManterContrato:txtFilial').focus();
				return false;		
			} else {
				if (document.getElementById('incParticipantesManterContrato:txtControle').value == ""){
					alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
					document.getElementById('incParticipantesManterContrato:txtControle').focus();
					return false;		
				}
			}
		}
		*/
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById('incParticipantesManterContrato:txtCpf')) == "") {
			alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
			document.getElementById('incParticipantesManterContrato:txtCpf').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById('incParticipantesManterContrato:txtControleCpf')) == "") {
				alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
				document.getElementById('incParticipantesManterContrato:txtControleCpf').focus();
				return false;	
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById('incParticipantesManterContrato:txtNomeRazaoSocial')) == "") {
			alert(msgcampo + ' ' + msgnome + ' ' + msgnecessario);
			document.getElementById('incParticipantesManterContrato:txtNomeRazaoSocial').focus();
			return false;	
		}
	} else if (objRdoCliente[3].checked) {
		if (allTrim(document.getElementById('incParticipantesManterContrato:txtBanco')) == "") {
			alert(msgcampo + ' ' + msgbanco + ' ' + msgnecessario);
			document.getElementById('incParticipantesManterContrato:txtBanco').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById('incParticipantesManterContrato:txtAgencia')) == "") {
				alert(msgcampo + ' ' + msgagencia + ' ' + msgnecessario);
				document.getElementById('incParticipantesManterContrato:txtAgencia').focus();
				return false;		
			} else {
				if(allTrim(document.getElementById('incParticipantesManterContrato:txtConta')) == "") {
					alert(msgcampo + ' ' + msgconta + ' ' + msgnecessario);
					document.getElementById('incParticipantesManterContrato:txtConta').focus();
					return false;		
				} 

				else {
						if (1 != 1) {
						return false;
					}	
					/*
					  comentado porque o campo dig da conta foi removido da tela. vreghini
					if(document.getElementById('incParticipantesManterContrato:txtContaDig').value == ""){
						alert(msgcampo + ' ' + 'Dig' + ' ' + msgnecessario);
						document.getElementById('incParticipantesManterContrato:txtContaDig').focus();
						return false;
					}
					*/
				
				}
			}
		}
	} 
	
	return true;						
}
function checaCampoObrigatorioHist(msgcampo, msgnecessario, msgdata,msgcpf,msgcnpj){
var objRdoCliente = document.getElementsByName('radioHistorico');
	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById('hisParticipantesManterContrato:txtCpf')) == "") {
			alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
			
			return false;		
		} else {
			if(allTrim(document.getElementById('hisParticipantesManterContrato:txtControleCpf')) == "") {
				alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
				
				return false;	
			}
		}
	}else if (objRdoCliente[1].checked) {
				if (allTrim(document.getElementById('hisParticipantesManterContrato:txtCnpj')) == "") {
					alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
					
					return false;		
				} else {
					if (allTrim(document.getElementById('hisParticipantesManterContrato:txtFilial')) == "") {
						alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
						
						return false;		
					} else {
						if (allTrim(document.getElementById('hisParticipantesManterContrato:txtControle')) == ""){
							alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
							t.getElementById('hisParticipantesManterContrato:txtControle').focus();
							return false;		
						}
					}
				}
				
			}
	 
	if ( document.getElementById('hisParticipantesManterContrato:dataInicial.day').value == null ||
		 document.getElementById('hisParticipantesManterContrato:dataInicial.day').value == ''   ||
		 document.getElementById('hisParticipantesManterContrato:dataInicial.month').value == null ||
		 document.getElementById('hisParticipantesManterContrato:dataInicial.month').value == ''   ||
		 document.getElementById('hisParticipantesManterContrato:dataInicial.year').value == null ||
		 document.getElementById('hisParticipantesManterContrato:dataInicial.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if ( document.getElementById('hisParticipantesManterContrato:dataFinal.day').value == null ||
		 document.getElementById('hisParticipantesManterContrato:dataFinal.day').value == ''   ||
		 document.getElementById('hisParticipantesManterContrato:dataFinal.month').value == null ||
		 document.getElementById('hisParticipantesManterContrato:dataFinal.month').value == ''   ||
		 document.getElementById('hisParticipantesManterContrato:dataFinal.year').value == null ||
		 document.getElementById('hisParticipantesManterContrato:dataFinal.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}

	return true;
}

function validaBloquearDesbloquear(msgcampo, msgnecessario, msgcondicao, msgmotivo){

var objRdoCondicao = document.getElementsByName('radioCondicao');
	var campos= '';
	
	if ( !objRdoCondicao[1].checked && !objRdoCondicao[2].checked) {
		campos += (msgcampo + ' ' + msgcondicao + ' ' + msgnecessario + '\n');
	}
	
	if(document.getElementById('bloqDesbloqParticipantesBloquearDesbloquear:cboMotivoBloqueioDesbloqueio').value == null ||
		document.getElementById('bloqDesbloqParticipantesBloquearDesbloquear:cboMotivoBloqueioDesbloqueio').value == 0) {	
			campos += (msgcampo + ' ' + msgmotivo + ' ' + msgnecessario + '\n');
	}	 
	
	if(campos != ''){
		alert(campos);
		campos = '';
		return false;
	}else{
		return true;
	}		
}

function validarProxCampoIdentClienteContratoParticipante(form, flagPesquisa){
	var objRdoCliente = document.getElementsByName('radioIncluir');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCnpjFilial = document.getElementById(form.id + ':txtFilial');
	var campoCnpjControle = document.getElementById(form.id + ':txtControle');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoCpfControle = document.getElementById(form.id + ':txtControleCpf');
	var campoNomeRazao = document.getElementById(form.id + ':txtNomeRazaoSocial');
	var campoBanco = document.getElementById(form.id + ':txtBanco');
	var campoAgencia = document.getElementById(form.id + ':txtAgencia');
	var campoConta = document.getElementById(form.id + ':txtConta');
	
	if(objRdoCliente[0].checked){
		campoCnpj.disabled = false;
		campoCnpj.focus();
		campoCnpjFilial.disabled = false;
		campoCnpjControle.disabled = false;
		
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCpf.value = '';
		campoCpfControle.value = '';
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[1].checked){
		campoCpf.disabled = false;
		campoCpf.focus();
		campoCpfControle.disabled = false;
	
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[2].checked){
		campoNomeRazao.disabled = false;
		campoNomeRazao.focus();
			
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[3].checked){
		campoBanco.disabled = false;
		campoAgencia.disabled = false;
		campoConta.disabled = false;
		campoBanco.value = 237;
		campoAgencia.focus();
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else{
		document.getElementById(form.id + ':btnConsultar').disabled = true;
		return false;
	}
}

function validarProxCampoIdentClienteContratoHistorico(form, flagPesquisa){
	var objRdoCliente = document.getElementsByName('radioHistorico');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCnpjFilial = document.getElementById(form.id + ':txtFilial');
	var campoCnpjControle = document.getElementById(form.id + ':txtControle');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoCpfControle = document.getElementById(form.id + ':txtControleCpf');
	
	if(objRdoCliente[0].checked){
		campoCnpj.disabled = false;
		campoCnpj.focus();
		campoCnpjFilial.disabled = false;
		campoCnpjControle.disabled = false;
		
		campoCpf.value = '';
		campoCpfControle.value = '';
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[1].checked){
		campoCpf.disabled = false;
		campoCpf.focus();
		campoCpfControle.disabled = false;
	
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else{
		document.getElementById(form.id + ':btnConsultar').disabled = true;
		return false;
	}
}