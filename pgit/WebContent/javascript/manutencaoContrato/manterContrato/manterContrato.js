function confirmar(form, msgConfirma){
	
		var hiddenConfirm;
		
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id + ':hiddenConfirma'){	
				hiddenConfirm = form.elements[i];
				break;
			}
		}
		
		hiddenConfirm.value=confirm(msgConfirma);
	
}

function validaCpoConsultaCliente(msgcampo, msgnecessario, msgcnpj, msgcpf, msgnome, msgbanco, msgagencia, msgconta){

var objRdoCliente = document.getElementsByName('radioIncluir');

	if (objRdoCliente[0].checked) {
		if (document.getElementById('incParticipantesManterContrato:txtCnpj').value == "") {
			alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
			document.getElementById('incParticipantesManterContrato:txtCnpj').focus();
			return false;		
		} else {
			if (document.getElementById('incParticipantesManterContrato:txtFilial').value == "") {
				alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
				document.getElementById('incParticipantesManterContrato:txtFilial').focus();
				return false;		
			} else {
				if (document.getElementById('incParticipantesManterContrato:txtControle').value == ""){
					alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
					document.getElementById('incParticipantesManterContrato:txtControle').focus();
					return false;		
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (document.getElementById('incParticipantesManterContrato:txtCpf').value == "") {
			alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
			document.getElementById('incParticipantesManterContrato:txtCpf').focus();
			return false;		
		} else {
			if(document.getElementById('incParticipantesManterContrato:txtControleCpf').value == "") {
				alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
				document.getElementById('incParticipantesManterContrato:txtControleCpf').focus();
				return false;	
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (document.getElementById('incParticipantesManterContrato:txtNomeRazaoSocial').value == "") {
			alert(msgcampo + ' ' + msgnome + ' ' + msgnecessario);
			document.getElementById('incParticipantesManterContrato:txtNomeRazaoSocial').focus();
			return false;	
		}
	} else if (objRdoCliente[3].checked) {
		if (document.getElementById('incParticipantesManterContrato:txtBanco').value == "") {
			alert(msgcampo + ' ' + msgbanco + ' ' + msgnecessario);
			document.getElementById('incParticipantesManterContrato:txtBanco').focus();
			return false;		
		} else {
			if(document.getElementById('incParticipantesManterContrato:txtAgencia').value == "") {
				alert(msgcampo + ' ' + msgagencia + ' ' + msgnecessario);
				document.getElementById('incParticipantesManterContrato:txtAgencia').focus();
				return false;		
			} else {
				if(document.getElementById('incParticipantesManterContrato:txtConta').value == "") {
					alert(msgcampo + ' ' + msgconta + ' ' + msgnecessario);
					document.getElementById('incParticipantesManterContrato:txtConta').focus();
					return false;		
				} 

				else {
						if (1 != 1) {
						return false;
					}	
					/*
					  comentado porque o campo dig da conta foi removido da tela. vreghini
					if(document.getElementById('incParticipantesManterContrato:txtContaDig').value == ""){
						alert(msgcampo + ' ' + 'Dig' + ' ' + msgnecessario);
						document.getElementById('incParticipantesManterContrato:txtContaDig').focus();
						return false;
					}
					*/
				
				}
			}
		}
	} 
	
	return true;						
}


function validaPesquisarContas(msgcampo, msgnecessario, msgcnpj, msgcpf, msgbanco, msgagencia, msgconta, msgdigito, msgtipoconta,msgfinalidade){

var campos= '';
var objRadio = document.getElementsByName('filtroRadio');

	if (objRadio[0].checked) {
	
		var validarCnpj=true;
		if (allTrim(document.getElementById('conContasManterContrato:txtCnpj')) == null || allTrim(document.getElementById('conContasManterContrato:txtCnpj')) == '') {
			validarCnpj = false;
		}
		if (allTrim(document.getElementById('conContasManterContrato:txtFilial')) == null || allTrim(document.getElementById('conContasManterContrato:txtFilial')) == '') {
			validarCnpj = false;
		}
		if (allTrim(document.getElementById('conContasManterContrato:txtControle')) == null || allTrim(document.getElementById('conContasManterContrato:txtControle')) == '') {
			validarCnpj = false;
		}		
		
		if (!validarCnpj){ 
			campos = msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '\n';
			alert(campos);
			return false;	
		}	
		
	}else if (objRadio[1].checked) {
		var validarCpf = true;
		
		if (allTrim(document.getElementById('conContasManterContrato:txtCpf')) == null || allTrim(document.getElementById('conContasManterContrato:txtCpf')) == '') {
			validarCpf = false;
		}
		if (allTrim(document.getElementById('conContasManterContrato:txtControleCpf')) == null || allTrim(document.getElementById('conContasManterContrato:txtControleCpf')) == '') {
			validarCpf = false;
		}	
		
		if (!validarCpf){ 
			campos = msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '\n';
			alert(campos);
			return false;	
		}	
	
	}else if (objRadio[2].checked) {
		var validar=true;
		
		if (allTrim(document.getElementById('conContasManterContrato:banco')) == '') {
			campos += (msgcampo + ' ' + msgbanco + ' ' + msgnecessario+ '\n');
			validar = false;		
		}
		if(allTrim(document.getElementById('conContasManterContrato:agencia')) == '' ) {
				campos += (msgcampo + ' ' + msgagencia + ' ' + msgnecessario+ '\n');
				validar = false;		
		} 
		if(allTrim(document.getElementById('conContasManterContrato:conta')) == '') {
					campos += (msgcampo + ' ' + msgconta + ' ' + msgnecessario+ '\n');
					validar = false;		
		}
		if(allTrim(document.getElementById('conContasManterContrato:digito')) == '') {
					campos += (msgcampo + ' ' + msgdigito + ' ' + msgnecessario+ '\n');
					validar = false;		
		}
		if(document.getElementById('conContasManterContrato:tipoConta').value == null || document.getElementById('conContasManterContrato:tipoConta').value == 0) {
			campos += (msgcampo + ' ' + msgtipoconta + ' ' + msgnecessario+ '\n');
			validar = false;		
		 }
	
		if (!validar){ 
			alert(campos);
			return false;	
		}
	}else{ 
		if (objRadio[3].checked) {
			if ( document.getElementById('conContasManterContrato:finalidade').value == null || document.getElementById('conContasManterContrato:finalidade').value == 0) {
				alert(msgcampo + ' ' + msgfinalidade + ' ' + msgnecessario);
				document.getElementById('conContasManterContrato:finalidade').focus();
				return false;		
			}
		}	
	} 
	
	return true;						
}

function validaPesquisarContasIncluir(msgcampo, msgnecessario,msgcnpj, msgcpf, msgbanco, msgagencia, msgconta, msgdigito, msgtipoconta){
	var objRadio = document.getElementsByName('filtroRadio');
	var campos= '';

	if (objRadio[0].checked) {
	
		var validarCnpj=true;
		if (allTrim(document.getElementById('incContasManterContrato:txtCnpj')) == null || allTrim(document.getElementById('incContasManterContrato:txtCnpj')) == '') {
			validarCnpj = false;
		}
		if (allTrim(document.getElementById('incContasManterContrato:txtFilial')) == null || allTrim(document.getElementById('incContasManterContrato:txtFilial')) == '') {
			validarCnpj = false;
		}
		if (allTrim(document.getElementById('incContasManterContrato:txtControle')) == null || allTrim(document.getElementById('incContasManterContrato:txtControle')) == '') {
			validarCnpj = false;
		}		
		
		if (!validarCnpj){ 
			campos = msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '\n';
			alert(campos);
			return false;	
		}	
		
	}else if (objRadio[1].checked) {
		var validarCpf = true;
		
		if (allTrim(document.getElementById('incContasManterContrato:txtCpf')) == null || allTrim(document.getElementById('incContasManterContrato:txtCpf')) == '') {
			validarCpf = false;
		}
		if (allTrim(document.getElementById('incContasManterContrato:txtControleCpf')) == null || allTrim(document.getElementById('incContasManterContrato:txtControleCpf')) == '') {
			validarCpf = false;
		}	
		
		if (!validarCpf){ 
			campos = msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '\n';
			alert(campos);
			return false;	
		}	
	
	}else if (objRadio[2].checked) {
		var validar=true;

		if (allTrim(document.getElementById('incContasManterContrato:banco')) == '') {
			campos += (msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '\n');
			validar = false;		
		} 
		if(allTrim(document.getElementById('incContasManterContrato:agencia')) == '') {
			campos +=(msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n');
			validar =  false;		
		} 
		if(allTrim(document.getElementById('incContasManterContrato:conta')) == '') {
			campos += (msgcampo + ' ' + msgconta + ' ' + msgnecessario + '\n');
			validar = false;		
		}
		
		if(document.getElementById('incContasManterContrato:tipoConta').value == null || document.getElementById('incContasManterContrato:tipoConta').value == 0) {
			campos +=(msgcampo + ' ' + msgtipoconta + ' ' + msgnecessario + '\n');
			validar = false;		
		}
		
		if (!validar){ 
			alert(campos);
			return false;	
		}			
	}	

}		

function validaPesquisarHistorico(msgcampo, msgnecessario, msgparticipante, msgbanco, msgagencia, msgconta, msgdigito, msgtipoconta,msgfinalidade){

var objRadio = document.getElementsByName('filtroRadio');

	if (objRadio[0].checked) {
		if ( document.getElementById('histContasManterContrato:participante').value == null || document.getElementById('histContasManterContrato:participante').value == 0) {
			alert(msgcampo + ' ' + msgparticipante + ' ' + msgnecessario);
			document.getElementById('histContasManterContrato:participante').focus();
			return false;		
			}
	}else if (objRadio[1].checked) {
		if (document.getElementById('histContasManterContrato:banco').value == '') {
			alert(msgcampo + ' ' + msgbanco + ' ' + msgnecessario);
			document.getElementById('histContasManterContrato:banco').focus();
			return false;		
		} else {
			if(document.getElementById('histContasManterContrato:agencia').value == '') {
				alert(msgcampo + ' ' + msgagencia + ' ' + msgnecessario);
				document.getElementById('histContasManterContrato:agencia').focus();
				return false;		
			} else {
				if(document.getElementById('histContasManterContrato:conta').value == '') {
					alert(msgcampo + ' ' + msgconta + ' ' + msgnecessario);
					document.getElementById('histContasManterContrato:conta').focus();
					return false;		
				}else {
				if(document.getElementById('histContasManterContrato:digito').value == '') {
					alert(msgcampo + ' ' + msgdigito + ' ' + msgnecessario);
					document.getElementById('histContasManterContrato:digito').focus();
					return false;		
				}else{
				if(document.getElementById('histContasManterContrato:tipoConta').value == null || document.getElementById('histContasManterContrato:tipoConta').value == 0) {
					alert(msgcampo + ' ' + msgtipoconta + ' ' + msgnecessario);
					document.getElementById('histContasManterContrato:tipoConta').focus();
					return false;		
				 }
			}
			}
			}						
			
		}
	}else{ 
		if (objRadio[2].checked) {
			if ( document.getElementById('histContasManterContrato:finalidade').value == null || document.getElementById('histContasManterContrato:finalidade').value == 0) {
				alert(msgcampo + ' ' + msgfinalidade + ' ' + msgnecessario);
				document.getElementById('histContasManterContrato:finalidade').focus();
				return false;		
			}
		}	
	} 
	
	return true;						
}

function validaPesquisarRelacionamentoHistorico(form, msgCampo, msgNecessario, msgParticipante, msgBanco, msgAgencia, msgConta, msgDigito, msgData, msgCnpj, msgCpf ){
	var campos = '';
	
	if (document.getElementById(form.id + ':calendarioDe.day').value == null ||
		document.getElementById(form.id + ':calendarioDe.day').value == ''   ||
		document.getElementById(form.id + ':calendarioDe.month').value == null ||
		document.getElementById(form.id + ':calendarioDe.month').value == ''   ||
		document.getElementById(form.id + ':calendarioDe.year').value == null ||
		document.getElementById(form.id + ':calendarioDe.year').value == '' ||
		document.getElementById(form.id + ':calendarioAte.day').value == null ||
		document.getElementById(form.id + ':calendarioAte.day').value == ''   ||
		document.getElementById(form.id + ':calendarioAte.month').value == null ||
		document.getElementById(form.id + ':calendarioAte.month').value == ''   ||
		document.getElementById(form.id + ':calendarioAte.year').value == null ||
		document.getElementById(form.id + ':calendarioAte.year').value == '') {
		campos = campos + msgCampo + ' ' + msgData + ' ' + msgNecessario + '\n';
	}
	
	if (campos != ''){
		alert(campos);
		campos = '';
		return false;
	}
	else{
		return true;
	}	
}

function validaPesquisarRelacionamentoIncluir(msgcampo, msgnecessario, msgparticipante,msgbanco, msgagencia, msgconta,msgdigito, msgtipoconta, msgcnpj, msgcpf, msgfinalidade){
	var campos = '';
	var objRadio = document.getElementsByName('filtroRadio');

	if (objRadio[0].checked) {
		var validarCnpj=true;
		if (allTrim(document.getElementById('relIncContasManterContrato:txtCnpj')) == null || allTrim(document.getElementById('relIncContasManterContrato:txtCnpj')) == '') {
			validarCnpj = false;
		}
		if (allTrim(document.getElementById('relIncContasManterContrato:txtFilial')) == null || allTrim(document.getElementById('relIncContasManterContrato:txtFilial')) == '') {
			validarCnpj = false;
		}
		if (allTrim(document.getElementById('relIncContasManterContrato:txtControle')) == null || allTrim(document.getElementById('relIncContasManterContrato:txtControle')) == '') {
			validarCnpj = false;
		}		
		
		if (!validarCnpj){ 
			campos = msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '\n';
			alert(campos);
			return false;	
		}	
	}else if (objRadio[1].checked) {
		var validarCpf = true;
		
		if (allTrim(document.getElementById('relIncContasManterContrato:txtCpf')) == null || allTrim(document.getElementById('relIncContasManterContrato:txtCpf')) == '') {
			validarCpf = false;
		}
		if (allTrim(document.getElementById('relIncContasManterContrato:txtControleCpf')) == null || allTrim(document.getElementById('relIncContasManterContrato:txtControleCpf')) == '') {
			validarCpf = false;
		}	
		
		if (!validarCpf){ 
			campos = msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '\n';
			alert(campos);
			return false;	
		}	
	
	}else if (objRadio[2].checked) {
		var valida = true;
		
		if (allTrim(document.getElementById('relIncContasManterContrato:banco')) == '') {
			campos += (msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '\n');
			valida = false;		
		} 
		if(allTrim(document.getElementById('relIncContasManterContrato:agencia')) == '') {
			campos += (msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n');
			valida = false;		
		} 
		if(allTrim(document.getElementById('relIncContasManterContrato:conta')) == '') {
			campos += (msgcampo + ' ' + msgconta + ' ' + msgnecessario + '\n');
			valida = false;		
		}
		if(allTrim(document.getElementById('relIncContasManterContrato:digito')) == '') {
			campos += (msgcampo + ' ' + msgdigito + ' ' + msgnecessario + '\n');
			valida = false;		
		}
		if(document.getElementById('relIncContasManterContrato:tipoConta').value == null || document.getElementById('relIncContasManterContrato:tipoConta').value == 0) {
			campos += (msgcampo + ' ' + msgtipoconta + ' ' + msgnecessario + '\n');
			valida = false;		
		 }
		
		if (!valida){ 
			alert(campos);
			return false;	
		}	
			 
	}
	
	else{ 
		if (objRadio[3].checked) {
			if ( document.getElementById('relIncContasManterContrato:finalidade').value == null || document.getElementById('relIncContasManterContrato:finalidade').value == 0) {
				alert(msgcampo + ' ' + msgfinalidade + ' ' + msgnecessario);
				document.getElementById('relIncContasManterContrato:finalidade').focus();
				return false;		
			}
		}	
	}
	return true;									
}

function validaFinalidadeRelIncConfirmar(msgcampo, msgnecessario,msgfinalidade, msgConfirma){

	if ( document.getElementById('relIncContasManterContrato2:finalidade').value == null || document.getElementById('relIncContasManterContrato2:finalidade').value == 0) {
		alert(msgcampo + ' ' + msgfinalidade + ' ' + msgnecessario);
		document.getElementById('relIncContasManterContrato2:hiddenConfirma').value = false;	
		return document.getElementById('relIncContasManterContrato2:hiddenConfirma').value;	
	}
	
	document.getElementById('relIncContasManterContrato2:hiddenConfirma').value = confirm(msgConfirma);
	
	return document.getElementById('relIncContasManterContrato2:hiddenConfirma').value;						
}

function validaRelacionamentoContas(msgcampo, msgnecessario,msgfinalidade){

if ( document.getElementById('relContasManterContrato:finalidade').value == null || document.getElementById('relContasManterContrato:finalidade').value == 0) {
		alert(msgcampo + ' ' + msgfinalidade + ' ' + msgnecessario);
		return false;		
	}
	
	return true;						
}

function validaTelaHistorico(msgcampo, msgnecessario, msgempresagestora ,msgtipocontrato,msgnumero){

	if ( document.getElementById('conManterContratoHistoricoForm:cboEmpresaGestora').value == null || document.getElementById('conManterContratoHistoricoForm:cboEmpresaGestora').value == 0) {
		alert(msgcampo + ' ' + msgempresagestora + ' ' + msgnecessario);
		document.getElementById('conManterContratoHistoricoForm:cboEmpresaGestora').focus();
		return false;		
	}
	
	if ( document.getElementById('conManterContratoHistoricoForm:cboTipoContrato').value == null || document.getElementById('conManterContratoHistoricoForm:cboTipoContrato').value == 0) {
		alert(msgcampo + ' ' + msgtipocontrato + ' ' + msgnecessario);
		document.getElementById('conManterContratoHistoricoForm:cboTipoContrato').focus();
		return false;		
	}	
	
	if ( document.getElementById('conManterContratoHistoricoForm:txtNumero').value == null || document.getElementById('conManterContratoHistoricoForm:txtNumero').value == 0) {
		alert(msgcampo + ' ' + msgnumero + ' ' + msgnecessario);
		document.getElementById('conManterContratoHistoricoForm:txtNumero').focus();
		return false;		
	}		

	return true;									
}


function validaAlterarContas(msgcampo, msgnecessario, msgradio1 ,msgradio2,msgou, msgunidadefederativa, msgmunicipio, msgSelecionarFinalidade){
	var valida = true;
	var objRadio = document.getElementsByName('rdoMunicipio');
	var msg = '';

	if (!document.getElementById('altContasManterContrato:dataTable_0:sorLista').checked && 
			!document.getElementById('altContasManterContrato:dataTable_1:sorLista').checked  && 
					!document.getElementById('altContasManterContrato:dataTable_2:sorLista').checked &&
							!document.getElementById('altContasManterContrato:dataTable_3:sorLista').checked &&
								!document.getElementById('altContasManterContrato:dataTable_4:sorLista').checked){
		alert(msgSelecionarFinalidade);
		return false;
	}
	
	
	if (document.getElementById('altContasManterContrato:dataTable_0:sorLista').checked){
		if ( !objRadio[1].checked && !objRadio[2].checked ) {
			alert(msgcampo + ' ' + msgradio1 + ' ' + msgou + ' ' + msgradio2 + ' ' + msgnecessario);
			return false;
		}	
	}		
	
	if ( objRadio[2].checked ) {

		if ( document.getElementById('altContasManterContrato:unidadeFederativa').value == null || document.getElementById('altContasManterContrato:unidadeFederativa').value == 0) {
			msg = (msgcampo + ' ' + msgunidadefederativa + ' ' + msgnecessario + '\n');
			valida = false;		
		}
		
		if ( document.getElementById('altContasManterContrato:municipio').value == null || document.getElementById('altContasManterContrato:municipio').value == 0) {
			msg += (msgcampo + ' ' + msgmunicipio + ' ' + msgnecessario);
			valida = false;		
		}	
		
	}
	
	if ( document.getElementById('altContasManterContrato:txtApelido').value.search( /[^ a-z0-9]/i ) != -1 )
	{
		msg += "N\u00E3o \u00E9 permitido caracteres especiais no campo Apelido" ;
		valida = false;
	}
	
	//if (form.elements[i].value == ''){
		
	//	msg += (msgcampo + ' ' + 'Apelido \u00E9 obrigat\u00F3rio' + ' ' + msgnecessario);
	//}
	
	if (msg != '')
	   alert(msg);
	
	return valida;									
}

function validaPesquisarRelacionamentoSubstituir(msgcampo, msgnecessario, msgparticipante,msgbanco, msgagencia, msgconta,msgdigito, msgtipoconta, msgcnpj, msgcpf, msgfinalidade){
	var campos = '';
	var objRadio = document.getElementsByName('filtroRadio');

	if (objRadio[0].checked) {
		var validarCnpj=true;
		if (allTrim(document.getElementById('subsContasManterContrato:txtCnpj')) == null || allTrim(document.getElementById('subsContasManterContrato:txtCnpj')) == '') {
			validarCnpj = false;
		}
		if (allTrim(document.getElementById('subsContasManterContrato:txtFilial')) == null || allTrim(document.getElementById('subsContasManterContrato:txtFilial')) == '') {
			validarCnpj = false;
		}
		if (allTrim(document.getElementById('subsContasManterContrato:txtControle')) == null || allTrim(document.getElementById('subsContasManterContrato:txtControle')) == '') {
			validarCnpj = false;
		}		
		
		if (!validarCnpj){ 
			campos = msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '\n';
			alert(campos);
			return false;	
		}	
	}else if (objRadio[1].checked) {
		var validarCpf = true;
		
		if (allTrim(document.getElementById('subsContasManterContrato:txtCpf')) == null || allTrim(document.getElementById('subsContasManterContrato:txtCpf')) == '') {
			validarCpf = false;
		}
		if (allTrim(document.getElementById('subsContasManterContrato:txtControleCpf')) == null || allTrim(document.getElementById('subsContasManterContrato:txtControleCpf')) == '') {
			validarCpf = false;
		}	
		
		if (!validarCpf){ 
			campos = msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '\n';
			alert(campos);
			return false;	
		}	
	
	}else if (objRadio[2].checked) {
		var valida = true;
		
		if (allTrim(document.getElementById('subsContasManterContrato:banco')) == '') {
			campos += (msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '\n');
			valida = false;		
		} 
		if(allTrim(document.getElementById('subsContasManterContrato:agencia')) == '') {
			campos += (msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n');
			valida = false;		
		} 
		if(allTrim(document.getElementById('subsContasManterContrato:conta')) == '') {
			campos += (msgcampo + ' ' + msgconta + ' ' + msgnecessario + '\n');
			valida = false;		
		}
		if(allTrim(document.getElementById('subsContasManterContrato:digito')) == '') {
			campos += (msgcampo + ' ' + msgdigito + ' ' + msgnecessario + '\n');
			valida = false;		
		}
		if(document.getElementById('subsContasManterContrato:tipoConta').value == null || document.getElementById('subsContasManterContrato:tipoConta').value == 0) {
			campos += (msgcampo + ' ' + msgtipoconta + ' ' + msgnecessario + '\n');
			valida = false;		
		 }
		
		if (!valida){ 
			alert(campos);
			return false;	
		}	
			 
	}else if(objRadio[3].checked){
		var validaFinalidade = true;
		
		if(document.getElementById('subsContasManterContrato:finalidade').value == null || document.getElementById('subsContasManterContrato:finalidade').value == 0) {
			campos += (msgcampo + ' ' + msgfinalidade + ' ' + msgnecessario + '\n');
			validaFinalidade = false;		
		 }
		
		if (!validaFinalidade){ 
			alert(campos);
			return false;	
		}	
	}
	
	return true;									
}

function validaHistoricoConta(msgcampo, msgnecessario, msgData){
	var campos;
	campos = '';
	
	if (document.getElementById('histContasManterContrato:calendarioDe.day').value == null ||
		document.getElementById('histContasManterContrato:calendarioDe.day').value == ''   ||
		document.getElementById('histContasManterContrato:calendarioDe.month').value == null ||
		document.getElementById('histContasManterContrato:calendarioDe.month').value == ''   ||
		document.getElementById('histContasManterContrato:calendarioDe.year').value == null ||
		document.getElementById('histContasManterContrato:calendarioDe.year').value == '' ||
		document.getElementById('histContasManterContrato:calendarioAte.day').value == null ||
		document.getElementById('histContasManterContrato:calendarioAte.day').value == ''   ||
		document.getElementById('histContasManterContrato:calendarioAte.month').value == null ||
		document.getElementById('histContasManterContrato:calendarioAte.month').value == ''   ||
		document.getElementById('histContasManterContrato:calendarioAte.year').value == null ||
		document.getElementById('histContasManterContrato:calendarioAte.year').value == '') {
		campos = campos + msgcampo + ' ' + msgData + ' ' + msgnecessario + '\n';
	}
	
	if (campos != ''){
		alert(campos);
		campos = '';
		return false;
	}else{
		return true;
	}	
}

function validaClassificacao(msgcampo, msgnecessario, msgclassificacao) {
	var objClassificacao = document.getElementById('altParticipantesManterContrato:cboClassificacaoAreaNegocio');
	if (objClassificacao.value == 0) {
		alert(msgcampo + ' ' + msgclassificacao + ' ' + msgnecessario);
		objClassificacao.focus();
		return false;
	}

	return true;
}

function validarProxCampoIdentClienteContratoContas(form, flagPesquisa){
	var objRdoCliente = document.getElementsByName('filtroRadio');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCnpjFilial = document.getElementById(form.id + ':txtFilial');
	var campoCnpjControle = document.getElementById(form.id + ':txtControle');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoCpfControle = document.getElementById(form.id + ':txtControleCpf');
	var campoBanco = document.getElementById(form.id + ':banco');
	var campoAgencia = document.getElementById(form.id + ':agencia');
	var campoConta = document.getElementById(form.id + ':conta');
	var campoDigito = document.getElementById(form.id + ':digito');
	var campoFinalidade = document.getElementById(form.id + ':finalidade');
	
	if(objRdoCliente[0].checked){
		campoCnpj.disabled = false;
		campoCnpj.focus();
		campoCnpjFilial.disabled = false;
		campoCnpjControle.disabled = false;
		
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoDigito.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		campoDigito.value = '';
		
		campoFinalidade.disabled = true;
		
		campoCpf.value = '';
		campoCpfControle.value = '';
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoDigito.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
			campoFinalidade.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[1].checked){
		campoCpf.disabled = false;
		campoCpf.focus();
		campoCpfControle.disabled = false;
	
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoDigito.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		campoDigito.value = '';
		
		campoFinalidade.disabled = true;
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoDigito.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
			campoFinalidade.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[2].checked){
		campoBanco.disabled = false;
		campoAgencia.disabled = false;
		campoConta.disabled = false;
		campoDigito.disabled = false;
		campoBanco.value = 237;
		campoAgencia.focus();
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoFinalidade.disabled = true;
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoDigito.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
			campoFinalidade.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[3].checked){
		campoFinalidade.disabled = false;
		campoFinalidade.focus();
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoDigito.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		campoDigito.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoDigito.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
			campoFinalidade.disabled = true;
		}
		
		return true;
	}else{
		return false;
	}
}

function validarProxCampoIdentClienteContratoContasIncluir(form, flagPesquisa){
	var objRdoCliente = document.getElementsByName('filtroRadio');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCnpjFilial = document.getElementById(form.id + ':txtFilial');
	var campoCnpjControle = document.getElementById(form.id + ':txtControle');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoCpfControle = document.getElementById(form.id + ':txtControleCpf');
	var campoBanco = document.getElementById(form.id + ':banco');
	var campoAgencia = document.getElementById(form.id + ':agencia');
	var campoConta = document.getElementById(form.id + ':conta');
	var campoDigito = document.getElementById(form.id + ':digito');
	
	if(objRdoCliente[0].checked){
		campoCnpj.disabled = false;
		campoCnpj.focus();
		campoCnpjFilial.disabled = false;
		campoCnpjControle.disabled = false;
		
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoDigito.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		campoDigito.value = '';
		
		campoCpf.value = '';
		campoCpfControle.value = '';
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoDigito.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[1].checked){
		campoCpf.disabled = false;
		campoCpf.focus();
		campoCpfControle.disabled = false;
	
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoDigito.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		campoDigito.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoDigito.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[2].checked){
		campoBanco.disabled = false;
		campoAgencia.disabled = false;
		campoConta.disabled = false;
		campoDigito.disabled = false;
		campoBanco.value = 237;
		campoAgencia.focus();
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoDigito.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else{
		document.getElementById(form.id + ':btnConsultar').disabled = true;
		return false;
	}
}

function confirmarFneb(url){
	loadModalJQuery();
	if (url != '' ) {
		document.location.href = url;
	} else {
		return false;
	}
}


function selecionarTodosParticipantes(form, check){

	for(i=0; i<form.elements.length; i++)	
	{
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
		
	}	
	
	
	document.getElementById(form.id + ':btnSelecionar').disabled = !check.checked;	
}
