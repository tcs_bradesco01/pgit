function limparNomeAgencia(form) {
	form.elements[form.id + ':txtNomeAgencia'].value = '';
}

function checarCamposObrigatorios(form, arrCampos, msgCampo, msgNecessario) {
	var campos = '';

	for(var i = 0; i < arrCampos.length; i++) {
		var campo = document.getElementById(form.id + ':' + arrCampos[i].id);
		if (campo != null) {
			if (allTrim(campo) == '') {
				campos = campos + msgCampo + ' ' + arrCampos[i].label + ' ' + msgNecessario + '\n';
			}
		}
	}

	var hidden = document.getElementById(form.id + ':hiddenObrigatoriedade');

	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
		campos = '';
		return false;
	}else{
		hidden.value  ='T';
		return true;
	}
}