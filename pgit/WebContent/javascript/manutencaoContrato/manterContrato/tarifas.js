function checarObrigatoriedadeAlterarCondicoesCobranca(form, msgCampo, msgNecessario, msgPeriodicidade, msgDiaFec, msgDiaFec2, msgQtddDia){

	var campos, hidden;
	campos = '';

	if (document.getElementById(form.id + ':peridiciodade').value == 0 ){
		campos = campos + msgCampo + ' ' + msgPeriodicidade + ' ' + msgNecessario + '\n';
	}else{
		if (document.getElementById(form.id + ':peridiciodade').value == 4 ){
			if(document.getElementById(form.id + ':chkDiaFechamento').checked == false){
				if (document.getElementById(form.id + ':txtDiaFechamento').value == ''){
					campos = campos + msgCampo + ' ' + msgDiaFec + ' ' + msgNecessario + '\n';
				}
			}
			
			if(document.getElementById(form.id + ':chkQtdDiaCobranca').checked == false){
				if (document.getElementById(form.id + ':txtQuantDia').value == ''){
					campos = campos + msgCampo + ' ' + msgQtddDia + ' ' + msgNecessario + '\n';
				}
			}
		}else{
			if(document.getElementById(form.id + ':chkQtdDiaCobranca').checked == false){
				if (document.getElementById(form.id + ':txtQuantDia').value == ''){
					campos = campos + msgCampo + ' ' + msgQtddDia + ' ' + msgNecessario + '\n';
				}
			}
		}
	}
	
	if (campos != ''){
		document.getElementById(form.id + ':hiddenObrigatoriedade').value = 'F';
		alert(campos);
		campos = '';
		return false;
	}else{
		document.getElementById(form.id + ':hiddenObrigatoriedade').value = 'T';
		return true;
	}
}

function checarObrigatoriedadeAlterarCondicoesReajuste(form, msgCampo, msgNecessario, msgTipoReajuste, msgQuantMes, msgIndiceEconomico, msgPercentualIndi,
													   msgPercentualIndi2, msgPercentualIndi3, msgPercentualFlex, msgPercentualFlex2, msgPercentualFlex3){

	var campos, hidden;
	campos = '';
	
	var percentualIndiceAux = 0.00;
	var percentualFlexibilizacao = 0.00;
	
	hidden = document.getElementById(form.id + ':hiddenObrigatoriedade');

	var objTipoReajuste = document.getElementById(form.id + ':tipoReajuste');
	if (objTipoReajuste.value == '0') {
		campos = campos + msgCampo + ' ' + msgTipoReajuste + ' ' + msgNecessario + '\n';
	} else if (objTipoReajuste.value != '1') {
		
		var objQuantMes = document.getElementById(form.id + ':txtQuantMes');
		if (allTrim(objQuantMes) == ''){
			campos = campos + msgCampo + ' ' + msgQuantMes + ' ' + msgNecessario + '\n';
		}

		if (objTipoReajuste.value == '3') { // REFERENCIA INDICE ECONOMICO
			var objIndiceEconomico = document.getElementById(form.id + ':indiceEconomico');
			if (objIndiceEconomico.value == '0'){
				campos = campos + msgCampo + ' ' + msgIndiceEconomico + ' ' + msgNecessario + '\n';
			}

			var objPercentualIndi = document.getElementById(form.id + ':txtPercentualIndi');
			if (objPercentualIndi.value == ''){
				campos = campos + msgCampo + ' ' + msgPercentualIndi + ' ' + msgNecessario + '\n';
			} else {
				percentualIndiceAux = 	parseFloat(objPercentualIndi.value.replace(',','.'));		

				if (percentualIndiceAux == 0.00){
					campos = campos + msgPercentualIndi3 + '\n';
				} else if (percentualIndiceAux > 100.00){
					campos = campos + msgPercentualIndi2 + '\n';
				}
			}
		} else if (objTipoReajuste.value == '4') { // REFERENCIA CATALOGO
			var objPercFlex = document.getElementById(form.id + ':txtPercentualFlex');
			if (objPercFlex.value == ''){
				campos = campos + msgCampo + ' ' + msgPercentualFlex + ' ' + msgNecessario + '\n';
			} else {
				percentualFlexibilizacao = 	parseFloat(objPercFlex.value.replace(',','.'));		

				if (percentualFlexibilizacao == 0.00){
					campos = campos + msgPercentualFlex3 + '\n';
				} else if (percentualFlexibilizacao > 100.00){
					campos = campos + msgPercentualFlex2 + '\n';
				}
			}
		}
	}

	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
		campos = '';
	}else{
		hidden.value  ='T';
	}
}

function validaCamposHist(form, msgCampo, msgNecessario, tipoServico, modalidade, operacao){

var hidden;
var mensagem = "";

	for(i=0; i<form.elements.length; i++){
	
		if(form.elements[i].id == form.id + ':hiddenObrigatoriedade'){
					hidden = form.elements[i];
			}
	
		if(form.elements[i].id == form.id + ':tipoServico'){
				if(form.elements[i].value == 0){
					mensagem += msgCampo + ' ' + tipoServico + ' ' + msgNecessario+  '\n';
				}
		}
		//if(form.elements[i].id == form.id + ':modalidade'){
		//		if(form.elements[i].value == 0){
		//			mensagem += msgCampo + ' ' + modalidade + ' ' + msgNecessario+  '\n';
		//		}
		//}
		if(form.elements[i].id == form.id + ':operacao'){
				if(form.elements[i].value == 0){
					mensagem += msgCampo + ' ' + operacao + ' ' + msgNecessario+  '\n';
				}
		}
		
	}
	if(mensagem != ''){
		alert(mensagem);
		hidden.value = "F";
		
	}else
		hidden.value = "V";	
}

function limitarDigitosRange(campo) {
	
	var form = "altCondicoesCobrancaForm";
	var peridiciodade = document.getElementById(form + ':peridiciodade');
	var min = 1; 
	var max;
	
	if (peridiciodade.value != '4') {
		max = 5;
	}else{
		max = 15;
	}
	
	if (campo && campo.value != "") {

		var valor = parseInt(campo.value);
		if (valor < parseInt(min) || valor > parseInt(max)) {
			campo.value = "";
			return false;
		}
	}
	return true;
}



function limitarDigitosRange1(campo) {
	var form = "altCondicoesCobrancaForm";
	var min = 1; 
	var max = 28;
	if (campo && campo.value != "") {

		var valor = parseInt(campo.value);
		if (valor < parseInt(min) || valor > parseInt(max)) {
			campo.value = "";
			return false;
		}
	}
	return true;
}