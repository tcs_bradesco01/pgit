function validarAltLayoutsAvancar(msgcampo, msgnecessario, msgNivelControle, msgTipoControle, msgPerdcInicCont, msgTpRejAcol, msgPercRegInex, msgintervalo){

	var campos;
	campos = '';

	if(document.getElementById('altLayoutsArquivos1:cboNivelControle').value == 0) {
		campos = campos + msgcampo + ' ' + msgNivelControle + ' ' + msgnecessario+ '\n';
	}else if((document.getElementById('altLayoutsArquivos1:cboNivelControle').value == 1) || (document.getElementById('altLayoutsArquivos1:cboNivelControle').value == 2)){
		if(document.getElementById('altLayoutsArquivos1:cboTipoControle').value == 0) {
			campos = campos + msgcampo + ' ' + msgTipoControle + ' ' + msgnecessario+ '\n';
		}
	}
	
	if(document.getElementById('altLayoutsArquivos1:cboPeriodicidade').value == 0) {
		campos = campos + msgcampo + ' ' + msgPerdcInicCont + ' ' + msgnecessario+ '\n';
	}
	
	var objRadioFiltro = document.getElementsByName('altLayoutsArquivos1:radioFiltro');
		if (!objRadioFiltro[1].checked && !objRadioFiltro[2].checked) {
			campos = campos + msgcampo + ' ' + msgTpRejAcol + ' ' + msgnecessario + '\n';
		}else if(objRadioFiltro[1].checked){
			if(document.getElementById('altLayoutsArquivos1:inpPercRegIncons').value == '') {
				campos = campos + msgcampo + ' ' + msgPercRegInex + ' ' + msgnecessario+ '\n';
			}
			else{
				if ((parseInt(document.getElementById('altLayoutsArquivos1:inpPercRegIncons').value,10)) <= 0 || 
					(parseInt(document.getElementById('altLayoutsArquivos1:inpPercRegIncons').value,10)) > 100){
					campos = campos + msgcampo + ' ' + msgPercRegInex + ' ' + msgintervalo + '\n';
				}	
			}
		}else if(objRadioFiltro[2].checked){
			document.getElementById('altLayoutsArquivos1:inpPercRegIncons').disabled = false;
		}
		
	

	if (campos != ''){
		document.getElementById('altLayoutsArquivos1:hiddenObrigatoriedade').value = 'F';
		alert(campos);
	}else{
		document.getElementById('altLayoutsArquivos1:hiddenObrigatoriedade').value = 'T';
	}			
}

function confirmarAlteracao(msg){
   if (!confirm(msg)) 
	 return false;

   return true;		 
}

function validarIncRetornoLayoutsAvancar(msgcampo, msgnecessario, msgTipoRetorno, msgIntervaloGeracao, msgTipoMontagem,
										 msgTipoOcorrenciasArquivo, msgTipoClassificacaoRetorno, msgOrdenacaoRegistros){


	 var campos;
	 campos = '';

	if(document.getElementById('incRetornoLayoutsArquivos1:cboTipoRetorno').value == 0) {
		campos = campos + msgcampo + ' ' + msgTipoRetorno + ' ' + msgnecessario+ '\n';
	}
	
	if(document.getElementById('incRetornoLayoutsArquivos1:intervaloGeracao').value == 0) {
		campos = campos + msgcampo + ' ' + msgIntervaloGeracao + ' ' + msgnecessario+ '\n';
	}
	
	if(document.getElementById('incRetornoLayoutsArquivos1:tipoMontagem').value == 0) {
		campos = campos + msgcampo + ' ' + msgTipoMontagem + ' ' + msgnecessario+ '\n';
	}
	
	if(document.getElementById('incRetornoLayoutsArquivos1:tipoOcorrenciasArquivo').value == 0) {
		campos = campos + msgcampo + ' ' + msgTipoOcorrenciasArquivo + ' ' + msgnecessario+ '\n';
	}
	
	if(document.getElementById('incRetornoLayoutsArquivos1:tipoClassificacaoRetorno').value == 0) {
		campos = campos + msgcampo + ' ' + msgTipoClassificacaoRetorno + ' ' + msgnecessario+ '\n';
	}
	
	var objRadio = document.getElementsByName('incRetornoLayoutsArquivos1:ordenacaoRegistros');
	
	if ((!objRadio[1].checked) && (!objRadio[2].checked) && (!objRadio[3].checked)) {
		campos = campos + msgcampo + ' ' + msgOrdenacaoRegistros + ' ' + msgnecessario+ '\n';
	}

	if (campos != ''){
		document.getElementById('incRetornoLayoutsArquivos1:hiddenObrigatoriedade').value = 'F';
		alert(campos);
	}else{
		document.getElementById('incRetornoLayoutsArquivos1:hiddenObrigatoriedade').value = 'T';
		
	}
	
}

function validarAltRetornoLayoutsAvancar(msgcampo, msgnecessario, msgIntervaloGeracao, msgTipoMontagem, msgTipoOcorrenciasArquivo,
										 msgTipoClassificacaoRetorno, msgOrdenacaoRegistros){

	 var campos;
	 campos = '';
	 
	if(document.getElementById('altRetornoLayoutsArquivos1:intervaloGeracao').value == 0) {
		campos = campos + msgcampo + ' ' + msgIntervaloGeracao + ' ' + msgnecessario+ '\n';
	}
	
	if(document.getElementById('altRetornoLayoutsArquivos1:tipoMontagem').value == 0) {
		campos = campos + msgcampo + ' ' + msgTipoMontagem + ' ' + msgnecessario+ '\n';
	}
	
	if(document.getElementById('altRetornoLayoutsArquivos1:tipoOcorrenciasArquivo').value == 0) {
		campos = campos + msgcampo + ' ' + msgTipoOcorrenciasArquivo + ' ' + msgnecessario+ '\n';
	}
	
	if(document.getElementById('altRetornoLayoutsArquivos1:tipoClassificacaoRetorno').value == 0) {
		campos = campos + msgcampo + ' ' + msgTipoClassificacaoRetorno + ' ' + msgnecessario+ '\n';
	}
	
	var objRadio = document.getElementsByName('altRetornoLayoutsArquivos1:ordenacaoRegistros');
	
	if ((!objRadio[1].checked) && (!objRadio[2].checked) && (!objRadio[3].checked)) {
		campos = campos + msgcampo + ' ' + msgOrdenacaoRegistros + ' ' + msgnecessario+ '\n';
	}

	if (campos != ''){
		alert(campos);
		campos = '';
		return false;
	}else{
		return true;
	}
}

function validarSubstituirLayoutArquivos(msgcampo, msgnecessario, msglayout, msgaplicativoformatacao,msgremessaprincipal, msgalternativoremessa, 
 	msgprincipalretorno, msgalternativoretorno, msgempresaresponsaveltransmissao, msgnivelcontrole, msgtipocontrole, msgperiodicidade,
 	msgusername, msgnumeromaximoremessa, msginppercregincons, msgtiporejeicaoacolhimento, msgremessa, msgretorno,msgcentrocusto,msgcodigoserieaplicativo  ){

	var campos = '';
	var valido = true;
	
	/*Validação dos Combos*/ 
	if(document.getElementById('subLayoutsArquivos1:layout').value == null || document.getElementById('subLayoutsArquivos1:layout').value == 0) {
		campos = campos + msgcampo + ' ' + msglayout + ' ' + msgnecessario+ '\n';
		valido = false;
	}
	 
	if(document.getElementById('subLayoutsArquivos1:aplicativoFormatacao').value == null || document.getElementById('subLayoutsArquivos1:aplicativoFormatacao').value == 0) {
		campos = campos + msgcampo + ' ' + msgcentrocusto + ' ' + msgnecessario+ '\n';
		valido = false;
	}
	
	/*
	if(document.getElementById('subLayoutsArquivos1:centroCusto').value == null || document.getElementById('subLayoutsArquivos1:centroCusto').value == 0) {
		campos = campos + msgcampo + ' ' + msgaplicativoformatacao + ' ' + msgnecessario+ '\n';
		valido = false;
	}	
	
	if(document.getElementById('subLayoutsArquivos1:codigoSerieAplicativo').value == '') {
		campos = campos + msgcampo + ' ' + msgcodigoserieaplicativo  + ' ' + msgnecessario+ '\n';
		valido = false;		
	}	
	
	//MEIO DE TRANSMISSÃO - REMESSA
	if(document.getElementById('subLayoutsArquivos1:principalRemessa').value == null || document.getElementById('subLayoutsArquivos1:principalRemessa').value == 0) {
		campos = campos + msgcampo + ' ' + msgremessa + ' ' +  msgremessaprincipal + ' ' + msgnecessario+ '\n';
		valido = false;		
	}
	
	if(document.getElementById('subLayoutsArquivos1:alternativoRemessa').value == null || document.getElementById('subLayoutsArquivos1:alternativoRemessa').value == 0) {
		campos = campos + msgcampo + ' ' + msgremessa + ' ' + msgalternativoremessa + ' ' + msgnecessario+ '\n';
		valido = false;		
	}	

	//MEIO DE TRANSMISSÃO - RETORNO
	if(document.getElementById('subLayoutsArquivos1:principalRetorno').value == null || document.getElementById('subLayoutsArquivos1:principalRetorno').value == 0) {
		campos = campos + msgcampo + ' ' + msgretorno + ' ' + msgprincipalretorno + ' ' + msgnecessario+ '\n';
		valido = false;		
	}		 
	
	if(document.getElementById('subLayoutsArquivos1:alternativoRetorno').value == null || document.getElementById('subLayoutsArquivos1:alternativoRetorno').value == 0) {
		campos = campos + msgcampo + ' ' + msgretorno + ' ' + msgalternativoretorno + ' ' + msgnecessario+ '\n';
		valido = false;		
	}
	
	if(document.getElementById('subLayoutsArquivos1:username').value == '') {
		campos = campos + msgcampo + ' ' + msgusername  + ' ' + msgnecessario+ '\n';
		valido = false;		
	}	
	
	if(document.getElementById('subLayoutsArquivos1:empresaResponsavelTransmissao').value == '' ) {
		campos = campos + msgcampo + ' ' + msgempresaresponsaveltransmissao  + ' ' + msgnecessario+ '\n';
		valido = false;		
	}	*/	
	
	if(document.getElementById('subLayoutsArquivos1:cboNivelControle').value == null || document.getElementById('subLayoutsArquivos1:cboNivelControle').value == 0) {
		campos = campos + msgcampo + ' ' + msgnivelcontrole  + ' ' + msgnecessario+ '\n';
		valido = false;		
	}
	
	if(document.getElementById('subLayoutsArquivos1:cboTipoControle').value == null || document.getElementById('subLayoutsArquivos1:cboTipoControle').value == 0) {
		campos = campos + msgcampo + ' ' + msgtipocontrole  + ' ' + msgnecessario+ '\n';
		valido = false;		
	}					

	if(document.getElementById('subLayoutsArquivos1:cboPeriodicidade').value == null || document.getElementById('subLayoutsArquivos1:cboPeriodicidade').value == 0) {
		campos = campos + msgcampo + ' ' + msgperiodicidade  + ' ' + msgnecessario+ '\n';
		valido = false;		
	}
	
	if(document.getElementById('subLayoutsArquivos1:numeroMaximoRemessa').value == '') {
		campos = campos + msgcampo + ' ' +msgnumeromaximoremessa + ' ' + msgnecessario+ '\n';
		valido = false;		
	}			

	var objRadio = document.getElementsByName('subLayoutsArquivos1:radioFiltro');
	if( !objRadio[1].checked && !objRadio[2].checked ) {
		campos = campos + msgcampo + ' ' + msgtiporejeicaoacolhimento + ' ' + msgnecessario+ '\n';
		valido = false;		
	}
	
	/*if(document.getElementById('subLayoutsArquivos1:inpPercRegIncons').value == '') {
		campos = campos + msgcampo + ' ' + msginppercregincons + ' ' + msgusername  + ' ' + msgnecessario+ '\n';
		valido = false;		
	}*/
	
	if (!valido){
	   alert(campos);	
	   return false;
	}
	
	return true;
}

function validarAlterarServico(msg, msgLayout){

		
		
	for(i=0; i< document.getElementById('altServicoLayout:hiddenTamanho').value; i++){
	
		if(document.getElementById('altServicoLayout:dataTable_' + i + ':layoutContratado').value == null || document.getElementById('altServicoLayout:dataTable_' + i + ':layoutContratado').value == 0) {
		
			alert(msg);
			return false;
		}
	}
	
	/*
	for(i=0; i< document.getElementById('altServicoLayout:hiddenTamanho').value; i++){
		
		 layout =  document.getElementById('altServicoLayout:dataTable_' + i + ':layoutContratado').value; 
	 		
	 		for(j=0; j< document.getElementById('altServicoLayout:hiddenTamanho').value; j++){
	 		
				 if((i != j) && (layout == document.getElementById('altServicoLayout:dataTable_' + j + ':layoutContratado').value) ){
				 
				 			alert(msgLayout);
				 			return false;
				 }
			}	
	}
	*/
	return true;
	
}

function confirmarSubstituicao(msg){
   if (!confirm(msg)) 
	 return false;

   return true;		 
}

function desabilitaCampo(form, radio){
	//Seu radio
	
	if(radio.value == 2){
		document.getElementById('altLayoutsArquivos1:inpPercRegIncons').disabled = true;
		document.getElementById('altLayoutsArquivos1:inpPercRegIncons').value = '0.00';
		
	}else{
		document.getElementById('altLayoutsArquivos1:inpPercRegIncons').disabled = false;
	}
	
}

function checarIncluirLayout(form, msgCampo, msgNecessario, msgLayout, msgSituacao, msgResponsavel, msgPercentual){

	var msgValidacao;
	msgValidacao = '';
	
	var objRdo = document.getElementsByName('rdoSituacao');
		
	if (document.getElementById(form.id +':cboLayout').value == '0') {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgLayout + ' ' + msgNecessario + '!\n';
	} 
	
	if (!objRdo[1].checked && !objRdo[2].checked) {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgSituacao + ' ' + msgNecessario + '!\n';
	} 
	
	if (document.getElementById(form.id +':cboResponsavel').value == '0') {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgResponsavel + ' ' + msgNecessario + '!\n';
	} 
	
	if (document.getElementById(form.id +':cboResponsavel').value == '1' || document.getElementById(form.id +':cboResponsavel').value == '3' ) {
		if (document.getElementById(form.id +':txtPercentual').value == "") {
			msgValidacao = msgValidacao + msgCampo + ' ' + msgPercentual + ' ' + msgNecessario + '!\n';
		} 
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		msgValidacao = '';
		return false;
	}else{
		return true;
	}
	
}

function desabilitaConsultar(form, msgcampo, msgnecessario, msgdatainclusao, msglayout){

	var msgValidacao = '';	
	
		
	if (document.getElementById(form.id + ':calendarioDe.day').value == null ||
		allTrim(document.getElementById(form.id + ':calendarioDe.day')) == ''   ||
		document.getElementById(form.id + ':calendarioDe.month').value == null ||
		allTrim(document.getElementById(form.id + ':calendarioDe.month')) == ''   ||
		document.getElementById(form.id + ':calendarioDe.year').value == null ||
		allTrim(document.getElementById(form.id + ':calendarioDe.year')) == '' ||
		document.getElementById(form.id + ':calendarioAte.day').value == null ||
		allTrim(document.getElementById(form.id + ':calendarioAte.day')) == ''   ||
		document.getElementById(form.id + ':calendarioAte.month').value == null ||
		allTrim(document.getElementById(form.id + ':calendarioAte.month')) == ''   ||
		document.getElementById(form.id + ':calendarioAte.year').value == null ||
		allTrim(document.getElementById(form.id + ':calendarioAte.year')) == '') {
		
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdatainclusao + ' ' + msgnecessario + '!' + '\n';	
	}
	
	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	

	return true;
	
}

function selecionarTodos(form, check){
	for(i=0; i<form.elements.length; i++) {
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].click();
			form.elements[i].checked = check.checked;
		}	
		
	}		
}
		

