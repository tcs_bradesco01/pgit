function validarTamanhoEDigitado(element) {
	var tecla = window.event.keyCode;
	
	if (tecla == 13) {
		event.keyCode = 0; 
		event.returnValue = false;
	} else if (element.value.length > 200) {
		element.value = element.value.substring( 0, 200 );
	}
}

function validarEnter() {
	var tecla = window.event.keyCode;
	
	if (tecla == 13) {
		event.keyCode = 0; 
		event.returnValue = false;
	}
}