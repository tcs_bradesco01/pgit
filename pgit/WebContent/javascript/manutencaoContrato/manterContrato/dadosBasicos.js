function checarObrigatoriedade(formId, msgCampo, msgNecessario, msgDescricao, msgIdioma, msgMoeda, msgSetor, msgCodigoGerente, msgGerente, msgFormaAutorizacao) {

	var campos;
	campos = '';
	
	var objTxtDescricao = document.getElementById(formId + ':txtDescricao');
	if (objTxtDescricao != null && allTrim(objTxtDescricao) == '') {
		campos = campos + msgCampo + ' ' + msgDescricao + ' ' + msgNecessario + '\n';
	}
	
	var objIdioma = document.getElementById(formId + ':idioma');
	if (objIdioma != null && objIdioma.value == '0') {
		campos = campos + msgCampo + ' ' + msgIdioma + ' ' + msgNecessario + '\n';
	}
	
	var objMoeda = document.getElementById(formId + ':moeda');
	if (objMoeda != null && objMoeda.value == '0') {
		campos = campos + msgCampo + ' ' + msgMoeda + ' ' + msgNecessario + '\n';
	}
	
	var objSetor = document.getElementById(formId + ':setor');
	if (objSetor != null && objSetor.value == '0') {
		campos = campos + msgCampo + ' ' + msgSetor + ' ' + msgNecessario + '\n';
	}

	var objRadioFormaAutorizacao = document.getElementsByName(formId + ':radioFormaAutorizacao');
	if (objRadioFormaAutorizacao != null && objRadioFormaAutorizacao.length > 1) {
		var radioChecado = false;
		for (var i = 1; i < objRadioFormaAutorizacao.length; i++) {
			if (objRadioFormaAutorizacao[i].checked) {
				radioChecado = true;
			}
		}

		if (!objRadioFormaAutorizacao[1].disabled && !radioChecado) {
			campos = campos + msgCampo + ' ' + msgFormaAutorizacao + ' ' + msgNecessario + '\n';
		}
	}

	var objCodigoGerente = document.getElementById(formId + ':codigoGerente');
	if (objCodigoGerente != null && allTrim(objCodigoGerente) == '') {
		campos = campos + msgCampo + ' ' + msgCodigoGerente + ' ' + msgNecessario + '\n';
	}

	var objTxtNomeGerente = document.getElementById(formId + ':txtNomeGerente');
	if (objTxtNomeGerente != null && allTrim(objTxtNomeGerente) == '') {
		campos = campos + msgCampo + ' ' + msgGerente + ' ' + msgNecessario + '\n';
	}

	if (campos != '') {
		alert(campos);
		return false;
	} else {
		return true;
	}
}

function limparNomeGerente (form){
	
		form.elements[form.id + ':txtNomeGerente'].value = '';
		
}

function checarObrigatoriedadeEncerrar(form, msgCampo, msgNecessario, msgMotivoSituacao){

	var campos, hidden;
	campos = '';
	
	for(i=0; i<form.elements.length; i++){
		
		if (form.elements[i].id == form.id + ':hiddenObrigatoriedade'){
				hidden = form.elements[i];
		}
		
		if (form.elements[i].id == form.id + ':motivoSituacao'){
			if (form.elements[i].value == '0'){
				campos = campos + msgCampo + ' ' + msgMotivoSituacao + ' ' + msgNecessario + '\n';
			}
		}
		
	}
	
	if (campos != ''){
		hidden.value  ='F';
		alert(campos);
		campos = '';
	}else{
		hidden.value  ='T';
	}
}



		
		