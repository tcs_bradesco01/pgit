function checarQtdModalidade(form, msg){
	qtdModalidades = document.getElementById("incServicosManterContrato2Form:hiddenCountCheckedModalidade").value;
	if (qtdModalidades > 10){
		alert(msg);
	}
}

function checarQtdModalidade2(form, msg){
	qtdModalidades = document.getElementById("incModServicosManterContrato:hiddenCountCheckedModalidade").value;
	if (qtdModalidades > 10){
		alert(msg);
	}
}


function validaServicosConfiguracao(msgCampo, msgNecessario, 
										msgAberturaContaBancoPostalBradescoSeguros,msgAcaoNaoComprovacao,msgAdesaoSacadoEletronico,msgAgendamentoDebitosPendentesVeiculos,
										msgAgendarTituloRastreadoFilial,msgAgendarTituloRastreadoProprio,msgAgruparCorrespondencias,msgAutorizacaoComplementarAgencia,
										msgBloquearEmissaoPapeleta,msgCadastroUtilizadoRecadastramento,msgCadastroConsultaEnderecoEnvio,msgCapturarTitulosDataRegistro,
										msgCobrarTarifasFuncionarios,msgCodigoFormulario,msgCondicaoEnquadramento,msgControleExpiracaoCredito,
										msgCriterioCompostoEnquadramento,msgCriterioPrincipalEnquadramento,msgDataInicioBloqueioPapeleta,msgDataInicioRastreamento,
										msgDataFimPeriodoAcertoDados,msgDataFimRecadastramento,msgDataInicioPeriodoAcertoDados,msgDataInicioRecadastramento,
										msgDataLimiteEnquadramentoConvenioContaSalario,msgDataLimiteVinculoClienteBeneficiario,msgDemonstraInformacoesAreaReservada,msgDestinoEnvio,
										msgDestinoEnvioCorrespondencia,msgEfetuaConsistenciaEspecieBeneficio,
										msgEmissaoAntecipadaCartaoContaSalario,msgEmiteAvisoComprovacaoVida,msgEmiteMsgRecadastramento,msgExigeLiberacaoLoteProcessado,
										msgFormaAutorizacaoPagamentos,msgFormaEnvioPagamentos,msgFormaManutencaoCadastroFavorecidos,msgFormaManutencaoCadastroProcuradores,
										msgFormularioImpressao,msgGerarLancamentoFuturoCredito,msgGerarLancamentoFuturoDebito,msgGerarLancamentoProgramado,
										msgGerarRetornoOperacoesRealizadasInternet,msgInformaAgenciaContaCredito,msgMidiaDisponibilizacaoCorrentista,msgMidiaMensagemRecadastramento,
										msgMomentoEnvio,msgMomentoIndicacaoCreditoEfetivado,msgOcorrenciaDebito,msgPercentualMaximoRegistrosInconsistentesLote,
										msgPeriodicidadeEmissao,msgPeriodicidadeManutencaoCadastroProcuradores,msgPeriodicidadeEnvioRemessaManutencao,msgPeriodicidadePesquisaDebitosPendentesVeiculos,
										msgPermiteAcertosDados,msgPermiteAnteciparRecadastramento,msgPermiteContigenciaPagamento,msgPermiteDebitoOnline,
										msgPermiteEnvioRemessaManutencaoCadastro,msgPermiteEstornoPagamento,msgPermiteFavorecidoConsultarPagamento,msgPermitePagarMenor,
										msgPermitePagarVencido,msgPermitirAgrupamentoCorrespondencia,msgPesquisaDe,msgPossuiExpiracaoCredito,
										msgPreAutorizacaoCliente,msgPrioridadeDebito,msgProcessamentoEfetivacaoPagamento,msgQuantidadeDiasFloating,
										msgQuantidadeDiasRepique,msgQuantidadeDiasEmissaoAvisoAntesInicioComprovacao,msgQuantidadeDiasEmissaoAvisoAntesVencimento,msgQuantidadeDiasExpiracaoCredito,
										msgQuantidadeDiasAvisoAntecipado,msgQuantidadeDiasEnvioAntecipadoFormulario,msgQuantidadeDiasParaExpiracao,msgQuantidadeDiasParaInativacaoFavorecido,
										msgQuantidadeFasesEtapa,msgQuantidadeLinhasComprovante,msgQuantidadeMesesEmissao,msgQuantidadeMesesPeriodicidadeComprovacao,
										msgQuantidadeMesesEtapa,msgQuantidadeMesesFase,msgQuantidadeViasEmitir,msgQuantidadeViasPagasRegistro,
										msgQuantidadeEtapasRecadastramento,msgQuantidadeLimiteCartaoContaSalarioSolicitacao,msgQuantidadeLimiteDiasParaPagamentoVencido,msgQuantidadeMaximaRegistroInconsistentesLote,
										msgRastrearNotasFiscais,msgRastrearTituloTerceiros,msgTextoInformacaoAreaReservada,msgTipoCartaoContaSalario,
										msgTipoConsistenciaCpfCnpjProprietario,msgTipoConsistenciaIdentificacaoBeneficiario,msgTipoCargaCadastroBeneficiario,msgTipoConsistenciaInscricaoFavorecido,
										msgTipoConsistenciaCpfCnpjFavorecido,msgTipoConsolidacaoPagamentosComprovante,msgTipoDataControleFloating,msgTipoEmissao,
										msgTipoFormacaoListaDebito,msgTipoInscricaoFavorecido,msgTipoRastreamentoTitulos,msgTipoRejeicaoLote,
										msgTipoRejeicaoEfetivacao,msgTipoRejeicaoAgendamento,msgTipoTratamentoContaTransferida,msgTipoIdentificacaoBeneficiario,
										msgTratamentoFeriadosDataPagamento,msgTratamentoFeriadoFimVigencia,msgTratamentoListaDebitoSemNumeracao,msgTratamentoValorDivergente,
										msgUtilizaCadastroFavorecidoControlePagamentos,msgUtilizaCafastroOrgaosPagadores,msgUtilizaCadastroProcuradores,
										msgUtilizaFrasesPreCadastradas,msgUtilizaListaDebito,msgUtilizaMensagemPersonalizadaOnline,msgValidarNomeFavorecidoReceitaFederal,
										msgValorLimiteDiario,msgValorLimiteIndividual,msgValorMaximoPagamentoFavorecidoNaoCadastrado,
										msgTipoContaCredito,msgTipoDeConsultaDeSaldo,msgPermiteCorrespondenciaAberta,msgEfetuarManutencaoCadastroProcuradores4,
										msgConfServicosManterContratoDestino, msgRetornoOperacoes, msgIndTipoRetornoInternet){

	var campos;
	campos = '';
	if(document.getElementById('confServicosManterContrato:cdParametro').value == '1') {
	
		if(document.getElementById('confServicosManterContrato:cboFormaEnvioPagamento').value == null || document.getElementById('confServicosManterContrato:cboFormaEnvioPagamento').value == 0) {
			campos = campos + msgCampo + ' ' + msgFormaEnvioPagamentos + ' ' + msgNecessario + '!' + '\n';
		}
			
		if(document.getElementById('confServicosManterContrato:cboFormaAutorizacaoPag').value == null || document.getElementById('confServicosManterContrato:cboFormaAutorizacaoPag').value == 0) {
			campos = campos + msgCampo + ' ' + msgFormaAutorizacaoPagamentos + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoAutCompAg');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgAutorizacaoComplementarAgencia + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementById('confServicosManterContrato:comboRetornoOperacoesSelecionado1');
		if (radioTemp.value == 0) {
			campos = campos + msgCampo + ' ' + msgRetornoOperacoes + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:radioCadastroFloating');
		if (radioTemp[1].checked) {
			if(document.getElementById('confServicosManterContrato:cboTipoDataControleFloating').value == null || document.getElementById('confServicosManterContrato:cboTipoDataControleFloating').value == 0) {
				campos = campos + msgCampo + ' ' + msgTipoDataControleFloating + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
	}
	
	if(document.getElementById('confServicosManterContrato:cdParametro').value == '2') {
		
		if(document.getElementById('confServicosManterContrato:cboFormaEnvioPagamento2').value == null || document.getElementById('confServicosManterContrato:cboFormaEnvioPagamento2').value == 0) {
			campos = campos + msgCampo + ' ' + msgFormaEnvioPagamentos + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confServicosManterContrato:cboFormaAutorizacaoPag2').value == null || document.getElementById('confServicosManterContrato:cboFormaAutorizacaoPag2').value == 0) {
			campos = campos + msgCampo + ' ' + msgFormaAutorizacaoPagamentos + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoPreAutCliente2');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPreAutorizacaoCliente + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoAutCompAg2');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgAutorizacaoComplementarAgencia + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementById('confServicosManterContrato:comboRetornoOperacoesSelecionado2');
		if (radioTemp.value == 0) {
			campos = campos + msgCampo + ' ' + msgRetornoOperacoes + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:radioCadastroFloating5');
		if (radioTemp[1].checked) {
			if(document.getElementById('confServicosManterContrato:cboTipoDataControleFloating2').value == null || document.getElementById('confServicosManterContrato:cboTipoDataControleFloating2').value == 0) {
				campos = campos + msgCampo + ' ' + msgTipoDataControleFloating + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoAbertContaBancoPostBradSeg');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgAberturaContaBancoPostalBradescoSeguros + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoEmissaoAntCartaoContSal');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgEmissaoAntecipadaCartaoContaSalario + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (radioTemp[1].checked) {
			if(document.getElementById('confServicosManterContrato:cboTipoCartaoContaSalario').value == null || document.getElementById('confServicosManterContrato:cboTipoCartaoContaSalario').value == 0) {
				campos = campos + msgCampo + ' ' + msgTipoCartaoContaSalario + ' ' + msgNecessario + '!' + '\n';
			}
			
			if (allTrim(document.getElementById('confServicosManterContrato:txtqtddLimiteCartaoContaSalSol')) == '') {
				campos = campos + msgCampo + ' ' + msgQuantidadeLimiteCartaoContaSalarioSolicitacao + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		if (document.getElementById('confServicosManterContrato:dtLimiteEnqConvContaSal.day').value == null ||
			allTrim(document.getElementById('confServicosManterContrato:dtLimiteEnqConvContaSal.day')) == ''   ||
			document.getElementById('confServicosManterContrato:dtLimiteEnqConvContaSal.month').value == null ||
			allTrim(document.getElementById('confServicosManterContrato:dtLimiteEnqConvContaSal.month')) == ''   ||
			document.getElementById('confServicosManterContrato:dtLimiteEnqConvContaSal.year').value == null ||
			allTrim(document.getElementById('confServicosManterContrato:dtLimiteEnqConvContaSal.year')) == '') {
				campos = campos + msgCampo + ' ' + msgDataLimiteEnquadramentoConvenioContaSalario + ' ' + msgNecessario + '!' + '\n';
		}
	}
	
	if(document.getElementById('confServicosManterContrato:cdParametro').value == '3') {

		if(document.getElementById('confServicosManterContrato:cboFormaEnvioPagamento3').value == null || document.getElementById('confServicosManterContrato:cboFormaEnvioPagamento3').value == 0) {
			campos = campos + msgCampo + ' ' + msgFormaEnvioPagamentos + ' ' + msgNecessario + '!' + '\n';
		}

		if(document.getElementById('confServicosManterContrato:cboFormaAutorizacaoPag3').value == null || document.getElementById('confServicosManterContrato:cboFormaAutorizacaoPag3').value == 0) {
			campos = campos + msgCampo + ' ' + msgFormaAutorizacaoPagamentos + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoPreAutCliente3');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPreAutorizacaoCliente + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoAutCompAg3');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgAutorizacaoComplementarAgencia + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementById('confServicosManterContrato:comboRetornoOperacoesSelecionado3');
		if (radioTemp.value == 0) {
			campos = campos + msgCampo + ' ' + msgRetornoOperacoes + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:radioCadastroFloating3');
		if (radioTemp[1].checked) {
			if(document.getElementById('confServicosManterContrato:cboTipoDataControleFloating3').value == null || document.getElementById('confServicosManterContrato:cboTipoDataControleFloating3').value == 0) {
				campos = campos + msgCampo + ' ' + msgTipoDataControleFloating + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		if(document.getElementById('confServicosManterContrato:cboTipoConsolidacaoPagamentosComprovante3').value == null || document.getElementById('confServicosManterContrato:cboTipoConsolidacaoPagamentosComprovante3').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoConsolidacaoPagamentosComprovante + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoPesquisaDe');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPesquisaDe + ' ' + msgNecessario + '!' + '\n';
		}else{
			if(radioTemp[1].checked){
				if(document.getElementById('confServicosManterContrato:cboPeriodicidadePesquisaDebitosPendentesVeiculos').value == null || document.getElementById('confServicosManterContrato:cboPeriodicidadePesquisaDebitosPendentesVeiculos').value == 0) {
					campos = campos + msgCampo + ' ' + msgPeriodicidadePesquisaDebitosPendentesVeiculos + ' ' + msgNecessario + '!' + '\n';
				}
			}
		}
		
		
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoAgendamentoDebitosPendentesVeiculos');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgAgendamentoDebitosPendentesVeiculos + ' ' + msgNecessario + '!' + '\n';
		}
	}
	
	if(document.getElementById('confServicosManterContrato:cdParametro').value == '4') {
	
		if(document.getElementById('confServicosManterContrato:cboFormaEnvioPagamento4').value == null || document.getElementById('confServicosManterContrato:cboFormaEnvioPagamento4').value == 0) {
			campos = campos + msgCampo + ' ' + msgFormaEnvioPagamentos + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confServicosManterContrato:cboFormaAutorizacaoPag4').value == null || document.getElementById('confServicosManterContrato:cboFormaAutorizacaoPag4').value == 0) {
			campos = campos + msgCampo + ' ' + msgFormaAutorizacaoPagamentos + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoPreAutCliente4');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPreAutorizacaoCliente + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoAutCompAg4');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgAutorizacaoComplementarAgencia + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementById('confServicosManterContrato:comboRetornoOperacoesSelecionado4');
		if (radioTemp.value == 0) {
			campos = campos + msgCampo + ' ' + msgRetornoOperacoes + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confServicosManterContrato:cboTipoDataControleFloating4').value == null || document.getElementById('confServicosManterContrato:cboTipoDataControleFloating4').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoDataControleFloating + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoInformaAgenciaContaCred');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgInformaAgenciaContaCredito + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confServicosManterContrato:cboTipoIdentificacaoBeneficiario').value == null || document.getElementById('confServicosManterContrato:cboTipoIdentificacaoBeneficiario').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoIdentificacaoBeneficiario + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoUtilizaCadastroOrgaosPagadores');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgUtilizaCafastroOrgaosPagadores + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoUtilizaCadastroProcuradores');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgUtilizaCadastroProcuradores + ' ' + msgNecessario + '!' + '\n';
		}
		
		var radioTemp2 = document.getElementsByName('confServicosManterContrato:rdoEfetuarManutencaoCadastroProcuradores4');
		if (radioTemp[1].checked) {
			if (!radioTemp2[1].checked && !radioTemp2[2].checked) {
				campos = campos + msgCampo + ' ' + msgEfetuarManutencaoCadastroProcuradores4 + ' ' + msgNecessario + '!' + '\n';
			}
		}
					
		if (radioTemp2[1].checked && radioTemp[1].checked) {			
			if(document.getElementById('confServicosManterContrato:cboPeriocidadeManCadProc').value == null || document.getElementById('confServicosManterContrato:cboPeriocidadeManCadProc').value == 0) {
				campos = campos + msgCampo + ' ' + msgPeriodicidadeManutencaoCadastroProcuradores + ' ' + msgNecessario + '!' + '\n';
			}
			
			if(document.getElementById('confServicosManterContrato:cboFormaManCadProcurador').value == null || document.getElementById('confServicosManterContrato:cboFormaManCadProcurador').value == 0) {
				campos = campos + msgCampo + ' ' + msgFormaManutencaoCadastroProcuradores + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoPossuiExpiracaoCredito');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPossuiExpiracaoCredito + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confServicosManterContrato:txtQtddDiasExpiracaoCredito')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasExpiracaoCredito + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (radioTemp[1].checked) {
			if(document.getElementById('confServicosManterContrato:cboContExpCredConta').value == null || document.getElementById('confServicosManterContrato:cboContExpCredConta').value == 0) {
				campos = campos + msgCampo + ' ' + msgControleExpiracaoCredito + ' ' + msgNecessario + '!' + '\n';
			}
		}
				
		if(document.getElementById('confServicosManterContrato:cboMomentoIndCredEfetivado').value == null || document.getElementById('confServicosManterContrato:cboMomentoIndCredEfetivado').value == 0) {
			campos = campos + msgCampo + ' ' + msgMomentoIndicacaoCreditoEfetivado + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confServicosManterContrato:cboTratamentoFeriadoFimVigenciaCredito').value == null || document.getElementById('confServicosManterContrato:cboTratamentoFeriadoFimVigenciaCredito').value == 0) {
			campos = campos + msgCampo + ' ' + msgTratamentoFeriadoFimVigencia + ' ' + msgNecessario + '!' + '\n';
		}
	}
	
	if(document.getElementById('confServicosManterContrato:cdParametro').value == '5') {
		if(document.getElementById('confServicosManterContrato:cboFormaManCadFav5').value == null || document.getElementById('confServicosManterContrato:cboFormaManCadFav5').value == 0) {
			campos = campos + msgCampo + ' ' + msgFormaManutencaoCadastroFavorecidos + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confServicosManterContrato:txtQtddDiasInativFav5')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasParaInativacaoFavorecido + ' ' + msgNecessario + '!' + '\n';
		}		
	
		if(document.getElementById('confServicosManterContrato:cboTipoConInsFav5').value == null || document.getElementById('confServicosManterContrato:cboTipoConInsFav5').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoConsistenciaInscricaoFavorecido + ' ' + msgNecessario + '!' + '\n';
		}	
	
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoGerarRetOpReaInt5');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgGerarRetornoOperacoesRealizadasInternet + ' ' + msgNecessario + '!' + '\n';
		}
	}

	if(document.getElementById('confServicosManterContrato:cdParametro').value == '6') {
	/*
		if(document.getElementById('confServicosManterContrato:cboPeriodicidade').value == null || document.getElementById('confServicosManterContrato:cboPeriodicidade').value == 0) {
			campos = campos + msgcampo + ' ' + msgPeriodicidade + ' ' + msgNecessario + '!' + '\n';
			
		}
		
		if(document.getElementById('confServicosManterContrato:cboDestino').value == null || document.getElementById('confServicosManterContrato:cboDestino').value == 0) {
			campos = campos + msgcampo + ' ' + msgDestino + ' ' + msgNecessario + '!' + '\n';
			
		}
		
		if(document.getElementById('confServicosManterContrato:cboPermitCorrespAberta').value == null || document.getElementById('confServicosManterContrato:cboPermitCorrespAberta').value == 0) {
			campos = campos + msgcampo + ' ' + msgPermitCorrespAberta + ' ' + msgNecessario + '!' + '\n';
			
		}
		
		if(document.getElementById('confServicosManterContrato:cboDemonstInfAreaRes').value == null || document.getElementById('confServicosManterContrato:cboDemonstInfAreaRes').value == 0) {
			campos = campos + msgcampo + ' ' + msgDemonstInfAreaRes + ' ' + msgNecessario + '!' + '\n';
			
		}
		
		
		if(document.getElementById('confServicosManterContrato:cboAgruparCorrespondencia').value == null || document.getElementById('confServicosManterContrato:cboAgruparCorrespondencia').value == 0) {
			campos = campos + msgcampo + ' ' + msgAgruparCorrespondencia + ' ' + msgNecessario + '!' + '\n';
			
		}
		
		if (allTrim(document.getElementById('confServicosManterContrato:txtQuantVias')) == "") {
			campos = campos + msgcampo + ' ' + msgQuantVias + ' ' + msgNecessario + '!' + '\n';
			
		}
		*/
	}

	if ((document.getElementById('confServicosManterContrato:cdParametro').value == '7') ||
		(document.getElementById('confServicosManterContrato:cdParametro').value == '8') ||
		(document.getElementById('confServicosManterContrato:cdParametro').value == '9') ||
		(document.getElementById('confServicosManterContrato:cdParametro').value == '10') ||
		(document.getElementById('confServicosManterContrato:cdParametro').value == '11')) {
		
		if(document.getElementById('confServicosManterContrato:cboPeriodicidadeEmissao7').value == null || document.getElementById('confServicosManterContrato:cboPeriodicidadeEmissao7').value == 0) {
			campos = campos + msgCampo + ' ' + msgPeriodicidadeEmissao + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContrato:cboDestinoEnvio7');		
		if (!radioTemp[1].checked && !radioTemp[2].checked && !radioTemp[3].checked) {
			campos = campos + msgCampo + ' ' + msgConfServicosManterContratoDestino + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confServicosManterContrato:qtddViasEmitir7')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeViasEmitir + ' ' + msgNecessario + '!' + '\n';
		}

		radioTemp = document.getElementsByName('confServicosManterContrato:rdoPermiteCorrespondenciaAberta7');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteCorrespondenciaAberta + ' ' + msgNecessario + '!' + '\n';
		}					
				
		radioTemp = document.getElementsByName('confServicosManterContrato:rdoAgruparCorresp7');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgAgruparCorrespondencias + ' ' + msgNecessario + '!' + '\n';
		}					

		radioTemp = document.getElementsByName('confServicosManterContrato:rdoDemonstraInfAreaRes7');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgDemonstraInformacoesAreaReservada + ' ' + msgNecessario + '!' + '\n';
		}	
		
	}
	
	
	
	if (campos != ''){
		alert(campos);
		campos = '';
		return false;
	}else{
		return true;
	}
}




function validaModalidadesConfiguracao(msgCampo, msgNecessario, 
										msgAberturaContaBancoPostalBradescoSeguros,msgAcaoNaoComprovacao,msgAdesaoSacadoEletronico,msgAgendamentoDebitosPendentesVeiculos,
										msgAgendarTituloRastreadoFilial,msgAgendarTituloRastreadoProprio,msgAgruparCorrespondencias,msgAutorizacaoComplementarAgencia,
										msgBloquearEmissaoPapeleta,msgCadastroUtilizadoRecadastramento,msgCadastroConsultaEnderecoEnvio,msgCapturarTitulosDataRegistro,
										msgCobrarTarifasFuncionarios,msgCodigoFormulario,msgCondicaoEnquadramento,msgControleExpiracaoCredito,
										msgCriterioCompostoEnquadramento,msgCriterioPrincipalEnquadramento,msgDataInicioBloqueioPapeleta,msgDataInicioRastreamento,
										msgDataFimPeriodoAcertoDados,msgDataFimRecadastramento,msgDataInicioPeriodoAcertoDados,msgDataInicioRecadastramento,
										msgDataLimiteEnquadramentoConvenioContaSalario,msgDataLimiteVinculoClienteBeneficiario,msgDemonstraInformacoesAreaReservada,msgDestinoEnvio,
										msgDestinoEnvioCorrespondencia,msgEfetuaConsistenciaEspecieBeneficio,
										msgEmissaoAntecipadaCartaoContaSalario,msgEmiteAvisoComprovacaoVida,msgEmiteMsgRecadastramento,msgExigeLiberacaoLoteProcessado,
										msgFormaAutorizacaoPagamentos,msgFormaEnvioPagamentos,msgFormaManutencaoCadastroFavorecidos,msgFormaManutencaoCadastroProcuradores,
										msgFormularioImpressao,msgGerarLancamentoFuturoCredito,msgGerarLancamentoFuturoDebito,msgGerarLancamentoProgramado,
										msgGerarRetornoOperacoesRealizadasInternet,msgInformaAgenciaContaCredito,msgMidiaDisponibilizacaoCorrentista,msgMidiaMensagemRecadastramento,
										msgMomentoEnvio,msgMomentoIndicacaoCreditoEfetivado,msgOcorrenciaDebito,msgPercentualMaximoRegistrosInconsistentesLote,
										msgPeriodicidadeEmissao,msgPeriodicidadeManutencaoCadastroProcuradores,msgPeriodicidadeEnvioRemessaManutencao,msgPeriodicidadePesquisaDebitosPendentesVeiculos,
										msgPermiteAcertosDados,msgPermiteAnteciparRecadastramento,msgPermiteContigenciaPagamento,msgPermiteDebitoOnline,
										msgPermiteEnvioRemessaManutencaoCadastro,msgPermiteEstornoPagamento,msgPermiteFavorecidoConsultarPagamento,msgPermitePagarMenor,
										msgPermitePagarVencido,msgPermitirAgrupamentoCorrespondencia,msgPesquisaDe,msgPossuiExpiracaoCredito,
										msgPreAutorizacaoCliente,msgPrioridadeDebito,msgProcessamentoEfetivacaoPagamento,msgQuantidadeDiasFloating,
										msgQuantidadeDiasRepique,msgQuantidadeDiasEmissaoAvisoAntesInicioComprovacao,msgQuantidadeDiasEmissaoAvisoAntesVencimento,msgQuantidadeDiasExpiracaoCredito,
										msgQuantidadeDiasAvisoAntecipado,msgQuantidadeDiasEnvioAntecipadoFormulario,msgQuantidadeDiasParaExpiracao,msgLocalEmissao,msgQuantidadeDiasParaInativacaoFavorecido,
										msgQuantidadeFasesEtapa,msgQuantidadeLinhasComprovante,msgQuantidadeMesesEmissao,msgQuantidadeMesesPeriodicidadeComprovacao,
										msgQuantidadeMesesEtapa,msgQuantidadeMesesFase,msgQuantidadeViasEmitir,msgQuantidadeViasPagasRegistro,
										msgQuantidadeEtapasRecadastramento,msgQuantidadeLimiteCartaoContaSalarioSolicitacao,msgQuantidadeLimiteDiasParaPagamentoVencido,msgQuantidadeMaximaRegistroInconsistentesLote,
										msgRastrearNotasFiscais,msgRastrearTituloTerceiros,msgTextoInformacaoAreaReservada,msgTipoCartaoContaSalario,
										msgTipoConsistenciaCpfCnpjProprietario,msgTipoConsistenciaIdentificacaoBeneficiario,msgTipoCargaCadastroBeneficiario,msgTipoConsistenciaInscricaoFavorecido,
										msgTipoConsistenciaCpfCnpjFavorecido,msgTipoConsolidacaoPagamentosComprovante,msgTipoDataControleFloating,msgTipoEmissao,
										msgTipoFormacaoListaDebito,msgTipoInscricaoFavorecido,msgTipoRastreamentoTitulos,msgTipoRejeicaoLote,
										msgTipoRejeicaoEfetivacao,msgTipoRejeicaoAgendamento,msgTipoTratamentoContaTransferida,msgTipoIdentificacaoBeneficiario,
										msgTratamentoFeriadosDataPagamento,msgTratamentoFeriadoFimVigencia,msgTratamentoListaDebitoSemNumeracao,msgTratamentoValorDivergente,
										msgUtilizaCadastroFavorecidoControlePagamentos,msgUtilizaCafastroOrgaosPagadores,msgUtilizaCadastroProcuradores,
										msgUtilizaFrasesPreCadastradas,msgUtilizaListaDebito,msgUtilizaMensagemPersonalizadaOnline,msgValidarNomeFavorecidoReceitaFederal,
										msgValorLimiteDiario,msgValorLimiteIndividual,msgValorMaximoPagamentoFavorecidoNaoCadastrado,
										msgTipoContaCredito,msgTipoDeConsultaDeSaldo, msgFeriadoLocal, msgTipoFormatacaoPrimeiraLinhaExtrato, msgPermiteAgAutGrade) {

	var campos = '';
	var radioTemp;
	var valorTemp;
	var cdParametro = document.getElementById('confModServicosManterContrato:cdParametro');
	var feriadoLocal = null;

	if ((cdParametro.value == '17') || (cdParametro.value == '18') || (cdParametro.value == '24') || (cdParametro.value == '25')) {
		if(document.getElementById('confModServicosManterContrato:cboTipoConsultaSaldo17').value == null || document.getElementById('confModServicosManterContrato:cboTipoConsultaSaldo17').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoDeConsultaDeSaldo + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboProcessamentoEfetivacaoPagamento17').value == null || document.getElementById('confModServicosManterContrato:cboProcessamentoEfetivacaoPagamento17').value == 0) {
			campos = campos + msgCampo + ' ' + msgProcessamentoEfetivacaoPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboTratFevDataPag17').value == null || document.getElementById('confModServicosManterContrato:cboTratFevDataPag17').value == 0) {
			campos = campos + msgCampo + ' ' + msgTratamentoFeriadosDataPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoPermiteFavConPag17');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteFavorecidoConsultarPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboTipoRejeicaoAgendamento17').value == null || document.getElementById('confModServicosManterContrato:cboTipoRejeicaoAgendamento17').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoAgendamento + ' ' + msgNecessario + '!' + '\n';
		}

		if(document.getElementById('confModServicosManterContrato:cboTipoRejeicaoEfetivacao17').value == null || document.getElementById('confModServicosManterContrato:cboTipoRejeicaoEfetivacao17').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoEfetivacao + ' ' + msgNecessario + '!' + '\n';
		}

		var qtdeMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContrato:txtQtddMaximaRegistroInconLote17'));
		var percMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContrato:txtPercentualMaximoRegistroInconsLote17'))
		if(document.getElementById('confModServicosManterContrato:cboTipoRejeicaoAgendamento17').value == 3) {
			if(qtdeMaxRegLote1 == '' || qtdeMaxRegLote1 == '0'){
				if(percMaxRegLote1 == '' || percMaxRegLote1 == '0'){
					campos = campos + msgQuantidadePercentualMaximoRegistrosInconsistentesLote + '!' + '\n';
				}		
			}		
		}
		
		if(document.getElementById('confModServicosManterContrato:cboPrioridadeDebito17').value == null || document.getElementById('confModServicosManterContrato:cboPrioridadeDebito17').value == 0) {
			campos = campos + msgCampo + ' ' + msgPrioridadeDebito + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoGerarLancFutDebito17');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgGerarLancamentoFuturoDebito + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoUtilizaCadFavControlePag17');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgUtilizaCadastroFavorecidoControlePagamentos + ' ' + msgNecessario + '!' + '\n';
		}

		if (radioTemp[1].checked){
			if (allTrim(document.getElementById('confModServicosManterContrato:txtValorMaximoFavNaoCad17')) == '') {
				campos = campos + msgCampo + ' ' + msgValorMaximoPagamentoFavorecidoNaoCadastrado + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		if (allTrim(document.getElementById('confModServicosManterContrato:txtValorLimitInd17')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteIndividual + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContrato:txtValorLimDiario17')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteDiario + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContrato:txtQtddDiasRepique17')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasRepique + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContrato:txtQtddDiasFloating17')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasFloating + ' ' + msgNecessario + '!' + '\n';
		}

		feriadoLocal = document.getElementById('confModServicosManterContrato:cboFeriadoLocal17');
		
		if ((cdParametro.value == '17') || (cdParametro.value == '18')) {
			var radio2LinhaExtrato = document.getElementsByName('confModServicosManterContrato:rdoDemonstra2LinhaExtrato17');
			
			if (!radio2LinhaExtrato[1].checked && !radio2LinhaExtrato[2].checked) {
				campos += msgCampo + ' Demonstra 2.Linha de Extrato ' + msgNecessario + '!' + '\n';
			}
		}
		
		if (cdParametro.value == '18') {
			var radioPermiteAgAutGrade = document.getElementsByName('confModServicosManterContrato:rdoPermiteAgAutGrade');
			
			if (!radioPermiteAgAutGrade[1].checked && !radioPermiteAgAutGrade[2].checked) {
				campos += msgCampo + ' ' + msgPermiteAgAutGrade + ' ' + msgNecessario + '!' + '\n';
			}
		}
	} else {
		feriadoLocal = document.getElementById('confModServicosManterContrato:cboFeriadoLocal' + cdParametro.value);
	}

	if ((cdParametro.value == '15')){ 
		
		if(document.getElementById('confModServicosManterContrato:cboMomentoEnvio15').value == null || document.getElementById('confModServicosManterContrato:cboMomentoEnvio15').value == 0) {
			campos = campos + msgCampo + ' ' + msgMomentoEnvio + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboCadConsultaEndEnvio15').value == null || document.getElementById('confModServicosManterContrato:cboCadConsultaEndEnvio15').value == 0) {
			campos = campos + msgCampo + ' ' + msgCadastroConsultaEnderecoEnvio + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContrato:qtddDiasAvisoAntecipado15')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasAvisoAntecipado + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoPermitirAgrupamentoCorrespondencia15');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermitirAgrupamentoCorrespondencia + ' ' + msgNecessario + '!' + '\n';
		}		
	}
	
	if ((cdParametro.value == '16')){ 
		if(document.getElementById('confModServicosManterContrato:cboMomentoEnvio16').value == null || document.getElementById('confModServicosManterContrato:cboMomentoEnvio16').value == 0) {
			campos = campos + msgCampo + ' ' + msgMomentoEnvio + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboCadConsultaEndEnvio16').value == null || document.getElementById('confModServicosManterContrato:cboCadConsultaEndEnvio16').value == 0) {
			campos = campos + msgCampo + ' ' + msgCadastroConsultaEnderecoEnvio + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContrato:qtddDiasAvisoAntecipado16')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasEnvioAntecipadoFormulario + ' ' + msgNecessario + '!' + '\n';
		}		

		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoPermitirAgrupamentoCorrespondencia16');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermitirAgrupamentoCorrespondencia + ' ' + msgNecessario + '!' + '\n';
		}
	}	
		
	if (cdParametro.value == '19'){
		if(document.getElementById('confModServicosManterContrato:cboTipoConsultaSaldo19').value == null || document.getElementById('confModServicosManterContrato:cboTipoConsultaSaldo19').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoDeConsultaDeSaldo + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboProcessamentoEfetivacaoPagamento19').value == null || document.getElementById('confModServicosManterContrato:cboProcessamentoEfetivacaoPagamento19').value == 0) {
			campos = campos + msgCampo + ' ' + msgProcessamentoEfetivacaoPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboTratFevDataPag19').value == null || document.getElementById('confModServicosManterContrato:cboTratFevDataPag19').value == 0) {
			campos = campos + msgCampo + ' ' + msgTratamentoFeriadosDataPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoPermiteFavConPag19');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteFavorecidoConsultarPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboTipoRejeicaoAgendamento19').value == null || document.getElementById('confModServicosManterContrato:cboTipoRejeicaoAgendamento19').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoAgendamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboTipoRejeicaoEfetivacao19').value == null || document.getElementById('confModServicosManterContrato:cboTipoRejeicaoEfetivacao19').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoEfetivacao + ' ' + msgNecessario + '!' + '\n';
		}
		
		var qtdeMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContrato:txtQtddMaximaRegistroInconLote19'));
		var percMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContrato:txtPercentualMaximoRegistroInconsLote19'))
		if(document.getElementById('confModServicosManterContrato:cboTipoRejeicaoAgendamento19').value == 3) {
			if(qtdeMaxRegLote1 == '' || qtdeMaxRegLote1 == '0'){
				if(percMaxRegLote1 == '' || percMaxRegLote1 == '0'){
					campos = campos + msgQuantidadePercentualMaximoRegistrosInconsistentesLote + '!' + '\n';
				}		
			}		
		}
		
		if(document.getElementById('confModServicosManterContrato:cboPrioridadeDebito19').value == null || document.getElementById('confModServicosManterContrato:cboPrioridadeDebito19').value == 0) {
			campos = campos + msgCampo + ' ' + msgPrioridadeDebito + ' ' + msgNecessario + '!' + '\n';
		}		
		
		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoGerarLancFutDebito19');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgGerarLancamentoFuturoDebito + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoUtilizaCadFavControlePag19');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgUtilizaCadastroFavorecidoControlePagamentos + ' ' + msgNecessario + '!' + '\n';
		}		
		
		if (radioTemp[1].checked){
			if (allTrim(document.getElementById('confModServicosManterContrato:txtValorMaximoFavNaoCad19')) == '') {
				campos = campos + msgCampo + ' ' + msgValorMaximoPagamentoFavorecidoNaoCadastrado + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		if (allTrim(document.getElementById('confModServicosManterContrato:txtValorLimitInd19')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteIndividual + ' ' + msgNecessario + '!' + '\n';
		}			
		
		if (allTrim(document.getElementById('confModServicosManterContrato:txtValorLimDiario19')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteDiario + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContrato:txtQtddDiasRepique19')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasRepique + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContrato:txtQtddDiasFloating19')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasFloating + ' ' + msgNecessario + '!' + '\n';
		}

		var comboOcorrenciaDebito = document.getElementById('confModServicosManterContrato:cboOcorrenciaDebito19');
		if (comboOcorrenciaDebito != null) {
			if (comboOcorrenciaDebito.value == null || comboOcorrenciaDebito.value == 0) {
				campos = campos + msgCampo + ' ' + msgOcorrenciaDebito + ' ' + msgNecessario + '!' + '\n';
			}
		}

		if(document.getElementById('confModServicosManterContrato:cboTipoInscFav19').value == null || document.getElementById('confModServicosManterContrato:cboTipoInscFav19').value == -1) {
			campos = campos + msgCampo + ' ' + msgTipoInscricaoFavorecido + ' ' + msgNecessario + '!' + '\n';
		}

		if (allTrim(document.getElementById('confModServicosManterContrato:txtQtddDiasExpiracao19')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasParaExpiracao + ' ' + msgNecessario + '!' + '\n';
		}

		if(document.getElementById('confModServicosManterContrato:cboLocalEmissao').value == null || document.getElementById('confModServicosManterContrato:cboLocalEmissao').value == '0') {
			campos = campos + msgCampo + ' ' + msgLocalEmissao + ' ' + msgNecessario + '!' + '\n';
		}
		
		var radio2LinhaExtrato = document.getElementsByName('confModServicosManterContrato:rdoDemonstra2LinhaExtrato19');
		
		if (!radio2LinhaExtrato[1].checked && !radio2LinhaExtrato[2].checked) {
			campos += msgCampo + ' Demonstra 2.Linha de Extrato ' + msgNecessario + '!' + '\n';
		}
	}

	if (cdParametro.value == '20') {
		if(document.getElementById('confModServicosManterContrato:cboTipoConsultaSaldo20').value == null || document.getElementById('confModServicosManterContrato:cboTipoConsultaSaldo20').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoDeConsultaDeSaldo + ' ' + msgNecessario + '!' + '\n';
		}

		if(document.getElementById('confModServicosManterContrato:cboProcessamentoEfetivacaoPagamento20').value == null || document.getElementById('confModServicosManterContrato:cboProcessamentoEfetivacaoPagamento20').value == 0) {
			campos = campos + msgCampo + ' ' + msgProcessamentoEfetivacaoPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboTratFevDataPag20').value == null || document.getElementById('confModServicosManterContrato:cboTratFevDataPag20').value == 0) {
			campos = campos + msgCampo + ' ' + msgTratamentoFeriadosDataPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoPermiteFavConPag20');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteFavorecidoConsultarPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboTipoRejeicaoAgendamento20').value == null || document.getElementById('confModServicosManterContrato:cboTipoRejeicaoAgendamento20').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoAgendamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboTipoRejeicaoEfetivacao20').value == null || document.getElementById('confModServicosManterContrato:cboTipoRejeicaoEfetivacao20').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoEfetivacao + ' ' + msgNecessario + '!' + '\n';
		}
		
		var qtdeMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContrato:txtQtddMaximaRegistroInconLote20'));
		var percMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContrato:txtPercentualMaximoRegistroInconsLote20'))
		if(document.getElementById('confModServicosManterContrato:cboTipoRejeicaoAgendamento20').value == 3) {
			if(qtdeMaxRegLote1 == '' || qtdeMaxRegLote1 == '0'){
				if(percMaxRegLote1 == '' || percMaxRegLote1 == '0'){
					campos = campos + msgQuantidadePercentualMaximoRegistrosInconsistentesLote + '!' + '\n';
				}		
			}		
		}

		if(document.getElementById('confModServicosManterContrato:cboPrioridadeDebito20').value == null || document.getElementById('confModServicosManterContrato:cboPrioridadeDebito20').value == 0) {
			campos = campos + msgCampo + ' ' + msgPrioridadeDebito + ' ' + msgNecessario + '!' + '\n';
		}

		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoUtilizaCadFavControlePag20');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgUtilizaCadastroFavorecidoControlePagamentos + ' ' + msgNecessario + '!' + '\n';
		}		

		if (radioTemp[1].checked){
			if (allTrim(document.getElementById('confModServicosManterContrato:txtValorMaximoFavNaoCad20')) == '') {
				campos = campos + msgCampo + ' ' + msgValorMaximoPagamentoFavorecidoNaoCadastrado + ' ' + msgNecessario + '!' + '\n';
			}
		}	
		
		if (allTrim(document.getElementById('confModServicosManterContrato:txtValorLimitInd20')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteIndividual + ' ' + msgNecessario + '!' + '\n';
		}			

		if (allTrim(document.getElementById('confModServicosManterContrato:txtValorLimDiario20')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteDiario + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContrato:txtQtddDiasRepique20')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasRepique + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContrato:txtQtddDiasFloating20')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasFloating + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoGerarLancFutDebito20');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgGerarLancamentoFuturoDebito + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoGerarLancFutCredito20');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgGerarLancamentoFuturoCredito + ' ' + msgNecessario + '!' + '\n';
		}

		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoGerarLancProg20');
		if (radioTemp.length > 0) {
			if (!radioTemp[1].checked && !radioTemp[2].checked) {
				campos = campos + msgCampo + ' ' + msgGerarLancamentoProgramado + ' ' + msgNecessario + '!' + '\n';
			}
		}

		if (document.getElementById('confModServicosManterContrato:cboTratContaTrans20').value == null || document.getElementById('confModServicosManterContrato:cboTratContaTrans20').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoTratamentoContaTransferida + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContrato:cboTipoConsCpfCnpjFav20').value == null || document.getElementById('confModServicosManterContrato:cboTipoConsCpfCnpjFav20').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoConsistenciaCpfCnpjFavorecido + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContrato:rdoValidarNomeFavReceitaFed20');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgValidarNomeFavorecidoReceitaFederal + ' ' + msgNecessario + '!' + '\n';
		}

		var radio2LinhaExtrato = document.getElementsByName('confModServicosManterContrato:rdoDemonstra2LinhaExtrato20');
		
		if (!radio2LinhaExtrato[1].checked && !radio2LinhaExtrato[2].checked) {
			campos += msgCampo + ' Demonstra 2.Linha de Extrato ' + msgNecessario + '!' + '\n';
		}
		
		var radioTipoFormatacaoPrimeiraLinha = document.getElementsByName('confModServicosManterContrato:rdoTipoFormatacaoPrimeiraLinhaExtratoCredito');
		
		if (!radioTipoFormatacaoPrimeiraLinha[1].checked && !radioTipoFormatacaoPrimeiraLinha[2].checked) {
			campos += msgCampo + ' ' + msgTipoFormatacaoPrimeiraLinhaExtrato + ' ' + msgNecessario + '!' + '\n';
		}
	}

	if (feriadoLocal && feriadoLocal.value == '0') {
		campos = campos + msgCampo + ' ' + '' + msgFeriadoLocal + ' ' + msgNecessario + '!' + '\n';
	}

	if (campos != '') {
		alert(campos);
		campos = '';
		return false;
	}

	return true;
}

function validaModalidadesConfiguracaoP2(msgCampo, msgNecessario, 
										msgAberturaContaBancoPostalBradescoSeguros,msgAcaoNaoComprovacao,msgAdesaoSacadoEletronico,msgAgendamentoDebitosPendentesVeiculos,
										msgAgendarTituloRastreadoFilial,msgAgendarTituloRastreadoProprio,msgAgruparCorrespondencias,msgAutorizacaoComplementarAgencia,
										msgBloquearEmissaoPapeleta,msgCadastroUtilizadoRecadastramento,msgCadastroConsultaEnderecoEnvio,msgCobrarTarifasFuncionarios,
										msgCodigoFormulario,msgCondicaoEnquadramento,msgControleExpiracaoCredito,msgCriterioCompostoEnquadramento,
										msgCriterioPrincipalEnquadramento,msgDataInicioBloqueioPapeleta,msgDataInicioRastreamento,
										msgDataFimPeriodoAcertoDados,msgDataFimRecadastramento,msgDataInicioPeriodoAcertoDados,msgDataInicioRecadastramento,
										msgDataLimiteEnquadramentoConvenioContaSalario,msgDataLimiteVinculoClienteBeneficiario,msgDemonstraInformacoesAreaReservada,msgDestinoEnvio,
										msgDestinoEnvioCorrespondencia,msgEfetuaConsistenciaEspecieBeneficio,
										msgEmissaoAntecipadaCartaoContaSalario,msgEmiteAvisoComprovacaoVida,msgEmiteMsgRecadastramento,msgExigeLiberacaoLoteProcessado,
										msgFormaAutorizacaoPagamentos,msgFormaEnvioPagamentos,msgFormaManutencaoCadastroFavorecidos,msgFormaManutencaoCadastroProcuradores,
										msgFormularioImpressao,msgGerarLancamentoFuturoCredito,msgGerarLancamentoFuturoDebito,msgGerarLancamentoProgramado,
										msgGerarRetornoOperacoesRealizadasInternet,msgInformaAgenciaContaCredito,msgMidiaDisponibilizacaoCorrentista,msgMidiaMensagemRecadastramento,
										msgMomentoEnvio,msgMomentoIndicacaoCreditoEfetivado,msgOcorrenciaDebito,msgPercentualMaximoRegistrosInconsistentesLote,
										msgPeriodicidadeEmissao,msgPeriodicidadeManutencaoCadastroProcuradores,msgPeriodicidadeEnvioRemessaManutencao,msgPeriodicidadePesquisaDebitosPendentesVeiculos,
										msgPermiteAcertosDados,msgPermiteAnteciparRecadastramento,msgPermiteContigenciaPagamento,msgPermiteDebitoOnline,
										msgPermiteEnvioRemessaManutencaoCadastro,msgPermiteEstornoPagamento,msgPermiteFavorecidoConsultarPagamento,msgPermitePagarMenor,
										msgPermitePagarVencido,msgPermitirAgrupamentoCorrespondencia,msgPesquisaDe,msgPossuiExpiracaoCredito,
										msgPreAutorizacaoCliente,msgPrioridadeDebito,msgProcessamentoEfetivacaoPagamento,msgQuantidadeDiasFloating,
										msgQuantidadeDiasRepique,msgQuantidadeDiasEmissaoAvisoAntesInicioComprovacao,msgQuantidadeDiasEmissaoAvisoAntesVencimento,msgQuantidadeDiasExpiracaoCredito,
										msgQuantidadeDiasAvisoAntecipado,msgQuantidadeDiasEnvioAntecipadoFormulario,msgQuantidadeDiasParaExpiracao,msgQuantidadeDiasParaInativacaoFavorecido,
										msgQuantidadeFasesEtapa,msgQuantidadeLinhasComprovante,msgQuantidadeMesesEmissao,msgQuantidadeMesesPeriodicidadeComprovacao,
										msgQuantidadeMesesEtapa,msgQuantidadeMesesFase,msgQuantidadeViasEmitir,msgQuantidadeViasPagasRegistro,
										msgQuantidadeEtapasRecadastramento,msgQuantidadeLimiteCartaoContaSalarioSolicitacao,msgQuantidadeLimiteDiasParaPagamentoVencido,msgQuantidadeMaximaRegistroInconsistentesLote,
										msgRastrearNotasFiscais,msgRastrearTituloTerceiros,msgTextoInformacaoAreaReservada,msgTipoCartaoContaSalario,
										msgTipoConsistenciaCpfCnpjProprietario,msgTipoConsistenciaIdentificacaoBeneficiario,msgTipoCargaCadastroBeneficiario,msgTipoConsistenciaInscricaoFavorecido,
										msgTipoConsistenciaCpfCnpjFavorecido,msgTipoConsolidacaoPagamentosComprovante,msgTipoDataControleFloating,msgTipoEmissao,
										msgTipoFormacaoListaDebito,msgTipoInscricaoFavorecido,msgTipoRastreamentoTitulos,msgTipoRejeicaoLote,
										msgTipoRejeicaoEfetivacao,msgTipoRejeicaoAgendamento,msgTipoTratamentoContaTransferida,msgTipoIdentificacaoBeneficiario,
										msgTratamentoFeriadosDataPagamento,msgTratamentoFeriadoFimVigencia,msgTratamentoListaDebitoSemNumeracao,msgTratamentoValorDivergente,
										msgUtilizaCadastroFavorecidoControlePagamentos,msgUtilizaCafastroOrgaosPagadores,msgUtilizaCadastroProcuradores,
										msgUtilizaFrasesPreCadastradas,msgUtilizaListaDebito,msgUtilizaMensagemPersonalizadaOnline,msgValorLimiteDiario,msgValorLimiteIndividual,
										msgValorMaximoPagamentoFavorecidoNaoCadastrado,msgTipoContaCredito,msgTipoDeConsultaDeSaldo,
										msgQuantidadePercentualMaximoRegistrosInconsistentesLote, msgFeriadoLocal, msgIndicadorDdaRetorno) {

	var campos = '';
	var radioTemp;
	var valorTemp;
	var cdParametro = document.getElementById('confModServicosManterContratoP2:cdParametro');

	if (cdParametro.value == '21') {
		if(document.getElementById('confModServicosManterContratoP2:cboTipoConsultaSaldo21').value == null || document.getElementById('confModServicosManterContratoP2:cboTipoConsultaSaldo21').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoDeConsultaDeSaldo + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContratoP2:cboProcessamentoEfetivacaoPagamento21').value == null || document.getElementById('confModServicosManterContratoP2:cboProcessamentoEfetivacaoPagamento21').value == 0) {
			campos = campos + msgCampo + ' ' + msgProcessamentoEfetivacaoPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContratoP2:cboTratFevDataPag21').value == null || document.getElementById('confModServicosManterContratoP2:cboTratFevDataPag21').value == 0) {
			campos = campos + msgCampo + ' ' + msgTratamentoFeriadosDataPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoPermiteFavConPag21');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteFavorecidoConsultarPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContratoP2:cboTipoRejeicaoAgendamento21').value == null || document.getElementById('confModServicosManterContratoP2:cboTipoRejeicaoAgendamento21').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoAgendamento + ' ' + msgNecessario + '!' + '\n';
		}	

		if(document.getElementById('confModServicosManterContratoP2:cboTipoRejeicaoEfetivacao21').value == null || document.getElementById('confModServicosManterContratoP2:cboTipoRejeicaoEfetivacao21').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoEfetivacao + ' ' + msgNecessario + '!' + '\n';
		}			

		var qtdeMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContratoP2:txtQtddMaximaRegistroInconLote21'));
		var percMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContratoP2:txtPercentualMaximoRegistroInconsLote21'))
		if(document.getElementById('confModServicosManterContratoP2:cboTipoRejeicaoAgendamento21').value == 3) {
			if(qtdeMaxRegLote1 == '' || qtdeMaxRegLote1 == '0'){
				if(percMaxRegLote1 == '' || percMaxRegLote1 == '0'){
					campos = campos + msgQuantidadePercentualMaximoRegistrosInconsistentesLote + '!' + '\n';
				}		
			}		
		}
		
		if(document.getElementById('confModServicosManterContratoP2:cboPrioridadeDebito21').value == null || document.getElementById('confModServicosManterContratoP2:cboPrioridadeDebito21').value == 0) {
			campos = campos + msgCampo + ' ' + msgPrioridadeDebito + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoPermiteDebOnline21');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteDebitoOnline + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoUtilizaCadFavControlePag21');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgUtilizaCadastroFavorecidoControlePagamentos + ' ' + msgNecessario + '!' + '\n';
		}

		if (radioTemp[1].checked){		
			if (allTrim(document.getElementById('confModServicosManterContratoP2:txtValorMaximoFavNaoCad21')) == '') {
				campos = campos + msgCampo + ' ' + msgValorMaximoPagamentoFavorecidoNaoCadastrado + ' ' + msgNecessario + '!' + '\n';
			}else{
				valorTemp = parseFloat(document.getElementById('confModServicosManterContratoP2:txtValorMaximoFavNaoCad21').value.replace(',','.'));
				if (valorTemp == '0.00'){
					campos = campos + msgCampo + ' ' + msgValorMaximoPagamentoFavorecidoNaoCadastrado + ' ' + msgNecessario + '!' + '\n';
				}
			}
		}
		
		if (allTrim(document.getElementById('confModServicosManterContratoP2:txtQtddDiasRepique21')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasRepique + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContratoP2:txtQtddDiasFloating21')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasFloating + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoGerarLacnFutDeb21');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgGerarLancamentoFuturoDebito + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoGerarLacnFutCred21');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgGerarLancamentoFuturoCredito + ' ' + msgNecessario + '!' + '\n';
		}		
				
		//if (allTrim(document.getElementById('confModServicosManterContratoP2:txtValorLimitInd21')) == '') {
		//	campos = campos + msgCampo + ' ' + msgValorLimiteIndividual + ' ' + msgNecessario + '!' + '\n';
		//}else{
		//	valorTemp = parseFloat(document.getElementById('confModServicosManterContratoP2:txtValorLimitInd21').value.replace(',','.'));
		//	if (valorTemp == '0.00'){
		//		campos = campos + msgCampo + ' ' + msgValorLimiteIndividual + ' ' + msgNecessario + '!' + '\n';
		//	}
		//}
		
		//if (allTrim(document.getElementById('confModServicosManterContratoP2:txtValorLimDiario21')) == '') {
		//	campos = campos + msgCampo + ' ' + msgValorLimiteDiario + ' ' + msgNecessario + '!' + '\n';
		//}else{
		//	valorTemp = parseFloat(document.getElementById('confModServicosManterContratoP2:txtValorLimDiario21').value.replace(',','.'));
		//	if (valorTemp == '0.00'){
		//		campos = campos + msgCampo + ' ' + msgValorLimiteDiario + ' ' + msgNecessario + '!' + '\n';
		//	}
		//}
	}

	if ((cdParametro.value == '22') || (cdParametro.value == '23')) { 
		if(document.getElementById('confModServicosManterContratoP2:cboTipoConsultaSaldo22').value == null || document.getElementById('confModServicosManterContratoP2:cboTipoConsultaSaldo22').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoDeConsultaDeSaldo + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContratoP2:cboProcessamentoEfetivacaoPagamento22').value == null || document.getElementById('confModServicosManterContratoP2:cboProcessamentoEfetivacaoPagamento22').value == 0) {
			campos = campos + msgCampo + ' ' + msgProcessamentoEfetivacaoPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContratoP2:cboTratFevDataPag22').value == null || document.getElementById('confModServicosManterContratoP2:cboTratFevDataPag22').value == 0) {
			campos = campos + msgCampo + ' ' + msgTratamentoFeriadosDataPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoPermiteFavConPag22');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteFavorecidoConsultarPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContratoP2:cboTipoRejeicaoAgendamento22').value == null || document.getElementById('confModServicosManterContratoP2:cboTipoRejeicaoAgendamento22').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoAgendamento + ' ' + msgNecessario + '!' + '\n';
		}	

		if(document.getElementById('confModServicosManterContratoP2:cboTipoRejeicaoEfetivacao22').value == null || document.getElementById('confModServicosManterContratoP2:cboTipoRejeicaoEfetivacao22').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoEfetivacao + ' ' + msgNecessario + '!' + '\n';
		}			

		var qtdeMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContratoP2:txtQtddMaximaRegistroInconLote22'));
		var percMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContratoP2:txtPercentualMaximoRegistroInconsLote22'))
		if(document.getElementById('confModServicosManterContratoP2:cboTipoRejeicaoAgendamento22').value == 3) {
			if(qtdeMaxRegLote1 == '' || qtdeMaxRegLote1 == '0'){
				if(percMaxRegLote1 == '' || percMaxRegLote1 == '0'){
					campos = campos + msgQuantidadePercentualMaximoRegistrosInconsistentesLote + '!' + '\n';
				}		
			}		
		}
		
		if(document.getElementById('confModServicosManterContratoP2:cboPrioridadeDebito22').value == null || document.getElementById('confModServicosManterContratoP2:cboPrioridadeDebito22').value == 0) {
			campos = campos + msgCampo + ' ' + msgPrioridadeDebito + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoGerarLancFutDebito22');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgGerarLancamentoFuturoDebito + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoUtilizaCadFavControlePag22');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgUtilizaCadastroFavorecidoControlePagamentos + ' ' + msgNecessario + '!' + '\n';
		}

		if (radioTemp[1].checked){
			if (allTrim(document.getElementById('confModServicosManterContratoP2:txtValorMaximoFavNaoCad22')) == '') {
				campos = campos + msgCampo + ' ' + msgValorMaximoPagamentoFavorecidoNaoCadastrado + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		if (allTrim(document.getElementById('confModServicosManterContratoP2:txtValorLimitInd22')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteIndividual + ' ' + msgNecessario + '!' + '\n';
		}

		if (allTrim(document.getElementById('confModServicosManterContratoP2:txtValorLimDiario22')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteDiario + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContratoP2:txtQtddDiasRepique22')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasRepique + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContratoP2:txtQtddDiasFloating22')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasFloating + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoPermitePagarMenor22');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermitePagarMenor + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoPermitePagarVencido22');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermitePagarVencido + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (radioTemp[1].checked) {
			if (allTrim(document.getElementById('confModServicosManterContratoP2:txtQuantidadeLimiteDiasPagamentoVencido22')) == '') {
				campos = campos + msgCampo + ' ' + msgQuantidadeLimiteDiasParaPagamentoVencido + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		var radio2LinhaExtrato = document.getElementsByName('confModServicosManterContratoP2:rdoDemonstra2LinhaExtrato22');
		
		if (!radio2LinhaExtrato[1].checked && !radio2LinhaExtrato[2].checked) {
			campos += msgCampo + ' Demonstra 2.Linha de Extrato ' + msgNecessario + '!' + '\n';
		}
		
		var radioIndicadorDdaRetorno = document.getElementsByName('confModServicosManterContratoP2:rdoIndicadorDdaRetorno');
		
		if (!radioIndicadorDdaRetorno[1].checked && !radioIndicadorDdaRetorno[2].checked) {
			campos += msgCampo + ' ' + msgIndicadorDdaRetorno + ' ' + msgNecessario + '!' + '\n';
		}
	}
	
	if (cdParametro.value == '26'){
		if(document.getElementById('confModServicosManterContratoP2:cboTipoRastreamentoTitulo26').value == null || document.getElementById('confModServicosManterContratoP2:cboTipoRastreamentoTitulo26').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRastreamentoTitulos + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoRastrearTituloTerceiros26');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgRastrearTituloTerceiros + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoRastrearNotasFiscais26');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgRastrearNotasFiscais + ' ' + msgNecessario + '!' + '\n';
		}			
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoAgendarTituloRastProp26');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgAgendarTituloRastreadoProprio + ' ' + msgNecessario + '!' + '\n';
		}				
				
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoAgendarTituloRastreadoFilial26');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgAgendarTituloRastreadoFilial + ' ' + msgNecessario + '!' + '\n';
		}		
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoBloqEmissaoPapeleta26');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgBloquearEmissaoPapeleta + ' ' + msgNecessario + '!' + '\n';
		}		
				
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoAdesaoSacadoEletronico26');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgAdesaoSacadoEletronico + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (document.getElementById('confModServicosManterContratoP2:dtInicioRastreamento26.day').value == null ||
			allTrim(document.getElementById('confModServicosManterContratoP2:dtInicioRastreamento26.day')) == ''   ||
			document.getElementById('confModServicosManterContratoP2:dtInicioRastreamento26.month').value == null ||
			allTrim(document.getElementById('confModServicosManterContratoP2:dtInicioRastreamento26.month')) == ''   ||
			document.getElementById('confModServicosManterContratoP2:dtInicioRastreamento26.year').value == null ||
			allTrim(document.getElementById('confModServicosManterContratoP2:dtInicioRastreamento26.year')) == '') {
				campos = campos + msgCampo + ' ' + msgDataInicioRastreamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP2:rdoBloqEmissaoPapeleta26');
		if(radioTemp[1].checked){
			if (document.getElementById('confModServicosManterContratoP2:dtInicioBloqPapeleta26.day').value == null ||
				allTrim(document.getElementById('confModServicosManterContratoP2:dtInicioBloqPapeleta26.day')) == ''   ||
				document.getElementById('confModServicosManterContratoP2:dtInicioBloqPapeleta26.month').value == null ||
				allTrim(document.getElementById('confModServicosManterContratoP2:dtInicioBloqPapeleta26.month')) == ''   ||
				document.getElementById('confModServicosManterContratoP2:dtInicioBloqPapeleta26.year').value == null ||
				allTrim(document.getElementById('confModServicosManterContratoP2:dtInicioBloqPapeleta26.year')) == '') {
					campos = campos + msgCampo + ' ' + msgDataInicioBloqueioPapeleta + ' ' + msgNecessario + '!' + '\n';
			}
		}
	}

	var feriadoLocal = null;
	if (cdParametro.value == '22' || cdParametro.value == '23') {
		feriadoLocal = document.getElementById('confModServicosManterContratoP2:cboFeriadoLocal22');
	} else {
		feriadoLocal = document.getElementById('confModServicosManterContratoP2:cboFeriadoLocal' + cdParametro.value);
	}

	if (feriadoLocal && feriadoLocal.value == '0') {
		campos = campos + msgCampo + ' ' + '' + msgFeriadoLocal + ' ' + msgNecessario + '!' + '\n';
	}

	if (campos != '') {
		alert(campos);
		campos = '';
		return false;
	}

	return true;
}

function validaModalidadesConfiguracaoP3(msgCampo, msgNecessario, 
										msgAberturaContaBancoPostalBradescoSeguros,msgAcaoNaoComprovacao,msgAdesaoSacadoEletronico,msgAgendamentoDebitosPendentesVeiculos,
										msgAgendarTituloRastreadoFilial,msgAgendarTituloRastreadoProprio,msgAgruparCorrespondencias,msgAutorizacaoComplementarAgencia,
										msgBloquearEmissaoPapeleta,msgCadastroUtilizadoRecadastramento,msgCadastroConsultaEnderecoEnvio,msgCapturarTitulosDataRegistro,
										msgCobrarTarifasFuncionarios,msgCodigoFormulario,msgCondicaoEnquadramento,msgControleExpiracaoCredito,
										msgCriterioCompostoEnquadramento,msgCriterioPrincipalEnquadramento,msgDataInicioBloqueioPapeleta,msgDataInicioRastreamento,
										msgDataFimPeriodoAcertoDados,msgDataFimRecadastramento,msgDataInicioPeriodoAcertoDados,msgDataInicioRecadastramento,
										msgDataLimiteEnquadramentoConvenioContaSalario,msgDataLimiteVinculoClienteBeneficiario,msgDemonstraInformacoesAreaReservada,msgDestinoEnvio,
										msgDestinoEnvioCorrespondencia,msgEfetuaConsistenciaEspecieBeneficio,
										msgEmissaoAntecipadaCartaoContaSalario,msgEmiteAvisoComprovacaoVida,msgEmiteMsgRecadastramento,msgExigeLiberacaoLoteProcessado,
										msgFormaAutorizacaoPagamentos,msgFormaEnvioPagamentos,msgFormaManutencaoCadastroFavorecidos,msgFormaManutencaoCadastroProcuradores,
										msgFormularioImpressao,msgGerarLancamentoFuturoCredito,msgGerarLancamentoFuturoDebito,msgGerarLancamentoProgramado,
										msgGerarRetornoOperacoesRealizadasInternet,msgInformaAgenciaContaCredito,msgMidiaDisponibilizacaoCorrentista,msgMidiaMensagemRecadastramento,
										msgMomentoEnvio,msgMomentoIndicacaoCreditoEfetivado,msgOcorrenciaDebito,msgPercentualMaximoRegistrosInconsistentesLote,
										msgPeriodicidadeEmissao,msgPeriodicidadeManutencaoCadastroProcuradores,msgPeriodicidadeEnvioRemessaManutencao,msgPeriodicidadePesquisaDebitosPendentesVeiculos,
										msgPermiteAcertosDados,msgPermiteAnteciparRecadastramento,msgPermiteContigenciaPagamento,msgPermiteDebitoOnline,
										msgPermiteEnvioRemessaManutencaoCadastro,msgPermiteEstornoPagamento,msgPermiteFavorecidoConsultarPagamento,msgPermitePagarMenor,
										msgPermitePagarVencido,msgPermitirAgrupamentoCorrespondencia,msgPesquisaDe,msgPossuiExpiracaoCredito,
										msgPreAutorizacaoCliente,msgPrioridadeDebito,msgProcessamentoEfetivacaoPagamento,msgQuantidadeDiasFloating,
										msgQuantidadeDiasRepique,msgQuantidadeDiasEmissaoAvisoAntesInicioComprovacao,msgQuantidadeDiasEmissaoAvisoAntesVencimento,msgQuantidadeDiasExpiracaoCredito,
										msgQuantidadeDiasAvisoAntecipado,msgQuantidadeDiasEnvioAntecipadoFormulario,msgQuantidadeDiasParaExpiracao,msgQuantidadeDiasParaInativacaoFavorecido,
										msgQuantidadeFasesEtapa,msgQuantidadeLinhasComprovante,msgQuantidadeMesesEmissao,msgQuantidadeMesesPeriodicidadeComprovacao,
										msgQuantidadeMesesEtapa,msgQuantidadeMesesFase,msgQuantidadeViasEmitir,msgQuantidadeViasPagasRegistro,
										msgQuantidadeEtapasRecadastramento,msgQuantidadeLimiteCartaoContaSalarioSolicitacao,msgQuantidadeLimiteDiasParaPagamentoVencido,msgQuantidadeMaximaRegistroInconsistentesLote,
										msgRastrearNotasFiscais,msgRastrearTituloTerceiros,msgTextoInformacaoAreaReservada,msgTipoCartaoContaSalario,
										msgTipoConsistenciaCpfCnpjProprietario,msgTipoConsistenciaIdentificacaoBeneficiario,msgTipoCargaCadastroBeneficiario,msgTipoConsistenciaInscricaoFavorecido,
										msgTipoConsistenciaCpfCnpjFavorecido,msgTipoConsolidacaoPagamentosComprovante,msgTipoDataControleFloating,msgTipoEmissao,
										msgTipoFormacaoListaDebito,msgTipoInscricaoFavorecido,msgTipoRastreamentoTitulos,msgTipoRejeicaoLote,
										msgTipoRejeicaoEfetivacao,msgTipoRejeicaoAgendamento,msgTipoTratamentoContaTransferida,msgTipoIdentificacaoBeneficiario,
										msgTratamentoFeriadosDataPagamento,msgTratamentoFeriadoFimVigencia,msgTratamentoListaDebitoSemNumeracao,msgTratamentoValorDivergente,
										msgUtilizaCadastroFavorecidoControlePagamentos,msgUtilizaCafastroOrgaosPagadores,msgUtilizaCadastroProcuradores,
										msgUtilizaFrasesPreCadastradas,msgUtilizaListaDebito,msgUtilizaMensagemPersonalizadaOnline,
										msgValorLimiteDiario,msgValorLimiteIndividual,msgValorMaximoPagamentoFavorecidoNaoCadastrado,
										msgTipoContaCredito,msgTipoDeConsultaDeSaldo,msgQuantidadePercentualMaximoRegistrosInconsistentesLote, msgCompVidaDestino,
										msgFeriadoLocal) {

	var campos = '';
	var radioTemp;
	var valorTemp;
		
	if (document.getElementById('confModServicosManterContratoP3:cdParametro').value == '27'){
		if(document.getElementById('confModServicosManterContratoP3:cboAcaoNaoComprovacao27').value == null || document.getElementById('confModServicosManterContratoP3:cboAcaoNaoComprovacao27').value == 0) {
			campos = campos + msgCampo + ' ' + msgAcaoNaoComprovacao + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContratoP3:txtQtddMesesPeriodiCompro27')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeMesesPeriodicidadeComprovacao + ' ' + msgNecessario + '!' + '\n';
		}
		
		var radioTempEmiteAviso = document.getElementsByName('confModServicosManterContratoP3:rdoEmiteAvisoCompVida27');
		var radioTempDestino = document.getElementsByName('confModServicosManterContratoP3:cboDestinoEnvio27');
		if (!radioTempEmiteAviso[1].checked && !radioTempEmiteAviso[2].checked) {
			campos = campos + msgCampo + ' ' + msgEmiteAvisoComprovacaoVida + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (radioTempEmiteAviso[1].checked) {
			if (allTrim(document.getElementById('confModServicosManterContratoP3:txtQtddDiasEmiAviAntIniComp27')) == '') {
				campos = campos + msgCampo + ' ' + msgQuantidadeDiasEmissaoAvisoAntesInicioComprovacao + ' ' + msgNecessario + '!' + '\n';
			}
			
			if (allTrim(document.getElementById('confModServicosManterContratoP3:txtQtddDiasEmiAviAntVenc27')) == '') {
				campos = campos + msgCampo + ' ' + msgQuantidadeDiasEmissaoAvisoAntesVencimento + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP3:rdoUtilMsgPersoOnline');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgUtilizaMensagemPersonalizadaOnline + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (radioTempEmiteAviso[1].checked) {
			if (!radioTempDestino[1].checked && !radioTempDestino[2].checked && !radioTempDestino[3].checked) {
				campos = campos + msgCampo + ' ' + msgCompVidaDestino + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		if (radioTempEmiteAviso[1].checked && !radioTempDestino[1].checked) {
			if(document.getElementById('confModServicosManterContratoP3:cboCadastroConEndEnvio27').value == null || document.getElementById('confModServicosManterContratoP3:cboCadastroConEndEnvio27').value == 0) {
				campos = campos + msgCampo + ' ' + msgCadastroConsultaEnderecoEnvio + ' ' + msgNecessario + '!' + '\n';
			}
		}		
	}
	
	if ((document.getElementById('confModServicosManterContratoP3:cdParametro').value == '28') ||
		(document.getElementById('confModServicosManterContratoP3:cdParametro').value == '29') ||
		(document.getElementById('confModServicosManterContratoP3:cdParametro').value == '30') ||
		(document.getElementById('confModServicosManterContratoP3:cdParametro').value == '31')){
	 
		if(document.getElementById('confModServicosManterContratoP3:cboTipoConsultaSaldo28').value == null || document.getElementById('confModServicosManterContratoP3:cboTipoConsultaSaldo28').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoDeConsultaDeSaldo + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContratoP3:cboProcessamentoEfetivacaoPagamento28').value == null || document.getElementById('confModServicosManterContratoP3:cboProcessamentoEfetivacaoPagamento28').value == 0) {
			campos = campos + msgCampo + ' ' + msgProcessamentoEfetivacaoPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContratoP3:cboTratFevDataPag28').value == null || document.getElementById('confModServicosManterContratoP3:cboTratFevDataPag28').value == 0) {
			campos = campos + msgCampo + ' ' + msgTratamentoFeriadosDataPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP3:rdoPermiteFavConPag28');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteFavorecidoConsultarPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confModServicosManterContratoP3:cboTipoRejeicaoAgendamento28').value == null || document.getElementById('confModServicosManterContratoP3:cboTipoRejeicaoAgendamento28').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoAgendamento + ' ' + msgNecessario + '!' + '\n';
		}	
		
		if(document.getElementById('confModServicosManterContratoP3:cboTipoRejeicaoEfetivacao28').value == null || document.getElementById('confModServicosManterContratoP3:cboTipoRejeicaoEfetivacao28').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoEfetivacao + ' ' + msgNecessario + '!' + '\n';
		}			
		
		var qtdeMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContratoP3:txtQtddMaximaRegistroInconLote28'));
		var percMaxRegLote1 = allTrim(document.getElementById('confModServicosManterContratoP3:txtPercentualMaximoRegistroInconsLote28'))
		if(document.getElementById('confModServicosManterContratoP3:cboTipoRejeicaoAgendamento28').value == 3) {
			if(qtdeMaxRegLote1 == '' || qtdeMaxRegLote1 == '0'){
				if(percMaxRegLote1 == '' || percMaxRegLote1 == '0'){
					campos = campos + msgQuantidadePercentualMaximoRegistrosInconsistentesLote + '!' + '\n';
				}		
			}		
		}
		
		if(document.getElementById('confModServicosManterContratoP3:cboPrioridadeDebito28').value == null || document.getElementById('confModServicosManterContratoP3:cboPrioridadeDebito28').value == 0) {
			campos = campos + msgCampo + ' ' + msgPrioridadeDebito + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP3:rdoGerarLancFutDebito28');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgGerarLancamentoFuturoDebito + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP3:rdoUtilizaCadFavControlePag28');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgUtilizaCadastroFavorecidoControlePagamentos + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (radioTemp[1].checked){
			if (allTrim(document.getElementById('confModServicosManterContratoP3:txtValorMaximoFavNaoCad28')) == '') {
				campos = campos + msgCampo + ' ' + msgValorMaximoPagamentoFavorecidoNaoCadastrado + ' ' + msgNecessario + '!' + '\n';
			}
		}
		
		if (allTrim(document.getElementById('confModServicosManterContratoP3:txtValorLimitInd28')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteIndividual + ' ' + msgNecessario + '!' + '\n';
		}/*else{
			valorTemp = parseFloat(document.getElementById('confModServicosManterContratoP3:txtValorLimitInd28').value.replace(',','.'));
			if (valorTemp == '0.00'){
				campos = campos + msgCampo + ' ' + msgValorLimiteIndividual + ' ' + msgNecessario + '!' + '\n';
			}
		}*/
		
		if (allTrim(document.getElementById('confModServicosManterContratoP3:txtValorLimDiario28')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteDiario + ' ' + msgNecessario + '!' + '\n';
		}/*else{
			valorTemp = parseFloat(document.getElementById('confModServicosManterContratoP3:txtValorLimDiario28').value.replace(',','.'));
			if (valorTemp == '0.00'){
				campos = campos + msgCampo + ' ' + msgValorLimiteDiario + ' ' + msgNecessario + '!' + '\n';
			}
		}*/	
		
		if (allTrim(document.getElementById('confModServicosManterContratoP3:txtQtddDiasRepique28')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasRepique + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confModServicosManterContratoP3:txtQtddDiasFloating28')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasFloating + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confModServicosManterContratoP3:rdoPermiteContig28');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteContigenciaPagamento + ' ' + msgNecessario + '!' + '\n';
		}
		
		radio2LinhaExtrato = document.getElementsByName('confModServicosManterContratoP3:rdoDemonstra2LinhaExtrato28');
		if (!radio2LinhaExtrato[1].checked && !radio2LinhaExtrato[2].checked) {
			campos += msgCampo + ' Demonstra 2.Linha de Extrato ' + msgNecessario + '!' + '\n';
		}
	}
	
	if (document.getElementById('confModServicosManterContratoP3:cdParametro').value == '32'){
		if(document.getElementById('confModServicosManterContratoP3:cboTipoConsultaSaldo32').value == null || document.getElementById('confModServicosManterContratoP3:cboTipoConsultaSaldo32').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoDeConsultaDeSaldo + ' ' + msgNecessario + '!' + '\n';
		}

		if(document.getElementById('confModServicosManterContratoP3:cboProcessamentoEfetivacaoPagamento32').value == null || document.getElementById('confModServicosManterContratoP3:cboProcessamentoEfetivacaoPagamento32').value == 0) {
			campos = campos + msgCampo + ' ' + msgProcessamentoEfetivacaoPagamento + ' ' + msgNecessario + '!' + '\n';
		}

		if(document.getElementById('confModServicosManterContratoP3:cboTratFevDataPag32').value == null || document.getElementById('confModServicosManterContratoP3:cboTratFevDataPag32').value == 0) {
			campos = campos + msgCampo + ' ' + msgTratamentoFeriadosDataPagamento + ' ' + msgNecessario + '!' + '\n';
		}

		radioTemp = document.getElementsByName('confModServicosManterContratoP3:rdoPermiteFavConPag32');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteFavorecidoConsultarPagamento + ' ' + msgNecessario + '!' + '\n';
		}

		if(document.getElementById('confModServicosManterContratoP3:cboTipoRejeicaoAgendamento32').value == null || document.getElementById('confModServicosManterContratoP3:cboTipoRejeicaoAgendamento32').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoAgendamento + ' ' + msgNecessario + '!' + '\n';
		}	

		if(document.getElementById('confModServicosManterContratoP3:cboTipoRejeicaoEfetivacao32').value == null || document.getElementById('confModServicosManterContratoP3:cboTipoRejeicaoEfetivacao32').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoEfetivacao + ' ' + msgNecessario + '!' + '\n';
		}			

		var qtdeMaxRegLote = allTrim(document.getElementById('confModServicosManterContratoP3:txtQtddMaximaRegistroInconLote32'));
		var percMaxRegLote = allTrim(document.getElementById('confModServicosManterContratoP3:txtPercentualMaximoRegistroInconsLote32'))
		if(document.getElementById('confModServicosManterContratoP3:cboTipoRejeicaoAgendamento32').value == 3) {
			if(qtdeMaxRegLote == '' || qtdeMaxRegLote == '0'){
				if(percMaxRegLote == '' || percMaxRegLote == '0'){
					campos = campos + msgQuantidadePercentualMaximoRegistrosInconsistentesLote + '!' + '\n';
				}		
			}		
		}

		if(document.getElementById('confModServicosManterContratoP3:cboPrioridadeDebito32').value == null || document.getElementById('confModServicosManterContratoP3:cboPrioridadeDebito32').value == 0) {
			campos = campos + msgCampo + ' ' + msgPrioridadeDebito + ' ' + msgNecessario + '!' + '\n';
		}

		radioTemp = document.getElementsByName('confModServicosManterContratoP3:rdoGerarLancFutDebito32');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgGerarLancamentoFuturoDebito + ' ' + msgNecessario + '!' + '\n';
		}

		radioTemp = document.getElementsByName('confModServicosManterContratoP3:rdoUtilizaCadFavControlePag32');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgUtilizaCadastroFavorecidoControlePagamentos + ' ' + msgNecessario + '!' + '\n';
		}

		if (radioTemp[1].checked){
			if (allTrim(document.getElementById('confModServicosManterContratoP3:txtValorMaximoFavNaoCad32')) == '') {
				campos = campos + msgCampo + ' ' + msgValorMaximoPagamentoFavorecidoNaoCadastrado + ' ' + msgNecessario + '!' + '\n';
			}
		}

		if (allTrim(document.getElementById('confModServicosManterContratoP3:txtValorLimitInd32')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteIndividual + ' ' + msgNecessario + '!' + '\n';
		}/*else{
			valorTemp = parseFloat(document.getElementById('confModServicosManterContratoP3:txtValorLimitInd32').value.replace(',','.'));
			if (valorTemp == '0.00'){
				campos = campos + msgCampo + ' ' + msgValorLimiteIndividual + ' ' + msgNecessario + '!' + '\n';
			}
		}*/

		if (allTrim(document.getElementById('confModServicosManterContratoP3:txtValorLimDiario32')) == '') {
			campos = campos + msgCampo + ' ' + msgValorLimiteDiario + ' ' + msgNecessario + '!' + '\n';
		}/*else{
			valorTemp = parseFloat(document.getElementById('confModServicosManterContratoP3:txtValorLimDiario32').value.replace(',','.'));
			if (valorTemp == '0.00'){
				campos = campos + msgCampo + ' ' + msgValorLimiteDiario + ' ' + msgNecessario + '!' + '\n';
			}
		}*/	

		if (allTrim(document.getElementById('confModServicosManterContratoP3:txtQtddDiasRepique32')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasRepique + ' ' + msgNecessario + '!' + '\n';
		}

		if (allTrim(document.getElementById('confModServicosManterContratoP3:txtQtddDiasFloating32')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeDiasFloating + ' ' + msgNecessario + '!' + '\n';
		}
	}

	var feriadoLocal = document.getElementById('confModServicosManterContratoP3:cboFeriadoLocal');
	if (feriadoLocal && feriadoLocal.value == '0') {
		campos = campos + msgCampo + ' ' + '' + msgFeriadoLocal + ' ' + msgNecessario + '!' + '\n';
	}

	if (campos != ''){
		alert(campos);
		campos = '';
		return false;
	}else{
		return true;
	}

}

function validaHistorico(msgcampo, msgnecessario, msgTipoServico, msgData){

	var campos;
	campos = '';
	
	if(document.getElementById('histServicosManterContrato:cboTipoServico').value == null || document.getElementById('histServicosManterContrato:cboTipoServico').value == 0) {
		campos = campos + msgcampo + ' ' + msgTipoServico + ' ' + msgNecessario + '!' + '\n';
	}
	
	if (document.getElementById('histServicosManterContrato:calendarioDe.day').value == null ||
		document.getElementById('histServicosManterContrato:calendarioDe.day').value == ''   ||
		document.getElementById('histServicosManterContrato:calendarioDe.month').value == null ||
		document.getElementById('histServicosManterContrato:calendarioDe.month').value == ''   ||
		document.getElementById('histServicosManterContrato:calendarioDe.year').value == null ||
		document.getElementById('histServicosManterContrato:calendarioDe.year').value == '' ||
		document.getElementById('histServicosManterContrato:calendarioAte.day').value == null ||
		document.getElementById('histServicosManterContrato:calendarioAte.day').value == ''   ||
		document.getElementById('histServicosManterContrato:calendarioAte.month').value == null ||
		document.getElementById('histServicosManterContrato:calendarioAte.month').value == ''   ||
		document.getElementById('histServicosManterContrato:calendarioAte.year').value == null ||
		document.getElementById('histServicosManterContrato:calendarioAte.year').value == '') {
		campos = campos + msgcampo + ' ' + msgData + ' ' + msgNecessario + '!' + '\n';
	}
	
	if (campos != ''){
		alert(campos);
		campos = '';
		return false;
	}else{
		return true;
	}
	
}

function validaHistoricoModalidade(msgcampo, msgnecessario, msgTipoServico,msgModalidade ,msgData){
	var campos;
	campos = '';
	
	if(document.getElementById('histServicosModalidade:cboTipoServico').value == null || document.getElementById('histServicosModalidade:cboTipoServico').value == 0) {
		campos = campos + msgcampo + ' ' + msgTipoServico + ' ' + msgNecessario + '!' + '\n';
	}
	if(document.getElementById('histServicosModalidade:cboModalidade').value == null || document.getElementById('histServicosModalidade:cboModalidade').value == 0) {
		campos = campos + msgcampo + ' ' + msgModalidade + ' ' + msgNecessario + '!' + '\n';
	}
	
	if (document.getElementById('histServicosModalidade:calendarioDe.day').value == null ||
		document.getElementById('histServicosModalidade:calendarioDe.day').value == ''   ||
		document.getElementById('histServicosModalidade:calendarioDe.month').value == null ||
		document.getElementById('histServicosModalidade:calendarioDe.month').value == ''   ||
		document.getElementById('histServicosModalidade:calendarioDe.year').value == null ||
		document.getElementById('histServicosModalidade:calendarioDe.year').value == '' ||
		document.getElementById('histServicosModalidade:calendarioAte.day').value == null ||
		document.getElementById('histServicosModalidade:calendarioAte.day').value == ''   ||
		document.getElementById('histServicosModalidade:calendarioAte.month').value == null ||
		document.getElementById('histServicosModalidade:calendarioAte.month').value == ''   ||
		document.getElementById('histServicosModalidade:calendarioAte.year').value == null ||
		document.getElementById('histServicosModalidade:calendarioAte.year').value == '') {
		campos = campos + msgcampo + ' ' + msgData + ' ' + msgNecessario + '!' + '\n';
	}
	
	if (campos != ''){
		alert(campos);
		campos = '';
		return false;
	}else{
		return true;
	}	
}
function validaServicosConfiguracaoP2(msgCampo, msgNecessario, 
										msgAberturaContaBancoPostalBradescoSeguros,msgAcaoNaoComprovacao,msgAdesaoSacadoEletronico,msgAgendamentoDebitosPendentesVeiculos,
										msgAgendarTituloRastreadoFilial,msgAgendarTituloRastreadoProprio,msgAgruparCorrespondencias,msgAutorizacaoComplementarAgencia,
										msgBloquearEmissaoPapeleta,msgCadastroUtilizadoRecadastramento,msgCadastroConsultaEnderecoEnvio,msgCapturarTitulosDataRegistro,
										msgCobrarTarifasFuncionarios,msgCodigoFormulario,msgCondicaoEnquadramento,msgControleExpiracaoCredito,
										msgCriterioCompostoEnquadramento,msgCriterioPrincipalEnquadramento,msgDataInicioBloqueioPapeleta,msgDataInicioRastreamento,
										msgDataFimPeriodoAcertoDados,msgDataFimRecadastramento,msgDataInicioPeriodoAcertoDados,msgDataInicioRecadastramento,
										msgDataLimiteEnquadramentoConvenioContaSalario,msgDataLimiteVinculoClienteBeneficiario,msgDemonstraInformacoesAreaReservada,msgDestinoEnvio,
										msgDestinoEnvioCorrespondencia,msgEfetuaConsistenciaEspecieBeneficio,
										msgEmissaoAntecipadaCartaoContaSalario,msgEmiteAvisoComprovacaoVida,msgEmiteMsgRecadastramento,msgExigeLiberacaoLoteProcessado,
										msgFormaAutorizacaoPagamentos,msgFormaEnvioPagamentos,msgFormaManutencaoCadastroFavorecidos,msgFormaManutencaoCadastroProcuradores,
										msgFormularioImpressao,msgGerarLancamentoFuturoCredito,msgGerarLancamentoFuturoDebito,msgGerarLancamentoProgramado,
										msgGerarRetornoOperacoesRealizadasInternet,msgInformaAgenciaContaCredito,msgMidiaDisponibilizacaoCorrentista,msgMidiaMensagemRecadastramento,
										msgMomentoEnvio,msgMomentoIndicacaoCreditoEfetivado,msgOcorrenciaDebito,msgPercentualMaximoRegistrosInconsistentesLote,
										msgPeriodicidadeEmissao,msgPeriodicidadeManutencaoCadastroProcuradores,msgPeriodicidadeEnvioRemessaManutencao,msgPeriodicidadePesquisaDebitosPendentesVeiculos,
										msgPermiteAcertosDados,msgPermiteAnteciparRecadastramento,msgPermiteContigenciaPagamento,msgPermiteDebitoOnline,
										msgPermiteEnvioRemessaManutencaoCadastro,msgPermiteEstornoPagamento,msgPermiteFavorecidoConsultarPagamento,msgPermitePagarMenor,
										msgPermitePagarVencido,msgPermitirAgrupamentoCorrespondencia,msgPesquisaDe,msgPossuiExpiracaoCredito,
										msgPreAutorizacaoCliente,msgPrioridadeDebito,msgProcessamentoEfetivacaoPagamento,msgQuantidadeDiasFloating,
										msgQuantidadeDiasRepique,msgQuantidadeDiasEmissaoAvisoAntesInicioComprovacao,msgQuantidadeDiasEmissaoAvisoAntesVencimento,msgQuantidadeDiasExpiracaoCredito,
										msgQuantidadeDiasAvisoAntecipado,msgQuantidadeDiasEnvioAntecipadoFormulario,msgQuantidadeDiasParaExpiracao,msgQuantidadeDiasParaInativacaoFavorecido,
										msgQuantidadeFasesEtapa,msgQuantidadeLinhasComprovante,msgQuantidadeMesesEmissao,msgQuantidadeMesesPeriodicidadeComprovacao,
										msgQuantidadeMesesEtapa,msgQuantidadeMesesFase,msgQuantidadeViasEmitir,msgQuantidadeViasPagasRegistro,
										msgQuantidadeEtapasRecadastramento,msgQuantidadeLimiteCartaoContaSalarioSolicitacao,msgQuantidadeLimiteDiasParaPagamentoVencido,msgQuantidadeMaximaRegistroInconsistentesLote,
										msgRastrearNotasFiscais,msgRastrearTituloTerceiros,msgTextoInformacaoAreaReservada,msgTipoCartaoContaSalario,
										msgTipoConsistenciaCpfCnpjProprietario,msgTipoConsistenciaIdentificacaoBeneficiario,msgTipoCargaCadastroBeneficiario,msgTipoConsistenciaInscricaoFavorecido,
										msgTipoConsistenciaCpfCnpjFavorecido,msgTipoConsolidacaoPagamentosComprovante,msgTipoDataControleFloating,msgTipoEmissao,
										msgTipoFormacaoListaDebito,msgTipoInscricaoFavorecido,msgTipoRastreamentoTitulos,msgTipoRejeicaoLote,
										msgTipoRejeicaoEfetivacao,msgTipoRejeicaoAgendamento,msgTipoTratamentoContaTransferida,msgTipoIdentificacaoBeneficiario,
										msgTratamentoFeriadosDataPagamento,msgTratamentoFeriadoFimVigencia,msgTratamentoListaDebitoSemNumeracao,msgTratamentoValorDivergente,
										msgUtilizaCadastroFavorecidoControlePagamentos,msgUtilizaCafastroOrgaosPagadores,msgUtilizaCadastroProcuradores,
										msgUtilizaFrasesPreCadastradas,msgUtilizaListaDebito,msgUtilizaMensagemPersonalizadaOnline,msgValidarNomeFavorecidoReceitaFederal,
										msgValorLimiteDiario,msgValorLimiteIndividual,msgValorMaximoPagamentoFavorecidoNaoCadastrado,
										msgTipoContaCredito,msgTipoDeConsultaDeSaldo,msgPermiteCorrespondenciaAberta,
										msgMeioDisponibilizacaoCorrentista, msgMeioDisponibilizacaoNaoCorrentista){

	var campos;
	campos = '';

	if ((document.getElementById('confServicosManterContratoP2:cdParametro').value == '12') || (document.getElementById('confServicosManterContratoP2:cdParametro').value == '13')){
		
		if(document.getElementById('confServicosManterContratoP2:cboTipoRejeicao12').value == null || document.getElementById('confServicosManterContratoP2:cboTipoRejeicao12').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoRejeicaoLote + ' ' + msgNecessario + '!' + '\n';
		}

		if(document.getElementById('confServicosManterContratoP2:cboMeioDisponiCorren12').value == null || document.getElementById('confServicosManterContratoP2:cboMeioDisponiCorren12').value == 0) {
			campos = campos + msgCampo + ' ' + msgMeioDisponibilizacaoCorrentista + ' ' + msgNecessario + '!' + '\n';
		}	
		
		if(document.getElementById('confServicosManterContratoP2:cboMidiaDiposniCorre12').value == null || document.getElementById('confServicosManterContratoP2:cboMidiaDiposniCorre12').value == 0) {
			campos = campos + msgCampo + ' ' + msgMidiaDisponibilizacaoCorrentista + ' ' + msgNecessario + '!' + '\n';
		}			
		
		if(document.getElementById('confServicosManterContratoP2:cboMeioDisponiNaoCorren12').value == null || document.getElementById('confServicosManterContratoP2:cboMeioDisponiNaoCorren12').value == 0) {
			campos = campos + msgCampo + ' ' + msgMeioDisponibilizacaoNaoCorrentista + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confServicosManterContratoP2:cboDestinoEnvioCorres12').value == null || document.getElementById('confServicosManterContratoP2:cboDestinoEnvioCorres12').value == 0) {
			campos = campos + msgCampo + ' ' + msgDestinoEnvioCorrespondencia + ' ' + msgNecessario + '!' + '\n';
		}
		
		if(document.getElementById('confServicosManterContratoP2:cboCadConEndEnvio12').value == null || document.getElementById('confServicosManterContratoP2:cboCadConEndEnvio12').value == 0) {
			campos = campos + msgCampo + ' ' + msgCadastroConsultaEnderecoEnvio + ' ' + msgNecessario + '!' + '\n';
		}
		
		radioTemp = document.getElementsByName('confServicosManterContratoP2:rdoUtilizaFrasesPreCad12');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgUtilizaFrasesPreCadastradas + ' ' + msgNecessario + '!' + '\n';
		}			
		
		radioTemp = document.getElementsByName('confServicosManterContratoP2:rdoCobrarTafFuncionarios12');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgCobrarTarifasFuncionarios + ' ' + msgNecessario + '!' + '\n';
		}
			
		if (allTrim(document.getElementById('confServicosManterContratoP2:txtQtddMesesEmissao')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeMesesEmissao + ' ' + msgNecessario + '!' + '\n';
		}
			
		if (allTrim(document.getElementById('confServicosManterContratoP2:txtQtddViaEmitir12')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeViasEmitir + ' ' + msgNecessario + '!' + '\n';
		}			
		
		if (allTrim(document.getElementById('confServicosManterContratoP2:txtQtddViasPagasRegistro12')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeViasPagasRegistro + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confServicosManterContratoP2:txtQtddLinhasComprovante12')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeLinhasComprovante + ' ' + msgNecessario + '!' + '\n';
		}
		
		if (allTrim(document.getElementById('confServicosManterContratoP2:txtFormularioImpressao12')) == '') {
			campos = campos + msgCampo + ' ' + msgFormularioImpressao + ' ' + msgNecessario + '!' + '\n';
		}
			
		radioTemp = document.getElementsByName('confServicosManterContratoP2:rdoGerarRetOpeReali12');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgGerarRetornoOperacoesRealizadasInternet + ' ' + msgNecessario + '!' + '\n';
		}
	}
	
	if(document.getElementById('confServicosManterContratoP2:cdParametro').value == '14'){
		if(document.getElementById('confServicosManterContratoP2:cboTipoIdentificacaoBeneficiario').value == null || document.getElementById('confServicosManterContratoP2:cboTipoIdentificacaoBeneficiario').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoIdentificacaoBeneficiario + ' ' + msgNecessario + '!' + '\n';
		}

		if(document.getElementById('confServicosManterContratoP2:cboTipoConsistenciaIdentificacaoBeneficiario').value == null || document.getElementById('confServicosManterContratoP2:cboTipoConsistenciaIdentificacaoBeneficiario').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoConsistenciaIdentificacaoBeneficiario + ' ' + msgNecessario + '!' + '\n';
		} 
 
		if(document.getElementById('confServicosManterContratoP2:cboCondicaoEnquadramento').value == null || document.getElementById('confServicosManterContratoP2:cboCondicaoEnquadramento').value == 0) {
			campos = campos + msgCampo + ' ' + msgCondicaoEnquadramento + ' ' + msgNecessario + '!' + '\n';
		}
 
		if(document.getElementById('confServicosManterContratoP2:cboCritPrincEnquadramento').value == null || document.getElementById('confServicosManterContratoP2:cboCritPrincEnquadramento').value == 0) {
			campos = campos + msgCampo + ' ' + msgCriterioPrincipalEnquadramento + ' ' + msgNecessario + '!' + '\n';
		}
 
		if(document.getElementById('confServicosManterContratoP2:cboCritCompEnquadramento').value == null || document.getElementById('confServicosManterContratoP2:cboCritCompEnquadramento').value == 0) {
			campos = campos + msgCampo + ' ' + msgCriterioCompostoEnquadramento + ' ' + msgNecessario + '!' + '\n';
		}
 
		if (allTrim(document.getElementById('confServicosManterContratoP2:quantidadeEtapasRecadastramento')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeEtapasRecadastramento + ' ' + msgNecessario + '!' + '\n';
		}
 
		if (allTrim(document.getElementById('confServicosManterContratoP2:qtddFasesPorEtapa')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeFasesEtapa + ' ' + msgNecessario + '!' + '\n';
		}	

		if (allTrim(document.getElementById('confServicosManterContratoP2:qtddMesesPorEtapa')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeMesesEtapa + ' ' + msgNecessario + '!' + '\n';
		}

		if (allTrim(document.getElementById('confServicosManterContratoP2:qtddMesesPorFase')) == '') {
			campos = campos + msgCampo + ' ' + msgQuantidadeMesesFase + ' ' + msgNecessario + '!' + '\n';
		}
 
		if(document.getElementById('confServicosManterContratoP2:cboCadUtilizadoRec').value == null || document.getElementById('confServicosManterContratoP2:cboCadUtilizadoRec').value == 0) {
			campos = campos + msgCampo + ' ' + msgCadastroUtilizadoRecadastramento + ' ' + msgNecessario + '!' + '\n';
		}
 
		if (document.getElementById('confServicosManterContratoP2:dtInicioRecadastramento.day').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtInicioRecadastramento.day')) == ''   ||
			document.getElementById('confServicosManterContratoP2:dtInicioRecadastramento.month').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtInicioRecadastramento.month')) == ''   ||
			document.getElementById('confServicosManterContratoP2:dtInicioRecadastramento.year').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtInicioRecadastramento.year')) == '') {
				campos = campos + msgCampo + ' ' + msgDataInicioRecadastramento + ' ' + msgNecessario + '!' + '\n';
		}
 
		if (document.getElementById('confServicosManterContratoP2:dtFimRecadastramento.day').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtFimRecadastramento.day')) == ''   ||
			document.getElementById('confServicosManterContratoP2:dtFimRecadastramento.month').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtFimRecadastramento.month')) == ''   ||
			document.getElementById('confServicosManterContratoP2:dtFimRecadastramento.year').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtFimRecadastramento.year')) == '') {
				campos = campos + msgCampo + ' ' + msgDataFimRecadastramento + ' ' + msgNecessario + '!' + '\n';
		}
 
		radioTemp = document.getElementsByName('confServicosManterContratoP2:rdoPermiteEnvioRemessaManutencaoCadastro');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteEnvioRemessaManutencaoCadastro + ' ' + msgNecessario + '!' + '\n';
		}
 

		if(document.getElementById('confServicosManterContratoP2:cboPeriodicidadeEnvioRemessaManutencao').value == null || document.getElementById('confServicosManterContratoP2:cboPeriodicidadeEnvioRemessaManutencao').value == 0) {
			campos = campos + msgCampo + ' ' + msgPeriodicidadeEnvioRemessaManutencao + ' ' + msgNecessario + '!' + '\n';
		}
 
		if(document.getElementById('confServicosManterContratoP2:cboTipoCargaCadastroBeneficiario').value == null || document.getElementById('confServicosManterContratoP2:cboTipoCargaCadastroBeneficiario').value == 0) {
			campos = campos + msgCampo + ' ' + msgTipoCargaCadastroBeneficiario + ' ' + msgNecessario + '!' + '\n';
		}
 
		if (document.getElementById('confServicosManterContratoP2:dtLimiteVincClienteBenef.day').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtLimiteVincClienteBenef.day')) == ''   ||
			document.getElementById('confServicosManterContratoP2:dtLimiteVincClienteBenef.month').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtLimiteVincClienteBenef.month')) == ''   ||
			document.getElementById('confServicosManterContratoP2:dtLimiteVincClienteBenef.year').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtLimiteVincClienteBenef.year')) == '') {
				campos = campos + msgCampo + ' ' + msgDataLimiteVinculoClienteBeneficiario + ' ' + msgNecessario + '!' + '\n';
		}
 
		radioTemp = document.getElementsByName('confServicosManterContratoP2:rdoPermiteAcertosDados');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteAcertosDados + ' ' + msgNecessario + '!' + '\n';
		}
 
		radioTemp = document.getElementsByName('confServicosManterContratoP2:rdoPermiteAnteciparRecadastramento');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgPermiteAnteciparRecadastramento + ' ' + msgNecessario + '!' + '\n';
		}

		if (document.getElementById('confServicosManterContratoP2:dtInicioPerAcertoDados.day').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtInicioPerAcertoDados.day')) == ''   ||
			document.getElementById('confServicosManterContratoP2:dtInicioPerAcertoDados.month').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtInicioPerAcertoDados.month')) == ''   ||
			document.getElementById('confServicosManterContratoP2:dtInicioPerAcertoDados.year').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtInicioPerAcertoDados.year')) == '') {
				campos = campos + msgCampo + ' ' + msgDataInicioPeriodoAcertoDados + ' ' + msgNecessario + '!' + '\n';
		}
 
		if (document.getElementById('confServicosManterContratoP2:dtFimPerAcertoDados.day').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtFimPerAcertoDados.day')) == ''   ||
			document.getElementById('confServicosManterContratoP2:dtFimPerAcertoDados.month').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtFimPerAcertoDados.month')) == ''   ||
			document.getElementById('confServicosManterContratoP2:dtFimPerAcertoDados.year').value == null ||
			allTrim(document.getElementById('confServicosManterContratoP2:dtFimPerAcertoDados.year')) == '') {
				campos = campos + msgCampo + ' ' + msgDataFimPeriodoAcertoDados + ' ' + msgNecessario + '!' + '\n';
		}
 
		radioTemp = document.getElementsByName('confServicosManterContratoP2:rdoEmiteMsgRecadMidia');
		if (!radioTemp[1].checked && !radioTemp[2].checked) {
			campos = campos + msgCampo + ' ' + msgEmiteMsgRecadastramento + ' ' + msgNecessario + '!' + '\n';
		} 
		
		if(radioTemp[1].checked){
			if(document.getElementById('confServicosManterContratoP2:cboMidiaMsgRecad').value == null || document.getElementById('confServicosManterContratoP2:cboMidiaMsgRecad').value == 0) {
				campos = campos + msgCampo + ' ' + msgMidiaMensagemRecadastramento + ' ' + msgNecessario + '!' + '\n';
			}
		}

		//radioTemp = document.getElementsByName('confServicosManterContratoP2:rdoGerarRetornoOperRealizadaInternet');
		//if (!radioTemp[1].checked && !radioTemp[2].checked) {
		//	campos = campos + msgCampo + ' ' + msgGerarRetornoOperacoesRealizadasInternet + ' ' + msgNecessario + '!' + '\n';
		//} 
	}

	if (campos != ''){
		alert(campos);
		campos = '';
		return false;
	}else{
		return true;
	}
	
}


function replaceAll(string){

	  while (string.indexOf(".") > 0){
        string = string.replace(".","");	
	  }
	  return string;
}	

function limparDtoRepresentante(idBase) {
	document.getElementById(idBase + 'inputDsNome').value = '';
	document.getElementById(idBase + 'inputCdAgencia').value = '';
	document.getElementById(idBase + 'inputCdConta').value = '';
	document.getElementById(idBase + 'inputCdContaDigito').value = '';
}