function validaPesquisarContas(msgcampo, msgnecessario, msgcnpj,msgcpf, msgbanco, msgagencia, msgconta, msgdigito, msgtipoconta,msgfinalidade){
	
	var objRadio = document.getElementsByName('filtroRadio');
	

	if (objRadio[0].checked) {
		if (allTrim(document.getElementById('bloquearDesbloquearContasContratoConta:txtCnpj')) == "") {
			alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
			document.getElementById('bloquearDesbloquearContasContratoConta:txtCnpj').focus();
			return false;		
		} else {
			if (allTrim(document.getElementById('bloquearDesbloquearContasContratoConta:txtFilial')) == "") {
				alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
				document.getElementById('bloquearDesbloquearContasContratoConta:txtFilial').focus();
				return false;		
			} else {
				if (allTrim(document.getElementById('bloquearDesbloquearContasContratoConta:txtControle')) == ""){
					alert(msgcampo + ' ' + msgcnpj + ' ' + msgnecessario);
					document.getElementById('bloquearDesbloquearContasContratoConta:txtControle').focus();
					return false;		
				}
			}
		}
	}else if (objRadio[1].checked) {
		if (allTrim(document.getElementById('bloquearDesbloquearContasContratoConta:txtCpf')) == "") {
			alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
			document.getElementById('bloquearDesbloquearContasContratoConta:txtCpf').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById('bloquearDesbloquearContasContratoConta:txtControleCpf')) == "") {
				alert(msgcampo + ' ' + msgcpf + ' ' + msgnecessario);
				document.getElementById('bloquearDesbloquearContasContratoConta:txtControleCpf').focus();
				return false;	
			}
		}
	}else if (objRadio[2].checked) {
	
		var campos = '';
		if (allTrim(document.getElementById('bloquearDesbloquearContasContratoConta:banco')) == '') {
			campos = campos + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '\n';				
		} 
		if(allTrim(document.getElementById('bloquearDesbloquearContasContratoConta:agencia')) == '') {
			campos = campos +  msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n';			
		}
		if(allTrim(document.getElementById('bloquearDesbloquearContasContratoConta:conta')) == '') {
			campos = campos +  msgcampo + ' ' + msgconta + ' ' + msgnecessario + '\n';		
		}else{
			if(allTrim(document.getElementById('bloquearDesbloquearContasContratoConta:digito')) == '') {
				campos = campos +  msgcampo + ' ' + msgdigito + ' ' + msgnecessario + '\n';
			}
		}
		if(document.getElementById('bloquearDesbloquearContasContratoConta:tipoConta').value == null || document.getElementById('bloquearDesbloquearContasContratoConta:tipoConta').value == 0) {
			campos = campos +  msgcampo + ' ' + msgtipoconta + ' ' + msgnecessario + '\n';
		}								
			
		if (campos != ''){
			alert(campos);
			return false;
		}
		
	}else{ 
		if (objRadio[3].checked) {
			if ( document.getElementById('bloquearDesbloquearContasContratoConta:finalidade').value == null || document.getElementById('bloquearDesbloquearContasContratoConta:finalidade').value == 0) {
				alert(msgcampo + ' ' + msgfinalidade + ' ' + msgnecessario);
				document.getElementById('bloquearDesbloquearContasContratoConta:finalidade').focus();
				return false;		
			}
		}	
	} 
	
	return true;						
}