function validaCampos(msgcampo, msgnecessario, cliente, msgempresa, msgtipo, msg){

	if (document.getElementById('conManterSolManutContratoForm:empresaGestora').value == 0) {
		alert(msgcampo + ' ' + msgempresa + ' ' + msgnecessario);
		document.getElementById('conManterSolManutContratoForm:empresaGestora').focus();
		return false;		
	} 
	if (document.getElementById('conManterSolManutContratoForm:tipoContrato').value == 0) {
		alert(msgcampo + ' ' + msgtipo + ' ' + msgnecessario);
		document.getElementById('conManterSolManutContratoForm:tipoContrato').focus();
		return false;		
	} 
		
	if (document.getElementById('conManterSolManutContratoForm:empresaGestora').value == 0 && document.getElementById('conManterSolManutContratoForm:tipoContrato').value == 0 && document.getElementById('conManterSolManutContratoForm:numero').value == '')
	{
		if (cliente == ''){
		
			if ((document.getElementById('conManterSolManutContratoForm:agenciaOperadora').value == 0) || (document.getElementById('conManterSolManutContratoForm:gerenteNome').value == 0)
				|| (document.getElementById('conManterSolManutContratoForm:situacao').value == 0)) {
	
				alert(msg);
				return false;		
			}
		}
	}		
	return true;
}