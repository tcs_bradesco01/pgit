function validaCamposAvancarAlterarMeios(form, msgcampo, msgnecessario, msgnivelcontrole, msgtipocontrole, msgperiodicidade,msgMeioTransmissaoPrincipalRemessa,
		msgMeioTransmissaoPrincipalRetorno,msgsEmpresaResponsavelTransmissao,msgsResponsavelCustoTransmissaoArquivo,msgsVolumeMensalRegistrosTrafegados,
		msgsNomeCliente,msgsDDDCliente,msgsTelefoneCliente,msgsRamalCliente,msgsEmailCliente,msgsNomeSolicitante,msgsDDDSolicitante,msgsTelefoneSolicitante,msgsRamalSolicitante,msgsEmailSolicitante,
		msgsJustificativa,msgsJuncaoRespCustoTransm,msgsPercentualCustoTransmissaoArquivo){

	var msgValidacao = '';
	var flagMeioTransmissaoPrincipalRemessa = true;
	var flagMeioTransmissaoAlternativoRemessa = true;
	var flagMeioTransmissaoAlternativoRetorno = true;
	var flagMeioTransmissaoPrincipalRetorno= true;
	var flagMeioTransmissao14 = true ;
	var flagCustoTransmissao = true;

	if (document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value == null || document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value < 1 ) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgMeioTransmissaoPrincipalRemessa + ' ' + msgnecessario + '!\n';
	}

	if (document.getElementById(form.id +':cboMeioTransmissaoAlternativoRemessa').value == null || document.getElementById(form.id +':cboMeioTransmissaoAlternativoRemessa').value < 1 ) {
//		msgValidacao = msgValidacao + msgcampo + ' ' + 'Meio de Transmiss\u00e3o Alternativo ( Remessa )' + ' ' + msgnecessario + '!\n';
	}

	if (document.getElementById(form.id +':cboNivelControleRetorno').value == null || document.getElementById(form.id +':cboNivelControleRetorno').value < 1 ) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgMeioTransmissaoPrincipalRetorno + ' ' + msgnecessario + '!\n';
	} else if (document.getElementById(form.id +':cboNivelControleRetorno').value == 1 || document.getElementById(form.id +':cboNivelControleRetorno').value == 2 ) {
		if (document.getElementById(form.id +':cboTipoControleRetorno').value == null || document.getElementById(form.id +':cboTipoControleRetorno').value < 1 ) {
//			msgValidacao = msgValidacao + msgcampo + ' ' + 'Meio de Transmiss\u00e3o Alternativo ( Retorno ) '  +  msgnecessario + '!\n';
		}
	}
	
	if(document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value  != '0'){
		switch(document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value ){
			case '7':flagMeioTransmissaoPrincipalRemessa = false;break;
			case '8':flagMeioTransmissaoPrincipalRemessa = false;break;
			case '9':flagMeioTransmissaoPrincipalRemessa = false;break;
			default:flagMeioTransmissaoPrincipalRemessa =true;
		}
	}
	if(document.getElementById(form.id +':cboMeioTransmissaoAlternativoRemessa').value  != '0'){
		switch(document.getElementById(form.id +':cboMeioTransmissaoAlternativoRemessa').value){
			case '7': flagMeioTransmissaoAlternativoRemessa = false;break;
			case '8': flagMeioTransmissaoAlternativoRemessa = false;break;
			case '9': flagMeioTransmissaoAlternativoRemessa = false;break;
			default:flagMeioTransmissaoAlternativoRemessa =true;
		}
	}
	if(document.getElementById(form.id +':cboTipoControleRetorno').value != '0'){
		switch(document.getElementById(form.id +':cboTipoControleRetorno').value){
			case '7': flagMeioTransmissaoAlternativoRetorno = false;break;
			case '8':flagMeioTransmissaoAlternativoRetorno = false;break;
			case '9':flagMeioTransmissaoAlternativoRetorno = false;break;
			default:flagMeioTransmissaoAlternativoRetorno =true;
		}
	}
	if(document.getElementById(form.id +':cboNivelControleRetorno').value != '0'){
		switch(document.getElementById(form.id +':cboNivelControleRetorno').value){
			case '7': flagMeioTransmissaoPrincipalRetorno = false;break;
			case '8': flagMeioTransmissaoPrincipalRetorno = false;break;
			case '9': flagMeioTransmissaoPrincipalRetorno = false;break;
			default:flagMeioTransmissaoPrincipalRetorno =true;
		}
	}
	if(document.getElementById(form.id +':cboMeioTransmissaoPrincipalRemessa').value == '14' 
		|| document.getElementById(form.id +':cboMeioTransmissaoAlternativoRemessa').value == '14' 
		|| document.getElementById(form.id +':cboTipoControleRetorno').value == '14' 
		|| document.getElementById(form.id +':cboNivelControleRetorno').value == '14')
	{
		 flagMeioTransmissao14 = false;
		 flagCustoMeioTransmissao = false;
	}

	if(flagMeioTransmissao14 == false){
		
		if(document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '1' || document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '3' ||
				document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '2'){
			if(document.getElementById(form.id +':txtJuncaoRespCustoTransm').value == null || document.getElementById(form.id +':txtJuncaoRespCustoTransm').value == ''){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsJuncaoRespCustoTransm + ' ' + msgnecessario + '!\n';
			}
		}	
			
		if(document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '3'){
			if(document.getElementById(form.id +':txtPercentualCustoTransArq').value == null || document.getElementById(form.id +':txtPercentualCustoTransArq').value == ''){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsPercentualCustoTransmissaoArquivo + ' ' + msgnecessario + '!\n';
			}
		}
		
		if (document.getElementById(form.id +':cboEmpresaRespTransDadosControle').value == null || document.getElementById(form.id +':cboEmpresaRespTransDadosControle').value == '0' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsEmpresaResponsavelTransmissao + ' ' + msgnecessario + '!\n';
		}

		if (document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == null || document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value =='0' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsResponsavelCustoTransmissaoArquivo + ' ' + msgnecessario + '!\n';
		}

		if (document.getElementById(form.id +':txtVolumeMensalRegTraf').value == null || document.getElementById(form.id +':txtVolumeMensalRegTraf').value == '' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsVolumeMensalRegistrosTrafegados + ' ' + msgnecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':txtNomeContato').value == null || document.getElementById(form.id +':txtNomeContato').value == '' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsNomeCliente + ' ' + msgnecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':txtDDDContato').value == null || document.getElementById(form.id +':txtDDDContato').value == '' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsDDDCliente + ' ' + msgnecessario + '!\n';
		}

		if (document.getElementById(form.id +':txtTelefoneContato').value == null || document.getElementById(form.id +':txtTelefoneContato').value == '' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsTelefoneCliente + ' ' + msgnecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':txtEmailContato').value == null || document.getElementById(form.id +':txtEmailContato').value == '' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsEmailCliente + ' ' + msgnecessario + '!\n';
		}else{
			email = document.getElementById(form.id + ':txtEmailContato').value
			var iArroba = 0;
			var subEmail = '';
			var erro = 0;

			if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
				iArroba = email.indexOf('@');
				proxArroba = email.substring(iArroba + 1, iArroba + 2);
				if (iArroba < 1)
				erro = 1;
				if (proxArroba == '.')
				erro = 1;
				if (email.substring(email.length - 1, email.length) == '.')
				erro = 1;
			} else {
				erro = 1;
			}

		    if (erro == 1){
		    	msgValidacao = msgValidacao + msgcampo + ' ' + msgsEmailCliente + ' ' + 'inv�lido' + '!\n';
		    }
		}

		if (document.getElementById(form.id +':txtNomeSolictante').value == null || document.getElementById(form.id +':txtNomeSolictante').value == '' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsNomeSolicitante + ' ' + msgnecessario + '!\n';
		}

		if (document.getElementById(form.id +':txtDDDSolictante').value == null || document.getElementById(form.id +':txtDDDSolictante').value == '' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsDDDSolicitante + ' ' + msgnecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':txtTelefoneSolictante').value == null || document.getElementById(form.id +':txtTelefoneSolictante').value == '' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsTelefoneSolicitante + ' ' + msgnecessario + '!\n';
		}
		
		if (document.getElementById(form.id +':txtEmailSolictante').value == null || document.getElementById(form.id +':txtEmailSolictante').value == '' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsEmailSolicitante + ' ' + msgnecessario + '!\n';
		}else{
			email = document.getElementById(form.id + ':txtEmailSolictante').value
			var iArroba = 0;
			var subEmail = '';
			var erro = 0;

			if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
				iArroba = email.indexOf('@');
				proxArroba = email.substring(iArroba + 1, iArroba + 2);
				if (iArroba < 1)
				erro = 1;
				if (proxArroba == '.')
				erro = 1;
				if (email.substring(email.length - 1, email.length) == '.')
				erro = 1;
			} else {
				erro = 1;
			}

		    if (erro == 1){
		    	msgValidacao = msgValidacao + msgcampo + ' ' + msgsEmailSolicitante + ' ' +  'inv�lido' + '!\n';
		    }
		}
		
		if (document.getElementById(form.id +':txtJustificativa').value == null || document.getElementById(form.id +':txtJustificativa').value == '' ) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgsJustificativa + ' ' + msgnecessario + '!\n';
		}
		}else if(flagMeioTransmissaoPrincipalRemessa == false 
			 ||flagMeioTransmissaoAlternativoRemessa == false 
			 || flagMeioTransmissaoAlternativoRetorno == false 
			 || flagMeioTransmissaoPrincipalRetorno == false )	
		{	
			
			if(document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '1' || document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '3' ||
					document.getElementById(form.id +':cboRespCustoTransArqDadosControle').value == '2'){
				if(document.getElementById(form.id +':txtJuncaoRespCustoTransm').value == null || document.getElementById(form.id +':txtJuncaoRespCustoTransm').value == ''){
					msgValidacao = msgValidacao + msgcampo + ' ' + msgsJuncaoRespCustoTransm + ' ' + msgnecessario + '!\n';
				}
			}
		
			if (document.getElementById(form.id +':txtVolumeMensalRegTraf').value == null || document.getElementById(form.id +':txtVolumeMensalRegTraf').value == '' ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsVolumeMensalRegistrosTrafegados + ' ' + msgnecessario + '!\n';
			}
		
			if (document.getElementById(form.id +':txtNomeContato').value == null || document.getElementById(form.id +':txtNomeContato').value == '' ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsNomeCliente + ' ' + msgnecessario + '!\n';
			}
			
			if (document.getElementById(form.id +':txtDDDContato').value == null || document.getElementById(form.id +':txtDDDContato').value == '' ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsDDDCliente + ' ' + msgnecessario + '!\n';
			}
		
			if (document.getElementById(form.id +':txtTelefoneContato').value == null || document.getElementById(form.id +':txtTelefoneContato').value == '' ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsTelefoneCliente + ' ' + msgnecessario + '!\n';
			}
			
			if (document.getElementById(form.id +':txtEmailContato').value == null || document.getElementById(form.id +':txtEmailContato').value == '' ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsEmailCliente + ' ' + msgnecessario + '!\n';
			}else{
				email = document.getElementById(form.id + ':txtEmailContato').value
				var iArroba = 0;
				var subEmail = '';
				var erro = 0;

				if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
					iArroba = email.indexOf('@');
					proxArroba = email.substring(iArroba + 1, iArroba + 2);
					if (iArroba < 1)
					erro = 1;
					if (proxArroba == '.')
					erro = 1;
					if (email.substring(email.length - 1, email.length) == '.')
					erro = 1;
				} else {
					erro = 1;
				}

			    if (erro == 1){
			    	msgValidacao = msgValidacao + msgcampo + ' ' + msgsEmailCliente + ' ' + 'inv�lido' + '!\n';
			    }
			}
		
			if (document.getElementById(form.id +':txtNomeSolictante').value == null || document.getElementById(form.id +':txtNomeSolictante').value == '' ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsNomeSolicitante + ' ' + msgnecessario + '!\n';
			}
		
			if (document.getElementById(form.id +':txtDDDSolictante').value == null || document.getElementById(form.id +':txtDDDSolictante').value == '' ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsDDDSolicitante + ' ' + msgnecessario + '!\n';
			}
			
			if (document.getElementById(form.id +':txtTelefoneSolictante').value == null || document.getElementById(form.id +':txtTelefoneSolictante').value == '' ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsTelefoneSolicitante + ' ' + msgnecessario + '!\n';
			}
			
			if (document.getElementById(form.id +':txtEmailSolictante').value == null || document.getElementById(form.id +':txtEmailSolictante').value == '' ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsEmailSolicitante + ' ' + msgnecessario + '!\n';
			}else{
				email = document.getElementById(form.id + ':txtEmailSolictante').value
				var iArroba = 0;
				var subEmail = '';
				var erro = 0;

				if (email.indexOf('@') != -1 && email.indexOf('.') != -1 && email.indexOf(' ') < 0) {
					iArroba = email.indexOf('@');
					proxArroba = email.substring(iArroba + 1, iArroba + 2);
					if (iArroba < 1)
					erro = 1;
					if (proxArroba == '.')
					erro = 1;
					if (email.substring(email.length - 1, email.length) == '.')
					erro = 1;
				} else {
					erro = 1;
				}

			    if (erro == 1){
			    	msgValidacao =  msgValidacao + msgcampo + ' ' + msgsEmailSolicitante + ' ' + 'inv�lido' + '!\n';
			    }
			}
			
			if (document.getElementById(form.id +':txtJustificativa').value == null || document.getElementById(form.id +':txtJustificativa').value == '' ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgsJustificativa + ' ' + msgnecessario + '!\n';
			}
		}

	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validarPercentualBanco(form) {
	var banco = document.getElementById(form +':txtBanco');
	var cliente = document.getElementById(form +':txtCliente');
	var percentual = banco.value.replace(",", ".");

	if(parseFloat(percentual) > 100) {
		alert("O campo Banco n\u00e3o pode ser maior que 100!");
		banco.value = "";
		cliente.value = "";
		banco.focus();
		return false;
	}
}