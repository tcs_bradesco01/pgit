function validaCamposFiltro(form, msgcampo, msgnecessario, msgnumero, msgdata, msgversao){

	var objRdoCliente = document.getElementsByName('radioFiltro');
	
	if (objRdoCliente[0].checked) {
		if (document.getElementById(form.id + ':numeroDocumento').value == "") {
			alert(msgcampo + ' ' + msgnumero + ' ' + msgnecessario);
			document.getElementById(form.id +':numeroDocumento').focus();
			return false;		
			}
		if (document.getElementById(form.id + ':versao').value == "") {
			alert(msgcampo + ' ' + msgversao + ' ' + msgnecessario);
			document.getElementById(form.id +':versao').focus();
			return false;		
			}
	} 
	else if (objRdoCliente[1].checked) {
		if (document.getElementById(form.id +':txtDiaInicio').value == "" || document.getElementById(form.id +':txtMesInicio').value == "" || document.getElementById(form.id +':txtAnoInicio').value == "" 
			|| document.getElementById(form.id +':txtDiaFim').value == "" || document.getElementById(form.id +':txtMesFim').value == "" || document.getElementById(form.id +':txtAnoFim').value == "") {
				alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
				document.getElementById(form.id +':txtDiaInicio').focus();
				return false;	
			}
		} 
	
	return true;						
}
