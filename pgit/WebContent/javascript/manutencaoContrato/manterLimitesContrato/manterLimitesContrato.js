function validaCampoLimite(form, msgcampo, msgnecessario, msgnovoLimite){

	
		if (document.getElementById(form.id + ':novoLimite').value == "") {
			alert(msgcampo + ' ' + msgnovoLimite + ' ' + msgnecessario);
			document.getElementById(form.id +':novoLimite').focus();
			return false;		
			}
	
	
	return true;						
}

function validaPesquisarLimiteConta(msgcampo, msgnecessario, msgcnpj, msgcpf, msgbanco, msgagencia, msgconta, msgdigito, msgtipoconta){

var campos= '';
var objRadio = document.getElementsByName('filtroRadio');

	if (objRadio[0].checked) {
	
		var validarCnpj=true;
		if (allTrim(document.getElementById('conLimiteConta:txtCnpj')) == null || allTrim(document.getElementById('conLimiteConta:txtCnpj')) == '') {
			validarCnpj = false;
		}
		if (allTrim(document.getElementById('conLimiteConta:txtFilial')) == null || allTrim(document.getElementById('conLimiteConta:txtFilial')) == '') {
			validarCnpj = false;
		}
		if (allTrim(document.getElementById('conLimiteConta:txtControle')) == null || allTrim(document.getElementById('conLimiteConta:txtControle')) == '') {
			validarCnpj = false;
		}		
		
		if (!validarCnpj){ 
			campos = msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '\n';
			alert(campos);
			return false;	
		}	
		
	}else if (objRadio[1].checked) {
		var validarCpf = true;
		
		if (allTrim(document.getElementById('conLimiteConta:txtCpf')) == null || allTrim(document.getElementById('conLimiteConta:txtCpf')) == '') {
			validarCpf = false;
		}
		if (allTrim(document.getElementById('conLimiteConta:txtControleCpf')) == null || allTrim(document.getElementById('conLimiteConta:txtControleCpf')) == '') {
			validarCpf = false;
		}	
		
		if (!validarCpf){ 
			campos = msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '\n';
			alert(campos);
			return false;	
		}	
	
	}else if (objRadio[2].checked) {
		var validar=true;
		
		if (allTrim(document.getElementById('conLimiteConta:banco')) == '') {
			campos += (msgcampo + ' ' + msgbanco + ' ' + msgnecessario+ '\n');
			validar = false;		
		}
		if(allTrim(document.getElementById('conLimiteConta:agencia')) == '' ) {
				campos += (msgcampo + ' ' + msgagencia + ' ' + msgnecessario+ '\n');
				validar = false;		
		} 
		if(allTrim(document.getElementById('conLimiteConta:conta')) == '') {
					campos += (msgcampo + ' ' + msgconta + ' ' + msgnecessario+ '\n');
					validar = false;		
		}
		
		//if(allTrim(document.getElementById('conLimiteConta:digito')) == '') {
		//			campos += (msgcampo + ' ' + msgdigito + ' ' + msgnecessario+ '\n');
		//			validar = false;		
		//}
		//if(document.getElementById('conLimiteConta:tipoConta').value == null || document.getElementById('conLimiteConta:tipoConta').value == 0) {
		//	campos += (msgcampo + ' ' + msgtipoconta + ' ' + msgnecessario+ '\n');
		//	validar = false;		
		//}
	
		if (!validar){ 
			alert(campos);
			return false;	
		}
	} 
	
	return true;						
}

function replaceAll(string){
	while (string.indexOf(".") > 0){
		string = string.replace(".","");
	}
	string = parseFloat(string.replace(",", ""));
	return string;
}	

function validaCamposAlterarConta(form, msgcampo, msgnecessario, msgvalor, msgadata){ 
	var msgValidacao = '';

	var valorLimite = replaceAll(document.getElementById(form.id +':txtValorLimite').value);

	if(allTrim(document.getElementById(form.id +':txtValorLimite')) != '' && valorLimite != '0'){
		if (document.getElementById(form.id + ':dataPrazoInicio.day').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoInicio.day')) == ''   ||
			document.getElementById(form.id + ':dataPrazoInicio.month').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoInicio.month')) == ''   ||
			document.getElementById(form.id + ':dataPrazoInicio.year').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoInicio.year')) == '' ||
			document.getElementById(form.id + ':dataPrazoFim.day').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoFim.day')) == ''   ||
			document.getElementById(form.id + ':dataPrazoFim.month').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoFim.month')) == ''   ||
			document.getElementById(form.id + ':dataPrazoFim.year').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoFim.year')) == '') {

			msgValidacao = msgValidacao + msgcampo + ' ' + msgadata + ' ' + msgnecessario + '!' + '\n';				
		}
	}

	var valorLimiteTED = replaceAll(document.getElementById(form.id +':txtValorLimiteTED').value);

	if(allTrim(document.getElementById(form.id +':txtValorLimiteTED')) != '' && valorLimiteTED != '0'){
		if (document.getElementById(form.id + ':dataPrazoInicioTED.day').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoInicioTED.day')) == ''   ||
			document.getElementById(form.id + ':dataPrazoInicioTED.month').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoInicioTED.month')) == ''   ||
			document.getElementById(form.id + ':dataPrazoInicioTED.year').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoInicioTED.year')) == '' ||
			document.getElementById(form.id + ':dataPrazoFimTED.day').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoFimTED.day')) == ''   ||
			document.getElementById(form.id + ':dataPrazoFimTED.month').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoFimTED.month')) == ''   ||
			document.getElementById(form.id + ':dataPrazoFimTED.year').value == null ||
			allTrim(document.getElementById(form.id + ':dataPrazoFimTED.year')) == '') {

			msgValidacao = msgValidacao + msgcampo + ' ' + msgadata + ' ' + msgnecessario + '!' + '\n';				
		}
	}

	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;						
}

function validaCampoHistorico(form, msgcampo, msgnecessario, msgvalor, msgadata){
	var msgValidacao = '';
	
	if (document.getElementById(form.id + ':calendarioDe.day').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioDe.day')) == ''   ||
			document.getElementById(form.id + ':calendarioDe.month').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioDe.month')) == ''   ||
			document.getElementById(form.id + ':calendarioDe.year').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioDe.year')) == '' ||
			document.getElementById(form.id + ':calendarioAte.day').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioAte.day')) == ''   ||
			document.getElementById(form.id + ':calendarioAte.month').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioAte.month')) == ''   ||
			document.getElementById(form.id + ':calendarioAte.year').value == null ||
			allTrim(document.getElementById(form.id + ':calendarioAte.year')) == '') {

			msgValidacao = msgValidacao + msgcampo + ' '+ msgadata + ' ' + msgnecessario + '!' + '\n';				
		}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}