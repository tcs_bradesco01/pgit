function validaCamposContratoA(form,msgcampo, msgnecessario, msgempresa, msgtipo, msgnumero){

	var msgValidacao = '';


	if (document.getElementById(form.id + ':empresaGestora').value == 0) {
		msgValidacao =msgValidacao +   msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '!\n';
	} 
	
	if (document.getElementById(form.id + ':tipoContrato').value == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '!\n';

	} 
	
	if (document.getElementById(form.id + ':numero').value == '') {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' ' + msgnecessario + '!\n';

	}
		

	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;
	
}

function validaCamposContratoB(form,msgcampo, msgnecessario, msgempresa, msgtipo, msgnumero){

	var msgValidacao = '';


	if (document.getElementById(form.id + ':empresaGestoraB').value == 0) {
		msgValidacao =msgValidacao +   msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '!\n';
	} 
	
	if (document.getElementById(form.id + ':tipoContratoB').value == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '!\n';

	} 
	
	if (document.getElementById(form.id + ':numeroB').value == '') {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' ' + msgnecessario + '!\n';

	}
		

	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;
	
}

function selecionarTodos(form, check){
	for(i=0; i<form.elements.length; i++) {
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){
			form.elements[i].click();
		}			
	}	
	
	//document.getElementById(form.id + ':btnLimparSelecao').disabled = !check.checked;
	//document.getElementById(form.id + ':btnAlterarSelecionados').disabled = !check.checked;
}

function tiraSelecionarTodos(form){
	document.getElementById(form.id + ':dataTable:checkSelecionarTodos').checked = false;		
}

function desbloquearTela() {
	showDiv(false);
	FormSubmit_bloquerTeclado(false);
}

function bloquearTela() {
	showDiv(true);
	FormSubmit_bloquerTeclado(true);
}