function bloquearRenegociar() {
	document.getElementById("conManterSolManutContratoForm:btnRenegociar").disabled = true;
}

function bloquearRenegociarNovo() {
	document.getElementById("conManterSolManutContratoForm:btnRenegociarNovo").disabled = true;
}

function desbloquearRenegociar() {
	document.getElementById("conManterSolManutContratoForm:btnRenegociar").disabled = false;
}

function desbloquearRenegociarNovo() {
	document.getElementById("conManterSolManutContratoForm:btnRenegociarNovo").disabled = false;
}

function validaCamposConsultaCliente(form, msgcampo, msgnecessario, msgcnpj, msgcpf, msgnome, msgbanco, msgagencia, msgconta){

	var msgValidacao = '';

	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');

	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById(form.id + ':txtCnpj')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';
			
		} else {
			if (allTrim(document.getElementById(form.id +':txtFilial'))== "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';					
			} else {
				if (allTrim(document.getElementById(form.id + ':txtControle')) == ""){
					msgValidacao = msgValidacao +   msgcampo + ' ' + msgcnpj + ' ' + msgnecessario + '!\n';				
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById(form.id +':txtCpf')) == "") {
			msgValidacao = msgValidacao +   msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
			
		} else {
			if(allTrim(document.getElementById(form.id +':txtControleCpf')) == "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgcpf + ' ' + msgnecessario + '!\n';
				
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById(form.id +':txtNomeRazaoSocial')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnome + ' ' + msgnecessario + '!\n';
			
		}
	} else if (objRdoCliente[3].checked) {
		if (allTrim(document.getElementById(form.id +':txtBanco')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '!\n';
				
		} 
		if(allTrim(document.getElementById(form.id +':txtAgencia')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '!\n';
					
		}	
		if(allTrim(document.getElementById(form.id +':txtConta')) == "") {
				msgValidacao = msgValidacao +  msgcampo + ' ' + msgconta + ' ' + msgnecessario + '!\n';					
		
		}
		
	} 
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;						
}


function validaCamposContrato(form,msgcampo, msgnecessario, cliente, msgempresa, msgtipo, msg,msgConsultarGerente){
							  
	var msgValidacao = '';


	if (document.getElementById(form.id + ':empresaGestora').value == 0) {
		msgValidacao =msgValidacao + msgcampo + ' ' + msgempresa + ' ' + '!\n';
	} 
	
	if (document.getElementById(form.id + ':tipoContrato').value == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + '!\n';

	} 
		

	if ((cliente == null) || (cliente ==  '')){
	
		
	if  ( (allTrim(document.getElementById(form.id + ':numero')) == '') && (allTrim(document.getElementById(form.id + ':agenciaOperadora'))  == 0) && (allTrim(document.getElementById(form.id + ':gerenteNome')) == 0)
			&& (document.getElementById(form.id + ':situacao').value == 0)) {

			msgValidacao = msgValidacao + ' ' + msg + ' ' + '!\n';			


		}
	}
	


	if ((allTrim(document.getElementById(form.id +  ':gerenteId')) != '') && (allTrim(document.getElementById(form.id +  ':gerenteNome')) == '' ) ){
				msgValidacao = msgValidacao +  msgConsultarGerente + '!\n';

	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;
	
}

function validaCamposContratoCPFCNPJBloq(form,msgcampo, msgnecessario, cliente, msgempresa, msgtipo, msg,msgConsultarGerente){

	var msgValidacao = '';


	if (document.getElementById(form.id + ':empresaGestora').value == 0) {
		msgValidacao =msgValidacao +   msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '!\n';
	} 
	
	if (document.getElementById(form.id + ':tipoContrato').value == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '!\n';

	} 
		
	if  ( (allTrim(document.getElementById(form.id + ':numero')) == '') && (allTrim(document.getElementById(form.id + ':agenciaOperadora'))  == 0) && (allTrim(document.getElementById(form.id + ':gerenteNome')) == 0)
			&& (document.getElementById(form.id + ':situacao').value == 0)) {

			msgValidacao = msgValidacao + msgcampo + ' ' + msg + ' ' + msgnecessario + '!\n';			


	}
	
	


	if ((allTrim(document.getElementById(form.id +  ':gerenteId')) != '') && (allTrim(document.getElementById(form.id +  ':gerenteNome')) == '' ) ){
				msgValidacao = msgValidacao +  msgConsultarGerente + '!\n';

	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;
	
}

function validaCamposContratoSemSituacao(form,msgcampo, msgnecessario, cliente, msgempresa, msgtipo, msg,msgConsultarGerente){

	var msgValidacao = '';


	if (document.getElementById(form.id + ':empresaGestora').value == 0) {
		msgValidacao =msgValidacao +   msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '!\n';
	} 
	
	if (document.getElementById(form.id + ':tipoContrato').value == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '!\n';

	} 
		

	//if (document.getElementById(form.id +  ':empresaGestora').value != 0 && document.getElementById(form.id + ':tipoContrato').value != 0 && document.getElementById(form.id + ':numero').value == '') {		

		if ((cliente == null) || (cliente ==  '')){
		
	
			if ((allTrim(document.getElementById(form.id + ':numero')) == '') && (allTrim(document.getElementById(form.id + ':agenciaOperadora')) == 0) && (document.getElementById(form.id + ':gerenteNome').value == 0)
				) {
	
				msgValidacao = msgValidacao + msg + '!\n';


			}
		}
	
	//}

	if ((allTrim(document.getElementById(form.id +  ':gerenteId'))  != '') && (allTrim(document.getElementById(form.id +  ':gerenteNome')) == '' ) ){
				msgValidacao = msgValidacao +  msgConsultarGerente + '!\n';

	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;
	
}

function validaCamposContratoSemSituacaoSemGerente(form,msgcampo, msgnecessario, cliente, msgempresa, msgtipo, msg){

	var msgValidacao = '';

	//validando campo "Empresa Gestora do Contrato"
	if (document.getElementById(form.id + ':empresaGestora').value == 0) {
		msgValidacao =msgValidacao +   msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '!\n';
	} 
	
	//validando campo "Tipo do Contrato"
	if (document.getElementById(form.id + ':tipoContrato').value == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '!\n';

	} 
		

	if ((cliente == null) || (cliente ==  '')){
	

		if ((allTrim(document.getElementById(form.id + ':numero')) == '')){
			msgValidacao = msgValidacao + msgcampo + ' ' + msg + ' ' + msgnecessario + '!\n';
		}
	}
	
	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		desbloquearTela();
		return false;
	}

	return true;
	
}



function limparDesabilitarCampos(form){


	//	if ((document.getElementById(form.id + ':empresaGestora').value != 0) && (document.getElementById(form.id + ':tipoContrato').value !=0)) {

			document.getElementById(form.id + ':agenciaOperadora').value ='';
			document.getElementById(form.id + ':gerenteId').value ='';
			document.getElementById(form.id + ':gerenteNome').value ='';
			document.getElementById(form.id + ':situacao').value = 0 ;
			document.getElementById(form.id + ':agenciaOperadora').readOnly =true;
			document.getElementById(form.id + ':gerenteId').readOnly =true;
			document.getElementById(form.id + ':situacao').disabled = true ;
			document.getElementById(form.id + ':habilitaCampos').value = true ;

	/*	}else{
			document.getElementById(form.id + ':agenciaOperadora').readOnly =false;
			document.getElementById(form.id + ':gerenteId').readOnly =false;
			document.getElementById(form.id + ':situacao').disabled = false ;
			document.getElementById(form.id + ':habilitaCampos').value = false ;
			
		
		}*/

}


function limparNomeGerente(form){
			document.getElementById(form.id + ':gerenteNome').value ='';
}

function validarProxCampoIdentClienteContrato(form){
	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCnpjFilial = document.getElementById(form.id + ':txtFilial');
	var campoCnpjControle = document.getElementById(form.id + ':txtControle');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoCpfControle = document.getElementById(form.id + ':txtControleCpf');
	var campoNomeRazao = document.getElementById(form.id + ':txtNomeRazaoSocial');
	var campoBanco = document.getElementById(form.id + ':txtBanco');
	var campoAgencia = document.getElementById(form.id + ':txtAgencia');
	var campoConta = document.getElementById(form.id + ':txtConta');
	
	if(objRdoCliente[0].checked){
		campoCnpj.disabled = false;
		campoCnpj.focus();
		campoCnpjFilial.disabled = false;
		campoCnpjControle.disabled = false;
		
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCpf.value = '';
		campoCpfControle.value = '';
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		return true;
	}else if(objRdoCliente[1].checked){
		campoCpf.disabled = false;
		campoCpf.focus();
		campoCpfControle.disabled = false;
	
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		return true;
	}else if(objRdoCliente[2].checked){
		campoNomeRazao.disabled = false;
		campoNomeRazao.focus();
			
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		return true;
	}else if(objRdoCliente[3].checked){
		campoBanco.disabled = false;
		campoAgencia.disabled = false;
		campoConta.disabled = false;
		campoBanco.value = 237;
		campoAgencia.focus();
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		return true;
	}else{
		document.getElementById(form.id + ':btoConsultarCliente').disabled = true;
		return false;
	}
}

function validarProxCampoIdentClienteContratoFlag(form, flagPesquisa){
	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCnpjFilial = document.getElementById(form.id + ':txtFilial');
	var campoCnpjControle = document.getElementById(form.id + ':txtControle');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoCpfControle = document.getElementById(form.id + ':txtControleCpf');
	var campoNomeRazao = document.getElementById(form.id + ':txtNomeRazaoSocial');
	var campoBanco = document.getElementById(form.id + ':txtBanco');
	var campoAgencia = document.getElementById(form.id + ':txtAgencia');
	var campoConta = document.getElementById(form.id + ':txtConta');
	
	if(objRdoCliente[0].checked){
		campoCnpj.disabled = false;
		campoCnpj.focus();
		campoCnpjFilial.disabled = false;
		campoCnpjControle.disabled = false;
		
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCpf.value = '';
		campoCpfControle.value = '';
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[1].checked){
		campoCpf.disabled = false;
		campoCpf.focus();
		campoCpfControle.disabled = false;
	
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[2].checked){
		campoNomeRazao.disabled = false;
		campoNomeRazao.focus();
			
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[3].checked){
		campoBanco.disabled = false;
		campoAgencia.disabled = false;
		campoConta.disabled = false;
		campoBanco.value = 237;
		campoAgencia.focus();
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else{
		document.getElementById(form.id + ':btoConsultarCliente').disabled = true;
		return false;
	}
}


function validarProxCamp(form, flagPesquisa){
	var objRdoCliente = document.getElementsByName('filtroRadio');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCnpjFilial = document.getElementById(form.id + ':txtFilial');
	var campoCnpjControle = document.getElementById(form.id + ':txtControle');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoCpfControle = document.getElementById(form.id + ':txtControleCpf');
	var campoBanco = document.getElementById(form.id + ':txtBanco');
	var campoAgencia = document.getElementById(form.id + ':txtAgencia');
	var campoConta = document.getElementById(form.id + ':txtConta');
	var campoDigito = document.getElementById(form.id + ':digito');
	var campoFinalidade = document.getElementById(form.id + ':finalidade');
	
	if(objRdoCliente[0].checked){
		campoCnpj.disabled = false;
		campoCnpj.focus();
		campoCnpjFilial.disabled = false;
		campoCnpjControle.disabled = false;
		
		
		
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;

		
		return true;
	}else if(objRdoCliente[1].checked){
		campoCpf.disabled = false;
		campoCpf.focus();
		campoCpfControle.disabled = false;
	
		

		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		campoDigito.value = '';
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
	
		return true;
	}else if(objRdoCliente[2].checked){
		campoBanco.disabled = false;
		campoAgencia.disabled = false;
		campoConta.disabled = false;
		campoDigito.disabled = false;
		campoBanco.value = 237;
		campoAgencia.focus();
		

		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btnConsultar').disabled = false;
		
	
		return true;
		
	}else{
		return false;
	}
}

function focoGerenteResponsavel(form) {
	setTimeout(function() {document.getElementById(form.id + ':gerenteId').focus();});
}

function gravarNomeCampoParaFoco(nomeCampo) {
	document.getElementById('resumoContratoForm:nomeCampoFoco').value = nomeCampo; 
}
function posicionarFocoTabela() {
	var nomeCampo = document.getElementById('resumoContratoForm:nomeCampoFoco').value;
	
	if (nomeCampo != null) {
		document.getElementById('resumoContratoForm:' + nomeCampo).focus();
	}
}
