function checaCampoObrigatorio(msgcampo, msgnecessario, msgdata, msgmotivo){
	    
	if ( document.getElementById('registrarAssinaturaFormalizacaoAltContrato:calendarioDataAssinatura.day').value == null ||
		 document.getElementById('registrarAssinaturaFormalizacaoAltContrato:calendarioDataAssinatura.day').value == ''   ||
		 document.getElementById('registrarAssinaturaFormalizacaoAltContrato:calendarioDataAssinatura.month').value == null ||
		 document.getElementById('registrarAssinaturaFormalizacaoAltContrato:calendarioDataAssinatura.month').value == ''   ||
		 document.getElementById('registrarAssinaturaFormalizacaoAltContrato:calendarioDataAssinatura.year').value == null ||
		 document.getElementById('registrarAssinaturaFormalizacaoAltContrato:calendarioDataAssinatura.year').value == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if (allTrim(document.getElementById('registrarAssinaturaFormalizacaoAltContrato:numContratoFisico')) == '') {
		alert(msgcampo + ' ' + msgmotivo + ' ' + msgnecessario);
		document.getElementById('registrarAssinaturaFormalizacaoAltContrato:numContratoFisico').focus();
		return false;		
	}	

}

function confirmar(form, msgConfirma){
	
		var hiddenConfirm;
		
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id + ':hiddenConfirma'){	
				hiddenConfirm = form.elements[i];
				break;
			}
		}
		hiddenConfirm.value=confirm(msgConfirma);
}
