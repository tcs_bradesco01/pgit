function selecionarTodasContas(form, check){
	for(i=0; i<form.elements.length; i++){
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
	}	
	
	document.getElementById(form.id + ':btnSelecionar').disabled = !check.checked;
}


function selecionarTodos(form, check){
	for(i=0; i<form.elements.length; i++) {
		if (form.elements[i].id.indexOf('dataTable_') != -1 ){		
			form.elements[i].checked = check.checked;
		}	
	}	
	
	document.getElementById(form.id + ':btnAvancar').disabled = !check.checked;
}


function checarObrigatoriedade(form, msgCampo, msgNecessario, msgEmpresaGestoraContrato, msgTipoContrato, msgCdAgenciaGestora, msgDsAgenciaGestora,
									  msgCdGerenteResponsavel, msgDsGerenteResponsavel, msgIdioma, msgMoeda, msgSetor, msgIdentificacaoContrato, msgCnpj,
									  msgCpf, msgNomeRazaoSocial, msgBanco, msgAgencia, msgConta){

	var msgValidacao;
	msgValidacao = '';
	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	
	if (document.getElementById(form.id +':empresaGestoraContrato').value == "") {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgEmpresaGestoraContrato + ' ' + msgNecessario + '!\n';
	} 
			
	if (document.getElementById(form.id +':tipoContrato').value == "") {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgTipoContrato + ' ' + msgNecessario + '!\n';
	} 		
	
	if (allTrim(document.getElementById(form.id +':txtAgenciaGestora')) == "") {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgCdAgenciaGestora + ' ' + msgNecessario + '!\n';
	} 
		
	if (allTrim(document.getElementById(form.id +':txtDsAgenciaGestora')) == "") {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgDsAgenciaGestora + ' ' + msgNecessario + '!\n';
	} 	
	
	if (allTrim(document.getElementById(form.id +':gerenteId')) == "") {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgCdGerenteResponsavel + ' ' + msgNecessario + '!\n';
	} 	
	
	if (allTrim(document.getElementById(form.id +':gerenteNome')) == "") {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgDsGerenteResponsavel + ' ' + msgNecessario + '!\n';
	} 
		
	if (document.getElementById(form.id +':idioma').value == 0) {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgIdioma + ' ' + msgNecessario + '!\n';
	} 	
		
	if (document.getElementById(form.id +':moeda').value == 0) {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgMoeda + ' ' + msgNecessario + '!\n';
	} 	
		
	if (document.getElementById(form.id +':setor').value == 0) {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgSetor + ' ' + msgNecessario + '!\n';
	} 	
		
	if (allTrim(document.getElementById(form.id +':txtIdentificacaoContrato')) == '') {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgIdentificacaoContrato + ' ' + msgNecessario + '!\n';
	} 	
		
	if (objRdoCliente[0].checked) {
			if (allTrim(document.getElementById(form.id + ':txtCnpj')) == "") {
				msgValidacao = msgValidacao + msgCampo + ' ' + msgCnpj + ' ' + msgNecessario + '!\n';
			
			}else{
				if (allTrim(document.getElementById(form.id +':txtFilial')) == "") {
					msgValidacao = msgValidacao +  msgCampo + ' ' + msgCnpj + ' ' + msgNecessario + '!\n';					
				}else {
					if (document.getElementById(form.id + ':txtControle').value == ""){
						msgValidacao = msgValidacao +   msgCampo + ' ' + msgCnpj + ' ' + msgNecessario + '!\n';				
					}
				}
			}
		}else if (objRdoCliente[1].checked) {
			if (allTrim(document.getElementById(form.id +':txtCpf')) == "") {
				msgValidacao = msgValidacao +   msgCampo + ' ' + msgCpf + ' ' + msgNecessario + '!\n';
			
			} else {
				if(allTrim(document.getElementById(form.id +':txtControleCpf')) == "") {
					msgValidacao = msgValidacao +  msgCampo + ' ' + msgCpf + ' ' + msgNecessario + '!\n';
				}
			}
		}else if (objRdoCliente[2].checked) {
			if (allTrim(document.getElementById(form.id +':txtNomeRazaoSocial')) == '') {
				msgValidacao = msgValidacao + msgCampo + ' ' + msgNomeRazaoSocial + ' ' + msgNecessario + '!\n';
			} 
			
		}else if (objRdoCliente[3].checked) {
			if (allTrim(document.getElementById(form.id +':txtBanco')) == "") {
				msgValidacao = msgValidacao + msgCampo + ' ' + msgBanco + ' ' + msgNecessario + '!\n';
			} 
			if (allTrim(document.getElementById(form.id +':txtAgencia')) == "") {
				msgValidacao = msgValidacao + msgCampo + ' ' + msgAgencia + ' ' + msgNecessario + '!\n';
			} 
			if (allTrim(document.getElementById(form.id +':txtConta')) == "") {
				msgValidacao = msgValidacao + msgCampo + ' ' + msgConta + ' ' + msgNecessario + '!\n';
			} 
			
		}
		
		if (msgValidacao != ''){
			alert(msgValidacao);
			msgValidacao = '';
			return false;
		}else{
			return true;
		}
	
	
}

function validarCamposObrigatoriosConta(form, msgCampo, msgNecessario, msgBanco, msgAgencia, msgConta){
	var msgValidacao = '';
	
	if (allTrim(document.getElementById(form +':txtBanco')) == "") {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgBanco + ' ' + msgNecessario + '!\n';
	} 
	if (allTrim(document.getElementById(form +':txtAgencia')) == "") {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgAgencia + ' ' + msgNecessario + '!\n';
	} 
	if (allTrim(document.getElementById(form +':txtConta')) == "") {
		msgValidacao = msgValidacao + msgCampo + ' ' + msgConta + ' ' + msgNecessario + '!\n';
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		msgValidacao = '';
		return false;
	}else{
		return true;
	}
}

function limparAgenciaGestora(form){
			document.getElementById(form.id + ':txtDsAgenciaGestora').value ='';
}