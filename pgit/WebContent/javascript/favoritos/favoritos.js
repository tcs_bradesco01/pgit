function validaCpoConsultaCliente(form){
	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	var campo = '';

	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById(form.id + ':txtCnpj')) == "") {
			alert("O campo CNPJ esta incompleto ou n\u00E3o esta preenchido.");
			document.getElementById(form.id + ':txtCnpj').focus();
			return false;		
		} else {
			if (allTrim(document.getElementById(form.id + ':txtFilial')) == "") {
				alert("O campo CNPJ esta incompleto ou n\u00E3o esta preenchido.");
				document.getElementById(form.id + ':txtFilial').focus();
				return false;		
			} else {
				if (allTrim(document.getElementById(form.id + ':txtControle')) == ""){
					alert("O campo CNPJ esta incompleto ou n\u00E3o esta preenchido.");
					document.getElementById(form.id + ':txtControle').focus();
					return false;		
				}
			}
		}
	} else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById(form.id + ':txtCpf')) == "") {
			alert("O campo CPF esta incompleto ou n\u00E3o esta preenchido.");
			document.getElementById(form.id + ':txtCpf').focus();
			return false;		
		} else {
			if(allTrim(document.getElementById(form.id + ':txtControleCpf')) == "") {
				alert("O campo CPF esta incompleto ou n\u00E3o esta preenchido.");
				document.getElementById(form.id + ':txtControleCpf').focus();
				return false;	
			}
		}
	} else if (objRdoCliente[2].checked) {
		if (allTrim(document.getElementById(form.id + ':txtNomeRazaoSocial')) == "") {
			alert("O campo Nome/Razao Social n\u00E3o esta preenchido.");
			document.getElementById(form.id + ':txtNomeRazaoSocial').focus();
			return false;	
		}
	} else if (objRdoCliente[3].checked) {
		if (allTrim(document.getElementById(form.id + ':txtBanco')) == "") {
			campo = "O campo BANCO n\u00E3o esta preenchido.";
			if(allTrim(document.getElementById(form.id + ':txtAgencia')) == "") {
				campo += "\nO campo AGENCIA n\u00E3o esta preenchido."
				if(allTrim(document.getElementById(form.id + ':txtConta')) == "") {	
					campo += "\nO campo CONTA n\u00E3o esta preenchido.";
				}			
			}
			alert (campo);
			document.getElementById(form.id + ':txtBanco').focus();
			return false;		
		} else { 
			if(allTrim(document.getElementById(form.id + ':txtAgencia')) == "") {
				campo = "O campo AGENCIA n\u00E3o esta preenchido.";
				if(document.getElementById(form.id + ':txtConta').value == "") {	
					campo += "\nO campo CONTA n\u00E3o esta preenchido.";
				}	
				alert (campo);
				document.getElementById(form.id + ':txtAgencia').focus();
				return false;		
			} else {
				if(allTrim(document.getElementById(form.id + ':txtConta')) == "") {
					alert("O campo CONTA n\u00E3o esta preenchido.");
					document.getElementById(form.id + ':txtConta').focus();
					return false;		
				}
			}
		}
	} 

	return true;						
}



function validaCpoContrato(form){
	var campo = '';
	if (document.getElementById(form.id + ':selEmpresaGestoraContrada').value == 0) {
		campo = "Obrigatorio selecionar uma Empresa Gestora do Contrato.";
		if (document.getElementById(form.id + ':selTipoContrato').value == 0) {
			campo += "\nObrigatorio selecionar um Tipo de Contrato.";
		}	
		alert(campo);
		document.getElementById(form.id + ':selEmpresaGestoraContrada').focus();
		return false;
	}else if (document.getElementById(form.id + ':selTipoContrato').value == 0) {
			alert("Obrigatorio selecionar um Tipo de Contrato.");
			document.getElementById(form.id + ':selTipoContrato').focus();
			return false;		
	}
	return true;
}

function validaCpoContratoConsulta(form, msgcampo, msgnecessario,msgempresa,msgtipo, msgnumero, cliente){
	var msgValidacao = '';

	if ((cliente == null) || (cliente ==  '')){
		if (document.getElementById(form.id + ':selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById(form.id + ':selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
		
		if (allTrim(document.getElementById(form.id + ':txtNumeroContrato')) == "") {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgnumero + ' ' + msgnecessario + '\n';
		} 
	
	}else{
		if (document.getElementById(form.id + ':selEmpresaGestoraContrada').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgempresa + ' ' + msgnecessario + '\n';
		} 
		
		if (document.getElementById(form.id + ':selTipoContrato').value == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgtipo + ' ' + msgnecessario + '\n';
		}
	}	
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}

function validarCampoBloqueDesbloq() {

	var radio = document.getElementsByName('rdoDesBloq');
	
	if (document.getElementById('frmBloqDesbManterFavorecido:selMotivoBloq').value == 0 && radio[0].checked == true) {
		alert("O campo de bloqueio " + String.fromCharCode(233) + " de preenchimento obrigat" + String.fromCharCode(243) + "rio.");
		document.getElementById('frmBloqDesbManterFavorecido:selMotivoBloq').focus();
		return false;
	}
	
	return true;
}

function validarCampoBloqueDesbloqConta() {

	var radio = document.getElementsByName('rdoDesBloq');
	
	if (document.getElementById('frmBloqDesManterFavorecido:selMotivoBloq').value == 0 && radio[0].checked == true) {
		alert("O campo de bloqueio " + String.fromCharCode(233) + " de preenchimento obrigat" + String.fromCharCode(243) + "rio.");
		document.getElementById('frmBloqDesManterFavorecido:selMotivoBloq').focus();
		return false;
	}
	
	return true;
}

function validarCampoHistorico(form, msgcampo, msgnecessario, msgcodigofavorecido, msginscricaofavorecido, msgtipofavorecido){
	var msgValidacao = '';
	var radio = document.getElementsByName('rdoFiltroHistorico');
	if(radio[0].checked){
		if (allTrim(document.getElementById(form.id + ':codigoFavorecido')) == '') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgcodigofavorecido + ' ' + msgnecessario + '\n';
		}	
	}
	else{
		if(radio[1].checked){
			if (allTrim(document.getElementById(form.id + ':inscricaoFavorecido')) == '') {
				msgValidacao = msgValidacao + msgcampo + ' ' + msginscricaofavorecido + ' ' + msgnecessario + '\n';
			}
			if (document.getElementById(form.id + ':tipoFavorecido').value == 0) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgtipofavorecido + ' ' + msgnecessario + '\n';
			} 			
		}
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;
}

function validarCampoHistoricoConta(form, msgcampo, msgnecessario, msgbanco, msgagencia, msgconta, msgtipoconta){
	var msgValidacao = '';

	if (allTrim(document.getElementById(form.id + ':txtBanco')) == '' ||  parseInt(allTrim(document.getElementById(form.id +':txtBanco')),10) == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '\n';
	}
	if (allTrim(document.getElementById(form.id + ':txtAgencia')) == '' ||  parseInt(allTrim(document.getElementById(form.id +':txtAgencia')),10) == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n';
	}	
	if (allTrim(document.getElementById(form.id + ':txtConta')) == '' ||  parseInt(allTrim(document.getElementById(form.id +':txtConta')),10) == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgnecessario + '\n';
	}
	if (document.getElementById(form.id + ':tipoConta').value == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgtipoconta + ' ' + msgnecessario + '\n';
	} 			
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;
}

function validarFiltroConsContaSalarioDest() {
	var campo = '';
	var objRdoCliente = document.getElementsByName('rdoFiltroPesquisa');


	if (objRdoCliente[0].checked) {
		if (allTrim(document.getElementById('frmCadCtaSalDestForm:txtBancoP')) == "") {
			campo = "O campo Banco n\u00E3o esta preenchido.";
			if(allTrim(document.getElementById('frmCadCtaSalDestForm:txtAgenciaP')) == "") {
				campo += "\nO campo Agencia n\u00E3o esta preenchido.";
				if(allTrim(document.getElementById('frmCadCtaSalDestForm:txtContaP')) == "") {	
					campo += "\nO campo Conta n\u00E3o esta preenchido.";
				}			
			}else if(allTrim(document.getElementById('frmCadCtaSalDestForm:txtContaP')) == "") {	
				campo += "\nO campo Conta n\u00E3o esta preenchido.";
			}
			alert (campo);
			document.getElementById('frmCadCtaSalDestForm:txtBancoP').focus();
			return false;		
		}else if(allTrim(document.getElementById('frmCadCtaSalDestForm:txtAgenciaP')) == "") {
			campo = "O campo Agencia n\u00E3o esta preenchido.";
			if(allTrim(document.getElementById('frmCadCtaSalDestForm:txtContaP')) == "") {	
				campo += "\nO campo Conta n\u00E3o esta preenchido.";
			}	
			alert (campo);
			document.getElementById('frmCadCtaSalDestForm:txtAgenciaP').focus();
			return false;		
		}else if (allTrim(document.getElementById('frmCadCtaSalDestForm:txtContaP')) == "") {
			alert ("O campo Conta n\u00E3o esta preenchido.");
			document.getElementById('frmCadCtaSalDestForm:txtContaP').focus();
			return false;		
		}
	}
	else if (objRdoCliente[1].checked) {
		if (allTrim(document.getElementById('frmCadCtaSalDestForm:txtCpfP')) == "") {
			campo = "O campo CPF n\u00E3o esta preenchido.";
			if (allTrim(document.getElementById('frmCadCtaSalDestForm:txtControleCpfP')) == "") {
				campo += "O campo Digito Controle n\u00E3o esta preenchido.";
			}
			alert (campo);
			document.getElementById('frmCadCtaSalDestForm:txtCpfP').focus();
			return false;
		} else if (allTrim(document.getElementById('frmCadCtaSalDestForm:txtControleCpfP')) == "") {
			alert ("O campo Digito Controle n\u00E3o esta preenchido.");
			document.getElementById('frmCadCtaSalDestForm:txtControleCpfP').focus();
			return false;
		}
	}
	
	return true;	
}

function validarCampoHistoricoContaSalarioDestino(form, msgcampo, msgnecessario, msgbanco, msgagencia, msgconta, msgdigitoconta){
	var msgValidacao = '';

	if (allTrim(document.getElementById(form.id + ':txtBanco')) == '' ||  parseInt(allTrim(document.getElementById(form.id +':txtBanco')),10) == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '\n';
	}
	if (allTrim(document.getElementById(form.id + ':txtAgencia')) == '' ||  parseInt(allTrim(document.getElementById(form.id +':txtAgencia')),10) == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n';
	}	
	if (allTrim(document.getElementById(form.id + ':txtConta')) == '' ||  parseInt(allTrim(document.getElementById(form.id +':txtConta')),10) == 0) {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgnecessario + '\n';
	}
	if (allTrim(document.getElementById(form.id + ':txtDigitoConta')) == '') {
		msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoconta + ' ' + msgnecessario + '\n';
	}		
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;
}

function validarCampoHistoricoVinculoEmpresaPagadoraContaSalario(form, msgcampo, msgnecessario, msgbanco, msgagencia, msgconta, msgdigitoconta){
	var msgValidacao = '';

	if(document.getElementById(form.id + ':chkDadosContaSalarioHistorico').checked){
		if (allTrim(document.getElementById(form.id + ':txtBanco')) == '' ||  parseInt(allTrim(document.getElementById(form.id +':txtBanco')),10) == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '\n';
		}
		if (allTrim(document.getElementById(form.id + ':txtAgencia')) == '' ||  parseInt(allTrim(document.getElementById(form.id +':txtAgencia')),10) == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '\n';
		}	
		if (allTrim(document.getElementById(form.id + ':txtConta')) == '' ||  parseInt(allTrim(document.getElementById(form.id +':txtConta')),10) == 0) {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgnecessario + '\n';
		}
		if (allTrim(document.getElementById(form.id + ':txtDigitoConta')) == '') {
			msgValidacao = msgValidacao + msgcampo + ' ' + msgdigitoconta + ' ' + msgnecessario + '\n';
		}	
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	
	return true;
}

function validaCpoConsultaArgumentos(form){ 

	var objRdoArgumentoPesquisa = document.getElementsByName('rdoArgumentoPesquisa');
	var campo = '';
	
	//if (objRdoArgumentoPesquisa[0].checked) {
	//	if (document.getElementById(form.id + ':txtNomeFavorecido').value == "") {
	//		alert("O campo Nome do Favorecido n\u00E3o esta preenchido.");
	//		document.getElementById(form.id + ':txtNomeFavorecido').focus();
	//		return false;		
	//	} 
	// } 
	 
	if (objRdoArgumentoPesquisa[0].checked) {
	if (allTrim(document.getElementById(form.id + ':txtFavorecido')) == "") {
			alert("O campo Codigo do Favorecido n\u00E3o esta preenchido.");
			document.getElementById(form.id + ':txtFavorecido').focus();
			return false;		
		} 
	}	
	
		
	if (objRdoArgumentoPesquisa[1].checked) {
	
		if(document.getElementById(form.id +':selTipoInscricao').value == '1'){
			if (allTrim(document.getElementById(form.id + ':txtInscricaoCpf')) == "") {
				alert("O campo Inscricao n\u00E3o esta preenchido.");
				document.getElementById(form.id + ':txtInscricaoCpf').focus();
				return false;		
			} 
		}
		
		if(document.getElementById(form.id +':selTipoInscricao').value == '2'){
			if (allTrim(document.getElementById(form.id + ':txtInscricaoCnpj')) == "") {
				alert("O campo Inscricao n\u00E3o esta preenchido.");
				document.getElementById(form.id + ':txtInscricaoCnpj').focus();
				return false;		
			} 
		}
		
		if(document.getElementById(form.id +':selTipoInscricao').value == '3'){
			if (allTrim(document.getElementById(form.id + ':txtInscricaoPis')) == "") {
				alert("O campo Inscricao n\u00E3o esta preenchido.");
				document.getElementById(form.id + ':txtInscricaoPis').focus();
				return false;		
			} 
		}
		
		if(document.getElementById(form.id +':selTipoInscricao').value == '9'){
			if (allTrim(document.getElementById(form.id + ':txtInscricaoOutros')) == "") {
				alert("O campo Inscricao n\u00E3o esta preenchido.");
				document.getElementById(form.id + ':txtInscricaoOutros').focus();
				return false;		
			} 
		}
	}
	return true;						
}

function focoAgencias(form){
	var objRdoAgencia = document.getElementsByName('rdoFiltroCliente');
	 
	if (objRdoAgencia[0].checked == true) {
		document.getElementById(form.id + ':txtCnpj').focus();
	}	
	if (objRdoAgencia[1].checked == true) {
		document.getElementById(form.id + ':txtCpf').focus();
	}	
	if (objRdoAgencia[2].checked == true) {
		document.getElementById(form.id + ':txtNomeRazaoSocial').focus();
	}	
	if (objRdoAgencia[3].checked == true) {
		document.getElementById(form.id + ':txtAgencia').focus();
	}	
	return true;						
}

function proximoCampo(quatindade,form,campoAtual,proximoCampo){

	var sCampoAtual = document.forms [form].elements [campoAtual].value;
	
	if(sCampoAtual.length == quatindade){
		document.forms [form].elements [proximoCampo].focus(); 
	}
	
}

function validaFiltrosPesquisa(form, msgcampo, msgnecessario, msgbanco, msgagencia, msgconta, msgdigito, msgDiferenteZeros){

		var msgValidacao = '';

		if (allTrim(document.getElementById(form.id +':txtAgenciaP')) != '') {
			if (allTrim(document.getElementById(form.id +':txtAgenciaP')) == 0 ) {
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgDiferenteZeros + '!' + '\n';
			} else {
				if (allTrim(document.getElementById(form.id +':txtBancoP')) == ''){
					msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '!' + '\n';
				}
				if (allTrim(document.getElementById(form.id +':txtContaP')) == ''){
					msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgnecessario + '!' + '\n';
				} else {
					if (parseInt(allTrim(document.getElementById(form.id +':txtContaP')),10) == 0 )
						msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgDiferenteZeros + '!' + '\n';
				}
				if(allTrim(document.getElementById(form.id +':txtDigitoP')) == ''){
					msgValidacao = msgValidacao + msgcampo + ' ' + msgdigito + ' ' + msgnecessario + '!' + '\n';				
				}	
			}
		}
		
		if (allTrim(document.getElementById(form.id +':txtContaP')) != '') {
			if (allTrim(document.getElementById(form.id +':txtBancoP')) == ''){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgbanco + ' ' + msgnecessario + '!' + '\n';
			}
			if (allTrim(document.getElementById(form.id +':txtAgenciaP')) == ''){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '!' + '\n';
			} else {
				if (allTrim(document.getElementById(form.id +':txtAgenciaP')) == 0 )
					msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgDiferenteZeros + '!' + '\n';
			}
			if(allTrim(document.getElementById(form.id +':txtDigitoP')) == ''){
				msgValidacao = msgValidacao + msgcampo + ' ' + msgdigito + ' ' + msgnecessario + '!' + '\n';				
			}	
		}

		if (msgValidacao != ''){
			alert(msgValidacao);
			return false;
		}

		return true;	
}

function validarProxCampoIdentClienteContratoFlag(form, flagPesquisa){
	var objRdoCliente = document.getElementsByName('rdoFiltroCliente');
	var campoCnpj = document.getElementById(form.id + ':txtCnpj');
	var campoCnpjFilial = document.getElementById(form.id + ':txtFilial');
	var campoCnpjControle = document.getElementById(form.id + ':txtControle');
	var campoCpf = document.getElementById(form.id + ':txtCpf');
	var campoCpfControle = document.getElementById(form.id + ':txtControleCpf');
	var campoNomeRazao = document.getElementById(form.id + ':txtNomeRazaoSocial');
	var campoBanco = document.getElementById(form.id + ':txtBanco');
	var campoAgencia = document.getElementById(form.id + ':txtAgencia');
	var campoConta = document.getElementById(form.id + ':txtConta');
	
	if(objRdoCliente[0].checked){
		campoCnpj.disabled = false;
		campoCnpj.focus();
		campoCnpjFilial.disabled = false;
		campoCnpjControle.disabled = false;
		
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCpf.value = '';
		campoCpfControle.value = '';
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[1].checked){
		campoCpf.disabled = false;
		campoCpf.focus();
		campoCpfControle.disabled = false;
	
		
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[2].checked){
		campoNomeRazao.disabled = false;
		campoNomeRazao.focus();
			
		campoBanco.disabled = true;
		campoAgencia.disabled = true;
		campoConta.disabled = true;
		campoBanco.value = '';
		campoAgencia.value = '';
		campoConta.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else if(objRdoCliente[3].checked){
		campoBanco.disabled = false;
		campoAgencia.disabled = false;
		campoConta.disabled = false;
		campoBanco.value = 237;
		campoAgencia.focus();
		
		campoNomeRazao.disabled = true;
		campoNomeRazao.value = '';
		
		campoCnpj.disabled = true;
		campoCnpjFilial.disabled = true;
		campoCnpjControle.disabled = true;
		campoCnpj.value = '';
		campoCnpjFilial.value = '';
		campoCnpjControle.value = '';
		
		campoCpf.disabled = true;
		campoCpfControle.disabled = true;
		campoCpf.value = '';
		campoCpfControle.value = '';
		
		document.getElementById(form.id + ':btoConsultarCliente').disabled = false;
		
		if(flagPesquisa == 'true'){
			campoBanco.disabled = true;
			campoAgencia.disabled = true;
			campoConta.disabled = true;
			campoNomeRazao.disabled = true;
			campoCpf.disabled = true;
			campoCpfControle.disabled = true;
			campoCnpj.disabled = true;
			campoCnpjFilial.disabled = true;
			campoCnpjControle.disabled = true;
		}
		
		return true;
	}else{
		document.getElementById(form.id + ':btoConsultarCliente').disabled = true;
		return false;
	}
}