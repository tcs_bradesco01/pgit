function valida(msgsubramo, msgclasse,msgdescricaoag){
	
	var conglomerado = document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:selOperacao').value;
	
	var diretoria = document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:selDiretoria').value;
	
	var gerencia = document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:selGerencia').value;
	
	var segmento = document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:selSegmento').value;
	
	
	
	if(conglomerado == 0){
		alert('Empresa do Conglomerado \u00E9 necess\u00E1rio!');
		return false;		
	}
	
	if(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:chkDiretoriaReg').checked == true && diretoria == 0){
		alert('Selecione a Diretoria!');
		return false; 
	}
	
	if(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:chkGerenciaReg').checked == true && gerencia == 0){
		alert('Selecione a Ger\u00EAncia!');
		return false;
	}else if (document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:chkGerenciaReg').checked == true && diretoria == 0 ){
			alert('Selecione a Diretoria!');
			return false;
	}	
	
	if(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:chkAgOperadora').checked == true && allTrim(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:txtAgencia')) == ""){
		alert('Preencha o campo Ag\u00EAncia Operadora!');
		return false; 		
	} 
		
//	if(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:chkAgOperadora').checked == true && document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:hiddenAgencia').value == "" || document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:hiddenAgencia').value == null){
//		alert('Preencha o campo ' + msgdescricaoag + '!');
//		return false;
//	}else 
	
	if (document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:chkAgOperadora').checked == true && gerencia == 0 ){
			alert('Selecione a Diretoria e a Ger\u00EAncia!');
			return false;
	}
	
	if(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:chkSegmento').checked == true && segmento == 0){
		alert('Selecione o Segmento!');
		return false;	
	} 
	
	if(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:chkPartGrupoEco').checked == true && allTrim(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:txtPartGrupoEco')) == ""){
		alert('Preencha o campo Participante do Grupo Economico!');
		return false;	
	}
	
	if(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:chkAtividadeEconomica').checked == true && document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:cboClasseRamo').value == 0 || document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:cboClasseRamo').value == null){
		alert('Selecione a ' + msgclasse + '!');
		return false;	
	}
	 
	if(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:chkAtividadeEconomica').checked == true && document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:cboSubRamoAtividade').value == 0 || document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:cboSubRamoAtividade').value == null){
		alert('Preencha o campo ' + msgsubramo + '!');
		return false;	
	}	


}

function validaAgenciaOperadora(){

	if(allTrim(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:txtAgencia')) == ""){
		alert('Preencha o campo Ag\u00EAncia Operadora!');
		return false; 		
	}
	
	return true;

}

function validaGrupoEconomico(){ 

	if(document.getElementById('frmIncluirSolicitacaoRelatorioFavorecido:txtPartGrupoEco').value == ""){
		alert('Preencha o campo Grupo Economico!');
		return false;
	}	
	
	return true;
}