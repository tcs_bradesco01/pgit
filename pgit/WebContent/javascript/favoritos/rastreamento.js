function validarFiltroEstatistica(msgcampo, msgnecessario) {
	var campos = '';
	var objFocus = new Array();
	var descontoTarifa = 0.00;
	
	if (!validarCampoData('frmIncSolicRastFavorecido:txtPeriodoInicio')) {
		campos = campos + msgcampo + ' ' + "Data Inicio" + ' ' + msgnecessario + '\n';
		objFocus.push(document.getElementById('frmIncSolicRastFavorecido:txtPeriodoInicio.day'));
	}
	if (!validarCampoData('frmIncSolicRastFavorecido:txtPeriodoFim')) {
		campos = campos + msgcampo + ' ' + "Data Fim" + ' ' + msgnecessario + '\n';
		objFocus.push(document.getElementById('frmIncSolicRastFavorecido:txtPeriodoFim.day'));
	}
	
	if(document.getElementById('frmIncSolicRastFavorecido:txtPeriodoInicio.year').value > document.getElementById('frmIncSolicRastFavorecido:txtPeriodoFim.year').value){
		campos = campos +  "Periodo de Rastreamento Inicial nao pode ser maior que Periodo Final" + '!' + '\n';	
		desbloquearTela();
	}else{
		if(document.getElementById('frmIncSolicRastFavorecido:txtPeriodoInicio.year').value == document.getElementById('frmIncSolicRastFavorecido:txtPeriodoFim.year').value){
			if(document.getElementById('frmIncSolicRastFavorecido:txtPeriodoInicio.month').value > document.getElementById('frmIncSolicRastFavorecido:txtPeriodoFim.month').value){
				campos = campos +  "Periodo de Rastreamento Inicial nao pode ser maior que Periodo Final" + '!' + '\n';	
				desbloquearTela();
			}else{
				if(document.getElementById('frmIncSolicRastFavorecido:txtPeriodoInicio.month').value == document.getElementById('frmIncSolicRastFavorecido:txtPeriodoFim.month').value){
					if(document.getElementById('frmIncSolicRastFavorecido:txtPeriodoInicio.day').value > document.getElementById('frmIncSolicRastFavorecido:txtPeriodoFim.day').value){
						campos = campos +  "Periodo de Rastreamento Inicial nao pode ser maior que Periodo Final" + '!' + '\n';	
						desbloquearTela();
					}
				}
			}
		}
	}
	
	var objCentroCusto = document.getElementById('frmIncSolicRastFavorecido:selTipoServico');
	if (objCentroCusto.value == 0) {
		campos = campos + msgcampo + ' ' + "Tipo Servi\u00E7o" + ' ' + msgnecessario + '\n';
		objFocus.push(objCentroCusto);
	}
	var objModalidade = document.getElementById('frmIncSolicRastFavorecido:selModalidadeServico');
	
	
	
	if (campos != '') {
		alert(campos);
		if (objFocus != null && objFocus.length > 0) {
			objFocus[0].focus();
			desbloquearTela();
		}
		return false;
	}
	desbloquearTela();
	return valida();
}

function valida(){
	var banco = document.getElementById('frmIncSolicRastFavorecido:txtBanco').value;
	var agencia = document.getElementById('frmIncSolicRastFavorecido:txtAgencia').value;
	var conta = document.getElementById('frmIncSolicRastFavorecido:txtConta').value;
	var digito = document.getElementById('frmIncSolicRastFavorecido:txtDigito').value;
	
	if (banco == '' && (agencia != '' || conta != '')) {
		alert('Informe o argumento completo da conta de d\u00E9bito.');
		desbloquearTela();
		return false;
	}else if (agencia == '' && conta != '') {
		alert('Informe o argumento completo da conta de d\u00E9bito.');
		desbloquearTela();
		return false;
	}else if (conta == '' && (agencia != '')) {
		alert('Informe o argumento completo da conta de d\u00E9bito.');
		desbloquearTela();
		return false;
	}else if (digito != '' && (agencia == '' || conta == '')) {
		alert('Informe o argumento completo da conta de d\u00E9bito.');
		return false;
	}else if(document.getElementById('frmIncSolicRastFavorecido:chkDesconto').checked == true && document.getElementById('frmIncSolicRastFavorecido:txtDesconto').value == ""){
		alert('O campo Desconto tarifa \u00E9 de preenchimento obrigat\u00f3rio.');
		return false;
	}
					
	return true;
}

function validaDesconto() {
	var objDesconto = document.getElementById('frmIncSolicRastFavorecido:txtDesconto');
	if (!objDesconto.disabled) {
		var valorDesconto = parseFloat(objDesconto.value.replace(",", "."));

		if (valorDesconto < 0 || valorDesconto > 100) {
			alert("O desconto deve ser maior que 0,00 e menor ou igual a 100,00");
			objDesconto.value = "";
		}
	}
}

function validarCampoDescontoTarifa(object) {
	var percentual = object.value.replace(",",".");

	if(percentual > 100.00){
		//object.value = object.value.substring(0, object.value.length-1) 
		object.focus();
		alert("Desconto permitido na faixa de 0,00 a 100,00");
	}
}