function validaCamposInclusao(form, msgcampo, msgnecessario, msgagencia, msgrazao, msgconta, msgDiferenteZeros){

	var msgValidacao = '';

	if(allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == ''){
		msgValidacao = msgValidacao + msgcampo + ' ' + "Banco" + ' ' + msgnecessario + '!' + '\n';		
	}else{
		if (allTrim(document.getElementById(form.id +':txtBancoContaDebitoCheck')) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + "Banco" + ' ' + msgDiferenteZeros + '!' + '\n';
	}	
	if(allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == ''){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgnecessario + '!' + '\n';		
	}else{
		if (allTrim(document.getElementById(form.id +':txtAgenciaContaDebitoCheck')) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgagencia + ' ' + msgDiferenteZeros + '!' + '\n';
	}
	if((allTrim(document.getElementById(form.id +':txtGrupoContabilCheck')) == '') || (allTrim(document.getElementById(form.id +':txtSubGrupoContabilCheck')) == '')){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgrazao + ' ' + msgnecessario + '!' + '\n';		
	}else{
		if ((allTrim(document.getElementById(form.id +':txtGrupoContabilCheck')) == 0 ) || (allTrim(document.getElementById(form.id +':txtSubGrupoContabilCheck')) == 0 ))
			msgValidacao = msgValidacao + msgcampo + ' ' + msgrazao + ' ' + msgDiferenteZeros + '!' + '\n';
	}
	if((allTrim(document.getElementById(form.id +':txtContaDebitoCheck')) == '') || (allTrim(document.getElementById(form.id +':txtDigContaDebitoCheck')) == '')){
		msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgnecessario + '!' + '\n';
	}else{
		if (parseInt(allTrim(document.getElementById(form.id +':txtContaDebitoCheck')),10) == 0 )
			msgValidacao = msgValidacao + msgcampo + ' ' + msgconta + ' ' + msgDiferenteZeros + '!' + '\n';
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}

	return true;
}