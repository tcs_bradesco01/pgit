function iniciarPagina(){	
	document.getElementById('simulacaoTarifaNeg:botaoInicial').click();

}

function calculaPercentual(form,objeto,vlrPadrao){

	alert('entrou percentual');
	var aux = objeto.id.substring(31,objeto.id.length);

	aux = aux.substring(0,aux.indexOf(':'));
	
	var percentual = document.getElementById(form.id + ':' + aux + ':txtPercentualTarifaIndividual');
	
	
	vlrProposta = objeto.value;
	
	 while (vlrProposta.indexOf(".") > 0){
        vlrProposta = vlrProposta.replace(".","");	
    }
	vlrProposta = vlrProposta.replace(",", ".");
		
	vlrPercentual = (vlrProposta * 100)/vlrPadrao;
	
	vlrPercentual = vlrPercentual.toFixed(2);
	vlrPercentual = vlrPercentual+'';
    vlrPercentual = vlrPercentual.replace(".","");	
	vlrPercentual = vlrPercentual.replace(".",",");	
		 
	percentual.value = $.mask.string( vlrPercentual, {mask : '99,999.99', type : 'reverse'});;
	

}



function calculaValorProposta(form,objeto,vlrPadrao){

	var aux = objeto.id.substring(31,objeto.id.length);

	aux = aux.substring(0,aux.indexOf(':'));
	
	var txtVlrProposta = document.getElementById(form.id + ':' + aux + ':txtVlrTarifaIndProposta');
		
	vlrPercentual = objeto.value;
	
	while (vlrPercentual.indexOf(".") > 0){
        vlrPercentual = vlrPercentual.replace(".","");	
    }
	vlrPercentual = vlrPercentual.replace(",", ".");
		
	vlrProposta = (vlrPercentual * vlrPadrao)/100 ;
	

	vlrProposta = vlrProposta.toFixed(2);
	vlrProposta = vlrProposta+'';
    vlrProposta = vlrProposta.replace(".","");	
	vlrProposta = vlrProposta.replace(".",",");	
		 
	txtVlrProposta.value = $.mask.string( vlrProposta, {mask : '99,999.999.999.999.999', type : 'reverse'});;
	

}



function calculaValorMedio(form,objeto){

	alert('entrou');

	var aux = objeto.id.substring(31,objeto.id.length);

	aux = aux.substring(0,aux.indexOf(':'));
	
	var txtQtdeEstimada = document.getElementById(form.id + ':' + aux + ':txtQtdeEstimada');
	
	var txtVlrTarifaIndProposta = document.getElementById(form.id + ':' + aux + ':txtVlrTarifaIndProposta');
		 
	var txtVlrMedio  = document.getElementById(form.id + ':' + aux + ':txtVlrMedio');
	
//	var txtValorTotal  = document.getElementById(form.id + ':' + aux + ':txtValorTotal');
	
//	var hiddenVlrTotal  = document.getElementById(form.id + ':' + aux + ':hiddenVlrTotal');
	
	var hiddenVlrMedio  = document.getElementById(form.id + ':' + aux + ':hiddenVlrMedio');
	
	
	
	if (txtQtdeEstimada.value != null && txtQtdeEstimada.value != '' && txtVlrTarifaIndProposta.value != null && txtVlrTarifaIndProposta.value != 0){
	
		var vlrProposto = txtVlrTarifaIndProposta.value;
		
		
		while (vlrProposto.indexOf(".") > 0){
	        vlrProposto = vlrProposto.replace(".","");	
	    }
		vlrProposto = vlrProposto.replace(",", ".");
		
		
		var qtdeEstimada =  txtQtdeEstimada.value;
		
		while (qtdeEstimada.indexOf(".") > 0){
	        qtdeEstimada = vlrProposto.replace(".","");	
	    }
		qtdeEstimada = qtdeEstimada.replace(",", ".");
		

		var valorMedio = vlrProposto * qtdeEstimada;
		
//		var total = somaValoresMedios(form,aux);
		

//		total = total +valorMedio;
		
		
		valorMedio = valorMedio.toFixed(2);
		valorMedio = valorMedio+'';
	    valorMedio = valorMedio.replace(".","");	
		valorMedio = valorMedio.replace(".",",");
		
//		total = total.toFixed(2);
//		total = total+'';
//	    total = total.replace(".","");	
//		total = total.replace(".",",");
		
//		txtValorTotal.innerHTML = $.mask.string( total, {mask : '99,999.999.999.999.999', type : 'reverse', defaultValue: '000'}) ;
		
//		hiddenVlrTotal.value = $.mask.string( total, {mask : '99,999.999.999.999.999', type : 'reverse', defaultValue: '000'}) ;
		
		hiddenVlrMedio.value = $.mask.string( valorMedio, {mask : '99,999.999.999.999.999', type : 'reverse', defaultValue: '000'});;
		
		txtVlrMedio.innerHTML = $.mask.string( valorMedio, {mask : '99,999.999.999.999.999', type : 'reverse', defaultValue: '000'}) ;
		
	}else{

//		txtValorTotal.innerHTML = $.mask.string( '000', {mask : '99,999.999.999.999.999', type : 'reverse', defaultValue: '000'}) ;
//		hiddenVlrTotal.value = $.mask.string( '000', {mask : '99,999.999.999.999.999', type : 'reverse', defaultValue: '000'}) ;

		txtVlrMedio.innerHTML = $.mask.string( '000', {mask : '99,999.999.999.999.999', type : 'reverse', defaultValue: '000'}) ;
		hiddenVlrMedio.value = $.mask.string( '000', {mask : '99,999.999.999.999.999', type : 'reverse', defaultValue: '000'}) ;
	}
	
}




function somaValoresMedios(form,linha){

	var soma = 0.00;
	var aux;
    var txtVlrMedio;
    var valor = 0;
   	var hiddenTotal  = document.getElementById(form.id + ':hiddenTotal');
	for(i=0; i<form.elements.length; i++) {
		if (form.elements[i].id.indexOf('check') != -1  ){		
			if (form.elements[i].checked){
				 aux = form.elements[i].id.substring(31,form.elements[i].id.length);
	
				 aux = aux.substring(0,aux.indexOf(':'));
				 
				if (aux != linha){
					valorMedio = document.getElementById(form.id + ':' + aux + ':hiddenVlrMedio');
					
					valor = valorMedio.value
					if (valor == null){
						valor = 0
					}else{
						while (valor.indexOf(".") > 0){
					        valor = valor.replace(".","");	
					    }
						valor = valor.replace(",", ".");
					}
				
					
					soma = soma + parseFloat(valor);
				}
			}
		}	
		
	}	
	


	return soma;
}