/************************************************
*Function: allTrim()
*Remove espa�os em branco a direita e a esquerda
*Par�metro:objeto
************************************************/  
function allTrim(objeto){
	if(objeto.value == null)
		return '';

	return objeto.value.replace(/^\s+|\s+$/g,"");
}
		
/************************************************
*Function: envoy()
*Detecta si ya se ha hecho un submit en un formulario
*Par�metros:no
*Retorno:messagem alert
************************************************/       
var count=0;
function envoy() {                       
  if (count == 0){
	  count++;
      return true;
  }else {
     alert("Os dados est�o sendo processados...");
     return false;
  }
}


/************************************************
* function naoEspeciaisApenas
* Retorna apenas caracteres n�o especiais.
* Input: myfield, e 
************************************************/       
function naoEspeciaisApenas(myfield, e)
{
    var key;
    var keychar;
    
    if (window.event)
       key = window.event.keyCode;
    else if (e)
       key = e.which;
    else
       return true;
    keychar = String.fromCharCode(key);
    // control keys
    if ((key==null) || (key==0) || (key==8) || 
        (key==9) || (key==13) || (key==27) )
       return true;
    // caracteres especiais
    else if ((key==39) || (key==38) || (key==64) || 
        (key==60) || (key==62) || (key==59) || 
        (key==125) || (key==123) || (key==34))
       return false;
    else
       return true;
}

/************************************************
* function : hasValue
* Descri��o : Verifica se a string nao e composta somente de 
* espa�os.
* Input: texto 
************************************************/       
function hasValue(texto) {
    var novoTexto = ""  
    for(iTmp=0;iTmp<texto.length;iTmp++) {
        if(texto.charAt(iTmp) != " ") {
            return true;
        }
    }
    return false;
}

/************************************************
* Function  : max_car
* Descri��o : Valida campo com mais de 255 caracteres
*************************************************/
function max_car(objeto,nome) {
    var desc = new String(objeto.value);
    if (desc.length > 255) {
        alert("Campo " + nome + " excedeu os 255 caracteres permitidos");
        objeto.value=desc.substring(0,255);		
        objeto.focus();			
        return false;
    }
}

/************************************************
* Function: showtip
* Cria um ToolTip
* Input: this, evento, texto
************************************************/

function showtip(current, e, text) {
    current.style.cursor = 'hand';
    if (document.all) {
        thetitle = text.split('<br>');
        if (thetitle.length > 1) {
            thetitles = '';
            for (i = 0; i < thetitle.length; i++)
                thetitles += thetitle[i];
            current.title = thetitles;
        } else 
            current.title = text;
    } else if (document.layers) {
        document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:12px;">' + text + '</layer>');
        document.tooltip.document.close();
        document.tooltip.left = e.pageX + 5;
        document.tooltip.top = e.pageY + 5;
        document.tooltip.visibility = "show";
    }
}


/************************************************
* Function: isMember
* Input: texto, texto
************************************************/
function isMember(strValue, strArray) {
    var isMemberResult = false;
    var strArrayLen = strArray.length;
    var iStrArray;

    for (iStrArray=0; iStrArray < strArrayLen; iStrArray++) {
        if (strValue == strArray[iStrArray]) {
            isMemberResult = true;
            break;
        }
    }

    return isMemberResult;
}

/************************************************
* Function: fieldFormat
* Input: inputObj, format, allowed
************************************************/
function fieldFormat(inputObj, format, allowed) {
    var thisValue = inputObj.value;
    var thisLength = thisValue.length;
    var formatLength = format.length;
    var validArray = new Array();

    for (var i=0; i < allowed.length; i++) {
        validArray[i] = allowed.substr(i,1);
    }

    for (var i=0; i < thisLength; i++) {
        if (i < formatLength) {
            if (format.substr(i,1) == '*') {
                if (!isMember(thisValue.substr(i,1), validArray)) {
                    thisValue = thisValue.substr(0,i) + thisValue.substr(i+1);
                    thisLength = thisValue.length;
                    i--;
                }
            } else {
                if (thisValue.substr(i,1) != format.substr(i,1)) {
                    thisValue = thisValue.substr(0,i) + format.substr(i,1) + thisValue.substr(i);
                    thisLength = thisValue.length;
                }
            }
        } else {
            thisValue = thisValue.substr(0,i);
            thisLength = thisValue.length;
        }
    }

    if (inputObj.value != thisValue) {
        inputObj.value = thisValue;
    }
}

function habilitarBotaoEstatisticasLote(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{


		if (form.elements[i].id == 'pesqConsultarLotesForm:btnEstatisticasLote'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqConsultarLotesForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}

function habilitarBotaoLoteEOcorrencia(form, radio, btoLote, btoOcorrencia, habLote, habRemessa){

	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
 		if (form.elements[i].id == 'pesqArqRemessaRecebidoForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		

	if(habLote == 'V'){
		form.elements [btoLote].disabled = false; 
	} else {
		form.elements [btoLote].disabled = true;
	}

	if(habRemessa == 'V'){
		form.elements [btoOcorrencia].disabled = false; 
	} else {
		form.elements [btoOcorrencia].disabled = true; 	
	}	
}

/* function habilitarBotaoConsultarClienteRemessa(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqArqRemessaRecebidoForm:btnConsultarCliente'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqArqRemessaRecebidoForm:hiddenRadioFiltro'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}*/

function habilitarBotaoOcorrencias(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{	
		if (form.elements[i].id == 'pesqArqRemessaRecebidoInconsistenteForm:btnOcorrencia'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqArqRemessaRecebidoInconsistenteForm:hiddenRadioIncosistente'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}	
	}	
}

function habilitarBotaoConfirmarCliente(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'identificacaoClienteForm:btnConfirmar'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'identificacaoClienteForm:hiddenRadioCliente'){		
			hidden = form.elements[i];
			hidden.value = radio.value;	
		}
		
	}	

}

function habilitarCamposFiltroManterComposicao(form, radio){	
	var hidden;

	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){
			if (form.elements[i].id == 'pesqManterComposicaoForm:hiddenRadioFiltro'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			
			if (form.elements[i].id == 'pesqManterComposicaoForm:formaLiquidacao'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:produto'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:operacao'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:tipoServico'){
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:formaLancamento'){
				form.elements[i].disabled=true;
			}
		}
	}
	
	if ( radio.value == 1 ) {
		for(i=0; i<form.elements.length; i++){
			if (form.elements[i].id == 'pesqManterComposicaoForm:hiddenRadioFiltro'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:formaLiquidacao'){
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:produto'){
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:operacao'){
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:tipoServico'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'pesqManterComposicaoForm:formaLancamento'){
				form.elements[i].disabled=false;
			}
		}
	}
}		


function habilitarCamposFiltroIdenticacaoCliente(form, radio){	
	var hidden;
	//Filtro para Club
	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id+':hiddenRadioFiltroArgumento'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}

			if (form.elements[i].id == form.id+':txtCnpj1'){
				form.elements[i].value="";				
				form.elements[i].readOnly=false;
				form.elements[i].focus();				
			}
			if (form.elements[i].id == form.id+':txtCnpj2'){
				form.elements[i].value="";
				form.elements[i].readOnly=false;
			}
			if (form.elements[i].id == form.id+':txtCnpj3'){
				form.elements[i].value="";
				form.elements[i].readOnly=false;
			}			
			if (form.elements[i].id == form.id+':btnLimparDados'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == form.id+':txtCpf1'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			if (form.elements[i].id == form.id+':txtCpf2'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			
			if (form.elements[i].id == form.id+':txtNomeRazao'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			if (form.elements[i].id == form.id+':txtDiaNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}	
				
			if (form.elements[i].id == form.id+':txtMesNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
					
			if (form.elements[i].id == form.id+':txtAnosNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}		
			
			if (form.elements[i].id == form.id+':txtBanco'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}		
			if (form.elements[i].id == form.id+':txtAgencia'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}														
			if (form.elements[i].id == form.id+':txtConta'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}				
		}
	}
	
	//Filtro para CPF
	if ( radio.value == 1 ){
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id+':hiddenRadioFiltroArgumento'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}

			if (form.elements[i].id == form.id+':txtCnpj1'){
				form.elements[i].value="";
				form.elements[i].focus();
				form.elements[i].readOnly=true;
				
			}
			if (form.elements[i].id == form.id+':txtCnpj2'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			
			if (form.elements[i].id == form.id+':txtCnpj3'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			
			if (form.elements[i].id == form.id+':btnLimparDados'){
				form.elements[i].disabled=false;
			}
			
			if (form.elements[i].id == form.id+':txtCpf1'){
				form.elements[i].value="";
				form.elements[i].readOnly=false;
				form.elements[i].focus();				
			}
			if (form.elements[i].id == form.id+':txtCpf2'){
				form.elements[i].value="";
				form.elements[i].readOnly=false;
			}
			
			if (form.elements[i].id == form.id+':txtNomeRazao'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			if (form.elements[i].id == form.id+':txtDiaNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}	
				
			if (form.elements[i].id == form.id+':txtMesNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
					
			if (form.elements[i].id == form.id+':txtAnosNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}	
			if (form.elements[i].id == form.id+':txtBanco'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}		
			if (form.elements[i].id == form.id+':txtAgencia'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}														
			if (form.elements[i].id == form.id+':txtConta'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}				
		}		
	}
	
	//Filtro para nomeRazao e DataNascimento/Fundacao
	if ( radio.value == 2 ){
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id+':hiddenRadioFiltroArgumento'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}

			if (form.elements[i].id == form.id+':txtCnpj1'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			if (form.elements[i].id == form.id+':txtCnpj2'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			
			if (form.elements[i].id == form.id+':txtCnpj3'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			
			if (form.elements[i].id == form.id+':txtCpf1'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			if (form.elements[i].id == form.id+':txtCpf2'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			
			if (form.elements[i].id == form.id+':txtNomeRazao'){
				form.elements[i].value="";
				form.elements[i].readOnly=false;
				form.elements[i].focus();				
			}
			if (form.elements[i].id == form.id+':txtDiaNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=false;
			}	
				
			if (form.elements[i].id == form.id+':txtMesNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=false;
			}
					
			if (form.elements[i].id == form.id+':txtAnosNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=false;
			}	
			
			if (form.elements[i].id == form.id+':txtBanco'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}		
			if (form.elements[i].id == form.id+':txtAgencia'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}														
			if (form.elements[i].id == form.id+':txtConta'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}				
		}			
	}
	
	//Filtro para Banco/Agencia/Conta
	if ( radio.value == 3 ){
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == form.id+':hiddenRadioFiltroArgumento'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}

			if (form.elements[i].id == form.id+':txtCnpj1'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			if (form.elements[i].id == form.id+':txtCnpj2'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			if (form.elements[i].id == form.id+':txtCnpj3'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			
			if (form.elements[i].id == form.id+':txtCpf1'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			if (form.elements[i].id == form.id+':txtCpf2'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			
			if (form.elements[i].id == form.id+':txtNomeRazao'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
			if (form.elements[i].id == form.id+':txtDiaNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}	
				
			if (form.elements[i].id == form.id+':txtMesNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}
					
			if (form.elements[i].id == form.id+':txtAnosNasc'){
				form.elements[i].value="";
				form.elements[i].readOnly=true;
			}			
			
			if (form.elements[i].id == form.id+':txtBanco'){
				form.elements[i].value = '237';
				form.elements[i].readOnly=false;
			}		
			if (form.elements[i].id == form.id+':txtAgencia'){
				form.elements[i].value="";				
				form.elements[i].readOnly=false;
				form.elements[i].focus();
			}														
			if (form.elements[i].id == form.id+':txtConta'){
				form.elements[i].value="";
				form.elements[i].readOnly=false;
			}				
		}			
		
	}
}

function valida_cnpj(form)
{
	var cnpj, hidden, radio;
	var cnpj1, cnpj2,cnpj3;
    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
    digitos_iguais = 1;
	
	for(i=0; i<form.elements.length; i++){
	
		if (form.elements[i].id == form.id+':hiddenRadioFiltroArgumento'){
			radio = form.elements[i];
		}
		if (form.elements[i].id == form.id+':txtCnpj1'){
			cnpj1 = form.elements[i].value;
		}
		if (form.elements[i].id == form.id+':txtCnpj2'){
			cnpj2 = form.elements[i].value;
		}
		if (form.elements[i].id == form.id+':txtCnpj3'){
			cnpj3 = form.elements[i].value;
		}
		
		if (form.elements[i].id == form.id+':hiddenRadioValidaCnpj'){	
			hidden = form.elements[i];
		}
	}
	
	cnpj = cnpj1+cnpj2+cnpj3;

	cnpj = cnpj.replace("-", "");
	cnpj = cnpj.replace("/", "");			
    while (cnpj.indexOf(".") > 0){
        cnpj = cnpj.replace(".","");	
    }
	
	
	if (radio.value == '0'){
	
	      if (cnpj.length < 14 && cnpj.length < 15){
	         hidden.value  ='F';
	         return false;
	         
	      }
	            
	      for (i = 0; i < cnpj.length - 1; i++)
	          if (cnpj.charAt(i) != cnpj.charAt(i + 1))
	          {
	             digitos_iguais = 0;
	             break;
	          }
	      
	      if (!digitos_iguais)
	      {
	         tamanho = cnpj.length - 2
	         numeros = cnpj.substring(0,tamanho);
	         digitos = cnpj.substring(tamanho);
	         soma = 0;
	         pos = tamanho - 7;
	         for (i = tamanho; i >= 1; i--)
	      	 {
	              soma += numeros.charAt(tamanho - i) * pos--;
	              if (pos < 2)
	                 pos = 9;
	         }
	         resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	         if (resultado != digitos.charAt(0)){
	         	hidden.value  ='F';
	         	return false;
	         }
	         tamanho = tamanho + 1;
	         numeros = cnpj.substring(0,tamanho);
	         soma = 0;
	         pos = tamanho - 7;
	         for (i = tamanho; i >= 1; i--)
	         {
	             soma += numeros.charAt(tamanho - i) * pos--;
	             if (pos < 2)
	                pos = 9;
	         }
	         resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	         if (resultado != digitos.charAt(1)){
		        hidden.value  ='F';
	            return false;
	         }
	         hidden.value  ='T';
	         return true;
	     }
	     else{
		     hidden.value  ='F';
	         return false;
	     }
	 }else{
	     hidden.value  ='T';
	 	return true;
	 }
} 


function valida_cpf(form){
      var numeros, digitos, soma, i, resultado, digitos_iguais;
    	var cpf1,cpf2;
   	var hidden, radio;
   	
   		for(i=0; i<form.elements.length; i++){
	
			if (form.elements[i].id == form.id+':hiddenRadioFiltroArgumento'){
				radio = form.elements[i];
			}
		
			
			if (form.elements[i].id == form.id+':txtCpf1'){
				cpf1 = form.elements[i].value;
			}
			if (form.elements[i].id == form.id+':txtCpf2'){
				cpf2 = form.elements[i].value;
			}
			
			if (form.elements[i].id == form.id+':hiddenRadioValidaCPF'){		
				hidden = form.elements[i];
			}

		}
   	
	if (radio.value == '1'){
          
		  cpf = cpf1 + cpf2;
	      digitos_iguais = 1;
	      if (cpf.length < 11){
	            hidden.value  ='F';
	            return false;
	      }
	      for (i = 0; i < cpf.length - 1; i++)
	            if (cpf.charAt(i) != cpf.charAt(i + 1))
	                  {
	                  digitos_iguais = 0;
	                  break;
	                  }
	      if (!digitos_iguais)
	            {
	            numeros = cpf.substring(0,9);
	            digitos = cpf.substring(9);
	            soma = 0;
	            for (i = 10; i > 1; i--)
	                  soma += numeros.charAt(10 - i) * i;
	            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	            if (resultado != digitos.charAt(0)){
       	               hidden.value  ='F';
	                  return false;
	            }
	            numeros = cpf.substring(0,10);
	            soma = 0;
	            for (i = 11; i > 1; i--)
	                  soma += numeros.charAt(11 - i) * i;
	            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	            if (resultado != digitos.charAt(1)){
  	               hidden.value  ='F';
					return false;
	             }
			    hidden.value  ='T';
				return true;

	      }else{
  	               hidden.value  ='F';
					return false;

	      }
	  }else{
               hidden.value  ='T';
				return true;
	  }
}


function valida_cnpjCliente(form)
{
	var cnpj;
    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
    digitos_iguais = 1;
    var hidden, radio;
      
      
	for(i=0; i<form.elements.length; i++){
	
		if (form.elements[i].id == 'identificacaoClienteForm:hiddenRadioFiltroArgumento'){
			radio = form.elements[i];
		}
	
		if (form.elements[i].id == 'identificacaoClienteForm:txtCpfCnpj'){
			cnpj = form.elements[i].value;
			cnpj = cnpj.replace("-", "");
			cnpj = cnpj.replace("/", "");			
	        while (cnpj.indexOf(".") > 0){
	            cnpj = cnpj.replace(".","");	
	        }
		}

		if (form.elements[i].id == 'identificacaoClienteForm:hiddenRadioValidaCPFCliente'){		
			hidden = form.elements[i];
		}
	}
	
	if (radio.value == 1){
	
	      if (cnpj.length < 14 && cnpj.length < 15){
	         hidden.value  ='F';
	         return false;
	         
	      }
	            
	      for (i = 0; i < cnpj.length - 1; i++)
	          if (cnpj.charAt(i) != cnpj.charAt(i + 1))
	          {
	             digitos_iguais = 0;
	             break;
	          }
	      
	      if (!digitos_iguais)
	      {
	         tamanho = cnpj.length - 2
	         numeros = cnpj.substring(0,tamanho);
	         digitos = cnpj.substring(tamanho);
	         soma = 0;
	         pos = tamanho - 7;
	         for (i = tamanho; i >= 1; i--)
	      	 {
	              soma += numeros.charAt(tamanho - i) * pos--;
	              if (pos < 2)
	                 pos = 9;
	         }
	         resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	         if (resultado != digitos.charAt(0)){
	         	hidden.value  ='F';
	         	return false;
	         }
	         tamanho = tamanho + 1;
	         numeros = cnpj.substring(0,tamanho);
	         soma = 0;
	         pos = tamanho - 7;
	         for (i = tamanho; i >= 1; i--)
	         {
	             soma += numeros.charAt(tamanho - i) * pos--;
	             if (pos < 2)
	                pos = 9;
	         }
	         resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	         if (resultado != digitos.charAt(1)){
		        hidden.value  ='F';
	            return false;
	         }
	         hidden.value  ='T';
	         return true;
	     }
	     else{
		     hidden.value  ='F';
	         return false;
	     }
	 }else{
	 	return true;
	 }
	 }
	 
	function habilitarBotaoConsultarClienteRetorno(form, radio){	
		var hidden;
		for(i=0; i<form.elements.length; i++)
		{
			if (form.elements[i].id == 'pesqArqRetornoForm:btnConsultarCliente'){
				form.elements[i].disabled=false;
			}	
			if (form.elements[i].id == 'pesqArqRetornoForm:hiddenRadioFiltro'){		
				hidden = form.elements[i];
				hidden.value = radio.value;		
			}
			
		}	
	}
	 
function habilitarBotaosLoteRetorno(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqArqRetornoForm:btnPesquisarLotes'){
			form.elements[i].disabled=false;
		}	

 		if (form.elements[i].id == 'pesqArqRetornoForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}
	 
function habilitarBotaoEstatisticasLoteRetorno(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqConsultarLotesRetornoForm:btnEstatisticasLote'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqConsultarLotesRetornoForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;
		}
	}		
}
	 
function radioGridInconsistentesCMPI0010(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqEstatisticasInconsistentesForm:btnInconsistentes'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqEstatisticasInconsistentesForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;	
		}
		
	}	
}		

	function habilitarBotaoConsultarClienteEmissaoAutomatica(form, radio){	
		var hidden;
		for(i=0; i<form.elements.length; i++)
		{
			if (form.elements[i].id == 'pesqEmissaoAutomaticaForm:btnConsultarCliente'){
				form.elements[i].disabled=false;
			}	
			if (form.elements[i].id == 'pesqEmissaoAutomaticaForm:hiddenRadioFiltro'){		
				hidden = form.elements[i];
				hidden.value = radio.value;		
			}
			
		}	
	}
 function habilitarBotaosPesqEmissaoAutomatica(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqEmissaoAutomaticaForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

		if (form.elements[i].id == 'pesqEmissaoAutomaticaForm:btnExcluir'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqEmissaoAutomaticaForm:btnHistorico'){
			form.elements[i].disabled=false;
		}

 		if (form.elements[i].id == 'pesqEmissaoAutomaticaForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}
 
  function habilitarBotaosHistorico(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'historicoEmissaoAutomaticaForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

 		if (form.elements[i].id == 'historicoEmissaoAutomaticaForm:hiddenRadioHistorico'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
 }

  function pgic0001HabilitarCampos(form, radio){	
  	if ( radio.value == 0 ) {
  		for(i=0; i<form.elements.length; i++){
			if (form.elements[i].id == 'pgic0001Form:txtCodigo'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'pgic0001Form:btnIncluir'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'pgic0001Form:btnHistorico'){
				form.elements[i].disabled=false;
			}
		}
	}
  }

  function habilitarBotaosPgic0001(form, radio){
  	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pgic0001Form:btnDetalhar'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pgic0001Form:btnAlterar'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pgic0001Form:btnExcluir'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pgic0001Form:hiddenRadioGrid'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
  
  }
  
  function habilitarBotaoPgic0008(form, radio){
  	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pgic0008Form:btnDetalhar'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pgic0008Form:hiddenRadioGrid'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}

	}		
  
  }



 function habilitarBotaosPesqManterComposicao(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterComposicaoForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

		if (form.elements[i].id == 'pesqManterComposicaoForm:btnExcluir'){
			form.elements[i].disabled=false;
		}	

 		if (form.elements[i].id == 'pesqManterComposicaoForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}


function habilitarCamposFiltroContrato(form, radio){	
var hidden;

	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'identificacaoContratoForm:hiddenRadioFiltroArgumento'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}

			if (form.elements[i].id == 'identificacaoContratoForm:btnConsultar'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:btnLimparDados'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:empresa'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:tipo'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:numero'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:agenciaOperadora'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:gerenteResponsavel'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.day'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.month'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.year'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			
			
			
			if (form.elements[i].id == 'identificacaoContratoForm:situacaoContrato'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
		}
	}
	

	if ( radio.value == 1 ){
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'identificacaoContratoForm:hiddenRadioFiltroArgumento'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}

			if (form.elements[i].id == 'identificacaoContratoForm:btnConsultar'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:btnLimparDados'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:empresa'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:tipo'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:numero'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:agenciaOperadora'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:gerenteResponsavel'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.day'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.month'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.year'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			
			if (form.elements[i].id == 'identificacaoContratoForm:situacaoContrato'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
	
		}		
	}
	

	if ( radio.value == 2 ){
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'identificacaoContratoForm:hiddenRadioFiltroArgumento'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}

			if (form.elements[i].id == 'identificacaoContratoForm:btnConsultar'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:btnLimparDados'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:empresa'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:tipo'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:numero'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:agenciaOperadora'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:gerenteResponsavel'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.day'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}		
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.month'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}		
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.year'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}		
			
			if (form.elements[i].id == 'identificacaoContratoForm:situacaoContrato'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
	
		}			
	}
	

	if ( radio.value == 3 ){
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'identificacaoContratoForm:hiddenRadioFiltroArgumento'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}

			if (form.elements[i].id == 'identificacaoContratoForm:btnConsultar'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:btnLimparDados'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:empresa'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:tipo'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:numero'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:agenciaOperadora'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:gerenteResponsavel'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.day'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.month'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.year'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			
			if (form.elements[i].id == 'identificacaoContratoForm:situacaoContrato'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}		
		
		}			
		
	}
}

function habilitarBotaoConfirmarContrato(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'identificacaoContratoForm:btnConfirmar'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'identificacaoContratoForm:hiddenRadioContrato'){		
			hidden = form.elements[i];
			hidden.value = radio.value;	
		}
		
	}	

}


 function habilitarBotaosPesqManterLancamento(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterLancamentoForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

		if (form.elements[i].id == 'pesqManterLancamentoForm:btnExcluir'){
			form.elements[i].disabled=false;
		}	

		if (form.elements[i].id == 'pesqManterLancamentoForm:btnAlterar'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterLancamentoForm:btnHistorico'){
			form.elements[i].disabled=false;
		}	
 		if (form.elements[i].id == 'pesqManterLancamentoForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}

function habilitarCamposFiltroContrato(form, radio){	
var hidden;

	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'pesqManterLancamentoForm:hiddenRadioFiltroArgumento'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}

			if (form.elements[i].id == 'identificacaoContratoForm:btnConsultar'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:btnLimparDados'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:empresa'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:tipo'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:numero'){
				form.elements[i].value="";
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:agenciaOperadora'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:gerenteResponsavel'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.day'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.month'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			if (form.elements[i].id == 'identificacaoContratoForm:calendarioData.year'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
			
			
			if (form.elements[i].id == 'identificacaoContratoForm:situacaoContrato'){
				form.elements[i].value="";
				form.elements[i].disabled=true;
			}		
		}
	}
}

function habilitarCamposFiltroManterLancamento(form, radio){	
var hidden;
	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'pesqManterLancamentoForm:hiddenRadioFiltro'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'pesqManterLancamentoForm:codigoMensagem'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].name == 'pesqManterLancamentoForm:restricaoFiltro'){
				form.elements[i].disabled=false;
			}
		}
	}
	if ( radio.value == 1 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'pesqManterLancamentoForm:hiddenRadioFiltro'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'pesqManterLancamentoForm:codigoLancamentoDebito'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'pesqManterLancamentoForm:codigoLancamentoCredito'){
				form.elements[i].disabled=false;
			}
		}
	}
	if ( radio.value == 2 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'pesqManterLancamentoForm:hiddenRadioFiltro'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'pesqManterLancamentoForm:produto'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'pesqManterLancamentoForm:operacao'){
				form.elements[i].disabled=false;
			}
		}
	}
	
	
}

function habilitarCamposFiltroManterLancamentoHistorico(form, radio){	
var hidden;
	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'historicoManterLancamentoForm:hiddenRadioFiltro'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'historicoManterLancamentoForm:codigoMensagem'){
				form.elements[i].disabled=false;
			}
		}
	}
}

 function habilitarBotaosPesqManterLancamentoHistorico(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'historicoManterLancamentoForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

		
	}		
}

 function habilitarBotaosManterCadastroControlesProcesso(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterCadastroControlesProcessoForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

		if (form.elements[i].id == 'pesqManterCadastroControlesProcessoForm:btnExcluir'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterCadastroControlesProcessoForm:btnAlterar'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterCadastroControlesProcessoForm:btnBloquear'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterCadastroControlesProcessoForm:btnHistorico'){
			form.elements[i].disabled=false;
		}	

 		if (form.elements[i].id == 'pesqManterCadastroControlesProcessoForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}


 function habilitarBotaosHistoricoManterCadastroControlesProcesso(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'historicoManterCadastroControlesProcessoForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

 		if (form.elements[i].id == 'historicoManterCadastroControlesProcessoForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}

function habilitarBotaoConsultarClienteManterVinculacaoMensagensHistoricoComplementar(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'manterVincMsgHistoricoComplementarOperacaoContratadaForm:btnConsultarCliente'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'manterVincMsgHistoricoComplementarOperacaoContratadaForm:hiddenRadioFiltro'){		
			hidden = form.elements[i];
			hidden.value = radio.value;				
		}		
	}	
}

 function habilitarBotaosManterFuncoesAlcada(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterFuncoesAlcadaForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

		if (form.elements[i].id == 'pesqManterFuncoesAlcadaForm:btnExcluir'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterFuncoesAlcadaForm:btnAlterar'){
			form.elements[i].disabled=false;
		}	

 		if (form.elements[i].id == 'pesqManterFuncoesAlcadaForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}


function habilitarCamposFiltroManterFuncoesAlcadaIncluir(form, radio){	
var hidden;
	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:hiddenRadioRegra'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:regra'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:operacao'){
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:produto'){
				form.elements[i].disabled=true;
			}
		}
	}
	if ( radio.value == 1 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:hiddenRadioRegra'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:operacao'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:produto'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'incluirManterFuncoesAlcadaForm:regra'){
				form.elements[i].disabled=true;
			}
			
		}
	}
}

function habilitarCamposFiltroManterFuncoesAlcadaAlterar(form, radio){	
var hidden;
	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:hiddenRadioRegra'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:regra'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:operacao'){
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:produto'){
				form.elements[i].disabled=true;
			}
		}
	}
	if ( radio.value == 1 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:hiddenRadioRegra'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:operacao'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:produto'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'alterarManterFuncoesAlcadaForm:regra'){
				form.elements[i].disabled=true;
			}
		}
	}
}

 function habilitarBotaosManterMensagemComprovanteSalarial(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterMensagemComprovanteSalarialForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

		if (form.elements[i].id == 'pesqManterMensagemComprovanteSalarialForm:btnExcluir'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterMensagemComprovanteSalarialForm:btnAlterar'){
			form.elements[i].disabled=false;
		}	

 		if (form.elements[i].id == 'pesqManterMensagemComprovanteSalarialForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}

 function habilitarBotaosManterOrgaoPagador(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterOrgaoPagadorForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

		if (form.elements[i].id == 'pesqManterOrgaoPagadorForm:btnExcluir'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterOrgaoPagadorForm:btnAlterar'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterOrgaoPagadorForm:btnBloquear'){
			form.elements[i].disabled=false;
		}	

 		if (form.elements[i].id == 'pesqManterOrgaoPagadorForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}

function habilitarCamposFiltroManterOrgaoPagadorIncluir(form, radio){	
var hidden;
	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'incluirManterOrgaoPagadorForm:hiddenRadio'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'incluirManterOrgaoPagadorForm:empresaGrupoBradesco'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'incluirManterOrgaoPagadorForm:empresaParceira'){
				form.elements[i].disabled=true;
			}
		}
	}
	if ( radio.value == 1 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'incluirManterOrgaoPagadorForm:hiddenRadio'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'incluirManterOrgaoPagadorForm:empresaGrupoBradesco'){
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'incluirManterOrgaoPagadorForm:empresaParceira'){
				form.elements[i].disabled=false;
			}
		}
	}
	
}
function habilitarCamposFiltroManterOrgaoPagadorAlterar(form, radio){	
var hidden;
	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'alterarManterOrgaoPagadorForm:hiddenRadio'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'alterarManterOrgaoPagadorForm:empresaGrupoBradesco'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'alterarManterOrgaoPagadorForm:empresaParceira'){
				form.elements[i].disabled=true;
			}
		}
	}
	if ( radio.value == 1 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'alterarManterOrgaoPagadorForm:hiddenRadio'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'alterarManterOrgaoPagadorForm:empresaGrupoBradesco'){
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'alterarManterOrgaoPagadorForm:empresaParceira'){
				form.elements[i].disabled=false;
			}
		}
	}
	
}






function alterarRadioIndicadorResponsabilidadePGIC0054(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterCadastroTipoPendencia:hiddenRadioIndicadorResponsabilidade'){		
			hidden = form.elements[i];
			hidden.value = radio.value;
		}		
	}		
}

function alterarRadioTipoBaixaPGIC0054(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterCadastroTipoPendencia:hiddenRadioTipoBaixa'){		
			hidden = form.elements[i];
			hidden.value = radio.value;
		}		
	}		
}

function alterarRadioTipoBaixaPGIC0056(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:hiddenTipoBaixa'){		
			hidden = form.elements[i];
			hidden.value = radio.value;
			
		}		
	}		
}

function alterarRadioIndicadorResponsabilidadePGIC0056(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaIncluir:hiddenIndicadorResponsabilidade'){		
			hidden = form.elements[i];
			hidden.value = radio.value;
			
		}		
	}		
}

function alterarRadioTipoBaixaPGIC0058(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:hiddenTipoBaixa'){		
			hidden = form.elements[i];
			hidden.value = radio.value;
			
		}		
	}		
}

function alterarRadioIndicadorResponsabilidadePGIC0058(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterCadastroTipoPendenciaAlterar:hiddenIndicadorResponsabilidade'){		
			hidden = form.elements[i];
			hidden.value = radio.value;
			
		}		
	}		
}     

 function habilitarBotaoConsultarClienteContratoPGIC0165(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterAssociacaoMensagemComprovanteSalarial:btnConsultarCliente'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterAssociacaoMensagemComprovanteSalarial:hiddenRadioClienteContrato'){		
			hidden = form.elements[i];
			hidden.value = radio.value;
		}		
	}		
}

function alterarRadioIndicadorMensagemPGIC0167(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'ManterAssociacaoMensagemComprovanteSalarialIncluir:hiddenIndicadorMensagem'){		
			hidden = form.elements[i];
			hidden.value = radio.value;
		}		
	}		
}    


 function habilitarBotaosManterMensagemAlertaGestor(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterMensagemAlertaGestorForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

		if (form.elements[i].id == 'pesqManterMensagemAlertaGestorForm:btnExcluir'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterMensagemAlertaGestorForm:btnAlterar'){
			form.elements[i].disabled=false;
		}	

 		if (form.elements[i].id == 'pesqManterOrgaoPagadorForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}
function habilitarCamposFiltroManterMensagemAlertaGestorIncluir(form, radio){	
var hidden;
	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:hiddenRadio'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:codigoFuncionario'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:nomeFuncionario'){
				form.elements[i].disabled=true;
			}
		}
	}
	if ( radio.value == 1 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:hiddenRadio'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:codigoFuncionario'){
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'incluirManterMensagemAlertaGestorForm:nomeFuncionario'){
				form.elements[i].disabled=false;
			}
		}
	}
	
}
function habilitarCamposFiltroManterMensagemAlertaGestorAlterar(form, radio){	
var hidden;
	if ( radio.value == 0 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:hiddenRadio'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:codigoFuncionario'){
				form.elements[i].disabled=false;
			}
			if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:nomeFuncionario'){
				form.elements[i].disabled=true;
			}
		}
	}
	if ( radio.value == 1 ) {
		for(i=0; i<form.elements.length; i++){

			if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:hiddenRadio'){		
				hidden = form.elements[i];
				hidden.value = radio.value;	
			}
			if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:codigoFuncionario'){
				form.elements[i].disabled=true;
			}
			if (form.elements[i].id == 'alterarManterMensagemAlertaGestorForm:nomeFuncionario'){
				form.elements[i].disabled=false;
			}
		}
	}
	
}

 function habilitarBotaosManterPrioridadeCreditoProduto(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterPrioridadeCreditoProdutoForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

		if (form.elements[i].id == 'pesqManterPrioridadeCreditoProdutoForm:btnExcluir'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterPrioridadeCreditoProdutoForm:btnAlterar'){
			form.elements[i].disabled=false;
		}	

 		if (form.elements[i].id == 'pesqManterPrioridadeCreditoProdutoForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}

function radioGridAssociacaoLinhaTipoMensagem(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'manterAssociacaoLinhaTipoMensagemForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}
		if (form.elements[i].id == 'manterAssociacaoLinhaTipoMensagemForm:btnAlterar'){
			form.elements[i].disabled=false;
		}
		if (form.elements[i].id == 'manterAssociacaoLinhaTipoMensagemForm:btnExcluir'){
			form.elements[i].disabled=false;
		}		
		if (form.elements[i].id == 'manterAssociacaoLinhaTipoMensagemForm:hiddenRadioMensagem'){		
			hidden = form.elements[i];
			hidden.value = radio.value;	
		}
		
	}	
}


function habilitarBotaoConsultarClienteVincMsgLancamentoPersonalizadoOperacaoContratada(form, radio){	
		var hidden;
		for(i=0; i<form.elements.length; i++)
		{
			if (form.elements[i].id == 'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:btnConsultarCliente'){
				form.elements[i].disabled=false;
			}	
			if (form.elements[i].id == 'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:hiddenRadioFiltro'){		
				hidden = form.elements[i];
				hidden.value = radio.value;		
			}
			
		}	
}

function habilitarBotaosPesqVincMsgLancamentoPersonalizadoOperacaoContratada(form, radio){
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}
		
		if (form.elements[i].id == 'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:btnExcluir'){
			form.elements[i].disabled=false;
		}		

 		if (form.elements[i].id == 'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}	
}

function habilitarBotaoAvancarIncluirVincMsgLancamentoPersonalizadoOperacaoContratada(form, radio){
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:btnAvancar'){
			form.elements[i].disabled=false;
		}

 		if (form.elements[i].id == 'manterVincMsgLancamentoPersonalizadoOperacaoContratadaForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}	
}


function habilitarBotaosPesqRegraSegregacaoAcessoDados(form, radio){
var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}
		
		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:btnExcluir'){
			form.elements[i].disabled=false;
		}
		
		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:btnAlterar'){
			form.elements[i].disabled=false;
		}		
		
		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:btnExcecao'){
			form.elements[i].disabled=false;
		}		
		

 		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
	}
}


function guardarEscolhaTipoManutencao(form, radio){
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
 		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:hiddenRadioTipoManutencao'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
	}	
}


function habilitarBotaosPesqExcecaoSegregacaoAcessoDados(form, radio){
var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}
		
		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:btnExcluir'){
			form.elements[i].disabled=false;
		}
		
		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:btnAlterar'){
			form.elements[i].disabled=false;
		}				
 		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:hiddenRadioExcecao'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
	}
}


function guardarEscolhaTipoExcecao(form, radio){
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
 		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:hiddenRadioTipoExcecao'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
	}	
}

function guardarEscolhaTipoAbrangenciaExcecao(form, radio){
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
 		if (form.elements[i].id == 'manterRegraSegregacaoAcessoDadosForm:hiddenRadioTipoAbrangenciaExcecao'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
	}	
}

function carregarParametrosManterContrato(){
	document.getElementById('conManterContratoParametros:btnCarregarParametros').click();
}

function habilitarBotaoConsultarClientemanterSolicitacaoRecuperacaoComprovantesExpirados(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'manterSolicitacaoRecuperacaoComprovantesExpiradosForm:btnConsultarCliente'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'manterSolicitacaoRecuperacaoComprovantesExpiradosForm:hiddenRadioFiltro'){		
			hidden = form.elements[i];
			hidden.value = radio.value;				
		}		
	}	
}

function habilitarBotaosManterTipoComprovanteForm(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'pesqManterTipoComprovanteForm:btnDetalhar'){
			form.elements[i].disabled=false;
		}	

		if (form.elements[i].id == 'pesqManterTipoComprovanteForm:btnExcluir'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'pesqManterTipoComprovanteForm:btnAlterar'){
			form.elements[i].disabled=false;
		}	

 		if (form.elements[i].id == 'pesqManterTipoComprovanteForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;		
		}
		
	}		
}

function habilitarBotaosPesqAcoesControladas(form, radio){	
	var hidden;
	for(i=0; i<form.elements.length; i++)
	{
		if (form.elements[i].id == 'manterTipoAcoesControladasForm:btnExcluir'){
			form.elements[i].disabled=false;
		}	
		if (form.elements[i].id == 'manterTipoAcoesControladasForm:hiddenRadio'){		
			hidden = form.elements[i];
			hidden.value = radio.value;				
		}		
	}	
}

function habilitarComboSitProc(){

	var campo = document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:processamento'].value; 
	
	if (campo == 3){	
		document.getElementById('comboHabilitado').style.display = 'block';
		document.getElementById('comboDesabilitado').style.display = 'none'
	}else{
		document.getElementById('comboHabilitado').style.display = 'none'
		document.getElementById('comboDesabilitado').style.display = 'block';
	}
}

function habilitarCaixasDeTextoSequenciaCoEmpresa(){

	var campo = document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:produto'].value; 
	
	if (campo != ""){	
		document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtPerfil'].readOnly = false;
		
	}else{
		document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtPerfil'].readOnly = true; 	
		document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtPerfil'].value = ''; 	
		document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtSeqRemessa'].value = ''; 
		document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtSeqRemessa'].readOnly = true; 		
		document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:processamento'].value = ''; 	
		document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:processamento'].value = '';
		document.getElementById('comboHabilitado').style.display = 'none'
		document.getElementById('comboDesabilitado').style.display = 'block'; 	
	}
}

function habilitaCaixaDePesquisaSequenciaRemessa(){

	var campo = document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtPerfil'].value; 
	
	if (campo != ""){	
		var valor = parseFloat(campo);
	
		if(valor > 0 ){	
			document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtSeqRemessa'].readOnly = false;
		}		
	}else{
		document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtSeqRemessa'].readOnly = true;
		document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtSeqRemessa'].value = ''; 		
	}

}


function habilitaCaixaDePesquisaSequenciaRetorno(){

	var campo = document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtPerfil'].value; 
	
	if (campo != ""){	
		var valor = parseFloat(campo);
	
		if(valor > 0 ){	
			document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtSeqRetorno'].readOnly = false;
		}		
	}else{
		document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtSeqRetorno'].readOnly = true;
		document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtSeqRetorno'].value = ''; 		
	}

}

function habilitarCaixasDeTextoSequenciaCoEmpresaRetorno(){

	var campo = document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:produto'].value; 
	
	if (campo != ""){	
		document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtPerfil'].readOnly = false;
		
	}else{
		document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtPerfil'].readOnly = true; 	
		document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtPerfil'].value = ''; 	
		document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtSeqRetorno'].readOnly = true; 
		document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtSeqRetorno'].value = ''; 		
		document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:tipoRetorno'].value = ''; 	
		document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:situacao'].value = '';			
	}

}


/*
*Fun��o para validar a valor
*/
function validarData(sform,scampo,periodo){

	if ((document.forms [sform].elements [scampo] != null) && (document.forms [sform].elements [scampo].value != '')){
	
		var valor = parseFloat(document.forms [sform].elements [scampo].value);
	
		if(valor != null){	
			if (periodo == 'dia'){	
				if ((valor == 0) || (valor > 31)){
					document.forms [sform].elements [scampo].value = '';
					document.forms [sform].elements [scampo].focus();
					alert('Digite o dia entre o  n\u00famero 1 e 31');
					
				}
			} else if (periodo == 'mes'){	
				if ((valor == 0) || (valor > 12)){
					document.forms [sform].elements [scampo].value = '';
					document.forms [sform].elements [scampo].focus();
					alert('Digite o m\u00eas entre o  n\u00famero 1 e 12!');
				}
			} else{
				if ((valor < 1970) || (valor > 2999)){
					document.forms [sform].elements [scampo].value = '';
					document.forms [sform].elements [scampo].focus();
					alert('Digite o ano entre o  n\u00famero 1970 e 2999!');
				}		
			}
		}
	}
}

function validarDataMsg(sform,scampo,periodo,msg){

	if ((document.forms [sform].elements [scampo] != null) && (document.forms [sform].elements [scampo].value != '')){
	
		var valor = parseFloat(document.forms [sform].elements [scampo].value);
	
		if(valor != null){	
			if (periodo == 'dia'){	
				if ((valor == 0) || (valor > 31)){
					document.forms [sform].elements [scampo].value = '';
					document.forms [sform].elements [scampo].focus();
					alert(msg);
					
				}
			} else if (periodo == 'mes'){	
				if ((valor == 0) || (valor > 12)){
					document.forms [sform].elements [scampo].value = '';
					document.forms [sform].elements [scampo].focus();
					alert(msg);
				}
			} else{
				if ((valor < 1970) || (valor > 2999)){
					document.forms [sform].elements [scampo].value = '';
					document.forms [sform].elements [scampo].focus();
					alert(msg);
				}		
			}
		}
	}
}

/*
*Fun��o para validar os campos da tela de consulta arquivo remessa
*/
function proximoCampo(quatindade,form,campoAtual,proximoCampo){

	var sCampoAtual = document.forms[form].elements[campoAtual].value;
	
	if(sCampoAtual.length == quatindade){
		document.forms[form].elements[proximoCampo].focus(); 
	}
	
}


/*
* Fun��es para inserir m�scara
*/
function aplicamascara(sform,scampo,sfuncao){
	v_obj= document.forms [sform].elements [scampo];
    v_fun=sfuncao    
	setTimeout("execmascara()",1)
}

/*
*Fun��o para executar as m�scara
*/
function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}

/*
*Fun��es de valida��o
*/
function leech(v){
    v=v.replace(/o/gi,"0")
    v=v.replace(/i/gi,"1")
    v=v.replace(/z/gi,"2")
    v=v.replace(/e/gi,"3")
    v=v.replace(/a/gi,"4")
    v=v.replace(/s/gi,"5")
    v=v.replace(/t/gi,"7")
    return v
}

function numeros(v){
    return v.replace(/\D/g,"")
}

function telefone(v){
    v=v.replace(/\D/g,"")
    v=v.replace(/^(\d\d)(\d)/g,"($1) $2")
    v=v.replace(/(\d{4})(\d)/,"$1-$2")
    return v
}

function cpf(v){
    v=v.replace(/\D/g,"")
    v=v.replace(/(\d{3})(\d)/,"$1.$2")
    v=v.replace(/(\d{3})(\d)/,"$1.$2")
    v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2") 
    return v
}

function cep(v){
    v=v.replace(/D/g,"") 
    v=v.replace(/^(\d{5})(\d)/,"$1-$2") 
    return v
}

function cnpj(v){
    v=v.replace(/\D/g,"")                           
    v=v.replace(/^(\d{2})(\d)/,"$1.$2")  
    v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3") 
    v=v.replace(/\.(\d{3})(\d)/,".$1/$2") 
    v=v.replace(/(\d{4})(\d)/,"$1-$2") 
    return v
}

function data(v){
    v=v.replace(/\D/g,"")
	v=v.replace(/(\d{2})(\d)/,"$1/$2")
	v=v.replace(/(\d{2})(\d)/,"$1/$2") 	
    return v
}

function hora(v){
    v=v.replace(/\D/g,"")
	v=v.replace(/(\d{2})(\d)/,"$1:$2")
	return v 
}

function horaMinSeg(v){
    v=v.replace(/\D/g,"")
	v=v.replace(/^([0-1]\d|2[0-3]):[0-5]\d:[0-5]\d$/)
	return v 
}


function valor(v){
    v=v.replace(/\D/g,"")
	v=v.replace(/^([0-9]{2}\.?){2}-[0-9]{2}$/,"$1.$2");
	v=v.replace(/(\d{2})(\d)/g,"$1.$2")
	v=v.replace(/(\d)(\d{2})$/,"$1.$2")	 
	return v
}

function area(v){ 
    v=v.replace(/\D/g,"")
	v=v.replace(/(\d)(\d{2})$/,"$1.$2")
	return v
}

function getFocus(){

	if(document.forms['pesqArqRemessaRecebidoForm'] != null){	
		if(document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtCnpj1'] != null ){		
			document.forms['pesqArqRemessaRecebidoForm'].elements['pesqArqRemessaRecebidoForm:txtCnpj1'].focus();
		
		}	
	}
	
	if(document.forms['pesqArqRetornoForm'] != null){	
		if(document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtCnpj1'] != null ){		
			document.forms['pesqArqRetornoForm'].elements['pesqArqRetornoForm:txtCnpj1'].focus();
		
		}	
	}
	
}


function maxLength(campo, limit) {
	if (campo.value.length > limit) {
		campo.value = campo.value.substring(0, limit);
	}
}



function confirmar(msgConfirma){
	return confirm(msgConfirma);
}

function chamarBotao(){
	document.forms['pesquisaContratoNegociacaoAuxForm'].elements['pesquisaContratoNegociacaoAuxForm:btnRedirecionar'].click();
}



function foco(objeto){

	if (objeto.type == 'radio'){
		var objRdoCliente = document.getElementsByName(objeto.name);	
		objRdoCliente[objeto.value].focus();			
	}else{	
		var objetoCheck = document.getElementById(objeto.id);		
		objetoCheck.focus();
	}
	
	return true;
}


function recuperarDataCalendario(form,idCalendario){
	data = new Date();
	mes = data.getMonth() + 1;
	ano = data.getFullYear();
	
	if ( allTrim(document.getElementById(form.id + ':' + idCalendario + '.day')) == null ||
		  allTrim(document.getElementById(form.id + ':' + idCalendario + '.day')) == '' )
		  
		document.getElementById(form.id + ':' + idCalendario + '.day').value = 01;
	
	if ( allTrim(document.getElementById(form.id + ':' + idCalendario + '.month')) == null ||
		  allTrim(document.getElementById(form.id + ':' + idCalendario + '.month')) == '' )
		  
		document.getElementById(form.id + ':' + idCalendario + '.month').value = mes;

	if ( allTrim(document.getElementById(form.id + ':' + idCalendario + '.year')) == null ||
		  allTrim(document.getElementById(form.id + ':' + idCalendario + '.year')) == '' )
		  
		document.getElementById(form.id + ':' + idCalendario + '.year').value = ano;
	

}

function formataCNPJ(campo, teclapres){
	if(window.event){
		var tecla = teclapres.keyCode;    
	}
	else
		tecla = teclapres.which;

	if(tecla > 47 && tecla < 58){
		var vr = new String(campo.value);
	   
	   	vr = vr.replace(".", "");
	   	vr = vr.replace(".", "");
	   	vr = vr.replace("/", "");
	   	vr = vr.replace("-", "");
	
	   	tam = vr.length + 1;

    	if(tam > 2 && tam < 6)
        	campo.value = vr.substr(0, 2) + '.' + vr.substr(2, tam);
      	if(tam >= 6 && tam < 9)
        	campo.value = vr.substr(0,2) + '.' + vr.substr(2,3) + '.' + vr.substr(5,tam-5);
      	if(tam >= 9 && tam < 13)
        	campo.value = vr.substr(0,2) + '.' + vr.substr(2,3) + '.' + vr.substr(5,3) + '/' + vr.substr(8,tam-8);
     	if(tam >= 13 && tam < 15)
        	campo.value = vr.substr(0,2) + '.' + vr.substr(2,3) + '.' + vr.substr(5,3) + '/' + vr.substr(8,4)+ '-' + vr.substr(12,tam-12);
        
        return true;
    }
    else{
    	event.keyCode = 0;
		return false;
    }
}

function formataPIS(campo, teclapres){
	if(window.event){
		var tecla = teclapres.keyCode;    
	}
	else
		tecla = teclapres.which;

	if(tecla > 47 && tecla < 58){
		var vr = new String(campo.value);
	   
	   	vr = vr.replace(".", "");
	   	vr = vr.replace(".", "");
	   	vr = vr.replace("-", "");
	
	   	tam = vr.length + 1;
	   	
    	if(tam == 4)
			campo.value = vr.substr(0, 3) + '.';
		if(tam == 9)
			campo.value = vr.substr(0, 3) + '.' + vr.substr(3, 8) + '.';
		if(tam == 11)
			campo.value = vr.substr(0, 3) + '.' + vr.substr(3, 5) + '.' + vr.substr(8, 2) + '-';
			
        return true;
    }
    else{
    	event.keyCode = 0;
		return false;
    }
}
 


function formataCPF(campo, teclapres){
	if(window.event){
		var tecla = teclapres.keyCode;    
	}
	else
		tecla = teclapres.which;

	if(tecla > 47 && tecla < 58){
		var vr = new String(campo.value);
	   
	   	vr = vr.replace(".", "");
	   	vr = vr.replace(".", "");
	   	vr = vr.replace("-", "");
	
	   	tam = vr.length + 1;
	   	
    	if(tam == 4)
			campo.value = vr.substr(0, 3) + '.';
		if(tam == 7)
			campo.value = vr.substr(0, 3) + '.' + vr.substr(3, 6) + '.';
		if(tam == 10)
			campo.value = vr.substr(0, 3) + '.' + vr.substr(3, 3) + '.' + vr.substr(6, 3) + '-';
			
        return true;
    }
    else{
    	event.keyCode = 0;
		return false;
    }
}

function validaPIS(campo, msg1) {
	var pis = new String(campo.value);
	
	if(pis.length > 0 && pis.length < 14){
		alert(msg1);
		campo.value = '';
		return false;
	}
	
	return true;
}

function validaCPF(campo, msg1) {
	var cpf = new String(campo.value);
	
	if(cpf.length > 0 && cpf.length < 14){
		alert(msg1);
		campo.value = '';
		return false;
	}
	if(cpf.length == 14){
		cpf = cpf.replace(".", "");
	   	cpf = cpf.replace(".", "");
	   	cpf = cpf.replace("-", "");
	   	
		erro = new String;
	
		if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
			erro += msg1;
		}
		var a = [];
		var b = new Number;
		var c = 11;
		for (i=0; i<11; i++){
			a[i] = cpf.charAt(i);
			if (i < 9)
				b += (a[i] * --c);
		}
		if ((x = b % 11) < 2){
			a[9] = 0;
		}
		else{
			a[9] = 11-x;
		}
		b = 0;
		c = 11;
		for (y=0; y<10; y++) 
			b += (a[y] * c--);
		if ((x = b % 11) < 2){
			a[10] = 0;
		}
		else{
			a[10] = 11-x;
		}
		if((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])){
			erro += msg1;
		}
		if (erro.length > 0){
			alert(erro);
			campo.value = '';
			return false;
		}
	}
	return true;
}

function validaCNPJ(campo, msg1){
	var cnpj = new String(campo.value);
	
	if(cnpj.length > 0 && cnpj.length < 18){
		alert(msg1);
		campo.value = '';
		return false;
	}
	if(cnpj.length == 18){
		cnpj = cnpj.replace(".","");
		cnpj = cnpj.replace(".","");
		cnpj = cnpj.replace("-","");
		cnpj = cnpj.replace("/","");
		
		erro = new String;
		var a = [];
		var b = new Number;
		var c = [6,5,4,3,2,9,8,7,6,5,4,3,2];
		
		if (cnpj == "00000000000000" || cnpj == "11111111111111" || cnpj == "22222222222222" || cnpj == "33333333333333" || cnpj == "44444444444444" || cnpj == "55555555555555" || cnpj == "66666666666666" || cnpj == "77777777777777" || cnpj == "88888888888888" || cnpj == "99999999999999"){
			erro += msg1;
		}else{
		
			for (i=0; i<12; i++){
				a[i] = cnpj.charAt(i);
				b += a[i] * c[i+1];
			}
			if((x = b % 11) < 2){
				a[12] = 0;
			}
			else{
				a[12] = 11-x;
			}
			b = 0;
			
			for(y=0; y<13; y++){
				b += (a[y] * c[y]);
			}
			if((x = b % 11) < 2){
				a[13] = 0;
			}
			else{
				a[13] = 11-x;
			}
			if((cnpj.charAt(12) != a[12]) || (cnpj.charAt(13) != a[13])){
				erro += msg1;
			}
		}	
		
		if(erro.length > 0){
			alert(erro);
			campo.value = '';
			return false;
		}
	}
	return true;
}

function validaCampoNumericoColado(campo){

	var conteudo = clipboardData.getData('Text');
	var valor = campo.value
	
	
	if(conteudo == null ||(trim(conteudo) == '')){
		clipboardData.clearData();
	}	
	
	if((valor == null) || (trim(valor) == '')){
		campo.value = '';
	}	

	if(valor != null){
		//verifica se o valor arrastado para o campo n�o � numerico.
		if( (isNaN(valor) || (valor.indexOf(' ') != -1) || (valor.indexOf('.') != -1)) || ( parseInt(valor) < 0)){
			campo.value = '';
		}
	}
	
	if(conteudo != null){
		//verifica se o conte�do da AT contem caracters diferentes de n�meros ou ponto.
		if((isNaN(conteudo) || (valor.indexOf(' ') != -1) || (conteudo.indexOf('.') != -1)) || (parseInt(conteudo) < 0)){
			clipboardData.clearData();
		}
	}
}

function compara_datas(data_inicial, data_final, msg1){   
	//Verifica se a data inicial � maior que a data final
	var controle = 0;

	dia_inicial = data_inicial.substr(0,2);   
	dia_final = data_final.substr(0,2);   
	mes_inicial = data_inicial.substr(3,2);   
	mes_final = data_final.substr(3,2);   
	ano_inicial = data_inicial.substr(6,4);   
	ano_final = data_final.substr(6,4);

	if(ano_inicial > ano_final){   
		controle = controle + 1;
	} else {   
		if(ano_inicial == ano_final){   
			if(mes_inicial > mes_final){   
				controle = controle + 1;
			} else {   
				if(mes_inicial == mes_final){   
					if(dia_inicial > dia_final){   
						controle = controle + 1;
					}
				}
			}   
		}
	}

	if (controle > 0) {
		return msg1+'\n';
	} else {
		return '';
	}
}

String.prototype.replaceAll = function(de, para) {
    var str = this;
    var pos = str.indexOf(de);

    while (pos > -1) {
		str = str.replace(de, para);
		pos = str.indexOf(de);
	}

    return (str);
};