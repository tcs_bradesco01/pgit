function validaCamposConsultar(form, msgcampo, msgnecessario, msgdata){
	var msgValidacao = '';

    if (document.getElementById(form.id + ':txtPeriodoInicio.day').value == null ||
		allTrim(document.getElementById(form.id + ':txtPeriodoInicio.day')) == ''   ||
		document.getElementById(form.id + ':txtPeriodoInicio.month').value == null ||
		allTrim(document.getElementById(form.id + ':txtPeriodoInicio.month')) == ''   ||
		document.getElementById(form.id + ':txtPeriodoInicio.year').value == null ||
		allTrim(document.getElementById(form.id + ':txtPeriodoInicio.year')) == '' ||
		document.getElementById(form.id + ':txtPeriodoFim.day').value == null ||
		allTrim(document.getElementById(form.id + ':txtPeriodoFim.day')) == ''   ||
		document.getElementById(form.id + ':txtPeriodoFim.month').value == null ||
		allTrim(document.getElementById(form.id + ':txtPeriodoFim.month')) == ''   ||
		document.getElementById(form.id + ':txtPeriodoFim.year').value == null ||
		allTrim(document.getElementById(form.id + ':txtPeriodoFim.year')) == '')
		{
		  msgValidacao = msgcampo + ' ' + msgdata + ' ' + msgnecessario + '!\n';
	    }
	
	if (msgValidacao != '')
	{
		alert(msgValidacao);
		return false;
	}
    else
    {
		return true;	
	}					
}

function validarSolicitacao(form, msgRegistroSelecionado,msgcampo,msgnecessario, msgDestino, msgMaximoRegistros){

	var registroSelecionado = false;
	var msgValidacao = '';
	var qtdeRegistrosSelecionados = document.getElementById(form.id + ':hiddenQtdeRegistrosSelecionados').value
		
	
	if (!document.getElementById(form.id + ':relatorio').checked &&  !document.getElementById(form.id + ':arquivoISD').checked && !document.getElementById(form.id + ':arquivoRetorno').checked){
	
			  msgValidacao = msgcampo + ' ' + msgDestino + ' ' + msgnecessario + '!\n';
	}
	if (qtdeRegistrosSelecionados <=0){
		 msgValidacao = msgValidacao + msgRegistroSelecionado + ' \n';
	}
	
	if (qtdeRegistrosSelecionados > 40){
		 msgValidacao =msgValidacao + msgMaximoRegistros + ' \n';
	}
	
	
	if (msgValidacao != '')	{
		alert(msgValidacao);
		return false;
	}else{
		return true;	
	}		

}

function atualizaRegistrosSelecionado( check){

	var qtdeRegistrosSelecionados = document.getElementById( 'incSolicitacaoRecuperacaoRemessaForm:hiddenQtdeRegistrosSelecionados');
	if (check.checked){
		qtdeRegistrosSelecionados.value = parseInt(qtdeRegistrosSelecionados.value,10) + 1;
	}else{
		qtdeRegistrosSelecionados.value = parseInt(qtdeRegistrosSelecionados.value,10) - 1;
	}
	
	if (qtdeRegistrosSelecionados.value > 0){
		document.getElementById('incSolicitacaoRecuperacaoRemessaForm:btnSolicitarRecuperacao').disabled = false;
	}else{
		document.getElementById('incSolicitacaoRecuperacaoRemessaForm:btnSolicitarRecuperacao').disabled = true;
	}
	
}
