function confirmar(msgConfirma){
	return confirm(msgConfirma);
}


function validaCamposConsultar(form, msgcampo, msgnecessario, msgdata){



	var msgValidacao = '';

	
		if ( document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoInicio.day').value == null ||
		 allTrim(document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoInicio.day')) == ''   ||
		 document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoInicio.month').value == null ||
		 allTrim(document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoInicio.month'))== ''   ||
		 document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoInicio.year').value == null ||
		 allTrim(document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoInicio.year')) == '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if ( document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoFim.day').value == null ||
		 allTrim(document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoFim.day')) == ''   ||
		 document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoFim.month').value == null ||
		 allTrim(document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoFim.month'))== ''   ||
		 document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoFim.year').value == null ||
		 allTrim(document.getElementById('conSolicitacaoRecuperacaoRemessaForm:txtPeriodoFim.year'))== '' ) {
			alert(msgcampo + ' ' + msgdata + ' ' + msgnecessario);
			return false;		
	}
	
	if (msgValidacao != ''){
		alert(msgValidacao);
		return false;
	}
	

	return true;						
}
