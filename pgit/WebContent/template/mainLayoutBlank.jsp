<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="authz" %>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="b"%>
<%@ taglib uri="http://bradesco.com.br/app_components" prefix="app"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<%   
   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1   
   response.setHeader("Pragma","no-cache"); //HTTP 1.0   
   response.setDateHeader ("Expires", 0); //prevents caching   
   response.setHeader("Cache-Control","no-store"); //HTTP 1.1   
%>  

<tiles:useAttribute id="title" name="title" classname="java.lang.String"
	scope="request" ignore="true" />

<tiles:useAttribute id="description" name="description"
	classname="java.lang.String" scope="request" ignore="true" />
<tiles:useAttribute id="author" name="author"
	classname="java.lang.String" scope="request" ignore="true" />
<tiles:useAttribute id="copyright" name="copyright"
	classname="java.lang.String" scope="request" ignore="true" />
<tiles:useAttribute id="keywords" name="keywords"
	classname="java.lang.String" scope="request" ignore="true" />

<tiles:useAttribute name="css" ignore="true" />
<tiles:useAttribute name="especificCSS" ignore="true" />
<tiles:useAttribute name="js" ignore="true" />
<tiles:useAttribute name="especificJS" ignore="true" />

<html>

<f:view>
	<f:loadBundle basename="#{facesContext.application.messageBundle}"
		var="msgs" />

	<head>
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Content-Type" content="text/html;CHARSET=iso-8859-1" />
	<meta http-equiv="Page-Exit"
		content="progid:DXImageTransform.Microsoft.Fade(Duration=0.2,overlap=1)">

	<meta name="description"
		content='<h:outputText value="#{msgs[description]}"/>' />
	<meta name="author" content='<h:outputText value="#{msgs[author]}"/>' />
	<meta name="copyright"
		content='<h:outputText value="#{msgs[copyright]}"/>' />
	<meta name="keywords"
		content='<h:outputText value="#{msgs[keywords]}"/>' />
	<meta name="version"
		content="v20100122" />

	<title><h:outputText value="#{msgs[title]}" /></title>

	<c:if test="${css!=null}">
		<c:forEach items="${css}" var="item">
			<c:if test="${item.tooltip==null}">
				<link rel="stylesheet" type="text/css"
					href="<c:url value="${item.link}"/>">
			</c:if>
			<c:if test="${item.tooltip=='print'}">
				<link rel="stylesheet" type="text/css" media="print"
					href="<c:url value="${item.link}"/>">
			</c:if>
		</c:forEach>
	</c:if>

	<c:if test="${especificCSS!=null}">
		<c:forEach items="${especificCSS}" var="item">
			<c:if test="${item.tooltip==null}">
				<link rel="stylesheet" type="text/css"
					href="<c:url value="${item.link}"/>">
			</c:if>
			<c:if test="${item.tooltip=='print'}">
				<link rel="stylesheet" type="text/css" media="print"
					href="<c:url value="${item.link}"/>">
			</c:if>
		</c:forEach>
	</c:if>

	<c:if test="${js!=null}">
		<c:forEach items="${js}" var="item">
			<script language="JavaScript" src="<c:url value="${item.link}"/>">
			</script>
		</c:forEach>
	</c:if>

	<c:if test="${especificJS!=null}">
		<c:forEach items="${especificJS}" var="item">
			<script language="JavaScript" src="<c:url value="${item.link}"/>">
			</script>
		</c:forEach>
	</c:if>
	</head>

	<tiles:useAttribute id="onload" name="onload" scope="request" ignore="true" classname="java.lang.String" />
	<body leftmargin="0" topmargin="0" scroll="no" onload="${onload}">

		<b:brPanelGrid id="pageContentLayout" border="0" columns="1"
					cellpadding="0" cellspacing="0" styleClass="tmainpage2"
					columnClasses="cbodyContext" rowClasses="cbodyTitleRow, cbodyRow">
			<f:subview id="pathView">
				<tiles:useAttribute id="selection" name="selection" scope="request" ignore="true" classname="java.lang.String" />
				<b:brPanelGrid cellpadding="0" cellspacing="0" border="0" align="left">
					<b:brOutputText value="#{msgs[selection]}" styleClass="selectionMenu" escape="false" />
				</b:brPanelGrid>
			</f:subview>
			<b:brPanelGroup id="pageBody">
				<tiles:insert attribute="body" flush="false" ignore="true" />
			</b:brPanelGroup>
		</b:brPanelGrid>
	</body>
</f:view>
</html>