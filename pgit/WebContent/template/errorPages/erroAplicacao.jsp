<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://bradesco.com.br/html_custom_components" prefix="br"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/tomahawk" prefix="t"%>
<f:verbatim>
	<br>
	<br>
</f:verbatim>
<br:brPanelGrid columns="1" cellspacing="1" styleClass="tela_erro_default_panelGrid"
	columnClasses="tela_erro_detalhado_panelGroup">
	<br:brPanelGroup>
		<br:brOutputTextBold styleClass="tela_erro_detalhado" escape="false"
			value="#{utilidadeBean.exceptionMessageMsgGeral}"
			title="#{utilidadeBean.exceptionMessageMsgGeral}" />
	</br:brPanelGroup>
</br:brPanelGrid>
<f:verbatim>
	<br>
	<br>
	<br>
</f:verbatim>
<h:form id="form">
	<t:jsValueSet name="texto1" value="#{msgs['erro.link.ver.mais']}" />
	<t:jsValueSet name="texto2" value="#{msgs['erro.link.esconder.mais']}" />
	<br:brPanelGrid columns="1" cellspacing="0" align="center"
		styleClass="tela_erro_link_detalhes_panelGrid">
		<br:brPanelGroup>
			<br:brCommandButton id="voltar" action="nav_index" title="#{msgs.button_back}"
				value="#{msgs.button_back}" styleClass="button" immediate="true"
				rendered="#{utilidadeBean.exceptionMessageHistoryBack == false}" />
			<br:brCommandButton id="voltarHistory" onclick="history.back(); return false;"
				title="#{msgs.button_back}" value="#{msgs.button_back}" styleClass="button" immediate="true"
				type="button" rendered="#{utilidadeBean.exceptionMessageHistoryBack == true}" />
			<br:brOutputText value="&nbsp;&nbsp;" escape="false"
				rendered="#{utilidadeBean.exibirBotaoDetalhe}" />
			<br:brCommandButton id="commandButtonId" value="#{msgs['erro.link.ver.mais']}"
				styleClass="button" immediate="true" type="button"
				rendered="#{utilidadeBean.exibirBotaoDetalhe}" 
				onclick="(document.getElementById('form:panelId1').style.display == 'none')
						? document.getElementById('form:panelId1').style.display=''
						: document.getElementById('form:panelId1').style.display='none';
				 	 	(document.getElementById('form:panelId1').style.display == 'none')
						? this.innerText=texto1 : this.innerText=texto2;
					 	return false" />
		</br:brPanelGroup>
	</br:brPanelGrid>
	<f:verbatim>
		<br>
	</f:verbatim>
	<br:brPanelGrid id="panelId1" columns="1" align="center" styleClass="tela_erro_detalhes_panelGrid"
		style="display:none">
		<br:brOutputText escape="false" styleClass="tela_erro_detalhes"
			value="#{utilidadeBean.exceptionMessageCodErro}"
			rendered="#{utilidadeBean.exceptionMessageCodErro != null}" />
		<br:brOutputText escape="false" styleClass="tela_erro_detalhes"
			value="#{utilidadeBean.exceptionMessageMsgDetalhado}"
			rendered="#{utilidadeBean.exceptionMessageMsgDetalhado != null}" />
		<br:brOutputText escape="false" styleClass="tela_erro_detalhes"
			value="#{utilidadeBean.exceptionMessageCamada}"
			rendered="#{utilidadeBean.exceptionMessageCamada != null}" />
		<br:brOutputText escape="false" styleClass="tela_erro_detalhes"
			value="#{utilidadeBean.exceptionMessageTipo}"
			rendered="#{utilidadeBean.exceptionMessageTipo != null}" />
		<br:brOutputText escape="false" styleClass="tela_erro_detalhes"
			value="#{utilidadeBean.exceptionMessageOrigem}"
			rendered="#{utilidadeBean.exceptionMessageOrigem != null}" />
		<br:brOutputText escape="false" styleClass="tela_erro_detalhes"
			value="#{utilidadeBean.exceptionMessageCausa}"
			rendered="#{utilidadeBean.exceptionMessageCausa != null}" />
	</br:brPanelGrid>
</h:form>
