/*
 * Nome: br.com.bradesco.web.pgit.servlet
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.bradesco.web.aq.application.pdc.session.bean.PDCPaginationBean;
import br.com.bradesco.web.aq.application.pdc.session.manager.IPDCSessionManager;
import br.com.bradesco.web.aq.application.pdc.util.PDCServiceFactory;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.spring.BradescoSpringUtils;
import br.com.bradesco.web.pgit.exception.PgitException;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.impdiaad.request.ImpdiaadRequest;
import br.com.bradesco.web.pgit.service.data.pdc.impdiaad.response.ImpdiaadResponse;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircontratomulticanal.request.ImprimirContratoPgitMulticanalRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimircontratomulticanal.response.ImprimirContratoPgitMulticanalResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: impressaoImpdTF
 * <p>
 * Prop�sito:
 * </p>
 * @author : todo!
 * @version :
 * @see HttpServlet
 */
public class impressaoImpdTF extends HttpServlet {
	
	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public impressaoImpdTF() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		FactoryAdapter factoryAdapter = (FactoryAdapter) BradescoSpringUtils.getApplicationContext().getBean("factoryAdapter");
		
		long contrato = Long.parseLong(request.getParameter("contrato"));
		int  tipoContrato = Integer.parseInt(request.getParameter("tipoContrato"));
		long pessoaJuridica = Long.parseLong(request.getParameter("pessoaJuridica"));
		
		ImprimirContratoPgitMulticanalRequest requestContrato = new ImprimirContratoPgitMulticanalRequest();

		requestContrato.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(pessoaJuridica));
		requestContrato.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(tipoContrato));
		requestContrato.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(contrato));
		requestContrato.setCdVersao(1);
		
		factoryAdapter.getImprimirContratoMulticanalPDCAdapter().invokeProcess(requestContrato);
		List<?> listaProtocolos = BradescoCommonServiceFactory.getSessionManager().getProtocols();
        Long protocolo = new Long(0l);
        if (listaProtocolos != null && !listaProtocolos.isEmpty()) {
            protocolo = Long.valueOf((String)listaProtocolos.get(listaProtocolos.size()-1));
        }
		
	    ImpdiaadRequest requestImpd = new ImpdiaadRequest();

        requestImpd.setIdDocumentoGerado(protocolo);
        requestImpd.setTipoImpressao("O");//TIPO DE IMPRESS�O = O (ONLINE)
        requestImpd.setPaginaDocumento(1); //PAGINA = 1 (DESDE A PAGINA 1)

        try {
        	ImpdiaadResponse responseImpd = factoryAdapter.getImpdiaadPDCAdapter().invokeProcess(requestImpd);

            IPDCSessionManager pdcSessionManager = PDCServiceFactory.getPdcSessionManager();

            PDCPaginationBean paginationBean = pdcSessionManager.getPDCPaginationBean(null);
            byte[] array = new byte[responseImpd.getTamanhoDocumento()];

            List inputStream = paginationBean.getParamMap("inputStreams");
			if(inputStream != null){
				for(Object o: inputStream){
					array = (byte[]) o;
	
				}
			}
			int tamanhoDocto = responseImpd.getTamanhoDocumento();

			String docId = null;
			docId = "DOC" + Math.abs(Math.random() / 100);
		 
			if (array != null && array.length > 0 && tamanhoDocto != 0) {
				ByteArrayOutputStream dadosDocto = new ByteArrayOutputStream();
				dadosDocto.write(array);
				
				// DEVOLVE O ARQUIVO PARA O BROWSER
				download(dadosDocto.toByteArray(), docId + "." + "pdf", response);
			}
			
            
        } catch (IOException ex) {
            throw new PgitException(ex.getMessage(), ex, "");
        } catch (Exception e) {
			response.setContentType("plain/text");   
			ServletOutputStream outputStream = response.getOutputStream();
			outputStream.write("N�O FOI POSS�VEL IMPRIMIR O CONTRATO, TENTE MAIS TARDE".getBytes());
		}
		
		PgitFacesUtils.generateReportResponse("contrato");
		
	}
	
	/**
	 * Download.
	 *
	 * @param arquivo the arquivo
	 * @param nome the nome
	 * @param response the response
	 * @throws Exception the exception
	 */
	public static void download(byte[] arquivo, String nome, HttpServletResponse response) throws Exception {   
		
	    response.setContentType("application/pdf");   
	    response.setContentLength(arquivo.length);   
	    response.addHeader("Content-Disposition", "attachment; filename=" + "\"" + nome + "\"");   
	    ServletOutputStream outputStream = response.getOutputStream();
		    
	    outputStream.write(arquivo, 0, arquivo.length);   
	    outputStream.flush();   
	}
	
	/**
	 * Get: facesContext.
	 *
	 * @return facesContext
	 */
	public static FacesContext getFacesContext() {   
	    return FacesContext.getCurrentInstance();   
	}   

}
