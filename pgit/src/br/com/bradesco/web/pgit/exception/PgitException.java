/*
* =========================================================================
* 
* Client:       Bradesco (BR)
* Project:      Arquitectura Bradesco Canal Web
* Development:  GFT Iberia (http://www.gft.com)
* -------------------------------------------------------------------------
* Revision - Last:
* $Source: /Repositorio/TIMelhorias_AQ/Projetos/PilotoIntranet/JavaSource/br/com/bradesco/web/pgit/exception/PgitException.java,v $
* $Id: PgitException.java,v 1.4 2009/05/13 21:16:38 cpm.com.br\heslei.silva Exp $
* $State: Exp $
* -------------------------------------------------------------------------
* Revision - History:
* $Log: PgitException.java,v $
* Revision 1.4  2009/05/13 21:16:38  cpm.com.br\heslei.silva
* organizacao da version 2 para o head
*
* Revision 1.2  2009/04/30 19:49:12  corporate\edwin.costa
* Alterado o diret�rio de fontes para JavaSource
*
* Revision 1.1  2009/03/16 16:18:55  corporate\marcio.alves
* Adicionando o PilotoIntranet ao CVS
*
*
* =========================================================================
*/

package br.com.bradesco.web.pgit.exception;

import br.com.bradesco.web.aq.application.error.BradescoApplicationException;
import br.com.bradesco.web.aq.application.error.ExceptionConstants;

/**
 * <p><b>Title:</b>        Arquitectura Bradesco Canal Web.</p>
 * <p><b>Description:</b></p>
 * <p>Insertar aqu� la descripci�n del Tipo.</p>
 * 
 * @author       GFT Iberia Solutions / Emagine <BR/>
 * copyright Copyright (c) 2006 <BR/>
 * created   01-ago-2006 <BR/>
 * @version      1.0
 */

public class PgitException extends BradescoApplicationException {

	  /**
     * 
     * Construtor da exce��o.
     * 
     * @param message mensagem geral interna de erro.
     * @param code codigo identificativo da exce��o.
     */
    public PgitException(String message, String code) {
       this(null, message, null, code);
    }
    
    /**
     * 
     * Construtor da exce��o.
     * 
     * @param camada camada a qual pertenece a exce��o.
     * @param message mensagem geral interno de exce��o.
     * @param code codigo identificativo da exce��o.
     */
    public PgitException(String camada, String message, String code) {
        this(camada, message, null, code);
    }
    
    /**
     * 
     * Construtor da exce��o.
     * 
     * @param message mensagem geral interno de erro.
     * @param cause objeto Throwable que origina a apari��o desta exce��o.
     * @param code codigo identificativo da exce��o.
     */
    public PgitException(String message, Throwable cause, String code) {
        this(null, message, cause, code);
    }
    
    /**
     /**
     * 
     * Construtor da exce��o.
     * 
     * @param camada camada na que � reportada a exce��o.
     * @param message mensaje general interno de excepcion.
     * @param cause objeto Throwable que origina a apari��o desta exce��o.
     * @param code codigo identificativo da exce��o.
     */
    public PgitException(String camada, String message, Throwable cause, String code) {
        super(message, cause, code);
        if (camada == null) {
            setCamada(ExceptionConstants.CAMADA_APRESENTACAO_ID);
        } else {
            setCamada(camada);
        }    
        
        //Estabelece que n�o deve gerar tra�ado de log de tipo 'ERRO'.
        setLoggable(false);
    }
    
    /**
     * 
     * Construtor da exce��o.
     * 
     * @param message mensagem geral interno de erro.
     * @param code codigo identificativo da exce��o.
     * @param histBack booleano para voltar atr�s
     */
    public PgitException(String message, String code, boolean histBack) {
       this(null, message, null, code, histBack);
    }
    
    /**
     * 
     * Construtor da exce��o.
     * 
     * @param camada camada a qual a exce��o pertence.
     * @param message mensagem geral interno de exce��o.
     * @param code codigo identificativo da exce��o.
     * @param histBack booleano para voltar atr�s
     */
    public PgitException(String camada, String message, String code, boolean histBack) {
        this(camada, message, null, code, histBack);
    }
    
    /**
     * 
     * Construtor da exce��o.
     * 
     * @param message mensagem geral interno de erro.
     * @param cause objeto Throwable que origina a apari��o desta exce��o.
     * @param code codigo identificativo da exce��o.
     * @param histBack booleano para voltar atr�s
     */
    public PgitException(String message, Throwable cause, String code, boolean histBack) {
        this(null, message, cause, code, histBack);
    }
    
     /**
     * 
     * Construtor da exce��o.
     * 
     * @param camada camada na que � reportada a exce��o.
     * @param message mensagem geral interno de exce��o.
     * @param cause objeto Throwable que origina a apari��o desta exce��o.
     * @param code codigo identificativo da exce��o.
     * @param histBack booleano para voltar atr�s
     */
    public PgitException(String camada, String message, Throwable cause, String code, boolean histBack) {
        super(message, cause, code);
        if (camada == null) {
            setCamada(ExceptionConstants.CAMADA_APRESENTACAO_ID);
        } else {
            setCamada(camada);
        }    

        setHistoryBack(histBack);

        //Estabelece que n�o deve gerar tra�ado de log de tipo 'ERROR'.
        setLoggable(false);
    }

}
