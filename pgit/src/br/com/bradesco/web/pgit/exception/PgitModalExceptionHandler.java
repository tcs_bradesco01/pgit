/*
 * Nome: br.com.bradesco.web.pgit.exception
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.exception;

import br.com.bradesco.web.aq.application.error.BradescoBaseException;
import br.com.bradesco.web.aq.application.error.config.IExceptionConfig;
import br.com.bradesco.web.aq.application.error.handler.BradescoApplicationExceptionHandler;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;

/**
 * Nome: PgitModalExceptionHandler
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class PgitModalExceptionHandler extends BradescoApplicationExceptionHandler {

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.aq.application.error.handler.DefaultExceptionHandler#beforePerformForward(java.lang.Exception, br.com.bradesco.web.aq.application.error.config.IExceptionConfig)
	 */
	@Override
	protected boolean beforePerformForward(Exception exception, IExceptionConfig exceptionConfig) {
		return true;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.aq.application.error.handler.AbstractBaseExceptionHandlerImpl#performForward(java.lang.Exception, br.com.bradesco.web.aq.application.error.config.IExceptionConfig)
	 */
	@Override
	protected void performForward(Exception exception, IExceptionConfig exceptionConfig) {
		if (exception instanceof BradescoBaseException) {
			String message = "(" + getMessageCode((BradescoBaseException) exception) + ") " + exception.getMessage();
			BradescoFacesUtils.addInfoModalMessage(message, false);
		} else {
			BradescoFacesUtils.addInfoModalMessage(exception.getMessage(), false);
		}
	}

	/**
	 * Get: messageCode.
	 *
	 * @param exception the exception
	 * @return messageCode
	 */
	private String getMessageCode(BradescoBaseException exception) {
		if (exception.getCode() != null && !exception.getCode().trim().equals("")) {
			int lastIndex = exception.getCode().lastIndexOf(".");
			if (lastIndex >= 0 && (lastIndex + 1) < exception.getCode().length()) {
				return exception.getCode().substring(lastIndex + 1);
			}
		}
		return "";
	}
}
