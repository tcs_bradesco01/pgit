/*
* =========================================================================
* 
* Client:       Bradesco (BR)
* Project:      Arquitectura Bradesco Canal Web
* Development:  GFT Iberia (http://www.gft.com)
* -------------------------------------------------------------------------
* Revision - Last:
* $Source: /Repositorio/TIMelhorias_AQ/Projetos/PilotoIntranet/JavaSource/br/com/bradesco/web/pgit/exception/PgitExceptionHandler.java,v $
* $Id: PgitExceptionHandler.java,v 1.4 2009/05/13 21:16:38 cpm.com.br\heslei.silva Exp $
* $State: Exp $
* -------------------------------------------------------------------------
* Revision - History:
* $Log: PgitExceptionHandler.java,v $
* Revision 1.4  2009/05/13 21:16:38  cpm.com.br\heslei.silva
* organizacao da version 2 para o head
*
* Revision 1.2  2009/04/30 19:49:12  corporate\edwin.costa
* Alterado o diret�rio de fontes para JavaSource
*
* Revision 1.1  2009/03/16 16:18:55  corporate\marcio.alves
* Adicionando o PilotoIntranet ao CVS
*
*
* =========================================================================
*/

package br.com.bradesco.web.pgit.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import br.com.bradesco.web.aq.application.error.BradescoBaseException;
import br.com.bradesco.web.aq.application.error.config.IExceptionConfig;
import br.com.bradesco.web.aq.application.error.handler.BradescoApplicationExceptionHandler;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;

/**
 * <p><b>Title:</b>        Arquitectura Bradesco Canal Web.</p>
 * <p><b>Description:</b></p>
 * <p>Insertar aqu� la descripci�n del Tipo.</p>
 * 
 * @author       GFT Iberia Solutions / Emagine <BR/>
 * copyright Copyright (c) 2006 <BR/>
 * created   01-ago-2006 <BR/>
 * @version      1.0
 */

public class PgitExceptionHandler extends BradescoApplicationExceptionHandler {


    /** Atributo MENSAGEM_CAUSE_ERRO_LABEL_KEY. */
    public static final String MENSAGEM_CAUSE_ERRO_LABEL_KEY = "erro.mensagem.cause.label";

    /** Atributo CODIGO_ERRO_LABEL_KEY. */
    public static final String CODIGO_ERRO_LABEL_KEY         = "erro.codigo.label";

    /** Atributo MENSAGEM_GERAL_LABEL_KEY. */
    public static final String MENSAGEM_GERAL_LABEL_KEY      = "erro.mensagem.geral.label";

    /** Atributo MENSAGEM_DETALHADO_LABEL_KEY. */
    public static final String MENSAGEM_DETALHADO_LABEL_KEY  = "erro.mensagem.detalhado.label";

    /** Atributo REDIRECT_MESSAGE_LABEL_KEY. */
    public static final String REDIRECT_MESSAGE_LABEL_KEY    = "erro.mensagem.redirect.label";

    /** Atributo TIPO_ERRO_LABEL_KEY. */
    public static final String TIPO_ERRO_LABEL_KEY           = "erro.tipo.label";

    /** Atributo STACK_ERRO_LABEL_KEY. */
    public static final String STACK_ERRO_LABEL_KEY          = "erro.stack.label";

    /** Atributo CAMADA_ERRO_LABEL_KEY. */
    public static final String CAMADA_ERRO_LABEL_KEY         = "erro.camada.label";

	
    /**
     * Nome do atributo na request onde ser� armazenada a exe��o acontecida.
     */
    public static final String NOME_ATRIBUTO_REQ_ACAO_PGIT_EXCECAO = "webFlowExcecao";

    /**
     * Coment�rios para o Construtor.
     * 
     */
    public PgitExceptionHandler() {
        super();
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.aq.application.error.handler.DefaultExceptionHandler#beforePerformForward(java.lang.Exception, br.com.bradesco.web.aq.application.error.config.IExceptionConfig)
     */
    @Override
	protected boolean beforePerformForward(Exception config, IExceptionConfig aException) {
		if (config instanceof BradescoBaseException) {
			addErrorMessageCode(((BradescoBaseException) config).getCode());
		}

		StringWriter stackTrace = new StringWriter();
		config.printStackTrace(new PrintWriter(stackTrace));
		BradescoFacesUtils.addGlobalErrorFacesMessage(config.getMessage(), stackTrace.toString());
        return true;
	}

	/**
	 * Add error message code.
	 *
	 * @param messageCode the message code
	 */
	private void addErrorMessageCode(String messageCode) {
		FacesContext.getCurrentInstance().addMessage("messageCode", new FacesMessage(FacesMessage.SEVERITY_ERROR, messageCode, messageCode));
	}

}
