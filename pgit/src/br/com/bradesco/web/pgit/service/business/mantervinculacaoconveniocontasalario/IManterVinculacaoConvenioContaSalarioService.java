/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.AlterarVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.AlterarVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.FaixasRenda;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.FolhaPagamentoEntradaDto;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.FolhaPagamentoSaidaDto;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.IncluirVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.IncluirVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosConvenioContaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosConvenioContaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarVincConvnCtaSalarioSaidaDTO;

/**
 * Nome: IManterVinculacaoConvenioContaSalarioService
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface IManterVinculacaoConvenioContaSalarioService {

	/**
	 * Listar dados conta convenio.
	 *
	 * @param entrada the entrada
	 * @return the list< listar dados cta convn lista ocorrencias dt o>
	 */
	ListarDadosCtaConvnSaidaDTO listarDadosContaConvenio(ListarDadosCtaConvnEntradaDTO entrada);
	
	/**
	 * Incluir vinc convn cta salario.
	 *
	 * @param entrada the entrada
	 * @return the incluir vinc convn cta salario saida dto
	 */
	IncluirVincConvnCtaSalarioSaidaDTO incluirVincConvnCtaSalario(IncluirVincConvnCtaSalarioEntradaDTO entrada);
	
	/**
	 * Detalhar vinc convn cta salario.
	 *
	 * @param entrada the entrada
	 * @return the detalhar vinc convn cta salario saida dto
	 */
	DetalharVincConvnCtaSalarioSaidaDTO detalharVincConvnCtaSalario(DetalharVincConvnCtaSalarioEntradaDTO entrada);
	
	/**
	 * Alterar vinc convn cta salario.
	 *
	 * @param entrada the entrada
	 * @return the alterar vinc convn cta salario saida dto
	 */
	AlterarVincConvnCtaSalarioSaidaDTO alterarVincConvnCtaSalario(AlterarVincConvnCtaSalarioEntradaDTO entrada);
	
	/**
	 * Excluir vinc convn cta salario.
	 *
	 * @param entrada the entrada
	 * @return the excluir vinc convn cta salario saida dto
	 */
	ExcluirVincConvnCtaSalarioSaidaDTO excluirVincConvnCtaSalario(ExcluirVincConvnCtaSalarioEntradaDTO entrada);

	/**
	 * Detalhar folha pagamento.
	 *
	 * @param entrada the entrada
	 * @return the folha pagamento saida dto
	 */
	FolhaPagamentoSaidaDto detalharFolhaPagamento(FolhaPagamentoEntradaDto entrada);

	/**
     * Detalhar piramide salarial.
     * 
     * @param entrada
     *            the entrada
     * @return the detalhar piramide saida dto
     */
	DetalharPiramideSaidaDTO detalharPiramideSalarial(DetalharPiramideEntradaDTO entrada);
	
	/**
     * Listar faixas salariais.
     *
     * @return the list
     */
    List<FaixasRenda> listarFaixasSalariais();
    
    /**
     * Listar Dados do Convenio da Conta Salario.
     *
     * @return the list
     */
    ListarDadosConvenioContaSalarioSaidaDTO listarDadosConvenioContaSalario(ListarDadosConvenioContaSalarioEntradaDTO entrada);
    /**
     * Listar Dados do Convenio da Conta Salario - Novo Fluxo.
     *
     * @return the list
     */
    ListarVincConvnCtaSalarioSaidaDTO listarConvenioContaSalarioNovo(ListarVincConvnCtaSalarioEntradaDTO entrada);
}