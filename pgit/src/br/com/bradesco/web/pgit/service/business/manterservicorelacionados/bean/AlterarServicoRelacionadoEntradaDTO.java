package br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean;

public class AlterarServicoRelacionadoEntradaDTO {

	private Integer cdProdutoServicoOperacao;
	private Integer cdProdutoOperacaoRelacionado;
	private Integer cdRelacionamentoProduto;
	private Long cdServicoCompostoPagamento;
	private Integer cdNaturezaOperacaoPagamento;
	private Integer nrOrdenacaoServicoRelacionado;
	private Integer cdIndicadorServicoNegociavel;
	private Integer cdIndicadorNegociavelServico;
	private Integer cdIndicadorContigenciaServico;
	private Integer cdIndicadorManutencaoServico;

	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	public void setCdProdutoOperacaoRelacionado(
			Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}

	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}

	public Long getCdServicoCompostoPagamento() {
		return cdServicoCompostoPagamento;
	}

	public void setCdServicoCompostoPagamento(Long cdServicoCompostoPagamento) {
		this.cdServicoCompostoPagamento = cdServicoCompostoPagamento;
	}

	public Integer getCdNaturezaOperacaoPagamento() {
		return cdNaturezaOperacaoPagamento;
	}

	public void setCdNaturezaOperacaoPagamento(
			Integer cdNaturezaOperacaoPagamento) {
		this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
	}

	public Integer getNrOrdenacaoServicoRelacionado() {
		return nrOrdenacaoServicoRelacionado;
	}

	public void setNrOrdenacaoServicoRelacionado(
			Integer nrOrdenacaoServicoRelacionado) {
		this.nrOrdenacaoServicoRelacionado = nrOrdenacaoServicoRelacionado;
	}

	public Integer getCdIndicadorServicoNegociavel() {
		return cdIndicadorServicoNegociavel;
	}

	public void setCdIndicadorServicoNegociavel(
			Integer cdIndicadorServicoNegociavel) {
		this.cdIndicadorServicoNegociavel = cdIndicadorServicoNegociavel;
	}

	public Integer getCdIndicadorNegociavelServico() {
		return cdIndicadorNegociavelServico;
	}

	public void setCdIndicadorNegociavelServico(
			Integer cdIndicadorNegociavelServico) {
		this.cdIndicadorNegociavelServico = cdIndicadorNegociavelServico;
	}

	public Integer getCdIndicadorContigenciaServico() {
		return cdIndicadorContigenciaServico;
	}

	public void setCdIndicadorContigenciaServico(
			Integer cdIndicadorContigenciaServico) {
		this.cdIndicadorContigenciaServico = cdIndicadorContigenciaServico;
	}

	public Integer getCdIndicadorManutencaoServico() {
		return cdIndicadorManutencaoServico;
	}

	public void setCdIndicadorManutencaoServico(
			Integer cdIndicadorManutencaoServico) {
		this.cdIndicadorManutencaoServico = cdIndicadorManutencaoServico;
	}

	public boolean isIndicadorServicoNegociavel() {
		return getCdIndicadorServicoNegociavel().compareTo(1) == 0;
	}

}
