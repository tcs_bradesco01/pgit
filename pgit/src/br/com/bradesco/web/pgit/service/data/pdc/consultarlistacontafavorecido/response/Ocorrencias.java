/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontafavorecido.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrenciaContaFavorecido
     */
    private int _nrOcorrenciaContaFavorecido = 0;

    /**
     * keeps track of state for field: _nrOcorrenciaContaFavorecido
     */
    private boolean _has_nrOcorrenciaContaFavorecido;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _nrContaFavorecido
     */
    private long _nrContaFavorecido = 0;

    /**
     * keeps track of state for field: _nrContaFavorecido
     */
    private boolean _has_nrContaFavorecido;

    /**
     * Field _nrDigitoConta
     */
    private java.lang.String _nrDigitoConta;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _dsCdTipoConta
     */
    private java.lang.String _dsCdTipoConta;

    /**
     * Field _dsSituacaoConta
     */
    private java.lang.String _dsSituacaoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontafavorecido.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteNrContaFavorecido
     * 
     */
    public void deleteNrContaFavorecido()
    {
        this._has_nrContaFavorecido= false;
    } //-- void deleteNrContaFavorecido() 

    /**
     * Method deleteNrOcorrenciaContaFavorecido
     * 
     */
    public void deleteNrOcorrenciaContaFavorecido()
    {
        this._has_nrOcorrenciaContaFavorecido= false;
    } //-- void deleteNrOcorrenciaContaFavorecido() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsCdTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsCdTipoConta'.
     */
    public java.lang.String getDsCdTipoConta()
    {
        return this._dsCdTipoConta;
    } //-- java.lang.String getDsCdTipoConta() 

    /**
     * Returns the value of field 'dsSituacaoConta'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoConta'.
     */
    public java.lang.String getDsSituacaoConta()
    {
        return this._dsSituacaoConta;
    } //-- java.lang.String getDsSituacaoConta() 

    /**
     * Returns the value of field 'nrContaFavorecido'.
     * 
     * @return long
     * @return the value of field 'nrContaFavorecido'.
     */
    public long getNrContaFavorecido()
    {
        return this._nrContaFavorecido;
    } //-- long getNrContaFavorecido() 

    /**
     * Returns the value of field 'nrDigitoConta'.
     * 
     * @return String
     * @return the value of field 'nrDigitoConta'.
     */
    public java.lang.String getNrDigitoConta()
    {
        return this._nrDigitoConta;
    } //-- java.lang.String getNrDigitoConta() 

    /**
     * Returns the value of field 'nrOcorrenciaContaFavorecido'.
     * 
     * @return int
     * @return the value of field 'nrOcorrenciaContaFavorecido'.
     */
    public int getNrOcorrenciaContaFavorecido()
    {
        return this._nrOcorrenciaContaFavorecido;
    } //-- int getNrOcorrenciaContaFavorecido() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasNrContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrContaFavorecido()
    {
        return this._has_nrContaFavorecido;
    } //-- boolean hasNrContaFavorecido() 

    /**
     * Method hasNrOcorrenciaContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrenciaContaFavorecido()
    {
        return this._has_nrOcorrenciaContaFavorecido;
    } //-- boolean hasNrOcorrenciaContaFavorecido() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsCdTipoConta'.
     * 
     * @param dsCdTipoConta the value of field 'dsCdTipoConta'.
     */
    public void setDsCdTipoConta(java.lang.String dsCdTipoConta)
    {
        this._dsCdTipoConta = dsCdTipoConta;
    } //-- void setDsCdTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoConta'.
     * 
     * @param dsSituacaoConta the value of field 'dsSituacaoConta'.
     */
    public void setDsSituacaoConta(java.lang.String dsSituacaoConta)
    {
        this._dsSituacaoConta = dsSituacaoConta;
    } //-- void setDsSituacaoConta(java.lang.String) 

    /**
     * Sets the value of field 'nrContaFavorecido'.
     * 
     * @param nrContaFavorecido the value of field
     * 'nrContaFavorecido'.
     */
    public void setNrContaFavorecido(long nrContaFavorecido)
    {
        this._nrContaFavorecido = nrContaFavorecido;
        this._has_nrContaFavorecido = true;
    } //-- void setNrContaFavorecido(long) 

    /**
     * Sets the value of field 'nrDigitoConta'.
     * 
     * @param nrDigitoConta the value of field 'nrDigitoConta'.
     */
    public void setNrDigitoConta(java.lang.String nrDigitoConta)
    {
        this._nrDigitoConta = nrDigitoConta;
    } //-- void setNrDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrenciaContaFavorecido'.
     * 
     * @param nrOcorrenciaContaFavorecido the value of field
     * 'nrOcorrenciaContaFavorecido'.
     */
    public void setNrOcorrenciaContaFavorecido(int nrOcorrenciaContaFavorecido)
    {
        this._nrOcorrenciaContaFavorecido = nrOcorrenciaContaFavorecido;
        this._has_nrOcorrenciaContaFavorecido = true;
    } //-- void setNrOcorrenciaContaFavorecido(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontafavorecido.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontafavorecido.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontafavorecido.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontafavorecido.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
