/*
 * Nome: br.com.bradesco.web.pgit.service.report.base
 * 
 * Compilador:
 * 
 * Propósito:
 * 
 * Data da criação: 17/09/2014
 * 
 * Parâmetros de Compilação:
 */
package br.com.bradesco.web.pgit.service.report.base;

import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Implementação de evento para desenhar linha tracejada.
 */
public class PdfPCellDashedLineEvent implements PdfPCellEvent {

	/**
	 * Realiza o desenho da linha tracejada.
	 *
	 * @see com.lowagie.text.pdf.PdfPCellEvent#cellLayout(com.lowagie.text.pdf.PdfPCell, com.lowagie.text.Rectangle, com.lowagie.text.pdf.PdfContentByte[])
	 */
    public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {
            PdfContentByte cb = canvases[PdfPTable.LINECANVAS];
            cb.setLineCap(PdfContentByte.LINE_CAP_PROJECTING_SQUARE);
            cb.setLineDash(6, 0);
            cb.moveTo(position.left(), position.bottom());
            cb.lineTo(position.right(), position.bottom());
            cb.stroke();
    }
}
