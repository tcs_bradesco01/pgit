/*
 * Nome: br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean;

/**
 * Nome: ListarParticipantesOrgaoOcorrencias
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarParticipantesOrgaoOcorrencias {
	
	   /** Atributo cdpessoaJuridicaContrato. */
   	private Long cdpessoaJuridicaContrato;
	   
   	/** Atributo cdTipoContratoNegocio. */
   	private Integer cdTipoContratoNegocio;
	   
   	/** Atributo cdPessoaJuridicaVinculo. */
   	private Long cdPessoaJuridicaVinculo;
	   
   	/** Atributo cdTipoContratoVinculo. */
   	private Integer cdTipoContratoVinculo;
	   
   	/** Atributo nrSequenciaContratoVinculo. */
   	private Long nrSequenciaContratoVinculo;
	   
   	/** Atributo cdTipoVincContrato. */
   	private Integer cdTipoVincContrato;
	   
   	/** Atributo cdPessoa. */
   	private Long cdPessoa;
	   
   	/** Atributo cdSituacaoRegistro. */
   	private String cdSituacaoRegistro;
	   
   	/** Atributo cdCorpoCpfCnpj. */
   	private Long cdCorpoCpfCnpj;
	   
   	/** Atributo cdFilialCnpj. */
   	private Integer cdFilialCnpj;
	   
   	/** Atributo cdControleCPF. */
   	private Integer cdControleCPF;
	   
   	/** Atributo dsNomeAgenciaDebito. */
   	private String dsNomeAgenciaDebito;
	   
   	/** Atributo cdContaDebito. */
   	private Long cdContaDebito;
	   
   	/** Atributo cdDigitoContaDebito. */
   	private String cdDigitoContaDebito;
	   
   	/** Atributo codAgenciaDesc. */
   	private String codAgenciaDesc;
	   
   	/** Atributo nrSequenciaContratoNegocio. */
   	private Long nrSequenciaContratoNegocio;
	   
   	/** Atributo dsNomeRazaoSocial. */
   	private String dsNomeRazaoSocial;
	   
   	/** Atributo cpfCnpjFormatado. */
   	private String cpfCnpjFormatado;
	   
   	/** Atributo cdAgenciaDebito. */
   	private Integer cdAgenciaDebito;
	   
   	/** Atributo contaDigito. */
   	private String contaDigito;
	   
   	/** Atributo dsSituacaoRegistro. */
   	private String dsSituacaoRegistro;
	   
		/**
		 * Get: cdpessoaJuridicaContrato.
		 *
		 * @return cdpessoaJuridicaContrato
		 */
		public Long getCdpessoaJuridicaContrato() {
			return cdpessoaJuridicaContrato;
		}
		
		/**
		 * Set: cdpessoaJuridicaContrato.
		 *
		 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
		 */
		public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
			this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
		}
		
		/**
		 * Get: cdTipoContratoNegocio.
		 *
		 * @return cdTipoContratoNegocio
		 */
		public Integer getCdTipoContratoNegocio() {
			return cdTipoContratoNegocio;
		}
		
		/**
		 * Set: cdTipoContratoNegocio.
		 *
		 * @param cdTipoContratoNegocio the cd tipo contrato negocio
		 */
		public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
			this.cdTipoContratoNegocio = cdTipoContratoNegocio;
		}
		
		/**
		 * Get: cdPessoaJuridicaVinculo.
		 *
		 * @return cdPessoaJuridicaVinculo
		 */
		public Long getCdPessoaJuridicaVinculo() {
			return cdPessoaJuridicaVinculo;
		}
		
		/**
		 * Set: cdPessoaJuridicaVinculo.
		 *
		 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
		 */
		public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
			this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
		}
		
		/**
		 * Get: cdTipoContratoVinculo.
		 *
		 * @return cdTipoContratoVinculo
		 */
		public Integer getCdTipoContratoVinculo() {
			return cdTipoContratoVinculo;
		}
		
		/**
		 * Set: cdTipoContratoVinculo.
		 *
		 * @param cdTipoContratoVinculo the cd tipo contrato vinculo
		 */
		public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
			this.cdTipoContratoVinculo = cdTipoContratoVinculo;
		}
		
		/**
		 * Get: nrSequenciaContratoVinculo.
		 *
		 * @return nrSequenciaContratoVinculo
		 */
		public Long getNrSequenciaContratoVinculo() {
			return nrSequenciaContratoVinculo;
		}
		
		/**
		 * Set: nrSequenciaContratoVinculo.
		 *
		 * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
		 */
		public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
			this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
		}
		
		/**
		 * Get: cdTipoVincContrato.
		 *
		 * @return cdTipoVincContrato
		 */
		public Integer getCdTipoVincContrato() {
			return cdTipoVincContrato;
		}
		
		/**
		 * Set: cdTipoVincContrato.
		 *
		 * @param cdTipoVincContrato the cd tipo vinc contrato
		 */
		public void setCdTipoVincContrato(Integer cdTipoVincContrato) {
			this.cdTipoVincContrato = cdTipoVincContrato;
		}
		
		/**
		 * Get: cdPessoa.
		 *
		 * @return cdPessoa
		 */
		public Long getCdPessoa() {
			return cdPessoa;
		}
		
		/**
		 * Set: cdPessoa.
		 *
		 * @param cdPessoa the cd pessoa
		 */
		public void setCdPessoa(Long cdPessoa) {
			this.cdPessoa = cdPessoa;
		}
		
		/**
		 * Get: cdSituacaoRegistro.
		 *
		 * @return cdSituacaoRegistro
		 */
		public String getCdSituacaoRegistro() {
			return cdSituacaoRegistro;
		}
		
		/**
		 * Set: cdSituacaoRegistro.
		 *
		 * @param cdSituacaoRegistro the cd situacao registro
		 */
		public void setCdSituacaoRegistro(String cdSituacaoRegistro) {
			this.cdSituacaoRegistro = cdSituacaoRegistro;
		}
		
		/**
		 * Get: cdCorpoCpfCnpj.
		 *
		 * @return cdCorpoCpfCnpj
		 */
		public Long getCdCorpoCpfCnpj() {
			return cdCorpoCpfCnpj;
		}
		
		/**
		 * Set: cdCorpoCpfCnpj.
		 *
		 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
		 */
		public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
			this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
		}
		
		/**
		 * Get: cdFilialCnpj.
		 *
		 * @return cdFilialCnpj
		 */
		public Integer getCdFilialCnpj() {
			return cdFilialCnpj;
		}
		
		/**
		 * Set: cdFilialCnpj.
		 *
		 * @param cdFilialCnpj the cd filial cnpj
		 */
		public void setCdFilialCnpj(Integer cdFilialCnpj) {
			this.cdFilialCnpj = cdFilialCnpj;
		}
		
		/**
		 * Get: cdControleCPF.
		 *
		 * @return cdControleCPF
		 */
		public Integer getCdControleCPF() {
			return cdControleCPF;
		}
		
		/**
		 * Set: cdControleCPF.
		 *
		 * @param cdControleCPF the cd controle cpf
		 */
		public void setCdControleCPF(Integer cdControleCPF) {
			this.cdControleCPF = cdControleCPF;
		}
		
		/**
		 * Get: dsNomeAgenciaDebito.
		 *
		 * @return dsNomeAgenciaDebito
		 */
		public String getDsNomeAgenciaDebito() {
			return dsNomeAgenciaDebito;
		}
		
		/**
		 * Set: dsNomeAgenciaDebito.
		 *
		 * @param dsNomeAgenciaDebito the ds nome agencia debito
		 */
		public void setDsNomeAgenciaDebito(String dsNomeAgenciaDebito) {
			this.dsNomeAgenciaDebito = dsNomeAgenciaDebito;
		}
		
		/**
		 * Get: cdContaDebito.
		 *
		 * @return cdContaDebito
		 */
		public Long getCdContaDebito() {
			return cdContaDebito;
		}
		
		/**
		 * Set: cdContaDebito.
		 *
		 * @param cdContaDebito the cd conta debito
		 */
		public void setCdContaDebito(Long cdContaDebito) {
			this.cdContaDebito = cdContaDebito;
		}
		
		/**
		 * Get: cdDigitoContaDebito.
		 *
		 * @return cdDigitoContaDebito
		 */
		public String getCdDigitoContaDebito() {
			return cdDigitoContaDebito;
		}
		
		/**
		 * Set: cdDigitoContaDebito.
		 *
		 * @param cdDigitoContaDebito the cd digito conta debito
		 */
		public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
			this.cdDigitoContaDebito = cdDigitoContaDebito;
		}
		
		/**
		 * Get: codAgenciaDesc.
		 *
		 * @return codAgenciaDesc
		 */
		public String getCodAgenciaDesc() {
			return codAgenciaDesc;
		}
		
		/**
		 * Set: codAgenciaDesc.
		 *
		 * @param codAgenciaDesc the cod agencia desc
		 */
		public void setCodAgenciaDesc(String codAgenciaDesc) {
			this.codAgenciaDesc = codAgenciaDesc;
		}
		
		/**
		 * Get: nrSequenciaContratoNegocio.
		 *
		 * @return nrSequenciaContratoNegocio
		 */
		public Long getNrSequenciaContratoNegocio() {
			return nrSequenciaContratoNegocio;
		}
		
		/**
		 * Set: nrSequenciaContratoNegocio.
		 *
		 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
		 */
		public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
			this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
		}
		
		/**
		 * Get: dsNomeRazaoSocial.
		 *
		 * @return dsNomeRazaoSocial
		 */
		public String getDsNomeRazaoSocial() {
			return dsNomeRazaoSocial;
		}
		
		/**
		 * Set: dsNomeRazaoSocial.
		 *
		 * @param dsNomeRazaoSocial the ds nome razao social
		 */
		public void setDsNomeRazaoSocial(String dsNomeRazaoSocial) {
			this.dsNomeRazaoSocial = dsNomeRazaoSocial;
		}
		
		/**
		 * Get: cpfCnpjFormatado.
		 *
		 * @return cpfCnpjFormatado
		 */
		public String getCpfCnpjFormatado() {
			return cpfCnpjFormatado;
		}
		
		/**
		 * Set: cpfCnpjFormatado.
		 *
		 * @param cpfCnpjFormatado the cpf cnpj formatado
		 */
		public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
			this.cpfCnpjFormatado = cpfCnpjFormatado;
		}
		
		/**
		 * Get: cdAgenciaDebito.
		 *
		 * @return cdAgenciaDebito
		 */
		public Integer getCdAgenciaDebito() {
			return cdAgenciaDebito;
		}
		
		/**
		 * Set: cdAgenciaDebito.
		 *
		 * @param cdAgenciaDebito the cd agencia debito
		 */
		public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
			this.cdAgenciaDebito = cdAgenciaDebito;
		}
		
		/**
		 * Get: contaDigito.
		 *
		 * @return contaDigito
		 */
		public String getContaDigito() {
			return contaDigito;
		}
		
		/**
		 * Set: contaDigito.
		 *
		 * @param contaDigito the conta digito
		 */
		public void setContaDigito(String contaDigito) {
			this.contaDigito = contaDigito;
		}
		
		/**
		 * Get: dsSituacaoRegistro.
		 *
		 * @return dsSituacaoRegistro
		 */
		public String getDsSituacaoRegistro() {
			return dsSituacaoRegistro;
		}
		
		/**
		 * Set: dsSituacaoRegistro.
		 *
		 * @param dsSituacaoRegistro the ds situacao registro
		 */
		public void setDsSituacaoRegistro(String dsSituacaoRegistro) {
			this.dsSituacaoRegistro = dsSituacaoRegistro;
		}

}
