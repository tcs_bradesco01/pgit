/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.DetalharSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.DetalharSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ExcluirSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ExcluirSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.IncluirSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.IncluirSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArqRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArqRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArquivoRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArquivoRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarSolBaseRetransmissaoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterSolicRetransmissaoArquivosRetorno
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterSolicRetransmissaoArquivosRetornoService {

    /**
     * Listar sol base retransmissao.
     *
     * @param entradaDTO the entrada dto
     * @return the list< listar sol base retransmissao saida dt o>
     */
    List<ListarSolBaseRetransmissaoSaidaDTO> listarSolBaseRetransmissao (ListarSolBaseRetransmissaoEntradaDTO entradaDTO);
    
    /**
     * Detalhar sol base retransmissao.
     *
     * @param entradaDTO the entrada dto
     * @return the detalhar sol base retransmissao saida dto
     */
    DetalharSolBaseRetransmissaoSaidaDTO detalharSolBaseRetransmissao(DetalharSolBaseRetransmissaoEntradaDTO entradaDTO);
    
    /**
     * Excluir sol base retransmissao.
     *
     * @param entradaDTO the entrada dto
     * @return the excluir sol base retransmissao saida dto
     */
    ExcluirSolBaseRetransmissaoSaidaDTO excluirSolBaseRetransmissao(ExcluirSolBaseRetransmissaoEntradaDTO entradaDTO);
    
    /**
     * Listar arquivo retorno.
     *
     * @param entradaDTO the entrada dto
     * @return the list< listar arquivo retorno saida dt o>
     */
    List<ListarArquivoRetornoSaidaDTO> listarArquivoRetorno (ListarArquivoRetornoEntradaDTO entradaDTO);
    
    /**
     * Incluir sol base retransmissao.
     *
     * @param entradaDTO the entrada dto
     * @return the incluir sol base retransmissao saida dto
     */
    IncluirSolBaseRetransmissaoSaidaDTO incluirSolBaseRetransmissao(IncluirSolBaseRetransmissaoEntradaDTO entradaDTO);
    
    /**
     * Listar arquivo retransmissao.
     *
     * @param entradaDTO the entrada dto
     * @return the list< listar arq retransmissao saida dt o>
     */
    List<ListarArqRetransmissaoSaidaDTO> listarArquivoRetransmissao(ListarArqRetransmissaoEntradaDTO entradaDTO);
}

