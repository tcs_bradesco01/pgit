/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias10.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias10 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsFtiTipoTitulo
     */
    private java.lang.String _dsFtiTipoTitulo;

    /**
     * Field _dsFtiTipoConsultaSaldo
     */
    private java.lang.String _dsFtiTipoConsultaSaldo;

    /**
     * Field _dsFtiTipoProcessamento
     */
    private java.lang.String _dsFtiTipoProcessamento;

    /**
     * Field _dsFtiTipoTratamentoFeriado
     */
    private java.lang.String _dsFtiTipoTratamentoFeriado;

    /**
     * Field _dsFtiTipoRejeicaoEfetivacao
     */
    private java.lang.String _dsFtiTipoRejeicaoEfetivacao;

    /**
     * Field _dsFtiTipoRejeicaoAgendamento
     */
    private java.lang.String _dsFtiTipoRejeicaoAgendamento;

    /**
     * Field _cdFtiIndicadorPercentualMaximo
     */
    private java.lang.String _cdFtiIndicadorPercentualMaximo;

    /**
     * Field _pcFtiMaximoInconsistente
     */
    private int _pcFtiMaximoInconsistente = 0;

    /**
     * keeps track of state for field: _pcFtiMaximoInconsistente
     */
    private boolean _has_pcFtiMaximoInconsistente;

    /**
     * Field _cdFtiIndicadorQuantidadeMaxima
     */
    private java.lang.String _cdFtiIndicadorQuantidadeMaxima;

    /**
     * Field _qtFtiMaximaInconsistencia
     */
    private int _qtFtiMaximaInconsistencia = 0;

    /**
     * keeps track of state for field: _qtFtiMaximaInconsistencia
     */
    private boolean _has_qtFtiMaximaInconsistencia;

    /**
     * Field _cdFtiIndicadorValorMaximo
     */
    private java.lang.String _cdFtiIndicadorValorMaximo;

    /**
     * Field _vlFtiMaximoFavorecidoNao
     */
    private java.math.BigDecimal _vlFtiMaximoFavorecidoNao = new java.math.BigDecimal("0");

    /**
     * Field _dsFtiPrioridadeDebito
     */
    private java.lang.String _dsFtiPrioridadeDebito;

    /**
     * Field _cdFtiIndicadorValorDia
     */
    private java.lang.String _cdFtiIndicadorValorDia;

    /**
     * Field _vlFtiLimiteDiario
     */
    private java.math.BigDecimal _vlFtiLimiteDiario = new java.math.BigDecimal("0");

    /**
     * Field _cdFtiIndicadorValorIndividual
     */
    private java.lang.String _cdFtiIndicadorValorIndividual;

    /**
     * Field _vlFtiLimiteIndividual
     */
    private java.math.BigDecimal _vlFtiLimiteIndividual = new java.math.BigDecimal("0");

    /**
     * Field _cdFtiIndicadorQuantidadeFloating
     */
    private java.lang.String _cdFtiIndicadorQuantidadeFloating;

    /**
     * Field _qtFtiDiaFloating
     */
    private int _qtFtiDiaFloating = 0;

    /**
     * keeps track of state for field: _qtFtiDiaFloating
     */
    private boolean _has_qtFtiDiaFloating;

    /**
     * Field _cdFtiIndicadorCadastroFavorecido
     */
    private java.lang.String _cdFtiIndicadorCadastroFavorecido;

    /**
     * Field _cdFtiUtilizadoFavorecido
     */
    private java.lang.String _cdFtiUtilizadoFavorecido;

    /**
     * Field _cdFtiIndicadorQuantidadeRepique
     */
    private java.lang.String _cdFtiIndicadorQuantidadeRepique;

    /**
     * Field _qtFtiDiaRepique
     */
    private int _qtFtiDiaRepique = 0;

    /**
     * keeps track of state for field: _qtFtiDiaRepique
     */
    private boolean _has_qtFtiDiaRepique;

    /**
     * Field _dsTIpoConsultaSaldoSuperior
     */
    private java.lang.String _dsTIpoConsultaSaldoSuperior;

    /**
     * Field _ftiPagamentoVencido
     */
    private java.lang.String _ftiPagamentoVencido;

    /**
     * Field _ftiPagamentoMenor
     */
    private java.lang.String _ftiPagamentoMenor;

    /**
     * Field _ftiDsTipoConsFavorecido
     */
    private java.lang.String _ftiDsTipoConsFavorecido;

    /**
     * Field _ftiCferiLoc
     */
    private java.lang.String _ftiCferiLoc;

    /**
     * Field _ftiQuantidadeMaxVencido
     */
    private int _ftiQuantidadeMaxVencido = 0;

    /**
     * keeps track of state for field: _ftiQuantidadeMaxVencido
     */
    private boolean _has_ftiQuantidadeMaxVencido;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias10() 
     {
        super();
        setVlFtiMaximoFavorecidoNao(new java.math.BigDecimal("0"));
        setVlFtiLimiteDiario(new java.math.BigDecimal("0"));
        setVlFtiLimiteIndividual(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteFtiQuantidadeMaxVencido
     * 
     */
    public void deleteFtiQuantidadeMaxVencido()
    {
        this._has_ftiQuantidadeMaxVencido= false;
    } //-- void deleteFtiQuantidadeMaxVencido() 

    /**
     * Method deletePcFtiMaximoInconsistente
     * 
     */
    public void deletePcFtiMaximoInconsistente()
    {
        this._has_pcFtiMaximoInconsistente= false;
    } //-- void deletePcFtiMaximoInconsistente() 

    /**
     * Method deleteQtFtiDiaFloating
     * 
     */
    public void deleteQtFtiDiaFloating()
    {
        this._has_qtFtiDiaFloating= false;
    } //-- void deleteQtFtiDiaFloating() 

    /**
     * Method deleteQtFtiDiaRepique
     * 
     */
    public void deleteQtFtiDiaRepique()
    {
        this._has_qtFtiDiaRepique= false;
    } //-- void deleteQtFtiDiaRepique() 

    /**
     * Method deleteQtFtiMaximaInconsistencia
     * 
     */
    public void deleteQtFtiMaximaInconsistencia()
    {
        this._has_qtFtiMaximaInconsistencia= false;
    } //-- void deleteQtFtiMaximaInconsistencia() 

    /**
     * Returns the value of field
     * 'cdFtiIndicadorCadastroFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdFtiIndicadorCadastroFavorecido'
     */
    public java.lang.String getCdFtiIndicadorCadastroFavorecido()
    {
        return this._cdFtiIndicadorCadastroFavorecido;
    } //-- java.lang.String getCdFtiIndicadorCadastroFavorecido() 

    /**
     * Returns the value of field 'cdFtiIndicadorPercentualMaximo'.
     * 
     * @return String
     * @return the value of field 'cdFtiIndicadorPercentualMaximo'.
     */
    public java.lang.String getCdFtiIndicadorPercentualMaximo()
    {
        return this._cdFtiIndicadorPercentualMaximo;
    } //-- java.lang.String getCdFtiIndicadorPercentualMaximo() 

    /**
     * Returns the value of field
     * 'cdFtiIndicadorQuantidadeFloating'.
     * 
     * @return String
     * @return the value of field 'cdFtiIndicadorQuantidadeFloating'
     */
    public java.lang.String getCdFtiIndicadorQuantidadeFloating()
    {
        return this._cdFtiIndicadorQuantidadeFloating;
    } //-- java.lang.String getCdFtiIndicadorQuantidadeFloating() 

    /**
     * Returns the value of field 'cdFtiIndicadorQuantidadeMaxima'.
     * 
     * @return String
     * @return the value of field 'cdFtiIndicadorQuantidadeMaxima'.
     */
    public java.lang.String getCdFtiIndicadorQuantidadeMaxima()
    {
        return this._cdFtiIndicadorQuantidadeMaxima;
    } //-- java.lang.String getCdFtiIndicadorQuantidadeMaxima() 

    /**
     * Returns the value of field
     * 'cdFtiIndicadorQuantidadeRepique'.
     * 
     * @return String
     * @return the value of field 'cdFtiIndicadorQuantidadeRepique'.
     */
    public java.lang.String getCdFtiIndicadorQuantidadeRepique()
    {
        return this._cdFtiIndicadorQuantidadeRepique;
    } //-- java.lang.String getCdFtiIndicadorQuantidadeRepique() 

    /**
     * Returns the value of field 'cdFtiIndicadorValorDia'.
     * 
     * @return String
     * @return the value of field 'cdFtiIndicadorValorDia'.
     */
    public java.lang.String getCdFtiIndicadorValorDia()
    {
        return this._cdFtiIndicadorValorDia;
    } //-- java.lang.String getCdFtiIndicadorValorDia() 

    /**
     * Returns the value of field 'cdFtiIndicadorValorIndividual'.
     * 
     * @return String
     * @return the value of field 'cdFtiIndicadorValorIndividual'.
     */
    public java.lang.String getCdFtiIndicadorValorIndividual()
    {
        return this._cdFtiIndicadorValorIndividual;
    } //-- java.lang.String getCdFtiIndicadorValorIndividual() 

    /**
     * Returns the value of field 'cdFtiIndicadorValorMaximo'.
     * 
     * @return String
     * @return the value of field 'cdFtiIndicadorValorMaximo'.
     */
    public java.lang.String getCdFtiIndicadorValorMaximo()
    {
        return this._cdFtiIndicadorValorMaximo;
    } //-- java.lang.String getCdFtiIndicadorValorMaximo() 

    /**
     * Returns the value of field 'cdFtiUtilizadoFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdFtiUtilizadoFavorecido'.
     */
    public java.lang.String getCdFtiUtilizadoFavorecido()
    {
        return this._cdFtiUtilizadoFavorecido;
    } //-- java.lang.String getCdFtiUtilizadoFavorecido() 

    /**
     * Returns the value of field 'dsFtiPrioridadeDebito'.
     * 
     * @return String
     * @return the value of field 'dsFtiPrioridadeDebito'.
     */
    public java.lang.String getDsFtiPrioridadeDebito()
    {
        return this._dsFtiPrioridadeDebito;
    } //-- java.lang.String getDsFtiPrioridadeDebito() 

    /**
     * Returns the value of field 'dsFtiTipoConsultaSaldo'.
     * 
     * @return String
     * @return the value of field 'dsFtiTipoConsultaSaldo'.
     */
    public java.lang.String getDsFtiTipoConsultaSaldo()
    {
        return this._dsFtiTipoConsultaSaldo;
    } //-- java.lang.String getDsFtiTipoConsultaSaldo() 

    /**
     * Returns the value of field 'dsFtiTipoProcessamento'.
     * 
     * @return String
     * @return the value of field 'dsFtiTipoProcessamento'.
     */
    public java.lang.String getDsFtiTipoProcessamento()
    {
        return this._dsFtiTipoProcessamento;
    } //-- java.lang.String getDsFtiTipoProcessamento() 

    /**
     * Returns the value of field 'dsFtiTipoRejeicaoAgendamento'.
     * 
     * @return String
     * @return the value of field 'dsFtiTipoRejeicaoAgendamento'.
     */
    public java.lang.String getDsFtiTipoRejeicaoAgendamento()
    {
        return this._dsFtiTipoRejeicaoAgendamento;
    } //-- java.lang.String getDsFtiTipoRejeicaoAgendamento() 

    /**
     * Returns the value of field 'dsFtiTipoRejeicaoEfetivacao'.
     * 
     * @return String
     * @return the value of field 'dsFtiTipoRejeicaoEfetivacao'.
     */
    public java.lang.String getDsFtiTipoRejeicaoEfetivacao()
    {
        return this._dsFtiTipoRejeicaoEfetivacao;
    } //-- java.lang.String getDsFtiTipoRejeicaoEfetivacao() 

    /**
     * Returns the value of field 'dsFtiTipoTitulo'.
     * 
     * @return String
     * @return the value of field 'dsFtiTipoTitulo'.
     */
    public java.lang.String getDsFtiTipoTitulo()
    {
        return this._dsFtiTipoTitulo;
    } //-- java.lang.String getDsFtiTipoTitulo() 

    /**
     * Returns the value of field 'dsFtiTipoTratamentoFeriado'.
     * 
     * @return String
     * @return the value of field 'dsFtiTipoTratamentoFeriado'.
     */
    public java.lang.String getDsFtiTipoTratamentoFeriado()
    {
        return this._dsFtiTipoTratamentoFeriado;
    } //-- java.lang.String getDsFtiTipoTratamentoFeriado() 

    /**
     * Returns the value of field 'dsTIpoConsultaSaldoSuperior'.
     * 
     * @return String
     * @return the value of field 'dsTIpoConsultaSaldoSuperior'.
     */
    public java.lang.String getDsTIpoConsultaSaldoSuperior()
    {
        return this._dsTIpoConsultaSaldoSuperior;
    } //-- java.lang.String getDsTIpoConsultaSaldoSuperior() 

    /**
     * Returns the value of field 'ftiCferiLoc'.
     * 
     * @return String
     * @return the value of field 'ftiCferiLoc'.
     */
    public java.lang.String getFtiCferiLoc()
    {
        return this._ftiCferiLoc;
    } //-- java.lang.String getFtiCferiLoc() 

    /**
     * Returns the value of field 'ftiDsTipoConsFavorecido'.
     * 
     * @return String
     * @return the value of field 'ftiDsTipoConsFavorecido'.
     */
    public java.lang.String getFtiDsTipoConsFavorecido()
    {
        return this._ftiDsTipoConsFavorecido;
    } //-- java.lang.String getFtiDsTipoConsFavorecido() 

    /**
     * Returns the value of field 'ftiPagamentoMenor'.
     * 
     * @return String
     * @return the value of field 'ftiPagamentoMenor'.
     */
    public java.lang.String getFtiPagamentoMenor()
    {
        return this._ftiPagamentoMenor;
    } //-- java.lang.String getFtiPagamentoMenor() 

    /**
     * Returns the value of field 'ftiPagamentoVencido'.
     * 
     * @return String
     * @return the value of field 'ftiPagamentoVencido'.
     */
    public java.lang.String getFtiPagamentoVencido()
    {
        return this._ftiPagamentoVencido;
    } //-- java.lang.String getFtiPagamentoVencido() 

    /**
     * Returns the value of field 'ftiQuantidadeMaxVencido'.
     * 
     * @return int
     * @return the value of field 'ftiQuantidadeMaxVencido'.
     */
    public int getFtiQuantidadeMaxVencido()
    {
        return this._ftiQuantidadeMaxVencido;
    } //-- int getFtiQuantidadeMaxVencido() 

    /**
     * Returns the value of field 'pcFtiMaximoInconsistente'.
     * 
     * @return int
     * @return the value of field 'pcFtiMaximoInconsistente'.
     */
    public int getPcFtiMaximoInconsistente()
    {
        return this._pcFtiMaximoInconsistente;
    } //-- int getPcFtiMaximoInconsistente() 

    /**
     * Returns the value of field 'qtFtiDiaFloating'.
     * 
     * @return int
     * @return the value of field 'qtFtiDiaFloating'.
     */
    public int getQtFtiDiaFloating()
    {
        return this._qtFtiDiaFloating;
    } //-- int getQtFtiDiaFloating() 

    /**
     * Returns the value of field 'qtFtiDiaRepique'.
     * 
     * @return int
     * @return the value of field 'qtFtiDiaRepique'.
     */
    public int getQtFtiDiaRepique()
    {
        return this._qtFtiDiaRepique;
    } //-- int getQtFtiDiaRepique() 

    /**
     * Returns the value of field 'qtFtiMaximaInconsistencia'.
     * 
     * @return int
     * @return the value of field 'qtFtiMaximaInconsistencia'.
     */
    public int getQtFtiMaximaInconsistencia()
    {
        return this._qtFtiMaximaInconsistencia;
    } //-- int getQtFtiMaximaInconsistencia() 

    /**
     * Returns the value of field 'vlFtiLimiteDiario'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlFtiLimiteDiario'.
     */
    public java.math.BigDecimal getVlFtiLimiteDiario()
    {
        return this._vlFtiLimiteDiario;
    } //-- java.math.BigDecimal getVlFtiLimiteDiario() 

    /**
     * Returns the value of field 'vlFtiLimiteIndividual'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlFtiLimiteIndividual'.
     */
    public java.math.BigDecimal getVlFtiLimiteIndividual()
    {
        return this._vlFtiLimiteIndividual;
    } //-- java.math.BigDecimal getVlFtiLimiteIndividual() 

    /**
     * Returns the value of field 'vlFtiMaximoFavorecidoNao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlFtiMaximoFavorecidoNao'.
     */
    public java.math.BigDecimal getVlFtiMaximoFavorecidoNao()
    {
        return this._vlFtiMaximoFavorecidoNao;
    } //-- java.math.BigDecimal getVlFtiMaximoFavorecidoNao() 

    /**
     * Method hasFtiQuantidadeMaxVencido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasFtiQuantidadeMaxVencido()
    {
        return this._has_ftiQuantidadeMaxVencido;
    } //-- boolean hasFtiQuantidadeMaxVencido() 

    /**
     * Method hasPcFtiMaximoInconsistente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasPcFtiMaximoInconsistente()
    {
        return this._has_pcFtiMaximoInconsistente;
    } //-- boolean hasPcFtiMaximoInconsistente() 

    /**
     * Method hasQtFtiDiaFloating
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtFtiDiaFloating()
    {
        return this._has_qtFtiDiaFloating;
    } //-- boolean hasQtFtiDiaFloating() 

    /**
     * Method hasQtFtiDiaRepique
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtFtiDiaRepique()
    {
        return this._has_qtFtiDiaRepique;
    } //-- boolean hasQtFtiDiaRepique() 

    /**
     * Method hasQtFtiMaximaInconsistencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtFtiMaximaInconsistencia()
    {
        return this._has_qtFtiMaximaInconsistencia;
    } //-- boolean hasQtFtiMaximaInconsistencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFtiIndicadorCadastroFavorecido'.
     * 
     * @param cdFtiIndicadorCadastroFavorecido the value of field
     * 'cdFtiIndicadorCadastroFavorecido'.
     */
    public void setCdFtiIndicadorCadastroFavorecido(java.lang.String cdFtiIndicadorCadastroFavorecido)
    {
        this._cdFtiIndicadorCadastroFavorecido = cdFtiIndicadorCadastroFavorecido;
    } //-- void setCdFtiIndicadorCadastroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdFtiIndicadorPercentualMaximo'.
     * 
     * @param cdFtiIndicadorPercentualMaximo the value of field
     * 'cdFtiIndicadorPercentualMaximo'.
     */
    public void setCdFtiIndicadorPercentualMaximo(java.lang.String cdFtiIndicadorPercentualMaximo)
    {
        this._cdFtiIndicadorPercentualMaximo = cdFtiIndicadorPercentualMaximo;
    } //-- void setCdFtiIndicadorPercentualMaximo(java.lang.String) 

    /**
     * Sets the value of field 'cdFtiIndicadorQuantidadeFloating'.
     * 
     * @param cdFtiIndicadorQuantidadeFloating the value of field
     * 'cdFtiIndicadorQuantidadeFloating'.
     */
    public void setCdFtiIndicadorQuantidadeFloating(java.lang.String cdFtiIndicadorQuantidadeFloating)
    {
        this._cdFtiIndicadorQuantidadeFloating = cdFtiIndicadorQuantidadeFloating;
    } //-- void setCdFtiIndicadorQuantidadeFloating(java.lang.String) 

    /**
     * Sets the value of field 'cdFtiIndicadorQuantidadeMaxima'.
     * 
     * @param cdFtiIndicadorQuantidadeMaxima the value of field
     * 'cdFtiIndicadorQuantidadeMaxima'.
     */
    public void setCdFtiIndicadorQuantidadeMaxima(java.lang.String cdFtiIndicadorQuantidadeMaxima)
    {
        this._cdFtiIndicadorQuantidadeMaxima = cdFtiIndicadorQuantidadeMaxima;
    } //-- void setCdFtiIndicadorQuantidadeMaxima(java.lang.String) 

    /**
     * Sets the value of field 'cdFtiIndicadorQuantidadeRepique'.
     * 
     * @param cdFtiIndicadorQuantidadeRepique the value of field
     * 'cdFtiIndicadorQuantidadeRepique'.
     */
    public void setCdFtiIndicadorQuantidadeRepique(java.lang.String cdFtiIndicadorQuantidadeRepique)
    {
        this._cdFtiIndicadorQuantidadeRepique = cdFtiIndicadorQuantidadeRepique;
    } //-- void setCdFtiIndicadorQuantidadeRepique(java.lang.String) 

    /**
     * Sets the value of field 'cdFtiIndicadorValorDia'.
     * 
     * @param cdFtiIndicadorValorDia the value of field
     * 'cdFtiIndicadorValorDia'.
     */
    public void setCdFtiIndicadorValorDia(java.lang.String cdFtiIndicadorValorDia)
    {
        this._cdFtiIndicadorValorDia = cdFtiIndicadorValorDia;
    } //-- void setCdFtiIndicadorValorDia(java.lang.String) 

    /**
     * Sets the value of field 'cdFtiIndicadorValorIndividual'.
     * 
     * @param cdFtiIndicadorValorIndividual the value of field
     * 'cdFtiIndicadorValorIndividual'.
     */
    public void setCdFtiIndicadorValorIndividual(java.lang.String cdFtiIndicadorValorIndividual)
    {
        this._cdFtiIndicadorValorIndividual = cdFtiIndicadorValorIndividual;
    } //-- void setCdFtiIndicadorValorIndividual(java.lang.String) 

    /**
     * Sets the value of field 'cdFtiIndicadorValorMaximo'.
     * 
     * @param cdFtiIndicadorValorMaximo the value of field
     * 'cdFtiIndicadorValorMaximo'.
     */
    public void setCdFtiIndicadorValorMaximo(java.lang.String cdFtiIndicadorValorMaximo)
    {
        this._cdFtiIndicadorValorMaximo = cdFtiIndicadorValorMaximo;
    } //-- void setCdFtiIndicadorValorMaximo(java.lang.String) 

    /**
     * Sets the value of field 'cdFtiUtilizadoFavorecido'.
     * 
     * @param cdFtiUtilizadoFavorecido the value of field
     * 'cdFtiUtilizadoFavorecido'.
     */
    public void setCdFtiUtilizadoFavorecido(java.lang.String cdFtiUtilizadoFavorecido)
    {
        this._cdFtiUtilizadoFavorecido = cdFtiUtilizadoFavorecido;
    } //-- void setCdFtiUtilizadoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsFtiPrioridadeDebito'.
     * 
     * @param dsFtiPrioridadeDebito the value of field
     * 'dsFtiPrioridadeDebito'.
     */
    public void setDsFtiPrioridadeDebito(java.lang.String dsFtiPrioridadeDebito)
    {
        this._dsFtiPrioridadeDebito = dsFtiPrioridadeDebito;
    } //-- void setDsFtiPrioridadeDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsFtiTipoConsultaSaldo'.
     * 
     * @param dsFtiTipoConsultaSaldo the value of field
     * 'dsFtiTipoConsultaSaldo'.
     */
    public void setDsFtiTipoConsultaSaldo(java.lang.String dsFtiTipoConsultaSaldo)
    {
        this._dsFtiTipoConsultaSaldo = dsFtiTipoConsultaSaldo;
    } //-- void setDsFtiTipoConsultaSaldo(java.lang.String) 

    /**
     * Sets the value of field 'dsFtiTipoProcessamento'.
     * 
     * @param dsFtiTipoProcessamento the value of field
     * 'dsFtiTipoProcessamento'.
     */
    public void setDsFtiTipoProcessamento(java.lang.String dsFtiTipoProcessamento)
    {
        this._dsFtiTipoProcessamento = dsFtiTipoProcessamento;
    } //-- void setDsFtiTipoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFtiTipoRejeicaoAgendamento'.
     * 
     * @param dsFtiTipoRejeicaoAgendamento the value of field
     * 'dsFtiTipoRejeicaoAgendamento'.
     */
    public void setDsFtiTipoRejeicaoAgendamento(java.lang.String dsFtiTipoRejeicaoAgendamento)
    {
        this._dsFtiTipoRejeicaoAgendamento = dsFtiTipoRejeicaoAgendamento;
    } //-- void setDsFtiTipoRejeicaoAgendamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFtiTipoRejeicaoEfetivacao'.
     * 
     * @param dsFtiTipoRejeicaoEfetivacao the value of field
     * 'dsFtiTipoRejeicaoEfetivacao'.
     */
    public void setDsFtiTipoRejeicaoEfetivacao(java.lang.String dsFtiTipoRejeicaoEfetivacao)
    {
        this._dsFtiTipoRejeicaoEfetivacao = dsFtiTipoRejeicaoEfetivacao;
    } //-- void setDsFtiTipoRejeicaoEfetivacao(java.lang.String) 

    /**
     * Sets the value of field 'dsFtiTipoTitulo'.
     * 
     * @param dsFtiTipoTitulo the value of field 'dsFtiTipoTitulo'.
     */
    public void setDsFtiTipoTitulo(java.lang.String dsFtiTipoTitulo)
    {
        this._dsFtiTipoTitulo = dsFtiTipoTitulo;
    } //-- void setDsFtiTipoTitulo(java.lang.String) 

    /**
     * Sets the value of field 'dsFtiTipoTratamentoFeriado'.
     * 
     * @param dsFtiTipoTratamentoFeriado the value of field
     * 'dsFtiTipoTratamentoFeriado'.
     */
    public void setDsFtiTipoTratamentoFeriado(java.lang.String dsFtiTipoTratamentoFeriado)
    {
        this._dsFtiTipoTratamentoFeriado = dsFtiTipoTratamentoFeriado;
    } //-- void setDsFtiTipoTratamentoFeriado(java.lang.String) 

    /**
     * Sets the value of field 'dsTIpoConsultaSaldoSuperior'.
     * 
     * @param dsTIpoConsultaSaldoSuperior the value of field
     * 'dsTIpoConsultaSaldoSuperior'.
     */
    public void setDsTIpoConsultaSaldoSuperior(java.lang.String dsTIpoConsultaSaldoSuperior)
    {
        this._dsTIpoConsultaSaldoSuperior = dsTIpoConsultaSaldoSuperior;
    } //-- void setDsTIpoConsultaSaldoSuperior(java.lang.String) 

    /**
     * Sets the value of field 'ftiCferiLoc'.
     * 
     * @param ftiCferiLoc the value of field 'ftiCferiLoc'.
     */
    public void setFtiCferiLoc(java.lang.String ftiCferiLoc)
    {
        this._ftiCferiLoc = ftiCferiLoc;
    } //-- void setFtiCferiLoc(java.lang.String) 

    /**
     * Sets the value of field 'ftiDsTipoConsFavorecido'.
     * 
     * @param ftiDsTipoConsFavorecido the value of field
     * 'ftiDsTipoConsFavorecido'.
     */
    public void setFtiDsTipoConsFavorecido(java.lang.String ftiDsTipoConsFavorecido)
    {
        this._ftiDsTipoConsFavorecido = ftiDsTipoConsFavorecido;
    } //-- void setFtiDsTipoConsFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'ftiPagamentoMenor'.
     * 
     * @param ftiPagamentoMenor the value of field
     * 'ftiPagamentoMenor'.
     */
    public void setFtiPagamentoMenor(java.lang.String ftiPagamentoMenor)
    {
        this._ftiPagamentoMenor = ftiPagamentoMenor;
    } //-- void setFtiPagamentoMenor(java.lang.String) 

    /**
     * Sets the value of field 'ftiPagamentoVencido'.
     * 
     * @param ftiPagamentoVencido the value of field
     * 'ftiPagamentoVencido'.
     */
    public void setFtiPagamentoVencido(java.lang.String ftiPagamentoVencido)
    {
        this._ftiPagamentoVencido = ftiPagamentoVencido;
    } //-- void setFtiPagamentoVencido(java.lang.String) 

    /**
     * Sets the value of field 'ftiQuantidadeMaxVencido'.
     * 
     * @param ftiQuantidadeMaxVencido the value of field
     * 'ftiQuantidadeMaxVencido'.
     */
    public void setFtiQuantidadeMaxVencido(int ftiQuantidadeMaxVencido)
    {
        this._ftiQuantidadeMaxVencido = ftiQuantidadeMaxVencido;
        this._has_ftiQuantidadeMaxVencido = true;
    } //-- void setFtiQuantidadeMaxVencido(int) 

    /**
     * Sets the value of field 'pcFtiMaximoInconsistente'.
     * 
     * @param pcFtiMaximoInconsistente the value of field
     * 'pcFtiMaximoInconsistente'.
     */
    public void setPcFtiMaximoInconsistente(int pcFtiMaximoInconsistente)
    {
        this._pcFtiMaximoInconsistente = pcFtiMaximoInconsistente;
        this._has_pcFtiMaximoInconsistente = true;
    } //-- void setPcFtiMaximoInconsistente(int) 

    /**
     * Sets the value of field 'qtFtiDiaFloating'.
     * 
     * @param qtFtiDiaFloating the value of field 'qtFtiDiaFloating'
     */
    public void setQtFtiDiaFloating(int qtFtiDiaFloating)
    {
        this._qtFtiDiaFloating = qtFtiDiaFloating;
        this._has_qtFtiDiaFloating = true;
    } //-- void setQtFtiDiaFloating(int) 

    /**
     * Sets the value of field 'qtFtiDiaRepique'.
     * 
     * @param qtFtiDiaRepique the value of field 'qtFtiDiaRepique'.
     */
    public void setQtFtiDiaRepique(int qtFtiDiaRepique)
    {
        this._qtFtiDiaRepique = qtFtiDiaRepique;
        this._has_qtFtiDiaRepique = true;
    } //-- void setQtFtiDiaRepique(int) 

    /**
     * Sets the value of field 'qtFtiMaximaInconsistencia'.
     * 
     * @param qtFtiMaximaInconsistencia the value of field
     * 'qtFtiMaximaInconsistencia'.
     */
    public void setQtFtiMaximaInconsistencia(int qtFtiMaximaInconsistencia)
    {
        this._qtFtiMaximaInconsistencia = qtFtiMaximaInconsistencia;
        this._has_qtFtiMaximaInconsistencia = true;
    } //-- void setQtFtiMaximaInconsistencia(int) 

    /**
     * Sets the value of field 'vlFtiLimiteDiario'.
     * 
     * @param vlFtiLimiteDiario the value of field
     * 'vlFtiLimiteDiario'.
     */
    public void setVlFtiLimiteDiario(java.math.BigDecimal vlFtiLimiteDiario)
    {
        this._vlFtiLimiteDiario = vlFtiLimiteDiario;
    } //-- void setVlFtiLimiteDiario(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlFtiLimiteIndividual'.
     * 
     * @param vlFtiLimiteIndividual the value of field
     * 'vlFtiLimiteIndividual'.
     */
    public void setVlFtiLimiteIndividual(java.math.BigDecimal vlFtiLimiteIndividual)
    {
        this._vlFtiLimiteIndividual = vlFtiLimiteIndividual;
    } //-- void setVlFtiLimiteIndividual(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlFtiMaximoFavorecidoNao'.
     * 
     * @param vlFtiMaximoFavorecidoNao the value of field
     * 'vlFtiMaximoFavorecidoNao'.
     */
    public void setVlFtiMaximoFavorecidoNao(java.math.BigDecimal vlFtiMaximoFavorecidoNao)
    {
        this._vlFtiMaximoFavorecidoNao = vlFtiMaximoFavorecidoNao;
    } //-- void setVlFtiMaximoFavorecidoNao(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias10
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias10 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
