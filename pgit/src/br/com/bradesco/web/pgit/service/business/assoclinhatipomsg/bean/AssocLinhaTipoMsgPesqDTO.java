/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean;

/**
 * Nome: AssocLinhaTipoMsgPesqDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AssocLinhaTipoMsgPesqDTO {
	//Request
	/** Atributo tipoMensagem. */
	private String tipoMensagem;
	
	/** Atributo idioma. */
	private String idioma;
	
	/** Atributo recurso. */
	private String recurso;
	
	/** Atributo codigo. */
	private String codigo;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo ordemLinha. */
	private String ordemLinha;
	
	//Linha selecionada da grid de Pesquisa de Mensagem
	/** Atributo linhaSelecionada. */
	private int linhaSelecionada;

	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Get: idioma.
	 *
	 * @return idioma
	 */
	public String getIdioma() {
		return idioma;
	}

	/**
	 * Set: idioma.
	 *
	 * @param idioma the idioma
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	/**
	 * Get: linhaSelecionada.
	 *
	 * @return linhaSelecionada
	 */
	public int getLinhaSelecionada() {
		return linhaSelecionada;
	}

	/**
	 * Set: linhaSelecionada.
	 *
	 * @param linhaSelecionada the linha selecionada
	 */
	public void setLinhaSelecionada(int linhaSelecionada) {
		this.linhaSelecionada = linhaSelecionada;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: ordemLinha.
	 *
	 * @return ordemLinha
	 */
	public String getOrdemLinha() {
		return ordemLinha;
	}

	/**
	 * Set: ordemLinha.
	 *
	 * @param ordemLinha the ordem linha
	 */
	public void setOrdemLinha(String ordemLinha) {
		this.ordemLinha = ordemLinha;
	}

	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public String getRecurso() {
		return recurso;
	}

	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public String getTipoMensagem() {
		return tipoMensagem;
	}

	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(String tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}
	
	
	
	
	
}
