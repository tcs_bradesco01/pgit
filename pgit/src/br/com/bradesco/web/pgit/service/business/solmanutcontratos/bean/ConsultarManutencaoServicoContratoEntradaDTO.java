/*
 * Nome: br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;

/**
 * Nome: ConsultarManutencaoServicoContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarManutencaoServicoContratoEntradaDTO {
	
	/** Atributo cdPessoaJuridica. */
	private long cdPessoaJuridica;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo cdTipoManutencaoContrato. */
	private int cdTipoManutencaoContrato;
	
	/** Atributo nrManutencaoContratoNegocio. */
	private long nrManutencaoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoManutencaoContrato.
	 *
	 * @return cdTipoManutencaoContrato
	 */
	public int getCdTipoManutencaoContrato() {
		return cdTipoManutencaoContrato;
	}
	
	/**
	 * Set: cdTipoManutencaoContrato.
	 *
	 * @param cdTipoManutencaoContrato the cd tipo manutencao contrato
	 */
	public void setCdTipoManutencaoContrato(int cdTipoManutencaoContrato) {
		this.cdTipoManutencaoContrato = cdTipoManutencaoContrato;
	}
	
	/**
	 * Get: nrManutencaoContratoNegocio.
	 *
	 * @return nrManutencaoContratoNegocio
	 */
	public long getNrManutencaoContratoNegocio() {
		return nrManutencaoContratoNegocio;
	}
	
	/**
	 * Set: nrManutencaoContratoNegocio.
	 *
	 * @param nrManutencaoContratoNegocio the nr manutencao contrato negocio
	 */
	public void setNrManutencaoContratoNegocio(long nrManutencaoContratoNegocio) {
		this.nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

}
