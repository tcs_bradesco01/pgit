/*
 * Nome: br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cancelarpagamentosconsolidados.bean;

/**
 * Nome: CancelarPagtosIntegralEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class CancelarPagtosIntegralEntradaDTO {

	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo nrCorpoCpfCnpj. */
	private Long nrCorpoCpfCnpj;
	
	/** Atributo nrFilialCnpj. */
	private Integer nrFilialCnpj;
	
	/** Atributo nrControleCpfCnpj. */
	private Integer nrControleCpfCnpj;
	
	/** Atributo dtPagamento. */
	private String dtPagamento;
	
	/** Atributo nrArquivoRemessaPagamento. */
	private Long nrArquivoRemessaPagamento;
	
	/** Atributo cdBanco. */
	private Integer cdBanco;
	
	/** Atributo cdAgenciaBancaria. */
	private Integer cdAgenciaBancaria;
	
	/** Atributo cdContaBancaria. */
	private Long cdContaBancaria;
	
	/** Atributo cdDigitoContaBancaria. */
	private String cdDigitoContaBancaria;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private Integer cdProdutoServicoRelacionado;
	
	/** Atributo cdSituacaoOperacaoPagamento. */
	private Integer cdSituacaoOperacaoPagamento;
	
	/** Atributo cdMotivoSituacaoPagamento. */
	private Integer cdMotivoSituacaoPagamento;
	
    /** Atributo cdPessoaContratoDebito. */
    private Long cdPessoaContratoDebito;
    
    /** Atributo cdTipoContratoDebito. */
    private Integer cdTipoContratoDebito;
    
    /** Atributo nrSequenciaContratoDebito. */
    private Long nrSequenciaContratoDebito;	

	/**
	 * Get: cdAgenciaBancaria.
	 *
	 * @return cdAgenciaBancaria
	 */
	public Integer getCdAgenciaBancaria() {
		return cdAgenciaBancaria;
	}

	/**
	 * Set: cdAgenciaBancaria.
	 *
	 * @param cdAgenciaBancaria the cd agencia bancaria
	 */
	public void setCdAgenciaBancaria(Integer cdAgenciaBancaria) {
		this.cdAgenciaBancaria = cdAgenciaBancaria;
	}

	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}

	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	/**
	 * Get: cdContaBancaria.
	 *
	 * @return cdContaBancaria
	 */
	public Long getCdContaBancaria() {
		return cdContaBancaria;
	}

	/**
	 * Set: cdContaBancaria.
	 *
	 * @param cdContaBancaria the cd conta bancaria
	 */
	public void setCdContaBancaria(Long cdContaBancaria) {
		this.cdContaBancaria = cdContaBancaria;
	}

	/**
	 * Get: cdDigitoContaBancaria.
	 *
	 * @return cdDigitoContaBancaria
	 */
	public String getCdDigitoContaBancaria() {
		return cdDigitoContaBancaria;
	}

	/**
	 * Set: cdDigitoContaBancaria.
	 *
	 * @param cdDigitoContaBancaria the cd digito conta bancaria
	 */
	public void setCdDigitoContaBancaria(String cdDigitoContaBancaria) {
		this.cdDigitoContaBancaria = cdDigitoContaBancaria;
	}

	/**
	 * Get: cdMotivoSituacaoPagamento.
	 *
	 * @return cdMotivoSituacaoPagamento
	 */
	public Integer getCdMotivoSituacaoPagamento() {
		return cdMotivoSituacaoPagamento;
	}

	/**
	 * Set: cdMotivoSituacaoPagamento.
	 *
	 * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
		this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}

	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(
			Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(
			Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	/**
	 * Get: nrArquivoRemessaPagamento.
	 *
	 * @return nrArquivoRemessaPagamento
	 */
	public Long getNrArquivoRemessaPagamento() {
		return nrArquivoRemessaPagamento;
	}

	/**
	 * Set: nrArquivoRemessaPagamento.
	 *
	 * @param nrArquivoRemessaPagamento the nr arquivo remessa pagamento
	 */
	public void setNrArquivoRemessaPagamento(Long nrArquivoRemessaPagamento) {
		this.nrArquivoRemessaPagamento = nrArquivoRemessaPagamento;
	}

	/**
	 * Get: nrCorpoCpfCnpj.
	 *
	 * @return nrCorpoCpfCnpj
	 */
	public Long getNrCorpoCpfCnpj() {
		return nrCorpoCpfCnpj;
	}

	/**
	 * Set: nrCorpoCpfCnpj.
	 *
	 * @param nrCorpoCpfCnpj the nr corpo cpf cnpj
	 */
	public void setNrCorpoCpfCnpj(Long nrCorpoCpfCnpj) {
		this.nrCorpoCpfCnpj = nrCorpoCpfCnpj;
	}

	/**
	 * Get: nrFilialCnpj.
	 *
	 * @return nrFilialCnpj
	 */
	public Integer getNrFilialCnpj() {
		return nrFilialCnpj;
	}

	/**
	 * Set: nrFilialCnpj.
	 *
	 * @param nrFilialCnpj the nr filial cnpj
	 */
	public void setNrFilialCnpj(Integer nrFilialCnpj) {
		this.nrFilialCnpj = nrFilialCnpj;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdPessoaContratoDebito.
	 *
	 * @return cdPessoaContratoDebito
	 */
	public Long getCdPessoaContratoDebito() {
		return cdPessoaContratoDebito;
	}

	/**
	 * Set: cdPessoaContratoDebito.
	 *
	 * @param cdPessoaContratoDebito the cd pessoa contrato debito
	 */
	public void setCdPessoaContratoDebito(Long cdPessoaContratoDebito) {
		this.cdPessoaContratoDebito = cdPessoaContratoDebito;
	}

	/**
	 * Get: cdTipoContratoDebito.
	 *
	 * @return cdTipoContratoDebito
	 */
	public Integer getCdTipoContratoDebito() {
		return cdTipoContratoDebito;
	}

	/**
	 * Set: cdTipoContratoDebito.
	 *
	 * @param cdTipoContratoDebito the cd tipo contrato debito
	 */
	public void setCdTipoContratoDebito(Integer cdTipoContratoDebito) {
		this.cdTipoContratoDebito = cdTipoContratoDebito;
	}

	/**
	 * Get: nrSequenciaContratoDebito.
	 *
	 * @return nrSequenciaContratoDebito
	 */
	public Long getNrSequenciaContratoDebito() {
		return nrSequenciaContratoDebito;
	}

	/**
	 * Set: nrSequenciaContratoDebito.
	 *
	 * @param nrSequenciaContratoDebito the nr sequencia contrato debito
	 */
	public void setNrSequenciaContratoDebito(Long nrSequenciaContratoDebito) {
		this.nrSequenciaContratoDebito = nrSequenciaContratoDebito;
	}

	/**
	 * Get: nrControleCpfCnpj.
	 *
	 * @return nrControleCpfCnpj
	 */
	public Integer getNrControleCpfCnpj() {
		return nrControleCpfCnpj;
	}

	/**
	 * Set: nrControleCpfCnpj.
	 *
	 * @param nrControleCpfCnpj the nr controle cpf cnpj
	 */
	public void setNrControleCpfCnpj(Integer nrControleCpfCnpj) {
		this.nrControleCpfCnpj = nrControleCpfCnpj;
	}
	
	
	
}
