/*
 * Nome: br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean;

import java.util.List;

/**
 * Nome: ListarSoliRelatorioOrgaosPublicosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSoliRelatorioOrgaosPublicosSaidaDTO{
	
	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo ocorrencias. */
	private List<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO> ocorrencias;

	
	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}
	
	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
	
	/**
	 * Set: ocorrencias.
	 *
	 * @param ocorrencias the ocorrencias
	 */
	public void setOcorrencias(List<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}
	
	/**
	 * Get: ocorrencias.
	 *
	 * @return ocorrencias
	 */
	public List<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO> getOcorrencias() {
		return ocorrencias;
	}
}