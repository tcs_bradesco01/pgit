package br.com.bradesco.web.pgit.service.business.confcestastarifas.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.confcestastarifas.IConfCestaTarifaService;
import br.com.bradesco.web.pgit.service.business.confcestastarifas.dto.ConfCestaTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.confcestastarifas.dto.ConfCestaTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.confcestastarifas.dto.OcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.bean.NumerosEmissoesPagasEnum;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.confcestatarifa.request.ConfCestaTarifaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.confcestatarifa.response.ConfCestaTarifaResponse;

public class IConfCestaTarifaServiceImpl implements IConfCestaTarifaService{
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;	
	
	
	public ConfCestaTarifaSaidaDTO consultarConfCestaTarifa(ConfCestaTarifaEntradaDTO entrada) {
		
		ConfCestaTarifaRequest request = new ConfCestaTarifaRequest();
		ConfCestaTarifaSaidaDTO saida = new ConfCestaTarifaSaidaDTO();
		List<OcorrenciasSaidaDTO> listaConfCestasTarifas = new ArrayList<OcorrenciasSaidaDTO>();
		
		request.setMaxOcorrencias(entrada.getMaxOcorrencias());
		request.setCdPessoasJuridicaContrato(entrada.getCdPessoasJuridicaContrato());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNumSequenciaNegocio(entrada.getNumSequenciaNegocio());
		
		try{
			ConfCestaTarifaResponse response = new ConfCestaTarifaResponse();
			response = getFactoryAdapter().getConfCestaTarifaPDCAdapter().invokeProcess(request);
			saida.setNumLinhas(response.getNumLinhas());
			
			for(int i = 0; i < response.getOcorrenciasCount(); i++ ){
				
				OcorrenciasSaidaDTO occurs = new OcorrenciasSaidaDTO();			
				occurs.setCdCestaTariafaFlexbilizacao(response.getOcorrencias(i).getCdCestaTariafaFlexbilizacao());
				occurs.setDataVigenciaFim(response.getOcorrencias(i).getDataVigenciaFim());
				occurs.setDataVigenciaInicio(response.getOcorrencias(i).getDataVigenciaInicio());
				occurs.setDsCestaTariafaFlexbilizacao(response.getOcorrencias(i).getDsCestaTariafaFlexbilizacao());
				occurs.setPorcentagemTariafaFlexbilizacaoContrato(response.getOcorrencias(i).getPorcentagemTariafaFlexbilizacaoContrato());
				occurs.setVlTariafaFlexbilizacaoContrato(response.getOcorrencias(i).getVlTariafaFlexbilizacaoContrato());
				occurs.setVlTarifaPadrao(response.getOcorrencias(i).getVlTarifaPadrao());
				
				listaConfCestasTarifas.add(occurs);						
			}		
			saida.setListaOcorrenciasSaidaDTO(listaConfCestasTarifas);			
						
		}catch (Exception e) {
			saida.setNumLinhas(0);
			saida.setListaOcorrenciasSaidaDTO(null);
		}
		
		return saida;
		
	}
	

	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

}
