/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharduplicacaoarqretorno.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharDuplicacaoArqRetornoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharDuplicacaoArqRetornoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridContrato
     */
    private long _cdPessoaJuridContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridContrato
     */
    private boolean _has_cdPessoaJuridContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSeqContratoNegocio
     */
    private long _nrSeqContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSeqContratoNegocio
     */
    private boolean _has_nrSeqContratoNegocio;

    /**
     * Field _cdPessoaJuridVinc
     */
    private long _cdPessoaJuridVinc = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridVinc
     */
    private boolean _has_cdPessoaJuridVinc;

    /**
     * Field _cdTipoContratoVinc
     */
    private int _cdTipoContratoVinc = 0;

    /**
     * keeps track of state for field: _cdTipoContratoVinc
     */
    private boolean _has_cdTipoContratoVinc;

    /**
     * Field _nrSeqContratoVinc
     */
    private long _nrSeqContratoVinc = 0;

    /**
     * keeps track of state for field: _nrSeqContratoVinc
     */
    private boolean _has_nrSeqContratoVinc;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdTipoArquivoRetorno
     */
    private int _cdTipoArquivoRetorno = 0;

    /**
     * keeps track of state for field: _cdTipoArquivoRetorno
     */
    private boolean _has_cdTipoArquivoRetorno;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdInstrucaoEnvioRetorno
     */
    private int _cdInstrucaoEnvioRetorno = 0;

    /**
     * keeps track of state for field: _cdInstrucaoEnvioRetorno
     */
    private boolean _has_cdInstrucaoEnvioRetorno;

    /**
     * Field _cdIndicadorDuplicidadeRetorno
     */
    private int _cdIndicadorDuplicidadeRetorno = 0;

    /**
     * keeps track of state for field: _cdIndicadorDuplicidadeRetorn
     */
    private boolean _has_cdIndicadorDuplicidadeRetorno;

    /**
     * Field _cdProdutoServicoOper
     */
    private int _cdProdutoServicoOper = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOper
     */
    private boolean _has_cdProdutoServicoOper;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharDuplicacaoArqRetornoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharduplicacaoarqretorno.request.DetalharDuplicacaoArqRetornoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorDuplicidadeRetorno
     * 
     */
    public void deleteCdIndicadorDuplicidadeRetorno()
    {
        this._has_cdIndicadorDuplicidadeRetorno= false;
    } //-- void deleteCdIndicadorDuplicidadeRetorno() 

    /**
     * Method deleteCdInstrucaoEnvioRetorno
     * 
     */
    public void deleteCdInstrucaoEnvioRetorno()
    {
        this._has_cdInstrucaoEnvioRetorno= false;
    } //-- void deleteCdInstrucaoEnvioRetorno() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridContrato
     * 
     */
    public void deleteCdPessoaJuridContrato()
    {
        this._has_cdPessoaJuridContrato= false;
    } //-- void deleteCdPessoaJuridContrato() 

    /**
     * Method deleteCdPessoaJuridVinc
     * 
     */
    public void deleteCdPessoaJuridVinc()
    {
        this._has_cdPessoaJuridVinc= false;
    } //-- void deleteCdPessoaJuridVinc() 

    /**
     * Method deleteCdProdutoServicoOper
     * 
     */
    public void deleteCdProdutoServicoOper()
    {
        this._has_cdProdutoServicoOper= false;
    } //-- void deleteCdProdutoServicoOper() 

    /**
     * Method deleteCdTipoArquivoRetorno
     * 
     */
    public void deleteCdTipoArquivoRetorno()
    {
        this._has_cdTipoArquivoRetorno= false;
    } //-- void deleteCdTipoArquivoRetorno() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoContratoVinc
     * 
     */
    public void deleteCdTipoContratoVinc()
    {
        this._has_cdTipoContratoVinc= false;
    } //-- void deleteCdTipoContratoVinc() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteNrSeqContratoNegocio
     * 
     */
    public void deleteNrSeqContratoNegocio()
    {
        this._has_nrSeqContratoNegocio= false;
    } //-- void deleteNrSeqContratoNegocio() 

    /**
     * Method deleteNrSeqContratoVinc
     * 
     */
    public void deleteNrSeqContratoVinc()
    {
        this._has_nrSeqContratoVinc= false;
    } //-- void deleteNrSeqContratoVinc() 

    /**
     * Returns the value of field 'cdIndicadorDuplicidadeRetorno'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorDuplicidadeRetorno'.
     */
    public int getCdIndicadorDuplicidadeRetorno()
    {
        return this._cdIndicadorDuplicidadeRetorno;
    } //-- int getCdIndicadorDuplicidadeRetorno() 

    /**
     * Returns the value of field 'cdInstrucaoEnvioRetorno'.
     * 
     * @return int
     * @return the value of field 'cdInstrucaoEnvioRetorno'.
     */
    public int getCdInstrucaoEnvioRetorno()
    {
        return this._cdInstrucaoEnvioRetorno;
    } //-- int getCdInstrucaoEnvioRetorno() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridContrato'.
     */
    public long getCdPessoaJuridContrato()
    {
        return this._cdPessoaJuridContrato;
    } //-- long getCdPessoaJuridContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridVinc'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridVinc'.
     */
    public long getCdPessoaJuridVinc()
    {
        return this._cdPessoaJuridVinc;
    } //-- long getCdPessoaJuridVinc() 

    /**
     * Returns the value of field 'cdProdutoServicoOper'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOper'.
     */
    public int getCdProdutoServicoOper()
    {
        return this._cdProdutoServicoOper;
    } //-- int getCdProdutoServicoOper() 

    /**
     * Returns the value of field 'cdTipoArquivoRetorno'.
     * 
     * @return int
     * @return the value of field 'cdTipoArquivoRetorno'.
     */
    public int getCdTipoArquivoRetorno()
    {
        return this._cdTipoArquivoRetorno;
    } //-- int getCdTipoArquivoRetorno() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoVinc'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoVinc'.
     */
    public int getCdTipoContratoVinc()
    {
        return this._cdTipoContratoVinc;
    } //-- int getCdTipoContratoVinc() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'nrSeqContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoNegocio'.
     */
    public long getNrSeqContratoNegocio()
    {
        return this._nrSeqContratoNegocio;
    } //-- long getNrSeqContratoNegocio() 

    /**
     * Returns the value of field 'nrSeqContratoVinc'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoVinc'.
     */
    public long getNrSeqContratoVinc()
    {
        return this._nrSeqContratoVinc;
    } //-- long getNrSeqContratoVinc() 

    /**
     * Method hasCdIndicadorDuplicidadeRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorDuplicidadeRetorno()
    {
        return this._has_cdIndicadorDuplicidadeRetorno;
    } //-- boolean hasCdIndicadorDuplicidadeRetorno() 

    /**
     * Method hasCdInstrucaoEnvioRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInstrucaoEnvioRetorno()
    {
        return this._has_cdInstrucaoEnvioRetorno;
    } //-- boolean hasCdInstrucaoEnvioRetorno() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridContrato()
    {
        return this._has_cdPessoaJuridContrato;
    } //-- boolean hasCdPessoaJuridContrato() 

    /**
     * Method hasCdPessoaJuridVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridVinc()
    {
        return this._has_cdPessoaJuridVinc;
    } //-- boolean hasCdPessoaJuridVinc() 

    /**
     * Method hasCdProdutoServicoOper
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOper()
    {
        return this._has_cdProdutoServicoOper;
    } //-- boolean hasCdProdutoServicoOper() 

    /**
     * Method hasCdTipoArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoArquivoRetorno()
    {
        return this._has_cdTipoArquivoRetorno;
    } //-- boolean hasCdTipoArquivoRetorno() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoVinc()
    {
        return this._has_cdTipoContratoVinc;
    } //-- boolean hasCdTipoContratoVinc() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasNrSeqContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoNegocio()
    {
        return this._has_nrSeqContratoNegocio;
    } //-- boolean hasNrSeqContratoNegocio() 

    /**
     * Method hasNrSeqContratoVinc
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoVinc()
    {
        return this._has_nrSeqContratoVinc;
    } //-- boolean hasNrSeqContratoVinc() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorDuplicidadeRetorno'.
     * 
     * @param cdIndicadorDuplicidadeRetorno the value of field
     * 'cdIndicadorDuplicidadeRetorno'.
     */
    public void setCdIndicadorDuplicidadeRetorno(int cdIndicadorDuplicidadeRetorno)
    {
        this._cdIndicadorDuplicidadeRetorno = cdIndicadorDuplicidadeRetorno;
        this._has_cdIndicadorDuplicidadeRetorno = true;
    } //-- void setCdIndicadorDuplicidadeRetorno(int) 

    /**
     * Sets the value of field 'cdInstrucaoEnvioRetorno'.
     * 
     * @param cdInstrucaoEnvioRetorno the value of field
     * 'cdInstrucaoEnvioRetorno'.
     */
    public void setCdInstrucaoEnvioRetorno(int cdInstrucaoEnvioRetorno)
    {
        this._cdInstrucaoEnvioRetorno = cdInstrucaoEnvioRetorno;
        this._has_cdInstrucaoEnvioRetorno = true;
    } //-- void setCdInstrucaoEnvioRetorno(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridContrato'.
     * 
     * @param cdPessoaJuridContrato the value of field
     * 'cdPessoaJuridContrato'.
     */
    public void setCdPessoaJuridContrato(long cdPessoaJuridContrato)
    {
        this._cdPessoaJuridContrato = cdPessoaJuridContrato;
        this._has_cdPessoaJuridContrato = true;
    } //-- void setCdPessoaJuridContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridVinc'.
     * 
     * @param cdPessoaJuridVinc the value of field
     * 'cdPessoaJuridVinc'.
     */
    public void setCdPessoaJuridVinc(long cdPessoaJuridVinc)
    {
        this._cdPessoaJuridVinc = cdPessoaJuridVinc;
        this._has_cdPessoaJuridVinc = true;
    } //-- void setCdPessoaJuridVinc(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOper'.
     * 
     * @param cdProdutoServicoOper the value of field
     * 'cdProdutoServicoOper'.
     */
    public void setCdProdutoServicoOper(int cdProdutoServicoOper)
    {
        this._cdProdutoServicoOper = cdProdutoServicoOper;
        this._has_cdProdutoServicoOper = true;
    } //-- void setCdProdutoServicoOper(int) 

    /**
     * Sets the value of field 'cdTipoArquivoRetorno'.
     * 
     * @param cdTipoArquivoRetorno the value of field
     * 'cdTipoArquivoRetorno'.
     */
    public void setCdTipoArquivoRetorno(int cdTipoArquivoRetorno)
    {
        this._cdTipoArquivoRetorno = cdTipoArquivoRetorno;
        this._has_cdTipoArquivoRetorno = true;
    } //-- void setCdTipoArquivoRetorno(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoContratoVinc'.
     * 
     * @param cdTipoContratoVinc the value of field
     * 'cdTipoContratoVinc'.
     */
    public void setCdTipoContratoVinc(int cdTipoContratoVinc)
    {
        this._cdTipoContratoVinc = cdTipoContratoVinc;
        this._has_cdTipoContratoVinc = true;
    } //-- void setCdTipoContratoVinc(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'nrSeqContratoNegocio'.
     * 
     * @param nrSeqContratoNegocio the value of field
     * 'nrSeqContratoNegocio'.
     */
    public void setNrSeqContratoNegocio(long nrSeqContratoNegocio)
    {
        this._nrSeqContratoNegocio = nrSeqContratoNegocio;
        this._has_nrSeqContratoNegocio = true;
    } //-- void setNrSeqContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSeqContratoVinc'.
     * 
     * @param nrSeqContratoVinc the value of field
     * 'nrSeqContratoVinc'.
     */
    public void setNrSeqContratoVinc(long nrSeqContratoVinc)
    {
        this._nrSeqContratoVinc = nrSeqContratoVinc;
        this._has_nrSeqContratoVinc = true;
    } //-- void setNrSeqContratoVinc(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharDuplicacaoArqRetornoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharduplicacaoarqretorno.request.DetalharDuplicacaoArqRetornoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharduplicacaoarqretorno.request.DetalharDuplicacaoArqRetornoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharduplicacaoarqretorno.request.DetalharDuplicacaoArqRetornoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharduplicacaoarqretorno.request.DetalharDuplicacaoArqRetornoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
