/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Propósito:
 * 
 * Data da criação: 17/09/2014
 * 
 * Parâmetros de Compilação:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

/**
 * Nome: ServicosDTO
 * <p>
 * Propósito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ServicosDTO {
	//Aviso Movimentação Favorecido
	/** Atributo dsAvisoTipoAvisoFavorecido. */
	private String dsAvisoTipoAvisoFavorecido;
    
    /** Atributo dsAvisoPeriodicidadeEmissaoFavorecido. */
    private String dsAvisoPeriodicidadeEmissaoFavorecido;
    
    /** Atributo dsAvisoDestinoFavorecido. */
    private String dsAvisoDestinoFavorecido;
    
    /** Atributo dsAvisoPermissaoCorrespondenciaAberturaFavorecido. */
    private String dsAvisoPermissaoCorrespondenciaAberturaFavorecido;
    
    /** Atributo dsAvisoDemontracaoInformacaoReservadaFavorecido. */
    private String dsAvisoDemontracaoInformacaoReservadaFavorecido;
    
    /** Atributo cdAvisoQuantidadeViasEmitirFavorecido. */
    private String cdAvisoQuantidadeViasEmitirFavorecido;
    
    /** Atributo dsAvisoAgrupamentoCorrespondenciaFavorecido. */
    private String dsAvisoAgrupamentoCorrespondenciaFavorecido;
    
  //Aviso Movimentação Pagador
	/** Atributo dsAvisoTipoAvisoPagador. */
  private String dsAvisoTipoAvisoPagador;
    
    /** Atributo dsAvisoPeriodicidadeEmissaoPagador. */
    private String dsAvisoPeriodicidadeEmissaoPagador;
    
    /** Atributo dsAvisoDestinoPagador. */
    private String dsAvisoDestinoPagador;
    
    /** Atributo dsAvisoPermissaoCorrespondenciaAberturaPagador. */
    private String dsAvisoPermissaoCorrespondenciaAberturaPagador;
    
    /** Atributo dsAvisoDemontracaoInformacaoReservadaPagador. */
    private String dsAvisoDemontracaoInformacaoReservadaPagador;
    
    /** Atributo cdAvisoQuantidadeViasEmitirPagador. */
    private String cdAvisoQuantidadeViasEmitirPagador;
    
    /** Atributo dsAvisoAgrupamentoCorrespondenciaPagador. */
    private String dsAvisoAgrupamentoCorrespondenciaPagador;
    
    //Comprovante Pagamento Pagador
    /** Atributo cdComprovanteTipoComprovantePagador. */
    private String cdComprovanteTipoComprovantePagador;
	
	/** Atributo dsComprovantePeriodicidadeEmissaoPagador. */
	private String dsComprovantePeriodicidadeEmissaoPagador;
	
	/** Atributo dsComprovanteDestinoPagador. */
	private String dsComprovanteDestinoPagador;
	
	/** Atributo dsComprovantePermissaoCorrespondenteAberturaPagador. */
	private String dsComprovantePermissaoCorrespondenteAberturaPagador;
	
	/** Atributo dsDemonstrativoInformacaoReservadaPagador. */
	private String dsDemonstrativoInformacaoReservadaPagador;
	
	/** Atributo qtViasEmitirPagador. */
	private String qtViasEmitirPagador;
	
	/** Atributo dsAgrupamentoCorrespondentePagador. */
	private String dsAgrupamentoCorrespondentePagador;
	
	//Comprovante Pagamento Favorecido
    /** Atributo cdComprovanteTipoComprovanteFavorecido. */
	private String cdComprovanteTipoComprovanteFavorecido;
	
	/** Atributo dsComprovantePeriodicidadeEmissaoFavorecido. */
	private String dsComprovantePeriodicidadeEmissaoFavorecido;
	
	/** Atributo dsComprovanteDestinoFavorecido. */
	private String dsComprovanteDestinoFavorecido;
	
	/** Atributo dsComprovantePermissaoCorrespondenteAberturaFavorecido. */
	private String dsComprovantePermissaoCorrespondenteAberturaFavorecido;
	
	/** Atributo dsDemonstrativoInformacaoReservadaFavorecido. */
	private String dsDemonstrativoInformacaoReservadaFavorecido;
	
	/** Atributo qtViasEmitirFavorecido. */
	private String qtViasEmitirFavorecido;
	
	/** Atributo dsAgrupamentoCorrespondenteFavorecido. */
	private String dsAgrupamentoCorrespondenteFavorecido;
	
	//Comprovante Salarial
	/** Atributo dsCsdTipoComprovanteSalarial. */
	private String dsCsdTipoComprovanteSalarial;
    
    /** Atributo dsCsdTipoRejeicaoLoteSalarial. */
    private String dsCsdTipoRejeicaoLoteSalarial;
    
    /** Atributo dsCsdMeioDisponibilizacaoCorrentistaSalarial. */
    private String dsCsdMeioDisponibilizacaoCorrentistaSalarial;
    
    /** Atributo dsCsdMediaDisponibilidadeCorrentistaSalarial. */
    private String dsCsdMediaDisponibilidadeCorrentistaSalarial;
    
    /** Atributo dsCsdMediaDisponibilidadeNumeroCorrentistasSalarial. */
    private String dsCsdMediaDisponibilidadeNumeroCorrentistasSalarial;
    
    /** Atributo dsCsdDestinoSalarial. */
    private String dsCsdDestinoSalarial;
    
    /** Atributo qtCsdMesEmissaoSalarial. */
    private String qtCsdMesEmissaoSalarial;
    
    /** Atributo dsCsdCadastroConservadorEnderecoSalarial. */
    private String dsCsdCadastroConservadorEnderecoSalarial;
    
    /** Atributo cdCsdQuantidadeViasEmitirSalarial. */
    private String cdCsdQuantidadeViasEmitirSalarial;
    
  //Comprovante Diversos
	/** Atributo dsCsdTipoComprovanteDiversos. */
  private String dsCsdTipoComprovanteDiversos;
    
    /** Atributo dsCsdTipoRejeicaoLoteDiversos. */
    private String dsCsdTipoRejeicaoLoteDiversos;
    
    /** Atributo dsCsdMeioDisponibilizacaoCorrentistaDiversos. */
    private String dsCsdMeioDisponibilizacaoCorrentistaDiversos;
    
    /** Atributo dsCsdMediaDisponibilidadeCorrentistaDiversos. */
    private String dsCsdMediaDisponibilidadeCorrentistaDiversos;
    
    /** Atributo dsCsdMediaDisponibilidadeNumeroCorrentistasDiversos. */
    private String dsCsdMediaDisponibilidadeNumeroCorrentistasDiversos;
    
    /** Atributo dsCsdDestinoDiversos. */
    private String dsCsdDestinoDiversos;
    
    /** Atributo qtCsdMesEmissaoDiversos. */
    private String qtCsdMesEmissaoDiversos;
    
    /** Atributo dsCsdCadastroConservadorEnderecoDiversos. */
    private String dsCsdCadastroConservadorEnderecoDiversos;
    
    /** Atributo cdCsdQuantidadeViasEmitirDiversos. */
    private String cdCsdQuantidadeViasEmitirDiversos;
    
    //Cadastro de Favorecido
    /** Atributo cdIndcadorCadastroFavorecido. */
    private String cdIndcadorCadastroFavorecido;
    
    /** Atributo dsCadastroFormaManutencao. */
    private String dsCadastroFormaManutencao;
	
	/** Atributo cdCadastroQuantidadeDiasInativos. */
	private String cdCadastroQuantidadeDiasInativos;
	
	/** Atributo dsCadastroTipoConsistenciaInscricao. */
	private String dsCadastroTipoConsistenciaInscricao;
	
	//Recadastramento de Beneficiarios
	/** Atributo dsServicoRecadastro. */
	private String dsServicoRecadastro;
	
	/** Atributo dsRecTipoIdentificacaoBeneficio. */
	private String dsRecTipoIdentificacaoBeneficio;
	
	/** Atributo dsRecTipoConsistenciaIdentificacao. */
	private String dsRecTipoConsistenciaIdentificacao;
	
	/** Atributo dsRecCondicaoEnquadramento. */
	private String dsRecCondicaoEnquadramento;
	
	/** Atributo dsRecCriterioPrincipalEnquadramento. */
	private String dsRecCriterioPrincipalEnquadramento;
	
	/** Atributo dsRecTipoCriterioCompostoEnquadramento. */
	private String dsRecTipoCriterioCompostoEnquadramento;
	
	/** Atributo qtRecEtapaRecadastramento. */
	private String qtRecEtapaRecadastramento;
	
	/** Atributo qtRecFaseEtapaRecadastramento. */
	private String qtRecFaseEtapaRecadastramento;
	
	/** Atributo dtRecInicioRecadastramento. */
	private String dtRecInicioRecadastramento;
	
	/** Atributo dtRecFinalRecadastramento. */
	private String dtRecFinalRecadastramento;
	
	/** Atributo dsRecGeradorRetornoInternet. */
	private String dsRecGeradorRetornoInternet;
	
	//Aviso de Recadastramento  
	/** Atributo dsRecTipoModalidadeAviso. */
	private String dsRecTipoModalidadeAviso;
    
    /** Atributo dsRecMomentoEnvioAviso. */
    private String dsRecMomentoEnvioAviso;
    
    /** Atributo dsRecDestinoAviso. */
    private String dsRecDestinoAviso;
    
    /** Atributo dsRecCadastroEnderecoUtilizadoAviso. */
    private String dsRecCadastroEnderecoUtilizadoAviso;
    
    /** Atributo qtRecDiaAvisoAntecipadoAviso. */
    private String qtRecDiaAvisoAntecipadoAviso;
    
    /** Atributo dsRecPermissaoAgrupamentoAviso. */
    private String dsRecPermissaoAgrupamentoAviso;
    
  //Formulário de Recadastramento
	/** Atributo dsRecTipoModalidadeFormulario. */
  private String dsRecTipoModalidadeFormulario;
    
    /** Atributo dsRecMomentoEnvioFormulario. */
    private String dsRecMomentoEnvioFormulario;
    
    /** Atributo dsRecDestinoFormulario. */
    private String dsRecDestinoFormulario;
    
    /** Atributo dsRecCadastroEnderecoUtilizadoFormulario. */
    private String dsRecCadastroEnderecoUtilizadoFormulario;
    
    /** Atributo qtRecDiaAntecipadoFormulario. */
    private String qtRecDiaAntecipadoFormulario;
    
    /** Atributo dsRecPermissaoAgrupamentoFormulario. */
    private String dsRecPermissaoAgrupamentoFormulario;
    
	/**
	 * Get: dsAvisoTipoAvisoFavorecido.
	 *
	 * @return dsAvisoTipoAvisoFavorecido
	 */
	public String getDsAvisoTipoAvisoFavorecido() {
		return dsAvisoTipoAvisoFavorecido;
	}
	
	/**
	 * Set: dsAvisoTipoAvisoFavorecido.
	 *
	 * @param dsAvisoTipoAvisoFavorecido the ds aviso tipo aviso favorecido
	 */
	public void setDsAvisoTipoAvisoFavorecido(String dsAvisoTipoAvisoFavorecido) {
		this.dsAvisoTipoAvisoFavorecido = dsAvisoTipoAvisoFavorecido;
	}
	
	/**
	 * Get: dsAvisoPeriodicidadeEmissaoFavorecido.
	 *
	 * @return dsAvisoPeriodicidadeEmissaoFavorecido
	 */
	public String getDsAvisoPeriodicidadeEmissaoFavorecido() {
		return dsAvisoPeriodicidadeEmissaoFavorecido;
	}
	
	/**
	 * Set: dsAvisoPeriodicidadeEmissaoFavorecido.
	 *
	 * @param dsAvisoPeriodicidadeEmissaoFavorecido the ds aviso periodicidade emissao favorecido
	 */
	public void setDsAvisoPeriodicidadeEmissaoFavorecido(String dsAvisoPeriodicidadeEmissaoFavorecido) {
		this.dsAvisoPeriodicidadeEmissaoFavorecido = dsAvisoPeriodicidadeEmissaoFavorecido;
	}
	
	/**
	 * Get: dsAvisoDestinoFavorecido.
	 *
	 * @return dsAvisoDestinoFavorecido
	 */
	public String getDsAvisoDestinoFavorecido() {
		return dsAvisoDestinoFavorecido;
	}
	
	/**
	 * Set: dsAvisoDestinoFavorecido.
	 *
	 * @param dsAvisoDestinoFavorecido the ds aviso destino favorecido
	 */
	public void setDsAvisoDestinoFavorecido(String dsAvisoDestinoFavorecido) {
		this.dsAvisoDestinoFavorecido = dsAvisoDestinoFavorecido;
	}
	
	/**
	 * Get: dsAvisoPermissaoCorrespondenciaAberturaFavorecido.
	 *
	 * @return dsAvisoPermissaoCorrespondenciaAberturaFavorecido
	 */
	public String getDsAvisoPermissaoCorrespondenciaAberturaFavorecido() {
		return dsAvisoPermissaoCorrespondenciaAberturaFavorecido;
	}
	
	/**
	 * Set: dsAvisoPermissaoCorrespondenciaAberturaFavorecido.
	 *
	 * @param dsAvisoPermissaoCorrespondenciaAberturaFavorecido the ds aviso permissao correspondencia abertura favorecido
	 */
	public void setDsAvisoPermissaoCorrespondenciaAberturaFavorecido(
					String dsAvisoPermissaoCorrespondenciaAberturaFavorecido) {
		this.dsAvisoPermissaoCorrespondenciaAberturaFavorecido = dsAvisoPermissaoCorrespondenciaAberturaFavorecido;
	}
	
	/**
	 * Get: dsAvisoDemontracaoInformacaoReservadaFavorecido.
	 *
	 * @return dsAvisoDemontracaoInformacaoReservadaFavorecido
	 */
	public String getDsAvisoDemontracaoInformacaoReservadaFavorecido() {
		return dsAvisoDemontracaoInformacaoReservadaFavorecido;
	}
	
	/**
	 * Set: dsAvisoDemontracaoInformacaoReservadaFavorecido.
	 *
	 * @param dsAvisoDemontracaoInformacaoReservadaFavorecido the ds aviso demontracao informacao reservada favorecido
	 */
	public void setDsAvisoDemontracaoInformacaoReservadaFavorecido(String dsAvisoDemontracaoInformacaoReservadaFavorecido) {
		this.dsAvisoDemontracaoInformacaoReservadaFavorecido = dsAvisoDemontracaoInformacaoReservadaFavorecido;
	}
	
	/**
	 * Get: cdAvisoQuantidadeViasEmitirFavorecido.
	 *
	 * @return cdAvisoQuantidadeViasEmitirFavorecido
	 */
	public String getCdAvisoQuantidadeViasEmitirFavorecido() {
		return cdAvisoQuantidadeViasEmitirFavorecido;
	}
	
	/**
	 * Set: cdAvisoQuantidadeViasEmitirFavorecido.
	 *
	 * @param cdAvisoQuantidadeViasEmitirFavorecido the cd aviso quantidade vias emitir favorecido
	 */
	public void setCdAvisoQuantidadeViasEmitirFavorecido(String cdAvisoQuantidadeViasEmitirFavorecido) {
		this.cdAvisoQuantidadeViasEmitirFavorecido = cdAvisoQuantidadeViasEmitirFavorecido;
	}
	
	/**
	 * Get: dsAvisoAgrupamentoCorrespondenciaFavorecido.
	 *
	 * @return dsAvisoAgrupamentoCorrespondenciaFavorecido
	 */
	public String getDsAvisoAgrupamentoCorrespondenciaFavorecido() {
		return dsAvisoAgrupamentoCorrespondenciaFavorecido;
	}
	
	/**
	 * Set: dsAvisoAgrupamentoCorrespondenciaFavorecido.
	 *
	 * @param dsAvisoAgrupamentoCorrespondenciaFavorecido the ds aviso agrupamento correspondencia favorecido
	 */
	public void setDsAvisoAgrupamentoCorrespondenciaFavorecido(String dsAvisoAgrupamentoCorrespondenciaFavorecido) {
		this.dsAvisoAgrupamentoCorrespondenciaFavorecido = dsAvisoAgrupamentoCorrespondenciaFavorecido;
	}
	
	/**
	 * Get: dsAvisoTipoAvisoPagador.
	 *
	 * @return dsAvisoTipoAvisoPagador
	 */
	public String getDsAvisoTipoAvisoPagador() {
		return dsAvisoTipoAvisoPagador;
	}
	
	/**
	 * Set: dsAvisoTipoAvisoPagador.
	 *
	 * @param dsAvisoTipoAvisoPagador the ds aviso tipo aviso pagador
	 */
	public void setDsAvisoTipoAvisoPagador(String dsAvisoTipoAvisoPagador) {
		this.dsAvisoTipoAvisoPagador = dsAvisoTipoAvisoPagador;
	}
	
	/**
	 * Get: dsAvisoPeriodicidadeEmissaoPagador.
	 *
	 * @return dsAvisoPeriodicidadeEmissaoPagador
	 */
	public String getDsAvisoPeriodicidadeEmissaoPagador() {
		return dsAvisoPeriodicidadeEmissaoPagador;
	}
	
	/**
	 * Set: dsAvisoPeriodicidadeEmissaoPagador.
	 *
	 * @param dsAvisoPeriodicidadeEmissaoPagador the ds aviso periodicidade emissao pagador
	 */
	public void setDsAvisoPeriodicidadeEmissaoPagador(String dsAvisoPeriodicidadeEmissaoPagador) {
		this.dsAvisoPeriodicidadeEmissaoPagador = dsAvisoPeriodicidadeEmissaoPagador;
	}
	
	/**
	 * Get: dsAvisoDestinoPagador.
	 *
	 * @return dsAvisoDestinoPagador
	 */
	public String getDsAvisoDestinoPagador() {
		return dsAvisoDestinoPagador;
	}
	
	/**
	 * Set: dsAvisoDestinoPagador.
	 *
	 * @param dsAvisoDestinoPagador the ds aviso destino pagador
	 */
	public void setDsAvisoDestinoPagador(String dsAvisoDestinoPagador) {
		this.dsAvisoDestinoPagador = dsAvisoDestinoPagador;
	}
	
	/**
	 * Get: dsAvisoPermissaoCorrespondenciaAberturaPagador.
	 *
	 * @return dsAvisoPermissaoCorrespondenciaAberturaPagador
	 */
	public String getDsAvisoPermissaoCorrespondenciaAberturaPagador() {
		return dsAvisoPermissaoCorrespondenciaAberturaPagador;
	}
	
	/**
	 * Set: dsAvisoPermissaoCorrespondenciaAberturaPagador.
	 *
	 * @param dsAvisoPermissaoCorrespondenciaAberturaPagador the ds aviso permissao correspondencia abertura pagador
	 */
	public void setDsAvisoPermissaoCorrespondenciaAberturaPagador(String dsAvisoPermissaoCorrespondenciaAberturaPagador) {
		this.dsAvisoPermissaoCorrespondenciaAberturaPagador = dsAvisoPermissaoCorrespondenciaAberturaPagador;
	}
	
	/**
	 * Get: dsAvisoDemontracaoInformacaoReservadaPagador.
	 *
	 * @return dsAvisoDemontracaoInformacaoReservadaPagador
	 */
	public String getDsAvisoDemontracaoInformacaoReservadaPagador() {
		return dsAvisoDemontracaoInformacaoReservadaPagador;
	}
	
	/**
	 * Set: dsAvisoDemontracaoInformacaoReservadaPagador.
	 *
	 * @param dsAvisoDemontracaoInformacaoReservadaPagador the ds aviso demontracao informacao reservada pagador
	 */
	public void setDsAvisoDemontracaoInformacaoReservadaPagador(String dsAvisoDemontracaoInformacaoReservadaPagador) {
		this.dsAvisoDemontracaoInformacaoReservadaPagador = dsAvisoDemontracaoInformacaoReservadaPagador;
	}
	
	/**
	 * Get: cdAvisoQuantidadeViasEmitirPagador.
	 *
	 * @return cdAvisoQuantidadeViasEmitirPagador
	 */
	public String getCdAvisoQuantidadeViasEmitirPagador() {
		return cdAvisoQuantidadeViasEmitirPagador;
	}
	
	/**
	 * Set: cdAvisoQuantidadeViasEmitirPagador.
	 *
	 * @param cdAvisoQuantidadeViasEmitirPagador the cd aviso quantidade vias emitir pagador
	 */
	public void setCdAvisoQuantidadeViasEmitirPagador(String cdAvisoQuantidadeViasEmitirPagador) {
		this.cdAvisoQuantidadeViasEmitirPagador = cdAvisoQuantidadeViasEmitirPagador;
	}
	
	/**
	 * Get: dsAvisoAgrupamentoCorrespondenciaPagador.
	 *
	 * @return dsAvisoAgrupamentoCorrespondenciaPagador
	 */
	public String getDsAvisoAgrupamentoCorrespondenciaPagador() {
		return dsAvisoAgrupamentoCorrespondenciaPagador;
	}
	
	/**
	 * Set: dsAvisoAgrupamentoCorrespondenciaPagador.
	 *
	 * @param dsAvisoAgrupamentoCorrespondenciaPagador the ds aviso agrupamento correspondencia pagador
	 */
	public void setDsAvisoAgrupamentoCorrespondenciaPagador(String dsAvisoAgrupamentoCorrespondenciaPagador) {
		this.dsAvisoAgrupamentoCorrespondenciaPagador = dsAvisoAgrupamentoCorrespondenciaPagador;
	}
	
	/**
	 * Get: cdComprovanteTipoComprovantePagador.
	 *
	 * @return cdComprovanteTipoComprovantePagador
	 */
	public String getCdComprovanteTipoComprovantePagador() {
		return cdComprovanteTipoComprovantePagador;
	}
	
	/**
	 * Set: cdComprovanteTipoComprovantePagador.
	 *
	 * @param cdComprovanteTipoComprovantePagador the cd comprovante tipo comprovante pagador
	 */
	public void setCdComprovanteTipoComprovantePagador(String cdComprovanteTipoComprovantePagador) {
		this.cdComprovanteTipoComprovantePagador = cdComprovanteTipoComprovantePagador;
	}
	
	/**
	 * Get: dsComprovantePeriodicidadeEmissaoPagador.
	 *
	 * @return dsComprovantePeriodicidadeEmissaoPagador
	 */
	public String getDsComprovantePeriodicidadeEmissaoPagador() {
		return dsComprovantePeriodicidadeEmissaoPagador;
	}
	
	/**
	 * Set: dsComprovantePeriodicidadeEmissaoPagador.
	 *
	 * @param dsComprovantePeriodicidadeEmissaoPagador the ds comprovante periodicidade emissao pagador
	 */
	public void setDsComprovantePeriodicidadeEmissaoPagador(String dsComprovantePeriodicidadeEmissaoPagador) {
		this.dsComprovantePeriodicidadeEmissaoPagador = dsComprovantePeriodicidadeEmissaoPagador;
	}
	
	/**
	 * Get: dsComprovanteDestinoPagador.
	 *
	 * @return dsComprovanteDestinoPagador
	 */
	public String getDsComprovanteDestinoPagador() {
		return dsComprovanteDestinoPagador;
	}
	
	/**
	 * Set: dsComprovanteDestinoPagador.
	 *
	 * @param dsComprovanteDestinoPagador the ds comprovante destino pagador
	 */
	public void setDsComprovanteDestinoPagador(String dsComprovanteDestinoPagador) {
		this.dsComprovanteDestinoPagador = dsComprovanteDestinoPagador;
	}
	
	/**
	 * Get: dsComprovantePermissaoCorrespondenteAberturaPagador.
	 *
	 * @return dsComprovantePermissaoCorrespondenteAberturaPagador
	 */
	public String getDsComprovantePermissaoCorrespondenteAberturaPagador() {
		return dsComprovantePermissaoCorrespondenteAberturaPagador;
	}
	
	/**
	 * Set: dsComprovantePermissaoCorrespondenteAberturaPagador.
	 *
	 * @param dsComprovantePermissaoCorrespondenteAberturaPagador the ds comprovante permissao correspondente abertura pagador
	 */
	public void setDsComprovantePermissaoCorrespondenteAberturaPagador(
					String dsComprovantePermissaoCorrespondenteAberturaPagador) {
		this.dsComprovantePermissaoCorrespondenteAberturaPagador = dsComprovantePermissaoCorrespondenteAberturaPagador;
	}
	
	/**
	 * Get: dsDemonstrativoInformacaoReservadaPagador.
	 *
	 * @return dsDemonstrativoInformacaoReservadaPagador
	 */
	public String getDsDemonstrativoInformacaoReservadaPagador() {
		return dsDemonstrativoInformacaoReservadaPagador;
	}
	
	/**
	 * Set: dsDemonstrativoInformacaoReservadaPagador.
	 *
	 * @param dsDemonstrativoInformacaoReservadaPagador the ds demonstrativo informacao reservada pagador
	 */
	public void setDsDemonstrativoInformacaoReservadaPagador(String dsDemonstrativoInformacaoReservadaPagador) {
		this.dsDemonstrativoInformacaoReservadaPagador = dsDemonstrativoInformacaoReservadaPagador;
	}
	
	/**
	 * Get: qtViasEmitirPagador.
	 *
	 * @return qtViasEmitirPagador
	 */
	public String getQtViasEmitirPagador() {
		return qtViasEmitirPagador;
	}
	
	/**
	 * Set: qtViasEmitirPagador.
	 *
	 * @param qtViasEmitirPagador the qt vias emitir pagador
	 */
	public void setQtViasEmitirPagador(String qtViasEmitirPagador) {
		this.qtViasEmitirPagador = qtViasEmitirPagador;
	}
	
	/**
	 * Get: dsAgrupamentoCorrespondentePagador.
	 *
	 * @return dsAgrupamentoCorrespondentePagador
	 */
	public String getDsAgrupamentoCorrespondentePagador() {
		return dsAgrupamentoCorrespondentePagador;
	}
	
	/**
	 * Set: dsAgrupamentoCorrespondentePagador.
	 *
	 * @param dsAgrupamentoCorrespondentePagador the ds agrupamento correspondente pagador
	 */
	public void setDsAgrupamentoCorrespondentePagador(String dsAgrupamentoCorrespondentePagador) {
		this.dsAgrupamentoCorrespondentePagador = dsAgrupamentoCorrespondentePagador;
	}
	
	/**
	 * Get: cdComprovanteTipoComprovanteFavorecido.
	 *
	 * @return cdComprovanteTipoComprovanteFavorecido
	 */
	public String getCdComprovanteTipoComprovanteFavorecido() {
		return cdComprovanteTipoComprovanteFavorecido;
	}
	
	/**
	 * Set: cdComprovanteTipoComprovanteFavorecido.
	 *
	 * @param cdComprovanteTipoComprovanteFavorecido the cd comprovante tipo comprovante favorecido
	 */
	public void setCdComprovanteTipoComprovanteFavorecido(String cdComprovanteTipoComprovanteFavorecido) {
		this.cdComprovanteTipoComprovanteFavorecido = cdComprovanteTipoComprovanteFavorecido;
	}
	
	/**
	 * Get: dsComprovantePeriodicidadeEmissaoFavorecido.
	 *
	 * @return dsComprovantePeriodicidadeEmissaoFavorecido
	 */
	public String getDsComprovantePeriodicidadeEmissaoFavorecido() {
		return dsComprovantePeriodicidadeEmissaoFavorecido;
	}
	
	/**
	 * Set: dsComprovantePeriodicidadeEmissaoFavorecido.
	 *
	 * @param dsComprovantePeriodicidadeEmissaoFavorecido the ds comprovante periodicidade emissao favorecido
	 */
	public void setDsComprovantePeriodicidadeEmissaoFavorecido(String dsComprovantePeriodicidadeEmissaoFavorecido) {
		this.dsComprovantePeriodicidadeEmissaoFavorecido = dsComprovantePeriodicidadeEmissaoFavorecido;
	}
	
	/**
	 * Get: dsComprovanteDestinoFavorecido.
	 *
	 * @return dsComprovanteDestinoFavorecido
	 */
	public String getDsComprovanteDestinoFavorecido() {
		return dsComprovanteDestinoFavorecido;
	}
	
	/**
	 * Set: dsComprovanteDestinoFavorecido.
	 *
	 * @param dsComprovanteDestinoFavorecido the ds comprovante destino favorecido
	 */
	public void setDsComprovanteDestinoFavorecido(String dsComprovanteDestinoFavorecido) {
		this.dsComprovanteDestinoFavorecido = dsComprovanteDestinoFavorecido;
	}
	
	/**
	 * Get: dsComprovantePermissaoCorrespondenteAberturaFavorecido.
	 *
	 * @return dsComprovantePermissaoCorrespondenteAberturaFavorecido
	 */
	public String getDsComprovantePermissaoCorrespondenteAberturaFavorecido() {
		return dsComprovantePermissaoCorrespondenteAberturaFavorecido;
	}
	
	/**
	 * Set: dsComprovantePermissaoCorrespondenteAberturaFavorecido.
	 *
	 * @param dsComprovantePermissaoCorrespondenteAberturaFavorecido the ds comprovante permissao correspondente abertura favorecido
	 */
	public void setDsComprovantePermissaoCorrespondenteAberturaFavorecido(
					String dsComprovantePermissaoCorrespondenteAberturaFavorecido) {
		this.dsComprovantePermissaoCorrespondenteAberturaFavorecido = dsComprovantePermissaoCorrespondenteAberturaFavorecido;
	}
	
	/**
	 * Get: dsDemonstrativoInformacaoReservadaFavorecido.
	 *
	 * @return dsDemonstrativoInformacaoReservadaFavorecido
	 */
	public String getDsDemonstrativoInformacaoReservadaFavorecido() {
		return dsDemonstrativoInformacaoReservadaFavorecido;
	}
	
	/**
	 * Set: dsDemonstrativoInformacaoReservadaFavorecido.
	 *
	 * @param dsDemonstrativoInformacaoReservadaFavorecido the ds demonstrativo informacao reservada favorecido
	 */
	public void setDsDemonstrativoInformacaoReservadaFavorecido(String dsDemonstrativoInformacaoReservadaFavorecido) {
		this.dsDemonstrativoInformacaoReservadaFavorecido = dsDemonstrativoInformacaoReservadaFavorecido;
	}
	
	/**
	 * Get: qtViasEmitirFavorecido.
	 *
	 * @return qtViasEmitirFavorecido
	 */
	public String getQtViasEmitirFavorecido() {
		return qtViasEmitirFavorecido;
	}
	
	/**
	 * Set: qtViasEmitirFavorecido.
	 *
	 * @param qtViasEmitirFavorecido the qt vias emitir favorecido
	 */
	public void setQtViasEmitirFavorecido(String qtViasEmitirFavorecido) {
		this.qtViasEmitirFavorecido = qtViasEmitirFavorecido;
	}
	
	/**
	 * Get: dsAgrupamentoCorrespondenteFavorecido.
	 *
	 * @return dsAgrupamentoCorrespondenteFavorecido
	 */
	public String getDsAgrupamentoCorrespondenteFavorecido() {
		return dsAgrupamentoCorrespondenteFavorecido;
	}
	
	/**
	 * Set: dsAgrupamentoCorrespondenteFavorecido.
	 *
	 * @param dsAgrupamentoCorrespondenteFavorecido the ds agrupamento correspondente favorecido
	 */
	public void setDsAgrupamentoCorrespondenteFavorecido(String dsAgrupamentoCorrespondenteFavorecido) {
		this.dsAgrupamentoCorrespondenteFavorecido = dsAgrupamentoCorrespondenteFavorecido;
	}
	
	/**
	 * Get: dsCsdTipoComprovanteSalarial.
	 *
	 * @return dsCsdTipoComprovanteSalarial
	 */
	public String getDsCsdTipoComprovanteSalarial() {
		return dsCsdTipoComprovanteSalarial;
	}
	
	/**
	 * Set: dsCsdTipoComprovanteSalarial.
	 *
	 * @param dsCsdTipoComprovanteSalarial the ds csd tipo comprovante salarial
	 */
	public void setDsCsdTipoComprovanteSalarial(String dsCsdTipoComprovanteSalarial) {
		this.dsCsdTipoComprovanteSalarial = dsCsdTipoComprovanteSalarial;
	}
	
	/**
	 * Get: dsCsdTipoRejeicaoLoteSalarial.
	 *
	 * @return dsCsdTipoRejeicaoLoteSalarial
	 */
	public String getDsCsdTipoRejeicaoLoteSalarial() {
		return dsCsdTipoRejeicaoLoteSalarial;
	}
	
	/**
	 * Set: dsCsdTipoRejeicaoLoteSalarial.
	 *
	 * @param dsCsdTipoRejeicaoLoteSalarial the ds csd tipo rejeicao lote salarial
	 */
	public void setDsCsdTipoRejeicaoLoteSalarial(String dsCsdTipoRejeicaoLoteSalarial) {
		this.dsCsdTipoRejeicaoLoteSalarial = dsCsdTipoRejeicaoLoteSalarial;
	}
	
	/**
	 * Get: dsCsdMeioDisponibilizacaoCorrentistaSalarial.
	 *
	 * @return dsCsdMeioDisponibilizacaoCorrentistaSalarial
	 */
	public String getDsCsdMeioDisponibilizacaoCorrentistaSalarial() {
		return dsCsdMeioDisponibilizacaoCorrentistaSalarial;
	}
	
	/**
	 * Set: dsCsdMeioDisponibilizacaoCorrentistaSalarial.
	 *
	 * @param dsCsdMeioDisponibilizacaoCorrentistaSalarial the ds csd meio disponibilizacao correntista salarial
	 */
	public void setDsCsdMeioDisponibilizacaoCorrentistaSalarial(String dsCsdMeioDisponibilizacaoCorrentistaSalarial) {
		this.dsCsdMeioDisponibilizacaoCorrentistaSalarial = dsCsdMeioDisponibilizacaoCorrentistaSalarial;
	}
	
	/**
	 * Get: dsCsdMediaDisponibilidadeCorrentistaSalarial.
	 *
	 * @return dsCsdMediaDisponibilidadeCorrentistaSalarial
	 */
	public String getDsCsdMediaDisponibilidadeCorrentistaSalarial() {
		return dsCsdMediaDisponibilidadeCorrentistaSalarial;
	}
	
	/**
	 * Set: dsCsdMediaDisponibilidadeCorrentistaSalarial.
	 *
	 * @param dsCsdMediaDisponibilidadeCorrentistaSalarial the ds csd media disponibilidade correntista salarial
	 */
	public void setDsCsdMediaDisponibilidadeCorrentistaSalarial(String dsCsdMediaDisponibilidadeCorrentistaSalarial) {
		this.dsCsdMediaDisponibilidadeCorrentistaSalarial = dsCsdMediaDisponibilidadeCorrentistaSalarial;
	}
	
	/**
	 * Get: dsCsdMediaDisponibilidadeNumeroCorrentistasSalarial.
	 *
	 * @return dsCsdMediaDisponibilidadeNumeroCorrentistasSalarial
	 */
	public String getDsCsdMediaDisponibilidadeNumeroCorrentistasSalarial() {
		return dsCsdMediaDisponibilidadeNumeroCorrentistasSalarial;
	}
	
	/**
	 * Set: dsCsdMediaDisponibilidadeNumeroCorrentistasSalarial.
	 *
	 * @param dsCsdMediaDisponibilidadeNumeroCorrentistasSalarial the ds csd media disponibilidade numero correntistas salarial
	 */
	public void setDsCsdMediaDisponibilidadeNumeroCorrentistasSalarial(
					String dsCsdMediaDisponibilidadeNumeroCorrentistasSalarial) {
		this.dsCsdMediaDisponibilidadeNumeroCorrentistasSalarial = dsCsdMediaDisponibilidadeNumeroCorrentistasSalarial;
	}
	
	/**
	 * Get: dsCsdDestinoSalarial.
	 *
	 * @return dsCsdDestinoSalarial
	 */
	public String getDsCsdDestinoSalarial() {
		return dsCsdDestinoSalarial;
	}
	
	/**
	 * Set: dsCsdDestinoSalarial.
	 *
	 * @param dsCsdDestinoSalarial the ds csd destino salarial
	 */
	public void setDsCsdDestinoSalarial(String dsCsdDestinoSalarial) {
		this.dsCsdDestinoSalarial = dsCsdDestinoSalarial;
	}
	
	/**
	 * Get: qtCsdMesEmissaoSalarial.
	 *
	 * @return qtCsdMesEmissaoSalarial
	 */
	public String getQtCsdMesEmissaoSalarial() {
		return qtCsdMesEmissaoSalarial;
	}
	
	/**
	 * Set: qtCsdMesEmissaoSalarial.
	 *
	 * @param qtCsdMesEmissaoSalarial the qt csd mes emissao salarial
	 */
	public void setQtCsdMesEmissaoSalarial(String qtCsdMesEmissaoSalarial) {
		this.qtCsdMesEmissaoSalarial = qtCsdMesEmissaoSalarial;
	}
	
	/**
	 * Get: dsCsdCadastroConservadorEnderecoSalarial.
	 *
	 * @return dsCsdCadastroConservadorEnderecoSalarial
	 */
	public String getDsCsdCadastroConservadorEnderecoSalarial() {
		return dsCsdCadastroConservadorEnderecoSalarial;
	}
	
	/**
	 * Set: dsCsdCadastroConservadorEnderecoSalarial.
	 *
	 * @param dsCsdCadastroConservadorEnderecoSalarial the ds csd cadastro conservador endereco salarial
	 */
	public void setDsCsdCadastroConservadorEnderecoSalarial(String dsCsdCadastroConservadorEnderecoSalarial) {
		this.dsCsdCadastroConservadorEnderecoSalarial = dsCsdCadastroConservadorEnderecoSalarial;
	}
	
	/**
	 * Get: cdCsdQuantidadeViasEmitirSalarial.
	 *
	 * @return cdCsdQuantidadeViasEmitirSalarial
	 */
	public String getCdCsdQuantidadeViasEmitirSalarial() {
		return cdCsdQuantidadeViasEmitirSalarial;
	}
	
	/**
	 * Set: cdCsdQuantidadeViasEmitirSalarial.
	 *
	 * @param cdCsdQuantidadeViasEmitirSalarial the cd csd quantidade vias emitir salarial
	 */
	public void setCdCsdQuantidadeViasEmitirSalarial(String cdCsdQuantidadeViasEmitirSalarial) {
		this.cdCsdQuantidadeViasEmitirSalarial = cdCsdQuantidadeViasEmitirSalarial;
	}
	
	/**
	 * Get: dsCsdTipoComprovanteDiversos.
	 *
	 * @return dsCsdTipoComprovanteDiversos
	 */
	public String getDsCsdTipoComprovanteDiversos() {
		return dsCsdTipoComprovanteDiversos;
	}
	
	/**
	 * Set: dsCsdTipoComprovanteDiversos.
	 *
	 * @param dsCsdTipoComprovanteDiversos the ds csd tipo comprovante diversos
	 */
	public void setDsCsdTipoComprovanteDiversos(String dsCsdTipoComprovanteDiversos) {
		this.dsCsdTipoComprovanteDiversos = dsCsdTipoComprovanteDiversos;
	}
	
	/**
	 * Get: dsCsdTipoRejeicaoLoteDiversos.
	 *
	 * @return dsCsdTipoRejeicaoLoteDiversos
	 */
	public String getDsCsdTipoRejeicaoLoteDiversos() {
		return dsCsdTipoRejeicaoLoteDiversos;
	}
	
	/**
	 * Set: dsCsdTipoRejeicaoLoteDiversos.
	 *
	 * @param dsCsdTipoRejeicaoLoteDiversos the ds csd tipo rejeicao lote diversos
	 */
	public void setDsCsdTipoRejeicaoLoteDiversos(String dsCsdTipoRejeicaoLoteDiversos) {
		this.dsCsdTipoRejeicaoLoteDiversos = dsCsdTipoRejeicaoLoteDiversos;
	}
	
	/**
	 * Get: dsCsdMeioDisponibilizacaoCorrentistaDiversos.
	 *
	 * @return dsCsdMeioDisponibilizacaoCorrentistaDiversos
	 */
	public String getDsCsdMeioDisponibilizacaoCorrentistaDiversos() {
		return dsCsdMeioDisponibilizacaoCorrentistaDiversos;
	}
	
	/**
	 * Set: dsCsdMeioDisponibilizacaoCorrentistaDiversos.
	 *
	 * @param dsCsdMeioDisponibilizacaoCorrentistaDiversos the ds csd meio disponibilizacao correntista diversos
	 */
	public void setDsCsdMeioDisponibilizacaoCorrentistaDiversos(String dsCsdMeioDisponibilizacaoCorrentistaDiversos) {
		this.dsCsdMeioDisponibilizacaoCorrentistaDiversos = dsCsdMeioDisponibilizacaoCorrentistaDiversos;
	}
	
	/**
	 * Get: dsCsdMediaDisponibilidadeCorrentistaDiversos.
	 *
	 * @return dsCsdMediaDisponibilidadeCorrentistaDiversos
	 */
	public String getDsCsdMediaDisponibilidadeCorrentistaDiversos() {
		return dsCsdMediaDisponibilidadeCorrentistaDiversos;
	}
	
	/**
	 * Set: dsCsdMediaDisponibilidadeCorrentistaDiversos.
	 *
	 * @param dsCsdMediaDisponibilidadeCorrentistaDiversos the ds csd media disponibilidade correntista diversos
	 */
	public void setDsCsdMediaDisponibilidadeCorrentistaDiversos(String dsCsdMediaDisponibilidadeCorrentistaDiversos) {
		this.dsCsdMediaDisponibilidadeCorrentistaDiversos = dsCsdMediaDisponibilidadeCorrentistaDiversos;
	}
	
	/**
	 * Get: dsCsdMediaDisponibilidadeNumeroCorrentistasDiversos.
	 *
	 * @return dsCsdMediaDisponibilidadeNumeroCorrentistasDiversos
	 */
	public String getDsCsdMediaDisponibilidadeNumeroCorrentistasDiversos() {
		return dsCsdMediaDisponibilidadeNumeroCorrentistasDiversos;
	}
	
	/**
	 * Set: dsCsdMediaDisponibilidadeNumeroCorrentistasDiversos.
	 *
	 * @param dsCsdMediaDisponibilidadeNumeroCorrentistasDiversos the ds csd media disponibilidade numero correntistas diversos
	 */
	public void setDsCsdMediaDisponibilidadeNumeroCorrentistasDiversos(
					String dsCsdMediaDisponibilidadeNumeroCorrentistasDiversos) {
		this.dsCsdMediaDisponibilidadeNumeroCorrentistasDiversos = dsCsdMediaDisponibilidadeNumeroCorrentistasDiversos;
	}
	
	/**
	 * Get: dsCsdDestinoDiversos.
	 *
	 * @return dsCsdDestinoDiversos
	 */
	public String getDsCsdDestinoDiversos() {
		return dsCsdDestinoDiversos;
	}
	
	/**
	 * Set: dsCsdDestinoDiversos.
	 *
	 * @param dsCsdDestinoDiversos the ds csd destino diversos
	 */
	public void setDsCsdDestinoDiversos(String dsCsdDestinoDiversos) {
		this.dsCsdDestinoDiversos = dsCsdDestinoDiversos;
	}
	
	/**
	 * Get: qtCsdMesEmissaoDiversos.
	 *
	 * @return qtCsdMesEmissaoDiversos
	 */
	public String getQtCsdMesEmissaoDiversos() {
		return qtCsdMesEmissaoDiversos;
	}
	
	/**
	 * Set: qtCsdMesEmissaoDiversos.
	 *
	 * @param qtCsdMesEmissaoDiversos the qt csd mes emissao diversos
	 */
	public void setQtCsdMesEmissaoDiversos(String qtCsdMesEmissaoDiversos) {
		this.qtCsdMesEmissaoDiversos = qtCsdMesEmissaoDiversos;
	}
	
	/**
	 * Get: dsCsdCadastroConservadorEnderecoDiversos.
	 *
	 * @return dsCsdCadastroConservadorEnderecoDiversos
	 */
	public String getDsCsdCadastroConservadorEnderecoDiversos() {
		return dsCsdCadastroConservadorEnderecoDiversos;
	}
	
	/**
	 * Set: dsCsdCadastroConservadorEnderecoDiversos.
	 *
	 * @param dsCsdCadastroConservadorEnderecoDiversos the ds csd cadastro conservador endereco diversos
	 */
	public void setDsCsdCadastroConservadorEnderecoDiversos(String dsCsdCadastroConservadorEnderecoDiversos) {
		this.dsCsdCadastroConservadorEnderecoDiversos = dsCsdCadastroConservadorEnderecoDiversos;
	}
	
	/**
	 * Get: cdCsdQuantidadeViasEmitirDiversos.
	 *
	 * @return cdCsdQuantidadeViasEmitirDiversos
	 */
	public String getCdCsdQuantidadeViasEmitirDiversos() {
		return cdCsdQuantidadeViasEmitirDiversos;
	}
	
	/**
	 * Set: cdCsdQuantidadeViasEmitirDiversos.
	 *
	 * @param cdCsdQuantidadeViasEmitirDiversos the cd csd quantidade vias emitir diversos
	 */
	public void setCdCsdQuantidadeViasEmitirDiversos(String cdCsdQuantidadeViasEmitirDiversos) {
		this.cdCsdQuantidadeViasEmitirDiversos = cdCsdQuantidadeViasEmitirDiversos;
	}
	
	/**
	 * Get: cdIndcadorCadastroFavorecido.
	 *
	 * @return cdIndcadorCadastroFavorecido
	 */
	public String getCdIndcadorCadastroFavorecido() {
		return cdIndcadorCadastroFavorecido;
	}
	
	/**
	 * Set: cdIndcadorCadastroFavorecido.
	 *
	 * @param cdIndcadorCadastroFavorecido the cd indcador cadastro favorecido
	 */
	public void setCdIndcadorCadastroFavorecido(String cdIndcadorCadastroFavorecido) {
		this.cdIndcadorCadastroFavorecido = cdIndcadorCadastroFavorecido;
	}
	
	/**
	 * Get: dsCadastroFormaManutencao.
	 *
	 * @return dsCadastroFormaManutencao
	 */
	public String getDsCadastroFormaManutencao() {
		return dsCadastroFormaManutencao;
	}
	
	/**
	 * Set: dsCadastroFormaManutencao.
	 *
	 * @param dsCadastroFormaManutencao the ds cadastro forma manutencao
	 */
	public void setDsCadastroFormaManutencao(String dsCadastroFormaManutencao) {
		this.dsCadastroFormaManutencao = dsCadastroFormaManutencao;
	}
	
	/**
	 * Get: cdCadastroQuantidadeDiasInativos.
	 *
	 * @return cdCadastroQuantidadeDiasInativos
	 */
	public String getCdCadastroQuantidadeDiasInativos() {
		return cdCadastroQuantidadeDiasInativos;
	}
	
	/**
	 * Set: cdCadastroQuantidadeDiasInativos.
	 *
	 * @param cdCadastroQuantidadeDiasInativos the cd cadastro quantidade dias inativos
	 */
	public void setCdCadastroQuantidadeDiasInativos(String cdCadastroQuantidadeDiasInativos) {
		this.cdCadastroQuantidadeDiasInativos = cdCadastroQuantidadeDiasInativos;
	}
	
	/**
	 * Get: dsCadastroTipoConsistenciaInscricao.
	 *
	 * @return dsCadastroTipoConsistenciaInscricao
	 */
	public String getDsCadastroTipoConsistenciaInscricao() {
		return dsCadastroTipoConsistenciaInscricao;
	}
	
	/**
	 * Set: dsCadastroTipoConsistenciaInscricao.
	 *
	 * @param dsCadastroTipoConsistenciaInscricao the ds cadastro tipo consistencia inscricao
	 */
	public void setDsCadastroTipoConsistenciaInscricao(String dsCadastroTipoConsistenciaInscricao) {
		this.dsCadastroTipoConsistenciaInscricao = dsCadastroTipoConsistenciaInscricao;
	}
	
	/**
	 * Get: dsServicoRecadastro.
	 *
	 * @return dsServicoRecadastro
	 */
	public String getDsServicoRecadastro() {
		return dsServicoRecadastro;
	}
	
	/**
	 * Set: dsServicoRecadastro.
	 *
	 * @param dsServicoRecadastro the ds servico recadastro
	 */
	public void setDsServicoRecadastro(String dsServicoRecadastro) {
		this.dsServicoRecadastro = dsServicoRecadastro;
	}
	
	/**
	 * Get: dsRecTipoIdentificacaoBeneficio.
	 *
	 * @return dsRecTipoIdentificacaoBeneficio
	 */
	public String getDsRecTipoIdentificacaoBeneficio() {
		return dsRecTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Set: dsRecTipoIdentificacaoBeneficio.
	 *
	 * @param dsRecTipoIdentificacaoBeneficio the ds rec tipo identificacao beneficio
	 */
	public void setDsRecTipoIdentificacaoBeneficio(String dsRecTipoIdentificacaoBeneficio) {
		this.dsRecTipoIdentificacaoBeneficio = dsRecTipoIdentificacaoBeneficio;
	}
	
	/**
	 * Get: dsRecTipoConsistenciaIdentificacao.
	 *
	 * @return dsRecTipoConsistenciaIdentificacao
	 */
	public String getDsRecTipoConsistenciaIdentificacao() {
		return dsRecTipoConsistenciaIdentificacao;
	}
	
	/**
	 * Set: dsRecTipoConsistenciaIdentificacao.
	 *
	 * @param dsRecTipoConsistenciaIdentificacao the ds rec tipo consistencia identificacao
	 */
	public void setDsRecTipoConsistenciaIdentificacao(String dsRecTipoConsistenciaIdentificacao) {
		this.dsRecTipoConsistenciaIdentificacao = dsRecTipoConsistenciaIdentificacao;
	}
	
	/**
	 * Get: dsRecCondicaoEnquadramento.
	 *
	 * @return dsRecCondicaoEnquadramento
	 */
	public String getDsRecCondicaoEnquadramento() {
		return dsRecCondicaoEnquadramento;
	}
	
	/**
	 * Set: dsRecCondicaoEnquadramento.
	 *
	 * @param dsRecCondicaoEnquadramento the ds rec condicao enquadramento
	 */
	public void setDsRecCondicaoEnquadramento(String dsRecCondicaoEnquadramento) {
		this.dsRecCondicaoEnquadramento = dsRecCondicaoEnquadramento;
	}
	
	/**
	 * Get: dsRecCriterioPrincipalEnquadramento.
	 *
	 * @return dsRecCriterioPrincipalEnquadramento
	 */
	public String getDsRecCriterioPrincipalEnquadramento() {
		return dsRecCriterioPrincipalEnquadramento;
	}
	
	/**
	 * Set: dsRecCriterioPrincipalEnquadramento.
	 *
	 * @param dsRecCriterioPrincipalEnquadramento the ds rec criterio principal enquadramento
	 */
	public void setDsRecCriterioPrincipalEnquadramento(String dsRecCriterioPrincipalEnquadramento) {
		this.dsRecCriterioPrincipalEnquadramento = dsRecCriterioPrincipalEnquadramento;
	}
	
	/**
	 * Get: dsRecTipoCriterioCompostoEnquadramento.
	 *
	 * @return dsRecTipoCriterioCompostoEnquadramento
	 */
	public String getDsRecTipoCriterioCompostoEnquadramento() {
		return dsRecTipoCriterioCompostoEnquadramento;
	}
	
	/**
	 * Set: dsRecTipoCriterioCompostoEnquadramento.
	 *
	 * @param dsRecTipoCriterioCompostoEnquadramento the ds rec tipo criterio composto enquadramento
	 */
	public void setDsRecTipoCriterioCompostoEnquadramento(String dsRecTipoCriterioCompostoEnquadramento) {
		this.dsRecTipoCriterioCompostoEnquadramento = dsRecTipoCriterioCompostoEnquadramento;
	}
	
	/**
	 * Get: qtRecEtapaRecadastramento.
	 *
	 * @return qtRecEtapaRecadastramento
	 */
	public String getQtRecEtapaRecadastramento() {
		return qtRecEtapaRecadastramento;
	}
	
	/**
	 * Set: qtRecEtapaRecadastramento.
	 *
	 * @param qtRecEtapaRecadastramento the qt rec etapa recadastramento
	 */
	public void setQtRecEtapaRecadastramento(String qtRecEtapaRecadastramento) {
		this.qtRecEtapaRecadastramento = qtRecEtapaRecadastramento;
	}
	
	/**
	 * Get: qtRecFaseEtapaRecadastramento.
	 *
	 * @return qtRecFaseEtapaRecadastramento
	 */
	public String getQtRecFaseEtapaRecadastramento() {
		return qtRecFaseEtapaRecadastramento;
	}
	
	/**
	 * Set: qtRecFaseEtapaRecadastramento.
	 *
	 * @param qtRecFaseEtapaRecadastramento the qt rec fase etapa recadastramento
	 */
	public void setQtRecFaseEtapaRecadastramento(String qtRecFaseEtapaRecadastramento) {
		this.qtRecFaseEtapaRecadastramento = qtRecFaseEtapaRecadastramento;
	}
	
	/**
	 * Get: dtRecInicioRecadastramento.
	 *
	 * @return dtRecInicioRecadastramento
	 */
	public String getDtRecInicioRecadastramento() {
		return dtRecInicioRecadastramento;
	}
	
	/**
	 * Set: dtRecInicioRecadastramento.
	 *
	 * @param dtRecInicioRecadastramento the dt rec inicio recadastramento
	 */
	public void setDtRecInicioRecadastramento(String dtRecInicioRecadastramento) {
		this.dtRecInicioRecadastramento = dtRecInicioRecadastramento;
	}
	
	/**
	 * Get: dtRecFinalRecadastramento.
	 *
	 * @return dtRecFinalRecadastramento
	 */
	public String getDtRecFinalRecadastramento() {
		return dtRecFinalRecadastramento;
	}
	
	/**
	 * Set: dtRecFinalRecadastramento.
	 *
	 * @param dtRecFinalRecadastramento the dt rec final recadastramento
	 */
	public void setDtRecFinalRecadastramento(String dtRecFinalRecadastramento) {
		this.dtRecFinalRecadastramento = dtRecFinalRecadastramento;
	}
	
	/**
	 * Get: dsRecGeradorRetornoInternet.
	 *
	 * @return dsRecGeradorRetornoInternet
	 */
	public String getDsRecGeradorRetornoInternet() {
		return dsRecGeradorRetornoInternet;
	}
	
	/**
	 * Set: dsRecGeradorRetornoInternet.
	 *
	 * @param dsRecGeradorRetornoInternet the ds rec gerador retorno internet
	 */
	public void setDsRecGeradorRetornoInternet(String dsRecGeradorRetornoInternet) {
		this.dsRecGeradorRetornoInternet = dsRecGeradorRetornoInternet;
	}
	
	/**
	 * Get: dsRecTipoModalidadeAviso.
	 *
	 * @return dsRecTipoModalidadeAviso
	 */
	public String getDsRecTipoModalidadeAviso() {
		return dsRecTipoModalidadeAviso;
	}
	
	/**
	 * Set: dsRecTipoModalidadeAviso.
	 *
	 * @param dsRecTipoModalidadeAviso the ds rec tipo modalidade aviso
	 */
	public void setDsRecTipoModalidadeAviso(String dsRecTipoModalidadeAviso) {
		this.dsRecTipoModalidadeAviso = dsRecTipoModalidadeAviso;
	}
	
	/**
	 * Get: dsRecMomentoEnvioAviso.
	 *
	 * @return dsRecMomentoEnvioAviso
	 */
	public String getDsRecMomentoEnvioAviso() {
		return dsRecMomentoEnvioAviso;
	}
	
	/**
	 * Set: dsRecMomentoEnvioAviso.
	 *
	 * @param dsRecMomentoEnvioAviso the ds rec momento envio aviso
	 */
	public void setDsRecMomentoEnvioAviso(String dsRecMomentoEnvioAviso) {
		this.dsRecMomentoEnvioAviso = dsRecMomentoEnvioAviso;
	}
	
	/**
	 * Get: dsRecDestinoAviso.
	 *
	 * @return dsRecDestinoAviso
	 */
	public String getDsRecDestinoAviso() {
		return dsRecDestinoAviso;
	}
	
	/**
	 * Set: dsRecDestinoAviso.
	 *
	 * @param dsRecDestinoAviso the ds rec destino aviso
	 */
	public void setDsRecDestinoAviso(String dsRecDestinoAviso) {
		this.dsRecDestinoAviso = dsRecDestinoAviso;
	}
	
	/**
	 * Get: dsRecCadastroEnderecoUtilizadoAviso.
	 *
	 * @return dsRecCadastroEnderecoUtilizadoAviso
	 */
	public String getDsRecCadastroEnderecoUtilizadoAviso() {
		return dsRecCadastroEnderecoUtilizadoAviso;
	}
	
	/**
	 * Set: dsRecCadastroEnderecoUtilizadoAviso.
	 *
	 * @param dsRecCadastroEnderecoUtilizadoAviso the ds rec cadastro endereco utilizado aviso
	 */
	public void setDsRecCadastroEnderecoUtilizadoAviso(String dsRecCadastroEnderecoUtilizadoAviso) {
		this.dsRecCadastroEnderecoUtilizadoAviso = dsRecCadastroEnderecoUtilizadoAviso;
	}
	
	/**
	 * Get: qtRecDiaAvisoAntecipadoAviso.
	 *
	 * @return qtRecDiaAvisoAntecipadoAviso
	 */
	public String getQtRecDiaAvisoAntecipadoAviso() {
		return qtRecDiaAvisoAntecipadoAviso;
	}
	
	/**
	 * Set: qtRecDiaAvisoAntecipadoAviso.
	 *
	 * @param qtRecDiaAvisoAntecipadoAviso the qt rec dia aviso antecipado aviso
	 */
	public void setQtRecDiaAvisoAntecipadoAviso(String qtRecDiaAvisoAntecipadoAviso) {
		this.qtRecDiaAvisoAntecipadoAviso = qtRecDiaAvisoAntecipadoAviso;
	}
	
	/**
	 * Get: dsRecPermissaoAgrupamentoAviso.
	 *
	 * @return dsRecPermissaoAgrupamentoAviso
	 */
	public String getDsRecPermissaoAgrupamentoAviso() {
		return dsRecPermissaoAgrupamentoAviso;
	}
	
	/**
	 * Set: dsRecPermissaoAgrupamentoAviso.
	 *
	 * @param dsRecPermissaoAgrupamentoAviso the ds rec permissao agrupamento aviso
	 */
	public void setDsRecPermissaoAgrupamentoAviso(String dsRecPermissaoAgrupamentoAviso) {
		this.dsRecPermissaoAgrupamentoAviso = dsRecPermissaoAgrupamentoAviso;
	}
	
	/**
	 * Get: dsRecTipoModalidadeFormulario.
	 *
	 * @return dsRecTipoModalidadeFormulario
	 */
	public String getDsRecTipoModalidadeFormulario() {
		return dsRecTipoModalidadeFormulario;
	}
	
	/**
	 * Set: dsRecTipoModalidadeFormulario.
	 *
	 * @param dsRecTipoModalidadeFormulario the ds rec tipo modalidade formulario
	 */
	public void setDsRecTipoModalidadeFormulario(String dsRecTipoModalidadeFormulario) {
		this.dsRecTipoModalidadeFormulario = dsRecTipoModalidadeFormulario;
	}
	
	/**
	 * Get: dsRecMomentoEnvioFormulario.
	 *
	 * @return dsRecMomentoEnvioFormulario
	 */
	public String getDsRecMomentoEnvioFormulario() {
		return dsRecMomentoEnvioFormulario;
	}
	
	/**
	 * Set: dsRecMomentoEnvioFormulario.
	 *
	 * @param dsRecMomentoEnvioFormulario the ds rec momento envio formulario
	 */
	public void setDsRecMomentoEnvioFormulario(String dsRecMomentoEnvioFormulario) {
		this.dsRecMomentoEnvioFormulario = dsRecMomentoEnvioFormulario;
	}
	
	/**
	 * Get: dsRecDestinoFormulario.
	 *
	 * @return dsRecDestinoFormulario
	 */
	public String getDsRecDestinoFormulario() {
		return dsRecDestinoFormulario;
	}
	
	/**
	 * Set: dsRecDestinoFormulario.
	 *
	 * @param dsRecDestinoFormulario the ds rec destino formulario
	 */
	public void setDsRecDestinoFormulario(String dsRecDestinoFormulario) {
		this.dsRecDestinoFormulario = dsRecDestinoFormulario;
	}
	
	/**
	 * Get: dsRecCadastroEnderecoUtilizadoFormulario.
	 *
	 * @return dsRecCadastroEnderecoUtilizadoFormulario
	 */
	public String getDsRecCadastroEnderecoUtilizadoFormulario() {
		return dsRecCadastroEnderecoUtilizadoFormulario;
	}
	
	/**
	 * Set: dsRecCadastroEnderecoUtilizadoFormulario.
	 *
	 * @param dsRecCadastroEnderecoUtilizadoFormulario the ds rec cadastro endereco utilizado formulario
	 */
	public void setDsRecCadastroEnderecoUtilizadoFormulario(String dsRecCadastroEnderecoUtilizadoFormulario) {
		this.dsRecCadastroEnderecoUtilizadoFormulario = dsRecCadastroEnderecoUtilizadoFormulario;
	}
	
	/**
	 * Get: qtRecDiaAntecipadoFormulario.
	 *
	 * @return qtRecDiaAntecipadoFormulario
	 */
	public String getQtRecDiaAntecipadoFormulario() {
		return qtRecDiaAntecipadoFormulario;
	}
	
	/**
	 * Set: qtRecDiaAntecipadoFormulario.
	 *
	 * @param qtRecDiaAntecipadoFormulario the qt rec dia antecipado formulario
	 */
	public void setQtRecDiaAntecipadoFormulario(String qtRecDiaAntecipadoFormulario) {
		this.qtRecDiaAntecipadoFormulario = qtRecDiaAntecipadoFormulario;
	}
	
	/**
	 * Get: dsRecPermissaoAgrupamentoFormulario.
	 *
	 * @return dsRecPermissaoAgrupamentoFormulario
	 */
	public String getDsRecPermissaoAgrupamentoFormulario() {
		return dsRecPermissaoAgrupamentoFormulario;
	}
	
	/**
	 * Set: dsRecPermissaoAgrupamentoFormulario.
	 *
	 * @param dsRecPermissaoAgrupamentoFormulario the ds rec permissao agrupamento formulario
	 */
	public void setDsRecPermissaoAgrupamentoFormulario(String dsRecPermissaoAgrupamentoFormulario) {
		this.dsRecPermissaoAgrupamentoFormulario = dsRecPermissaoAgrupamentoFormulario;
	}
}
