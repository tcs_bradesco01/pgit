/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarMotivoSituacaoPendContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarMotivoSituacaoPendContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdSituacaoPendenciaContrato
     */
    private int _cdSituacaoPendenciaContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoPendenciaContrato
     */
    private boolean _has_cdSituacaoPendenciaContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarMotivoSituacaoPendContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.request.ListarMotivoSituacaoPendContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoPendenciaContrato
     * 
     */
    public void deleteCdSituacaoPendenciaContrato()
    {
        this._has_cdSituacaoPendenciaContrato= false;
    } //-- void deleteCdSituacaoPendenciaContrato() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdSituacaoPendenciaContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPendenciaContrato'.
     */
    public int getCdSituacaoPendenciaContrato()
    {
        return this._cdSituacaoPendenciaContrato;
    } //-- int getCdSituacaoPendenciaContrato() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdSituacaoPendenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPendenciaContrato()
    {
        return this._has_cdSituacaoPendenciaContrato;
    } //-- boolean hasCdSituacaoPendenciaContrato() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoPendenciaContrato'.
     * 
     * @param cdSituacaoPendenciaContrato the value of field
     * 'cdSituacaoPendenciaContrato'.
     */
    public void setCdSituacaoPendenciaContrato(int cdSituacaoPendenciaContrato)
    {
        this._cdSituacaoPendenciaContrato = cdSituacaoPendenciaContrato;
        this._has_cdSituacaoPendenciaContrato = true;
    } //-- void setCdSituacaoPendenciaContrato(int) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarMotivoSituacaoPendContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.request.ListarMotivoSituacaoPendContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.request.ListarMotivoSituacaoPendContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.request.ListarMotivoSituacaoPendContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivosituacaopendcontrato.request.ListarMotivoSituacaoPendContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
