/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMensagemLinhaExtrato
     */
    private int _cdMensagemLinhaExtrato = 0;

    /**
     * keeps track of state for field: _cdMensagemLinhaExtrato
     */
    private boolean _has_cdMensagemLinhaExtrato;

    /**
     * Field _cdIndicadorRestricaoContrato
     */
    private int _cdIndicadorRestricaoContrato = 0;

    /**
     * keeps track of state for field: _cdIndicadorRestricaoContrato
     */
    private boolean _has_cdIndicadorRestricaoContrato;

    /**
     * Field _cdTipoMensagemExtrato
     */
    private int _cdTipoMensagemExtrato = 0;

    /**
     * keeps track of state for field: _cdTipoMensagemExtrato
     */
    private boolean _has_cdTipoMensagemExtrato;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _dsProdutoServicoOperacao
     */
    private java.lang.String _dsProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoOperacaoRelacionado
     */
    private java.lang.String _dsProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionadoProduto
     */
    private int _cdRelacionadoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionadoProduto
     */
    private boolean _has_cdRelacionadoProduto;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorRestricaoContrato
     * 
     */
    public void deleteCdIndicadorRestricaoContrato()
    {
        this._has_cdIndicadorRestricaoContrato= false;
    } //-- void deleteCdIndicadorRestricaoContrato() 

    /**
     * Method deleteCdMensagemLinhaExtrato
     * 
     */
    public void deleteCdMensagemLinhaExtrato()
    {
        this._has_cdMensagemLinhaExtrato= false;
    } //-- void deleteCdMensagemLinhaExtrato() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionadoProduto
     * 
     */
    public void deleteCdRelacionadoProduto()
    {
        this._has_cdRelacionadoProduto= false;
    } //-- void deleteCdRelacionadoProduto() 

    /**
     * Method deleteCdTipoMensagemExtrato
     * 
     */
    public void deleteCdTipoMensagemExtrato()
    {
        this._has_cdTipoMensagemExtrato= false;
    } //-- void deleteCdTipoMensagemExtrato() 

    /**
     * Returns the value of field 'cdIndicadorRestricaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRestricaoContrato'.
     */
    public int getCdIndicadorRestricaoContrato()
    {
        return this._cdIndicadorRestricaoContrato;
    } //-- int getCdIndicadorRestricaoContrato() 

    /**
     * Returns the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @return int
     * @return the value of field 'cdMensagemLinhaExtrato'.
     */
    public int getCdMensagemLinhaExtrato()
    {
        return this._cdMensagemLinhaExtrato;
    } //-- int getCdMensagemLinhaExtrato() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionadoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionadoProduto'.
     */
    public int getCdRelacionadoProduto()
    {
        return this._cdRelacionadoProduto;
    } //-- int getCdRelacionadoProduto() 

    /**
     * Returns the value of field 'cdTipoMensagemExtrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoMensagemExtrato'.
     */
    public int getCdTipoMensagemExtrato()
    {
        return this._cdTipoMensagemExtrato;
    } //-- int getCdTipoMensagemExtrato() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionado'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionado()
    {
        return this._dsProdutoOperacaoRelacionado;
    } //-- java.lang.String getDsProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServicoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServicoOperacao'.
     */
    public java.lang.String getDsProdutoServicoOperacao()
    {
        return this._dsProdutoServicoOperacao;
    } //-- java.lang.String getDsProdutoServicoOperacao() 

    /**
     * Method hasCdIndicadorRestricaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRestricaoContrato()
    {
        return this._has_cdIndicadorRestricaoContrato;
    } //-- boolean hasCdIndicadorRestricaoContrato() 

    /**
     * Method hasCdMensagemLinhaExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemLinhaExtrato()
    {
        return this._has_cdMensagemLinhaExtrato;
    } //-- boolean hasCdMensagemLinhaExtrato() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionadoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionadoProduto()
    {
        return this._has_cdRelacionadoProduto;
    } //-- boolean hasCdRelacionadoProduto() 

    /**
     * Method hasCdTipoMensagemExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMensagemExtrato()
    {
        return this._has_cdTipoMensagemExtrato;
    } //-- boolean hasCdTipoMensagemExtrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorRestricaoContrato'.
     * 
     * @param cdIndicadorRestricaoContrato the value of field
     * 'cdIndicadorRestricaoContrato'.
     */
    public void setCdIndicadorRestricaoContrato(int cdIndicadorRestricaoContrato)
    {
        this._cdIndicadorRestricaoContrato = cdIndicadorRestricaoContrato;
        this._has_cdIndicadorRestricaoContrato = true;
    } //-- void setCdIndicadorRestricaoContrato(int) 

    /**
     * Sets the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @param cdMensagemLinhaExtrato the value of field
     * 'cdMensagemLinhaExtrato'.
     */
    public void setCdMensagemLinhaExtrato(int cdMensagemLinhaExtrato)
    {
        this._cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
        this._has_cdMensagemLinhaExtrato = true;
    } //-- void setCdMensagemLinhaExtrato(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionadoProduto'.
     * 
     * @param cdRelacionadoProduto the value of field
     * 'cdRelacionadoProduto'.
     */
    public void setCdRelacionadoProduto(int cdRelacionadoProduto)
    {
        this._cdRelacionadoProduto = cdRelacionadoProduto;
        this._has_cdRelacionadoProduto = true;
    } //-- void setCdRelacionadoProduto(int) 

    /**
     * Sets the value of field 'cdTipoMensagemExtrato'.
     * 
     * @param cdTipoMensagemExtrato the value of field
     * 'cdTipoMensagemExtrato'.
     */
    public void setCdTipoMensagemExtrato(int cdTipoMensagemExtrato)
    {
        this._cdTipoMensagemExtrato = cdTipoMensagemExtrato;
        this._has_cdTipoMensagemExtrato = true;
    } //-- void setCdTipoMensagemExtrato(int) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @param dsProdutoOperacaoRelacionado the value of field
     * 'dsProdutoOperacaoRelacionado'.
     */
    public void setDsProdutoOperacaoRelacionado(java.lang.String dsProdutoOperacaoRelacionado)
    {
        this._dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    } //-- void setDsProdutoOperacaoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServicoOperacao'.
     * 
     * @param dsProdutoServicoOperacao the value of field
     * 'dsProdutoServicoOperacao'.
     */
    public void setDsProdutoServicoOperacao(java.lang.String dsProdutoServicoOperacao)
    {
        this._dsProdutoServicoOperacao = dsProdutoServicoOperacao;
    } //-- void setDsProdutoServicoOperacao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmsghistoricocomplementar.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
