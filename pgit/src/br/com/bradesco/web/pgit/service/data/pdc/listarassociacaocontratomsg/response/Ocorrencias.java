/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarassociacaocontratomsg.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCtrlMensagemSalarial
     */
    private int _cdCtrlMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdCtrlMensagemSalarial
     */
    private boolean _has_cdCtrlMensagemSalarial;

    /**
     * Field _dsCtrlMensagemSalarial
     */
    private java.lang.String _dsCtrlMensagemSalarial;

    /**
     * Field _cdPessoaJuridContrato
     */
    private long _cdPessoaJuridContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridContrato
     */
    private boolean _has_cdPessoaJuridContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSeqContratoNegocio
     */
    private long _nrSeqContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSeqContratoNegocio
     */
    private boolean _has_nrSeqContratoNegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarassociacaocontratomsg.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCtrlMensagemSalarial
     * 
     */
    public void deleteCdCtrlMensagemSalarial()
    {
        this._has_cdCtrlMensagemSalarial= false;
    } //-- void deleteCdCtrlMensagemSalarial() 

    /**
     * Method deleteCdPessoaJuridContrato
     * 
     */
    public void deleteCdPessoaJuridContrato()
    {
        this._has_cdPessoaJuridContrato= false;
    } //-- void deleteCdPessoaJuridContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrSeqContratoNegocio
     * 
     */
    public void deleteNrSeqContratoNegocio()
    {
        this._has_nrSeqContratoNegocio= false;
    } //-- void deleteNrSeqContratoNegocio() 

    /**
     * Returns the value of field 'cdCtrlMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdCtrlMensagemSalarial'.
     */
    public int getCdCtrlMensagemSalarial()
    {
        return this._cdCtrlMensagemSalarial;
    } //-- int getCdCtrlMensagemSalarial() 

    /**
     * Returns the value of field 'cdPessoaJuridContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridContrato'.
     */
    public long getCdPessoaJuridContrato()
    {
        return this._cdPessoaJuridContrato;
    } //-- long getCdPessoaJuridContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dsCtrlMensagemSalarial'.
     * 
     * @return String
     * @return the value of field 'dsCtrlMensagemSalarial'.
     */
    public java.lang.String getDsCtrlMensagemSalarial()
    {
        return this._dsCtrlMensagemSalarial;
    } //-- java.lang.String getDsCtrlMensagemSalarial() 

    /**
     * Returns the value of field 'nrSeqContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSeqContratoNegocio'.
     */
    public long getNrSeqContratoNegocio()
    {
        return this._nrSeqContratoNegocio;
    } //-- long getNrSeqContratoNegocio() 

    /**
     * Method hasCdCtrlMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlMensagemSalarial()
    {
        return this._has_cdCtrlMensagemSalarial;
    } //-- boolean hasCdCtrlMensagemSalarial() 

    /**
     * Method hasCdPessoaJuridContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridContrato()
    {
        return this._has_cdPessoaJuridContrato;
    } //-- boolean hasCdPessoaJuridContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrSeqContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContratoNegocio()
    {
        return this._has_nrSeqContratoNegocio;
    } //-- boolean hasNrSeqContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCtrlMensagemSalarial'.
     * 
     * @param cdCtrlMensagemSalarial the value of field
     * 'cdCtrlMensagemSalarial'.
     */
    public void setCdCtrlMensagemSalarial(int cdCtrlMensagemSalarial)
    {
        this._cdCtrlMensagemSalarial = cdCtrlMensagemSalarial;
        this._has_cdCtrlMensagemSalarial = true;
    } //-- void setCdCtrlMensagemSalarial(int) 

    /**
     * Sets the value of field 'cdPessoaJuridContrato'.
     * 
     * @param cdPessoaJuridContrato the value of field
     * 'cdPessoaJuridContrato'.
     */
    public void setCdPessoaJuridContrato(long cdPessoaJuridContrato)
    {
        this._cdPessoaJuridContrato = cdPessoaJuridContrato;
        this._has_cdPessoaJuridContrato = true;
    } //-- void setCdPessoaJuridContrato(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dsCtrlMensagemSalarial'.
     * 
     * @param dsCtrlMensagemSalarial the value of field
     * 'dsCtrlMensagemSalarial'.
     */
    public void setDsCtrlMensagemSalarial(java.lang.String dsCtrlMensagemSalarial)
    {
        this._dsCtrlMensagemSalarial = dsCtrlMensagemSalarial;
    } //-- void setDsCtrlMensagemSalarial(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqContratoNegocio'.
     * 
     * @param nrSeqContratoNegocio the value of field
     * 'nrSeqContratoNegocio'.
     */
    public void setNrSeqContratoNegocio(long nrSeqContratoNegocio)
    {
        this._nrSeqContratoNegocio = nrSeqContratoNegocio;
        this._has_nrSeqContratoNegocio = true;
    } //-- void setNrSeqContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarassociacaocontratomsg.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarassociacaocontratomsg.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarassociacaocontratomsg.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarassociacaocontratomsg.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
