/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listardependempresa.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarDependEmpresaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarDependEmpresaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdTipoPessoaJuridica
     */
    private long _cdTipoPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdTipoPessoaJuridica
     */
    private boolean _has_cdTipoPessoaJuridica;

    /**
     * Field _cdTipoUnidadeOrganizacional
     */
    private int _cdTipoUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeOrganizacional
     */
    private boolean _has_cdTipoUnidadeOrganizacional;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarDependEmpresaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardependempresa.request.ListarDependEmpresaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoPessoaJuridica
     * 
     */
    public void deleteCdTipoPessoaJuridica()
    {
        this._has_cdTipoPessoaJuridica= false;
    } //-- void deleteCdTipoPessoaJuridica() 

    /**
     * Method deleteCdTipoUnidadeOrganizacional
     * 
     */
    public void deleteCdTipoUnidadeOrganizacional()
    {
        this._has_cdTipoUnidadeOrganizacional= false;
    } //-- void deleteCdTipoUnidadeOrganizacional() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdTipoPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdTipoPessoaJuridica'.
     */
    public long getCdTipoPessoaJuridica()
    {
        return this._cdTipoPessoaJuridica;
    } //-- long getCdTipoPessoaJuridica() 

    /**
     * Returns the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeOrganizacional'.
     */
    public int getCdTipoUnidadeOrganizacional()
    {
        return this._cdTipoUnidadeOrganizacional;
    } //-- int getCdTipoUnidadeOrganizacional() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdTipoPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoPessoaJuridica()
    {
        return this._has_cdTipoPessoaJuridica;
    } //-- boolean hasCdTipoPessoaJuridica() 

    /**
     * Method hasCdTipoUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeOrganizacional()
    {
        return this._has_cdTipoUnidadeOrganizacional;
    } //-- boolean hasCdTipoUnidadeOrganizacional() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoPessoaJuridica'.
     * 
     * @param cdTipoPessoaJuridica the value of field
     * 'cdTipoPessoaJuridica'.
     */
    public void setCdTipoPessoaJuridica(long cdTipoPessoaJuridica)
    {
        this._cdTipoPessoaJuridica = cdTipoPessoaJuridica;
        this._has_cdTipoPessoaJuridica = true;
    } //-- void setCdTipoPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @param cdTipoUnidadeOrganizacional the value of field
     * 'cdTipoUnidadeOrganizacional'.
     */
    public void setCdTipoUnidadeOrganizacional(int cdTipoUnidadeOrganizacional)
    {
        this._cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
        this._has_cdTipoUnidadeOrganizacional = true;
    } //-- void setCdTipoUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarDependEmpresaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listardependempresa.request.ListarDependEmpresaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listardependempresa.request.ListarDependEmpresaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listardependempresa.request.ListarDependEmpresaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listardependempresa.request.ListarDependEmpresaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
