/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean;

import static br.com.bradesco.web.pgit.view.converters.FormatarData.formatarDataTrilha;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.view.converters.FormatarData;

// TODO: Auto-generated Javadoc
/**
 * Nome: DetalharManterFormAltContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharManterFormAltContratoSaidaDTO {
	
	 /** Atributo codMensagem. */
 	private String codMensagem;
	 
 	/** Atributo mensagem. */
 	private String mensagem;
	 
 	/** Atributo hrInclusaoRegistro. */
 	private String hrInclusaoRegistro;
	 
 	/** Atributo dsSituacaoAditivoContrato. */
 	private String dsSituacaoAditivoContrato;
	 
 	/** Atributo hrAssinaturaAditivoContrato. */
 	private String hrAssinaturaAditivoContrato;
	 
 	/** Atributo dsMotivoSituacaoAditivo. */
 	private String dsMotivoSituacaoAditivo;
     
     /** Atributo cdIndicadorContratoComl. */
     private Integer cdIndicadorContratoComl;
     
     /** Atributo cdUsuarioInclusao. */
     private String cdUsuarioInclusao;
     
     /** Atributo cdUsuarioInclusaoExterno. */
     private String cdUsuarioInclusaoExterno;
     
     /** Atributo hrInclusaoRegistroTrilha. */
     private String hrInclusaoRegistroTrilha;
     
     /** Atributo cdOperacaoCanalInclusao. */
     private String cdOperacaoCanalInclusao;
     
     /** Atributo cdTipoCanalInclusaoTrilha. */
     private Integer cdTipoCanalInclusaoTrilha;
     
     /** Atributo dsTipoCanalInclusaoTrilha. */
     private String dsTipoCanalInclusaoTrilha;
     
     /** Atributo cdUsuarioManutencao. */
     private String cdUsuarioManutencao;
     
     /** Atributo cdUsuarioManutencaoExterno. */
     private String cdUsuarioManutencaoExterno;
     
     /** Atributo hrManutencaoRegistro. */
     private String hrManutencaoRegistro;
     
     /** Atributo cdOperacaoCanalManutencao. */
     private String cdOperacaoCanalManutencao;
     
     /** Atributo cdTipoCanalManutencao. */
     private Integer cdTipoCanalManutencao;
     
     /** Atributo dsTipoCanalManutencao. */
     private String dsTipoCanalManutencao;
	
	
	/** Atributo listaParticipantes. */
	private List<ParticipantesDTO> listaParticipantes = new ArrayList<ParticipantesDTO>();
	
	/** Atributo listaServicos. */
	private List<ServicosDTO> listaServicos = new ArrayList<ServicosDTO>();
	
	/** Atributo listaVinculos. */
	private List<VinculoDTO> listaVinculos = new ArrayList<VinculoDTO>();
	
	/** Atributo listaPerfis. */
	private List<PerfilDTO> listaPerfis= new ArrayList<PerfilDTO>();
	
    /** Atributo numeroOcorrenciasParticipante. */
    private Integer numeroOcorrenciasParticipante;
    
    /** Atributo numeroOcorrenciasVinculacao. */
    private Integer numeroOcorrenciasVinculacao;
    
    /** Atributo numeroOcorrenciasServicos. */
    private Integer numeroOcorrenciasServicos;
    
    /** Atributo nrOcorrenciasPerfil. */
    private Integer nrOcorrenciasPerfil;
    
    /** Atributo cdFormaAutorizacaoPagamento. */
    private Integer cdFormaAutorizacaoPagamento;
    
    /** Atributo dsFormaAutorizacaoPagamento. */
    private String dsFormaAutorizacaoPagamento;
    
    /** Atributo cdUnidadeOrganizacional. */
    private Integer cdUnidadeOrganizacional;
    
    /** Atributo dsUnidadeOrganizacional. */
    private String dsUnidadeOrganizacional;
    
    /** Atributo cdFuncionarioGerenteContrato. */
    private Long cdFuncionarioGerenteContrato;
    
    /** Atributo dsFuncionarioGerenteContrato. */
    private String dsFuncionarioGerenteContrato;
	
    /**
     * Get: unidadeOrganizacionalFormatada.
     *
     * @return unidadeOrganizacionalFormatada
     */
    public String getUnidadeOrganizacionalFormatada(){
        if(getCdUnidadeOrganizacional() == null || getCdUnidadeOrganizacional() == 0 || 
             getDsUnidadeOrganizacional() == null || "".equals(getDsUnidadeOrganizacional())){
            return getCdUnidadeOrganizacional() + getDsUnidadeOrganizacional();
        }
        return getCdUnidadeOrganizacional() + " - " + getDsUnidadeOrganizacional();
    }
   
    public String getTipoCanalInclusaoTrilhaFormatado(){
        if(getCdTipoCanalInclusaoTrilha() == null || getCdTipoCanalInclusaoTrilha() == 0 || 
           getDsTipoCanalInclusaoTrilha() == null || "".equals(getDsTipoCanalInclusaoTrilha())){
            return getCdTipoCanalInclusaoTrilha() + getDsTipoCanalInclusaoTrilha();
        }
        return getCdTipoCanalInclusaoTrilha() + " - " + getDsTipoCanalInclusaoTrilha();
    }
    
    public String getTipoCanalManutencaoFormatado(){
        if(getCdTipoCanalManutencao() == null || getCdTipoCanalManutencao() == 0 || 
                        getDsTipoCanalManutencao() == null || "".equals(getDsTipoCanalManutencao())){
            if(getCdTipoCanalManutencao() == 0 ){
                return "";
            }
            
            return getCdTipoCanalManutencao() + getDsTipoCanalManutencao();
        }
        return getCdTipoCanalManutencao() + " - " + getDsTipoCanalManutencao();
    }
    
    public String getOperacaoCanalManutencaoFormatado(){
       if(getCdOperacaoCanalManutencao() == null || getDsTipoCanalInclusaoTrilha().equals("0")){
           return "";
       }
        return getDsTipoCanalInclusaoTrilha();
    }
    
     
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: dsSituacaoAditivoContrato.
	 *
	 * @return dsSituacaoAditivoContrato
	 */
	public String getDsSituacaoAditivoContrato() {
		return dsSituacaoAditivoContrato;
	}
	
	/**
	 * Set: dsSituacaoAditivoContrato.
	 *
	 * @param dsSituacaoAditivoContrato the ds situacao aditivo contrato
	 */
	public void setDsSituacaoAditivoContrato(String dsSituacaoAditivoContrato) {
		this.dsSituacaoAditivoContrato = dsSituacaoAditivoContrato;
	}
	
	/**
	 * Get: hrAssinaturaAditivoContrato.
	 *
	 * @return hrAssinaturaAditivoContrato
	 */
	public String getHrAssinaturaAditivoContrato() {
		return hrAssinaturaAditivoContrato;
	}
	
	/**
	 * Set: hrAssinaturaAditivoContrato.
	 *
	 * @param hrAssinaturaAditivoContrato the hr assinatura aditivo contrato
	 */
	public void setHrAssinaturaAditivoContrato(String hrAssinaturaAditivoContrato) {
		this.hrAssinaturaAditivoContrato = hrAssinaturaAditivoContrato;
	}
	
	/**
	 * Get: dsMotivoSituacaoAditivo.
	 *
	 * @return dsMotivoSituacaoAditivo
	 */
	public String getDsMotivoSituacaoAditivo() {
		return dsMotivoSituacaoAditivo;
	}
	
	/**
	 * Set: dsMotivoSituacaoAditivo.
	 *
	 * @param dsMotivoSituacaoAditivo the ds motivo situacao aditivo
	 */
	public void setDsMotivoSituacaoAditivo(String dsMotivoSituacaoAditivo) {
		this.dsMotivoSituacaoAditivo = dsMotivoSituacaoAditivo;
	}
	
	/**
	 * Get: cdIndicadorContratoComl.
	 *
	 * @return cdIndicadorContratoComl
	 */
	public Integer getCdIndicadorContratoComl() {
		return cdIndicadorContratoComl;
	}
	
	/**
	 * Set: cdIndicadorContratoComl.
	 *
	 * @param cdIndicadorContratoComl the cd indicador contrato coml
	 */
	public void setCdIndicadorContratoComl(Integer cdIndicadorContratoComl) {
		this.cdIndicadorContratoComl = cdIndicadorContratoComl;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: hrInclusaoRegistroTrilha.
	 *
	 * @return hrInclusaoRegistroTrilha
	 */
	public String getHrInclusaoRegistroTrilha() {
		return hrInclusaoRegistroTrilha;
	}
	
	/**
	 * Set: hrInclusaoRegistroTrilha.
	 *
	 * @param hrInclusaoRegistroTrilha the hr inclusao registro trilha
	 */
	public void setHrInclusaoRegistroTrilha(String hrInclusaoRegistroTrilha) {
		this.hrInclusaoRegistroTrilha = hrInclusaoRegistroTrilha;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdTipoCanalInclusaoTrilha.
	 *
	 * @return cdTipoCanalInclusaoTrilha
	 */
	public Integer getCdTipoCanalInclusaoTrilha() {
		return cdTipoCanalInclusaoTrilha;
	}
	
	/**
	 * Set: cdTipoCanalInclusaoTrilha.
	 *
	 * @param cdTipoCanalInclusaoTrilha the cd tipo canal inclusao trilha
	 */
	public void setCdTipoCanalInclusaoTrilha(Integer cdTipoCanalInclusaoTrilha) {
		this.cdTipoCanalInclusaoTrilha = cdTipoCanalInclusaoTrilha;
	}
	
	/**
	 * Get: dsTipoCanalInclusaoTrilha.
	 *
	 * @return dsTipoCanalInclusaoTrilha
	 */
	public String getDsTipoCanalInclusaoTrilha() {
		return dsTipoCanalInclusaoTrilha;
	}
	
	/**
	 * Set: dsTipoCanalInclusaoTrilha.
	 *
	 * @param dsTipoCanalInclusaoTrilha the ds tipo canal inclusao trilha
	 */
	public void setDsTipoCanalInclusaoTrilha(String dsTipoCanalInclusaoTrilha) {
		this.dsTipoCanalInclusaoTrilha = dsTipoCanalInclusaoTrilha;
	}
	
	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}
	
	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}
	
	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}
	
	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}
	
	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}
	
	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}
	
	/**
	 * Get: listaParticipantes.
	 *
	 * @return listaParticipantes
	 */
	public List<ParticipantesDTO> getListaParticipantes() {
		return listaParticipantes;
	}
	
	/**
	 * Set: listaParticipantes.
	 *
	 * @param listaParticipantes the lista participantes
	 */
	public void setListaParticipantes(List<ParticipantesDTO> listaParticipantes) {
		this.listaParticipantes = listaParticipantes;
	}
	
	/**
	 * Get: listaServicos.
	 *
	 * @return listaServicos
	 */
	public List<ServicosDTO> getListaServicos() {
		return listaServicos;
	}
	
	/**
	 * Set: listaServicos.
	 *
	 * @param listaServicos the lista servicos
	 */
	public void setListaServicos(List<ServicosDTO> listaServicos) {
		this.listaServicos = listaServicos;
	}
	
	/**
	 * Get: listaVinculos.
	 *
	 * @return listaVinculos
	 */
	public List<VinculoDTO> getListaVinculos() {
		return listaVinculos;
	}
	
	/**
	 * Set: listaVinculos.
	 *
	 * @param listaVinculos the lista vinculos
	 */
	public void setListaVinculos(List<VinculoDTO> listaVinculos) {
		this.listaVinculos = listaVinculos;
	}
	
	/**
	 * Get: listaPerfis.
	 *
	 * @return listaPerfis
	 */
	public List<PerfilDTO> getListaPerfis() {
		return listaPerfis;
	}
	
	/**
	 * Set: listaPerfis.
	 *
	 * @param listaPerfis the lista perfis
	 */
	public void setListaPerfis(List<PerfilDTO> listaPerfis) {
		this.listaPerfis = listaPerfis;
	}
	
	/**
	 * Get: numeroOcorrenciasParticipante.
	 *
	 * @return numeroOcorrenciasParticipante
	 */
	public Integer getNumeroOcorrenciasParticipante() {
		return numeroOcorrenciasParticipante;
	}
	
	/**
	 * Set: numeroOcorrenciasParticipante.
	 *
	 * @param numeroOcorrenciasParticipante the numero ocorrencias participante
	 */
	public void setNumeroOcorrenciasParticipante(
			Integer numeroOcorrenciasParticipante) {
		this.numeroOcorrenciasParticipante = numeroOcorrenciasParticipante;
	}
	
	/**
	 * Get: numeroOcorrenciasVinculacao.
	 *
	 * @return numeroOcorrenciasVinculacao
	 */
	public Integer getNumeroOcorrenciasVinculacao() {
		return numeroOcorrenciasVinculacao;
	}
	
	/**
	 * Set: numeroOcorrenciasVinculacao.
	 *
	 * @param numeroOcorrenciasVinculacao the numero ocorrencias vinculacao
	 */
	public void setNumeroOcorrenciasVinculacao(Integer numeroOcorrenciasVinculacao) {
		this.numeroOcorrenciasVinculacao = numeroOcorrenciasVinculacao;
	}
	
	/**
	 * Get: numeroOcorrenciasServicos.
	 *
	 * @return numeroOcorrenciasServicos
	 */
	public Integer getNumeroOcorrenciasServicos() {
		return numeroOcorrenciasServicos;
	}
	
	/**
	 * Set: numeroOcorrenciasServicos.
	 *
	 * @param numeroOcorrenciasServicos the numero ocorrencias servicos
	 */
	public void setNumeroOcorrenciasServicos(Integer numeroOcorrenciasServicos) {
		this.numeroOcorrenciasServicos = numeroOcorrenciasServicos;
	}
	
	/**
	 * Get: nrOcorrenciasPerfil.
	 *
	 * @return nrOcorrenciasPerfil
	 */
	public Integer getNrOcorrenciasPerfil() {
		return nrOcorrenciasPerfil;
	}
	
	/**
	 * Set: nrOcorrenciasPerfil.
	 *
	 * @param nrOcorrenciasPerfil the nr ocorrencias perfil
	 */
	public void setNrOcorrenciasPerfil(Integer nrOcorrenciasPerfil) {
		this.nrOcorrenciasPerfil = nrOcorrenciasPerfil;
	}

    /**
     * Get: cdFormaAutorizacaoPagamento.
     *
     * @return cdFormaAutorizacaoPagamento
     */
    public Integer getCdFormaAutorizacaoPagamento() {
        return cdFormaAutorizacaoPagamento;
    }

    /**
     * Set: cdFormaAutorizacaoPagamento.
     *
     * @param cdFormaAutorizacaoPagamento the cd forma autorizacao pagamento
     */
    public void setCdFormaAutorizacaoPagamento(Integer cdFormaAutorizacaoPagamento) {
        this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
    }

    /**
     * Get: dsFormaAutorizacaoPagamento.
     *
     * @return dsFormaAutorizacaoPagamento
     */
    public String getDsFormaAutorizacaoPagamento() {
        return dsFormaAutorizacaoPagamento;
    }

    /**
     * Set: dsFormaAutorizacaoPagamento.
     *
     * @param dsFormaAutorizacaoPagamento the ds forma autorizacao pagamento
     */
    public void setDsFormaAutorizacaoPagamento(String dsFormaAutorizacaoPagamento) {
        this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    }

    /**
     * Get: cdUnidadeOrganizacional.
     *
     * @return cdUnidadeOrganizacional
     */
    public Integer getCdUnidadeOrganizacional() {
        return cdUnidadeOrganizacional;
    }

    /**
     * Set: cdUnidadeOrganizacional.
     *
     * @param cdUnidadeOrganizacional the cd unidade organizacional
     */
    public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
        this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
    }

    /**
     * Get: dsUnidadeOrganizacional.
     *
     * @return dsUnidadeOrganizacional
     */
    public String getDsUnidadeOrganizacional() {
        return dsUnidadeOrganizacional;
    }

    /**
     * Set: dsUnidadeOrganizacional.
     *
     * @param dsUnidadeOrganizacional the ds unidade organizacional
     */
    public void setDsUnidadeOrganizacional(String dsUnidadeOrganizacional) {
        this.dsUnidadeOrganizacional = dsUnidadeOrganizacional;
    }

    /**
     * Get: cdFuncionarioGerenteContrato.
     *
     * @return cdFuncionarioGerenteContrato
     */
    public Long getCdFuncionarioGerenteContrato() {
        return cdFuncionarioGerenteContrato;
    }

    /**
     * Set: cdFuncionarioGerenteContrato.
     *
     * @param cdFuncionarioGerenteContrato the cd funcionario gerente contrato
     */
    public void setCdFuncionarioGerenteContrato(Long cdFuncionarioGerenteContrato) {
        this.cdFuncionarioGerenteContrato = cdFuncionarioGerenteContrato;
    }

    /**
     * Get: dsFuncionarioGerenteContrato.
     *
     * @return dsFuncionarioGerenteContrato
     */
    public String getDsFuncionarioGerenteContrato() {
        return dsFuncionarioGerenteContrato;
    }

    /**
     * Set: dsFuncionarioGerenteContrato.
     *
     * @param dsFuncionarioGerenteContrato the ds funcionario gerente contrato
     */
    public void setDsFuncionarioGerenteContrato(String dsFuncionarioGerenteContrato) {
        this.dsFuncionarioGerenteContrato = dsFuncionarioGerenteContrato;
    }
	
	
	
	
}
