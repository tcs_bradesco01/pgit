/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultaremail.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCpfCnpjRecebedor
     */
    private long _cdCpfCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjRecebedor
     */
    private boolean _has_cdCpfCnpjRecebedor;

    /**
     * Field _cdFilialCnpjRecebedor
     */
    private int _cdFilialCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjRecebedor
     */
    private boolean _has_cdFilialCnpjRecebedor;

    /**
     * Field _cdControleCpfRecebedor
     */
    private int _cdControleCpfRecebedor = 0;

    /**
     * keeps track of state for field: _cdControleCpfRecebedor
     */
    private boolean _has_cdControleCpfRecebedor;

    /**
     * Field _dsNomeRazao
     */
    private java.lang.String _dsNomeRazao;

    /**
     * Field _cdEnderecoEletronico
     */
    private java.lang.String _cdEnderecoEletronico;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaremail.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleCpfRecebedor
     * 
     */
    public void deleteCdControleCpfRecebedor()
    {
        this._has_cdControleCpfRecebedor= false;
    } //-- void deleteCdControleCpfRecebedor() 

    /**
     * Method deleteCdCpfCnpjRecebedor
     * 
     */
    public void deleteCdCpfCnpjRecebedor()
    {
        this._has_cdCpfCnpjRecebedor= false;
    } //-- void deleteCdCpfCnpjRecebedor() 

    /**
     * Method deleteCdFilialCnpjRecebedor
     * 
     */
    public void deleteCdFilialCnpjRecebedor()
    {
        this._has_cdFilialCnpjRecebedor= false;
    } //-- void deleteCdFilialCnpjRecebedor() 

    /**
     * Returns the value of field 'cdControleCpfRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfRecebedor'.
     */
    public int getCdControleCpfRecebedor()
    {
        return this._cdControleCpfRecebedor;
    } //-- int getCdControleCpfRecebedor() 

    /**
     * Returns the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjRecebedor'.
     */
    public long getCdCpfCnpjRecebedor()
    {
        return this._cdCpfCnpjRecebedor;
    } //-- long getCdCpfCnpjRecebedor() 

    /**
     * Returns the value of field 'cdEnderecoEletronico'.
     * 
     * @return String
     * @return the value of field 'cdEnderecoEletronico'.
     */
    public java.lang.String getCdEnderecoEletronico()
    {
        return this._cdEnderecoEletronico;
    } //-- java.lang.String getCdEnderecoEletronico() 

    /**
     * Returns the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjRecebedor'.
     */
    public int getCdFilialCnpjRecebedor()
    {
        return this._cdFilialCnpjRecebedor;
    } //-- int getCdFilialCnpjRecebedor() 

    /**
     * Returns the value of field 'dsNomeRazao'.
     * 
     * @return String
     * @return the value of field 'dsNomeRazao'.
     */
    public java.lang.String getDsNomeRazao()
    {
        return this._dsNomeRazao;
    } //-- java.lang.String getDsNomeRazao() 

    /**
     * Method hasCdControleCpfRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfRecebedor()
    {
        return this._has_cdControleCpfRecebedor;
    } //-- boolean hasCdControleCpfRecebedor() 

    /**
     * Method hasCdCpfCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjRecebedor()
    {
        return this._has_cdCpfCnpjRecebedor;
    } //-- boolean hasCdCpfCnpjRecebedor() 

    /**
     * Method hasCdFilialCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjRecebedor()
    {
        return this._has_cdFilialCnpjRecebedor;
    } //-- boolean hasCdFilialCnpjRecebedor() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleCpfRecebedor'.
     * 
     * @param cdControleCpfRecebedor the value of field
     * 'cdControleCpfRecebedor'.
     */
    public void setCdControleCpfRecebedor(int cdControleCpfRecebedor)
    {
        this._cdControleCpfRecebedor = cdControleCpfRecebedor;
        this._has_cdControleCpfRecebedor = true;
    } //-- void setCdControleCpfRecebedor(int) 

    /**
     * Sets the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @param cdCpfCnpjRecebedor the value of field
     * 'cdCpfCnpjRecebedor'.
     */
    public void setCdCpfCnpjRecebedor(long cdCpfCnpjRecebedor)
    {
        this._cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
        this._has_cdCpfCnpjRecebedor = true;
    } //-- void setCdCpfCnpjRecebedor(long) 

    /**
     * Sets the value of field 'cdEnderecoEletronico'.
     * 
     * @param cdEnderecoEletronico the value of field
     * 'cdEnderecoEletronico'.
     */
    public void setCdEnderecoEletronico(java.lang.String cdEnderecoEletronico)
    {
        this._cdEnderecoEletronico = cdEnderecoEletronico;
    } //-- void setCdEnderecoEletronico(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @param cdFilialCnpjRecebedor the value of field
     * 'cdFilialCnpjRecebedor'.
     */
    public void setCdFilialCnpjRecebedor(int cdFilialCnpjRecebedor)
    {
        this._cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
        this._has_cdFilialCnpjRecebedor = true;
    } //-- void setCdFilialCnpjRecebedor(int) 

    /**
     * Sets the value of field 'dsNomeRazao'.
     * 
     * @param dsNomeRazao the value of field 'dsNomeRazao'.
     */
    public void setDsNomeRazao(java.lang.String dsNomeRazao)
    {
        this._dsNomeRazao = dsNomeRazao;
    } //-- void setDsNomeRazao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultaremail.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultaremail.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultaremail.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaremail.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
