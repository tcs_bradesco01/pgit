/*
 * Nome: br.com.bradesco.web.pgit.service.business.moedasoperacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.moedasoperacao.bean;

/**
 * Nome: MoedaOperacaoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class MoedaOperacaoDTO {

	/** Atributo servico. */
	private String servico;
	
	/** Atributo servicoRelacionado. */
	private String servicoRelacionado;
	
	/** Atributo moedaIndice. */
	private String moedaIndice;
	
	/** Atributo descricao. */
	private String descricao;
	
	/**
	 * Get: descricao.
	 *
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	
	/**
	 * Set: descricao.
	 *
	 * @param descricao the descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Get: moedaIndice.
	 *
	 * @return moedaIndice
	 */
	public String getMoedaIndice() {
		return moedaIndice;
	}
	
	/**
	 * Set: moedaIndice.
	 *
	 * @param moedaIndice the moeda indice
	 */
	public void setMoedaIndice(String moedaIndice) {
		this.moedaIndice = moedaIndice;
	}
	
	/**
	 * Get: servico.
	 *
	 * @return servico
	 */
	public String getServico() {
		return servico;
	}
	
	/**
	 * Set: servico.
	 *
	 * @param servico the servico
	 */
	public void setServico(String servico) {
		this.servico = servico;
	}
	
	/**
	 * Get: servicoRelacionado.
	 *
	 * @return servicoRelacionado
	 */
	public String getServicoRelacionado() {
		return servicoRelacionado;
	}

	/**
	 * Set: servicoRelacionado.
	 *
	 * @param servicoRelacionado the servico relacionado
	 */
	public void setServicoRelacionado(String servicoRelacionado) {
		this.servicoRelacionado = servicoRelacionado;
	}

}
