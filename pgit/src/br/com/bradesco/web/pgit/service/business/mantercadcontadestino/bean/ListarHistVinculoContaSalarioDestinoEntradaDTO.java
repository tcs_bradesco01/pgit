/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

/**
 * Nome: ListarHistVinculoContaSalarioDestinoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHistVinculoContaSalarioDestinoEntradaDTO {

    /** Atributo cdPessoaJuridNegocio. */
    private Long cdPessoaJuridNegocio;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSeqContratoNegocio. */
    private Long nrSeqContratoNegocio;

    /** Atributo cdPessoaJuridVinc. */
    private Long cdPessoaJuridVinc;

    /** Atributo cdTipoContratoVinc. */
    private Integer cdTipoContratoVinc;

    /** Atributo nrSeqContratoVinc. */
    private Long nrSeqContratoVinc;

    /** Atributo cdTipoVinculoContrato. */
    private Integer cdTipoVinculoContrato;

    /** Atributo cdBanco. */
    private Integer cdBanco;

    /** Atributo cdAgencia. */
    private Integer cdAgencia;

    /** Atributo cdConta. */
    private Long cdConta;

    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;

    /** Atributo dataInicio. */
    private String dataInicio;

    /** Atributo dataFim. */
    private String dataFim;

    /** Atributo cdPesquisaLista. */
    private Integer cdPesquisaLista;

    /**
     * Listar hist vinculo conta salario destino entrada dto.
     */
    public ListarHistVinculoContaSalarioDestinoEntradaDTO() {
	super();
    }

    /**
     * Get: cdPessoaJuridNegocio.
     *
     * @return cdPessoaJuridNegocio
     */
    public Long getCdPessoaJuridNegocio() {
	return cdPessoaJuridNegocio;
    }

    /**
     * Set: cdPessoaJuridNegocio.
     *
     * @param cdPessoaJuridNegocio the cd pessoa jurid negocio
     */
    public void setCdPessoaJuridNegocio(Long cdPessoaJuridNegocio) {
	this.cdPessoaJuridNegocio = cdPessoaJuridNegocio;
    }

    /**
     * Get: cdTipoContratoVinc.
     *
     * @return cdTipoContratoVinc
     */
    public Integer getCdTipoContratoVinc() {
	return cdTipoContratoVinc;
    }

    /**
     * Set: cdTipoContratoVinc.
     *
     * @param cdTipoContratoVinc the cd tipo contrato vinc
     */
    public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
	this.cdTipoContratoVinc = cdTipoContratoVinc;
    }

    /**
     * Get: nrSeqContratoVinc.
     *
     * @return nrSeqContratoVinc
     */
    public Long getNrSeqContratoVinc() {
	return nrSeqContratoVinc;
    }

    /**
     * Set: nrSeqContratoVinc.
     *
     * @param nrSeqContratoVinc the nr seq contrato vinc
     */
    public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
	this.nrSeqContratoVinc = nrSeqContratoVinc;
    }

    /**
     * Get: cdPessoaJuridVinc.
     *
     * @return cdPessoaJuridVinc
     */
    public Long getCdPessoaJuridVinc() {
	return cdPessoaJuridVinc;
    }

    /**
     * Set: cdPessoaJuridVinc.
     *
     * @param cdPessoaJuridVinc the cd pessoa jurid vinc
     */
    public void setCdPessoaJuridVinc(Long cdPessoaJuridVinc) {
	this.cdPessoaJuridVinc = cdPessoaJuridVinc;
    }

    /**
     * Get: cdTipoVinculoContrato.
     *
     * @return cdTipoVinculoContrato
     */
    public Integer getCdTipoVinculoContrato() {
	return cdTipoVinculoContrato;
    }

    /**
     * Set: cdTipoVinculoContrato.
     *
     * @param cdTipoVinculoContrato the cd tipo vinculo contrato
     */
    public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
	this.cdTipoVinculoContrato = cdTipoVinculoContrato;
    }

    /**
     * Get: nrSeqContratoNegocio.
     *
     * @return nrSeqContratoNegocio
     */
    public Long getNrSeqContratoNegocio() {
	return nrSeqContratoNegocio;
    }

    /**
     * Set: nrSeqContratoNegocio.
     *
     * @param nrSeqContratoNegocio the nr seq contrato negocio
     */
    public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
	this.nrSeqContratoNegocio = nrSeqContratoNegocio;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdAgencia.
     *
     * @return cdAgencia
     */
    public Integer getCdAgencia() {
	return cdAgencia;
    }

    /**
     * Set: cdAgencia.
     *
     * @param cdAgencia the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
	this.cdAgencia = cdAgencia;
    }

    /**
     * Get: cdBanco.
     *
     * @return cdBanco
     */
    public Integer getCdBanco() {
	return cdBanco;
    }

    /**
     * Set: cdBanco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
	this.cdBanco = cdBanco;
    }

    /**
     * Get: cdConta.
     *
     * @return cdConta
     */
    public Long getCdConta() {
	return cdConta;
    }

    /**
     * Set: cdConta.
     *
     * @param cdConta the cd conta
     */
    public void setCdConta(Long cdConta) {
	this.cdConta = cdConta;
    }

    /**
     * Get: cdDigitoConta.
     *
     * @return cdDigitoConta
     */
    public String getCdDigitoConta() {
	return cdDigitoConta;
    }

    /**
     * Set: cdDigitoConta.
     *
     * @param cdDigitoConta the cd digito conta
     */
    public void setCdDigitoConta(String cdDigitoConta) {
	this.cdDigitoConta = cdDigitoConta;
    }

    /**
     * Get: cdPesquisaLista.
     *
     * @return cdPesquisaLista
     */
    public Integer getCdPesquisaLista() {
	return cdPesquisaLista;
    }

    /**
     * Set: cdPesquisaLista.
     *
     * @param cdPesquisaLista the cd pesquisa lista
     */
    public void setCdPesquisaLista(Integer cdPesquisaLista) {
	this.cdPesquisaLista = cdPesquisaLista;
    }

    /**
     * Get: dataFim.
     *
     * @return dataFim
     */
    public String getDataFim() {
	return dataFim;
    }

    /**
     * Set: dataFim.
     *
     * @param dataFim the data fim
     */
    public void setDataFim(String dataFim) {
	this.dataFim = dataFim;
    }

    /**
     * Get: dataInicio.
     *
     * @return dataInicio
     */
    public String getDataInicio() {
	return dataInicio;
    }

    /**
     * Set: dataInicio.
     *
     * @param dataInicio the data inicio
     */
    public void setDataInicio(String dataInicio) {
	this.dataInicio = dataInicio;
    }
}
