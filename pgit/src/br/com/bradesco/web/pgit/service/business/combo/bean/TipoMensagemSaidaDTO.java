/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: TipoMensagemSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoMensagemSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdCist. */
	private String cdCist;
	
	/** Atributo nrEventoMensagemNegocio. */
	private int nrEventoMensagemNegocio;
	
	/** Atributo cdRecGedorMensagem. */
	private int cdRecGedorMensagem;
	
	/** Atributo cdIdiomaTextoMensagem. */
	private int cdIdiomaTextoMensagem;
	
	/** Atributo cdTipoMensagem. */
	private int cdTipoMensagem;
	
	/** Atributo dsTipoMensagem. */
	private String dsTipoMensagem;
	
	/** Atributo nrSeqMensagemAlerta. */
	private int nrSeqMensagemAlerta; 
		
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdCist.
	 *
	 * @return cdCist
	 */
	public String getCdCist() {
		return cdCist;
	}
	
	/**
	 * Set: cdCist.
	 *
	 * @param cdCist the cd cist
	 */
	public void setCdCist(String cdCist) {
		this.cdCist = cdCist;
	}
	
	/**
	 * Get: cdIdiomaTextoMensagem.
	 *
	 * @return cdIdiomaTextoMensagem
	 */
	public int getCdIdiomaTextoMensagem() {
		return cdIdiomaTextoMensagem;
	}
	
	/**
	 * Set: cdIdiomaTextoMensagem.
	 *
	 * @param cdIdiomaTextoMensagem the cd idioma texto mensagem
	 */
	public void setCdIdiomaTextoMensagem(int cdIdiomaTextoMensagem) {
		this.cdIdiomaTextoMensagem = cdIdiomaTextoMensagem;
	}
	
	/**
	 * Get: cdRecGedorMensagem.
	 *
	 * @return cdRecGedorMensagem
	 */
	public int getCdRecGedorMensagem() {
		return cdRecGedorMensagem;
	}
	
	/**
	 * Set: cdRecGedorMensagem.
	 *
	 * @param cdRecGedorMensagem the cd rec gedor mensagem
	 */
	public void setCdRecGedorMensagem(int cdRecGedorMensagem) {
		this.cdRecGedorMensagem = cdRecGedorMensagem;
	}
	
	/**
	 * Get: nrEventoMensagemNegocio.
	 *
	 * @return nrEventoMensagemNegocio
	 */
	public int getNrEventoMensagemNegocio() {
		return nrEventoMensagemNegocio;
	}
	
	/**
	 * Set: nrEventoMensagemNegocio.
	 *
	 * @param nrEventoMensagemNegocio the nr evento mensagem negocio
	 */
	public void setNrEventoMensagemNegocio(int nrEventoMensagemNegocio) {
		this.nrEventoMensagemNegocio = nrEventoMensagemNegocio;
	}
	
	/**
	 * Get: cdTipoMensagem.
	 *
	 * @return cdTipoMensagem
	 */
	public int getCdTipoMensagem() {
		return cdTipoMensagem;
	}
	
	/**
	 * Set: cdTipoMensagem.
	 *
	 * @param cdTipoMensagem the cd tipo mensagem
	 */
	public void setCdTipoMensagem(int cdTipoMensagem) {
		this.cdTipoMensagem = cdTipoMensagem;
	}
	
	/**
	 * Get: dsTipoMensagem.
	 *
	 * @return dsTipoMensagem
	 */
	public String getDsTipoMensagem() {
		return dsTipoMensagem;
	}
	
	/**
	 * Set: dsTipoMensagem.
	 *
	 * @param dsTipoMensagem the ds tipo mensagem
	 */
	public void setDsTipoMensagem(String dsTipoMensagem) {
		this.dsTipoMensagem = dsTipoMensagem;
	}
	
	/**
	 * Get: nrSeqMensagemAlerta.
	 *
	 * @return nrSeqMensagemAlerta
	 */
	public int getNrSeqMensagemAlerta() {
		return nrSeqMensagemAlerta;
	}
	
	/**
	 * Set: nrSeqMensagemAlerta.
	 *
	 * @param nrSeqMensagemAlerta the nr seq mensagem alerta
	 */
	public void setNrSeqMensagemAlerta(int nrSeqMensagemAlerta) {
		this.nrSeqMensagemAlerta = nrSeqMensagemAlerta;
	}
	
	

}
