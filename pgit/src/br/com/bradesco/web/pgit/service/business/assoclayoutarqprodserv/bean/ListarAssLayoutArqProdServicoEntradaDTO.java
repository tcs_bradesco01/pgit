/*
 * Nome: br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean;

/**
 * Nome: ListarAssLayoutArqProdServicoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarAssLayoutArqProdServicoEntradaDTO {

	/** Atributo tipoLayoutArquivo. */
	private Integer tipoLayoutArquivo;
	
	/** Atributo tipoServico. */
	private Integer tipoServico;
	
	/** Atributo cdIdentificadorLayoutNegocio. */
	private Integer cdIdentificadorLayoutNegocio;

	/**
	 * Get: cdIdentificadorLayoutNegocio.
	 *
	 * @return cdIdentificadorLayoutNegocio
	 */
	public Integer getCdIdentificadorLayoutNegocio() {
		return cdIdentificadorLayoutNegocio;
	}

	/**
	 * Set: cdIdentificadorLayoutNegocio.
	 *
	 * @param cdIdentificadorLayoutNegocio the cd identificador layout negocio
	 */
	public void setCdIdentificadorLayoutNegocio(Integer cdIdentificadorLayoutNegocio) {
		this.cdIdentificadorLayoutNegocio = cdIdentificadorLayoutNegocio;
	}

	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public Integer getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}

	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(Integer tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public Integer getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(Integer tipoServico) {
		this.tipoServico = tipoServico;
	}
	

	
}
