/*
 * Nome: br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean;

/**
 * Nome: ListarLiquidacaoPagamentoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarLiquidacaoPagamentoEntradaDTO {

	/** Atributo qtConsultas. */
	private Integer qtConsultas;
	
	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;
	
	/** Atributo dtInicioVigencia. */
	private String dtInicioVigencia;
	
	/** Atributo dtFinalVigencia. */
	private String dtFinalVigencia;
	
	/** Atributo hrInclusaoRegistroHist. */
	private String hrInclusaoRegistroHist;
	
	/** Atributo hrConsultasSaldoPagamento. */
	private String hrConsultasSaldoPagamento;
	
	/** Atributo hrConsultaFolhaPgto. */
	private String hrConsultaFolhaPgto;

	/**
	 * Listar liquidacao pagamento entrada dto.
	 */
	public ListarLiquidacaoPagamentoEntradaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: dtFinalVigencia.
	 *
	 * @return dtFinalVigencia
	 */
	public String getDtFinalVigencia() {
		return dtFinalVigencia;
	}

	/**
	 * Set: dtFinalVigencia.
	 *
	 * @param dtFinalVigencia the dt final vigencia
	 */
	public void setDtFinalVigencia(String dtFinalVigencia) {
		this.dtFinalVigencia = dtFinalVigencia;
	}

	/**
	 * Get: dtInicioVigencia.
	 *
	 * @return dtInicioVigencia
	 */
	public String getDtInicioVigencia() {
		return dtInicioVigencia;
	}

	/**
	 * Set: dtInicioVigencia.
	 *
	 * @param dtInicioVigencia the dt inicio vigencia
	 */
	public void setDtInicioVigencia(String dtInicioVigencia) {
		this.dtInicioVigencia = dtInicioVigencia;
	}

	/**
	 * Get: hrInclusaoRegistroHist.
	 *
	 * @return hrInclusaoRegistroHist
	 */
	public String getHrInclusaoRegistroHist() {
		return hrInclusaoRegistroHist;
	}

	/**
	 * Set: hrInclusaoRegistroHist.
	 *
	 * @param hrInclusaoRegistroHist the hr inclusao registro hist
	 */
	public void setHrInclusaoRegistroHist(String hrInclusaoRegistroHist) {
		this.hrInclusaoRegistroHist = hrInclusaoRegistroHist;
	}

	/**
	 * Get: qtConsultas.
	 *
	 * @return qtConsultas
	 */
	public Integer getQtConsultas() {
		return qtConsultas;
	}

	/**
	 * Set: qtConsultas.
	 *
	 * @param qtConsultas the qt consultas
	 */
	public void setQtConsultas(Integer qtConsultas) {
		this.qtConsultas = qtConsultas;
	}

	/**
	 * Get: hrConsultasSaldoPagamento.
	 *
	 * @return hrConsultasSaldoPagamento
	 */
	public String getHrConsultasSaldoPagamento() {
		return hrConsultasSaldoPagamento;
	}

	/**
	 * Set: hrConsultasSaldoPagamento.
	 *
	 * @param hrConsultasSaldoPagamento the hr consultas saldo pagamento
	 */
	public void setHrConsultasSaldoPagamento(String hrConsultasSaldoPagamento) {
		this.hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
	}

	/**
	 * Get: hrConsultaFolhaPgto.
	 *
	 * @return hrConsultaFolhaPgto
	 */
	public String getHrConsultaFolhaPgto() {
		return hrConsultaFolhaPgto;
	}

	/**
	 * Set: hrConsultaFolhaPgto.
	 *
	 * @param hrConsultaFolhaPgto the hr consulta folha pgto
	 */
	public void setHrConsultaFolhaPgto(String hrConsultaFolhaPgto) {
		this.hrConsultaFolhaPgto = hrConsultaFolhaPgto;
	}

}
