package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

public class ListarDadosConvenioContaSalarioEntradaDTO {
	
	private Integer maxOcorrencias;
	private Long codPessoaJuridica;
	private Integer codTipoContratoNegocio;
	private Long numSeqContratoNegocio;
	
	public ListarDadosConvenioContaSalarioEntradaDTO() {
		super();
	}

	public ListarDadosConvenioContaSalarioEntradaDTO(Integer maxOcorrencias, Long codPessoaJuridica, 
			Integer codTipoContratoNegocio, Long numSeqContratoNegocio) {
		this.maxOcorrencias = maxOcorrencias;
		this.codPessoaJuridica = codPessoaJuridica;
		this.codTipoContratoNegocio = codTipoContratoNegocio;
		this.numSeqContratoNegocio = numSeqContratoNegocio;
	}
	
	public ListarDadosConvenioContaSalarioEntradaDTO(Long codPessoaJuridica, 
			Integer codTipoContratoNegocio, Long numSeqContratoNegocio) {
		this.maxOcorrencias = 50;
		this.codPessoaJuridica = codPessoaJuridica;
		this.codTipoContratoNegocio = codTipoContratoNegocio;
		this.numSeqContratoNegocio = numSeqContratoNegocio;
	}

	public Integer getMaxOcorrencias() {
		return maxOcorrencias;
	}

	public void setMaxOcorrencias(Integer maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}

	public Long getCodPessoaJuridica() {
		return codPessoaJuridica;
	}

	public void setCodPessoaJuridica(Long codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}

	public Integer getCodTipoContratoNegocio() {
		return codTipoContratoNegocio;
	}

	public void setCodTipoContratoNegocio(Integer codTipoContratoNegocio) {
		this.codTipoContratoNegocio = codTipoContratoNegocio;
	}

	public Long getNumSeqContratoNegocio() {
		return numSeqContratoNegocio;
	}

	public void setNumSeqContratoNegocio(Long numSeqContratoNegocio) {
		this.numSeqContratoNegocio = numSeqContratoNegocio;
	}
	
}