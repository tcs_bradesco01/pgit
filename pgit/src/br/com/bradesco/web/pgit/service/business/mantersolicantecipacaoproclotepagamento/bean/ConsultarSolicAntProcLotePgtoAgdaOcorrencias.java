package br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.bean;

public class ConsultarSolicAntProcLotePgtoAgdaOcorrencias {

	private Integer cdSolicitacaoPagamentoIntegrado;
	private Integer nrSolicitacaoPagamentoIntegrado;
	private Long cdpessoaJuridicaContrato;
	private Integer cdTipoContratoNegocio;
	private Long nrSequenciaContratoNegocio;
	private Long nrLoteInterno;
	private Integer cdTipoLayoutArquivo;
	private String dsTipoLayoutArquivo;
	private Integer cdSituacaoSolicitacaoPagamento;
	private String dsSituacaoSolicitaoPgto;
	private Integer cdMotivoSolicitacao;
	private String dsMotivoSolicitacao;
	private String dsEmpresa;
	private String dsSolicitacao;
	private String hrSolicitacao;

	public Integer getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}

	public void setCdSolicitacaoPagamentoIntegrado(
			Integer cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}

	public Integer getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}

	public void setNrSolicitacaoPagamentoIntegrado(
			Integer nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}

	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public Long getNrLoteInterno() {
		return nrLoteInterno;
	}

	public void setNrLoteInterno(Long nrLoteInterno) {
		this.nrLoteInterno = nrLoteInterno;
	}

	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	public Integer getCdSituacaoSolicitacaoPagamento() {
		return cdSituacaoSolicitacaoPagamento;
	}

	public void setCdSituacaoSolicitacaoPagamento(
			Integer cdSituacaoSolicitacaoPagamento) {
		this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
	}

	public String getDsSituacaoSolicitaoPgto() {
		return dsSituacaoSolicitaoPgto;
	}

	public void setDsSituacaoSolicitaoPgto(String dsSituacaoSolicitaoPgto) {
		this.dsSituacaoSolicitaoPgto = dsSituacaoSolicitaoPgto;
	}

	public Integer getCdMotivoSolicitacao() {
		return cdMotivoSolicitacao;
	}

	public void setCdMotivoSolicitacao(Integer cdMotivoSolicitacao) {
		this.cdMotivoSolicitacao = cdMotivoSolicitacao;
	}

	public String getDsMotivoSolicitacao() {
		return dsMotivoSolicitacao;
	}

	public void setDsMotivoSolicitacao(String dsMotivoSolicitacao) {
		this.dsMotivoSolicitacao = dsMotivoSolicitacao;
	}

	public String getDsEmpresa() {
		return dsEmpresa;
	}

	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	public String getDsSolicitacao() {
		return dsSolicitacao;
	}

	public void setDsSolicitacao(String dsSolicitacao) {
		this.dsSolicitacao = dsSolicitacao;
	}

	public String getHrSolicitacao() {
		return hrSolicitacao;
	}

	public void setHrSolicitacao(String hrSolicitacao) {
		this.hrSolicitacao = hrSolicitacao;
	}

}
