/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaralcada.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarAlcadaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarAlcadaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _cdProgPagamentoIntegrado
     */
    private java.lang.String _cdProgPagamentoIntegrado;

    /**
     * Field _cdTipoAcaoPagamento
     */
    private int _cdTipoAcaoPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoAcaoPagamento
     */
    private boolean _has_cdTipoAcaoPagamento;

    /**
     * Field _cdTipoAcessoAlcada
     */
    private int _cdTipoAcessoAlcada = 0;

    /**
     * keeps track of state for field: _cdTipoAcessoAlcada
     */
    private boolean _has_cdTipoAcessoAlcada;

    /**
     * Field _qtConsultas
     */
    private int _qtConsultas = 0;

    /**
     * keeps track of state for field: _qtConsultas
     */
    private boolean _has_qtConsultas;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarAlcadaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaralcada.request.ListarAlcadaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoAcaoPagamento
     * 
     */
    public void deleteCdTipoAcaoPagamento()
    {
        this._has_cdTipoAcaoPagamento= false;
    } //-- void deleteCdTipoAcaoPagamento() 

    /**
     * Method deleteCdTipoAcessoAlcada
     * 
     */
    public void deleteCdTipoAcessoAlcada()
    {
        this._has_cdTipoAcessoAlcada= false;
    } //-- void deleteCdTipoAcessoAlcada() 

    /**
     * Method deleteQtConsultas
     * 
     */
    public void deleteQtConsultas()
    {
        this._has_qtConsultas= false;
    } //-- void deleteQtConsultas() 

    /**
     * Returns the value of field 'cdProgPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'cdProgPagamentoIntegrado'.
     */
    public java.lang.String getCdProgPagamentoIntegrado()
    {
        return this._cdProgPagamentoIntegrado;
    } //-- java.lang.String getCdProgPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdTipoAcaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcaoPagamento'.
     */
    public int getCdTipoAcaoPagamento()
    {
        return this._cdTipoAcaoPagamento;
    } //-- int getCdTipoAcaoPagamento() 

    /**
     * Returns the value of field 'cdTipoAcessoAlcada'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcessoAlcada'.
     */
    public int getCdTipoAcessoAlcada()
    {
        return this._cdTipoAcessoAlcada;
    } //-- int getCdTipoAcessoAlcada() 

    /**
     * Returns the value of field 'qtConsultas'.
     * 
     * @return int
     * @return the value of field 'qtConsultas'.
     */
    public int getQtConsultas()
    {
        return this._qtConsultas;
    } //-- int getQtConsultas() 

    /**
     * Method hasCdTipoAcaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcaoPagamento()
    {
        return this._has_cdTipoAcaoPagamento;
    } //-- boolean hasCdTipoAcaoPagamento() 

    /**
     * Method hasCdTipoAcessoAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcessoAlcada()
    {
        return this._has_cdTipoAcessoAlcada;
    } //-- boolean hasCdTipoAcessoAlcada() 

    /**
     * Method hasQtConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtConsultas()
    {
        return this._has_qtConsultas;
    } //-- boolean hasQtConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProgPagamentoIntegrado'.
     * 
     * @param cdProgPagamentoIntegrado the value of field
     * 'cdProgPagamentoIntegrado'.
     */
    public void setCdProgPagamentoIntegrado(java.lang.String cdProgPagamentoIntegrado)
    {
        this._cdProgPagamentoIntegrado = cdProgPagamentoIntegrado;
    } //-- void setCdProgPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoAcaoPagamento'.
     * 
     * @param cdTipoAcaoPagamento the value of field
     * 'cdTipoAcaoPagamento'.
     */
    public void setCdTipoAcaoPagamento(int cdTipoAcaoPagamento)
    {
        this._cdTipoAcaoPagamento = cdTipoAcaoPagamento;
        this._has_cdTipoAcaoPagamento = true;
    } //-- void setCdTipoAcaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoAcessoAlcada'.
     * 
     * @param cdTipoAcessoAlcada the value of field
     * 'cdTipoAcessoAlcada'.
     */
    public void setCdTipoAcessoAlcada(int cdTipoAcessoAlcada)
    {
        this._cdTipoAcessoAlcada = cdTipoAcessoAlcada;
        this._has_cdTipoAcessoAlcada = true;
    } //-- void setCdTipoAcessoAlcada(int) 

    /**
     * Sets the value of field 'qtConsultas'.
     * 
     * @param qtConsultas the value of field 'qtConsultas'.
     */
    public void setQtConsultas(int qtConsultas)
    {
        this._qtConsultas = qtConsultas;
        this._has_qtConsultas = true;
    } //-- void setQtConsultas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarAlcadaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaralcada.request.ListarAlcadaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaralcada.request.ListarAlcadaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaralcada.request.ListarAlcadaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaralcada.request.ListarAlcadaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
