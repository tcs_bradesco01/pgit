/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.IManterSolicGeracaoArqRetBasePagtosService;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.IManterSolicGeracaoArqRetBasePagtosServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.DetalharSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.DetalharSolBasePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ExcluirSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ExcluirSolBasePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.IncluirSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.IncluirSolBasePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ListarSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ListarSolBasePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasepagto.request.DetalharSolBasePagtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolbasepagto.response.DetalharSolBasePagtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbasepagto.request.ExcluirSolBasePagtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolbasepagto.response.ExcluirSolBasePagtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasepagto.request.IncluirSolBasePagtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolbasepagto.response.IncluirSolBasePagtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbasepagto.request.ListarSolBasePagtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolbasepagto.response.ListarSolBasePagtoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterSolicGeracaoArqRetBasePagtos
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterSolicGeracaoArqRetBasePagtosServiceImpl implements IManterSolicGeracaoArqRetBasePagtosService {
	
	/** Atributo nf. */
	private java.text.NumberFormat nf = java.text.NumberFormat.getInstance( new Locale( "pt_br" ) );
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;	
	
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
    
	
    /**
     * Manter solic geracao arq ret base pagtos service impl.
     */
    public ManterSolicGeracaoArqRetBasePagtosServiceImpl() {
    }
    

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.IManterSolicGeracaoArqRetBasePagtosService#listarSolBasePagto(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ListarSolBasePagtoEntradaDTO)
     */
    public List<ListarSolBasePagtoSaidaDTO> listarSolBasePagto (ListarSolBasePagtoEntradaDTO entradaDTO) {
     ListarSolBasePagtoRequest request = new ListarSolBasePagtoRequest();
     ListarSolBasePagtoResponse response = new ListarSolBasePagtoResponse();
     List<ListarSolBasePagtoSaidaDTO> listaSaida = new ArrayList<ListarSolBasePagtoSaidaDTO>();
     
     request.setNumeroOcorrencias(IManterSolicGeracaoArqRetBasePagtosServiceConstants.NUMERO_OCORRENCIAS);
     request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
 	 request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
	 request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
	 request.setDtInicioInclusaoRemessa(PgitUtil.verificaStringNula(entradaDTO.getDtInicioInclusaoRemessa()));
	 request.setDtFinalInclusaoRemessa(PgitUtil.verificaStringNula(entradaDTO.getDtFinalInclusaoRemessa()));
	 request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));
	 request.setCdSituacaoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacao()));
     request.setCdOrigemSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOrigemSolicitacao()));
     
     response = getFactoryAdapter().getListarSolBasePagtoPDCAdapter().invokeProcess(request);
     
     ListarSolBasePagtoSaidaDTO saida;
     
     for(int i=0;i<response.getOcorrenciasCount();i++){
    	 saida = new ListarSolBasePagtoSaidaDTO();
    	
    	 saida.setCodMensagem(response.getCodMensagem());
		 saida.setMensagem(response.getMensagem());
		 saida.setQtdeRegistros(response.getQtdeRegistros());
    	 
    	 saida.setNrSolicitacaoPagamento(response.getOcorrencias(i).getNrSolicitacaoPagamento());
    	 saida.setHrSolicitacao(response.getOcorrencias(i).getHrSolicitacao());
    	 saida.setHrSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getOcorrencias(i).getHrSolicitacao()));
    	 saida.setCdSituacaoSolicitacao(response.getOcorrencias(i).getCdSituacaoSolicitacao());
    	 saida.setDsSituacaoSolicitacao(response.getOcorrencias(i).getDsSituacaoSolicitacao());
    	 saida.setResultadoProcessamento(response.getOcorrencias(i).getResultadoProcessamento());
    	 saida.setHrAtendimentoSolicitacao(response.getOcorrencias(i).getHrAtendimentoSolicitacao());
    	 saida.setHrAtendimentoSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getOcorrencias(i).getHrAtendimentoSolicitacao()));    	 
    	 saida.setQtdeRegistrosSolicitacao(response.getOcorrencias(i).getQtdeRegistrosSolicitacao());
    	 saida.setQtdeRegistrosSolicitacaoFormatada(nf.format(saida.getQtdeRegistrosSolicitacao()));
    	 
    	 listaSaida.add(saida);
     }
               
     return listaSaida;
    }
    
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.IManterSolicGeracaoArqRetBasePagtosService#detalharSolBasePagto(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.DetalharSolBasePagtoEntradaDTO)
     */
    public DetalharSolBasePagtoSaidaDTO detalharSolBasePagto(DetalharSolBasePagtoEntradaDTO entradaDTO) {
		
    	DetalharSolBasePagtoRequest request = new DetalharSolBasePagtoRequest();
    	DetalharSolBasePagtoResponse response = new DetalharSolBasePagtoResponse();	
    	DetalharSolBasePagtoSaidaDTO saidaDTO = new DetalharSolBasePagtoSaidaDTO();
		
    	request.setNrSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamento()));    	
    	
		response = getFactoryAdapter().getDetalharSolBasePagtoPDCAdapter().invokeProcess(request);		
			
		saidaDTO.setCodMensagem(response.getCodMensagem());
	    saidaDTO.setMensagem(response.getMensagem());
	    
	    saidaDTO.setNrSolicitacaoPagamento(response.getNrSolicitacaoPagamento());
	    saidaDTO.setHrSolicitacaoPagamento(response.getHrSolicitacaoPagamento());
	    saidaDTO.setHrSolicitacaoPagamentoFormatada(FormatarData.formatarDataTrilha(response.getHrSolicitacaoPagamento()));
	    saidaDTO.setDsSituacaoSolicitacao(response.getDsSituacaoSolicitacao());
	    saidaDTO.setResultadoProcessamento(response.getResultadoProcessamento());
	    saidaDTO.setHrAtendimentoSolicitacao(response.getHrAtendimentoSolicitacao());
	    saidaDTO.setHrAtendimentoSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getHrAtendimentoSolicitacao()));
	    saidaDTO.setQtdeRegistrosSolicitacao(response.getQtdeRegistrosSolicitacao());
	    saidaDTO.setDtInicioPagamento(response.getDtInicioPagamento());
	    saidaDTO.setDtFimPagamento(response.getDtFimPagamento());
	    saidaDTO.setCdServico(response.getCdServico());
	    saidaDTO.setCdServicoRelacionado(response.getCdServicoRelacionado());
	    saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
	    saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
	    saidaDTO.setDigitoAgenciaDebito(response.getDigitoAgenciaDebito());
	    saidaDTO.setCdContaDebito(response.getCdContaDebito());
	    saidaDTO.setDigitoContaDebito(response.getDigitoContaDebito());
	    saidaDTO.setIndicadorPagos(response.getIndicadorPagos());
	    saidaDTO.setIndicadorNaoPagos(response.getIndicadorNaoPagos());
	    saidaDTO.setIndicadorAgendados(response.getIndicadorAgendados());
	    saidaDTO.setCdOrigem(response.getCdOrigem());
	    saidaDTO.setCdDestino(response.getCdDestino());
	    saidaDTO.setCdFavorecido(response.getCdFavorecido());
	    saidaDTO.setDsFavorecido(response.getDsFavorecido());
	    saidaDTO.setCdInscricaoFavorecido(response.getCdInscricaoFavorecido());
	    saidaDTO.setCdTipoInscricaoFavorecido(response.getCdTipoInscricaoFavorecido());
	    saidaDTO.setNmMinemonicoFavorecido(response.getNmMinemonicoFavorecido());
	    saidaDTO.setVlTarifaPadrao(response.getVlTarifaPadrao());
	    saidaDTO.setVlrTarifaAtual(response.getVlrTarifaAtual());
	    saidaDTO.setNumPercentualDescTarifa(response.getNumPercentualDescTarifa());
	    saidaDTO.setCdCananInclusao(response.getCdCananInclusao());
	    saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
	    saidaDTO.setCdAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
	    saidaDTO.setNmOperacaoFluxoInclusao(response.getNmOperacaoFluxoInclusao());
	    saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
	    saidaDTO.setHrInclusaoRegistroFormatada(FormatarData.formatarDataTrilha(response.getHrInclusaoRegistro()));
	    saidaDTO.setCdCanalManutencao(response.getCdCanalManutencao());
	    saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());
	    saidaDTO.setCdAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
	    saidaDTO.setNmOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
	    saidaDTO.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
	    saidaDTO.setHrManutencaoRegistroFormatada(FormatarData.formatarDataTrilha(response.getHrManutencaoRegistro()));
	    if(saidaDTO.getCdBancoDebito() != 0 && saidaDTO.getCdBancoDebito() != null){
	    	saidaDTO.setContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(saidaDTO.getCdBancoDebito(), saidaDTO.getCdAgenciaDebito(), saidaDTO.getDigitoAgenciaDebito(), saidaDTO.getCdContaDebito(), saidaDTO.getDigitoContaDebito(), false));
	    }
	    else{
	    	saidaDTO.setContaDebitoFormatado("");
		}
	      
	    
	    return saidaDTO;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.IManterSolicGeracaoArqRetBasePagtosService#incluirSolBasePagto(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.IncluirSolBasePagtoEntradaDTO)
     */
    public IncluirSolBasePagtoSaidaDTO incluirSolBasePagto(IncluirSolBasePagtoEntradaDTO entradaDTO) {
		
    	IncluirSolBasePagtoRequest request = new IncluirSolBasePagtoRequest();
    	IncluirSolBasePagtoResponse response = new IncluirSolBasePagtoResponse();	
    	IncluirSolBasePagtoSaidaDTO saidaDTO = new IncluirSolBasePagtoSaidaDTO();
		    	
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
	    request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
	    request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
	    request.setDtInicioPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtInicioPagamento()));
	    request.setDtFimPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtFimPagamento()));
	    request.setCdServico(PgitUtil.verificaIntegerNulo(entradaDTO.getCdServico()));
	    request.setCdServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdServicoRelacionado()));
	    request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoDebito()));
	    request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaDebito()));
	    request.setCdContaDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdContaDebito()));
	    request.setDigitoContaDebito(PgitUtil.DIGITO_CONTA);
	    request.setCdIndicadorPagos(PgitUtil.verificaStringNula(entradaDTO.getCdIndicadorPagos()));
	    request.setCdIndicadorNaoPagos(PgitUtil.verificaStringNula(entradaDTO.getCdIndicadorNaoPagos()));
	    request.setCdIndicadorAgendados(PgitUtil.verificaStringNula(entradaDTO.getCdIndicadorAgendados()));
	    request.setCdFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getCdFavorecido()));
	    request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));
	    request.setCdTipoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoInscricaoFavorecido()));
	    request.setNmMinemonicoFavorecido(PgitUtil.verificaStringNula(entradaDTO.getNmMinemonicoFavorecido()));
	    request.setCdOrigem(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOrigem()));
	    request.setCdDestino(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDestino()));
	    request.setVlTarifaPadrao(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlTarifaPadrao()));
	    request.setNumPercentualDescTarifa(PgitUtil.verificaBigDecimalNulo(entradaDTO.getNumPercentualDescTarifa()));
	    request.setVlrTarifaAtual(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlrTarifaAtual()));
	    
	    request.setCdPessoaJuridicaConta(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaConta()));
	    request.setCdTipoContratoConta(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoConta()));
	    request.setNrSequenciaContratoConta(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoConta()));
	  
		response = getFactoryAdapter().getIncluirSolBasePagtoPDCAdapter().invokeProcess(request);	
			
		saidaDTO.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
		saidaDTO.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));		
	    
	    return saidaDTO;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.IManterSolicGeracaoArqRetBasePagtosService#excluirSolBasePagto(br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ExcluirSolBasePagtoEntradaDTO)
     */
    public ExcluirSolBasePagtoSaidaDTO excluirSolBasePagto(ExcluirSolBasePagtoEntradaDTO entradaDTO) {
		
    	ExcluirSolBasePagtoRequest request = new ExcluirSolBasePagtoRequest();
    	ExcluirSolBasePagtoResponse response = new ExcluirSolBasePagtoResponse();		
    	
    	request.setNrSolicitacaoGeracaoRetorno(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoGeracaoRetorno()));
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		    	    					
    	ExcluirSolBasePagtoSaidaDTO saidaDTO = new ExcluirSolBasePagtoSaidaDTO();
    	
		response = getFactoryAdapter().getExcluirSolBasePagtoPDCAdapter().invokeProcess(request);	
			
		saidaDTO.setCodMensagem(PgitUtil.verificaStringNula(response.getCodMensagem()));
		saidaDTO.setMensagem(PgitUtil.verificaStringNula(response.getMensagem()));		
	    
	    return saidaDTO;
	} 
    
    
}

