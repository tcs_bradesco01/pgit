/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;



/**
 * Nome: ListarParticipantesOcorrenciasSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarParticipantesOcorrenciasSaidaDTO {

    /** The cd indicador participante. */
    private Integer cdIndicadorParticipante = null;

    /** The cd cpf cnpj. */
    private String cdCpfCnpj = null;

    /** The cd participante. */
    private Long cdParticipante = null;

    /** The ds nome participante. */
    private String dsNomeParticipante = null;

    /** The cd agencia. */
    private Integer cdAgencia = null;

    /** The cd digito agencia. */
    private Integer cdDigitoAgencia = null;

    /** The ds agencia. */
    private String dsAgencia = null;

    /** The cd conta. */
    private Long cdConta = null;

    /** The cd digito conta. */
    private String cdDigitoConta = null;

    /** The ds logradouro participante. */
    private String dsLogradouroParticipante = null;

    /** The nr logradouro participante. */
    private String nrLogradouroParticipante = null;

    /** The ds bairro cliente participante. */
    private String dsBairroClienteParticipante = null;

    /** The ds municipio cliente participante. */
    private String dsMunicipioClienteParticipante = null;

    /** The ds sigla uf participante. */
    private String dsSiglaUfParticipante = null;

    /** The cd cep. */
    private Integer cdCep = null;

    /** The cd cep complemento. */
    private Integer cdCepComplemento = null;
    
    /** The cd indicador quebra. */
    private String cdIndicadorQuebra = null;

    /**
     * Instancia um novo ListarParticipantesOcorrenciasSaidaDTO
     *
     * @see
     */
    public ListarParticipantesOcorrenciasSaidaDTO() {
        super();
    }

    /**
     * Sets the cd indicador participante.
     *
     * @param cdIndicadorParticipante the cd indicador participante
     */
    public void setCdIndicadorParticipante(Integer cdIndicadorParticipante) {
        this.cdIndicadorParticipante = cdIndicadorParticipante;
    }

    /**
     * Gets the cd indicador participante.
     *
     * @return the cd indicador participante
     */
    public Integer getCdIndicadorParticipante() {
        return this.cdIndicadorParticipante;
    }

    /**
     * Sets the cd cpf cnpj.
     *
     * @param cdCpfCnpj the cd cpf cnpj
     */
    public void setCdCpfCnpj(String cdCpfCnpj) {
        this.cdCpfCnpj = cdCpfCnpj;
    }

    /**
     * Gets the cd cpf cnpj.
     *
     * @return the cd cpf cnpj
     */
    public String getCdCpfCnpj() {
        return this.cdCpfCnpj;
    }

    /**
     * Sets the cd participante.
     *
     * @param cdParticipante the cd participante
     */
    public void setCdParticipante(Long cdParticipante) {
        this.cdParticipante = cdParticipante;
    }

    /**
     * Gets the cd participante.
     *
     * @return the cd participante
     */
    public Long getCdParticipante() {
        return this.cdParticipante;
    }

    /**
     * Sets the ds nome participante.
     *
     * @param dsNomeParticipante the ds nome participante
     */
    public void setDsNomeParticipante(String dsNomeParticipante) {
        this.dsNomeParticipante = dsNomeParticipante;
    }

    /**
     * Gets the ds nome participante.
     *
     * @return the ds nome participante
     */
    public String getDsNomeParticipante() {
        return this.dsNomeParticipante;
    }

    /**
     * Sets the cd agencia.
     *
     * @param cdAgencia the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
        this.cdAgencia = cdAgencia;
    }

    /**
     * Gets the cd agencia.
     *
     * @return the cd agencia
     */
    public Integer getCdAgencia() {
        return this.cdAgencia;
    }

    /**
     * Sets the cd digito agencia.
     *
     * @param cdDigitoAgencia the cd digito agencia
     */
    public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
        this.cdDigitoAgencia = cdDigitoAgencia;
    }

    /**
     * Gets the cd digito agencia.
     *
     * @return the cd digito agencia
     */
    public Integer getCdDigitoAgencia() {
        return this.cdDigitoAgencia;
    }

    /**
     * Sets the ds agencia.
     *
     * @param dsAgencia the ds agencia
     */
    public void setDsAgencia(String dsAgencia) {
        this.dsAgencia = dsAgencia;
    }

    /**
     * Gets the ds agencia.
     *
     * @return the ds agencia
     */
    public String getDsAgencia() {
        return this.dsAgencia;
    }

    /**
     * Sets the cd conta.
     *
     * @param cdConta the cd conta
     */
    public void setCdConta(Long cdConta) {
        this.cdConta = cdConta;
    }

    /**
     * Gets the cd conta.
     *
     * @return the cd conta
     */
    public Long getCdConta() {
        return this.cdConta;
    }

    /**
     * Sets the cd digito conta.
     *
     * @param cdDigitoConta the cd digito conta
     */
    public void setCdDigitoConta(String cdDigitoConta) {
        this.cdDigitoConta = cdDigitoConta;
    }

    /**
     * Gets the cd digito conta.
     *
     * @return the cd digito conta
     */
    public String getCdDigitoConta() {
        return this.cdDigitoConta;
    }

    /**
     * Sets the ds logradouro participante.
     *
     * @param dsLogradouroParticipante the ds logradouro participante
     */
    public void setDsLogradouroParticipante(String dsLogradouroParticipante) {
        this.dsLogradouroParticipante = dsLogradouroParticipante;
    }

    /**
     * Gets the ds logradouro participante.
     *
     * @return the ds logradouro participante
     */
    public String getDsLogradouroParticipante() {
        return this.dsLogradouroParticipante;
    }

    /**
     * Sets the nr logradouro participante.
     *
     * @param nrLogradouroParticipante the nr logradouro participante
     */
    public void setNrLogradouroParticipante(String nrLogradouroParticipante) {
        this.nrLogradouroParticipante = nrLogradouroParticipante;
    }

    /**
     * Gets the nr logradouro participante.
     *
     * @return the nr logradouro participante
     */
    public String getNrLogradouroParticipante() {
        return this.nrLogradouroParticipante;
    }

    /**
     * Sets the ds bairro cliente participante.
     *
     * @param dsBairroClienteParticipante the ds bairro cliente participante
     */
    public void setDsBairroClienteParticipante(String dsBairroClienteParticipante) {
        this.dsBairroClienteParticipante = dsBairroClienteParticipante;
    }

    /**
     * Gets the ds bairro cliente participante.
     *
     * @return the ds bairro cliente participante
     */
    public String getDsBairroClienteParticipante() {
        return this.dsBairroClienteParticipante;
    }

    /**
     * Sets the ds municipio cliente participante.
     *
     * @param dsMunicipioClienteParticipante the ds municipio cliente participante
     */
    public void setDsMunicipioClienteParticipante(String dsMunicipioClienteParticipante) {
        this.dsMunicipioClienteParticipante = dsMunicipioClienteParticipante;
    }

    /**
     * Gets the ds municipio cliente participante.
     *
     * @return the ds municipio cliente participante
     */
    public String getDsMunicipioClienteParticipante() {
        return this.dsMunicipioClienteParticipante;
    }

    /**
     * Sets the ds sigla uf participante.
     *
     * @param dsSiglaUfParticipante the ds sigla uf participante
     */
    public void setDsSiglaUfParticipante(String dsSiglaUfParticipante) {
        this.dsSiglaUfParticipante = dsSiglaUfParticipante;
    }

    /**
     * Gets the ds sigla uf participante.
     *
     * @return the ds sigla uf participante
     */
    public String getDsSiglaUfParticipante() {
        return this.dsSiglaUfParticipante;
    }

    /**
     * Sets the cd cep.
     *
     * @param cdCep the cd cep
     */
    public void setCdCep(Integer cdCep) {
        this.cdCep = cdCep;
    }

    /**
     * Gets the cd cep.
     *
     * @return the cd cep
     */
    public Integer getCdCep() {
        return this.cdCep;
    }

    /**
     * Sets the cd cep complemento.
     *
     * @param cdCepComplemento the cd cep complemento
     */
    public void setCdCepComplemento(Integer cdCepComplemento) {
        this.cdCepComplemento = cdCepComplemento;
    }

    /**
     * Gets the cd cep complemento.
     *
     * @return the cd cep complemento
     */
    public Integer getCdCepComplemento() {
        return this.cdCepComplemento;
    }

	/**
	 * Sets the cd indicador quebra.
	 *
	 * @param cdIndicadorQuebra the cdIndicadorQuebra to set
	 */
	public void setCdIndicadorQuebra(String cdIndicadorQuebra) {
		this.cdIndicadorQuebra = cdIndicadorQuebra;
	}

	/**
	 * Gets the cd indicador quebra.
	 *
	 * @return the cdIndicadorQuebra
	 */
	public String getCdIndicadorQuebra() {
		return cdIndicadorQuebra;
	}
}