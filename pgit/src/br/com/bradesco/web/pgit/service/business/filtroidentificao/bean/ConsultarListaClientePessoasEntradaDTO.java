/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroidentificao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroidentificao.bean;

/**
 * Nome: ConsultarListaClientePessoasEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaClientePessoasEntradaDTO implements Cloneable{

	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;

	/** Atributo cdCpfCnpj. */
	private Long cdCpfCnpj;

	/** Atributo cdCpf. */
	private Long cdCpf;

	/** Atributo cdFilialCnpj. */
	private Integer cdFilialCnpj;

	/** Atributo cdControleCnpj. */
	private Integer cdControleCnpj;

	/** Atributo cdControleCpf. */
	private Integer cdControleCpf;

	/** Atributo cdBanco. */
	private Integer cdBanco;

	/** Atributo cdAgenciaBancaria. */
	private Integer cdAgenciaBancaria;

	/** Atributo cdContaBancaria. */
	private Long cdContaBancaria;

	/** Atributo dsNomeRazaoSocial. */
	private String dsNomeRazaoSocial;

	/** Atributo cdTipoConta. */
	private Integer cdTipoConta;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo dtNascimentoFundacao. */
	private String dtNascimentoFundacao = null ; 

	/**
	 * Consultar lista cliente pessoas entrada dto.
	 */
	public ConsultarListaClientePessoasEntradaDTO() {
		super();
	}

	/**
	 * Get: cdAgenciaBancaria.
	 *
	 * @return cdAgenciaBancaria
	 */
	public Integer getCdAgenciaBancaria() {
		return cdAgenciaBancaria;
	}

	/**
	 * Set: cdAgenciaBancaria.
	 *
	 * @param cdAgenciaBancaria the cd agencia bancaria
	 */
	public void setCdAgenciaBancaria(Integer cdAgenciaBancaria) {
		this.cdAgenciaBancaria = cdAgenciaBancaria;
	}

	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}

	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	/**
	 * Get: cdContaBancaria.
	 *
	 * @return cdContaBancaria
	 */
	public Long getCdContaBancaria() {
		return cdContaBancaria;
	}

	/**
	 * Set: cdContaBancaria.
	 *
	 * @param cdContaBancaria the cd conta bancaria
	 */
	public void setCdContaBancaria(Long cdContaBancaria) {
		this.cdContaBancaria = cdContaBancaria;
	}

	/**
	 * Get: cdControleCnpj.
	 *
	 * @return cdControleCnpj
	 */
	public Integer getCdControleCnpj() {
		return cdControleCnpj;
	}

	/**
	 * Set: cdControleCnpj.
	 *
	 * @param cdControleCnpj the cd controle cnpj
	 */
	public void setCdControleCnpj(Integer cdControleCnpj) {
		this.cdControleCnpj = cdControleCnpj;
	}

	/**
	 * Get: cdControleCpf.
	 *
	 * @return cdControleCpf
	 */
	public Integer getCdControleCpf() {
		return cdControleCpf;
	}

	/**
	 * Set: cdControleCpf.
	 *
	 * @param cdControleCpf the cd controle cpf
	 */
	public void setCdControleCpf(Integer cdControleCpf) {
		this.cdControleCpf = cdControleCpf;
	}

	/**
	 * Get: cdCpf.
	 *
	 * @return cdCpf
	 */
	public Long getCdCpf() {
		return cdCpf;
	}

	/**
	 * Set: cdCpf.
	 *
	 * @param cdCpf the cd cpf
	 */
	public void setCdCpf(Long cdCpf) {
		this.cdCpf = cdCpf;
	}

	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}

	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}

	/**
	 * Get: cdFilialCnpj.
	 *
	 * @return cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}

	/**
	 * Set: cdFilialCnpj.
	 *
	 * @param cdFilialCnpj the cd filial cnpj
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}

	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}

	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}

	/**
	 * Get: dsNomeRazaoSocial.
	 *
	 * @return dsNomeRazaoSocial
	 */
	public String getDsNomeRazaoSocial() {
		return dsNomeRazaoSocial;
	}

	/**
	 * Set: dsNomeRazaoSocial.
	 *
	 * @param dsNomeRazaoSocial the ds nome razao social
	 */
	public void setDsNomeRazaoSocial(String dsNomeRazaoSocial) {
		this.dsNomeRazaoSocial = dsNomeRazaoSocial;
	}

	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}

	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}

	/**
	 * Is cnpj ou cpf.
	 *
	 * @return true, if is cnpj ou cpf
	 */
	public boolean isCnpjOuCpf() {
		return this.getCdFilialCnpj() == null || this.getCdFilialCnpj().intValue() == 0;
	}
	

	/**
	 * Get: dtNascimentoFundacao.
	 *
	 * @return dtNascimentoFundacao
	 */
	public String getDtNascimentoFundacao() {
		return dtNascimentoFundacao;
	}


	/**
	 * Set: dtNascimentoFundacao.
	 *
	 * @param dtNascimentoFundacao the dt nascimento fundacao
	 */
	public void setDtNascimentoFundacao(String dtNascimentoFundacao) {
		this.dtNascimentoFundacao = dtNascimentoFundacao;
	}

	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}

	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}

	/**
	 * Is cnpj e cpf desabilitados.
	 *
	 * @return true, if is cnpj e cpf desabilitados
	 */
	public boolean isCnpjECpfDesabilitados() {

		return ((this.getCdControleCpf()  == null || this.getCdControleCpf().intValue() == 0) &&
				(this.getCdControleCnpj() == null || this.getCdControleCnpj().intValue() == 0)&&
				(this.getCdCpfCnpj()      == null || this.getCdCpfCnpj().intValue() == 0)     &&
				(this.getCdCpf()          == null || this.getCdCpf().intValue() == 0)         &&
				(this.getCdFilialCnpj()   == null || this.getCdFilialCnpj().intValue() == 0));
	}


	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
