/*
 * Nome: br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean;

/**
 * Nome: TipoPendenciaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoPendenciaSaidaDTO {
	

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdIndicadorResponsavelPagamento. */
	private int cdIndicadorResponsavelPagamento;
	
	/** Atributo dsEmailRespPgto. */
	private String dsEmailRespPgto;
	
	/** Atributo dsIndicadorResponsavelPagamento. */
	private String dsIndicadorResponsavelPagamento;
	
	/** Atributo cdPendenciaPagamentoIntegrado. */
	private long cdPendenciaPagamentoIntegrado;
	
	/** Atributo cdTipoBaixaPendencia. */
	private int cdTipoBaixaPendencia;	
	
	/** Atributo dsTipoBaixaPendencia. */
	private String dsTipoBaixaPendencia;
	
	/** Atributo cdTipoUnidadeOrganizacional. */
	private int cdTipoUnidadeOrganizacional;
	
	/** Atributo dsTipoUnidadeOrganizacional. */
	private String dsTipoUnidadeOrganizacional;
	
	/** Atributo dsPendenciaPagamentoIntegrado. */
	private String dsPendenciaPagamentoIntegrado;
	
	/** Atributo cdPessoaJuridica. */
	private long cdPessoaJuridica;
	
	/** Atributo dsCodigoPessoaJuridica. */
	private String dsCodigoPessoaJuridica;
	
	/** Atributo nrSequenciaUnidadeOrganizacional. */
	private int nrSequenciaUnidadeOrganizacional;
	
	/** Atributo dsNumeroSequenciaUnidadeOrganizacional. */
	private String dsNumeroSequenciaUnidadeOrganizacional;
	
	/** Atributo dsResumoPendenciaPagamento. */
	private String dsResumoPendenciaPagamento;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdAutenticacaoSegurancaInclusao. */
	private String cdAutenticacaoSegurancaInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo nrOperacaoFluxoInclusao. */
	private String nrOperacaoFluxoInclusao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdAutenticacaoSegurancaManutencao. */
	private String cdAutenticacaoSegurancaManutencao;
	
	/** Atributo hrManutencaoRegistroManutencao. */
	private String hrManutencaoRegistroManutencao;
	
	/** Atributo nrOperacaoFluxoManutencao. */
	private String nrOperacaoFluxoManutencao;	
	
	/**
	 * Get: cdIndicadorResponsavelPagamento.
	 *
	 * @return cdIndicadorResponsavelPagamento
	 */
	public int getCdIndicadorResponsavelPagamento() {
		return cdIndicadorResponsavelPagamento;
	}
	
	/**
	 * Set: cdIndicadorResponsavelPagamento.
	 *
	 * @param cdIndicadorResponsavelPagamento the cd indicador responsavel pagamento
	 */
	public void setCdIndicadorResponsavelPagamento(
			int cdIndicadorResponsavelPagamento) {
		this.cdIndicadorResponsavelPagamento = cdIndicadorResponsavelPagamento;
	}
	
	/**
	 * Get: cdPendenciaPagamentoIntegrado.
	 *
	 * @return cdPendenciaPagamentoIntegrado
	 */
	public long getCdPendenciaPagamentoIntegrado() {
		return cdPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Set: cdPendenciaPagamentoIntegrado.
	 *
	 * @param cdPendenciaPagamentoIntegrado the cd pendencia pagamento integrado
	 */
	public void setCdPendenciaPagamentoIntegrado(long cdPendenciaPagamentoIntegrado) {
		this.cdPendenciaPagamentoIntegrado = cdPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Get: cdTipoBaixaPendencia.
	 *
	 * @return cdTipoBaixaPendencia
	 */
	public int getCdTipoBaixaPendencia() {
		return cdTipoBaixaPendencia;
	}
	
	/**
	 * Set: cdTipoBaixaPendencia.
	 *
	 * @param cdTipoBaixaPendencia the cd tipo baixa pendencia
	 */
	public void setCdTipoBaixaPendencia(int cdTipoBaixaPendencia) {
		this.cdTipoBaixaPendencia = cdTipoBaixaPendencia;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacional.
	 *
	 * @return cdTipoUnidadeOrganizacional
	 */
	public int getCdTipoUnidadeOrganizacional() {
		return cdTipoUnidadeOrganizacional;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacional.
	 *
	 * @param cdTipoUnidadeOrganizacional the cd tipo unidade organizacional
	 */
	public void setCdTipoUnidadeOrganizacional(int cdTipoUnidadeOrganizacional) {
		this.cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsPendenciaPagamentoIntegrado.
	 *
	 * @return dsPendenciaPagamentoIntegrado
	 */
	public String getDsPendenciaPagamentoIntegrado() {
		return dsPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Set: dsPendenciaPagamentoIntegrado.
	 *
	 * @param dsPendenciaPagamentoIntegrado the ds pendencia pagamento integrado
	 */
	public void setDsPendenciaPagamentoIntegrado(
			String dsPendenciaPagamentoIntegrado) {
		this.dsPendenciaPagamentoIntegrado = dsPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: dsTipoUnidadeOrganizacional.
	 *
	 * @return dsTipoUnidadeOrganizacional
	 */
	public String getDsTipoUnidadeOrganizacional() {
		return dsTipoUnidadeOrganizacional;
	}
	
	/**
	 * Set: dsTipoUnidadeOrganizacional.
	 *
	 * @param dsTipoUnidadeOrganizacional the ds tipo unidade organizacional
	 */
	public void setDsTipoUnidadeOrganizacional(String dsTipoUnidadeOrganizacional) {
		this.dsTipoUnidadeOrganizacional = dsTipoUnidadeOrganizacional;
	}
	
	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}
	
	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: dsCodigoPessoaJuridica.
	 *
	 * @return dsCodigoPessoaJuridica
	 */
	public String getDsCodigoPessoaJuridica() {
		return dsCodigoPessoaJuridica;
	}
	
	/**
	 * Set: dsCodigoPessoaJuridica.
	 *
	 * @param dsCodigoPessoaJuridica the ds codigo pessoa juridica
	 */
	public void setDsCodigoPessoaJuridica(String dsCodigoPessoaJuridica) {
		this.dsCodigoPessoaJuridica = dsCodigoPessoaJuridica;
	}
	
	/**
	 * Get: dsNumeroSequenciaUnidadeOrganizacional.
	 *
	 * @return dsNumeroSequenciaUnidadeOrganizacional
	 */
	public String getDsNumeroSequenciaUnidadeOrganizacional() {
		return dsNumeroSequenciaUnidadeOrganizacional;
	}
	
	/**
	 * Set: dsNumeroSequenciaUnidadeOrganizacional.
	 *
	 * @param dsNumeroSequenciaUnidadeOrganizacional the ds numero sequencia unidade organizacional
	 */
	public void setDsNumeroSequenciaUnidadeOrganizacional(
			String dsNumeroSequenciaUnidadeOrganizacional) {
		this.dsNumeroSequenciaUnidadeOrganizacional = dsNumeroSequenciaUnidadeOrganizacional;
	}
	
	/**
	 * Get: dsResumoPendenciaPagamento.
	 *
	 * @return dsResumoPendenciaPagamento
	 */
	public String getDsResumoPendenciaPagamento() {
		return dsResumoPendenciaPagamento;
	}
	
	/**
	 * Set: dsResumoPendenciaPagamento.
	 *
	 * @param dsResumoPendenciaPagamento the ds resumo pendencia pagamento
	 */
	public void setDsResumoPendenciaPagamento(String dsResumoPendenciaPagamento) {
		this.dsResumoPendenciaPagamento = dsResumoPendenciaPagamento;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistroManutencao.
	 *
	 * @return hrManutencaoRegistroManutencao
	 */
	public String getHrManutencaoRegistroManutencao() {
		return hrManutencaoRegistroManutencao;
	}
	
	/**
	 * Set: hrManutencaoRegistroManutencao.
	 *
	 * @param hrManutencaoRegistroManutencao the hr manutencao registro manutencao
	 */
	public void setHrManutencaoRegistroManutencao(
			String hrManutencaoRegistroManutencao) {
		this.hrManutencaoRegistroManutencao = hrManutencaoRegistroManutencao;
	}
	
	/**
	 * Get: nrOperacaoFluxoInclusao.
	 *
	 * @return nrOperacaoFluxoInclusao
	 */
	public String getNrOperacaoFluxoInclusao() {
		return nrOperacaoFluxoInclusao;
	}
	
	/**
	 * Set: nrOperacaoFluxoInclusao.
	 *
	 * @param nrOperacaoFluxoInclusao the nr operacao fluxo inclusao
	 */
	public void setNrOperacaoFluxoInclusao(String nrOperacaoFluxoInclusao) {
		this.nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
	}
	
	/**
	 * Get: nrOperacaoFluxoManutencao.
	 *
	 * @return nrOperacaoFluxoManutencao
	 */
	public String getNrOperacaoFluxoManutencao() {
		return nrOperacaoFluxoManutencao;
	}
	
	/**
	 * Set: nrOperacaoFluxoManutencao.
	 *
	 * @param nrOperacaoFluxoManutencao the nr operacao fluxo manutencao
	 */
	public void setNrOperacaoFluxoManutencao(String nrOperacaoFluxoManutencao) {
		this.nrOperacaoFluxoManutencao = nrOperacaoFluxoManutencao;
	}
	
	/**
	 * Get: nrSequenciaUnidadeOrganizacional.
	 *
	 * @return nrSequenciaUnidadeOrganizacional
	 */
	public int getNrSequenciaUnidadeOrganizacional() {
		return nrSequenciaUnidadeOrganizacional;
	}
	
	/**
	 * Set: nrSequenciaUnidadeOrganizacional.
	 *
	 * @param nrSequenciaUnidadeOrganizacional the nr sequencia unidade organizacional
	 */
	public void setNrSequenciaUnidadeOrganizacional(
			int nrSequenciaUnidadeOrganizacional) {
		this.nrSequenciaUnidadeOrganizacional = nrSequenciaUnidadeOrganizacional;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsIndicadorResponsavelPagamento.
	 *
	 * @return dsIndicadorResponsavelPagamento
	 */
	public String getDsIndicadorResponsavelPagamento() {
		return dsIndicadorResponsavelPagamento;
	}
	
	/**
	 * Set: dsIndicadorResponsavelPagamento.
	 *
	 * @param dsIndicadorResponsavelPagamento the ds indicador responsavel pagamento
	 */
	public void setDsIndicadorResponsavelPagamento(
			String dsIndicadorResponsavelPagamento) {
		this.dsIndicadorResponsavelPagamento = dsIndicadorResponsavelPagamento;
	}
	
	/**
	 * Get: dsTipoBaixaPendencia.
	 *
	 * @return dsTipoBaixaPendencia
	 */
	public String getDsTipoBaixaPendencia() {
		return dsTipoBaixaPendencia;
	}
	
	/**
	 * Set: dsTipoBaixaPendencia.
	 *
	 * @param dsTipoBaixaPendencia the ds tipo baixa pendencia
	 */
	public void setDsTipoBaixaPendencia(String dsTipoBaixaPendencia) {
		this.dsTipoBaixaPendencia = dsTipoBaixaPendencia;
	}
	
	/**
	 * Get: dsEmailRespPgto.
	 *
	 * @return dsEmailRespPgto
	 */
	public String getDsEmailRespPgto() {
		return dsEmailRespPgto;
	}
	
	/**
	 * Set: dsEmailRespPgto.
	 *
	 * @param dsEmailRespPgto the ds email resp pgto
	 */
	public void setDsEmailRespPgto(String dsEmailRespPgto) {
		this.dsEmailRespPgto = dsEmailRespPgto;
	}
	
}
