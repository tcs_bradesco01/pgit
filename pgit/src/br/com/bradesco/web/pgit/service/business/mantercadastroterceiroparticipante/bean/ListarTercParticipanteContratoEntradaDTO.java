/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean;

/**
 * Nome: ListarTercParticipanteContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTercParticipanteContratoEntradaDTO{
	
	/** Atributo qtdeConsultas. */
	private Integer qtdeConsultas;
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cdCpfCnpjTerceiro. */
	private Long cdCpfCnpjTerceiro;
	
	/** Atributo cdFilialCnpjTerceiro. */
	private Integer cdFilialCnpjTerceiro;
	
	/** Atributo cdControleCnpjTerceiro. */
	private Integer cdControleCnpjTerceiro;
	
	/** Atributo nrRastreaTerceiro. */
	private Integer nrRastreaTerceiro;

	/**
	 * Get: qtdeConsultas.
	 *
	 * @return qtdeConsultas
	 */
	public Integer getQtdeConsultas(){
		return qtdeConsultas;
	}

	/**
	 * Set: qtdeConsultas.
	 *
	 * @param qtdeConsultas the qtde consultas
	 */
	public void setQtdeConsultas(Integer qtdeConsultas){
		this.qtdeConsultas = qtdeConsultas;
	}

	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato(){
		return cdpessoaJuridicaContrato;
	}

	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato){
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio(){
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio){
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio(){
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio){
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa(){
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa){
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa(){
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa){
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Get: cdCpfCnpjTerceiro.
	 *
	 * @return cdCpfCnpjTerceiro
	 */
	public Long getCdCpfCnpjTerceiro(){
		return cdCpfCnpjTerceiro;
	}

	/**
	 * Set: cdCpfCnpjTerceiro.
	 *
	 * @param cdCpfCnpjTerceiro the cd cpf cnpj terceiro
	 */
	public void setCdCpfCnpjTerceiro(Long cdCpfCnpjTerceiro){
		this.cdCpfCnpjTerceiro = cdCpfCnpjTerceiro;
	}

	/**
	 * Get: cdFilialCnpjTerceiro.
	 *
	 * @return cdFilialCnpjTerceiro
	 */
	public Integer getCdFilialCnpjTerceiro(){
		return cdFilialCnpjTerceiro;
	}

	/**
	 * Set: cdFilialCnpjTerceiro.
	 *
	 * @param cdFilialCnpjTerceiro the cd filial cnpj terceiro
	 */
	public void setCdFilialCnpjTerceiro(Integer cdFilialCnpjTerceiro){
		this.cdFilialCnpjTerceiro = cdFilialCnpjTerceiro;
	}

	/**
	 * Get: cdControleCnpjTerceiro.
	 *
	 * @return cdControleCnpjTerceiro
	 */
	public Integer getCdControleCnpjTerceiro(){
		return cdControleCnpjTerceiro;
	}

	/**
	 * Set: cdControleCnpjTerceiro.
	 *
	 * @param cdControleCnpjTerceiro the cd controle cnpj terceiro
	 */
	public void setCdControleCnpjTerceiro(Integer cdControleCnpjTerceiro){
		this.cdControleCnpjTerceiro = cdControleCnpjTerceiro;
	}

	/**
	 * Get: nrRastreaTerceiro.
	 *
	 * @return nrRastreaTerceiro
	 */
	public Integer getNrRastreaTerceiro() {
	    return nrRastreaTerceiro;
	}

	/**
	 * Set: nrRastreaTerceiro.
	 *
	 * @param nrRastreaTerceiro the nr rastrea terceiro
	 */
	public void setNrRastreaTerceiro(Integer nrRastreaTerceiro) {
	    this.nrRastreaTerceiro = nrRastreaTerceiro;
	}
}