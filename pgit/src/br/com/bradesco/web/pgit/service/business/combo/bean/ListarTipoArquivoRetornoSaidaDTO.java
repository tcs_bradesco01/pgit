/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarTipoArquivoRetornoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTipoArquivoRetornoSaidaDTO {
	

	/** Atributo cdTipoArquivoRetorno. */
	private int cdTipoArquivoRetorno;
	
	/** Atributo dsTipoArquivoRetorno. */
	private String dsTipoArquivoRetorno;
	
	/**
	 * Listar tipo arquivo retorno saida dto.
	 *
	 * @param cdTipoArquivoRetorno the cd tipo arquivo retorno
	 * @param dsTipoArquivoRetorno the ds tipo arquivo retorno
	 */
	public ListarTipoArquivoRetornoSaidaDTO( int cdTipoArquivoRetorno, String dsTipoArquivoRetorno){
		this.cdTipoArquivoRetorno = cdTipoArquivoRetorno;
		this.dsTipoArquivoRetorno = dsTipoArquivoRetorno;
	}
	
	
	/**
	 * Get: cdTipoArquivoRetorno.
	 *
	 * @return cdTipoArquivoRetorno
	 */
	public int getCdTipoArquivoRetorno() {
		return cdTipoArquivoRetorno;
	}
	
	/**
	 * Set: cdTipoArquivoRetorno.
	 *
	 * @param cdArquivoRetorno the cd tipo arquivo retorno
	 */
	public void setCdTipoArquivoRetorno(int cdArquivoRetorno) {
		this.cdTipoArquivoRetorno = cdArquivoRetorno;
	}
	
	/**
	 * Get: dsTipoArquivoRetorno.
	 *
	 * @return dsTipoArquivoRetorno
	 */
	public String getDsTipoArquivoRetorno() {
		return dsTipoArquivoRetorno;
	}
	
	/**
	 * Set: dsTipoArquivoRetorno.
	 *
	 * @param dsArquivoRetorno the ds tipo arquivo retorno
	 */
	public void setDsTipoArquivoRetorno(String dsArquivoRetorno) {
		this.dsTipoArquivoRetorno = dsArquivoRetorno;
	}
	
	

}
