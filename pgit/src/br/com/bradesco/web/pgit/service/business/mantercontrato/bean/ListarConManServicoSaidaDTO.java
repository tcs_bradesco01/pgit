/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarConManServicoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarConManServicoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo hrInclusaoRegistroHistorico. */
	private String hrInclusaoRegistroHistorico;
	
	/** Atributo cdProduto. */
	private int cdProduto;
	
	/** Atributo dsProduto. */
	private String dsProduto;
	
	/** Atributo dtManutencao. */
	private String dtManutencao;
	
	/** Atributo hrManutencao. */
	private String hrManutencao;
	
	/** Atributo cdUsuario. */
	private String cdUsuario;
	
	/** Atributo cdModalidade. */
	private int cdModalidade;
	
	/** Atributo dsModalidade. */
	private String dsModalidade;
	
	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;
	
	/** Atributo dsServico. */
	private String dsServico;
	
	/** Atributo dsOperacao. */
	private String dsOperacao;
	
	/** Atributo dtIniVig. */
	private String dtIniVig;
	
	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;
	
	/** Atributo cdModServico. */
	private Integer cdModServico;
	
	/** Atributo cdOperacaoModServico. */
	private Integer cdOperacaoModServico;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo dtManutencaoFormatada. */
	private String dtManutencaoFormatada;
	
	//campo cdTipoSerivo do PDC
	/** Atributo cdParametroTela. */
	private int cdParametroTela;	
	

	/**
	 * Get: dtHrManutencaoFormatada.
	 *
	 * @return dtHrManutencaoFormatada
	 */
	public String getDtHrManutencaoFormatada() {
		return dtManutencaoFormatada + " - " + hrManutencao;
	}
	
	/**
	 * Get: dtManutencaoFormatada.
	 *
	 * @return dtManutencaoFormatada
	 */
	public String getDtManutencaoFormatada() {
		return dtManutencaoFormatada;
	}
	
	/**
	 * Set: dtManutencaoFormatada.
	 *
	 * @param hrManutencaoFormatada the dt manutencao formatada
	 */
	public void setDtManutencaoFormatada(String hrManutencaoFormatada) {
		this.dtManutencaoFormatada = hrManutencaoFormatada;
	}

	/**
	 * Get: cdParametroTela.
	 *
	 * @return cdParametroTela
	 */
	public int getCdParametroTela() {
		return cdParametroTela;
	}
	
	/**
	 * Set: cdParametroTela.
	 *
	 * @param cdParametroTela the cd parametro tela
	 */
	public void setCdParametroTela(int cdParametroTela) {
		this.cdParametroTela = cdParametroTela;
	}
	
	/**
	 * Get: cdProduto.
	 *
	 * @return cdProduto
	 */
	public int getCdProduto() {
		return cdProduto;
	}
	
	/**
	 * Set: cdProduto.
	 *
	 * @param cdProduto the cd produto
	 */
	public void setCdProduto(int cdProduto) {
		this.cdProduto = cdProduto;
	}
	
	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}
	
	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsProduto.
	 *
	 * @return dsProduto
	 */
	public String getDsProduto() {
		return dsProduto;
	}
	
	/**
	 * Set: dsProduto.
	 *
	 * @param dsProduto the ds produto
	 */
	public void setDsProduto(String dsProduto) {
		this.dsProduto = dsProduto;
	}
	
	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}
	
	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}
	
	/**
	 * Get: hrInclusaoRegistroHistorico.
	 *
	 * @return hrInclusaoRegistroHistorico
	 */
	public String getHrInclusaoRegistroHistorico() {
		return hrInclusaoRegistroHistorico;
	}
	
	/**
	 * Set: hrInclusaoRegistroHistorico.
	 *
	 * @param hrInclusaoRegistroHistorico the hr inclusao registro historico
	 */
	public void setHrInclusaoRegistroHistorico(String hrInclusaoRegistroHistorico) {
		this.hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
	}
	
	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}
	
	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
	
	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}
	
	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}
	
	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}
	
	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}
	
	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}
	
	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}
	
	/**
	 * Get: dsOperacao.
	 *
	 * @return dsOperacao
	 */
	public String getDsOperacao() {
		return dsOperacao;
	}
	
	/**
	 * Set: dsOperacao.
	 *
	 * @param dsOperacao the ds operacao
	 */
	public void setDsOperacao(String dsOperacao) {
		this.dsOperacao = dsOperacao;
	}
	
	/**
	 * Get: dtIniVig.
	 *
	 * @return dtIniVig
	 */
	public String getDtIniVig() {
		return dtIniVig;
	}
	
	/**
	 * Set: dtIniVig.
	 *
	 * @param dtIniVig the dt ini vig
	 */
	public void setDtIniVig(String dtIniVig) {
		this.dtIniVig = dtIniVig;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: cdModServico.
	 *
	 * @return cdModServico
	 */
	public Integer getCdModServico() {
		return cdModServico;
	}
	
	/**
	 * Set: cdModServico.
	 *
	 * @param cdModServico the cd mod servico
	 */
	public void setCdModServico(Integer cdModServico) {
		this.cdModServico = cdModServico;
	}
	
	/**
	 * Get: cdOperacaoModServico.
	 *
	 * @return cdOperacaoModServico
	 */
	public Integer getCdOperacaoModServico() {
		return cdOperacaoModServico;
	}
	
	/**
	 * Set: cdOperacaoModServico.
	 *
	 * @param cdOperacaoModServico the cd operacao mod servico
	 */
	public void setCdOperacaoModServico(Integer cdOperacaoModServico) {
		this.cdOperacaoModServico = cdOperacaoModServico;
	}
}