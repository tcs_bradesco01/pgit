/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.emissaoautcomp;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.BloqDesbloqEmissaoAutAvisosComprvEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.BloqDesbloqEmissaoAutAvisosComprvSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharHistEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharHistEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ExcluirEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ExcluirEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.IncluirEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.IncluirEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarHistEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarHistEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarIncluirEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarIncluirEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarTipoServicoModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarTipoServicoModalidadeOcorrenciaSaidaDTO;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: EmissaoAutomaticaAvisoComprovante
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IEmissaoAutCompService {

    /**
     * M�todo de exemplo.
     */
    void sampleEmissaoAutomaticaAvisoComprovante();

    
    /**
     * Listar emissao aut comp.
     *
     * @param listarEmissaoAutCompEntradaDTO the listar emissao aut comp entrada dto
     * @return the list< listar emissao aut comp saida dt o>
     */
    List<ListarEmissaoAutCompSaidaDTO> listarEmissaoAutComp(ListarEmissaoAutCompEntradaDTO listarEmissaoAutCompEntradaDTO);
    
    /**
     * Incluir emissao aut comp.
     *
     * @param incluirEmissaoAutCompEntradaDTO the incluir emissao aut comp entrada dto
     * @return the incluir emissao aut comp saida dto
     */
    IncluirEmissaoAutCompSaidaDTO incluirEmissaoAutComp(IncluirEmissaoAutCompEntradaDTO incluirEmissaoAutCompEntradaDTO);
    
    /**
     * Excluir emissao aut comp.
     *
     * @param excluirEmissaoAutCompEntradaDTO the excluir emissao aut comp entrada dto
     * @return the excluir emissao aut comp saida dto
     */
    ExcluirEmissaoAutCompSaidaDTO excluirEmissaoAutComp(ExcluirEmissaoAutCompEntradaDTO excluirEmissaoAutCompEntradaDTO);
    
    /**
     * Detalhar emissao aut comp.
     *
     * @param detalharEmissaoAutCompEntradaDTO the detalhar emissao aut comp entrada dto
     * @return the detalhar emissao aut comp saida dto
     */
    DetalharEmissaoAutCompSaidaDTO detalharEmissaoAutComp (DetalharEmissaoAutCompEntradaDTO detalharEmissaoAutCompEntradaDTO);
    
    /**
     * Listar hist emissao aut comp.
     *
     * @param listarHistEmissaoAutCompEntradaDTO the listar hist emissao aut comp entrada dto
     * @return the list< listar hist emissao aut comp saida dt o>
     */
    List<ListarHistEmissaoAutCompSaidaDTO> listarHistEmissaoAutComp(ListarHistEmissaoAutCompEntradaDTO listarHistEmissaoAutCompEntradaDTO);
    
    /**
     * Detalhar hist emissao aut comp.
     *
     * @param detalharHistEmissaoAutCompEntradaDTO the detalhar hist emissao aut comp entrada dto
     * @return the detalhar hist emissao aut comp saida dto
     */
    DetalharHistEmissaoAutCompSaidaDTO detalharHistEmissaoAutComp (DetalharHistEmissaoAutCompEntradaDTO detalharHistEmissaoAutCompEntradaDTO);
    
    /**
     * Listar incluir emissao aut comp.
     *
     * @param listarIncluirEmissaoAutCompEntradaDTO the listar incluir emissao aut comp entrada dto
     * @return the list< listar incluir emissao aut comp saida dt o>
     */
    List<ListarIncluirEmissaoAutCompSaidaDTO> listarIncluirEmissaoAutComp(ListarIncluirEmissaoAutCompEntradaDTO listarIncluirEmissaoAutCompEntradaDTO);
    
    /**
     * Bloq desbloq emissao aut avisos comprv.
     *
     * @param entradaDTO the entrada dto
     * @return the bloq desbloq emissao aut avisos comprv saida dto
     */
    BloqDesbloqEmissaoAutAvisosComprvSaidaDTO bloqDesbloqEmissaoAutAvisosComprv (BloqDesbloqEmissaoAutAvisosComprvEntradaDTO entradaDTO);
    
    /**
     * Listar tipo servico modalidade.
     *
     * @param entrada the entrada
     * @return the list< listar tipo servico modalidade ocorrencia saida dt o>
     */
    List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> listarTipoServicoModalidade(ListarTipoServicoModalidadeEntradaDTO entrada);
    
}

