/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean;

/**
 * Nome: ConsultarLiberacaoPreviaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarLiberacaoPreviaEntradaDTO {
	
	/** Atributo nmPessoaJuridica. */
	private Long nmPessoaJuridica;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo nrSequencialContrato. */
	private Long nrSequencialContrato;
	
	/** Atributo cdPessoaParticipacao. */
	private Long cdPessoaParticipacao;
	
	/** Atributo dtInicioLiberacao. */
	private String dtInicioLiberacao;
	
	/** Atributo dtFinalLiberacao. */
	private String dtFinalLiberacao;
	
	/** Atributo cdBanco. */
	private Integer cdBanco;
	
	/** Atributo cdAgencia. */
	private Integer cdAgencia;
	
	/** Atributo cdConta. */
	private Long cdConta;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo cdTipoPesquisa. */
	private Integer cdTipoPesquisa;
	
	/**
	 * Get: cdTipoPesquisa.
	 *
	 * @return cdTipoPesquisa
	 */
	public Integer getCdTipoPesquisa() {
		return cdTipoPesquisa;
	}
	
	/**
	 * Set: cdTipoPesquisa.
	 *
	 * @param cdTipoPesquisa the cd tipo pesquisa
	 */
	public void setCdTipoPesquisa(Integer cdTipoPesquisa) {
		this.cdTipoPesquisa = cdTipoPesquisa;
	}
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdPessoaParticipacao.
	 *
	 * @return cdPessoaParticipacao
	 */
	public Long getCdPessoaParticipacao() {
		return cdPessoaParticipacao;
	}
	
	/**
	 * Set: cdPessoaParticipacao.
	 *
	 * @param cdPessoaParticipacao the cd pessoa participacao
	 */
	public void setCdPessoaParticipacao(Long cdPessoaParticipacao) {
		this.cdPessoaParticipacao = cdPessoaParticipacao;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: dtFinalLiberacao.
	 *
	 * @return dtFinalLiberacao
	 */
	public String getDtFinalLiberacao() {
		return dtFinalLiberacao;
	}
	
	/**
	 * Set: dtFinalLiberacao.
	 *
	 * @param dtFinalLiberacao the dt final liberacao
	 */
	public void setDtFinalLiberacao(String dtFinalLiberacao) {
		this.dtFinalLiberacao = dtFinalLiberacao;
	}
	
	/**
	 * Get: dtInicioLiberacao.
	 *
	 * @return dtInicioLiberacao
	 */
	public String getDtInicioLiberacao() {
		return dtInicioLiberacao;
	}
	
	/**
	 * Set: dtInicioLiberacao.
	 *
	 * @param dtInicioLiberacao the dt inicio liberacao
	 */
	public void setDtInicioLiberacao(String dtInicioLiberacao) {
		this.dtInicioLiberacao = dtInicioLiberacao;
	}
	
	/**
	 * Get: nmPessoaJuridica.
	 *
	 * @return nmPessoaJuridica
	 */
	public Long getNmPessoaJuridica() {
		return nmPessoaJuridica;
	}
	
	/**
	 * Set: nmPessoaJuridica.
	 *
	 * @param nmPessoaJuridica the nm pessoa juridica
	 */
	public void setNmPessoaJuridica(Long nmPessoaJuridica) {
		this.nmPessoaJuridica = nmPessoaJuridica;
	}
	
	/**
	 * Get: nrSequencialContrato.
	 *
	 * @return nrSequencialContrato
	 */
	public Long getNrSequencialContrato() {
		return nrSequencialContrato;
	}
	
	/**
	 * Set: nrSequencialContrato.
	 *
	 * @param nrSequencialContrato the nr sequencial contrato
	 */
	public void setNrSequencialContrato(Long nrSequencialContrato) {
		this.nrSequencialContrato = nrSequencialContrato;
	}

	


}
