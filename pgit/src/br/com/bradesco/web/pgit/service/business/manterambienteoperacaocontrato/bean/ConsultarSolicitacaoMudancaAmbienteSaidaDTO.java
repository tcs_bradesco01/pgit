/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

/**
 * Nome: ConsultarSolicitacaoMudancaAmbienteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolicitacaoMudancaAmbienteSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdSolicitacao. */
	private int cdSolicitacao;
	
	/** Atributo nrSolicitacao. */
	private int nrSolicitacao;
	
	/** Atributo cdTipoServico. */
	private int cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo cdAmbienteAtivoSolicitacao. */
	private String cdAmbienteAtivoSolicitacao;
	
	/** Atributo dtProgramadaMudancaAmbiente. */
	private String dtProgramadaMudancaAmbiente;
	
	/** Atributo cdSituacaoSolicitacaoPagamento. */
	private int cdSituacaoSolicitacaoPagamento;
	
	/** Atributo dsSituacaoSolicitacao. */
	private String dsSituacaoSolicitacao;
	
	/** Atributo dtSolicitacao. */
	private String dtSolicitacao;
	
	/** Atributo dsModalidade. */
	private String dsModalidade;
	
	/** Atributo cdModalidade. */
	private int cdModalidade;
	
	
	/**
	 * Get: cdAmbienteAtivoSolicitacao.
	 *
	 * @return cdAmbienteAtivoSolicitacao
	 */
	public String getCdAmbienteAtivoSolicitacao() {
		return cdAmbienteAtivoSolicitacao;
	}
	
	/**
	 * Set: cdAmbienteAtivoSolicitacao.
	 *
	 * @param cdAmbienteAtivoSolicitacao the cd ambiente ativo solicitacao
	 */
	public void setCdAmbienteAtivoSolicitacao(String cdAmbienteAtivoSolicitacao) {
		this.cdAmbienteAtivoSolicitacao = cdAmbienteAtivoSolicitacao;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdProdutoServicoOperacao the cd tipo servico
	 */
	public void setCdTipoServico(int cdProdutoServicoOperacao) {
		this.cdTipoServico = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdSituacaoSolicitacaoPagamento.
	 *
	 * @return cdSituacaoSolicitacaoPagamento
	 */
	public int getCdSituacaoSolicitacaoPagamento() {
		return cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoSolicitacaoPagamento.
	 *
	 * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
	 */
	public void setCdSituacaoSolicitacaoPagamento(int cdSituacaoSolicitacaoPagamento) {
		this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
	}
	
	/**
	 * Get: cdSolicitacao.
	 *
	 * @return cdSolicitacao
	 */
	public int getCdSolicitacao() {
		return cdSolicitacao;
	}
	
	/**
	 * Set: cdSolicitacao.
	 *
	 * @param cdSolicitacao the cd solicitacao
	 */
	public void setCdSolicitacao(int cdSolicitacao) {
		this.cdSolicitacao = cdSolicitacao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsProdutoServicoOperacao the ds tipo servico
	 */
	public void setDsTipoServico(String dsProdutoServicoOperacao) {
		this.dsTipoServico = dsProdutoServicoOperacao;
	}
	
	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}
	
	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}
	
	/**
	 * Get: dtProgramadaMudancaAmbiente.
	 *
	 * @return dtProgramadaMudancaAmbiente
	 */
	public String getDtProgramadaMudancaAmbiente() {
		return dtProgramadaMudancaAmbiente;
	}
	
	/**
	 * Set: dtProgramadaMudancaAmbiente.
	 *
	 * @param dtProgramadaMudancaAmbiente the dt programada mudanca ambiente
	 */
	public void setDtProgramadaMudancaAmbiente(String dtProgramadaMudancaAmbiente) {
		this.dtProgramadaMudancaAmbiente = dtProgramadaMudancaAmbiente;
	}
	
	/**
	 * Get: dtSolicitacao.
	 *
	 * @return dtSolicitacao
	 */
	public String getDtSolicitacao() {
		return dtSolicitacao;
	}
	
	/**
	 * Set: dtSolicitacao.
	 *
	 * @param dtSolicitacao the dt solicitacao
	 */
	public void setDtSolicitacao(String dtSolicitacao) {
		this.dtSolicitacao = dtSolicitacao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public int getNrSolicitacao() {
		return nrSolicitacao;
	}
	
	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(int nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}
	
	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}
	
	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

}
