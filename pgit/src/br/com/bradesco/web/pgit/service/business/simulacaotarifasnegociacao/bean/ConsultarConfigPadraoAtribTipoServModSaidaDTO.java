/*
 * Nome: br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.service.data.pdc.consultarconfigpadraoatribtiposervmod.response.ConsultarConfigPadraoAtribTipoServModResponse;

/**
 * Nome: DetalharSimulacaoTarifaNegociacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarConfigPadraoAtribTipoServModSaidaDTO {
	
    private String codMensagem;
    private String mensagem;
    private Integer cdAcaoNaoVida;
    private Integer cdAcertoDadoRecadastro;
    private Integer cdAgendaDebitoVeiculo;
    private Integer cdAgendaPagamentoVencido;
    private Integer cdAGendaValorMenor;
    private Integer cdAgrupamentoAviso;
    private Integer cdAgrupamentoComprovado;
    private Integer cdAgptoFormularioRecadastro;
    private Integer cdAntecRecadastroBeneficiario;
    private Integer cdAreaReservada;
    private Integer cdBasaeRecadastroBeneficiario;
    private Integer cdBloqueioEmissaoPplta;
    private Integer cdCaptuTituloRegistro;
    private Integer cdCobrancaTarifa;
    private Integer cdConsDebitoVeiculo;
    private Integer cdConsEndereco;
    private Integer cdConsSaldoPagamento;
    private Integer cdContgConsSaldo;
    private Integer cdCreditoNaoUtilizado;
    private Integer cdCriterioEnquaBeneficiario;
    private Integer cdCriterioEnquaRcadt;
    private Integer cdCriterioRstrbTitulo;
    private Integer cdCtciaEspecieBeneficiario;
    private Integer cdCtciaIdBeneficiario;
    private Integer cdCtciaInscricaoFavorecido;
    private Integer cdCtciaProprietarioVeiculo;
    private Integer cdDispzContaCredito;
    private Integer cdDispzDiversosCrrtt;
    private Integer cdDispzDiversaoNao;
    private Integer cdDispzSalarialCrrtt;
    private Integer cdDispzSalarialNao;
    private Integer cdDestinoAviso;
    private Integer cdDestinoComprovante;
    private Integer cdDestinoFormularioRcadt;
    private Integer cdEnvelopeAberto;
    private Integer cdFavorecidoConsPgto;
    private Integer cdFormaAutrzPgto;
    private Integer cdFormaEnvioPagamento;
    private Integer cdFormaExpirCredito;
    private Integer cdFormaManutencao;
    private Integer cdFrasePreCadastro;
    private Integer cdIndicadorAgendaTitulo;
    private Integer cdIndicadorAutrzCliente;
    private Integer cdIndicadorAutrzCompl;
    private Integer cdIndicadorCadOrg;
    private Integer cdIndicadorCadProcd;
    private Integer cdIndicadorCartaoSalarial;
    private Integer cdIndicadorEconomicoReajuste;
    private Integer cdIndicadorExpirCredito;
    private Integer cdIndicadorLctoPgmd;
    private Integer cdIndicadorMensagemPerso;
    private Integer cdLancamentoFuturoCredito;
    private Integer cdLancamentoFuturoDebito;
    private Integer cdLiberacaoLoteProcs;
    private Integer cdManutencaoBaseRcadt;
    private Integer cdMidiaDisponivel;
    private Integer cdMidiaMensagemRcadt;
    private Integer cdMomenAvisoRcadt;
    private Integer cdMomenCreditoEfetivo;
    private Integer cdMomenDebitoPagamento;
    private Integer cdMomenFormulaRcadt;
    private Integer cdMomenProcmPagamento;
    private Integer cdMensagemRcadtMidia;
    private Integer cdPermissaoDebitoOnline;
    private Integer cdNaturezaOperacaoPagamento;
    private Integer cdPerdcAviso;
    private Integer cdPerdcCobrancaTarifa;
    private Integer cdPerdcComprovante;
    private Integer cdPerdcConsVeiculo;
    private Integer cdPerdcEnvioRemessa;
    private Integer cdPerdcManutencaoProcd;
    private Integer cdPagamentoNaoUtil;
    private Integer cdPrincipalEnquaRcadt;
    private Integer cdPriorEfetivacaoPagamento;
    private Integer cdRejeiAgendaLote;
    private Integer cdRejeiEfetivacaoLote;
    private Integer cdRejeiLote;
    private Integer cdRastreabNotaFiscal;
    private Integer cdRastreabTituloTerc;
    private Integer cdTipoCargaRcadt;
    private Integer cdTipoCataoSalarial;
    private Integer cdTipoDataFloat;
    private Integer cdTipoDivergenciaVeiculo;
    private Integer cdTipoIdBenef;
    private Integer cdTipoLayoutArquivo;
    private Integer cdTipoReajusteTarifa;
    private Integer cdTratoContaTransf;
    private Integer cdUtilzFavorecidoCtrl;
    private Integer nrFechamentoApurcTarifa;
    private BigDecimal cdPercentualIndiceReajusteTarifa;
    private Integer cdPercentualMaximoInconLote;
    private BigDecimal cdPercentualReducaoTarifaCatlg;
    private Integer qtAntecedencia;
    private Integer qtAnteriorVencimentoCompv;
    private Integer qtDiaCobrancaTarifa;
    private Integer qtDiaExpiracao;
    private Integer qtDiaFloatPagamento;
    private Integer qtDiaInativFavorecido;
    private Integer qtDiaRepiqCons;
    private Integer qtEtapaRcadtBeneficiario;
    private Integer qtFaseRcadtBenefiario;
    private Integer qtLimiteLinhas;
    private Integer qtLimiteSolicitacaoCatao;
    private Integer qtMaximaInconLote;
    private Integer qtMaximaTituloVencido;
    private Integer qtMesComprovante;
    private Integer qtMesRcadt;
    private Integer qtMesFaseRcadt;
    private Integer qtMesReajusteTarifa;
    private Integer qtViaAviso;
    private Integer qtViaCobranca;
    private Integer qtViaComprovante;
    private BigDecimal vlFavorecidoNaoCadtr;
    private BigDecimal vlLimiteDiaPagamento;
    private BigDecimal vlLimiteIndvdPagamento;
    private Integer cdIndicadorEmissaoAviso;
    private Integer cdIndicadorRetornoInternet;
    private Integer cdIndicadorListaDebito;
    private Integer cdTipoFormacaoLista;
    private Integer cdTipoConsistenciaLista;
    private Integer cdTipoConsultaComprovante;
    private Integer cdValidacaoNomeFavorecido;
    private Integer cdTipoContaFavorecido;
    private Integer cdIndLancamentoPersonalizado;
    private Integer cdAgendaRastreabilidadeFilial;
    private Integer cdIndicadorAdesaoSacador;
    private Integer cdMeioPagamentoCredito;
    private Integer cdTipoIsncricaoFavorecido;
    private Integer cdIndicadorBancoPostal;
    private Integer cdFormularioContratoCliente;
    private Integer cdConsultaSaldoValorSuperior;
    
    
    
    public ConsultarConfigPadraoAtribTipoServModSaidaDTO() {
    	super();
    }
    
	public ConsultarConfigPadraoAtribTipoServModSaidaDTO(ConsultarConfigPadraoAtribTipoServModResponse response) {
		super();
		this.codMensagem = response.getCodMensagem();
		this.mensagem = response.getMensagem();
		this.cdAcaoNaoVida = response.getCdAcaoNaoVida();
		this.cdAcertoDadoRecadastro = response.getCdAcertoDadoRecadastro();
		this.cdAgendaDebitoVeiculo = response.getCdAgendaDebitoVeiculo();
		this.cdAgendaPagamentoVencido = response.getCdAgendaPagamentoVencido();
		this.cdAGendaValorMenor = response.getCdAGendaValorMenor();
		this.cdAgrupamentoAviso = response.getCdAgrupamentoAviso();
		this.cdAgrupamentoComprovado = response.getCdAgrupamentoComprovado();
		this.cdAgptoFormularioRecadastro = response.getCdAgptoFormularioRecadastro();
		this.cdAntecRecadastroBeneficiario = response.getCdAntecRecadastroBeneficiario();
		this.cdAreaReservada = response.getCdAreaReservada();
		this.cdBasaeRecadastroBeneficiario = response.getCdBasaeRecadastroBeneficiario();
		this.cdBloqueioEmissaoPplta = response.getCdBloqueioEmissaoPplta();
		this.cdCaptuTituloRegistro = response.getCdCaptuTituloRegistro();
		this.cdCobrancaTarifa = response.getCdCobrancaTarifa();
		this.cdConsDebitoVeiculo = response.getCdConsDebitoVeiculo();
		this.cdConsEndereco = response.getCdConsEndereco();
		this.cdConsSaldoPagamento = response.getCdConsSaldoPagamento();
		this.cdContgConsSaldo = response.getCdContgConsSaldo();
		this.cdCreditoNaoUtilizado = response.getCdCreditoNaoUtilizado();
		this.cdCriterioEnquaBeneficiario = response.getCdCriterioEnquaBeneficiario();
		this.cdCriterioEnquaRcadt = response.getCdCriterioEnquaRcadt();
		this.cdCriterioRstrbTitulo = response.getCdCriterioRstrbTitulo();
		this.cdCtciaEspecieBeneficiario = response.getCdCtciaEspecieBeneficiario();
		this.cdCtciaIdBeneficiario = response.getCdCtciaIdBeneficiario();
		this.cdCtciaInscricaoFavorecido = response.getCdCtciaInscricaoFavorecido();
		this.cdCtciaProprietarioVeiculo = response.getCdCtciaProprietarioVeiculo();
		this.cdDispzContaCredito = response.getCdDispzContaCredito();
		this.cdDispzDiversosCrrtt = response.getCdDispzDiversosCrrtt();
		this.cdDispzDiversaoNao = response.getCdDispzDiversaoNao();
		this.cdDispzSalarialCrrtt = response.getCdDispzSalarialCrrtt();
		this.cdDispzSalarialNao = response.getCdDispzSalarialNao();
		this.cdDestinoAviso = response.getCdDestinoAviso();
		this.cdDestinoComprovante = response.getCdDestinoComprovante();
		this.cdDestinoFormularioRcadt = response.getCdDestinoFormularioRcadt();
		this.cdEnvelopeAberto = response.getCdEnvelopeAberto();
		this.cdFavorecidoConsPgto = response.getCdFavorecidoConsPgto();
		this.cdFormaAutrzPgto = response.getCdFormaAutrzPgto();
		this.cdFormaEnvioPagamento = response.getCdFormaEnvioPagamento();
		this.cdFormaExpirCredito = response.getCdFormaExpirCredito();
		this.cdFormaManutencao = response.getCdFormaManutencao();
		this.cdFrasePreCadastro = response.getCdFrasePreCadastro();
		this.cdIndicadorAgendaTitulo = response.getCdIndicadorAgendaTitulo();
		this.cdIndicadorAutrzCliente = response.getCdIndicadorAutrzCliente();
		this.cdIndicadorAutrzCompl = response.getCdIndicadorAutrzCompl();
		this.cdIndicadorCadOrg = response.getCdIndicadorCadOrg();
		this.cdIndicadorCadProcd = response.getCdIndicadorCadProcd();
		this.cdIndicadorCartaoSalarial = response.getCdIndicadorCartaoSalarial();
		this.cdIndicadorEconomicoReajuste = response.getCdIndicadorEconomicoReajuste();
		this.cdIndicadorExpirCredito = response.getCdIndicadorExpirCredito();
		this.cdIndicadorLctoPgmd = response.getCdIndicadorLctoPgmd();
		this.cdIndicadorMensagemPerso = response.getCdIndicadorMensagemPerso();
		this.cdLancamentoFuturoCredito = response.getCdLancamentoFuturoCredito();
		this.cdLancamentoFuturoDebito = response.getCdLancamentoFuturoDebito();
		this.cdLiberacaoLoteProcs = response.getCdLiberacaoLoteProcs();
		this.cdManutencaoBaseRcadt = response.getCdManutencaoBaseRcadt();
		this.cdMidiaDisponivel = response.getCdMidiaDisponivel();
		this.cdMidiaMensagemRcadt = response.getCdMidiaMensagemRcadt();
		this.cdMomenAvisoRcadt = response.getCdMomenAvisoRcadt();
		this.cdMomenCreditoEfetivo = response.getCdMomenCreditoEfetivo();
		this.cdMomenDebitoPagamento = response.getCdMomenDebitoPagamento();
		this.cdMomenFormulaRcadt = response.getCdMomenFormulaRcadt();
		this.cdMomenProcmPagamento = response.getCdMomenProcmPagamento();
		this.cdMensagemRcadtMidia = response.getCdMensagemRcadtMidia();
		this.cdPermissaoDebitoOnline = response.getCdPermissaoDebitoOnline();
		this.cdNaturezaOperacaoPagamento = response.getCdNaturezaOperacaoPagamento();
		this.cdPerdcAviso = response.getCdPerdcAviso();
		this.cdPerdcCobrancaTarifa = response.getCdPerdcCobrancaTarifa();
		this.cdPerdcComprovante = response.getCdPerdcComprovante();
		this.cdPerdcConsVeiculo = response.getCdPerdcConsVeiculo();
		this.cdPerdcEnvioRemessa = response.getCdPerdcEnvioRemessa();
		this.cdPerdcManutencaoProcd = response.getCdPerdcManutencaoProcd();
		this.cdPagamentoNaoUtil = response.getCdPagamentoNaoUtil();
		this.cdPrincipalEnquaRcadt = response.getCdPrincipalEnquaRcadt();
		this.cdPriorEfetivacaoPagamento = response.getCdPriorEfetivacaoPagamento();
		this.cdRejeiAgendaLote = response.getCdRejeiAgendaLote();
		this.cdRejeiEfetivacaoLote = response.getCdRejeiEfetivacaoLote();
		this.cdRejeiLote = response.getCdRejeiLote();
		this.cdRastreabNotaFiscal = response.getCdRastreabNotaFiscal();
		this.cdRastreabTituloTerc = response.getCdRastreabTituloTerc();
		this.cdTipoCargaRcadt = response.getCdTipoCargaRcadt();
		this.cdTipoCataoSalarial = response.getCdTipoCataoSalarial();
		this.cdTipoDataFloat = response.getCdTipoDataFloat();
		this.cdTipoDivergenciaVeiculo = response.getCdTipoDivergenciaVeiculo();
		this.cdTipoIdBenef = response.getCdTipoIdBenef();
		this.cdTipoLayoutArquivo = response.getCdTipoLayoutArquivo();
		this.cdTipoReajusteTarifa = response.getCdTipoReajusteTarifa();
		this.cdTratoContaTransf = response.getCdTratoContaTransf();
		this.cdUtilzFavorecidoCtrl = response.getCdUtilzFavorecidoCtrl();
		this.nrFechamentoApurcTarifa = response.getNrFechamentoApurcTarifa();
		this.cdPercentualIndiceReajusteTarifa = response.getCdPercentualIndiceReajusteTarifa();
		this.cdPercentualMaximoInconLote = response.getCdPercentualMaximoInconLote();
		this.cdPercentualReducaoTarifaCatlg = response.getCdPercentualReducaoTarifaCatlg();
		this.qtAntecedencia = response.getQtAntecedencia();
		this.qtAnteriorVencimentoCompv = response.getQtAnteriorVencimentoCompv();
		this.qtDiaCobrancaTarifa = response.getQtDiaCobrancaTarifa();
		this.qtDiaExpiracao = response.getQtDiaExpiracao();
		this.qtDiaFloatPagamento = response.getQtDiaFloatPagamento();
		this.qtDiaInativFavorecido = response.getQtDiaInativFavorecido();
		this.qtDiaRepiqCons = response.getQtDiaRepiqCons();
		this.qtEtapaRcadtBeneficiario = response.getQtEtapaRcadtBeneficiario();
		this.qtFaseRcadtBenefiario = response.getQtFaseRcadtBenefiario();
		this.qtLimiteLinhas = response.getQtLimiteLinhas();
		this.qtLimiteSolicitacaoCatao = response.getQtLimiteSolicitacaoCatao();
		this.qtMaximaInconLote = response.getQtMaximaInconLote();
		this.qtMaximaTituloVencido = response.getQtMaximaTituloVencido();
		this.qtMesComprovante = response.getQtMesComprovante();
		this.qtMesRcadt = response.getQtMesRcadt();
		this.qtMesFaseRcadt = response.getQtMesFaseRcadt();
		this.qtMesReajusteTarifa = response.getQtMesReajusteTarifa();
		this.qtViaAviso = response.getQtViaAviso();
		this.qtViaCobranca = response.getQtViaCobranca();
		this.qtViaComprovante = response.getQtViaComprovante();
		this.vlFavorecidoNaoCadtr = response.getVlFavorecidoNaoCadtr();
		this.vlLimiteDiaPagamento = response.getVlLimiteDiaPagamento();
		this.vlLimiteIndvdPagamento = response.getVlLimiteIndvdPagamento();
		this.cdIndicadorEmissaoAviso = response.getCdIndicadorEmissaoAviso();
		this.cdIndicadorRetornoInternet = response.getCdIndicadorRetornoInternet();
		this.cdIndicadorListaDebito = response.getCdIndicadorListaDebito();
		this.cdTipoFormacaoLista = response.getCdTipoFormacaoLista();
		this.cdTipoConsistenciaLista = response.getCdTipoConsistenciaLista();
		this.cdTipoConsultaComprovante = response.getCdTipoConsultaComprovante();
		this.cdValidacaoNomeFavorecido = response.getCdValidacaoNomeFavorecido();
		this.cdTipoContaFavorecido = response.getCdTipoContaFavorecido();
		this.cdIndLancamentoPersonalizado = response.getCdIndLancamentoPersonalizado();
		this.cdAgendaRastreabilidadeFilial = response.getCdAgendaRastreabilidadeFilial();
		this.cdIndicadorAdesaoSacador = response.getCdIndicadorAdesaoSacador();
		this.cdMeioPagamentoCredito = response.getCdMeioPagamentoCredito();
		this.cdTipoIsncricaoFavorecido = response.getCdTipoIsncricaoFavorecido();
		this.cdIndicadorBancoPostal = response.getCdIndicadorBancoPostal();
		this.cdFormularioContratoCliente = response.getCdFormularioContratoCliente();
		this.cdConsultaSaldoValorSuperior = response.getCdConsultaSaldoValorSuperior();
	}
	
	
	/**
	 * Nome: getCodMensagem
	 *
	 * @exception
	 * @throws
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	/**
	 * Nome: setCodMensagem
	 *
	 * @exception
	 * @throws
	 * @param codMensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	/**
	 * Nome: getMensagem
	 *
	 * @exception
	 * @throws
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	/**
	 * Nome: setMensagem
	 *
	 * @exception
	 * @throws
	 * @param mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	/**
	 * Nome: getCdAcaoNaoVida
	 *
	 * @exception
	 * @throws
	 * @return cdAcaoNaoVida
	 */
	public Integer getCdAcaoNaoVida() {
		return cdAcaoNaoVida;
	}
	/**
	 * Nome: setCdAcaoNaoVida
	 *
	 * @exception
	 * @throws
	 * @param cdAcaoNaoVida
	 */
	public void setCdAcaoNaoVida(Integer cdAcaoNaoVida) {
		this.cdAcaoNaoVida = cdAcaoNaoVida;
	}
	/**
	 * Nome: getCdAcertoDadoRecadastro
	 *
	 * @exception
	 * @throws
	 * @return cdAcertoDadoRecadastro
	 */
	public Integer getCdAcertoDadoRecadastro() {
		return cdAcertoDadoRecadastro;
	}
	/**
	 * Nome: setCdAcertoDadoRecadastro
	 *
	 * @exception
	 * @throws
	 * @param cdAcertoDadoRecadastro
	 */
	public void setCdAcertoDadoRecadastro(Integer cdAcertoDadoRecadastro) {
		this.cdAcertoDadoRecadastro = cdAcertoDadoRecadastro;
	}
	/**
	 * Nome: getCdAgendaDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @return cdAgendaDebitoVeiculo
	 */
	public Integer getCdAgendaDebitoVeiculo() {
		return cdAgendaDebitoVeiculo;
	}
	/**
	 * Nome: setCdAgendaDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @param cdAgendaDebitoVeiculo
	 */
	public void setCdAgendaDebitoVeiculo(Integer cdAgendaDebitoVeiculo) {
		this.cdAgendaDebitoVeiculo = cdAgendaDebitoVeiculo;
	}
	/**
	 * Nome: getCdAgendaPagamentoVencido
	 *
	 * @exception
	 * @throws
	 * @return cdAgendaPagamentoVencido
	 */
	public Integer getCdAgendaPagamentoVencido() {
		return cdAgendaPagamentoVencido;
	}
	/**
	 * Nome: setCdAgendaPagamentoVencido
	 *
	 * @exception
	 * @throws
	 * @param cdAgendaPagamentoVencido
	 */
	public void setCdAgendaPagamentoVencido(Integer cdAgendaPagamentoVencido) {
		this.cdAgendaPagamentoVencido = cdAgendaPagamentoVencido;
	}
	/**
	 * Nome: getCdAGendaValorMenor
	 *
	 * @exception
	 * @throws
	 * @return cdAGendaValorMenor
	 */
	public Integer getCdAGendaValorMenor() {
		return cdAGendaValorMenor;
	}
	/**
	 * Nome: setCdAGendaValorMenor
	 *
	 * @exception
	 * @throws
	 * @param cdAGendaValorMenor
	 */
	public void setCdAGendaValorMenor(Integer cdAGendaValorMenor) {
		this.cdAGendaValorMenor = cdAGendaValorMenor;
	}
	/**
	 * Nome: getCdAgrupamentoAviso
	 *
	 * @exception
	 * @throws
	 * @return cdAgrupamentoAviso
	 */
	public Integer getCdAgrupamentoAviso() {
		return cdAgrupamentoAviso;
	}
	/**
	 * Nome: setCdAgrupamentoAviso
	 *
	 * @exception
	 * @throws
	 * @param cdAgrupamentoAviso
	 */
	public void setCdAgrupamentoAviso(Integer cdAgrupamentoAviso) {
		this.cdAgrupamentoAviso = cdAgrupamentoAviso;
	}
	/**
	 * Nome: getCdAgrupamentoComprovado
	 *
	 * @exception
	 * @throws
	 * @return cdAgrupamentoComprovado
	 */
	public Integer getCdAgrupamentoComprovado() {
		return cdAgrupamentoComprovado;
	}
	/**
	 * Nome: setCdAgrupamentoComprovado
	 *
	 * @exception
	 * @throws
	 * @param cdAgrupamentoComprovado
	 */
	public void setCdAgrupamentoComprovado(Integer cdAgrupamentoComprovado) {
		this.cdAgrupamentoComprovado = cdAgrupamentoComprovado;
	}
	/**
	 * Nome: getCdAgptoFormularioRecadastro
	 *
	 * @exception
	 * @throws
	 * @return cdAgptoFormularioRecadastro
	 */
	public Integer getCdAgptoFormularioRecadastro() {
		return cdAgptoFormularioRecadastro;
	}
	/**
	 * Nome: setCdAgptoFormularioRecadastro
	 *
	 * @exception
	 * @throws
	 * @param cdAgptoFormularioRecadastro
	 */
	public void setCdAgptoFormularioRecadastro(Integer cdAgptoFormularioRecadastro) {
		this.cdAgptoFormularioRecadastro = cdAgptoFormularioRecadastro;
	}
	/**
	 * Nome: getCdAntecRecadastroBeneficiario
	 *
	 * @exception
	 * @throws
	 * @return cdAntecRecadastroBeneficiario
	 */
	public Integer getCdAntecRecadastroBeneficiario() {
		return cdAntecRecadastroBeneficiario;
	}
	/**
	 * Nome: setCdAntecRecadastroBeneficiario
	 *
	 * @exception
	 * @throws
	 * @param cdAntecRecadastroBeneficiario
	 */
	public void setCdAntecRecadastroBeneficiario(
			Integer cdAntecRecadastroBeneficiario) {
		this.cdAntecRecadastroBeneficiario = cdAntecRecadastroBeneficiario;
	}
	/**
	 * Nome: getCdAreaReservada
	 *
	 * @exception
	 * @throws
	 * @return cdAreaReservada
	 */
	public Integer getCdAreaReservada() {
		return cdAreaReservada;
	}
	/**
	 * Nome: setCdAreaReservada
	 *
	 * @exception
	 * @throws
	 * @param cdAreaReservada
	 */
	public void setCdAreaReservada(Integer cdAreaReservada) {
		this.cdAreaReservada = cdAreaReservada;
	}
	/**
	 * Nome: getCdBasaeRecadastroBeneficiario
	 *
	 * @exception
	 * @throws
	 * @return cdBasaeRecadastroBeneficiario
	 */
	public Integer getCdBasaeRecadastroBeneficiario() {
		return cdBasaeRecadastroBeneficiario;
	}
	/**
	 * Nome: setCdBasaeRecadastroBeneficiario
	 *
	 * @exception
	 * @throws
	 * @param cdBasaeRecadastroBeneficiario
	 */
	public void setCdBasaeRecadastroBeneficiario(
			Integer cdBasaeRecadastroBeneficiario) {
		this.cdBasaeRecadastroBeneficiario = cdBasaeRecadastroBeneficiario;
	}
	/**
	 * Nome: getCdBloqueioEmissaoPplta
	 *
	 * @exception
	 * @throws
	 * @return cdBloqueioEmissaoPplta
	 */
	public Integer getCdBloqueioEmissaoPplta() {
		return cdBloqueioEmissaoPplta;
	}
	/**
	 * Nome: setCdBloqueioEmissaoPplta
	 *
	 * @exception
	 * @throws
	 * @param cdBloqueioEmissaoPplta
	 */
	public void setCdBloqueioEmissaoPplta(Integer cdBloqueioEmissaoPplta) {
		this.cdBloqueioEmissaoPplta = cdBloqueioEmissaoPplta;
	}
	/**
	 * Nome: getCdCaptuTituloRegistro
	 *
	 * @exception
	 * @throws
	 * @return cdCaptuTituloRegistro
	 */
	public Integer getCdCaptuTituloRegistro() {
		return cdCaptuTituloRegistro;
	}
	/**
	 * Nome: setCdCaptuTituloRegistro
	 *
	 * @exception
	 * @throws
	 * @param cdCaptuTituloRegistro
	 */
	public void setCdCaptuTituloRegistro(Integer cdCaptuTituloRegistro) {
		this.cdCaptuTituloRegistro = cdCaptuTituloRegistro;
	}
	/**
	 * Nome: getCdCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @return cdCobrancaTarifa
	 */
	public Integer getCdCobrancaTarifa() {
		return cdCobrancaTarifa;
	}
	/**
	 * Nome: setCdCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @param cdCobrancaTarifa
	 */
	public void setCdCobrancaTarifa(Integer cdCobrancaTarifa) {
		this.cdCobrancaTarifa = cdCobrancaTarifa;
	}
	/**
	 * Nome: getCdConsDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @return cdConsDebitoVeiculo
	 */
	public Integer getCdConsDebitoVeiculo() {
		return cdConsDebitoVeiculo;
	}
	/**
	 * Nome: setCdConsDebitoVeiculo
	 *
	 * @exception
	 * @throws
	 * @param cdConsDebitoVeiculo
	 */
	public void setCdConsDebitoVeiculo(Integer cdConsDebitoVeiculo) {
		this.cdConsDebitoVeiculo = cdConsDebitoVeiculo;
	}
	/**
	 * Nome: getCdConsEndereco
	 *
	 * @exception
	 * @throws
	 * @return cdConsEndereco
	 */
	public Integer getCdConsEndereco() {
		return cdConsEndereco;
	}
	/**
	 * Nome: setCdConsEndereco
	 *
	 * @exception
	 * @throws
	 * @param cdConsEndereco
	 */
	public void setCdConsEndereco(Integer cdConsEndereco) {
		this.cdConsEndereco = cdConsEndereco;
	}
	/**
	 * Nome: getCdConsSaldoPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdConsSaldoPagamento
	 */
	public Integer getCdConsSaldoPagamento() {
		return cdConsSaldoPagamento;
	}
	/**
	 * Nome: setCdConsSaldoPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdConsSaldoPagamento
	 */
	public void setCdConsSaldoPagamento(Integer cdConsSaldoPagamento) {
		this.cdConsSaldoPagamento = cdConsSaldoPagamento;
	}
	/**
	 * Nome: getCdContgConsSaldo
	 *
	 * @exception
	 * @throws
	 * @return cdContgConsSaldo
	 */
	public Integer getCdContgConsSaldo() {
		return cdContgConsSaldo;
	}
	/**
	 * Nome: setCdContgConsSaldo
	 *
	 * @exception
	 * @throws
	 * @param cdContgConsSaldo
	 */
	public void setCdContgConsSaldo(Integer cdContgConsSaldo) {
		this.cdContgConsSaldo = cdContgConsSaldo;
	}
	/**
	 * Nome: getCdCreditoNaoUtilizado
	 *
	 * @exception
	 * @throws
	 * @return cdCreditoNaoUtilizado
	 */
	public Integer getCdCreditoNaoUtilizado() {
		return cdCreditoNaoUtilizado;
	}
	/**
	 * Nome: setCdCreditoNaoUtilizado
	 *
	 * @exception
	 * @throws
	 * @param cdCreditoNaoUtilizado
	 */
	public void setCdCreditoNaoUtilizado(Integer cdCreditoNaoUtilizado) {
		this.cdCreditoNaoUtilizado = cdCreditoNaoUtilizado;
	}
	/**
	 * Nome: getCdCriterioEnquaBeneficiario
	 *
	 * @exception
	 * @throws
	 * @return cdCriterioEnquaBeneficiario
	 */
	public Integer getCdCriterioEnquaBeneficiario() {
		return cdCriterioEnquaBeneficiario;
	}
	/**
	 * Nome: setCdCriterioEnquaBeneficiario
	 *
	 * @exception
	 * @throws
	 * @param cdCriterioEnquaBeneficiario
	 */
	public void setCdCriterioEnquaBeneficiario(Integer cdCriterioEnquaBeneficiario) {
		this.cdCriterioEnquaBeneficiario = cdCriterioEnquaBeneficiario;
	}
	/**
	 * Nome: getCdCriterioEnquaRcadt
	 *
	 * @exception
	 * @throws
	 * @return cdCriterioEnquaRcadt
	 */
	public Integer getCdCriterioEnquaRcadt() {
		return cdCriterioEnquaRcadt;
	}
	/**
	 * Nome: setCdCriterioEnquaRcadt
	 *
	 * @exception
	 * @throws
	 * @param cdCriterioEnquaRcadt
	 */
	public void setCdCriterioEnquaRcadt(Integer cdCriterioEnquaRcadt) {
		this.cdCriterioEnquaRcadt = cdCriterioEnquaRcadt;
	}
	/**
	 * Nome: getCdCriterioRstrbTitulo
	 *
	 * @exception
	 * @throws
	 * @return cdCriterioRstrbTitulo
	 */
	public Integer getCdCriterioRstrbTitulo() {
		return cdCriterioRstrbTitulo;
	}
	/**
	 * Nome: setCdCriterioRstrbTitulo
	 *
	 * @exception
	 * @throws
	 * @param cdCriterioRstrbTitulo
	 */
	public void setCdCriterioRstrbTitulo(Integer cdCriterioRstrbTitulo) {
		this.cdCriterioRstrbTitulo = cdCriterioRstrbTitulo;
	}
	/**
	 * Nome: getCdCtciaEspecieBeneficiario
	 *
	 * @exception
	 * @throws
	 * @return cdCtciaEspecieBeneficiario
	 */
	public Integer getCdCtciaEspecieBeneficiario() {
		return cdCtciaEspecieBeneficiario;
	}
	/**
	 * Nome: setCdCtciaEspecieBeneficiario
	 *
	 * @exception
	 * @throws
	 * @param cdCtciaEspecieBeneficiario
	 */
	public void setCdCtciaEspecieBeneficiario(Integer cdCtciaEspecieBeneficiario) {
		this.cdCtciaEspecieBeneficiario = cdCtciaEspecieBeneficiario;
	}
	/**
	 * Nome: getCdCtciaIdBeneficiario
	 *
	 * @exception
	 * @throws
	 * @return cdCtciaIdBeneficiario
	 */
	public Integer getCdCtciaIdBeneficiario() {
		return cdCtciaIdBeneficiario;
	}
	/**
	 * Nome: setCdCtciaIdBeneficiario
	 *
	 * @exception
	 * @throws
	 * @param cdCtciaIdBeneficiario
	 */
	public void setCdCtciaIdBeneficiario(Integer cdCtciaIdBeneficiario) {
		this.cdCtciaIdBeneficiario = cdCtciaIdBeneficiario;
	}
	/**
	 * Nome: getCdCtciaInscricaoFavorecido
	 *
	 * @exception
	 * @throws
	 * @return cdCtciaInscricaoFavorecido
	 */
	public Integer getCdCtciaInscricaoFavorecido() {
		return cdCtciaInscricaoFavorecido;
	}
	/**
	 * Nome: setCdCtciaInscricaoFavorecido
	 *
	 * @exception
	 * @throws
	 * @param cdCtciaInscricaoFavorecido
	 */
	public void setCdCtciaInscricaoFavorecido(Integer cdCtciaInscricaoFavorecido) {
		this.cdCtciaInscricaoFavorecido = cdCtciaInscricaoFavorecido;
	}
	/**
	 * Nome: getCdCtciaProprietarioVeiculo
	 *
	 * @exception
	 * @throws
	 * @return cdCtciaProprietarioVeiculo
	 */
	public Integer getCdCtciaProprietarioVeiculo() {
		return cdCtciaProprietarioVeiculo;
	}
	/**
	 * Nome: setCdCtciaProprietarioVeiculo
	 *
	 * @exception
	 * @throws
	 * @param cdCtciaProprietarioVeiculo
	 */
	public void setCdCtciaProprietarioVeiculo(Integer cdCtciaProprietarioVeiculo) {
		this.cdCtciaProprietarioVeiculo = cdCtciaProprietarioVeiculo;
	}
	/**
	 * Nome: getCdDispzContaCredito
	 *
	 * @exception
	 * @throws
	 * @return cdDispzContaCredito
	 */
	public Integer getCdDispzContaCredito() {
		return cdDispzContaCredito;
	}
	/**
	 * Nome: setCdDispzContaCredito
	 *
	 * @exception
	 * @throws
	 * @param cdDispzContaCredito
	 */
	public void setCdDispzContaCredito(Integer cdDispzContaCredito) {
		this.cdDispzContaCredito = cdDispzContaCredito;
	}
	/**
	 * Nome: getCdDispzDiversosCrrtt
	 *
	 * @exception
	 * @throws
	 * @return cdDispzDiversosCrrtt
	 */
	public Integer getCdDispzDiversosCrrtt() {
		return cdDispzDiversosCrrtt;
	}
	/**
	 * Nome: setCdDispzDiversosCrrtt
	 *
	 * @exception
	 * @throws
	 * @param cdDispzDiversosCrrtt
	 */
	public void setCdDispzDiversosCrrtt(Integer cdDispzDiversosCrrtt) {
		this.cdDispzDiversosCrrtt = cdDispzDiversosCrrtt;
	}
	/**
	 * Nome: getCdDispzDiversaoNao
	 *
	 * @exception
	 * @throws
	 * @return cdDispzDiversaoNao
	 */
	public Integer getCdDispzDiversaoNao() {
		return cdDispzDiversaoNao;
	}
	/**
	 * Nome: setCdDispzDiversaoNao
	 *
	 * @exception
	 * @throws
	 * @param cdDispzDiversaoNao
	 */
	public void setCdDispzDiversaoNao(Integer cdDispzDiversaoNao) {
		this.cdDispzDiversaoNao = cdDispzDiversaoNao;
	}
	/**
	 * Nome: getCdDispzSalarialCrrtt
	 *
	 * @exception
	 * @throws
	 * @return cdDispzSalarialCrrtt
	 */
	public Integer getCdDispzSalarialCrrtt() {
		return cdDispzSalarialCrrtt;
	}
	/**
	 * Nome: setCdDispzSalarialCrrtt
	 *
	 * @exception
	 * @throws
	 * @param cdDispzSalarialCrrtt
	 */
	public void setCdDispzSalarialCrrtt(Integer cdDispzSalarialCrrtt) {
		this.cdDispzSalarialCrrtt = cdDispzSalarialCrrtt;
	}
	/**
	 * Nome: getCdDispzSalarialNao
	 *
	 * @exception
	 * @throws
	 * @return cdDispzSalarialNao
	 */
	public Integer getCdDispzSalarialNao() {
		return cdDispzSalarialNao;
	}
	/**
	 * Nome: setCdDispzSalarialNao
	 *
	 * @exception
	 * @throws
	 * @param cdDispzSalarialNao
	 */
	public void setCdDispzSalarialNao(Integer cdDispzSalarialNao) {
		this.cdDispzSalarialNao = cdDispzSalarialNao;
	}
	/**
	 * Nome: getCdDestinoAviso
	 *
	 * @exception
	 * @throws
	 * @return cdDestinoAviso
	 */
	public Integer getCdDestinoAviso() {
		return cdDestinoAviso;
	}
	/**
	 * Nome: setCdDestinoAviso
	 *
	 * @exception
	 * @throws
	 * @param cdDestinoAviso
	 */
	public void setCdDestinoAviso(Integer cdDestinoAviso) {
		this.cdDestinoAviso = cdDestinoAviso;
	}
	/**
	 * Nome: getCdDestinoComprovante
	 *
	 * @exception
	 * @throws
	 * @return cdDestinoComprovante
	 */
	public Integer getCdDestinoComprovante() {
		return cdDestinoComprovante;
	}
	/**
	 * Nome: setCdDestinoComprovante
	 *
	 * @exception
	 * @throws
	 * @param cdDestinoComprovante
	 */
	public void setCdDestinoComprovante(Integer cdDestinoComprovante) {
		this.cdDestinoComprovante = cdDestinoComprovante;
	}
	/**
	 * Nome: getCdDestinoFormularioRcadt
	 *
	 * @exception
	 * @throws
	 * @return cdDestinoFormularioRcadt
	 */
	public Integer getCdDestinoFormularioRcadt() {
		return cdDestinoFormularioRcadt;
	}
	/**
	 * Nome: setCdDestinoFormularioRcadt
	 *
	 * @exception
	 * @throws
	 * @param cdDestinoFormularioRcadt
	 */
	public void setCdDestinoFormularioRcadt(Integer cdDestinoFormularioRcadt) {
		this.cdDestinoFormularioRcadt = cdDestinoFormularioRcadt;
	}
	/**
	 * Nome: getCdEnvelopeAberto
	 *
	 * @exception
	 * @throws
	 * @return cdEnvelopeAberto
	 */
	public Integer getCdEnvelopeAberto() {
		return cdEnvelopeAberto;
	}
	/**
	 * Nome: setCdEnvelopeAberto
	 *
	 * @exception
	 * @throws
	 * @param cdEnvelopeAberto
	 */
	public void setCdEnvelopeAberto(Integer cdEnvelopeAberto) {
		this.cdEnvelopeAberto = cdEnvelopeAberto;
	}
	/**
	 * Nome: getCdFavorecidoConsPgto
	 *
	 * @exception
	 * @throws
	 * @return cdFavorecidoConsPgto
	 */
	public Integer getCdFavorecidoConsPgto() {
		return cdFavorecidoConsPgto;
	}
	/**
	 * Nome: setCdFavorecidoConsPgto
	 *
	 * @exception
	 * @throws
	 * @param cdFavorecidoConsPgto
	 */
	public void setCdFavorecidoConsPgto(Integer cdFavorecidoConsPgto) {
		this.cdFavorecidoConsPgto = cdFavorecidoConsPgto;
	}
	/**
	 * Nome: getCdFormaAutrzPgto
	 *
	 * @exception
	 * @throws
	 * @return cdFormaAutrzPgto
	 */
	public Integer getCdFormaAutrzPgto() {
		return cdFormaAutrzPgto;
	}
	/**
	 * Nome: setCdFormaAutrzPgto
	 *
	 * @exception
	 * @throws
	 * @param cdFormaAutrzPgto
	 */
	public void setCdFormaAutrzPgto(Integer cdFormaAutrzPgto) {
		this.cdFormaAutrzPgto = cdFormaAutrzPgto;
	}
	/**
	 * Nome: getCdFormaEnvioPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdFormaEnvioPagamento
	 */
	public Integer getCdFormaEnvioPagamento() {
		return cdFormaEnvioPagamento;
	}
	/**
	 * Nome: setCdFormaEnvioPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdFormaEnvioPagamento
	 */
	public void setCdFormaEnvioPagamento(Integer cdFormaEnvioPagamento) {
		this.cdFormaEnvioPagamento = cdFormaEnvioPagamento;
	}
	/**
	 * Nome: getCdFormaExpirCredito
	 *
	 * @exception
	 * @throws
	 * @return cdFormaExpirCredito
	 */
	public Integer getCdFormaExpirCredito() {
		return cdFormaExpirCredito;
	}
	/**
	 * Nome: setCdFormaExpirCredito
	 *
	 * @exception
	 * @throws
	 * @param cdFormaExpirCredito
	 */
	public void setCdFormaExpirCredito(Integer cdFormaExpirCredito) {
		this.cdFormaExpirCredito = cdFormaExpirCredito;
	}
	/**
	 * Nome: getCdFormaManutencao
	 *
	 * @exception
	 * @throws
	 * @return cdFormaManutencao
	 */
	public Integer getCdFormaManutencao() {
		return cdFormaManutencao;
	}
	/**
	 * Nome: setCdFormaManutencao
	 *
	 * @exception
	 * @throws
	 * @param cdFormaManutencao
	 */
	public void setCdFormaManutencao(Integer cdFormaManutencao) {
		this.cdFormaManutencao = cdFormaManutencao;
	}
	/**
	 * Nome: getCdFrasePreCadastro
	 *
	 * @exception
	 * @throws
	 * @return cdFrasePreCadastro
	 */
	public Integer getCdFrasePreCadastro() {
		return cdFrasePreCadastro;
	}
	/**
	 * Nome: setCdFrasePreCadastro
	 *
	 * @exception
	 * @throws
	 * @param cdFrasePreCadastro
	 */
	public void setCdFrasePreCadastro(Integer cdFrasePreCadastro) {
		this.cdFrasePreCadastro = cdFrasePreCadastro;
	}
	/**
	 * Nome: getCdIndicadorAgendaTitulo
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorAgendaTitulo
	 */
	public Integer getCdIndicadorAgendaTitulo() {
		return cdIndicadorAgendaTitulo;
	}
	/**
	 * Nome: setCdIndicadorAgendaTitulo
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorAgendaTitulo
	 */
	public void setCdIndicadorAgendaTitulo(Integer cdIndicadorAgendaTitulo) {
		this.cdIndicadorAgendaTitulo = cdIndicadorAgendaTitulo;
	}
	/**
	 * Nome: getCdIndicadorAutrzCliente
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorAutrzCliente
	 */
	public Integer getCdIndicadorAutrzCliente() {
		return cdIndicadorAutrzCliente;
	}
	/**
	 * Nome: setCdIndicadorAutrzCliente
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorAutrzCliente
	 */
	public void setCdIndicadorAutrzCliente(Integer cdIndicadorAutrzCliente) {
		this.cdIndicadorAutrzCliente = cdIndicadorAutrzCliente;
	}
	/**
	 * Nome: getCdIndicadorAutrzCompl
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorAutrzCompl
	 */
	public Integer getCdIndicadorAutrzCompl() {
		return cdIndicadorAutrzCompl;
	}
	/**
	 * Nome: setCdIndicadorAutrzCompl
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorAutrzCompl
	 */
	public void setCdIndicadorAutrzCompl(Integer cdIndicadorAutrzCompl) {
		this.cdIndicadorAutrzCompl = cdIndicadorAutrzCompl;
	}
	/**
	 * Nome: getCdIndicadorCadOrg
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorCadOrg
	 */
	public Integer getCdIndicadorCadOrg() {
		return cdIndicadorCadOrg;
	}
	/**
	 * Nome: setCdIndicadorCadOrg
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorCadOrg
	 */
	public void setCdIndicadorCadOrg(Integer cdIndicadorCadOrg) {
		this.cdIndicadorCadOrg = cdIndicadorCadOrg;
	}
	/**
	 * Nome: getCdIndicadorCadProcd
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorCadProcd
	 */
	public Integer getCdIndicadorCadProcd() {
		return cdIndicadorCadProcd;
	}
	/**
	 * Nome: setCdIndicadorCadProcd
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorCadProcd
	 */
	public void setCdIndicadorCadProcd(Integer cdIndicadorCadProcd) {
		this.cdIndicadorCadProcd = cdIndicadorCadProcd;
	}
	/**
	 * Nome: getCdIndicadorCartaoSalarial
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorCartaoSalarial
	 */
	public Integer getCdIndicadorCartaoSalarial() {
		return cdIndicadorCartaoSalarial;
	}
	/**
	 * Nome: setCdIndicadorCartaoSalarial
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorCartaoSalarial
	 */
	public void setCdIndicadorCartaoSalarial(Integer cdIndicadorCartaoSalarial) {
		this.cdIndicadorCartaoSalarial = cdIndicadorCartaoSalarial;
	}
	/**
	 * Nome: getCdIndicadorEconomicoReajuste
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorEconomicoReajuste
	 */
	public Integer getCdIndicadorEconomicoReajuste() {
		return cdIndicadorEconomicoReajuste;
	}
	/**
	 * Nome: setCdIndicadorEconomicoReajuste
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorEconomicoReajuste
	 */
	public void setCdIndicadorEconomicoReajuste(Integer cdIndicadorEconomicoReajuste) {
		this.cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
	}
	/**
	 * Nome: getCdIndicadorExpirCredito
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorExpirCredito
	 */
	public Integer getCdIndicadorExpirCredito() {
		return cdIndicadorExpirCredito;
	}
	/**
	 * Nome: setCdIndicadorExpirCredito
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorExpirCredito
	 */
	public void setCdIndicadorExpirCredito(Integer cdIndicadorExpirCredito) {
		this.cdIndicadorExpirCredito = cdIndicadorExpirCredito;
	}
	/**
	 * Nome: getCdIndicadorLctoPgmd
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorLctoPgmd
	 */
	public Integer getCdIndicadorLctoPgmd() {
		return cdIndicadorLctoPgmd;
	}
	/**
	 * Nome: setCdIndicadorLctoPgmd
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorLctoPgmd
	 */
	public void setCdIndicadorLctoPgmd(Integer cdIndicadorLctoPgmd) {
		this.cdIndicadorLctoPgmd = cdIndicadorLctoPgmd;
	}
	/**
	 * Nome: getCdIndicadorMensagemPerso
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorMensagemPerso
	 */
	public Integer getCdIndicadorMensagemPerso() {
		return cdIndicadorMensagemPerso;
	}
	/**
	 * Nome: setCdIndicadorMensagemPerso
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorMensagemPerso
	 */
	public void setCdIndicadorMensagemPerso(Integer cdIndicadorMensagemPerso) {
		this.cdIndicadorMensagemPerso = cdIndicadorMensagemPerso;
	}
	/**
	 * Nome: getCdLancamentoFuturoCredito
	 *
	 * @exception
	 * @throws
	 * @return cdLancamentoFuturoCredito
	 */
	public Integer getCdLancamentoFuturoCredito() {
		return cdLancamentoFuturoCredito;
	}
	/**
	 * Nome: setCdLancamentoFuturoCredito
	 *
	 * @exception
	 * @throws
	 * @param cdLancamentoFuturoCredito
	 */
	public void setCdLancamentoFuturoCredito(Integer cdLancamentoFuturoCredito) {
		this.cdLancamentoFuturoCredito = cdLancamentoFuturoCredito;
	}
	/**
	 * Nome: getCdLancamentoFuturoDebito
	 *
	 * @exception
	 * @throws
	 * @return cdLancamentoFuturoDebito
	 */
	public Integer getCdLancamentoFuturoDebito() {
		return cdLancamentoFuturoDebito;
	}
	/**
	 * Nome: setCdLancamentoFuturoDebito
	 *
	 * @exception
	 * @throws
	 * @param cdLancamentoFuturoDebito
	 */
	public void setCdLancamentoFuturoDebito(Integer cdLancamentoFuturoDebito) {
		this.cdLancamentoFuturoDebito = cdLancamentoFuturoDebito;
	}
	/**
	 * Nome: getCdLiberacaoLoteProcs
	 *
	 * @exception
	 * @throws
	 * @return cdLiberacaoLoteProcs
	 */
	public Integer getCdLiberacaoLoteProcs() {
		return cdLiberacaoLoteProcs;
	}
	/**
	 * Nome: setCdLiberacaoLoteProcs
	 *
	 * @exception
	 * @throws
	 * @param cdLiberacaoLoteProcs
	 */
	public void setCdLiberacaoLoteProcs(Integer cdLiberacaoLoteProcs) {
		this.cdLiberacaoLoteProcs = cdLiberacaoLoteProcs;
	}
	/**
	 * Nome: getCdManutencaoBaseRcadt
	 *
	 * @exception
	 * @throws
	 * @return cdManutencaoBaseRcadt
	 */
	public Integer getCdManutencaoBaseRcadt() {
		return cdManutencaoBaseRcadt;
	}
	/**
	 * Nome: setCdManutencaoBaseRcadt
	 *
	 * @exception
	 * @throws
	 * @param cdManutencaoBaseRcadt
	 */
	public void setCdManutencaoBaseRcadt(Integer cdManutencaoBaseRcadt) {
		this.cdManutencaoBaseRcadt = cdManutencaoBaseRcadt;
	}
	/**
	 * Nome: getCdMidiaDisponivel
	 *
	 * @exception
	 * @throws
	 * @return cdMidiaDisponivel
	 */
	public Integer getCdMidiaDisponivel() {
		return cdMidiaDisponivel;
	}
	/**
	 * Nome: setCdMidiaDisponivel
	 *
	 * @exception
	 * @throws
	 * @param cdMidiaDisponivel
	 */
	public void setCdMidiaDisponivel(Integer cdMidiaDisponivel) {
		this.cdMidiaDisponivel = cdMidiaDisponivel;
	}
	/**
	 * Nome: getCdMidiaMensagemRcadt
	 *
	 * @exception
	 * @throws
	 * @return cdMidiaMensagemRcadt
	 */
	public Integer getCdMidiaMensagemRcadt() {
		return cdMidiaMensagemRcadt;
	}
	/**
	 * Nome: setCdMidiaMensagemRcadt
	 *
	 * @exception
	 * @throws
	 * @param cdMidiaMensagemRcadt
	 */
	public void setCdMidiaMensagemRcadt(Integer cdMidiaMensagemRcadt) {
		this.cdMidiaMensagemRcadt = cdMidiaMensagemRcadt;
	}
	/**
	 * Nome: getCdMomenAvisoRcadt
	 *
	 * @exception
	 * @throws
	 * @return cdMomenAvisoRcadt
	 */
	public Integer getCdMomenAvisoRcadt() {
		return cdMomenAvisoRcadt;
	}
	/**
	 * Nome: setCdMomenAvisoRcadt
	 *
	 * @exception
	 * @throws
	 * @param cdMomenAvisoRcadt
	 */
	public void setCdMomenAvisoRcadt(Integer cdMomenAvisoRcadt) {
		this.cdMomenAvisoRcadt = cdMomenAvisoRcadt;
	}
	/**
	 * Nome: getCdMomenCreditoEfetivo
	 *
	 * @exception
	 * @throws
	 * @return cdMomenCreditoEfetivo
	 */
	public Integer getCdMomenCreditoEfetivo() {
		return cdMomenCreditoEfetivo;
	}
	/**
	 * Nome: setCdMomenCreditoEfetivo
	 *
	 * @exception
	 * @throws
	 * @param cdMomenCreditoEfetivo
	 */
	public void setCdMomenCreditoEfetivo(Integer cdMomenCreditoEfetivo) {
		this.cdMomenCreditoEfetivo = cdMomenCreditoEfetivo;
	}
	/**
	 * Nome: getCdMomenDebitoPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdMomenDebitoPagamento
	 */
	public Integer getCdMomenDebitoPagamento() {
		return cdMomenDebitoPagamento;
	}
	/**
	 * Nome: setCdMomenDebitoPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdMomenDebitoPagamento
	 */
	public void setCdMomenDebitoPagamento(Integer cdMomenDebitoPagamento) {
		this.cdMomenDebitoPagamento = cdMomenDebitoPagamento;
	}
	/**
	 * Nome: getCdMomenFormulaRcadt
	 *
	 * @exception
	 * @throws
	 * @return cdMomenFormulaRcadt
	 */
	public Integer getCdMomenFormulaRcadt() {
		return cdMomenFormulaRcadt;
	}
	/**
	 * Nome: setCdMomenFormulaRcadt
	 *
	 * @exception
	 * @throws
	 * @param cdMomenFormulaRcadt
	 */
	public void setCdMomenFormulaRcadt(Integer cdMomenFormulaRcadt) {
		this.cdMomenFormulaRcadt = cdMomenFormulaRcadt;
	}
	/**
	 * Nome: getCdMomenProcmPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdMomenProcmPagamento
	 */
	public Integer getCdMomenProcmPagamento() {
		return cdMomenProcmPagamento;
	}
	/**
	 * Nome: setCdMomenProcmPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdMomenProcmPagamento
	 */
	public void setCdMomenProcmPagamento(Integer cdMomenProcmPagamento) {
		this.cdMomenProcmPagamento = cdMomenProcmPagamento;
	}
	/**
	 * Nome: getCdMensagemRcadtMidia
	 *
	 * @exception
	 * @throws
	 * @return cdMensagemRcadtMidia
	 */
	public Integer getCdMensagemRcadtMidia() {
		return cdMensagemRcadtMidia;
	}
	/**
	 * Nome: setCdMensagemRcadtMidia
	 *
	 * @exception
	 * @throws
	 * @param cdMensagemRcadtMidia
	 */
	public void setCdMensagemRcadtMidia(Integer cdMensagemRcadtMidia) {
		this.cdMensagemRcadtMidia = cdMensagemRcadtMidia;
	}
	/**
	 * Nome: getCdPermissaoDebitoOnline
	 *
	 * @exception
	 * @throws
	 * @return cdPermissaoDebitoOnline
	 */
	public Integer getCdPermissaoDebitoOnline() {
		return cdPermissaoDebitoOnline;
	}
	/**
	 * Nome: setCdPermissaoDebitoOnline
	 *
	 * @exception
	 * @throws
	 * @param cdPermissaoDebitoOnline
	 */
	public void setCdPermissaoDebitoOnline(Integer cdPermissaoDebitoOnline) {
		this.cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
	}
	/**
	 * Nome: getCdNaturezaOperacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdNaturezaOperacaoPagamento
	 */
	public Integer getCdNaturezaOperacaoPagamento() {
		return cdNaturezaOperacaoPagamento;
	}
	/**
	 * Nome: setCdNaturezaOperacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdNaturezaOperacaoPagamento
	 */
	public void setCdNaturezaOperacaoPagamento(Integer cdNaturezaOperacaoPagamento) {
		this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
	}
	/**
	 * Nome: getCdPerdcAviso
	 *
	 * @exception
	 * @throws
	 * @return cdPerdcAviso
	 */
	public Integer getCdPerdcAviso() {
		return cdPerdcAviso;
	}
	/**
	 * Nome: setCdPerdcAviso
	 *
	 * @exception
	 * @throws
	 * @param cdPerdcAviso
	 */
	public void setCdPerdcAviso(Integer cdPerdcAviso) {
		this.cdPerdcAviso = cdPerdcAviso;
	}
	/**
	 * Nome: getCdPerdcCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @return cdPerdcCobrancaTarifa
	 */
	public Integer getCdPerdcCobrancaTarifa() {
		return cdPerdcCobrancaTarifa;
	}
	/**
	 * Nome: setCdPerdcCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @param cdPerdcCobrancaTarifa
	 */
	public void setCdPerdcCobrancaTarifa(Integer cdPerdcCobrancaTarifa) {
		this.cdPerdcCobrancaTarifa = cdPerdcCobrancaTarifa;
	}
	/**
	 * Nome: getCdPerdcComprovante
	 *
	 * @exception
	 * @throws
	 * @return cdPerdcComprovante
	 */
	public Integer getCdPerdcComprovante() {
		return cdPerdcComprovante;
	}
	/**
	 * Nome: setCdPerdcComprovante
	 *
	 * @exception
	 * @throws
	 * @param cdPerdcComprovante
	 */
	public void setCdPerdcComprovante(Integer cdPerdcComprovante) {
		this.cdPerdcComprovante = cdPerdcComprovante;
	}
	/**
	 * Nome: getCdPerdcConsVeiculo
	 *
	 * @exception
	 * @throws
	 * @return cdPerdcConsVeiculo
	 */
	public Integer getCdPerdcConsVeiculo() {
		return cdPerdcConsVeiculo;
	}
	/**
	 * Nome: setCdPerdcConsVeiculo
	 *
	 * @exception
	 * @throws
	 * @param cdPerdcConsVeiculo
	 */
	public void setCdPerdcConsVeiculo(Integer cdPerdcConsVeiculo) {
		this.cdPerdcConsVeiculo = cdPerdcConsVeiculo;
	}
	/**
	 * Nome: getCdPerdcEnvioRemessa
	 *
	 * @exception
	 * @throws
	 * @return cdPerdcEnvioRemessa
	 */
	public Integer getCdPerdcEnvioRemessa() {
		return cdPerdcEnvioRemessa;
	}
	/**
	 * Nome: setCdPerdcEnvioRemessa
	 *
	 * @exception
	 * @throws
	 * @param cdPerdcEnvioRemessa
	 */
	public void setCdPerdcEnvioRemessa(Integer cdPerdcEnvioRemessa) {
		this.cdPerdcEnvioRemessa = cdPerdcEnvioRemessa;
	}
	/**
	 * Nome: getCdPerdcManutencaoProcd
	 *
	 * @exception
	 * @throws
	 * @return cdPerdcManutencaoProcd
	 */
	public Integer getCdPerdcManutencaoProcd() {
		return cdPerdcManutencaoProcd;
	}
	/**
	 * Nome: setCdPerdcManutencaoProcd
	 *
	 * @exception
	 * @throws
	 * @param cdPerdcManutencaoProcd
	 */
	public void setCdPerdcManutencaoProcd(Integer cdPerdcManutencaoProcd) {
		this.cdPerdcManutencaoProcd = cdPerdcManutencaoProcd;
	}
	/**
	 * Nome: getCdPagamentoNaoUtil
	 *
	 * @exception
	 * @throws
	 * @return cdPagamentoNaoUtil
	 */
	public Integer getCdPagamentoNaoUtil() {
		return cdPagamentoNaoUtil;
	}
	/**
	 * Nome: setCdPagamentoNaoUtil
	 *
	 * @exception
	 * @throws
	 * @param cdPagamentoNaoUtil
	 */
	public void setCdPagamentoNaoUtil(Integer cdPagamentoNaoUtil) {
		this.cdPagamentoNaoUtil = cdPagamentoNaoUtil;
	}
	/**
	 * Nome: getCdPrincipalEnquaRcadt
	 *
	 * @exception
	 * @throws
	 * @return cdPrincipalEnquaRcadt
	 */
	public Integer getCdPrincipalEnquaRcadt() {
		return cdPrincipalEnquaRcadt;
	}
	/**
	 * Nome: setCdPrincipalEnquaRcadt
	 *
	 * @exception
	 * @throws
	 * @param cdPrincipalEnquaRcadt
	 */
	public void setCdPrincipalEnquaRcadt(Integer cdPrincipalEnquaRcadt) {
		this.cdPrincipalEnquaRcadt = cdPrincipalEnquaRcadt;
	}
	/**
	 * Nome: getCdPriorEfetivacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @return cdPriorEfetivacaoPagamento
	 */
	public Integer getCdPriorEfetivacaoPagamento() {
		return cdPriorEfetivacaoPagamento;
	}
	/**
	 * Nome: setCdPriorEfetivacaoPagamento
	 *
	 * @exception
	 * @throws
	 * @param cdPriorEfetivacaoPagamento
	 */
	public void setCdPriorEfetivacaoPagamento(Integer cdPriorEfetivacaoPagamento) {
		this.cdPriorEfetivacaoPagamento = cdPriorEfetivacaoPagamento;
	}
	/**
	 * Nome: getCdRejeiAgendaLote
	 *
	 * @exception
	 * @throws
	 * @return cdRejeiAgendaLote
	 */
	public Integer getCdRejeiAgendaLote() {
		return cdRejeiAgendaLote;
	}
	/**
	 * Nome: setCdRejeiAgendaLote
	 *
	 * @exception
	 * @throws
	 * @param cdRejeiAgendaLote
	 */
	public void setCdRejeiAgendaLote(Integer cdRejeiAgendaLote) {
		this.cdRejeiAgendaLote = cdRejeiAgendaLote;
	}
	/**
	 * Nome: getCdRejeiEfetivacaoLote
	 *
	 * @exception
	 * @throws
	 * @return cdRejeiEfetivacaoLote
	 */
	public Integer getCdRejeiEfetivacaoLote() {
		return cdRejeiEfetivacaoLote;
	}
	/**
	 * Nome: setCdRejeiEfetivacaoLote
	 *
	 * @exception
	 * @throws
	 * @param cdRejeiEfetivacaoLote
	 */
	public void setCdRejeiEfetivacaoLote(Integer cdRejeiEfetivacaoLote) {
		this.cdRejeiEfetivacaoLote = cdRejeiEfetivacaoLote;
	}
	/**
	 * Nome: getCdRejeiLote
	 *
	 * @exception
	 * @throws
	 * @return cdRejeiLote
	 */
	public Integer getCdRejeiLote() {
		return cdRejeiLote;
	}
	/**
	 * Nome: setCdRejeiLote
	 *
	 * @exception
	 * @throws
	 * @param cdRejeiLote
	 */
	public void setCdRejeiLote(Integer cdRejeiLote) {
		this.cdRejeiLote = cdRejeiLote;
	}
	/**
	 * Nome: getCdRastreabNotaFiscal
	 *
	 * @exception
	 * @throws
	 * @return cdRastreabNotaFiscal
	 */
	public Integer getCdRastreabNotaFiscal() {
		return cdRastreabNotaFiscal;
	}
	/**
	 * Nome: setCdRastreabNotaFiscal
	 *
	 * @exception
	 * @throws
	 * @param cdRastreabNotaFiscal
	 */
	public void setCdRastreabNotaFiscal(Integer cdRastreabNotaFiscal) {
		this.cdRastreabNotaFiscal = cdRastreabNotaFiscal;
	}
	/**
	 * Nome: getCdRastreabTituloTerc
	 *
	 * @exception
	 * @throws
	 * @return cdRastreabTituloTerc
	 */
	public Integer getCdRastreabTituloTerc() {
		return cdRastreabTituloTerc;
	}
	/**
	 * Nome: setCdRastreabTituloTerc
	 *
	 * @exception
	 * @throws
	 * @param cdRastreabTituloTerc
	 */
	public void setCdRastreabTituloTerc(Integer cdRastreabTituloTerc) {
		this.cdRastreabTituloTerc = cdRastreabTituloTerc;
	}
	/**
	 * Nome: getCdTipoCargaRcadt
	 *
	 * @exception
	 * @throws
	 * @return cdTipoCargaRcadt
	 */
	public Integer getCdTipoCargaRcadt() {
		return cdTipoCargaRcadt;
	}
	/**
	 * Nome: setCdTipoCargaRcadt
	 *
	 * @exception
	 * @throws
	 * @param cdTipoCargaRcadt
	 */
	public void setCdTipoCargaRcadt(Integer cdTipoCargaRcadt) {
		this.cdTipoCargaRcadt = cdTipoCargaRcadt;
	}
	/**
	 * Nome: getCdTipoCataoSalarial
	 *
	 * @exception
	 * @throws
	 * @return cdTipoCataoSalarial
	 */
	public Integer getCdTipoCataoSalarial() {
		return cdTipoCataoSalarial;
	}
	/**
	 * Nome: setCdTipoCataoSalarial
	 *
	 * @exception
	 * @throws
	 * @param cdTipoCataoSalarial
	 */
	public void setCdTipoCataoSalarial(Integer cdTipoCataoSalarial) {
		this.cdTipoCataoSalarial = cdTipoCataoSalarial;
	}
	/**
	 * Nome: getCdTipoDataFloat
	 *
	 * @exception
	 * @throws
	 * @return cdTipoDataFloat
	 */
	public Integer getCdTipoDataFloat() {
		return cdTipoDataFloat;
	}
	/**
	 * Nome: setCdTipoDataFloat
	 *
	 * @exception
	 * @throws
	 * @param cdTipoDataFloat
	 */
	public void setCdTipoDataFloat(Integer cdTipoDataFloat) {
		this.cdTipoDataFloat = cdTipoDataFloat;
	}
	/**
	 * Nome: getCdTipoDivergenciaVeiculo
	 *
	 * @exception
	 * @throws
	 * @return cdTipoDivergenciaVeiculo
	 */
	public Integer getCdTipoDivergenciaVeiculo() {
		return cdTipoDivergenciaVeiculo;
	}
	/**
	 * Nome: setCdTipoDivergenciaVeiculo
	 *
	 * @exception
	 * @throws
	 * @param cdTipoDivergenciaVeiculo
	 */
	public void setCdTipoDivergenciaVeiculo(Integer cdTipoDivergenciaVeiculo) {
		this.cdTipoDivergenciaVeiculo = cdTipoDivergenciaVeiculo;
	}
	/**
	 * Nome: getCdTipoIdBenef
	 *
	 * @exception
	 * @throws
	 * @return cdTipoIdBenef
	 */
	public Integer getCdTipoIdBenef() {
		return cdTipoIdBenef;
	}
	/**
	 * Nome: setCdTipoIdBenef
	 *
	 * @exception
	 * @throws
	 * @param cdTipoIdBenef
	 */
	public void setCdTipoIdBenef(Integer cdTipoIdBenef) {
		this.cdTipoIdBenef = cdTipoIdBenef;
	}
	/**
	 * Nome: getCdTipoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	/**
	 * Nome: setCdTipoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @param cdTipoLayoutArquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	/**
	 * Nome: getCdTipoReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @return cdTipoReajusteTarifa
	 */
	public Integer getCdTipoReajusteTarifa() {
		return cdTipoReajusteTarifa;
	}
	/**
	 * Nome: setCdTipoReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @param cdTipoReajusteTarifa
	 */
	public void setCdTipoReajusteTarifa(Integer cdTipoReajusteTarifa) {
		this.cdTipoReajusteTarifa = cdTipoReajusteTarifa;
	}
	/**
	 * Nome: getCdTratoContaTransf
	 *
	 * @exception
	 * @throws
	 * @return cdTratoContaTransf
	 */
	public Integer getCdTratoContaTransf() {
		return cdTratoContaTransf;
	}
	/**
	 * Nome: setCdTratoContaTransf
	 *
	 * @exception
	 * @throws
	 * @param cdTratoContaTransf
	 */
	public void setCdTratoContaTransf(Integer cdTratoContaTransf) {
		this.cdTratoContaTransf = cdTratoContaTransf;
	}
	/**
	 * Nome: getCdUtilzFavorecidoCtrl
	 *
	 * @exception
	 * @throws
	 * @return cdUtilzFavorecidoCtrl
	 */
	public Integer getCdUtilzFavorecidoCtrl() {
		return cdUtilzFavorecidoCtrl;
	}
	/**
	 * Nome: setCdUtilzFavorecidoCtrl
	 *
	 * @exception
	 * @throws
	 * @param cdUtilzFavorecidoCtrl
	 */
	public void setCdUtilzFavorecidoCtrl(Integer cdUtilzFavorecidoCtrl) {
		this.cdUtilzFavorecidoCtrl = cdUtilzFavorecidoCtrl;
	}
	/**
	 * Nome: getNrFechamentoApurcTarifa
	 *
	 * @exception
	 * @throws
	 * @return nrFechamentoApurcTarifa
	 */
	public Integer getNrFechamentoApurcTarifa() {
		return nrFechamentoApurcTarifa;
	}
	/**
	 * Nome: setNrFechamentoApurcTarifa
	 *
	 * @exception
	 * @throws
	 * @param nrFechamentoApurcTarifa
	 */
	public void setNrFechamentoApurcTarifa(Integer nrFechamentoApurcTarifa) {
		this.nrFechamentoApurcTarifa = nrFechamentoApurcTarifa;
	}
	/**
	 * Nome: getCdPercentualIndiceReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @return cdPercentualIndiceReajusteTarifa
	 */
	public BigDecimal getCdPercentualIndiceReajusteTarifa() {
		return cdPercentualIndiceReajusteTarifa;
	}
	/**
	 * Nome: setCdPercentualIndiceReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @param cdPercentualIndiceReajusteTarifa
	 */
	public void setCdPercentualIndiceReajusteTarifa(
			BigDecimal cdPercentualIndiceReajusteTarifa) {
		this.cdPercentualIndiceReajusteTarifa = cdPercentualIndiceReajusteTarifa;
	}
	/**
	 * Nome: getCdPercentualMaximoInconLote
	 *
	 * @exception
	 * @throws
	 * @return cdPercentualMaximoInconLote
	 */
	public Integer getCdPercentualMaximoInconLote() {
		return cdPercentualMaximoInconLote;
	}
	/**
	 * Nome: setCdPercentualMaximoInconLote
	 *
	 * @exception
	 * @throws
	 * @param cdPercentualMaximoInconLote
	 */
	public void setCdPercentualMaximoInconLote(Integer cdPercentualMaximoInconLote) {
		this.cdPercentualMaximoInconLote = cdPercentualMaximoInconLote;
	}
	/**
	 * Nome: getCdPercentualReducaoTarifaCatlg
	 *
	 * @exception
	 * @throws
	 * @return cdPercentualReducaoTarifaCatlg
	 */
	public BigDecimal getCdPercentualReducaoTarifaCatlg() {
		return cdPercentualReducaoTarifaCatlg;
	}
	/**
	 * Nome: setCdPercentualReducaoTarifaCatlg
	 *
	 * @exception
	 * @throws
	 * @param cdPercentualReducaoTarifaCatlg
	 */
	public void setCdPercentualReducaoTarifaCatlg(
			BigDecimal cdPercentualReducaoTarifaCatlg) {
		this.cdPercentualReducaoTarifaCatlg = cdPercentualReducaoTarifaCatlg;
	}
	/**
	 * Nome: getQtAntecedencia
	 *
	 * @exception
	 * @throws
	 * @return qtAntecedencia
	 */
	public Integer getQtAntecedencia() {
		return qtAntecedencia;
	}
	/**
	 * Nome: setQtAntecedencia
	 *
	 * @exception
	 * @throws
	 * @param qtAntecedencia
	 */
	public void setQtAntecedencia(Integer qtAntecedencia) {
		this.qtAntecedencia = qtAntecedencia;
	}
	/**
	 * Nome: getQtAnteriorVencimentoCompv
	 *
	 * @exception
	 * @throws
	 * @return qtAnteriorVencimentoCompv
	 */
	public Integer getQtAnteriorVencimentoCompv() {
		return qtAnteriorVencimentoCompv;
	}
	/**
	 * Nome: setQtAnteriorVencimentoCompv
	 *
	 * @exception
	 * @throws
	 * @param qtAnteriorVencimentoCompv
	 */
	public void setQtAnteriorVencimentoCompv(Integer qtAnteriorVencimentoCompv) {
		this.qtAnteriorVencimentoCompv = qtAnteriorVencimentoCompv;
	}
	/**
	 * Nome: getQtDiaCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @return qtDiaCobrancaTarifa
	 */
	public Integer getQtDiaCobrancaTarifa() {
		return qtDiaCobrancaTarifa;
	}
	/**
	 * Nome: setQtDiaCobrancaTarifa
	 *
	 * @exception
	 * @throws
	 * @param qtDiaCobrancaTarifa
	 */
	public void setQtDiaCobrancaTarifa(Integer qtDiaCobrancaTarifa) {
		this.qtDiaCobrancaTarifa = qtDiaCobrancaTarifa;
	}
	/**
	 * Nome: getQtDiaExpiracao
	 *
	 * @exception
	 * @throws
	 * @return qtDiaExpiracao
	 */
	public Integer getQtDiaExpiracao() {
		return qtDiaExpiracao;
	}
	/**
	 * Nome: setQtDiaExpiracao
	 *
	 * @exception
	 * @throws
	 * @param qtDiaExpiracao
	 */
	public void setQtDiaExpiracao(Integer qtDiaExpiracao) {
		this.qtDiaExpiracao = qtDiaExpiracao;
	}
	/**
	 * Nome: getQtDiaFloatPagamento
	 *
	 * @exception
	 * @throws
	 * @return qtDiaFloatPagamento
	 */
	public Integer getQtDiaFloatPagamento() {
		return qtDiaFloatPagamento;
	}
	/**
	 * Nome: setQtDiaFloatPagamento
	 *
	 * @exception
	 * @throws
	 * @param qtDiaFloatPagamento
	 */
	public void setQtDiaFloatPagamento(Integer qtDiaFloatPagamento) {
		this.qtDiaFloatPagamento = qtDiaFloatPagamento;
	}
	/**
	 * Nome: getQtDiaInativFavorecido
	 *
	 * @exception
	 * @throws
	 * @return qtDiaInativFavorecido
	 */
	public Integer getQtDiaInativFavorecido() {
		return qtDiaInativFavorecido;
	}
	/**
	 * Nome: setQtDiaInativFavorecido
	 *
	 * @exception
	 * @throws
	 * @param qtDiaInativFavorecido
	 */
	public void setQtDiaInativFavorecido(Integer qtDiaInativFavorecido) {
		this.qtDiaInativFavorecido = qtDiaInativFavorecido;
	}
	/**
	 * Nome: getQtDiaRepiqCons
	 *
	 * @exception
	 * @throws
	 * @return qtDiaRepiqCons
	 */
	public Integer getQtDiaRepiqCons() {
		return qtDiaRepiqCons;
	}
	/**
	 * Nome: setQtDiaRepiqCons
	 *
	 * @exception
	 * @throws
	 * @param qtDiaRepiqCons
	 */
	public void setQtDiaRepiqCons(Integer qtDiaRepiqCons) {
		this.qtDiaRepiqCons = qtDiaRepiqCons;
	}
	/**
	 * Nome: getQtEtapaRcadtBeneficiario
	 *
	 * @exception
	 * @throws
	 * @return qtEtapaRcadtBeneficiario
	 */
	public Integer getQtEtapaRcadtBeneficiario() {
		return qtEtapaRcadtBeneficiario;
	}
	/**
	 * Nome: setQtEtapaRcadtBeneficiario
	 *
	 * @exception
	 * @throws
	 * @param qtEtapaRcadtBeneficiario
	 */
	public void setQtEtapaRcadtBeneficiario(Integer qtEtapaRcadtBeneficiario) {
		this.qtEtapaRcadtBeneficiario = qtEtapaRcadtBeneficiario;
	}
	/**
	 * Nome: getQtFaseRcadtBenefiario
	 *
	 * @exception
	 * @throws
	 * @return qtFaseRcadtBenefiario
	 */
	public Integer getQtFaseRcadtBenefiario() {
		return qtFaseRcadtBenefiario;
	}
	/**
	 * Nome: setQtFaseRcadtBenefiario
	 *
	 * @exception
	 * @throws
	 * @param qtFaseRcadtBenefiario
	 */
	public void setQtFaseRcadtBenefiario(Integer qtFaseRcadtBenefiario) {
		this.qtFaseRcadtBenefiario = qtFaseRcadtBenefiario;
	}
	/**
	 * Nome: getQtLimiteLinhas
	 *
	 * @exception
	 * @throws
	 * @return qtLimiteLinhas
	 */
	public Integer getQtLimiteLinhas() {
		return qtLimiteLinhas;
	}
	/**
	 * Nome: setQtLimiteLinhas
	 *
	 * @exception
	 * @throws
	 * @param qtLimiteLinhas
	 */
	public void setQtLimiteLinhas(Integer qtLimiteLinhas) {
		this.qtLimiteLinhas = qtLimiteLinhas;
	}
	/**
	 * Nome: getQtLimiteSolicitacaoCatao
	 *
	 * @exception
	 * @throws
	 * @return qtLimiteSolicitacaoCatao
	 */
	public Integer getQtLimiteSolicitacaoCatao() {
		return qtLimiteSolicitacaoCatao;
	}
	/**
	 * Nome: setQtLimiteSolicitacaoCatao
	 *
	 * @exception
	 * @throws
	 * @param qtLimiteSolicitacaoCatao
	 */
	public void setQtLimiteSolicitacaoCatao(Integer qtLimiteSolicitacaoCatao) {
		this.qtLimiteSolicitacaoCatao = qtLimiteSolicitacaoCatao;
	}
	/**
	 * Nome: getQtMaximaInconLote
	 *
	 * @exception
	 * @throws
	 * @return qtMaximaInconLote
	 */
	public Integer getQtMaximaInconLote() {
		return qtMaximaInconLote;
	}
	/**
	 * Nome: setQtMaximaInconLote
	 *
	 * @exception
	 * @throws
	 * @param qtMaximaInconLote
	 */
	public void setQtMaximaInconLote(Integer qtMaximaInconLote) {
		this.qtMaximaInconLote = qtMaximaInconLote;
	}
	/**
	 * Nome: getQtMaximaTituloVencido
	 *
	 * @exception
	 * @throws
	 * @return qtMaximaTituloVencido
	 */
	public Integer getQtMaximaTituloVencido() {
		return qtMaximaTituloVencido;
	}
	/**
	 * Nome: setQtMaximaTituloVencido
	 *
	 * @exception
	 * @throws
	 * @param qtMaximaTituloVencido
	 */
	public void setQtMaximaTituloVencido(Integer qtMaximaTituloVencido) {
		this.qtMaximaTituloVencido = qtMaximaTituloVencido;
	}
	/**
	 * Nome: getQtMesComprovante
	 *
	 * @exception
	 * @throws
	 * @return qtMesComprovante
	 */
	public Integer getQtMesComprovante() {
		return qtMesComprovante;
	}
	/**
	 * Nome: setQtMesComprovante
	 *
	 * @exception
	 * @throws
	 * @param qtMesComprovante
	 */
	public void setQtMesComprovante(Integer qtMesComprovante) {
		this.qtMesComprovante = qtMesComprovante;
	}
	/**
	 * Nome: getQtMesRcadt
	 *
	 * @exception
	 * @throws
	 * @return qtMesRcadt
	 */
	public Integer getQtMesRcadt() {
		return qtMesRcadt;
	}
	/**
	 * Nome: setQtMesRcadt
	 *
	 * @exception
	 * @throws
	 * @param qtMesRcadt
	 */
	public void setQtMesRcadt(Integer qtMesRcadt) {
		this.qtMesRcadt = qtMesRcadt;
	}
	/**
	 * Nome: getQtMesFaseRcadt
	 *
	 * @exception
	 * @throws
	 * @return qtMesFaseRcadt
	 */
	public Integer getQtMesFaseRcadt() {
		return qtMesFaseRcadt;
	}
	/**
	 * Nome: setQtMesFaseRcadt
	 *
	 * @exception
	 * @throws
	 * @param qtMesFaseRcadt
	 */
	public void setQtMesFaseRcadt(Integer qtMesFaseRcadt) {
		this.qtMesFaseRcadt = qtMesFaseRcadt;
	}
	/**
	 * Nome: getQtMesReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @return qtMesReajusteTarifa
	 */
	public Integer getQtMesReajusteTarifa() {
		return qtMesReajusteTarifa;
	}
	/**
	 * Nome: setQtMesReajusteTarifa
	 *
	 * @exception
	 * @throws
	 * @param qtMesReajusteTarifa
	 */
	public void setQtMesReajusteTarifa(Integer qtMesReajusteTarifa) {
		this.qtMesReajusteTarifa = qtMesReajusteTarifa;
	}
	/**
	 * Nome: getQtViaAviso
	 *
	 * @exception
	 * @throws
	 * @return qtViaAviso
	 */
	public Integer getQtViaAviso() {
		return qtViaAviso;
	}
	/**
	 * Nome: setQtViaAviso
	 *
	 * @exception
	 * @throws
	 * @param qtViaAviso
	 */
	public void setQtViaAviso(Integer qtViaAviso) {
		this.qtViaAviso = qtViaAviso;
	}
	/**
	 * Nome: getQtViaCobranca
	 *
	 * @exception
	 * @throws
	 * @return qtViaCobranca
	 */
	public Integer getQtViaCobranca() {
		return qtViaCobranca;
	}
	/**
	 * Nome: setQtViaCobranca
	 *
	 * @exception
	 * @throws
	 * @param qtViaCobranca
	 */
	public void setQtViaCobranca(Integer qtViaCobranca) {
		this.qtViaCobranca = qtViaCobranca;
	}
	/**
	 * Nome: getQtViaComprovante
	 *
	 * @exception
	 * @throws
	 * @return qtViaComprovante
	 */
	public Integer getQtViaComprovante() {
		return qtViaComprovante;
	}
	/**
	 * Nome: setQtViaComprovante
	 *
	 * @exception
	 * @throws
	 * @param qtViaComprovante
	 */
	public void setQtViaComprovante(Integer qtViaComprovante) {
		this.qtViaComprovante = qtViaComprovante;
	}
	/**
	 * Nome: getVlFavorecidoNaoCadtr
	 *
	 * @exception
	 * @throws
	 * @return vlFavorecidoNaoCadtr
	 */
	public BigDecimal getVlFavorecidoNaoCadtr() {
		return vlFavorecidoNaoCadtr;
	}
	/**
	 * Nome: setVlFavorecidoNaoCadtr
	 *
	 * @exception
	 * @throws
	 * @param vlFavorecidoNaoCadtr
	 */
	public void setVlFavorecidoNaoCadtr(BigDecimal vlFavorecidoNaoCadtr) {
		this.vlFavorecidoNaoCadtr = vlFavorecidoNaoCadtr;
	}
	/**
	 * Nome: getVlLimiteDiaPagamento
	 *
	 * @exception
	 * @throws
	 * @return vlLimiteDiaPagamento
	 */
	public BigDecimal getVlLimiteDiaPagamento() {
		return vlLimiteDiaPagamento;
	}
	/**
	 * Nome: setVlLimiteDiaPagamento
	 *
	 * @exception
	 * @throws
	 * @param vlLimiteDiaPagamento
	 */
	public void setVlLimiteDiaPagamento(BigDecimal vlLimiteDiaPagamento) {
		this.vlLimiteDiaPagamento = vlLimiteDiaPagamento;
	}
	/**
	 * Nome: getVlLimiteIndvdPagamento
	 *
	 * @exception
	 * @throws
	 * @return vlLimiteIndvdPagamento
	 */
	public BigDecimal getVlLimiteIndvdPagamento() {
		return vlLimiteIndvdPagamento;
	}
	/**
	 * Nome: setVlLimiteIndvdPagamento
	 *
	 * @exception
	 * @throws
	 * @param vlLimiteIndvdPagamento
	 */
	public void setVlLimiteIndvdPagamento(BigDecimal vlLimiteIndvdPagamento) {
		this.vlLimiteIndvdPagamento = vlLimiteIndvdPagamento;
	}
	/**
	 * Nome: getCdIndicadorEmissaoAviso
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorEmissaoAviso
	 */
	public Integer getCdIndicadorEmissaoAviso() {
		return cdIndicadorEmissaoAviso;
	}
	/**
	 * Nome: setCdIndicadorEmissaoAviso
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorEmissaoAviso
	 */
	public void setCdIndicadorEmissaoAviso(Integer cdIndicadorEmissaoAviso) {
		this.cdIndicadorEmissaoAviso = cdIndicadorEmissaoAviso;
	}
	/**
	 * Nome: getCdIndicadorRetornoInternet
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorRetornoInternet
	 */
	public Integer getCdIndicadorRetornoInternet() {
		return cdIndicadorRetornoInternet;
	}
	/**
	 * Nome: setCdIndicadorRetornoInternet
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorRetornoInternet
	 */
	public void setCdIndicadorRetornoInternet(Integer cdIndicadorRetornoInternet) {
		this.cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
	}
	/**
	 * Nome: getCdIndicadorListaDebito
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorListaDebito
	 */
	public Integer getCdIndicadorListaDebito() {
		return cdIndicadorListaDebito;
	}
	/**
	 * Nome: setCdIndicadorListaDebito
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorListaDebito
	 */
	public void setCdIndicadorListaDebito(Integer cdIndicadorListaDebito) {
		this.cdIndicadorListaDebito = cdIndicadorListaDebito;
	}
	/**
	 * Nome: getCdTipoFormacaoLista
	 *
	 * @exception
	 * @throws
	 * @return cdTipoFormacaoLista
	 */
	public Integer getCdTipoFormacaoLista() {
		return cdTipoFormacaoLista;
	}
	/**
	 * Nome: setCdTipoFormacaoLista
	 *
	 * @exception
	 * @throws
	 * @param cdTipoFormacaoLista
	 */
	public void setCdTipoFormacaoLista(Integer cdTipoFormacaoLista) {
		this.cdTipoFormacaoLista = cdTipoFormacaoLista;
	}
	/**
	 * Nome: getCdTipoConsistenciaLista
	 *
	 * @exception
	 * @throws
	 * @return cdTipoConsistenciaLista
	 */
	public Integer getCdTipoConsistenciaLista() {
		return cdTipoConsistenciaLista;
	}
	/**
	 * Nome: setCdTipoConsistenciaLista
	 *
	 * @exception
	 * @throws
	 * @param cdTipoConsistenciaLista
	 */
	public void setCdTipoConsistenciaLista(Integer cdTipoConsistenciaLista) {
		this.cdTipoConsistenciaLista = cdTipoConsistenciaLista;
	}
	/**
	 * Nome: getCdTipoConsultaComprovante
	 *
	 * @exception
	 * @throws
	 * @return cdTipoConsultaComprovante
	 */
	public Integer getCdTipoConsultaComprovante() {
		return cdTipoConsultaComprovante;
	}
	/**
	 * Nome: setCdTipoConsultaComprovante
	 *
	 * @exception
	 * @throws
	 * @param cdTipoConsultaComprovante
	 */
	public void setCdTipoConsultaComprovante(Integer cdTipoConsultaComprovante) {
		this.cdTipoConsultaComprovante = cdTipoConsultaComprovante;
	}
	/**
	 * Nome: getCdValidacaoNomeFavorecido
	 *
	 * @exception
	 * @throws
	 * @return cdValidacaoNomeFavorecido
	 */
	public Integer getCdValidacaoNomeFavorecido() {
		return cdValidacaoNomeFavorecido;
	}
	/**
	 * Nome: setCdValidacaoNomeFavorecido
	 *
	 * @exception
	 * @throws
	 * @param cdValidacaoNomeFavorecido
	 */
	public void setCdValidacaoNomeFavorecido(Integer cdValidacaoNomeFavorecido) {
		this.cdValidacaoNomeFavorecido = cdValidacaoNomeFavorecido;
	}
	/**
	 * Nome: getCdTipoContaFavorecido
	 *
	 * @exception
	 * @throws
	 * @return cdTipoContaFavorecido
	 */
	public Integer getCdTipoContaFavorecido() {
		return cdTipoContaFavorecido;
	}
	/**
	 * Nome: setCdTipoContaFavorecido
	 *
	 * @exception
	 * @throws
	 * @param cdTipoContaFavorecido
	 */
	public void setCdTipoContaFavorecido(Integer cdTipoContaFavorecido) {
		this.cdTipoContaFavorecido = cdTipoContaFavorecido;
	}
	/**
	 * Nome: getCdIndLancamentoPersonalizado
	 *
	 * @exception
	 * @throws
	 * @return cdIndLancamentoPersonalizado
	 */
	public Integer getCdIndLancamentoPersonalizado() {
		return cdIndLancamentoPersonalizado;
	}
	/**
	 * Nome: setCdIndLancamentoPersonalizado
	 *
	 * @exception
	 * @throws
	 * @param cdIndLancamentoPersonalizado
	 */
	public void setCdIndLancamentoPersonalizado(Integer cdIndLancamentoPersonalizado) {
		this.cdIndLancamentoPersonalizado = cdIndLancamentoPersonalizado;
	}
	/**
	 * Nome: getCdAgendaRastreabilidadeFilial
	 *
	 * @exception
	 * @throws
	 * @return cdAgendaRastreabilidadeFilial
	 */
	public Integer getCdAgendaRastreabilidadeFilial() {
		return cdAgendaRastreabilidadeFilial;
	}
	/**
	 * Nome: setCdAgendaRastreabilidadeFilial
	 *
	 * @exception
	 * @throws
	 * @param cdAgendaRastreabilidadeFilial
	 */
	public void setCdAgendaRastreabilidadeFilial(
			Integer cdAgendaRastreabilidadeFilial) {
		this.cdAgendaRastreabilidadeFilial = cdAgendaRastreabilidadeFilial;
	}
	/**
	 * Nome: getCdIndicadorAdesaoSacador
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorAdesaoSacador
	 */
	public Integer getCdIndicadorAdesaoSacador() {
		return cdIndicadorAdesaoSacador;
	}
	/**
	 * Nome: setCdIndicadorAdesaoSacador
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorAdesaoSacador
	 */
	public void setCdIndicadorAdesaoSacador(Integer cdIndicadorAdesaoSacador) {
		this.cdIndicadorAdesaoSacador = cdIndicadorAdesaoSacador;
	}
	/**
	 * Nome: getCdMeioPagamentoCredito
	 *
	 * @exception
	 * @throws
	 * @return cdMeioPagamentoCredito
	 */
	public Integer getCdMeioPagamentoCredito() {
		return cdMeioPagamentoCredito;
	}
	/**
	 * Nome: setCdMeioPagamentoCredito
	 *
	 * @exception
	 * @throws
	 * @param cdMeioPagamentoCredito
	 */
	public void setCdMeioPagamentoCredito(Integer cdMeioPagamentoCredito) {
		this.cdMeioPagamentoCredito = cdMeioPagamentoCredito;
	}
	/**
	 * Nome: getCdTipoIsncricaoFavorecido
	 *
	 * @exception
	 * @throws
	 * @return cdTipoIsncricaoFavorecido
	 */
	public Integer getCdTipoIsncricaoFavorecido() {
		return cdTipoIsncricaoFavorecido;
	}
	/**
	 * Nome: setCdTipoIsncricaoFavorecido
	 *
	 * @exception
	 * @throws
	 * @param cdTipoIsncricaoFavorecido
	 */
	public void setCdTipoIsncricaoFavorecido(Integer cdTipoIsncricaoFavorecido) {
		this.cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
	}
	/**
	 * Nome: getCdIndicadorBancoPostal
	 *
	 * @exception
	 * @throws
	 * @return cdIndicadorBancoPostal
	 */
	public Integer getCdIndicadorBancoPostal() {
		return cdIndicadorBancoPostal;
	}
	/**
	 * Nome: setCdIndicadorBancoPostal
	 *
	 * @exception
	 * @throws
	 * @param cdIndicadorBancoPostal
	 */
	public void setCdIndicadorBancoPostal(Integer cdIndicadorBancoPostal) {
		this.cdIndicadorBancoPostal = cdIndicadorBancoPostal;
	}
	/**
	 * Nome: getCdFormularioContratoCliente
	 *
	 * @exception
	 * @throws
	 * @return cdFormularioContratoCliente
	 */
	public Integer getCdFormularioContratoCliente() {
		return cdFormularioContratoCliente;
	}
	/**
	 * Nome: setCdFormularioContratoCliente
	 *
	 * @exception
	 * @throws
	 * @param cdFormularioContratoCliente
	 */
	public void setCdFormularioContratoCliente(Integer cdFormularioContratoCliente) {
		this.cdFormularioContratoCliente = cdFormularioContratoCliente;
	}
	/**
	 * Nome: getCdConsultaSaldoValorSuperior
	 *
	 * @exception
	 * @throws
	 * @return cdConsultaSaldoValorSuperior
	 */
	public Integer getCdConsultaSaldoValorSuperior() {
		return cdConsultaSaldoValorSuperior;
	}
	/**
	 * Nome: setCdConsultaSaldoValorSuperior
	 *
	 * @exception
	 * @throws
	 * @param cdConsultaSaldoValorSuperior
	 */
	public void setCdConsultaSaldoValorSuperior(Integer cdConsultaSaldoValorSuperior) {
		this.cdConsultaSaldoValorSuperior = cdConsultaSaldoValorSuperior;
	}

}
