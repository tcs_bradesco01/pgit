package br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean;

public class AlterarOperacaoServicoPagtoIntegradoSaidaDTO {

    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;

    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem(){
        return codMensagem;
    }

    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem){
        this.codMensagem = codMensagem;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem(){
        return mensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem){
        this.mensagem = mensagem;
    }
}
