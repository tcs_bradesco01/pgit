/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;


/**
 * Nome: ListarEnderecoParticipanteOcorrenciasSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarEnderecoParticipanteOcorrenciasSaidaDTO {

    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdClub. */
    private Long cdClub;

    /** Atributo cdCpfCnpj. */
    private Long cdCpfCnpj;

    /** Atributo cdCnpjContrato. */
    private Integer cdCnpjContrato;

    /** Atributo cdCpfCnpjDigito. */
    private Integer cdCpfCnpjDigito;

    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;

    /** Atributo cdTipoParticipacaoPessoa. */
    private Integer cdTipoParticipacaoPessoa;

    /** Atributo dsTipoParticipante. */
    private String dsTipoParticipante;

    /** Atributo cdDescricaoSituacaoParticapante. */
    private String cdDescricaoSituacaoParticapante;

    /** Atributo dsTipoServico. */
    private String dsTipoServico;

    /** Atributo dsModalidadeServico. */
    private String dsModalidadeServico;

    /** Atributo dsOperacaoCatalogo. */
    private String dsOperacaoCatalogo;

    /** Atributo cdFinalidadeEnderecoContrato. */
    private Integer cdFinalidadeEnderecoContrato;

    /** Atributo cdEspecieEndereco. */
    private Integer cdEspecieEndereco;

    /** Atributo nrSequenciaEnderecoOpcional. */
    private Integer nrSequenciaEnderecoOpcional;

    /** Atributo cdUsoEnderecoPessoa. */
    private Integer cdUsoEnderecoPessoa;

    /** Atributo cdUsoPostal. */
    private Integer cdUsoPostal;

    /**
     * Set: cdPessoaJuridicaContrato.
     *
     * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
     */
    public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
        this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
    }

    /**
     * Get: cdPessoaJuridicaContrato.
     *
     * @return cdPessoaJuridicaContrato
     */
    public Long getCdPessoaJuridicaContrato() {
        return this.cdPessoaJuridicaContrato;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set: cdClub.
     *
     * @param cdClub the cd club
     */
    public void setCdClub(Long cdClub) {
        this.cdClub = cdClub;
    }

    /**
     * Get: cdClub.
     *
     * @return cdClub
     */
    public Long getCdClub() {
        return this.cdClub;
    }

    /**
     * Set: cdCpfCnpj.
     *
     * @param cdCpfCnpj the cd cpf cnpj
     */
    public void setCdCpfCnpj(Long cdCpfCnpj) {
        this.cdCpfCnpj = cdCpfCnpj;
    }

    /**
     * Get: cdCpfCnpj.
     *
     * @return cdCpfCnpj
     */
    public Long getCdCpfCnpj() {
        return this.cdCpfCnpj;
    }

    /**
     * Set: cdCnpjContrato.
     *
     * @param cdCnpjContrato the cd cnpj contrato
     */
    public void setCdCnpjContrato(Integer cdCnpjContrato) {
        this.cdCnpjContrato = cdCnpjContrato;
    }

    /**
     * Get: cdCnpjContrato.
     *
     * @return cdCnpjContrato
     */
    public Integer getCdCnpjContrato() {
        return this.cdCnpjContrato;
    }

    /**
     * Set: cdCpfCnpjDigito.
     *
     * @param cdCpfCnpjDigito the cd cpf cnpj digito
     */
    public void setCdCpfCnpjDigito(Integer cdCpfCnpjDigito) {
        this.cdCpfCnpjDigito = cdCpfCnpjDigito;
    }

    /**
     * Get: cdCpfCnpjDigito.
     *
     * @return cdCpfCnpjDigito
     */
    public Integer getCdCpfCnpjDigito() {
        return this.cdCpfCnpjDigito;
    }

    /**
     * Set: dsRazaoSocial.
     *
     * @param dsRazaoSocial the ds razao social
     */
    public void setDsRazaoSocial(String dsRazaoSocial) {
        this.dsRazaoSocial = dsRazaoSocial;
    }

    /**
     * Get: dsRazaoSocial.
     *
     * @return dsRazaoSocial
     */
    public String getDsRazaoSocial() {
        return this.dsRazaoSocial;
    }

    /**
     * Set: cdTipoParticipacaoPessoa.
     *
     * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
     */
    public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
        this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
    }

    /**
     * Get: cdTipoParticipacaoPessoa.
     *
     * @return cdTipoParticipacaoPessoa
     */
    public Integer getCdTipoParticipacaoPessoa() {
        return this.cdTipoParticipacaoPessoa;
    }

    /**
     * Set: dsTipoParticipante.
     *
     * @param dsTipoParticipante the ds tipo participante
     */
    public void setDsTipoParticipante(String dsTipoParticipante) {
        this.dsTipoParticipante = dsTipoParticipante;
    }

    /**
     * Get: dsTipoParticipante.
     *
     * @return dsTipoParticipante
     */
    public String getDsTipoParticipante() {
        return this.dsTipoParticipante;
    }

    /**
     * Set: cdDescricaoSituacaoParticapante.
     *
     * @param cdDescricaoSituacaoParticapante the cd descricao situacao particapante
     */
    public void setCdDescricaoSituacaoParticapante(String cdDescricaoSituacaoParticapante) {
        this.cdDescricaoSituacaoParticapante = cdDescricaoSituacaoParticapante;
    }

    /**
     * Get: cdDescricaoSituacaoParticapante.
     *
     * @return cdDescricaoSituacaoParticapante
     */
    public String getCdDescricaoSituacaoParticapante() {
        return this.cdDescricaoSituacaoParticapante;
    }

    /**
     * Set: dsTipoServico.
     *
     * @param dsTipoServico the ds tipo servico
     */
    public void setDsTipoServico(String dsTipoServico) {
        this.dsTipoServico = dsTipoServico;
    }

    /**
     * Get: dsTipoServico.
     *
     * @return dsTipoServico
     */
    public String getDsTipoServico() {
        return this.dsTipoServico;
    }

    /**
     * Set: dsModalidadeServico.
     *
     * @param dsModalidadeServico the ds modalidade servico
     */
    public void setDsModalidadeServico(String dsModalidadeServico) {
        this.dsModalidadeServico = dsModalidadeServico;
    }

    /**
     * Get: dsModalidadeServico.
     *
     * @return dsModalidadeServico
     */
    public String getDsModalidadeServico() {
        return this.dsModalidadeServico;
    }

    /**
     * Set: dsOperacaoCatalogo.
     *
     * @param dsOperacaoCatalogo the ds operacao catalogo
     */
    public void setDsOperacaoCatalogo(String dsOperacaoCatalogo) {
        this.dsOperacaoCatalogo = dsOperacaoCatalogo;
    }

    /**
     * Get: dsOperacaoCatalogo.
     *
     * @return dsOperacaoCatalogo
     */
    public String getDsOperacaoCatalogo() {
        return this.dsOperacaoCatalogo;
    }

    /**
     * Set: cdFinalidadeEnderecoContrato.
     *
     * @param cdFinalidadeEnderecoContrato the cd finalidade endereco contrato
     */
    public void setCdFinalidadeEnderecoContrato(Integer cdFinalidadeEnderecoContrato) {
        this.cdFinalidadeEnderecoContrato = cdFinalidadeEnderecoContrato;
    }

    /**
     * Get: cdFinalidadeEnderecoContrato.
     *
     * @return cdFinalidadeEnderecoContrato
     */
    public Integer getCdFinalidadeEnderecoContrato() {
        return this.cdFinalidadeEnderecoContrato;
    }

    /**
     * Get: cdEspecieEndereco.
     *
     * @return cdEspecieEndereco
     */
    public Integer getCdEspecieEndereco() {
		return cdEspecieEndereco;
	}

	/**
	 * Set: cdEspecieEndereco.
	 *
	 * @param cdEspecieEndereco the cd especie endereco
	 */
	public void setCdEspecieEndereco(Integer cdEspecieEndereco) {
		this.cdEspecieEndereco = cdEspecieEndereco;
	}

	/**
	 * Get: nrSequenciaEnderecoOpcional.
	 *
	 * @return nrSequenciaEnderecoOpcional
	 */
	public Integer getNrSequenciaEnderecoOpcional() {
		return nrSequenciaEnderecoOpcional;
	}

	/**
	 * Set: nrSequenciaEnderecoOpcional.
	 *
	 * @param nrSequenciaEnderecoOpcional the nr sequencia endereco opcional
	 */
	public void setNrSequenciaEnderecoOpcional(Integer nrSequenciaEnderecoOpcional) {
		this.nrSequenciaEnderecoOpcional = nrSequenciaEnderecoOpcional;
	}

	/**
	 * Get: cdUsoEnderecoPessoa.
	 *
	 * @return cdUsoEnderecoPessoa
	 */
	public Integer getCdUsoEnderecoPessoa() {
		return cdUsoEnderecoPessoa;
	}

	/**
	 * Set: cdUsoEnderecoPessoa.
	 *
	 * @param cdUsoEnderecoPessoa the cd uso endereco pessoa
	 */
	public void setCdUsoEnderecoPessoa(Integer cdUsoEnderecoPessoa) {
		this.cdUsoEnderecoPessoa = cdUsoEnderecoPessoa;
	}

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
    	return CpfCnpjUtils.formatarCpfCnpj(getCdCpfCnpj(), getCdCnpjContrato(), getCdCpfCnpjDigito());
    }

    /**
     * Get: cdDsTipoParticipacao.
     *
     * @return cdDsTipoParticipacao
     */
    public String getCdDsTipoParticipacao() {
    	if (getCdTipoParticipacaoPessoa() == null && getDsTipoParticipante() == null) {
    		return "";
    	}

    	StringBuilder codDesc = new StringBuilder();
    	if (getCdTipoParticipacaoPessoa() != null) {
    		codDesc.append(getCdTipoParticipacaoPessoa());
    	}
    	codDesc.append(" - ");
    	if (getDsTipoParticipante() != null) {
    		codDesc.append(getDsTipoParticipante());
    	}
    	return codDesc.toString();
    }

	/**
	 * Get: cdUsoPostal.
	 *
	 * @return cdUsoPostal
	 */
	public Integer getCdUsoPostal() {
		return cdUsoPostal;
	}

	/**
	 * Set: cdUsoPostal.
	 *
	 * @param cdUsoPostal the cd uso postal
	 */
	public void setCdUsoPostal(Integer cdUsoPostal) {
		this.cdUsoPostal = cdUsoPostal;
	}
}