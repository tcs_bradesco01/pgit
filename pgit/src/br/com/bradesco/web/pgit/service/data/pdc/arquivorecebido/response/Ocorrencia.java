/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Ocorrencia.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencia implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cpf
     */
    private java.lang.String _cpf;

    /**
     * Field _nome
     */
    private java.lang.String _nome;

    /**
     * Field _produto
     */
    private java.lang.String _produto;

    /**
     * Field _perfil
     */
    private java.lang.String _perfil;

    /**
     * Field _remessa
     */
    private java.lang.String _remessa;

    /**
     * Field _dataHoraRecepcao
     */
    private java.lang.String _dataHoraRecepcao;

    /**
     * Field _processamento
     */
    private java.lang.String _processamento;

    /**
     * Field _resultadoProcessamento
     */
    private java.lang.String _resultadoProcessamento;

    /**
     * Field _qtdRegistros
     */
    private int _qtdRegistros;

    /**
     * keeps track of state for field: _qtdRegistros
     */
    private boolean _has_qtdRegistros;

    /**
     * Field _valorTotal
     */
    private double _valorTotal;

    /**
     * keeps track of state for field: _valorTotal
     */
    private boolean _has_valorTotal;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencia() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.response.Ocorrencia()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteQtdRegistros
     * 
     */
    public void deleteQtdRegistros()
    {
        this._has_qtdRegistros= false;
    } //-- void deleteQtdRegistros() 

    /**
     * Method deleteValorTotal
     * 
     */
    public void deleteValorTotal()
    {
        this._has_valorTotal= false;
    } //-- void deleteValorTotal() 

    /**
     * Returns the value of field 'cpf'.
     * 
     * @return String
     * @return the value of field 'cpf'.
     */
    public java.lang.String getCpf()
    {
        return this._cpf;
    } //-- java.lang.String getCpf() 

    /**
     * Returns the value of field 'dataHoraRecepcao'.
     * 
     * @return String
     * @return the value of field 'dataHoraRecepcao'.
     */
    public java.lang.String getDataHoraRecepcao()
    {
        return this._dataHoraRecepcao;
    } //-- java.lang.String getDataHoraRecepcao() 

    /**
     * Returns the value of field 'nome'.
     * 
     * @return String
     * @return the value of field 'nome'.
     */
    public java.lang.String getNome()
    {
        return this._nome;
    } //-- java.lang.String getNome() 

    /**
     * Returns the value of field 'perfil'.
     * 
     * @return String
     * @return the value of field 'perfil'.
     */
    public java.lang.String getPerfil()
    {
        return this._perfil;
    } //-- java.lang.String getPerfil() 

    /**
     * Returns the value of field 'processamento'.
     * 
     * @return String
     * @return the value of field 'processamento'.
     */
    public java.lang.String getProcessamento()
    {
        return this._processamento;
    } //-- java.lang.String getProcessamento() 

    /**
     * Returns the value of field 'produto'.
     * 
     * @return String
     * @return the value of field 'produto'.
     */
    public java.lang.String getProduto()
    {
        return this._produto;
    } //-- java.lang.String getProduto() 

    /**
     * Returns the value of field 'qtdRegistros'.
     * 
     * @return int
     * @return the value of field 'qtdRegistros'.
     */
    public int getQtdRegistros()
    {
        return this._qtdRegistros;
    } //-- int getQtdRegistros() 

    /**
     * Returns the value of field 'remessa'.
     * 
     * @return String
     * @return the value of field 'remessa'.
     */
    public java.lang.String getRemessa()
    {
        return this._remessa;
    } //-- java.lang.String getRemessa() 

    /**
     * Returns the value of field 'resultadoProcessamento'.
     * 
     * @return String
     * @return the value of field 'resultadoProcessamento'.
     */
    public java.lang.String getResultadoProcessamento()
    {
        return this._resultadoProcessamento;
    } //-- java.lang.String getResultadoProcessamento() 

    /**
     * Returns the value of field 'valorTotal'.
     * 
     * @return double
     * @return the value of field 'valorTotal'.
     */
    public double getValorTotal()
    {
        return this._valorTotal;
    } //-- double getValorTotal() 

    /**
     * Method hasQtdRegistros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdRegistros()
    {
        return this._has_qtdRegistros;
    } //-- boolean hasQtdRegistros() 

    /**
     * Method hasValorTotal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorTotal()
    {
        return this._has_valorTotal;
    } //-- boolean hasValorTotal() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cpf'.
     * 
     * @param cpf the value of field 'cpf'.
     */
    public void setCpf(java.lang.String cpf)
    {
        this._cpf = cpf;
    } //-- void setCpf(java.lang.String) 

    /**
     * Sets the value of field 'dataHoraRecepcao'.
     * 
     * @param dataHoraRecepcao the value of field 'dataHoraRecepcao'
     */
    public void setDataHoraRecepcao(java.lang.String dataHoraRecepcao)
    {
        this._dataHoraRecepcao = dataHoraRecepcao;
    } //-- void setDataHoraRecepcao(java.lang.String) 

    /**
     * Sets the value of field 'nome'.
     * 
     * @param nome the value of field 'nome'.
     */
    public void setNome(java.lang.String nome)
    {
        this._nome = nome;
    } //-- void setNome(java.lang.String) 

    /**
     * Sets the value of field 'perfil'.
     * 
     * @param perfil the value of field 'perfil'.
     */
    public void setPerfil(java.lang.String perfil)
    {
        this._perfil = perfil;
    } //-- void setPerfil(java.lang.String) 

    /**
     * Sets the value of field 'processamento'.
     * 
     * @param processamento the value of field 'processamento'.
     */
    public void setProcessamento(java.lang.String processamento)
    {
        this._processamento = processamento;
    } //-- void setProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'produto'.
     * 
     * @param produto the value of field 'produto'.
     */
    public void setProduto(java.lang.String produto)
    {
        this._produto = produto;
    } //-- void setProduto(java.lang.String) 

    /**
     * Sets the value of field 'qtdRegistros'.
     * 
     * @param qtdRegistros the value of field 'qtdRegistros'.
     */
    public void setQtdRegistros(int qtdRegistros)
    {
        this._qtdRegistros = qtdRegistros;
        this._has_qtdRegistros = true;
    } //-- void setQtdRegistros(int) 

    /**
     * Sets the value of field 'remessa'.
     * 
     * @param remessa the value of field 'remessa'.
     */
    public void setRemessa(java.lang.String remessa)
    {
        this._remessa = remessa;
    } //-- void setRemessa(java.lang.String) 

    /**
     * Sets the value of field 'resultadoProcessamento'.
     * 
     * @param resultadoProcessamento the value of field
     * 'resultadoProcessamento'.
     */
    public void setResultadoProcessamento(java.lang.String resultadoProcessamento)
    {
        this._resultadoProcessamento = resultadoProcessamento;
    } //-- void setResultadoProcessamento(java.lang.String) 

    /**
     * Sets the value of field 'valorTotal'.
     * 
     * @param valorTotal the value of field 'valorTotal'.
     */
    public void setValorTotal(double valorTotal)
    {
        this._valorTotal = valorTotal;
        this._has_valorTotal = true;
    } //-- void setValorTotal(double) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencia
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.response.Ocorrencia unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.response.Ocorrencia) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.response.Ocorrencia.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.arquivorecebido.response.Ocorrencia unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
