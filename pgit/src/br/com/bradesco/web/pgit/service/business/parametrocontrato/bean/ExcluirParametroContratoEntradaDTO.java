package br.com.bradesco.web.pgit.service.business.parametrocontrato.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 22/01/16.
 */
public class ExcluirParametroContratoEntradaDTO {

    /** The cdpessoa juridica contrato. */
    private Long cdpessoaJuridicaContrato = null;

    /** The cd tipo contrato negocio. */
    private Integer cdTipoContratoNegocio = null;

    /** The nr sequencia contrato negocio. */
    private Long nrSequenciaContratoNegocio = null;

    /** The cd parametro. */
    private Integer cdParametro = null;

    /**
     * Sets the cdpessoa juridica contrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Gets the cdpessoa juridica contrato.
     *
     * @return the cdpessoa juridica contrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Sets the cd tipo contrato negocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Gets the cd tipo contrato negocio.
     *
     * @return the cd tipo contrato negocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Sets the nr sequencia contrato negocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Gets the nr sequencia contrato negocio.
     *
     * @return the nr sequencia contrato negocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Sets the cd parametro.
     *
     * @param cdParametro the cd parametro
     */
    public void setCdParametro(Integer cdParametro) {
        this.cdParametro = cdParametro;
    }

    /**
     * Gets the cd parametro.
     *
     * @return the cd parametro
     */
    public Integer getCdParametro() {
        return this.cdParametro;
    }
}