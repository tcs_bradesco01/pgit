package br.com.bradesco.web.pgit.service.business.parametrocontrato.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 22/01/16.
 */
public class ListarParametroContratoEntradaDTO {

    /** The nr maximo ocorrencias. */
    private Integer nrMaximoOcorrencias = null;

    /** The cdpessoa juridica contrato. */
    private Long cdpessoaJuridicaContrato = null;

    /** The cd tipo contrato negocio. */
    private Integer cdTipoContratoNegocio = null;

    /** The nr sequencia contrato negocio. */
    private Long nrSequenciaContratoNegocio = null;

    /**
     * Sets the nr maximo ocorrencias.
     *
     * @param nrMaximoOcorrencias the nr maximo ocorrencias
     */
    public void setNrMaximoOcorrencias(Integer nrMaximoOcorrencias) {
        this.nrMaximoOcorrencias = nrMaximoOcorrencias;
    }

    /**
     * Gets the nr maximo ocorrencias.
     *
     * @return the nr maximo ocorrencias
     */
    public Integer getNrMaximoOcorrencias() {
        return this.nrMaximoOcorrencias;
    }

    /**
     * Sets the cdpessoa juridica contrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Gets the cdpessoa juridica contrato.
     *
     * @return the cdpessoa juridica contrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Sets the cd tipo contrato negocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Gets the cd tipo contrato negocio.
     *
     * @return the cd tipo contrato negocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Sets the nr sequencia contrato negocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Gets the nr sequencia contrato negocio.
     *
     * @return the nr sequencia contrato negocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }
}