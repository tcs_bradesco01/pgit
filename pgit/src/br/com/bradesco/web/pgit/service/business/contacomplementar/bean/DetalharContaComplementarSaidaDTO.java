package br.com.bradesco.web.pgit.service.business.contacomplementar.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

public class DetalharContaComplementarSaidaDTO {
	
	private Long cdpessoaJuridicaContrato;
	private Integer cdTipoContratoNegocio;
	private Long nrSequenciaContratoNegocio;
	private Integer cdContaComplementar;
	
	private Long cdCorpoCnpjCpf;
	private Integer cdFilialCpfCnpj;
	private Integer CdCtrlCpfCnpj;
	
	private String dsNomeParticipante;
	
	private Long cdPessoaJuridicaContratoConta;
	private Integer cdTipoContratoConta;
	
	private Long nrSequenciaContratoConta;
	
	private Integer cdBanco;
	private String dsBanco;
	
	private Integer cdAgencia;
	private String  dsAgencia;

	private Long cdConta;
	private String cdDigitoConta;
	
	private String cdUsuarioInclusao;
	private String hrInclusaoRegistro;
	
	private Integer cdTipoCanalInclusao;
	private String cdUsuarioManutencao;
	
	private String hrManutencaoRegistro;
	private Integer cdTipoCanalManutencao;
	
	public DetalharContaComplementarSaidaDTO() {
		super();
	}

	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public Integer getCdContaComplementar() {
		return cdContaComplementar;
	}

	public void setCdContaComplementar(Integer cdContaComplementar) {
		this.cdContaComplementar = cdContaComplementar;
	}

	public Long getCdCorpoCnpjCpf() {
		return cdCorpoCnpjCpf;
	}

	public void setCdCorpoCnpjCpf(Long cdCorpoCnpjCpf) {
		this.cdCorpoCnpjCpf = cdCorpoCnpjCpf;
	}

	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}

	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}

	public Integer getCdCtrlCpfCnpj() {
		return CdCtrlCpfCnpj;
	}

	public void setCdCtrlCpfCnpj(Integer cdCtrlCpfCnpj) {
		CdCtrlCpfCnpj = cdCtrlCpfCnpj;
	}

	public String getDsNomeParticipante() {
		return dsNomeParticipante;
	}

	public void setDsNomeParticipante(String dsNomeParticipante) {
		this.dsNomeParticipante = dsNomeParticipante;
	}

	public Long getCdPessoaJuridicaContratoConta() {
		return cdPessoaJuridicaContratoConta;
	}

	public void setCdPessoaJuridicaContratoConta(Long cdPessoaJuridicaContratoConta) {
		this.cdPessoaJuridicaContratoConta = cdPessoaJuridicaContratoConta;
	}

	public Integer getCdTipoContratoConta() {
		return cdTipoContratoConta;
	}

	public void setCdTipoContratoConta(Integer cdTipoContratoConta) {
		this.cdTipoContratoConta = cdTipoContratoConta;
	}

	public Long getNrSequenciaContratoConta() {
		return nrSequenciaContratoConta;
	}

	public void setNrSequenciaContratoConta(Long nrSequenciaContratoConta) {
		this.nrSequenciaContratoConta = nrSequenciaContratoConta;
	}

	public Integer getCdBanco() {
		return cdBanco;
	}

	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	public String getDsBanco() {
		return dsBanco;
	}

	public void setDsBanco(String dsBanco) {
		this.dsBanco = dsBanco;
	}

	public Integer getCdAgencia() {
		return cdAgencia;
	}

	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	public String getDsAgencia() {
		return dsAgencia;
	}

	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}

	public Long getCdConta() {
		return cdConta;
	}

	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}

	public String getCdDigitoConta() {
		return cdDigitoConta;
	}

	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}

	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}
	
	public String getCnpjOuCpfFormatado() {
		return CpfCnpjUtils.formatCpfCnpjCompleto(cdCorpoCnpjCpf, cdFilialCpfCnpj, CdCtrlCpfCnpj);
	}
}