/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ConsultarCondCobTarifaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarCondCobTarifaEntradaDTO {
	
	/** Atributo cdPessoaContrato. */
	private long cdPessoaContrato;
	
	/** Atributo cdTipoContrato. */
	private int cdTipoContrato;
	
	/** Atributo nrSequenciaContrato. */
	private long nrSequenciaContrato;
	
	/** Atributo cdServico. */
	private int cdServico;
	
	/** Atributo cdModalidade. */
	private int cdModalidade;
	
	/** Atributo cdRelacionamentoProduto. */
	private int cdRelacionamentoProduto;
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
	
	/**
	 * Get: cdPessoaContrato.
	 *
	 * @return cdPessoaContrato
	 */
	public long getCdPessoaContrato() {
		return cdPessoaContrato;
	}
	
	/**
	 * Set: cdPessoaContrato.
	 *
	 * @param cdPessoaContrato the cd pessoa contrato
	 */
	public void setCdPessoaContrato(long cdPessoaContrato) {
		this.cdPessoaContrato = cdPessoaContrato;
	}
	
	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public int getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(int cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	
	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public int getCdServico() {
		return cdServico;
	}
	
	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(int cdServico) {
		this.cdServico = cdServico;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public int getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(int cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	
	
	
	

}
