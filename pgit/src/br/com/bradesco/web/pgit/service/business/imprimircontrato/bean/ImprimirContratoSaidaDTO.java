/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimircontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimircontrato.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.service.data.pdc.imprimircontrato.response.ImprimirContratoResponse;

/**
 * Nome: ImprimirContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoSaidaDTO {
    
    /** Atributo cdClubPessoa. */
    private Long cdClubPessoa;
    
    /** Atributo cdCpfCnpjParticipante. */
    private String cdCpfCnpjParticipante;
    
    /** Atributo nmParticipante. */
    private String nmParticipante;
    
    /** Atributo cdPessoaEmpresa. */
    private Long cdPessoaEmpresa;
    
    /** Atributo cdCpfCnpjEmpresa. */
    private String cdCpfCnpjEmpresa;
    
    /** Atributo nmEmpresa. */
    private String nmEmpresa;
    
    /** Atributo cdAgenciaGestorContrato. */
    private Integer cdAgenciaGestorContrato;
    
    /** Atributo cdDigitoAgenciaGestor. */
    private Integer cdDigitoAgenciaGestor;
    
    /** Atributo dsAgenciaGestor. */
    private String dsAgenciaGestor;
    
    /** Atributo cdContaAgenciaGestor. */
    private Long cdContaAgenciaGestor;
    
    /** Atributo cdDigitoContaGestor. */
    private String cdDigitoContaGestor;
    
    /** Atributo cdCepAgencia. */
    private Integer cdCepAgencia;
    
    /** Atributo cdComplementoCepAgencia. */
    private Integer cdComplementoCepAgencia;
    
    /** Atributo dsLogradouroAgencia. */
    private String dsLogradouroAgencia;
    
    /** Atributo nrLogradouroAgencia. */
    private String nrLogradouroAgencia;
    
    /** Atributo dsComplementoAgencia. */
    private String dsComplementoAgencia;
    
    /** Atributo dsBairroAgencia. */
    private String dsBairroAgencia;
    
    /** Atributo dsCidadeAgencia. */
    private String dsCidadeAgencia;
    
    /** Atributo cdSiglaUfAgencia. */
    private String cdSiglaUfAgencia;
    
    /** Atributo cdCep. */
    private Integer cdCep;
    
    /** Atributo cdCepComplemento. */
    private Integer cdCepComplemento;
    
    /** Atributo dsLogradouroPessoa. */
    private String dsLogradouroPessoa;
    
    /** Atributo dsLogradouroNumero. */
    private String dsLogradouroNumero;
    
    /** Atributo dsComplementoEndereco. */
    private String dsComplementoEndereco;
    
    /** Atributo dsBairroEndereco. */
    private String dsBairroEndereco;
    
    /** Atributo dsCidadeEndereco. */
    private String dsCidadeEndereco;
    
    /** Atributo cdSiglaUf. */
    private String cdSiglaUf;
    
    /** Atributo qtDiaInatividade. */
    private Integer qtDiaInatividade;
    
    /** Atributo dsDiaInatividade. */
    private String dsDiaInatividade;
    
    /** Atributo cdImpressaoFolha. */
    private String cdImpressaoFolha;
    
    /** Atributo vlTarifaFolha. */
    private BigDecimal vlTarifaFolha;
    
    /** Atributo dsTarifa. */
    private String dsTarifa;
    
    /** Atributo qtDiaAviso. */
    private Integer qtDiaAviso;
    
    /** Atributo dsDiaAviso. */
    private String dsDiaAviso;
    
    /** Atributo dsImpressaoFornecedor. */
    private String dsImpressaoFornecedor;
    
    /** Atributo dsCreditoConta. */
    private String dsCreditoConta;
    
    /** Atributo dsChequeOp. */
    private String dsChequeOp;
    
    /** Atributo dsDoc. */
    private String dsDoc;
    
    /** Atributo dsTed. */
    private String dsTed;
    
    /** Atributo dsTituloBradesco. */
    private String dsTituloBradesco;
    
    /** Atributo dsTituloOutros. */
    private String dsTituloOutros;
    
    /** Atributo dsImpressaoPtrbLine. */
    private String dsImpressaoPtrbLine;
    
    /** Atributo dsImpressaoPtrbEmpresa. */
    private String dsImpressaoPtrbEmpresa;
    
    /** Atributo dsImpressaoPtrbTributo. */
    private String dsImpressaoPtrbTributo;
    
    /** Atributo dsImpressaoComprovante. */
    private String dsImpressaoComprovante;
    
    /** Atributo qtMesComprovante. */
    private Integer qtMesComprovante;
    
    /** Atributo dsTipoRejeicao. */
    private String dsTipoRejeicao;
    
	/**
	 * Imprimir contrato saida dto.
	 */
	public ImprimirContratoSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Imprimir contrato saida dto.
	 *
	 * @param response the response
	 */
	public ImprimirContratoSaidaDTO(ImprimirContratoResponse response) {
		super();
		this.cdClubPessoa = response.getCdClubPessoa();
		this.cdCpfCnpjParticipante = response.getCdCpfCnpjParticipante();
		this.nmParticipante = response.getNmParticipante();
		this.cdPessoaEmpresa = response.getCdPessoaEmpresa();
		this.cdCpfCnpjEmpresa = response.getCdCpfCnpjEmpresa();
		this.nmEmpresa = response.getNmEmpresa();
		this.cdAgenciaGestorContrato = response.getCdAgenciaGestorContrato();
		this.cdDigitoAgenciaGestor = response.getCdDigitoAgenciaGestor();
		this.dsAgenciaGestor = response.getDsAgenciaGestor();
		this.cdContaAgenciaGestor = response.getCdContaAgenciaGestor();
		this.cdDigitoContaGestor = response.getCdDigitoContaGestor();
		this.cdCepAgencia = response.getCdCepAgencia();
		this.cdComplementoCepAgencia = response.getCdComplementoCepAgencia();
		this.dsLogradouroAgencia = response.getDsLogradouroAgencia();
		this.nrLogradouroAgencia = response.getNrLogradouroAgencia();
		this.dsComplementoAgencia = response.getDsComplementoAgencia();
		this.dsBairroAgencia = response.getDsBairroAgencia();
		this.dsCidadeAgencia = response.getDsCidadeAgencia();
		this.cdSiglaUfAgencia = response.getCdSiglaUfAgencia();
		this.cdCep = response.getCdCep();
		this.cdCepComplemento = response.getCdCepComplemento();
		this.dsLogradouroPessoa = response.getDsLogradouroPessoa();
		this.dsLogradouroNumero = response.getDsLogradouroNumero();
		this.dsComplementoEndereco = response.getDsComplementoEndereco();
		this.dsBairroEndereco = response.getDsBairroEndereco();
		this.dsCidadeEndereco = response.getDsCidadeEndereco();
		this.cdSiglaUf = response.getCdSiglaUf();
		this.qtDiaInatividade = response.getQtDiaInatividade();
		this.dsDiaInatividade = response.getDsDiaInatividade();
		this.cdImpressaoFolha = response.getCdImpressaoFolha();
		this.vlTarifaFolha = response.getVlTarifaFolha();
		this.dsTarifa = response.getDsTarifa();
		this.qtDiaAviso = response.getQtDiaAviso();
		this.dsDiaAviso = response.getDsDiaAviso();
		this.dsImpressaoFornecedor = response.getDsImpressaoFornecedor();
		this.dsCreditoConta = response.getDsCreditoConta();
		this.dsChequeOp = response.getDsChequeOp();
		this.dsDoc = response.getDsDoc();
		this.dsTed = response.getDsTed();
		this.dsTituloBradesco = response.getDsTituloBradesco();
		this.dsTituloOutros = response.getDsTituloOutros();
		this.dsImpressaoPtrbLine = response.getDsImpressaoPtrbLine();
		this.dsImpressaoPtrbEmpresa = response.getDsImpressaoPtrbEmpresa();
		this.dsImpressaoPtrbTributo = response.getDsImpressaoPtrbTributo();
		this.dsImpressaoComprovante = response.getDsImpressaoComprovante();
		this.qtMesComprovante = response.getQtMesComprovante();
		this.dsTipoRejeicao = response.getDsTipoRejeicao();
	}

	/**
	 * Get: cdAgenciaGestorContrato.
	 *
	 * @return cdAgenciaGestorContrato
	 */
	public Integer getCdAgenciaGestorContrato() {
		return cdAgenciaGestorContrato;
	}

	/**
	 * Set: cdAgenciaGestorContrato.
	 *
	 * @param cdAgenciaGestorContrato the cd agencia gestor contrato
	 */
	public void setCdAgenciaGestorContrato(Integer cdAgenciaGestorContrato) {
		this.cdAgenciaGestorContrato = cdAgenciaGestorContrato;
	}

	/**
	 * Get: cdCep.
	 *
	 * @return cdCep
	 */
	public Integer getCdCep() {
		return cdCep;
	}

	/**
	 * Get: cdCepAgencia.
	 *
	 * @return cdCepAgencia
	 */
	public Integer getCdCepAgencia() {
		return cdCepAgencia;
	}

	/**
	 * Set: cdCepAgencia.
	 *
	 * @param cdCepAgencia the cd cep agencia
	 */
	public void setCdCepAgencia(Integer cdCepAgencia) {
		this.cdCepAgencia = cdCepAgencia;
	}

	/**
	 * Get: cdClubPessoa.
	 *
	 * @return cdClubPessoa
	 */
	public Long getCdClubPessoa() {
		return cdClubPessoa;
	}

	/**
	 * Set: cdClubPessoa.
	 *
	 * @param cdClubPessoa the cd club pessoa
	 */
	public void setCdClubPessoa(Long cdClubPessoa) {
		this.cdClubPessoa = cdClubPessoa;
	}

	/**
	 * Get: cdComplementoCepAgencia.
	 *
	 * @return cdComplementoCepAgencia
	 */
	public Integer getCdComplementoCepAgencia() {
		return cdComplementoCepAgencia;
	}

	/**
	 * Set: cdComplementoCepAgencia.
	 *
	 * @param cdComplementoCepAgencia the cd complemento cep agencia
	 */
	public void setCdComplementoCepAgencia(Integer cdComplementoCepAgencia) {
		this.cdComplementoCepAgencia = cdComplementoCepAgencia;
	}

	/**
	 * Get: cdContaAgenciaGestor.
	 *
	 * @return cdContaAgenciaGestor
	 */
	public Long getCdContaAgenciaGestor() {
		return cdContaAgenciaGestor;
	}

	/**
	 * Set: cdContaAgenciaGestor.
	 *
	 * @param cdContaAgenciaGestor the cd conta agencia gestor
	 */
	public void setCdContaAgenciaGestor(Long cdContaAgenciaGestor) {
		this.cdContaAgenciaGestor = cdContaAgenciaGestor;
	}

	/**
	 * Get: cdCpfCnpjEmpresa.
	 *
	 * @return cdCpfCnpjEmpresa
	 */
	public String getCdCpfCnpjEmpresa() {
		return cdCpfCnpjEmpresa;
	}

	/**
	 * Set: cdCpfCnpjEmpresa.
	 *
	 * @param cdCpfCnpjEmpresa the cd cpf cnpj empresa
	 */
	public void setCdCpfCnpjEmpresa(String cdCpfCnpjEmpresa) {
		this.cdCpfCnpjEmpresa = cdCpfCnpjEmpresa;
	}

	/**
	 * Get: cdCpfCnpjParticipante.
	 *
	 * @return cdCpfCnpjParticipante
	 */
	public String getCdCpfCnpjParticipante() {
		return cdCpfCnpjParticipante;
	}

	/**
	 * Set: cdCpfCnpjParticipante.
	 *
	 * @param cdCpfCnpjParticipante the cd cpf cnpj participante
	 */
	public void setCdCpfCnpjParticipante(String cdCpfCnpjParticipante) {
		this.cdCpfCnpjParticipante = cdCpfCnpjParticipante;
	}

	/**
	 * Get: cdDigitoAgenciaGestor.
	 *
	 * @return cdDigitoAgenciaGestor
	 */
	public Integer getCdDigitoAgenciaGestor() {
		return cdDigitoAgenciaGestor;
	}

	/**
	 * Set: cdDigitoAgenciaGestor.
	 *
	 * @param cdDigitoAgenciaGestor the cd digito agencia gestor
	 */
	public void setCdDigitoAgenciaGestor(Integer cdDigitoAgenciaGestor) {
		this.cdDigitoAgenciaGestor = cdDigitoAgenciaGestor;
	}

	/**
	 * Get: cdDigitoContaGestor.
	 *
	 * @return cdDigitoContaGestor
	 */
	public String getCdDigitoContaGestor() {
		return cdDigitoContaGestor;
	}

	/**
	 * Set: cdDigitoContaGestor.
	 *
	 * @param cdDigitoContaGestor the cd digito conta gestor
	 */
	public void setCdDigitoContaGestor(String cdDigitoContaGestor) {
		this.cdDigitoContaGestor = cdDigitoContaGestor;
	}

	/**
	 * Get: cdImpressaoFolha.
	 *
	 * @return cdImpressaoFolha
	 */
	public String getCdImpressaoFolha() {
		return cdImpressaoFolha;
	}

	/**
	 * Set: cdImpressaoFolha.
	 *
	 * @param cdImpressaoFolha the cd impressao folha
	 */
	public void setCdImpressaoFolha(String cdImpressaoFolha) {
		this.cdImpressaoFolha = cdImpressaoFolha;
	}

	/**
	 * Get: cdPessoaEmpresa.
	 *
	 * @return cdPessoaEmpresa
	 */
	public Long getCdPessoaEmpresa() {
		return cdPessoaEmpresa;
	}

	/**
	 * Set: cdPessoaEmpresa.
	 *
	 * @param cdPessoaEmpresa the cd pessoa empresa
	 */
	public void setCdPessoaEmpresa(Long cdPessoaEmpresa) {
		this.cdPessoaEmpresa = cdPessoaEmpresa;
	}

	/**
	 * Get: cdSiglaUf.
	 *
	 * @return cdSiglaUf
	 */
	public String getCdSiglaUf() {
		return cdSiglaUf;
	}

	/**
	 * Set: cdSiglaUf.
	 *
	 * @param cdSiglaUf the cd sigla uf
	 */
	public void setCdSiglaUf(String cdSiglaUf) {
		this.cdSiglaUf = cdSiglaUf;
	}

	/**
	 * Get: cdSiglaUfAgencia.
	 *
	 * @return cdSiglaUfAgencia
	 */
	public String getCdSiglaUfAgencia() {
		return cdSiglaUfAgencia;
	}

	/**
	 * Set: cdSiglaUfAgencia.
	 *
	 * @param cdSiglaUfAgencia the cd sigla uf agencia
	 */
	public void setCdSiglaUfAgencia(String cdSiglaUfAgencia) {
		this.cdSiglaUfAgencia = cdSiglaUfAgencia;
	}

	/**
	 * Get: dsAgenciaGestor.
	 *
	 * @return dsAgenciaGestor
	 */
	public String getDsAgenciaGestor() {
		return dsAgenciaGestor;
	}

	/**
	 * Set: dsAgenciaGestor.
	 *
	 * @param dsAgenciaGestor the ds agencia gestor
	 */
	public void setDsAgenciaGestor(String dsAgenciaGestor) {
		this.dsAgenciaGestor = dsAgenciaGestor;
	}

	/**
	 * Get: dsBairroAgencia.
	 *
	 * @return dsBairroAgencia
	 */
	public String getDsBairroAgencia() {
		return dsBairroAgencia;
	}

	/**
	 * Set: dsBairroAgencia.
	 *
	 * @param dsBairroAgencia the ds bairro agencia
	 */
	public void setDsBairroAgencia(String dsBairroAgencia) {
		this.dsBairroAgencia = dsBairroAgencia;
	}

	/**
	 * Get: dsChequeOp.
	 *
	 * @return dsChequeOp
	 */
	public String getDsChequeOp() {
		return dsChequeOp;
	}

	/**
	 * Set: dsChequeOp.
	 *
	 * @param dsChequeOp the ds cheque op
	 */
	public void setDsChequeOp(String dsChequeOp) {
		this.dsChequeOp = dsChequeOp;
	}

	/**
	 * Get: dsCidadeAgencia.
	 *
	 * @return dsCidadeAgencia
	 */
	public String getDsCidadeAgencia() {
		return dsCidadeAgencia;
	}

	/**
	 * Set: dsCidadeAgencia.
	 *
	 * @param dsCidadeAgencia the ds cidade agencia
	 */
	public void setDsCidadeAgencia(String dsCidadeAgencia) {
		this.dsCidadeAgencia = dsCidadeAgencia;
	}

	/**
	 * Get: dsComplementoAgencia.
	 *
	 * @return dsComplementoAgencia
	 */
	public String getDsComplementoAgencia() {
		return dsComplementoAgencia;
	}

	/**
	 * Set: dsComplementoAgencia.
	 *
	 * @param dsComplementoAgencia the ds complemento agencia
	 */
	public void setDsComplementoAgencia(String dsComplementoAgencia) {
		this.dsComplementoAgencia = dsComplementoAgencia;
	}

	/**
	 * Get: dsComplementoEndereco.
	 *
	 * @return dsComplementoEndereco
	 */
	public String getDsComplementoEndereco() {
		return dsComplementoEndereco;
	}

	/**
	 * Set: dsComplementoEndereco.
	 *
	 * @param dsComplementoEndereco the ds complemento endereco
	 */
	public void setDsComplementoEndereco(String dsComplementoEndereco) {
		this.dsComplementoEndereco = dsComplementoEndereco;
	}

	/**
	 * Get: dsCreditoConta.
	 *
	 * @return dsCreditoConta
	 */
	public String getDsCreditoConta() {
		return dsCreditoConta;
	}

	/**
	 * Set: dsCreditoConta.
	 *
	 * @param dsCreditoConta the ds credito conta
	 */
	public void setDsCreditoConta(String dsCreditoConta) {
		this.dsCreditoConta = dsCreditoConta;
	}

	/**
	 * Get: dsDoc.
	 *
	 * @return dsDoc
	 */
	public String getDsDoc() {
		return dsDoc;
	}

	/**
	 * Set: dsDoc.
	 *
	 * @param dsDoc the ds doc
	 */
	public void setDsDoc(String dsDoc) {
		this.dsDoc = dsDoc;
	}

	/**
	 * Get: dsImpressaoComprovante.
	 *
	 * @return dsImpressaoComprovante
	 */
	public String getDsImpressaoComprovante() {
		return dsImpressaoComprovante;
	}

	/**
	 * Set: dsImpressaoComprovante.
	 *
	 * @param dsImpressaoComprovante the ds impressao comprovante
	 */
	public void setDsImpressaoComprovante(String dsImpressaoComprovante) {
		this.dsImpressaoComprovante = dsImpressaoComprovante;
	}

	/**
	 * Get: dsImpressaoFornecedor.
	 *
	 * @return dsImpressaoFornecedor
	 */
	public String getDsImpressaoFornecedor() {
		return dsImpressaoFornecedor;
	}

	/**
	 * Set: dsImpressaoFornecedor.
	 *
	 * @param dsImpressaoFornecedor the ds impressao fornecedor
	 */
	public void setDsImpressaoFornecedor(String dsImpressaoFornecedor) {
		this.dsImpressaoFornecedor = dsImpressaoFornecedor;
	}

	/**
	 * Get: dsImpressaoPtrbEmpresa.
	 *
	 * @return dsImpressaoPtrbEmpresa
	 */
	public String getDsImpressaoPtrbEmpresa() {
		return dsImpressaoPtrbEmpresa;
	}

	/**
	 * Set: dsImpressaoPtrbEmpresa.
	 *
	 * @param dsImpressaoPtrbEmpresa the ds impressao ptrb empresa
	 */
	public void setDsImpressaoPtrbEmpresa(String dsImpressaoPtrbEmpresa) {
		this.dsImpressaoPtrbEmpresa = dsImpressaoPtrbEmpresa;
	}

	/**
	 * Get: dsImpressaoPtrbLine.
	 *
	 * @return dsImpressaoPtrbLine
	 */
	public String getDsImpressaoPtrbLine() {
		return dsImpressaoPtrbLine;
	}

	/**
	 * Set: dsImpressaoPtrbLine.
	 *
	 * @param dsImpressaoPtrbLine the ds impressao ptrb line
	 */
	public void setDsImpressaoPtrbLine(String dsImpressaoPtrbLine) {
		this.dsImpressaoPtrbLine = dsImpressaoPtrbLine;
	}

	/**
	 * Get: dsImpressaoPtrbTributo.
	 *
	 * @return dsImpressaoPtrbTributo
	 */
	public String getDsImpressaoPtrbTributo() {
		return dsImpressaoPtrbTributo;
	}

	/**
	 * Set: dsImpressaoPtrbTributo.
	 *
	 * @param dsImpressaoPtrbTributo the ds impressao ptrb tributo
	 */
	public void setDsImpressaoPtrbTributo(String dsImpressaoPtrbTributo) {
		this.dsImpressaoPtrbTributo = dsImpressaoPtrbTributo;
	}

	/**
	 * Get: dsLogradouroAgencia.
	 *
	 * @return dsLogradouroAgencia
	 */
	public String getDsLogradouroAgencia() {
		return dsLogradouroAgencia;
	}

	/**
	 * Set: dsLogradouroAgencia.
	 *
	 * @param dsLogradouroAgencia the ds logradouro agencia
	 */
	public void setDsLogradouroAgencia(String dsLogradouroAgencia) {
		this.dsLogradouroAgencia = dsLogradouroAgencia;
	}

	/**
	 * Get: dsLogradouroNumero.
	 *
	 * @return dsLogradouroNumero
	 */
	public String getDsLogradouroNumero() {
		return dsLogradouroNumero;
	}

	/**
	 * Set: dsLogradouroNumero.
	 *
	 * @param dsLogradouroNumero the ds logradouro numero
	 */
	public void setDsLogradouroNumero(String dsLogradouroNumero) {
		this.dsLogradouroNumero = dsLogradouroNumero;
	}

	/**
	 * Get: dsTed.
	 *
	 * @return dsTed
	 */
	public String getDsTed() {
		return dsTed;
	}

	/**
	 * Set: dsTed.
	 *
	 * @param dsTed the ds ted
	 */
	public void setDsTed(String dsTed) {
		this.dsTed = dsTed;
	}

	/**
	 * Get: dsTipoRejeicao.
	 *
	 * @return dsTipoRejeicao
	 */
	public String getDsTipoRejeicao() {
		return dsTipoRejeicao;
	}

	/**
	 * Set: dsTipoRejeicao.
	 *
	 * @param dsTipoRejeicao the ds tipo rejeicao
	 */
	public void setDsTipoRejeicao(String dsTipoRejeicao) {
		this.dsTipoRejeicao = dsTipoRejeicao;
	}

	/**
	 * Get: dsTituloBradesco.
	 *
	 * @return dsTituloBradesco
	 */
	public String getDsTituloBradesco() {
		return dsTituloBradesco;
	}

	/**
	 * Set: dsTituloBradesco.
	 *
	 * @param dsTituloBradesco the ds titulo bradesco
	 */
	public void setDsTituloBradesco(String dsTituloBradesco) {
		this.dsTituloBradesco = dsTituloBradesco;
	}

	/**
	 * Get: dsTituloOutros.
	 *
	 * @return dsTituloOutros
	 */
	public String getDsTituloOutros() {
		return dsTituloOutros;
	}

	/**
	 * Set: dsTituloOutros.
	 *
	 * @param dsTituloOutros the ds titulo outros
	 */
	public void setDsTituloOutros(String dsTituloOutros) {
		this.dsTituloOutros = dsTituloOutros;
	}

	/**
	 * Get: nmEmpresa.
	 *
	 * @return nmEmpresa
	 */
	public String getNmEmpresa() {
		return nmEmpresa;
	}

	/**
	 * Set: nmEmpresa.
	 *
	 * @param nmEmpresa the nm empresa
	 */
	public void setNmEmpresa(String nmEmpresa) {
		this.nmEmpresa = nmEmpresa;
	}

	/**
	 * Get: nmParticipante.
	 *
	 * @return nmParticipante
	 */
	public String getNmParticipante() {
		return nmParticipante;
	}

	/**
	 * Set: nmParticipante.
	 *
	 * @param nmParticipante the nm participante
	 */
	public void setNmParticipante(String nmParticipante) {
		this.nmParticipante = nmParticipante;
	}

	/**
	 * Get: nrLogradouroAgencia.
	 *
	 * @return nrLogradouroAgencia
	 */
	public String getNrLogradouroAgencia() {
		return nrLogradouroAgencia;
	}

	/**
	 * Set: nrLogradouroAgencia.
	 *
	 * @param nrLogradouroAgencia the nr logradouro agencia
	 */
	public void setNrLogradouroAgencia(String nrLogradouroAgencia) {
		this.nrLogradouroAgencia = nrLogradouroAgencia;
	}

	/**
	 * Get: qtDiaAviso.
	 *
	 * @return qtDiaAviso
	 */
	public Integer getQtDiaAviso() {
		return qtDiaAviso;
	}

	/**
	 * Set: qtDiaAviso.
	 *
	 * @param qtDiaAviso the qt dia aviso
	 */
	public void setQtDiaAviso(Integer qtDiaAviso) {
		this.qtDiaAviso = qtDiaAviso;
	}

	/**
	 * Get: qtDiaInatividade.
	 *
	 * @return qtDiaInatividade
	 */
	public Integer getQtDiaInatividade() {
		return qtDiaInatividade;
	}

	/**
	 * Set: qtDiaInatividade.
	 *
	 * @param qtDiaInatividade the qt dia inatividade
	 */
	public void setQtDiaInatividade(Integer qtDiaInatividade) {
		this.qtDiaInatividade = qtDiaInatividade;
	}

	/**
	 * Get: qtMesComprovante.
	 *
	 * @return qtMesComprovante
	 */
	public Integer getQtMesComprovante() {
		return qtMesComprovante;
	}

	/**
	 * Set: qtMesComprovante.
	 *
	 * @param qtMesComprovante the qt mes comprovante
	 */
	public void setQtMesComprovante(Integer qtMesComprovante) {
		this.qtMesComprovante = qtMesComprovante;
	}

	/**
	 * Get: vlTarifaFolha.
	 *
	 * @return vlTarifaFolha
	 */
	public BigDecimal getVlTarifaFolha() {
		return vlTarifaFolha;
	}

	/**
	 * Set: vlTarifaFolha.
	 *
	 * @param vlTarifaFolha the vl tarifa folha
	 */
	public void setVlTarifaFolha(BigDecimal vlTarifaFolha) {
		this.vlTarifaFolha = vlTarifaFolha;
	}

	/**
	 * Get: cdCepComplemento.
	 *
	 * @return cdCepComplemento
	 */
	public Integer getCdCepComplemento() {
		return cdCepComplemento;
	}

	/**
	 * Set: cdCepComplemento.
	 *
	 * @param cdCepComplemento the cd cep complemento
	 */
	public void setCdCepComplemento(Integer cdCepComplemento) {
		this.cdCepComplemento = cdCepComplemento;
	}

	/**
	 * Get: dsBairroEndereco.
	 *
	 * @return dsBairroEndereco
	 */
	public String getDsBairroEndereco() {
		return dsBairroEndereco;
	}

	/**
	 * Set: dsBairroEndereco.
	 *
	 * @param dsBairroEndereco the ds bairro endereco
	 */
	public void setDsBairroEndereco(String dsBairroEndereco) {
		this.dsBairroEndereco = dsBairroEndereco;
	}

	/**
	 * Get: dsCidadeEndereco.
	 *
	 * @return dsCidadeEndereco
	 */
	public String getDsCidadeEndereco() {
		return dsCidadeEndereco;
	}

	/**
	 * Set: dsCidadeEndereco.
	 *
	 * @param dsCidadeEndereco the ds cidade endereco
	 */
	public void setDsCidadeEndereco(String dsCidadeEndereco) {
		this.dsCidadeEndereco = dsCidadeEndereco;
	}

	/**
	 * Get: dsLogradouroPessoa.
	 *
	 * @return dsLogradouroPessoa
	 */
	public String getDsLogradouroPessoa() {
		return dsLogradouroPessoa;
	}

	/**
	 * Set: dsLogradouroPessoa.
	 *
	 * @param dsLogradouroPessoa the ds logradouro pessoa
	 */
	public void setDsLogradouroPessoa(String dsLogradouroPessoa) {
		this.dsLogradouroPessoa = dsLogradouroPessoa;
	}

	/**
	 * Set: cdCep.
	 *
	 * @param cdCep the cd cep
	 */
	public void setCdCep(Integer cdCep) {
		this.cdCep = cdCep;
	}

	/**
	 * Get: dsDiaAviso.
	 *
	 * @return dsDiaAviso
	 */
	public String getDsDiaAviso() {
		return dsDiaAviso;
	}

	/**
	 * Set: dsDiaAviso.
	 *
	 * @param dsDiaAviso the ds dia aviso
	 */
	public void setDsDiaAviso(String dsDiaAviso) {
		this.dsDiaAviso = dsDiaAviso;
	}

	/**
	 * Get: dsDiaInatividade.
	 *
	 * @return dsDiaInatividade
	 */
	public String getDsDiaInatividade() {
		return dsDiaInatividade;
	}

	/**
	 * Set: dsDiaInatividade.
	 *
	 * @param dsDiaInatividade the ds dia inatividade
	 */
	public void setDsDiaInatividade(String dsDiaInatividade) {
		this.dsDiaInatividade = dsDiaInatividade;
	}

	/**
	 * Get: dsTarifa.
	 *
	 * @return dsTarifa
	 */
	public String getDsTarifa() {
		return dsTarifa;
	}

	/**
	 * Set: dsTarifa.
	 *
	 * @param dsTarifa the ds tarifa
	 */
	public void setDsTarifa(String dsTarifa) {
		this.dsTarifa = dsTarifa;
	}
}
