package br.com.bradesco.web.pgit.service.business.contacomplementar.bean;

public class ListarContaComplementarEntrada {
	
	private Integer maxOcorrencias = null;
	private Long cdpessoaJuridicaContrato = null;
	private Integer cdTipoContratoNegocio = null;
	private Long nrSequenciaContratoNegocio = null;
	
	private Long cdCorpoCnpjCpf = null;
	private Integer cdFilialCnpjCpf = null;
	private Integer cdControleCnpjCpf = null;
	
	private Integer cdBanco = null;
	private Integer cdAgencia = null;
	private Long cdConta = null;
	private String cdDigitoConta = null;

	public Integer getMaxOcorrencias() {
		return maxOcorrencias;
	}
	public void setMaxOcorrencias(Integer maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	public Long getCdCorpoCnpjCpf() {
		return cdCorpoCnpjCpf;
	}
	public void setCdCorpoCnpjCpf(Long cdCorpoCnpjCpf) {
		this.cdCorpoCnpjCpf = cdCorpoCnpjCpf;
	}
	public Integer getCdFilialCnpjCpf() {
		return cdFilialCnpjCpf;
	}
	public void setCdFilialCnpjCpf(Integer cdFilialCnpjCpf) {
		this.cdFilialCnpjCpf = cdFilialCnpjCpf;
	}
	public Integer getCdControleCnpjCpf() {
		return cdControleCnpjCpf;
	}
	public void setCdControleCnpjCpf(Integer cdControleCnpjCpf) {
		this.cdControleCnpjCpf = cdControleCnpjCpf;
	}
	public Integer getCdBanco() {
		return cdBanco;
	}
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	public Long getCdConta() {
		return cdConta;
	}
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
}