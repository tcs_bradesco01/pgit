/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmoedaeconomica.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdIndicadorEconomico
     */
    private int _cdIndicadorEconomico = 0;

    /**
     * keeps track of state for field: _cdIndicadorEconomico
     */
    private boolean _has_cdIndicadorEconomico;

    /**
     * Field _dsIndicadorEconomico
     */
    private java.lang.String _dsIndicadorEconomico;

    /**
     * Field _dsSiglaEconomico
     */
    private java.lang.String _dsSiglaEconomico;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmoedaeconomica.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorEconomico
     * 
     */
    public void deleteCdIndicadorEconomico()
    {
        this._has_cdIndicadorEconomico= false;
    } //-- void deleteCdIndicadorEconomico() 

    /**
     * Returns the value of field 'cdIndicadorEconomico'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorEconomico'.
     */
    public int getCdIndicadorEconomico()
    {
        return this._cdIndicadorEconomico;
    } //-- int getCdIndicadorEconomico() 

    /**
     * Returns the value of field 'dsIndicadorEconomico'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorEconomico'.
     */
    public java.lang.String getDsIndicadorEconomico()
    {
        return this._dsIndicadorEconomico;
    } //-- java.lang.String getDsIndicadorEconomico() 

    /**
     * Returns the value of field 'dsSiglaEconomico'.
     * 
     * @return String
     * @return the value of field 'dsSiglaEconomico'.
     */
    public java.lang.String getDsSiglaEconomico()
    {
        return this._dsSiglaEconomico;
    } //-- java.lang.String getDsSiglaEconomico() 

    /**
     * Method hasCdIndicadorEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorEconomico()
    {
        return this._has_cdIndicadorEconomico;
    } //-- boolean hasCdIndicadorEconomico() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorEconomico'.
     * 
     * @param cdIndicadorEconomico the value of field
     * 'cdIndicadorEconomico'.
     */
    public void setCdIndicadorEconomico(int cdIndicadorEconomico)
    {
        this._cdIndicadorEconomico = cdIndicadorEconomico;
        this._has_cdIndicadorEconomico = true;
    } //-- void setCdIndicadorEconomico(int) 

    /**
     * Sets the value of field 'dsIndicadorEconomico'.
     * 
     * @param dsIndicadorEconomico the value of field
     * 'dsIndicadorEconomico'.
     */
    public void setDsIndicadorEconomico(java.lang.String dsIndicadorEconomico)
    {
        this._dsIndicadorEconomico = dsIndicadorEconomico;
    } //-- void setDsIndicadorEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsSiglaEconomico'.
     * 
     * @param dsSiglaEconomico the value of field 'dsSiglaEconomico'
     */
    public void setDsSiglaEconomico(java.lang.String dsSiglaEconomico)
    {
        this._dsSiglaEconomico = dsSiglaEconomico;
    } //-- void setDsSiglaEconomico(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmoedaeconomica.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmoedaeconomica.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmoedaeconomica.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmoedaeconomica.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
