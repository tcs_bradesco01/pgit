/*
 * Nome: br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean;


/**
 * Nome: TipoPendenciaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoPendenciaEntradaDTO {
	
    /** Atributo cdTipoPendenciaPagamentoIntegrado. */
    private Long cdTipoPendenciaPagamentoIntegrado;
    
    /** Atributo cdTipoUnidadeOrganizacional. */
    private Integer cdTipoUnidadeOrganizacional;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
    /** Atributo cdUnidadeOrganizacional. */
    private Integer cdUnidadeOrganizacional;
    
    /** Atributo dsPendenciaPagamentoIntegrado. */
    private String dsPendenciaPagamentoIntegrado;
    
    /** Atributo rsResumoPendenciaPagamento. */
    private String rsResumoPendenciaPagamento;
    
    /** Atributo cdTipoBaixaPendencia. */
    private Integer cdTipoBaixaPendencia;
    
    /** Atributo cdIndicadorResponsavelPagamento. */
    private Integer cdIndicadorResponsavelPagamento;
    
    /** Atributo dsEmailRespPgto. */
    private String dsEmailRespPgto;
    
    /** Atributo numeroConsultas. */
    private Integer numeroConsultas;
    
	/**
	 * Get: numeroConsultas.
	 *
	 * @return numeroConsultas
	 */
	public Integer getNumeroConsultas() {
		return numeroConsultas;
	}
	
	/**
	 * Set: numeroConsultas.
	 *
	 * @param numeroConsultas the numero consultas
	 */
	public void setNumeroConsultas(Integer numeroConsultas) {
		this.numeroConsultas = numeroConsultas;
	}
	
	/**
	 * Get: cdIndicadorResponsavelPagamento.
	 *
	 * @return cdIndicadorResponsavelPagamento
	 */
	public Integer getCdIndicadorResponsavelPagamento() {
		return cdIndicadorResponsavelPagamento;
	}
	
	/**
	 * Set: cdIndicadorResponsavelPagamento.
	 *
	 * @param cdIndicadorResponsavelPagamento the cd indicador responsavel pagamento
	 */
	public void setCdIndicadorResponsavelPagamento(
			Integer cdIndicadorResponsavelPagamento) {
		this.cdIndicadorResponsavelPagamento = cdIndicadorResponsavelPagamento;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdTipoBaixaPendencia.
	 *
	 * @return cdTipoBaixaPendencia
	 */
	public Integer getCdTipoBaixaPendencia() {
		return cdTipoBaixaPendencia;
	}
	
	/**
	 * Set: cdTipoBaixaPendencia.
	 *
	 * @param cdTipoBaixaPendencia the cd tipo baixa pendencia
	 */
	public void setCdTipoBaixaPendencia(Integer cdTipoBaixaPendencia) {
		this.cdTipoBaixaPendencia = cdTipoBaixaPendencia;
	}
	
	/**
	 * Get: cdTipoPendenciaPagamentoIntegrado.
	 *
	 * @return cdTipoPendenciaPagamentoIntegrado
	 */
	public Long getCdTipoPendenciaPagamentoIntegrado() {
		return cdTipoPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Set: cdTipoPendenciaPagamentoIntegrado.
	 *
	 * @param cdTipoPendenciaPagamentoIntegrado the cd tipo pendencia pagamento integrado
	 */
	public void setCdTipoPendenciaPagamentoIntegrado(
			Long cdTipoPendenciaPagamentoIntegrado) {
		this.cdTipoPendenciaPagamentoIntegrado = cdTipoPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Get: cdTipoUnidadeOrganizacional.
	 *
	 * @return cdTipoUnidadeOrganizacional
	 */
	public Integer getCdTipoUnidadeOrganizacional() {
		return cdTipoUnidadeOrganizacional;
	}
	
	/**
	 * Set: cdTipoUnidadeOrganizacional.
	 *
	 * @param cdTipoUnidadeOrganizacional the cd tipo unidade organizacional
	 */
	public void setCdTipoUnidadeOrganizacional(Integer cdTipoUnidadeOrganizacional) {
		this.cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
	}
	
	/**
	 * Get: cdUnidadeOrganizacional.
	 *
	 * @return cdUnidadeOrganizacional
	 */
	public Integer getCdUnidadeOrganizacional() {
		return cdUnidadeOrganizacional;
	}
	
	/**
	 * Set: cdUnidadeOrganizacional.
	 *
	 * @param cdUnidadeOrganizacional the cd unidade organizacional
	 */
	public void setCdUnidadeOrganizacional(Integer cdUnidadeOrganizacional) {
		this.cdUnidadeOrganizacional = cdUnidadeOrganizacional;
	}
	
	/**
	 * Get: dsPendenciaPagamentoIntegrado.
	 *
	 * @return dsPendenciaPagamentoIntegrado
	 */
	public String getDsPendenciaPagamentoIntegrado() {
		return dsPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Set: dsPendenciaPagamentoIntegrado.
	 *
	 * @param dsPendenciaPagamentoIntegrado the ds pendencia pagamento integrado
	 */
	public void setDsPendenciaPagamentoIntegrado(
			String dsPendenciaPagamentoIntegrado) {
		this.dsPendenciaPagamentoIntegrado = dsPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Get: rsResumoPendenciaPagamento.
	 *
	 * @return rsResumoPendenciaPagamento
	 */
	public String getRsResumoPendenciaPagamento() {
		return rsResumoPendenciaPagamento;
	}
	
	/**
	 * Set: rsResumoPendenciaPagamento.
	 *
	 * @param rsResumoPendenciaPagamento the rs resumo pendencia pagamento
	 */
	public void setRsResumoPendenciaPagamento(String rsResumoPendenciaPagamento) {
		this.rsResumoPendenciaPagamento = rsResumoPendenciaPagamento;
	}
	
	/**
	 * Get: dsEmailRespPgto.
	 *
	 * @return dsEmailRespPgto
	 */
	public String getDsEmailRespPgto() {
		return dsEmailRespPgto;
	}
	
	/**
	 * Set: dsEmailRespPgto.
	 *
	 * @param dsEmailRespPgto the ds email resp pgto
	 */
	public void setDsEmailRespPgto(String dsEmailRespPgto) {
		this.dsEmailRespPgto = dsEmailRespPgto;
	}
    
	
}
