/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharcanlibpagtosconssemconssaldo.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class DetalharCanLibPagtosConsSemConsSaldoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class DetalharCanLibPagtosConsSemConsSaldoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _nrCnpjCpf
     */
    private long _nrCnpjCpf = 0;

    /**
     * keeps track of state for field: _nrCnpjCpf
     */
    private boolean _has_nrCnpjCpf;

    /**
     * Field _nrFilialcnpjCpf
     */
    private int _nrFilialcnpjCpf = 0;

    /**
     * keeps track of state for field: _nrFilialcnpjCpf
     */
    private boolean _has_nrFilialcnpjCpf;

    /**
     * Field _nrControleCnpjCpf
     */
    private int _nrControleCnpjCpf = 0;

    /**
     * keeps track of state for field: _nrControleCnpjCpf
     */
    private boolean _has_nrControleCnpjCpf;

    /**
     * Field _nrArquivoRemessaPagamento
     */
    private long _nrArquivoRemessaPagamento = 0;

    /**
     * keeps track of state for field: _nrArquivoRemessaPagamento
     */
    private boolean _has_nrArquivoRemessaPagamento;

    /**
     * Field _dtCreditoPagamento
     */
    private java.lang.String _dtCreditoPagamento;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _cdSituacaoOperacaoPagamento
     */
    private int _cdSituacaoOperacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoOperacaoPagamento
     */
    private boolean _has_cdSituacaoOperacaoPagamento;

    /**
     * Field _cdAgendadoPagaoNaoPago
     */
    private int _cdAgendadoPagaoNaoPago = 0;

    /**
     * keeps track of state for field: _cdAgendadoPagaoNaoPago
     */
    private boolean _has_cdAgendadoPagaoNaoPago;

    /**
     * Field _cdPessoaContratoDebito
     */
    private long _cdPessoaContratoDebito = 0;

    /**
     * keeps track of state for field: _cdPessoaContratoDebito
     */
    private boolean _has_cdPessoaContratoDebito;

    /**
     * Field _cdTipoContratoDebito
     */
    private int _cdTipoContratoDebito = 0;

    /**
     * keeps track of state for field: _cdTipoContratoDebito
     */
    private boolean _has_cdTipoContratoDebito;

    /**
     * Field _nrSequenciaContratoDebito
     */
    private long _nrSequenciaContratoDebito = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoDebito
     */
    private boolean _has_nrSequenciaContratoDebito;

    /**
     * Field _cdTituloPgtoRastreado
     */
    private int _cdTituloPgtoRastreado = 0;

    /**
     * keeps track of state for field: _cdTituloPgtoRastreado
     */
    private boolean _has_cdTituloPgtoRastreado;

    /**
     * Field _cdIndicadorAutorizacaoPagador
     */
    private int _cdIndicadorAutorizacaoPagador = 0;

    /**
     * keeps track of state for field: _cdIndicadorAutorizacaoPagado
     */
    private boolean _has_cdIndicadorAutorizacaoPagador;

    /**
     * Field _nrLoteInternoPagamento
     */
    private long _nrLoteInternoPagamento = 0;

    /**
     * keeps track of state for field: _nrLoteInternoPagamento
     */
    private boolean _has_nrLoteInternoPagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public DetalharCanLibPagtosConsSemConsSaldoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcanlibpagtosconssemconssaldo.request.DetalharCanLibPagtosConsSemConsSaldoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdAgendadoPagaoNaoPago
     * 
     */
    public void deleteCdAgendadoPagaoNaoPago()
    {
        this._has_cdAgendadoPagaoNaoPago= false;
    } //-- void deleteCdAgendadoPagaoNaoPago() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdIndicadorAutorizacaoPagador
     * 
     */
    public void deleteCdIndicadorAutorizacaoPagador()
    {
        this._has_cdIndicadorAutorizacaoPagador= false;
    } //-- void deleteCdIndicadorAutorizacaoPagador() 

    /**
     * Method deleteCdPessoaContratoDebito
     * 
     */
    public void deleteCdPessoaContratoDebito()
    {
        this._has_cdPessoaContratoDebito= false;
    } //-- void deleteCdPessoaContratoDebito() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdSituacaoOperacaoPagamento
     * 
     */
    public void deleteCdSituacaoOperacaoPagamento()
    {
        this._has_cdSituacaoOperacaoPagamento= false;
    } //-- void deleteCdSituacaoOperacaoPagamento() 

    /**
     * Method deleteCdTipoContratoDebito
     * 
     */
    public void deleteCdTipoContratoDebito()
    {
        this._has_cdTipoContratoDebito= false;
    } //-- void deleteCdTipoContratoDebito() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTituloPgtoRastreado
     * 
     */
    public void deleteCdTituloPgtoRastreado()
    {
        this._has_cdTituloPgtoRastreado= false;
    } //-- void deleteCdTituloPgtoRastreado() 

    /**
     * Method deleteNrArquivoRemessaPagamento
     * 
     */
    public void deleteNrArquivoRemessaPagamento()
    {
        this._has_nrArquivoRemessaPagamento= false;
    } //-- void deleteNrArquivoRemessaPagamento() 

    /**
     * Method deleteNrCnpjCpf
     * 
     */
    public void deleteNrCnpjCpf()
    {
        this._has_nrCnpjCpf= false;
    } //-- void deleteNrCnpjCpf() 

    /**
     * Method deleteNrControleCnpjCpf
     * 
     */
    public void deleteNrControleCnpjCpf()
    {
        this._has_nrControleCnpjCpf= false;
    } //-- void deleteNrControleCnpjCpf() 

    /**
     * Method deleteNrFilialcnpjCpf
     * 
     */
    public void deleteNrFilialcnpjCpf()
    {
        this._has_nrFilialcnpjCpf= false;
    } //-- void deleteNrFilialcnpjCpf() 

    /**
     * Method deleteNrLoteInternoPagamento
     * 
     */
    public void deleteNrLoteInternoPagamento()
    {
        this._has_nrLoteInternoPagamento= false;
    } //-- void deleteNrLoteInternoPagamento() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoDebito
     * 
     */
    public void deleteNrSequenciaContratoDebito()
    {
        this._has_nrSequenciaContratoDebito= false;
    } //-- void deleteNrSequenciaContratoDebito() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdAgendadoPagaoNaoPago'.
     * 
     * @return int
     * @return the value of field 'cdAgendadoPagaoNaoPago'.
     */
    public int getCdAgendadoPagaoNaoPago()
    {
        return this._cdAgendadoPagaoNaoPago;
    } //-- int getCdAgendadoPagaoNaoPago() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdIndicadorAutorizacaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAutorizacaoPagador'.
     */
    public int getCdIndicadorAutorizacaoPagador()
    {
        return this._cdIndicadorAutorizacaoPagador;
    } //-- int getCdIndicadorAutorizacaoPagador() 

    /**
     * Returns the value of field 'cdPessoaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'cdPessoaContratoDebito'.
     */
    public long getCdPessoaContratoDebito()
    {
        return this._cdPessoaContratoDebito;
    } //-- long getCdPessoaContratoDebito() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoOperacaoPagamento'.
     */
    public int getCdSituacaoOperacaoPagamento()
    {
        return this._cdSituacaoOperacaoPagamento;
    } //-- int getCdSituacaoOperacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoContratoDebito'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoDebito'.
     */
    public int getCdTipoContratoDebito()
    {
        return this._cdTipoContratoDebito;
    } //-- int getCdTipoContratoDebito() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTituloPgtoRastreado'.
     * 
     * @return int
     * @return the value of field 'cdTituloPgtoRastreado'.
     */
    public int getCdTituloPgtoRastreado()
    {
        return this._cdTituloPgtoRastreado;
    } //-- int getCdTituloPgtoRastreado() 

    /**
     * Returns the value of field 'dtCreditoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtCreditoPagamento'.
     */
    public java.lang.String getDtCreditoPagamento()
    {
        return this._dtCreditoPagamento;
    } //-- java.lang.String getDtCreditoPagamento() 

    /**
     * Returns the value of field 'nrArquivoRemessaPagamento'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRemessaPagamento'.
     */
    public long getNrArquivoRemessaPagamento()
    {
        return this._nrArquivoRemessaPagamento;
    } //-- long getNrArquivoRemessaPagamento() 

    /**
     * Returns the value of field 'nrCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'nrCnpjCpf'.
     */
    public long getNrCnpjCpf()
    {
        return this._nrCnpjCpf;
    } //-- long getNrCnpjCpf() 

    /**
     * Returns the value of field 'nrControleCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'nrControleCnpjCpf'.
     */
    public int getNrControleCnpjCpf()
    {
        return this._nrControleCnpjCpf;
    } //-- int getNrControleCnpjCpf() 

    /**
     * Returns the value of field 'nrFilialcnpjCpf'.
     * 
     * @return int
     * @return the value of field 'nrFilialcnpjCpf'.
     */
    public int getNrFilialcnpjCpf()
    {
        return this._nrFilialcnpjCpf;
    } //-- int getNrFilialcnpjCpf() 

    /**
     * Returns the value of field 'nrLoteInternoPagamento'.
     * 
     * @return long
     * @return the value of field 'nrLoteInternoPagamento'.
     */
    public long getNrLoteInternoPagamento()
    {
        return this._nrLoteInternoPagamento;
    } //-- long getNrLoteInternoPagamento() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoDebito'.
     */
    public long getNrSequenciaContratoDebito()
    {
        return this._nrSequenciaContratoDebito;
    } //-- long getNrSequenciaContratoDebito() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdAgendadoPagaoNaoPago
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgendadoPagaoNaoPago()
    {
        return this._has_cdAgendadoPagaoNaoPago;
    } //-- boolean hasCdAgendadoPagaoNaoPago() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdIndicadorAutorizacaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAutorizacaoPagador()
    {
        return this._has_cdIndicadorAutorizacaoPagador;
    } //-- boolean hasCdIndicadorAutorizacaoPagador() 

    /**
     * Method hasCdPessoaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaContratoDebito()
    {
        return this._has_cdPessoaContratoDebito;
    } //-- boolean hasCdPessoaContratoDebito() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdSituacaoOperacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoOperacaoPagamento()
    {
        return this._has_cdSituacaoOperacaoPagamento;
    } //-- boolean hasCdSituacaoOperacaoPagamento() 

    /**
     * Method hasCdTipoContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoDebito()
    {
        return this._has_cdTipoContratoDebito;
    } //-- boolean hasCdTipoContratoDebito() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTituloPgtoRastreado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTituloPgtoRastreado()
    {
        return this._has_cdTituloPgtoRastreado;
    } //-- boolean hasCdTituloPgtoRastreado() 

    /**
     * Method hasNrArquivoRemessaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRemessaPagamento()
    {
        return this._has_nrArquivoRemessaPagamento;
    } //-- boolean hasNrArquivoRemessaPagamento() 

    /**
     * Method hasNrCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrCnpjCpf()
    {
        return this._has_nrCnpjCpf;
    } //-- boolean hasNrCnpjCpf() 

    /**
     * Method hasNrControleCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrControleCnpjCpf()
    {
        return this._has_nrControleCnpjCpf;
    } //-- boolean hasNrControleCnpjCpf() 

    /**
     * Method hasNrFilialcnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrFilialcnpjCpf()
    {
        return this._has_nrFilialcnpjCpf;
    } //-- boolean hasNrFilialcnpjCpf() 

    /**
     * Method hasNrLoteInternoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrLoteInternoPagamento()
    {
        return this._has_nrLoteInternoPagamento;
    } //-- boolean hasNrLoteInternoPagamento() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoDebito()
    {
        return this._has_nrSequenciaContratoDebito;
    } //-- boolean hasNrSequenciaContratoDebito() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdAgendadoPagaoNaoPago'.
     * 
     * @param cdAgendadoPagaoNaoPago the value of field
     * 'cdAgendadoPagaoNaoPago'.
     */
    public void setCdAgendadoPagaoNaoPago(int cdAgendadoPagaoNaoPago)
    {
        this._cdAgendadoPagaoNaoPago = cdAgendadoPagaoNaoPago;
        this._has_cdAgendadoPagaoNaoPago = true;
    } //-- void setCdAgendadoPagaoNaoPago(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorAutorizacaoPagador'.
     * 
     * @param cdIndicadorAutorizacaoPagador the value of field
     * 'cdIndicadorAutorizacaoPagador'.
     */
    public void setCdIndicadorAutorizacaoPagador(int cdIndicadorAutorizacaoPagador)
    {
        this._cdIndicadorAutorizacaoPagador = cdIndicadorAutorizacaoPagador;
        this._has_cdIndicadorAutorizacaoPagador = true;
    } //-- void setCdIndicadorAutorizacaoPagador(int) 

    /**
     * Sets the value of field 'cdPessoaContratoDebito'.
     * 
     * @param cdPessoaContratoDebito the value of field
     * 'cdPessoaContratoDebito'.
     */
    public void setCdPessoaContratoDebito(long cdPessoaContratoDebito)
    {
        this._cdPessoaContratoDebito = cdPessoaContratoDebito;
        this._has_cdPessoaContratoDebito = true;
    } //-- void setCdPessoaContratoDebito(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdSituacaoOperacaoPagamento'.
     * 
     * @param cdSituacaoOperacaoPagamento the value of field
     * 'cdSituacaoOperacaoPagamento'.
     */
    public void setCdSituacaoOperacaoPagamento(int cdSituacaoOperacaoPagamento)
    {
        this._cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
        this._has_cdSituacaoOperacaoPagamento = true;
    } //-- void setCdSituacaoOperacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoContratoDebito'.
     * 
     * @param cdTipoContratoDebito the value of field
     * 'cdTipoContratoDebito'.
     */
    public void setCdTipoContratoDebito(int cdTipoContratoDebito)
    {
        this._cdTipoContratoDebito = cdTipoContratoDebito;
        this._has_cdTipoContratoDebito = true;
    } //-- void setCdTipoContratoDebito(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTituloPgtoRastreado'.
     * 
     * @param cdTituloPgtoRastreado the value of field
     * 'cdTituloPgtoRastreado'.
     */
    public void setCdTituloPgtoRastreado(int cdTituloPgtoRastreado)
    {
        this._cdTituloPgtoRastreado = cdTituloPgtoRastreado;
        this._has_cdTituloPgtoRastreado = true;
    } //-- void setCdTituloPgtoRastreado(int) 

    /**
     * Sets the value of field 'dtCreditoPagamento'.
     * 
     * @param dtCreditoPagamento the value of field
     * 'dtCreditoPagamento'.
     */
    public void setDtCreditoPagamento(java.lang.String dtCreditoPagamento)
    {
        this._dtCreditoPagamento = dtCreditoPagamento;
    } //-- void setDtCreditoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRemessaPagamento'.
     * 
     * @param nrArquivoRemessaPagamento the value of field
     * 'nrArquivoRemessaPagamento'.
     */
    public void setNrArquivoRemessaPagamento(long nrArquivoRemessaPagamento)
    {
        this._nrArquivoRemessaPagamento = nrArquivoRemessaPagamento;
        this._has_nrArquivoRemessaPagamento = true;
    } //-- void setNrArquivoRemessaPagamento(long) 

    /**
     * Sets the value of field 'nrCnpjCpf'.
     * 
     * @param nrCnpjCpf the value of field 'nrCnpjCpf'.
     */
    public void setNrCnpjCpf(long nrCnpjCpf)
    {
        this._nrCnpjCpf = nrCnpjCpf;
        this._has_nrCnpjCpf = true;
    } //-- void setNrCnpjCpf(long) 

    /**
     * Sets the value of field 'nrControleCnpjCpf'.
     * 
     * @param nrControleCnpjCpf the value of field
     * 'nrControleCnpjCpf'.
     */
    public void setNrControleCnpjCpf(int nrControleCnpjCpf)
    {
        this._nrControleCnpjCpf = nrControleCnpjCpf;
        this._has_nrControleCnpjCpf = true;
    } //-- void setNrControleCnpjCpf(int) 

    /**
     * Sets the value of field 'nrFilialcnpjCpf'.
     * 
     * @param nrFilialcnpjCpf the value of field 'nrFilialcnpjCpf'.
     */
    public void setNrFilialcnpjCpf(int nrFilialcnpjCpf)
    {
        this._nrFilialcnpjCpf = nrFilialcnpjCpf;
        this._has_nrFilialcnpjCpf = true;
    } //-- void setNrFilialcnpjCpf(int) 

    /**
     * Sets the value of field 'nrLoteInternoPagamento'.
     * 
     * @param nrLoteInternoPagamento the value of field
     * 'nrLoteInternoPagamento'.
     */
    public void setNrLoteInternoPagamento(long nrLoteInternoPagamento)
    {
        this._nrLoteInternoPagamento = nrLoteInternoPagamento;
        this._has_nrLoteInternoPagamento = true;
    } //-- void setNrLoteInternoPagamento(long) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoDebito'.
     * 
     * @param nrSequenciaContratoDebito the value of field
     * 'nrSequenciaContratoDebito'.
     */
    public void setNrSequenciaContratoDebito(long nrSequenciaContratoDebito)
    {
        this._nrSequenciaContratoDebito = nrSequenciaContratoDebito;
        this._has_nrSequenciaContratoDebito = true;
    } //-- void setNrSequenciaContratoDebito(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return DetalharCanLibPagtosConsSemConsSaldoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharcanlibpagtosconssemconssaldo.request.DetalharCanLibPagtosConsSemConsSaldoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharcanlibpagtosconssemconssaldo.request.DetalharCanLibPagtosConsSemConsSaldoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharcanlibpagtosconssemconssaldo.request.DetalharCanLibPagtosConsSemConsSaldoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharcanlibpagtosconssemconssaldo.request.DetalharCanLibPagtosConsSemConsSaldoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
