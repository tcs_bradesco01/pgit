/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.AlterarPerfilContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.AlterarPerfilContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharHistAssocTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharHistAssocTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharVincTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharVincTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ExcluirVincTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ExcluirVincTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirPagamentoSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirPagamentoSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincContMaeFilhoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincContMaeFilhoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarAssocTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarAssocTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarContratoMaeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarContratoMaeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarHistAssocTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarHistAssocTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarParticipantesSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterVincPerfilTrocaArqPart
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterVincPerfilTrocaArqPartService {

    /**
     * Listar participantes.
     *
     * @param listarParticipantesEntradaDTO the listar participantes entrada dto
     * @return the list< listar participantes saida dt o>
     */
    List<ListarParticipantesSaidaDTO> listarParticipantes (ListarParticipantesEntradaDTO listarParticipantesEntradaDTO);
    
    /**
     * Listar participante.
     *
     * @param entrada the entrada
     * @return the list< listar participante saida dt o>
     */
    List<ListarParticipanteSaidaDTO> listarParticipante(ListarParticipanteEntradaDTO entrada);
    
    /**
     * Listar assoc troca arq participante.
     *
     * @param entrada the entrada
     * @return the list< listar assoc troca arq participante saida dt o>
     */
    List<ListarAssocTrocaArqParticipanteSaidaDTO> listarAssocTrocaArqParticipante(ListarAssocTrocaArqParticipanteEntradaDTO entrada);

    /**
     * Listar hist assoc troca arq participante.
     *
     * @param entrada the entrada
     * @return the list< listar hist assoc troca arq participante saida dt o>
     */
    List<ListarHistAssocTrocaArqParticipanteSaidaDTO> listarHistAssocTrocaArqParticipante(ListarHistAssocTrocaArqParticipanteEntradaDTO entrada);
    
    /**
     * Detalhar hist assoc troca arq participante.
     *
     * @param entrada the entrada
     * @return the detalhar hist assoc troca arq participante saida dto
     */
    DetalharHistAssocTrocaArqParticipanteSaidaDTO detalharHistAssocTrocaArqParticipante(DetalharHistAssocTrocaArqParticipanteEntradaDTO entrada);
    
    /**
     * Incluir vinc troca arq participante.
     *
     * @param entrada the entrada
     * @return the incluir vinc troca arq participante saida dto
     */
    IncluirVincTrocaArqParticipanteSaidaDTO incluirVincTrocaArqParticipante(IncluirVincTrocaArqParticipanteEntradaDTO entrada);
    
    /**
     * Excluir vinc troca arq participante.
     *
     * @param entrada the entrada
     * @return the excluir vinc troca arq participante saida dto
     */
    ExcluirVincTrocaArqParticipanteSaidaDTO excluirVincTrocaArqParticipante(ExcluirVincTrocaArqParticipanteEntradaDTO entrada);

    /**
     * Detalhar vinc troca arq participante.
     *
     * @param entrada the entrada
     * @return the detalhar vinc troca arq participante saida dto
     */
    DetalharVincTrocaArqParticipanteSaidaDTO detalharVincTrocaArqParticipante(DetalharVincTrocaArqParticipanteEntradaDTO entrada);
    
    /**
     * Alterar perfil contrato.
     *
     * @param entrada the entrada
     * @return the alterar perfil contrato saida dto
     */
    AlterarPerfilContratoSaidaDTO alterarPerfilContrato(AlterarPerfilContratoEntradaDTO entrada);

    /**
     * Alterar perfil contrato.
     *
     * @param entrada the entrada
     * @return the alterar perfil contrato saida dto
     */
    IncluirPagamentoSalarioSaidaDTO incluirPagamentoSalario(IncluirPagamentoSalarioEntradaDTO entrada);


    /**
     * Listar Contrato Mae.
     *
     * @param entrada the entrada
     * @return the listar contrato mae saida dto
     */
    ListarContratoMaeSaidaDTO listarContratoMae(ListarContratoMaeEntradaDTO entrada);
    
    /**
     * Incluir vinc contrato mae contrato filho. 
     *
     * @param entrada the entrada
     * @return the Incluir vinc contrato mae contrato filho saida dto
     */
    IncluirVincContMaeFilhoSaidaDTO incluirVincContratoMaeContratoFilho(IncluirVincContMaeFilhoEntradaDTO entrada);    

}

