/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarparticipantescontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdClub
     */
    private long _cdClub = 0;

    /**
     * keeps track of state for field: _cdClub
     */
    private boolean _has_cdClub;

    /**
     * Field _cdCorpoCpfCnpj
     */
    private long _cdCorpoCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpj
     */
    private boolean _has_cdCorpoCpfCnpj;

    /**
     * Field _cdCnpjContrato
     */
    private int _cdCnpjContrato = 0;

    /**
     * keeps track of state for field: _cdCnpjContrato
     */
    private boolean _has_cdCnpjContrato;

    /**
     * Field _cdCpfCnpjDigito
     */
    private int _cdCpfCnpjDigito = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjDigito
     */
    private boolean _has_cdCpfCnpjDigito;

    /**
     * Field _cdRazaoSocial
     */
    private java.lang.String _cdRazaoSocial;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _dsTipoParticipante
     */
    private java.lang.String _dsTipoParticipante;

    /**
     * Field _cdDescricaoSituacaoParticapante
     */
    private java.lang.String _cdDescricaoSituacaoParticapante;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparticipantescontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClub
     * 
     */
    public void deleteCdClub()
    {
        this._has_cdClub= false;
    } //-- void deleteCdClub() 

    /**
     * Method deleteCdCnpjContrato
     * 
     */
    public void deleteCdCnpjContrato()
    {
        this._has_cdCnpjContrato= false;
    } //-- void deleteCdCnpjContrato() 

    /**
     * Method deleteCdCorpoCpfCnpj
     * 
     */
    public void deleteCdCorpoCpfCnpj()
    {
        this._has_cdCorpoCpfCnpj= false;
    } //-- void deleteCdCorpoCpfCnpj() 

    /**
     * Method deleteCdCpfCnpjDigito
     * 
     */
    public void deleteCdCpfCnpjDigito()
    {
        this._has_cdCpfCnpjDigito= false;
    } //-- void deleteCdCpfCnpjDigito() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdClub'.
     * 
     * @return long
     * @return the value of field 'cdClub'.
     */
    public long getCdClub()
    {
        return this._cdClub;
    } //-- long getCdClub() 

    /**
     * Returns the value of field 'cdCnpjContrato'.
     * 
     * @return int
     * @return the value of field 'cdCnpjContrato'.
     */
    public int getCdCnpjContrato()
    {
        return this._cdCnpjContrato;
    } //-- int getCdCnpjContrato() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpj'.
     */
    public long getCdCorpoCpfCnpj()
    {
        return this._cdCorpoCpfCnpj;
    } //-- long getCdCorpoCpfCnpj() 

    /**
     * Returns the value of field 'cdCpfCnpjDigito'.
     * 
     * @return int
     * @return the value of field 'cdCpfCnpjDigito'.
     */
    public int getCdCpfCnpjDigito()
    {
        return this._cdCpfCnpjDigito;
    } //-- int getCdCpfCnpjDigito() 

    /**
     * Returns the value of field
     * 'cdDescricaoSituacaoParticapante'.
     * 
     * @return String
     * @return the value of field 'cdDescricaoSituacaoParticapante'.
     */
    public java.lang.String getCdDescricaoSituacaoParticapante()
    {
        return this._cdDescricaoSituacaoParticapante;
    } //-- java.lang.String getCdDescricaoSituacaoParticapante() 

    /**
     * Returns the value of field 'cdRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'cdRazaoSocial'.
     */
    public java.lang.String getCdRazaoSocial()
    {
        return this._cdRazaoSocial;
    } //-- java.lang.String getCdRazaoSocial() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dsTipoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipante'.
     */
    public java.lang.String getDsTipoParticipante()
    {
        return this._dsTipoParticipante;
    } //-- java.lang.String getDsTipoParticipante() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdClub
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClub()
    {
        return this._has_cdClub;
    } //-- boolean hasCdClub() 

    /**
     * Method hasCdCnpjContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjContrato()
    {
        return this._has_cdCnpjContrato;
    } //-- boolean hasCdCnpjContrato() 

    /**
     * Method hasCdCorpoCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpj()
    {
        return this._has_cdCorpoCpfCnpj;
    } //-- boolean hasCdCorpoCpfCnpj() 

    /**
     * Method hasCdCpfCnpjDigito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjDigito()
    {
        return this._has_cdCpfCnpjDigito;
    } //-- boolean hasCdCpfCnpjDigito() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClub'.
     * 
     * @param cdClub the value of field 'cdClub'.
     */
    public void setCdClub(long cdClub)
    {
        this._cdClub = cdClub;
        this._has_cdClub = true;
    } //-- void setCdClub(long) 

    /**
     * Sets the value of field 'cdCnpjContrato'.
     * 
     * @param cdCnpjContrato the value of field 'cdCnpjContrato'.
     */
    public void setCdCnpjContrato(int cdCnpjContrato)
    {
        this._cdCnpjContrato = cdCnpjContrato;
        this._has_cdCnpjContrato = true;
    } //-- void setCdCnpjContrato(int) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpj'.
     * 
     * @param cdCorpoCpfCnpj the value of field 'cdCorpoCpfCnpj'.
     */
    public void setCdCorpoCpfCnpj(long cdCorpoCpfCnpj)
    {
        this._cdCorpoCpfCnpj = cdCorpoCpfCnpj;
        this._has_cdCorpoCpfCnpj = true;
    } //-- void setCdCorpoCpfCnpj(long) 

    /**
     * Sets the value of field 'cdCpfCnpjDigito'.
     * 
     * @param cdCpfCnpjDigito the value of field 'cdCpfCnpjDigito'.
     */
    public void setCdCpfCnpjDigito(int cdCpfCnpjDigito)
    {
        this._cdCpfCnpjDigito = cdCpfCnpjDigito;
        this._has_cdCpfCnpjDigito = true;
    } //-- void setCdCpfCnpjDigito(int) 

    /**
     * Sets the value of field 'cdDescricaoSituacaoParticapante'.
     * 
     * @param cdDescricaoSituacaoParticapante the value of field
     * 'cdDescricaoSituacaoParticapante'.
     */
    public void setCdDescricaoSituacaoParticapante(java.lang.String cdDescricaoSituacaoParticapante)
    {
        this._cdDescricaoSituacaoParticapante = cdDescricaoSituacaoParticapante;
    } //-- void setCdDescricaoSituacaoParticapante(java.lang.String) 

    /**
     * Sets the value of field 'cdRazaoSocial'.
     * 
     * @param cdRazaoSocial the value of field 'cdRazaoSocial'.
     */
    public void setCdRazaoSocial(java.lang.String cdRazaoSocial)
    {
        this._cdRazaoSocial = cdRazaoSocial;
    } //-- void setCdRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'dsTipoParticipante'.
     * 
     * @param dsTipoParticipante the value of field
     * 'dsTipoParticipante'.
     */
    public void setDsTipoParticipante(java.lang.String dsTipoParticipante)
    {
        this._dsTipoParticipante = dsTipoParticipante;
    } //-- void setDsTipoParticipante(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarparticipantescontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarparticipantescontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarparticipantescontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparticipantescontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
