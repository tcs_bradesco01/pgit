/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarservicosemissao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProdutoOperacao
     */
    private int _cdProdutoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacao
     */
    private boolean _has_cdProdutoOperacao;

    /**
     * Field _cdProdutoRelacionado
     */
    private int _cdProdutoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoRelacionado
     */
    private boolean _has_cdProdutoRelacionado;

    /**
     * Field _cdRelacionamento
     */
    private int _cdRelacionamento = 0;

    /**
     * keeps track of state for field: _cdRelacionamento
     */
    private boolean _has_cdRelacionamento;

    /**
     * Field _dsServico
     */
    private java.lang.String _dsServico;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarservicosemissao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdProdutoOperacao
     * 
     */
    public void deleteCdProdutoOperacao()
    {
        this._has_cdProdutoOperacao= false;
    } //-- void deleteCdProdutoOperacao() 

    /**
     * Method deleteCdProdutoRelacionado
     * 
     */
    public void deleteCdProdutoRelacionado()
    {
        this._has_cdProdutoRelacionado= false;
    } //-- void deleteCdProdutoRelacionado() 

    /**
     * Method deleteCdRelacionamento
     * 
     */
    public void deleteCdRelacionamento()
    {
        this._has_cdRelacionamento= false;
    } //-- void deleteCdRelacionamento() 

    /**
     * Returns the value of field 'cdProdutoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacao'.
     */
    public int getCdProdutoOperacao()
    {
        return this._cdProdutoOperacao;
    } //-- int getCdProdutoOperacao() 

    /**
     * Returns the value of field 'cdProdutoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoRelacionado'.
     */
    public int getCdProdutoRelacionado()
    {
        return this._cdProdutoRelacionado;
    } //-- int getCdProdutoRelacionado() 

    /**
     * Returns the value of field 'cdRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamento'.
     */
    public int getCdRelacionamento()
    {
        return this._cdRelacionamento;
    } //-- int getCdRelacionamento() 

    /**
     * Returns the value of field 'dsServico'.
     * 
     * @return String
     * @return the value of field 'dsServico'.
     */
    public java.lang.String getDsServico()
    {
        return this._dsServico;
    } //-- java.lang.String getDsServico() 

    /**
     * Method hasCdProdutoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacao()
    {
        return this._has_cdProdutoOperacao;
    } //-- boolean hasCdProdutoOperacao() 

    /**
     * Method hasCdProdutoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoRelacionado()
    {
        return this._has_cdProdutoRelacionado;
    } //-- boolean hasCdProdutoRelacionado() 

    /**
     * Method hasCdRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamento()
    {
        return this._has_cdRelacionamento;
    } //-- boolean hasCdRelacionamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProdutoOperacao'.
     * 
     * @param cdProdutoOperacao the value of field
     * 'cdProdutoOperacao'.
     */
    public void setCdProdutoOperacao(int cdProdutoOperacao)
    {
        this._cdProdutoOperacao = cdProdutoOperacao;
        this._has_cdProdutoOperacao = true;
    } //-- void setCdProdutoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoRelacionado'.
     * 
     * @param cdProdutoRelacionado the value of field
     * 'cdProdutoRelacionado'.
     */
    public void setCdProdutoRelacionado(int cdProdutoRelacionado)
    {
        this._cdProdutoRelacionado = cdProdutoRelacionado;
        this._has_cdProdutoRelacionado = true;
    } //-- void setCdProdutoRelacionado(int) 

    /**
     * Sets the value of field 'cdRelacionamento'.
     * 
     * @param cdRelacionamento the value of field 'cdRelacionamento'
     */
    public void setCdRelacionamento(int cdRelacionamento)
    {
        this._cdRelacionamento = cdRelacionamento;
        this._has_cdRelacionamento = true;
    } //-- void setCdRelacionamento(int) 

    /**
     * Sets the value of field 'dsServico'.
     * 
     * @param dsServico the value of field 'dsServico'.
     */
    public void setDsServico(java.lang.String dsServico)
    {
        this._dsServico = dsServico;
    } //-- void setDsServico(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarservicosemissao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarservicosemissao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarservicosemissao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarservicosemissao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
