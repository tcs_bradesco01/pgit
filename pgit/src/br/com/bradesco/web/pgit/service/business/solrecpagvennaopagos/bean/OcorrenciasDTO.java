/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean;

/**
 * Nome: OcorrenciasDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasDTO {
   
   /** Atributo cdPessoaJuridicaContrato. */
   private Long cdPessoaJuridicaContrato;
   
   /** Atributo cdTipoContratoNegocio. */
   private Integer cdTipoContratoNegocio;
   
   /** Atributo nrSequenciaContratoNegocio. */
   private Long nrSequenciaContratoNegocio;
   
   /** Atributo cdControlePagamento. */
   private String cdControlePagamento;
   
   /** Atributo cdModalidadePagamento. */
   private Integer cdModalidadePagamento;
   
   /** Atributo dsEfetivacaoPagamento. */
   private String dsEfetivacaoPagamento ;
   
	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}
	
	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}
	
	/**
	 * Get: cdModalidadePagamento.
	 *
	 * @return cdModalidadePagamento
	 */
	public Integer getCdModalidadePagamento() {
		return cdModalidadePagamento;
	}
	
	/**
	 * Set: cdModalidadePagamento.
	 *
	 * @param cdModalidadePagamento the cd modalidade pagamento
	 */
	public void setCdModalidadePagamento(Integer cdModalidadePagamento) {
		this.cdModalidadePagamento = cdModalidadePagamento;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: dsEfetivacaoPagamento.
	 *
	 * @return dsEfetivacaoPagamento
	 */
	public String getDsEfetivacaoPagamento() {
		return dsEfetivacaoPagamento;
	}
	
	/**
	 * Set: dsEfetivacaoPagamento.
	 *
	 * @param dsEfetivacaoPagamento the ds efetivacao pagamento
	 */
	public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
		this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
	}
	
   
}
