/*
 * Nome: br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean;

/**
 * Nome: ListarAssociacaoContratoMsgSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarAssociacaoContratoMsgSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo codMensagemSalarial. */
	private int codMensagemSalarial;
	
	/** Atributo dsMensagemSalarial. */
	private String dsMensagemSalarial;
	
	/** Atributo contratoPssoaJurdContr. */
	private long contratoPssoaJurdContr;
	
	/** Atributo contratoTpoContrNegoc. */
	private int contratoTpoContrNegoc;
	
	/** Atributo contratoSeqContrNegoc. */
	private long contratoSeqContrNegoc;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: codMensagemSalarial.
	 *
	 * @return codMensagemSalarial
	 */
	public int getCodMensagemSalarial() {
		return codMensagemSalarial;
	}
	
	/**
	 * Set: codMensagemSalarial.
	 *
	 * @param codMensagemSalarial the cod mensagem salarial
	 */
	public void setCodMensagemSalarial(int codMensagemSalarial) {
		this.codMensagemSalarial = codMensagemSalarial;
	}
	
	/**
	 * Get: dsMensagemSalarial.
	 *
	 * @return dsMensagemSalarial
	 */
	public String getDsMensagemSalarial() {
		return dsMensagemSalarial;
	}
	
	/**
	 * Set: dsMensagemSalarial.
	 *
	 * @param dsMensagemSalarial the ds mensagem salarial
	 */
	public void setDsMensagemSalarial(String dsMensagemSalarial) {
		this.dsMensagemSalarial = dsMensagemSalarial;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: contratoPssoaJurdContr.
	 *
	 * @return contratoPssoaJurdContr
	 */
	public long getContratoPssoaJurdContr() {
		return contratoPssoaJurdContr;
	}

	/**
	 * Set: contratoPssoaJurdContr.
	 *
	 * @param contratoPssoaJurdContr the contrato pssoa jurd contr
	 */
	public void setContratoPssoaJurdContr(long contratoPssoaJurdContr) {
		this.contratoPssoaJurdContr = contratoPssoaJurdContr;
	}

	/**
	 * Get: contratoSeqContrNegoc.
	 *
	 * @return contratoSeqContrNegoc
	 */
	public long getContratoSeqContrNegoc() {
		return contratoSeqContrNegoc;
	}

	/**
	 * Set: contratoSeqContrNegoc.
	 *
	 * @param contratoSeqContrNegoc the contrato seq contr negoc
	 */
	public void setContratoSeqContrNegoc(long contratoSeqContrNegoc) {
		this.contratoSeqContrNegoc = contratoSeqContrNegoc;
	}

	/**
	 * Get: contratoTpoContrNegoc.
	 *
	 * @return contratoTpoContrNegoc
	 */
	public int getContratoTpoContrNegoc() {
		return contratoTpoContrNegoc;
	}

	/**
	 * Set: contratoTpoContrNegoc.
	 *
	 * @param contratoTpoContrNegoc the contrato tpo contr negoc
	 */
	public void setContratoTpoContrNegoc(int contratoTpoContrNegoc) {
		this.contratoTpoContrNegoc = contratoTpoContrNegoc;
	}
	
	
	

}


