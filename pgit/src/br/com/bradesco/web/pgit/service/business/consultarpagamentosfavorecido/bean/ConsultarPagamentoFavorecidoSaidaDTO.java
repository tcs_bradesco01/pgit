/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.bean;

import static br.com.bradesco.web.pgit.utils.PgitUtil.formatBancoAgenciaConta;

import java.math.BigDecimal;

/**
 * Nome: ConsultarPagamentoFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagamentoFavorecidoSaidaDTO {
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo corpoCfpCnpj. */
    private Long corpoCfpCnpj;
    
    /** Atributo filialCfpCnpj. */
    private Integer filialCfpCnpj;
    
    /** Atributo controleCfpCnpj. */
    private Integer controleCfpCnpj;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo nmRazaoSocial. */
    private String nmRazaoSocial;
    
    /** Atributo cdEmpresa. */
    private Long cdEmpresa;
    
    /** Atributo dsEmpresa. */
    private String dsEmpresa;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrContrato. */
    private Long nrContrato;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdResumoProdutoServico. */
    private String cdResumoProdutoServico;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo dsOperacaoProdutoServico. */
    private String dsOperacaoProdutoServico;
    
    /** Atributo cdControlePagamento. */
    private String cdControlePagamento;
    
    /** Atributo dtCraditoPagamento. */
    private String dtCraditoPagamento;
    
    /** Atributo vlrEfetivacaoPagamentoCliente. */
    private BigDecimal vlrEfetivacaoPagamentoCliente;
    
    /** Atributo cdInscricaoFavorecido. */
    private Long cdInscricaoFavorecido;
    
    /** Atributo dsAbrevFavorecido. */
    private String dsAbrevFavorecido;
    
    /** Atributo cdBancoDebito. */
    private Integer cdBancoDebito;
    
    /** Atributo cdAgenciaDebito. */
    private Integer cdAgenciaDebito;
    
    /** Atributo cdDigitoAgenciaDebito. */
    private Integer cdDigitoAgenciaDebito;
    
    /** Atributo cdContaDebito. */
    private Long cdContaDebito;
    
    /** Atributo cdDigitoContaDebito. */
    private String cdDigitoContaDebito;
    
    /** Atributo cdBancoCredito. */
    private Integer cdBancoCredito;
    
    /** Atributo cdAgenciaCredito. */
    private Integer cdAgenciaCredito;
    
    /** Atributo cdDigitoAgenciaCredito. */
    private Integer cdDigitoAgenciaCredito;
    
    /** Atributo cdContaCredito. */
    private Long cdContaCredito;
    
    /** Atributo cdDigitoContaCredito. */
    private String cdDigitoContaCredito;
    
    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento;
    
    /** Atributo cdMotivoSituacaoPagamento. */
    private String cdMotivoSituacaoPagamento;
    
    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;
    
    /** Atributo dsTipoTela. */
    private Integer dsTipoTela;
    
    /** Atributo dsEfetivacaoPagamento. */
    private String dsEfetivacaoPagamento;
    
    /** Atributo cpfCnpjFormatado. */
    private String cpfCnpjFormatado;   
    
    /** Atributo favorecidoBeneficiarioFormatado. */
    private String favorecidoBeneficiarioFormatado;
    
    /** Atributo bancoAgenciaContaCreditoFormatado. */
    private String bancoAgenciaContaCreditoFormatado;
    
    /** Atributo bancoAgenciaContaDebitoFormatado. */
    private String bancoAgenciaContaDebitoFormatado;
    
    /** Atributo dtFormatada. */
    private String dtFormatada;    
    
    /** Atributo vlPagamentoFormatado. */
    private String vlPagamentoFormatado;
    
    /** Atributo bancoDebitoFormatado. */
    private String bancoDebitoFormatado;
    
    /** Atributo agenciaDebitoFormatada. */
    private String agenciaDebitoFormatada;
    
    /** Atributo contaDebitoFormatada. */
    private String contaDebitoFormatada;
    
    /** Atributo bancoCreditoFormatado. */
    private String bancoCreditoFormatado;
    
    /** Atributo agenciaCreditoFormatada. */
    private String agenciaCreditoFormatada;
    
    /** Atributo contaCreditoFormatada. */
    private String contaCreditoFormatada;  
    
    /** Atributo numControleInternoLote. */
    private Long numControleInternoLote;
    
    /** Atributo dsIndicadorPagamento. */
    private String dsIndicadorPagamento;
    
	/** The cd banco salarial. */
	private Integer cdBancoSalarial;
	
	/** The cd agencia salarial. */
	private Integer cdAgenciaSalarial;
	
	/** The cd digito agencia salarial. */
	private Integer cdDigitoAgenciaSalarial;
	
	/** The cd conta salarial. */
	private Long cdContaSalarial;
	
	/** The cd digito conta salarial. */
	private String cdDigitoContaSalarial;
	
	private String cdIspbPagtoDestino;
	
	private String contaPagtoDestino;
	
	public String getBancoAgenciaContaSalarioFormatado(){
    	return  formatBancoAgenciaConta(getCdBancoSalarial(),getCdAgenciaSalarial(), getCdDigitoAgenciaSalarial(), 
    			getCdContaSalarial(), getCdDigitoContaSalarial(), true);
	}
    
	/**
	 * Get: vlPagamentoFormatado.
	 *
	 * @return vlPagamentoFormatado
	 */
	public String getVlPagamentoFormatado() {
		return vlPagamentoFormatado;
	}
	
	/**
	 * Set: vlPagamentoFormatado.
	 *
	 * @param vlPagamentoFormatado the vl pagamento formatado
	 */
	public void setVlPagamentoFormatado(String vlPagamentoFormatado) {
		this.vlPagamentoFormatado = vlPagamentoFormatado;
	}
	
	/**
	 * Get: dtFormatada.
	 *
	 * @return dtFormatada
	 */
	public String getDtFormatada() {
		return dtFormatada;
	}
	
	/**
	 * Set: dtFormatada.
	 *
	 * @param dtFormatada the dt formatada
	 */
	public void setDtFormatada(String dtFormatada) {
		this.dtFormatada = dtFormatada;
	}
	
	/**
	 * Get: bancoAgenciaContaCreditoFormatado.
	 *
	 * @return bancoAgenciaContaCreditoFormatado
	 */
	public String getBancoAgenciaContaCreditoFormatado() {
		return bancoAgenciaContaCreditoFormatado;
	}
	
	/**
	 * Set: bancoAgenciaContaCreditoFormatado.
	 *
	 * @param bancoAgenciaContaCreditoFormatado the banco agencia conta credito formatado
	 */
	public void setBancoAgenciaContaCreditoFormatado(
			String bancoAgenciaContaCreditoFormatado) {
		this.bancoAgenciaContaCreditoFormatado = bancoAgenciaContaCreditoFormatado;
	}
	
	/**
	 * Get: bancoAgenciaContaDebitoFormatado.
	 *
	 * @return bancoAgenciaContaDebitoFormatado
	 */
	public String getBancoAgenciaContaDebitoFormatado() {
		return bancoAgenciaContaDebitoFormatado;
	}
	
	/**
	 * Set: bancoAgenciaContaDebitoFormatado.
	 *
	 * @param bancoAgenciaContaDebitoFormatado the banco agencia conta debito formatado
	 */
	public void setBancoAgenciaContaDebitoFormatado(
			String bancoAgenciaContaDebitoFormatado) {
		this.bancoAgenciaContaDebitoFormatado = bancoAgenciaContaDebitoFormatado;
	}
	
	/**
	 * Get: favorecidoBeneficiarioFormatado.
	 *
	 * @return favorecidoBeneficiarioFormatado
	 */
	public String getFavorecidoBeneficiarioFormatado() {
		return favorecidoBeneficiarioFormatado;
	}
	
	/**
	 * Set: favorecidoBeneficiarioFormatado.
	 *
	 * @param favorecidoBeneficiarioFormatado the favorecido beneficiario formatado
	 */
	public void setFavorecidoBeneficiarioFormatado(
			String favorecidoBeneficiarioFormatado) {
		this.favorecidoBeneficiarioFormatado = favorecidoBeneficiarioFormatado;
	}
	
	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return cpfCnpjFormatado;
	}
	
	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}
	
	/**
	 * Get: cdAgenciaCredito.
	 *
	 * @return cdAgenciaCredito
	 */
	public Integer getCdAgenciaCredito() {
		return cdAgenciaCredito;
	}
	
	/**
	 * Set: cdAgenciaCredito.
	 *
	 * @param cdAgenciaCredito the cd agencia credito
	 */
	public void setCdAgenciaCredito(Integer cdAgenciaCredito) {
		this.cdAgenciaCredito = cdAgenciaCredito;
	}
	
	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}
	
	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}
	
	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public Integer getCdBancoCredito() {
		return cdBancoCredito;
	}
	
	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(Integer cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}
	
	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}
	
	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}
	
	/**
	 * Get: cdContaCredito.
	 *
	 * @return cdContaCredito
	 */
	public Long getCdContaCredito() {
		return cdContaCredito;
	}
	
	/**
	 * Set: cdContaCredito.
	 *
	 * @param cdContaCredito the cd conta credito
	 */
	public void setCdContaCredito(Long cdContaCredito) {
		this.cdContaCredito = cdContaCredito;
	}
	
	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}
	
	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}
	
	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}
	
	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}
	
	/**
	 * Get: cdDigitoAgenciaCredito.
	 *
	 * @return cdDigitoAgenciaCredito
	 */
	public Integer getCdDigitoAgenciaCredito() {
		return cdDigitoAgenciaCredito;
	}
	
	/**
	 * Set: cdDigitoAgenciaCredito.
	 *
	 * @param cdDigitoAgenciaCredito the cd digito agencia credito
	 */
	public void setCdDigitoAgenciaCredito(Integer cdDigitoAgenciaCredito) {
		this.cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
	}
	
	/**
	 * Get: cdDigitoAgenciaDebito.
	 *
	 * @return cdDigitoAgenciaDebito
	 */
	public Integer getCdDigitoAgenciaDebito() {
		return cdDigitoAgenciaDebito;
	}
	
	/**
	 * Set: cdDigitoAgenciaDebito.
	 *
	 * @param cdDigitoAgenciaDebito the cd digito agencia debito
	 */
	public void setCdDigitoAgenciaDebito(Integer cdDigitoAgenciaDebito) {
		this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
	}
	
	/**
	 * Get: cdDigitoContaCredito.
	 *
	 * @return cdDigitoContaCredito
	 */
	public String getCdDigitoContaCredito() {
		return cdDigitoContaCredito;
	}
	
	/**
	 * Set: cdDigitoContaCredito.
	 *
	 * @param cdDigitoContaCredito the cd digito conta credito
	 */
	public void setCdDigitoContaCredito(String cdDigitoContaCredito) {
		this.cdDigitoContaCredito = cdDigitoContaCredito;
	}
	
	/**
	 * Get: cdDigitoContaDebito.
	 *
	 * @return cdDigitoContaDebito
	 */
	public String getCdDigitoContaDebito() {
		return cdDigitoContaDebito;
	}
	
	/**
	 * Set: cdDigitoContaDebito.
	 *
	 * @param cdDigitoContaDebito the cd digito conta debito
	 */
	public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
		this.cdDigitoContaDebito = cdDigitoContaDebito;
	}
	
	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}
	
	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}
	
	/**
	 * Get: cdInscricaoFavorecido.
	 *
	 * @return cdInscricaoFavorecido
	 */
	public Long getCdInscricaoFavorecido() {
		return cdInscricaoFavorecido;
	}
	
	/**
	 * Set: cdInscricaoFavorecido.
	 *
	 * @param cdInscricaoFavorecido the cd inscricao favorecido
	 */
	public void setCdInscricaoFavorecido(Long cdInscricaoFavorecido) {
		this.cdInscricaoFavorecido = cdInscricaoFavorecido;
	}
	
	/**
	 * Get: cdMotivoSituacaoPagamento.
	 *
	 * @return cdMotivoSituacaoPagamento
	 */
	public String getCdMotivoSituacaoPagamento() {
		return cdMotivoSituacaoPagamento;
	}
	
	/**
	 * Set: cdMotivoSituacaoPagamento.
	 *
	 * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(String cdMotivoSituacaoPagamento) {
		this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdResumoProdutoServico.
	 *
	 * @return cdResumoProdutoServico
	 */
	public String getCdResumoProdutoServico() {
		return cdResumoProdutoServico;
	}
	
	/**
	 * Set: cdResumoProdutoServico.
	 *
	 * @param cdResumoProdutoServico the cd resumo produto servico
	 */
	public void setCdResumoProdutoServico(String cdResumoProdutoServico) {
		this.cdResumoProdutoServico = cdResumoProdutoServico;
	}
	
	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}
	
	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: controleCfpCnpj.
	 *
	 * @return controleCfpCnpj
	 */
	public Integer getControleCfpCnpj() {
		return controleCfpCnpj;
	}
	
	/**
	 * Set: controleCfpCnpj.
	 *
	 * @param controleCfpCnpj the controle cfp cnpj
	 */
	public void setControleCfpCnpj(Integer controleCfpCnpj) {
		this.controleCfpCnpj = controleCfpCnpj;
	}
	
	/**
	 * Get: corpoCfpCnpj.
	 *
	 * @return corpoCfpCnpj
	 */
	public Long getCorpoCfpCnpj() {
		return corpoCfpCnpj;
	}
	
	/**
	 * Set: corpoCfpCnpj.
	 *
	 * @param corpoCfpCnpj the corpo cfp cnpj
	 */
	public void setCorpoCfpCnpj(Long corpoCfpCnpj) {
		this.corpoCfpCnpj = corpoCfpCnpj;
	}
	
	/**
	 * Get: dsAbrevFavorecido.
	 *
	 * @return dsAbrevFavorecido
	 */
	public String getDsAbrevFavorecido() {
		return dsAbrevFavorecido;
	}
	
	/**
	 * Set: dsAbrevFavorecido.
	 *
	 * @param dsAbrevFavorecido the ds abrev favorecido
	 */
	public void setDsAbrevFavorecido(String dsAbrevFavorecido) {
		this.dsAbrevFavorecido = dsAbrevFavorecido;
	}
	
	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}
	
	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}
	
	/**
	 * Get: dsOperacaoProdutoServico.
	 *
	 * @return dsOperacaoProdutoServico
	 */
	public String getDsOperacaoProdutoServico() {
		return dsOperacaoProdutoServico;
	}
	
	/**
	 * Set: dsOperacaoProdutoServico.
	 *
	 * @param dsOperacaoProdutoServico the ds operacao produto servico
	 */
	public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
		this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
	}
	
	/**
	 * Get: dsTipoTela.
	 *
	 * @return dsTipoTela
	 */
	public Integer getDsTipoTela() {
		return dsTipoTela;
	}
	
	/**
	 * Set: dsTipoTela.
	 *
	 * @param dsTipoTela the ds tipo tela
	 */
	public void setDsTipoTela(Integer dsTipoTela) {
		this.dsTipoTela = dsTipoTela;
	}
	
	/**
	 * Get: dtCraditoPagamento.
	 *
	 * @return dtCraditoPagamento
	 */
	public String getDtCraditoPagamento() {
		return dtCraditoPagamento;
	}
	
	/**
	 * Set: dtCraditoPagamento.
	 *
	 * @param dtCraditoPagamento the dt cradito pagamento
	 */
	public void setDtCraditoPagamento(String dtCraditoPagamento) {
		this.dtCraditoPagamento = dtCraditoPagamento;
	}
	
	/**
	 * Get: filialCfpCnpj.
	 *
	 * @return filialCfpCnpj
	 */
	public Integer getFilialCfpCnpj() {
		return filialCfpCnpj;
	}
	
	/**
	 * Set: filialCfpCnpj.
	 *
	 * @param filialCfpCnpj the filial cfp cnpj
	 */
	public void setFilialCfpCnpj(Integer filialCfpCnpj) {
		this.filialCfpCnpj = filialCfpCnpj;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nmRazaoSocial.
	 *
	 * @return nmRazaoSocial
	 */
	public String getNmRazaoSocial() {
		return nmRazaoSocial;
	}
	
	/**
	 * Set: nmRazaoSocial.
	 *
	 * @param nmRazaoSocial the nm razao social
	 */
	public void setNmRazaoSocial(String nmRazaoSocial) {
		this.nmRazaoSocial = nmRazaoSocial;
	}
	
	/**
	 * Get: nrContrato.
	 *
	 * @return nrContrato
	 */
	public Long getNrContrato() {
		return nrContrato;
	}
	
	/**
	 * Set: nrContrato.
	 *
	 * @param nrContrato the nr contrato
	 */
	public void setNrContrato(Long nrContrato) {
		this.nrContrato = nrContrato;
	}
	
	/**
	 * Get: vlrEfetivacaoPagamentoCliente.
	 *
	 * @return vlrEfetivacaoPagamentoCliente
	 */
	public BigDecimal getVlrEfetivacaoPagamentoCliente() {
		return vlrEfetivacaoPagamentoCliente;
	}
	
	/**
	 * Set: vlrEfetivacaoPagamentoCliente.
	 *
	 * @param vlrEfetivacaoPagamentoCliente the vlr efetivacao pagamento cliente
	 */
	public void setVlrEfetivacaoPagamentoCliente(
			BigDecimal vlrEfetivacaoPagamentoCliente) {
		this.vlrEfetivacaoPagamentoCliente = vlrEfetivacaoPagamentoCliente;
	}
	
	/**
	 * Get: agenciaCreditoFormatada.
	 *
	 * @return agenciaCreditoFormatada
	 */
	public String getAgenciaCreditoFormatada() {
		return agenciaCreditoFormatada;
	}
	
	/**
	 * Set: agenciaCreditoFormatada.
	 *
	 * @param agenciaCreditoFormatada the agencia credito formatada
	 */
	public void setAgenciaCreditoFormatada(String agenciaCreditoFormatada) {
		this.agenciaCreditoFormatada = agenciaCreditoFormatada;
	}
	
	/**
	 * Get: agenciaDebitoFormatada.
	 *
	 * @return agenciaDebitoFormatada
	 */
	public String getAgenciaDebitoFormatada() {
		return agenciaDebitoFormatada;
	}
	
	/**
	 * Set: agenciaDebitoFormatada.
	 *
	 * @param agenciaDebitoFormatada the agencia debito formatada
	 */
	public void setAgenciaDebitoFormatada(String agenciaDebitoFormatada) {
		this.agenciaDebitoFormatada = agenciaDebitoFormatada;
	}
	
	/**
	 * Get: bancoCreditoFormatado.
	 *
	 * @return bancoCreditoFormatado
	 */
	public String getBancoCreditoFormatado() {
		return bancoCreditoFormatado;
	}
	
	/**
	 * Set: bancoCreditoFormatado.
	 *
	 * @param bancoCreditoFormatado the banco credito formatado
	 */
	public void setBancoCreditoFormatado(String bancoCreditoFormatado) {
		this.bancoCreditoFormatado = bancoCreditoFormatado;
	}
	
	/**
	 * Get: bancoDebitoFormatado.
	 *
	 * @return bancoDebitoFormatado
	 */
	public String getBancoDebitoFormatado() {
		return bancoDebitoFormatado;
	}
	
	/**
	 * Set: bancoDebitoFormatado.
	 *
	 * @param bancoDebitoFormatado the banco debito formatado
	 */
	public void setBancoDebitoFormatado(String bancoDebitoFormatado) {
		this.bancoDebitoFormatado = bancoDebitoFormatado;
	}
	
	/**
	 * Get: contaCreditoFormatada.
	 *
	 * @return contaCreditoFormatada
	 */
	public String getContaCreditoFormatada() {
		return contaCreditoFormatada;
	}
	
	/**
	 * Set: contaCreditoFormatada.
	 *
	 * @param contaCreditoFormatada the conta credito formatada
	 */
	public void setContaCreditoFormatada(String contaCreditoFormatada) {
		this.contaCreditoFormatada = contaCreditoFormatada;
	}
	
	/**
	 * Get: contaDebitoFormatada.
	 *
	 * @return contaDebitoFormatada
	 */
	public String getContaDebitoFormatada() {
		return contaDebitoFormatada;
	}
	
	/**
	 * Set: contaDebitoFormatada.
	 *
	 * @param contaDebitoFormatada the conta debito formatada
	 */
	public void setContaDebitoFormatada(String contaDebitoFormatada) {
		this.contaDebitoFormatada = contaDebitoFormatada;
	}
	
	/**
	 * Get: dsEfetivacaoPagamento.
	 *
	 * @return dsEfetivacaoPagamento
	 */
	public String getDsEfetivacaoPagamento() {
		return dsEfetivacaoPagamento;
	}
	
	/**
	 * Set: dsEfetivacaoPagamento.
	 *
	 * @param dsEfetivacaoPagamento the ds efetivacao pagamento
	 */
	public void setDsEfetivacaoPagamento(String dsEfetivacaoPagamento) {
		this.dsEfetivacaoPagamento = dsEfetivacaoPagamento;
	}
	
	/**
	 * Get: numControleInternoLote.
	 *
	 * @return numControleInternoLote
	 */
	public Long getNumControleInternoLote() {
		return numControleInternoLote;
	}
	
	/**
	 * Set: numControleInternoLote.
	 *
	 * @param numControleInternoLote the num controle interno lote
	 */
	public void setNumControleInternoLote(Long numControleInternoLote) {
		this.numControleInternoLote = numControleInternoLote;
	}

    /**
     * Nome: getDsIndicadorPagamento
     *
     * @return dsIndicadorPagamento
     */
    public String getDsIndicadorPagamento() {
        return dsIndicadorPagamento;
    }

    /**
     * Nome: setDsIndicadorPagamento
     *
     * @param dsIndicadorPagamento
     */
    public void setDsIndicadorPagamento(String dsIndicadorPagamento) {
        this.dsIndicadorPagamento = dsIndicadorPagamento;
    }

	/**
	 * @return the cdBancoSalarial
	 */
	public Integer getCdBancoSalarial() {
		return cdBancoSalarial;
	}

	/**
	 * @param cdBancoSalarial the cdBancoSalarial to set
	 */
	public void setCdBancoSalarial(Integer cdBancoSalarial) {
		this.cdBancoSalarial = cdBancoSalarial;
	}

	/**
	 * @return the cdAgenciaSalarial
	 */
	public Integer getCdAgenciaSalarial() {
		return cdAgenciaSalarial;
	}

	/**
	 * @param cdAgenciaSalarial the cdAgenciaSalarial to set
	 */
	public void setCdAgenciaSalarial(Integer cdAgenciaSalarial) {
		this.cdAgenciaSalarial = cdAgenciaSalarial;
	}

	/**
	 * @return the cdDigitoAgenciaSalarial
	 */
	public Integer getCdDigitoAgenciaSalarial() {
		return cdDigitoAgenciaSalarial;
	}

	/**
	 * @param cdDigitoAgenciaSalarial the cdDigitoAgenciaSalarial to set
	 */
	public void setCdDigitoAgenciaSalarial(Integer cdDigitoAgenciaSalarial) {
		this.cdDigitoAgenciaSalarial = cdDigitoAgenciaSalarial;
	}

	/**
	 * @return the cdContaSalarial
	 */
	public Long getCdContaSalarial() {
		return cdContaSalarial;
	}

	/**
	 * @param cdContaSalarial the cdContaSalarial to set
	 */
	public void setCdContaSalarial(Long cdContaSalarial) {
		this.cdContaSalarial = cdContaSalarial;
	}

	/**
	 * @return the cdDigitoContaSalarial
	 */
	public String getCdDigitoContaSalarial() {
		return cdDigitoContaSalarial;
	}

	/**
	 * @param cdDigitoContaSalarial the cdDigitoContaSalarial to set
	 */
	public void setCdDigitoContaSalarial(String cdDigitoContaSalarial) {
		this.cdDigitoContaSalarial = cdDigitoContaSalarial;
	}

	public String getCdIspbPagtoDestino() {
		return cdIspbPagtoDestino;
	}

	public void setCdIspbPagtoDestino(String cdIspbPagtoDestino) {
		this.cdIspbPagtoDestino = cdIspbPagtoDestino;
	}

	public String getContaPagtoDestino() {
		return contaPagtoDestino;
	}

	public void setContaPagtoDestino(String contaPagtoDestino) {
		this.contaPagtoDestino = contaPagtoDestino;
	}
	
}