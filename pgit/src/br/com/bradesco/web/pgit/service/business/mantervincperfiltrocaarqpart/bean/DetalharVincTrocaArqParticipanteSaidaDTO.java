/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: DetalharVincTrocaArqParticipanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharVincTrocaArqParticipanteSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsCodigoTipoLayout. */
	private String dsCodigoTipoLayout;
	
	/** Atributo cdPerfilTrocaArquivo. */
	private Long cdPerfilTrocaArquivo;
	
	/** Atributo cdCorpoCpfCnpj. */
	private Long cdCorpoCpfCnpj;
	
	/** Atributo cdCnpjContrato. */
	private String cdCnpjContrato;
	
	/** Atributo cdCpfCnpjDigito. */
	private Integer cdCpfCnpjDigito;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo cdTipoParticipante. */
	private String cdTipoParticipante;
	
	/** Atributo dsTipoParticipante. */
	private String dsTipoParticipante;
	
	/** Atributo cdSituacaoParticipante. */
	private Long cdSituacaoParticipante;
	
	/** Atributo cdDescricaoSituacaoParticapante. */
	private String cdDescricaoSituacaoParticapante;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo cdClub. */
	private String cdClub;
	
	/** Atributo cdCanal. */
	private String cdCanal;
	
	/** Atributo dsCanal. */
	private String dsCanal;
	
	/** Atributo cdOperacaoCanalIncusao. */
	private String cdOperacaoCanalIncusao;
	
	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;
	
	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;
	
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;
	
	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdOperacaoCanalManutencao. */
	private String cdOperacaoCanalManutencao;
	
	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;
	
	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;
	
	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;
	
	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo cpfFormatado. */
	private String cpfFormatado;
	
	/** Atributo canalInclusaoFormatado. */
	private String canalInclusaoFormatado;
	
	/** Atributo canalManutencaoFormatado. */
	private String canalManutencaoFormatado;
	
	/** Atributo dsCanalMantencao. */
	private String dsCanalMantencao;
	
	/** Atributo cdTipoLayoutMae. */
	private Integer cdTipoLayoutMae;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo cdPerfilTrocaMae. */
	private Long cdPerfilTrocaMae;
	
	/** Atributo cdPessoaJuridicaMae. */
	private Long cdPessoaJuridicaMae;
	
	/** Atributo cdTipoContratoMae. */
	private Integer cdTipoContratoMae;
	
	/** Atributo nrSequencialControtaoMae. */
	private Long nrSequencialContratoMae;
	
	/** Atributo cdTipoParticipanteMae. */
	private Integer cdTipoParticipanteMae;
	
	/** Atributo cdCpessoaMae. */
	private Long cdCpessoaMae;
	
	/** Atributo cdCpfCnpj. */
	private Long cdCpfCnpj;
	
	/** Atributo cdFilialCnpj. */
	private Integer cdFilialCnpj;
	
	/** Atributo cdControleCnpj. */
	private Integer cdControleCnpj;
	
	/** Atributo dsNomeRazao. */
	private String dsNomeRazao;
	
	public String getCpfCnpjFormatadoContratoMae(){
		return CpfCnpjUtils.formatarCpfCnpj(cdCpfCnpj, cdFilialCnpj, cdControleCnpj);
	}
	
	/**
	 * Get: cpfFormatado.
	 *
	 * @return cpfFormatado
	 */
	public String getCpfFormatado() {
		return cpfFormatado;
	}

	/**
	 * Set: cpfFormatado.
	 *
	 * @param cpfFormatado the cpf formatado
	 */
	public void setCpfFormatado(String cpfFormatado) {
		this.cpfFormatado = cpfFormatado;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo(){
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo){
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: dsCodigoTipoLayout.
	 *
	 * @return dsCodigoTipoLayout
	 */
	public String getDsCodigoTipoLayout(){
		return dsCodigoTipoLayout;
	}

	/**
	 * Set: dsCodigoTipoLayout.
	 *
	 * @param dsCodigoTipoLayout the ds codigo tipo layout
	 */
	public void setDsCodigoTipoLayout(String dsCodigoTipoLayout){
		this.dsCodigoTipoLayout = dsCodigoTipoLayout;
	}

	/**
	 * Get: cdPerfilTrocaArquivo.
	 *
	 * @return cdPerfilTrocaArquivo
	 */
	public Long getCdPerfilTrocaArquivo(){
		return cdPerfilTrocaArquivo;
	}

	/**
	 * Set: cdPerfilTrocaArquivo.
	 *
	 * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
	 */
	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo){
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj(){
		return cdCorpoCpfCnpj;
	}

	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj){
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}

	/**
	 * Get: cdCnpjContrato.
	 *
	 * @return cdCnpjContrato
	 */
	public String getCdCnpjContrato(){
		return cdCnpjContrato;
	}

	/**
	 * Set: cdCnpjContrato.
	 *
	 * @param cdCnpjContrato the cd cnpj contrato
	 */
	public void setCdCnpjContrato(String cdCnpjContrato){
		this.cdCnpjContrato = cdCnpjContrato;
	}

	/**
	 * Get: cdCpfCnpjDigito.
	 *
	 * @return cdCpfCnpjDigito
	 */
	public Integer getCdCpfCnpjDigito(){
		return cdCpfCnpjDigito;
	}

	/**
	 * Set: cdCpfCnpjDigito.
	 *
	 * @param cdCpfCnpjDigito the cd cpf cnpj digito
	 */
	public void setCdCpfCnpjDigito(Integer cdCpfCnpjDigito){
		this.cdCpfCnpjDigito = cdCpfCnpjDigito;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial(){
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial){
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: cdTipoParticipante.
	 *
	 * @return cdTipoParticipante
	 */
	public String getCdTipoParticipante(){
		return cdTipoParticipante;
	}

	/**
	 * Set: cdTipoParticipante.
	 *
	 * @param cdTipoParticipante the cd tipo participante
	 */
	public void setCdTipoParticipante(String cdTipoParticipante){
		this.cdTipoParticipante = cdTipoParticipante;
	}

	/**
	 * Get: dsTipoParticipante.
	 *
	 * @return dsTipoParticipante
	 */
	public String getDsTipoParticipante(){
		return dsTipoParticipante;
	}

	/**
	 * Set: dsTipoParticipante.
	 *
	 * @param dsTipoParticipante the ds tipo participante
	 */
	public void setDsTipoParticipante(String dsTipoParticipante){
		this.dsTipoParticipante = dsTipoParticipante;
	}

	/**
	 * Get: cdSituacaoParticipante.
	 *
	 * @return cdSituacaoParticipante
	 */
	public Long getCdSituacaoParticipante(){
		return cdSituacaoParticipante;
	}

	/**
	 * Set: cdSituacaoParticipante.
	 *
	 * @param cdSituacaoParticipante the cd situacao participante
	 */
	public void setCdSituacaoParticipante(Long cdSituacaoParticipante){
		this.cdSituacaoParticipante = cdSituacaoParticipante;
	}

	/**
	 * Get: cdDescricaoSituacaoParticapante.
	 *
	 * @return cdDescricaoSituacaoParticapante
	 */
	public String getCdDescricaoSituacaoParticapante(){
		return cdDescricaoSituacaoParticapante;
	}

	/**
	 * Set: cdDescricaoSituacaoParticapante.
	 *
	 * @param cdDescricaoSituacaoParticapante the cd descricao situacao particapante
	 */
	public void setCdDescricaoSituacaoParticapante(String cdDescricaoSituacaoParticapante){
		this.cdDescricaoSituacaoParticapante = cdDescricaoSituacaoParticapante;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio(){
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio){
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio(){
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio){
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa(){
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa){
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public String getCdClub(){
		return cdClub;
	}

	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(String cdClub){
		this.cdClub = cdClub;
	}

	/**
	 * Get: cdCanal.
	 *
	 * @return cdCanal
	 */
	public String getCdCanal(){
		return cdCanal;
	}

	/**
	 * Set: cdCanal.
	 *
	 * @param cdCanal the cd canal
	 */
	public void setCdCanal(String cdCanal){
		this.cdCanal = cdCanal;
	}

	/**
	 * Get: dsCanal.
	 *
	 * @return dsCanal
	 */
	public String getDsCanal(){
		return dsCanal;
	}

	/**
	 * Set: dsCanal.
	 *
	 * @param dsCanal the ds canal
	 */
	public void setDsCanal(String dsCanal){
		this.dsCanal = dsCanal;
	}

	/**
	 * Get: cdOperacaoCanalIncusao.
	 *
	 * @return cdOperacaoCanalIncusao
	 */
	public String getCdOperacaoCanalIncusao(){
		return cdOperacaoCanalIncusao;
	}

	/**
	 * Set: cdOperacaoCanalIncusao.
	 *
	 * @param cdOperacaoCanalIncusao the cd operacao canal incusao
	 */
	public void setCdOperacaoCanalIncusao(String cdOperacaoCanalIncusao){
		this.cdOperacaoCanalIncusao = cdOperacaoCanalIncusao;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao(){
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao){
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao(){
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao){
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno(){
		return cdUsuarioInclusaoExterno;
	}

	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno){
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro(){
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro){
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao(){
		return cdOperacaoCanalManutencao;
	}

	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao){
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao(){
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao){
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno(){
		return cdUsuarioManutencaoExterno;
	}

	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno){
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro(){
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro){
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}

	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}

	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}

	/**
	 * Get: canalInclusaoFormatado.
	 *
	 * @return canalInclusaoFormatado
	 */
	public String getCanalInclusaoFormatado() {
		return canalInclusaoFormatado;
	}

	/**
	 * Set: canalInclusaoFormatado.
	 *
	 * @param canalInclusaoFormatado the canal inclusao formatado
	 */
	public void setCanalInclusaoFormatado(String canalInclusaoFormatado) {
		this.canalInclusaoFormatado = canalInclusaoFormatado;
	}

	/**
	 * Get: canalManutencaoFormatado.
	 *
	 * @return canalManutencaoFormatado
	 */
	public String getCanalManutencaoFormatado() {
		return canalManutencaoFormatado;
	}

	/**
	 * Set: canalManutencaoFormatado.
	 *
	 * @param canalManutencaoFormatado the canal manutencao formatado
	 */
	public void setCanalManutencaoFormatado(String canalManutencaoFormatado) {
		this.canalManutencaoFormatado = canalManutencaoFormatado;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * @return the dsCanalMantencao
	 */
	public String getDsCanalMantencao() {
		return dsCanalMantencao;
	}

	/**
	 * @param dsCanalMantencao the dsCanalMantencao to set
	 */
	public void setDsCanalMantencao(String dsCanalMantencao) {
		this.dsCanalMantencao = dsCanalMantencao;
	}

	/**
	 * @return the cdTipoLayoutMae
	 */
	public Integer getCdTipoLayoutMae() {
		return cdTipoLayoutMae;
	}

	/**
	 * @param cdTipoLayoutMae the cdTipoLayoutMae to set
	 */
	public void setCdTipoLayoutMae(Integer cdTipoLayoutMae) {
		this.cdTipoLayoutMae = cdTipoLayoutMae;
	}

	/**
	 * @return the dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * @param dsTipoLayoutArquivo the dsTipoLayoutArquivo to set
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * @return the cdPerfilTrocaMae
	 */
	public Long getCdPerfilTrocaMae() {
		return cdPerfilTrocaMae;
	}

	/**
	 * @param cdPerfilTrocaMae the cdPerfilTrocaMae to set
	 */
	public void setCdPerfilTrocaMae(Long cdPerfilTrocaMae) {
		this.cdPerfilTrocaMae = cdPerfilTrocaMae;
	}

	/**
	 * @return the cdPessoaJuridicaMae
	 */
	public Long getCdPessoaJuridicaMae() {
		return cdPessoaJuridicaMae;
	}

	/**
	 * @param cdPessoaJuridicaMae the cdPessoaJuridicaMae to set
	 */
	public void setCdPessoaJuridicaMae(Long cdPessoaJuridicaMae) {
		this.cdPessoaJuridicaMae = cdPessoaJuridicaMae;
	}

	/**
	 * @return the cdTipoContratoMae
	 */
	public Integer getCdTipoContratoMae() {
		return cdTipoContratoMae;
	}

	/**
	 * @param cdTipoContratoMae the cdTipoContratoMae to set
	 */
	public void setCdTipoContratoMae(Integer cdTipoContratoMae) {
		this.cdTipoContratoMae = cdTipoContratoMae;
	}

	/**
	 * @return the nrSequencialControtaoMae
	 */
	public Long getNrSequencialControtaoMae() {
		return nrSequencialContratoMae;
	}

	/**
	 * @param nrSequencialControtaoMae the nrSequencialControtaoMae to set
	 */
	public void setNrSequencialContratoMae(Long nrSequencialContratoMae) {
		this.nrSequencialContratoMae = nrSequencialContratoMae;
	}

	/**
	 * @return the cdTipoParticipanteMae
	 */
	public Integer getCdTipoParticipanteMae() {
		return cdTipoParticipanteMae;
	}

	/**
	 * @param cdTipoParticipanteMae the cdTipoParticipanteMae to set
	 */
	public void setCdTipoParticipanteMae(Integer cdTipoParticipanteMae) {
		this.cdTipoParticipanteMae = cdTipoParticipanteMae;
	}

	/**
	 * @return the cdCpessoaMae
	 */
	public Long getCdCpessoaMae() {
		return cdCpessoaMae;
	}

	/**
	 * @param cdCpessoaMae the cdCpessoaMae to set
	 */
	public void setCdCpessoaMae(Long cdCpessoaMae) {
		this.cdCpessoaMae = cdCpessoaMae;
	}

	/**
	 * @return the cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}

	/**
	 * @param cdCpfCnpj the cdCpfCnpj to set
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}

	/**
	 * @return the cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}

	/**
	 * @param cdFilialCnpj the cdFilialCnpj to set
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}

	/**
	 * @return the cdControleCnpj
	 */
	public Integer getCdControleCnpj() {
		return cdControleCnpj;
	}

	/**
	 * @param cdControleCnpj the cdControleCnpj to set
	 */
	public void setCdControleCnpj(Integer cdControleCnpj) {
		this.cdControleCnpj = cdControleCnpj;
	}

	/**
	 * @return the dsNomeRazao
	 */
	public String getDsNomeRazao() {
		return dsNomeRazao;
	}

	/**
	 * @param dsNomeRazao the dsNomeRazao to set
	 */
	public void setDsNomeRazao(String dsNomeRazao) {
		this.dsNomeRazao = dsNomeRazao;
	}
	
	
}