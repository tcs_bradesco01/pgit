/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarTipoArquivoRetornoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarTipoArquivoRetornoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdTipoLayout
     */
    private int _cdTipoLayout = 0;

    /**
     * keeps track of state for field: _cdTipoLayout
     */
    private boolean _has_cdTipoLayout;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarTipoArquivoRetornoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.request.ListarTipoArquivoRetornoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoLayout
     * 
     */
    public void deleteCdTipoLayout()
    {
        this._has_cdTipoLayout= false;
    } //-- void deleteCdTipoLayout() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdTipoLayout'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayout'.
     */
    public int getCdTipoLayout()
    {
        return this._cdTipoLayout;
    } //-- int getCdTipoLayout() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdTipoLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayout()
    {
        return this._has_cdTipoLayout;
    } //-- boolean hasCdTipoLayout() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoLayout'.
     * 
     * @param cdTipoLayout the value of field 'cdTipoLayout'.
     */
    public void setCdTipoLayout(int cdTipoLayout)
    {
        this._cdTipoLayout = cdTipoLayout;
        this._has_cdTipoLayout = true;
    } //-- void setCdTipoLayout(int) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarTipoArquivoRetornoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.request.ListarTipoArquivoRetornoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.request.ListarTipoArquivoRetornoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.request.ListarTipoArquivoRetornoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipoarquivoretorno.request.ListarTipoArquivoRetornoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
