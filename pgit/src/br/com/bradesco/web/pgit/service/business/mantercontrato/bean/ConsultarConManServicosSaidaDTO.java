/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarConManServicosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarConManServicosSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;
	
	private String dsIndicador;
	
	private int dsOrigemIndicador;

	/** Atributo cdPessoaJuridicaContrato. */
	private long cdPessoaJuridicaContrato;

	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;

	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;

	/** Atributo cdProdutoServicoOperacao. */
	private int cdProdutoServicoOperacao;

	/** Atributo cdProdutoServicoRelacionado. */
	private int cdProdutoServicoRelacionado;

	/** Atributo hrInclusaoRegistroHistorico. */
	private String hrInclusaoRegistroHistorico;

	/** Atributo cdAcaoNaoVida. */
	private int cdAcaoNaoVida;

	/** Atributo dsAcaoNaoVida. */
	private String dsAcaoNaoVida;

	/** Atributo cdAcertoDadosRecadastramento. */
	private int cdAcertoDadosRecadastramento;

	/** Atributo dsAcertoDadosRecadastramento. */
	private String dsAcertoDadosRecadastramento;

	/** Atributo cdAgendamentoDebitoVeiculo. */
	private int cdAgendamentoDebitoVeiculo;

	/** Atributo dsAgendamentoDebitoVeiculo. */
	private String dsAgendamentoDebitoVeiculo;

	/** Atributo cdAgendamentoPagamentoVencido. */
	private int cdAgendamentoPagamentoVencido;

	/** Atributo dsAgendamentoPagamentoVencido. */
	private String dsAgendamentoPagamentoVencido;

	/** Atributo cdAgendamentoRastreabilidadeFinal. */
	private int cdAgendamentoRastreabilidadeFinal;

	/** Atributo dsAgendamentoRastreabilidadeFinal. */
	private String dsAgendamentoRastreabilidadeFinal;

	/** Atributo cdAgendamentoValorMenor. */
	private int cdAgendamentoValorMenor;

	/** Atributo dsAgendamentoValorMenor. */
	private String dsAgendamentoValorMenor;

	/** Atributo cdAgrupamentoAviso. */
	private int cdAgrupamentoAviso;

	/** Atributo dsAgrupamentoAviso. */
	private String dsAgrupamentoAviso;

	/** Atributo cdAgrupamentoComprovante. */
	private int cdAgrupamentoComprovante;

	/** Atributo dsAgrupamentoComprovante. */
	private String dsAgrupamentoComprovante;

	/** Atributo cdAgrupamentoFormularioRecadastro. */
	private int cdAgrupamentoFormularioRecadastro;

	/** Atributo dsAgrupamentoFormularioRecadastro. */
	private String dsAgrupamentoFormularioRecadastro;

	/** Atributo cdAntecipacaoRecadastramentoBeneficiario. */
	private int cdAntecipacaoRecadastramentoBeneficiario;

	/** Atributo dsAntecipacaoRecadastramentoBeneficiario. */
	private String dsAntecipacaoRecadastramentoBeneficiario;

	/** Atributo cdAreaReservada. */
	private int cdAreaReservada;

	/** Atributo dsAreaReservada. */
	private String dsAreaReservada;

	/** Atributo cdBaseRecadastramentoBeneficio. */
	private int cdBaseRecadastramentoBeneficio;

	/** Atributo dsBaseRecadastramentoBeneficio. */
	private String dsBaseRecadastramentoBeneficio;

	/** Atributo cdBloqueioEmissaoPapeleta. */
	private int cdBloqueioEmissaoPapeleta;

	/** Atributo dsBloqueioEmissaoPapeleta. */
	private String dsBloqueioEmissaoPapeleta;

	/** Atributo cdCapturaTituloRegistrado. */
	private int cdCapturaTituloRegistrado;

	/** Atributo dsCapturaTituloRegistrado. */
	private String dsCapturaTituloRegistrado;

	/** Atributo cdCobrancaTarifa. */
	private int cdCobrancaTarifa;

	/** Atributo dsCobrancaTarifa. */
	private String dsCobrancaTarifa;

	/** Atributo cdConsultaDebitoVeiculo. */
	private int cdConsultaDebitoVeiculo;

	/** Atributo dsConsultaDebitoVeiculo. */
	private String dsConsultaDebitoVeiculo;

	/** Atributo cdConsultaEndereco. */
	private int cdConsultaEndereco;

	/** Atributo dsConsultaEndereco. */
	private String dsConsultaEndereco;

	/** Atributo cdConsultaSaldoPagamento. */
	private int cdConsultaSaldoPagamento;

	/** Atributo dsConsultaSaldoPagamento. */
	private String dsConsultaSaldoPagamento;

	/** Atributo cdContagemConsultaSaldo. */
	private int cdContagemConsultaSaldo;

	/** Atributo dsContagemConsultaSaldo. */
	private String dsContagemConsultaSaldo;

	/** Atributo cdCreditoNaoUtilizado. */
	private int cdCreditoNaoUtilizado;

	/** Atributo dsCreditoNaoUtilizado. */
	private String dsCreditoNaoUtilizado;

	/** Atributo cdCriterioEnquandraBeneficio. */
	private int cdCriterioEnquandraBeneficio;

	/** Atributo dsCriterioEnquandraBeneficio. */
	private String dsCriterioEnquandraBeneficio;

	/** Atributo cdCriterioEnquadraRecadastramento. */
	private int cdCriterioEnquadraRecadastramento;

	/** Atributo dsCriterioEnquadraRecadastramento. */
	private String dsCriterioEnquadraRecadastramento;

	/** Atributo cdCriterioRastreabilidadeTitulo. */
	private int cdCriterioRastreabilidadeTitulo;

	/** Atributo dsCriterioRastreabilidadeTitulo. */
	private String dsCriterioRastreabilidadeTitulo;

	/** Atributo cdCctciaEspeBeneficio. */
	private int cdCctciaEspeBeneficio;

	/** Atributo dsCctciaEspeBeneficio. */
	private String dsCctciaEspeBeneficio;

	/** Atributo cdCctciaIdentificacaoBeneficio. */
	private int cdCctciaIdentificacaoBeneficio;

	/** Atributo dsCctciaIdentificacaoBeneficio. */
	private String dsCctciaIdentificacaoBeneficio;

	/** Atributo cdCctciaInscricaoFavorecido. */
	private int cdCctciaInscricaoFavorecido;

	/** Atributo dsCctciaInscricaoFavorecido. */
	private String dsCctciaInscricaoFavorecido;

	/** Atributo cdCctciaProprietarioVeculo. */
	private int cdCctciaProprietarioVeculo;

	/** Atributo dsCctciaProprietarioVeculo. */
	private String dsCctciaProprietarioVeculo;

	/** Atributo cdDisponibilizacaoContaCredito. */
	private int cdDisponibilizacaoContaCredito;

	/** Atributo dsDisponibilizacaoContaCredito. */
	private String dsDisponibilizacaoContaCredito;

	/** Atributo cdDisponibilizacaoDiversoCriterio. */
	private int cdDisponibilizacaoDiversoCriterio;

	/** Atributo dsDisponibilizacaoDiversoCriterio. */
	private String dsDisponibilizacaoDiversoCriterio;

	/** Atributo cdDisponibilizacaoDiversoNao. */
	private int cdDisponibilizacaoDiversoNao;

	/** Atributo dsDisponibilizacaoDiversoNao. */
	private String dsDisponibilizacaoDiversoNao;

	/** Atributo cdDisponibilizacaoSalarioCriterio. */
	private int cdDisponibilizacaoSalarioCriterio;

	/** Atributo dsDisponibilizacaoSalarioCriterio. */
	private String dsDisponibilizacaoSalarioCriterio;

	/** Atributo cdDisponibilizacaoSalarioNao. */
	private int cdDisponibilizacaoSalarioNao;

	/** Atributo dsDisponibilizacaoSalarioNao. */
	private String dsDisponibilizacaoSalarioNao;

	/** Atributo cdDestinoAviso. */
	private int cdDestinoAviso;

	/** Atributo dsDestinoAviso. */
	private String dsDestinoAviso;

	/** Atributo cdDestinoComprovante. */
	private int cdDestinoComprovante;

	/** Atributo dsDestinoComprovante. */
	private String dsDestinoComprovante;

	/** Atributo cdDestinoFormularioRecadastramento. */
	private int cdDestinoFormularioRecadastramento;

	/** Atributo dsDestinoFormularioRecadastramento. */
	private String dsDestinoFormularioRecadastramento;

	/** Atributo cdEnvelopeAberto. */
	private int cdEnvelopeAberto;

	/** Atributo dsEnvelopeAberto. */
	private String dsEnvelopeAberto;

	/** Atributo cdFavorecidoConsultaPagamento. */
	private int cdFavorecidoConsultaPagamento;

	/** Atributo dsFavorecidoConsultaPagamento. */
	private String dsFavorecidoConsultaPagamento;

	/** Atributo cdFormaAutorizacaoPagamento. */
	private int cdFormaAutorizacaoPagamento;

	/** Atributo dsFormaAutorizacaoPagamento. */
	private String dsFormaAutorizacaoPagamento;

	/** Atributo cdFormaEnvioPagamento. */
	private int cdFormaEnvioPagamento;

	/** Atributo dsFormaEnvioPagamento. */
	private String dsFormaEnvioPagamento;

	/** Atributo cdFormaEstornoCredito. */
	private int cdFormaEstornoCredito;

	/** Atributo dsFormaEstornoCredito. */
	private String dsFormaEstornoCredito;

	/** Atributo cdFormaExpiracaoCredito. */
	private int cdFormaExpiracaoCredito;

	/** Atributo dsFormaExpiracaoCredito. */
	private String dsFormaExpiracaoCredito;

	/** Atributo cdFormaManutencao. */
	private int cdFormaManutencao;

	/** Atributo dsFormaManutencao. */
	private String dsFormaManutencao;

	/** Atributo cdFrasePrecadastrada. */
	private int cdFrasePrecadastrada;

	/** Atributo dsFrasePrecadastrada. */
	private String dsFrasePrecadastrada;

	/** Atributo cdIndicadorAgendamentoTitulo. */
	private int cdIndicadorAgendamentoTitulo;

	/** Atributo dsIndicadorAgendamentoTitulo. */
	private String dsIndicadorAgendamentoTitulo;

	/** Atributo cdIndicadorAutorizacaoCliente. */
	private int cdIndicadorAutorizacaoCliente;

	/** Atributo dsIndicadorAutorizacaoCliente. */
	private String dsIndicadorAutorizacaoCliente;

	/** Atributo cdIndicadorAutorizacaoComplemento. */
	private int cdIndicadorAutorizacaoComplemento;

	/** Atributo dsIndicadorAutorizacaoComplemento. */
	private String dsIndicadorAutorizacaoComplemento;

	/** Atributo cdIndicadorCadastroorganizacao. */
	private int cdIndicadorCadastroorganizacao;

	/** Atributo dsIndicadorCadastroorganizacao. */
	private String dsIndicadorCadastroorganizacao;

	/** Atributo cdIndicadorCadastroProcurador. */
	private int cdIndicadorCadastroProcurador;

	/** Atributo dsIndicadorCadastroProcurador. */
	private String dsIndicadorCadastroProcurador;

	/** Atributo cdIndicadorCartaoSalario. */
	private int cdIndicadorCartaoSalario;

	/** Atributo dsIndicadorCartaoSalario. */
	private String dsIndicadorCartaoSalario;

	/** Atributo cdIndicadorEconomicoReajuste. */
	private int cdIndicadorEconomicoReajuste;

	/** Atributo dsIndicadorEconomicoReajuste. */
	private String dsIndicadorEconomicoReajuste;

	/** Atributo cdindicadorExpiraCredito. */
	private int cdindicadorExpiraCredito;

	/** Atributo dsindicadorExpiraCredito. */
	private String dsindicadorExpiraCredito;

	/** Atributo cdindicadorLancamentoProgramado. */
	private int cdindicadorLancamentoProgramado;

	/** Atributo dsindicadorLancamentoProgramado. */
	private String dsindicadorLancamentoProgramado;

	/** Atributo cdIndicadorMensagemPersonalizada. */
	private int cdIndicadorMensagemPersonalizada;

	/** Atributo dsIndicadorMensagemPersonalizada. */
	private String dsIndicadorMensagemPersonalizada;

	/** Atributo cdLancamentoFuturoCredito. */
	private int cdLancamentoFuturoCredito;

	/** Atributo dsLancamentoFuturoCredito. */
	private String dsLancamentoFuturoCredito;

	/** Atributo cdLancamentoFuturoDebito. */
	private int cdLancamentoFuturoDebito;

	/** Atributo dsLancamentoFuturoDebito. */
	private String dsLancamentoFuturoDebito;

	/** Atributo cdLiberacaoLoteProcessado. */
	private int cdLiberacaoLoteProcessado;

	/** Atributo dsLiberacaoLoteProcessado. */
	private String dsLiberacaoLoteProcessado;

	/** Atributo cdManutencaoBaseRecadastramento. */
	private int cdManutencaoBaseRecadastramento;

	/** Atributo dsManutencaoBaseRecadastramento. */
	private String dsManutencaoBaseRecadastramento;

	/** Atributo cdMeioPagamentoDebito. */
	private int cdMeioPagamentoDebito;

	/** Atributo dsMeioPagamentoDebito. */
	private String dsMeioPagamentoDebito;

	/** Atributo cdMidiaDisponivel. */
	private int cdMidiaDisponivel;

	/** Atributo dsMidiaDisponivel. */
	private String dsMidiaDisponivel;

	/** Atributo cdMidiaMensagemRecadastramento. */
	private int cdMidiaMensagemRecadastramento;

	/** Atributo dsMidiaMensagemRecadastramento. */
	private String dsMidiaMensagemRecadastramento;

	/** Atributo cdMomentoAvisoRecadastramento. */
	private int cdMomentoAvisoRecadastramento;

	/** Atributo dsMomentoAvisoRecadastramento. */
	private String dsMomentoAvisoRecadastramento;

	/** Atributo cdMomentoCreditoEfetivacao. */
	private int cdMomentoCreditoEfetivacao;

	/** Atributo dsMomentoCreditoEfetivacao. */
	private String dsMomentoCreditoEfetivacao;

	/** Atributo cdMomentoDebitoPagamento. */
	private int cdMomentoDebitoPagamento;

	/** Atributo dsMomentoDebitoPagamento. */
	private String dsMomentoDebitoPagamento;

	/** Atributo cdMomentoFormularioRecadastramento. */
	private int cdMomentoFormularioRecadastramento;

	/** Atributo dsMomentoFormularioRecadastramento. */
	private String dsMomentoFormularioRecadastramento;

	/** Atributo cdMomentoProcessamentoPagamento. */
	private int cdMomentoProcessamentoPagamento;

	/** Atributo dsMomentoProcessamentoPagamento. */
	private String dsMomentoProcessamentoPagamento;

	/** Atributo cdMensagemRecadastramentoMidia. */
	private int cdMensagemRecadastramentoMidia;

	/** Atributo dsMensagemRecadastramentoMidia. */
	private String dsMensagemRecadastramentoMidia;

	/** Atributo cdPermissaoDebitoOnline. */
	private int cdPermissaoDebitoOnline;

	/** Atributo dsPermissaoDebitoOnline. */
	private String dsPermissaoDebitoOnline;

	/** Atributo cdNaturezaOperacaoPagamento. */
	private int cdNaturezaOperacaoPagamento;

	/** Atributo dsNaturezaOperacaoPagamento. */
	private String dsNaturezaOperacaoPagamento;

	/** Atributo cdOutraidentificacaoFavorecido. */
	private int cdOutraidentificacaoFavorecido;

	/** Atributo dsOutraidentificacaoFavorecido. */
	private String dsOutraidentificacaoFavorecido;

	/** Atributo cdPeriodicidadeAviso. */
	private int cdPeriodicidadeAviso;

	/** Atributo dsPeriodicidadeAviso. */
	private String dsPeriodicidadeAviso;

	/** Atributo cdPeriodicidadeCobrancaTarifa. */
	private int cdPeriodicidadeCobrancaTarifa;

	/** Atributo dsPeriodicidadeCobrancaTarifa. */
	private String dsPeriodicidadeCobrancaTarifa;

	/** Atributo cdPeriodicidadeComprovante. */
	private int cdPeriodicidadeComprovante;

	/** Atributo dsPeriodicidadeComprovante. */
	private String dsPeriodicidadeComprovante;

	/** Atributo cdPeriodicidadeConsultaVeiculo. */
	private int cdPeriodicidadeConsultaVeiculo;

	/** Atributo dsPeriodicidadeConsultaVeiculo. */
	private String dsPeriodicidadeConsultaVeiculo;

	/** Atributo cdPeriodicidadeEnvioRemessa. */
	private int cdPeriodicidadeEnvioRemessa;

	/** Atributo dsPeriodicidadeEnvioRemessa. */
	private String dsPeriodicidadeEnvioRemessa;

	/** Atributo cdPeriodicidadeManutencaoProcd. */
	private int cdPeriodicidadeManutencaoProcd;

	/** Atributo dsPeriodicidadeManutencaoProcd. */
	private String dsPeriodicidadeManutencaoProcd;

	/** Atributo cdPeriodicidadeReajusteTarifa. */
	private int cdPeriodicidadeReajusteTarifa;

	/** Atributo dsPeriodicidadeReajusteTarifa. */
	private String dsPeriodicidadeReajusteTarifa;

	/** Atributo cdPagamentoNaoUtil. */
	private int cdPagamentoNaoUtil;

	/** Atributo dsPagamentoNaoUtil. */
	private String dsPagamentoNaoUtil;

	/** Atributo cdPrincipalEnquaRecadastramento. */
	private int cdPrincipalEnquaRecadastramento;

	/** Atributo dsPrincipalEnquaRecadastramento. */
	private String dsPrincipalEnquaRecadastramento;

	/** Atributo cdPrioridadeEfetivacaoPagamento. */
	private int cdPrioridadeEfetivacaoPagamento;

	/** Atributo dsPrioridadeEfetivacaoPagamento. */
	private String dsPrioridadeEfetivacaoPagamento;

	/** Atributo cdRejeicaoAgendamentoLote. */
	private int cdRejeicaoAgendamentoLote;

	/** Atributo dsRejeicaoAgendamentoLote. */
	private String dsRejeicaoAgendamentoLote;

	/** Atributo cdRejeicaoEfetivacaoLote. */
	private int cdRejeicaoEfetivacaoLote;

	/** Atributo dsRejeicaoEfetivacaoLote. */
	private String dsRejeicaoEfetivacaoLote;

	/** Atributo cdRejeicaoLote. */
	private int cdRejeicaoLote;

	/** Atributo dsRejeicaoLote. */
	private String dsRejeicaoLote;

	/** Atributo cdRastreabilidadeNotaFiscal. */
	private int cdRastreabilidadeNotaFiscal;

	/** Atributo dsRastreabilidadeNotaFiscal. */
	private String dsRastreabilidadeNotaFiscal;

	/** Atributo cdRastreabilidadeTituloTerceiro. */
	private int cdRastreabilidadeTituloTerceiro;

	/** Atributo dsRastreabilidadeTituloTerceiro. */
	private String dsRastreabilidadeTituloTerceiro;

	/** Atributo cdTipoCargaRecadastramento. */
	private int cdTipoCargaRecadastramento;

	/** Atributo dsTipoCargaRecadastramento. */
	private String dsTipoCargaRecadastramento;

	/** Atributo cdTipoCartaoSalario. */
	private int cdTipoCartaoSalario;

	/** Atributo dsTipoCartaoSalario. */
	private String dsTipoCartaoSalario;

	/** Atributo cdTipoDataFloating. */
	private int cdTipoDataFloating;

	/** Atributo dsTipoDataFloating. */
	private String dsTipoDataFloating;

	/** Atributo cdTipoDivergenciaVeiculo. */
	private int cdTipoDivergenciaVeiculo;

	/** Atributo dsTipoDivergenciaVeiculo. */
	private String dsTipoDivergenciaVeiculo;

	/** Atributo cdTipoEfetivacaoPagamento. */
	private int cdTipoEfetivacaoPagamento;

	/** Atributo dsTipoEfetivacaoPagamento. */
	private String dsTipoEfetivacaoPagamento;

	/** Atributo cdTipoIdentificacaoBeneficio. */
	private int cdTipoIdentificacaoBeneficio;

	/** Atributo dsTipoIdentificacaoBeneficio. */
	private String dsTipoIdentificacaoBeneficio;

	/** Atributo cdTipoLayoutArquivo. */
	private int cdTipoLayoutArquivo;

	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;

	/** Atributo cdTipoReajusteTarifa. */
	private int cdTipoReajusteTarifa;

	/** Atributo dsTipoReajusteTarifa. */
	private String dsTipoReajusteTarifa;

	/** Atributo cdTratamentoContaTransferida. */
	private int cdTratamentoContaTransferida;

	/** Atributo dsTratamentoContaTransferida. */
	private String dsTratamentoContaTransferida;

	/** Atributo cdUtilizacaoFavorecidoControle. */
	private int cdUtilizacaoFavorecidoControle;

	/** Atributo dsUtilizacaoFavorecidoControle. */
	private String dsUtilizacaoFavorecidoControle;

	/** Atributo dtEnquaContaSalario. */
	private String dtEnquaContaSalario;

	/** Atributo dtInicioBloqueioPapeleta. */
	private String dtInicioBloqueioPapeleta;

	/** Atributo dtInicioRastreabilidadeTitulo. */
	private String dtInicioRastreabilidadeTitulo;

	/** Atributo dtFimAcertoRecadastramento. */
	private String dtFimAcertoRecadastramento;

	/** Atributo dtFimRecadastramentoBeneficio. */
	private String dtFimRecadastramentoBeneficio;

	/** Atributo dtInicioAcertoRecadastramento. */
	private String dtInicioAcertoRecadastramento;

	/** Atributo dtInicioRecadastramentoBeneficio. */
	private String dtInicioRecadastramentoBeneficio;

	/** Atributo dtLimiteVinculoCarga. */
	private String dtLimiteVinculoCarga;

	/** Atributo dtRegistroTitulo. */
	private String dtRegistroTitulo;

	/** Atributo nrFechamentoApuracaoTarifa. */
	private long nrFechamentoApuracaoTarifa;

	/** Atributo percentualIndiceReajusteTarifa. */
	private BigDecimal percentualIndiceReajusteTarifa;

	/** Atributo percentualMaximoInconsistenteLote. */
	private int percentualMaximoInconsistenteLote;

	/** Atributo periodicidadeTarifaCatalogo. */
	private BigDecimal periodicidadeTarifaCatalogo;

	/** Atributo quantidadeAntecedencia. */
	private int quantidadeAntecedencia;

	/** Atributo quantidadeAnteriorVencimentoComprovante. */
	private int quantidadeAnteriorVencimentoComprovante;

	/** Atributo quantidadeDiaCobrancaTarifa. */
	private int quantidadeDiaCobrancaTarifa;

	/** Atributo quantidadeDiaExpiracao. */
	private int quantidadeDiaExpiracao;

	/** Atributo dsLocalEmissao. */
	private String dsLocalEmissao;

	/** Atributo quantidadeDiaFloatingPagamento. */
	private int quantidadeDiaFloatingPagamento;

	/** Atributo quantidadeDiaInatividadeFavorecido. */
	private int quantidadeDiaInatividadeFavorecido;

	/** Atributo quantidadeDiaRepiqueConsulta. */
	private int quantidadeDiaRepiqueConsulta;

	/** Atributo quantidadeEtapaRecadastramentoBeneficio. */
	private int quantidadeEtapaRecadastramentoBeneficio;

	/** Atributo quantidadeFaseRecadastramentoBeneficio. */
	private int quantidadeFaseRecadastramentoBeneficio;

	/** Atributo quantidadeLimiteLinha. */
	private int quantidadeLimiteLinha;

	/** Atributo quantidadeSolicitacaoCartao. */
	private int quantidadeSolicitacaoCartao;

	/** Atributo quantidadeMaximaInconsistenteLote. */
	private int quantidadeMaximaInconsistenteLote;

	/** Atributo quantidadeMaximaTituloVencido. */
	private int quantidadeMaximaTituloVencido;

	/** Atributo quantidadeMesComprovante. */
	private int quantidadeMesComprovante;

	/** Atributo quantidadeMesEtapaRecadastramento. */
	private int quantidadeMesEtapaRecadastramento;

	/** Atributo quantidadeMesFaseRecadastramento. */
	private int quantidadeMesFaseRecadastramento;

	/** Atributo quantidadeMesReajusteTarifa. */
	private int quantidadeMesReajusteTarifa;

	/** Atributo quantidadeViaAviso. */
	private int quantidadeViaAviso;

	/** Atributo quantidadeViaCombranca. */
	private int quantidadeViaCombranca;

	/** Atributo quantidadeViaComprovante. */
	private int quantidadeViaComprovante;

	/** Atributo valorFavorecidoNaoCadastrado. */
	private BigDecimal valorFavorecidoNaoCadastrado;

	/** Atributo valorLimiteDiarioPagamento. */
	private BigDecimal valorLimiteDiarioPagamento;

	/** Atributo valorLimiteIndividualPagamento. */
	private BigDecimal valorLimiteIndividualPagamento;

	// TRILHA
	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;

	/** Atributo cdUsuarioExternoInclusao. */
	private String cdUsuarioExternoInclusao;

	/** Atributo hrManutencaoRegistroInclusao. */
	private String hrManutencaoRegistroInclusao;

	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;

	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;

	/** Atributo nrOperacaoFluxoInclusao. */
	private String nrOperacaoFluxoInclusao;

	/** Atributo cdUsuarioAlteracao. */
	private String cdUsuarioAlteracao;

	/** Atributo cdUsuarioExternoAlteracao. */
	private String cdUsuarioExternoAlteracao;

	/** Atributo hrManutencaoRegistroAlteracao. */
	private String hrManutencaoRegistroAlteracao;

	/** Atributo cdCanalAlteracao. */
	private int cdCanalAlteracao;

	/** Atributo dsCanalAlteracao. */
	private String dsCanalAlteracao;

	/** Atributo nrOperacaoFluxoAlteracao. */
	private String nrOperacaoFluxoAlteracao;

	/** Atributo cdIndicadorListaDebito. */
	private Integer cdIndicadorListaDebito;

	/** Atributo cdTipoFormacaoLista. */
	private Integer cdTipoFormacaoLista;

	/** Atributo dsTipoFormacaoLista. */
	private String dsTipoFormacaoLista;
	
	/** Atributo cdTipoConsistenciaLista. */
	private Integer cdTipoConsistenciaLista;
	
	/** Atributo dsTipoConsistenciaLista. */
	private String dsTipoConsistenciaLista;
	
	/** Atributo cdIndicadorRetornoInternet. */
	private Integer cdIndicadorRetornoInternet;
	
	/** Atributo cdTipoConsultaComprovante. */
	private Integer cdTipoConsultaComprovante;
	
	/** Atributo dsTipoConsolidacaoComprovante. */
	private String dsTipoConsolidacaoComprovante;
	
	/** Atributo cdIndicadorBancoPostal. */
	private Integer cdIndicadorBancoPostal;
	
	/** Atributo dsAreaReservada2. */
	private String dsAreaReservada2;

	/** Atributo cdAmbienteServicoContrato. */
	private String cdAmbienteServicoContrato;
	
	/** Atributo dsSituacaoServicoRelacionado. */
	private String dsSituacaoServicoRelacionado;
	
	/** Atributo cdConsultaSaldoValorSuperior. */
	private Integer cdConsultaSaldoValorSuperior;
	
	/** Atributo dsConsultaSaldoValorSuperior. */
	private String dsConsultaSaldoValorSuperior;

	/** Atributo cdIndicadorFeriadoLocal. */
	private Integer cdIndicadorFeriadoLocal;

	/** Atributo dsIndicadorFeriadoLocal. */
	private String dsIndicadorFeriadoLocal;
	
    /** Atributo cdIndicadorSegundaLinha. */
    private Integer cdIndicadorSegundaLinha;

    /** Atributo dsIndicadorSegundaLinha. */
    private String dsIndicadorSegundaLinha;
	
    /** Atributo cdFloatServicoContrato. */
    private Integer cdFloatServicoContrato;
    
    /** Atributo dsFloatServicoContrato. */
    private String dsFloatServicoContrato;

    /** Atributo dsCodigoIndFeriadoLocal. */
    private String dsCodigoIndFeriadoLocal;
    
    /** Atributo dsExigeFilial. */
    private String dsExigeFilial;

    /** Atributo cdPreenchimentoLancamentoPersonalizado. */
    private Integer cdPreenchimentoLancamentoPersonalizado;

    /** Atributo dsPreenchimentoLancamentoPersonalizado. */
    private String dsPreenchimentoLancamentoPersonalizado;

    /** Atributo cdTituloDdaRetorno. */
    private Integer cdTituloDdaRetorno;
    
    /** Atributo dsTituloDdaRetorno. */
    private String dsTituloDdaRetorno;

    /** Atributo cdIndicadorAgendaGrade. */
    private Integer cdIndicadorAgendaGrade;
    
    /** Atributo dsIndicadorAgendaGrade. */
    private String dsIndicadorAgendaGrade;

    /** Atributo qtDiaUtilPgto. */
    private Integer qtDiaUtilPgto;
    
    /** The cd indicador utiliza mora. */
    private Integer cdIndicadorUtilizaMora;
    
    /** The ds indicador utiliza mora. */
    private String  dsIndicadorUtilizaMora;

    /** Atributo vlPercentualDiferencaTolerada. */
    private BigDecimal vlPercentualDiferencaTolerada = null;
    
    /** Atributo cdConsistenciaCpfCnpjBenefAvalNpc. */
    private Integer cdConsistenciaCpfCnpjBenefAvalNpc;
    
    /** Atributo dsConsistenciaCpfCnpjBenefAvalNpc. */
    private String dsConsistenciaCpfCnpjBenefAvalNpc;	
    
    /** Atributo cdIdentificadortipoRetornoInternet. */
    private int cdIdentificadorTipoRetornoInternet;
    
    /** Atributo dsCodIdentificadortipoRetornoInternet. */
    private String dsCodIdentificadorTipoRetornoInternet;
    
    
	/**
	 * Get: dsAreaReservada2.
	 *
	 * @return dsAreaReservada2
	 */
	public String getDsAreaReservada2() {
		return dsAreaReservada2;
	}

	/**
	 * Set: dsAreaReservada2.
	 *
	 * @param dsAreaReservada2 the ds area reservada2
	 */
	public void setDsAreaReservada2(String dsAreaReservada2) {
		this.dsAreaReservada2 = dsAreaReservada2;
	}

	/**
	 * Get: cdIndicadorBancoPostal.
	 *
	 * @return cdIndicadorBancoPostal
	 */
	public Integer getCdIndicadorBancoPostal() {
		return cdIndicadorBancoPostal;
	}

	/**
	 * Set: cdIndicadorBancoPostal.
	 *
	 * @param cdIndicadorBancoPostal the cd indicador banco postal
	 */
	public void setCdIndicadorBancoPostal(Integer cdIndicadorBancoPostal) {
		this.cdIndicadorBancoPostal = cdIndicadorBancoPostal;
	}

	/**
	 * Get: dsTipoConsolidacaoComprovante.
	 *
	 * @return dsTipoConsolidacaoComprovante
	 */
	public String getDsTipoConsolidacaoComprovante() {
		return dsTipoConsolidacaoComprovante;
	}

	/**
	 * Set: dsTipoConsolidacaoComprovante.
	 *
	 * @param dsTipoConsolidacaoComprovante the ds tipo consolidacao comprovante
	 */
	public void setDsTipoConsolidacaoComprovante(String dsTipoConsolidacaoComprovante) {
		this.dsTipoConsolidacaoComprovante = dsTipoConsolidacaoComprovante;
	}

	/**
	 * Get: cdTipoConsultaComprovante.
	 *
	 * @return cdTipoConsultaComprovante
	 */
	public Integer getCdTipoConsultaComprovante() {
		return cdTipoConsultaComprovante;
	}

	/**
	 * Set: cdTipoConsultaComprovante.
	 *
	 * @param cdTipoConsultaComprovante the cd tipo consulta comprovante
	 */
	public void setCdTipoConsultaComprovante(Integer cdTipoConsultaComprovante) {
		this.cdTipoConsultaComprovante = cdTipoConsultaComprovante;
	}

	/**
	 * Get: cdIndicadorRetornoInternet.
	 *
	 * @return cdIndicadorRetornoInternet
	 */
	public Integer getCdIndicadorRetornoInternet() {
		return cdIndicadorRetornoInternet;
	}

	/**
	 * Set: cdIndicadorRetornoInternet.
	 *
	 * @param cdIndicadorRetornoInternet the cd indicador retorno internet
	 */
	public void setCdIndicadorRetornoInternet(Integer cdIndicadorRetornoInternet) {
		this.cdIndicadorRetornoInternet = cdIndicadorRetornoInternet;
	}

	/**
	 * Get: dsTipoConsistenciaLista.
	 *
	 * @return dsTipoConsistenciaLista
	 */
	public String getDsTipoConsistenciaLista() {
		return dsTipoConsistenciaLista;
	}

	/**
	 * Set: dsTipoConsistenciaLista.
	 *
	 * @param dsTipoConsistenciaLista the ds tipo consistencia lista
	 */
	public void setDsTipoConsistenciaLista(String dsTipoConsistenciaLista) {
		this.dsTipoConsistenciaLista = dsTipoConsistenciaLista;
	}

	/**
	 * Get: cdTipoConsistenciaLista.
	 *
	 * @return cdTipoConsistenciaLista
	 */
	public Integer getCdTipoConsistenciaLista() {
		return cdTipoConsistenciaLista;
	}

	/**
	 * Set: cdTipoConsistenciaLista.
	 *
	 * @param cdTipoConsistenciaLista the cd tipo consistencia lista
	 */
	public void setCdTipoConsistenciaLista(Integer cdTipoConsistenciaLista) {
		this.cdTipoConsistenciaLista = cdTipoConsistenciaLista;
	}

	/**
	 * Get: cdTipoFormacaoLista.
	 *
	 * @return cdTipoFormacaoLista
	 */
	public Integer getCdTipoFormacaoLista() {
		return cdTipoFormacaoLista;
	}

	/**
	 * Set: cdTipoFormacaoLista.
	 *
	 * @param cdTipoFormacaoLista the cd tipo formacao lista
	 */
	public void setCdTipoFormacaoLista(Integer cdTipoFormacaoLista) {
		this.cdTipoFormacaoLista = cdTipoFormacaoLista;
	}

	/**
	 * Get: dsTipoFormacaoLista.
	 *
	 * @return dsTipoFormacaoLista
	 */
	public String getDsTipoFormacaoLista() {
		return dsTipoFormacaoLista;
	}

	/**
	 * Set: dsTipoFormacaoLista.
	 *
	 * @param dsTipoFormacaoLista the ds tipo formacao lista
	 */
	public void setDsTipoFormacaoLista(String dsTipoFormacaoLista) {
		this.dsTipoFormacaoLista = dsTipoFormacaoLista;
	}

	/**
	 * Get: cdIndicadorListaDebito.
	 *
	 * @return cdIndicadorListaDebito
	 */
	public Integer getCdIndicadorListaDebito() {
		return cdIndicadorListaDebito;
	}

	/**
	 * Set: cdIndicadorListaDebito.
	 *
	 * @param cdIndicadorListaDebito the cd indicador lista debito
	 */
	public void setCdIndicadorListaDebito(Integer cdIndicadorListaDebito) {
		this.cdIndicadorListaDebito = cdIndicadorListaDebito;
	}

	/**
	 * Get: cdAcaoNaoVida.
	 *
	 * @return cdAcaoNaoVida
	 */
	public int getCdAcaoNaoVida() {
		return cdAcaoNaoVida;
	}

	/**
	 * Set: cdAcaoNaoVida.
	 *
	 * @param cdAcaoNaoVida the cd acao nao vida
	 */
	public void setCdAcaoNaoVida(int cdAcaoNaoVida) {
		this.cdAcaoNaoVida = cdAcaoNaoVida;
	}

	/**
	 * Get: cdAcertoDadosRecadastramento.
	 *
	 * @return cdAcertoDadosRecadastramento
	 */
	public int getCdAcertoDadosRecadastramento() {
		return cdAcertoDadosRecadastramento;
	}

	/**
	 * Set: cdAcertoDadosRecadastramento.
	 *
	 * @param cdAcertoDadosRecadastramento the cd acerto dados recadastramento
	 */
	public void setCdAcertoDadosRecadastramento(int cdAcertoDadosRecadastramento) {
		this.cdAcertoDadosRecadastramento = cdAcertoDadosRecadastramento;
	}

	/**
	 * Get: cdAgendamentoDebitoVeiculo.
	 *
	 * @return cdAgendamentoDebitoVeiculo
	 */
	public int getCdAgendamentoDebitoVeiculo() {
		return cdAgendamentoDebitoVeiculo;
	}

	/**
	 * Set: cdAgendamentoDebitoVeiculo.
	 *
	 * @param cdAgendamentoDebitoVeiculo the cd agendamento debito veiculo
	 */
	public void setCdAgendamentoDebitoVeiculo(int cdAgendamentoDebitoVeiculo) {
		this.cdAgendamentoDebitoVeiculo = cdAgendamentoDebitoVeiculo;
	}

	/**
	 * Get: cdAgendamentoPagamentoVencido.
	 *
	 * @return cdAgendamentoPagamentoVencido
	 */
	public int getCdAgendamentoPagamentoVencido() {
		return cdAgendamentoPagamentoVencido;
	}

	/**
	 * Set: cdAgendamentoPagamentoVencido.
	 *
	 * @param cdAgendamentoPagamentoVencido the cd agendamento pagamento vencido
	 */
	public void setCdAgendamentoPagamentoVencido(int cdAgendamentoPagamentoVencido) {
		this.cdAgendamentoPagamentoVencido = cdAgendamentoPagamentoVencido;
	}

	/**
	 * Get: cdAgendamentoRastreabilidadeFinal.
	 *
	 * @return cdAgendamentoRastreabilidadeFinal
	 */
	public int getCdAgendamentoRastreabilidadeFinal() {
		return cdAgendamentoRastreabilidadeFinal;
	}

	/**
	 * Set: cdAgendamentoRastreabilidadeFinal.
	 *
	 * @param cdAgendamentoRastreabilidadeFinal the cd agendamento rastreabilidade final
	 */
	public void setCdAgendamentoRastreabilidadeFinal(int cdAgendamentoRastreabilidadeFinal) {
		this.cdAgendamentoRastreabilidadeFinal = cdAgendamentoRastreabilidadeFinal;
	}

	/**
	 * Get: cdAgendamentoValorMenor.
	 *
	 * @return cdAgendamentoValorMenor
	 */
	public int getCdAgendamentoValorMenor() {
		return cdAgendamentoValorMenor;
	}

	/**
	 * Set: cdAgendamentoValorMenor.
	 *
	 * @param cdAgendamentoValorMenor the cd agendamento valor menor
	 */
	public void setCdAgendamentoValorMenor(int cdAgendamentoValorMenor) {
		this.cdAgendamentoValorMenor = cdAgendamentoValorMenor;
	}

	/**
	 * Get: cdAgrupamentoAviso.
	 *
	 * @return cdAgrupamentoAviso
	 */
	public int getCdAgrupamentoAviso() {
		return cdAgrupamentoAviso;
	}

	/**
	 * Set: cdAgrupamentoAviso.
	 *
	 * @param cdAgrupamentoAviso the cd agrupamento aviso
	 */
	public void setCdAgrupamentoAviso(int cdAgrupamentoAviso) {
		this.cdAgrupamentoAviso = cdAgrupamentoAviso;
	}

	/**
	 * Get: cdAgrupamentoComprovante.
	 *
	 * @return cdAgrupamentoComprovante
	 */
	public int getCdAgrupamentoComprovante() {
		return cdAgrupamentoComprovante;
	}

	/**
	 * Set: cdAgrupamentoComprovante.
	 *
	 * @param cdAgrupamentoComprovante the cd agrupamento comprovante
	 */
	public void setCdAgrupamentoComprovante(int cdAgrupamentoComprovante) {
		this.cdAgrupamentoComprovante = cdAgrupamentoComprovante;
	}

	/**
	 * Get: cdAgrupamentoFormularioRecadastro.
	 *
	 * @return cdAgrupamentoFormularioRecadastro
	 */
	public int getCdAgrupamentoFormularioRecadastro() {
		return cdAgrupamentoFormularioRecadastro;
	}

	/**
	 * Set: cdAgrupamentoFormularioRecadastro.
	 *
	 * @param cdAgrupamentoFormularioRecadastro the cd agrupamento formulario recadastro
	 */
	public void setCdAgrupamentoFormularioRecadastro(int cdAgrupamentoFormularioRecadastro) {
		this.cdAgrupamentoFormularioRecadastro = cdAgrupamentoFormularioRecadastro;
	}

	/**
	 * Get: cdAntecipacaoRecadastramentoBeneficiario.
	 *
	 * @return cdAntecipacaoRecadastramentoBeneficiario
	 */
	public int getCdAntecipacaoRecadastramentoBeneficiario() {
		return cdAntecipacaoRecadastramentoBeneficiario;
	}

	/**
	 * Set: cdAntecipacaoRecadastramentoBeneficiario.
	 *
	 * @param cdAntecipacaoRecadastramentoBeneficiario the cd antecipacao recadastramento beneficiario
	 */
	public void setCdAntecipacaoRecadastramentoBeneficiario(int cdAntecipacaoRecadastramentoBeneficiario) {
		this.cdAntecipacaoRecadastramentoBeneficiario = cdAntecipacaoRecadastramentoBeneficiario;
	}

	/**
	 * Get: cdAreaReservada.
	 *
	 * @return cdAreaReservada
	 */
	public int getCdAreaReservada() {
		return cdAreaReservada;
	}

	/**
	 * Set: cdAreaReservada.
	 *
	 * @param cdAreaReservada the cd area reservada
	 */
	public void setCdAreaReservada(int cdAreaReservada) {
		this.cdAreaReservada = cdAreaReservada;
	}

	/**
	 * Get: cdBaseRecadastramentoBeneficio.
	 *
	 * @return cdBaseRecadastramentoBeneficio
	 */
	public int getCdBaseRecadastramentoBeneficio() {
		return cdBaseRecadastramentoBeneficio;
	}

	/**
	 * Set: cdBaseRecadastramentoBeneficio.
	 *
	 * @param cdBaseRecadastramentoBeneficio the cd base recadastramento beneficio
	 */
	public void setCdBaseRecadastramentoBeneficio(int cdBaseRecadastramentoBeneficio) {
		this.cdBaseRecadastramentoBeneficio = cdBaseRecadastramentoBeneficio;
	}

	/**
	 * Get: cdBloqueioEmissaoPapeleta.
	 *
	 * @return cdBloqueioEmissaoPapeleta
	 */
	public int getCdBloqueioEmissaoPapeleta() {
		return cdBloqueioEmissaoPapeleta;
	}

	/**
	 * Set: cdBloqueioEmissaoPapeleta.
	 *
	 * @param cdBloqueioEmissaoPapeleta the cd bloqueio emissao papeleta
	 */
	public void setCdBloqueioEmissaoPapeleta(int cdBloqueioEmissaoPapeleta) {
		this.cdBloqueioEmissaoPapeleta = cdBloqueioEmissaoPapeleta;
	}

	/**
	 * Get: cdCanalAlteracao.
	 *
	 * @return cdCanalAlteracao
	 */
	public int getCdCanalAlteracao() {
		return cdCanalAlteracao;
	}

	/**
	 * Set: cdCanalAlteracao.
	 *
	 * @param cdCanalAlteracao the cd canal alteracao
	 */
	public void setCdCanalAlteracao(int cdCanalAlteracao) {
		this.cdCanalAlteracao = cdCanalAlteracao;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: cdCapturaTituloRegistrado.
	 *
	 * @return cdCapturaTituloRegistrado
	 */
	public int getCdCapturaTituloRegistrado() {
		return cdCapturaTituloRegistrado;
	}

	/**
	 * Set: cdCapturaTituloRegistrado.
	 *
	 * @param cdCapturaTituloRegistrado the cd captura titulo registrado
	 */
	public void setCdCapturaTituloRegistrado(int cdCapturaTituloRegistrado) {
		this.cdCapturaTituloRegistrado = cdCapturaTituloRegistrado;
	}

	/**
	 * Get: cdCctciaEspeBeneficio.
	 *
	 * @return cdCctciaEspeBeneficio
	 */
	public int getCdCctciaEspeBeneficio() {
		return cdCctciaEspeBeneficio;
	}

	/**
	 * Set: cdCctciaEspeBeneficio.
	 *
	 * @param cdCctciaEspeBeneficio the cd cctcia espe beneficio
	 */
	public void setCdCctciaEspeBeneficio(int cdCctciaEspeBeneficio) {
		this.cdCctciaEspeBeneficio = cdCctciaEspeBeneficio;
	}

	/**
	 * Get: cdCctciaIdentificacaoBeneficio.
	 *
	 * @return cdCctciaIdentificacaoBeneficio
	 */
	public int getCdCctciaIdentificacaoBeneficio() {
		return cdCctciaIdentificacaoBeneficio;
	}

	/**
	 * Set: cdCctciaIdentificacaoBeneficio.
	 *
	 * @param cdCctciaIdentificacaoBeneficio the cd cctcia identificacao beneficio
	 */
	public void setCdCctciaIdentificacaoBeneficio(int cdCctciaIdentificacaoBeneficio) {
		this.cdCctciaIdentificacaoBeneficio = cdCctciaIdentificacaoBeneficio;
	}

	/**
	 * Get: cdCctciaInscricaoFavorecido.
	 *
	 * @return cdCctciaInscricaoFavorecido
	 */
	public int getCdCctciaInscricaoFavorecido() {
		return cdCctciaInscricaoFavorecido;
	}

	/**
	 * Set: cdCctciaInscricaoFavorecido.
	 *
	 * @param cdCctciaInscricaoFavorecido the cd cctcia inscricao favorecido
	 */
	public void setCdCctciaInscricaoFavorecido(int cdCctciaInscricaoFavorecido) {
		this.cdCctciaInscricaoFavorecido = cdCctciaInscricaoFavorecido;
	}

	/**
	 * Get: cdCctciaProprietarioVeculo.
	 *
	 * @return cdCctciaProprietarioVeculo
	 */
	public int getCdCctciaProprietarioVeculo() {
		return cdCctciaProprietarioVeculo;
	}

	/**
	 * Set: cdCctciaProprietarioVeculo.
	 *
	 * @param cdCctciaProprietarioVeculo the cd cctcia proprietario veculo
	 */
	public void setCdCctciaProprietarioVeculo(int cdCctciaProprietarioVeculo) {
		this.cdCctciaProprietarioVeculo = cdCctciaProprietarioVeculo;
	}

	/**
	 * Get: cdCobrancaTarifa.
	 *
	 * @return cdCobrancaTarifa
	 */
	public int getCdCobrancaTarifa() {
		return cdCobrancaTarifa;
	}

	/**
	 * Set: cdCobrancaTarifa.
	 *
	 * @param cdCobrancaTarifa the cd cobranca tarifa
	 */
	public void setCdCobrancaTarifa(int cdCobrancaTarifa) {
		this.cdCobrancaTarifa = cdCobrancaTarifa;
	}

	/**
	 * Get: cdConsultaDebitoVeiculo.
	 *
	 * @return cdConsultaDebitoVeiculo
	 */
	public int getCdConsultaDebitoVeiculo() {
		return cdConsultaDebitoVeiculo;
	}

	/**
	 * Set: cdConsultaDebitoVeiculo.
	 *
	 * @param cdConsultaDebitoVeiculo the cd consulta debito veiculo
	 */
	public void setCdConsultaDebitoVeiculo(int cdConsultaDebitoVeiculo) {
		this.cdConsultaDebitoVeiculo = cdConsultaDebitoVeiculo;
	}

	/**
	 * Get: cdConsultaEndereco.
	 *
	 * @return cdConsultaEndereco
	 */
	public int getCdConsultaEndereco() {
		return cdConsultaEndereco;
	}

	/**
	 * Set: cdConsultaEndereco.
	 *
	 * @param cdConsultaEndereco the cd consulta endereco
	 */
	public void setCdConsultaEndereco(int cdConsultaEndereco) {
		this.cdConsultaEndereco = cdConsultaEndereco;
	}

	/**
	 * Get: cdConsultaSaldoPagamento.
	 *
	 * @return cdConsultaSaldoPagamento
	 */
	public int getCdConsultaSaldoPagamento() {
		return cdConsultaSaldoPagamento;
	}

	/**
	 * Set: cdConsultaSaldoPagamento.
	 *
	 * @param cdConsultaSaldoPagamento the cd consulta saldo pagamento
	 */
	public void setCdConsultaSaldoPagamento(int cdConsultaSaldoPagamento) {
		this.cdConsultaSaldoPagamento = cdConsultaSaldoPagamento;
	}

	/**
	 * Get: cdContagemConsultaSaldo.
	 *
	 * @return cdContagemConsultaSaldo
	 */
	public int getCdContagemConsultaSaldo() {
		return cdContagemConsultaSaldo;
	}

	/**
	 * Set: cdContagemConsultaSaldo.
	 *
	 * @param cdContagemConsultaSaldo the cd contagem consulta saldo
	 */
	public void setCdContagemConsultaSaldo(int cdContagemConsultaSaldo) {
		this.cdContagemConsultaSaldo = cdContagemConsultaSaldo;
	}

	/**
	 * Get: cdCreditoNaoUtilizado.
	 *
	 * @return cdCreditoNaoUtilizado
	 */
	public int getCdCreditoNaoUtilizado() {
		return cdCreditoNaoUtilizado;
	}

	/**
	 * Set: cdCreditoNaoUtilizado.
	 *
	 * @param cdCreditoNaoUtilizado the cd credito nao utilizado
	 */
	public void setCdCreditoNaoUtilizado(int cdCreditoNaoUtilizado) {
		this.cdCreditoNaoUtilizado = cdCreditoNaoUtilizado;
	}

	/**
	 * Get: cdCriterioEnquadraRecadastramento.
	 *
	 * @return cdCriterioEnquadraRecadastramento
	 */
	public int getCdCriterioEnquadraRecadastramento() {
		return cdCriterioEnquadraRecadastramento;
	}

	/**
	 * Set: cdCriterioEnquadraRecadastramento.
	 *
	 * @param cdCriterioEnquadraRecadastramento the cd criterio enquadra recadastramento
	 */
	public void setCdCriterioEnquadraRecadastramento(int cdCriterioEnquadraRecadastramento) {
		this.cdCriterioEnquadraRecadastramento = cdCriterioEnquadraRecadastramento;
	}

	/**
	 * Get: cdCriterioEnquandraBeneficio.
	 *
	 * @return cdCriterioEnquandraBeneficio
	 */
	public int getCdCriterioEnquandraBeneficio() {
		return cdCriterioEnquandraBeneficio;
	}

	/**
	 * Set: cdCriterioEnquandraBeneficio.
	 *
	 * @param cdCriterioEnquandraBeneficio the cd criterio enquandra beneficio
	 */
	public void setCdCriterioEnquandraBeneficio(int cdCriterioEnquandraBeneficio) {
		this.cdCriterioEnquandraBeneficio = cdCriterioEnquandraBeneficio;
	}

	/**
	 * Get: cdCriterioRastreabilidadeTitulo.
	 *
	 * @return cdCriterioRastreabilidadeTitulo
	 */
	public int getCdCriterioRastreabilidadeTitulo() {
		return cdCriterioRastreabilidadeTitulo;
	}

	/**
	 * Set: cdCriterioRastreabilidadeTitulo.
	 *
	 * @param cdCriterioRastreabilidadeTitulo the cd criterio rastreabilidade titulo
	 */
	public void setCdCriterioRastreabilidadeTitulo(int cdCriterioRastreabilidadeTitulo) {
		this.cdCriterioRastreabilidadeTitulo = cdCriterioRastreabilidadeTitulo;
	}

	/**
	 * Get: cdDestinoAviso.
	 *
	 * @return cdDestinoAviso
	 */
	public int getCdDestinoAviso() {
		return cdDestinoAviso;
	}

	/**
	 * Set: cdDestinoAviso.
	 *
	 * @param cdDestinoAviso the cd destino aviso
	 */
	public void setCdDestinoAviso(int cdDestinoAviso) {
		this.cdDestinoAviso = cdDestinoAviso;
	}

	/**
	 * Get: cdDestinoComprovante.
	 *
	 * @return cdDestinoComprovante
	 */
	public int getCdDestinoComprovante() {
		return cdDestinoComprovante;
	}

	/**
	 * Set: cdDestinoComprovante.
	 *
	 * @param cdDestinoComprovante the cd destino comprovante
	 */
	public void setCdDestinoComprovante(int cdDestinoComprovante) {
		this.cdDestinoComprovante = cdDestinoComprovante;
	}

	/**
	 * Get: cdDestinoFormularioRecadastramento.
	 *
	 * @return cdDestinoFormularioRecadastramento
	 */
	public int getCdDestinoFormularioRecadastramento() {
		return cdDestinoFormularioRecadastramento;
	}

	/**
	 * Set: cdDestinoFormularioRecadastramento.
	 *
	 * @param cdDestinoFormularioRecadastramento the cd destino formulario recadastramento
	 */
	public void setCdDestinoFormularioRecadastramento(int cdDestinoFormularioRecadastramento) {
		this.cdDestinoFormularioRecadastramento = cdDestinoFormularioRecadastramento;
	}

	/**
	 * Get: cdDisponibilizacaoContaCredito.
	 *
	 * @return cdDisponibilizacaoContaCredito
	 */
	public int getCdDisponibilizacaoContaCredito() {
		return cdDisponibilizacaoContaCredito;
	}

	/**
	 * Set: cdDisponibilizacaoContaCredito.
	 *
	 * @param cdDisponibilizacaoContaCredito the cd disponibilizacao conta credito
	 */
	public void setCdDisponibilizacaoContaCredito(int cdDisponibilizacaoContaCredito) {
		this.cdDisponibilizacaoContaCredito = cdDisponibilizacaoContaCredito;
	}

	/**
	 * Get: cdDisponibilizacaoDiversoCriterio.
	 *
	 * @return cdDisponibilizacaoDiversoCriterio
	 */
	public int getCdDisponibilizacaoDiversoCriterio() {
		return cdDisponibilizacaoDiversoCriterio;
	}

	/**
	 * Set: cdDisponibilizacaoDiversoCriterio.
	 *
	 * @param cdDisponibilizacaoDiversoCriterio the cd disponibilizacao diverso criterio
	 */
	public void setCdDisponibilizacaoDiversoCriterio(int cdDisponibilizacaoDiversoCriterio) {
		this.cdDisponibilizacaoDiversoCriterio = cdDisponibilizacaoDiversoCriterio;
	}

	/**
	 * Get: cdDisponibilizacaoDiversoNao.
	 *
	 * @return cdDisponibilizacaoDiversoNao
	 */
	public int getCdDisponibilizacaoDiversoNao() {
		return cdDisponibilizacaoDiversoNao;
	}

	/**
	 * Set: cdDisponibilizacaoDiversoNao.
	 *
	 * @param cdDisponibilizacaoDiversoNao the cd disponibilizacao diverso nao
	 */
	public void setCdDisponibilizacaoDiversoNao(int cdDisponibilizacaoDiversoNao) {
		this.cdDisponibilizacaoDiversoNao = cdDisponibilizacaoDiversoNao;
	}

	/**
	 * Get: cdDisponibilizacaoSalarioCriterio.
	 *
	 * @return cdDisponibilizacaoSalarioCriterio
	 */
	public int getCdDisponibilizacaoSalarioCriterio() {
		return cdDisponibilizacaoSalarioCriterio;
	}

	/**
	 * Set: cdDisponibilizacaoSalarioCriterio.
	 *
	 * @param cdDisponibilizacaoSalarioCriterio the cd disponibilizacao salario criterio
	 */
	public void setCdDisponibilizacaoSalarioCriterio(int cdDisponibilizacaoSalarioCriterio) {
		this.cdDisponibilizacaoSalarioCriterio = cdDisponibilizacaoSalarioCriterio;
	}

	/**
	 * Get: cdDisponibilizacaoSalarioNao.
	 *
	 * @return cdDisponibilizacaoSalarioNao
	 */
	public int getCdDisponibilizacaoSalarioNao() {
		return cdDisponibilizacaoSalarioNao;
	}

	/**
	 * Set: cdDisponibilizacaoSalarioNao.
	 *
	 * @param cdDisponibilizacaoSalarioNao the cd disponibilizacao salario nao
	 */
	public void setCdDisponibilizacaoSalarioNao(int cdDisponibilizacaoSalarioNao) {
		this.cdDisponibilizacaoSalarioNao = cdDisponibilizacaoSalarioNao;
	}

	/**
	 * Get: cdEnvelopeAberto.
	 *
	 * @return cdEnvelopeAberto
	 */
	public int getCdEnvelopeAberto() {
		return cdEnvelopeAberto;
	}

	/**
	 * Set: cdEnvelopeAberto.
	 *
	 * @param cdEnvelopeAberto the cd envelope aberto
	 */
	public void setCdEnvelopeAberto(int cdEnvelopeAberto) {
		this.cdEnvelopeAberto = cdEnvelopeAberto;
	}

	/**
	 * Get: cdFavorecidoConsultaPagamento.
	 *
	 * @return cdFavorecidoConsultaPagamento
	 */
	public int getCdFavorecidoConsultaPagamento() {
		return cdFavorecidoConsultaPagamento;
	}

	/**
	 * Set: cdFavorecidoConsultaPagamento.
	 *
	 * @param cdFavorecidoConsultaPagamento the cd favorecido consulta pagamento
	 */
	public void setCdFavorecidoConsultaPagamento(int cdFavorecidoConsultaPagamento) {
		this.cdFavorecidoConsultaPagamento = cdFavorecidoConsultaPagamento;
	}

	/**
	 * Get: cdFormaAutorizacaoPagamento.
	 *
	 * @return cdFormaAutorizacaoPagamento
	 */
	public int getCdFormaAutorizacaoPagamento() {
		return cdFormaAutorizacaoPagamento;
	}

	/**
	 * Set: cdFormaAutorizacaoPagamento.
	 *
	 * @param cdFormaAutorizacaoPagamento the cd forma autorizacao pagamento
	 */
	public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento) {
		this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
	}

	/**
	 * Get: cdFormaEnvioPagamento.
	 *
	 * @return cdFormaEnvioPagamento
	 */
	public int getCdFormaEnvioPagamento() {
		return cdFormaEnvioPagamento;
	}

	/**
	 * Set: cdFormaEnvioPagamento.
	 *
	 * @param cdFormaEnvioPagamento the cd forma envio pagamento
	 */
	public void setCdFormaEnvioPagamento(int cdFormaEnvioPagamento) {
		this.cdFormaEnvioPagamento = cdFormaEnvioPagamento;
	}

	/**
	 * Get: cdFormaEstornoCredito.
	 *
	 * @return cdFormaEstornoCredito
	 */
	public int getCdFormaEstornoCredito() {
		return cdFormaEstornoCredito;
	}

	/**
	 * Set: cdFormaEstornoCredito.
	 *
	 * @param cdFormaEstornoCredito the cd forma estorno credito
	 */
	public void setCdFormaEstornoCredito(int cdFormaEstornoCredito) {
		this.cdFormaEstornoCredito = cdFormaEstornoCredito;
	}

	/**
	 * Get: cdFormaExpiracaoCredito.
	 *
	 * @return cdFormaExpiracaoCredito
	 */
	public int getCdFormaExpiracaoCredito() {
		return cdFormaExpiracaoCredito;
	}

	/**
	 * Set: cdFormaExpiracaoCredito.
	 *
	 * @param cdFormaExpiracaoCredito the cd forma expiracao credito
	 */
	public void setCdFormaExpiracaoCredito(int cdFormaExpiracaoCredito) {
		this.cdFormaExpiracaoCredito = cdFormaExpiracaoCredito;
	}

	/**
	 * Get: cdFormaManutencao.
	 *
	 * @return cdFormaManutencao
	 */
	public int getCdFormaManutencao() {
		return cdFormaManutencao;
	}

	/**
	 * Set: cdFormaManutencao.
	 *
	 * @param cdFormaManutencao the cd forma manutencao
	 */
	public void setCdFormaManutencao(int cdFormaManutencao) {
		this.cdFormaManutencao = cdFormaManutencao;
	}

	/**
	 * Get: cdFrasePrecadastrada.
	 *
	 * @return cdFrasePrecadastrada
	 */
	public int getCdFrasePrecadastrada() {
		return cdFrasePrecadastrada;
	}

	/**
	 * Set: cdFrasePrecadastrada.
	 *
	 * @param cdFrasePrecadastrada the cd frase precadastrada
	 */
	public void setCdFrasePrecadastrada(int cdFrasePrecadastrada) {
		this.cdFrasePrecadastrada = cdFrasePrecadastrada;
	}

	/**
	 * Get: cdIndicadorAgendamentoTitulo.
	 *
	 * @return cdIndicadorAgendamentoTitulo
	 */
	public int getCdIndicadorAgendamentoTitulo() {
		return cdIndicadorAgendamentoTitulo;
	}

	/**
	 * Set: cdIndicadorAgendamentoTitulo.
	 *
	 * @param cdIndicadorAgendamentoTitulo the cd indicador agendamento titulo
	 */
	public void setCdIndicadorAgendamentoTitulo(int cdIndicadorAgendamentoTitulo) {
		this.cdIndicadorAgendamentoTitulo = cdIndicadorAgendamentoTitulo;
	}

	/**
	 * Get: cdIndicadorAutorizacaoCliente.
	 *
	 * @return cdIndicadorAutorizacaoCliente
	 */
	public int getCdIndicadorAutorizacaoCliente() {
		return cdIndicadorAutorizacaoCliente;
	}

	/**
	 * Set: cdIndicadorAutorizacaoCliente.
	 *
	 * @param cdIndicadorAutorizacaoCliente the cd indicador autorizacao cliente
	 */
	public void setCdIndicadorAutorizacaoCliente(int cdIndicadorAutorizacaoCliente) {
		this.cdIndicadorAutorizacaoCliente = cdIndicadorAutorizacaoCliente;
	}

	/**
	 * Get: cdIndicadorAutorizacaoComplemento.
	 *
	 * @return cdIndicadorAutorizacaoComplemento
	 */
	public int getCdIndicadorAutorizacaoComplemento() {
		return cdIndicadorAutorizacaoComplemento;
	}

	/**
	 * Set: cdIndicadorAutorizacaoComplemento.
	 *
	 * @param cdIndicadorAutorizacaoComplemento the cd indicador autorizacao complemento
	 */
	public void setCdIndicadorAutorizacaoComplemento(int cdIndicadorAutorizacaoComplemento) {
		this.cdIndicadorAutorizacaoComplemento = cdIndicadorAutorizacaoComplemento;
	}

	/**
	 * Get: cdIndicadorCadastroorganizacao.
	 *
	 * @return cdIndicadorCadastroorganizacao
	 */
	public int getCdIndicadorCadastroorganizacao() {
		return cdIndicadorCadastroorganizacao;
	}

	/**
	 * Set: cdIndicadorCadastroorganizacao.
	 *
	 * @param cdIndicadorCadastroorganizacao the cd indicador cadastroorganizacao
	 */
	public void setCdIndicadorCadastroorganizacao(int cdIndicadorCadastroorganizacao) {
		this.cdIndicadorCadastroorganizacao = cdIndicadorCadastroorganizacao;
	}

	/**
	 * Get: cdIndicadorCadastroProcurador.
	 *
	 * @return cdIndicadorCadastroProcurador
	 */
	public int getCdIndicadorCadastroProcurador() {
		return cdIndicadorCadastroProcurador;
	}

	/**
	 * Set: cdIndicadorCadastroProcurador.
	 *
	 * @param cdIndicadorCadastroProcurador the cd indicador cadastro procurador
	 */
	public void setCdIndicadorCadastroProcurador(int cdIndicadorCadastroProcurador) {
		this.cdIndicadorCadastroProcurador = cdIndicadorCadastroProcurador;
	}

	/**
	 * Get: cdIndicadorCartaoSalario.
	 *
	 * @return cdIndicadorCartaoSalario
	 */
	public int getCdIndicadorCartaoSalario() {
		return cdIndicadorCartaoSalario;
	}

	/**
	 * Set: cdIndicadorCartaoSalario.
	 *
	 * @param cdIndicadorCartaoSalario the cd indicador cartao salario
	 */
	public void setCdIndicadorCartaoSalario(int cdIndicadorCartaoSalario) {
		this.cdIndicadorCartaoSalario = cdIndicadorCartaoSalario;
	}

	/**
	 * Get: cdIndicadorEconomicoReajuste.
	 *
	 * @return cdIndicadorEconomicoReajuste
	 */
	public int getCdIndicadorEconomicoReajuste() {
		return cdIndicadorEconomicoReajuste;
	}

	/**
	 * Set: cdIndicadorEconomicoReajuste.
	 *
	 * @param cdIndicadorEconomicoReajuste the cd indicador economico reajuste
	 */
	public void setCdIndicadorEconomicoReajuste(int cdIndicadorEconomicoReajuste) {
		this.cdIndicadorEconomicoReajuste = cdIndicadorEconomicoReajuste;
	}

	/**
	 * Get: cdindicadorExpiraCredito.
	 *
	 * @return cdindicadorExpiraCredito
	 */
	public int getCdindicadorExpiraCredito() {
		return cdindicadorExpiraCredito;
	}

	/**
	 * Set: cdindicadorExpiraCredito.
	 *
	 * @param cdindicadorExpiraCredito the cdindicador expira credito
	 */
	public void setCdindicadorExpiraCredito(int cdindicadorExpiraCredito) {
		this.cdindicadorExpiraCredito = cdindicadorExpiraCredito;
	}

	/**
	 * Get: cdindicadorLancamentoProgramado.
	 *
	 * @return cdindicadorLancamentoProgramado
	 */
	public int getCdindicadorLancamentoProgramado() {
		return cdindicadorLancamentoProgramado;
	}

	/**
	 * Set: cdindicadorLancamentoProgramado.
	 *
	 * @param cdindicadorLancamentoProgramado the cdindicador lancamento programado
	 */
	public void setCdindicadorLancamentoProgramado(int cdindicadorLancamentoProgramado) {
		this.cdindicadorLancamentoProgramado = cdindicadorLancamentoProgramado;
	}

	/**
	 * Get: cdIndicadorMensagemPersonalizada.
	 *
	 * @return cdIndicadorMensagemPersonalizada
	 */
	public int getCdIndicadorMensagemPersonalizada() {
		return cdIndicadorMensagemPersonalizada;
	}

	/**
	 * Set: cdIndicadorMensagemPersonalizada.
	 *
	 * @param cdIndicadorMensagemPersonalizada the cd indicador mensagem personalizada
	 */
	public void setCdIndicadorMensagemPersonalizada(int cdIndicadorMensagemPersonalizada) {
		this.cdIndicadorMensagemPersonalizada = cdIndicadorMensagemPersonalizada;
	}

	/**
	 * Get: cdLancamentoFuturoCredito.
	 *
	 * @return cdLancamentoFuturoCredito
	 */
	public int getCdLancamentoFuturoCredito() {
		return cdLancamentoFuturoCredito;
	}

	/**
	 * Set: cdLancamentoFuturoCredito.
	 *
	 * @param cdLancamentoFuturoCredito the cd lancamento futuro credito
	 */
	public void setCdLancamentoFuturoCredito(int cdLancamentoFuturoCredito) {
		this.cdLancamentoFuturoCredito = cdLancamentoFuturoCredito;
	}

	/**
	 * Get: cdLancamentoFuturoDebito.
	 *
	 * @return cdLancamentoFuturoDebito
	 */
	public int getCdLancamentoFuturoDebito() {
		return cdLancamentoFuturoDebito;
	}

	/**
	 * Set: cdLancamentoFuturoDebito.
	 *
	 * @param cdLancamentoFuturoDebito the cd lancamento futuro debito
	 */
	public void setCdLancamentoFuturoDebito(int cdLancamentoFuturoDebito) {
		this.cdLancamentoFuturoDebito = cdLancamentoFuturoDebito;
	}

	/**
	 * Get: cdLiberacaoLoteProcessado.
	 *
	 * @return cdLiberacaoLoteProcessado
	 */
	public int getCdLiberacaoLoteProcessado() {
		return cdLiberacaoLoteProcessado;
	}

	/**
	 * Set: cdLiberacaoLoteProcessado.
	 *
	 * @param cdLiberacaoLoteProcessado the cd liberacao lote processado
	 */
	public void setCdLiberacaoLoteProcessado(int cdLiberacaoLoteProcessado) {
		this.cdLiberacaoLoteProcessado = cdLiberacaoLoteProcessado;
	}

	/**
	 * Get: cdManutencaoBaseRecadastramento.
	 *
	 * @return cdManutencaoBaseRecadastramento
	 */
	public int getCdManutencaoBaseRecadastramento() {
		return cdManutencaoBaseRecadastramento;
	}

	/**
	 * Set: cdManutencaoBaseRecadastramento.
	 *
	 * @param cdManutencaoBaseRecadastramento the cd manutencao base recadastramento
	 */
	public void setCdManutencaoBaseRecadastramento(int cdManutencaoBaseRecadastramento) {
		this.cdManutencaoBaseRecadastramento = cdManutencaoBaseRecadastramento;
	}

	/**
	 * Get: cdMeioPagamentoDebito.
	 *
	 * @return cdMeioPagamentoDebito
	 */
	public int getCdMeioPagamentoDebito() {
		return cdMeioPagamentoDebito;
	}

	/**
	 * Set: cdMeioPagamentoDebito.
	 *
	 * @param cdMeioPagamentoDebito the cd meio pagamento debito
	 */
	public void setCdMeioPagamentoDebito(int cdMeioPagamentoDebito) {
		this.cdMeioPagamentoDebito = cdMeioPagamentoDebito;
	}

	/**
	 * Get: cdMensagemRecadastramentoMidia.
	 *
	 * @return cdMensagemRecadastramentoMidia
	 */
	public int getCdMensagemRecadastramentoMidia() {
		return cdMensagemRecadastramentoMidia;
	}

	/**
	 * Set: cdMensagemRecadastramentoMidia.
	 *
	 * @param cdMensagemRecadastramentoMidia the cd mensagem recadastramento midia
	 */
	public void setCdMensagemRecadastramentoMidia(int cdMensagemRecadastramentoMidia) {
		this.cdMensagemRecadastramentoMidia = cdMensagemRecadastramentoMidia;
	}

	/**
	 * Get: cdMidiaDisponivel.
	 *
	 * @return cdMidiaDisponivel
	 */
	public int getCdMidiaDisponivel() {
		return cdMidiaDisponivel;
	}

	/**
	 * Set: cdMidiaDisponivel.
	 *
	 * @param cdMidiaDisponivel the cd midia disponivel
	 */
	public void setCdMidiaDisponivel(int cdMidiaDisponivel) {
		this.cdMidiaDisponivel = cdMidiaDisponivel;
	}

	/**
	 * Get: cdMidiaMensagemRecadastramento.
	 *
	 * @return cdMidiaMensagemRecadastramento
	 */
	public int getCdMidiaMensagemRecadastramento() {
		return cdMidiaMensagemRecadastramento;
	}

	/**
	 * Set: cdMidiaMensagemRecadastramento.
	 *
	 * @param cdMidiaMensagemRecadastramento the cd midia mensagem recadastramento
	 */
	public void setCdMidiaMensagemRecadastramento(int cdMidiaMensagemRecadastramento) {
		this.cdMidiaMensagemRecadastramento = cdMidiaMensagemRecadastramento;
	}

	/**
	 * Get: cdMomentoAvisoRecadastramento.
	 *
	 * @return cdMomentoAvisoRecadastramento
	 */
	public int getCdMomentoAvisoRecadastramento() {
		return cdMomentoAvisoRecadastramento;
	}

	/**
	 * Set: cdMomentoAvisoRecadastramento.
	 *
	 * @param cdMomentoAvisoRecadastramento the cd momento aviso recadastramento
	 */
	public void setCdMomentoAvisoRecadastramento(int cdMomentoAvisoRecadastramento) {
		this.cdMomentoAvisoRecadastramento = cdMomentoAvisoRecadastramento;
	}

	/**
	 * Get: cdMomentoCreditoEfetivacao.
	 *
	 * @return cdMomentoCreditoEfetivacao
	 */
	public int getCdMomentoCreditoEfetivacao() {
		return cdMomentoCreditoEfetivacao;
	}

	/**
	 * Set: cdMomentoCreditoEfetivacao.
	 *
	 * @param cdMomentoCreditoEfetivacao the cd momento credito efetivacao
	 */
	public void setCdMomentoCreditoEfetivacao(int cdMomentoCreditoEfetivacao) {
		this.cdMomentoCreditoEfetivacao = cdMomentoCreditoEfetivacao;
	}

	/**
	 * Get: cdMomentoDebitoPagamento.
	 *
	 * @return cdMomentoDebitoPagamento
	 */
	public int getCdMomentoDebitoPagamento() {
		return cdMomentoDebitoPagamento;
	}

	/**
	 * Set: cdMomentoDebitoPagamento.
	 *
	 * @param cdMomentoDebitoPagamento the cd momento debito pagamento
	 */
	public void setCdMomentoDebitoPagamento(int cdMomentoDebitoPagamento) {
		this.cdMomentoDebitoPagamento = cdMomentoDebitoPagamento;
	}

	/**
	 * Get: cdMomentoFormularioRecadastramento.
	 *
	 * @return cdMomentoFormularioRecadastramento
	 */
	public int getCdMomentoFormularioRecadastramento() {
		return cdMomentoFormularioRecadastramento;
	}

	/**
	 * Set: cdMomentoFormularioRecadastramento.
	 *
	 * @param cdMomentoFormularioRecadastramento the cd momento formulario recadastramento
	 */
	public void setCdMomentoFormularioRecadastramento(int cdMomentoFormularioRecadastramento) {
		this.cdMomentoFormularioRecadastramento = cdMomentoFormularioRecadastramento;
	}

	/**
	 * Get: cdMomentoProcessamentoPagamento.
	 *
	 * @return cdMomentoProcessamentoPagamento
	 */
	public int getCdMomentoProcessamentoPagamento() {
		return cdMomentoProcessamentoPagamento;
	}

	/**
	 * Set: cdMomentoProcessamentoPagamento.
	 *
	 * @param cdMomentoProcessamentoPagamento the cd momento processamento pagamento
	 */
	public void setCdMomentoProcessamentoPagamento(int cdMomentoProcessamentoPagamento) {
		this.cdMomentoProcessamentoPagamento = cdMomentoProcessamentoPagamento;
	}

	/**
	 * Get: cdNaturezaOperacaoPagamento.
	 *
	 * @return cdNaturezaOperacaoPagamento
	 */
	public int getCdNaturezaOperacaoPagamento() {
		return cdNaturezaOperacaoPagamento;
	}

	/**
	 * Set: cdNaturezaOperacaoPagamento.
	 *
	 * @param cdNaturezaOperacaoPagamento the cd natureza operacao pagamento
	 */
	public void setCdNaturezaOperacaoPagamento(int cdNaturezaOperacaoPagamento) {
		this.cdNaturezaOperacaoPagamento = cdNaturezaOperacaoPagamento;
	}

	/**
	 * Get: cdOutraidentificacaoFavorecido.
	 *
	 * @return cdOutraidentificacaoFavorecido
	 */
	public int getCdOutraidentificacaoFavorecido() {
		return cdOutraidentificacaoFavorecido;
	}

	/**
	 * Set: cdOutraidentificacaoFavorecido.
	 *
	 * @param cdOutraidentificacaoFavorecido the cd outraidentificacao favorecido
	 */
	public void setCdOutraidentificacaoFavorecido(int cdOutraidentificacaoFavorecido) {
		this.cdOutraidentificacaoFavorecido = cdOutraidentificacaoFavorecido;
	}

	/**
	 * Get: cdPagamentoNaoUtil.
	 *
	 * @return cdPagamentoNaoUtil
	 */
	public int getCdPagamentoNaoUtil() {
		return cdPagamentoNaoUtil;
	}

	/**
	 * Set: cdPagamentoNaoUtil.
	 *
	 * @param cdPagamentoNaoUtil the cd pagamento nao util
	 */
	public void setCdPagamentoNaoUtil(int cdPagamentoNaoUtil) {
		this.cdPagamentoNaoUtil = cdPagamentoNaoUtil;
	}

	/**
	 * Get: cdPeriodicidadeAviso.
	 *
	 * @return cdPeriodicidadeAviso
	 */
	public int getCdPeriodicidadeAviso() {
		return cdPeriodicidadeAviso;
	}

	/**
	 * Set: cdPeriodicidadeAviso.
	 *
	 * @param cdPeriodicidadeAviso the cd periodicidade aviso
	 */
	public void setCdPeriodicidadeAviso(int cdPeriodicidadeAviso) {
		this.cdPeriodicidadeAviso = cdPeriodicidadeAviso;
	}

	/**
	 * Get: cdPeriodicidadeCobrancaTarifa.
	 *
	 * @return cdPeriodicidadeCobrancaTarifa
	 */
	public int getCdPeriodicidadeCobrancaTarifa() {
		return cdPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Set: cdPeriodicidadeCobrancaTarifa.
	 *
	 * @param cdPeriodicidadeCobrancaTarifa the cd periodicidade cobranca tarifa
	 */
	public void setCdPeriodicidadeCobrancaTarifa(int cdPeriodicidadeCobrancaTarifa) {
		this.cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Get: cdPeriodicidadeComprovante.
	 *
	 * @return cdPeriodicidadeComprovante
	 */
	public int getCdPeriodicidadeComprovante() {
		return cdPeriodicidadeComprovante;
	}

	/**
	 * Set: cdPeriodicidadeComprovante.
	 *
	 * @param cdPeriodicidadeComprovante the cd periodicidade comprovante
	 */
	public void setCdPeriodicidadeComprovante(int cdPeriodicidadeComprovante) {
		this.cdPeriodicidadeComprovante = cdPeriodicidadeComprovante;
	}

	/**
	 * Get: cdPeriodicidadeConsultaVeiculo.
	 *
	 * @return cdPeriodicidadeConsultaVeiculo
	 */
	public int getCdPeriodicidadeConsultaVeiculo() {
		return cdPeriodicidadeConsultaVeiculo;
	}

	/**
	 * Set: cdPeriodicidadeConsultaVeiculo.
	 *
	 * @param cdPeriodicidadeConsultaVeiculo the cd periodicidade consulta veiculo
	 */
	public void setCdPeriodicidadeConsultaVeiculo(int cdPeriodicidadeConsultaVeiculo) {
		this.cdPeriodicidadeConsultaVeiculo = cdPeriodicidadeConsultaVeiculo;
	}

	/**
	 * Get: cdPeriodicidadeEnvioRemessa.
	 *
	 * @return cdPeriodicidadeEnvioRemessa
	 */
	public int getCdPeriodicidadeEnvioRemessa() {
		return cdPeriodicidadeEnvioRemessa;
	}

	/**
	 * Set: cdPeriodicidadeEnvioRemessa.
	 *
	 * @param cdPeriodicidadeEnvioRemessa the cd periodicidade envio remessa
	 */
	public void setCdPeriodicidadeEnvioRemessa(int cdPeriodicidadeEnvioRemessa) {
		this.cdPeriodicidadeEnvioRemessa = cdPeriodicidadeEnvioRemessa;
	}

	/**
	 * Get: cdPeriodicidadeManutencaoProcd.
	 *
	 * @return cdPeriodicidadeManutencaoProcd
	 */
	public int getCdPeriodicidadeManutencaoProcd() {
		return cdPeriodicidadeManutencaoProcd;
	}

	/**
	 * Set: cdPeriodicidadeManutencaoProcd.
	 *
	 * @param cdPeriodicidadeManutencaoProcd the cd periodicidade manutencao procd
	 */
	public void setCdPeriodicidadeManutencaoProcd(int cdPeriodicidadeManutencaoProcd) {
		this.cdPeriodicidadeManutencaoProcd = cdPeriodicidadeManutencaoProcd;
	}

	/**
	 * Get: cdPeriodicidadeReajusteTarifa.
	 *
	 * @return cdPeriodicidadeReajusteTarifa
	 */
	public int getCdPeriodicidadeReajusteTarifa() {
		return cdPeriodicidadeReajusteTarifa;
	}

	/**
	 * Set: cdPeriodicidadeReajusteTarifa.
	 *
	 * @param cdPeriodicidadeReajusteTarifa the cd periodicidade reajuste tarifa
	 */
	public void setCdPeriodicidadeReajusteTarifa(int cdPeriodicidadeReajusteTarifa) {
		this.cdPeriodicidadeReajusteTarifa = cdPeriodicidadeReajusteTarifa;
	}

	/**
	 * Get: cdPermissaoDebitoOnline.
	 *
	 * @return cdPermissaoDebitoOnline
	 */
	public int getCdPermissaoDebitoOnline() {
		return cdPermissaoDebitoOnline;
	}

	/**
	 * Set: cdPermissaoDebitoOnline.
	 *
	 * @param cdPermissaoDebitoOnline the cd permissao debito online
	 */
	public void setCdPermissaoDebitoOnline(int cdPermissaoDebitoOnline) {
		this.cdPermissaoDebitoOnline = cdPermissaoDebitoOnline;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdPrincipalEnquaRecadastramento.
	 *
	 * @return cdPrincipalEnquaRecadastramento
	 */
	public int getCdPrincipalEnquaRecadastramento() {
		return cdPrincipalEnquaRecadastramento;
	}

	/**
	 * Set: cdPrincipalEnquaRecadastramento.
	 *
	 * @param cdPrincipalEnquaRecadastramento the cd principal enqua recadastramento
	 */
	public void setCdPrincipalEnquaRecadastramento(int cdPrincipalEnquaRecadastramento) {
		this.cdPrincipalEnquaRecadastramento = cdPrincipalEnquaRecadastramento;
	}

	/**
	 * Get: cdPrioridadeEfetivacaoPagamento.
	 *
	 * @return cdPrioridadeEfetivacaoPagamento
	 */
	public int getCdPrioridadeEfetivacaoPagamento() {
		return cdPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Set: cdPrioridadeEfetivacaoPagamento.
	 *
	 * @param cdPrioridadeEfetivacaoPagamento the cd prioridade efetivacao pagamento
	 */
	public void setCdPrioridadeEfetivacaoPagamento(int cdPrioridadeEfetivacaoPagamento) {
		this.cdPrioridadeEfetivacaoPagamento = cdPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public int getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public int getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}

	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}

	/**
	 * Get: cdRastreabilidadeNotaFiscal.
	 *
	 * @return cdRastreabilidadeNotaFiscal
	 */
	public int getCdRastreabilidadeNotaFiscal() {
		return cdRastreabilidadeNotaFiscal;
	}

	/**
	 * Set: cdRastreabilidadeNotaFiscal.
	 *
	 * @param cdRastreabilidadeNotaFiscal the cd rastreabilidade nota fiscal
	 */
	public void setCdRastreabilidadeNotaFiscal(int cdRastreabilidadeNotaFiscal) {
		this.cdRastreabilidadeNotaFiscal = cdRastreabilidadeNotaFiscal;
	}

	/**
	 * Get: cdRastreabilidadeTituloTerceiro.
	 *
	 * @return cdRastreabilidadeTituloTerceiro
	 */
	public int getCdRastreabilidadeTituloTerceiro() {
		return cdRastreabilidadeTituloTerceiro;
	}

	/**
	 * Set: cdRastreabilidadeTituloTerceiro.
	 *
	 * @param cdRastreabilidadeTituloTerceiro the cd rastreabilidade titulo terceiro
	 */
	public void setCdRastreabilidadeTituloTerceiro(int cdRastreabilidadeTituloTerceiro) {
		this.cdRastreabilidadeTituloTerceiro = cdRastreabilidadeTituloTerceiro;
	}

	/**
	 * Get: cdRejeicaoAgendamentoLote.
	 *
	 * @return cdRejeicaoAgendamentoLote
	 */
	public int getCdRejeicaoAgendamentoLote() {
		return cdRejeicaoAgendamentoLote;
	}

	/**
	 * Set: cdRejeicaoAgendamentoLote.
	 *
	 * @param cdRejeicaoAgendamentoLote the cd rejeicao agendamento lote
	 */
	public void setCdRejeicaoAgendamentoLote(int cdRejeicaoAgendamentoLote) {
		this.cdRejeicaoAgendamentoLote = cdRejeicaoAgendamentoLote;
	}

	/**
	 * Get: cdRejeicaoEfetivacaoLote.
	 *
	 * @return cdRejeicaoEfetivacaoLote
	 */
	public int getCdRejeicaoEfetivacaoLote() {
		return cdRejeicaoEfetivacaoLote;
	}

	/**
	 * Set: cdRejeicaoEfetivacaoLote.
	 *
	 * @param cdRejeicaoEfetivacaoLote the cd rejeicao efetivacao lote
	 */
	public void setCdRejeicaoEfetivacaoLote(int cdRejeicaoEfetivacaoLote) {
		this.cdRejeicaoEfetivacaoLote = cdRejeicaoEfetivacaoLote;
	}

	/**
	 * Get: cdRejeicaoLote.
	 *
	 * @return cdRejeicaoLote
	 */
	public int getCdRejeicaoLote() {
		return cdRejeicaoLote;
	}

	/**
	 * Set: cdRejeicaoLote.
	 *
	 * @param cdRejeicaoLote the cd rejeicao lote
	 */
	public void setCdRejeicaoLote(int cdRejeicaoLote) {
		this.cdRejeicaoLote = cdRejeicaoLote;
	}

	/**
	 * Get: cdTipoCargaRecadastramento.
	 *
	 * @return cdTipoCargaRecadastramento
	 */
	public int getCdTipoCargaRecadastramento() {
		return cdTipoCargaRecadastramento;
	}

	/**
	 * Set: cdTipoCargaRecadastramento.
	 *
	 * @param cdTipoCargaRecadastramento the cd tipo carga recadastramento
	 */
	public void setCdTipoCargaRecadastramento(int cdTipoCargaRecadastramento) {
		this.cdTipoCargaRecadastramento = cdTipoCargaRecadastramento;
	}

	/**
	 * Get: cdTipoCartaoSalario.
	 *
	 * @return cdTipoCartaoSalario
	 */
	public int getCdTipoCartaoSalario() {
		return cdTipoCartaoSalario;
	}

	/**
	 * Set: cdTipoCartaoSalario.
	 *
	 * @param cdTipoCartaoSalario the cd tipo cartao salario
	 */
	public void setCdTipoCartaoSalario(int cdTipoCartaoSalario) {
		this.cdTipoCartaoSalario = cdTipoCartaoSalario;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: cdTipoDataFloating.
	 *
	 * @return cdTipoDataFloating
	 */
	public int getCdTipoDataFloating() {
		return cdTipoDataFloating;
	}

	/**
	 * Set: cdTipoDataFloating.
	 *
	 * @param cdTipoDataFloating the cd tipo data floating
	 */
	public void setCdTipoDataFloating(int cdTipoDataFloating) {
		this.cdTipoDataFloating = cdTipoDataFloating;
	}

	/**
	 * Get: cdTipoDivergenciaVeiculo.
	 *
	 * @return cdTipoDivergenciaVeiculo
	 */
	public int getCdTipoDivergenciaVeiculo() {
		return cdTipoDivergenciaVeiculo;
	}

	/**
	 * Set: cdTipoDivergenciaVeiculo.
	 *
	 * @param cdTipoDivergenciaVeiculo the cd tipo divergencia veiculo
	 */
	public void setCdTipoDivergenciaVeiculo(int cdTipoDivergenciaVeiculo) {
		this.cdTipoDivergenciaVeiculo = cdTipoDivergenciaVeiculo;
	}

	/**
	 * Get: cdTipoEfetivacaoPagamento.
	 *
	 * @return cdTipoEfetivacaoPagamento
	 */
	public int getCdTipoEfetivacaoPagamento() {
		return cdTipoEfetivacaoPagamento;
	}

	/**
	 * Set: cdTipoEfetivacaoPagamento.
	 *
	 * @param cdTipoEfetivacaoPagamento the cd tipo efetivacao pagamento
	 */
	public void setCdTipoEfetivacaoPagamento(int cdTipoEfetivacaoPagamento) {
		this.cdTipoEfetivacaoPagamento = cdTipoEfetivacaoPagamento;
	}

	/**
	 * Get: cdTipoIdentificacaoBeneficio.
	 *
	 * @return cdTipoIdentificacaoBeneficio
	 */
	public int getCdTipoIdentificacaoBeneficio() {
		return cdTipoIdentificacaoBeneficio;
	}

	/**
	 * Set: cdTipoIdentificacaoBeneficio.
	 *
	 * @param cdTipoIdentificacaoBeneficio the cd tipo identificacao beneficio
	 */
	public void setCdTipoIdentificacaoBeneficio(int cdTipoIdentificacaoBeneficio) {
		this.cdTipoIdentificacaoBeneficio = cdTipoIdentificacaoBeneficio;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public int getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdTipoReajusteTarifa.
	 *
	 * @return cdTipoReajusteTarifa
	 */
	public int getCdTipoReajusteTarifa() {
		return cdTipoReajusteTarifa;
	}

	/**
	 * Set: cdTipoReajusteTarifa.
	 *
	 * @param cdTipoReajusteTarifa the cd tipo reajuste tarifa
	 */
	public void setCdTipoReajusteTarifa(int cdTipoReajusteTarifa) {
		this.cdTipoReajusteTarifa = cdTipoReajusteTarifa;
	}

	/**
	 * Get: cdTratamentoContaTransferida.
	 *
	 * @return cdTratamentoContaTransferida
	 */
	public int getCdTratamentoContaTransferida() {
		return cdTratamentoContaTransferida;
	}

	/**
	 * Set: cdTratamentoContaTransferida.
	 *
	 * @param cdTratamentoContaTransferida the cd tratamento conta transferida
	 */
	public void setCdTratamentoContaTransferida(int cdTratamentoContaTransferida) {
		this.cdTratamentoContaTransferida = cdTratamentoContaTransferida;
	}

	/**
	 * Get: cdUsuarioAlteracao.
	 *
	 * @return cdUsuarioAlteracao
	 */
	public String getCdUsuarioAlteracao() {
		return cdUsuarioAlteracao;
	}

	/**
	 * Set: cdUsuarioAlteracao.
	 *
	 * @param cdUsuarioAlteracao the cd usuario alteracao
	 */
	public void setCdUsuarioAlteracao(String cdUsuarioAlteracao) {
		this.cdUsuarioAlteracao = cdUsuarioAlteracao;
	}

	/**
	 * Get: cdUsuarioExternoAlteracao.
	 *
	 * @return cdUsuarioExternoAlteracao
	 */
	public String getCdUsuarioExternoAlteracao() {
		return cdUsuarioExternoAlteracao;
	}

	/**
	 * Set: cdUsuarioExternoAlteracao.
	 *
	 * @param cdUsuarioExternoAlteracao the cd usuario externo alteracao
	 */
	public void setCdUsuarioExternoAlteracao(String cdUsuarioExternoAlteracao) {
		this.cdUsuarioExternoAlteracao = cdUsuarioExternoAlteracao;
	}

	/**
	 * Get: cdUsuarioExternoInclusao.
	 *
	 * @return cdUsuarioExternoInclusao
	 */
	public String getCdUsuarioExternoInclusao() {
		return cdUsuarioExternoInclusao;
	}

	/**
	 * Set: cdUsuarioExternoInclusao.
	 *
	 * @param cdUsuarioExternoInclusao the cd usuario externo inclusao
	 */
	public void setCdUsuarioExternoInclusao(String cdUsuarioExternoInclusao) {
		this.cdUsuarioExternoInclusao = cdUsuarioExternoInclusao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUtilizacaoFavorecidoControle.
	 *
	 * @return cdUtilizacaoFavorecidoControle
	 */
	public int getCdUtilizacaoFavorecidoControle() {
		return cdUtilizacaoFavorecidoControle;
	}

	/**
	 * Set: cdUtilizacaoFavorecidoControle.
	 *
	 * @param cdUtilizacaoFavorecidoControle the cd utilizacao favorecido controle
	 */
	public void setCdUtilizacaoFavorecidoControle(int cdUtilizacaoFavorecidoControle) {
		this.cdUtilizacaoFavorecidoControle = cdUtilizacaoFavorecidoControle;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsAcaoNaoVida.
	 *
	 * @return dsAcaoNaoVida
	 */
	public String getDsAcaoNaoVida() {
		return dsAcaoNaoVida;
	}

	/**
	 * Set: dsAcaoNaoVida.
	 *
	 * @param dsAcaoNaoVida the ds acao nao vida
	 */
	public void setDsAcaoNaoVida(String dsAcaoNaoVida) {
		this.dsAcaoNaoVida = dsAcaoNaoVida;
	}

	/**
	 * Get: dsAcertoDadosRecadastramento.
	 *
	 * @return dsAcertoDadosRecadastramento
	 */
	public String getDsAcertoDadosRecadastramento() {
		return dsAcertoDadosRecadastramento;
	}

	/**
	 * Set: dsAcertoDadosRecadastramento.
	 *
	 * @param dsAcertoDadosRecadastramento the ds acerto dados recadastramento
	 */
	public void setDsAcertoDadosRecadastramento(String dsAcertoDadosRecadastramento) {
		this.dsAcertoDadosRecadastramento = dsAcertoDadosRecadastramento;
	}

	/**
	 * Get: dsAgendamentoDebitoVeiculo.
	 *
	 * @return dsAgendamentoDebitoVeiculo
	 */
	public String getDsAgendamentoDebitoVeiculo() {
		return dsAgendamentoDebitoVeiculo;
	}

	/**
	 * Set: dsAgendamentoDebitoVeiculo.
	 *
	 * @param dsAgendamentoDebitoVeiculo the ds agendamento debito veiculo
	 */
	public void setDsAgendamentoDebitoVeiculo(String dsAgendamentoDebitoVeiculo) {
		this.dsAgendamentoDebitoVeiculo = dsAgendamentoDebitoVeiculo;
	}

	/**
	 * Get: dsAgendamentoPagamentoVencido.
	 *
	 * @return dsAgendamentoPagamentoVencido
	 */
	public String getDsAgendamentoPagamentoVencido() {
		return dsAgendamentoPagamentoVencido;
	}

	/**
	 * Set: dsAgendamentoPagamentoVencido.
	 *
	 * @param dsAgendamentoPagamentoVencido the ds agendamento pagamento vencido
	 */
	public void setDsAgendamentoPagamentoVencido(String dsAgendamentoPagamentoVencido) {
		this.dsAgendamentoPagamentoVencido = dsAgendamentoPagamentoVencido;
	}

	/**
	 * Get: dsAgendamentoRastreabilidadeFinal.
	 *
	 * @return dsAgendamentoRastreabilidadeFinal
	 */
	public String getDsAgendamentoRastreabilidadeFinal() {
		return dsAgendamentoRastreabilidadeFinal;
	}

	/**
	 * Set: dsAgendamentoRastreabilidadeFinal.
	 *
	 * @param dsAgendamentoRastreabilidadeFinal the ds agendamento rastreabilidade final
	 */
	public void setDsAgendamentoRastreabilidadeFinal(String dsAgendamentoRastreabilidadeFinal) {
		this.dsAgendamentoRastreabilidadeFinal = dsAgendamentoRastreabilidadeFinal;
	}

	/**
	 * Get: dsAgendamentoValorMenor.
	 *
	 * @return dsAgendamentoValorMenor
	 */
	public String getDsAgendamentoValorMenor() {
		return dsAgendamentoValorMenor;
	}

	/**
	 * Set: dsAgendamentoValorMenor.
	 *
	 * @param dsAgendamentoValorMenor the ds agendamento valor menor
	 */
	public void setDsAgendamentoValorMenor(String dsAgendamentoValorMenor) {
		this.dsAgendamentoValorMenor = dsAgendamentoValorMenor;
	}

	/**
	 * Get: dsAgrupamentoAviso.
	 *
	 * @return dsAgrupamentoAviso
	 */
	public String getDsAgrupamentoAviso() {
		return dsAgrupamentoAviso;
	}

	/**
	 * Set: dsAgrupamentoAviso.
	 *
	 * @param dsAgrupamentoAviso the ds agrupamento aviso
	 */
	public void setDsAgrupamentoAviso(String dsAgrupamentoAviso) {
		this.dsAgrupamentoAviso = dsAgrupamentoAviso;
	}

	/**
	 * Get: dsAgrupamentoComprovante.
	 *
	 * @return dsAgrupamentoComprovante
	 */
	public String getDsAgrupamentoComprovante() {
		return dsAgrupamentoComprovante;
	}

	/**
	 * Set: dsAgrupamentoComprovante.
	 *
	 * @param dsAgrupamentoComprovante the ds agrupamento comprovante
	 */
	public void setDsAgrupamentoComprovante(String dsAgrupamentoComprovante) {
		this.dsAgrupamentoComprovante = dsAgrupamentoComprovante;
	}

	/**
	 * Get: dsAgrupamentoFormularioRecadastro.
	 *
	 * @return dsAgrupamentoFormularioRecadastro
	 */
	public String getDsAgrupamentoFormularioRecadastro() {
		return dsAgrupamentoFormularioRecadastro;
	}

	/**
	 * Set: dsAgrupamentoFormularioRecadastro.
	 *
	 * @param dsAgrupamentoFormularioRecadastro the ds agrupamento formulario recadastro
	 */
	public void setDsAgrupamentoFormularioRecadastro(String dsAgrupamentoFormularioRecadastro) {
		this.dsAgrupamentoFormularioRecadastro = dsAgrupamentoFormularioRecadastro;
	}

	/**
	 * Get: dsAntecipacaoRecadastramentoBeneficiario.
	 *
	 * @return dsAntecipacaoRecadastramentoBeneficiario
	 */
	public String getDsAntecipacaoRecadastramentoBeneficiario() {
		return dsAntecipacaoRecadastramentoBeneficiario;
	}

	/**
	 * Set: dsAntecipacaoRecadastramentoBeneficiario.
	 *
	 * @param dsAntecipacaoRecadastramentoBeneficiario the ds antecipacao recadastramento beneficiario
	 */
	public void setDsAntecipacaoRecadastramentoBeneficiario(String dsAntecipacaoRecadastramentoBeneficiario) {
		this.dsAntecipacaoRecadastramentoBeneficiario = dsAntecipacaoRecadastramentoBeneficiario;
	}

	/**
	 * Get: dsAreaReservada.
	 *
	 * @return dsAreaReservada
	 */
	public String getDsAreaReservada() {
		return dsAreaReservada;
	}

	/**
	 * Set: dsAreaReservada.
	 *
	 * @param dsAreaReservada the ds area reservada
	 */
	public void setDsAreaReservada(String dsAreaReservada) {
		this.dsAreaReservada = dsAreaReservada;
	}

	/**
	 * Get: dsBaseRecadastramentoBeneficio.
	 *
	 * @return dsBaseRecadastramentoBeneficio
	 */
	public String getDsBaseRecadastramentoBeneficio() {
		return dsBaseRecadastramentoBeneficio;
	}

	/**
	 * Set: dsBaseRecadastramentoBeneficio.
	 *
	 * @param dsBaseRecadastramentoBeneficio the ds base recadastramento beneficio
	 */
	public void setDsBaseRecadastramentoBeneficio(String dsBaseRecadastramentoBeneficio) {
		this.dsBaseRecadastramentoBeneficio = dsBaseRecadastramentoBeneficio;
	}

	/**
	 * Get: dsBloqueioEmissaoPapeleta.
	 *
	 * @return dsBloqueioEmissaoPapeleta
	 */
	public String getDsBloqueioEmissaoPapeleta() {
		return dsBloqueioEmissaoPapeleta;
	}

	/**
	 * Set: dsBloqueioEmissaoPapeleta.
	 *
	 * @param dsBloqueioEmissaoPapeleta the ds bloqueio emissao papeleta
	 */
	public void setDsBloqueioEmissaoPapeleta(String dsBloqueioEmissaoPapeleta) {
		this.dsBloqueioEmissaoPapeleta = dsBloqueioEmissaoPapeleta;
	}

	/**
	 * Get: dsCanalAlteracao.
	 *
	 * @return dsCanalAlteracao
	 */
	public String getDsCanalAlteracao() {
		return dsCanalAlteracao;
	}

	/**
	 * Set: dsCanalAlteracao.
	 *
	 * @param dsCanalAlteracao the ds canal alteracao
	 */
	public void setDsCanalAlteracao(String dsCanalAlteracao) {
		this.dsCanalAlteracao = dsCanalAlteracao;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCapturaTituloRegistrado.
	 *
	 * @return dsCapturaTituloRegistrado
	 */
	public String getDsCapturaTituloRegistrado() {
		return dsCapturaTituloRegistrado;
	}

	/**
	 * Set: dsCapturaTituloRegistrado.
	 *
	 * @param dsCapturaTituloRegistrado the ds captura titulo registrado
	 */
	public void setDsCapturaTituloRegistrado(String dsCapturaTituloRegistrado) {
		this.dsCapturaTituloRegistrado = dsCapturaTituloRegistrado;
	}

	/**
	 * Get: dsCctciaEspeBeneficio.
	 *
	 * @return dsCctciaEspeBeneficio
	 */
	public String getDsCctciaEspeBeneficio() {
		return dsCctciaEspeBeneficio;
	}

	/**
	 * Set: dsCctciaEspeBeneficio.
	 *
	 * @param dsCctciaEspeBeneficio the ds cctcia espe beneficio
	 */
	public void setDsCctciaEspeBeneficio(String dsCctciaEspeBeneficio) {
		this.dsCctciaEspeBeneficio = dsCctciaEspeBeneficio;
	}

	/**
	 * Get: dsCctciaIdentificacaoBeneficio.
	 *
	 * @return dsCctciaIdentificacaoBeneficio
	 */
	public String getDsCctciaIdentificacaoBeneficio() {
		return dsCctciaIdentificacaoBeneficio;
	}

	/**
	 * Set: dsCctciaIdentificacaoBeneficio.
	 *
	 * @param dsCctciaIdentificacaoBeneficio the ds cctcia identificacao beneficio
	 */
	public void setDsCctciaIdentificacaoBeneficio(String dsCctciaIdentificacaoBeneficio) {
		this.dsCctciaIdentificacaoBeneficio = dsCctciaIdentificacaoBeneficio;
	}

	/**
	 * Get: dsCctciaInscricaoFavorecido.
	 *
	 * @return dsCctciaInscricaoFavorecido
	 */
	public String getDsCctciaInscricaoFavorecido() {
		return dsCctciaInscricaoFavorecido;
	}

	/**
	 * Set: dsCctciaInscricaoFavorecido.
	 *
	 * @param dsCctciaInscricaoFavorecido the ds cctcia inscricao favorecido
	 */
	public void setDsCctciaInscricaoFavorecido(String dsCctciaInscricaoFavorecido) {
		this.dsCctciaInscricaoFavorecido = dsCctciaInscricaoFavorecido;
	}

	/**
	 * Get: dsCctciaProprietarioVeculo.
	 *
	 * @return dsCctciaProprietarioVeculo
	 */
	public String getDsCctciaProprietarioVeculo() {
		return dsCctciaProprietarioVeculo;
	}

	/**
	 * Set: dsCctciaProprietarioVeculo.
	 *
	 * @param dsCctciaProprietarioVeculo the ds cctcia proprietario veculo
	 */
	public void setDsCctciaProprietarioVeculo(String dsCctciaProprietarioVeculo) {
		this.dsCctciaProprietarioVeculo = dsCctciaProprietarioVeculo;
	}

	/**
	 * Get: dsCobrancaTarifa.
	 *
	 * @return dsCobrancaTarifa
	 */
	public String getDsCobrancaTarifa() {
		return dsCobrancaTarifa;
	}

	/**
	 * Set: dsCobrancaTarifa.
	 *
	 * @param dsCobrancaTarifa the ds cobranca tarifa
	 */
	public void setDsCobrancaTarifa(String dsCobrancaTarifa) {
		this.dsCobrancaTarifa = dsCobrancaTarifa;
	}

	/**
	 * Get: dsConsultaDebitoVeiculo.
	 *
	 * @return dsConsultaDebitoVeiculo
	 */
	public String getDsConsultaDebitoVeiculo() {
		return dsConsultaDebitoVeiculo;
	}

	/**
	 * Set: dsConsultaDebitoVeiculo.
	 *
	 * @param dsConsultaDebitoVeiculo the ds consulta debito veiculo
	 */
	public void setDsConsultaDebitoVeiculo(String dsConsultaDebitoVeiculo) {
		this.dsConsultaDebitoVeiculo = dsConsultaDebitoVeiculo;
	}

	/**
	 * Get: dsConsultaEndereco.
	 *
	 * @return dsConsultaEndereco
	 */
	public String getDsConsultaEndereco() {
		return dsConsultaEndereco;
	}

	/**
	 * Set: dsConsultaEndereco.
	 *
	 * @param dsConsultaEndereco the ds consulta endereco
	 */
	public void setDsConsultaEndereco(String dsConsultaEndereco) {
		this.dsConsultaEndereco = dsConsultaEndereco;
	}

	/**
	 * Get: dsConsultaSaldoPagamento.
	 *
	 * @return dsConsultaSaldoPagamento
	 */
	public String getDsConsultaSaldoPagamento() {
		return dsConsultaSaldoPagamento;
	}

	/**
	 * Set: dsConsultaSaldoPagamento.
	 *
	 * @param dsConsultaSaldoPagamento the ds consulta saldo pagamento
	 */
	public void setDsConsultaSaldoPagamento(String dsConsultaSaldoPagamento) {
		this.dsConsultaSaldoPagamento = dsConsultaSaldoPagamento;
	}

	/**
	 * Get: dsContagemConsultaSaldo.
	 *
	 * @return dsContagemConsultaSaldo
	 */
	public String getDsContagemConsultaSaldo() {
		return dsContagemConsultaSaldo;
	}

	/**
	 * Set: dsContagemConsultaSaldo.
	 *
	 * @param dsContagemConsultaSaldo the ds contagem consulta saldo
	 */
	public void setDsContagemConsultaSaldo(String dsContagemConsultaSaldo) {
		this.dsContagemConsultaSaldo = dsContagemConsultaSaldo;
	}

	/**
	 * Get: dsCreditoNaoUtilizado.
	 *
	 * @return dsCreditoNaoUtilizado
	 */
	public String getDsCreditoNaoUtilizado() {
		return dsCreditoNaoUtilizado;
	}

	/**
	 * Set: dsCreditoNaoUtilizado.
	 *
	 * @param dsCreditoNaoUtilizado the ds credito nao utilizado
	 */
	public void setDsCreditoNaoUtilizado(String dsCreditoNaoUtilizado) {
		this.dsCreditoNaoUtilizado = dsCreditoNaoUtilizado;
	}

	/**
	 * Get: dsCriterioEnquadraRecadastramento.
	 *
	 * @return dsCriterioEnquadraRecadastramento
	 */
	public String getDsCriterioEnquadraRecadastramento() {
		return dsCriterioEnquadraRecadastramento;
	}

	/**
	 * Set: dsCriterioEnquadraRecadastramento.
	 *
	 * @param dsCriterioEnquadraRecadastramento the ds criterio enquadra recadastramento
	 */
	public void setDsCriterioEnquadraRecadastramento(String dsCriterioEnquadraRecadastramento) {
		this.dsCriterioEnquadraRecadastramento = dsCriterioEnquadraRecadastramento;
	}

	/**
	 * Get: dsCriterioEnquandraBeneficio.
	 *
	 * @return dsCriterioEnquandraBeneficio
	 */
	public String getDsCriterioEnquandraBeneficio() {
		return dsCriterioEnquandraBeneficio;
	}

	/**
	 * Set: dsCriterioEnquandraBeneficio.
	 *
	 * @param dsCriterioEnquandraBeneficio the ds criterio enquandra beneficio
	 */
	public void setDsCriterioEnquandraBeneficio(String dsCriterioEnquandraBeneficio) {
		this.dsCriterioEnquandraBeneficio = dsCriterioEnquandraBeneficio;
	}

	/**
	 * Get: dsCriterioRastreabilidadeTitulo.
	 *
	 * @return dsCriterioRastreabilidadeTitulo
	 */
	public String getDsCriterioRastreabilidadeTitulo() {
		return dsCriterioRastreabilidadeTitulo;
	}

	/**
	 * Set: dsCriterioRastreabilidadeTitulo.
	 *
	 * @param dsCriterioRastreabilidadeTitulo the ds criterio rastreabilidade titulo
	 */
	public void setDsCriterioRastreabilidadeTitulo(String dsCriterioRastreabilidadeTitulo) {
		this.dsCriterioRastreabilidadeTitulo = dsCriterioRastreabilidadeTitulo;
	}

	/**
	 * Get: dsDestinoAviso.
	 *
	 * @return dsDestinoAviso
	 */
	public String getDsDestinoAviso() {
		return dsDestinoAviso;
	}

	/**
	 * Set: dsDestinoAviso.
	 *
	 * @param dsDestinoAviso the ds destino aviso
	 */
	public void setDsDestinoAviso(String dsDestinoAviso) {
		this.dsDestinoAviso = dsDestinoAviso;
	}

	/**
	 * Get: dsDestinoComprovante.
	 *
	 * @return dsDestinoComprovante
	 */
	public String getDsDestinoComprovante() {
		return dsDestinoComprovante;
	}

	/**
	 * Set: dsDestinoComprovante.
	 *
	 * @param dsDestinoComprovante the ds destino comprovante
	 */
	public void setDsDestinoComprovante(String dsDestinoComprovante) {
		this.dsDestinoComprovante = dsDestinoComprovante;
	}

	/**
	 * Get: dsDestinoFormularioRecadastramento.
	 *
	 * @return dsDestinoFormularioRecadastramento
	 */
	public String getDsDestinoFormularioRecadastramento() {
		return dsDestinoFormularioRecadastramento;
	}

	/**
	 * Set: dsDestinoFormularioRecadastramento.
	 *
	 * @param dsDestinoFormularioRecadastramento the ds destino formulario recadastramento
	 */
	public void setDsDestinoFormularioRecadastramento(String dsDestinoFormularioRecadastramento) {
		this.dsDestinoFormularioRecadastramento = dsDestinoFormularioRecadastramento;
	}

	/**
	 * Get: dsDisponibilizacaoContaCredito.
	 *
	 * @return dsDisponibilizacaoContaCredito
	 */
	public String getDsDisponibilizacaoContaCredito() {
		return dsDisponibilizacaoContaCredito;
	}

	/**
	 * Set: dsDisponibilizacaoContaCredito.
	 *
	 * @param dsDisponibilizacaoContaCredito the ds disponibilizacao conta credito
	 */
	public void setDsDisponibilizacaoContaCredito(String dsDisponibilizacaoContaCredito) {
		this.dsDisponibilizacaoContaCredito = dsDisponibilizacaoContaCredito;
	}

	/**
	 * Get: dsDisponibilizacaoDiversoCriterio.
	 *
	 * @return dsDisponibilizacaoDiversoCriterio
	 */
	public String getDsDisponibilizacaoDiversoCriterio() {
		return dsDisponibilizacaoDiversoCriterio;
	}

	/**
	 * Set: dsDisponibilizacaoDiversoCriterio.
	 *
	 * @param dsDisponibilizacaoDiversoCriterio the ds disponibilizacao diverso criterio
	 */
	public void setDsDisponibilizacaoDiversoCriterio(String dsDisponibilizacaoDiversoCriterio) {
		this.dsDisponibilizacaoDiversoCriterio = dsDisponibilizacaoDiversoCriterio;
	}

	/**
	 * Get: dsDisponibilizacaoDiversoNao.
	 *
	 * @return dsDisponibilizacaoDiversoNao
	 */
	public String getDsDisponibilizacaoDiversoNao() {
		return dsDisponibilizacaoDiversoNao;
	}

	/**
	 * Set: dsDisponibilizacaoDiversoNao.
	 *
	 * @param dsDisponibilizacaoDiversoNao the ds disponibilizacao diverso nao
	 */
	public void setDsDisponibilizacaoDiversoNao(String dsDisponibilizacaoDiversoNao) {
		this.dsDisponibilizacaoDiversoNao = dsDisponibilizacaoDiversoNao;
	}

	/**
	 * Get: dsDisponibilizacaoSalarioCriterio.
	 *
	 * @return dsDisponibilizacaoSalarioCriterio
	 */
	public String getDsDisponibilizacaoSalarioCriterio() {
		return dsDisponibilizacaoSalarioCriterio;
	}

	/**
	 * Set: dsDisponibilizacaoSalarioCriterio.
	 *
	 * @param dsDisponibilizacaoSalarioCriterio the ds disponibilizacao salario criterio
	 */
	public void setDsDisponibilizacaoSalarioCriterio(String dsDisponibilizacaoSalarioCriterio) {
		this.dsDisponibilizacaoSalarioCriterio = dsDisponibilizacaoSalarioCriterio;
	}

	/**
	 * Get: dsDisponibilizacaoSalarioNao.
	 *
	 * @return dsDisponibilizacaoSalarioNao
	 */
	public String getDsDisponibilizacaoSalarioNao() {
		return dsDisponibilizacaoSalarioNao;
	}

	/**
	 * Set: dsDisponibilizacaoSalarioNao.
	 *
	 * @param dsDisponibilizacaoSalarioNao the ds disponibilizacao salario nao
	 */
	public void setDsDisponibilizacaoSalarioNao(String dsDisponibilizacaoSalarioNao) {
		this.dsDisponibilizacaoSalarioNao = dsDisponibilizacaoSalarioNao;
	}

	/**
	 * Get: dsEnvelopeAberto.
	 *
	 * @return dsEnvelopeAberto
	 */
	public String getDsEnvelopeAberto() {
		return dsEnvelopeAberto;
	}

	/**
	 * Set: dsEnvelopeAberto.
	 *
	 * @param dsEnvelopeAberto the ds envelope aberto
	 */
	public void setDsEnvelopeAberto(String dsEnvelopeAberto) {
		this.dsEnvelopeAberto = dsEnvelopeAberto;
	}

	/**
	 * Get: dsFavorecidoConsultaPagamento.
	 *
	 * @return dsFavorecidoConsultaPagamento
	 */
	public String getDsFavorecidoConsultaPagamento() {
		return dsFavorecidoConsultaPagamento;
	}

	/**
	 * Set: dsFavorecidoConsultaPagamento.
	 *
	 * @param dsFavorecidoConsultaPagamento the ds favorecido consulta pagamento
	 */
	public void setDsFavorecidoConsultaPagamento(String dsFavorecidoConsultaPagamento) {
		this.dsFavorecidoConsultaPagamento = dsFavorecidoConsultaPagamento;
	}

	/**
	 * Get: dsFormaAutorizacaoPagamento.
	 *
	 * @return dsFormaAutorizacaoPagamento
	 */
	public String getDsFormaAutorizacaoPagamento() {
		return dsFormaAutorizacaoPagamento;
	}

	/**
	 * Set: dsFormaAutorizacaoPagamento.
	 *
	 * @param dsFormaAutorizacaoPagamento the ds forma autorizacao pagamento
	 */
	public void setDsFormaAutorizacaoPagamento(String dsFormaAutorizacaoPagamento) {
		this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
	}

	/**
	 * Get: dsFormaEnvioPagamento.
	 *
	 * @return dsFormaEnvioPagamento
	 */
	public String getDsFormaEnvioPagamento() {
		return dsFormaEnvioPagamento;
	}

	/**
	 * Set: dsFormaEnvioPagamento.
	 *
	 * @param dsFormaEnvioPagamento the ds forma envio pagamento
	 */
	public void setDsFormaEnvioPagamento(String dsFormaEnvioPagamento) {
		this.dsFormaEnvioPagamento = dsFormaEnvioPagamento;
	}

	/**
	 * Get: dsFormaEstornoCredito.
	 *
	 * @return dsFormaEstornoCredito
	 */
	public String getDsFormaEstornoCredito() {
		return dsFormaEstornoCredito;
	}

	/**
	 * Set: dsFormaEstornoCredito.
	 *
	 * @param dsFormaEstornoCredito the ds forma estorno credito
	 */
	public void setDsFormaEstornoCredito(String dsFormaEstornoCredito) {
		this.dsFormaEstornoCredito = dsFormaEstornoCredito;
	}

	/**
	 * Get: dsFormaExpiracaoCredito.
	 *
	 * @return dsFormaExpiracaoCredito
	 */
	public String getDsFormaExpiracaoCredito() {
		return dsFormaExpiracaoCredito;
	}

	/**
	 * Set: dsFormaExpiracaoCredito.
	 *
	 * @param dsFormaExpiracaoCredito the ds forma expiracao credito
	 */
	public void setDsFormaExpiracaoCredito(String dsFormaExpiracaoCredito) {
		this.dsFormaExpiracaoCredito = dsFormaExpiracaoCredito;
	}

	/**
	 * Get: dsFormaManutencao.
	 *
	 * @return dsFormaManutencao
	 */
	public String getDsFormaManutencao() {
		return dsFormaManutencao;
	}

	/**
	 * Set: dsFormaManutencao.
	 *
	 * @param dsFormaManutencao the ds forma manutencao
	 */
	public void setDsFormaManutencao(String dsFormaManutencao) {
		this.dsFormaManutencao = dsFormaManutencao;
	}

	/**
	 * Get: dsFrasePrecadastrada.
	 *
	 * @return dsFrasePrecadastrada
	 */
	public String getDsFrasePrecadastrada() {
		return dsFrasePrecadastrada;
	}

	/**
	 * Set: dsFrasePrecadastrada.
	 *
	 * @param dsFrasePrecadastrada the ds frase precadastrada
	 */
	public void setDsFrasePrecadastrada(String dsFrasePrecadastrada) {
		this.dsFrasePrecadastrada = dsFrasePrecadastrada;
	}

	/**
	 * Get: dsIndicadorAgendamentoTitulo.
	 *
	 * @return dsIndicadorAgendamentoTitulo
	 */
	public String getDsIndicadorAgendamentoTitulo() {
		return dsIndicadorAgendamentoTitulo;
	}

	/**
	 * Set: dsIndicadorAgendamentoTitulo.
	 *
	 * @param dsIndicadorAgendamentoTitulo the ds indicador agendamento titulo
	 */
	public void setDsIndicadorAgendamentoTitulo(String dsIndicadorAgendamentoTitulo) {
		this.dsIndicadorAgendamentoTitulo = dsIndicadorAgendamentoTitulo;
	}

	/**
	 * Get: dsIndicadorAutorizacaoCliente.
	 *
	 * @return dsIndicadorAutorizacaoCliente
	 */
	public String getDsIndicadorAutorizacaoCliente() {
		return dsIndicadorAutorizacaoCliente;
	}

	/**
	 * Set: dsIndicadorAutorizacaoCliente.
	 *
	 * @param dsIndicadorAutorizacaoCliente the ds indicador autorizacao cliente
	 */
	public void setDsIndicadorAutorizacaoCliente(String dsIndicadorAutorizacaoCliente) {
		this.dsIndicadorAutorizacaoCliente = dsIndicadorAutorizacaoCliente;
	}

	/**
	 * Get: dsIndicadorAutorizacaoComplemento.
	 *
	 * @return dsIndicadorAutorizacaoComplemento
	 */
	public String getDsIndicadorAutorizacaoComplemento() {
		return dsIndicadorAutorizacaoComplemento;
	}

	/**
	 * Set: dsIndicadorAutorizacaoComplemento.
	 *
	 * @param dsIndicadorAutorizacaoComplemento the ds indicador autorizacao complemento
	 */
	public void setDsIndicadorAutorizacaoComplemento(String dsIndicadorAutorizacaoComplemento) {
		this.dsIndicadorAutorizacaoComplemento = dsIndicadorAutorizacaoComplemento;
	}

	/**
	 * Get: dsIndicadorCadastroorganizacao.
	 *
	 * @return dsIndicadorCadastroorganizacao
	 */
	public String getDsIndicadorCadastroorganizacao() {
		return dsIndicadorCadastroorganizacao;
	}

	/**
	 * Set: dsIndicadorCadastroorganizacao.
	 *
	 * @param dsIndicadorCadastroorganizacao the ds indicador cadastroorganizacao
	 */
	public void setDsIndicadorCadastroorganizacao(String dsIndicadorCadastroorganizacao) {
		this.dsIndicadorCadastroorganizacao = dsIndicadorCadastroorganizacao;
	}

	/**
	 * Get: dsIndicadorCadastroProcurador.
	 *
	 * @return dsIndicadorCadastroProcurador
	 */
	public String getDsIndicadorCadastroProcurador() {
		return dsIndicadorCadastroProcurador;
	}

	/**
	 * Set: dsIndicadorCadastroProcurador.
	 *
	 * @param dsIndicadorCadastroProcurador the ds indicador cadastro procurador
	 */
	public void setDsIndicadorCadastroProcurador(String dsIndicadorCadastroProcurador) {
		this.dsIndicadorCadastroProcurador = dsIndicadorCadastroProcurador;
	}

	/**
	 * Get: dsIndicadorCartaoSalario.
	 *
	 * @return dsIndicadorCartaoSalario
	 */
	public String getDsIndicadorCartaoSalario() {
		return dsIndicadorCartaoSalario;
	}

	/**
	 * Set: dsIndicadorCartaoSalario.
	 *
	 * @param dsIndicadorCartaoSalario the ds indicador cartao salario
	 */
	public void setDsIndicadorCartaoSalario(String dsIndicadorCartaoSalario) {
		this.dsIndicadorCartaoSalario = dsIndicadorCartaoSalario;
	}

	/**
	 * Get: dsIndicadorEconomicoReajuste.
	 *
	 * @return dsIndicadorEconomicoReajuste
	 */
	public String getDsIndicadorEconomicoReajuste() {
		return dsIndicadorEconomicoReajuste;
	}

	/**
	 * Set: dsIndicadorEconomicoReajuste.
	 *
	 * @param dsIndicadorEconomicoReajuste the ds indicador economico reajuste
	 */
	public void setDsIndicadorEconomicoReajuste(String dsIndicadorEconomicoReajuste) {
		this.dsIndicadorEconomicoReajuste = dsIndicadorEconomicoReajuste;
	}

	/**
	 * Get: dsindicadorExpiraCredito.
	 *
	 * @return dsindicadorExpiraCredito
	 */
	public String getDsindicadorExpiraCredito() {
		return dsindicadorExpiraCredito;
	}

	/**
	 * Set: dsindicadorExpiraCredito.
	 *
	 * @param dsindicadorExpiraCredito the dsindicador expira credito
	 */
	public void setDsindicadorExpiraCredito(String dsindicadorExpiraCredito) {
		this.dsindicadorExpiraCredito = dsindicadorExpiraCredito;
	}

	/**
	 * Get: dsindicadorLancamentoProgramado.
	 *
	 * @return dsindicadorLancamentoProgramado
	 */
	public String getDsindicadorLancamentoProgramado() {
		return dsindicadorLancamentoProgramado;
	}

	/**
	 * Set: dsindicadorLancamentoProgramado.
	 *
	 * @param dsindicadorLancamentoProgramado the dsindicador lancamento programado
	 */
	public void setDsindicadorLancamentoProgramado(String dsindicadorLancamentoProgramado) {
		this.dsindicadorLancamentoProgramado = dsindicadorLancamentoProgramado;
	}

	/**
	 * Get: dsIndicadorMensagemPersonalizada.
	 *
	 * @return dsIndicadorMensagemPersonalizada
	 */
	public String getDsIndicadorMensagemPersonalizada() {
		return dsIndicadorMensagemPersonalizada;
	}

	/**
	 * Set: dsIndicadorMensagemPersonalizada.
	 *
	 * @param dsIndicadorMensagemPersonalizada the ds indicador mensagem personalizada
	 */
	public void setDsIndicadorMensagemPersonalizada(String dsIndicadorMensagemPersonalizada) {
		this.dsIndicadorMensagemPersonalizada = dsIndicadorMensagemPersonalizada;
	}

	/**
	 * Get: dsLancamentoFuturoCredito.
	 *
	 * @return dsLancamentoFuturoCredito
	 */
	public String getDsLancamentoFuturoCredito() {
		return dsLancamentoFuturoCredito;
	}

	/**
	 * Set: dsLancamentoFuturoCredito.
	 *
	 * @param dsLancamentoFuturoCredito the ds lancamento futuro credito
	 */
	public void setDsLancamentoFuturoCredito(String dsLancamentoFuturoCredito) {
		this.dsLancamentoFuturoCredito = dsLancamentoFuturoCredito;
	}

	/**
	 * Get: dsLancamentoFuturoDebito.
	 *
	 * @return dsLancamentoFuturoDebito
	 */
	public String getDsLancamentoFuturoDebito() {
		return dsLancamentoFuturoDebito;
	}

	/**
	 * Set: dsLancamentoFuturoDebito.
	 *
	 * @param dsLancamentoFuturoDebito the ds lancamento futuro debito
	 */
	public void setDsLancamentoFuturoDebito(String dsLancamentoFuturoDebito) {
		this.dsLancamentoFuturoDebito = dsLancamentoFuturoDebito;
	}

	/**
	 * Get: dsLiberacaoLoteProcessado.
	 *
	 * @return dsLiberacaoLoteProcessado
	 */
	public String getDsLiberacaoLoteProcessado() {
		return dsLiberacaoLoteProcessado;
	}

	/**
	 * Set: dsLiberacaoLoteProcessado.
	 *
	 * @param dsLiberacaoLoteProcessado the ds liberacao lote processado
	 */
	public void setDsLiberacaoLoteProcessado(String dsLiberacaoLoteProcessado) {
		this.dsLiberacaoLoteProcessado = dsLiberacaoLoteProcessado;
	}

	/**
	 * Get: dsManutencaoBaseRecadastramento.
	 *
	 * @return dsManutencaoBaseRecadastramento
	 */
	public String getDsManutencaoBaseRecadastramento() {
		return dsManutencaoBaseRecadastramento;
	}

	/**
	 * Set: dsManutencaoBaseRecadastramento.
	 *
	 * @param dsManutencaoBaseRecadastramento the ds manutencao base recadastramento
	 */
	public void setDsManutencaoBaseRecadastramento(String dsManutencaoBaseRecadastramento) {
		this.dsManutencaoBaseRecadastramento = dsManutencaoBaseRecadastramento;
	}

	/**
	 * Get: dsMeioPagamentoDebito.
	 *
	 * @return dsMeioPagamentoDebito
	 */
	public String getDsMeioPagamentoDebito() {
		return dsMeioPagamentoDebito;
	}

	/**
	 * Set: dsMeioPagamentoDebito.
	 *
	 * @param dsMeioPagamentoDebito the ds meio pagamento debito
	 */
	public void setDsMeioPagamentoDebito(String dsMeioPagamentoDebito) {
		this.dsMeioPagamentoDebito = dsMeioPagamentoDebito;
	}

	/**
	 * Get: dsMensagemRecadastramentoMidia.
	 *
	 * @return dsMensagemRecadastramentoMidia
	 */
	public String getDsMensagemRecadastramentoMidia() {
		return dsMensagemRecadastramentoMidia;
	}

	/**
	 * Set: dsMensagemRecadastramentoMidia.
	 *
	 * @param dsMensagemRecadastramentoMidia the ds mensagem recadastramento midia
	 */
	public void setDsMensagemRecadastramentoMidia(String dsMensagemRecadastramentoMidia) {
		this.dsMensagemRecadastramentoMidia = dsMensagemRecadastramentoMidia;
	}

	/**
	 * Get: dsMidiaDisponivel.
	 *
	 * @return dsMidiaDisponivel
	 */
	public String getDsMidiaDisponivel() {
		return dsMidiaDisponivel;
	}

	/**
	 * Set: dsMidiaDisponivel.
	 *
	 * @param dsMidiaDisponivel the ds midia disponivel
	 */
	public void setDsMidiaDisponivel(String dsMidiaDisponivel) {
		this.dsMidiaDisponivel = dsMidiaDisponivel;
	}

	/**
	 * Get: dsMidiaMensagemRecadastramento.
	 *
	 * @return dsMidiaMensagemRecadastramento
	 */
	public String getDsMidiaMensagemRecadastramento() {
		return dsMidiaMensagemRecadastramento;
	}

	/**
	 * Set: dsMidiaMensagemRecadastramento.
	 *
	 * @param dsMidiaMensagemRecadastramento the ds midia mensagem recadastramento
	 */
	public void setDsMidiaMensagemRecadastramento(String dsMidiaMensagemRecadastramento) {
		this.dsMidiaMensagemRecadastramento = dsMidiaMensagemRecadastramento;
	}

	/**
	 * Get: dsMomentoAvisoRecadastramento.
	 *
	 * @return dsMomentoAvisoRecadastramento
	 */
	public String getDsMomentoAvisoRecadastramento() {
		return dsMomentoAvisoRecadastramento;
	}

	/**
	 * Set: dsMomentoAvisoRecadastramento.
	 *
	 * @param dsMomentoAvisoRecadastramento the ds momento aviso recadastramento
	 */
	public void setDsMomentoAvisoRecadastramento(String dsMomentoAvisoRecadastramento) {
		this.dsMomentoAvisoRecadastramento = dsMomentoAvisoRecadastramento;
	}

	/**
	 * Get: dsMomentoCreditoEfetivacao.
	 *
	 * @return dsMomentoCreditoEfetivacao
	 */
	public String getDsMomentoCreditoEfetivacao() {
		return dsMomentoCreditoEfetivacao;
	}

	/**
	 * Set: dsMomentoCreditoEfetivacao.
	 *
	 * @param dsMomentoCreditoEfetivacao the ds momento credito efetivacao
	 */
	public void setDsMomentoCreditoEfetivacao(String dsMomentoCreditoEfetivacao) {
		this.dsMomentoCreditoEfetivacao = dsMomentoCreditoEfetivacao;
	}

	/**
	 * Get: dsMomentoDebitoPagamento.
	 *
	 * @return dsMomentoDebitoPagamento
	 */
	public String getDsMomentoDebitoPagamento() {
		return dsMomentoDebitoPagamento;
	}

	/**
	 * Set: dsMomentoDebitoPagamento.
	 *
	 * @param dsMomentoDebitoPagamento the ds momento debito pagamento
	 */
	public void setDsMomentoDebitoPagamento(String dsMomentoDebitoPagamento) {
		this.dsMomentoDebitoPagamento = dsMomentoDebitoPagamento;
	}

	/**
	 * Get: dsMomentoFormularioRecadastramento.
	 *
	 * @return dsMomentoFormularioRecadastramento
	 */
	public String getDsMomentoFormularioRecadastramento() {
		return dsMomentoFormularioRecadastramento;
	}

	/**
	 * Set: dsMomentoFormularioRecadastramento.
	 *
	 * @param dsMomentoFormularioRecadastramento the ds momento formulario recadastramento
	 */
	public void setDsMomentoFormularioRecadastramento(String dsMomentoFormularioRecadastramento) {
		this.dsMomentoFormularioRecadastramento = dsMomentoFormularioRecadastramento;
	}

	/**
	 * Get: dsMomentoProcessamentoPagamento.
	 *
	 * @return dsMomentoProcessamentoPagamento
	 */
	public String getDsMomentoProcessamentoPagamento() {
		return dsMomentoProcessamentoPagamento;
	}

	/**
	 * Set: dsMomentoProcessamentoPagamento.
	 *
	 * @param dsMomentoProcessamentoPagamento the ds momento processamento pagamento
	 */
	public void setDsMomentoProcessamentoPagamento(String dsMomentoProcessamentoPagamento) {
		this.dsMomentoProcessamentoPagamento = dsMomentoProcessamentoPagamento;
	}

	/**
	 * Get: dsNaturezaOperacaoPagamento.
	 *
	 * @return dsNaturezaOperacaoPagamento
	 */
	public String getDsNaturezaOperacaoPagamento() {
		return dsNaturezaOperacaoPagamento;
	}

	/**
	 * Set: dsNaturezaOperacaoPagamento.
	 *
	 * @param dsNaturezaOperacaoPagamento the ds natureza operacao pagamento
	 */
	public void setDsNaturezaOperacaoPagamento(String dsNaturezaOperacaoPagamento) {
		this.dsNaturezaOperacaoPagamento = dsNaturezaOperacaoPagamento;
	}

	/**
	 * Get: dsOutraidentificacaoFavorecido.
	 *
	 * @return dsOutraidentificacaoFavorecido
	 */
	public String getDsOutraidentificacaoFavorecido() {
		return dsOutraidentificacaoFavorecido;
	}

	/**
	 * Set: dsOutraidentificacaoFavorecido.
	 *
	 * @param dsOutraidentificacaoFavorecido the ds outraidentificacao favorecido
	 */
	public void setDsOutraidentificacaoFavorecido(String dsOutraidentificacaoFavorecido) {
		this.dsOutraidentificacaoFavorecido = dsOutraidentificacaoFavorecido;
	}

	/**
	 * Get: dsPagamentoNaoUtil.
	 *
	 * @return dsPagamentoNaoUtil
	 */
	public String getDsPagamentoNaoUtil() {
		return dsPagamentoNaoUtil;
	}

	/**
	 * Set: dsPagamentoNaoUtil.
	 *
	 * @param dsPagamentoNaoUtil the ds pagamento nao util
	 */
	public void setDsPagamentoNaoUtil(String dsPagamentoNaoUtil) {
		this.dsPagamentoNaoUtil = dsPagamentoNaoUtil;
	}

	/**
	 * Get: dsPeriodicidadeAviso.
	 *
	 * @return dsPeriodicidadeAviso
	 */
	public String getDsPeriodicidadeAviso() {
		return dsPeriodicidadeAviso;
	}

	/**
	 * Set: dsPeriodicidadeAviso.
	 *
	 * @param dsPeriodicidadeAviso the ds periodicidade aviso
	 */
	public void setDsPeriodicidadeAviso(String dsPeriodicidadeAviso) {
		this.dsPeriodicidadeAviso = dsPeriodicidadeAviso;
	}

	/**
	 * Get: dsPeriodicidadeCobrancaTarifa.
	 *
	 * @return dsPeriodicidadeCobrancaTarifa
	 */
	public String getDsPeriodicidadeCobrancaTarifa() {
		return dsPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Set: dsPeriodicidadeCobrancaTarifa.
	 *
	 * @param dsPeriodicidadeCobrancaTarifa the ds periodicidade cobranca tarifa
	 */
	public void setDsPeriodicidadeCobrancaTarifa(String dsPeriodicidadeCobrancaTarifa) {
		this.dsPeriodicidadeCobrancaTarifa = dsPeriodicidadeCobrancaTarifa;
	}

	/**
	 * Get: dsPeriodicidadeComprovante.
	 *
	 * @return dsPeriodicidadeComprovante
	 */
	public String getDsPeriodicidadeComprovante() {
		return dsPeriodicidadeComprovante;
	}

	/**
	 * Set: dsPeriodicidadeComprovante.
	 *
	 * @param dsPeriodicidadeComprovante the ds periodicidade comprovante
	 */
	public void setDsPeriodicidadeComprovante(String dsPeriodicidadeComprovante) {
		this.dsPeriodicidadeComprovante = dsPeriodicidadeComprovante;
	}

	/**
	 * Get: dsPeriodicidadeConsultaVeiculo.
	 *
	 * @return dsPeriodicidadeConsultaVeiculo
	 */
	public String getDsPeriodicidadeConsultaVeiculo() {
		return dsPeriodicidadeConsultaVeiculo;
	}

	/**
	 * Set: dsPeriodicidadeConsultaVeiculo.
	 *
	 * @param dsPeriodicidadeConsultaVeiculo the ds periodicidade consulta veiculo
	 */
	public void setDsPeriodicidadeConsultaVeiculo(String dsPeriodicidadeConsultaVeiculo) {
		this.dsPeriodicidadeConsultaVeiculo = dsPeriodicidadeConsultaVeiculo;
	}

	/**
	 * Get: dsPeriodicidadeEnvioRemessa.
	 *
	 * @return dsPeriodicidadeEnvioRemessa
	 */
	public String getDsPeriodicidadeEnvioRemessa() {
		return dsPeriodicidadeEnvioRemessa;
	}

	/**
	 * Set: dsPeriodicidadeEnvioRemessa.
	 *
	 * @param dsPeriodicidadeEnvioRemessa the ds periodicidade envio remessa
	 */
	public void setDsPeriodicidadeEnvioRemessa(String dsPeriodicidadeEnvioRemessa) {
		this.dsPeriodicidadeEnvioRemessa = dsPeriodicidadeEnvioRemessa;
	}

	/**
	 * Get: dsPeriodicidadeManutencaoProcd.
	 *
	 * @return dsPeriodicidadeManutencaoProcd
	 */
	public String getDsPeriodicidadeManutencaoProcd() {
		return dsPeriodicidadeManutencaoProcd;
	}

	/**
	 * Set: dsPeriodicidadeManutencaoProcd.
	 *
	 * @param dsPeriodicidadeManutencaoProcd the ds periodicidade manutencao procd
	 */
	public void setDsPeriodicidadeManutencaoProcd(String dsPeriodicidadeManutencaoProcd) {
		this.dsPeriodicidadeManutencaoProcd = dsPeriodicidadeManutencaoProcd;
	}

	/**
	 * Get: dsPeriodicidadeReajusteTarifa.
	 *
	 * @return dsPeriodicidadeReajusteTarifa
	 */
	public String getDsPeriodicidadeReajusteTarifa() {
		return dsPeriodicidadeReajusteTarifa;
	}

	/**
	 * Set: dsPeriodicidadeReajusteTarifa.
	 *
	 * @param dsPeriodicidadeReajusteTarifa the ds periodicidade reajuste tarifa
	 */
	public void setDsPeriodicidadeReajusteTarifa(String dsPeriodicidadeReajusteTarifa) {
		this.dsPeriodicidadeReajusteTarifa = dsPeriodicidadeReajusteTarifa;
	}

	/**
	 * Get: dsPermissaoDebitoOnline.
	 *
	 * @return dsPermissaoDebitoOnline
	 */
	public String getDsPermissaoDebitoOnline() {
		return dsPermissaoDebitoOnline;
	}

	/**
	 * Set: dsPermissaoDebitoOnline.
	 *
	 * @param dsPermissaoDebitoOnline the ds permissao debito online
	 */
	public void setDsPermissaoDebitoOnline(String dsPermissaoDebitoOnline) {
		this.dsPermissaoDebitoOnline = dsPermissaoDebitoOnline;
	}

	/**
	 * Get: dsPrincipalEnquaRecadastramento.
	 *
	 * @return dsPrincipalEnquaRecadastramento
	 */
	public String getDsPrincipalEnquaRecadastramento() {
		return dsPrincipalEnquaRecadastramento;
	}

	/**
	 * Set: dsPrincipalEnquaRecadastramento.
	 *
	 * @param dsPrincipalEnquaRecadastramento the ds principal enqua recadastramento
	 */
	public void setDsPrincipalEnquaRecadastramento(String dsPrincipalEnquaRecadastramento) {
		this.dsPrincipalEnquaRecadastramento = dsPrincipalEnquaRecadastramento;
	}

	/**
	 * Get: dsPrioridadeEfetivacaoPagamento.
	 *
	 * @return dsPrioridadeEfetivacaoPagamento
	 */
	public String getDsPrioridadeEfetivacaoPagamento() {
		return dsPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Set: dsPrioridadeEfetivacaoPagamento.
	 *
	 * @param dsPrioridadeEfetivacaoPagamento the ds prioridade efetivacao pagamento
	 */
	public void setDsPrioridadeEfetivacaoPagamento(String dsPrioridadeEfetivacaoPagamento) {
		this.dsPrioridadeEfetivacaoPagamento = dsPrioridadeEfetivacaoPagamento;
	}

	/**
	 * Get: dsRastreabilidadeNotaFiscal.
	 *
	 * @return dsRastreabilidadeNotaFiscal
	 */
	public String getDsRastreabilidadeNotaFiscal() {
		return dsRastreabilidadeNotaFiscal;
	}

	/**
	 * Set: dsRastreabilidadeNotaFiscal.
	 *
	 * @param dsRastreabilidadeNotaFiscal the ds rastreabilidade nota fiscal
	 */
	public void setDsRastreabilidadeNotaFiscal(String dsRastreabilidadeNotaFiscal) {
		this.dsRastreabilidadeNotaFiscal = dsRastreabilidadeNotaFiscal;
	}

	/**
	 * Get: dsRastreabilidadeTituloTerceiro.
	 *
	 * @return dsRastreabilidadeTituloTerceiro
	 */
	public String getDsRastreabilidadeTituloTerceiro() {
		return dsRastreabilidadeTituloTerceiro;
	}

	/**
	 * Set: dsRastreabilidadeTituloTerceiro.
	 *
	 * @param dsRastreabilidadeTituloTerceiro the ds rastreabilidade titulo terceiro
	 */
	public void setDsRastreabilidadeTituloTerceiro(String dsRastreabilidadeTituloTerceiro) {
		this.dsRastreabilidadeTituloTerceiro = dsRastreabilidadeTituloTerceiro;
	}

	/**
	 * Get: dsRejeicaoAgendamentoLote.
	 *
	 * @return dsRejeicaoAgendamentoLote
	 */
	public String getDsRejeicaoAgendamentoLote() {
		return dsRejeicaoAgendamentoLote;
	}

	/**
	 * Set: dsRejeicaoAgendamentoLote.
	 *
	 * @param dsRejeicaoAgendamentoLote the ds rejeicao agendamento lote
	 */
	public void setDsRejeicaoAgendamentoLote(String dsRejeicaoAgendamentoLote) {
		this.dsRejeicaoAgendamentoLote = dsRejeicaoAgendamentoLote;
	}

	/**
	 * Get: dsRejeicaoEfetivacaoLote.
	 *
	 * @return dsRejeicaoEfetivacaoLote
	 */
	public String getDsRejeicaoEfetivacaoLote() {
		return dsRejeicaoEfetivacaoLote;
	}

	/**
	 * Set: dsRejeicaoEfetivacaoLote.
	 *
	 * @param dsRejeicaoEfetivacaoLote the ds rejeicao efetivacao lote
	 */
	public void setDsRejeicaoEfetivacaoLote(String dsRejeicaoEfetivacaoLote) {
		this.dsRejeicaoEfetivacaoLote = dsRejeicaoEfetivacaoLote;
	}

	/**
	 * Get: dsRejeicaoLote.
	 *
	 * @return dsRejeicaoLote
	 */
	public String getDsRejeicaoLote() {
		return dsRejeicaoLote;
	}

	/**
	 * Set: dsRejeicaoLote.
	 *
	 * @param dsRejeicaoLote the ds rejeicao lote
	 */
	public void setDsRejeicaoLote(String dsRejeicaoLote) {
		this.dsRejeicaoLote = dsRejeicaoLote;
	}

	/**
	 * Get: dsTipoCargaRecadastramento.
	 *
	 * @return dsTipoCargaRecadastramento
	 */
	public String getDsTipoCargaRecadastramento() {
		return dsTipoCargaRecadastramento;
	}

	/**
	 * Set: dsTipoCargaRecadastramento.
	 *
	 * @param dsTipoCargaRecadastramento the ds tipo carga recadastramento
	 */
	public void setDsTipoCargaRecadastramento(String dsTipoCargaRecadastramento) {
		this.dsTipoCargaRecadastramento = dsTipoCargaRecadastramento;
	}

	/**
	 * Get: dsTipoCartaoSalario.
	 *
	 * @return dsTipoCartaoSalario
	 */
	public String getDsTipoCartaoSalario() {
		return dsTipoCartaoSalario;
	}

	/**
	 * Set: dsTipoCartaoSalario.
	 *
	 * @param dsTipoCartaoSalario the ds tipo cartao salario
	 */
	public void setDsTipoCartaoSalario(String dsTipoCartaoSalario) {
		this.dsTipoCartaoSalario = dsTipoCartaoSalario;
	}

	/**
	 * Get: dsTipoDataFloating.
	 *
	 * @return dsTipoDataFloating
	 */
	public String getDsTipoDataFloating() {
		return dsTipoDataFloating;
	}

	/**
	 * Set: dsTipoDataFloating.
	 *
	 * @param dsTipoDataFloating the ds tipo data floating
	 */
	public void setDsTipoDataFloating(String dsTipoDataFloating) {
		this.dsTipoDataFloating = dsTipoDataFloating;
	}

	/**
	 * Get: dsTipoDivergenciaVeiculo.
	 *
	 * @return dsTipoDivergenciaVeiculo
	 */
	public String getDsTipoDivergenciaVeiculo() {
		return dsTipoDivergenciaVeiculo;
	}

	/**
	 * Set: dsTipoDivergenciaVeiculo.
	 *
	 * @param dsTipoDivergenciaVeiculo the ds tipo divergencia veiculo
	 */
	public void setDsTipoDivergenciaVeiculo(String dsTipoDivergenciaVeiculo) {
		this.dsTipoDivergenciaVeiculo = dsTipoDivergenciaVeiculo;
	}

	/**
	 * Get: dsTipoEfetivacaoPagamento.
	 *
	 * @return dsTipoEfetivacaoPagamento
	 */
	public String getDsTipoEfetivacaoPagamento() {
		return dsTipoEfetivacaoPagamento;
	}

	/**
	 * Set: dsTipoEfetivacaoPagamento.
	 *
	 * @param dsTipoEfetivacaoPagamento the ds tipo efetivacao pagamento
	 */
	public void setDsTipoEfetivacaoPagamento(String dsTipoEfetivacaoPagamento) {
		this.dsTipoEfetivacaoPagamento = dsTipoEfetivacaoPagamento;
	}

	/**
	 * Get: dsTipoIdentificacaoBeneficio.
	 *
	 * @return dsTipoIdentificacaoBeneficio
	 */
	public String getDsTipoIdentificacaoBeneficio() {
		return dsTipoIdentificacaoBeneficio;
	}

	/**
	 * Set: dsTipoIdentificacaoBeneficio.
	 *
	 * @param dsTipoIdentificacaoBeneficio the ds tipo identificacao beneficio
	 */
	public void setDsTipoIdentificacaoBeneficio(String dsTipoIdentificacaoBeneficio) {
		this.dsTipoIdentificacaoBeneficio = dsTipoIdentificacaoBeneficio;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Get: dsTipoReajusteTarifa.
	 *
	 * @return dsTipoReajusteTarifa
	 */
	public String getDsTipoReajusteTarifa() {
		return dsTipoReajusteTarifa;
	}

	/**
	 * Set: dsTipoReajusteTarifa.
	 *
	 * @param dsTipoReajusteTarifa the ds tipo reajuste tarifa
	 */
	public void setDsTipoReajusteTarifa(String dsTipoReajusteTarifa) {
		this.dsTipoReajusteTarifa = dsTipoReajusteTarifa;
	}

	/**
	 * Get: dsTratamentoContaTransferida.
	 *
	 * @return dsTratamentoContaTransferida
	 */
	public String getDsTratamentoContaTransferida() {
		return dsTratamentoContaTransferida;
	}

	/**
	 * Set: dsTratamentoContaTransferida.
	 *
	 * @param dsTratamentoContaTransferida the ds tratamento conta transferida
	 */
	public void setDsTratamentoContaTransferida(String dsTratamentoContaTransferida) {
		this.dsTratamentoContaTransferida = dsTratamentoContaTransferida;
	}

	/**
	 * Get: dsUtilizacaoFavorecidoControle.
	 *
	 * @return dsUtilizacaoFavorecidoControle
	 */
	public String getDsUtilizacaoFavorecidoControle() {
		return dsUtilizacaoFavorecidoControle;
	}

	/**
	 * Set: dsUtilizacaoFavorecidoControle.
	 *
	 * @param dsUtilizacaoFavorecidoControle the ds utilizacao favorecido controle
	 */
	public void setDsUtilizacaoFavorecidoControle(String dsUtilizacaoFavorecidoControle) {
		this.dsUtilizacaoFavorecidoControle = dsUtilizacaoFavorecidoControle;
	}

	/**
	 * Get: dtEnquaContaSalario.
	 *
	 * @return dtEnquaContaSalario
	 */
	public String getDtEnquaContaSalario() {
		return dtEnquaContaSalario;
	}

	/**
	 * Set: dtEnquaContaSalario.
	 *
	 * @param dtEnquaContaSalario the dt enqua conta salario
	 */
	public void setDtEnquaContaSalario(String dtEnquaContaSalario) {
		this.dtEnquaContaSalario = dtEnquaContaSalario;
	}

	/**
	 * Get: dtFimAcertoRecadastramento.
	 *
	 * @return dtFimAcertoRecadastramento
	 */
	public String getDtFimAcertoRecadastramento() {
		return dtFimAcertoRecadastramento;
	}

	/**
	 * Set: dtFimAcertoRecadastramento.
	 *
	 * @param dtFimAcertoRecadastramento the dt fim acerto recadastramento
	 */
	public void setDtFimAcertoRecadastramento(String dtFimAcertoRecadastramento) {
		this.dtFimAcertoRecadastramento = dtFimAcertoRecadastramento;
	}

	/**
	 * Get: dtFimRecadastramentoBeneficio.
	 *
	 * @return dtFimRecadastramentoBeneficio
	 */
	public String getDtFimRecadastramentoBeneficio() {
		return dtFimRecadastramentoBeneficio;
	}

	/**
	 * Set: dtFimRecadastramentoBeneficio.
	 *
	 * @param dtFimRecadastramentoBeneficio the dt fim recadastramento beneficio
	 */
	public void setDtFimRecadastramentoBeneficio(String dtFimRecadastramentoBeneficio) {
		this.dtFimRecadastramentoBeneficio = dtFimRecadastramentoBeneficio;
	}

	/**
	 * Get: dtInicioAcertoRecadastramento.
	 *
	 * @return dtInicioAcertoRecadastramento
	 */
	public String getDtInicioAcertoRecadastramento() {
		return dtInicioAcertoRecadastramento;
	}

	/**
	 * Set: dtInicioAcertoRecadastramento.
	 *
	 * @param dtInicioAcertoRecadastramento the dt inicio acerto recadastramento
	 */
	public void setDtInicioAcertoRecadastramento(String dtInicioAcertoRecadastramento) {
		this.dtInicioAcertoRecadastramento = dtInicioAcertoRecadastramento;
	}

	/**
	 * Get: dtInicioBloqueioPapeleta.
	 *
	 * @return dtInicioBloqueioPapeleta
	 */
	public String getDtInicioBloqueioPapeleta() {
		return dtInicioBloqueioPapeleta;
	}

	/**
	 * Set: dtInicioBloqueioPapeleta.
	 *
	 * @param dtInicioBloqueioPapeleta the dt inicio bloqueio papeleta
	 */
	public void setDtInicioBloqueioPapeleta(String dtInicioBloqueioPapeleta) {
		this.dtInicioBloqueioPapeleta = dtInicioBloqueioPapeleta;
	}

	/**
	 * Get: dtInicioRastreabilidadeTitulo.
	 *
	 * @return dtInicioRastreabilidadeTitulo
	 */
	public String getDtInicioRastreabilidadeTitulo() {
		return dtInicioRastreabilidadeTitulo;
	}

	/**
	 * Set: dtInicioRastreabilidadeTitulo.
	 *
	 * @param dtInicioRastreabilidadeTitulo the dt inicio rastreabilidade titulo
	 */
	public void setDtInicioRastreabilidadeTitulo(String dtInicioRastreabilidadeTitulo) {
		this.dtInicioRastreabilidadeTitulo = dtInicioRastreabilidadeTitulo;
	}

	/**
	 * Get: dtInicioRecadastramentoBeneficio.
	 *
	 * @return dtInicioRecadastramentoBeneficio
	 */
	public String getDtInicioRecadastramentoBeneficio() {
		return dtInicioRecadastramentoBeneficio;
	}

	/**
	 * Set: dtInicioRecadastramentoBeneficio.
	 *
	 * @param dtInicioRecadastramentoBeneficio the dt inicio recadastramento beneficio
	 */
	public void setDtInicioRecadastramentoBeneficio(String dtInicioRecadastramentoBeneficio) {
		this.dtInicioRecadastramentoBeneficio = dtInicioRecadastramentoBeneficio;
	}

	/**
	 * Get: dtLimiteVinculoCarga.
	 *
	 * @return dtLimiteVinculoCarga
	 */
	public String getDtLimiteVinculoCarga() {
		return dtLimiteVinculoCarga;
	}

	/**
	 * Set: dtLimiteVinculoCarga.
	 *
	 * @param dtLimiteVinculoCarga the dt limite vinculo carga
	 */
	public void setDtLimiteVinculoCarga(String dtLimiteVinculoCarga) {
		this.dtLimiteVinculoCarga = dtLimiteVinculoCarga;
	}

	/**
	 * Get: dtRegistroTitulo.
	 *
	 * @return dtRegistroTitulo
	 */
	public String getDtRegistroTitulo() {
		return dtRegistroTitulo;
	}

	/**
	 * Set: dtRegistroTitulo.
	 *
	 * @param dtRegistroTitulo the dt registro titulo
	 */
	public void setDtRegistroTitulo(String dtRegistroTitulo) {
		this.dtRegistroTitulo = dtRegistroTitulo;
	}

	/**
	 * Get: hrInclusaoRegistroHistorico.
	 *
	 * @return hrInclusaoRegistroHistorico
	 */
	public String getHrInclusaoRegistroHistorico() {
		return hrInclusaoRegistroHistorico;
	}

	/**
	 * Set: hrInclusaoRegistroHistorico.
	 *
	 * @param hrInclusaoRegistroHistorico the hr inclusao registro historico
	 */
	public void setHrInclusaoRegistroHistorico(String hrInclusaoRegistroHistorico) {
		this.hrInclusaoRegistroHistorico = hrInclusaoRegistroHistorico;
	}

	/**
	 * Get: hrManutencaoRegistroAlteracao.
	 *
	 * @return hrManutencaoRegistroAlteracao
	 */
	public String getHrManutencaoRegistroAlteracao() {
		return hrManutencaoRegistroAlteracao;
	}

	/**
	 * Set: hrManutencaoRegistroAlteracao.
	 *
	 * @param hrManutencaoRegistroAlteracao the hr manutencao registro alteracao
	 */
	public void setHrManutencaoRegistroAlteracao(String hrManutencaoRegistroAlteracao) {
		this.hrManutencaoRegistroAlteracao = hrManutencaoRegistroAlteracao;
	}

	/**
	 * Get: hrManutencaoRegistroInclusao.
	 *
	 * @return hrManutencaoRegistroInclusao
	 */
	public String getHrManutencaoRegistroInclusao() {
		return hrManutencaoRegistroInclusao;
	}

	/**
	 * Set: hrManutencaoRegistroInclusao.
	 *
	 * @param hrManutencaoRegistroInclusao the hr manutencao registro inclusao
	 */
	public void setHrManutencaoRegistroInclusao(String hrManutencaoRegistroInclusao) {
		this.hrManutencaoRegistroInclusao = hrManutencaoRegistroInclusao;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nrFechamentoApuracaoTarifa.
	 *
	 * @return nrFechamentoApuracaoTarifa
	 */
	public long getNrFechamentoApuracaoTarifa() {
		return nrFechamentoApuracaoTarifa;
	}

	/**
	 * Set: nrFechamentoApuracaoTarifa.
	 *
	 * @param nrFechamentoApuracaoTarifa the nr fechamento apuracao tarifa
	 */
	public void setNrFechamentoApuracaoTarifa(long nrFechamentoApuracaoTarifa) {
		this.nrFechamentoApuracaoTarifa = nrFechamentoApuracaoTarifa;
	}

	/**
	 * Get: nrOperacaoFluxoAlteracao.
	 *
	 * @return nrOperacaoFluxoAlteracao
	 */
	public String getNrOperacaoFluxoAlteracao() {
		return nrOperacaoFluxoAlteracao;
	}

	/**
	 * Set: nrOperacaoFluxoAlteracao.
	 *
	 * @param nrOperacaoFluxoAlteracao the nr operacao fluxo alteracao
	 */
	public void setNrOperacaoFluxoAlteracao(String nrOperacaoFluxoAlteracao) {
		this.nrOperacaoFluxoAlteracao = nrOperacaoFluxoAlteracao;
	}

	/**
	 * Get: nrOperacaoFluxoInclusao.
	 *
	 * @return nrOperacaoFluxoInclusao
	 */
	public String getNrOperacaoFluxoInclusao() {
		return nrOperacaoFluxoInclusao;
	}

	/**
	 * Set: nrOperacaoFluxoInclusao.
	 *
	 * @param nrOperacaoFluxoInclusao the nr operacao fluxo inclusao
	 */
	public void setNrOperacaoFluxoInclusao(String nrOperacaoFluxoInclusao) {
		this.nrOperacaoFluxoInclusao = nrOperacaoFluxoInclusao;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: percentualIndiceReajusteTarifa.
	 *
	 * @return percentualIndiceReajusteTarifa
	 */
	public BigDecimal getPercentualIndiceReajusteTarifa() {
		return percentualIndiceReajusteTarifa;
	}

	/**
	 * Set: percentualIndiceReajusteTarifa.
	 *
	 * @param percentualIndiceReajusteTarifa the percentual indice reajuste tarifa
	 */
	public void setPercentualIndiceReajusteTarifa(BigDecimal percentualIndiceReajusteTarifa) {
		this.percentualIndiceReajusteTarifa = percentualIndiceReajusteTarifa;
	}

	/**
	 * Get: percentualMaximoInconsistenteLote.
	 *
	 * @return percentualMaximoInconsistenteLote
	 */
	public int getPercentualMaximoInconsistenteLote() {
		return percentualMaximoInconsistenteLote;
	}

	/**
	 * Set: percentualMaximoInconsistenteLote.
	 *
	 * @param percentualMaximoInconsistenteLote the percentual maximo inconsistente lote
	 */
	public void setPercentualMaximoInconsistenteLote(int percentualMaximoInconsistenteLote) {
		this.percentualMaximoInconsistenteLote = percentualMaximoInconsistenteLote;
	}

	/**
	 * Get: periodicidadeTarifaCatalogo.
	 *
	 * @return periodicidadeTarifaCatalogo
	 */
	public BigDecimal getPeriodicidadeTarifaCatalogo() {
		return periodicidadeTarifaCatalogo;
	}

	/**
	 * Set: periodicidadeTarifaCatalogo.
	 *
	 * @param periodicidadeTarifaCatalogo the periodicidade tarifa catalogo
	 */
	public void setPeriodicidadeTarifaCatalogo(BigDecimal periodicidadeTarifaCatalogo) {
		this.periodicidadeTarifaCatalogo = periodicidadeTarifaCatalogo;
	}

	/**
	 * Get: quantidadeAntecedencia.
	 *
	 * @return quantidadeAntecedencia
	 */
	public int getQuantidadeAntecedencia() {
		return quantidadeAntecedencia;
	}

	/**
	 * Set: quantidadeAntecedencia.
	 *
	 * @param quantidadeAntecedencia the quantidade antecedencia
	 */
	public void setQuantidadeAntecedencia(int quantidadeAntecedencia) {
		this.quantidadeAntecedencia = quantidadeAntecedencia;
	}

	/**
	 * Get: quantidadeAnteriorVencimentoComprovante.
	 *
	 * @return quantidadeAnteriorVencimentoComprovante
	 */
	public int getQuantidadeAnteriorVencimentoComprovante() {
		return quantidadeAnteriorVencimentoComprovante;
	}

	/**
	 * Set: quantidadeAnteriorVencimentoComprovante.
	 *
	 * @param quantidadeAnteriorVencimentoComprovante the quantidade anterior vencimento comprovante
	 */
	public void setQuantidadeAnteriorVencimentoComprovante(int quantidadeAnteriorVencimentoComprovante) {
		this.quantidadeAnteriorVencimentoComprovante = quantidadeAnteriorVencimentoComprovante;
	}

	/**
	 * Get: quantidadeDiaCobrancaTarifa.
	 *
	 * @return quantidadeDiaCobrancaTarifa
	 */
	public int getQuantidadeDiaCobrancaTarifa() {
		return quantidadeDiaCobrancaTarifa;
	}

	/**
	 * Set: quantidadeDiaCobrancaTarifa.
	 *
	 * @param quantidadeDiaCobrancaTarifa the quantidade dia cobranca tarifa
	 */
	public void setQuantidadeDiaCobrancaTarifa(int quantidadeDiaCobrancaTarifa) {
		this.quantidadeDiaCobrancaTarifa = quantidadeDiaCobrancaTarifa;
	}

	/**
	 * Get: quantidadeDiaExpiracao.
	 *
	 * @return quantidadeDiaExpiracao
	 */
	public int getQuantidadeDiaExpiracao() {
		return quantidadeDiaExpiracao;
	}

	/**
	 * Set: quantidadeDiaExpiracao.
	 *
	 * @param quantidadeDiaExpiracao the quantidade dia expiracao
	 */
	public void setQuantidadeDiaExpiracao(int quantidadeDiaExpiracao) {
		this.quantidadeDiaExpiracao = quantidadeDiaExpiracao;
	}

	/**
	 * Get: quantidadeDiaFloatingPagamento.
	 *
	 * @return quantidadeDiaFloatingPagamento
	 */
	public int getQuantidadeDiaFloatingPagamento() {
		return quantidadeDiaFloatingPagamento;
	}

	/**
	 * Set: quantidadeDiaFloatingPagamento.
	 *
	 * @param quantidadeDiaFloatingPagamento the quantidade dia floating pagamento
	 */
	public void setQuantidadeDiaFloatingPagamento(int quantidadeDiaFloatingPagamento) {
		this.quantidadeDiaFloatingPagamento = quantidadeDiaFloatingPagamento;
	}

	/**
	 * Get: quantidadeDiaInatividadeFavorecido.
	 *
	 * @return quantidadeDiaInatividadeFavorecido
	 */
	public int getQuantidadeDiaInatividadeFavorecido() {
		return quantidadeDiaInatividadeFavorecido;
	}

	/**
	 * Set: quantidadeDiaInatividadeFavorecido.
	 *
	 * @param quantidadeDiaInatividadeFavorecido the quantidade dia inatividade favorecido
	 */
	public void setQuantidadeDiaInatividadeFavorecido(int quantidadeDiaInatividadeFavorecido) {
		this.quantidadeDiaInatividadeFavorecido = quantidadeDiaInatividadeFavorecido;
	}

	/**
	 * Get: quantidadeDiaRepiqueConsulta.
	 *
	 * @return quantidadeDiaRepiqueConsulta
	 */
	public int getQuantidadeDiaRepiqueConsulta() {
		return quantidadeDiaRepiqueConsulta;
	}

	/**
	 * Set: quantidadeDiaRepiqueConsulta.
	 *
	 * @param quantidadeDiaRepiqueConsulta the quantidade dia repique consulta
	 */
	public void setQuantidadeDiaRepiqueConsulta(int quantidadeDiaRepiqueConsulta) {
		this.quantidadeDiaRepiqueConsulta = quantidadeDiaRepiqueConsulta;
	}

	/**
	 * Get: quantidadeEtapaRecadastramentoBeneficio.
	 *
	 * @return quantidadeEtapaRecadastramentoBeneficio
	 */
	public int getQuantidadeEtapaRecadastramentoBeneficio() {
		return quantidadeEtapaRecadastramentoBeneficio;
	}

	/**
	 * Set: quantidadeEtapaRecadastramentoBeneficio.
	 *
	 * @param quantidadeEtapaRecadastramentoBeneficio the quantidade etapa recadastramento beneficio
	 */
	public void setQuantidadeEtapaRecadastramentoBeneficio(int quantidadeEtapaRecadastramentoBeneficio) {
		this.quantidadeEtapaRecadastramentoBeneficio = quantidadeEtapaRecadastramentoBeneficio;
	}

	/**
	 * Get: quantidadeFaseRecadastramentoBeneficio.
	 *
	 * @return quantidadeFaseRecadastramentoBeneficio
	 */
	public int getQuantidadeFaseRecadastramentoBeneficio() {
		return quantidadeFaseRecadastramentoBeneficio;
	}

	/**
	 * Set: quantidadeFaseRecadastramentoBeneficio.
	 *
	 * @param quantidadeFaseRecadastramentoBeneficio the quantidade fase recadastramento beneficio
	 */
	public void setQuantidadeFaseRecadastramentoBeneficio(int quantidadeFaseRecadastramentoBeneficio) {
		this.quantidadeFaseRecadastramentoBeneficio = quantidadeFaseRecadastramentoBeneficio;
	}

	/**
	 * Get: quantidadeLimiteLinha.
	 *
	 * @return quantidadeLimiteLinha
	 */
	public int getQuantidadeLimiteLinha() {
		return quantidadeLimiteLinha;
	}

	/**
	 * Set: quantidadeLimiteLinha.
	 *
	 * @param quantidadeLimiteLinha the quantidade limite linha
	 */
	public void setQuantidadeLimiteLinha(int quantidadeLimiteLinha) {
		this.quantidadeLimiteLinha = quantidadeLimiteLinha;
	}

	/**
	 * Get: quantidadeMaximaInconsistenteLote.
	 *
	 * @return quantidadeMaximaInconsistenteLote
	 */
	public int getQuantidadeMaximaInconsistenteLote() {
		return quantidadeMaximaInconsistenteLote;
	}

	/**
	 * Set: quantidadeMaximaInconsistenteLote.
	 *
	 * @param quantidadeMaximaInconsistenteLote the quantidade maxima inconsistente lote
	 */
	public void setQuantidadeMaximaInconsistenteLote(int quantidadeMaximaInconsistenteLote) {
		this.quantidadeMaximaInconsistenteLote = quantidadeMaximaInconsistenteLote;
	}

	/**
	 * Get: quantidadeMaximaTituloVencido.
	 *
	 * @return quantidadeMaximaTituloVencido
	 */
	public int getQuantidadeMaximaTituloVencido() {
		return quantidadeMaximaTituloVencido;
	}

	/**
	 * Set: quantidadeMaximaTituloVencido.
	 *
	 * @param quantidadeMaximaTituloVencido the quantidade maxima titulo vencido
	 */
	public void setQuantidadeMaximaTituloVencido(int quantidadeMaximaTituloVencido) {
		this.quantidadeMaximaTituloVencido = quantidadeMaximaTituloVencido;
	}

	/**
	 * Get: quantidadeMesComprovante.
	 *
	 * @return quantidadeMesComprovante
	 */
	public int getQuantidadeMesComprovante() {
		return quantidadeMesComprovante;
	}

	/**
	 * Set: quantidadeMesComprovante.
	 *
	 * @param quantidadeMesComprovante the quantidade mes comprovante
	 */
	public void setQuantidadeMesComprovante(int quantidadeMesComprovante) {
		this.quantidadeMesComprovante = quantidadeMesComprovante;
	}

	/**
	 * Get: quantidadeMesEtapaRecadastramento.
	 *
	 * @return quantidadeMesEtapaRecadastramento
	 */
	public int getQuantidadeMesEtapaRecadastramento() {
		return quantidadeMesEtapaRecadastramento;
	}

	/**
	 * Set: quantidadeMesEtapaRecadastramento.
	 *
	 * @param quantidadeMesEtapaRecadastramento the quantidade mes etapa recadastramento
	 */
	public void setQuantidadeMesEtapaRecadastramento(int quantidadeMesEtapaRecadastramento) {
		this.quantidadeMesEtapaRecadastramento = quantidadeMesEtapaRecadastramento;
	}

	/**
	 * Get: quantidadeMesFaseRecadastramento.
	 *
	 * @return quantidadeMesFaseRecadastramento
	 */
	public int getQuantidadeMesFaseRecadastramento() {
		return quantidadeMesFaseRecadastramento;
	}

	/**
	 * Set: quantidadeMesFaseRecadastramento.
	 *
	 * @param quantidadeMesFaseRecadastramento the quantidade mes fase recadastramento
	 */
	public void setQuantidadeMesFaseRecadastramento(int quantidadeMesFaseRecadastramento) {
		this.quantidadeMesFaseRecadastramento = quantidadeMesFaseRecadastramento;
	}

	/**
	 * Get: quantidadeMesReajusteTarifa.
	 *
	 * @return quantidadeMesReajusteTarifa
	 */
	public int getQuantidadeMesReajusteTarifa() {
		return quantidadeMesReajusteTarifa;
	}

	/**
	 * Set: quantidadeMesReajusteTarifa.
	 *
	 * @param quantidadeMesReajusteTarifa the quantidade mes reajuste tarifa
	 */
	public void setQuantidadeMesReajusteTarifa(int quantidadeMesReajusteTarifa) {
		this.quantidadeMesReajusteTarifa = quantidadeMesReajusteTarifa;
	}

	/**
	 * Get: quantidadeSolicitacaoCartao.
	 *
	 * @return quantidadeSolicitacaoCartao
	 */
	public int getQuantidadeSolicitacaoCartao() {
		return quantidadeSolicitacaoCartao;
	}

	/**
	 * Set: quantidadeSolicitacaoCartao.
	 *
	 * @param quantidadeSolicitacaoCartao the quantidade solicitacao cartao
	 */
	public void setQuantidadeSolicitacaoCartao(int quantidadeSolicitacaoCartao) {
		this.quantidadeSolicitacaoCartao = quantidadeSolicitacaoCartao;
	}

	/**
	 * Get: quantidadeViaAviso.
	 *
	 * @return quantidadeViaAviso
	 */
	public int getQuantidadeViaAviso() {
		return quantidadeViaAviso;
	}

	/**
	 * Set: quantidadeViaAviso.
	 *
	 * @param quantidadeViaAviso the quantidade via aviso
	 */
	public void setQuantidadeViaAviso(int quantidadeViaAviso) {
		this.quantidadeViaAviso = quantidadeViaAviso;
	}

	/**
	 * Get: quantidadeViaCombranca.
	 *
	 * @return quantidadeViaCombranca
	 */
	public int getQuantidadeViaCombranca() {
		return quantidadeViaCombranca;
	}

	/**
	 * Set: quantidadeViaCombranca.
	 *
	 * @param quantidadeViaCombranca the quantidade via combranca
	 */
	public void setQuantidadeViaCombranca(int quantidadeViaCombranca) {
		this.quantidadeViaCombranca = quantidadeViaCombranca;
	}

	/**
	 * Get: quantidadeViaComprovante.
	 *
	 * @return quantidadeViaComprovante
	 */
	public int getQuantidadeViaComprovante() {
		return quantidadeViaComprovante;
	}

	/**
	 * Set: quantidadeViaComprovante.
	 *
	 * @param quantidadeViaComprovante the quantidade via comprovante
	 */
	public void setQuantidadeViaComprovante(int quantidadeViaComprovante) {
		this.quantidadeViaComprovante = quantidadeViaComprovante;
	}

	/**
	 * Get: valorFavorecidoNaoCadastrado.
	 *
	 * @return valorFavorecidoNaoCadastrado
	 */
	public BigDecimal getValorFavorecidoNaoCadastrado() {
		return valorFavorecidoNaoCadastrado;
	}

	/**
	 * Set: valorFavorecidoNaoCadastrado.
	 *
	 * @param valorFavorecidoNaoCadastrado the valor favorecido nao cadastrado
	 */
	public void setValorFavorecidoNaoCadastrado(BigDecimal valorFavorecidoNaoCadastrado) {
		this.valorFavorecidoNaoCadastrado = valorFavorecidoNaoCadastrado;
	}

	/**
	 * Get: valorLimiteDiarioPagamento.
	 *
	 * @return valorLimiteDiarioPagamento
	 */
	public BigDecimal getValorLimiteDiarioPagamento() {
		return valorLimiteDiarioPagamento;
	}

	/**
	 * Set: valorLimiteDiarioPagamento.
	 *
	 * @param valorLimiteDiarioPagamento the valor limite diario pagamento
	 */
	public void setValorLimiteDiarioPagamento(BigDecimal valorLimiteDiarioPagamento) {
		this.valorLimiteDiarioPagamento = valorLimiteDiarioPagamento;
	}

	/**
	 * Get: valorLimiteIndividualPagamento.
	 *
	 * @return valorLimiteIndividualPagamento
	 */
	public BigDecimal getValorLimiteIndividualPagamento() {
		return valorLimiteIndividualPagamento;
	}

	/**
	 * Set: valorLimiteIndividualPagamento.
	 *
	 * @param valorLimiteIndividualPagamento the valor limite individual pagamento
	 */
	public void setValorLimiteIndividualPagamento(BigDecimal valorLimiteIndividualPagamento) {
		this.valorLimiteIndividualPagamento = valorLimiteIndividualPagamento;
	}

	/**
	 * Get: cdAmbienteServicoContrato.
	 *
	 * @return cdAmbienteServicoContrato
	 */
	public String getCdAmbienteServicoContrato() {
		return cdAmbienteServicoContrato;
	}

	/**
	 * Set: cdAmbienteServicoContrato.
	 *
	 * @param cdAmbienteServicoContrato the cd ambiente servico contrato
	 */
	public void setCdAmbienteServicoContrato(String cdAmbienteServicoContrato) {
		this.cdAmbienteServicoContrato = cdAmbienteServicoContrato;
	}

	/**
	 * Get: dsLocalEmissao.
	 *
	 * @return dsLocalEmissao
	 */
	public String getDsLocalEmissao() {
		return dsLocalEmissao;
	}

	/**
	 * Set: dsLocalEmissao.
	 *
	 * @param dsLocalEmissao the ds local emissao
	 */
	public void setDsLocalEmissao(String dsLocalEmissao) {
		this.dsLocalEmissao = dsLocalEmissao;
	}

	/**
	 * Get: dsSituacaoServicoRelacionado.
	 *
	 * @return dsSituacaoServicoRelacionado
	 */
	public String getDsSituacaoServicoRelacionado() {
		return dsSituacaoServicoRelacionado;
	}

	/**
	 * Set: dsSituacaoServicoRelacionado.
	 *
	 * @param dsSituacaoServicoRelacionado the ds situacao servico relacionado
	 */
	public void setDsSituacaoServicoRelacionado(String dsSituacaoServicoRelacionado) {
		this.dsSituacaoServicoRelacionado = dsSituacaoServicoRelacionado;
	}

	/**
	 * Get: cdConsultaSaldoValorSuperior.
	 *
	 * @return cdConsultaSaldoValorSuperior
	 */
	public Integer getCdConsultaSaldoValorSuperior() {
		return cdConsultaSaldoValorSuperior;
	}

	/**
	 * Set: cdConsultaSaldoValorSuperior.
	 *
	 * @param cdConsultaSaldoValorSuperior the cd consulta saldo valor superior
	 */
	public void setCdConsultaSaldoValorSuperior(Integer cdConsultaSaldoValorSuperior) {
		this.cdConsultaSaldoValorSuperior = cdConsultaSaldoValorSuperior;
	}

	/**
	 * Get: dsConsultaSaldoValorSuperior.
	 *
	 * @return dsConsultaSaldoValorSuperior
	 */
	public String getDsConsultaSaldoValorSuperior() {
		return dsConsultaSaldoValorSuperior;
	}

	/**
	 * Set: dsConsultaSaldoValorSuperior.
	 *
	 * @param dsConsultaSaldoValorSuperior the ds consulta saldo valor superior
	 */
	public void setDsConsultaSaldoValorSuperior(String dsConsultaSaldoValorSuperior) {
		this.dsConsultaSaldoValorSuperior = dsConsultaSaldoValorSuperior;
	}

	/**
	 * Get: dsIndicadorFeriadoLocal.
	 *
	 * @return dsIndicadorFeriadoLocal
	 */
	public String getDsIndicadorFeriadoLocal() {
		return dsIndicadorFeriadoLocal;
	}

	/**
	 * Set: dsIndicadorFeriadoLocal.
	 *
	 * @param dsIndicadorFeriadoLocal the ds indicador feriado local
	 */
	public void setDsIndicadorFeriadoLocal(String dsIndicadorFeriadoLocal) {
		this.dsIndicadorFeriadoLocal = dsIndicadorFeriadoLocal;
	}

	/**
	 * Get: cdIndicadorFeriadoLocal.
	 *
	 * @return cdIndicadorFeriadoLocal
	 */
	public Integer getCdIndicadorFeriadoLocal() {
		return cdIndicadorFeriadoLocal;
	}

	/**
	 * Set: cdIndicadorFeriadoLocal.
	 *
	 * @param cdIndicadorFeriadoLocal the cd indicador feriado local
	 */
	public void setCdIndicadorFeriadoLocal(Integer cdIndicadorFeriadoLocal) {
		this.cdIndicadorFeriadoLocal = cdIndicadorFeriadoLocal;
	}

	/**
	 * Get: cdIndicadorSegundaLinha.
	 *
	 * @return cdIndicadorSegundaLinha
	 */
	public Integer getCdIndicadorSegundaLinha() {
		return cdIndicadorSegundaLinha;
	}

	/**
	 * Set: cdIndicadorSegundaLinha.
	 *
	 * @param cdIndicadorSegundaLinha the cd indicador segunda linha
	 */
	public void setCdIndicadorSegundaLinha(Integer cdIndicadorSegundaLinha) {
		this.cdIndicadorSegundaLinha = cdIndicadorSegundaLinha;
	}

	/**
	 * Get: dsIndicadorSegundaLinha.
	 *
	 * @return dsIndicadorSegundaLinha
	 */
	public String getDsIndicadorSegundaLinha() {
		return dsIndicadorSegundaLinha;
	}

	/**
	 * Set: dsIndicadorSegundaLinha.
	 *
	 * @param dsIndicadorSegundaLinha the ds indicador segunda linha
	 */
	public void setDsIndicadorSegundaLinha(String dsIndicadorSegundaLinha) {
		this.dsIndicadorSegundaLinha = dsIndicadorSegundaLinha;
	}

	/**
	 * Gets the cd float servico contrato.
	 *
	 * @return the cd float servico contrato
	 */
	public Integer getCdFloatServicoContrato() {
		return cdFloatServicoContrato;
	}

	/**
	 * Sets the cd float servico contrato.
	 *
	 * @param cdFloatServicoContrato the new cd float servico contrato
	 */
	public void setCdFloatServicoContrato(Integer cdFloatServicoContrato) {
		this.cdFloatServicoContrato = cdFloatServicoContrato;
	}

	/**
	 * Gets the ds float servico contrato.
	 *
	 * @return the ds float servico contrato
	 */
	public String getDsFloatServicoContrato() {
		return dsFloatServicoContrato;
	}

	/**
	 * Sets the ds float servico contrato.
	 *
	 * @param dsFloatServicoContrato the new ds float servico contrato
	 */
	public void setDsFloatServicoContrato(String dsFloatServicoContrato) {
		this.dsFloatServicoContrato = dsFloatServicoContrato;
	}

	/**
	 * Nome: setDsCodigoIndFeriadoLocal.
	 *
	 * @param dsCodigoIndFeriadoLocal the new ds codigo ind feriado local
	 */
	public void setDsCodigoIndFeriadoLocal(String dsCodigoIndFeriadoLocal) {
		this.dsCodigoIndFeriadoLocal = dsCodigoIndFeriadoLocal;
	}

	/**
	 * Nome: getDsCodigoIndFeriadoLocal.
	 *
	 * @return dsCodigoIndFeriadoLocal
	 */
	public String getDsCodigoIndFeriadoLocal() {
		return dsCodigoIndFeriadoLocal;
	}

	/**
	 * Sets the ds exige filial.
	 *
	 * @param dsExigeFilial the new ds exige filial
	 */
	public void setDsExigeFilial(String dsExigeFilial) {
		this.dsExigeFilial = dsExigeFilial;
	}

	/**
	 * Gets the ds exige filial.
	 *
	 * @return the ds exige filial
	 */
	public String getDsExigeFilial() {
		return dsExigeFilial;
	}

    /**
     * Nome: getCdPreenchimentoLancamentoPersonalizado.
     *
     * @return cdPreenchimentoLancamentoPersonalizado
     */
    public Integer getCdPreenchimentoLancamentoPersonalizado() {
        return cdPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: setCdPreenchimentoLancamentoPersonalizado.
     *
     * @param cdPreenchimentoLancamentoPersonalizado the new cd preenchimento lancamento personalizado
     */
    public void setCdPreenchimentoLancamentoPersonalizado(Integer cdPreenchimentoLancamentoPersonalizado) {
        this.cdPreenchimentoLancamentoPersonalizado = cdPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: getDsPreenchimentoLancamentoPersonalizado.
     *
     * @return dsPreenchimentoLancamentoPersonalizado
     */
    public String getDsPreenchimentoLancamentoPersonalizado() {
        return dsPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: setDsPreenchimentoLancamentoPersonalizado.
     *
     * @param dsPreenchimentoLancamentoPersonalizado the new ds preenchimento lancamento personalizado
     */
    public void setDsPreenchimentoLancamentoPersonalizado(String dsPreenchimentoLancamentoPersonalizado) {
        this.dsPreenchimentoLancamentoPersonalizado = dsPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: getCdTituloDdaRetorno.
     *
     * @return cdTituloDdaRetorno
     */
    public Integer getCdTituloDdaRetorno() {
        return cdTituloDdaRetorno;
    }

    /**
     * Nome: setCdTituloDdaRetorno.
     *
     * @param cdTituloDdaRetorno the new cd titulo dda retorno
     */
    public void setCdTituloDdaRetorno(Integer cdTituloDdaRetorno) {
        this.cdTituloDdaRetorno = cdTituloDdaRetorno;
    }

    /**
     * Nome: getDsTituloDdaRetorno.
     *
     * @return dsTituloDdaRetorno
     */
    public String getDsTituloDdaRetorno() {
        return dsTituloDdaRetorno;
    }

    /**
     * Nome: setDsTituloDdaRetorno.
     *
     * @param dsTituloDdaRetorno the new ds titulo dda retorno
     */
    public void setDsTituloDdaRetorno(String dsTituloDdaRetorno) {
        this.dsTituloDdaRetorno = dsTituloDdaRetorno;
    }

    /**
     * Nome: getCdIndicadorAgendaGrade.
     *
     * @return cdIndicadorAgendaGrade
     */
    public Integer getCdIndicadorAgendaGrade() {
        return cdIndicadorAgendaGrade;
    }

    /**
     * Nome: setCdIndicadorAgendaGrade.
     *
     * @param cdIndicadorAgendaGrade the new cd indicador agenda grade
     */
    public void setCdIndicadorAgendaGrade(Integer cdIndicadorAgendaGrade) {
        this.cdIndicadorAgendaGrade = cdIndicadorAgendaGrade;
    }

    /**
     * Nome: getDsIndicadorAgendaGrade.
     *
     * @return dsIndicadorAgendaGrade
     */
    public String getDsIndicadorAgendaGrade() {
        return dsIndicadorAgendaGrade;
    }

    /**
     * Nome: setDsIndicadorAgendaGrade.
     *
     * @param dsIndicadorAgendaGrade the new ds indicador agenda grade
     */
    public void setDsIndicadorAgendaGrade(String dsIndicadorAgendaGrade) {
        this.dsIndicadorAgendaGrade = dsIndicadorAgendaGrade;
    }

	/**
	 * Sets the qt dia util pgto.
	 *
	 * @param qtDiaUtilPgto the qtDiaUtilPgto to set
	 */
	public void setQtDiaUtilPgto(Integer qtDiaUtilPgto) {
		this.qtDiaUtilPgto = qtDiaUtilPgto;
	}

	/**
	 * Gets the qt dia util pgto.
	 *
	 * @return the qtDiaUtilPgto
	 */
	public Integer getQtDiaUtilPgto() {
		return qtDiaUtilPgto;
	}

	/**
     * Get: cdIndicadorUtilizaMora.
     * 
     * @return cdIndicadorUtilizaMora
     */
	public Integer getCdIndicadorUtilizaMora() {
		return cdIndicadorUtilizaMora;
	}

	/**
     * Set: cdIndicadorUtilizaMora.
     * 
     * @param cdIndicadorUtilizaMora
     *            the cd indicador utiliza mora
     */
	public void setCdIndicadorUtilizaMora(Integer cdIndicadorUtilizaMora) {
		this.cdIndicadorUtilizaMora = cdIndicadorUtilizaMora;
	}

	/**
     * Get: dsIndicadorUtilizaMora.
     * 
     * @return dsIndicadorUtilizaMora
     */
	public String getDsIndicadorUtilizaMora() {
		return dsIndicadorUtilizaMora;
	}

	/**
     * Set: dsIndicadorUtilizaMora.
     * 
     * @param dsIndicadorUtilizaMora
     *            the ds indicador utiliza mora
     */
	public void setDsIndicadorUtilizaMora(String dsIndicadorUtilizaMora) {
		this.dsIndicadorUtilizaMora = dsIndicadorUtilizaMora;
	}

    /**
     * Nome: getVlPercentualDiferencaTolerada
     *
     * @return vlPercentualDiferencaTolerada
     */
    public BigDecimal getVlPercentualDiferencaTolerada() {
        return vlPercentualDiferencaTolerada;
    }

    /**
     * Nome: setVlPercentualDiferencaTolerada
     *
     * @param vlPercentualDiferencaTolerada
     */
    public void setVlPercentualDiferencaTolerada(BigDecimal vlPercentualDiferencaTolerada) {
        this.vlPercentualDiferencaTolerada = vlPercentualDiferencaTolerada;
    }

	public Integer getCdConsistenciaCpfCnpjBenefAvalNpc() {
		return cdConsistenciaCpfCnpjBenefAvalNpc;
	}

	public void setCdConsistenciaCpfCnpjBenefAvalNpc(
			Integer cdConsistenciaCpfCnpjBenefAvalNpc) {
		this.cdConsistenciaCpfCnpjBenefAvalNpc = cdConsistenciaCpfCnpjBenefAvalNpc;
	}

	public String getDsConsistenciaCpfCnpjBenefAvalNpc() {
		return dsConsistenciaCpfCnpjBenefAvalNpc;
	}

	public void setDsConsistenciaCpfCnpjBenefAvalNpc(
			String dsConsistenciaCpfCnpjBenefAvalNpc) {
		this.dsConsistenciaCpfCnpjBenefAvalNpc = dsConsistenciaCpfCnpjBenefAvalNpc;
	}

	public void setDsIndicador(String dsIndicador) {
		this.dsIndicador = dsIndicador;
	}

	public String getDsIndicador() {
		return dsIndicador;
	}

	public void setDsOrigemIndicador(int dsOrigemIndicador) {
		this.dsOrigemIndicador = dsOrigemIndicador;
	}

	public int getDsOrigemIndicador() {
		return dsOrigemIndicador;
	}

	public int getCdIdentificadorTipoRetornoInternet() {
		return cdIdentificadorTipoRetornoInternet;
	}

	public void setCdIdentificadorTipoRetornoInternet(int cdIdentificadorTipoRetornoInternet) {
		this.cdIdentificadorTipoRetornoInternet = cdIdentificadorTipoRetornoInternet;
	}

	public String getDsCodIdentificadorTipoRetornoInternet() {
		return dsCodIdentificadorTipoRetornoInternet;
	}

	public void setDsCodIdentificadorTipoRetornoInternet(String dsCodIdentificadorTipoRetornoInternet) {
		this.dsCodIdentificadorTipoRetornoInternet = dsCodIdentificadorTipoRetornoInternet;
	}
    
    
}