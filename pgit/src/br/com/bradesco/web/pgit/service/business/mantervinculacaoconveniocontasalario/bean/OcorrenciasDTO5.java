package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.ListarConvenioContaSalarioResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarconveniocontasalario.response.Ocorrencias5;

public class OcorrenciasDTO5 implements Serializable{

	private static final long serialVersionUID = -4679365200889859876L;
	
	private Integer cdBancoFlQuest;
	private String dsBancoFlQuest;
	private BigDecimal pctBancoFlQuest;
	
	public OcorrenciasDTO5() {
		super();
	}

	public OcorrenciasDTO5(Integer cdBancoFlQuest, String dsBancoFlQuest,
			BigDecimal pctBancoFlQuest) {
		super();
		this.cdBancoFlQuest = cdBancoFlQuest;
		this.dsBancoFlQuest = dsBancoFlQuest;
		this.pctBancoFlQuest = pctBancoFlQuest;
	}
	
	public List<OcorrenciasDTO5> listarOcorrenciasDTO5(ListarConvenioContaSalarioResponse response) {
		
		List<OcorrenciasDTO5> listaOcorrenciasDTO5 = new ArrayList<OcorrenciasDTO5>();
		OcorrenciasDTO5 ocorrenciasDTO5 = new OcorrenciasDTO5();
		
		for(Ocorrencias5 retorno : response.getOcorrencias5()){
			ocorrenciasDTO5.setCdBancoFlQuest(retorno.getCdBancoFlQuest());
			ocorrenciasDTO5.setDsBancoFlQuest(retorno.getDsBancoFlQuest());
			ocorrenciasDTO5.setPctBancoFlQuest(retorno.getPctBancoFlQuest());
			listaOcorrenciasDTO5.add(ocorrenciasDTO5);
		}
		return listaOcorrenciasDTO5;
	}

	public Integer getCdBancoFlQuest() {
		return cdBancoFlQuest;
	}

	public void setCdBancoFlQuest(Integer cdBancoFlQuest) {
		this.cdBancoFlQuest = cdBancoFlQuest;
	}

	public String getDsBancoFlQuest() {
		return dsBancoFlQuest;
	}

	public void setDsBancoFlQuest(String dsBancoFlQuest) {
		this.dsBancoFlQuest = dsBancoFlQuest;
	}

	public BigDecimal getPctBancoFlQuest() {
		return pctBancoFlQuest;
	}

	public void setPctBancoFlQuest(BigDecimal pctBancoFlQuest) {
		this.pctBancoFlQuest = pctBancoFlQuest;
	}
	
}
