/*
 * Nome: br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean;

import br.com.bradesco.web.pgit.service.data.pdc.detalharliquidacaopagamento.request.*;
import br.com.bradesco.web.pgit.service.data.pdc.detalharliquidacaopagamento.response.DetalharLiquidacaoPagamentoResponse;



/**
 * Nome: DetalharLiquidacaoPagamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharLiquidacaoPagamentoSaidaDTO {

	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;
	
	/** Atributo dsFormaLiquidacao. */
	private String dsFormaLiquidacao;
	
	/** Atributo cdSituacao. */
	private String cdSituacao;
	
	/** Atributo dsSituacao. */
	private String dsSituacao;
	
	/** Atributo cdPrioridadeDebito. */
	private Integer cdPrioridadeDebito;
	
	/** Atributo cdControleHoraLimite. */
	private Integer cdControleHoraLimite;
	
	/** Atributo qtMinutosMargemSeguranca. */
	private Integer qtMinutosMargemSeguranca;
	
	/** Atributo hrLimiteProcessamentoPagamento. */
	private String hrLimiteProcessamentoPagamento;
	
	/** Atributo cdCanalInclusao. */
	private Integer cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdAutenticacaoSegregacaoInclusao. */
	private String cdAutenticacaoSegregacaoInclusao;
	
	/** Atributo nmOperacaoFluxoInlcusao. */
	private String nmOperacaoFluxoInlcusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdCanalManutencao. */
	private Integer cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdAutenticacaoSegregacaoManutencao. */
	private String cdAutenticacaoSegregacaoManutencao;
	
	/** Atributo nmOperacaoFluxoManutencao. */
	private String nmOperacaoFluxoManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo hrConsultasSaldoPagamento. */
	private String hrConsultasSaldoPagamento;
	
	/** Atributo hrConsultaFolhaPgto. */
	private String hrConsultaFolhaPgto;
	
	/** Atributo qtMinutosValorSuperior. */
	private Integer qtMinutosValorSuperior;
	
	/** Atributo hrLimiteValorSuperior. */
	private String hrLimiteValorSuperior;

	/** Atributo qtdTempoAgendamentoCobranca. */
    private Integer qtdTempoAgendamentoCobranca = null;

    /** Atributo qtdTempoEfetivacaoCiclicaCobranca. */
    private Integer qtdTempoEfetivacaoCiclicaCobranca = null;

    /** Atributo qtdTempoEfetivacaoDiariaCobranca. */
    private Integer qtdTempoEfetivacaoDiariaCobranca = null;
    
    /** Atributo qtdLimiteProcessamentoExcedido. */
    private Integer qtdLimiteProcessamentoExcedido = null;
   
    /** Atributo qtdLimiteSemResposta. */
    private Integer qtdLimiteSemResposta = null;
    

	/**
	 * Detalhar liquidacao pagamento saida dto.
	 */
	public DetalharLiquidacaoPagamentoSaidaDTO() {
		super();
	}

	/**
	 * Detalhar liquidacao pagamento saida dto.
	 *
	 * @param ocorrencia the ocorrencia
	 */
	public DetalharLiquidacaoPagamentoSaidaDTO(DetalharLiquidacaoPagamentoResponse  ocorrencia) {
		
		this.cdFormaLiquidacao = ocorrencia.getCdFormaLiquidacao();
		this.dsFormaLiquidacao = ocorrencia.getDsFormaLiquidacao();
		this.cdSituacao = ocorrencia.getCdSituacao();
		this.dsSituacao = ocorrencia.getDsSituacao();
		this.cdPrioridadeDebito = ocorrencia.getCdPrioridadeDebito();
		this.cdControleHoraLimite = ocorrencia.getCdControleHoraLimite();
		this.qtMinutosMargemSeguranca = ocorrencia.getQtMinutosMargemSeguranca();
		this.hrLimiteProcessamentoPagamento = ocorrencia.getHrLimiteProcessamentoPagamento();
		this.cdCanalInclusao = ocorrencia.getCdCanalInclusao();
		this.dsCanalInclusao = ocorrencia.getDsCanalInclusao();
		this.cdAutenticacaoSegregacaoInclusao = ocorrencia.getCdAutenticacaoSegregacaoInclusao();
		this.nmOperacaoFluxoInlcusao = ocorrencia.getNmOperacaoFluxoInlcusao();
		this.hrInclusaoRegistro = ocorrencia.getHrInclusaoRegistro();
		this.cdCanalManutencao = ocorrencia.getCdCanalManutencao();
		this.dsCanalManutencao = ocorrencia.getDsCanalManutencao();
		this.cdAutenticacaoSegregacaoManutencao = ocorrencia.getCdAutenticacaoSegregacaoManutencao();
		this.nmOperacaoFluxoManutencao = ocorrencia.getNmOperacaoFluxoManutencao();
		this.hrManutencaoRegistro = ocorrencia.getHrManutencaoRegistro();
		this.qtdLimiteSemResposta = ocorrencia.getQtdLimiteSemResposta();
	}

	/**
	 * Get: canalInclusao.
	 *
	 * @return canalInclusao
	 */
	public String getCanalInclusao() {
		return String.valueOf(cdCanalInclusao).equals("0") ? "" : cdCanalInclusao + "-" + dsCanalInclusao;
	}

	/**
	 * Get: canalManutencao.
	 *
	 * @return canalManutencao
	 */
	public String getCanalManutencao() {
		return String.valueOf(cdCanalManutencao).equals("0") ? "" : cdCanalManutencao + "-" + dsCanalManutencao;
	}

	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return cdSituacao + " - " + dsSituacao;
	}

	/**
	 * Get: cdAutenticacaoSegregacaoInclusao.
	 *
	 * @return cdAutenticacaoSegregacaoInclusao
	 */
	public String getCdAutenticacaoSegregacaoInclusao() {
		return cdAutenticacaoSegregacaoInclusao;
	}

	/**
	 * Set: cdAutenticacaoSegregacaoInclusao.
	 *
	 * @param cdAutenticacaoSegregacaoInclusao the cd autenticacao segregacao inclusao
	 */
	public void setCdAutenticacaoSegregacaoInclusao(String cdAutenticacaoSegregacaoInclusao) {
		this.cdAutenticacaoSegregacaoInclusao = cdAutenticacaoSegregacaoInclusao;
	}

	/**
	 * Get: cdAutenticacaoSegregacaoManutencao.
	 *
	 * @return cdAutenticacaoSegregacaoManutencao
	 */
	public String getCdAutenticacaoSegregacaoManutencao() {
		return cdAutenticacaoSegregacaoManutencao;
	}

	/**
	 * Set: cdAutenticacaoSegregacaoManutencao.
	 *
	 * @param cdAutenticacaoSegregacaoManutencao the cd autenticacao segregacao manutencao
	 */
	public void setCdAutenticacaoSegregacaoManutencao(String cdAutenticacaoSegregacaoManutencao) {
		this.cdAutenticacaoSegregacaoManutencao = cdAutenticacaoSegregacaoManutencao;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: cdControleHoraLimite.
	 *
	 * @return cdControleHoraLimite
	 */
	public Integer getCdControleHoraLimite() {
		return cdControleHoraLimite;
	}

	/**
	 * Set: cdControleHoraLimite.
	 *
	 * @param cdControleHoraLimite the cd controle hora limite
	 */
	public void setCdControleHoraLimite(Integer cdControleHoraLimite) {
		this.cdControleHoraLimite = cdControleHoraLimite;
	}

	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: cdPrioridadeDebito.
	 *
	 * @return cdPrioridadeDebito
	 */
	public Integer getCdPrioridadeDebito() {
		return cdPrioridadeDebito;
	}

	/**
	 * Set: cdPrioridadeDebito.
	 *
	 * @param cdPrioridadeDebito the cd prioridade debito
	 */
	public void setCdPrioridadeDebito(Integer cdPrioridadeDebito) {
		this.cdPrioridadeDebito = cdPrioridadeDebito;
	}

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public String getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(String cdSituacao) {
		this.cdSituacao = cdSituacao;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
		return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}

	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}

	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrLimiteProcessamentoPagamento.
	 *
	 * @return hrLimiteProcessamentoPagamento
	 */
	public String getHrLimiteProcessamentoPagamento() {
		return hrLimiteProcessamentoPagamento;
	}

	/**
	 * Set: hrLimiteProcessamentoPagamento.
	 *
	 * @param hrLimiteProcessamentoPagamento the hr limite processamento pagamento
	 */
	public void setHrLimiteProcessamentoPagamento(String hrLimiteProcessamentoPagamento) {
		this.hrLimiteProcessamentoPagamento = hrLimiteProcessamentoPagamento;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: nmOperacaoFluxoInlcusao.
	 *
	 * @return nmOperacaoFluxoInlcusao
	 */
	public String getNmOperacaoFluxoInlcusao() {
		return nmOperacaoFluxoInlcusao.trim().equals("0") ? "" : nmOperacaoFluxoInlcusao;
	}

	/**
	 * Set: nmOperacaoFluxoInlcusao.
	 *
	 * @param nmOperacaoFluxoInlcusao the nm operacao fluxo inlcusao
	 */
	public void setNmOperacaoFluxoInlcusao(String nmOperacaoFluxoInlcusao) {
		this.nmOperacaoFluxoInlcusao = nmOperacaoFluxoInlcusao;
	}

	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao.trim().equals("0") ? "" : nmOperacaoFluxoManutencao;
	}

	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}

	/**
	 * Get: qtMinutosMargemSeguranca.
	 *
	 * @return qtMinutosMargemSeguranca
	 */
	public Integer getQtMinutosMargemSeguranca() {
		return qtMinutosMargemSeguranca;
	}

	/**
	 * Set: qtMinutosMargemSeguranca.
	 *
	 * @param qtMinutosMargemSeguranca the qt minutos margem seguranca
	 */
	public void setQtMinutosMargemSeguranca(Integer qtMinutosMargemSeguranca) {
		this.qtMinutosMargemSeguranca = qtMinutosMargemSeguranca;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: hrConsultasSaldoPagamento.
	 *
	 * @return hrConsultasSaldoPagamento
	 */
	public String getHrConsultasSaldoPagamento() {
		return hrConsultasSaldoPagamento;
	}

	/**
	 * Set: hrConsultasSaldoPagamento.
	 *
	 * @param hrConsultasSaldoPagamento the hr consultas saldo pagamento
	 */
	public void setHrConsultasSaldoPagamento(String hrConsultasSaldoPagamento) {
		this.hrConsultasSaldoPagamento = hrConsultasSaldoPagamento;
	}

	/**
	 * Get: hrConsultaFolhaPgto.
	 *
	 * @return hrConsultaFolhaPgto
	 */
	public String getHrConsultaFolhaPgto() {
		return hrConsultaFolhaPgto;
	}

	/**
	 * Set: hrConsultaFolhaPgto.
	 *
	 * @param hrConsultaFolhaPgto the hr consulta folha pgto
	 */
	public void setHrConsultaFolhaPgto(String hrConsultaFolhaPgto) {
		this.hrConsultaFolhaPgto = hrConsultaFolhaPgto;
	}

	/**
	 * Get: qtMinutosValorSuperior.
	 *
	 * @return qtMinutosValorSuperior
	 */
	public Integer getQtMinutosValorSuperior() {
		return qtMinutosValorSuperior;
	}

	/**
	 * Set: qtMinutosValorSuperior.
	 *
	 * @param qtMinutosValorSuperior the qt minutos valor superior
	 */
	public void setQtMinutosValorSuperior(Integer qtMinutosValorSuperior) {
		this.qtMinutosValorSuperior = qtMinutosValorSuperior;
	}

	/**
	 * Get: hrLimiteValorSuperior.
	 *
	 * @return hrLimiteValorSuperior
	 */
	public String getHrLimiteValorSuperior() {
		return hrLimiteValorSuperior;
	}

	/**
	 * Set: hrLimiteValorSuperior.
	 *
	 * @param hrLimiteValorSuperior the hr limite valor superior
	 */
	public void setHrLimiteValorSuperior(String hrLimiteValorSuperior) {
		this.hrLimiteValorSuperior = hrLimiteValorSuperior;
	}

    /**
     * Nome: getQtdTempoAgendamentoCobranca
     *
     * @return qtdTempoAgendamentoCobranca
     */
    public Integer getQtdTempoAgendamentoCobranca() {
        return qtdTempoAgendamentoCobranca;
    }

    /**
     * Nome: setQtdTempoAgendamentoCobranca
     *
     * @param qtdTempoAgendamentoCobranca
     */
    public void setQtdTempoAgendamentoCobranca(Integer qtdTempoAgendamentoCobranca) {
        this.qtdTempoAgendamentoCobranca = qtdTempoAgendamentoCobranca;
    }

    /**
     * Nome: getQtdTempoEfetivacaoCiclicaCobranca
     *
     * @return qtdTempoEfetivacaoCiclicaCobranca
     */
    public Integer getQtdTempoEfetivacaoCiclicaCobranca() {
        return qtdTempoEfetivacaoCiclicaCobranca;
    }

    /**
     * Nome: setQtdTempoEfetivacaoCiclicaCobranca
     *
     * @param qtdTempoEfetivacaoCiclicaCobranca
     */
    public void setQtdTempoEfetivacaoCiclicaCobranca(Integer qtdTempoEfetivacaoCiclicaCobranca) {
        this.qtdTempoEfetivacaoCiclicaCobranca = qtdTempoEfetivacaoCiclicaCobranca;
    }

    /**
     * Nome: getQtdTempoEfetivacaoDiariaCobranca
     *
     * @return qtdTempoEfetivacaoDiariaCobranca
     */
    public Integer getQtdTempoEfetivacaoDiariaCobranca() {
        return qtdTempoEfetivacaoDiariaCobranca;
    }

    /**
     * Nome: setQtdTempoEfetivacaoDiariaCobranca
     *
     * @param qtdTempoEfetivacaoDiariaCobranca
     */
    public void setQtdTempoEfetivacaoDiariaCobranca(Integer qtdTempoEfetivacaoDiariaCobranca) {
        this.qtdTempoEfetivacaoDiariaCobranca = qtdTempoEfetivacaoDiariaCobranca;
    }

	public Integer getQtdLimiteSemResposta() {
		return qtdLimiteSemResposta;
	}

	public void setQtdLimiteSemResposta(Integer qtdLimiteSemResposta) {
		this.qtdLimiteSemResposta = qtdLimiteSemResposta;
	}

	public Integer getQtdLimiteProcessamentoExcedido() {
		return qtdLimiteProcessamentoExcedido;
	}

	public void setQtdLimiteProcessamentoExcedido(
			Integer qtdLimiteProcessamentoExcedido) {
		this.qtdLimiteProcessamentoExcedido = qtdLimiteProcessamentoExcedido;
	}

    
}
