/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ImprimirAnexoPrimeiroContratoLoopResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ImprimirAnexoPrimeiroContratoLoopResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _dtInclusaoContrato
     */
    private java.lang.String _dtInclusaoContrato;

    /**
     * Field _dtInicioVigencia
     */
    private java.lang.String _dtInicioVigencia;

    /**
     * Field _dtFinalVigencia
     */
    private java.lang.String _dtFinalVigencia;

    /**
     * Field _cdCpfCnpjRepresentante
     */
    private java.lang.String _cdCpfCnpjRepresentante;

    /**
     * Field _dsNomeRepresentante
     */
    private java.lang.String _dsNomeRepresentante;

    /**
     * Field _cdGrupoEconomico
     */
    private long _cdGrupoEconomico = 0;

    /**
     * keeps track of state for field: _cdGrupoEconomico
     */
    private boolean _has_cdGrupoEconomico;

    /**
     * Field _dsGrupoEconomico
     */
    private java.lang.String _dsGrupoEconomico;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;

    /**
     * Field _dsAtividadeEconomica
     */
    private java.lang.String _dsAtividadeEconomica;

    /**
     * Field _cdSegmentoEconomico
     */
    private int _cdSegmentoEconomico = 0;

    /**
     * keeps track of state for field: _cdSegmentoEconomico
     */
    private boolean _has_cdSegmentoEconomico;

    /**
     * Field _dsSegmentoEconomico
     */
    private java.lang.String _dsSegmentoEconomico;

    /**
     * Field _cdSubSegmento
     */
    private int _cdSubSegmento = 0;

    /**
     * keeps track of state for field: _cdSubSegmento
     */
    private boolean _has_cdSubSegmento;

    /**
     * Field _dsSubSegmento
     */
    private java.lang.String _dsSubSegmento;

    /**
     * Field _dsContrato
     */
    private java.lang.String _dsContrato;

    /**
     * Field _cdAgenciaGestoraContrato
     */
    private int _cdAgenciaGestoraContrato = 0;

    /**
     * keeps track of state for field: _cdAgenciaGestoraContrato
     */
    private boolean _has_cdAgenciaGestoraContrato;

    /**
     * Field _dsAgenciaGestoraContrato
     */
    private java.lang.String _dsAgenciaGestoraContrato;

    /**
     * Field _qtdeParticipante
     */
    private int _qtdeParticipante = 0;

    /**
     * keeps track of state for field: _qtdeParticipante
     */
    private boolean _has_qtdeParticipante;

    /**
     * Field _ocorrencias1List
     */
    private java.util.Vector _ocorrencias1List;

    /**
     * Field _qtdeConta
     */
    private int _qtdeConta = 0;

    /**
     * keeps track of state for field: _qtdeConta
     */
    private boolean _has_qtdeConta;

    /**
     * Field _ocorrencias2List
     */
    private java.util.Vector _ocorrencias2List;

    /**
     * Field _qtLayout
     */
    private int _qtLayout = 0;

    /**
     * keeps track of state for field: _qtLayout
     */
    private boolean _has_qtLayout;

    /**
     * Field _ocorrencias3List
     */
    private java.util.Vector _ocorrencias3List;


      //----------------/
     //- Constructors -/
    //----------------/

    public ImprimirAnexoPrimeiroContratoLoopResponse() 
     {
        super();
        _ocorrencias1List = new Vector();
        _ocorrencias2List = new Vector();
        _ocorrencias3List = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.ImprimirAnexoPrimeiroContratoLoopResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param vOcorrencias1
     */
    public void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias1List.addElement(vOcorrencias1);
    } //-- void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1) 

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void addOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias1List.insertElementAt(vOcorrencias1, index);
    } //-- void addOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param vOcorrencias2
     */
    public void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias2List.addElement(vOcorrencias2);
    } //-- void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void addOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias2List.insertElementAt(vOcorrencias2, index);
    } //-- void addOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2) 

    /**
     * Method addOcorrencias3
     * 
     * 
     * 
     * @param vOcorrencias3
     */
    public void addOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias3List.addElement(vOcorrencias3);
    } //-- void addOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3) 

    /**
     * Method addOcorrencias3
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias3
     */
    public void addOcorrencias3(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias3List.insertElementAt(vOcorrencias3, index);
    } //-- void addOcorrencias3(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3) 

    /**
     * Method deleteCdAgenciaGestoraContrato
     * 
     */
    public void deleteCdAgenciaGestoraContrato()
    {
        this._has_cdAgenciaGestoraContrato= false;
    } //-- void deleteCdAgenciaGestoraContrato() 

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdGrupoEconomico
     * 
     */
    public void deleteCdGrupoEconomico()
    {
        this._has_cdGrupoEconomico= false;
    } //-- void deleteCdGrupoEconomico() 

    /**
     * Method deleteCdSegmentoEconomico
     * 
     */
    public void deleteCdSegmentoEconomico()
    {
        this._has_cdSegmentoEconomico= false;
    } //-- void deleteCdSegmentoEconomico() 

    /**
     * Method deleteCdSubSegmento
     * 
     */
    public void deleteCdSubSegmento()
    {
        this._has_cdSubSegmento= false;
    } //-- void deleteCdSubSegmento() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Method deleteQtLayout
     * 
     */
    public void deleteQtLayout()
    {
        this._has_qtLayout= false;
    } //-- void deleteQtLayout() 

    /**
     * Method deleteQtdeConta
     * 
     */
    public void deleteQtdeConta()
    {
        this._has_qtdeConta= false;
    } //-- void deleteQtdeConta() 

    /**
     * Method deleteQtdeParticipante
     * 
     */
    public void deleteQtdeParticipante()
    {
        this._has_qtdeParticipante= false;
    } //-- void deleteQtdeParticipante() 

    /**
     * Method enumerateOcorrencias1
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias1()
    {
        return _ocorrencias1List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias1() 

    /**
     * Method enumerateOcorrencias2
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias2()
    {
        return _ocorrencias2List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias2() 

    /**
     * Method enumerateOcorrencias3
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias3()
    {
        return _ocorrencias3List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias3() 

    /**
     * Returns the value of field 'cdAgenciaGestoraContrato'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaGestoraContrato'.
     */
    public int getCdAgenciaGestoraContrato()
    {
        return this._cdAgenciaGestoraContrato;
    } //-- int getCdAgenciaGestoraContrato() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpjRepresentante'.
     */
    public java.lang.String getCdCpfCnpjRepresentante()
    {
        return this._cdCpfCnpjRepresentante;
    } //-- java.lang.String getCdCpfCnpjRepresentante() 

    /**
     * Returns the value of field 'cdGrupoEconomico'.
     * 
     * @return long
     * @return the value of field 'cdGrupoEconomico'.
     */
    public long getCdGrupoEconomico()
    {
        return this._cdGrupoEconomico;
    } //-- long getCdGrupoEconomico() 

    /**
     * Returns the value of field 'cdSegmentoEconomico'.
     * 
     * @return int
     * @return the value of field 'cdSegmentoEconomico'.
     */
    public int getCdSegmentoEconomico()
    {
        return this._cdSegmentoEconomico;
    } //-- int getCdSegmentoEconomico() 

    /**
     * Returns the value of field 'cdSubSegmento'.
     * 
     * @return int
     * @return the value of field 'cdSubSegmento'.
     */
    public int getCdSubSegmento()
    {
        return this._cdSubSegmento;
    } //-- int getCdSubSegmento() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgenciaGestoraContrato'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaGestoraContrato'.
     */
    public java.lang.String getDsAgenciaGestoraContrato()
    {
        return this._dsAgenciaGestoraContrato;
    } //-- java.lang.String getDsAgenciaGestoraContrato() 

    /**
     * Returns the value of field 'dsAtividadeEconomica'.
     * 
     * @return String
     * @return the value of field 'dsAtividadeEconomica'.
     */
    public java.lang.String getDsAtividadeEconomica()
    {
        return this._dsAtividadeEconomica;
    } //-- java.lang.String getDsAtividadeEconomica() 

    /**
     * Returns the value of field 'dsContrato'.
     * 
     * @return String
     * @return the value of field 'dsContrato'.
     */
    public java.lang.String getDsContrato()
    {
        return this._dsContrato;
    } //-- java.lang.String getDsContrato() 

    /**
     * Returns the value of field 'dsGrupoEconomico'.
     * 
     * @return String
     * @return the value of field 'dsGrupoEconomico'.
     */
    public java.lang.String getDsGrupoEconomico()
    {
        return this._dsGrupoEconomico;
    } //-- java.lang.String getDsGrupoEconomico() 

    /**
     * Returns the value of field 'dsNomeRepresentante'.
     * 
     * @return String
     * @return the value of field 'dsNomeRepresentante'.
     */
    public java.lang.String getDsNomeRepresentante()
    {
        return this._dsNomeRepresentante;
    } //-- java.lang.String getDsNomeRepresentante() 

    /**
     * Returns the value of field 'dsSegmentoEconomico'.
     * 
     * @return String
     * @return the value of field 'dsSegmentoEconomico'.
     */
    public java.lang.String getDsSegmentoEconomico()
    {
        return this._dsSegmentoEconomico;
    } //-- java.lang.String getDsSegmentoEconomico() 

    /**
     * Returns the value of field 'dsSubSegmento'.
     * 
     * @return String
     * @return the value of field 'dsSubSegmento'.
     */
    public java.lang.String getDsSubSegmento()
    {
        return this._dsSubSegmento;
    } //-- java.lang.String getDsSubSegmento() 

    /**
     * Returns the value of field 'dtFinalVigencia'.
     * 
     * @return String
     * @return the value of field 'dtFinalVigencia'.
     */
    public java.lang.String getDtFinalVigencia()
    {
        return this._dtFinalVigencia;
    } //-- java.lang.String getDtFinalVigencia() 

    /**
     * Returns the value of field 'dtInclusaoContrato'.
     * 
     * @return String
     * @return the value of field 'dtInclusaoContrato'.
     */
    public java.lang.String getDtInclusaoContrato()
    {
        return this._dtInclusaoContrato;
    } //-- java.lang.String getDtInclusaoContrato() 

    /**
     * Returns the value of field 'dtInicioVigencia'.
     * 
     * @return String
     * @return the value of field 'dtInicioVigencia'.
     */
    public java.lang.String getDtInicioVigencia()
    {
        return this._dtInicioVigencia;
    } //-- java.lang.String getDtInicioVigencia() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1 getOcorrencias1(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias1: Index value '"+index+"' not in range [0.."+(_ocorrencias1List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1) _ocorrencias1List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1 getOcorrencias1(int) 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1[] getOcorrencias1()
    {
        int size = _ocorrencias1List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1) _ocorrencias1List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1[] getOcorrencias1() 

    /**
     * Method getOcorrencias1Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias1Count()
    {
        return _ocorrencias1List.size();
    } //-- int getOcorrencias1Count() 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2 getOcorrencias2(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias2: Index value '"+index+"' not in range [0.."+(_ocorrencias2List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2 getOcorrencias2(int) 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2[] getOcorrencias2()
    {
        int size = _ocorrencias2List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2[] getOcorrencias2() 

    /**
     * Method getOcorrencias2Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias2Count()
    {
        return _ocorrencias2List.size();
    } //-- int getOcorrencias2Count() 

    /**
     * Method getOcorrencias3
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3 getOcorrencias3(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias3List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias3: Index value '"+index+"' not in range [0.."+(_ocorrencias3List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3) _ocorrencias3List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3 getOcorrencias3(int) 

    /**
     * Method getOcorrencias3
     * 
     * 
     * 
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3[] getOcorrencias3()
    {
        int size = _ocorrencias3List.size();
        br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3) _ocorrencias3List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3[] getOcorrencias3() 

    /**
     * Method getOcorrencias3Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias3Count()
    {
        return _ocorrencias3List.size();
    } //-- int getOcorrencias3Count() 

    /**
     * Returns the value of field 'qtLayout'.
     * 
     * @return int
     * @return the value of field 'qtLayout'.
     */
    public int getQtLayout()
    {
        return this._qtLayout;
    } //-- int getQtLayout() 

    /**
     * Returns the value of field 'qtdeConta'.
     * 
     * @return int
     * @return the value of field 'qtdeConta'.
     */
    public int getQtdeConta()
    {
        return this._qtdeConta;
    } //-- int getQtdeConta() 

    /**
     * Returns the value of field 'qtdeParticipante'.
     * 
     * @return int
     * @return the value of field 'qtdeParticipante'.
     */
    public int getQtdeParticipante()
    {
        return this._qtdeParticipante;
    } //-- int getQtdeParticipante() 

    /**
     * Method hasCdAgenciaGestoraContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaGestoraContrato()
    {
        return this._has_cdAgenciaGestoraContrato;
    } //-- boolean hasCdAgenciaGestoraContrato() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdGrupoEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoEconomico()
    {
        return this._has_cdGrupoEconomico;
    } //-- boolean hasCdGrupoEconomico() 

    /**
     * Method hasCdSegmentoEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmentoEconomico()
    {
        return this._has_cdSegmentoEconomico;
    } //-- boolean hasCdSegmentoEconomico() 

    /**
     * Method hasCdSubSegmento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubSegmento()
    {
        return this._has_cdSubSegmento;
    } //-- boolean hasCdSubSegmento() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method hasQtLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtLayout()
    {
        return this._has_qtLayout;
    } //-- boolean hasQtLayout() 

    /**
     * Method hasQtdeConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeConta()
    {
        return this._has_qtdeConta;
    } //-- boolean hasQtdeConta() 

    /**
     * Method hasQtdeParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeParticipante()
    {
        return this._has_qtdeParticipante;
    } //-- boolean hasQtdeParticipante() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias1
     * 
     */
    public void removeAllOcorrencias1()
    {
        _ocorrencias1List.removeAllElements();
    } //-- void removeAllOcorrencias1() 

    /**
     * Method removeAllOcorrencias2
     * 
     */
    public void removeAllOcorrencias2()
    {
        _ocorrencias2List.removeAllElements();
    } //-- void removeAllOcorrencias2() 

    /**
     * Method removeAllOcorrencias3
     * 
     */
    public void removeAllOcorrencias3()
    {
        _ocorrencias3List.removeAllElements();
    } //-- void removeAllOcorrencias3() 

    /**
     * Method removeOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1 removeOcorrencias1(int index)
    {
        java.lang.Object obj = _ocorrencias1List.elementAt(index);
        _ocorrencias1List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1 removeOcorrencias1(int) 

    /**
     * Method removeOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2 removeOcorrencias2(int index)
    {
        java.lang.Object obj = _ocorrencias2List.elementAt(index);
        _ocorrencias2List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2 removeOcorrencias2(int) 

    /**
     * Method removeOcorrencias3
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias3
     */
    public br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3 removeOcorrencias3(int index)
    {
        java.lang.Object obj = _ocorrencias3List.elementAt(index);
        _ocorrencias3List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3 removeOcorrencias3(int) 

    /**
     * Sets the value of field 'cdAgenciaGestoraContrato'.
     * 
     * @param cdAgenciaGestoraContrato the value of field
     * 'cdAgenciaGestoraContrato'.
     */
    public void setCdAgenciaGestoraContrato(int cdAgenciaGestoraContrato)
    {
        this._cdAgenciaGestoraContrato = cdAgenciaGestoraContrato;
        this._has_cdAgenciaGestoraContrato = true;
    } //-- void setCdAgenciaGestoraContrato(int) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdCpfCnpjRepresentante'.
     * 
     * @param cdCpfCnpjRepresentante the value of field
     * 'cdCpfCnpjRepresentante'.
     */
    public void setCdCpfCnpjRepresentante(java.lang.String cdCpfCnpjRepresentante)
    {
        this._cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
    } //-- void setCdCpfCnpjRepresentante(java.lang.String) 

    /**
     * Sets the value of field 'cdGrupoEconomico'.
     * 
     * @param cdGrupoEconomico the value of field 'cdGrupoEconomico'
     */
    public void setCdGrupoEconomico(long cdGrupoEconomico)
    {
        this._cdGrupoEconomico = cdGrupoEconomico;
        this._has_cdGrupoEconomico = true;
    } //-- void setCdGrupoEconomico(long) 

    /**
     * Sets the value of field 'cdSegmentoEconomico'.
     * 
     * @param cdSegmentoEconomico the value of field
     * 'cdSegmentoEconomico'.
     */
    public void setCdSegmentoEconomico(int cdSegmentoEconomico)
    {
        this._cdSegmentoEconomico = cdSegmentoEconomico;
        this._has_cdSegmentoEconomico = true;
    } //-- void setCdSegmentoEconomico(int) 

    /**
     * Sets the value of field 'cdSubSegmento'.
     * 
     * @param cdSubSegmento the value of field 'cdSubSegmento'.
     */
    public void setCdSubSegmento(int cdSubSegmento)
    {
        this._cdSubSegmento = cdSubSegmento;
        this._has_cdSubSegmento = true;
    } //-- void setCdSubSegmento(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaGestoraContrato'.
     * 
     * @param dsAgenciaGestoraContrato the value of field
     * 'dsAgenciaGestoraContrato'.
     */
    public void setDsAgenciaGestoraContrato(java.lang.String dsAgenciaGestoraContrato)
    {
        this._dsAgenciaGestoraContrato = dsAgenciaGestoraContrato;
    } //-- void setDsAgenciaGestoraContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsAtividadeEconomica'.
     * 
     * @param dsAtividadeEconomica the value of field
     * 'dsAtividadeEconomica'.
     */
    public void setDsAtividadeEconomica(java.lang.String dsAtividadeEconomica)
    {
        this._dsAtividadeEconomica = dsAtividadeEconomica;
    } //-- void setDsAtividadeEconomica(java.lang.String) 

    /**
     * Sets the value of field 'dsContrato'.
     * 
     * @param dsContrato the value of field 'dsContrato'.
     */
    public void setDsContrato(java.lang.String dsContrato)
    {
        this._dsContrato = dsContrato;
    } //-- void setDsContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsGrupoEconomico'.
     * 
     * @param dsGrupoEconomico the value of field 'dsGrupoEconomico'
     */
    public void setDsGrupoEconomico(java.lang.String dsGrupoEconomico)
    {
        this._dsGrupoEconomico = dsGrupoEconomico;
    } //-- void setDsGrupoEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeRepresentante'.
     * 
     * @param dsNomeRepresentante the value of field
     * 'dsNomeRepresentante'.
     */
    public void setDsNomeRepresentante(java.lang.String dsNomeRepresentante)
    {
        this._dsNomeRepresentante = dsNomeRepresentante;
    } //-- void setDsNomeRepresentante(java.lang.String) 

    /**
     * Sets the value of field 'dsSegmentoEconomico'.
     * 
     * @param dsSegmentoEconomico the value of field
     * 'dsSegmentoEconomico'.
     */
    public void setDsSegmentoEconomico(java.lang.String dsSegmentoEconomico)
    {
        this._dsSegmentoEconomico = dsSegmentoEconomico;
    } //-- void setDsSegmentoEconomico(java.lang.String) 

    /**
     * Sets the value of field 'dsSubSegmento'.
     * 
     * @param dsSubSegmento the value of field 'dsSubSegmento'.
     */
    public void setDsSubSegmento(java.lang.String dsSubSegmento)
    {
        this._dsSubSegmento = dsSubSegmento;
    } //-- void setDsSubSegmento(java.lang.String) 

    /**
     * Sets the value of field 'dtFinalVigencia'.
     * 
     * @param dtFinalVigencia the value of field 'dtFinalVigencia'.
     */
    public void setDtFinalVigencia(java.lang.String dtFinalVigencia)
    {
        this._dtFinalVigencia = dtFinalVigencia;
    } //-- void setDtFinalVigencia(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusaoContrato'.
     * 
     * @param dtInclusaoContrato the value of field
     * 'dtInclusaoContrato'.
     */
    public void setDtInclusaoContrato(java.lang.String dtInclusaoContrato)
    {
        this._dtInclusaoContrato = dtInclusaoContrato;
    } //-- void setDtInclusaoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioVigencia'.
     * 
     * @param dtInicioVigencia the value of field 'dtInicioVigencia'
     */
    public void setDtInicioVigencia(java.lang.String dtInicioVigencia)
    {
        this._dtInicioVigencia = dtInicioVigencia;
    } //-- void setDtInicioVigencia(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void setOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias1: Index value '"+index+"' not in range [0.." + (_ocorrencias1List.size() - 1) + "]");
        }
        _ocorrencias1List.setElementAt(vOcorrencias1, index);
    } //-- void setOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param ocorrencias1Array
     */
    public void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1[] ocorrencias1Array)
    {
        //-- copy array
        _ocorrencias1List.removeAllElements();
        for (int i = 0; i < ocorrencias1Array.length; i++) {
            _ocorrencias1List.addElement(ocorrencias1Array[i]);
        }
    } //-- void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias1) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void setOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias2: Index value '"+index+"' not in range [0.." + (_ocorrencias2List.size() - 1) + "]");
        }
        _ocorrencias2List.setElementAt(vOcorrencias2, index);
    } //-- void setOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param ocorrencias2Array
     */
    public void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2[] ocorrencias2Array)
    {
        //-- copy array
        _ocorrencias2List.removeAllElements();
        for (int i = 0; i < ocorrencias2Array.length; i++) {
            _ocorrencias2List.addElement(ocorrencias2Array[i]);
        }
    } //-- void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias2) 

    /**
     * Method setOcorrencias3
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias3
     */
    public void setOcorrencias3(int index, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3 vOcorrencias3)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias3List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias3: Index value '"+index+"' not in range [0.." + (_ocorrencias3List.size() - 1) + "]");
        }
        _ocorrencias3List.setElementAt(vOcorrencias3, index);
    } //-- void setOcorrencias3(int, br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3) 

    /**
     * Method setOcorrencias3
     * 
     * 
     * 
     * @param ocorrencias3Array
     */
    public void setOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3[] ocorrencias3Array)
    {
        //-- copy array
        _ocorrencias3List.removeAllElements();
        for (int i = 0; i < ocorrencias3Array.length; i++) {
            _ocorrencias3List.addElement(ocorrencias3Array[i]);
        }
    } //-- void setOcorrencias3(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.Ocorrencias3) 

    /**
     * Sets the value of field 'qtLayout'.
     * 
     * @param qtLayout the value of field 'qtLayout'.
     */
    public void setQtLayout(int qtLayout)
    {
        this._qtLayout = qtLayout;
        this._has_qtLayout = true;
    } //-- void setQtLayout(int) 

    /**
     * Sets the value of field 'qtdeConta'.
     * 
     * @param qtdeConta the value of field 'qtdeConta'.
     */
    public void setQtdeConta(int qtdeConta)
    {
        this._qtdeConta = qtdeConta;
        this._has_qtdeConta = true;
    } //-- void setQtdeConta(int) 

    /**
     * Sets the value of field 'qtdeParticipante'.
     * 
     * @param qtdeParticipante the value of field 'qtdeParticipante'
     */
    public void setQtdeParticipante(int qtdeParticipante)
    {
        this._qtdeParticipante = qtdeParticipante;
        this._has_qtdeParticipante = true;
    } //-- void setQtdeParticipante(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ImprimirAnexoPrimeiroContratoLoopResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.ImprimirAnexoPrimeiroContratoLoopResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.ImprimirAnexoPrimeiroContratoLoopResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.ImprimirAnexoPrimeiroContratoLoopResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexoprimeirocontratoloop.response.ImprimirAnexoPrimeiroContratoLoopResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
