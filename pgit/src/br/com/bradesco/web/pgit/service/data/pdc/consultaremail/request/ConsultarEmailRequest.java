/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultaremail.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarEmailRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarEmailRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdTipoPessoa
     */
    private java.lang.String _cdTipoPessoa;

    /**
     * Field _cdClub
     */
    private long _cdClub = 0;

    /**
     * keeps track of state for field: _cdClub
     */
    private boolean _has_cdClub;

    /**
     * Field _cdEmpresaContrato
     */
    private long _cdEmpresaContrato = 0;

    /**
     * keeps track of state for field: _cdEmpresaContrato
     */
    private boolean _has_cdEmpresaContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarEmailRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaremail.request.ConsultarEmailRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClub
     * 
     */
    public void deleteCdClub()
    {
        this._has_cdClub= false;
    } //-- void deleteCdClub() 

    /**
     * Method deleteCdEmpresaContrato
     * 
     */
    public void deleteCdEmpresaContrato()
    {
        this._has_cdEmpresaContrato= false;
    } //-- void deleteCdEmpresaContrato() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Returns the value of field 'cdClub'.
     * 
     * @return long
     * @return the value of field 'cdClub'.
     */
    public long getCdClub()
    {
        return this._cdClub;
    } //-- long getCdClub() 

    /**
     * Returns the value of field 'cdEmpresaContrato'.
     * 
     * @return long
     * @return the value of field 'cdEmpresaContrato'.
     */
    public long getCdEmpresaContrato()
    {
        return this._cdEmpresaContrato;
    } //-- long getCdEmpresaContrato() 

    /**
     * Returns the value of field 'cdTipoPessoa'.
     * 
     * @return String
     * @return the value of field 'cdTipoPessoa'.
     */
    public java.lang.String getCdTipoPessoa()
    {
        return this._cdTipoPessoa;
    } //-- java.lang.String getCdTipoPessoa() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Method hasCdClub
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClub()
    {
        return this._has_cdClub;
    } //-- boolean hasCdClub() 

    /**
     * Method hasCdEmpresaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresaContrato()
    {
        return this._has_cdEmpresaContrato;
    } //-- boolean hasCdEmpresaContrato() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClub'.
     * 
     * @param cdClub the value of field 'cdClub'.
     */
    public void setCdClub(long cdClub)
    {
        this._cdClub = cdClub;
        this._has_cdClub = true;
    } //-- void setCdClub(long) 

    /**
     * Sets the value of field 'cdEmpresaContrato'.
     * 
     * @param cdEmpresaContrato the value of field
     * 'cdEmpresaContrato'.
     */
    public void setCdEmpresaContrato(long cdEmpresaContrato)
    {
        this._cdEmpresaContrato = cdEmpresaContrato;
        this._has_cdEmpresaContrato = true;
    } //-- void setCdEmpresaContrato(long) 

    /**
     * Sets the value of field 'cdTipoPessoa'.
     * 
     * @param cdTipoPessoa the value of field 'cdTipoPessoa'.
     */
    public void setCdTipoPessoa(java.lang.String cdTipoPessoa)
    {
        this._cdTipoPessoa = cdTipoPessoa;
    } //-- void setCdTipoPessoa(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarEmailRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultaremail.request.ConsultarEmailRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultaremail.request.ConsultarEmailRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultaremail.request.ConsultarEmailRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaremail.request.ConsultarEmailRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
