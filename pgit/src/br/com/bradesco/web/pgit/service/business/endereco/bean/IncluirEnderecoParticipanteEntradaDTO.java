/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;


/**
 * Nome: IncluirEnderecoParticipanteEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirEnderecoParticipanteEntradaDTO {

    /** Atributo cdpessoaJuridicaContrato. */
    private Long cdpessoaJuridicaContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;

    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;

    /** Atributo cdTipoServico. */
    private Integer cdTipoServico;

    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;

    /** Atributo cdOperacaoProdutoServico. */
    private Integer cdOperacaoProdutoServico;

    /** Atributo cdTipoParticipacaoPessoa. */
    private Integer cdTipoParticipacaoPessoa;

    /** Atributo cdPessoa. */
    private Long cdPessoa;

    /** Atributo cdPessoaEndereco. */
    private Long cdPessoaEndereco;

    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;

    /** Atributo nrSequenciaEndereco. */
    private Integer nrSequenciaEndereco;

    /** Atributo cdUsoPostal. */
    private Integer cdUsoPostal;

    /** Atributo cdUsoEnderecoPessoa. */
    private Integer cdUsoEnderecoPessoa;

    /** Atributo rdoEnderecoPessoa. */
    private String rdoEnderecoPessoa;

    /** Atributo cdEspeceEndereco. */
    private Integer cdEspeceEndereco;

    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;

    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;

    /**
     * Set: cdpessoaJuridicaContrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Get: cdpessoaJuridicaContrato.
     *
     * @return cdpessoaJuridicaContrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    /**
     * Set: cdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Get: cdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
        return this.cdProdutoServicoOperacao;
    }

    /**
     * Set: cdProdutoOperacaoRelacionado.
     *
     * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
     */
    public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
        this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
    }

    /**
     * Get: cdProdutoOperacaoRelacionado.
     *
     * @return cdProdutoOperacaoRelacionado
     */
    public Integer getCdProdutoOperacaoRelacionado() {
        return this.cdProdutoOperacaoRelacionado;
    }

    /**
     * Set: cdOperacaoProdutoServico.
     *
     * @param cdOperacaoProdutoServico the cd operacao produto servico
     */
    public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
        this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
    }

    /**
     * Get: cdOperacaoProdutoServico.
     *
     * @return cdOperacaoProdutoServico
     */
    public Integer getCdOperacaoProdutoServico() {
        return this.cdOperacaoProdutoServico;
    }

    /**
     * Set: cdTipoParticipacaoPessoa.
     *
     * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
     */
    public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
        this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
    }

    /**
     * Get: cdTipoParticipacaoPessoa.
     *
     * @return cdTipoParticipacaoPessoa
     */
    public Integer getCdTipoParticipacaoPessoa() {
        return this.cdTipoParticipacaoPessoa;
    }

    /**
     * Set: cdPessoa.
     *
     * @param cdPessoa the cd pessoa
     */
    public void setCdPessoa(Long cdPessoa) {
        this.cdPessoa = cdPessoa;
    }

    /**
     * Get: cdPessoa.
     *
     * @return cdPessoa
     */
    public Long getCdPessoa() {
        return this.cdPessoa;
    }

    /**
     * Set: cdPessoaEndereco.
     *
     * @param cdPessoaEndereco the cd pessoa endereco
     */
    public void setCdPessoaEndereco(Long cdPessoaEndereco) {
        this.cdPessoaEndereco = cdPessoaEndereco;
    }

    /**
     * Get: cdPessoaEndereco.
     *
     * @return cdPessoaEndereco
     */
    public Long getCdPessoaEndereco() {
        return this.cdPessoaEndereco;
    }

    /**
     * Set: cdPessoaJuridica.
     *
     * @param cdPessoaJuridica the cd pessoa juridica
     */
    public void setCdPessoaJuridica(Long cdPessoaJuridica) {
        this.cdPessoaJuridica = cdPessoaJuridica;
    }

    /**
     * Get: cdPessoaJuridica.
     *
     * @return cdPessoaJuridica
     */
    public Long getCdPessoaJuridica() {
        return this.cdPessoaJuridica;
    }

    /**
     * Set: nrSequenciaEndereco.
     *
     * @param nrSequenciaEndereco the nr sequencia endereco
     */
    public void setNrSequenciaEndereco(Integer nrSequenciaEndereco) {
        this.nrSequenciaEndereco = nrSequenciaEndereco;
    }

    /**
     * Get: nrSequenciaEndereco.
     *
     * @return nrSequenciaEndereco
     */
    public Integer getNrSequenciaEndereco() {
        return this.nrSequenciaEndereco;
    }

    /**
     * Set: cdUsoPostal.
     *
     * @param cdUsoPostal the cd uso postal
     */
    public void setCdUsoPostal(Integer cdUsoPostal) {
        this.cdUsoPostal = cdUsoPostal;
    }

    /**
     * Get: cdUsoPostal.
     *
     * @return cdUsoPostal
     */
    public Integer getCdUsoPostal() {
        return this.cdUsoPostal;
    }

    /**
     * Set: cdUsoEnderecoPessoa.
     *
     * @param cdUsoEnderecoPessoa the cd uso endereco pessoa
     */
    public void setCdUsoEnderecoPessoa(Integer cdUsoEnderecoPessoa) {
        this.cdUsoEnderecoPessoa = cdUsoEnderecoPessoa;
    }

    /**
     * Get: cdUsoEnderecoPessoa.
     *
     * @return cdUsoEnderecoPessoa
     */
    public Integer getCdUsoEnderecoPessoa() {
        return this.cdUsoEnderecoPessoa;
    }

    /**
     * Set: cdEspeceEndereco.
     *
     * @param cdEspeceEndereco the cd espece endereco
     */
    public void setCdEspeceEndereco(Integer cdEspeceEndereco) {
        this.cdEspeceEndereco = cdEspeceEndereco;
    }

    /**
     * Get: cdEspeceEndereco.
     *
     * @return cdEspeceEndereco
     */
    public Integer getCdEspeceEndereco() {
        return this.cdEspeceEndereco;
    }

    /**
     * Set: cdOperacaoCanalInclusao.
     *
     * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
     */
    public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
        this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    }

    /**
     * Get: cdOperacaoCanalInclusao.
     *
     * @return cdOperacaoCanalInclusao
     */
    public String getCdOperacaoCanalInclusao() {
        return this.cdOperacaoCanalInclusao;
    }

    /**
     * Set: cdTipoCanalInclusao.
     *
     * @param cdTipoCanalInclusao the cd tipo canal inclusao
     */
    public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
        this.cdTipoCanalInclusao = cdTipoCanalInclusao;
    }

    /**
     * Get: cdTipoCanalInclusao.
     *
     * @return cdTipoCanalInclusao
     */
    public Integer getCdTipoCanalInclusao() {
        return this.cdTipoCanalInclusao;
    }

	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}

	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}

	/**
	 * Get: rdoEnderecoPessoa.
	 *
	 * @return rdoEnderecoPessoa
	 */
	public String getRdoEnderecoPessoa() {
		return rdoEnderecoPessoa;
	}

	/**
	 * Set: rdoEnderecoPessoa.
	 *
	 * @param rdoEnderecoPessoa the rdo endereco pessoa
	 */
	public void setRdoEnderecoPessoa(String rdoEnderecoPessoa) {
		this.rdoEnderecoPessoa = rdoEnderecoPessoa;
	}
}