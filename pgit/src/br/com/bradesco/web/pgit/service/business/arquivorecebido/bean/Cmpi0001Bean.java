/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivorecebido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivorecebido.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.log.ILogManager;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.IArquivoRecebidoService;
import br.com.bradesco.web.pgit.service.business.cmpi0000.bean.Cmpi0000Bean;
import br.com.bradesco.web.pgit.service.business.cmpi0000.bean.ListarClientesDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;

/**
 * Nome: Cmpi0001Bean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class Cmpi0001Bean {

	/** Atributo logger. */
	private ILogManager logger;

	/** Atributo mostraBotoes0001. */
	private boolean mostraBotoes0001;

	/** Atributo mostraBotoes0002. */
	private boolean mostraBotoes0002;

	/** Atributo mostraBotoes0003. */
	private boolean mostraBotoes0003;

	/** Atributo mostraBotoes0004. */
	private boolean mostraBotoes0004;

	/** Atributo mostraBotoes0005. */
	private boolean mostraBotoes0005;

	/** Atributo mostraBotoes0006. */
	private boolean mostraBotoes0006;

	/** Atributo validaCpf. */
	private String validaCpf;

	/** Atributo validaCnpj. */
	private String validaCnpj;

	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;

	/** Atributo validaCpfCliente. */
	private String validaCpfCliente;

	/** Atributo hiddenRadioInconsistente. */
	private int hiddenRadioInconsistente;

	/** Atributo codListaIncosistentes. */
	private String codListaIncosistentes;

	/** Atributo codListaRemessaRadio. */
	private int codListaRemessaRadio;

	/** Atributo filtro. */
	private int filtro;

	/** Atributo cpfFiltro. */
	private String cpfFiltro;

	/** Atributo dataDeFiltro. */
	private Date dataDeFiltro;

	/** Atributo dataAteFiltro. */
	private Date dataAteFiltro;

	/** Atributo numeroArquivoRemessa. */
	private String numeroArquivoRemessa;

	/** Atributo listaProdutoAux. */
	private List<ComboProdutoDTO> listaProdutoAux;

	/** Atributo produtoFiltro. */
	private String produtoFiltro;

	/** Atributo perfilFiltro. */
	private String perfilFiltro;

	/** Atributo sequenciaFiltro. */
	private String sequenciaFiltro;

	/** Atributo processamentoFiltro. */
	private String processamentoFiltro;

	/** Atributo resultadoProcessamentoFiltro. */
	private String resultadoProcessamentoFiltro;

	/** Atributo cpfcnpj. */
	private String cpfcnpj;

	/** Atributo filial. */
	private String filial;

	/** Atributo controlecpf. */
	private String controlecpf;

	/** Atributo cpf. */
	private String cpf;

	/** Atributo dataDe. */
	private String dataDe;

	/** Atributo dataAte. */
	private String dataAte;

	/** Atributo produto. */
	private String produto;

	/** Atributo perfil. */
	private String perfil;

	/** Atributo processamento. */
	private String processamento;

	/** Atributo resultadoProcessamento. */
	private String resultadoProcessamento;

	/** Atributo nomeRazao. */
	private String nomeRazao;

	/** Atributo banco. */
	private String banco;

	/** Atributo agencia. */
	private String agencia;

	/** Atributo razao. */
	private String razao;

	/** Atributo conta. */
	private String conta;

	/** Atributo codigoLancamento. */
	private String codigoLancamento;

	/** Atributo dataHoraRecepcao. */
	private String dataHoraRecepcao;

	/** Atributo dataRecepcaoInclusao. */
	private String dataRecepcaoInclusao;

	/** Atributo canalTransmissao. */
	private String canalTransmissao;

	/** Atributo layout. */
	private String layout;

	/** Atributo qtdRegistrosRemessa. */
	private String qtdRegistrosRemessa;

	/** Atributo valorTotalRemessa. */
	private String valorTotalRemessa;

	/** Atributo sequenciaRemessa. */
	private String sequenciaRemessa;

	/** Atributo qtdRegistrosLote. */
	private String qtdRegistrosLote;

	/** Atributo modalidadePagamento. */
	private String modalidadePagamento;

	/** Atributo valorTotalLote. */
	private String valorTotalLote;

	/** Atributo processamentoLote. */
	private String processamentoLote;

	/** Atributo condicaoLote. */
	private String condicaoLote;

	/** Atributo qtdInconsistentes. */
	private String qtdInconsistentes;

	/** Atributo valorInconsistentes. */
	private String valorInconsistentes;

	/** Atributo sequenciaRegistros. */
	private String sequenciaRegistros;

	/** Atributo instrucaoMovimento. */
	private String instrucaoMovimento;

	/** Atributo numeroPagamento. */
	private String numeroPagamento;

	/** Atributo codListaLote. */
	private int codListaLote;

	/** Atributo hdnCodListaLote. */
	private int hdnCodListaLote;

	/** Atributo sequenciaLote. */
	private String sequenciaLote;

	/** Atributo situacaoProcessamentoRemessa. */
	private String situacaoProcessamentoRemessa;

	/** Atributo resultadoProcessamentoLote. */
	private String resultadoProcessamentoLote;

	/** Atributo situacaoProcessamentoLote. */
	private String situacaoProcessamentoLote;

	/** Atributo modalidadePagamentoLote. */
	private String modalidadePagamentoLote;

	/** Atributo bancoAgenciaConta. */
	private String bancoAgenciaConta;

	/** Atributo qtdConsistentes. */
	private String qtdConsistentes;

	/** Atributo valorConsistentes. */
	private String valorConsistentes;

	/** Atributo dataNascimentoFundacao. */
	private Date dataNascimentoFundacao;

	/** Atributo filtroIdentificacaoCliente. */
	private String filtroIdentificacaoCliente;

	/** Atributo cpfCnpjIdentificaoCliente. */
	private String cpfCnpjIdentificaoCliente;

	/** Atributo nomeRazaoIdentificaCliente. */
	private String nomeRazaoIdentificaCliente;

	/** Atributo bancoIdentificacaoCliente. */
	private String bancoIdentificacaoCliente;

	/** Atributo agenciaIdentificacaoCliente. */
	private String agenciaIdentificacaoCliente;

	/** Atributo contaIdentificacaoCliente. */
	private String contaIdentificacaoCliente;

	/** Atributo identificacao. */
	private String identificacao;

	/** Atributo cmpi0000Bean. */
	private Cmpi0000Bean cmpi0000Bean;

	/** Atributo listaGridRemessa. */
	private List<ArquivoRecebidoDTO> listaGridRemessa;

	/** Atributo listaGridIncosistentes. */
	private List<ArquivoRecebidoInconsistentesDTO> listaGridIncosistentes;

	/** Atributo listaGridOcorrencia. */
	private List<OcorrenciaDTO> listaGridOcorrencia;

	/** Atributo arquivoRemessaRecebidoImpl. */
	private IArquivoRecebidoService arquivoRemessaRecebidoImpl;

	/** Atributo listaGridCliente. */
	private List<ListarClientesDTO> listaGridCliente;

	/** Atributo ambienteProcessamento. */
	private String ambienteProcessamento;

	/** Atributo listaOcorrencia. */
	private List<OcorrenciaDTO> listaOcorrencia;

	/** Atributo listaLote. */
	private List<LoteDTO> listaLote;

	/** Atributo hiddenRadioCliente. */
	private int hiddenRadioCliente;

	/** Atributo hiddenRadioFiltroArgumento. */
	private int hiddenRadioFiltroArgumento;

	/** Atributo listaGridEstatisticaLote. */
	private List<EstatisticaLoteDTO> listaGridEstatisticaLote;

	/** Atributo listaCodigo. */
	private List<SelectItem> listaCodigo = new ArrayList<SelectItem>();

	/** Atributo listaControleIncosistentes. */
	private List<SelectItem> listaControleIncosistentes = new ArrayList<SelectItem>();

	/** Atributo listaControleRemessa. */
	private List<SelectItem> listaControleRemessa = new ArrayList<SelectItem>();

	/** Atributo listaControleLote. */
	private List<SelectItem> listaControleLote = new ArrayList<SelectItem>();

	/** Atributo listaProcessamento. */
	private List<SelectItem> listaProcessamento = new ArrayList<SelectItem>();

	/** Atributo listaResultadoProcessamento. */
	private List<SelectItem> listaResultadoProcessamento = new ArrayList<SelectItem>();

	/** Atributo listaControleCliente. */
	private List<SelectItem> listaControleCliente = new ArrayList<SelectItem>();

	/** Atributo listaProduto. */
	private List<SelectItem> listaProduto = null;

	/** Atributo desabSitProc. */
	private boolean desabSitProc = true;

	/** Atributo desabPerfil. */
	private boolean desabPerfil;

	/** Atributo desabTextRemessa. */
	private boolean desabTextRemessa;

	/** Atributo desabEstatisticaIncons. */
	private boolean desabEstatisticaIncons = true;
	
	/** Atributo exibirFocus. */
	private Character exibirFocus;

	/**
	 * Cmpi0001 bean.
	 */
	public Cmpi0001Bean() {

	}

	/**
	 * Limpar pagina.
	 *
	 * @param event the event
	 */
	public void limparPagina(ActionEvent event) {

		logger.debug(this, "Iniciando a limpeza dos dados da p�gina.");

		this.setIdentificacao("0");

		desabPerfil = true;
		desabTextRemessa = true;

		cmpi0000Bean.setNomeRazaoFundacao("");
		cmpi0000Bean.setCpf("");
		cmpi0000Bean.setCpf1("");
		cmpi0000Bean.setCpf2("");
		cmpi0000Bean.setCnpj1("");
		cmpi0000Bean.setCnpj2("");
		cmpi0000Bean.setCnpj3("");
		cmpi0000Bean.setNomeRazaoIdentificaCliente("");
		cmpi0000Bean.setCnpj1aux("");
		cmpi0000Bean.setCnpj2aux("");
		cmpi0000Bean.setCnpj3aux("");
		cmpi0000Bean.setAgenciaIdentificacaoCliente("");
		cmpi0000Bean.setBancoIdentificacaoCliente("");
		cmpi0000Bean.setContaIdentificacaoCliente("");
		cmpi0000Bean.setDiaNasc("");
		cmpi0000Bean.setMesNasc("");
		cmpi0000Bean.setAnoNasc("");

		this.cmpi0000Bean.setDesabDataRecRet(true);
		this.cmpi0000Bean.setDesabDataFinRet(true);
		this.cmpi0000Bean.setDesabServico(true);
		this.cmpi0000Bean.setDesabCodEmpresaRet(true);
		this.cmpi0000Bean.setDesabSeqRemessaRet(true);
		this.setDesabSitProc(true);
		this.cmpi0000Bean.setDesabBtnConsultarRet(true);
		this.cmpi0000Bean.setDesabResTipo(true);
		this.cmpi0000Bean.setDesabBtnConsultarRem(true);

		this.setIdentificacao("0");
		limparDadosIdentificaoCliente();
		limparListaClientes();
		this.setDataDeFiltro(new Date());
		this.setDataAteFiltro(new Date());
		this.setProdutoFiltro("");
		this.setPerfilFiltro("");
		this.setSequenciaFiltro("");
		this.setProcessamentoFiltro("");
		this.setResultadoProcessamentoFiltro("");
		this.setMostraBotoes0001(false);
		this.setListaGridRemessa(new ArrayList<ArquivoRecebidoDTO>());

		loadComboProduto();

		exibirFocus = 'S';
		
		logger.debug(this, "Finalizando a limpeza dos dados da p�gina.");
	}

	/**
	 * Load combo produto.
	 */
	private void loadComboProduto() {

		if (listaProduto == null) {
			logger.debug(this, "Iniciando a consulta de servi�os  para popular o combo de servi�os.");
			try {
				List<ComboProdutoDTO> comboProdutoList = arquivoRemessaRecebidoImpl.listaComboProduto();
				int i = 0;
				listaProduto = new ArrayList<SelectItem>();
				for (i = 0; i < comboProdutoList.size(); i++) {
					listaProduto.add(new SelectItem(comboProdutoList.get(i).getCentroCusto(), comboProdutoList.get(i).getDescricaoCentroCusto()));
				}
			} catch (PdcAdapterException p) {
				this.setListaProduto(new ArrayList<SelectItem>());
				logger.debug(this, "N�o foi possivel carregar a lista do PDC.");
				logger.debug(this, "/tC�dgio: " + p.getCode() + "/t Mensagen: " + p.getMessage());
			} finally {
				logger.debug(this, "Finalizando o carregamento da lista de produtos.");
			}
		}

	}

	/**
	 * Limpar dados identificao cliente.
	 *
	 * @return the string
	 */
	public String limparDadosIdentificaoCliente() {
		return "ok";
	}

	/**
	 * Limpar lista clientes.
	 */
	public void limparListaClientes() {
		this.listaGridCliente = new ArrayList<ListarClientesDTO>();
	}

	/**
	 * Limpar erro.
	 */
	public void limparErro() {

		logger.debug(this, "Iniciando limpeza dos campos.");

		this.setMostraBotoes0001(false);

		cmpi0000Bean.setNomeRazaoFundacao("");
		cmpi0000Bean.setCpf("");
		cmpi0000Bean.setCnpj1("");
		cmpi0000Bean.setCnpj2("");
		cmpi0000Bean.setCnpj3("");
		cmpi0000Bean.setNomeRazaoIdentificaCliente("");
		cmpi0000Bean.setCnpj1aux("");
		cmpi0000Bean.setCnpj2aux("");
		cmpi0000Bean.setCnpj3aux("");
		cmpi0000Bean.setAgenciaIdentificacaoCliente("");
		cmpi0000Bean.setBancoIdentificacaoCliente("");
		cmpi0000Bean.setContaIdentificacaoCliente("");
		cmpi0000Bean.setDiaNasc("");
		cmpi0000Bean.setMesNasc("");
		cmpi0000Bean.setAnoNasc("");

		this.cmpi0000Bean.setDesabDataRecRet(true);
		this.cmpi0000Bean.setDesabDataFinRet(true);
		this.cmpi0000Bean.setDesabServico(true);
		this.cmpi0000Bean.setDesabCodEmpresaRet(true);
		this.cmpi0000Bean.setDesabSeqRemessaRet(true);
		this.setDesabSitProc(true);
		this.cmpi0000Bean.setDesabBtnConsultarRet(true);
		this.cmpi0000Bean.setDesabResTipo(true);
		this.cmpi0000Bean.setDesabBtnConsultarRem(true);

		this.setDataDeFiltro(new Date());
		this.setDataAteFiltro(new Date());
		this.setProdutoFiltro("");
		this.setPerfilFiltro("");
		this.setSequenciaFiltro("");
		this.setProcessamentoFiltro("");
		this.setResultadoProcessamentoFiltro("");
		this.setIdentificacao("0");

		this.setListaGridRemessa(new ArrayList<ArquivoRecebidoDTO>());

		exibirFocus = 'S';
		
		habilitarFiltros();
		
		logger.debug(this, "Finalizando limpeza dos campos.");
		
	}

	/**
	 * Voltar identificacao cliente.
	 *
	 * @return the string
	 */
	public String voltarIdentificacaoCliente() {
		this.listaGridCliente = null;
		limparDadosIdentificaoCliente();
		return "VOLTAR_PESQUISAR";
	}

	/**
	 * Find arquivo recebido.
	 *
	 * @return the list< arquivo recebido dt o>
	 */
	private List<ArquivoRecebidoDTO> findArquivoRecebido() {

		ArquivoRecebidoDTO dto = new ArquivoRecebidoDTO();

		if (cmpi0000Bean.getCnpj1aux() != null) {
			dto.setCpf_cliente(cmpi0000Bean.getCnpj1aux());
		} else {
			dto.setCpf_cliente("");
		}

		if (cmpi0000Bean.getCnpj2aux() != null) {
			dto.setCpf_filial(cmpi0000Bean.getCnpj2aux());
		} else {
			dto.setCpf_filial("");
		}

		if (cmpi0000Bean.getCnpj3aux() != null) {
			dto.setCpf_controle(cmpi0000Bean.getCnpj3aux());
		} else {
			dto.setCpf_controle("");
		}

		if (this.getDataDeFiltro() == null || this.getDataDeFiltro().equals("")) {
			dto.setDataRecepcaoDe("");
		} else {
			dto.setDataRecepcaoDe(SiteUtil.dateToString(this.getDataDeFiltro(), "dd.MM.yyyy"));
		}
			
		if (this.getDataAteFiltro() == null || this.getDataAteFiltro().equals("")) {
			dto.setDataRecepcaoAte("");
		} else {
			dto.setDataRecepcaoAte(SiteUtil.dateToString(this.getDataAteFiltro(), "dd.MM.yyyy"));
		}

		dto.setProduto(this.getProdutoFiltro());
		dto.setPerfil(this.getPerfilFiltro());
		dto.setProcessamento(this.getProcessamentoFiltro());
		dto.setResultadoProcessamento(this.getResultadoProcessamentoFiltro());
		dto.setNumeroArqRemessa(this.getSequenciaFiltro());

		List<ArquivoRecebidoDTO> listaArquivoRecebido = arquivoRemessaRecebidoImpl.listaGrid(dto);

		return listaArquivoRecebido;
	}

	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista() {

		logger.debug(this, "Iniciando o carregamento da lista.");

		try {
			if (this.validarCamposDePesquisa()) {
				
				//limpa a lista para preencher com os dados da nova consulta
				this.setListaGridRemessa(new ArrayList<ArquivoRecebidoDTO>());
				
				this.setListaGridRemessa(findArquivoRecebido());

				if (this.getListaGridRemessa().size() > 10) {
					this.setMostraBotoes0001(true);
				} else {
					this.setMostraBotoes0001(false);
				}

				List<SelectItem> listaSelectItem = new ArrayList<SelectItem>();

				for (int i = 0; i < this.getListaGridRemessa().size(); i++) {
					listaSelectItem.add(new SelectItem(i, " "));
				}
				this.setListaControleRemessa(listaSelectItem);
			}
		} catch (PdcAdapterException p) {
			String code = p.getCode().substring(p.getCode().length()-8,p.getCode().length());
			BradescoFacesUtils.addInfoModalMessage( "(" + code + ") " + p.getMessage(), "/CMPI0001.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			return null;
		} finally {
			logger.debug(this, "Finalizando o carregamento da lista.");
			habilitarFiltros();
		}

		return "ok";
	}

	/**
	 * Pesquisar arquivo recebido.
	 *
	 * @param event the event
	 */
	public void pesquisarArquivoRecebido(ActionEvent event) {
		logger.debug(this, "Iniciando o carregamento da lista para a pagina��o.");
		this.setListaGridRemessa(findArquivoRecebido());
		logger.debug(this, "Final da pesquisa para a pagina��o.");
	}

	/**
	 * Habilitar filtros.
	 */
	public void habilitarFiltros() {
		if (this.produtoFiltro == null) {
			this.produtoFiltro = "";
		}
		if (this.perfilFiltro == null) {
			this.perfilFiltro = "";
		}

		this.setDesabPerfil(this.produtoFiltro.equals(""));
		this.setDesabTextRemessa(this.perfilFiltro.equals(""));
	}

	/**
	 * Find lote.
	 *
	 * @return the list< lote dt o>
	 */
	private List<LoteDTO> findLote() {

		ArquivoRecebidoDTO arquivoRemessaRecebidoDTO = getListaGridRemessa().get(getCodListaRemessaRadio());

		LoteDTO loteDTO = new LoteDTO();

		loteDTO.setSistemaLegado(arquivoRemessaRecebidoDTO.getCodSistemaLegado());
		loteDTO.setControleCpfCnpj(arquivoRemessaRecebidoDTO.getCpf_controle());
		loteDTO.setCpfCnpj(arquivoRemessaRecebidoDTO.getCpf_cliente().replace(".", ""));
		loteDTO.setFilialCnpj(arquivoRemessaRecebidoDTO.getCpf_filial());
		loteDTO.setFiller("");
		loteDTO.setModalidadePagto("");
		loteDTO.setSequenciaRemessa("");
		loteDTO.setNumeroLote("");
		loteDTO.setPerfil(arquivoRemessaRecebidoDTO.getPerfil());
		loteDTO.setQtdeRegistros("");
		loteDTO.setRecurso("");
		loteDTO.setResultadoProcessamento("");
		loteDTO.setNumeroArquivoRemessa(arquivoRemessaRecebidoDTO.getNumeroArqRemessa());
		loteDTO.setTipoPesquisa("");
		loteDTO.setRecurso("");
		loteDTO.setDataInclusaoRemessa(arquivoRemessaRecebidoDTO.getDataRecepcaoInclusao());

		return arquivoRemessaRecebidoImpl.listaLote(loteDTO);
	}

	/**
	 * Consultar lotes.
	 *
	 * @return the string
	 */
	public String consultarLotes() {

		logger.debug(this, "Iniciando a consulta de lotes.");

		try {
			ArquivoRecebidoDTO arquivoRemessaRecebidoDTO = getListaGridRemessa().get(getCodListaRemessaRadio());

			setCpf(arquivoRemessaRecebidoDTO.getCpf_completo());
			setCpfcnpj(arquivoRemessaRecebidoDTO.getCpf_cliente());
			setFilial(arquivoRemessaRecebidoDTO.getCpf_filial());
			setControlecpf(arquivoRemessaRecebidoDTO.getCpf_controle());
			setNomeRazao(arquivoRemessaRecebidoDTO.getNome());
			setProduto(arquivoRemessaRecebidoDTO.getProduto());
			setPerfil(arquivoRemessaRecebidoDTO.getPerfil());
			setBanco(arquivoRemessaRecebidoDTO.getBanco());
			setAgencia(arquivoRemessaRecebidoDTO.getAgencia());
			setRazao(arquivoRemessaRecebidoDTO.getRazao());
			setConta(arquivoRemessaRecebidoDTO.getConta());
			setCodigoLancamento(arquivoRemessaRecebidoDTO.getCodigoLancamento());
			setCanalTransmissao(arquivoRemessaRecebidoDTO.getDescricaoCanalTransmissao());
			setSequenciaRemessa(arquivoRemessaRecebidoDTO.getNumeroArqRemessa());
			setDataHoraRecepcao(arquivoRemessaRecebidoDTO.getDataRecepcao());
			setAmbienteProcessamento(arquivoRemessaRecebidoDTO.getDescricaoAmbienteProcessamento());
			setLayout(arquivoRemessaRecebidoDTO.getDescricaoLayout());
			setSituacaoProcessamentoRemessa(arquivoRemessaRecebidoDTO.getSituacaoProcessamento());
			setResultadoProcessamento(arquivoRemessaRecebidoDTO.getResultadoProcessamento());
			setQtdRegistrosRemessa(arquivoRemessaRecebidoDTO.getQtdRegistros());
			setValorTotalRemessa(arquivoRemessaRecebidoDTO.getValorTotal());
			setDataRecepcaoInclusao(arquivoRemessaRecebidoDTO.getDataRecepcaoInclusao());

			setListaLote(findLote());

			List<SelectItem> listaSelectItem = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaLote().size(); i++) {
				listaSelectItem.add(new SelectItem(i, ""));
			}

			setListaControleLote(listaSelectItem);

			if (this.getListaLote().size() > 10) {
				this.setMostraBotoes0002(true);
			} else {
				this.setMostraBotoes0002(false);
			}

		} catch (PdcAdapterException p) {
			String code = p.getCode().substring(p.getCode().length()-8,p.getCode().length());
			BradescoFacesUtils.addInfoModalMessage( "(" + code + ") " + p.getMessage(), "/CMPI0001.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			return null;
		} finally {
			logger.debug(this, "Finalizando a consulta de lotes.");
		}

		return "CONSULTAR_LOTES";
	}

	/**
	 * Pesquisar lote.
	 *
	 * @param event the event
	 */
	public void pesquisarLote(ActionEvent event) {
		logger.debug(this, "Iniciando a consulta de lotes.");
		setListaLote(findLote());
		logger.debug(this, "Iniciando a consulta de lotes.");
	}

	/**
	 * Find o correncia remessa.
	 *
	 * @return the list< ocorrencia dt o>
	 */
	private List<OcorrenciaDTO> findOCorrenciaRemessa() {

		ArquivoRecebidoDTO arquivoRemessaRecebidoDTO = getListaGridRemessa().get(getCodListaRemessaRadio());

		OcorrenciaDTO ocorrenciaDTO = new OcorrenciaDTO();

		ocorrenciaDTO.setControleCpfCnpj(arquivoRemessaRecebidoDTO.getCpf_controle());
		ocorrenciaDTO.setCpfCnpj(arquivoRemessaRecebidoDTO.getCpf_cliente().replace(".", ""));
		ocorrenciaDTO.setFilialCnpj(arquivoRemessaRecebidoDTO.getCpf_filial());
		ocorrenciaDTO.setNumeroArquivoRemessa(arquivoRemessaRecebidoDTO.getNumeroArqRemessa());
		ocorrenciaDTO.setNumeroLoteRemessa(getSequenciaLote());
		ocorrenciaDTO.setNumeroSequenciaRemessa("");
		ocorrenciaDTO.setPerfil(getPerfil());
		ocorrenciaDTO.setCodigoSistemaLegado(arquivoRemessaRecebidoDTO.getCodSistemaLegado());
		ocorrenciaDTO.setTipoPesquisa("R");
		ocorrenciaDTO.setDataRecepcaoInclusao(getDataRecepcaoInclusao());

		return arquivoRemessaRecebidoImpl.listaOcorrencia(ocorrenciaDTO);
	}

	/**
	 * Consultar ocorrencias remessa.
	 *
	 * @return the string
	 */
	public String consultarOcorrenciasRemessa() {

		logger.debug(this, "Iniciando a consulta das ocorr�ncias da remessa.");

		try {
			ArquivoRecebidoDTO arquivoRemessaRecebidoDTO = getListaGridRemessa().get(getCodListaRemessaRadio());

			setCpfcnpj(arquivoRemessaRecebidoDTO.getCpf_cliente());
			setFilial(arquivoRemessaRecebidoDTO.getCpf_filial());
			setControlecpf(arquivoRemessaRecebidoDTO.getCpf_controle());
			setCpf(arquivoRemessaRecebidoDTO.getCpf_completo());
			setNomeRazao(arquivoRemessaRecebidoDTO.getNome());
			setProduto(arquivoRemessaRecebidoDTO.getProduto());
			setPerfil(arquivoRemessaRecebidoDTO.getPerfil());
			setBanco(arquivoRemessaRecebidoDTO.getBanco());
			setAgencia(arquivoRemessaRecebidoDTO.getAgencia());
			setRazao(arquivoRemessaRecebidoDTO.getRazao());
			setConta(arquivoRemessaRecebidoDTO.getConta());
			setCodigoLancamento(arquivoRemessaRecebidoDTO.getCodigoLancamento());
			setCanalTransmissao(arquivoRemessaRecebidoDTO.getDescricaoCanalTransmissao());
			setSequenciaRemessa(arquivoRemessaRecebidoDTO.getNumeroArqRemessa());
			setDataHoraRecepcao(arquivoRemessaRecebidoDTO.getDataRecepcao());
			setAmbienteProcessamento(arquivoRemessaRecebidoDTO.getDescricaoAmbienteProcessamento());
			setLayout(arquivoRemessaRecebidoDTO.getDescricaoLayout());
			setSituacaoProcessamentoRemessa(arquivoRemessaRecebidoDTO.getSituacaoProcessamento());
			setResultadoProcessamento(arquivoRemessaRecebidoDTO.getResultadoProcessamento());
			setQtdRegistrosRemessa(arquivoRemessaRecebidoDTO.getQtdRegistros());
			setValorTotalRemessa(arquivoRemessaRecebidoDTO.getValorTotal());
			setDataRecepcaoInclusao(arquivoRemessaRecebidoDTO.getDataRecepcaoInclusao());

			// fazendo a carga inicial
			setListaOcorrencia(findOCorrenciaRemessa());

			if (this.getListaOcorrencia().size() > 10) {
				this.setMostraBotoes0006(true);
			} else {
				this.setMostraBotoes0006(false);
			}

		} catch (PdcAdapterException p) {
			String code = p.getCode().substring(p.getCode().length()-8,p.getCode().length());
			BradescoFacesUtils.addInfoModalMessage( "(" + code + ") " + p.getMessage(), "/CMPI0001.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			return null;
		} finally {
			logger.debug(this, "Finalizando a consulta das ocorr�ncias da remessa.");
		}

		return "CONSULTAR_OCORRENCIAS_REMESSA";
	}

	/**
	 * Pequisar ocorrencia remessa.
	 *
	 * @param event the event
	 */
	public void pequisarOcorrenciaRemessa(ActionEvent event) {
		logger.debug(this, "Iniciando a consulta para ocorrencias da remessa.");
		setListaOcorrencia(findOCorrenciaRemessa());
		logger.debug(this, "Finalizou a consulta para ocorrencias da remessa.");
	}

	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar() {

		logger.debug(this, "Iniciando a limpeza dos campos.");

		cmpi0000Bean.setNomeRazaoFundacao("");
		cmpi0000Bean.setCpf("");
		cmpi0000Bean.setCpf1("");
		cmpi0000Bean.setCpf2("");
		cmpi0000Bean.setCnpj1("");
		cmpi0000Bean.setCnpj2("");
		cmpi0000Bean.setCnpj3("");
		cmpi0000Bean.setNomeRazaoIdentificaCliente("");
		cmpi0000Bean.setCnpj1aux("");
		cmpi0000Bean.setCnpj2aux("");
		cmpi0000Bean.setCnpj3aux("");
		cmpi0000Bean.setAgenciaIdentificacaoCliente("");
		cmpi0000Bean.setBancoIdentificacaoCliente("");
		cmpi0000Bean.setContaIdentificacaoCliente("");
		cmpi0000Bean.setDiaNasc("");
		cmpi0000Bean.setMesNasc("");
		cmpi0000Bean.setAnoNasc("");

		desabPerfil = true;
		desabTextRemessa = true;

		this.cmpi0000Bean.setDesabDataRecRet(true);
		this.cmpi0000Bean.setDesabDataFinRet(true);
		this.cmpi0000Bean.setDesabServico(true);
		this.cmpi0000Bean.setDesabCodEmpresaRet(true);
		this.cmpi0000Bean.setDesabSeqRemessaRet(true);
		this.setDesabSitProc(true);
		this.cmpi0000Bean.setDesabBtnConsultarRet(true);
		this.cmpi0000Bean.setDesabResTipo(true);
		this.cmpi0000Bean.setDesabBtnConsultarRem(true);

		this.setDataDeFiltro(new Date());
		this.setDataAteFiltro(new Date());
		this.setProdutoFiltro("");
		this.setPerfilFiltro("");
		this.setSequenciaFiltro("");
		this.setProcessamentoFiltro("");
		this.setResultadoProcessamentoFiltro("");
		this.setMostraBotoes0001(false);
		this.setListaGridRemessa(new ArrayList<ArquivoRecebidoDTO>());

		logger.debug(this, "Finalizando a limpeza dos campos.");

		return "ok";
	}

	/**
	 * Find estatistica.
	 *
	 * @return the list< estatistica lote dt o>
	 */
	private List<EstatisticaLoteDTO> findEstatistica() {
		LoteDTO loteDTO = (LoteDTO) getListaLote().get(getHdnCodListaLote());

		EstatisticaLoteDTO dto = new EstatisticaLoteDTO();

		dto.setSistemaLegado(loteDTO.getSistemaLegado());
		dto.setCpfCnpj(loteDTO.getCpfCnpj());
		dto.setCodigoFilialCnpj(loteDTO.getFilialCnpj());
		dto.setCodigoControleCpfCnpj(loteDTO.getControleCpfCnpj());
		dto.setPerfil(loteDTO.getPerfil());
		dto.setNumeroArquivoRemessa(loteDTO.getNumeroArquivoRemessa());
		dto.setNumeroLoteRemessa(getSequenciaLote());
		dto.setNumeroSequenciaRemessa("0");
		dto.setTipoPesquisa("");
		dto.setRecurso("");
		dto.setFiller("");
		dto.setDataInclusaoRemessa(loteDTO.getDataInclusaoRemessa());

		List<EstatisticaLoteDTO> listaEstatistica = arquivoRemessaRecebidoImpl.listaGridEstatisticasLote(dto);

		return listaEstatistica;
	}

	/**
	 * Consultar estatisticas lote.
	 *
	 * @return the string
	 */
	public String consultarEstatisticasLote() {

		logger.debug(this, "Iniciando a consulta das estat�sticas dos lotes.");

		try {
			LoteDTO loteDTO = (LoteDTO) getListaLote().get(getHdnCodListaLote());

			setSequenciaLote(loteDTO.getSeqLote());
			setSituacaoProcessamentoLote(loteDTO.getDescricaoSituacaoProcessamento());
			setResultadoProcessamentoLote(loteDTO.getDescricaoResultadoProcessamento());
			setBancoAgenciaConta(loteDTO.getBcoAgeCtaDebito());
			setModalidadePagamentoLote(loteDTO.getDescricaoModalidadePagto());
			setQtdRegistrosLote(loteDTO.getQtdeRegistros());
			setValorTotalLote(loteDTO.getValorTotal());
			setNumeroArquivoRemessa(loteDTO.getNumeroArquivoRemessa());
			setDataRecepcaoInclusao(loteDTO.getDataInclusaoRemessa());

			setListaGridEstatisticaLote(findEstatistica());

			this.setDesabEstatisticaIncons(true);
			for (EstatisticaLoteDTO item : this.getListaGridEstatisticaLote()) {
				if (!item.getRegInconsistenteQtde().equals("0")) {
					this.setDesabEstatisticaIncons(false);
				}
			}

			if (this.getListaGridEstatisticaLote().size() > 10) {
				this.setMostraBotoes0003(true);
			} else {
				this.setMostraBotoes0003(false);
			}

		} catch (PdcAdapterException p) {
			String code = p.getCode().substring(p.getCode().length()-8,p.getCode().length());
			BradescoFacesUtils.addInfoModalMessage( "(" + code + ") " + p.getMessage(), "/CMPI0002.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			return null;
		} finally {
			logger.debug(this, "Finalizando a consulta das estat�sticas dos lotes.");
		}

		return "ESTATISTICAS_LOTE";
	}

	/**
	 * Pesquisar estatistica lote.
	 *
	 * @param event the event
	 */
	public void pesquisarEstatisticaLote(ActionEvent event) {
		logger.debug(this, "Iniciando a consulta de estatisticas do lote.");
		setListaGridEstatisticaLote(findEstatistica());
		logger.debug(this, "Iniciando a consulta de estatisticas do lote.");
	}

	/**
	 * Consultar cliente.
	 *
	 * @return the string
	 */
	public String consultarCliente() {

		logger.debug(this, "Iniciando a consulta dos clientes.");

		try {

			String validaDocumento = validarCampos();
			cmpi0000Bean.setTela("REMESSA");
			cmpi0000Bean.setValidaCpfCliente(validaDocumento);
			cmpi0000Bean.setHiddenRadioFiltroArgumento(getHiddenRadioFiltroArgumento());

			if (validaDocumento.equals("T")) {
				ListarClientesDTO dto = new ListarClientesDTO();

				boolean cpfBool = (cmpi0000Bean.getCpf1() != null && !cmpi0000Bean.getCpf1().equals("")) || (cmpi0000Bean.getCpf2() != null && !cmpi0000Bean.getCpf2().equals(""));
				boolean cnpjBool = (cmpi0000Bean.getCnpj1() != null && !cmpi0000Bean.getCnpj1().equals("")) || (cmpi0000Bean.getCnpj2() != null && !cmpi0000Bean.getCnpj2().equals("")) || (cmpi0000Bean.getCnpj3() != null && !cmpi0000Bean.getCnpj3().equals(""));
				if (cnpjBool) {
					dto.setCodigoCpfCnpjCliente(cmpi0000Bean.getCnpj1());
					dto.setCodigoFilialCnpj(cmpi0000Bean.getCnpj2());
					dto.setControleCpfCnpj(cmpi0000Bean.getCnpj3());
				}

				if (cpfBool) {
					StringBuffer cpfConjunto = new StringBuffer();
					cpfConjunto.append(cmpi0000Bean.getCpf1() != null ? cmpi0000Bean.getCpf1() : "");
					cpfConjunto.append(cmpi0000Bean.getCpf2() != null ? cmpi0000Bean.getCpf2() : "");
					cmpi0000Bean.setCpfCnpjIdentificaoCliente(cpfConjunto.toString());
					dto.setCodigoCpfCnpjCliente(cmpi0000Bean.getCpf1());
					dto.setControleCpfCnpj(cmpi0000Bean.getCpf2());
					dto.setCodigoFilialCnpj("");
				}

				if (!cpfBool && !cnpjBool) {
					dto.setCodigoCpfCnpjCliente("");
					dto.setCodigoFilialCnpj("");
					dto.setControleCpfCnpj("");
				}

				String dataNasc = "";

				if ((!(SiteUtil.isEmptyOrNull(this.cmpi0000Bean.getDiaNasc()))) && (!(SiteUtil.isEmptyOrNull(this.cmpi0000Bean.getMesNasc()))) && (!(SiteUtil.isEmptyOrNull(this.cmpi0000Bean.getAnoNasc())))) {
					dataNasc = this.cmpi0000Bean.getDiaNasc() + "." + this.cmpi0000Bean.getMesNasc() + "." + this.cmpi0000Bean.getAnoNasc();
				}

				dto.setDataNascimentoFundacao(dataNasc);
				dto.setNomeRazao(this.cmpi0000Bean.getNomeRazaoIdentificaCliente().toUpperCase());
				dto.setCodigoBancoDebito(this.cmpi0000Bean.getBancoIdentificacaoCliente());
				dto.setAgenciaBancaria(this.cmpi0000Bean.getAgenciaIdentificacaoCliente());
				dto.setContaBancaria(this.cmpi0000Bean.getContaIdentificacaoCliente());

				this.cmpi0000Bean.setListaGridCliente(arquivoRemessaRecebidoImpl.listaGridClientes(dto));

				if (this.cmpi0000Bean.getListaGridCliente().size() > 1) {
					this.cmpi0000Bean.carregaListaClientes();
					this.cmpi0000Bean.limparCamposDePesquisa();

					this.setDataDeFiltro(new Date());
					this.setDataAteFiltro(new Date());
					this.setProdutoFiltro("");
					this.setPerfilFiltro("");
					this.setSequenciaFiltro("");
					this.setProcessamentoFiltro("");
					this.setResultadoProcessamentoFiltro("");
					this.setMostraBotoes0001(false);
					this.setIdentificacao("0");
					
					this.cmpi0000Bean.setDesabDataRec(false);
					this.cmpi0000Bean.setDesabDataFin(false);
					this.cmpi0000Bean.setDesabServico(false);
					this.cmpi0000Bean.setDesabCodEmpresa(false);
					this.cmpi0000Bean.setDesabSeqRemessa(false);
					this.cmpi0000Bean.setDesabResProc(true);
					this.cmpi0000Bean.setDesabBtnConsultarRem(false);

					this.desabSitProc = false;
					this.exibirFocus = 'N';
										
					this.setListaGridRemessa(new ArrayList<ArquivoRecebidoDTO>());

					return "CONSULTAR_CLIENTE";
				} else {

					this.cmpi0000Bean.setCpf(this.cmpi0000Bean.getListaGridCliente().get(0).getCpf());
					this.cmpi0000Bean.setNomeRazaoFundacao(this.cmpi0000Bean.getListaGridCliente().get(0).getNomeRazao());

					this.cmpi0000Bean.setCnpj1aux(new String(this.cmpi0000Bean.getListaGridCliente().get(0).getCpfCnpjCliente().replace(".", "")));
					this.cmpi0000Bean.setCnpj2aux(new String(this.cmpi0000Bean.getListaGridCliente().get(0).getFilialCnpj()));
					this.cmpi0000Bean.setCnpj3aux(new String(this.cmpi0000Bean.getListaGridCliente().get(0).getControleCpfCnpj()));

					this.cmpi0000Bean.setDesabDataRec(false);
					this.cmpi0000Bean.setDesabDataFin(false);
					this.cmpi0000Bean.setDesabServico(false);
					this.cmpi0000Bean.setDesabCodEmpresa(false);
					this.cmpi0000Bean.setDesabSeqRemessa(false);
					this.cmpi0000Bean.setDesabResProc(true);
					this.cmpi0000Bean.setDesabBtnConsultarRem(false);
					this.setMostraBotoes0001(false);
					this.cmpi0000Bean.limparCamposDePesquisa();
					this.setIdentificacao("0");

					this.desabSitProc = false;

					this.setDataDeFiltro(new Date());
					this.setDataAteFiltro(new Date());
					this.setProdutoFiltro("");
					this.setPerfilFiltro("");
					this.setSequenciaFiltro("");
					this.setProcessamentoFiltro("");
					this.setResultadoProcessamentoFiltro("");

					this.setListaGridRemessa(new ArrayList<ArquivoRecebidoDTO>());
					this.exibirFocus = 'N';
				}

				habilitarFiltros();

			}
		} catch (PdcAdapterException p) {
			String code = p.getCode().substring(p.getCode().length()-8,p.getCode().length());
			BradescoFacesUtils.addInfoModalMessage( "(" + code + ") " + p.getMessage(), "/CMPI0001.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			limparErro();
			return null;
		} finally {
			logger.debug(this, "Finalizando a consulta dos clientes.");
		}

		return "none";

	}

	/**
	 * Validar campos.
	 *
	 * @return the string
	 */
	private String validarCampos() {

		logger.debug(this, "Iniciando a valida��o dos campos do filtro.");

		String resultado = "T";

		if (filtro == 1) {

			if (this.cmpi0000Bean.getCpf1() == null || this.cmpi0000Bean.getCpf1().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getCpf1())) {
				resultado = "F";
			}

			if (this.cmpi0000Bean.getCpf2() == null || this.cmpi0000Bean.getCpf2().equals("")) {
				resultado = "F";
			}
		}

		if (filtro == 0) {

			if (this.cmpi0000Bean.getCnpj1() == null || this.cmpi0000Bean.getCnpj1().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getCnpj1())) {
				resultado = "F";
			}

			if (this.cmpi0000Bean.getCnpj2() == null || this.cmpi0000Bean.getCnpj2().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getCnpj2())) {
				resultado = "F";
			}

			if (this.cmpi0000Bean.getCnpj3() == null || this.cmpi0000Bean.getCnpj3().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getCnpj3())) {
				resultado = "F";
			}
		}

		if (filtro == 2) {

			if (this.cmpi0000Bean.getNomeRazaoIdentificaCliente() == null || this.cmpi0000Bean.getNomeRazaoIdentificaCliente().equals("")) {
				resultado = "F";
			}
		}

		if (filtro == 3) {
			if (this.cmpi0000Bean.getBancoIdentificacaoCliente() == null || this.cmpi0000Bean.getBancoIdentificacaoCliente().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getBancoIdentificacaoCliente())) {
				resultado = "F";
			}

			if (this.cmpi0000Bean.getAgenciaIdentificacaoCliente() == null || this.cmpi0000Bean.getAgenciaIdentificacaoCliente().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getAgenciaIdentificacaoCliente())) {
				resultado = "F";
			}

			if (this.cmpi0000Bean.getContaIdentificacaoCliente() == null || this.cmpi0000Bean.getContaIdentificacaoCliente().equals("") || validarCampoMenorZero(this.cmpi0000Bean.getContaIdentificacaoCliente())) {
				resultado = "F";
			}
		}

		logger.debug(this, "Finalizando a valida��o dos campos do filtro.");

		return resultado;
	}

	/**
	 * Validar campo menor zero.
	 *
	 * @param campo the campo
	 * @return true, if validar campo menor zero
	 */
	private boolean validarCampoMenorZero(String campo) {

		logger.debug(this, "Iniciando a valida��o dos campos.");

		boolean resultado = false;

		Long valor = new Long(campo);

		if (valor.longValue() < 0) {
			resultado = true;
		}

		logger.debug(this, "Finalizando a valida��o dos campos.");

		return resultado;
	}

	/**
	 * Validar campos de pesquisa.
	 *
	 * @return true, if validar campos de pesquisa
	 */
	private boolean validarCamposDePesquisa() {
		boolean resultado = true;

		logger.debug(this, "Iniciando a valida��o dos campos de pesquisa.");

		if (this.getPerfil() != null && !this.getPerfil().equals("")) {
			if (validarCampoMenorZero(this.getPerfil())) {
				resultado = false;
			}
		}

		if (this.getSequenciaRemessa() != null && !this.getSequenciaRemessa().equals("")) {
			if (validarCampoMenorZero(this.getSequenciaRemessa())) {
				resultado = false;
			}
		}

		logger.debug(this, "Finalizando a valida��o dos campos de pesquisa.");

		return resultado;
	}

	/**
	 * Get: listaProduto.
	 *
	 * @return listaProduto
	 */
	public List<SelectItem> getListaProduto() {
		loadComboProduto();
		return listaProduto;
	}

	/**
	 * Find arquivo inconsistente.
	 *
	 * @return the list< arquivo recebido inconsistentes dt o>
	 */
	private List<ArquivoRecebidoInconsistentesDTO> findArquivoInconsistente() {

		ArquivoRecebidoDTO arquivoRemessaRecebidoDTO = getListaGridRemessa().get(getCodListaRemessaRadio());

		ArquivoRecebidoInconsistentesDTO dto = new ArquivoRecebidoInconsistentesDTO();

		dto.setCodigoControleCpfCnpj(arquivoRemessaRecebidoDTO.getCpf_controle());
		dto.setCpfCnpj(arquivoRemessaRecebidoDTO.getCpf_cliente().replace(".", ""));
		dto.setCodigoFilialCnpj(arquivoRemessaRecebidoDTO.getCpf_filial());
		dto.setFiller("");
		dto.setNumeroArquivoRemessa(arquivoRemessaRecebidoDTO.getNumeroArqRemessa());
		dto.setNumeroLoteRemessa(getSequenciaLote());
		dto.setNumeroSequenciaRemessa("0");
		dto.setPerfil(arquivoRemessaRecebidoDTO.getPerfil());
		dto.setSistemaLegado(arquivoRemessaRecebidoDTO.getCodSistemaLegado());
		dto.setTipoPesquisa("");
		dto.setRecurso("");
		dto.setDataInclusaoRemessa(getDataRecepcaoInclusao());

		List<ArquivoRecebidoInconsistentesDTO> listaArquivoInconsistente = arquivoRemessaRecebidoImpl.listaGridIncosistentes(dto);

		return listaArquivoInconsistente;
	}

	/**
	 * Carrega lista incosistentes.
	 *
	 * @return the string
	 */
	public String carregaListaIncosistentes() {

		logger.debug(this, "Iniciando o carregamento da lista de inconsistentes.");

		try {

			setQtdConsistentes(PgitUtil.formatarCampoQuantidade(String.valueOf(getListaGridEstatisticaLote().get(0).getQtdeConsitentesLong())));
			setQtdInconsistentes(PgitUtil.formatarCampoQuantidade(String.valueOf(getListaGridEstatisticaLote().get(0).getQtdeInconsitentesLong())));
			setValorConsistentes(PgitUtil.formatarCampoDeValorMonetario(getListaGridEstatisticaLote().get(0).getSomaValorConsistentesDouble()));
			setValorInconsistentes(PgitUtil.formatarCampoDeValorMonetario(getListaGridEstatisticaLote().get(0).getSomaValorInconsistentesDouble()));

			setListaGridIncosistentes(findArquivoInconsistente());

			List<SelectItem> listaSelectItem = new ArrayList<SelectItem>();
			
			for (int i = 0; i <= getListaGridIncosistentes().size(); i++) {
				listaSelectItem.add(new SelectItem(i, " "));
			}

			setListaControleIncosistentes(listaSelectItem);

			if (this.getListaGridIncosistentes().size() > 10) {
				this.setMostraBotoes0004(true);
			} else {
				this.setMostraBotoes0004(false);
			}

		} catch (PdcAdapterException p) {
			String code = p.getCode().substring(p.getCode().length()-8,p.getCode().length());
			BradescoFacesUtils.addInfoModalMessage( "(" + code + ") " + p.getMessage(), "/CMPI0003.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			return null;
		}

		logger.debug(this, "Finalizando o carregamento da lista de inconsistentes.");

		return "AVANCAR_INCONSISTENTES";
	}

	/**
	 * Pesquisar inconsistencia.
	 *
	 * @param event the event
	 */
	public void pesquisarInconsistencia(ActionEvent event) {
		logger.debug(this, "Iniciando o carregamento da lista de inconsistencias.");
		setListaGridIncosistentes(findArquivoInconsistente());
		logger.debug(this, "Finalizou o carregamento da lista de ocorr�ncias.");
	}

	/**
	 * Find lista ocorrencia.
	 *
	 * @return the list< ocorrencia dt o>
	 */
	private List<OcorrenciaDTO> findListaOcorrencia() {

		ArquivoRecebidoDTO arquivoRemessaRecebidoDTO = getListaGridRemessa().get(getCodListaRemessaRadio());

		OcorrenciaDTO ocorrenciaDTO = new OcorrenciaDTO();

		ocorrenciaDTO.setControleCpfCnpj(arquivoRemessaRecebidoDTO.getCpf_controle());
		ocorrenciaDTO.setCpfCnpj(arquivoRemessaRecebidoDTO.getCpf_cliente().replace(".", ""));
		ocorrenciaDTO.setFilialCnpj(arquivoRemessaRecebidoDTO.getCpf_filial());
		ocorrenciaDTO.setFiller("");
		ocorrenciaDTO.setNumeroArquivoRemessa(arquivoRemessaRecebidoDTO.getNumeroArqRemessa());
		ocorrenciaDTO.setNumeroLoteRemessa(getSequenciaLote());
		ocorrenciaDTO.setNumeroSequenciaRemessa(getSequenciaRegistros());
		ocorrenciaDTO.setPerfil(arquivoRemessaRecebidoDTO.getPerfil());
		ocorrenciaDTO.setSistemaLegado(arquivoRemessaRecebidoDTO.getCodSistemaLegado());
		ocorrenciaDTO.setTipoPesquisa("L");
		ocorrenciaDTO.setRecurso("");
		ocorrenciaDTO.setDataRecepcaoInclusao(getDataRecepcaoInclusao());

		List<OcorrenciaDTO> listaOcorrencias = arquivoRemessaRecebidoImpl.listaOcorrencia(ocorrenciaDTO);

		return listaOcorrencias;
	}

	/**
	 * Carrega lista ocorrencias.
	 *
	 * @return the string
	 */
	public String carregaListaOcorrencias() {

		logger.debug(this, "Iniciando o carregamento da lista de ocorr�ncias.");

		try {
			ArquivoRecebidoInconsistentesDTO arquivoRemessaRecebidoInconsistentesDTO = getListaGridIncosistentes().get(getHiddenRadioInconsistente());

			LoteDTO loteDTO = (LoteDTO) getListaLote().get(getHdnCodListaLote());

			setModalidadePagamento(loteDTO.getDescricaoModalidadePagto());

			setSequenciaRegistros(arquivoRemessaRecebidoInconsistentesDTO.getNumeroSequenciaRegistroRemessa());
			setInstrucaoMovimento(arquivoRemessaRecebidoInconsistentesDTO.getDescricaoInstrucaoMovimento());
			setNumeroPagamento(arquivoRemessaRecebidoInconsistentesDTO.getControlePagamento());

			// fazendo a carga inicial
			setListaGridOcorrencia(findListaOcorrencia());

			if (this.getListaGridOcorrencia().size() > 10) {
				this.setMostraBotoes0005(true);
			} else {
				this.setMostraBotoes0005(false);
			}

		} catch (PdcAdapterException p) {
			String code = p.getCode().substring(p.getCode().length()-8,p.getCode().length());
			BradescoFacesUtils.addInfoModalMessage( "(" + code + ") " + p.getMessage(), "/CMPI0004.jsf", BradescoViewExceptionActionType.PATH, false);
			logger.debug(this, "Erro: " + p.getCode() + " Descri��o: " + p.getMessage() + " Classe: " + p.getClass() + " StackTrace: " + p.getStackTrace());
			return null;

		} finally {
			logger.debug(this, "Finalizando o carregamento da lista de ocorr�ncias.");
		}

		return "AVANCAR_OCORRENCIA";

	}

	/**
	 * Pesquisar ocorrencias.
	 *
	 * @param event the event
	 */
	public void pesquisarOcorrencias(ActionEvent event) {
		logger.debug(this, "Iniciando o carregamento da lista de ocorr�ncias.");
		setListaGridOcorrencia(findListaOcorrencia());
		logger.debug(this, "Finalizou o carregamento da lista de ocorr�ncias.");
	}

	/**
	 * Get: cpfFiltro.
	 *
	 * @return cpfFiltro
	 */
	public String getCpfFiltro() {
		return cpfFiltro;
	}

	/**
	 * Set: cpfFiltro.
	 *
	 * @param cpfFiltro the cpf filtro
	 */
	public void setCpfFiltro(String cpfFiltro) {
		this.cpfFiltro = cpfFiltro;
	}

	/**
	 * Get: perfilFiltro.
	 *
	 * @return perfilFiltro
	 */
	public String getPerfilFiltro() {
		return perfilFiltro;
	}

	/**
	 * Set: perfilFiltro.
	 *
	 * @param perfilFiltro the perfil filtro
	 */
	public void setPerfilFiltro(String perfilFiltro) {
		this.perfilFiltro = perfilFiltro;
	}

	/**
	 * Get: processamentoFiltro.
	 *
	 * @return processamentoFiltro
	 */
	public String getProcessamentoFiltro() {
		return processamentoFiltro;
	}

	/**
	 * Set: processamentoFiltro.
	 *
	 * @param processamentoFiltro the processamento filtro
	 */
	public void setProcessamentoFiltro(String processamentoFiltro) {
		this.processamentoFiltro = processamentoFiltro;
	}

	/**
	 * Get: produtoFiltro.
	 *
	 * @return produtoFiltro
	 */
	public String getProdutoFiltro() {
		return produtoFiltro;
	}

	/**
	 * Set: produtoFiltro.
	 *
	 * @param produtoFiltro the produto filtro
	 */
	public void setProdutoFiltro(String produtoFiltro) {
		this.produtoFiltro = produtoFiltro;
	}

	/**
	 * Get: resultadoProcessamentoFiltro.
	 *
	 * @return resultadoProcessamentoFiltro
	 */
	public String getResultadoProcessamentoFiltro() {
		return resultadoProcessamentoFiltro;
	}

	/**
	 * Set: resultadoProcessamentoFiltro.
	 *
	 * @param resultadoProcessamentoFiltro the resultado processamento filtro
	 */
	public void setResultadoProcessamentoFiltro(String resultadoProcessamentoFiltro) {
		this.resultadoProcessamentoFiltro = resultadoProcessamentoFiltro;
	}

	/**
	 * Get: sequenciaFiltro.
	 *
	 * @return sequenciaFiltro
	 */
	public String getSequenciaFiltro() {
		return sequenciaFiltro;
	}

	/**
	 * Set: sequenciaFiltro.
	 *
	 * @param sequenciaFiltro the sequencia filtro
	 */
	public void setSequenciaFiltro(String sequenciaFiltro) {
		this.sequenciaFiltro = sequenciaFiltro;
	}

	/**
	 * Get: listaGridRemessa.
	 *
	 * @return listaGridRemessa
	 */
	public List<ArquivoRecebidoDTO> getListaGridRemessa() {

		return listaGridRemessa;
	}

	/**
	 * Set: listaGridRemessa.
	 *
	 * @param listaGridRemessa the lista grid remessa
	 */
	public void setListaGridRemessa(List<ArquivoRecebidoDTO> listaGridRemessa) {
		this.listaGridRemessa = listaGridRemessa;
	}

	/**
	 * Get: arquivoRemessaRecebidoImpl.
	 *
	 * @return arquivoRemessaRecebidoImpl
	 */
	public IArquivoRecebidoService getArquivoRemessaRecebidoImpl() {
		return arquivoRemessaRecebidoImpl;
	}

	/**
	 * Set: arquivoRemessaRecebidoImpl.
	 *
	 * @param arquivoRemessaRecebidoImpl the arquivo remessa recebido impl
	 */
	public void setArquivoRemessaRecebidoImpl(IArquivoRecebidoService arquivoRemessaRecebidoImpl) {
		this.arquivoRemessaRecebidoImpl = arquivoRemessaRecebidoImpl;
	}

	/**
	 * Get: listaGridIncosistentes.
	 *
	 * @return listaGridIncosistentes
	 */
	public List<ArquivoRecebidoInconsistentesDTO> getListaGridIncosistentes() {
		return listaGridIncosistentes;
	}

	/**
	 * Set: listaGridIncosistentes.
	 *
	 * @param listaGridIncosistentes the lista grid incosistentes
	 */
	public void setListaGridIncosistentes(List<ArquivoRecebidoInconsistentesDTO> listaGridIncosistentes) {
		this.listaGridIncosistentes = listaGridIncosistentes;
	}

	/**
	 * Get: listaGridOcorrencia.
	 *
	 * @return listaGridOcorrencia
	 */
	public List<OcorrenciaDTO> getListaGridOcorrencia() {
		return listaGridOcorrencia;
	}

	/**
	 * Set: listaGridOcorrencia.
	 *
	 * @param listaGridOcorrencia the lista grid ocorrencia
	 */
	public void setListaGridOcorrencia(List<OcorrenciaDTO> listaGridOcorrencia) {
		this.listaGridOcorrencia = listaGridOcorrencia;
	}

	/**
	 * Get: listaCodigo.
	 *
	 * @return listaCodigo
	 */
	public List<SelectItem> getListaCodigo() {
		return listaCodigo;
	}

	/**
	 * Set: listaCodigo.
	 *
	 * @param listaCodigo the lista codigo
	 */
	public void setListaCodigo(List<SelectItem> listaCodigo) {
		this.listaCodigo = listaCodigo;
	}

	/**
	 * Get: listaControleIncosistentes.
	 *
	 * @return listaControleIncosistentes
	 */
	public List<SelectItem> getListaControleIncosistentes() {
		return listaControleIncosistentes;
	}

	/**
	 * Set: listaControleIncosistentes.
	 *
	 * @param listaControleIncosistentes the lista controle incosistentes
	 */
	public void setListaControleIncosistentes(List<SelectItem> listaControleIncosistentes) {
		this.listaControleIncosistentes = listaControleIncosistentes;
	}

	/**
	 * Get: qtdConsistentes.
	 *
	 * @return qtdConsistentes
	 */
	public String getQtdConsistentes() {
		return qtdConsistentes;
	}

	/**
	 * Set: qtdConsistentes.
	 *
	 * @param qtdConsistentes the qtd consistentes
	 */
	public void setQtdConsistentes(String qtdConsistentes) {
		this.qtdConsistentes = qtdConsistentes;
	}

	/**
	 * Get: qtdInconsistentes.
	 *
	 * @return qtdInconsistentes
	 */
	public String getQtdInconsistentes() {
		return qtdInconsistentes;
	}

	/**
	 * Set: qtdInconsistentes.
	 *
	 * @param qtdInconsistentes the qtd inconsistentes
	 */
	public void setQtdInconsistentes(String qtdInconsistentes) {
		this.qtdInconsistentes = qtdInconsistentes;
	}

	/**
	 * Get: valorConsistentes.
	 *
	 * @return valorConsistentes
	 */
	public String getValorConsistentes() {
		return valorConsistentes;
	}

	/**
	 * Set: valorConsistentes.
	 *
	 * @param valorConsistentes the valor consistentes
	 */
	public void setValorConsistentes(String valorConsistentes) {
		this.valorConsistentes = valorConsistentes;
	}

	/**
	 * Get: codListaIncosistentes.
	 *
	 * @return codListaIncosistentes
	 */
	public String getCodListaIncosistentes() {
		return codListaIncosistentes;
	}

	/**
	 * Set: codListaIncosistentes.
	 *
	 * @param codListaIncosistentes the cod lista incosistentes
	 */
	public void setCodListaIncosistentes(String codListaIncosistentes) {
		this.codListaIncosistentes = codListaIncosistentes;
	}

	/**
	 * Get: hiddenRadioInconsistente.
	 *
	 * @return hiddenRadioInconsistente
	 */
	public int getHiddenRadioInconsistente() {
		return hiddenRadioInconsistente;
	}

	/**
	 * Set: hiddenRadioInconsistente.
	 *
	 * @param hiddenRadioInconsistente the hidden radio inconsistente
	 */
	public void setHiddenRadioInconsistente(int hiddenRadioInconsistente) {
		this.hiddenRadioInconsistente = hiddenRadioInconsistente;
	}

	/**
	 * Get: listaGridCliente.
	 *
	 * @return listaGridCliente
	 */
	public List<ListarClientesDTO> getListaGridCliente() {
		return listaGridCliente;
	}

	/**
	 * Set: listaGridCliente.
	 *
	 * @param listaGridCliente the lista grid cliente
	 */
	public void setListaGridCliente(List<ListarClientesDTO> listaGridCliente) {
		this.listaGridCliente = listaGridCliente;
	}

	/**
	 * Get: hiddenRadioCliente.
	 *
	 * @return hiddenRadioCliente
	 */
	public int getHiddenRadioCliente() {
		return hiddenRadioCliente;
	}

	/**
	 * Set: hiddenRadioCliente.
	 *
	 * @param hiddenRadioCliente the hidden radio cliente
	 */
	public void setHiddenRadioCliente(int hiddenRadioCliente) {
		this.hiddenRadioCliente = hiddenRadioCliente;
	}

	/**
	 * Get: hiddenRadioFiltroArgumento.
	 *
	 * @return hiddenRadioFiltroArgumento
	 */
	public int getHiddenRadioFiltroArgumento() {
		return hiddenRadioFiltroArgumento;
	}

	/**
	 * Set: hiddenRadioFiltroArgumento.
	 *
	 * @param hiddenRadioFiltroArgumento the hidden radio filtro argumento
	 */
	public void setHiddenRadioFiltroArgumento(int hiddenRadioFiltroArgumento) {
		this.hiddenRadioFiltroArgumento = hiddenRadioFiltroArgumento;
	}

	/**
	 * Get: listaControleCliente.
	 *
	 * @return listaControleCliente
	 */
	public List<SelectItem> getListaControleCliente() {
		return listaControleCliente;
	}

	/**
	 * Set: listaControleCliente.
	 *
	 * @param listaControleCliente the lista controle cliente
	 */
	public void setListaControleCliente(List<SelectItem> listaControleCliente) {
		this.listaControleCliente = listaControleCliente;
	}

	/**
	 * Get: agenciaIdentificacaoCliente.
	 *
	 * @return agenciaIdentificacaoCliente
	 */
	public String getAgenciaIdentificacaoCliente() {
		return agenciaIdentificacaoCliente;
	}

	/**
	 * Set: agenciaIdentificacaoCliente.
	 *
	 * @param agenciaIdentificacaoCliente the agencia identificacao cliente
	 */
	public void setAgenciaIdentificacaoCliente(String agenciaIdentificacaoCliente) {
		this.agenciaIdentificacaoCliente = agenciaIdentificacaoCliente;
	}

	/**
	 * Get: bancoIdentificacaoCliente.
	 *
	 * @return bancoIdentificacaoCliente
	 */
	public String getBancoIdentificacaoCliente() {
		return bancoIdentificacaoCliente;
	}

	/**
	 * Set: bancoIdentificacaoCliente.
	 *
	 * @param bancoIdentificacaoCliente the banco identificacao cliente
	 */
	public void setBancoIdentificacaoCliente(String bancoIdentificacaoCliente) {
		this.bancoIdentificacaoCliente = bancoIdentificacaoCliente;
	}

	/**
	 * Get: contaIdentificacaoCliente.
	 *
	 * @return contaIdentificacaoCliente
	 */
	public String getContaIdentificacaoCliente() {
		return contaIdentificacaoCliente;
	}

	/**
	 * Set: contaIdentificacaoCliente.
	 *
	 * @param contaIdentificacaoCliente the conta identificacao cliente
	 */
	public void setContaIdentificacaoCliente(String contaIdentificacaoCliente) {
		this.contaIdentificacaoCliente = contaIdentificacaoCliente;
	}

	/**
	 * Get: cpfCnpjIdentificaoCliente.
	 *
	 * @return cpfCnpjIdentificaoCliente
	 */
	public String getCpfCnpjIdentificaoCliente() {
		return cpfCnpjIdentificaoCliente;
	}

	/**
	 * Set: cpfCnpjIdentificaoCliente.
	 *
	 * @param cpfCnpjIdentificaoCliente the cpf cnpj identificao cliente
	 */
	public void setCpfCnpjIdentificaoCliente(String cpfCnpjIdentificaoCliente) {
		this.cpfCnpjIdentificaoCliente = cpfCnpjIdentificaoCliente;
	}

	/**
	 * Get: dataNascimentoFundacao.
	 *
	 * @return dataNascimentoFundacao
	 */
	public Date getDataNascimentoFundacao() {
		return dataNascimentoFundacao;
	}

	/**
	 * Set: dataNascimentoFundacao.
	 *
	 * @param dataNascimentoFundacao the data nascimento fundacao
	 */
	public void setDataNascimentoFundacao(Date dataNascimentoFundacao) {
		this.dataNascimentoFundacao = dataNascimentoFundacao;
	}

	/**
	 * Get: filtroIdentificacaoCliente.
	 *
	 * @return filtroIdentificacaoCliente
	 */
	public String getFiltroIdentificacaoCliente() {
		return filtroIdentificacaoCliente;
	}

	/**
	 * Set: filtroIdentificacaoCliente.
	 *
	 * @param filtroIdentificacaoCliente the filtro identificacao cliente
	 */
	public void setFiltroIdentificacaoCliente(String filtroIdentificacaoCliente) {
		this.filtroIdentificacaoCliente = filtroIdentificacaoCliente;
	}

	/**
	 * Get: nomeRazaoIdentificaCliente.
	 *
	 * @return nomeRazaoIdentificaCliente
	 */
	public String getNomeRazaoIdentificaCliente() {
		return nomeRazaoIdentificaCliente;
	}

	/**
	 * Set: nomeRazaoIdentificaCliente.
	 *
	 * @param nomeRazaoIdentificaCliente the nome razao identifica cliente
	 */
	public void setNomeRazaoIdentificaCliente(String nomeRazaoIdentificaCliente) {
		this.nomeRazaoIdentificaCliente = nomeRazaoIdentificaCliente;
	}

	/**
	 * Get: qtdRegistrosRemessa.
	 *
	 * @return qtdRegistrosRemessa
	 */
	public String getQtdRegistrosRemessa() {
		return qtdRegistrosRemessa;
	}

	/**
	 * Set: qtdRegistrosRemessa.
	 *
	 * @param qtdRegistrosRemessa the qtd registros remessa
	 */
	public void setQtdRegistrosRemessa(String qtdRegistrosRemessa) {
		this.qtdRegistrosRemessa = qtdRegistrosRemessa;
	}

	/**
	 * Get: validaCpf.
	 *
	 * @return validaCpf
	 */
	public String getValidaCpf() {
		return validaCpf;
	}

	/**
	 * Set: validaCpf.
	 *
	 * @param validaCpf the valida cpf
	 */
	public void setValidaCpf(String validaCpf) {
		this.validaCpf = validaCpf;
	}

	/**
	 * Get: validaCpfCliente.
	 *
	 * @return validaCpfCliente
	 */
	public String getValidaCpfCliente() {
		return validaCpfCliente;
	}

	/**
	 * Set: validaCpfCliente.
	 *
	 * @param validaCpfCliente the valida cpf cliente
	 */
	public void setValidaCpfCliente(String validaCpfCliente) {
		this.validaCpfCliente = validaCpfCliente;
	}

	/**
	 * Get: cmpi0000Bean.
	 *
	 * @return cmpi0000Bean
	 */
	public Cmpi0000Bean getCmpi0000Bean() {
		return cmpi0000Bean;
	}

	/**
	 * Set: cmpi0000Bean.
	 *
	 * @param cmpi0000Bean the cmpi0000 bean
	 */
	public void setCmpi0000Bean(Cmpi0000Bean cmpi0000Bean) {
		this.cmpi0000Bean = cmpi0000Bean;
	}

	/**
	 * Is mostra botoes0001.
	 *
	 * @return true, if is mostra botoes0001
	 */
	public boolean isMostraBotoes0001() {
		return mostraBotoes0001;
	}

	/**
	 * Set: mostraBotoes0001.
	 *
	 * @param mostraBotoes0001 the mostra botoes0001
	 */
	public void setMostraBotoes0001(boolean mostraBotoes0001) {
		this.mostraBotoes0001 = mostraBotoes0001;
	}

	/**
	 * Is mostra botoes0002.
	 *
	 * @return true, if is mostra botoes0002
	 */
	public boolean isMostraBotoes0002() {
		return mostraBotoes0002;
	}

	/**
	 * Set: mostraBotoes0002.
	 *
	 * @param mostraBotoes0002 the mostra botoes0002
	 */
	public void setMostraBotoes0002(boolean mostraBotoes0002) {
		this.mostraBotoes0002 = mostraBotoes0002;
	}

	/**
	 * Is mostra botoes0003.
	 *
	 * @return true, if is mostra botoes0003
	 */
	public boolean isMostraBotoes0003() {
		return mostraBotoes0003;
	}

	/**
	 * Set: mostraBotoes0003.
	 *
	 * @param mostraBotoes0003 the mostra botoes0003
	 */
	public void setMostraBotoes0003(boolean mostraBotoes0003) {
		this.mostraBotoes0003 = mostraBotoes0003;
	}

	/**
	 * Is mostra botoes0004.
	 *
	 * @return true, if is mostra botoes0004
	 */
	public boolean isMostraBotoes0004() {
		return mostraBotoes0004;
	}

	/**
	 * Set: mostraBotoes0004.
	 *
	 * @param mostraBotoes0004 the mostra botoes0004
	 */
	public void setMostraBotoes0004(boolean mostraBotoes0004) {
		this.mostraBotoes0004 = mostraBotoes0004;
	}

	/**
	 * Is mostra botoes0005.
	 *
	 * @return true, if is mostra botoes0005
	 */
	public boolean isMostraBotoes0005() {
		return mostraBotoes0005;
	}

	/**
	 * Set: mostraBotoes0005.
	 *
	 * @param mostraBotoes0005 the mostra botoes0005
	 */
	public void setMostraBotoes0005(boolean mostraBotoes0005) {
		this.mostraBotoes0005 = mostraBotoes0005;
	}

	/**
	 * Is mostra botoes0006.
	 *
	 * @return true, if is mostra botoes0006
	 */
	public boolean isMostraBotoes0006() {
		return mostraBotoes0006;
	}

	/**
	 * Set: mostraBotoes0006.
	 *
	 * @param mostraBotoes0006 the mostra botoes0006
	 */
	public void setMostraBotoes0006(boolean mostraBotoes0006) {
		this.mostraBotoes0006 = mostraBotoes0006;
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: dataRecepcaoInclusao.
	 *
	 * @return dataRecepcaoInclusao
	 */
	public String getDataRecepcaoInclusao() {
		return dataRecepcaoInclusao;
	}

	/**
	 * Set: dataRecepcaoInclusao.
	 *
	 * @param dataRecepcaoInclusao the data recepcao inclusao
	 */
	public void setDataRecepcaoInclusao(String dataRecepcaoInclusao) {
		this.dataRecepcaoInclusao = dataRecepcaoInclusao;
	}

	/**
	 * Get: controlecpf.
	 *
	 * @return controlecpf
	 */
	public String getControlecpf() {
		return controlecpf;
	}

	/**
	 * Set: controlecpf.
	 *
	 * @param controlecpf the controlecpf
	 */
	public void setControlecpf(String controlecpf) {
		this.controlecpf = controlecpf;
	}

	/**
	 * Get: cpfcnpj.
	 *
	 * @return cpfcnpj
	 */
	public String getCpfcnpj() {
		return cpfcnpj;
	}

	/**
	 * Set: cpfcnpj.
	 *
	 * @param cpfcnpj the cpfcnpj
	 */
	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}

	/**
	 * Get: filial.
	 *
	 * @return filial
	 */
	public String getFilial() {
		return filial;
	}

	/**
	 * Set: filial.
	 *
	 * @param filial the filial
	 */
	public void setFilial(String filial) {
		this.filial = filial;
	}

	/**
	 * Get: numeroArquivoRemessa.
	 *
	 * @return numeroArquivoRemessa
	 */
	public String getNumeroArquivoRemessa() {
		return numeroArquivoRemessa;
	}

	/**
	 * Set: numeroArquivoRemessa.
	 *
	 * @param numeroArquivoRemessa the numero arquivo remessa
	 */
	public void setNumeroArquivoRemessa(String numeroArquivoRemessa) {
		this.numeroArquivoRemessa = numeroArquivoRemessa;
	}

	/**
	 * Get: validaCnpj.
	 *
	 * @return validaCnpj
	 */
	public String getValidaCnpj() {
		return validaCnpj;
	}

	/**
	 * Set: validaCnpj.
	 *
	 * @param validaCnpj the valida cnpj
	 */
	public void setValidaCnpj(String validaCnpj) {
		this.validaCnpj = validaCnpj;
	}

	/**
	 * Get: bancoAgenciaConta.
	 *
	 * @return bancoAgenciaConta
	 */
	public String getBancoAgenciaConta() {
		return bancoAgenciaConta;
	}

	/**
	 * Set: bancoAgenciaConta.
	 *
	 * @param bancoAgenciaConta the banco agencia conta
	 */
	public void setBancoAgenciaConta(String bancoAgenciaConta) {
		this.bancoAgenciaConta = bancoAgenciaConta;
	}

	/**
	 * Get: listaGridEstatisticaLote.
	 *
	 * @return listaGridEstatisticaLote
	 */
	public List<EstatisticaLoteDTO> getListaGridEstatisticaLote() {
		return listaGridEstatisticaLote;
	}

	/**
	 * Set: listaGridEstatisticaLote.
	 *
	 * @param listaGridEstatisticaLote the lista grid estatistica lote
	 */
	public void setListaGridEstatisticaLote(List<EstatisticaLoteDTO> listaGridEstatisticaLote) {
		this.listaGridEstatisticaLote = listaGridEstatisticaLote;
	}

	/**
	 * Get: modalidadePagamentoLote.
	 *
	 * @return modalidadePagamentoLote
	 */
	public String getModalidadePagamentoLote() {
		return modalidadePagamentoLote;
	}

	/**
	 * Set: modalidadePagamentoLote.
	 *
	 * @param modalidadePagamentoLote the modalidade pagamento lote
	 */
	public void setModalidadePagamentoLote(String modalidadePagamentoLote) {
		this.modalidadePagamentoLote = modalidadePagamentoLote;
	}

	/**
	 * Get: resultadoProcessamentoLote.
	 *
	 * @return resultadoProcessamentoLote
	 */
	public String getResultadoProcessamentoLote() {
		return resultadoProcessamentoLote;
	}

	/**
	 * Set: resultadoProcessamentoLote.
	 *
	 * @param resultadoProcessamentoLote the resultado processamento lote
	 */
	public void setResultadoProcessamentoLote(String resultadoProcessamentoLote) {
		this.resultadoProcessamentoLote = resultadoProcessamentoLote;
	}

	/**
	 * Get: sequenciaLote.
	 *
	 * @return sequenciaLote
	 */
	public String getSequenciaLote() {
		return sequenciaLote;
	}

	/**
	 * Set: sequenciaLote.
	 *
	 * @param sequenciaLote the sequencia lote
	 */
	public void setSequenciaLote(String sequenciaLote) {
		this.sequenciaLote = sequenciaLote;
	}

	/**
	 * Get: situacaoProcessamentoLote.
	 *
	 * @return situacaoProcessamentoLote
	 */
	public String getSituacaoProcessamentoLote() {
		return situacaoProcessamentoLote;
	}

	/**
	 * Set: situacaoProcessamentoLote.
	 *
	 * @param situacaoProcessamentoLote the situacao processamento lote
	 */
	public void setSituacaoProcessamentoLote(String situacaoProcessamentoLote) {
		this.situacaoProcessamentoLote = situacaoProcessamentoLote;
	}

	/**
	 * Get: situacaoProcessamentoRemessa.
	 *
	 * @return situacaoProcessamentoRemessa
	 */
	public String getSituacaoProcessamentoRemessa() {
		return situacaoProcessamentoRemessa;
	}

	/**
	 * Set: situacaoProcessamentoRemessa.
	 *
	 * @param situacaoProcessamentoRemessa the situacao processamento remessa
	 */
	public void setSituacaoProcessamentoRemessa(String situacaoProcessamentoRemessa) {
		this.situacaoProcessamentoRemessa = situacaoProcessamentoRemessa;
	}

	/**
	 * Get: hdnCodListaLote.
	 *
	 * @return hdnCodListaLote
	 */
	public int getHdnCodListaLote() {
		return hdnCodListaLote;
	}

	/**
	 * Set: hdnCodListaLote.
	 *
	 * @param hdnCodListaLote the hdn cod lista lote
	 */
	public void setHdnCodListaLote(int hdnCodListaLote) {
		this.hdnCodListaLote = hdnCodListaLote;
	}

	/**
	 * Get: codListaRemessaRadio.
	 *
	 * @return codListaRemessaRadio
	 */
	public int getCodListaRemessaRadio() {
		return codListaRemessaRadio;
	}

	/**
	 * Set: codListaRemessaRadio.
	 *
	 * @param codListaRemessaRadio the cod lista remessa radio
	 */
	public void setCodListaRemessaRadio(int codListaRemessaRadio) {
		this.codListaRemessaRadio = codListaRemessaRadio;
	}

	/**
	 * Get: filtro.
	 *
	 * @return filtro
	 */
	public int getFiltro() {
		return filtro;
	}

	/**
	 * Set: filtro.
	 *
	 * @param filtro the filtro
	 */
	public void setFiltro(int filtro) {
		this.filtro = filtro;
	}

	/**
	 * Get: listaProcessamento.
	 *
	 * @return listaProcessamento
	 */
	public List<SelectItem> getListaProcessamento() {
		return listaProcessamento;
	}

	/**
	 * Set: listaProcessamento.
	 *
	 * @param listaProcessamento the lista processamento
	 */
	public void setListaProcessamento(List<SelectItem> listaProcessamento) {
		this.listaProcessamento = listaProcessamento;
	}

	/**
	 * Set: listaProduto.
	 *
	 * @param listaProduto the lista produto
	 */
	public void setListaProduto(List<SelectItem> listaProduto) {
		this.listaProduto = listaProduto;
	}

	/**
	 * Get: listaProdutoAux.
	 *
	 * @return listaProdutoAux
	 */
	public List<ComboProdutoDTO> getListaProdutoAux() {
		return listaProdutoAux;
	}

	/**
	 * Set: listaProdutoAux.
	 *
	 * @param listaProdutoAux the lista produto aux
	 */
	public void setListaProdutoAux(List<ComboProdutoDTO> listaProdutoAux) {
		this.listaProdutoAux = listaProdutoAux;
	}

	/**
	 * Get: listaResultadoProcessamento.
	 *
	 * @return listaResultadoProcessamento
	 */
	public List<SelectItem> getListaResultadoProcessamento() {
		return listaResultadoProcessamento;
	}

	/**
	 * Set: listaResultadoProcessamento.
	 *
	 * @param listaResultadoProcessamento the lista resultado processamento
	 */
	public void setListaResultadoProcessamento(List<SelectItem> listaResultadoProcessamento) {
		this.listaResultadoProcessamento = listaResultadoProcessamento;
	}

	/**
	 * Get: dataAteFiltro.
	 *
	 * @return dataAteFiltro
	 */
	public Date getDataAteFiltro() {
		if (dataAteFiltro == null) {
			dataAteFiltro = new Date();
		}
		return dataAteFiltro;
	}

	/**
	 * Set: dataAteFiltro.
	 *
	 * @param dataAteFiltro the data ate filtro
	 */
	public void setDataAteFiltro(Date dataAteFiltro) {
		this.dataAteFiltro = dataAteFiltro;
	}

	/**
	 * Get: dataDeFiltro.
	 *
	 * @return dataDeFiltro
	 */
	public Date getDataDeFiltro() {
		if (dataDeFiltro == null) {
			dataDeFiltro = new Date();
		}
		return dataDeFiltro;
	}

	/**
	 * Set: dataDeFiltro.
	 *
	 * @param dataDeFiltro the data de filtro
	 */
	public void setDataDeFiltro(Date dataDeFiltro) {
		this.dataDeFiltro = dataDeFiltro;
	}

	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * Set: agencia.
	 *
	 * @param agencia the agencia
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * Set: banco.
	 *
	 * @param banco the banco
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}

	/**
	 * Get: canalTransmissao.
	 *
	 * @return canalTransmissao
	 */
	public String getCanalTransmissao() {
		return canalTransmissao;
	}

	/**
	 * Set: canalTransmissao.
	 *
	 * @param canalTransmissao the canal transmissao
	 */
	public void setCanalTransmissao(String canalTransmissao) {
		this.canalTransmissao = canalTransmissao;
	}

	/**
	 * Get: codigoLancamento.
	 *
	 * @return codigoLancamento
	 */
	public String getCodigoLancamento() {
		return codigoLancamento;
	}

	/**
	 * Set: codigoLancamento.
	 *
	 * @param codigoLancamento the codigo lancamento
	 */
	public void setCodigoLancamento(String codigoLancamento) {
		this.codigoLancamento = codigoLancamento;
	}

	/**
	 * Get: condicaoLote.
	 *
	 * @return condicaoLote
	 */
	public String getCondicaoLote() {
		return condicaoLote;
	}

	/**
	 * Set: condicaoLote.
	 *
	 * @param condicaoLote the condicao lote
	 */
	public void setCondicaoLote(String condicaoLote) {
		this.condicaoLote = condicaoLote;
	}

	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * Get: cpf.
	 *
	 * @return cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Set: cpf.
	 *
	 * @param cpf the cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Get: dataAte.
	 *
	 * @return dataAte
	 */
	public String getDataAte() {
		return dataAte;
	}

	/**
	 * Set: dataAte.
	 *
	 * @param dataAte the data ate
	 */
	public void setDataAte(String dataAte) {
		this.dataAte = dataAte;
	}

	/**
	 * Get: dataDe.
	 *
	 * @return dataDe
	 */
	public String getDataDe() {
		return dataDe;
	}

	/**
	 * Set: dataDe.
	 *
	 * @param dataDe the data de
	 */
	public void setDataDe(String dataDe) {
		this.dataDe = dataDe;
	}

	/**
	 * Get: dataHoraRecepcao.
	 *
	 * @return dataHoraRecepcao
	 */
	public String getDataHoraRecepcao() {
		return dataHoraRecepcao;
	}

	/**
	 * Set: dataHoraRecepcao.
	 *
	 * @param dataHoraRecepcao the data hora recepcao
	 */
	public void setDataHoraRecepcao(String dataHoraRecepcao) {
		this.dataHoraRecepcao = dataHoraRecepcao;
	}

	/**
	 * Get: instrucaoMovimento.
	 *
	 * @return instrucaoMovimento
	 */
	public String getInstrucaoMovimento() {
		return instrucaoMovimento;
	}

	/**
	 * Set: instrucaoMovimento.
	 *
	 * @param instrucaoMovimento the instrucao movimento
	 */
	public void setInstrucaoMovimento(String instrucaoMovimento) {
		this.instrucaoMovimento = instrucaoMovimento;
	}

	/**
	 * Get: layout.
	 *
	 * @return layout
	 */
	public String getLayout() {
		return layout;
	}

	/**
	 * Set: layout.
	 *
	 * @param layout the layout
	 */
	public void setLayout(String layout) {
		this.layout = layout;
	}

	/**
	 * Get: modalidadePagamento.
	 *
	 * @return modalidadePagamento
	 */
	public String getModalidadePagamento() {
		return modalidadePagamento;
	}

	/**
	 * Set: modalidadePagamento.
	 *
	 * @param modalidadePagamento the modalidade pagamento
	 */
	public void setModalidadePagamento(String modalidadePagamento) {
		this.modalidadePagamento = modalidadePagamento;
	}

	/**
	 * Get: nomeRazao.
	 *
	 * @return nomeRazao
	 */
	public String getNomeRazao() {
		return nomeRazao;
	}

	/**
	 * Set: nomeRazao.
	 *
	 * @param nomeRazao the nome razao
	 */
	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}

	/**
	 * Get: numeroPagamento.
	 *
	 * @return numeroPagamento
	 */
	public String getNumeroPagamento() {
		return numeroPagamento;
	}

	/**
	 * Set: numeroPagamento.
	 *
	 * @param numeroPagamento the numero pagamento
	 */
	public void setNumeroPagamento(String numeroPagamento) {
		this.numeroPagamento = numeroPagamento;
	}

	/**
	 * Get: perfil.
	 *
	 * @return perfil
	 */
	public String getPerfil() {
		return perfil;
	}

	/**
	 * Set: perfil.
	 *
	 * @param perfil the perfil
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	/**
	 * Get: processamento.
	 *
	 * @return processamento
	 */
	public String getProcessamento() {
		return processamento;
	}

	/**
	 * Set: processamento.
	 *
	 * @param processamento the processamento
	 */
	public void setProcessamento(String processamento) {
		this.processamento = processamento;
	}

	/**
	 * Get: processamentoLote.
	 *
	 * @return processamentoLote
	 */
	public String getProcessamentoLote() {
		return processamentoLote;
	}

	/**
	 * Set: processamentoLote.
	 *
	 * @param processamentoLote the processamento lote
	 */
	public void setProcessamentoLote(String processamentoLote) {
		this.processamentoLote = processamentoLote;
	}

	/**
	 * Get: produto.
	 *
	 * @return produto
	 */
	public String getProduto() {
		return produto;
	}

	/**
	 * Set: produto.
	 *
	 * @param produto the produto
	 */
	public void setProduto(String produto) {
		this.produto = produto;
	}

	/**
	 * Get: qtdRegistrosLote.
	 *
	 * @return qtdRegistrosLote
	 */
	public String getQtdRegistrosLote() {
		return qtdRegistrosLote;
	}

	/**
	 * Set: qtdRegistrosLote.
	 *
	 * @param qtdRegistrosLote the qtd registros lote
	 */
	public void setQtdRegistrosLote(String qtdRegistrosLote) {
		this.qtdRegistrosLote = qtdRegistrosLote;
	}

	/**
	 * Get: razao.
	 *
	 * @return razao
	 */
	public String getRazao() {
		return razao;
	}

	/**
	 * Set: razao.
	 *
	 * @param razao the razao
	 */
	public void setRazao(String razao) {
		this.razao = razao;
	}

	/**
	 * Get: resultadoProcessamento.
	 *
	 * @return resultadoProcessamento
	 */
	public String getResultadoProcessamento() {
		return resultadoProcessamento;
	}

	/**
	 * Set: resultadoProcessamento.
	 *
	 * @param resultadoProcessamento the resultado processamento
	 */
	public void setResultadoProcessamento(String resultadoProcessamento) {
		this.resultadoProcessamento = resultadoProcessamento;
	}

	/**
	 * Get: sequenciaRegistros.
	 *
	 * @return sequenciaRegistros
	 */
	public String getSequenciaRegistros() {
		return sequenciaRegistros;
	}

	/**
	 * Set: sequenciaRegistros.
	 *
	 * @param sequenciaRegistros the sequencia registros
	 */
	public void setSequenciaRegistros(String sequenciaRegistros) {
		this.sequenciaRegistros = sequenciaRegistros;
	}

	/**
	 * Get: sequenciaRemessa.
	 *
	 * @return sequenciaRemessa
	 */
	public String getSequenciaRemessa() {
		return sequenciaRemessa;
	}

	/**
	 * Set: sequenciaRemessa.
	 *
	 * @param sequenciaRemessa the sequencia remessa
	 */
	public void setSequenciaRemessa(String sequenciaRemessa) {
		this.sequenciaRemessa = sequenciaRemessa;
	}

	/**
	 * Get: valorInconsistentes.
	 *
	 * @return valorInconsistentes
	 */
	public String getValorInconsistentes() {
		return valorInconsistentes;
	}

	/**
	 * Set: valorInconsistentes.
	 *
	 * @param valorInconsistentes the valor inconsistentes
	 */
	public void setValorInconsistentes(String valorInconsistentes) {
		this.valorInconsistentes = valorInconsistentes;
	}

	/**
	 * Get: valorTotalLote.
	 *
	 * @return valorTotalLote
	 */
	public String getValorTotalLote() {
		return valorTotalLote;
	}

	/**
	 * Set: valorTotalLote.
	 *
	 * @param valorTotalLote the valor total lote
	 */
	public void setValorTotalLote(String valorTotalLote) {
		this.valorTotalLote = valorTotalLote;
	}

	/**
	 * Get: valorTotalRemessa.
	 *
	 * @return valorTotalRemessa
	 */
	public String getValorTotalRemessa() {
		return valorTotalRemessa;
	}

	/**
	 * Set: valorTotalRemessa.
	 *
	 * @param valorTotalRemessa the valor total remessa
	 */
	public void setValorTotalRemessa(String valorTotalRemessa) {
		this.valorTotalRemessa = valorTotalRemessa;
	}

	/**
	 * Get: ambienteProcessamento.
	 *
	 * @return ambienteProcessamento
	 */
	public String getAmbienteProcessamento() {
		return ambienteProcessamento;
	}

	/**
	 * Set: ambienteProcessamento.
	 *
	 * @param ambienteProcessamento the ambiente processamento
	 */
	public void setAmbienteProcessamento(String ambienteProcessamento) {
		this.ambienteProcessamento = ambienteProcessamento;
	}

	/**
	 * Get: listaOcorrencia.
	 *
	 * @return listaOcorrencia
	 */
	public List<OcorrenciaDTO> getListaOcorrencia() {
		return listaOcorrencia;
	}

	/**
	 * Set: listaOcorrencia.
	 *
	 * @param listaOcorrencia the lista ocorrencia
	 */
	public void setListaOcorrencia(List<OcorrenciaDTO> listaOcorrencia) {
		this.listaOcorrencia = listaOcorrencia;
	}

	/**
	 * Set: listaControleRemessa.
	 *
	 * @param listaControleRemessa the lista controle remessa
	 */
	public void setListaControleRemessa(List<SelectItem> listaControleRemessa) {
		this.listaControleRemessa = listaControleRemessa;
	}

	/**
	 * Get: listaControleRemessa.
	 *
	 * @return listaControleRemessa
	 */
	public List<SelectItem> getListaControleRemessa() {
		return listaControleRemessa;
	}

	/**
	 * Get: listaLote.
	 *
	 * @return listaLote
	 */
	public List<LoteDTO> getListaLote() {
		return listaLote;
	}

	/**
	 * Set: listaLote.
	 *
	 * @param listaLote the lista lote
	 */
	public void setListaLote(List<LoteDTO> listaLote) {
		this.listaLote = listaLote;
	}

	/**
	 * Get: codListaLote.
	 *
	 * @return codListaLote
	 */
	public int getCodListaLote() {
		return codListaLote;
	}

	/**
	 * Set: codListaLote.
	 *
	 * @param codListaLote the cod lista lote
	 */
	public void setCodListaLote(int codListaLote) {
		this.codListaLote = codListaLote;
	}

	/**
	 * Get: listaControleLote.
	 *
	 * @return listaControleLote
	 */
	public List<SelectItem> getListaControleLote() {
		return listaControleLote;
	}

	/**
	 * Set: listaControleLote.
	 *
	 * @param listaControleLote the lista controle lote
	 */
	public void setListaControleLote(List<SelectItem> listaControleLote) {
		this.listaControleLote = listaControleLote;
	}

	/**
	 * Get: identificacao.
	 *
	 * @return identificacao
	 */
	public String getIdentificacao() {
		return identificacao;
	}

	/**
	 * Set: identificacao.
	 *
	 * @param identificacao the identificacao
	 */
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	/**
	 * Is desab sit proc.
	 *
	 * @return true, if is desab sit proc
	 */
	public boolean isDesabSitProc() {
		return desabSitProc;
	}

	/**
	 * Set: desabSitProc.
	 *
	 * @param desabSitProc the desab sit proc
	 */
	public void setDesabSitProc(boolean desabSitProc) {
		this.desabSitProc = desabSitProc;
	}

	/**
	 * Is desab perfil.
	 *
	 * @return true, if is desab perfil
	 */
	public boolean isDesabPerfil() {
		return desabPerfil;
	}

	/**
	 * Set: desabPerfil.
	 *
	 * @param desabPerfil the desab perfil
	 */
	public void setDesabPerfil(boolean desabPerfil) {
		this.desabPerfil = desabPerfil;
	}

	/**
	 * Is desab text remessa.
	 *
	 * @return true, if is desab text remessa
	 */
	public boolean isDesabTextRemessa() {
		return desabTextRemessa;
	}

	/**
	 * Set: desabTextRemessa.
	 *
	 * @param desabTextRemessa the desab text remessa
	 */
	public void setDesabTextRemessa(boolean desabTextRemessa) {
		this.desabTextRemessa = desabTextRemessa;
	}

	/**
	 * Is desab estatistica incons.
	 *
	 * @return true, if is desab estatistica incons
	 */
	public boolean isDesabEstatisticaIncons() {
		return desabEstatisticaIncons;
	}

	/**
	 * Set: desabEstatisticaIncons.
	 *
	 * @param desabEstatisticaIncons the desab estatistica incons
	 */
	public void setDesabEstatisticaIncons(boolean desabEstatisticaIncons) {
		this.desabEstatisticaIncons = desabEstatisticaIncons;
	}

	/**
	 * Get: exibirFocus.
	 *
	 * @return exibirFocus
	 */
	public Character getExibirFocus() {
		return exibirFocus;
	}

	/**
	 * Set: exibirFocus.
	 *
	 * @param exibirFocus the exibir focus
	 */
	public void setExibirFocus(Character exibirFocus) {
		this.exibirFocus = exibirFocus;
	}

	/**
	 * Get: logger.
	 *
	 * @return logger
	 */
	public ILogManager getLogger() {
		return logger;
	}

	/**
	 * Set: logger.
	 *
	 * @param logger the logger
	 */
	public void setLogger(ILogManager logger) {
		this.logger = logger;
	}
	
	/**
	 * Voltar remessa.
	 *
	 * @return the string
	 */
	public String voltarRemessa(){
		this.loadComboProduto();
		this.findArquivoRecebido();
		
		return "VOLTAR_PESQUISA";
	}
	
	/**
	 * Voltar lote.
	 *
	 * @return the string
	 */
	public String voltarLote(){
		this.findLote();

		return "VOLTAR_DADOS_LOTE";
	}
	
	/**
	 * Voltar estatisticas.
	 *
	 * @return the string
	 */
	public String voltarEstatisticas(){
		this.findEstatistica();
		
		return "VOLTAR_ESTATISTICA";
	}
	
	/**
	 * Voltar inconsistencia.
	 *
	 * @return the string
	 */
	public String voltarInconsistencia(){
		this.findArquivoInconsistente();
		
		return "VOLTAR_INCONSISTENTES";
	}

}
