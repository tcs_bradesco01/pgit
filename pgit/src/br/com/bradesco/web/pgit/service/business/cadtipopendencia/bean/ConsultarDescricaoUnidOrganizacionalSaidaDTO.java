/*
 * Nome: br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean;


/**
 * Nome: ConsultarDescricaoUnidOrganizacionalSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDescricaoUnidOrganizacionalSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo dsNumeroSeqUnidadeOrganizacional. */
	private String dsNumeroSeqUnidadeOrganizacional;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsNumeroSeqUnidadeOrganizacional.
	 *
	 * @return dsNumeroSeqUnidadeOrganizacional
	 */
	public String getDsNumeroSeqUnidadeOrganizacional() {
		return dsNumeroSeqUnidadeOrganizacional;
	}
	
	/**
	 * Set: dsNumeroSeqUnidadeOrganizacional.
	 *
	 * @param dsNumeroSeqUnidadeOrganizacional the ds numero seq unidade organizacional
	 */
	public void setDsNumeroSeqUnidadeOrganizacional(
			String dsNumeroSeqUnidadeOrganizacional) {
		this.dsNumeroSeqUnidadeOrganizacional = dsNumeroSeqUnidadeOrganizacional;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	
	
}
