/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOperadoraEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOperadoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.DetalharSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.DetalharSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ExcluirSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ExcluirSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.IncluirSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.IncluirSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ListarIncluirSolEmiComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ListarIncluirSolEmiComprovanteSaidaDTO;
/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: SolEmissaoComprovantePagtoClientePag
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ISolEmissaoComprovantePagtoClientePagService {
	 
 	/**
 	 * Listar incluir sol emi comprovante.
 	 *
 	 * @param entradaDTO the entrada dto
 	 * @return the list< listar incluir sol emi comprovante saida dt o>
 	 */
 	List<ListarIncluirSolEmiComprovanteSaidaDTO> listarIncluirSolEmiComprovante(ListarIncluirSolEmiComprovanteEntradaDTO entradaDTO);
	 
 	/**
 	 * Consultar agencia operadora.
 	 *
 	 * @param entradaDTO the entrada dto
 	 * @return the consultar agencia operadora saida dto
 	 */
 	ConsultarAgenciaOperadoraSaidaDTO consultarAgenciaOperadora(ConsultarAgenciaOperadoraEntradaDTO entradaDTO);
	 
 	/**
 	 * Consultar agencia ope tarifa padrao.
 	 *
 	 * @param entradaDTO the entrada dto
 	 * @return the consultar agencia ope tarifa padrao saida dto
 	 */
 	ConsultarAgenciaOpeTarifaPadraoSaidaDTO consultarAgenciaOpeTarifaPadrao(ConsultarAgenciaOpeTarifaPadraoEntradaDTO entradaDTO);
	 
 	/**
 	 * Consultar sol emi comprovante clie pagador.
 	 *
 	 * @param entradaDTO the entrada dto
 	 * @return the list< consultar sol emi comprovante clie pagador saida dt o>
 	 */
 	List<ConsultarSolEmiComprovanteCliePagadorSaidaDTO> consultarSolEmiComprovanteCliePagador(ConsultarSolEmiComprovanteCliePagadorEntradaDTO entradaDTO);
	 
 	/**
 	 * Incluir sol emi comprovante clie pagador.
 	 *
 	 * @param entradaDTO the entrada dto
 	 * @return the incluir sol emi comprovante clie pagador saida dto
 	 */
 	IncluirSolEmiComprovanteCliePagadorSaidaDTO incluirSolEmiComprovanteCliePagador (IncluirSolEmiComprovanteCliePagadorEntradaDTO entradaDTO);
	 
 	/**
 	 * Detalhar sol emi comprovante clie pagador.
 	 *
 	 * @param entradaDTO the entrada dto
 	 * @return the detalhar sol emi comprovante clie pagador saida dto
 	 */
 	DetalharSolEmiComprovanteCliePagadorSaidaDTO detalharSolEmiComprovanteCliePagador (DetalharSolEmiComprovanteCliePagadorEntradaDTO entradaDTO);
	 
 	/**
 	 * Excluir sol emi comprovante clie pagador.
 	 *
 	 * @param entradaDTO the entrada dto
 	 * @return the excluir sol emi comprovante clie pagador saida dto
 	 */
 	ExcluirSolEmiComprovanteCliePagadorSaidaDTO excluirSolEmiComprovanteCliePagador(ExcluirSolEmiComprovanteCliePagadorEntradaDTO entradaDTO);
}

