/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlote.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencia.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencia implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroLoteRemessa
     */
    private long _numeroLoteRemessa = 0;

    /**
     * keeps track of state for field: _numeroLoteRemessa
     */
    private boolean _has_numeroLoteRemessa;

    /**
     * Field _bancoDbto
     */
    private int _bancoDbto = 0;

    /**
     * keeps track of state for field: _bancoDbto
     */
    private boolean _has_bancoDbto;

    /**
     * Field _agenciaDbto
     */
    private int _agenciaDbto = 0;

    /**
     * keeps track of state for field: _agenciaDbto
     */
    private boolean _has_agenciaDbto;

    /**
     * Field _contaDbto
     */
    private long _contaDbto = 0;

    /**
     * keeps track of state for field: _contaDbto
     */
    private boolean _has_contaDbto;

    /**
     * Field _codModalidadePgto
     */
    private int _codModalidadePgto = 0;

    /**
     * keeps track of state for field: _codModalidadePgto
     */
    private boolean _has_codModalidadePgto;

    /**
     * Field _descricaoModalidadePgto
     */
    private java.lang.String _descricaoModalidadePgto;

    /**
     * Field _qtdeRegistros
     */
    private long _qtdeRegistros = 0;

    /**
     * keeps track of state for field: _qtdeRegistros
     */
    private boolean _has_qtdeRegistros;

    /**
     * Field _valorRegistros
     */
    private double _valorRegistros = 0;

    /**
     * keeps track of state for field: _valorRegistros
     */
    private boolean _has_valorRegistros;

    /**
     * Field _cosSituacaoLote
     */
    private int _cosSituacaoLote = 0;

    /**
     * keeps track of state for field: _cosSituacaoLote
     */
    private boolean _has_cosSituacaoLote;

    /**
     * Field _descricaoSituacaoLote
     */
    private java.lang.String _descricaoSituacaoLote;

    /**
     * Field _condicaoLote
     */
    private int _condicaoLote = 0;

    /**
     * keeps track of state for field: _condicaoLote
     */
    private boolean _has_condicaoLote;

    /**
     * Field _descicaoCondicaoLote
     */
    private java.lang.String _descicaoCondicaoLote;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencia() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlote.response.Ocorrencia()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteAgenciaDbto
     * 
     */
    public void deleteAgenciaDbto()
    {
        this._has_agenciaDbto= false;
    } //-- void deleteAgenciaDbto() 

    /**
     * Method deleteBancoDbto
     * 
     */
    public void deleteBancoDbto()
    {
        this._has_bancoDbto= false;
    } //-- void deleteBancoDbto() 

    /**
     * Method deleteCodModalidadePgto
     * 
     */
    public void deleteCodModalidadePgto()
    {
        this._has_codModalidadePgto= false;
    } //-- void deleteCodModalidadePgto() 

    /**
     * Method deleteCondicaoLote
     * 
     */
    public void deleteCondicaoLote()
    {
        this._has_condicaoLote= false;
    } //-- void deleteCondicaoLote() 

    /**
     * Method deleteContaDbto
     * 
     */
    public void deleteContaDbto()
    {
        this._has_contaDbto= false;
    } //-- void deleteContaDbto() 

    /**
     * Method deleteCosSituacaoLote
     * 
     */
    public void deleteCosSituacaoLote()
    {
        this._has_cosSituacaoLote= false;
    } //-- void deleteCosSituacaoLote() 

    /**
     * Method deleteNumeroLoteRemessa
     * 
     */
    public void deleteNumeroLoteRemessa()
    {
        this._has_numeroLoteRemessa= false;
    } //-- void deleteNumeroLoteRemessa() 

    /**
     * Method deleteQtdeRegistros
     * 
     */
    public void deleteQtdeRegistros()
    {
        this._has_qtdeRegistros= false;
    } //-- void deleteQtdeRegistros() 

    /**
     * Method deleteValorRegistros
     * 
     */
    public void deleteValorRegistros()
    {
        this._has_valorRegistros= false;
    } //-- void deleteValorRegistros() 

    /**
     * Returns the value of field 'agenciaDbto'.
     * 
     * @return int
     * @return the value of field 'agenciaDbto'.
     */
    public int getAgenciaDbto()
    {
        return this._agenciaDbto;
    } //-- int getAgenciaDbto() 

    /**
     * Returns the value of field 'bancoDbto'.
     * 
     * @return int
     * @return the value of field 'bancoDbto'.
     */
    public int getBancoDbto()
    {
        return this._bancoDbto;
    } //-- int getBancoDbto() 

    /**
     * Returns the value of field 'codModalidadePgto'.
     * 
     * @return int
     * @return the value of field 'codModalidadePgto'.
     */
    public int getCodModalidadePgto()
    {
        return this._codModalidadePgto;
    } //-- int getCodModalidadePgto() 

    /**
     * Returns the value of field 'condicaoLote'.
     * 
     * @return int
     * @return the value of field 'condicaoLote'.
     */
    public int getCondicaoLote()
    {
        return this._condicaoLote;
    } //-- int getCondicaoLote() 

    /**
     * Returns the value of field 'contaDbto'.
     * 
     * @return long
     * @return the value of field 'contaDbto'.
     */
    public long getContaDbto()
    {
        return this._contaDbto;
    } //-- long getContaDbto() 

    /**
     * Returns the value of field 'cosSituacaoLote'.
     * 
     * @return int
     * @return the value of field 'cosSituacaoLote'.
     */
    public int getCosSituacaoLote()
    {
        return this._cosSituacaoLote;
    } //-- int getCosSituacaoLote() 

    /**
     * Returns the value of field 'descicaoCondicaoLote'.
     * 
     * @return String
     * @return the value of field 'descicaoCondicaoLote'.
     */
    public java.lang.String getDescicaoCondicaoLote()
    {
        return this._descicaoCondicaoLote;
    } //-- java.lang.String getDescicaoCondicaoLote() 

    /**
     * Returns the value of field 'descricaoModalidadePgto'.
     * 
     * @return String
     * @return the value of field 'descricaoModalidadePgto'.
     */
    public java.lang.String getDescricaoModalidadePgto()
    {
        return this._descricaoModalidadePgto;
    } //-- java.lang.String getDescricaoModalidadePgto() 

    /**
     * Returns the value of field 'descricaoSituacaoLote'.
     * 
     * @return String
     * @return the value of field 'descricaoSituacaoLote'.
     */
    public java.lang.String getDescricaoSituacaoLote()
    {
        return this._descricaoSituacaoLote;
    } //-- java.lang.String getDescricaoSituacaoLote() 

    /**
     * Returns the value of field 'numeroLoteRemessa'.
     * 
     * @return long
     * @return the value of field 'numeroLoteRemessa'.
     */
    public long getNumeroLoteRemessa()
    {
        return this._numeroLoteRemessa;
    } //-- long getNumeroLoteRemessa() 

    /**
     * Returns the value of field 'qtdeRegistros'.
     * 
     * @return long
     * @return the value of field 'qtdeRegistros'.
     */
    public long getQtdeRegistros()
    {
        return this._qtdeRegistros;
    } //-- long getQtdeRegistros() 

    /**
     * Returns the value of field 'valorRegistros'.
     * 
     * @return double
     * @return the value of field 'valorRegistros'.
     */
    public double getValorRegistros()
    {
        return this._valorRegistros;
    } //-- double getValorRegistros() 

    /**
     * Method hasAgenciaDbto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasAgenciaDbto()
    {
        return this._has_agenciaDbto;
    } //-- boolean hasAgenciaDbto() 

    /**
     * Method hasBancoDbto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasBancoDbto()
    {
        return this._has_bancoDbto;
    } //-- boolean hasBancoDbto() 

    /**
     * Method hasCodModalidadePgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCodModalidadePgto()
    {
        return this._has_codModalidadePgto;
    } //-- boolean hasCodModalidadePgto() 

    /**
     * Method hasCondicaoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCondicaoLote()
    {
        return this._has_condicaoLote;
    } //-- boolean hasCondicaoLote() 

    /**
     * Method hasContaDbto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasContaDbto()
    {
        return this._has_contaDbto;
    } //-- boolean hasContaDbto() 

    /**
     * Method hasCosSituacaoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCosSituacaoLote()
    {
        return this._has_cosSituacaoLote;
    } //-- boolean hasCosSituacaoLote() 

    /**
     * Method hasNumeroLoteRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroLoteRemessa()
    {
        return this._has_numeroLoteRemessa;
    } //-- boolean hasNumeroLoteRemessa() 

    /**
     * Method hasQtdeRegistros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtdeRegistros()
    {
        return this._has_qtdeRegistros;
    } //-- boolean hasQtdeRegistros() 

    /**
     * Method hasValorRegistros
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorRegistros()
    {
        return this._has_valorRegistros;
    } //-- boolean hasValorRegistros() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'agenciaDbto'.
     * 
     * @param agenciaDbto the value of field 'agenciaDbto'.
     */
    public void setAgenciaDbto(int agenciaDbto)
    {
        this._agenciaDbto = agenciaDbto;
        this._has_agenciaDbto = true;
    } //-- void setAgenciaDbto(int) 

    /**
     * Sets the value of field 'bancoDbto'.
     * 
     * @param bancoDbto the value of field 'bancoDbto'.
     */
    public void setBancoDbto(int bancoDbto)
    {
        this._bancoDbto = bancoDbto;
        this._has_bancoDbto = true;
    } //-- void setBancoDbto(int) 

    /**
     * Sets the value of field 'codModalidadePgto'.
     * 
     * @param codModalidadePgto the value of field
     * 'codModalidadePgto'.
     */
    public void setCodModalidadePgto(int codModalidadePgto)
    {
        this._codModalidadePgto = codModalidadePgto;
        this._has_codModalidadePgto = true;
    } //-- void setCodModalidadePgto(int) 

    /**
     * Sets the value of field 'condicaoLote'.
     * 
     * @param condicaoLote the value of field 'condicaoLote'.
     */
    public void setCondicaoLote(int condicaoLote)
    {
        this._condicaoLote = condicaoLote;
        this._has_condicaoLote = true;
    } //-- void setCondicaoLote(int) 

    /**
     * Sets the value of field 'contaDbto'.
     * 
     * @param contaDbto the value of field 'contaDbto'.
     */
    public void setContaDbto(long contaDbto)
    {
        this._contaDbto = contaDbto;
        this._has_contaDbto = true;
    } //-- void setContaDbto(long) 

    /**
     * Sets the value of field 'cosSituacaoLote'.
     * 
     * @param cosSituacaoLote the value of field 'cosSituacaoLote'.
     */
    public void setCosSituacaoLote(int cosSituacaoLote)
    {
        this._cosSituacaoLote = cosSituacaoLote;
        this._has_cosSituacaoLote = true;
    } //-- void setCosSituacaoLote(int) 

    /**
     * Sets the value of field 'descicaoCondicaoLote'.
     * 
     * @param descicaoCondicaoLote the value of field
     * 'descicaoCondicaoLote'.
     */
    public void setDescicaoCondicaoLote(java.lang.String descicaoCondicaoLote)
    {
        this._descicaoCondicaoLote = descicaoCondicaoLote;
    } //-- void setDescicaoCondicaoLote(java.lang.String) 

    /**
     * Sets the value of field 'descricaoModalidadePgto'.
     * 
     * @param descricaoModalidadePgto the value of field
     * 'descricaoModalidadePgto'.
     */
    public void setDescricaoModalidadePgto(java.lang.String descricaoModalidadePgto)
    {
        this._descricaoModalidadePgto = descricaoModalidadePgto;
    } //-- void setDescricaoModalidadePgto(java.lang.String) 

    /**
     * Sets the value of field 'descricaoSituacaoLote'.
     * 
     * @param descricaoSituacaoLote the value of field
     * 'descricaoSituacaoLote'.
     */
    public void setDescricaoSituacaoLote(java.lang.String descricaoSituacaoLote)
    {
        this._descricaoSituacaoLote = descricaoSituacaoLote;
    } //-- void setDescricaoSituacaoLote(java.lang.String) 

    /**
     * Sets the value of field 'numeroLoteRemessa'.
     * 
     * @param numeroLoteRemessa the value of field
     * 'numeroLoteRemessa'.
     */
    public void setNumeroLoteRemessa(long numeroLoteRemessa)
    {
        this._numeroLoteRemessa = numeroLoteRemessa;
        this._has_numeroLoteRemessa = true;
    } //-- void setNumeroLoteRemessa(long) 

    /**
     * Sets the value of field 'qtdeRegistros'.
     * 
     * @param qtdeRegistros the value of field 'qtdeRegistros'.
     */
    public void setQtdeRegistros(long qtdeRegistros)
    {
        this._qtdeRegistros = qtdeRegistros;
        this._has_qtdeRegistros = true;
    } //-- void setQtdeRegistros(long) 

    /**
     * Sets the value of field 'valorRegistros'.
     * 
     * @param valorRegistros the value of field 'valorRegistros'.
     */
    public void setValorRegistros(double valorRegistros)
    {
        this._valorRegistros = valorRegistros;
        this._has_valorRegistros = true;
    } //-- void setValorRegistros(double) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencia
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlote.response.Ocorrencia unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlote.response.Ocorrencia) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlote.response.Ocorrencia.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlote.response.Ocorrencia unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
