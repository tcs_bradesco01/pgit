/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetalhefavorecido.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDetalheFavorecidoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDetalheFavorecidoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _dsTipoFavorecido
     */
    private java.lang.String _dsTipoFavorecido;

    /**
     * Field _dsInscricaoFavorecido
     */
    private java.lang.String _dsInscricaoFavorecido;

    /**
     * Field _mtBloqueio
     */
    private java.lang.String _mtBloqueio;

    /**
     * Field _cdTipoFavorecido
     */
    private int _cdTipoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoFavorecido
     */
    private boolean _has_cdTipoFavorecido;

    /**
     * Field _cdTipoInscricaoFavorecido
     */
    private int _cdTipoInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoFavorecido
     */
    private boolean _has_cdTipoInscricaoFavorecido;

    /**
     * Field _cdMotivoBloqueioFavorecido
     */
    private int _cdMotivoBloqueioFavorecido = 0;

    /**
     * keeps track of state for field: _cdMotivoBloqueioFavorecido
     */
    private boolean _has_cdMotivoBloqueioFavorecido;

    /**
     * Field _cdInscricaoFavorecidoCliente
     */
    private long _cdInscricaoFavorecidoCliente = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecidoCliente
     */
    private boolean _has_cdInscricaoFavorecidoCliente;

    /**
     * Field _dsCompletaFavorecidoCliente
     */
    private java.lang.String _dsCompletaFavorecidoCliente;

    /**
     * Field _dsAbreviacaoFavorecidoCliente
     */
    private java.lang.String _dsAbreviacaoFavorecidoCliente;

    /**
     * Field _dsOrganizacaoEmissorDocumento
     */
    private java.lang.String _dsOrganizacaoEmissorDocumento;

    /**
     * Field _dtExpedicaoDocumentoFavorecido
     */
    private java.lang.String _dtExpedicaoDocumentoFavorecido;

    /**
     * Field _cdSexoFavorecidoCliente
     */
    private java.lang.String _cdSexoFavorecidoCliente;

    /**
     * Field _cdAreaFavorecidoCliente
     */
    private int _cdAreaFavorecidoCliente = 0;

    /**
     * keeps track of state for field: _cdAreaFavorecidoCliente
     */
    private boolean _has_cdAreaFavorecidoCliente;

    /**
     * Field _cdFoneFavorecidoCliente
     */
    private java.lang.String _cdFoneFavorecidoCliente;

    /**
     * Field _cdAreaFaxFavorecido
     */
    private int _cdAreaFaxFavorecido = 0;

    /**
     * keeps track of state for field: _cdAreaFaxFavorecido
     */
    private boolean _has_cdAreaFaxFavorecido;

    /**
     * Field _cdFaxFavorecidoCliente
     */
    private java.lang.String _cdFaxFavorecidoCliente;

    /**
     * Field _cdRamalFaxFavorecido
     */
    private int _cdRamalFaxFavorecido = 0;

    /**
     * keeps track of state for field: _cdRamalFaxFavorecido
     */
    private boolean _has_cdRamalFaxFavorecido;

    /**
     * Field _cdAreaCelularFavorecido
     */
    private int _cdAreaCelularFavorecido = 0;

    /**
     * keeps track of state for field: _cdAreaCelularFavorecido
     */
    private boolean _has_cdAreaCelularFavorecido;

    /**
     * Field _cdCelularFavorecidoCliente
     */
    private java.lang.String _cdCelularFavorecidoCliente;

    /**
     * Field _cdAreaFoneComercial
     */
    private int _cdAreaFoneComercial = 0;

    /**
     * keeps track of state for field: _cdAreaFoneComercial
     */
    private boolean _has_cdAreaFoneComercial;

    /**
     * Field _cdFoneComercialFavorecido
     */
    private java.lang.String _cdFoneComercialFavorecido;

    /**
     * Field _cdRamalComercialFavorecido
     */
    private int _cdRamalComercialFavorecido = 0;

    /**
     * keeps track of state for field: _cdRamalComercialFavorecido
     */
    private boolean _has_cdRamalComercialFavorecido;

    /**
     * Field _enEmailFavorecidoCliente
     */
    private java.lang.String _enEmailFavorecidoCliente;

    /**
     * Field _enEmailComercialFavorecido
     */
    private java.lang.String _enEmailComercialFavorecido;

    /**
     * Field _cdSmsFavorecidoCliente
     */
    private java.lang.String _cdSmsFavorecidoCliente;

    /**
     * Field _enLogradouroFavorecidoCliente
     */
    private java.lang.String _enLogradouroFavorecidoCliente;

    /**
     * Field _enNumeroLogradouroFavorecido
     */
    private java.lang.String _enNumeroLogradouroFavorecido;

    /**
     * Field _enComplementoLogradouroFavorecido
     */
    private java.lang.String _enComplementoLogradouroFavorecido;

    /**
     * Field _enBairroFavorecidoCliente
     */
    private java.lang.String _enBairroFavorecidoCliente;

    /**
     * Field _dsEstadoFavorecidoCliente
     */
    private java.lang.String _dsEstadoFavorecidoCliente;

    /**
     * Field _cdSiglaEstadoFavorecidoCliente
     */
    private java.lang.String _cdSiglaEstadoFavorecidoCliente;

    /**
     * Field _cdCepFavorecidoCliente
     */
    private int _cdCepFavorecidoCliente = 0;

    /**
     * keeps track of state for field: _cdCepFavorecidoCliente
     */
    private boolean _has_cdCepFavorecidoCliente;

    /**
     * Field _cdComplementoCepFavorecido
     */
    private int _cdComplementoCepFavorecido = 0;

    /**
     * keeps track of state for field: _cdComplementoCepFavorecido
     */
    private boolean _has_cdComplementoCepFavorecido;

    /**
     * Field _enLogradrouroCorrespondenciaFavorecido
     */
    private java.lang.String _enLogradrouroCorrespondenciaFavorecido;

    /**
     * Field _enNumeroLogradouroCorrespondencia
     */
    private java.lang.String _enNumeroLogradouroCorrespondencia;

    /**
     * Field _enComplementoLogradouroCorrespondencia
     */
    private java.lang.String _enComplementoLogradouroCorrespondencia;

    /**
     * Field _enBairroCorrespondenciaFavorecido
     */
    private java.lang.String _enBairroCorrespondenciaFavorecido;

    /**
     * Field _dsEstadoCorrespondenciaFavorecido
     */
    private java.lang.String _dsEstadoCorrespondenciaFavorecido;

    /**
     * Field _cdSiglaEstadoCorrespondencia
     */
    private java.lang.String _cdSiglaEstadoCorrespondencia;

    /**
     * Field _cdCepCorrespondenciaFavorecido
     */
    private int _cdCepCorrespondenciaFavorecido = 0;

    /**
     * keeps track of state for field:
     * _cdCepCorrespondenciaFavorecido
     */
    private boolean _has_cdCepCorrespondenciaFavorecido;

    /**
     * Field _cdComplementoCepCorrespondencia
     */
    private int _cdComplementoCepCorrespondencia = 0;

    /**
     * keeps track of state for field:
     * _cdComplementoCepCorrespondencia
     */
    private boolean _has_cdComplementoCepCorrespondencia;

    /**
     * Field _cdNitFavorecidoCliente
     */
    private long _cdNitFavorecidoCliente = 0;

    /**
     * keeps track of state for field: _cdNitFavorecidoCliente
     */
    private boolean _has_cdNitFavorecidoCliente;

    /**
     * Field _dsMunicipioFavorecidoCliente
     */
    private java.lang.String _dsMunicipioFavorecidoCliente;

    /**
     * Field _cdInscricaoEstadoFavorecido
     */
    private long _cdInscricaoEstadoFavorecido = 0;

    /**
     * keeps track of state for field: _cdInscricaoEstadoFavorecido
     */
    private boolean _has_cdInscricaoEstadoFavorecido;

    /**
     * Field _cdSiglaUfInscricaoEstadual
     */
    private java.lang.String _cdSiglaUfInscricaoEstadual;

    /**
     * Field _dsMunicipalInscricaoEstado
     */
    private java.lang.String _dsMunicipalInscricaoEstado;

    /**
     * Field _cdSituacaoFavorecidoCliente
     */
    private int _cdSituacaoFavorecidoCliente = 0;

    /**
     * keeps track of state for field: _cdSituacaoFavorecidoCliente
     */
    private boolean _has_cdSituacaoFavorecidoCliente;

    /**
     * Field _hrBloqueioFavorecidoCliente
     */
    private java.lang.String _hrBloqueioFavorecidoCliente;

    /**
     * Field _dsObservacaoControleFavorecido
     */
    private java.lang.String _dsObservacaoControleFavorecido;

    /**
     * Field _cdIndicadorRetornoCliente
     */
    private int _cdIndicadorRetornoCliente = 0;

    /**
     * keeps track of state for field: _cdIndicadorRetornoCliente
     */
    private boolean _has_cdIndicadorRetornoCliente;

    /**
     * Field _dsRetornoCliente
     */
    private java.lang.String _dsRetornoCliente;

    /**
     * Field _dtUltimaMovimentacaoFavorecido
     */
    private java.lang.String _dtUltimaMovimentacaoFavorecido;

    /**
     * Field _cdIndicadorFavorecidoInativo
     */
    private int _cdIndicadorFavorecidoInativo = 0;

    /**
     * keeps track of state for field: _cdIndicadorFavorecidoInativo
     */
    private boolean _has_cdIndicadorFavorecidoInativo;

    /**
     * Field _cdEstadoCivilFavorecido
     */
    private int _cdEstadoCivilFavorecido = 0;

    /**
     * keeps track of state for field: _cdEstadoCivilFavorecido
     */
    private boolean _has_cdEstadoCivilFavorecido;

    /**
     * Field _cdUfEmpresaFavorecido
     */
    private java.lang.String _cdUfEmpresaFavorecido;

    /**
     * Field _dtNascimentoFavorecidoCliente
     */
    private java.lang.String _dtNascimentoFavorecidoCliente;

    /**
     * Field _dsEmpresaFavorecidoCliente
     */
    private java.lang.String _dsEmpresaFavorecidoCliente;

    /**
     * Field _dsMunicipioEmpresaFavorecido
     */
    private java.lang.String _dsMunicipioEmpresaFavorecido;

    /**
     * Field _dsConjugeFavorecidoCliente
     */
    private java.lang.String _dsConjugeFavorecidoCliente;

    /**
     * Field _dsNacionalidadeFavorecidoCliente
     */
    private java.lang.String _dsNacionalidadeFavorecidoCliente;

    /**
     * Field _dsProfissaoFavorecidoCliente
     */
    private java.lang.String _dsProfissaoFavorecidoCliente;

    /**
     * Field _dsCargoFavorecidoCliente
     */
    private java.lang.String _dsCargoFavorecidoCliente;

    /**
     * Field _dsUfEmpreFavorecido
     */
    private java.lang.String _dsUfEmpreFavorecido;

    /**
     * Field _dsMaeFavorecidoCliente
     */
    private java.lang.String _dsMaeFavorecidoCliente;

    /**
     * Field _dsPaiFavorecidoCliente
     */
    private java.lang.String _dsPaiFavorecidoCliente;

    /**
     * Field _dsMunicipioNascimentoFavorecido
     */
    private java.lang.String _dsMunicipioNascimentoFavorecido;

    /**
     * Field _dsUfNascimentoFavorecido
     */
    private java.lang.String _dsUfNascimentoFavorecido;

    /**
     * Field _cdUfNascimentoFavorecido
     */
    private java.lang.String _cdUfNascimentoFavorecido;

    /**
     * Field _vlRendaMensalFavorecido
     */
    private java.math.BigDecimal _vlRendaMensalFavorecido = new java.math.BigDecimal("0");

    /**
     * Field _dtAtualizarEnderecoResidencial
     */
    private java.lang.String _dtAtualizarEnderecoResidencial;

    /**
     * Field _dtAtualizarEnderecoCorrespondencia
     */
    private java.lang.String _dtAtualizarEnderecoCorrespondencia;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterna
     */
    private java.lang.String _cdUsuarioInclusaoExterna;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalinclusao
     */
    private java.lang.String _dsCanalinclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterna
     */
    private java.lang.String _cdUsuarioManutencaoExterna;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAcaoManutencao
     */
    private int _cdAcaoManutencao = 0;

    /**
     * keeps track of state for field: _cdAcaoManutencao
     */
    private boolean _has_cdAcaoManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDetalheFavorecidoResponse() 
     {
        super();
        setVlRendaMensalFavorecido(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhefavorecido.response.ConsultarDetalheFavorecidoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoManutencao
     * 
     */
    public void deleteCdAcaoManutencao()
    {
        this._has_cdAcaoManutencao= false;
    } //-- void deleteCdAcaoManutencao() 

    /**
     * Method deleteCdAreaCelularFavorecido
     * 
     */
    public void deleteCdAreaCelularFavorecido()
    {
        this._has_cdAreaCelularFavorecido= false;
    } //-- void deleteCdAreaCelularFavorecido() 

    /**
     * Method deleteCdAreaFavorecidoCliente
     * 
     */
    public void deleteCdAreaFavorecidoCliente()
    {
        this._has_cdAreaFavorecidoCliente= false;
    } //-- void deleteCdAreaFavorecidoCliente() 

    /**
     * Method deleteCdAreaFaxFavorecido
     * 
     */
    public void deleteCdAreaFaxFavorecido()
    {
        this._has_cdAreaFaxFavorecido= false;
    } //-- void deleteCdAreaFaxFavorecido() 

    /**
     * Method deleteCdAreaFoneComercial
     * 
     */
    public void deleteCdAreaFoneComercial()
    {
        this._has_cdAreaFoneComercial= false;
    } //-- void deleteCdAreaFoneComercial() 

    /**
     * Method deleteCdCepCorrespondenciaFavorecido
     * 
     */
    public void deleteCdCepCorrespondenciaFavorecido()
    {
        this._has_cdCepCorrespondenciaFavorecido= false;
    } //-- void deleteCdCepCorrespondenciaFavorecido() 

    /**
     * Method deleteCdCepFavorecidoCliente
     * 
     */
    public void deleteCdCepFavorecidoCliente()
    {
        this._has_cdCepFavorecidoCliente= false;
    } //-- void deleteCdCepFavorecidoCliente() 

    /**
     * Method deleteCdComplementoCepCorrespondencia
     * 
     */
    public void deleteCdComplementoCepCorrespondencia()
    {
        this._has_cdComplementoCepCorrespondencia= false;
    } //-- void deleteCdComplementoCepCorrespondencia() 

    /**
     * Method deleteCdComplementoCepFavorecido
     * 
     */
    public void deleteCdComplementoCepFavorecido()
    {
        this._has_cdComplementoCepFavorecido= false;
    } //-- void deleteCdComplementoCepFavorecido() 

    /**
     * Method deleteCdEstadoCivilFavorecido
     * 
     */
    public void deleteCdEstadoCivilFavorecido()
    {
        this._has_cdEstadoCivilFavorecido= false;
    } //-- void deleteCdEstadoCivilFavorecido() 

    /**
     * Method deleteCdIndicadorFavorecidoInativo
     * 
     */
    public void deleteCdIndicadorFavorecidoInativo()
    {
        this._has_cdIndicadorFavorecidoInativo= false;
    } //-- void deleteCdIndicadorFavorecidoInativo() 

    /**
     * Method deleteCdIndicadorRetornoCliente
     * 
     */
    public void deleteCdIndicadorRetornoCliente()
    {
        this._has_cdIndicadorRetornoCliente= false;
    } //-- void deleteCdIndicadorRetornoCliente() 

    /**
     * Method deleteCdInscricaoEstadoFavorecido
     * 
     */
    public void deleteCdInscricaoEstadoFavorecido()
    {
        this._has_cdInscricaoEstadoFavorecido= false;
    } //-- void deleteCdInscricaoEstadoFavorecido() 

    /**
     * Method deleteCdInscricaoFavorecidoCliente
     * 
     */
    public void deleteCdInscricaoFavorecidoCliente()
    {
        this._has_cdInscricaoFavorecidoCliente= false;
    } //-- void deleteCdInscricaoFavorecidoCliente() 

    /**
     * Method deleteCdMotivoBloqueioFavorecido
     * 
     */
    public void deleteCdMotivoBloqueioFavorecido()
    {
        this._has_cdMotivoBloqueioFavorecido= false;
    } //-- void deleteCdMotivoBloqueioFavorecido() 

    /**
     * Method deleteCdNitFavorecidoCliente
     * 
     */
    public void deleteCdNitFavorecidoCliente()
    {
        this._has_cdNitFavorecidoCliente= false;
    } //-- void deleteCdNitFavorecidoCliente() 

    /**
     * Method deleteCdRamalComercialFavorecido
     * 
     */
    public void deleteCdRamalComercialFavorecido()
    {
        this._has_cdRamalComercialFavorecido= false;
    } //-- void deleteCdRamalComercialFavorecido() 

    /**
     * Method deleteCdRamalFaxFavorecido
     * 
     */
    public void deleteCdRamalFaxFavorecido()
    {
        this._has_cdRamalFaxFavorecido= false;
    } //-- void deleteCdRamalFaxFavorecido() 

    /**
     * Method deleteCdSituacaoFavorecidoCliente
     * 
     */
    public void deleteCdSituacaoFavorecidoCliente()
    {
        this._has_cdSituacaoFavorecidoCliente= false;
    } //-- void deleteCdSituacaoFavorecidoCliente() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoFavorecido
     * 
     */
    public void deleteCdTipoFavorecido()
    {
        this._has_cdTipoFavorecido= false;
    } //-- void deleteCdTipoFavorecido() 

    /**
     * Method deleteCdTipoInscricaoFavorecido
     * 
     */
    public void deleteCdTipoInscricaoFavorecido()
    {
        this._has_cdTipoInscricaoFavorecido= false;
    } //-- void deleteCdTipoInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdAcaoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdAcaoManutencao'.
     */
    public int getCdAcaoManutencao()
    {
        return this._cdAcaoManutencao;
    } //-- int getCdAcaoManutencao() 

    /**
     * Returns the value of field 'cdAreaCelularFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdAreaCelularFavorecido'.
     */
    public int getCdAreaCelularFavorecido()
    {
        return this._cdAreaCelularFavorecido;
    } //-- int getCdAreaCelularFavorecido() 

    /**
     * Returns the value of field 'cdAreaFavorecidoCliente'.
     * 
     * @return int
     * @return the value of field 'cdAreaFavorecidoCliente'.
     */
    public int getCdAreaFavorecidoCliente()
    {
        return this._cdAreaFavorecidoCliente;
    } //-- int getCdAreaFavorecidoCliente() 

    /**
     * Returns the value of field 'cdAreaFaxFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdAreaFaxFavorecido'.
     */
    public int getCdAreaFaxFavorecido()
    {
        return this._cdAreaFaxFavorecido;
    } //-- int getCdAreaFaxFavorecido() 

    /**
     * Returns the value of field 'cdAreaFoneComercial'.
     * 
     * @return int
     * @return the value of field 'cdAreaFoneComercial'.
     */
    public int getCdAreaFoneComercial()
    {
        return this._cdAreaFoneComercial;
    } //-- int getCdAreaFoneComercial() 

    /**
     * Returns the value of field 'cdCelularFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'cdCelularFavorecidoCliente'.
     */
    public java.lang.String getCdCelularFavorecidoCliente()
    {
        return this._cdCelularFavorecidoCliente;
    } //-- java.lang.String getCdCelularFavorecidoCliente() 

    /**
     * Returns the value of field 'cdCepCorrespondenciaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdCepCorrespondenciaFavorecido'.
     */
    public int getCdCepCorrespondenciaFavorecido()
    {
        return this._cdCepCorrespondenciaFavorecido;
    } //-- int getCdCepCorrespondenciaFavorecido() 

    /**
     * Returns the value of field 'cdCepFavorecidoCliente'.
     * 
     * @return int
     * @return the value of field 'cdCepFavorecidoCliente'.
     */
    public int getCdCepFavorecidoCliente()
    {
        return this._cdCepFavorecidoCliente;
    } //-- int getCdCepFavorecidoCliente() 

    /**
     * Returns the value of field
     * 'cdComplementoCepCorrespondencia'.
     * 
     * @return int
     * @return the value of field 'cdComplementoCepCorrespondencia'.
     */
    public int getCdComplementoCepCorrespondencia()
    {
        return this._cdComplementoCepCorrespondencia;
    } //-- int getCdComplementoCepCorrespondencia() 

    /**
     * Returns the value of field 'cdComplementoCepFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdComplementoCepFavorecido'.
     */
    public int getCdComplementoCepFavorecido()
    {
        return this._cdComplementoCepFavorecido;
    } //-- int getCdComplementoCepFavorecido() 

    /**
     * Returns the value of field 'cdEstadoCivilFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdEstadoCivilFavorecido'.
     */
    public int getCdEstadoCivilFavorecido()
    {
        return this._cdEstadoCivilFavorecido;
    } //-- int getCdEstadoCivilFavorecido() 

    /**
     * Returns the value of field 'cdFaxFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'cdFaxFavorecidoCliente'.
     */
    public java.lang.String getCdFaxFavorecidoCliente()
    {
        return this._cdFaxFavorecidoCliente;
    } //-- java.lang.String getCdFaxFavorecidoCliente() 

    /**
     * Returns the value of field 'cdFoneComercialFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdFoneComercialFavorecido'.
     */
    public java.lang.String getCdFoneComercialFavorecido()
    {
        return this._cdFoneComercialFavorecido;
    } //-- java.lang.String getCdFoneComercialFavorecido() 

    /**
     * Returns the value of field 'cdFoneFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'cdFoneFavorecidoCliente'.
     */
    public java.lang.String getCdFoneFavorecidoCliente()
    {
        return this._cdFoneFavorecidoCliente;
    } //-- java.lang.String getCdFoneFavorecidoCliente() 

    /**
     * Returns the value of field 'cdIndicadorFavorecidoInativo'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorFavorecidoInativo'.
     */
    public int getCdIndicadorFavorecidoInativo()
    {
        return this._cdIndicadorFavorecidoInativo;
    } //-- int getCdIndicadorFavorecidoInativo() 

    /**
     * Returns the value of field 'cdIndicadorRetornoCliente'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRetornoCliente'.
     */
    public int getCdIndicadorRetornoCliente()
    {
        return this._cdIndicadorRetornoCliente;
    } //-- int getCdIndicadorRetornoCliente() 

    /**
     * Returns the value of field 'cdInscricaoEstadoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoEstadoFavorecido'.
     */
    public long getCdInscricaoEstadoFavorecido()
    {
        return this._cdInscricaoEstadoFavorecido;
    } //-- long getCdInscricaoEstadoFavorecido() 

    /**
     * Returns the value of field 'cdInscricaoFavorecidoCliente'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecidoCliente'.
     */
    public long getCdInscricaoFavorecidoCliente()
    {
        return this._cdInscricaoFavorecidoCliente;
    } //-- long getCdInscricaoFavorecidoCliente() 

    /**
     * Returns the value of field 'cdMotivoBloqueioFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdMotivoBloqueioFavorecido'.
     */
    public int getCdMotivoBloqueioFavorecido()
    {
        return this._cdMotivoBloqueioFavorecido;
    } //-- int getCdMotivoBloqueioFavorecido() 

    /**
     * Returns the value of field 'cdNitFavorecidoCliente'.
     * 
     * @return long
     * @return the value of field 'cdNitFavorecidoCliente'.
     */
    public long getCdNitFavorecidoCliente()
    {
        return this._cdNitFavorecidoCliente;
    } //-- long getCdNitFavorecidoCliente() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdRamalComercialFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdRamalComercialFavorecido'.
     */
    public int getCdRamalComercialFavorecido()
    {
        return this._cdRamalComercialFavorecido;
    } //-- int getCdRamalComercialFavorecido() 

    /**
     * Returns the value of field 'cdRamalFaxFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdRamalFaxFavorecido'.
     */
    public int getCdRamalFaxFavorecido()
    {
        return this._cdRamalFaxFavorecido;
    } //-- int getCdRamalFaxFavorecido() 

    /**
     * Returns the value of field 'cdSexoFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'cdSexoFavorecidoCliente'.
     */
    public java.lang.String getCdSexoFavorecidoCliente()
    {
        return this._cdSexoFavorecidoCliente;
    } //-- java.lang.String getCdSexoFavorecidoCliente() 

    /**
     * Returns the value of field 'cdSiglaEstadoCorrespondencia'.
     * 
     * @return String
     * @return the value of field 'cdSiglaEstadoCorrespondencia'.
     */
    public java.lang.String getCdSiglaEstadoCorrespondencia()
    {
        return this._cdSiglaEstadoCorrespondencia;
    } //-- java.lang.String getCdSiglaEstadoCorrespondencia() 

    /**
     * Returns the value of field 'cdSiglaEstadoFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'cdSiglaEstadoFavorecidoCliente'.
     */
    public java.lang.String getCdSiglaEstadoFavorecidoCliente()
    {
        return this._cdSiglaEstadoFavorecidoCliente;
    } //-- java.lang.String getCdSiglaEstadoFavorecidoCliente() 

    /**
     * Returns the value of field 'cdSiglaUfInscricaoEstadual'.
     * 
     * @return String
     * @return the value of field 'cdSiglaUfInscricaoEstadual'.
     */
    public java.lang.String getCdSiglaUfInscricaoEstadual()
    {
        return this._cdSiglaUfInscricaoEstadual;
    } //-- java.lang.String getCdSiglaUfInscricaoEstadual() 

    /**
     * Returns the value of field 'cdSituacaoFavorecidoCliente'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoFavorecidoCliente'.
     */
    public int getCdSituacaoFavorecidoCliente()
    {
        return this._cdSituacaoFavorecidoCliente;
    } //-- int getCdSituacaoFavorecidoCliente() 

    /**
     * Returns the value of field 'cdSmsFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'cdSmsFavorecidoCliente'.
     */
    public java.lang.String getCdSmsFavorecidoCliente()
    {
        return this._cdSmsFavorecidoCliente;
    } //-- java.lang.String getCdSmsFavorecidoCliente() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoFavorecido'.
     */
    public int getCdTipoFavorecido()
    {
        return this._cdTipoFavorecido;
    } //-- int getCdTipoFavorecido() 

    /**
     * Returns the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoFavorecido'.
     */
    public int getCdTipoInscricaoFavorecido()
    {
        return this._cdTipoInscricaoFavorecido;
    } //-- int getCdTipoInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdUfEmpresaFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdUfEmpresaFavorecido'.
     */
    public java.lang.String getCdUfEmpresaFavorecido()
    {
        return this._cdUfEmpresaFavorecido;
    } //-- java.lang.String getCdUfEmpresaFavorecido() 

    /**
     * Returns the value of field 'cdUfNascimentoFavorecido'.
     * 
     * @return String
     * @return the value of field 'cdUfNascimentoFavorecido'.
     */
    public java.lang.String getCdUfNascimentoFavorecido()
    {
        return this._cdUfNascimentoFavorecido;
    } //-- java.lang.String getCdUfNascimentoFavorecido() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterna'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterna'.
     */
    public java.lang.String getCdUsuarioInclusaoExterna()
    {
        return this._cdUsuarioInclusaoExterna;
    } //-- java.lang.String getCdUsuarioInclusaoExterna() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterna'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterna'.
     */
    public java.lang.String getCdUsuarioManutencaoExterna()
    {
        return this._cdUsuarioManutencaoExterna;
    } //-- java.lang.String getCdUsuarioManutencaoExterna() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAbreviacaoFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsAbreviacaoFavorecidoCliente'.
     */
    public java.lang.String getDsAbreviacaoFavorecidoCliente()
    {
        return this._dsAbreviacaoFavorecidoCliente;
    } //-- java.lang.String getDsAbreviacaoFavorecidoCliente() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsCanalinclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalinclusao'.
     */
    public java.lang.String getDsCanalinclusao()
    {
        return this._dsCanalinclusao;
    } //-- java.lang.String getDsCanalinclusao() 

    /**
     * Returns the value of field 'dsCargoFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsCargoFavorecidoCliente'.
     */
    public java.lang.String getDsCargoFavorecidoCliente()
    {
        return this._dsCargoFavorecidoCliente;
    } //-- java.lang.String getDsCargoFavorecidoCliente() 

    /**
     * Returns the value of field 'dsCompletaFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsCompletaFavorecidoCliente'.
     */
    public java.lang.String getDsCompletaFavorecidoCliente()
    {
        return this._dsCompletaFavorecidoCliente;
    } //-- java.lang.String getDsCompletaFavorecidoCliente() 

    /**
     * Returns the value of field 'dsConjugeFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsConjugeFavorecidoCliente'.
     */
    public java.lang.String getDsConjugeFavorecidoCliente()
    {
        return this._dsConjugeFavorecidoCliente;
    } //-- java.lang.String getDsConjugeFavorecidoCliente() 

    /**
     * Returns the value of field 'dsEmpresaFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsEmpresaFavorecidoCliente'.
     */
    public java.lang.String getDsEmpresaFavorecidoCliente()
    {
        return this._dsEmpresaFavorecidoCliente;
    } //-- java.lang.String getDsEmpresaFavorecidoCliente() 

    /**
     * Returns the value of field
     * 'dsEstadoCorrespondenciaFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'dsEstadoCorrespondenciaFavorecido'.
     */
    public java.lang.String getDsEstadoCorrespondenciaFavorecido()
    {
        return this._dsEstadoCorrespondenciaFavorecido;
    } //-- java.lang.String getDsEstadoCorrespondenciaFavorecido() 

    /**
     * Returns the value of field 'dsEstadoFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsEstadoFavorecidoCliente'.
     */
    public java.lang.String getDsEstadoFavorecidoCliente()
    {
        return this._dsEstadoFavorecidoCliente;
    } //-- java.lang.String getDsEstadoFavorecidoCliente() 

    /**
     * Returns the value of field 'dsInscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsInscricaoFavorecido'.
     */
    public java.lang.String getDsInscricaoFavorecido()
    {
        return this._dsInscricaoFavorecido;
    } //-- java.lang.String getDsInscricaoFavorecido() 

    /**
     * Returns the value of field 'dsMaeFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsMaeFavorecidoCliente'.
     */
    public java.lang.String getDsMaeFavorecidoCliente()
    {
        return this._dsMaeFavorecidoCliente;
    } //-- java.lang.String getDsMaeFavorecidoCliente() 

    /**
     * Returns the value of field 'dsMunicipalInscricaoEstado'.
     * 
     * @return String
     * @return the value of field 'dsMunicipalInscricaoEstado'.
     */
    public java.lang.String getDsMunicipalInscricaoEstado()
    {
        return this._dsMunicipalInscricaoEstado;
    } //-- java.lang.String getDsMunicipalInscricaoEstado() 

    /**
     * Returns the value of field 'dsMunicipioEmpresaFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsMunicipioEmpresaFavorecido'.
     */
    public java.lang.String getDsMunicipioEmpresaFavorecido()
    {
        return this._dsMunicipioEmpresaFavorecido;
    } //-- java.lang.String getDsMunicipioEmpresaFavorecido() 

    /**
     * Returns the value of field 'dsMunicipioFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsMunicipioFavorecidoCliente'.
     */
    public java.lang.String getDsMunicipioFavorecidoCliente()
    {
        return this._dsMunicipioFavorecidoCliente;
    } //-- java.lang.String getDsMunicipioFavorecidoCliente() 

    /**
     * Returns the value of field
     * 'dsMunicipioNascimentoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsMunicipioNascimentoFavorecido'.
     */
    public java.lang.String getDsMunicipioNascimentoFavorecido()
    {
        return this._dsMunicipioNascimentoFavorecido;
    } //-- java.lang.String getDsMunicipioNascimentoFavorecido() 

    /**
     * Returns the value of field
     * 'dsNacionalidadeFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsNacionalidadeFavorecidoCliente'
     */
    public java.lang.String getDsNacionalidadeFavorecidoCliente()
    {
        return this._dsNacionalidadeFavorecidoCliente;
    } //-- java.lang.String getDsNacionalidadeFavorecidoCliente() 

    /**
     * Returns the value of field 'dsObservacaoControleFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsObservacaoControleFavorecido'.
     */
    public java.lang.String getDsObservacaoControleFavorecido()
    {
        return this._dsObservacaoControleFavorecido;
    } //-- java.lang.String getDsObservacaoControleFavorecido() 

    /**
     * Returns the value of field 'dsOrganizacaoEmissorDocumento'.
     * 
     * @return String
     * @return the value of field 'dsOrganizacaoEmissorDocumento'.
     */
    public java.lang.String getDsOrganizacaoEmissorDocumento()
    {
        return this._dsOrganizacaoEmissorDocumento;
    } //-- java.lang.String getDsOrganizacaoEmissorDocumento() 

    /**
     * Returns the value of field 'dsPaiFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsPaiFavorecidoCliente'.
     */
    public java.lang.String getDsPaiFavorecidoCliente()
    {
        return this._dsPaiFavorecidoCliente;
    } //-- java.lang.String getDsPaiFavorecidoCliente() 

    /**
     * Returns the value of field 'dsProfissaoFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dsProfissaoFavorecidoCliente'.
     */
    public java.lang.String getDsProfissaoFavorecidoCliente()
    {
        return this._dsProfissaoFavorecidoCliente;
    } //-- java.lang.String getDsProfissaoFavorecidoCliente() 

    /**
     * Returns the value of field 'dsRetornoCliente'.
     * 
     * @return String
     * @return the value of field 'dsRetornoCliente'.
     */
    public java.lang.String getDsRetornoCliente()
    {
        return this._dsRetornoCliente;
    } //-- java.lang.String getDsRetornoCliente() 

    /**
     * Returns the value of field 'dsTipoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTipoFavorecido'.
     */
    public java.lang.String getDsTipoFavorecido()
    {
        return this._dsTipoFavorecido;
    } //-- java.lang.String getDsTipoFavorecido() 

    /**
     * Returns the value of field 'dsUfEmpreFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsUfEmpreFavorecido'.
     */
    public java.lang.String getDsUfEmpreFavorecido()
    {
        return this._dsUfEmpreFavorecido;
    } //-- java.lang.String getDsUfEmpreFavorecido() 

    /**
     * Returns the value of field 'dsUfNascimentoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsUfNascimentoFavorecido'.
     */
    public java.lang.String getDsUfNascimentoFavorecido()
    {
        return this._dsUfNascimentoFavorecido;
    } //-- java.lang.String getDsUfNascimentoFavorecido() 

    /**
     * Returns the value of field
     * 'dtAtualizarEnderecoCorrespondencia'.
     * 
     * @return String
     * @return the value of field
     * 'dtAtualizarEnderecoCorrespondencia'.
     */
    public java.lang.String getDtAtualizarEnderecoCorrespondencia()
    {
        return this._dtAtualizarEnderecoCorrespondencia;
    } //-- java.lang.String getDtAtualizarEnderecoCorrespondencia() 

    /**
     * Returns the value of field 'dtAtualizarEnderecoResidencial'.
     * 
     * @return String
     * @return the value of field 'dtAtualizarEnderecoResidencial'.
     */
    public java.lang.String getDtAtualizarEnderecoResidencial()
    {
        return this._dtAtualizarEnderecoResidencial;
    } //-- java.lang.String getDtAtualizarEnderecoResidencial() 

    /**
     * Returns the value of field 'dtExpedicaoDocumentoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dtExpedicaoDocumentoFavorecido'.
     */
    public java.lang.String getDtExpedicaoDocumentoFavorecido()
    {
        return this._dtExpedicaoDocumentoFavorecido;
    } //-- java.lang.String getDtExpedicaoDocumentoFavorecido() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'dtNascimentoFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'dtNascimentoFavorecidoCliente'.
     */
    public java.lang.String getDtNascimentoFavorecidoCliente()
    {
        return this._dtNascimentoFavorecidoCliente;
    } //-- java.lang.String getDtNascimentoFavorecidoCliente() 

    /**
     * Returns the value of field 'dtUltimaMovimentacaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dtUltimaMovimentacaoFavorecido'.
     */
    public java.lang.String getDtUltimaMovimentacaoFavorecido()
    {
        return this._dtUltimaMovimentacaoFavorecido;
    } //-- java.lang.String getDtUltimaMovimentacaoFavorecido() 

    /**
     * Returns the value of field
     * 'enBairroCorrespondenciaFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'enBairroCorrespondenciaFavorecido'.
     */
    public java.lang.String getEnBairroCorrespondenciaFavorecido()
    {
        return this._enBairroCorrespondenciaFavorecido;
    } //-- java.lang.String getEnBairroCorrespondenciaFavorecido() 

    /**
     * Returns the value of field 'enBairroFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'enBairroFavorecidoCliente'.
     */
    public java.lang.String getEnBairroFavorecidoCliente()
    {
        return this._enBairroFavorecidoCliente;
    } //-- java.lang.String getEnBairroFavorecidoCliente() 

    /**
     * Returns the value of field
     * 'enComplementoLogradouroCorrespondencia'.
     * 
     * @return String
     * @return the value of field
     * 'enComplementoLogradouroCorrespondencia'.
     */
    public java.lang.String getEnComplementoLogradouroCorrespondencia()
    {
        return this._enComplementoLogradouroCorrespondencia;
    } //-- java.lang.String getEnComplementoLogradouroCorrespondencia() 

    /**
     * Returns the value of field
     * 'enComplementoLogradouroFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'enComplementoLogradouroFavorecido'.
     */
    public java.lang.String getEnComplementoLogradouroFavorecido()
    {
        return this._enComplementoLogradouroFavorecido;
    } //-- java.lang.String getEnComplementoLogradouroFavorecido() 

    /**
     * Returns the value of field 'enEmailComercialFavorecido'.
     * 
     * @return String
     * @return the value of field 'enEmailComercialFavorecido'.
     */
    public java.lang.String getEnEmailComercialFavorecido()
    {
        return this._enEmailComercialFavorecido;
    } //-- java.lang.String getEnEmailComercialFavorecido() 

    /**
     * Returns the value of field 'enEmailFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'enEmailFavorecidoCliente'.
     */
    public java.lang.String getEnEmailFavorecidoCliente()
    {
        return this._enEmailFavorecidoCliente;
    } //-- java.lang.String getEnEmailFavorecidoCliente() 

    /**
     * Returns the value of field 'enLogradouroFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'enLogradouroFavorecidoCliente'.
     */
    public java.lang.String getEnLogradouroFavorecidoCliente()
    {
        return this._enLogradouroFavorecidoCliente;
    } //-- java.lang.String getEnLogradouroFavorecidoCliente() 

    /**
     * Returns the value of field
     * 'enLogradrouroCorrespondenciaFavorecido'.
     * 
     * @return String
     * @return the value of field
     * 'enLogradrouroCorrespondenciaFavorecido'.
     */
    public java.lang.String getEnLogradrouroCorrespondenciaFavorecido()
    {
        return this._enLogradrouroCorrespondenciaFavorecido;
    } //-- java.lang.String getEnLogradrouroCorrespondenciaFavorecido() 

    /**
     * Returns the value of field
     * 'enNumeroLogradouroCorrespondencia'.
     * 
     * @return String
     * @return the value of field
     * 'enNumeroLogradouroCorrespondencia'.
     */
    public java.lang.String getEnNumeroLogradouroCorrespondencia()
    {
        return this._enNumeroLogradouroCorrespondencia;
    } //-- java.lang.String getEnNumeroLogradouroCorrespondencia() 

    /**
     * Returns the value of field 'enNumeroLogradouroFavorecido'.
     * 
     * @return String
     * @return the value of field 'enNumeroLogradouroFavorecido'.
     */
    public java.lang.String getEnNumeroLogradouroFavorecido()
    {
        return this._enNumeroLogradouroFavorecido;
    } //-- java.lang.String getEnNumeroLogradouroFavorecido() 

    /**
     * Returns the value of field 'hrBloqueioFavorecidoCliente'.
     * 
     * @return String
     * @return the value of field 'hrBloqueioFavorecidoCliente'.
     */
    public java.lang.String getHrBloqueioFavorecidoCliente()
    {
        return this._hrBloqueioFavorecidoCliente;
    } //-- java.lang.String getHrBloqueioFavorecidoCliente() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'mtBloqueio'.
     * 
     * @return String
     * @return the value of field 'mtBloqueio'.
     */
    public java.lang.String getMtBloqueio()
    {
        return this._mtBloqueio;
    } //-- java.lang.String getMtBloqueio() 

    /**
     * Returns the value of field 'vlRendaMensalFavorecido'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlRendaMensalFavorecido'.
     */
    public java.math.BigDecimal getVlRendaMensalFavorecido()
    {
        return this._vlRendaMensalFavorecido;
    } //-- java.math.BigDecimal getVlRendaMensalFavorecido() 

    /**
     * Method hasCdAcaoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoManutencao()
    {
        return this._has_cdAcaoManutencao;
    } //-- boolean hasCdAcaoManutencao() 

    /**
     * Method hasCdAreaCelularFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaCelularFavorecido()
    {
        return this._has_cdAreaCelularFavorecido;
    } //-- boolean hasCdAreaCelularFavorecido() 

    /**
     * Method hasCdAreaFavorecidoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaFavorecidoCliente()
    {
        return this._has_cdAreaFavorecidoCliente;
    } //-- boolean hasCdAreaFavorecidoCliente() 

    /**
     * Method hasCdAreaFaxFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaFaxFavorecido()
    {
        return this._has_cdAreaFaxFavorecido;
    } //-- boolean hasCdAreaFaxFavorecido() 

    /**
     * Method hasCdAreaFoneComercial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAreaFoneComercial()
    {
        return this._has_cdAreaFoneComercial;
    } //-- boolean hasCdAreaFoneComercial() 

    /**
     * Method hasCdCepCorrespondenciaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepCorrespondenciaFavorecido()
    {
        return this._has_cdCepCorrespondenciaFavorecido;
    } //-- boolean hasCdCepCorrespondenciaFavorecido() 

    /**
     * Method hasCdCepFavorecidoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCepFavorecidoCliente()
    {
        return this._has_cdCepFavorecidoCliente;
    } //-- boolean hasCdCepFavorecidoCliente() 

    /**
     * Method hasCdComplementoCepCorrespondencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdComplementoCepCorrespondencia()
    {
        return this._has_cdComplementoCepCorrespondencia;
    } //-- boolean hasCdComplementoCepCorrespondencia() 

    /**
     * Method hasCdComplementoCepFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdComplementoCepFavorecido()
    {
        return this._has_cdComplementoCepFavorecido;
    } //-- boolean hasCdComplementoCepFavorecido() 

    /**
     * Method hasCdEstadoCivilFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEstadoCivilFavorecido()
    {
        return this._has_cdEstadoCivilFavorecido;
    } //-- boolean hasCdEstadoCivilFavorecido() 

    /**
     * Method hasCdIndicadorFavorecidoInativo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorFavorecidoInativo()
    {
        return this._has_cdIndicadorFavorecidoInativo;
    } //-- boolean hasCdIndicadorFavorecidoInativo() 

    /**
     * Method hasCdIndicadorRetornoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRetornoCliente()
    {
        return this._has_cdIndicadorRetornoCliente;
    } //-- boolean hasCdIndicadorRetornoCliente() 

    /**
     * Method hasCdInscricaoEstadoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoEstadoFavorecido()
    {
        return this._has_cdInscricaoEstadoFavorecido;
    } //-- boolean hasCdInscricaoEstadoFavorecido() 

    /**
     * Method hasCdInscricaoFavorecidoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecidoCliente()
    {
        return this._has_cdInscricaoFavorecidoCliente;
    } //-- boolean hasCdInscricaoFavorecidoCliente() 

    /**
     * Method hasCdMotivoBloqueioFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoBloqueioFavorecido()
    {
        return this._has_cdMotivoBloqueioFavorecido;
    } //-- boolean hasCdMotivoBloqueioFavorecido() 

    /**
     * Method hasCdNitFavorecidoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNitFavorecidoCliente()
    {
        return this._has_cdNitFavorecidoCliente;
    } //-- boolean hasCdNitFavorecidoCliente() 

    /**
     * Method hasCdRamalComercialFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRamalComercialFavorecido()
    {
        return this._has_cdRamalComercialFavorecido;
    } //-- boolean hasCdRamalComercialFavorecido() 

    /**
     * Method hasCdRamalFaxFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRamalFaxFavorecido()
    {
        return this._has_cdRamalFaxFavorecido;
    } //-- boolean hasCdRamalFaxFavorecido() 

    /**
     * Method hasCdSituacaoFavorecidoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoFavorecidoCliente()
    {
        return this._has_cdSituacaoFavorecidoCliente;
    } //-- boolean hasCdSituacaoFavorecidoCliente() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoFavorecido()
    {
        return this._has_cdTipoFavorecido;
    } //-- boolean hasCdTipoFavorecido() 

    /**
     * Method hasCdTipoInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoFavorecido()
    {
        return this._has_cdTipoInscricaoFavorecido;
    } //-- boolean hasCdTipoInscricaoFavorecido() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoManutencao'.
     * 
     * @param cdAcaoManutencao the value of field 'cdAcaoManutencao'
     */
    public void setCdAcaoManutencao(int cdAcaoManutencao)
    {
        this._cdAcaoManutencao = cdAcaoManutencao;
        this._has_cdAcaoManutencao = true;
    } //-- void setCdAcaoManutencao(int) 

    /**
     * Sets the value of field 'cdAreaCelularFavorecido'.
     * 
     * @param cdAreaCelularFavorecido the value of field
     * 'cdAreaCelularFavorecido'.
     */
    public void setCdAreaCelularFavorecido(int cdAreaCelularFavorecido)
    {
        this._cdAreaCelularFavorecido = cdAreaCelularFavorecido;
        this._has_cdAreaCelularFavorecido = true;
    } //-- void setCdAreaCelularFavorecido(int) 

    /**
     * Sets the value of field 'cdAreaFavorecidoCliente'.
     * 
     * @param cdAreaFavorecidoCliente the value of field
     * 'cdAreaFavorecidoCliente'.
     */
    public void setCdAreaFavorecidoCliente(int cdAreaFavorecidoCliente)
    {
        this._cdAreaFavorecidoCliente = cdAreaFavorecidoCliente;
        this._has_cdAreaFavorecidoCliente = true;
    } //-- void setCdAreaFavorecidoCliente(int) 

    /**
     * Sets the value of field 'cdAreaFaxFavorecido'.
     * 
     * @param cdAreaFaxFavorecido the value of field
     * 'cdAreaFaxFavorecido'.
     */
    public void setCdAreaFaxFavorecido(int cdAreaFaxFavorecido)
    {
        this._cdAreaFaxFavorecido = cdAreaFaxFavorecido;
        this._has_cdAreaFaxFavorecido = true;
    } //-- void setCdAreaFaxFavorecido(int) 

    /**
     * Sets the value of field 'cdAreaFoneComercial'.
     * 
     * @param cdAreaFoneComercial the value of field
     * 'cdAreaFoneComercial'.
     */
    public void setCdAreaFoneComercial(int cdAreaFoneComercial)
    {
        this._cdAreaFoneComercial = cdAreaFoneComercial;
        this._has_cdAreaFoneComercial = true;
    } //-- void setCdAreaFoneComercial(int) 

    /**
     * Sets the value of field 'cdCelularFavorecidoCliente'.
     * 
     * @param cdCelularFavorecidoCliente the value of field
     * 'cdCelularFavorecidoCliente'.
     */
    public void setCdCelularFavorecidoCliente(java.lang.String cdCelularFavorecidoCliente)
    {
        this._cdCelularFavorecidoCliente = cdCelularFavorecidoCliente;
    } //-- void setCdCelularFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'cdCepCorrespondenciaFavorecido'.
     * 
     * @param cdCepCorrespondenciaFavorecido the value of field
     * 'cdCepCorrespondenciaFavorecido'.
     */
    public void setCdCepCorrespondenciaFavorecido(int cdCepCorrespondenciaFavorecido)
    {
        this._cdCepCorrespondenciaFavorecido = cdCepCorrespondenciaFavorecido;
        this._has_cdCepCorrespondenciaFavorecido = true;
    } //-- void setCdCepCorrespondenciaFavorecido(int) 

    /**
     * Sets the value of field 'cdCepFavorecidoCliente'.
     * 
     * @param cdCepFavorecidoCliente the value of field
     * 'cdCepFavorecidoCliente'.
     */
    public void setCdCepFavorecidoCliente(int cdCepFavorecidoCliente)
    {
        this._cdCepFavorecidoCliente = cdCepFavorecidoCliente;
        this._has_cdCepFavorecidoCliente = true;
    } //-- void setCdCepFavorecidoCliente(int) 

    /**
     * Sets the value of field 'cdComplementoCepCorrespondencia'.
     * 
     * @param cdComplementoCepCorrespondencia the value of field
     * 'cdComplementoCepCorrespondencia'.
     */
    public void setCdComplementoCepCorrespondencia(int cdComplementoCepCorrespondencia)
    {
        this._cdComplementoCepCorrespondencia = cdComplementoCepCorrespondencia;
        this._has_cdComplementoCepCorrespondencia = true;
    } //-- void setCdComplementoCepCorrespondencia(int) 

    /**
     * Sets the value of field 'cdComplementoCepFavorecido'.
     * 
     * @param cdComplementoCepFavorecido the value of field
     * 'cdComplementoCepFavorecido'.
     */
    public void setCdComplementoCepFavorecido(int cdComplementoCepFavorecido)
    {
        this._cdComplementoCepFavorecido = cdComplementoCepFavorecido;
        this._has_cdComplementoCepFavorecido = true;
    } //-- void setCdComplementoCepFavorecido(int) 

    /**
     * Sets the value of field 'cdEstadoCivilFavorecido'.
     * 
     * @param cdEstadoCivilFavorecido the value of field
     * 'cdEstadoCivilFavorecido'.
     */
    public void setCdEstadoCivilFavorecido(int cdEstadoCivilFavorecido)
    {
        this._cdEstadoCivilFavorecido = cdEstadoCivilFavorecido;
        this._has_cdEstadoCivilFavorecido = true;
    } //-- void setCdEstadoCivilFavorecido(int) 

    /**
     * Sets the value of field 'cdFaxFavorecidoCliente'.
     * 
     * @param cdFaxFavorecidoCliente the value of field
     * 'cdFaxFavorecidoCliente'.
     */
    public void setCdFaxFavorecidoCliente(java.lang.String cdFaxFavorecidoCliente)
    {
        this._cdFaxFavorecidoCliente = cdFaxFavorecidoCliente;
    } //-- void setCdFaxFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'cdFoneComercialFavorecido'.
     * 
     * @param cdFoneComercialFavorecido the value of field
     * 'cdFoneComercialFavorecido'.
     */
    public void setCdFoneComercialFavorecido(java.lang.String cdFoneComercialFavorecido)
    {
        this._cdFoneComercialFavorecido = cdFoneComercialFavorecido;
    } //-- void setCdFoneComercialFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdFoneFavorecidoCliente'.
     * 
     * @param cdFoneFavorecidoCliente the value of field
     * 'cdFoneFavorecidoCliente'.
     */
    public void setCdFoneFavorecidoCliente(java.lang.String cdFoneFavorecidoCliente)
    {
        this._cdFoneFavorecidoCliente = cdFoneFavorecidoCliente;
    } //-- void setCdFoneFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorFavorecidoInativo'.
     * 
     * @param cdIndicadorFavorecidoInativo the value of field
     * 'cdIndicadorFavorecidoInativo'.
     */
    public void setCdIndicadorFavorecidoInativo(int cdIndicadorFavorecidoInativo)
    {
        this._cdIndicadorFavorecidoInativo = cdIndicadorFavorecidoInativo;
        this._has_cdIndicadorFavorecidoInativo = true;
    } //-- void setCdIndicadorFavorecidoInativo(int) 

    /**
     * Sets the value of field 'cdIndicadorRetornoCliente'.
     * 
     * @param cdIndicadorRetornoCliente the value of field
     * 'cdIndicadorRetornoCliente'.
     */
    public void setCdIndicadorRetornoCliente(int cdIndicadorRetornoCliente)
    {
        this._cdIndicadorRetornoCliente = cdIndicadorRetornoCliente;
        this._has_cdIndicadorRetornoCliente = true;
    } //-- void setCdIndicadorRetornoCliente(int) 

    /**
     * Sets the value of field 'cdInscricaoEstadoFavorecido'.
     * 
     * @param cdInscricaoEstadoFavorecido the value of field
     * 'cdInscricaoEstadoFavorecido'.
     */
    public void setCdInscricaoEstadoFavorecido(long cdInscricaoEstadoFavorecido)
    {
        this._cdInscricaoEstadoFavorecido = cdInscricaoEstadoFavorecido;
        this._has_cdInscricaoEstadoFavorecido = true;
    } //-- void setCdInscricaoEstadoFavorecido(long) 

    /**
     * Sets the value of field 'cdInscricaoFavorecidoCliente'.
     * 
     * @param cdInscricaoFavorecidoCliente the value of field
     * 'cdInscricaoFavorecidoCliente'.
     */
    public void setCdInscricaoFavorecidoCliente(long cdInscricaoFavorecidoCliente)
    {
        this._cdInscricaoFavorecidoCliente = cdInscricaoFavorecidoCliente;
        this._has_cdInscricaoFavorecidoCliente = true;
    } //-- void setCdInscricaoFavorecidoCliente(long) 

    /**
     * Sets the value of field 'cdMotivoBloqueioFavorecido'.
     * 
     * @param cdMotivoBloqueioFavorecido the value of field
     * 'cdMotivoBloqueioFavorecido'.
     */
    public void setCdMotivoBloqueioFavorecido(int cdMotivoBloqueioFavorecido)
    {
        this._cdMotivoBloqueioFavorecido = cdMotivoBloqueioFavorecido;
        this._has_cdMotivoBloqueioFavorecido = true;
    } //-- void setCdMotivoBloqueioFavorecido(int) 

    /**
     * Sets the value of field 'cdNitFavorecidoCliente'.
     * 
     * @param cdNitFavorecidoCliente the value of field
     * 'cdNitFavorecidoCliente'.
     */
    public void setCdNitFavorecidoCliente(long cdNitFavorecidoCliente)
    {
        this._cdNitFavorecidoCliente = cdNitFavorecidoCliente;
        this._has_cdNitFavorecidoCliente = true;
    } //-- void setCdNitFavorecidoCliente(long) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdRamalComercialFavorecido'.
     * 
     * @param cdRamalComercialFavorecido the value of field
     * 'cdRamalComercialFavorecido'.
     */
    public void setCdRamalComercialFavorecido(int cdRamalComercialFavorecido)
    {
        this._cdRamalComercialFavorecido = cdRamalComercialFavorecido;
        this._has_cdRamalComercialFavorecido = true;
    } //-- void setCdRamalComercialFavorecido(int) 

    /**
     * Sets the value of field 'cdRamalFaxFavorecido'.
     * 
     * @param cdRamalFaxFavorecido the value of field
     * 'cdRamalFaxFavorecido'.
     */
    public void setCdRamalFaxFavorecido(int cdRamalFaxFavorecido)
    {
        this._cdRamalFaxFavorecido = cdRamalFaxFavorecido;
        this._has_cdRamalFaxFavorecido = true;
    } //-- void setCdRamalFaxFavorecido(int) 

    /**
     * Sets the value of field 'cdSexoFavorecidoCliente'.
     * 
     * @param cdSexoFavorecidoCliente the value of field
     * 'cdSexoFavorecidoCliente'.
     */
    public void setCdSexoFavorecidoCliente(java.lang.String cdSexoFavorecidoCliente)
    {
        this._cdSexoFavorecidoCliente = cdSexoFavorecidoCliente;
    } //-- void setCdSexoFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'cdSiglaEstadoCorrespondencia'.
     * 
     * @param cdSiglaEstadoCorrespondencia the value of field
     * 'cdSiglaEstadoCorrespondencia'.
     */
    public void setCdSiglaEstadoCorrespondencia(java.lang.String cdSiglaEstadoCorrespondencia)
    {
        this._cdSiglaEstadoCorrespondencia = cdSiglaEstadoCorrespondencia;
    } //-- void setCdSiglaEstadoCorrespondencia(java.lang.String) 

    /**
     * Sets the value of field 'cdSiglaEstadoFavorecidoCliente'.
     * 
     * @param cdSiglaEstadoFavorecidoCliente the value of field
     * 'cdSiglaEstadoFavorecidoCliente'.
     */
    public void setCdSiglaEstadoFavorecidoCliente(java.lang.String cdSiglaEstadoFavorecidoCliente)
    {
        this._cdSiglaEstadoFavorecidoCliente = cdSiglaEstadoFavorecidoCliente;
    } //-- void setCdSiglaEstadoFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'cdSiglaUfInscricaoEstadual'.
     * 
     * @param cdSiglaUfInscricaoEstadual the value of field
     * 'cdSiglaUfInscricaoEstadual'.
     */
    public void setCdSiglaUfInscricaoEstadual(java.lang.String cdSiglaUfInscricaoEstadual)
    {
        this._cdSiglaUfInscricaoEstadual = cdSiglaUfInscricaoEstadual;
    } //-- void setCdSiglaUfInscricaoEstadual(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoFavorecidoCliente'.
     * 
     * @param cdSituacaoFavorecidoCliente the value of field
     * 'cdSituacaoFavorecidoCliente'.
     */
    public void setCdSituacaoFavorecidoCliente(int cdSituacaoFavorecidoCliente)
    {
        this._cdSituacaoFavorecidoCliente = cdSituacaoFavorecidoCliente;
        this._has_cdSituacaoFavorecidoCliente = true;
    } //-- void setCdSituacaoFavorecidoCliente(int) 

    /**
     * Sets the value of field 'cdSmsFavorecidoCliente'.
     * 
     * @param cdSmsFavorecidoCliente the value of field
     * 'cdSmsFavorecidoCliente'.
     */
    public void setCdSmsFavorecidoCliente(java.lang.String cdSmsFavorecidoCliente)
    {
        this._cdSmsFavorecidoCliente = cdSmsFavorecidoCliente;
    } //-- void setCdSmsFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoFavorecido'.
     * 
     * @param cdTipoFavorecido the value of field 'cdTipoFavorecido'
     */
    public void setCdTipoFavorecido(int cdTipoFavorecido)
    {
        this._cdTipoFavorecido = cdTipoFavorecido;
        this._has_cdTipoFavorecido = true;
    } //-- void setCdTipoFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoFavorecido'.
     * 
     * @param cdTipoInscricaoFavorecido the value of field
     * 'cdTipoInscricaoFavorecido'.
     */
    public void setCdTipoInscricaoFavorecido(int cdTipoInscricaoFavorecido)
    {
        this._cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
        this._has_cdTipoInscricaoFavorecido = true;
    } //-- void setCdTipoInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdUfEmpresaFavorecido'.
     * 
     * @param cdUfEmpresaFavorecido the value of field
     * 'cdUfEmpresaFavorecido'.
     */
    public void setCdUfEmpresaFavorecido(java.lang.String cdUfEmpresaFavorecido)
    {
        this._cdUfEmpresaFavorecido = cdUfEmpresaFavorecido;
    } //-- void setCdUfEmpresaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdUfNascimentoFavorecido'.
     * 
     * @param cdUfNascimentoFavorecido the value of field
     * 'cdUfNascimentoFavorecido'.
     */
    public void setCdUfNascimentoFavorecido(java.lang.String cdUfNascimentoFavorecido)
    {
        this._cdUfNascimentoFavorecido = cdUfNascimentoFavorecido;
    } //-- void setCdUfNascimentoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterna'.
     * 
     * @param cdUsuarioInclusaoExterna the value of field
     * 'cdUsuarioInclusaoExterna'.
     */
    public void setCdUsuarioInclusaoExterna(java.lang.String cdUsuarioInclusaoExterna)
    {
        this._cdUsuarioInclusaoExterna = cdUsuarioInclusaoExterna;
    } //-- void setCdUsuarioInclusaoExterna(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterna'.
     * 
     * @param cdUsuarioManutencaoExterna the value of field
     * 'cdUsuarioManutencaoExterna'.
     */
    public void setCdUsuarioManutencaoExterna(java.lang.String cdUsuarioManutencaoExterna)
    {
        this._cdUsuarioManutencaoExterna = cdUsuarioManutencaoExterna;
    } //-- void setCdUsuarioManutencaoExterna(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAbreviacaoFavorecidoCliente'.
     * 
     * @param dsAbreviacaoFavorecidoCliente the value of field
     * 'dsAbreviacaoFavorecidoCliente'.
     */
    public void setDsAbreviacaoFavorecidoCliente(java.lang.String dsAbreviacaoFavorecidoCliente)
    {
        this._dsAbreviacaoFavorecidoCliente = dsAbreviacaoFavorecidoCliente;
    } //-- void setDsAbreviacaoFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalinclusao'.
     * 
     * @param dsCanalinclusao the value of field 'dsCanalinclusao'.
     */
    public void setDsCanalinclusao(java.lang.String dsCanalinclusao)
    {
        this._dsCanalinclusao = dsCanalinclusao;
    } //-- void setDsCanalinclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCargoFavorecidoCliente'.
     * 
     * @param dsCargoFavorecidoCliente the value of field
     * 'dsCargoFavorecidoCliente'.
     */
    public void setDsCargoFavorecidoCliente(java.lang.String dsCargoFavorecidoCliente)
    {
        this._dsCargoFavorecidoCliente = dsCargoFavorecidoCliente;
    } //-- void setDsCargoFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsCompletaFavorecidoCliente'.
     * 
     * @param dsCompletaFavorecidoCliente the value of field
     * 'dsCompletaFavorecidoCliente'.
     */
    public void setDsCompletaFavorecidoCliente(java.lang.String dsCompletaFavorecidoCliente)
    {
        this._dsCompletaFavorecidoCliente = dsCompletaFavorecidoCliente;
    } //-- void setDsCompletaFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsConjugeFavorecidoCliente'.
     * 
     * @param dsConjugeFavorecidoCliente the value of field
     * 'dsConjugeFavorecidoCliente'.
     */
    public void setDsConjugeFavorecidoCliente(java.lang.String dsConjugeFavorecidoCliente)
    {
        this._dsConjugeFavorecidoCliente = dsConjugeFavorecidoCliente;
    } //-- void setDsConjugeFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresaFavorecidoCliente'.
     * 
     * @param dsEmpresaFavorecidoCliente the value of field
     * 'dsEmpresaFavorecidoCliente'.
     */
    public void setDsEmpresaFavorecidoCliente(java.lang.String dsEmpresaFavorecidoCliente)
    {
        this._dsEmpresaFavorecidoCliente = dsEmpresaFavorecidoCliente;
    } //-- void setDsEmpresaFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsEstadoCorrespondenciaFavorecido'.
     * 
     * @param dsEstadoCorrespondenciaFavorecido the value of field
     * 'dsEstadoCorrespondenciaFavorecido'.
     */
    public void setDsEstadoCorrespondenciaFavorecido(java.lang.String dsEstadoCorrespondenciaFavorecido)
    {
        this._dsEstadoCorrespondenciaFavorecido = dsEstadoCorrespondenciaFavorecido;
    } //-- void setDsEstadoCorrespondenciaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsEstadoFavorecidoCliente'.
     * 
     * @param dsEstadoFavorecidoCliente the value of field
     * 'dsEstadoFavorecidoCliente'.
     */
    public void setDsEstadoFavorecidoCliente(java.lang.String dsEstadoFavorecidoCliente)
    {
        this._dsEstadoFavorecidoCliente = dsEstadoFavorecidoCliente;
    } //-- void setDsEstadoFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsInscricaoFavorecido'.
     * 
     * @param dsInscricaoFavorecido the value of field
     * 'dsInscricaoFavorecido'.
     */
    public void setDsInscricaoFavorecido(java.lang.String dsInscricaoFavorecido)
    {
        this._dsInscricaoFavorecido = dsInscricaoFavorecido;
    } //-- void setDsInscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsMaeFavorecidoCliente'.
     * 
     * @param dsMaeFavorecidoCliente the value of field
     * 'dsMaeFavorecidoCliente'.
     */
    public void setDsMaeFavorecidoCliente(java.lang.String dsMaeFavorecidoCliente)
    {
        this._dsMaeFavorecidoCliente = dsMaeFavorecidoCliente;
    } //-- void setDsMaeFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipalInscricaoEstado'.
     * 
     * @param dsMunicipalInscricaoEstado the value of field
     * 'dsMunicipalInscricaoEstado'.
     */
    public void setDsMunicipalInscricaoEstado(java.lang.String dsMunicipalInscricaoEstado)
    {
        this._dsMunicipalInscricaoEstado = dsMunicipalInscricaoEstado;
    } //-- void setDsMunicipalInscricaoEstado(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipioEmpresaFavorecido'.
     * 
     * @param dsMunicipioEmpresaFavorecido the value of field
     * 'dsMunicipioEmpresaFavorecido'.
     */
    public void setDsMunicipioEmpresaFavorecido(java.lang.String dsMunicipioEmpresaFavorecido)
    {
        this._dsMunicipioEmpresaFavorecido = dsMunicipioEmpresaFavorecido;
    } //-- void setDsMunicipioEmpresaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipioFavorecidoCliente'.
     * 
     * @param dsMunicipioFavorecidoCliente the value of field
     * 'dsMunicipioFavorecidoCliente'.
     */
    public void setDsMunicipioFavorecidoCliente(java.lang.String dsMunicipioFavorecidoCliente)
    {
        this._dsMunicipioFavorecidoCliente = dsMunicipioFavorecidoCliente;
    } //-- void setDsMunicipioFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsMunicipioNascimentoFavorecido'.
     * 
     * @param dsMunicipioNascimentoFavorecido the value of field
     * 'dsMunicipioNascimentoFavorecido'.
     */
    public void setDsMunicipioNascimentoFavorecido(java.lang.String dsMunicipioNascimentoFavorecido)
    {
        this._dsMunicipioNascimentoFavorecido = dsMunicipioNascimentoFavorecido;
    } //-- void setDsMunicipioNascimentoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsNacionalidadeFavorecidoCliente'.
     * 
     * @param dsNacionalidadeFavorecidoCliente the value of field
     * 'dsNacionalidadeFavorecidoCliente'.
     */
    public void setDsNacionalidadeFavorecidoCliente(java.lang.String dsNacionalidadeFavorecidoCliente)
    {
        this._dsNacionalidadeFavorecidoCliente = dsNacionalidadeFavorecidoCliente;
    } //-- void setDsNacionalidadeFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsObservacaoControleFavorecido'.
     * 
     * @param dsObservacaoControleFavorecido the value of field
     * 'dsObservacaoControleFavorecido'.
     */
    public void setDsObservacaoControleFavorecido(java.lang.String dsObservacaoControleFavorecido)
    {
        this._dsObservacaoControleFavorecido = dsObservacaoControleFavorecido;
    } //-- void setDsObservacaoControleFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsOrganizacaoEmissorDocumento'.
     * 
     * @param dsOrganizacaoEmissorDocumento the value of field
     * 'dsOrganizacaoEmissorDocumento'.
     */
    public void setDsOrganizacaoEmissorDocumento(java.lang.String dsOrganizacaoEmissorDocumento)
    {
        this._dsOrganizacaoEmissorDocumento = dsOrganizacaoEmissorDocumento;
    } //-- void setDsOrganizacaoEmissorDocumento(java.lang.String) 

    /**
     * Sets the value of field 'dsPaiFavorecidoCliente'.
     * 
     * @param dsPaiFavorecidoCliente the value of field
     * 'dsPaiFavorecidoCliente'.
     */
    public void setDsPaiFavorecidoCliente(java.lang.String dsPaiFavorecidoCliente)
    {
        this._dsPaiFavorecidoCliente = dsPaiFavorecidoCliente;
    } //-- void setDsPaiFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsProfissaoFavorecidoCliente'.
     * 
     * @param dsProfissaoFavorecidoCliente the value of field
     * 'dsProfissaoFavorecidoCliente'.
     */
    public void setDsProfissaoFavorecidoCliente(java.lang.String dsProfissaoFavorecidoCliente)
    {
        this._dsProfissaoFavorecidoCliente = dsProfissaoFavorecidoCliente;
    } //-- void setDsProfissaoFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsRetornoCliente'.
     * 
     * @param dsRetornoCliente the value of field 'dsRetornoCliente'
     */
    public void setDsRetornoCliente(java.lang.String dsRetornoCliente)
    {
        this._dsRetornoCliente = dsRetornoCliente;
    } //-- void setDsRetornoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoFavorecido'.
     * 
     * @param dsTipoFavorecido the value of field 'dsTipoFavorecido'
     */
    public void setDsTipoFavorecido(java.lang.String dsTipoFavorecido)
    {
        this._dsTipoFavorecido = dsTipoFavorecido;
    } //-- void setDsTipoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsUfEmpreFavorecido'.
     * 
     * @param dsUfEmpreFavorecido the value of field
     * 'dsUfEmpreFavorecido'.
     */
    public void setDsUfEmpreFavorecido(java.lang.String dsUfEmpreFavorecido)
    {
        this._dsUfEmpreFavorecido = dsUfEmpreFavorecido;
    } //-- void setDsUfEmpreFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsUfNascimentoFavorecido'.
     * 
     * @param dsUfNascimentoFavorecido the value of field
     * 'dsUfNascimentoFavorecido'.
     */
    public void setDsUfNascimentoFavorecido(java.lang.String dsUfNascimentoFavorecido)
    {
        this._dsUfNascimentoFavorecido = dsUfNascimentoFavorecido;
    } //-- void setDsUfNascimentoFavorecido(java.lang.String) 

    /**
     * Sets the value of field
     * 'dtAtualizarEnderecoCorrespondencia'.
     * 
     * @param dtAtualizarEnderecoCorrespondencia the value of field
     * 'dtAtualizarEnderecoCorrespondencia'.
     */
    public void setDtAtualizarEnderecoCorrespondencia(java.lang.String dtAtualizarEnderecoCorrespondencia)
    {
        this._dtAtualizarEnderecoCorrespondencia = dtAtualizarEnderecoCorrespondencia;
    } //-- void setDtAtualizarEnderecoCorrespondencia(java.lang.String) 

    /**
     * Sets the value of field 'dtAtualizarEnderecoResidencial'.
     * 
     * @param dtAtualizarEnderecoResidencial the value of field
     * 'dtAtualizarEnderecoResidencial'.
     */
    public void setDtAtualizarEnderecoResidencial(java.lang.String dtAtualizarEnderecoResidencial)
    {
        this._dtAtualizarEnderecoResidencial = dtAtualizarEnderecoResidencial;
    } //-- void setDtAtualizarEnderecoResidencial(java.lang.String) 

    /**
     * Sets the value of field 'dtExpedicaoDocumentoFavorecido'.
     * 
     * @param dtExpedicaoDocumentoFavorecido the value of field
     * 'dtExpedicaoDocumentoFavorecido'.
     */
    public void setDtExpedicaoDocumentoFavorecido(java.lang.String dtExpedicaoDocumentoFavorecido)
    {
        this._dtExpedicaoDocumentoFavorecido = dtExpedicaoDocumentoFavorecido;
    } //-- void setDtExpedicaoDocumentoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtNascimentoFavorecidoCliente'.
     * 
     * @param dtNascimentoFavorecidoCliente the value of field
     * 'dtNascimentoFavorecidoCliente'.
     */
    public void setDtNascimentoFavorecidoCliente(java.lang.String dtNascimentoFavorecidoCliente)
    {
        this._dtNascimentoFavorecidoCliente = dtNascimentoFavorecidoCliente;
    } //-- void setDtNascimentoFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dtUltimaMovimentacaoFavorecido'.
     * 
     * @param dtUltimaMovimentacaoFavorecido the value of field
     * 'dtUltimaMovimentacaoFavorecido'.
     */
    public void setDtUltimaMovimentacaoFavorecido(java.lang.String dtUltimaMovimentacaoFavorecido)
    {
        this._dtUltimaMovimentacaoFavorecido = dtUltimaMovimentacaoFavorecido;
    } //-- void setDtUltimaMovimentacaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'enBairroCorrespondenciaFavorecido'.
     * 
     * @param enBairroCorrespondenciaFavorecido the value of field
     * 'enBairroCorrespondenciaFavorecido'.
     */
    public void setEnBairroCorrespondenciaFavorecido(java.lang.String enBairroCorrespondenciaFavorecido)
    {
        this._enBairroCorrespondenciaFavorecido = enBairroCorrespondenciaFavorecido;
    } //-- void setEnBairroCorrespondenciaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'enBairroFavorecidoCliente'.
     * 
     * @param enBairroFavorecidoCliente the value of field
     * 'enBairroFavorecidoCliente'.
     */
    public void setEnBairroFavorecidoCliente(java.lang.String enBairroFavorecidoCliente)
    {
        this._enBairroFavorecidoCliente = enBairroFavorecidoCliente;
    } //-- void setEnBairroFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field
     * 'enComplementoLogradouroCorrespondencia'.
     * 
     * @param enComplementoLogradouroCorrespondencia the value of
     * field 'enComplementoLogradouroCorrespondencia'.
     */
    public void setEnComplementoLogradouroCorrespondencia(java.lang.String enComplementoLogradouroCorrespondencia)
    {
        this._enComplementoLogradouroCorrespondencia = enComplementoLogradouroCorrespondencia;
    } //-- void setEnComplementoLogradouroCorrespondencia(java.lang.String) 

    /**
     * Sets the value of field 'enComplementoLogradouroFavorecido'.
     * 
     * @param enComplementoLogradouroFavorecido the value of field
     * 'enComplementoLogradouroFavorecido'.
     */
    public void setEnComplementoLogradouroFavorecido(java.lang.String enComplementoLogradouroFavorecido)
    {
        this._enComplementoLogradouroFavorecido = enComplementoLogradouroFavorecido;
    } //-- void setEnComplementoLogradouroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'enEmailComercialFavorecido'.
     * 
     * @param enEmailComercialFavorecido the value of field
     * 'enEmailComercialFavorecido'.
     */
    public void setEnEmailComercialFavorecido(java.lang.String enEmailComercialFavorecido)
    {
        this._enEmailComercialFavorecido = enEmailComercialFavorecido;
    } //-- void setEnEmailComercialFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'enEmailFavorecidoCliente'.
     * 
     * @param enEmailFavorecidoCliente the value of field
     * 'enEmailFavorecidoCliente'.
     */
    public void setEnEmailFavorecidoCliente(java.lang.String enEmailFavorecidoCliente)
    {
        this._enEmailFavorecidoCliente = enEmailFavorecidoCliente;
    } //-- void setEnEmailFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'enLogradouroFavorecidoCliente'.
     * 
     * @param enLogradouroFavorecidoCliente the value of field
     * 'enLogradouroFavorecidoCliente'.
     */
    public void setEnLogradouroFavorecidoCliente(java.lang.String enLogradouroFavorecidoCliente)
    {
        this._enLogradouroFavorecidoCliente = enLogradouroFavorecidoCliente;
    } //-- void setEnLogradouroFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field
     * 'enLogradrouroCorrespondenciaFavorecido'.
     * 
     * @param enLogradrouroCorrespondenciaFavorecido the value of
     * field 'enLogradrouroCorrespondenciaFavorecido'.
     */
    public void setEnLogradrouroCorrespondenciaFavorecido(java.lang.String enLogradrouroCorrespondenciaFavorecido)
    {
        this._enLogradrouroCorrespondenciaFavorecido = enLogradrouroCorrespondenciaFavorecido;
    } //-- void setEnLogradrouroCorrespondenciaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'enNumeroLogradouroCorrespondencia'.
     * 
     * @param enNumeroLogradouroCorrespondencia the value of field
     * 'enNumeroLogradouroCorrespondencia'.
     */
    public void setEnNumeroLogradouroCorrespondencia(java.lang.String enNumeroLogradouroCorrespondencia)
    {
        this._enNumeroLogradouroCorrespondencia = enNumeroLogradouroCorrespondencia;
    } //-- void setEnNumeroLogradouroCorrespondencia(java.lang.String) 

    /**
     * Sets the value of field 'enNumeroLogradouroFavorecido'.
     * 
     * @param enNumeroLogradouroFavorecido the value of field
     * 'enNumeroLogradouroFavorecido'.
     */
    public void setEnNumeroLogradouroFavorecido(java.lang.String enNumeroLogradouroFavorecido)
    {
        this._enNumeroLogradouroFavorecido = enNumeroLogradouroFavorecido;
    } //-- void setEnNumeroLogradouroFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'hrBloqueioFavorecidoCliente'.
     * 
     * @param hrBloqueioFavorecidoCliente the value of field
     * 'hrBloqueioFavorecidoCliente'.
     */
    public void setHrBloqueioFavorecidoCliente(java.lang.String hrBloqueioFavorecidoCliente)
    {
        this._hrBloqueioFavorecidoCliente = hrBloqueioFavorecidoCliente;
    } //-- void setHrBloqueioFavorecidoCliente(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mtBloqueio'.
     * 
     * @param mtBloqueio the value of field 'mtBloqueio'.
     */
    public void setMtBloqueio(java.lang.String mtBloqueio)
    {
        this._mtBloqueio = mtBloqueio;
    } //-- void setMtBloqueio(java.lang.String) 

    /**
     * Sets the value of field 'vlRendaMensalFavorecido'.
     * 
     * @param vlRendaMensalFavorecido the value of field
     * 'vlRendaMensalFavorecido'.
     */
    public void setVlRendaMensalFavorecido(java.math.BigDecimal vlRendaMensalFavorecido)
    {
        this._vlRendaMensalFavorecido = vlRendaMensalFavorecido;
    } //-- void setVlRendaMensalFavorecido(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDetalheFavorecidoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetalhefavorecido.response.ConsultarDetalheFavorecidoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetalhefavorecido.response.ConsultarDetalheFavorecidoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetalhefavorecido.response.ConsultarDetalheFavorecidoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhefavorecido.response.ConsultarDetalheFavorecidoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
