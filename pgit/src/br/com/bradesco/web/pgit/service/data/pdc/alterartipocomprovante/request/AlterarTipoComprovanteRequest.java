/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterartipocomprovante.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class AlterarTipoComprovanteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarTipoComprovanteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoComprovanteSalarial
     */
    private int _cdTipoComprovanteSalarial = 0;

    /**
     * keeps track of state for field: _cdTipoComprovanteSalarial
     */
    private boolean _has_cdTipoComprovanteSalarial;

    /**
     * Field _dsTipoComprovanteSalarial
     */
    private java.lang.String _dsTipoComprovanteSalarial;

    /**
     * Field _cdControleMensagemSalarial
     */
    private int _cdControleMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdControleMensagemSalarial
     */
    private boolean _has_cdControleMensagemSalarial;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarTipoComprovanteRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterartipocomprovante.request.AlterarTipoComprovanteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleMensagemSalarial
     * 
     */
    public void deleteCdControleMensagemSalarial()
    {
        this._has_cdControleMensagemSalarial= false;
    } //-- void deleteCdControleMensagemSalarial() 

    /**
     * Method deleteCdTipoComprovanteSalarial
     * 
     */
    public void deleteCdTipoComprovanteSalarial()
    {
        this._has_cdTipoComprovanteSalarial= false;
    } //-- void deleteCdTipoComprovanteSalarial() 

    /**
     * Returns the value of field 'cdControleMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdControleMensagemSalarial'.
     */
    public int getCdControleMensagemSalarial()
    {
        return this._cdControleMensagemSalarial;
    } //-- int getCdControleMensagemSalarial() 

    /**
     * Returns the value of field 'cdTipoComprovanteSalarial'.
     * 
     * @return int
     * @return the value of field 'cdTipoComprovanteSalarial'.
     */
    public int getCdTipoComprovanteSalarial()
    {
        return this._cdTipoComprovanteSalarial;
    } //-- int getCdTipoComprovanteSalarial() 

    /**
     * Returns the value of field 'dsTipoComprovanteSalarial'.
     * 
     * @return String
     * @return the value of field 'dsTipoComprovanteSalarial'.
     */
    public java.lang.String getDsTipoComprovanteSalarial()
    {
        return this._dsTipoComprovanteSalarial;
    } //-- java.lang.String getDsTipoComprovanteSalarial() 

    /**
     * Method hasCdControleMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleMensagemSalarial()
    {
        return this._has_cdControleMensagemSalarial;
    } //-- boolean hasCdControleMensagemSalarial() 

    /**
     * Method hasCdTipoComprovanteSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoComprovanteSalarial()
    {
        return this._has_cdTipoComprovanteSalarial;
    } //-- boolean hasCdTipoComprovanteSalarial() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleMensagemSalarial'.
     * 
     * @param cdControleMensagemSalarial the value of field
     * 'cdControleMensagemSalarial'.
     */
    public void setCdControleMensagemSalarial(int cdControleMensagemSalarial)
    {
        this._cdControleMensagemSalarial = cdControleMensagemSalarial;
        this._has_cdControleMensagemSalarial = true;
    } //-- void setCdControleMensagemSalarial(int) 

    /**
     * Sets the value of field 'cdTipoComprovanteSalarial'.
     * 
     * @param cdTipoComprovanteSalarial the value of field
     * 'cdTipoComprovanteSalarial'.
     */
    public void setCdTipoComprovanteSalarial(int cdTipoComprovanteSalarial)
    {
        this._cdTipoComprovanteSalarial = cdTipoComprovanteSalarial;
        this._has_cdTipoComprovanteSalarial = true;
    } //-- void setCdTipoComprovanteSalarial(int) 

    /**
     * Sets the value of field 'dsTipoComprovanteSalarial'.
     * 
     * @param dsTipoComprovanteSalarial the value of field
     * 'dsTipoComprovanteSalarial'.
     */
    public void setDsTipoComprovanteSalarial(java.lang.String dsTipoComprovanteSalarial)
    {
        this._dsTipoComprovanteSalarial = dsTipoComprovanteSalarial;
    } //-- void setDsTipoComprovanteSalarial(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarTipoComprovanteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterartipocomprovante.request.AlterarTipoComprovanteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterartipocomprovante.request.AlterarTipoComprovanteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterartipocomprovante.request.AlterarTipoComprovanteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterartipocomprovante.request.AlterarTipoComprovanteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
