/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

import java.util.Date;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO {
	
	
/** Atributo cdSolicitacao. */
private Integer cdSolicitacao;

/** Atributo nrSolicitacao. */
private Integer nrSolicitacao;

/** Atributo cdProdutoServicoOperacao. */
private Integer cdProdutoServicoOperacao;

/** Atributo dsProdutoServicoOperacao. */
private String dsProdutoServicoOperacao;

/** Atributo cdAmbienteAtivoSolicitacao. */
private String cdAmbienteAtivoSolicitacao;

/** Atributo dtProgramadaMudancaAmbiente. */
private Date dtProgramadaMudancaAmbiente;

/** Atributo cdSituacaoSolicitacaoPagamento. */
private Integer cdSituacaoSolicitacaoPagamento;

/** Atributo dsSituacaoSolicitacao. */
private String dsSituacaoSolicitacao;

/** Atributo dtSolicitacao. */
private String dtSolicitacao;

/** Atributo cdProdutoServicoRelacionado. */
private Integer cdProdutoServicoRelacionado;

/** Atributo dsProdutoServicoRelacionado. */
private String dsProdutoServicoRelacionado;

/** Atributo cdTipoParticipacaoPessoa. */
private Integer cdTipoParticipacaoPessoa;

/** Atributo cdPessoa. */
private Long cdPessoa;

/** Atributo cdCnpjParticipante. */
private Long cdCnpjParticipante;

/** Atributo cdFilialParticipante. */
private Integer cdFilialParticipante;

/** Atributo cdControleCnpjParticipante. */
private Integer cdControleCnpjParticipante;

/** Atributo dsNomeRazaoParticipante. */
private String dsNomeRazaoParticipante;

/**
 * Get: cdSolicitacao.
 *
 * @return cdSolicitacao
 */
public Integer getCdSolicitacao() {
	return cdSolicitacao;
}

/**
 * Set: cdSolicitacao.
 *
 * @param cdSolicitacao the cd solicitacao
 */
public void setCdSolicitacao(Integer cdSolicitacao) {
	this.cdSolicitacao = cdSolicitacao;
}

/**
 * Get: nrSolicitacao.
 *
 * @return nrSolicitacao
 */
public Integer getNrSolicitacao() {
	return nrSolicitacao;
}

/**
 * Set: nrSolicitacao.
 *
 * @param nrSolicitacao the nr solicitacao
 */
public void setNrSolicitacao(Integer nrSolicitacao) {
	this.nrSolicitacao = nrSolicitacao;
}

/**
 * Get: cdProdutoServicoOperacao.
 *
 * @return cdProdutoServicoOperacao
 */
public Integer getCdProdutoServicoOperacao() {
	return cdProdutoServicoOperacao;
}

/**
 * Set: cdProdutoServicoOperacao.
 *
 * @param cdProdutoServicoOperacao the cd produto servico operacao
 */
public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
	this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
}

/**
 * Get: dsProdutoServicoOperacao.
 *
 * @return dsProdutoServicoOperacao
 */
public String getDsProdutoServicoOperacao() {
	return dsProdutoServicoOperacao;
}

/**
 * Set: dsProdutoServicoOperacao.
 *
 * @param dsProdutoServicoOperacao the ds produto servico operacao
 */
public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
	this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
}

/**
 * Get: cdAmbienteAtivoSolicitacao.
 *
 * @return cdAmbienteAtivoSolicitacao
 */
public String getCdAmbienteAtivoSolicitacao() {
	return cdAmbienteAtivoSolicitacao;
}

/**
 * Set: cdAmbienteAtivoSolicitacao.
 *
 * @param cdAmbienteAtivoSolicitacao the cd ambiente ativo solicitacao
 */
public void setCdAmbienteAtivoSolicitacao(String cdAmbienteAtivoSolicitacao) {
	this.cdAmbienteAtivoSolicitacao = cdAmbienteAtivoSolicitacao;
}

/**
 * Get: dtProgramadaMudancaAmbiente.
 *
 * @return dtProgramadaMudancaAmbiente
 */
public Date getDtProgramadaMudancaAmbiente() {
	return dtProgramadaMudancaAmbiente;
}

/**
 * Set: dtProgramadaMudancaAmbiente.
 *
 * @param dtProgramadaMudancaAmbiente the dt programada mudanca ambiente
 */
public void setDtProgramadaMudancaAmbiente(Date dtProgramadaMudancaAmbiente) {
	this.dtProgramadaMudancaAmbiente = dtProgramadaMudancaAmbiente;
}

/**
 * Get: cdSituacaoSolicitacaoPagamento.
 *
 * @return cdSituacaoSolicitacaoPagamento
 */
public Integer getCdSituacaoSolicitacaoPagamento() {
	return cdSituacaoSolicitacaoPagamento;
}

/**
 * Set: cdSituacaoSolicitacaoPagamento.
 *
 * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
 */
public void setCdSituacaoSolicitacaoPagamento(
		Integer cdSituacaoSolicitacaoPagamento) {
	this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
}

/**
 * Get: dsSituacaoSolicitacao.
 *
 * @return dsSituacaoSolicitacao
 */
public String getDsSituacaoSolicitacao() {
	return dsSituacaoSolicitacao;
}

/**
 * Set: dsSituacaoSolicitacao.
 *
 * @param dsSituacaoSolicitacao the ds situacao solicitacao
 */
public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
	this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
}

/**
 * Get: dtSolicitacao.
 *
 * @return dtSolicitacao
 */
public String getDtSolicitacao() {
	return dtSolicitacao;
}

/**
 * Set: dtSolicitacao.
 *
 * @param dtSolicitacao the dt solicitacao
 */
public void setDtSolicitacao(String dtSolicitacao) {
	this.dtSolicitacao = dtSolicitacao;
}

/**
 * Get: cdProdutoServicoRelacionado.
 *
 * @return cdProdutoServicoRelacionado
 */
public Integer getCdProdutoServicoRelacionado() {
	return cdProdutoServicoRelacionado;
}

/**
 * Set: cdProdutoServicoRelacionado.
 *
 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
 */
public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
	this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
}

/**
 * Get: dsProdutoServicoRelacionado.
 *
 * @return dsProdutoServicoRelacionado
 */
public String getDsProdutoServicoRelacionado() {
	return dsProdutoServicoRelacionado;
}

/**
 * Set: dsProdutoServicoRelacionado.
 *
 * @param dsProdutoServicoRelacionado the ds produto servico relacionado
 */
public void setDsProdutoServicoRelacionado(String dsProdutoServicoRelacionado) {
	this.dsProdutoServicoRelacionado = dsProdutoServicoRelacionado;
}

/**
 * Get: cdTipoParticipacaoPessoa.
 *
 * @return cdTipoParticipacaoPessoa
 */
public Integer getCdTipoParticipacaoPessoa() {
	return cdTipoParticipacaoPessoa;
}

/**
 * Set: cdTipoParticipacaoPessoa.
 *
 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
 */
public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
	this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
}

/**
 * Get: cdPessoa.
 *
 * @return cdPessoa
 */
public Long getCdPessoa() {
	return cdPessoa;
}

/**
 * Set: cdPessoa.
 *
 * @param cdPessoa the cd pessoa
 */
public void setCdPessoa(Long cdPessoa) {
	this.cdPessoa = cdPessoa;
}

/**
 * Get: cdCnpjParticipante.
 *
 * @return cdCnpjParticipante
 */
public Long getCdCnpjParticipante() {
	return cdCnpjParticipante;
}

/**
 * Set: cdCnpjParticipante.
 *
 * @param cdCnpjParticipante the cd cnpj participante
 */
public void setCdCnpjParticipante(Long cdCnpjParticipante) {
	this.cdCnpjParticipante = cdCnpjParticipante;
}

/**
 * Get: cdFilialParticipante.
 *
 * @return cdFilialParticipante
 */
public Integer getCdFilialParticipante() {
	return cdFilialParticipante;
}

/**
 * Set: cdFilialParticipante.
 *
 * @param cdFilialParticipante the cd filial participante
 */
public void setCdFilialParticipante(Integer cdFilialParticipante) {
	this.cdFilialParticipante = cdFilialParticipante;
}

/**
 * Get: cdControleCnpjParticipante.
 *
 * @return cdControleCnpjParticipante
 */
public Integer getCdControleCnpjParticipante() {
	return cdControleCnpjParticipante;
}

/**
 * Set: cdControleCnpjParticipante.
 *
 * @param cdControleCnpjParticipante the cd controle cnpj participante
 */
public void setCdControleCnpjParticipante(Integer cdControleCnpjParticipante) {
	this.cdControleCnpjParticipante = cdControleCnpjParticipante;
}

/**
 * Get: dsNomeRazaoParticipante.
 *
 * @return dsNomeRazaoParticipante
 */
public String getDsNomeRazaoParticipante() {
	return dsNomeRazaoParticipante;
}

/**
 * Set: dsNomeRazaoParticipante.
 *
 * @param dsNomeRazaoParticipante the ds nome razao participante
 */
public void setDsNomeRazaoParticipante(String dsNomeRazaoParticipante) {
	this.dsNomeRazaoParticipante = dsNomeRazaoParticipante;
}

/**
 * Get: cpfCnpjFormatado.
 *
 * @return cpfCnpjFormatado
 */
public String getCpfCnpjFormatado(){
	return CpfCnpjUtils.formatCpfCnpjCompleto(cdCnpjParticipante, cdFilialParticipante, cdControleCnpjParticipante);
}

}
