/*
 * Nome: ${package_name}
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: ${date}
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.math.BigDecimal;
import java.util.List;

import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * The Class DetalharPiramideSaidaDTO.
 */
public class DetalharPiramideSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem;

    /** The mensagem. */
    private String mensagem;

    /** The cd fai max ocorr. */
    private Integer cdFaiMaxOcorr;

    /** The cd tur max ocorr. */
    private Integer cdTurMaxOcorr;

    /** The cd fil max ocorr. */
    private Integer cdFilMaxOcorr;

    /** The cd con max ocorr. */
    private Integer cdConMaxOcorr;

    /** The cd fol max ocorr. */
    private Integer cdFolMaxOcorr;

    /** The cd coc max ocorr. */
    private Integer cdCocMaxOcorr;

    /** The cd pssoa jurid cta. */
    private Long cdPssoaJuridCta;

    /** The cd tipo contr cta. */
    private Integer cdTipoContrCta;

    /** The num sequencia contrato cta. */
    private Long numSequenciaContratoCta;

    /** The cd instituicao bancaria. */
    private Integer cdInstituicaoBancaria;

    /** The cd unidade organizacional custo. */
    private Integer cdUnidadeOrganizacionalCusto;

    /** The Nm conta. */
    private Integer nmConta;

    /** The num conta digito. */
    private Integer numContaDigito;

    /** The cd indicador num conve. */
    private Integer cdIndicadorNumConve;

    /** The cd conve cta salarial. */
    private Long cdConveCtaSalarial;

    /** The vl pagamento mes. */
    private BigDecimal vlPagamentoMes;

    /** The qtd funcionario fl pgto. */
    private Integer qtdFuncionarioFlPgto;

    /** The vl media salarial mes. */
    private BigDecimal vlMediaSalarialMes;

    /** The num dia credt prim. */
    private Integer numDiaCredtPrim;

    /** The num dia credt seg. */
    private Integer numDiaCredtSeg;

    /** The cd indicador quest salarial. */
    private Integer cdIndicadorQuestSalarial;

    /** The cd pagamento salarial mes. */
    private Integer cdPagamentoSalarialMes;

    /** The cd pagamento salarial qzena. */
    private Integer cdPagamentoSalarialQzena;

    /** The cd loc cta salarial. */
    private Integer cdLocCtaSalarial;

    /** The ds loc cta salarial. */
    private String dsLocCtaSalarial;

    /** The data reft fl pgto. */
    private Integer dataReftFlPgto;

    /** The ds texto justf conc. */
    private String dsTextoJustfConc;

    /** The ds texto justf recip. */
    private String dsTextoJustfRecip;

    /** The ds texto justf compl. */
    private String dsTextoJustfCompl;

    /** The cd indicador grp quest. */
    private Integer cdIndicadorGrpQuest;

    /** The nro grupo econm quest. */
    private Long nroGrupoEconmQuest;

    /** The ds grupo econm quest. */
    private String dsGrupoEconmQuest;

    /** The cd unidade medd ag. */
    private Integer cdUnidadeMeddAg;

    /** The ds abrev medd ag. */
    private String dsAbrevMeddAg;

    /** The cd udistc ag empr. */
    private Integer cdUdistcAgEmpr;

    /** The ds logradouro empr quest. */
    private String dsLogradouroEmprQuest;

    /** The ds bairro empr quest. */
    private String dsBairroEmprQuest;

    /** The cd mun empr quest. */
    private Long cdMunEmprQuest;

    /** The ds munc empre quest. */
    private String dsMuncEmpreQuest;

    /** The cd cep empr quest. */
    private Integer cdCepEmprQuest;

    /** The cd cep complemento. */
    private Integer cdCepComplemento;

    /** The cd uf empre quest. */
    private Integer cdUfEmpreQuest;

    /** The ds uf empr quest. */
    private String dsUfEmprQuest;

    /** The cd agpto atividade quest. */
    private Long cdAgptoAtividadeQuest;

    /** The ds ramo atividade quest. */
    private String dsRamoAtividadeQuest;

    /** The cd discagem direta inter empr. */
    private Integer cdDiscagemDiretaInterEmpr;

    /** The cd discagem direta dist empr. */
    private Integer cdDiscagemDiretaDistEmpr;

    /** The num fone empr quest. */
    private Long numFoneEmprQuest;

    /** The ds site empr quest. */
    private String dsSiteEmprQuest;

    /** The cd func ger relacionamento. */
    private Long cdFuncGerRelacionamento;

    /** The ds ger relacionamento quest. */
    private String dsGerRelacionamentoQuest;

    /** The cd discagem direta dist ger. */
    private Integer cdDiscagemDiretaDistGer;

    /** The cd discagem direta distan ger. */
    private Integer cdDiscagemDiretaDistanGer;

    /** The num fone ger quest. */
    private Long numFoneGerQuest;

    /** The cd segmento empr quest. */
    private Integer cdSegmentoEmprQuest;

    /** The ds abrev segmento empr. */
    private String dsAbrevSegmentoEmpr;

    /** The cd indicador fl quest. */
    private Integer cdIndicadorFlQuest;

    /** The ds indicador fl quest. */
    private String dsIndicadorFlQuest;

    /** The pctgm pre nov ano quest. */
    private BigDecimal pctgmPreNovAnoQuest;

    /** The cdprefe csign. */
    private Integer cdprefeCsign;

    /** The cdprefe pab cren. */
    private Integer cdprefePabCren;

    /** The cd indicador pae ccren. */
    private Integer cdIndicadorPaeCcren;

    /** The cd indicador ag ccren. */
    private Integer cdIndicadorAgCcren;

    /** The cd indicador insta estrt. */
    private Integer cdIndicadorInstaEstrt;

    /** The cd estrt insta empr. */
    private Integer cdEstrtInstaEmpr;

    /** The ds estrutura insta empr. */
    private String dsEstruturaInstaEmpr;

    /** The cdbanco vinc insta. */
    private Integer cdbancoVincInsta;

    /** The cd agencia vinc insta. */
    private Integer cdAgenciaVincInsta;

    /** The ds agencia vinc insta. */
    private String dsAgenciaVincInsta;

    /** The cd unidade medd insta. */
    private Integer cdUnidadeMeddInsta;

    /** The ds abrev insta. */
    private String dsAbrevInsta;

    /** The cd distc agencia insta. */
    private Integer cdDistcAgenciaInsta;

    /** The cd indicador visita insta. */
    private Integer cdIndicadorVisitaInsta;

    /** The ds indicador visita insta. */
    private String dsIndicadorVisitaInsta;

    /** The cd func ger visitante. */
    private Long cdFuncGerVisitante;

    /** The ds ger visitante insta. */
    private String dsGerVisitanteInsta;

    /** The cd unidade medd espac. */
    private Integer cdUnidadeMeddEspac;

    /** The ds abrev espac concedido. */
    private String dsAbrevEspacConcedido;

    /** The cd uespac concedido insta. */
    private Integer cdUespacConcedidoInsta;

    /** The cd tipo neces insta. */
    private Integer cdTipoNecesInsta;

    /** The ds tipo necess insta. */
    private String dsTipoNecessInsta;

    /** The cd indicador perda relacionamento. */
    private Integer cdIndicadorPerdaRelacionamento;

    /** The ds indicador perdarelacionamento. */
    private String dsIndicadorPerdarelacionamento;

    /** The cd pagamento sal credt. */
    private Integer cdPagamentoSalCredt;

    /** The cd pagamento sal creque. */
    private Integer cdPagamentoSalCreque;

    /** The cd pagamento sal dinheiro. */
    private Integer cdPagamentoSalDinheiro;

    /** The cd pagamento sal divers. */
    private Integer cdPagamentoSalDivers;

    /** The ds pagamento sal divers. */
    private String dsPagamentoSalDivers;

    /** Atributo dtMesAnoCorrespondente. */
    private Integer dtMesAnoCorrespondente;
    
    /** The ocorrencias1. */
    private List<DetalharPiramideOcorrencias1SaidaDTO> ocorrencias1;

    /** The ocorrencias2. */
    private List<DetalharPiramideOcorrencias2SaidaDTO> ocorrencias2;

    /** The ocorrencias3. */
    private List<DetalharPiramideOcorrencias3SaidaDTO> ocorrencias3;

    /** The ocorrencias4. */
    private List<DetalharPiramideOcorrencias4SaidaDTO> ocorrencias4;

    /** The ocorrencias5. */
    private List<DetalharPiramideOcorrencias5SaidaDTO> ocorrencias5;

    /** The ocorrencias6. */
    private List<DetalharPiramideOcorrencias6SaidaDTO> ocorrencias6;

    /** The total qtd funcionario filial01. */
    private Long totalQtdFuncionarioFilial01 = 0L;

    /** The total qtd funcionario filial02. */
    private Long totalQtdFuncionarioFilial02 = 0L;

    /** The total qtd funcionario filial03. */
    private Long totalQtdFuncionarioFilial03 = 0L;

    /** The total qtd funcionario filial04. */
    private Long totalQtdFuncionarioFilial04 = 0L;

    /** The total qtd funcionario filial05. */
    private Long totalQtdFuncionarioFilial05 = 0L;

    /** The total qtd funcionario filial06. */
    private Long totalQtdFuncionarioFilial06 = 0L;

    /** The total qtd funcionario filial07. */
    private Long totalQtdFuncionarioFilial07 = 0L;

    /** The total qtd funcionario filial08. */
    private Long totalQtdFuncionarioFilial08 = 0L;

    /** The total qtd funcionario filial09. */
    private Long totalQtdFuncionarioFilial09 = 0L;

    /** The total qtd funcionario filial10. */
    private Long totalQtdFuncionarioFilial10 = 0L;

    /** The total qtd func casa quest. */
    private Long totalQtdFuncCasaQuest = 0L;

    /** The total qtd func deslg quest. */
    private Long totalQtdFuncDeslgQuest = 0L;

    /** The total ptc renov faixa quest. */
    private BigDecimal totalPtcRenovFaixaQuest = BigDecimal.ZERO;

    /**
     * Set cod mensagem.
     * 
     * @param codMensagem
     *            the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Get cod mensagem.
     * 
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Set mensagem.
     * 
     * @param mensagem
     *            the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get mensagem.
     * 
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Set cd fai max ocorr.
     * 
     * @param cdFaiMaxOcorr
     *            the cd fai max ocorr
     */
    public void setCdFaiMaxOcorr(Integer cdFaiMaxOcorr) {
        this.cdFaiMaxOcorr = cdFaiMaxOcorr;
    }

    /**
     * Get cd fai max ocorr.
     * 
     * @return the cd fai max ocorr
     */
    public Integer getCdFaiMaxOcorr() {
        return this.cdFaiMaxOcorr;
    }

    /**
     * Set cd tur max ocorr.
     * 
     * @param cdTurMaxOcorr
     *            the cd tur max ocorr
     */
    public void setCdTurMaxOcorr(Integer cdTurMaxOcorr) {
        this.cdTurMaxOcorr = cdTurMaxOcorr;
    }

    /**
     * Get cd tur max ocorr.
     * 
     * @return the cd tur max ocorr
     */
    public Integer getCdTurMaxOcorr() {
        return this.cdTurMaxOcorr;
    }

    /**
     * Set cd fil max ocorr.
     * 
     * @param cdFilMaxOcorr
     *            the cd fil max ocorr
     */
    public void setCdFilMaxOcorr(Integer cdFilMaxOcorr) {
        this.cdFilMaxOcorr = cdFilMaxOcorr;
    }

    /**
     * Get cd fil max ocorr.
     * 
     * @return the cd fil max ocorr
     */
    public Integer getCdFilMaxOcorr() {
        return this.cdFilMaxOcorr;
    }

    /**
     * Set cd con max ocorr.
     * 
     * @param cdConMaxOcorr
     *            the cd con max ocorr
     */
    public void setCdConMaxOcorr(Integer cdConMaxOcorr) {
        this.cdConMaxOcorr = cdConMaxOcorr;
    }

    /**
     * Get cd con max ocorr.
     * 
     * @return the cd con max ocorr
     */
    public Integer getCdConMaxOcorr() {
        return this.cdConMaxOcorr;
    }

    /**
     * Set cd fol max ocorr.
     * 
     * @param cdFolMaxOcorr
     *            the cd fol max ocorr
     */
    public void setCdFolMaxOcorr(Integer cdFolMaxOcorr) {
        this.cdFolMaxOcorr = cdFolMaxOcorr;
    }

    /**
     * Get cd fol max ocorr.
     * 
     * @return the cd fol max ocorr
     */
    public Integer getCdFolMaxOcorr() {
        return this.cdFolMaxOcorr;
    }

    /**
     * Set cd coc max ocorr.
     * 
     * @param cdCocMaxOcorr
     *            the cd coc max ocorr
     */
    public void setCdCocMaxOcorr(Integer cdCocMaxOcorr) {
        this.cdCocMaxOcorr = cdCocMaxOcorr;
    }

    /**
     * Get cd coc max ocorr.
     * 
     * @return the cd coc max ocorr
     */
    public Integer getCdCocMaxOcorr() {
        return this.cdCocMaxOcorr;
    }

    /**
     * Set cd pssoa jurid cta.
     * 
     * @param cdPssoaJuridCta
     *            the cd pssoa jurid cta
     */
    public void setCdPssoaJuridCta(Long cdPssoaJuridCta) {
        this.cdPssoaJuridCta = cdPssoaJuridCta;
    }

    /**
     * Get cd pssoa jurid cta.
     * 
     * @return the cd pssoa jurid cta
     */
    public Long getCdPssoaJuridCta() {
        return this.cdPssoaJuridCta;
    }

    /**
     * Set cd tipo contr cta.
     * 
     * @param cdTipoContrCta
     *            the cd tipo contr cta
     */
    public void setCdTipoContrCta(Integer cdTipoContrCta) {
        this.cdTipoContrCta = cdTipoContrCta;
    }

    /**
     * Get cd tipo contr cta.
     * 
     * @return the cd tipo contr cta
     */
    public Integer getCdTipoContrCta() {
        return this.cdTipoContrCta;
    }

    /**
     * Set num sequencia contrato cta.
     * 
     * @param numSequenciaContratoCta
     *            the num sequencia contrato cta
     */
    public void setNumSequenciaContratoCta(Long numSequenciaContratoCta) {
        this.numSequenciaContratoCta = numSequenciaContratoCta;
    }

    /**
     * Get num sequencia contrato cta.
     * 
     * @return the num sequencia contrato cta
     */
    public Long getNumSequenciaContratoCta() {
        return this.numSequenciaContratoCta;
    }

    /**
     * Set cd instituicao bancaria.
     * 
     * @param cdInstituicaoBancaria
     *            the cd instituicao bancaria
     */
    public void setCdInstituicaoBancaria(Integer cdInstituicaoBancaria) {
        this.cdInstituicaoBancaria = cdInstituicaoBancaria;
    }

    /**
     * Get cd instituicao bancaria.
     * 
     * @return the cd instituicao bancaria
     */
    public Integer getCdInstituicaoBancaria() {
        return this.cdInstituicaoBancaria;
    }

    /**
     * Set cd unidade organizacional custo.
     * 
     * @param cdUnidadeOrganizacionalCusto
     *            the cd unidade organizacional custo
     */
    public void setCdUnidadeOrganizacionalCusto(Integer cdUnidadeOrganizacionalCusto) {
        this.cdUnidadeOrganizacionalCusto = cdUnidadeOrganizacionalCusto;
    }

    /**
     * Get cd unidade organizacional custo.
     * 
     * @return the cd unidade organizacional custo
     */
    public Integer getCdUnidadeOrganizacionalCusto() {
        return this.cdUnidadeOrganizacionalCusto;
    }

    /**
     * Set nm conta.
     * 
     * @param nmConta
     *            the nm conta
     */
    public void setNmConta(Integer nmConta) {
        this.nmConta = nmConta;
    }

    /**
     * Get nm conta.
     * 
     * @return the nm conta
     */
    public Integer getNmConta() {
        return this.nmConta;
    }

    /**
     * Set num conta digito.
     * 
     * @param numContaDigito
     *            the num conta digito
     */
    public void setNumContaDigito(Integer numContaDigito) {
        this.numContaDigito = numContaDigito;
    }

    /**
     * Get num conta digito.
     * 
     * @return the num conta digito
     */
    public Integer getNumContaDigito() {
        return this.numContaDigito;
    }

    /**
     * Set cd indicador num conve.
     * 
     * @param cdIndicadorNumConve
     *            the cd indicador num conve
     */
    public void setCdIndicadorNumConve(Integer cdIndicadorNumConve) {
        this.cdIndicadorNumConve = cdIndicadorNumConve;
    }

    /**
     * Get cd indicador num conve.
     * 
     * @return the cd indicador num conve
     */
    public Integer getCdIndicadorNumConve() {
        return this.cdIndicadorNumConve;
    }

    /**
     * Set cd conve cta salarial.
     * 
     * @param cdConveCtaSalarial
     *            the cd conve cta salarial
     */
    public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
        this.cdConveCtaSalarial = cdConveCtaSalarial;
    }

    /**
     * Get cd conve cta salarial.
     * 
     * @return the cd conve cta salarial
     */
    public Long getCdConveCtaSalarial() {
        return this.cdConveCtaSalarial;
    }

    /**
     * Set vl pagamento mes.
     * 
     * @param vlPagamentoMes
     *            the vl pagamento mes
     */
    public void setVlPagamentoMes(BigDecimal vlPagamentoMes) {
        this.vlPagamentoMes = vlPagamentoMes;
    }

    /**
     * Get vl pagamento mes.
     * 
     * @return the vl pagamento mes
     */
    public BigDecimal getVlPagamentoMes() {
        return this.vlPagamentoMes;
    }

    /**
     * Set qtd funcionario fl pgto.
     * 
     * @param qtdFuncionarioFlPgto
     *            the qtd funcionario fl pgto
     */
    public void setQtdFuncionarioFlPgto(Integer qtdFuncionarioFlPgto) {
        this.qtdFuncionarioFlPgto = qtdFuncionarioFlPgto;
    }

    /**
     * Get qtd funcionario fl pgto.
     * 
     * @return the qtd funcionario fl pgto
     */
    public Integer getQtdFuncionarioFlPgto() {
        return this.qtdFuncionarioFlPgto;
    }

    /**
     * Set vl media salarial mes.
     * 
     * @param vlMediaSalarialMes
     *            the vl media salarial mes
     */
    public void setVlMediaSalarialMes(BigDecimal vlMediaSalarialMes) {
        this.vlMediaSalarialMes = vlMediaSalarialMes;
    }

    /**
     * Get vl media salarial mes.
     * 
     * @return the vl media salarial mes
     */
    public BigDecimal getVlMediaSalarialMes() {
        return this.vlMediaSalarialMes;
    }

    /**
     * Set num dia credt prim.
     * 
     * @param numDiaCredtPrim
     *            the num dia credt prim
     */
    public void setNumDiaCredtPrim(Integer numDiaCredtPrim) {
        this.numDiaCredtPrim = numDiaCredtPrim;
    }

    /**
     * Get num dia credt prim.
     * 
     * @return the num dia credt prim
     */
    public Integer getNumDiaCredtPrim() {
        return this.numDiaCredtPrim;
    }

    /**
     * Set num dia credt seg.
     * 
     * @param numDiaCredtSeg
     *            the num dia credt seg
     */
    public void setNumDiaCredtSeg(Integer numDiaCredtSeg) {
        this.numDiaCredtSeg = numDiaCredtSeg;
    }

    /**
     * Get num dia credt seg.
     * 
     * @return the num dia credt seg
     */
    public Integer getNumDiaCredtSeg() {
        return this.numDiaCredtSeg;
    }

    /**
     * Set cd indicador quest salarial.
     * 
     * @param cdIndicadorQuestSalarial
     *            the cd indicador quest salarial
     */
    public void setCdIndicadorQuestSalarial(Integer cdIndicadorQuestSalarial) {
        this.cdIndicadorQuestSalarial = cdIndicadorQuestSalarial;
    }

    /**
     * Get cd indicador quest salarial.
     * 
     * @return the cd indicador quest salarial
     */
    public Integer getCdIndicadorQuestSalarial() {
        return this.cdIndicadorQuestSalarial;
    }

    /**
     * Set cd pagamento salarial mes.
     * 
     * @param cdPagamentoSalarialMes
     *            the cd pagamento salarial mes
     */
    public void setCdPagamentoSalarialMes(Integer cdPagamentoSalarialMes) {
        this.cdPagamentoSalarialMes = cdPagamentoSalarialMes;
    }

    /**
     * Get cd pagamento salarial mes.
     * 
     * @return the cd pagamento salarial mes
     */
    public Integer getCdPagamentoSalarialMes() {
        return this.cdPagamentoSalarialMes;
    }

    /**
     * Set cd pagamento salarial qzena.
     * 
     * @param cdPagamentoSalarialQzena
     *            the cd pagamento salarial qzena
     */
    public void setCdPagamentoSalarialQzena(Integer cdPagamentoSalarialQzena) {
        this.cdPagamentoSalarialQzena = cdPagamentoSalarialQzena;
    }

    /**
     * Get cd pagamento salarial qzena.
     * 
     * @return the cd pagamento salarial qzena
     */
    public Integer getCdPagamentoSalarialQzena() {
        return this.cdPagamentoSalarialQzena;
    }

    /**
     * Set cd loc cta salarial.
     * 
     * @param cdLocCtaSalarial
     *            the cd loc cta salarial
     */
    public void setCdLocCtaSalarial(Integer cdLocCtaSalarial) {
        this.cdLocCtaSalarial = cdLocCtaSalarial;
    }

    /**
     * Get cd loc cta salarial.
     * 
     * @return the cd loc cta salarial
     */
    public Integer getCdLocCtaSalarial() {
        return this.cdLocCtaSalarial;
    }

    /**
     * Set ds loc cta salarial.
     * 
     * @param dsLocCtaSalarial
     *            the ds loc cta salarial
     */
    public void setDsLocCtaSalarial(String dsLocCtaSalarial) {
        this.dsLocCtaSalarial = dsLocCtaSalarial;
    }

    /**
     * Get ds loc cta salarial.
     * 
     * @return the ds loc cta salarial
     */
    public String getDsLocCtaSalarial() {
        return this.dsLocCtaSalarial;
    }

    /**
     * Set data reft fl pgto.
     * 
     * @param dataReftFlPgto
     *            the data reft fl pgto
     */
    public void setDataReftFlPgto(Integer dataReftFlPgto) {
        this.dataReftFlPgto = dataReftFlPgto;
    }

    /**
     * Get data reft fl pgto.
     * 
     * @return the data reft fl pgto
     */
    public Integer getDataReftFlPgto() {
        return this.dataReftFlPgto;
    }

    /**
     * Set ds texto justf conc.
     * 
     * @param dsTextoJustfConc
     *            the ds texto justf conc
     */
    public void setDsTextoJustfConc(String dsTextoJustfConc) {
        this.dsTextoJustfConc = dsTextoJustfConc;
    }

    /**
     * Get ds texto justf conc.
     * 
     * @return the ds texto justf conc
     */
    public String getDsTextoJustfConc() {
        return this.dsTextoJustfConc;
    }

    /**
     * Set ds texto justf recip.
     * 
     * @param dsTextoJustfRecip
     *            the ds texto justf recip
     */
    public void setDsTextoJustfRecip(String dsTextoJustfRecip) {
        this.dsTextoJustfRecip = dsTextoJustfRecip;
    }

    /**
     * Get ds texto justf recip.
     * 
     * @return the ds texto justf recip
     */
    public String getDsTextoJustfRecip() {
        return this.dsTextoJustfRecip;
    }

    /**
     * Set ds texto justf compl.
     * 
     * @param dsTextoJustfCompl
     *            the ds texto justf compl
     */
    public void setDsTextoJustfCompl(String dsTextoJustfCompl) {
        this.dsTextoJustfCompl = dsTextoJustfCompl;
    }

    /**
     * Get ds texto justf compl.
     * 
     * @return the ds texto justf compl
     */
    public String getDsTextoJustfCompl() {
        return this.dsTextoJustfCompl;
    }

    /**
     * Set cd indicador grp quest.
     * 
     * @param cdIndicadorGrpQuest
     *            the cd indicador grp quest
     */
    public void setCdIndicadorGrpQuest(Integer cdIndicadorGrpQuest) {
        this.cdIndicadorGrpQuest = cdIndicadorGrpQuest;
    }

    /**
     * Get cd indicador grp quest.
     * 
     * @return the cd indicador grp quest
     */
    public Integer getCdIndicadorGrpQuest() {
        return this.cdIndicadorGrpQuest;
    }

    /**
     * Set nro grupo econm quest.
     * 
     * @param nroGrupoEconmQuest
     *            the nro grupo econm quest
     */
    public void setNroGrupoEconmQuest(Long nroGrupoEconmQuest) {
        this.nroGrupoEconmQuest = nroGrupoEconmQuest;
    }

    /**
     * Get nro grupo econm quest.
     * 
     * @return the nro grupo econm quest
     */
    public Long getNroGrupoEconmQuest() {
        return this.nroGrupoEconmQuest;
    }

    /**
     * Set ds grupo econm quest.
     * 
     * @param dsGrupoEconmQuest
     *            the ds grupo econm quest
     */
    public void setDsGrupoEconmQuest(String dsGrupoEconmQuest) {
        this.dsGrupoEconmQuest = dsGrupoEconmQuest;
    }

    /**
     * Get ds grupo econm quest.
     * 
     * @return the ds grupo econm quest
     */
    public String getDsGrupoEconmQuest() {
        return this.dsGrupoEconmQuest;
    }

    /**
     * Set cd unidade medd ag.
     * 
     * @param cdUnidadeMeddAg
     *            the cd unidade medd ag
     */
    public void setCdUnidadeMeddAg(Integer cdUnidadeMeddAg) {
        this.cdUnidadeMeddAg = cdUnidadeMeddAg;
    }

    /**
     * Get cd unidade medd ag.
     * 
     * @return the cd unidade medd ag
     */
    public Integer getCdUnidadeMeddAg() {
        return this.cdUnidadeMeddAg;
    }

    /**
     * Set ds abrev medd ag.
     * 
     * @param dsAbrevMeddAg
     *            the ds abrev medd ag
     */
    public void setDsAbrevMeddAg(String dsAbrevMeddAg) {
        this.dsAbrevMeddAg = dsAbrevMeddAg;
    }

    /**
     * Get ds abrev medd ag.
     * 
     * @return the ds abrev medd ag
     */
    public String getDsAbrevMeddAg() {
        return this.dsAbrevMeddAg;
    }

    /**
     * Set cd udistc ag empr.
     * 
     * @param cdUdistcAgEmpr
     *            the cd udistc ag empr
     */
    public void setCdUdistcAgEmpr(Integer cdUdistcAgEmpr) {
        this.cdUdistcAgEmpr = cdUdistcAgEmpr;
    }

    /**
     * Get cd udistc ag empr.
     * 
     * @return the cd udistc ag empr
     */
    public Integer getCdUdistcAgEmpr() {
        return this.cdUdistcAgEmpr;
    }

    /**
     * Set ds logradouro empr quest.
     * 
     * @param dsLogradouroEmprQuest
     *            the ds logradouro empr quest
     */
    public void setDsLogradouroEmprQuest(String dsLogradouroEmprQuest) {
        this.dsLogradouroEmprQuest = dsLogradouroEmprQuest;
    }

    /**
     * Get ds logradouro empr quest.
     * 
     * @return the ds logradouro empr quest
     */
    public String getDsLogradouroEmprQuest() {
        return this.dsLogradouroEmprQuest;
    }

    /**
     * Set ds bairro empr quest.
     * 
     * @param dsBairroEmprQuest
     *            the ds bairro empr quest
     */
    public void setDsBairroEmprQuest(String dsBairroEmprQuest) {
        this.dsBairroEmprQuest = dsBairroEmprQuest;
    }

    /**
     * Get ds bairro empr quest.
     * 
     * @return the ds bairro empr quest
     */
    public String getDsBairroEmprQuest() {
        return this.dsBairroEmprQuest;
    }

    /**
     * Set cd mun empr quest.
     * 
     * @param cdMunEmprQuest
     *            the cd mun empr quest
     */
    public void setCdMunEmprQuest(Long cdMunEmprQuest) {
        this.cdMunEmprQuest = cdMunEmprQuest;
    }

    /**
     * Get cd mun empr quest.
     * 
     * @return the cd mun empr quest
     */
    public Long getCdMunEmprQuest() {
        return this.cdMunEmprQuest;
    }

    /**
     * Set ds munc empre quest.
     * 
     * @param dsMuncEmpreQuest
     *            the ds munc empre quest
     */
    public void setDsMuncEmpreQuest(String dsMuncEmpreQuest) {
        this.dsMuncEmpreQuest = dsMuncEmpreQuest;
    }

    /**
     * Get ds munc empre quest.
     * 
     * @return the ds munc empre quest
     */
    public String getDsMuncEmpreQuest() {
        return this.dsMuncEmpreQuest;
    }

    /**
     * Set cd cep empr quest.
     * 
     * @param cdCepEmprQuest
     *            the cd cep empr quest
     */
    public void setCdCepEmprQuest(Integer cdCepEmprQuest) {
        this.cdCepEmprQuest = cdCepEmprQuest;
    }

    /**
     * Get cd cep empr quest.
     * 
     * @return the cd cep empr quest
     */
    public Integer getCdCepEmprQuest() {
        return this.cdCepEmprQuest;
    }

    /**
     * Set cd cep complemento.
     * 
     * @param cdCepComplemento
     *            the cd cep complemento
     */
    public void setCdCepComplemento(Integer cdCepComplemento) {
        this.cdCepComplemento = cdCepComplemento;
    }

    /**
     * Get cd cep complemento.
     * 
     * @return the cd cep complemento
     */
    public Integer getCdCepComplemento() {
        return this.cdCepComplemento;
    }

    /**
     * Set cd uf empre quest.
     * 
     * @param cdUfEmpreQuest
     *            the cd uf empre quest
     */
    public void setCdUfEmpreQuest(Integer cdUfEmpreQuest) {
        this.cdUfEmpreQuest = cdUfEmpreQuest;
    }

    /**
     * Get cd uf empre quest.
     * 
     * @return the cd uf empre quest
     */
    public Integer getCdUfEmpreQuest() {
        return this.cdUfEmpreQuest;
    }

    /**
     * Set ds uf empr quest.
     * 
     * @param dsUfEmprQuest
     *            the ds uf empr quest
     */
    public void setDsUfEmprQuest(String dsUfEmprQuest) {
        this.dsUfEmprQuest = dsUfEmprQuest;
    }

    /**
     * Get ds uf empr quest.
     * 
     * @return the ds uf empr quest
     */
    public String getDsUfEmprQuest() {
        return this.dsUfEmprQuest;
    }

    /**
     * Set cd agpto atividade quest.
     * 
     * @param cdAgptoAtividadeQuest
     *            the cd agpto atividade quest
     */
    public void setCdAgptoAtividadeQuest(Long cdAgptoAtividadeQuest) {
        this.cdAgptoAtividadeQuest = cdAgptoAtividadeQuest;
    }

    /**
     * Get cd agpto atividade quest.
     * 
     * @return the cd agpto atividade quest
     */
    public Long getCdAgptoAtividadeQuest() {
        return this.cdAgptoAtividadeQuest;
    }

    /**
     * Set ds ramo atividade quest.
     * 
     * @param dsRamoAtividadeQuest
     *            the ds ramo atividade quest
     */
    public void setDsRamoAtividadeQuest(String dsRamoAtividadeQuest) {
        this.dsRamoAtividadeQuest = dsRamoAtividadeQuest;
    }

    /**
     * Get ds ramo atividade quest.
     * 
     * @return the ds ramo atividade quest
     */
    public String getDsRamoAtividadeQuest() {
        return this.dsRamoAtividadeQuest;
    }

    /**
     * Set cd discagem direta inter empr.
     * 
     * @param cdDiscagemDiretaInterEmpr
     *            the cd discagem direta inter empr
     */
    public void setCdDiscagemDiretaInterEmpr(Integer cdDiscagemDiretaInterEmpr) {
        this.cdDiscagemDiretaInterEmpr = cdDiscagemDiretaInterEmpr;
    }

    /**
     * Get cd discagem direta inter empr.
     * 
     * @return the cd discagem direta inter empr
     */
    public Integer getCdDiscagemDiretaInterEmpr() {
        return this.cdDiscagemDiretaInterEmpr;
    }

    /**
     * Set cd discagem direta dist empr.
     * 
     * @param cdDiscagemDiretaDistEmpr
     *            the cd discagem direta dist empr
     */
    public void setCdDiscagemDiretaDistEmpr(Integer cdDiscagemDiretaDistEmpr) {
        this.cdDiscagemDiretaDistEmpr = cdDiscagemDiretaDistEmpr;
    }

    /**
     * Get cd discagem direta dist empr.
     * 
     * @return the cd discagem direta dist empr
     */
    public Integer getCdDiscagemDiretaDistEmpr() {
        return this.cdDiscagemDiretaDistEmpr;
    }

    /**
     * Set num fone empr quest.
     * 
     * @param numFoneEmprQuest
     *            the num fone empr quest
     */
    public void setNumFoneEmprQuest(Long numFoneEmprQuest) {
        this.numFoneEmprQuest = numFoneEmprQuest;
    }

    /**
     * Get num fone empr quest.
     * 
     * @return the num fone empr quest
     */
    public Long getNumFoneEmprQuest() {
        return this.numFoneEmprQuest;
    }

    /**
     * Set ds site empr quest.
     * 
     * @param dsSiteEmprQuest
     *            the ds site empr quest
     */
    public void setDsSiteEmprQuest(String dsSiteEmprQuest) {
        this.dsSiteEmprQuest = dsSiteEmprQuest;
    }

    /**
     * Get ds site empr quest.
     * 
     * @return the ds site empr quest
     */
    public String getDsSiteEmprQuest() {
        return this.dsSiteEmprQuest;
    }

    /**
     * Set cd func ger relacionamento.
     * 
     * @param cdFuncGerRelacionamento
     *            the cd func ger relacionamento
     */
    public void setCdFuncGerRelacionamento(Long cdFuncGerRelacionamento) {
        this.cdFuncGerRelacionamento = cdFuncGerRelacionamento;
    }

    /**
     * Get cd func ger relacionamento.
     * 
     * @return the cd func ger relacionamento
     */
    public Long getCdFuncGerRelacionamento() {
        return this.cdFuncGerRelacionamento;
    }

    /**
     * Set ds ger relacionamento quest.
     * 
     * @param dsGerRelacionamentoQuest
     *            the ds ger relacionamento quest
     */
    public void setDsGerRelacionamentoQuest(String dsGerRelacionamentoQuest) {
        this.dsGerRelacionamentoQuest = dsGerRelacionamentoQuest;
    }

    /**
     * Get ds ger relacionamento quest.
     * 
     * @return the ds ger relacionamento quest
     */
    public String getDsGerRelacionamentoQuest() {
        return this.dsGerRelacionamentoQuest;
    }

    /**
     * Set cd discagem direta dist ger.
     * 
     * @param cdDiscagemDiretaDistGer
     *            the cd discagem direta dist ger
     */
    public void setCdDiscagemDiretaDistGer(Integer cdDiscagemDiretaDistGer) {
        this.cdDiscagemDiretaDistGer = cdDiscagemDiretaDistGer;
    }

    /**
     * Get cd discagem direta dist ger.
     * 
     * @return the cd discagem direta dist ger
     */
    public Integer getCdDiscagemDiretaDistGer() {
        return this.cdDiscagemDiretaDistGer;
    }

    /**
     * Set cd discagem direta distan ger.
     * 
     * @param cdDiscagemDiretaDistanGer
     *            the cd discagem direta distan ger
     */
    public void setCdDiscagemDiretaDistanGer(Integer cdDiscagemDiretaDistanGer) {
        this.cdDiscagemDiretaDistanGer = cdDiscagemDiretaDistanGer;
    }

    /**
     * Get cd discagem direta distan ger.
     * 
     * @return the cd discagem direta distan ger
     */
    public Integer getCdDiscagemDiretaDistanGer() {
        return this.cdDiscagemDiretaDistanGer;
    }

    /**
     * Set num fone ger quest.
     * 
     * @param numFoneGerQuest
     *            the num fone ger quest
     */
    public void setNumFoneGerQuest(Long numFoneGerQuest) {
        this.numFoneGerQuest = numFoneGerQuest;
    }

    /**
     * Get num fone ger quest.
     * 
     * @return the num fone ger quest
     */
    public Long getNumFoneGerQuest() {
        return this.numFoneGerQuest;
    }

    /**
     * Set cd segmento empr quest.
     * 
     * @param cdSegmentoEmprQuest
     *            the cd segmento empr quest
     */
    public void setCdSegmentoEmprQuest(Integer cdSegmentoEmprQuest) {
        this.cdSegmentoEmprQuest = cdSegmentoEmprQuest;
    }

    /**
     * Get cd segmento empr quest.
     * 
     * @return the cd segmento empr quest
     */
    public Integer getCdSegmentoEmprQuest() {
        return this.cdSegmentoEmprQuest;
    }

    /**
     * Set ds abrev segmento empr.
     * 
     * @param dsAbrevSegmentoEmpr
     *            the ds abrev segmento empr
     */
    public void setDsAbrevSegmentoEmpr(String dsAbrevSegmentoEmpr) {
        this.dsAbrevSegmentoEmpr = dsAbrevSegmentoEmpr;
    }

    /**
     * Get ds abrev segmento empr.
     * 
     * @return the ds abrev segmento empr
     */
    public String getDsAbrevSegmentoEmpr() {
        return this.dsAbrevSegmentoEmpr;
    }

    /**
     * Set cd indicador fl quest.
     * 
     * @param cdIndicadorFlQuest
     *            the cd indicador fl quest
     */
    public void setCdIndicadorFlQuest(Integer cdIndicadorFlQuest) {
        this.cdIndicadorFlQuest = cdIndicadorFlQuest;
    }

    /**
     * Get cd indicador fl quest.
     * 
     * @return the cd indicador fl quest
     */
    public Integer getCdIndicadorFlQuest() {
        return this.cdIndicadorFlQuest;
    }

    /**
     * Set ds indicador fl quest.
     * 
     * @param dsIndicadorFlQuest
     *            the ds indicador fl quest
     */
    public void setDsIndicadorFlQuest(String dsIndicadorFlQuest) {
        this.dsIndicadorFlQuest = dsIndicadorFlQuest;
    }

    /**
     * Get ds indicador fl quest.
     * 
     * @return the ds indicador fl quest
     */
    public String getDsIndicadorFlQuest() {
        return this.dsIndicadorFlQuest;
    }

    /**
     * Set pctgm pre nov ano quest.
     * 
     * @param pctgmPreNovAnoQuest
     *            the pctgm pre nov ano quest
     */
    public void setPctgmPreNovAnoQuest(BigDecimal pctgmPreNovAnoQuest) {
        this.pctgmPreNovAnoQuest = pctgmPreNovAnoQuest;
    }

    /**
     * Get pctgm pre nov ano quest.
     * 
     * @return the pctgm pre nov ano quest
     */
    public BigDecimal getPctgmPreNovAnoQuest() {
        return this.pctgmPreNovAnoQuest;
    }

    /**
     * Set cdprefe csign.
     * 
     * @param cdprefeCsign
     *            the cdprefe csign
     */
    public void setCdprefeCsign(Integer cdprefeCsign) {
        this.cdprefeCsign = cdprefeCsign;
    }

    /**
     * Get cdprefe csign.
     * 
     * @return the cdprefe csign
     */
    public Integer getCdprefeCsign() {
        return this.cdprefeCsign;
    }

    /**
     * Set cdprefe pab cren.
     * 
     * @param cdprefePabCren
     *            the cdprefe pab cren
     */
    public void setCdprefePabCren(Integer cdprefePabCren) {
        this.cdprefePabCren = cdprefePabCren;
    }

    /**
     * Get cdprefe pab cren.
     * 
     * @return the cdprefe pab cren
     */
    public Integer getCdprefePabCren() {
        return this.cdprefePabCren;
    }

    /**
     * Set cd indicador pae ccren.
     * 
     * @param cdIndicadorPaeCcren
     *            the cd indicador pae ccren
     */
    public void setCdIndicadorPaeCcren(Integer cdIndicadorPaeCcren) {
        this.cdIndicadorPaeCcren = cdIndicadorPaeCcren;
    }

    /**
     * Get cd indicador pae ccren.
     * 
     * @return the cd indicador pae ccren
     */
    public Integer getCdIndicadorPaeCcren() {
        return this.cdIndicadorPaeCcren;
    }

    /**
     * Set cd indicador ag ccren.
     * 
     * @param cdIndicadorAgCcren
     *            the cd indicador ag ccren
     */
    public void setCdIndicadorAgCcren(Integer cdIndicadorAgCcren) {
        this.cdIndicadorAgCcren = cdIndicadorAgCcren;
    }

    /**
     * Get cd indicador ag ccren.
     * 
     * @return the cd indicador ag ccren
     */
    public Integer getCdIndicadorAgCcren() {
        return this.cdIndicadorAgCcren;
    }

    /**
     * Set cd indicador insta estrt.
     * 
     * @param cdIndicadorInstaEstrt
     *            the cd indicador insta estrt
     */
    public void setCdIndicadorInstaEstrt(Integer cdIndicadorInstaEstrt) {
        this.cdIndicadorInstaEstrt = cdIndicadorInstaEstrt;
    }

    /**
     * Get cd indicador insta estrt.
     * 
     * @return the cd indicador insta estrt
     */
    public Integer getCdIndicadorInstaEstrt() {
        return this.cdIndicadorInstaEstrt;
    }

    /**
     * Set cd estrt insta empr.
     * 
     * @param cdEstrtInstaEmpr
     *            the cd estrt insta empr
     */
    public void setCdEstrtInstaEmpr(Integer cdEstrtInstaEmpr) {
        this.cdEstrtInstaEmpr = cdEstrtInstaEmpr;
    }

    /**
     * Get cd estrt insta empr.
     * 
     * @return the cd estrt insta empr
     */
    public Integer getCdEstrtInstaEmpr() {
        return this.cdEstrtInstaEmpr;
    }

    /**
     * Set ds estrutura insta empr.
     * 
     * @param dsEstruturaInstaEmpr
     *            the ds estrutura insta empr
     */
    public void setDsEstruturaInstaEmpr(String dsEstruturaInstaEmpr) {
        this.dsEstruturaInstaEmpr = dsEstruturaInstaEmpr;
    }

    /**
     * Get ds estrutura insta empr.
     * 
     * @return the ds estrutura insta empr
     */
    public String getDsEstruturaInstaEmpr() {
        return this.dsEstruturaInstaEmpr;
    }

    /**
     * Set cdbanco vinc insta.
     * 
     * @param cdbancoVincInsta
     *            the cdbanco vinc insta
     */
    public void setCdbancoVincInsta(Integer cdbancoVincInsta) {
        this.cdbancoVincInsta = cdbancoVincInsta;
    }

    /**
     * Get cdbanco vinc insta.
     * 
     * @return the cdbanco vinc insta
     */
    public Integer getCdbancoVincInsta() {
        return this.cdbancoVincInsta;
    }

    /**
     * Set cd agencia vinc insta.
     * 
     * @param cdAgenciaVincInsta
     *            the cd agencia vinc insta
     */
    public void setCdAgenciaVincInsta(Integer cdAgenciaVincInsta) {
        this.cdAgenciaVincInsta = cdAgenciaVincInsta;
    }

    /**
     * Get cd agencia vinc insta.
     * 
     * @return the cd agencia vinc insta
     */
    public Integer getCdAgenciaVincInsta() {
        return this.cdAgenciaVincInsta;
    }

    /**
     * Set ds agencia vinc insta.
     * 
     * @param dsAgenciaVincInsta
     *            the ds agencia vinc insta
     */
    public void setDsAgenciaVincInsta(String dsAgenciaVincInsta) {
        this.dsAgenciaVincInsta = dsAgenciaVincInsta;
    }

    /**
     * Get ds agencia vinc insta.
     * 
     * @return the ds agencia vinc insta
     */
    public String getDsAgenciaVincInsta() {
        return this.dsAgenciaVincInsta;
    }

    /**
     * Set cd unidade medd insta.
     * 
     * @param cdUnidadeMeddInsta
     *            the cd unidade medd insta
     */
    public void setCdUnidadeMeddInsta(Integer cdUnidadeMeddInsta) {
        this.cdUnidadeMeddInsta = cdUnidadeMeddInsta;
    }

    /**
     * Get cd unidade medd insta.
     * 
     * @return the cd unidade medd insta
     */
    public Integer getCdUnidadeMeddInsta() {
        return this.cdUnidadeMeddInsta;
    }

    /**
     * Set ds abrev insta.
     * 
     * @param dsAbrevInsta
     *            the ds abrev insta
     */
    public void setDsAbrevInsta(String dsAbrevInsta) {
        this.dsAbrevInsta = dsAbrevInsta;
    }

    /**
     * Get ds abrev insta.
     * 
     * @return the ds abrev insta
     */
    public String getDsAbrevInsta() {
        return this.dsAbrevInsta;
    }

    /**
     * Set cd distc agencia insta.
     * 
     * @param cdDistcAgenciaInsta
     *            the cd distc agencia insta
     */
    public void setCdDistcAgenciaInsta(Integer cdDistcAgenciaInsta) {
        this.cdDistcAgenciaInsta = cdDistcAgenciaInsta;
    }

    /**
     * Get cd distc agencia insta.
     * 
     * @return the cd distc agencia insta
     */
    public Integer getCdDistcAgenciaInsta() {
        return this.cdDistcAgenciaInsta;
    }

    /**
     * Set cd indicador visita insta.
     * 
     * @param cdIndicadorVisitaInsta
     *            the cd indicador visita insta
     */
    public void setCdIndicadorVisitaInsta(Integer cdIndicadorVisitaInsta) {
        this.cdIndicadorVisitaInsta = cdIndicadorVisitaInsta;
    }

    /**
     * Get cd indicador visita insta.
     * 
     * @return the cd indicador visita insta
     */
    public Integer getCdIndicadorVisitaInsta() {
        return this.cdIndicadorVisitaInsta;
    }

    /**
     * Set ds indicador visita insta.
     * 
     * @param dsIndicadorVisitaInsta
     *            the ds indicador visita insta
     */
    public void setDsIndicadorVisitaInsta(String dsIndicadorVisitaInsta) {
        this.dsIndicadorVisitaInsta = dsIndicadorVisitaInsta;
    }

    /**
     * Get ds indicador visita insta.
     * 
     * @return the ds indicador visita insta
     */
    public String getDsIndicadorVisitaInsta() {
        return this.dsIndicadorVisitaInsta;
    }

    /**
     * Set cd func ger visitante.
     * 
     * @param cdFuncGerVisitante
     *            the cd func ger visitante
     */
    public void setCdFuncGerVisitante(Long cdFuncGerVisitante) {
        this.cdFuncGerVisitante = cdFuncGerVisitante;
    }

    /**
     * Get cd func ger visitante.
     * 
     * @return the cd func ger visitante
     */
    public Long getCdFuncGerVisitante() {
        return this.cdFuncGerVisitante;
    }

    /**
     * Set ds ger visitante insta.
     * 
     * @param dsGerVisitanteInsta
     *            the ds ger visitante insta
     */
    public void setDsGerVisitanteInsta(String dsGerVisitanteInsta) {
        this.dsGerVisitanteInsta = dsGerVisitanteInsta;
    }

    /**
     * Get ds ger visitante insta.
     * 
     * @return the ds ger visitante insta
     */
    public String getDsGerVisitanteInsta() {
        return this.dsGerVisitanteInsta;
    }

    /**
     * Set cd unidade medd espac.
     * 
     * @param cdUnidadeMeddEspac
     *            the cd unidade medd espac
     */
    public void setCdUnidadeMeddEspac(Integer cdUnidadeMeddEspac) {
        this.cdUnidadeMeddEspac = cdUnidadeMeddEspac;
    }

    /**
     * Get cd unidade medd espac.
     * 
     * @return the cd unidade medd espac
     */
    public Integer getCdUnidadeMeddEspac() {
        return this.cdUnidadeMeddEspac;
    }

    /**
     * Set ds abrev espac concedido.
     * 
     * @param dsAbrevEspacConcedido
     *            the ds abrev espac concedido
     */
    public void setDsAbrevEspacConcedido(String dsAbrevEspacConcedido) {
        this.dsAbrevEspacConcedido = dsAbrevEspacConcedido;
    }

    /**
     * Get ds abrev espac concedido.
     * 
     * @return the ds abrev espac concedido
     */
    public String getDsAbrevEspacConcedido() {
        return this.dsAbrevEspacConcedido;
    }

    /**
     * Set cd uespac concedido insta.
     * 
     * @param cdUespacConcedidoInsta
     *            the cd uespac concedido insta
     */
    public void setCdUespacConcedidoInsta(Integer cdUespacConcedidoInsta) {
        this.cdUespacConcedidoInsta = cdUespacConcedidoInsta;
    }

    /**
     * Get cd uespac concedido insta.
     * 
     * @return the cd uespac concedido insta
     */
    public Integer getCdUespacConcedidoInsta() {
        return this.cdUespacConcedidoInsta;
    }

    /**
     * Set cd tipo neces insta.
     * 
     * @param cdTipoNecesInsta
     *            the cd tipo neces insta
     */
    public void setCdTipoNecesInsta(Integer cdTipoNecesInsta) {
        this.cdTipoNecesInsta = cdTipoNecesInsta;
    }

    /**
     * Get cd tipo neces insta.
     * 
     * @return the cd tipo neces insta
     */
    public Integer getCdTipoNecesInsta() {
        return this.cdTipoNecesInsta;
    }

    /**
     * Set ds tipo necess insta.
     * 
     * @param dsTipoNecessInsta
     *            the ds tipo necess insta
     */
    public void setDsTipoNecessInsta(String dsTipoNecessInsta) {
        this.dsTipoNecessInsta = dsTipoNecessInsta;
    }

    /**
     * Get ds tipo necess insta.
     * 
     * @return the ds tipo necess insta
     */
    public String getDsTipoNecessInsta() {
        return this.dsTipoNecessInsta;
    }

    /**
     * Set cd indicador perda relacionamento.
     * 
     * @param cdIndicadorPerdaRelacionamento
     *            the cd indicador perda relacionamento
     */
    public void setCdIndicadorPerdaRelacionamento(Integer cdIndicadorPerdaRelacionamento) {
        this.cdIndicadorPerdaRelacionamento = cdIndicadorPerdaRelacionamento;
    }

    /**
     * Get cd indicador perda relacionamento.
     * 
     * @return the cd indicador perda relacionamento
     */
    public Integer getCdIndicadorPerdaRelacionamento() {
        return this.cdIndicadorPerdaRelacionamento;
    }

    /**
     * Set ds indicador perdarelacionamento.
     * 
     * @param dsIndicadorPerdarelacionamento
     *            the ds indicador perdarelacionamento
     */
    public void setDsIndicadorPerdarelacionamento(String dsIndicadorPerdarelacionamento) {
        this.dsIndicadorPerdarelacionamento = dsIndicadorPerdarelacionamento;
    }

    /**
     * Get ds indicador perdarelacionamento.
     * 
     * @return the ds indicador perdarelacionamento
     */
    public String getDsIndicadorPerdarelacionamento() {
        return this.dsIndicadorPerdarelacionamento;
    }

    /**
     * Set cd pagamento sal credt.
     * 
     * @param cdPagamentoSalCredt
     *            the cd pagamento sal credt
     */
    public void setCdPagamentoSalCredt(Integer cdPagamentoSalCredt) {
        this.cdPagamentoSalCredt = cdPagamentoSalCredt;
    }

    /**
     * Get cd pagamento sal credt.
     * 
     * @return the cd pagamento sal credt
     */
    public Integer getCdPagamentoSalCredt() {
        return this.cdPagamentoSalCredt;
    }

    /**
     * Set cd pagamento sal creque.
     * 
     * @param cdPagamentoSalCreque
     *            the cd pagamento sal creque
     */
    public void setCdPagamentoSalCreque(Integer cdPagamentoSalCreque) {
        this.cdPagamentoSalCreque = cdPagamentoSalCreque;
    }

    /**
     * Get cd pagamento sal creque.
     * 
     * @return the cd pagamento sal creque
     */
    public Integer getCdPagamentoSalCreque() {
        return this.cdPagamentoSalCreque;
    }

    /**
     * Set cd pagamento sal dinheiro.
     * 
     * @param cdPagamentoSalDinheiro
     *            the cd pagamento sal dinheiro
     */
    public void setCdPagamentoSalDinheiro(Integer cdPagamentoSalDinheiro) {
        this.cdPagamentoSalDinheiro = cdPagamentoSalDinheiro;
    }

    /**
     * Get cd pagamento sal dinheiro.
     * 
     * @return the cd pagamento sal dinheiro
     */
    public Integer getCdPagamentoSalDinheiro() {
        return this.cdPagamentoSalDinheiro;
    }

    /**
     * Set cd pagamento sal divers.
     * 
     * @param cdPagamentoSalDivers
     *            the cd pagamento sal divers
     */
    public void setCdPagamentoSalDivers(Integer cdPagamentoSalDivers) {
        this.cdPagamentoSalDivers = cdPagamentoSalDivers;
    }

    /**
     * Get cd pagamento sal divers.
     * 
     * @return the cd pagamento sal divers
     */
    public Integer getCdPagamentoSalDivers() {
        return this.cdPagamentoSalDivers;
    }

    /**
     * Set ds pagamento sal divers.
     * 
     * @param dsPagamentoSalDivers
     *            the ds pagamento sal divers
     */
    public void setDsPagamentoSalDivers(String dsPagamentoSalDivers) {
        this.dsPagamentoSalDivers = dsPagamentoSalDivers;
    }

    /**
     * Get ds pagamento sal divers.
     * 
     * @return the ds pagamento sal divers
     */
    public String getDsPagamentoSalDivers() {
        return this.dsPagamentoSalDivers;
    }

    /**
     * Set ocorrencias1.
     * 
     * @param ocorrencias1
     *            the ocorrencias1
     */
    public void setOcorrencias1(List<DetalharPiramideOcorrencias1SaidaDTO> ocorrencias1) {
        this.ocorrencias1 = ocorrencias1;
    }

    /**
     * Get ocorrencias1.
     * 
     * @return the ocorrencias1
     */
    public List<DetalharPiramideOcorrencias1SaidaDTO> getOcorrencias1() {
        return this.ocorrencias1;
    }

    /**
     * Set ocorrencias2.
     * 
     * @param ocorrencias2
     *            the ocorrencias2
     */
    public void setOcorrencias2(List<DetalharPiramideOcorrencias2SaidaDTO> ocorrencias2) {
        this.ocorrencias2 = ocorrencias2;
    }

    /**
     * Get ocorrencias2.
     * 
     * @return the ocorrencias2
     */
    public List<DetalharPiramideOcorrencias2SaidaDTO> getOcorrencias2() {
        return this.ocorrencias2;
    }

    /**
     * Set ocorrencias3.
     * 
     * @param ocorrencias3
     *            the ocorrencias3
     */
    public void setOcorrencias3(List<DetalharPiramideOcorrencias3SaidaDTO> ocorrencias3) {
        this.ocorrencias3 = ocorrencias3;
    }

    /**
     * Get ocorrencias3.
     * 
     * @return the ocorrencias3
     */
    public List<DetalharPiramideOcorrencias3SaidaDTO> getOcorrencias3() {
        return this.ocorrencias3;
    }

    /**
     * Set ocorrencias4.
     * 
     * @param ocorrencias4
     *            the ocorrencias4
     */
    public void setOcorrencias4(List<DetalharPiramideOcorrencias4SaidaDTO> ocorrencias4) {
        this.ocorrencias4 = ocorrencias4;
    }

    /**
     * Get ocorrencias4.
     * 
     * @return the ocorrencias4
     */
    public List<DetalharPiramideOcorrencias4SaidaDTO> getOcorrencias4() {
        return this.ocorrencias4;
    }

    /**
     * Set ocorrencias5.
     * 
     * @param ocorrencias5
     *            the ocorrencias5
     */
    public void setOcorrencias5(List<DetalharPiramideOcorrencias5SaidaDTO> ocorrencias5) {
        this.ocorrencias5 = ocorrencias5;
    }

    /**
     * Get ocorrencias5.
     * 
     * @return the ocorrencias5
     */
    public List<DetalharPiramideOcorrencias5SaidaDTO> getOcorrencias5() {
        return this.ocorrencias5;
    }

    /**
     * Set ocorrencias6.
     * 
     * @param ocorrencias6
     *            the ocorrencias6
     */
    public void setOcorrencias6(List<DetalharPiramideOcorrencias6SaidaDTO> ocorrencias6) {
        this.ocorrencias6 = ocorrencias6;
    }

    /**
     * Get ocorrencias6.
     * 
     * @return the ocorrencias6
     */
    public List<DetalharPiramideOcorrencias6SaidaDTO> getOcorrencias6() {
        return this.ocorrencias6;
    }

    /**
     * Nome: getTotalQtdFuncionarioFilial01
     *
     * @return totalQtdFuncionarioFilial01
     */
    public Long getTotalQtdFuncionarioFilial01() {
        return totalQtdFuncionarioFilial01;
    }

    /**
     * Nome: setTotalQtdFuncionarioFilial01
     *
     * @param totalQtdFuncionarioFilial01
     */
    public void setTotalQtdFuncionarioFilial01(Long totalQtdFuncionarioFilial01) {
        this.totalQtdFuncionarioFilial01 = totalQtdFuncionarioFilial01;
    }

    /**
     * Nome: getTotalQtdFuncionarioFilial02
     *
     * @return totalQtdFuncionarioFilial02
     */
    public Long getTotalQtdFuncionarioFilial02() {
        return totalQtdFuncionarioFilial02;
    }

    /**
     * Nome: setTotalQtdFuncionarioFilial02
     *
     * @param totalQtdFuncionarioFilial02
     */
    public void setTotalQtdFuncionarioFilial02(Long totalQtdFuncionarioFilial02) {
        this.totalQtdFuncionarioFilial02 = totalQtdFuncionarioFilial02;
    }

    /**
     * Nome: getTotalQtdFuncionarioFilial03
     *
     * @return totalQtdFuncionarioFilial03
     */
    public Long getTotalQtdFuncionarioFilial03() {
        return totalQtdFuncionarioFilial03;
    }

    /**
     * Nome: setTotalQtdFuncionarioFilial03
     *
     * @param totalQtdFuncionarioFilial03
     */
    public void setTotalQtdFuncionarioFilial03(Long totalQtdFuncionarioFilial03) {
        this.totalQtdFuncionarioFilial03 = totalQtdFuncionarioFilial03;
    }

    /**
     * Nome: getTotalQtdFuncionarioFilial04
     *
     * @return totalQtdFuncionarioFilial04
     */
    public Long getTotalQtdFuncionarioFilial04() {
        return totalQtdFuncionarioFilial04;
    }

    /**
     * Nome: setTotalQtdFuncionarioFilial04
     *
     * @param totalQtdFuncionarioFilial04
     */
    public void setTotalQtdFuncionarioFilial04(Long totalQtdFuncionarioFilial04) {
        this.totalQtdFuncionarioFilial04 = totalQtdFuncionarioFilial04;
    }

    /**
     * Nome: getTotalQtdFuncionarioFilial05
     *
     * @return totalQtdFuncionarioFilial05
     */
    public Long getTotalQtdFuncionarioFilial05() {
        return totalQtdFuncionarioFilial05;
    }

    /**
     * Nome: setTotalQtdFuncionarioFilial05
     *
     * @param totalQtdFuncionarioFilial05
     */
    public void setTotalQtdFuncionarioFilial05(Long totalQtdFuncionarioFilial05) {
        this.totalQtdFuncionarioFilial05 = totalQtdFuncionarioFilial05;
    }

    /**
     * Nome: getTotalQtdFuncionarioFilial06
     *
     * @return totalQtdFuncionarioFilial06
     */
    public Long getTotalQtdFuncionarioFilial06() {
        return totalQtdFuncionarioFilial06;
    }

    /**
     * Nome: setTotalQtdFuncionarioFilial06
     *
     * @param totalQtdFuncionarioFilial06
     */
    public void setTotalQtdFuncionarioFilial06(Long totalQtdFuncionarioFilial06) {
        this.totalQtdFuncionarioFilial06 = totalQtdFuncionarioFilial06;
    }

    /**
     * Nome: getTotalQtdFuncionarioFilial07
     *
     * @return totalQtdFuncionarioFilial07
     */
    public Long getTotalQtdFuncionarioFilial07() {
        return totalQtdFuncionarioFilial07;
    }

    /**
     * Nome: setTotalQtdFuncionarioFilial07
     *
     * @param totalQtdFuncionarioFilial07
     */
    public void setTotalQtdFuncionarioFilial07(Long totalQtdFuncionarioFilial07) {
        this.totalQtdFuncionarioFilial07 = totalQtdFuncionarioFilial07;
    }

    /**
     * Nome: getTotalQtdFuncionarioFilial08
     *
     * @return totalQtdFuncionarioFilial08
     */
    public Long getTotalQtdFuncionarioFilial08() {
        return totalQtdFuncionarioFilial08;
    }

    /**
     * Nome: setTotalQtdFuncionarioFilial08
     *
     * @param totalQtdFuncionarioFilial08
     */
    public void setTotalQtdFuncionarioFilial08(Long totalQtdFuncionarioFilial08) {
        this.totalQtdFuncionarioFilial08 = totalQtdFuncionarioFilial08;
    }

    /**
     * Nome: getTotalQtdFuncionarioFilial09
     *
     * @return totalQtdFuncionarioFilial09
     */
    public Long getTotalQtdFuncionarioFilial09() {
        return totalQtdFuncionarioFilial09;
    }

    /**
     * Nome: setTotalQtdFuncionarioFilial09
     *
     * @param totalQtdFuncionarioFilial09
     */
    public void setTotalQtdFuncionarioFilial09(Long totalQtdFuncionarioFilial09) {
        this.totalQtdFuncionarioFilial09 = totalQtdFuncionarioFilial09;
    }

    /**
     * Nome: getTotalQtdFuncionarioFilial10
     *
     * @return totalQtdFuncionarioFilial10
     */
    public Long getTotalQtdFuncionarioFilial10() {
        return totalQtdFuncionarioFilial10;
    }

    /**
     * Nome: setTotalQtdFuncionarioFilial10
     *
     * @param totalQtdFuncionarioFilial10
     */
    public void setTotalQtdFuncionarioFilial10(Long totalQtdFuncionarioFilial10) {
        this.totalQtdFuncionarioFilial10 = totalQtdFuncionarioFilial10;
    }

    /**
     * Nome: getTotalQtdFuncCasaQuest
     *
     * @return totalQtdFuncCasaQuest
     */
    public Long getTotalQtdFuncCasaQuest() {
        return totalQtdFuncCasaQuest;
    }

    /**
     * Nome: setTotalQtdFuncCasaQuest
     *
     * @param totalQtdFuncCasaQuest
     */
    public void setTotalQtdFuncCasaQuest(Long totalQtdFuncCasaQuest) {
        this.totalQtdFuncCasaQuest = totalQtdFuncCasaQuest;
    }

    /**
     * Nome: getTotalQtdFuncDeslgQuest
     *
     * @return totalQtdFuncDeslgQuest
     */
    public Long getTotalQtdFuncDeslgQuest() {
        return totalQtdFuncDeslgQuest;
    }

    /**
     * Nome: setTotalQtdFuncDeslgQuest
     *
     * @param totalQtdFuncDeslgQuest
     */
    public void setTotalQtdFuncDeslgQuest(Long totalQtdFuncDeslgQuest) {
        this.totalQtdFuncDeslgQuest = totalQtdFuncDeslgQuest;
    }

    /**
     * Nome: getTotalPtcRenovFaixaQuest
     *
     * @return totalPtcRenovFaixaQuest
     */
    public BigDecimal getTotalPtcRenovFaixaQuest() {
        return totalPtcRenovFaixaQuest;
    }

    /**
     * Nome: setTotalPtcRenovFaixaQuest
     *
     * @param totalPtcRenovFaixaQuest
     */
    public void setTotalPtcRenovFaixaQuest(BigDecimal totalPtcRenovFaixaQuest) {
        this.totalPtcRenovFaixaQuest = totalPtcRenovFaixaQuest;
    }

    /**
     * Nome: getDtMesAnoCorrespondente
     *
     * @return dtMesAnoCorrespondente
     */
    public Integer getDtMesAnoCorrespondente() {
        return dtMesAnoCorrespondente;
    }

    /**
     * Nome: setDtMesAnoCorrespondente
     *
     * @param dtMesAnoCorrespondente
     */
    public void setDtMesAnoCorrespondente(Integer dtMesAnoCorrespondente) {
        this.dtMesAnoCorrespondente = dtMesAnoCorrespondente;
    }

    /**
     * Get pagamento.
     * 
     * @return the pagamento
     */
    public String getPagamento() {
        if (cdPagamentoSalarialMes != null && cdPagamentoSalarialQzena != null) {
            if (cdPagamentoSalarialMes == 1 && cdPagamentoSalarialQzena == 1) {
                return "Quinzenal e Mensal";
            } else if (cdPagamentoSalarialMes == 1) {
                return "Mensal";
            } else if (cdPagamentoSalarialQzena == 1) {
                return "Quinzenal";
            }
        }

        return "";
    }

    /**
     * Get indicador grupo economico.
     * 
     * @return the indicador grupo economico
     */
    public String getIndicadorGrupoEconomico() {
        if (cdIndicadorGrpQuest != null) {
            if (cdIndicadorGrpQuest == 1) {
                return "Sim";
            } else if (cdIndicadorGrpQuest == 2) {
                return "N�o";
            }
        }

        return "";
    }

    /**
     * Get indicador preferencia credito consignado.
     * 
     * @return the indicador preferencia credito consignado
     */
    public String getIndicadorPreferenciaCreditoConsignado() {
        if (cdprefeCsign != null) {
            if (cdprefeCsign == 1) {
                return "Sim";
            } else if (cdprefeCsign == 2) {
                return "N�o";
            }
        }

        return "";
    }

    /**
     * Get: indicadorInstalacaoEstrutura.
     * 
     * @return indicadorInstalacaoEstrutura
     */
    public String getIndicadorInstalacaoEstrutura() {
        if (cdIndicadorInstaEstrt != null) {
            if (cdIndicadorInstaEstrt == 1) {
                return "Sim";
            } else if (cdIndicadorInstaEstrt == 2) {
                return "N�o";
            }
        }

        return "";
    }

    /**
     * Get cep empresa.
     * 
     * @return the cep empresa
     */
    public String getCepEmpresa() {
        return PgitUtil.formatarCep(cdCepEmprQuest, cdCepComplemento);
    }

    /**
     * Get telefone empresa.
     * 
     * @return the telefone empresa
     */
    public String getTelefoneEmpresa() {
        return PgitUtil.formatarTelefone(cdDiscagemDiretaDistEmpr, numFoneEmprQuest);
    }

    /**
     * Get telefone empresa.
     * 
     * @return the telefone empresa
     */
    public String getTelefoneContato() {
        return PgitUtil.formatarTelefone(cdDiscagemDiretaDistanGer, numFoneGerQuest);
    }

    /**
     * Nome: getMesAnoCorrespondente
     * 
     * @return
     */
    public String getMesAnoCorrespondente() {
        String mesAnoNumerico = NumberUtils.format(dtMesAnoCorrespondente, "000000");
        return String.format("%s/%s", mesAnoNumerico.substring(0, 2), mesAnoNumerico.substring(2));
    }

    /**
     * Nome: getCodigoGrupoEconomico
     * 
     * @return
     */
    public String getCodigoGrupoEconomico() {
        return String.format("%s %d", dsGrupoEconmQuest, nroGrupoEconmQuest);
    }

    /**
     * Nome: getDistanciaAgEmpresa
     * 
     * @return
     */
    public String getDistanciaAgEmpresa() {
        return String.format("%sKm", NumberUtils.format(cdUdistcAgEmpr, "#,##0"));
    }

    /**
     * Nome: getAtendimentoConcorrente
     * 
     * @return
     */
    public String getAtendimentoConcorrente() {
        String paEmpresas = " ";
        if (cdprefePabCren != null && cdprefePabCren == 1) {
            paEmpresas = "X";
        }
        String pae = " ";
        if (cdIndicadorPaeCcren != null && cdIndicadorPaeCcren == 1) {
            pae = "X";
        }
        String redeAg = " ";
        if (cdIndicadorAgCcren != null && cdIndicadorAgCcren == 1) {
            redeAg = "X";
        }
        String naoPossui = " ";
        if (paEmpresas.trim().length() == 0 && pae.trim().length() == 0 && redeAg.trim().length() == 0) {
            naoPossui = "X";
        }

        return String.format(
            "(%s) N�o possui (%s) PA-Empresas (%s) PAE (%s) Atendimento realizado pela rede de ag�ncias", naoPossui,
            paEmpresas, pae, redeAg);
    }

    /**
     * Nome: getPorcentagemAdmissaoDemissao
     * 
     * @return
     */
    public String getPorcentagemAdmissaoDemissao() {
        StringBuilder porcentagemAdmissaoDemissao = new StringBuilder();
        porcentagemAdmissaoDemissao.append(NumberUtils.format(pctgmPreNovAnoQuest));
        porcentagemAdmissaoDemissao.append("%");
        return porcentagemAdmissaoDemissao.toString();
    }

    /**
     * Nome: getDistanciaAgInstalacao
     * 
     * @return
     */
    public String getDistanciaAgInstalacao() {
        return String.format("%sKm", NumberUtils.format(cdAgenciaVincInsta, "#,##0"));
    }

    /**
     * Nome: getAreaInstalacao
     * 
     * @return
     */
    public Object getAreaInstalacao() {
        return String.format("%sm�", NumberUtils.format(cdUespacConcedidoInsta, "#,##0"));
    }

}