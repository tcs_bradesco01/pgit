package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;


/**
 * Arquivo criado em 29/02/16.
 */
public class ConsultarManutencaoConfigContratoEntradaDTO {

    private Long cdPessoaJuridica;

    private Integer cdTipoContratoNegocio;

    private Long nrSequenciaContratoNegocio;

    private Integer cdTipoManutencaoContrato;

    private Long nrManutencaoContratoNegocio;

    public void setCdPessoaJuridica(Long cdPessoaJuridica) {
        this.cdPessoaJuridica = cdPessoaJuridica;
    }

    public Long getCdPessoaJuridica() {
        return this.cdPessoaJuridica;
    }

    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }

    public void setCdTipoManutencaoContrato(Integer cdTipoManutencaoContrato) {
        this.cdTipoManutencaoContrato = cdTipoManutencaoContrato;
    }

    public Integer getCdTipoManutencaoContrato() {
        return this.cdTipoManutencaoContrato;
    }

    public void setNrManutencaoContratoNegocio(Long nrManutencaoContratoNegocio) {
        this.nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
    }

    public Long getNrManutencaoContratoNegocio() {
        return this.nrManutencaoContratoNegocio;
    }
}