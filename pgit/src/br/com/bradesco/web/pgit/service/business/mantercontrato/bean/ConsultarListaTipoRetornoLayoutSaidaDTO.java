/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ConsultarListaTipoRetornoLayoutSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaTipoRetornoLayoutSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
    
    /** Atributo cdTipoArquivoRetorno. */
    private int cdTipoArquivoRetorno;
    
    /** Atributo dsTipoArquivoRetorno. */
    private String dsTipoArquivoRetorno;
	
	/** Atributo dtInclusaoRegistro. */
	private String dtInclusaoRegistro;

	/**
	 * Get: cdTipoArquivoRetorno.
	 *
	 * @return cdTipoArquivoRetorno
	 */
	public int getCdTipoArquivoRetorno() {
		return cdTipoArquivoRetorno;
	}
	
	/**
	 * Set: cdTipoArquivoRetorno.
	 *
	 * @param cdTipoArquivoRetorno the cd tipo arquivo retorno
	 */
	public void setCdTipoArquivoRetorno(int cdTipoArquivoRetorno) {
		this.cdTipoArquivoRetorno = cdTipoArquivoRetorno;
	}
	
	/**
	 * Get: dsTipoArquivoRetorno.
	 *
	 * @return dsTipoArquivoRetorno
	 */
	public String getDsTipoArquivoRetorno() {
		return dsTipoArquivoRetorno;
	}
	
	/**
	 * Set: dsTipoArquivoRetorno.
	 *
	 * @param dsTipoArquivoRetorno the ds tipo arquivo retorno
	 */
	public void setDsTipoArquivoRetorno(String dsTipoArquivoRetorno) {
		this.dsTipoArquivoRetorno = dsTipoArquivoRetorno;
	}
	
	/**
	 * Get: dtInclusaoRegistro.
	 *
	 * @return dtInclusaoRegistro
	 */
	public String getDtInclusaoRegistro() {
		return dtInclusaoRegistro;
	}
	
	/**
	 * Set: dtInclusaoRegistro.
	 *
	 * @param dtInclusaoRegistro the dt inclusao registro
	 */
	public void setDtInclusaoRegistro(String dtInclusaoRegistro) {
		this.dtInclusaoRegistro = dtInclusaoRegistro;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
