/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

/**
 * Nome: ConsultarDetalheContaFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDetalheContaFavorecidoSaidaDTO {
	
	/** Atributo tpConta. */
	private String tpConta;
    
    /** Atributo dsBloqueio. */
    private String dsBloqueio;
    
    /** Atributo cdBloqueioContaFavorecido. */
    private Integer cdBloqueioContaFavorecido;
    
    /** Atributo dsObservacaoContaFavorecida. */
    private String dsObservacaoContaFavorecida;
    
    /** Atributo cdIndicadorRetornoCliente. */
    private Integer cdIndicadorRetornoCliente;
    
    /** Atributo cdSituacaoContaFavorecido. */
    private Integer cdSituacaoContaFavorecido;
    
    /** Atributo hrBloqueioContaFavorecido. */
    private String hrBloqueioContaFavorecido;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExterno. */
    private String cdUsuarioInclusaoExterno;
    
    /** Atributo dtInclusao. */
    private String dtInclusao;
    
    /** Atributo hrInclusao. */
    private String hrInclusao;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoExterna. */
    private String cdUsuarioManutencaoExterna;
    
    /** Atributo dtManutencao. */
    private String dtManutencao;
    
    /** Atributo hrManutencao. */
    private String hrManutencao;
    
    /** Atributo cdOperacaoCanalManutencao. */
    private String cdOperacaoCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
	/**
	 * Consultar detalhe conta favorecido saida dto.
	 */
	public ConsultarDetalheContaFavorecidoSaidaDTO() {
		super();
	}

	/**
	 * Get: cdBloqueioContaFavorecido.
	 *
	 * @return cdBloqueioContaFavorecido
	 */
	public Integer getCdBloqueioContaFavorecido() {
		return cdBloqueioContaFavorecido;
	}

	/**
	 * Set: cdBloqueioContaFavorecido.
	 *
	 * @param cdBloqueioContaFavorecido the cd bloqueio conta favorecido
	 */
	public void setCdBloqueioContaFavorecido(Integer cdBloqueioContaFavorecido) {
		this.cdBloqueioContaFavorecido = cdBloqueioContaFavorecido;
	}

	/**
	 * Get: cdIndicadorRetornoCliente.
	 *
	 * @return cdIndicadorRetornoCliente
	 */
	public Integer getCdIndicadorRetornoCliente() {
		return cdIndicadorRetornoCliente;
	}

	/**
	 * Set: cdIndicadorRetornoCliente.
	 *
	 * @param cdIndicadorRetornoCliente the cd indicador retorno cliente
	 */
	public void setCdIndicadorRetornoCliente(Integer cdIndicadorRetornoCliente) {
		this.cdIndicadorRetornoCliente = cdIndicadorRetornoCliente;
	}

	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}

	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	/**
	 * Get: cdOperacaoCanalManutencao.
	 *
	 * @return cdOperacaoCanalManutencao
	 */
	public String getCdOperacaoCanalManutencao() {
		return cdOperacaoCanalManutencao;
	}

	/**
	 * Set: cdOperacaoCanalManutencao.
	 *
	 * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
	 */
	public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
		this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
	}

	/**
	 * Get: cdSituacaoContaFavorecido.
	 *
	 * @return cdSituacaoContaFavorecido
	 */
	public Integer getCdSituacaoContaFavorecido() {
		return cdSituacaoContaFavorecido;
	}

	/**
	 * Set: cdSituacaoContaFavorecido.
	 *
	 * @param cdSituacaoContaFavorecido the cd situacao conta favorecido
	 */
	public void setCdSituacaoContaFavorecido(Integer cdSituacaoContaFavorecido) {
		this.cdSituacaoContaFavorecido = cdSituacaoContaFavorecido;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}

	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoExterna.
	 *
	 * @return cdUsuarioManutencaoExterna
	 */
	public String getCdUsuarioManutencaoExterna() {
		return cdUsuarioManutencaoExterna;
	}

	/**
	 * Set: cdUsuarioManutencaoExterna.
	 *
	 * @param cdUsuarioManutencaoExterna the cd usuario manutencao externa
	 */
	public void setCdUsuarioManutencaoExterna(String cdUsuarioManutencaoExterna) {
		this.cdUsuarioManutencaoExterna = cdUsuarioManutencaoExterna;
	}

	/**
	 * Get: dsBloqueio.
	 *
	 * @return dsBloqueio
	 */
	public String getDsBloqueio() {
		return dsBloqueio;
	}

	/**
	 * Set: dsBloqueio.
	 *
	 * @param dsBloqueio the ds bloqueio
	 */
	public void setDsBloqueio(String dsBloqueio) {
		this.dsBloqueio = dsBloqueio;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsObservacaoContaFavorecida.
	 *
	 * @return dsObservacaoContaFavorecida
	 */
	public String getDsObservacaoContaFavorecida() {
		return dsObservacaoContaFavorecida;
	}

	/**
	 * Set: dsObservacaoContaFavorecida.
	 *
	 * @param dsObservacaoContaFavorecida the ds observacao conta favorecida
	 */
	public void setDsObservacaoContaFavorecida(String dsObservacaoContaFavorecida) {
		this.dsObservacaoContaFavorecida = dsObservacaoContaFavorecida;
	}

	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: hrBloqueioContaFavorecido.
	 *
	 * @return hrBloqueioContaFavorecido
	 */
	public String getHrBloqueioContaFavorecido() {
		return hrBloqueioContaFavorecido;
	}

	/**
	 * Set: hrBloqueioContaFavorecido.
	 *
	 * @param hrBloqueioContaFavorecido the hr bloqueio conta favorecido
	 */
	public void setHrBloqueioContaFavorecido(String hrBloqueioContaFavorecido) {
		this.hrBloqueioContaFavorecido = hrBloqueioContaFavorecido;
	}

	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: tpConta.
	 *
	 * @return tpConta
	 */
	public String getTpConta() {
		return tpConta;
	}

	/**
	 * Set: tpConta.
	 *
	 * @param tpConta the tp conta
	 */
	public void setTpConta(String tpConta) {
		this.tpConta = tpConta;
	}
	
	/**
	 * Get: cdBloqueioContaFavorecidoStr.
	 *
	 * @return cdBloqueioContaFavorecidoStr
	 */
	public String getCdBloqueioContaFavorecidoStr(){
		return this.cdBloqueioContaFavorecido == null || this.cdBloqueioContaFavorecido.equals(0) ? "" : cdBloqueioContaFavorecido.toString();
	}
	

}
