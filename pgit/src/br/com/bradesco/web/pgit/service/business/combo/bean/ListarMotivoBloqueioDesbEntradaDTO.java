/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarMotivoBloqueioDesbEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMotivoBloqueioDesbEntradaDTO {
	
	/** Atributo cdSituacaoParticipanteContrato. */
	private Integer cdSituacaoParticipanteContrato;

	/**
	 * Get: cdSituacaoParticipanteContrato.
	 *
	 * @return cdSituacaoParticipanteContrato
	 */
	public Integer getCdSituacaoParticipanteContrato() {
		return cdSituacaoParticipanteContrato;
	}

	/**
	 * Set: cdSituacaoParticipanteContrato.
	 *
	 * @param cdSituacaoParticipanteContrato the cd situacao participante contrato
	 */
	public void setCdSituacaoParticipanteContrato(
			Integer cdSituacaoParticipanteContrato) {
		this.cdSituacaoParticipanteContrato = cdSituacaoParticipanteContrato;
	}
	
	
}
