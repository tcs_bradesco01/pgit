/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean;

/**
 * Nome: DetalharPagamentosConsolidadosEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharPagamentosConsolidadosEntradaDTO {
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato = null;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio = null;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio = null;
    
    /** Atributo nrCnpjCpf. */
    private Long nrCnpjCpf = null;
    
    /** Atributo nrFilialcnpjCpf. */
    private Integer nrFilialcnpjCpf = null;
    
    /** Atributo nrControleCnpjCpf. */
    private Integer nrControleCnpjCpf = null;
    
    /** Atributo nrArquivoRemessaPagamento. */
    private Long nrArquivoRemessaPagamento = null;
    
    /** Atributo dtCreditoPagamento. */
    private String dtCreditoPagamento = null;
    
    /** Atributo cdBanco. */
    private Integer cdBanco = null;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia = null;
    
    /** Atributo cdConta. */
    private Long cdConta = null;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta = null;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao = null;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado = null;
    
    /** Atributo cdSituacaoOperacaoPagamento. */
    private Integer cdSituacaoOperacaoPagamento = null;
    
    /** Atributo cdAgendadoPagaoNaoPago. */
    private Integer cdAgendadoPagaoNaoPago = null;
    
    /** Atributo cdPessoaContratoDebito. */
    private Long cdPessoaContratoDebito = null;
	
	/** Atributo cdTipoContratoDebito. */
	private Integer cdTipoContratoDebito = null;
	
	/** Atributo nrSequenciaContratoDebito. */
	private Long nrSequenciaContratoDebito = null;
	
	/** Atributo cdTituloPgtoRastreado. */
	private Integer cdTituloPgtoRastreado = null;
	
	/** Atributo cdIndicadorAutorizacaoPagador. */
	private Integer cdIndicadorAutorizacaoPagador = null;
	
	/** Atributo nrLoteInternoPagamento. */
    private Long nrLoteInternoPagamento = null;
    
    private String cdIspbPagtoDestino;
	
	private String contaPagtoDestino;
	
	/**
	 * Get: cdPessoaContratoDebito.
	 *
	 * @return cdPessoaContratoDebito
	 */
	public Long getCdPessoaContratoDebito() {
		return cdPessoaContratoDebito;
	}
	
	/**
	 * Set: cdPessoaContratoDebito.
	 *
	 * @param cdPessoaContratoDebito the cd pessoa contrato debito
	 */
	public void setCdPessoaContratoDebito(Long cdPessoaContratoDebito) {
		this.cdPessoaContratoDebito = cdPessoaContratoDebito;
	}
	
	/**
	 * Get: cdTipoContratoDebito.
	 *
	 * @return cdTipoContratoDebito
	 */
	public Integer getCdTipoContratoDebito() {
		return cdTipoContratoDebito;
	}
	
	/**
	 * Set: cdTipoContratoDebito.
	 *
	 * @param cdTipoContratoDebito the cd tipo contrato debito
	 */
	public void setCdTipoContratoDebito(Integer cdTipoContratoDebito) {
		this.cdTipoContratoDebito = cdTipoContratoDebito;
	}
	
	/**
	 * Get: nrSequenciaContratoDebito.
	 *
	 * @return nrSequenciaContratoDebito
	 */
	public Long getNrSequenciaContratoDebito() {
		return nrSequenciaContratoDebito;
	}
	
	/**
	 * Set: nrSequenciaContratoDebito.
	 *
	 * @param nrSequenciaContratoDebito the nr sequencia contrato debito
	 */
	public void setNrSequenciaContratoDebito(Long nrSequenciaContratoDebito) {
		this.nrSequenciaContratoDebito = nrSequenciaContratoDebito;
	}
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtCreditoPagamento.
	 *
	 * @return dtCreditoPagamento
	 */
	public String getDtCreditoPagamento() {
		return dtCreditoPagamento;
	}
	
	/**
	 * Set: dtCreditoPagamento.
	 *
	 * @param dtCreditoPagamento the dt credito pagamento
	 */
	public void setDtCreditoPagamento(String dtCreditoPagamento) {
		this.dtCreditoPagamento = dtCreditoPagamento;
	}
	
	/**
	 * Get: nrArquivoRemessaPagamento.
	 *
	 * @return nrArquivoRemessaPagamento
	 */
	public Long getNrArquivoRemessaPagamento() {
		return nrArquivoRemessaPagamento;
	}
	
	/**
	 * Set: nrArquivoRemessaPagamento.
	 *
	 * @param nrArquivoRemessaPagamento the nr arquivo remessa pagamento
	 */
	public void setNrArquivoRemessaPagamento(Long nrArquivoRemessaPagamento) {
		this.nrArquivoRemessaPagamento = nrArquivoRemessaPagamento;
	}
	
	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public Long getNrCnpjCpf() {
		return nrCnpjCpf;
	}
	
	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(Long nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}
	
	/**
	 * Get: nrControleCnpjCpf.
	 *
	 * @return nrControleCnpjCpf
	 */
	public Integer getNrControleCnpjCpf() {
		return nrControleCnpjCpf;
	}
	
	/**
	 * Set: nrControleCnpjCpf.
	 *
	 * @param nrControleCnpjCpf the nr controle cnpj cpf
	 */
	public void setNrControleCnpjCpf(Integer nrControleCnpjCpf) {
		this.nrControleCnpjCpf = nrControleCnpjCpf;
	}
	
	/**
	 * Get: nrFilialcnpjCpf.
	 *
	 * @return nrFilialcnpjCpf
	 */
	public Integer getNrFilialcnpjCpf() {
		return nrFilialcnpjCpf;
	}
	
	/**
	 * Set: nrFilialcnpjCpf.
	 *
	 * @param nrFilialcnpjCpf the nr filialcnpj cpf
	 */
	public void setNrFilialcnpjCpf(Integer nrFilialcnpjCpf) {
		this.nrFilialcnpjCpf = nrFilialcnpjCpf;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdAgendadoPagaoNaoPago.
	 *
	 * @return cdAgendadoPagaoNaoPago
	 */
	public Integer getCdAgendadoPagaoNaoPago() {
		return cdAgendadoPagaoNaoPago;
	}
	
	/**
	 * Set: cdAgendadoPagaoNaoPago.
	 *
	 * @param cdAgendadoPagaoNaoPago the cd agendado pagao nao pago
	 */
	public void setCdAgendadoPagaoNaoPago(Integer cdAgendadoPagaoNaoPago) {
		this.cdAgendadoPagaoNaoPago = cdAgendadoPagaoNaoPago;
	}
	
	/**
	 * Get: cdTituloPgtoRastreado.
	 *
	 * @return cdTituloPgtoRastreado
	 */
	public Integer getCdTituloPgtoRastreado() {
		return cdTituloPgtoRastreado;
	}
	
	/**
	 * Set: cdTituloPgtoRastreado.
	 *
	 * @param cdTituloPgtoRastreado the cd titulo pgto rastreado
	 */
	public void setCdTituloPgtoRastreado(Integer cdTituloPgtoRastreado) {
		this.cdTituloPgtoRastreado = cdTituloPgtoRastreado;
	}
	
	/**
	 * Get: cdIndicadorAutorizacaoPagador.
	 *
	 * @param cdIndicadorAutorizacaoPagador the cd Indicador Autorizacao Pagador
	 */
	public Integer getCdIndicadorAutorizacaoPagador() {
		return cdIndicadorAutorizacaoPagador;
	}

	/**
	 * Set: cdIndicadorAutorizacaoPagador.
	 *
	 * @param cdIndicadorAutorizacaoPagador the cd Indicador Autorizacao Pagador
	 */
	public void setCdIndicadorAutorizacaoPagador(Integer cdIndicadorAutorizacaoPagador) {
		this.cdIndicadorAutorizacaoPagador = cdIndicadorAutorizacaoPagador;
	}

	/**
	 * Get: nrLoteInternoPagamento.
	 *
	 * @param nrLoteInternoPagamento the nr Lote Interno Pagamento
	 */
	public Long getNrLoteInternoPagamento() {
		return nrLoteInternoPagamento;
	}
	
	/**
	 * Set: nrLoteInternoPagamento.
	 *
	 * @param nrLoteInternoPagamento the nr Lote Interno Pagamento
	 */
	public void setNrLoteInternoPagamento(Long nrLoteInternoPagamento) {
		this.nrLoteInternoPagamento = nrLoteInternoPagamento;
	}

	public String getCdIspbPagtoDestino() {
		return cdIspbPagtoDestino;
	}

	public void setCdIspbPagtoDestino(String cdIspbPagtoDestino) {
		this.cdIspbPagtoDestino = cdIspbPagtoDestino;
	}

	public String getContaPagtoDestino() {
		return contaPagtoDestino;
	}

	public void setContaPagtoDestino(String contaPagtoDestino) {
		this.contaPagtoDestino = contaPagtoDestino;
	}
	
}
