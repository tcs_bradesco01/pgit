/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class OcorrenciasVinculo.
 * 
 * @version $Revision$ $Date$
 */
public class OcorrenciasVinculo implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrCpfCnpjConta
     */
    private java.lang.String _nrCpfCnpjConta;

    /**
     * Field _nmRazaoSocialConta
     */
    private java.lang.String _nmRazaoSocialConta;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdTipoConta
     */
    private java.lang.String _cdTipoConta;

    /**
     * Field _dsAcaoManutencaoConta
     */
    private java.lang.String _dsAcaoManutencaoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public OcorrenciasVinculo() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasVinculo()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return String
     * @return the value of field 'cdTipoConta'.
     */
    public java.lang.String getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- java.lang.String getCdTipoConta() 

    /**
     * Returns the value of field 'dsAcaoManutencaoConta'.
     * 
     * @return String
     * @return the value of field 'dsAcaoManutencaoConta'.
     */
    public java.lang.String getDsAcaoManutencaoConta()
    {
        return this._dsAcaoManutencaoConta;
    } //-- java.lang.String getDsAcaoManutencaoConta() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'nmRazaoSocialConta'.
     * 
     * @return String
     * @return the value of field 'nmRazaoSocialConta'.
     */
    public java.lang.String getNmRazaoSocialConta()
    {
        return this._nmRazaoSocialConta;
    } //-- java.lang.String getNmRazaoSocialConta() 

    /**
     * Returns the value of field 'nrCpfCnpjConta'.
     * 
     * @return String
     * @return the value of field 'nrCpfCnpjConta'.
     */
    public java.lang.String getNrCpfCnpjConta()
    {
        return this._nrCpfCnpjConta;
    } //-- java.lang.String getNrCpfCnpjConta() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(java.lang.String cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
    } //-- void setCdTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsAcaoManutencaoConta'.
     * 
     * @param dsAcaoManutencaoConta the value of field
     * 'dsAcaoManutencaoConta'.
     */
    public void setDsAcaoManutencaoConta(java.lang.String dsAcaoManutencaoConta)
    {
        this._dsAcaoManutencaoConta = dsAcaoManutencaoConta;
    } //-- void setDsAcaoManutencaoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'nmRazaoSocialConta'.
     * 
     * @param nmRazaoSocialConta the value of field
     * 'nmRazaoSocialConta'.
     */
    public void setNmRazaoSocialConta(java.lang.String nmRazaoSocialConta)
    {
        this._nmRazaoSocialConta = nmRazaoSocialConta;
    } //-- void setNmRazaoSocialConta(java.lang.String) 

    /**
     * Sets the value of field 'nrCpfCnpjConta'.
     * 
     * @param nrCpfCnpjConta the value of field 'nrCpfCnpjConta'.
     */
    public void setNrCpfCnpjConta(java.lang.String nrCpfCnpjConta)
    {
        this._nrCpfCnpjConta = nrCpfCnpjConta;
    } //-- void setNrCpfCnpjConta(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return OcorrenciasVinculo
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasVinculo unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasVinculo) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasVinculo.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.OcorrenciasVinculo unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
