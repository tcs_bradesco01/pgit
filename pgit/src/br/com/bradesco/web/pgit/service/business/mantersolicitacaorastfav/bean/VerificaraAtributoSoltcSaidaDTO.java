/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean;

/**
 * Nome: VerificaraAtributoSoltcSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class VerificaraAtributoSoltcSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo dsTarifaBloqueio. */
	private String dsTarifaBloqueio;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: dsTarifaBloqueio.
	 *
	 * @return dsTarifaBloqueio
	 */
	public String getDsTarifaBloqueio() {
		return dsTarifaBloqueio;
	}

	/**
	 * Set: dsTarifaBloqueio.
	 *
	 * @param dsTarifaBloqueio the ds tarifa bloqueio
	 */
	public void setDsTarifaBloqueio(String dsTarifaBloqueio) {
		this.dsTarifaBloqueio = dsTarifaBloqueio;
	}

}
