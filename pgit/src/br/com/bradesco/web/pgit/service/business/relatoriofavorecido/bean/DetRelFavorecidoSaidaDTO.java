/*
 * Nome: br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean;

import java.math.BigDecimal;

import br.com.bradesco.web.pgit.utils.PgitUtil;



/**
 * Nome: DetRelFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetRelFavorecidoSaidaDTO {
	
	/** Atributo cdSituacaoSolicitacaoPagamento. */
	private Integer cdSituacaoSolicitacaoPagamento;
	
	/** Atributo dsSolicitacaoPagamento. */
	private String dsSolicitacaoPagamento;
    
    /** Atributo cdMotivoSituacaoSolicitacao. */
    private Integer cdMotivoSituacaoSolicitacao;
    
    /** Atributo dsMotivoSolicitacao. */
    private String dsMotivoSolicitacao;
    
    /** Atributo dtSolicitacao. */
    private String dtSolicitacao;
    
    /** Atributo hrSolicitacao. */
    private String hrSolicitacao;
    
    /** Atributo dtAtendimento. */
    private String dtAtendimento;
    
    /** Atributo hrAtendimento. */
    private String hrAtendimento;
    
    /** Atributo nrTarifaBonificacao. */
    private BigDecimal nrTarifaBonificacao;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo dsProdutoServicoOperacao. */
    private String dsProdutoServicoOperacao;
    
    /** Atributo cdProdutoOperRelacionado. */
    private Integer cdProdutoOperRelacionado;
    
    /** Atributo dsProdutoOperRelacionado. */
    private String dsProdutoOperRelacionado;
    
    /** Atributo cdRelacionamentoProduto. */
    private Integer cdRelacionamentoProduto;
    
    /** Atributo cdPessoaJuridicaDir. */
    private Long cdPessoaJuridicaDir;
    
    /** Atributo nrSeqUnidadeOrganizacionalDir. */
    private Integer nrSeqUnidadeOrganizacionalDir;
    
    /** Atributo dsUnidadeOrganizacionalDir. */
    private String dsUnidadeOrganizacionalDir;
    
    /** Atributo cdPessoaJuridicaGerc. */
    private Long cdPessoaJuridicaGerc;
    
    /** Atributo nrUnidadeOrganizacionalGerc. */
    private Integer nrUnidadeOrganizacionalGerc;
    
    /** Atributo dsOrganizacionalGerc. */
    private String dsOrganizacionalGerc;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;
    
    /** Atributo nrUnidadeOrganizacional. */
    private Integer nrUnidadeOrganizacional;
    
    /** Atributo dsOrganizacaoOperacional. */
    private String dsOrganizacaoOperacional;
    
    /** Atributo cdTipoRelatorio. */
    private Integer cdTipoRelatorio;
    
    /** Atributo dsTipoRelatorio. */
    private String dsTipoRelatorio;
    
    /** Atributo cdSegmentoCliente. */
    private Integer cdSegmentoCliente;
    
    /** Atributo dsSegmentoCliente. */
    private String dsSegmentoCliente;
    
    /** Atributo cdGrupoEconomico. */
    private Long cdGrupoEconomico;
    
    /** Atributo dsGrupoEconomico. */
    private String dsGrupoEconomico;
    
    /** Atributo cdClassAtividade. */
    private String cdClassAtividade;
    
    /** Atributo dsClassAtividade. */
    private String dsClassAtividade;
    
    /** Atributo cdRamoAtividadeEconomica. */
    private Integer cdRamoAtividadeEconomica;
    
    /** Atributo dsRamoAtividadeEconomica. */
    private String dsRamoAtividadeEconomica;
    
    /** Atributo cdRamoAtividade. */
    private Integer cdRamoAtividade;
    
    /** Atributo dsRamoAtividade. */
    private String dsRamoAtividade;
    
    /** Atributo cdAtividadeEconomica. */
    private Integer cdAtividadeEconomica;
    
    /** Atributo dsAtividadeEconomica. */
    private String dsAtividadeEconomica;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExter. */
    private String cdUsuarioInclusaoExter;
    
    /** Atributo dtInclusao. */
    private String dtInclusao;
    
    /** Atributo hrInclusao. */
    private String hrInclusao;
    
    /** Atributo cdOperCanalInclusao. */
    private String cdOperCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoExter. */
    private String cdUsuarioManutencaoExter;
    
    /** Atributo dtManutencao. */
    private String dtManutencao;
    
    /** Atributo hrManutencao. */
    private String hrManutencao;
    
    /** Atributo cdOperCanalManutencao. */
    private String cdOperCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
    /** Atributo dsClasseRamo. */
    private String dsClasseRamo;
    
    /** Atributo dsSubRamoAtividade. */
    private String dsSubRamoAtividade;
    
    /** Atributo dsEmpresa. */
    private String dsEmpresa;

    /**
     * Det rel favorecido saida dto.
     */
    public DetRelFavorecidoSaidaDTO() {
		super();
	}
    
    

   


	/**
	 * Get: cdAtividadeEconomica.
	 *
	 * @return cdAtividadeEconomica
	 */
	public Integer getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}

	/**
	 * Set: cdAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd atividade economica
	 */
	public void setCdAtividadeEconomica(Integer cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}

	/**
	 * Get: cdClassAtividade.
	 *
	 * @return cdClassAtividade
	 */
	public String getCdClassAtividade() {
		return cdClassAtividade;
	}

	/**
	 * Set: cdClassAtividade.
	 *
	 * @param cdClassAtividade the cd class atividade
	 */
	public void setCdClassAtividade(String cdClassAtividade) {
		this.cdClassAtividade = cdClassAtividade;
	}

	/**
	 * Get: cdGrupoEconomico.
	 *
	 * @return cdGrupoEconomico
	 */
	public Long getCdGrupoEconomico() {
		return cdGrupoEconomico;
	}

	/**
	 * Set: cdGrupoEconomico.
	 *
	 * @param cdGrupoEconomico the cd grupo economico
	 */
	public void setCdGrupoEconomico(Long cdGrupoEconomico) {
		this.cdGrupoEconomico = cdGrupoEconomico;
	}

	/**
	 * Get: cdMotivoSituacaoSolicitacao.
	 *
	 * @return cdMotivoSituacaoSolicitacao
	 */
	public Integer getCdMotivoSituacaoSolicitacao() {
		return cdMotivoSituacaoSolicitacao;
	}

	/**
	 * Set: cdMotivoSituacaoSolicitacao.
	 *
	 * @param cdMotivoSituacaoSolicitacao the cd motivo situacao solicitacao
	 */
	public void setCdMotivoSituacaoSolicitacao(Integer cdMotivoSituacaoSolicitacao) {
		this.cdMotivoSituacaoSolicitacao = cdMotivoSituacaoSolicitacao;
	}

	/**
	 * Get: cdOperCanalInclusao.
	 *
	 * @return cdOperCanalInclusao
	 */
	public String getCdOperCanalInclusao() {
		return cdOperCanalInclusao;
	}

	/**
	 * Set: cdOperCanalInclusao.
	 *
	 * @param cdOperCanalInclusao the cd oper canal inclusao
	 */
	public void setCdOperCanalInclusao(String cdOperCanalInclusao) {
		this.cdOperCanalInclusao = cdOperCanalInclusao;
	}

	/**
	 * Get: cdOperCanalManutencao.
	 *
	 * @return cdOperCanalManutencao
	 */
	public String getCdOperCanalManutencao() {
		return cdOperCanalManutencao;
	}

	/**
	 * Set: cdOperCanalManutencao.
	 *
	 * @param cdOperCanalManutencao the cd oper canal manutencao
	 */
	public void setCdOperCanalManutencao(String cdOperCanalManutencao) {
		this.cdOperCanalManutencao = cdOperCanalManutencao;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdPessoaJuridicaDir.
	 *
	 * @return cdPessoaJuridicaDir
	 */
	public Long getCdPessoaJuridicaDir() {
		return cdPessoaJuridicaDir;
	}

	/**
	 * Set: cdPessoaJuridicaDir.
	 *
	 * @param cdPessoaJuridicaDir the cd pessoa juridica dir
	 */
	public void setCdPessoaJuridicaDir(Long cdPessoaJuridicaDir) {
		this.cdPessoaJuridicaDir = cdPessoaJuridicaDir;
	}

	/**
	 * Get: cdPessoaJuridicaGerc.
	 *
	 * @return cdPessoaJuridicaGerc
	 */
	public Long getCdPessoaJuridicaGerc() {
		return cdPessoaJuridicaGerc;
	}

	/**
	 * Set: cdPessoaJuridicaGerc.
	 *
	 * @param cdPessoaJuridicaGerc the cd pessoa juridica gerc
	 */
	public void setCdPessoaJuridicaGerc(Long cdPessoaJuridicaGerc) {
		this.cdPessoaJuridicaGerc = cdPessoaJuridicaGerc;
	}

	/**
	 * Get: cdProdutoOperRelacionado.
	 *
	 * @return cdProdutoOperRelacionado
	 */
	public Integer getCdProdutoOperRelacionado() {
		return cdProdutoOperRelacionado;
	}

	/**
	 * Set: cdProdutoOperRelacionado.
	 *
	 * @param cdProdutoOperRelacionado the cd produto oper relacionado
	 */
	public void setCdProdutoOperRelacionado(Integer cdProdutoOperRelacionado) {
		this.cdProdutoOperRelacionado = cdProdutoOperRelacionado;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdRamoAtividade.
	 *
	 * @return cdRamoAtividade
	 */
	public Integer getCdRamoAtividade() {
		return cdRamoAtividade;
	}

	/**
	 * Set: cdRamoAtividade.
	 *
	 * @param cdRamoAtividade the cd ramo atividade
	 */
	public void setCdRamoAtividade(Integer cdRamoAtividade) {
		this.cdRamoAtividade = cdRamoAtividade;
	}

	/**
	 * Get: cdRamoAtividadeEconomica.
	 *
	 * @return cdRamoAtividadeEconomica
	 */
	public Integer getCdRamoAtividadeEconomica() {
		return cdRamoAtividadeEconomica;
	}

	/**
	 * Set: cdRamoAtividadeEconomica.
	 *
	 * @param cdRamoAtividadeEconomica the cd ramo atividade economica
	 */
	public void setCdRamoAtividadeEconomica(Integer cdRamoAtividadeEconomica) {
		this.cdRamoAtividadeEconomica = cdRamoAtividadeEconomica;
	}

	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}

	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}

	/**
	 * Get: cdSegmentoCliente.
	 *
	 * @return cdSegmentoCliente
	 */
	public Integer getCdSegmentoCliente() {
		return cdSegmentoCliente;
	}

	/**
	 * Set: cdSegmentoCliente.
	 *
	 * @param cdSegmentoCliente the cd segmento cliente
	 */
	public void setCdSegmentoCliente(Integer cdSegmentoCliente) {
		this.cdSegmentoCliente = cdSegmentoCliente;
	}

	/**
	 * Get: cdSituacaoSolicitacaoPagamento.
	 *
	 * @return cdSituacaoSolicitacaoPagamento
	 */
	public Integer getCdSituacaoSolicitacaoPagamento() {
		return cdSituacaoSolicitacaoPagamento;
	}

	/**
	 * Set: cdSituacaoSolicitacaoPagamento.
	 *
	 * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
	 */
	public void setCdSituacaoSolicitacaoPagamento(
			Integer cdSituacaoSolicitacaoPagamento) {
		this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdTipoRelatorio.
	 *
	 * @return cdTipoRelatorio
	 */
	public Integer getCdTipoRelatorio() {
		return cdTipoRelatorio;
	}

	/**
	 * Set: cdTipoRelatorio.
	 *
	 * @param cdTipoRelatorio the cd tipo relatorio
	 */
	public void setCdTipoRelatorio(Integer cdTipoRelatorio) {
		this.cdTipoRelatorio = cdTipoRelatorio;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cdUsuarioInclusaoExter.
	 *
	 * @return cdUsuarioInclusaoExter
	 */
	public String getCdUsuarioInclusaoExter() {
		return cdUsuarioInclusaoExter;
	}

	/**
	 * Set: cdUsuarioInclusaoExter.
	 *
	 * @param cdUsuarioInclusaoExter the cd usuario inclusao exter
	 */
	public void setCdUsuarioInclusaoExter(String cdUsuarioInclusaoExter) {
		this.cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: cdUsuarioManutencaoExter.
	 *
	 * @return cdUsuarioManutencaoExter
	 */
	public String getCdUsuarioManutencaoExter() {
		return cdUsuarioManutencaoExter;
	}

	/**
	 * Set: cdUsuarioManutencaoExter.
	 *
	 * @param cdUsuarioManutencaoExter the cd usuario manutencao exter
	 */
	public void setCdUsuarioManutencaoExter(String cdUsuarioManutencaoExter) {
		this.cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
	}

	/**
	 * Get: dsAtividadeEconomica.
	 *
	 * @return dsAtividadeEconomica
	 */
	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}

	/**
	 * Set: dsAtividadeEconomica.
	 *
	 * @param dsAtividadeEconomica the ds atividade economica
	 */
	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsClassAtividade.
	 *
	 * @return dsClassAtividade
	 */
	public String getDsClassAtividade() {
		return dsClassAtividade;
	}

	/**
	 * Set: dsClassAtividade.
	 *
	 * @param dsClassAtividade the ds class atividade
	 */
	public void setDsClassAtividade(String dsClassAtividade) {
		this.dsClassAtividade = dsClassAtividade;
	}

	/**
	 * Get: dsGrupoEconomico.
	 *
	 * @return dsGrupoEconomico
	 */
	public String getDsGrupoEconomico() {
		return dsGrupoEconomico;
	}

	/**
	 * Set: dsGrupoEconomico.
	 *
	 * @param dsGrupoEconomico the ds grupo economico
	 */
	public void setDsGrupoEconomico(String dsGrupoEconomico) {
		this.dsGrupoEconomico = dsGrupoEconomico;
	}

	/**
	 * Get: dsMotivoSolicitacao.
	 *
	 * @return dsMotivoSolicitacao
	 */
	public String getDsMotivoSolicitacao() {
		return dsMotivoSolicitacao;
	}

	/**
	 * Set: dsMotivoSolicitacao.
	 *
	 * @param dsMotivoSolicitacao the ds motivo solicitacao
	 */
	public void setDsMotivoSolicitacao(String dsMotivoSolicitacao) {
		this.dsMotivoSolicitacao = dsMotivoSolicitacao;
	}

	/**
	 * Get: dsOrganizacaoOperacional.
	 *
	 * @return dsOrganizacaoOperacional
	 */
	public String getDsOrganizacaoOperacional() {
		return dsOrganizacaoOperacional;
	}

	/**
	 * Set: dsOrganizacaoOperacional.
	 *
	 * @param dsOrganizacaoOperacional the ds organizacao operacional
	 */
	public void setDsOrganizacaoOperacional(String dsOrganizacaoOperacional) {
		this.dsOrganizacaoOperacional = dsOrganizacaoOperacional;
	}

	/**
	 * Get: dsOrganizacionalGerc.
	 *
	 * @return dsOrganizacionalGerc
	 */
	public String getDsOrganizacionalGerc() {
		return dsOrganizacionalGerc;
	}

	/**
	 * Set: dsOrganizacionalGerc.
	 *
	 * @param dsOrganizacionalGerc the ds organizacional gerc
	 */
	public void setDsOrganizacionalGerc(String dsOrganizacionalGerc) {
		this.dsOrganizacionalGerc = dsOrganizacionalGerc;
	}

	/**
	 * Get: dsProdutoOperRelacionado.
	 *
	 * @return dsProdutoOperRelacionado
	 */
	public String getDsProdutoOperRelacionado() {
		return dsProdutoOperRelacionado;
	}

	/**
	 * Set: dsProdutoOperRelacionado.
	 *
	 * @param dsProdutoOperRelacionado the ds produto oper relacionado
	 */
	public void setDsProdutoOperRelacionado(String dsProdutoOperRelacionado) {
		this.dsProdutoOperRelacionado = dsProdutoOperRelacionado;
	}

	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}

	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}

	/**
	 * Get: dsRamoAtividade.
	 *
	 * @return dsRamoAtividade
	 */
	public String getDsRamoAtividade() {
		return dsRamoAtividade;
	}

	/**
	 * Set: dsRamoAtividade.
	 *
	 * @param dsRamoAtividade the ds ramo atividade
	 */
	public void setDsRamoAtividade(String dsRamoAtividade) {
		this.dsRamoAtividade = dsRamoAtividade;
	}

	/**
	 * Get: dsRamoAtividadeEconomica.
	 *
	 * @return dsRamoAtividadeEconomica
	 */
	public String getDsRamoAtividadeEconomica() {
		return dsRamoAtividadeEconomica;
	}

	/**
	 * Set: dsRamoAtividadeEconomica.
	 *
	 * @param dsRamoAtividadeEconomica the ds ramo atividade economica
	 */
	public void setDsRamoAtividadeEconomica(String dsRamoAtividadeEconomica) {
		this.dsRamoAtividadeEconomica = dsRamoAtividadeEconomica;
	}

	/**
	 * Get: dsSegmentoCliente.
	 *
	 * @return dsSegmentoCliente
	 */
	public String getDsSegmentoCliente() {
		return dsSegmentoCliente;
	}

	/**
	 * Set: dsSegmentoCliente.
	 *
	 * @param dsSegmentoCliente the ds segmento cliente
	 */
	public void setDsSegmentoCliente(String dsSegmentoCliente) {
		this.dsSegmentoCliente = dsSegmentoCliente;
	}

	/**
	 * Get: dsSolicitacaoPagamento.
	 *
	 * @return dsSolicitacaoPagamento
	 */
	public String getDsSolicitacaoPagamento() {
		return dsSolicitacaoPagamento;
	}

	/**
	 * Set: dsSolicitacaoPagamento.
	 *
	 * @param dsSolicitacaoPagamento the ds solicitacao pagamento
	 */
	public void setDsSolicitacaoPagamento(String dsSolicitacaoPagamento) {
		this.dsSolicitacaoPagamento = dsSolicitacaoPagamento;
	}

	/**
	 * Get: dsTipoRelatorio.
	 *
	 * @return dsTipoRelatorio
	 */
	public String getDsTipoRelatorio() {
		return dsTipoRelatorio;
	}

	/**
	 * Set: dsTipoRelatorio.
	 *
	 * @param dsTipoRelatorio the ds tipo relatorio
	 */
	public void setDsTipoRelatorio(String dsTipoRelatorio) {
		this.dsTipoRelatorio = dsTipoRelatorio;
	}

	/**
	 * Get: dsUnidadeOrganizacionalDir.
	 *
	 * @return dsUnidadeOrganizacionalDir
	 */
	public String getDsUnidadeOrganizacionalDir() {
		return dsUnidadeOrganizacionalDir;
	}

	/**
	 * Set: dsUnidadeOrganizacionalDir.
	 *
	 * @param dsUnidadeOrganizacionalDir the ds unidade organizacional dir
	 */
	public void setDsUnidadeOrganizacionalDir(String dsUnidadeOrganizacionalDir) {
		this.dsUnidadeOrganizacionalDir = dsUnidadeOrganizacionalDir;
	}

	/**
	 * Get: dtAtendimento.
	 *
	 * @return dtAtendimento
	 */
	public String getDtAtendimento() {
		return dtAtendimento;
	}

	/**
	 * Set: dtAtendimento.
	 *
	 * @param dtAtendimento the dt atendimento
	 */
	public void setDtAtendimento(String dtAtendimento) {
		this.dtAtendimento = dtAtendimento;
	}

	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: dtSolicitacao.
	 *
	 * @return dtSolicitacao
	 */
	public String getDtSolicitacao() {
		return dtSolicitacao;
	}

	/**
	 * Set: dtSolicitacao.
	 *
	 * @param dtSolicitacao the dt solicitacao
	 */
	public void setDtSolicitacao(String dtSolicitacao) {
		this.dtSolicitacao = dtSolicitacao;
	}

	/**
	 * Get: hrAtendimento.
	 *
	 * @return hrAtendimento
	 */
	public String getHrAtendimento() {
		return hrAtendimento;
	}

	/**
	 * Set: hrAtendimento.
	 *
	 * @param hrAtendimento the hr atendimento
	 */
	public void setHrAtendimento(String hrAtendimento) {
		this.hrAtendimento = hrAtendimento;
	}

	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: hrSolicitacao.
	 *
	 * @return hrSolicitacao
	 */
	public String getHrSolicitacao() {
		return hrSolicitacao;
	}

	/**
	 * Set: hrSolicitacao.
	 *
	 * @param hrSolicitacao the hr solicitacao
	 */
	public void setHrSolicitacao(String hrSolicitacao) {
		this.hrSolicitacao = hrSolicitacao;
	}

	/**
	 * Get: nrSeqUnidadeOrganizacionalDir.
	 *
	 * @return nrSeqUnidadeOrganizacionalDir
	 */
	public Integer getNrSeqUnidadeOrganizacionalDir() {
		return nrSeqUnidadeOrganizacionalDir;
	}

	/**
	 * Set: nrSeqUnidadeOrganizacionalDir.
	 *
	 * @param nrSeqUnidadeOrganizacionalDir the nr seq unidade organizacional dir
	 */
	public void setNrSeqUnidadeOrganizacionalDir(
			Integer nrSeqUnidadeOrganizacionalDir) {
		this.nrSeqUnidadeOrganizacionalDir = nrSeqUnidadeOrganizacionalDir;
	}

	/**
	 * Get: nrTarifaBonificacao.
	 *
	 * @return nrTarifaBonificacao
	 */
	public BigDecimal getNrTarifaBonificacao() {
		return nrTarifaBonificacao;
	}

	/**
	 * Set: nrTarifaBonificacao.
	 *
	 * @param nrTarifaBonificacao the nr tarifa bonificacao
	 */
	public void setNrTarifaBonificacao(BigDecimal nrTarifaBonificacao) {
		this.nrTarifaBonificacao = nrTarifaBonificacao;
	}


	/**
	 * Get: nrUnidadeOrganizacional.
	 *
	 * @return nrUnidadeOrganizacional
	 */
	public Integer getNrUnidadeOrganizacional() {
		return nrUnidadeOrganizacional;
	}

	/**
	 * Set: nrUnidadeOrganizacional.
	 *
	 * @param nrUnidadeOrganizacional the nr unidade organizacional
	 */
	public void setNrUnidadeOrganizacional(Integer nrUnidadeOrganizacional) {
		this.nrUnidadeOrganizacional = nrUnidadeOrganizacional;
	}

	/**
	 * Get: nrUnidadeOrganizacionalGerc.
	 *
	 * @return nrUnidadeOrganizacionalGerc
	 */
	public Integer getNrUnidadeOrganizacionalGerc() {
		return nrUnidadeOrganizacionalGerc;
	}

	/**
	 * Set: nrUnidadeOrganizacionalGerc.
	 *
	 * @param nrUnidadeOrganizacionalGerc the nr unidade organizacional gerc
	 */
	public void setNrUnidadeOrganizacionalGerc(Integer nrUnidadeOrganizacionalGerc) {
		this.nrUnidadeOrganizacionalGerc = nrUnidadeOrganizacionalGerc;
	}
    
	/**
	 * Get: nrSeqUnidadeOrganizacionalDirStr.
	 *
	 * @return nrSeqUnidadeOrganizacionalDirStr
	 */
	public String getNrSeqUnidadeOrganizacionalDirStr(){
		
		return this.nrSeqUnidadeOrganizacionalDir == null || this.nrSeqUnidadeOrganizacionalDir.equals(0) ? "" : nrSeqUnidadeOrganizacionalDir.toString();   
	}
	
	/**
	 * Get: nrUnidadeOrganizacionalGercStr.
	 *
	 * @return nrUnidadeOrganizacionalGercStr
	 */
	public String getNrUnidadeOrganizacionalGercStr(){
		return this.nrUnidadeOrganizacionalGerc == null || this.nrUnidadeOrganizacionalGerc.equals(0) ? "" : nrUnidadeOrganizacionalGerc.toString();
	}
	
	/**
	 * Get: nrUnidadeOrganizacionalStr.
	 *
	 * @return nrUnidadeOrganizacionalStr
	 */
	public String getNrUnidadeOrganizacionalStr(){
		return this.nrUnidadeOrganizacional == null || this.nrUnidadeOrganizacional.equals(0) ? "" : nrUnidadeOrganizacional.toString();
	}
	
	/**
	 * Get: cdSegmentoClienteStr.
	 *
	 * @return cdSegmentoClienteStr
	 */
	public String getCdSegmentoClienteStr(){
		return this.cdSegmentoCliente == null || this.cdSegmentoCliente.equals(0) ? "" : cdSegmentoCliente.toString();
	}
	
	/**
	 * Get: cdGrupoEconomicoStr.
	 *
	 * @return cdGrupoEconomicoStr
	 */
	public String getCdGrupoEconomicoStr(){
		return this.cdGrupoEconomico == null || this.cdGrupoEconomico.equals(0L) ? "" : cdGrupoEconomico.toString();
	}
	
	/**
	 * Get: cdClassAtividadeStr.
	 *
	 * @return cdClassAtividadeStr
	 */
	public String getCdClassAtividadeStr(){
		return this.cdClassAtividade == null || this.cdClassAtividade.equals(0) ? "" : cdClassAtividade;
	}
	
	/**
	 * Get: cdRamoAtividadeEconomicaStr.
	 *
	 * @return cdRamoAtividadeEconomicaStr
	 */
	public String getCdRamoAtividadeEconomicaStr(){
		return this.cdRamoAtividadeEconomica == null || this.cdRamoAtividadeEconomica.equals(0) ? "" : cdRamoAtividadeEconomica.toString();
	}
	
	/**
	 * Get: cdRamoAtividadeStr.
	 *
	 * @return cdRamoAtividadeStr
	 */
	public String getCdRamoAtividadeStr(){
		return this.cdRamoAtividade == null || this.cdRamoAtividade.equals(0) ? "" : cdRamoAtividade.toString();
	}
	
	/**
	 * Get: cdAtividadeEconomicaStr.
	 *
	 * @return cdAtividadeEconomicaStr
	 */
	public String getCdAtividadeEconomicaStr(){
		return this.cdAtividadeEconomica == null || this.cdAtividadeEconomica.equals(0) ? "" : cdAtividadeEconomica.toString();
	}
	
	/**
	 * Get: cdTipoCanalManutencaoStr.
	 *
	 * @return cdTipoCanalManutencaoStr
	 */
	public String getCdTipoCanalManutencaoStr(){
		return this.cdTipoCanalManutencao == null || this.cdTipoCanalManutencao.equals(0) ? "" : cdTipoCanalManutencao.toString();
	}






	/**
	 * Get: dsClasseRamo.
	 *
	 * @return dsClasseRamo
	 */
	public String getDsClasseRamo() {
	    return dsClasseRamo;
	}






	/**
	 * Set: dsClasseRamo.
	 *
	 * @param dsClasseRamo the ds classe ramo
	 */
	public void setDsClasseRamo(String dsClasseRamo) {
	    this.dsClasseRamo = dsClasseRamo;
	}






	/**
	 * Get: dsSubRamoAtividade.
	 *
	 * @return dsSubRamoAtividade
	 */
	public String getDsSubRamoAtividade() {
	    return dsSubRamoAtividade;
	}






	/**
	 * Set: dsSubRamoAtividade.
	 *
	 * @param dsSubRamoAtividade the ds sub ramo atividade
	 */
	public void setDsSubRamoAtividade(String dsSubRamoAtividade) {
	    this.dsSubRamoAtividade = dsSubRamoAtividade;
	}






	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}






	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}
	
	/**
	 * Get: empresaConglomerado.
	 *
	 * @return empresaConglomerado
	 */
	public String getEmpresaConglomerado(){
		
		return PgitUtil.concatenarCampos(getCdPessoaJuridica(), getDsEmpresa(), " ");
		
	}
	
	
	
}
