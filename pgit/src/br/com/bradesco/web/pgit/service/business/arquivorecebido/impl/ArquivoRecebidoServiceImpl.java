/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.arquivorecebido.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.IArquivoRecebidoService;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ArquivoRecebidoDTO;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ArquivoRecebidoInconsistentesDTO;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ComboProdutoDTO;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.EstatisticaLoteDTO;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.LoteDTO;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.OcorrenciaDTO;
import br.com.bradesco.web.pgit.service.business.cmpi0000.bean.ListarClientesDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.IConsultarEstatisticasLotePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.request.ConsultarEstatisticasLoteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.ConsultarEstatisticasLoteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.IConsultarInconsistenciaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.request.ConsultarInconsistenciasRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.ConsultarInconsistenciasResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlote.IConsultarLotePDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlote.request.ConsultarLoteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlote.response.ConsultarLoteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarocorrencias.IConsultarOcorrenciasPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarocorrencias.request.ConsultarOcorrenciasRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarocorrencias.response.ConsultarOcorrenciasResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientes.IListarClientesPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientes.request.ListarClientesRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarclientes.response.ListarClientesResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarremessa.IListarRemessaPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.listarremessa.request.ListarRemessaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarremessa.response.ListarRemessaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.IMontarComboProdutoPDCAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.request.MontarComboProdutoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.montarcomboproduto.response.MontarComboProdutoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ArquivoRemessaRecebido
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ArquivoRecebidoServiceImpl implements IArquivoRecebidoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Construtor.
	 */
	public ArquivoRecebidoServiceImpl() {
		// TODO: Implementa��o
	}

	/**
	 * M�todo de exemplo.
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.arquivorecebido.IArquivoRemessaRecebido#sampleArquivoRemessaRecebido()
	 */
	public void sampleArquivoRemessaRecebido() {
		// TODO: Implementa�ao
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivorecebido.IArquivoRecebidoService#listaGrid(br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ArquivoRecebidoDTO)
	 */
	public List<ArquivoRecebidoDTO> listaGrid(ArquivoRecebidoDTO pArquivoRemessaRecebidoDTO) throws PdcAdapterException {

		List<ArquivoRecebidoDTO> lArquivos = new ArrayList<ArquivoRecebidoDTO>();
		ListarRemessaRequest lRequest = new ListarRemessaRequest();
		ListarRemessaResponse lResponse = null;

		lRequest.setNumeroOcorrencias(50);

		// HORA RECEPCAO INICIO
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRecebidoDTO.getDataRecepcaoDe()))) {
			lRequest.setHoraRecepInicio(pArquivoRemessaRecebidoDTO.getDataRecepcaoDe());
		} else {
			lRequest.setHoraRecepInicio("");
		}

		// HORA RECEPCAO FIM
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRecebidoDTO.getDataRecepcaoAte()))) {
			lRequest.setHoraRecepFim(pArquivoRemessaRecebidoDTO.getDataRecepcaoAte());
		} else {
			lRequest.setHoraRecepFim("");
		}

		// CENTRO DE CUSTO
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRecebidoDTO.getProduto()))) {
			lRequest.setCentroCusto(pArquivoRemessaRecebidoDTO.getProduto());
		} else {
			lRequest.setCentroCusto("");
		}

		// CNPJ
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRecebidoDTO.getCpf_cliente()))) {
			lRequest.setCpfCnpj(new Long(pArquivoRemessaRecebidoDTO.getCpf_cliente()));
		} else {
			lRequest.setCpfCnpj(0);
		}

		// FILIAL
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRecebidoDTO.getCpf_filial()))) {
			lRequest.setFilial(Integer.parseInt(pArquivoRemessaRecebidoDTO.getCpf_filial()));
		} else {
			lRequest.setFilial(0);
		}

		// CONTROLE
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRecebidoDTO.getCpf_controle()))) {
			lRequest.setControle(Integer.parseInt(pArquivoRemessaRecebidoDTO.getCpf_controle()));
		} else {
			lRequest.setControle(0);
		}

		// PERFIL COMUNICACAO
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRecebidoDTO.getPerfil()))) {
			lRequest.setPerfilComunicacao(new Long(pArquivoRemessaRecebidoDTO.getPerfil()));
		} else {
			lRequest.setPerfilComunicacao(0);
		}

		// NUMERO ARQUIVO REMESSA
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRecebidoDTO.getNumeroArqRemessa()))) {
			lRequest.setNumeroArquivoRemessa(Integer.parseInt(pArquivoRemessaRecebidoDTO.getNumeroArqRemessa()));
		} else {
			lRequest.setNumeroArquivoRemessa(0);
		}

		// SITUACAO PROCESSAMENTO
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRecebidoDTO.getProcessamento()))) {
			lRequest.setSituacaoProcessamento(Integer.parseInt(pArquivoRemessaRecebidoDTO.getProcessamento()));
		} else {
			lRequest.setSituacaoProcessamento(0);
		}

		// RESULTADO PROCESSAMENTO REMESSA
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRecebidoDTO.getResultadoProcessamento()))) {
			lRequest.setProcessamentoRemessa(Integer.parseInt(pArquivoRemessaRecebidoDTO.getResultadoProcessamento()));
		} else {
			lRequest.setProcessamentoRemessa(0);
		}

		// Fazemos a chamada do processo.
		IListarRemessaPDCAdapter listarRemessaPDCAdapter = factoryAdapter.getListarRemessaPDCAdapter();
		lResponse = listarRemessaPDCAdapter.invokeProcess(lRequest);

		ArquivoRecebidoDTO lArquivo;

		
		
		// Para cada item retornado do processo, adicionamos a lista de
		// retorno.
		for (int i = 0; i < lResponse.getNumeroLinhas(); i++) {

			lArquivo = new ArquivoRecebidoDTO();
			String cpfCliente = String.valueOf(lResponse.getOcorrencias(i).getCpfCnpj());
			String cpfFilial = String.valueOf(lResponse.getOcorrencias(i).getFilial());
			String cpfControle = String.valueOf(lResponse.getOcorrencias(i).getControle());

			String cpf = "";

			if (lResponse.getOcorrencias(i).getFilial() == 0) {
				cpf = PgitUtil.formatarCPFComMascara(cpfCliente, cpfControle);
			} else {
				cpf = PgitUtil.formatarCNPJComMascara(cpfCliente, cpfFilial, cpfControle);
			}

			lArquivo.setCpf_completo(cpf);
			lArquivo.setCpf_cliente(cpfCliente);
			lArquivo.setCpf_filial(cpfFilial);
			lArquivo.setCpf_controle(cpfControle);

			lArquivo.setNome(lResponse.getOcorrencias(i).getNomeRazaoSocial());
			lArquivo.setProduto(lResponse.getOcorrencias(i).getDescricaoProdutoPagamento());
			lArquivo.setCodSistemaLegado(lResponse.getOcorrencias(i).getCodigoSistema());
			lArquivo.setPerfil(String.valueOf(lResponse.getOcorrencias(i).getPerfilComunicacao()));
			lArquivo.setNumeroArqRemessa(String.valueOf(lResponse.getOcorrencias(i).getNumeroArquivoRemessa()));

			if (lResponse.getOcorrencias(i).getHoraInclusaoRemessa() != null) {
				lArquivo.setDataRecepcaoInclusao(lResponse.getOcorrencias(i).getHoraInclusaoRemessa());
			} else {
				lArquivo.setDataRecepcaoInclusao("");
			}

			if (lResponse.getOcorrencias(i).getHoraRecepcaoRemessa() != null) {

				try {
					lArquivo.setDataRecepcao(SiteUtil.changeStringDateFormat(lResponse.getOcorrencias(i).getHoraRecepcaoRemessa(), "yyyy-MM-dd-HH.mm.ss", "dd/MM/yyyy HH:mm:ss"));
				} catch (ParseException p) {
					lArquivo.setDataRecepcao("");
				}

			} else {
				lArquivo.setDataRecepcao("");
			}

			lArquivo.setProcessamento(String.valueOf(lResponse.getOcorrencias(i).getSituacaoProcessamento()));
			lArquivo.setSituacaoProcessamento(lResponse.getOcorrencias(i).getDescricaoProcesssamento()); // Situa��o

			if (lResponse.getOcorrencias(i).getCodigoProcessamentoRemessa() == 0) {
				lArquivo.setCodigoResultadoProcessamento("");
				lArquivo.setResultadoProcessamento(""); // Resultado
			} else {
				lArquivo.setCodigoResultadoProcessamento(String.valueOf(lResponse.getOcorrencias(i).getCodigoProcessamentoRemessa()));
				lArquivo.setResultadoProcessamento(lResponse.getOcorrencias(i).getDescricaoProcessamentoRemessa()); // Resultado
			}

			if (((lResponse.getOcorrencias(i).getSituacaoProcessamento() == 1) || (lResponse.getOcorrencias(i).getSituacaoProcessamento() == 2))) {

				lArquivo.setHabilitaBtnLote("F");
				lArquivo.setHabilitaBtnOcorrencia("F");
			} else if ((lResponse.getOcorrencias(i).getSituacaoProcessamento() == 3) && ((lResponse.getOcorrencias(i).getCodigoProcessamentoRemessa() == 2) || (lResponse.getOcorrencias(i).getCodigoProcessamentoRemessa() == 3))) {

				lArquivo.setHabilitaBtnLote("V");
				lArquivo.setHabilitaBtnOcorrencia("F");
			} else if ((lResponse.getOcorrencias(i).getSituacaoProcessamento() == 3) && (lResponse.getOcorrencias(i).getCodigoProcessamentoRemessa() == 1)) {

				lArquivo.setHabilitaBtnLote("V");
				lArquivo.setHabilitaBtnOcorrencia("V");
			}

			lArquivo.setQtdRegistros(PgitUtil.formatarCampoQuantidade(String.valueOf(lResponse.getOcorrencias(i).getQuantidadeRegistroRemessa())));
			lArquivo.setValorTotal(PgitUtil.formatarCampoDeValorMonetario(lResponse.getOcorrencias(i).getValorRegistroRemessa()));

			if (lResponse.getOcorrencias(i).getBancoDebito() != 0) {
				lArquivo.setBanco(String.valueOf(lResponse.getOcorrencias(i).getBancoDebito()));
			} else {
				lArquivo.setBanco("");
			}

			if (lResponse.getOcorrencias(i).getAgenciaBancariaDebito() != 0) {
				lArquivo.setAgencia(String.valueOf(lResponse.getOcorrencias(i).getAgenciaBancariaDebito()));
			} else {
				lArquivo.setAgencia("");
			}

			if (lResponse.getOcorrencias(i).getRazaoContaDebito() != 0) {
				lArquivo.setRazao(String.valueOf(lResponse.getOcorrencias(i).getRazaoContaDebito()));
			} else {
				lArquivo.setRazao("");
			}

			if (lResponse.getOcorrencias(i).getContaDebito() != 0) {
				lArquivo.setConta(String.valueOf(lResponse.getOcorrencias(i).getContaDebito()));
			} else {
				lArquivo.setConta("");
			}

			if (lResponse.getOcorrencias(i).getCodigoLancamento() != 0) {
				lArquivo.setCodigoLancamento(String.valueOf(lResponse.getOcorrencias(i).getCodigoLancamento()));
			} else {
				lArquivo.setCodigoLancamento("");
			}

			lArquivo.setLayout(String.valueOf(lResponse.getOcorrencias(i).getCodigoLayoutArquivo()));
			lArquivo.setDescricaoLayout(lResponse.getOcorrencias(i).getDescricaoLayoutArquivo());
			lArquivo.setCanalTransmissao(String.valueOf(lResponse.getOcorrencias(i).getCodigoMeioTransmissao()));
			lArquivo.setDescricaoCanalTransmissao(lResponse.getOcorrencias(i).getDescricaoMeioTransmissao());
			lArquivo.setAmbienteProcessamento(lResponse.getOcorrencias(i).getCodigoAmbiente());
			lArquivo.setDescricaoAmbienteProcessamento(lResponse.getOcorrencias(i).getDescricaoAmbiente());
			lArquivo.setRemessa(String.valueOf(lResponse.getOcorrencias(i).getNumeroArquivoRemessa()));
			lArquivo.setTotalRegistros(i);

			lArquivos.add(lArquivo);
		}

		return lArquivos;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivorecebido.IArquivoRecebidoService#listaOcorrencia(br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.OcorrenciaDTO)
	 */
	public List<OcorrenciaDTO> listaOcorrencia(OcorrenciaDTO pOcorrenciaDTO) throws PdcAdapterException {
		ConsultarOcorrenciasRequest lRequest = new ConsultarOcorrenciasRequest();
		ConsultarOcorrenciasResponse lResponse = null;
		List<OcorrenciaDTO> listaResultado = null;

		// NUMERO OCORRENCIAS
		lRequest.setNumeroOcorrencias(27);

		// verificar o tipo de pequisa
		if (pOcorrenciaDTO.getTipoPesquisa().equals("R")) {
			// CODIGO DO SISTEMA
			if (pOcorrenciaDTO.getCodigoSistemaLegado() != null && !pOcorrenciaDTO.getCodigoSistemaLegado().equals("")) {
				lRequest.setCodigoSistema(pOcorrenciaDTO.getCodigoSistemaLegado());
			} else {
				lRequest.setCodigoSistema("");
			}
		} else {
			// CODIGO DO SISTEMA
			if (pOcorrenciaDTO.getSistemaLegado() != null && !pOcorrenciaDTO.getSistemaLegado().equals("")) {
				lRequest.setCodigoSistema(pOcorrenciaDTO.getSistemaLegado());
			} else {
				lRequest.setCodigoSistema("");
			}
		}

		// CPF
		if (pOcorrenciaDTO.getCpfCnpj() != null && !pOcorrenciaDTO.getCpfCnpj().equals("")) {
			String cnpjCpf = new String(pOcorrenciaDTO.getCpfCnpj());
			lRequest.setCpfCnpj(new Long(cnpjCpf));
		} else {
			lRequest.setCpfCnpj(new Long(0));
		}

		// FILIAL
		if (pOcorrenciaDTO.getFilialCnpj() != null && !pOcorrenciaDTO.getFilialCnpj().equals("")) {
			lRequest.setFilial(Integer.parseInt(pOcorrenciaDTO.getFilialCnpj()));
		} else {
			lRequest.setFilial(0);
		}

		// CONTROLE
		if (pOcorrenciaDTO.getControleCpfCnpj() != null && !pOcorrenciaDTO.getControleCpfCnpj().equals("")) {
			lRequest.setControle(Integer.parseInt(pOcorrenciaDTO.getControleCpfCnpj()));
		} else {
			lRequest.setControle(0);
		}

		// PERFIL
		if (pOcorrenciaDTO.getPerfil() != null && !pOcorrenciaDTO.getPerfil().equals("")) {
			lRequest.setPerfilComunicacao(new Long(pOcorrenciaDTO.getPerfil()).longValue());
		} else {
			lRequest.setPerfilComunicacao(new Long(0));
		}

		// NUMERO ARQUIVO REMESSA
		if (pOcorrenciaDTO.getNumeroArquivoRemessa() != null && !pOcorrenciaDTO.getNumeroArquivoRemessa().equals("")) {
			lRequest.setNumeroArquivoRemessa(new Long(pOcorrenciaDTO.getNumeroArquivoRemessa()).longValue());
		} else {
			lRequest.setNumeroArquivoRemessa(new Long(0));
		}

		// HORA INCLUSAO REMESSA
		if (!(SiteUtil.isEmptyOrNull(pOcorrenciaDTO.getDataRecepcaoInclusao()))) {
			lRequest.setHoraInclusaoRemessa(pOcorrenciaDTO.getDataRecepcaoInclusao());
		} else {
			lRequest.setHoraInclusaoRemessa("");
		}

		// NUMERO LOTE REMESSA
		if (pOcorrenciaDTO.getNumeroLoteRemessa() != null && !pOcorrenciaDTO.getNumeroLoteRemessa().equals("")) {
			lRequest.setNumeroLoteRemessa(new Long(pOcorrenciaDTO.getNumeroLoteRemessa()).longValue());
		} else {
			lRequest.setNumeroLoteRemessa(new Long(0));
		}

		// SEQUENCIA REGISTRO REMESSA
		if (pOcorrenciaDTO.getNumeroSequenciaRemessa() != null && !pOcorrenciaDTO.getNumeroSequenciaRemessa().equals("")) {
			lRequest.setSequenciaRegistroRemessa(new Long(pOcorrenciaDTO.getNumeroSequenciaRemessa()).longValue());
		} else {
			lRequest.setSequenciaRegistroRemessa(new Long(0));
		}

		lRequest.setTipoPesquisa(pOcorrenciaDTO.getTipoPesquisa());

		IConsultarOcorrenciasPDCAdapter consultarOcorrenciasPDCAdapter = factoryAdapter.getConsultarOcorrenciasPDCAdapter();
		lResponse = consultarOcorrenciasPDCAdapter.invokeProcess(lRequest);
		OcorrenciaDTO ocorrenciaDTO;
		listaResultado = new ArrayList<OcorrenciaDTO>();
		for (int i = 0; i < lResponse.getNumeroLinhas(); i++) {
			ocorrenciaDTO = new OcorrenciaDTO();
			ocorrenciaDTO.setOcorrencia(lResponse.getOcorrencias(i).getTextoMensagem());
			ocorrenciaDTO.setConteudoEnviado(lResponse.getOcorrencias(i).getConteudoEnviado());
			listaResultado.add(ocorrenciaDTO);
		}

		return listaResultado;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivorecebido.IArquivoRecebidoService#listaLote(br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.LoteDTO)
	 */
	public List<LoteDTO> listaLote(LoteDTO pLoteDTO) throws PdcAdapterException {

		ConsultarLoteRequest lRequest = new ConsultarLoteRequest();
		ConsultarLoteResponse lResponse = null;
		List<LoteDTO> listaResultado = null;
		// NUMERO OCORRENCIAS
		lRequest.setMaximoOcorrencia(100);

		// Codigo sistema
		if (pLoteDTO.getSistemaLegado() != null && !pLoteDTO.getSistemaLegado().equals("")) {
			lRequest.setCodSistemaLegado(pLoteDTO.getSistemaLegado());
		} else {
			lRequest.setCodSistemaLegado("");
		}

		// CPF/CNPJ
		if (pLoteDTO.getCpfCnpj() != null && !pLoteDTO.getCpfCnpj().equals("")) {
			lRequest.setCpfCnpj(Long.parseLong(pLoteDTO.getCpfCnpj()));
		} else {
			lRequest.setCpfCnpj(0);
		}

		if (pLoteDTO.getFilialCnpj() != null && !pLoteDTO.getFilialCnpj().equals("")) {
			lRequest.setCodFilialCnpj(Integer.parseInt(pLoteDTO.getFilialCnpj()));
		} else {
			lRequest.setCodFilialCnpj(0);
		}

		if (pLoteDTO.getControleCpfCnpj() != null && !pLoteDTO.getControleCpfCnpj().equals("")) {
			lRequest.setControleCnpj(Integer.parseInt(pLoteDTO.getControleCpfCnpj()));
		} else {
			lRequest.setControleCnpj(0);
		}

		// PERFIL
		if (pLoteDTO.getPerfil() != null && !pLoteDTO.getPerfil().equals("")) {
			lRequest.setCodPerfil(Long.parseLong(pLoteDTO.getPerfil()));
		} else {
			lRequest.setCodPerfil(0);
		}

		// NUMERO ARQUIVO REMESSA
		if (pLoteDTO.getNumeroArquivoRemessa() != null && !pLoteDTO.getNumeroArquivoRemessa().equals("")) {
			lRequest.setNumArqRemessa(Long.parseLong(pLoteDTO.getNumeroArquivoRemessa()));
		} else {
			lRequest.setNumArqRemessa(0l);
		}

		// HORA INCLUSAO REMESSA
		if (!(SiteUtil.isEmptyOrNull(pLoteDTO.getDataInclusaoRemessa()))) {
			lRequest.setHrInclusaoRemessa(pLoteDTO.getDataInclusaoRemessa());
		} else {
			lRequest.setHrInclusaoRemessa("");
		}

		// NUMERO LOTE REMESSA
		if (pLoteDTO.getNumeroLote() != null && !pLoteDTO.getNumeroLote().equals("")) {
			lRequest.setNumLoteRemessa(new Long(pLoteDTO.getNumeroLote()));
		} else {
			lRequest.setNumLoteRemessa(new Long(0));
		}

		// SEQUENCIA REGISTRO
		if (pLoteDTO.getSequenciaRemessa() != null && !pLoteDTO.getSequenciaRemessa().equals("")) {
			lRequest.setNumSeqRegistroRemessa(new Long(pLoteDTO.getSequenciaRemessa()));
		} else {
			lRequest.setNumSeqRegistroRemessa(new Long(0));
		}

		lRequest.setTipoPesquisa("L");

		IConsultarLotePDCAdapter consultarLotesPDCAdapter = factoryAdapter.getConsultarLotePDCAdapter();
		lResponse = consultarLotesPDCAdapter.invokeProcess(lRequest);
		LoteDTO loteDTO;
		listaResultado = new ArrayList<LoteDTO>();
		StringBuffer bcoAgeCtaDebito = null;

		for (int i = 0; i < lResponse.getNumeroLinhas(); i++) {
			loteDTO = new LoteDTO();
			bcoAgeCtaDebito = new StringBuffer(Integer.toString(lResponse.getOcorrencia(i).getBancoDbto()));
			bcoAgeCtaDebito = bcoAgeCtaDebito.append("/");
			bcoAgeCtaDebito = bcoAgeCtaDebito.append(lResponse.getOcorrencia(i).getAgenciaDbto());
			bcoAgeCtaDebito = bcoAgeCtaDebito.append("/");
			bcoAgeCtaDebito = bcoAgeCtaDebito.append(lResponse.getOcorrencia(i).getContaDbto());

			loteDTO.setCpfCnpj(pLoteDTO.getCpfCnpj());
			loteDTO.setFilialCnpj(pLoteDTO.getFilialCnpj());
			loteDTO.setControleCpfCnpj(pLoteDTO.getControleCpfCnpj());
			loteDTO.setPerfil(pLoteDTO.getPerfil());
			loteDTO.setNumeroArquivoRemessa(pLoteDTO.getNumeroArquivoRemessa());
			loteDTO.setDataInclusaoRemessa(pLoteDTO.getDataInclusaoRemessa());
			loteDTO.setNumeroLote(pLoteDTO.getNumeroLote());
			loteDTO.setSequenciaRemessa(pLoteDTO.getSequenciaRemessa());
			loteDTO.setSistemaLegado(pLoteDTO.getSistemaLegado());

			loteDTO.setSeqLote(String.valueOf(lResponse.getOcorrencia(i).getNumeroLoteRemessa()));
			loteDTO.setBcoAgeCtaDebito(bcoAgeCtaDebito.toString());
			loteDTO.setModalidadePagto(String.valueOf(lResponse.getOcorrencia(i).getCodModalidadePgto()));
			loteDTO.setDescricaoModalidadePagto(lResponse.getOcorrencia(i).getDescricaoModalidadePgto());
			loteDTO.setQtdeRegistros(PgitUtil.formatarCampoQuantidade(String.valueOf(lResponse.getOcorrencia(i).getQtdeRegistros())));
			loteDTO.setValorTotal(PgitUtil.formatarCampoDeValorMonetario(lResponse.getOcorrencia(i).getValorRegistros()));
			
			loteDTO.setSituacaoProcessamento(String.valueOf(lResponse.getOcorrencia(i).getCosSituacaoLote()));
			loteDTO.setDescricaoSituacaoProcessamento(String.valueOf(lResponse.getOcorrencia(i).getDescricaoSituacaoLote()));

			loteDTO.setResultadoProcessamento(String.valueOf(lResponse.getOcorrencia(i).getCondicaoLote()));
			loteDTO.setDescricaoResultadoProcessamento(String.valueOf(lResponse.getOcorrencia(i).getDescicaoCondicaoLote()));

			loteDTO.setSequencia(i);
			listaResultado.add(loteDTO);

		}

		return listaResultado;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivorecebido.IArquivoRecebidoService#listaGridEstatisticasLote(br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.EstatisticaLoteDTO)
	 */
	public List<EstatisticaLoteDTO> listaGridEstatisticasLote(EstatisticaLoteDTO pEstatisticaLoteDTO) throws PdcAdapterException {

		ConsultarEstatisticasLoteRequest lRequest = new ConsultarEstatisticasLoteRequest();
		ConsultarEstatisticasLoteResponse lResponse = null;
		List<EstatisticaLoteDTO> listaResultado = null;

		// NUMERO OCORRENCIAS
		lRequest.setNumeroOcorrencias(100);

		// CODIGO SISTEMAS
		if (pEstatisticaLoteDTO.getSistemaLegado() != null && !pEstatisticaLoteDTO.getSistemaLegado().equals("")) {
			lRequest.setCodigoSistema(pEstatisticaLoteDTO.getSistemaLegado());
		} else {
			lRequest.setCodigoSistema("");
		}

		// CPF
		String cnpjCpf = "";
		if (pEstatisticaLoteDTO.getCpfCnpj() != null && !pEstatisticaLoteDTO.getCpfCnpj().equals("")) {
			cnpjCpf = new String(pEstatisticaLoteDTO.getCpfCnpj().replace(".", "").replace("/", "").replace("-", ""));
			lRequest.setCpfCnpj(new Long(cnpjCpf));

		} else {
			lRequest.setCpfCnpj(new Long("0"));
		}

		// FILIAL
		if (pEstatisticaLoteDTO.getCodigoFilialCnpj() != null && !pEstatisticaLoteDTO.getCodigoFilialCnpj().equals("")) {
			lRequest.setFilial(Integer.parseInt(pEstatisticaLoteDTO.getCodigoFilialCnpj()));
		} else {
			lRequest.setFilial(0);
		}

		// CONTROLE
		if (pEstatisticaLoteDTO.getCodigoControleCpfCnpj() != null && !pEstatisticaLoteDTO.getCodigoControleCpfCnpj().equals("")) {
			lRequest.setControle(Integer.parseInt(pEstatisticaLoteDTO.getCodigoControleCpfCnpj()));
		} else {
			lRequest.setControle(0);
		}

		// PERFIL
		if (pEstatisticaLoteDTO.getPerfil() != null && !pEstatisticaLoteDTO.getPerfil().equals("")) {
			lRequest.setPerfilComunicacao(new Long(pEstatisticaLoteDTO.getPerfil()));
		} else {
			lRequest.setPerfilComunicacao(new Long("0"));
		}

		// NUMERO ARQUIVO REMESSA
		if (pEstatisticaLoteDTO.getNumeroArquivoRemessa() != null && !pEstatisticaLoteDTO.getNumeroArquivoRemessa().equals("")) {
			lRequest.setNumeroArquivoRemessa(new Long(pEstatisticaLoteDTO.getNumeroArquivoRemessa()));
		} else {
			lRequest.setNumeroArquivoRemessa(new Long("0"));
		}

		// HORA INCLUSAO REMESSA
		if (!(SiteUtil.isEmptyOrNull(pEstatisticaLoteDTO.getDataInclusaoRemessa()))) {
			lRequest.setHoraInclusaoRemessa(pEstatisticaLoteDTO.getDataInclusaoRemessa());
		} else {
			lRequest.setHoraInclusaoRemessa("");
		}

		// NUMERO LOTE REMESSA
		if (pEstatisticaLoteDTO.getNumeroLoteRemessa() != null && !pEstatisticaLoteDTO.getNumeroLoteRemessa().equals("")) {
			lRequest.setNumeroLoteRemessa(new Long(pEstatisticaLoteDTO.getNumeroLoteRemessa()));
		} else {
			lRequest.setNumeroLoteRemessa(new Long("0"));
		}

		// NUMERO SEQUENCIA REMESSA
		if (pEstatisticaLoteDTO.getNumeroSequenciaRemessa() != null && !pEstatisticaLoteDTO.getNumeroSequenciaRemessa().equals("")) {
			lRequest.setNumeroSequenciaRemessa(new Long(pEstatisticaLoteDTO.getNumeroSequenciaRemessa()));
		} else {
			lRequest.setNumeroSequenciaRemessa(new Long("0"));
		}

		lRequest.setTipoPesquisa("L");

		// Fazemos a chamada do processo.
		IConsultarEstatisticasLotePDCAdapter estatisticasLotesPDCAdapter = factoryAdapter.getConsultarEstatisticasLotePDCAdapter();
		lResponse = estatisticasLotesPDCAdapter.invokeProcess(lRequest);

		EstatisticaLoteDTO estatisticaLoteDTO;
		listaResultado = new ArrayList<EstatisticaLoteDTO>();

		for (int i = 0; i < lResponse.getNumeroLinhas(); i++) {
			estatisticaLoteDTO = new EstatisticaLoteDTO();
			// Para cada registro da grid
			estatisticaLoteDTO.setRegConsistenteQtde(PgitUtil.formatarCampoQuantidade(String.valueOf(lResponse.getOcorrencias(i).getQuantidadeRegistroConsistente())));
			estatisticaLoteDTO.setRegConsistenteValor(PgitUtil.formatarCampoDeValorMonetario(lResponse.getOcorrencias(i).getValorRegistroConsistente()));
			estatisticaLoteDTO.setRegInconsistenteQtde(PgitUtil.formatarCampoQuantidade(String.valueOf(lResponse.getOcorrencias(i).getQuantidadeRegistroIncosistente())));
			estatisticaLoteDTO.setRegInconsistenteValor(PgitUtil.formatarCampoDeValorMonetario(lResponse.getOcorrencias(i).getValorRegistroInconsistente()));
			estatisticaLoteDTO.setRegTotalGeralQtde(PgitUtil.formatarCampoQuantidade(String.valueOf(lResponse.getOcorrencias(i).getQuantidadeRegistros())));
			estatisticaLoteDTO.setRegTotalGeralValor(PgitUtil.formatarCampoDeValorMonetario(lResponse.getOcorrencias(i).getValorRegistros()));

			estatisticaLoteDTO.setInstrucaoMovimento(String.valueOf(lResponse.getOcorrencias(i).getDescricaoMovimento()));

			// Geral
			estatisticaLoteDTO.setQtdeConsitentesLong(lResponse.getQuantidadeConsistente());
			estatisticaLoteDTO.setQtdeInconsitentesLong(lResponse.getQuantidadeInconsistente());
			
			estatisticaLoteDTO.setSomaValorConsistentesDouble(lResponse.getValorConsistente());			
			estatisticaLoteDTO.setSomaValorInconsistentesDouble(lResponse.getValorIncosistente());
			
			estatisticaLoteDTO.setTotalGeralQtde(lResponse.getQuantidadeTotal());
			estatisticaLoteDTO.setTotalGeralValor(lResponse.getValorTotal());

			listaResultado.add(estatisticaLoteDTO);

		}

		return listaResultado;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivorecebido.IArquivoRecebidoService#listaComboProduto()
	 */
	public List<ComboProdutoDTO> listaComboProduto() throws PdcAdapterException {

		List<ComboProdutoDTO> lArquivos = new ArrayList<ComboProdutoDTO>();
		MontarComboProdutoRequest lRequest = new MontarComboProdutoRequest();
		MontarComboProdutoResponse lResponse = null;

		// Fazemos a chamada do processo.
		IMontarComboProdutoPDCAdapter montarComboProdutoPDCAdapter = factoryAdapter.getMontarComboProdutoPDCAdapter();

		lRequest.setEntrada("");
		lResponse = montarComboProdutoPDCAdapter.invokeProcess(lRequest);

		ComboProdutoDTO lArquivo;

		// Para cada item retornado do processo, adicionamos a lista de
		// retorno.
		for (int i = 0; i < lResponse.getOcorrenciasCount(); i++) {

			lArquivo = new ComboProdutoDTO();
			lArquivo.setCentroCusto(String.valueOf(lResponse.getOcorrencias(i).getCodigoCentroCusto()));
			lArquivo.setDescricaoCentroCusto(lResponse.getOcorrencias(i).getDescricaoProdutoPagamento());
			lArquivo.setCodigoProduto(String.valueOf(lResponse.getOcorrencias(i).getCodigoProdutoPagamento()));
			lArquivos.add(lArquivo);
		}

		return lArquivos;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivorecebido.IArquivoRecebidoService#listaGridIncosistentes(br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ArquivoRecebidoInconsistentesDTO)
	 */
	public List<ArquivoRecebidoInconsistentesDTO> listaGridIncosistentes(ArquivoRecebidoInconsistentesDTO pArquivoRemessaRecebidoInconsistentesDTO) throws PdcAdapterException {

		List<ArquivoRecebidoInconsistentesDTO> lArquivosInconsistentes = new ArrayList<ArquivoRecebidoInconsistentesDTO>();
		ConsultarInconsistenciasRequest lRequest = new ConsultarInconsistenciasRequest();
		ConsultarInconsistenciasResponse lResponseConsultarInconsistencias = null;

		// NUMERO OCORRENCIAS
		lRequest.setNumeroOcorrencias(100);

		// SISTEMA LEGADO
		if (pArquivoRemessaRecebidoInconsistentesDTO.getSistemaLegado() != null && !pArquivoRemessaRecebidoInconsistentesDTO.getSistemaLegado().equals("")) {
			lRequest.setCodigoSistema(pArquivoRemessaRecebidoInconsistentesDTO.getSistemaLegado());
		} else {
			lRequest.setCodigoSistema("");
		}

		// CPF
		String cnpjCpf = "";
		if (pArquivoRemessaRecebidoInconsistentesDTO.getCpfCnpj() != null && !pArquivoRemessaRecebidoInconsistentesDTO.getCpfCnpj().equals("")) {
			cnpjCpf = new String(pArquivoRemessaRecebidoInconsistentesDTO.getCpfCnpj().replace(".", "").replace("/", "").replace("-", ""));
			lRequest.setCpfCnpj(new Long(cnpjCpf));

		} else {
			lRequest.setCpfCnpj(new Long("0"));
		}

		// FILIAL
		if (pArquivoRemessaRecebidoInconsistentesDTO.getCodigoFilialCnpj() != null && !pArquivoRemessaRecebidoInconsistentesDTO.getCodigoFilialCnpj().equals("")) {
			lRequest.setFilial(Integer.parseInt(pArquivoRemessaRecebidoInconsistentesDTO.getCodigoFilialCnpj()));
		} else {
			lRequest.setFilial(0);
		}

		// CONTROLE
		if (pArquivoRemessaRecebidoInconsistentesDTO.getCodigoControleCpfCnpj() != null && !pArquivoRemessaRecebidoInconsistentesDTO.getCodigoControleCpfCnpj().equals("")) {
			lRequest.setControle(Integer.parseInt(pArquivoRemessaRecebidoInconsistentesDTO.getCodigoControleCpfCnpj()));
		} else {
			lRequest.setControle(0);
		}

		// PERFIL
		if (pArquivoRemessaRecebidoInconsistentesDTO.getPerfil() != null && !pArquivoRemessaRecebidoInconsistentesDTO.getPerfil().equals("")) {
			lRequest.setPerfilComunicacao(new Long(pArquivoRemessaRecebidoInconsistentesDTO.getPerfil()));
		} else {
			lRequest.setPerfilComunicacao(new Long("0"));
		}

		// NUMERO ARQUIVO REMESSA
		if (pArquivoRemessaRecebidoInconsistentesDTO.getNumeroArquivoRemessa() != null && !pArquivoRemessaRecebidoInconsistentesDTO.getNumeroArquivoRemessa().equals("")) {
			lRequest.setNumeroArquivoRemessa(new Long(pArquivoRemessaRecebidoInconsistentesDTO.getNumeroArquivoRemessa()));
		} else {
			lRequest.setNumeroArquivoRemessa(new Long(0));
		}

		// HORA INCLUSAO REMESSA
		if (!(SiteUtil.isEmptyOrNull(pArquivoRemessaRecebidoInconsistentesDTO.getDataInclusaoRemessa()))) {
			lRequest.setHoraInclusaoRemessa(pArquivoRemessaRecebidoInconsistentesDTO.getDataInclusaoRemessa());
		} else {
			lRequest.setHoraInclusaoRemessa("");
		}

		// NUMERO LOTE REMESSA
		if (pArquivoRemessaRecebidoInconsistentesDTO.getNumeroLoteRemessa() != null && !pArquivoRemessaRecebidoInconsistentesDTO.getNumeroLoteRemessa().equals("")) {
			lRequest.setNumeroLoteRemessa(new Long(pArquivoRemessaRecebidoInconsistentesDTO.getNumeroLoteRemessa()));
		} else {
			lRequest.setNumeroLoteRemessa(new Long(0));
		}

		// SEQUENCIA REGISTRO REMESSA
		if (pArquivoRemessaRecebidoInconsistentesDTO.getNumeroSequenciaRemessa() != null && !pArquivoRemessaRecebidoInconsistentesDTO.getNumeroSequenciaRemessa().equals("")) {
			lRequest.setSequenciaRegistroRemessa(new Long(pArquivoRemessaRecebidoInconsistentesDTO.getNumeroSequenciaRemessa()));
		} else {
			lRequest.setSequenciaRegistroRemessa(new Long(0));
		}

		lRequest.setTipoPesquisa("L");

		// Fazemos a chamada do processo.
		IConsultarInconsistenciaPDCAdapter consultarInconsistenciasPDCAdapter = factoryAdapter.getConsultarInconsistenciaPDCAdapter();
		lResponseConsultarInconsistencias = consultarInconsistenciasPDCAdapter.invokeProcess(lRequest);

		// Para cada item retornado do processo, adicionamos a lista de
		// retorno.
		ArquivoRecebidoInconsistentesDTO arquivoRemessaRecebidoInconsistentesDTO;
		for (int i = 0; i < lResponseConsultarInconsistencias.getNumeroLinhas(); i++) {

			arquivoRemessaRecebidoInconsistentesDTO = new ArquivoRecebidoInconsistentesDTO();

			arquivoRemessaRecebidoInconsistentesDTO.setNumeroSequenciaRegistroRemessa(String.valueOf(lResponseConsultarInconsistencias.getCMPIW034CONSULTASSAIDA(i).getSequenciaRegistroRemessa()));
			arquivoRemessaRecebidoInconsistentesDTO.setCodigoInstrucaoMovimento(String.valueOf(lResponseConsultarInconsistencias.getCMPIW034CONSULTASSAIDA(i).getInstrucaoMovimento()));
			arquivoRemessaRecebidoInconsistentesDTO.setDescricaoInstrucaoMovimento(lResponseConsultarInconsistencias.getCMPIW034CONSULTASSAIDA(i).getDecricaoInstrucaoMovimento());
			arquivoRemessaRecebidoInconsistentesDTO.setControlePagamento(lResponseConsultarInconsistencias.getCMPIW034CONSULTASSAIDA(i).getControlePagamento());
			arquivoRemessaRecebidoInconsistentesDTO.setTotalRegistros(i);
			
			lArquivosInconsistentes.add(arquivoRemessaRecebidoInconsistentesDTO);
		}

		return lArquivosInconsistentes;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.arquivorecebido.IArquivoRecebidoService#listaGridClientes(br.com.bradesco.web.pgit.service.business.cmpi0000.bean.ListarClientesDTO)
	 */
	public List<ListarClientesDTO> listaGridClientes(ListarClientesDTO pListarClientesDTO) throws PdcAdapterException {

		List<ListarClientesDTO> lListarClientes = new ArrayList<ListarClientesDTO>();
		ListarClientesRequest listarClientesRequest = new ListarClientesRequest();
		ListarClientesResponse listarClientesResponse = null;

		// CPF/CNPJ
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getCodigoCpfCnpjCliente()))) {
			listarClientesRequest.setCpfCnpj(new Long(pListarClientesDTO.getCodigoCpfCnpjCliente()));
		} else {
			listarClientesRequest.setCpfCnpj(0);
		}

		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getCodigoFilialCnpj()))) {
			listarClientesRequest.setFilial(Integer.parseInt(pListarClientesDTO.getCodigoFilialCnpj()));
		} else {
			listarClientesRequest.setFilial(0);
		}

		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getControleCpfCnpj()))) {
			listarClientesRequest.setControle(Integer.parseInt(pListarClientesDTO.getControleCpfCnpj()));
		} else {
			listarClientesRequest.setControle(0);
		}

		// Nome Raz�o
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getNomeRazao()))) {
			listarClientesRequest.setNomeRazao(pListarClientesDTO.getNomeRazao());
		} else {
			listarClientesRequest.setNomeRazao("");
		}

		// DataNascimentoFundacao
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getDataNascimentoFundacao()))) {
			listarClientesRequest.setDataNascimentoFundacao(pListarClientesDTO.getDataNascimentoFundacao());
		} else {
			listarClientesRequest.setDataNascimentoFundacao("");
		}

		// Codigo Banco
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getCodigoBancoDebito()))) {
			listarClientesRequest.setBanco(Integer.parseInt(pListarClientesDTO.getCodigoBancoDebito()));
		} else {
			listarClientesRequest.setBanco(0);
		}

		// Agencia
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getAgenciaBancaria()))) {
			listarClientesRequest.setAgencia(Integer.parseInt(pListarClientesDTO.getAgenciaBancaria()));
		} else {
			listarClientesRequest.setAgencia(0);
		}

		// Conta
		if (!(SiteUtil.isEmptyOrNull(pListarClientesDTO.getContaBancaria()))) {
			listarClientesRequest.setConta(Integer.parseInt(pListarClientesDTO.getContaBancaria()));
		} else {
			listarClientesRequest.setConta(0);
		}

		// Fazemos a chamada do processo.
		IListarClientesPDCAdapter listarClientesPDCAdapter = factoryAdapter.getListarClientesPDCAdapter();
		listarClientesResponse = listarClientesPDCAdapter.invokeProcess(listarClientesRequest);

		StringBuffer bcoAgeConta = null;
		// Para cada item retornado do processo, adicionamos a lista de
		// retorno.

		String cpf = "";
		String cpfCliente = "";
		String cpfFilial = "";
		String cpfControle = "";

		for (int i = 0; i < listarClientesResponse.getNumeroLinhas(); i++) {
			ListarClientesDTO listarClientesDTO = new ListarClientesDTO();

			if (listarClientesResponse.getOcorrencias(i).getFilial() == 0) {

				cpfCliente = String.valueOf(listarClientesResponse.getOcorrencias(i).getCpfCnpj());

				cpfControle = String.valueOf(listarClientesResponse.getOcorrencias(i).getControle());

				cpf = PgitUtil.formatarCPFComMascara(cpfCliente, cpfControle);
			} else {

				cpfCliente = String.valueOf(listarClientesResponse.getOcorrencias(i).getCpfCnpj());

				cpfFilial = String.valueOf(listarClientesResponse.getOcorrencias(i).getFilial());

				cpfControle = String.valueOf(listarClientesResponse.getOcorrencias(i).getControle());

				cpf = PgitUtil.formatarCNPJComMascara(cpfCliente, cpfFilial, cpfControle);
			}

			listarClientesDTO.setCpf(cpf);
			listarClientesDTO.setCpfCnpjCliente(cpfCliente);
			listarClientesDTO.setFilialCnpj(cpfFilial);
			listarClientesDTO.setControleCpfCnpj(cpfControle);
			listarClientesDTO.setNomeRazao(listarClientesResponse.getOcorrencias(i).getNomeRazao());

			try {
				listarClientesDTO.setDataNascimentoFundacao(SiteUtil.changeStringDateFormat(listarClientesResponse.getOcorrencias(i).getDataNacscimentoFundacao(), "dd/MM/yyyy", "dd/MM/yyyy"));
			} catch (ParseException e) {
				listarClientesDTO.setDataNascimentoFundacao("");
			}

			bcoAgeConta = new StringBuffer();
			bcoAgeConta = bcoAgeConta.append(listarClientesResponse.getOcorrencias(i).getBanco());
			bcoAgeConta = bcoAgeConta.append("/");
			bcoAgeConta = bcoAgeConta.append(listarClientesResponse.getOcorrencias(i).getAgencia());
			bcoAgeConta = bcoAgeConta.append("/");
			bcoAgeConta = bcoAgeConta.append(listarClientesResponse.getOcorrencias(i).getConta());
			listarClientesDTO.setBcoAgeConta(bcoAgeConta.toString());
			listarClientesDTO.setTotalRegistros(i);
			lListarClientes.add(listarClientesDTO);
		}

		return lListarClientes;
	}

}
