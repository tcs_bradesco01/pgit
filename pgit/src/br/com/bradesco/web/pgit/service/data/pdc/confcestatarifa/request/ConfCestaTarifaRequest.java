/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.confcestatarifa.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConfCestaTarifaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConfCestaTarifaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _maxOcorrencias
     */
    private int _maxOcorrencias = 0;

    /**
     * keeps track of state for field: _maxOcorrencias
     */
    private boolean _has_maxOcorrencias;

    /**
     * Field _cdPessoasJuridicaContrato
     */
    private long _cdPessoasJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoasJuridicaContrato
     */
    private boolean _has_cdPessoasJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _numSequenciaNegocio
     */
    private long _numSequenciaNegocio = 0;

    /**
     * keeps track of state for field: _numSequenciaNegocio
     */
    private boolean _has_numSequenciaNegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConfCestaTarifaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.confcestatarifa.request.ConfCestaTarifaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoasJuridicaContrato
     * 
     */
    public void deleteCdPessoasJuridicaContrato()
    {
        this._has_cdPessoasJuridicaContrato= false;
    } //-- void deleteCdPessoasJuridicaContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteMaxOcorrencias
     * 
     */
    public void deleteMaxOcorrencias()
    {
        this._has_maxOcorrencias= false;
    } //-- void deleteMaxOcorrencias() 

    /**
     * Method deleteNumSequenciaNegocio
     * 
     */
    public void deleteNumSequenciaNegocio()
    {
        this._has_numSequenciaNegocio= false;
    } //-- void deleteNumSequenciaNegocio() 

    /**
     * Returns the value of field 'cdPessoasJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoasJuridicaContrato'.
     */
    public long getCdPessoasJuridicaContrato()
    {
        return this._cdPessoasJuridicaContrato;
    } //-- long getCdPessoasJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'maxOcorrencias'.
     * 
     * @return int
     * @return the value of field 'maxOcorrencias'.
     */
    public int getMaxOcorrencias()
    {
        return this._maxOcorrencias;
    } //-- int getMaxOcorrencias() 

    /**
     * Returns the value of field 'numSequenciaNegocio'.
     * 
     * @return long
     * @return the value of field 'numSequenciaNegocio'.
     */
    public long getNumSequenciaNegocio()
    {
        return this._numSequenciaNegocio;
    } //-- long getNumSequenciaNegocio() 

    /**
     * Method hasCdPessoasJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoasJuridicaContrato()
    {
        return this._has_cdPessoasJuridicaContrato;
    } //-- boolean hasCdPessoasJuridicaContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasMaxOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasMaxOcorrencias()
    {
        return this._has_maxOcorrencias;
    } //-- boolean hasMaxOcorrencias() 

    /**
     * Method hasNumSequenciaNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumSequenciaNegocio()
    {
        return this._has_numSequenciaNegocio;
    } //-- boolean hasNumSequenciaNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoasJuridicaContrato'.
     * 
     * @param cdPessoasJuridicaContrato the value of field
     * 'cdPessoasJuridicaContrato'.
     */
    public void setCdPessoasJuridicaContrato(long cdPessoasJuridicaContrato)
    {
        this._cdPessoasJuridicaContrato = cdPessoasJuridicaContrato;
        this._has_cdPessoasJuridicaContrato = true;
    } //-- void setCdPessoasJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'maxOcorrencias'.
     * 
     * @param maxOcorrencias the value of field 'maxOcorrencias'.
     */
    public void setMaxOcorrencias(int maxOcorrencias)
    {
        this._maxOcorrencias = maxOcorrencias;
        this._has_maxOcorrencias = true;
    } //-- void setMaxOcorrencias(int) 

    /**
     * Sets the value of field 'numSequenciaNegocio'.
     * 
     * @param numSequenciaNegocio the value of field
     * 'numSequenciaNegocio'.
     */
    public void setNumSequenciaNegocio(long numSequenciaNegocio)
    {
        this._numSequenciaNegocio = numSequenciaNegocio;
        this._has_numSequenciaNegocio = true;
    } //-- void setNumSequenciaNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConfCestaTarifaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.confcestatarifa.request.ConfCestaTarifaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.confcestatarifa.request.ConfCestaTarifaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.confcestatarifa.request.ConfCestaTarifaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.confcestatarifa.request.ConfCestaTarifaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
