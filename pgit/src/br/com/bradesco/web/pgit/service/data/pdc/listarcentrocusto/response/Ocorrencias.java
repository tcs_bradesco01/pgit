/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcentrocusto.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCentroCusto
     */
    private java.lang.String _cdCentroCusto;

    /**
     * Field _dsCentroCusto
     */
    private java.lang.String _dsCentroCusto;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcentrocusto.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'cdCentroCusto'.
     * 
     * @return String
     * @return the value of field 'cdCentroCusto'.
     */
    public java.lang.String getCdCentroCusto()
    {
        return this._cdCentroCusto;
    } //-- java.lang.String getCdCentroCusto() 

    /**
     * Returns the value of field 'dsCentroCusto'.
     * 
     * @return String
     * @return the value of field 'dsCentroCusto'.
     */
    public java.lang.String getDsCentroCusto()
    {
        return this._dsCentroCusto;
    } //-- java.lang.String getDsCentroCusto() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCentroCusto'.
     * 
     * @param cdCentroCusto the value of field 'cdCentroCusto'.
     */
    public void setCdCentroCusto(java.lang.String cdCentroCusto)
    {
        this._cdCentroCusto = cdCentroCusto;
    } //-- void setCdCentroCusto(java.lang.String) 

    /**
     * Sets the value of field 'dsCentroCusto'.
     * 
     * @param dsCentroCusto the value of field 'dsCentroCusto'.
     */
    public void setDsCentroCusto(java.lang.String dsCentroCusto)
    {
        this._dsCentroCusto = dsCentroCusto;
    } //-- void setDsCentroCusto(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcentrocusto.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcentrocusto.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcentrocusto.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcentrocusto.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
