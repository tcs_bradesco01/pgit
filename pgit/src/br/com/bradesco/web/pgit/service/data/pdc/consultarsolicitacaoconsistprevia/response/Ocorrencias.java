/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoconsistprevia.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdConsistenciaPreviaPagamento
     */
    private int _cdConsistenciaPreviaPagamento = 0;

    /**
     * keeps track of state for field: _cdConsistenciaPreviaPagament
     */
    private boolean _has_cdConsistenciaPreviaPagamento;

    /**
     * Field _cdInscricaoPagador
     */
    private long _cdInscricaoPagador = 0;

    /**
     * keeps track of state for field: _cdInscricaoPagador
     */
    private boolean _has_cdInscricaoPagador;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _hrInclusaoPagamento
     */
    private java.lang.String _hrInclusaoPagamento;

    /**
     * Field _cdSituacaoLotePagamento
     */
    private java.lang.String _cdSituacaoLotePagamento;

    /**
     * Field _dsSituacaoLotePagamento
     */
    private java.lang.String _dsSituacaoLotePagamento;

    /**
     * Field _dtGravacaoLote
     */
    private java.lang.String _dtGravacaoLote;

    /**
     * Field _cdTipoLote
     */
    private int _cdTipoLote = 0;

    /**
     * keeps track of state for field: _cdTipoLote
     */
    private boolean _has_cdTipoLote;

    /**
     * Field _nrLoteArquivoRemessa
     */
    private long _nrLoteArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrLoteArquivoRemessa
     */
    private boolean _has_nrLoteArquivoRemessa;

    /**
     * Field _cdSituacaoSolicitacao
     */
    private int _cdSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdSituacaoSolicitacao
     */
    private boolean _has_cdSituacaoSolicitacao;

    /**
     * Field _dsSituacaoSolicitacao
     */
    private java.lang.String _dsSituacaoSolicitacao;

    /**
     * Field _cdControleTransacao
     */
    private long _cdControleTransacao = 0;

    /**
     * keeps track of state for field: _cdControleTransacao
     */
    private boolean _has_cdControleTransacao;

    /**
     * Field _dtProcessoLote
     */
    private java.lang.String _dtProcessoLote;

    /**
     * Field _cdVolumeTotal
     */
    private long _cdVolumeTotal = 0;

    /**
     * keeps track of state for field: _cdVolumeTotal
     */
    private boolean _has_cdVolumeTotal;

    /**
     * Field _vlTotal
     */
    private java.math.BigDecimal _vlTotal = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlTotal(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoconsistprevia.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdConsistenciaPreviaPagamento
     * 
     */
    public void deleteCdConsistenciaPreviaPagamento()
    {
        this._has_cdConsistenciaPreviaPagamento= false;
    } //-- void deleteCdConsistenciaPreviaPagamento() 

    /**
     * Method deleteCdControleTransacao
     * 
     */
    public void deleteCdControleTransacao()
    {
        this._has_cdControleTransacao= false;
    } //-- void deleteCdControleTransacao() 

    /**
     * Method deleteCdInscricaoPagador
     * 
     */
    public void deleteCdInscricaoPagador()
    {
        this._has_cdInscricaoPagador= false;
    } //-- void deleteCdInscricaoPagador() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoSolicitacao
     * 
     */
    public void deleteCdSituacaoSolicitacao()
    {
        this._has_cdSituacaoSolicitacao= false;
    } //-- void deleteCdSituacaoSolicitacao() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoLote
     * 
     */
    public void deleteCdTipoLote()
    {
        this._has_cdTipoLote= false;
    } //-- void deleteCdTipoLote() 

    /**
     * Method deleteCdVolumeTotal
     * 
     */
    public void deleteCdVolumeTotal()
    {
        this._has_cdVolumeTotal= false;
    } //-- void deleteCdVolumeTotal() 

    /**
     * Method deleteNrLoteArquivoRemessa
     * 
     */
    public void deleteNrLoteArquivoRemessa()
    {
        this._has_nrLoteArquivoRemessa= false;
    } //-- void deleteNrLoteArquivoRemessa() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdConsistenciaPreviaPagamento'.
     * 
     * @return int
     * @return the value of field 'cdConsistenciaPreviaPagamento'.
     */
    public int getCdConsistenciaPreviaPagamento()
    {
        return this._cdConsistenciaPreviaPagamento;
    } //-- int getCdConsistenciaPreviaPagamento() 

    /**
     * Returns the value of field 'cdControleTransacao'.
     * 
     * @return long
     * @return the value of field 'cdControleTransacao'.
     */
    public long getCdControleTransacao()
    {
        return this._cdControleTransacao;
    } //-- long getCdControleTransacao() 

    /**
     * Returns the value of field 'cdInscricaoPagador'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoPagador'.
     */
    public long getCdInscricaoPagador()
    {
        return this._cdInscricaoPagador;
    } //-- long getCdInscricaoPagador() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoLotePagamento'.
     * 
     * @return String
     * @return the value of field 'cdSituacaoLotePagamento'.
     */
    public java.lang.String getCdSituacaoLotePagamento()
    {
        return this._cdSituacaoLotePagamento;
    } //-- java.lang.String getCdSituacaoLotePagamento() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacao'.
     */
    public int getCdSituacaoSolicitacao()
    {
        return this._cdSituacaoSolicitacao;
    } //-- int getCdSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoLote'.
     * 
     * @return int
     * @return the value of field 'cdTipoLote'.
     */
    public int getCdTipoLote()
    {
        return this._cdTipoLote;
    } //-- int getCdTipoLote() 

    /**
     * Returns the value of field 'cdVolumeTotal'.
     * 
     * @return long
     * @return the value of field 'cdVolumeTotal'.
     */
    public long getCdVolumeTotal()
    {
        return this._cdVolumeTotal;
    } //-- long getCdVolumeTotal() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Returns the value of field 'dsSituacaoLotePagamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoLotePagamento'.
     */
    public java.lang.String getDsSituacaoLotePagamento()
    {
        return this._dsSituacaoLotePagamento;
    } //-- java.lang.String getDsSituacaoLotePagamento() 

    /**
     * Returns the value of field 'dsSituacaoSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSolicitacao'.
     */
    public java.lang.String getDsSituacaoSolicitacao()
    {
        return this._dsSituacaoSolicitacao;
    } //-- java.lang.String getDsSituacaoSolicitacao() 

    /**
     * Returns the value of field 'dtGravacaoLote'.
     * 
     * @return String
     * @return the value of field 'dtGravacaoLote'.
     */
    public java.lang.String getDtGravacaoLote()
    {
        return this._dtGravacaoLote;
    } //-- java.lang.String getDtGravacaoLote() 

    /**
     * Returns the value of field 'dtProcessoLote'.
     * 
     * @return String
     * @return the value of field 'dtProcessoLote'.
     */
    public java.lang.String getDtProcessoLote()
    {
        return this._dtProcessoLote;
    } //-- java.lang.String getDtProcessoLote() 

    /**
     * Returns the value of field 'hrInclusaoPagamento'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoPagamento'.
     */
    public java.lang.String getHrInclusaoPagamento()
    {
        return this._hrInclusaoPagamento;
    } //-- java.lang.String getHrInclusaoPagamento() 

    /**
     * Returns the value of field 'nrLoteArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrLoteArquivoRemessa'.
     */
    public long getNrLoteArquivoRemessa()
    {
        return this._nrLoteArquivoRemessa;
    } //-- long getNrLoteArquivoRemessa() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'vlTotal'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotal'.
     */
    public java.math.BigDecimal getVlTotal()
    {
        return this._vlTotal;
    } //-- java.math.BigDecimal getVlTotal() 

    /**
     * Method hasCdConsistenciaPreviaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConsistenciaPreviaPagamento()
    {
        return this._has_cdConsistenciaPreviaPagamento;
    } //-- boolean hasCdConsistenciaPreviaPagamento() 

    /**
     * Method hasCdControleTransacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleTransacao()
    {
        return this._has_cdControleTransacao;
    } //-- boolean hasCdControleTransacao() 

    /**
     * Method hasCdInscricaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoPagador()
    {
        return this._has_cdInscricaoPagador;
    } //-- boolean hasCdInscricaoPagador() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacao()
    {
        return this._has_cdSituacaoSolicitacao;
    } //-- boolean hasCdSituacaoSolicitacao() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoLote
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLote()
    {
        return this._has_cdTipoLote;
    } //-- boolean hasCdTipoLote() 

    /**
     * Method hasCdVolumeTotal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdVolumeTotal()
    {
        return this._has_cdVolumeTotal;
    } //-- boolean hasCdVolumeTotal() 

    /**
     * Method hasNrLoteArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrLoteArquivoRemessa()
    {
        return this._has_nrLoteArquivoRemessa;
    } //-- boolean hasNrLoteArquivoRemessa() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdConsistenciaPreviaPagamento'.
     * 
     * @param cdConsistenciaPreviaPagamento the value of field
     * 'cdConsistenciaPreviaPagamento'.
     */
    public void setCdConsistenciaPreviaPagamento(int cdConsistenciaPreviaPagamento)
    {
        this._cdConsistenciaPreviaPagamento = cdConsistenciaPreviaPagamento;
        this._has_cdConsistenciaPreviaPagamento = true;
    } //-- void setCdConsistenciaPreviaPagamento(int) 

    /**
     * Sets the value of field 'cdControleTransacao'.
     * 
     * @param cdControleTransacao the value of field
     * 'cdControleTransacao'.
     */
    public void setCdControleTransacao(long cdControleTransacao)
    {
        this._cdControleTransacao = cdControleTransacao;
        this._has_cdControleTransacao = true;
    } //-- void setCdControleTransacao(long) 

    /**
     * Sets the value of field 'cdInscricaoPagador'.
     * 
     * @param cdInscricaoPagador the value of field
     * 'cdInscricaoPagador'.
     */
    public void setCdInscricaoPagador(long cdInscricaoPagador)
    {
        this._cdInscricaoPagador = cdInscricaoPagador;
        this._has_cdInscricaoPagador = true;
    } //-- void setCdInscricaoPagador(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoLotePagamento'.
     * 
     * @param cdSituacaoLotePagamento the value of field
     * 'cdSituacaoLotePagamento'.
     */
    public void setCdSituacaoLotePagamento(java.lang.String cdSituacaoLotePagamento)
    {
        this._cdSituacaoLotePagamento = cdSituacaoLotePagamento;
    } //-- void setCdSituacaoLotePagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacao'.
     * 
     * @param cdSituacaoSolicitacao the value of field
     * 'cdSituacaoSolicitacao'.
     */
    public void setCdSituacaoSolicitacao(int cdSituacaoSolicitacao)
    {
        this._cdSituacaoSolicitacao = cdSituacaoSolicitacao;
        this._has_cdSituacaoSolicitacao = true;
    } //-- void setCdSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoLote'.
     * 
     * @param cdTipoLote the value of field 'cdTipoLote'.
     */
    public void setCdTipoLote(int cdTipoLote)
    {
        this._cdTipoLote = cdTipoLote;
        this._has_cdTipoLote = true;
    } //-- void setCdTipoLote(int) 

    /**
     * Sets the value of field 'cdVolumeTotal'.
     * 
     * @param cdVolumeTotal the value of field 'cdVolumeTotal'.
     */
    public void setCdVolumeTotal(long cdVolumeTotal)
    {
        this._cdVolumeTotal = cdVolumeTotal;
        this._has_cdVolumeTotal = true;
    } //-- void setCdVolumeTotal(long) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoLotePagamento'.
     * 
     * @param dsSituacaoLotePagamento the value of field
     * 'dsSituacaoLotePagamento'.
     */
    public void setDsSituacaoLotePagamento(java.lang.String dsSituacaoLotePagamento)
    {
        this._dsSituacaoLotePagamento = dsSituacaoLotePagamento;
    } //-- void setDsSituacaoLotePagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSolicitacao'.
     * 
     * @param dsSituacaoSolicitacao the value of field
     * 'dsSituacaoSolicitacao'.
     */
    public void setDsSituacaoSolicitacao(java.lang.String dsSituacaoSolicitacao)
    {
        this._dsSituacaoSolicitacao = dsSituacaoSolicitacao;
    } //-- void setDsSituacaoSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dtGravacaoLote'.
     * 
     * @param dtGravacaoLote the value of field 'dtGravacaoLote'.
     */
    public void setDtGravacaoLote(java.lang.String dtGravacaoLote)
    {
        this._dtGravacaoLote = dtGravacaoLote;
    } //-- void setDtGravacaoLote(java.lang.String) 

    /**
     * Sets the value of field 'dtProcessoLote'.
     * 
     * @param dtProcessoLote the value of field 'dtProcessoLote'.
     */
    public void setDtProcessoLote(java.lang.String dtProcessoLote)
    {
        this._dtProcessoLote = dtProcessoLote;
    } //-- void setDtProcessoLote(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoPagamento'.
     * 
     * @param hrInclusaoPagamento the value of field
     * 'hrInclusaoPagamento'.
     */
    public void setHrInclusaoPagamento(java.lang.String hrInclusaoPagamento)
    {
        this._hrInclusaoPagamento = hrInclusaoPagamento;
    } //-- void setHrInclusaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'nrLoteArquivoRemessa'.
     * 
     * @param nrLoteArquivoRemessa the value of field
     * 'nrLoteArquivoRemessa'.
     */
    public void setNrLoteArquivoRemessa(long nrLoteArquivoRemessa)
    {
        this._nrLoteArquivoRemessa = nrLoteArquivoRemessa;
        this._has_nrLoteArquivoRemessa = true;
    } //-- void setNrLoteArquivoRemessa(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'vlTotal'.
     * 
     * @param vlTotal the value of field 'vlTotal'.
     */
    public void setVlTotal(java.math.BigDecimal vlTotal)
    {
        this._vlTotal = vlTotal;
    } //-- void setVlTotal(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoconsistprevia.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoconsistprevia.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoconsistprevia.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolicitacaoconsistprevia.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
