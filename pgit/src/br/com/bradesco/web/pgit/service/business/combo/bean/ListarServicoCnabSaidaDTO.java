/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarservicocnab.response.Ocorrencias;

/**
 * Nome: ListarServicoCnabSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarServicoCnabSaidaDTO {

    /** Atributo cdTipoServicoCnab. */
    private Integer cdTipoServicoCnab;
    
    /** Atributo dsTipoServicoCnab. */
    private String dsTipoServicoCnab;
    
    
	/**
	 * Listar servico cnab saida dto.
	 *
	 * @param saida the saida
	 */
	public ListarServicoCnabSaidaDTO(Ocorrencias saida) {
    	this.cdTipoServicoCnab = saida.getCdTipoServicoCnab();
        this.dsTipoServicoCnab = saida.getDsTipoServicoCnab();
	}

	/**
	 * Get: cdTipoServicoCnab.
	 *
	 * @return cdTipoServicoCnab
	 */
	public Integer getCdTipoServicoCnab() {
		return cdTipoServicoCnab;
	}

	/**
	 * Set: cdTipoServicoCnab.
	 *
	 * @param cdTipoServicoCnab the cd tipo servico cnab
	 */
	public void setCdTipoServicoCnab(Integer cdTipoServicoCnab) {
		this.cdTipoServicoCnab = cdTipoServicoCnab;
	}

	/**
	 * Get: dsTipoServicoCnab.
	 *
	 * @return dsTipoServicoCnab
	 */
	public String getDsTipoServicoCnab() {
		return dsTipoServicoCnab;
	}

	/**
	 * Set: dsTipoServicoCnab.
	 *
	 * @param dsTipoServicoCnab the ds tipo servico cnab
	 */
	public void setDsTipoServicoCnab(String dsTipoServicoCnab) {
		this.dsTipoServicoCnab = dsTipoServicoCnab;
	}

	/**
	 * Listar servico cnab saida dto.
	 *
	 * @param cdTipoServicoCnab the cd tipo servico cnab
	 * @param dsTipoServicoCnab the ds tipo servico cnab
	 */
	public ListarServicoCnabSaidaDTO(Integer cdTipoServicoCnab, String dsTipoServicoCnab) {
		super();
		this.cdTipoServicoCnab = cdTipoServicoCnab;
		this.dsTipoServicoCnab = dsTipoServicoCnab;
	}
    
	
}
