/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaoperadora.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarAgenciaOperadoraResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarAgenciaOperadoraResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPessoaJuridicaDepartamento
     */
    private long _cdPessoaJuridicaDepartamento = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaDepartamento
     */
    private boolean _has_cdPessoaJuridicaDepartamento;

    /**
     * Field _nrSequenciaDepartamento
     */
    private int _nrSequenciaDepartamento = 0;

    /**
     * keeps track of state for field: _nrSequenciaDepartamento
     */
    private boolean _has_nrSequenciaDepartamento;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarAgenciaOperadoraResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaoperadora.response.ConsultarAgenciaOperadoraResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdPessoaJuridicaDepartamento
     * 
     */
    public void deleteCdPessoaJuridicaDepartamento()
    {
        this._has_cdPessoaJuridicaDepartamento= false;
    } //-- void deleteCdPessoaJuridicaDepartamento() 

    /**
     * Method deleteNrSequenciaDepartamento
     * 
     */
    public void deleteNrSequenciaDepartamento()
    {
        this._has_nrSequenciaDepartamento= false;
    } //-- void deleteNrSequenciaDepartamento() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdPessoaJuridicaDepartamento'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaDepartamento'.
     */
    public long getCdPessoaJuridicaDepartamento()
    {
        return this._cdPessoaJuridicaDepartamento;
    } //-- long getCdPessoaJuridicaDepartamento() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrSequenciaDepartamento'.
     * 
     * @return int
     * @return the value of field 'nrSequenciaDepartamento'.
     */
    public int getNrSequenciaDepartamento()
    {
        return this._nrSequenciaDepartamento;
    } //-- int getNrSequenciaDepartamento() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdPessoaJuridicaDepartamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaDepartamento()
    {
        return this._has_cdPessoaJuridicaDepartamento;
    } //-- boolean hasCdPessoaJuridicaDepartamento() 

    /**
     * Method hasNrSequenciaDepartamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaDepartamento()
    {
        return this._has_nrSequenciaDepartamento;
    } //-- boolean hasNrSequenciaDepartamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaDepartamento'.
     * 
     * @param cdPessoaJuridicaDepartamento the value of field
     * 'cdPessoaJuridicaDepartamento'.
     */
    public void setCdPessoaJuridicaDepartamento(long cdPessoaJuridicaDepartamento)
    {
        this._cdPessoaJuridicaDepartamento = cdPessoaJuridicaDepartamento;
        this._has_cdPessoaJuridicaDepartamento = true;
    } //-- void setCdPessoaJuridicaDepartamento(long) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaDepartamento'.
     * 
     * @param nrSequenciaDepartamento the value of field
     * 'nrSequenciaDepartamento'.
     */
    public void setNrSequenciaDepartamento(int nrSequenciaDepartamento)
    {
        this._nrSequenciaDepartamento = nrSequenciaDepartamento;
        this._has_nrSequenciaDepartamento = true;
    } //-- void setNrSequenciaDepartamento(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarAgenciaOperadoraResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaoperadora.response.ConsultarAgenciaOperadoraResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaoperadora.response.ConsultarAgenciaOperadoraResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaoperadora.response.ConsultarAgenciaOperadoraResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaragenciaoperadora.response.ConsultarAgenciaOperadoraResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
