/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean;

/**
 * Nome: IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO {
   
   /** Atributo cdTipoAcesso. */
   private Integer cdTipoAcesso;
   
   /** Atributo hrSolicitacaoPagamento. */
   private String  hrSolicitacaoPagamento;
   
   /** Atributo cdUsuarioInclusao. */
   private String  cdUsuarioInclusao;
   
	/**
	 * Get: cdTipoAcesso.
	 *
	 * @return cdTipoAcesso
	 */
	public Integer getCdTipoAcesso() {
		return cdTipoAcesso;
	}
	
	/**
	 * Set: cdTipoAcesso.
	 *
	 * @param cdTipoAcesso the cd tipo acesso
	 */
	public void setCdTipoAcesso(Integer cdTipoAcesso) {
		this.cdTipoAcesso = cdTipoAcesso;
	}
	
	/**
	 * Get: hrSolicitacaoPagamento.
	 *
	 * @return hrSolicitacaoPagamento
	 */
	public String getHrSolicitacaoPagamento() {
		return hrSolicitacaoPagamento;
	}
	
	/**
	 * Set: hrSolicitacaoPagamento.
	 *
	 * @param hrSolicitacaoPagamento the hr solicitacao pagamento
	 */
	public void setHrSolicitacaoPagamento(String hrSolicitacaoPagamento) {
		this.hrSolicitacaoPagamento = hrSolicitacaoPagamento;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
} 

