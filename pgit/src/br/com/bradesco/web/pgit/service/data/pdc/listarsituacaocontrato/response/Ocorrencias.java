/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarsituacaocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSituacaoContratoNegocio
     */
    private int _cdSituacaoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdSituacaoContratoNegocio
     */
    private boolean _has_cdSituacaoContratoNegocio;

    /**
     * Field _dsSituacaoContratoNegocio
     */
    private java.lang.String _dsSituacaoContratoNegocio;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsituacaocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoContratoNegocio
     * 
     */
    public void deleteCdSituacaoContratoNegocio()
    {
        this._has_cdSituacaoContratoNegocio= false;
    } //-- void deleteCdSituacaoContratoNegocio() 

    /**
     * Returns the value of field 'cdSituacaoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContratoNegocio'.
     */
    public int getCdSituacaoContratoNegocio()
    {
        return this._cdSituacaoContratoNegocio;
    } //-- int getCdSituacaoContratoNegocio() 

    /**
     * Returns the value of field 'dsSituacaoContratoNegocio'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoContratoNegocio'.
     */
    public java.lang.String getDsSituacaoContratoNegocio()
    {
        return this._dsSituacaoContratoNegocio;
    } //-- java.lang.String getDsSituacaoContratoNegocio() 

    /**
     * Method hasCdSituacaoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContratoNegocio()
    {
        return this._has_cdSituacaoContratoNegocio;
    } //-- boolean hasCdSituacaoContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoContratoNegocio'.
     * 
     * @param cdSituacaoContratoNegocio the value of field
     * 'cdSituacaoContratoNegocio'.
     */
    public void setCdSituacaoContratoNegocio(int cdSituacaoContratoNegocio)
    {
        this._cdSituacaoContratoNegocio = cdSituacaoContratoNegocio;
        this._has_cdSituacaoContratoNegocio = true;
    } //-- void setCdSituacaoContratoNegocio(int) 

    /**
     * Sets the value of field 'dsSituacaoContratoNegocio'.
     * 
     * @param dsSituacaoContratoNegocio the value of field
     * 'dsSituacaoContratoNegocio'.
     */
    public void setDsSituacaoContratoNegocio(java.lang.String dsSituacaoContratoNegocio)
    {
        this._dsSituacaoContratoNegocio = dsSituacaoContratoNegocio;
    } //-- void setDsSituacaoContratoNegocio(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarsituacaocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarsituacaocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarsituacaocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsituacaocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
