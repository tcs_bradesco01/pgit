/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCorpoCpfCnpjParticipante
     */
    private long _cdCorpoCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdCorpoCpfCnpjParticipante
     */
    private boolean _has_cdCorpoCpfCnpjParticipante;
    
    /**
     * Field _cPssoa
     */
    private long _cPssoa = 0;
    
    /**
     * keeps track of state for field: _cPssoa
     */
    private boolean _has_cPssoa;

    /**
     * Field _cdFilialCpfCnpjParticipante
     */
    private int _cdFilialCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjParticipante
     */
    private boolean _has_cdFilialCpfCnpjParticipante;

    /**
     * Field _cdDigitoCpfCnpjParticipante
     */
    private int _cdDigitoCpfCnpjParticipante = 0;

    /**
     * keeps track of state for field: _cdDigitoCpfCnpjParticipante
     */
    private boolean _has_cdDigitoCpfCnpjParticipante;

    /**
     * Field _cdBancoRelacionamento
     */
    private int _cdBancoRelacionamento = 0;

    /**
     * keeps track of state for field: _cdBancoRelacionamento
     */
    private boolean _has_cdBancoRelacionamento;

    /**
     * Field _dsBancoRelacionamento
     */
    private java.lang.String _dsBancoRelacionamento;

    /**
     * Field _cdAgenciaRelacionamento
     */
    private int _cdAgenciaRelacionamento = 0;

    /**
     * keeps track of state for field: _cdAgenciaRelacionamento
     */
    private boolean _has_cdAgenciaRelacionamento;

    /**
     * Field _dsAgenciaRelacionamento
     */
    private java.lang.String _dsAgenciaRelacionamento;

    /**
     * Field _cdContaRelacionamento
     */
    private long _cdContaRelacionamento = 0;

    /**
     * keeps track of state for field: _cdContaRelacionamento
     */
    private boolean _has_cdContaRelacionamento;

    /**
     * Field _cdDigitoContaRelacionamento
     */
    private java.lang.String _cdDigitoContaRelacionamento;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _dsTipoConta
     */
    private java.lang.String _dsTipoConta;

    /**
     * Field _hrInclusaoManutencao
     */
    private java.lang.String _hrInclusaoManutencao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _nmParticipante
     */
    private java.lang.String _nmParticipante;

    /**
     * Field _dsTipoManutencao
     */
    private java.lang.String _dsTipoManutencao;

    /**
     * Field _cdTipoRelacionamento
     */
    private int _cdTipoRelacionamento = 0;

    /**
     * keeps track of state for field: _cdTipoRelacionamento
     */
    private boolean _has_cdTipoRelacionamento;

    /**
     * Field _dsTipoRelacionamento
     */
    private java.lang.String _dsTipoRelacionamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaRelacionamento
     * 
     */
    public void deleteCdAgenciaRelacionamento()
    {
        this._has_cdAgenciaRelacionamento= false;
    } //-- void deleteCdAgenciaRelacionamento() 

    /**
     * Method deleteCdBancoRelacionamento
     * 
     */
    public void deleteCdBancoRelacionamento()
    {
        this._has_cdBancoRelacionamento= false;
    } //-- void deleteCdBancoRelacionamento() 

    /**
     * Method deleteCdContaRelacionamento
     * 
     */
    public void deleteCdContaRelacionamento()
    {
        this._has_cdContaRelacionamento= false;
    } //-- void deleteCdContaRelacionamento() 

    /**
     * Method deleteCdCorpoCpfCnpjParticipante
     * 
     */
    public void deleteCdCorpoCpfCnpjParticipante()
    {
        this._has_cdCorpoCpfCnpjParticipante= false;
    } //-- void deleteCdCorpoCpfCnpjParticipante() 
    
    /**
     * Method deleteCPssoa
     * 
     */
    public void deleteCPssoa()
    {
        this._has_cPssoa= false;
    } //-- void deleteCPssoa()

    /**
     * Method deleteCdDigitoCpfCnpjParticipante
     * 
     */
    public void deleteCdDigitoCpfCnpjParticipante()
    {
        this._has_cdDigitoCpfCnpjParticipante= false;
    } //-- void deleteCdDigitoCpfCnpjParticipante() 

    /**
     * Method deleteCdFilialCpfCnpjParticipante
     * 
     */
    public void deleteCdFilialCpfCnpjParticipante()
    {
        this._has_cdFilialCpfCnpjParticipante= false;
    } //-- void deleteCdFilialCpfCnpjParticipante() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteCdTipoRelacionamento
     * 
     */
    public void deleteCdTipoRelacionamento()
    {
        this._has_cdTipoRelacionamento= false;
    } //-- void deleteCdTipoRelacionamento() 

    /**
     * Returns the value of field 'cdAgenciaRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaRelacionamento'.
     */
    public int getCdAgenciaRelacionamento()
    {
        return this._cdAgenciaRelacionamento;
    } //-- int getCdAgenciaRelacionamento() 

    /**
     * Returns the value of field 'cdBancoRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdBancoRelacionamento'.
     */
    public int getCdBancoRelacionamento()
    {
        return this._cdBancoRelacionamento;
    } //-- int getCdBancoRelacionamento() 

    /**
     * Returns the value of field 'cdContaRelacionamento'.
     * 
     * @return long
     * @return the value of field 'cdContaRelacionamento'.
     */
    public long getCdContaRelacionamento()
    {
        return this._cdContaRelacionamento;
    } //-- long getCdContaRelacionamento() 

    /**
     * Returns the value of field 'cdCorpoCpfCnpjParticipante'.
     * 
     * @return long
     * @return the value of field 'cdCorpoCpfCnpjParticipante'.
     */
    public long getCdCorpoCpfCnpjParticipante()
    {
        return this._cdCorpoCpfCnpjParticipante;
    } //-- long getCdCorpoCpfCnpjParticipante() 
    
    /**
     * Returns the value of field 'cdPssoa'.
     * 
     * @return long
     * @return the value of field 'cdPssoa'.
     */
    public long getCPssoa()
    {
        return this._cPssoa;
    } //-- long getCPssoa()

    /**
     * Returns the value of field 'cdDigitoContaRelacionamento'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaRelacionamento'.
     */
    public java.lang.String getCdDigitoContaRelacionamento()
    {
        return this._cdDigitoContaRelacionamento;
    } //-- java.lang.String getCdDigitoContaRelacionamento() 

    /**
     * Returns the value of field 'cdDigitoCpfCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdDigitoCpfCnpjParticipante'.
     */
    public int getCdDigitoCpfCnpjParticipante()
    {
        return this._cdDigitoCpfCnpjParticipante;
    } //-- int getCdDigitoCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjParticipante'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjParticipante'.
     */
    public int getCdFilialCpfCnpjParticipante()
    {
        return this._cdFilialCpfCnpjParticipante;
    } //-- int getCdFilialCpfCnpjParticipante() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'cdTipoRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelacionamento'.
     */
    public int getCdTipoRelacionamento()
    {
        return this._cdTipoRelacionamento;
    } //-- int getCdTipoRelacionamento() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'dsAgenciaRelacionamento'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaRelacionamento'.
     */
    public java.lang.String getDsAgenciaRelacionamento()
    {
        return this._dsAgenciaRelacionamento;
    } //-- java.lang.String getDsAgenciaRelacionamento() 

    /**
     * Returns the value of field 'dsBancoRelacionamento'.
     * 
     * @return String
     * @return the value of field 'dsBancoRelacionamento'.
     */
    public java.lang.String getDsBancoRelacionamento()
    {
        return this._dsBancoRelacionamento;
    } //-- java.lang.String getDsBancoRelacionamento() 

    /**
     * Returns the value of field 'dsTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsTipoConta'.
     */
    public java.lang.String getDsTipoConta()
    {
        return this._dsTipoConta;
    } //-- java.lang.String getDsTipoConta() 

    /**
     * Returns the value of field 'dsTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoManutencao'.
     */
    public java.lang.String getDsTipoManutencao()
    {
        return this._dsTipoManutencao;
    } //-- java.lang.String getDsTipoManutencao() 

    /**
     * Returns the value of field 'dsTipoRelacionamento'.
     * 
     * @return String
     * @return the value of field 'dsTipoRelacionamento'.
     */
    public java.lang.String getDsTipoRelacionamento()
    {
        return this._dsTipoRelacionamento;
    } //-- java.lang.String getDsTipoRelacionamento() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'hrInclusaoManutencao'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoManutencao'.
     */
    public java.lang.String getHrInclusaoManutencao()
    {
        return this._hrInclusaoManutencao;
    } //-- java.lang.String getHrInclusaoManutencao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'nmParticipante'.
     * 
     * @return String
     * @return the value of field 'nmParticipante'.
     */
    public java.lang.String getNmParticipante()
    {
        return this._nmParticipante;
    } //-- java.lang.String getNmParticipante() 

    /**
     * Method hasCdAgenciaRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaRelacionamento()
    {
        return this._has_cdAgenciaRelacionamento;
    } //-- boolean hasCdAgenciaRelacionamento() 

    /**
     * Method hasCdBancoRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoRelacionamento()
    {
        return this._has_cdBancoRelacionamento;
    } //-- boolean hasCdBancoRelacionamento() 

    /**
     * Method hasCdContaRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaRelacionamento()
    {
        return this._has_cdContaRelacionamento;
    } //-- boolean hasCdContaRelacionamento() 

    /**
     * Method hasCdCorpoCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCorpoCpfCnpjParticipante()
    {
        return this._has_cdCorpoCpfCnpjParticipante;
    } //-- boolean hasCdCorpoCpfCnpjParticipante() 
    
    /**
     * Method hasCPssoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCPssoa()
    {
        return this._has_cPssoa;
    } //-- boolean hasCPssoa()

    /**
     * Method hasCdDigitoCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoCpfCnpjParticipante()
    {
        return this._has_cdDigitoCpfCnpjParticipante;
    } //-- boolean hasCdDigitoCpfCnpjParticipante() 

    /**
     * Method hasCdFilialCpfCnpjParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjParticipante()
    {
        return this._has_cdFilialCpfCnpjParticipante;
    } //-- boolean hasCdFilialCpfCnpjParticipante() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasCdTipoRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelacionamento()
    {
        return this._has_cdTipoRelacionamento;
    } //-- boolean hasCdTipoRelacionamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaRelacionamento'.
     * 
     * @param cdAgenciaRelacionamento the value of field
     * 'cdAgenciaRelacionamento'.
     */
    public void setCdAgenciaRelacionamento(int cdAgenciaRelacionamento)
    {
        this._cdAgenciaRelacionamento = cdAgenciaRelacionamento;
        this._has_cdAgenciaRelacionamento = true;
    } //-- void setCdAgenciaRelacionamento(int) 

    /**
     * Sets the value of field 'cdBancoRelacionamento'.
     * 
     * @param cdBancoRelacionamento the value of field
     * 'cdBancoRelacionamento'.
     */
    public void setCdBancoRelacionamento(int cdBancoRelacionamento)
    {
        this._cdBancoRelacionamento = cdBancoRelacionamento;
        this._has_cdBancoRelacionamento = true;
    } //-- void setCdBancoRelacionamento(int) 

    /**
     * Sets the value of field 'cdContaRelacionamento'.
     * 
     * @param cdContaRelacionamento the value of field
     * 'cdContaRelacionamento'.
     */
    public void setCdContaRelacionamento(long cdContaRelacionamento)
    {
        this._cdContaRelacionamento = cdContaRelacionamento;
        this._has_cdContaRelacionamento = true;
    } //-- void setCdContaRelacionamento(long) 

    /**
     * Sets the value of field 'cdCorpoCpfCnpjParticipante'.
     * 
     * @param cdCorpoCpfCnpjParticipante the value of field
     * 'cdCorpoCpfCnpjParticipante'.
     */
    public void setCdCorpoCpfCnpjParticipante(long cdCorpoCpfCnpjParticipante)
    {
        this._cdCorpoCpfCnpjParticipante = cdCorpoCpfCnpjParticipante;
        this._has_cdCorpoCpfCnpjParticipante = true;
    } //-- void setCdCorpoCpfCnpjParticipante(long) 
    
    /**
     * Sets the value of field 'cPssoa'.
     * 
     * @param cCpssoa the value of field
     * 'cPssoa'.
     */
    public void setCPssoa(long cPssoa)
    {
        this._cPssoa = cPssoa;
        this._has_cPssoa = true;
    } //-- void setCPssoa(long)

    /**
     * Sets the value of field 'cdDigitoContaRelacionamento'.
     * 
     * @param cdDigitoContaRelacionamento the value of field
     * 'cdDigitoContaRelacionamento'.
     */
    public void setCdDigitoContaRelacionamento(java.lang.String cdDigitoContaRelacionamento)
    {
        this._cdDigitoContaRelacionamento = cdDigitoContaRelacionamento;
    } //-- void setCdDigitoContaRelacionamento(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoCpfCnpjParticipante'.
     * 
     * @param cdDigitoCpfCnpjParticipante the value of field
     * 'cdDigitoCpfCnpjParticipante'.
     */
    public void setCdDigitoCpfCnpjParticipante(int cdDigitoCpfCnpjParticipante)
    {
        this._cdDigitoCpfCnpjParticipante = cdDigitoCpfCnpjParticipante;
        this._has_cdDigitoCpfCnpjParticipante = true;
    } //-- void setCdDigitoCpfCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjParticipante'.
     * 
     * @param cdFilialCpfCnpjParticipante the value of field
     * 'cdFilialCpfCnpjParticipante'.
     */
    public void setCdFilialCpfCnpjParticipante(int cdFilialCpfCnpjParticipante)
    {
        this._cdFilialCpfCnpjParticipante = cdFilialCpfCnpjParticipante;
        this._has_cdFilialCpfCnpjParticipante = true;
    } //-- void setCdFilialCpfCnpjParticipante(int) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'cdTipoRelacionamento'.
     * 
     * @param cdTipoRelacionamento the value of field
     * 'cdTipoRelacionamento'.
     */
    public void setCdTipoRelacionamento(int cdTipoRelacionamento)
    {
        this._cdTipoRelacionamento = cdTipoRelacionamento;
        this._has_cdTipoRelacionamento = true;
    } //-- void setCdTipoRelacionamento(int) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaRelacionamento'.
     * 
     * @param dsAgenciaRelacionamento the value of field
     * 'dsAgenciaRelacionamento'.
     */
    public void setDsAgenciaRelacionamento(java.lang.String dsAgenciaRelacionamento)
    {
        this._dsAgenciaRelacionamento = dsAgenciaRelacionamento;
    } //-- void setDsAgenciaRelacionamento(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoRelacionamento'.
     * 
     * @param dsBancoRelacionamento the value of field
     * 'dsBancoRelacionamento'.
     */
    public void setDsBancoRelacionamento(java.lang.String dsBancoRelacionamento)
    {
        this._dsBancoRelacionamento = dsBancoRelacionamento;
    } //-- void setDsBancoRelacionamento(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConta'.
     * 
     * @param dsTipoConta the value of field 'dsTipoConta'.
     */
    public void setDsTipoConta(java.lang.String dsTipoConta)
    {
        this._dsTipoConta = dsTipoConta;
    } //-- void setDsTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoManutencao'.
     * 
     * @param dsTipoManutencao the value of field 'dsTipoManutencao'
     */
    public void setDsTipoManutencao(java.lang.String dsTipoManutencao)
    {
        this._dsTipoManutencao = dsTipoManutencao;
    } //-- void setDsTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoRelacionamento'.
     * 
     * @param dsTipoRelacionamento the value of field
     * 'dsTipoRelacionamento'.
     */
    public void setDsTipoRelacionamento(java.lang.String dsTipoRelacionamento)
    {
        this._dsTipoRelacionamento = dsTipoRelacionamento;
    } //-- void setDsTipoRelacionamento(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoManutencao'.
     * 
     * @param hrInclusaoManutencao the value of field
     * 'hrInclusaoManutencao'.
     */
    public void setHrInclusaoManutencao(java.lang.String hrInclusaoManutencao)
    {
        this._hrInclusaoManutencao = hrInclusaoManutencao;
    } //-- void setHrInclusaoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nmParticipante'.
     * 
     * @param nmParticipante the value of field 'nmParticipante'.
     */
    public void setNmParticipante(java.lang.String nmParticipante)
    {
        this._nmParticipante = nmParticipante;
    } //-- void setNmParticipante(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadashist.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
