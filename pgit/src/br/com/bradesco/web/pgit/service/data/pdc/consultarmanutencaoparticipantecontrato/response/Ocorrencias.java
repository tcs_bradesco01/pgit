/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipantecontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCpfCnpj
     */
    private java.lang.String _cdCpfCnpj;

    /**
     * Field _nomeParticipante
     */
    private java.lang.String _nomeParticipante;

    /**
     * Field _PGICW275DATAMANUT
     */
    private java.lang.String _PGICW275DATAMANUT;

    /**
     * Field _PGICW275HORAMANUT
     */
    private java.lang.String _PGICW275HORAMANUT;

    /**
     * Field _PGICW275USUARIOMANUT
     */
    private java.lang.String _PGICW275USUARIOMANUT;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipantecontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpj'.
     */
    public java.lang.String getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- java.lang.String getCdCpfCnpj() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'nomeParticipante'.
     * 
     * @return String
     * @return the value of field 'nomeParticipante'.
     */
    public java.lang.String getNomeParticipante()
    {
        return this._nomeParticipante;
    } //-- java.lang.String getNomeParticipante() 

    /**
     * Returns the value of field 'PGICW275DATAMANUT'.
     * 
     * @return String
     * @return the value of field 'PGICW275DATAMANUT'.
     */
    public java.lang.String getPGICW275DATAMANUT()
    {
        return this._PGICW275DATAMANUT;
    } //-- java.lang.String getPGICW275DATAMANUT() 

    /**
     * Returns the value of field 'PGICW275HORAMANUT'.
     * 
     * @return String
     * @return the value of field 'PGICW275HORAMANUT'.
     */
    public java.lang.String getPGICW275HORAMANUT()
    {
        return this._PGICW275HORAMANUT;
    } //-- java.lang.String getPGICW275HORAMANUT() 

    /**
     * Returns the value of field 'PGICW275USUARIOMANUT'.
     * 
     * @return String
     * @return the value of field 'PGICW275USUARIOMANUT'.
     */
    public java.lang.String getPGICW275USUARIOMANUT()
    {
        return this._PGICW275USUARIOMANUT;
    } //-- java.lang.String getPGICW275USUARIOMANUT() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(java.lang.String cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
    } //-- void setCdCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nomeParticipante'.
     * 
     * @param nomeParticipante the value of field 'nomeParticipante'
     */
    public void setNomeParticipante(java.lang.String nomeParticipante)
    {
        this._nomeParticipante = nomeParticipante;
    } //-- void setNomeParticipante(java.lang.String) 

    /**
     * Sets the value of field 'PGICW275DATAMANUT'.
     * 
     * @param PGICW275DATAMANUT the value of field
     * 'PGICW275DATAMANUT'.
     */
    public void setPGICW275DATAMANUT(java.lang.String PGICW275DATAMANUT)
    {
        this._PGICW275DATAMANUT = PGICW275DATAMANUT;
    } //-- void setPGICW275DATAMANUT(java.lang.String) 

    /**
     * Sets the value of field 'PGICW275HORAMANUT'.
     * 
     * @param PGICW275HORAMANUT the value of field
     * 'PGICW275HORAMANUT'.
     */
    public void setPGICW275HORAMANUT(java.lang.String PGICW275HORAMANUT)
    {
        this._PGICW275HORAMANUT = PGICW275HORAMANUT;
    } //-- void setPGICW275HORAMANUT(java.lang.String) 

    /**
     * Sets the value of field 'PGICW275USUARIOMANUT'.
     * 
     * @param PGICW275USUARIOMANUT the value of field
     * 'PGICW275USUARIOMANUT'.
     */
    public void setPGICW275USUARIOMANUT(java.lang.String PGICW275USUARIOMANUT)
    {
        this._PGICW275USUARIOMANUT = PGICW275USUARIOMANUT;
    } //-- void setPGICW275USUARIOMANUT(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipantecontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipantecontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipantecontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaoparticipantecontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
