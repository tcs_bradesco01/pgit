/**
 * Nome: br.com.bradesco.web.pgit.service.business.contacomplementar.impl
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */ 
package br.com.bradesco.web.pgit.service.business.contacomplementar.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.contacomplementar.IContaComplementarService;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.AlterarContaComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.DetalharContaComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.DetalharContaComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.IncluirContaComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.IncluirContaComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplementarEntrada;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplementarOcorrenciasSaida;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplementarSaida;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarcontacomplementar.request.AlterarContaComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarcontacomplementar.response.AlterarContaComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantescontrato.request.ConsultarParticipantesContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantescontrato.response.ConsultarParticipantesContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontacomplementar.request.DetalharContaComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontacomplementar.response.DetalharContaComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluircontacomplementar.request.ExcluirContaComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluircontacomplementar.response.ExcluirContaComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontacomplementar.request.IncluirContaComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluircontacomplementar.response.IncluirContaComplementarResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontacomplementar.request.ListarContaComplementarRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarcontacomplementar.response.ListarContaComplementarResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ContaComplementarServiceImpl
 * <p>
 * Prop�sito: Implementa��o do adaptador ContaComplementar
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IContaComplementarService
 */
public class ContaComplementarServiceImpl implements IContaComplementarService {

    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;
    
    public ContaComplementarServiceImpl() {
    	
    }
    public ListarContaComplementarSaida listarContasComplementar(ListarContaComplementarEntrada entrada) {
    	
    	ListarContaComplementarRequest request = new ListarContaComplementarRequest();
    	
        request.setCdPessoaJuridicaContrato(entrada.getCdpessoaJuridicaContrato());
        request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
        request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());
    	
        request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdAgencia()));
        request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
        request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
        request.setCdDigitoConta(PgitUtil.verificaStringNula(entrada.getCdDigitoConta()));
        
        request.setCdCorpoCnpjCpf(PgitUtil.verificaLongNulo(entrada.getCdCorpoCnpjCpf()));	
        request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjCpf()));
        request.setCdControleCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpjCpf()));
        request.setMaxOcorrencias(50);
        
        ListarContaComplementarResponse response =
            getFactoryAdapter().getListarContaComplementarPDCAdapter().invokeProcess(request);
    	
        ListarContaComplementarSaida registro = new ListarContaComplementarSaida();
        registro.setCodMensagem(response.getCodMensagem());
        registro.setMensagem(response.getMensagem());
        registro.setNrLinhas(response.getNrLinhas());
        
        List<ListarContaComplementarOcorrenciasSaida> listaOcorrencias = new ArrayList<ListarContaComplementarOcorrenciasSaida>();
        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
        	br.com.bradesco.web.pgit.service.data.pdc.listarcontacomplementar.response.Ocorrencias ocorrResponse = response.getOcorrencias(i);
        	
        	ListarContaComplementarOcorrenciasSaida ocorrencia = new ListarContaComplementarOcorrenciasSaida();
        	ocorrencia.setCdContaComplementar(ocorrResponse.getCdContaComplementar());
        	
        	ocorrencia.setCdCorpoCnpjCpf(ocorrResponse.getCdCorpoCnpjCpf());
        	ocorrencia.setCdFilialCpfCnpj(ocorrResponse.getCdFilialCpfCnpj());
        	ocorrencia.setCdCtrlCpfCnpj(ocorrResponse.getCdCtrlCpfCnpj());
        	
        	ocorrencia.setDsNomeRazaoSocial(ocorrResponse.getDsNomeRazaoSocial());
        	ocorrencia.setCdPessoaJuridicaContratoConta(ocorrResponse.getCdPessoaJuridicaContratoConta());
        	ocorrencia.setCdTipoContratoConta(ocorrResponse.getCdTipoContratoConta());
        	ocorrencia.setNrSequenciaContratoConta(ocorrResponse.getNrSequenciaContratoConta());
        	
        	ocorrencia.setCdBanco(ocorrResponse.getCdBanco());
        	ocorrencia.setDsBanco(ocorrResponse.getDsBanco());
        	ocorrencia.setCdAgencia(ocorrResponse.getCdAgencia());
        	ocorrencia.setDsAgencia(ocorrResponse.getDsAgencia());
        	ocorrencia.setCdConta(ocorrResponse.getCdConta());
        	ocorrencia.setCdDigitoConta(ocorrResponse.getCdDigitoConta());
        	
        	listaOcorrencias.add(ocorrencia);
        }
        
        registro.setOcorrencias(listaOcorrencias);
        return registro;
    }
    
	public List<ListarContaComplParticipantesSaidaDTO> listaParticipantes(ListarContaComplParticipantesEntradaDTO entrada) {
		
		ConsultarParticipantesContratoRequest request = new ConsultarParticipantesContratoRequest();
		
		request.setMaxOcorrencias(20);
		
        request.setCdpessoaJuridicaContrato(entrada.getCdpessoaJuridicaContrato());
        request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
        request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());		
		
        request.setCdCorpoCpfCnpj(PgitUtil.verificaLongNulo(entrada.getCdCorpoCpfCnpj()));	
        request.setCdFilialCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCpfCnpj()));
        request.setCdControleCpfCnpj(PgitUtil.verificaIntegerNulo(entrada.getCdControleCpfCnpj()));
        
        ConsultarParticipantesContratoResponse response = 
        	getFactoryAdapter().getConsultarParticipantesContratoPDCAdapter().invokeProcess(request);
		
        ArrayList<ListarContaComplParticipantesSaidaDTO> listaOcorrencia = new ArrayList<ListarContaComplParticipantesSaidaDTO>();
        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
        	
        	br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantescontrato.response.Ocorrencias ocorrResponse = response.getOcorrencias(i);
        	
        	ListarContaComplParticipantesSaidaDTO ocorrencia = new ListarContaComplParticipantesSaidaDTO();
        	
        	ocorrencia.setCdCorpoCpfCnpj(ocorrResponse.getCdCorpoCpfCnpj());
        	ocorrencia.setCdFilialCpfCnpj(ocorrResponse.getCdFilialCnpj());
        	ocorrencia.setCdControleCpfCnpj(ocorrResponse.getCdControleCpfCnpj());
        	
        	ocorrencia.setCdAtividadeEconomica(ocorrResponse.getCdAtividadeEconomica());
        	ocorrencia.setCdClassificacaoParticipacao(ocorrResponse.getCdClassificacaoParticipacao());
        	
        	ocorrencia.setCdGrupoEconomico(ocorrResponse.getCdGrupoEconomico());
        	ocorrencia.setCdMotivoParticipante(ocorrResponse.getCdMotivoParticipante());
        	
        	ocorrencia.setCdPessoaJuridicaContrato(ocorrResponse.getCdPessoaJuridicaContrato());
        	ocorrencia.setCdSegmentoEconomico(ocorrResponse.getCdSegmentoEconomico());
        	
        	ocorrencia.setCdSituacaoParticipante(ocorrResponse.getCdSituacaoParticipante());
        	ocorrencia.setCdSubSegmento(ocorrResponse.getCdSubSegmento());
        	
        	ocorrencia.setCdTipoParticipante(ocorrResponse.getCdTipoParticipante());
        	ocorrencia.setDsAtividadeEconomica(ocorrResponse.getDsAtividadeEconomica());
        	
        	ocorrencia.setDsClassificacaoParticipacao(ocorrResponse.getDsClassificacaoParticipacao());
        	ocorrencia.setDsGrupoEconomico(ocorrResponse.getDsGrupoEconomico());
        	
        	ocorrencia.setDsMotivoParticipante(ocorrResponse.getDsMotivoParticipante());
        	ocorrencia.setDsNomeRazaoSocial(ocorrResponse.getDsNomeRazaoSocial());
        	
        	ocorrencia.setDsSegmentoEconomico(ocorrResponse.getDsSegmentoEconomico());
        	ocorrencia.setDsSituacaoParticipante(ocorrResponse.getDsSituacaoParticipante());
        	
        	ocorrencia.setDsSubSegmentoCliente(ocorrResponse.getDsSubSegmentoCliente());
        	ocorrencia.setDsTipoParticipante(ocorrResponse.getDsTipoParticipante());
        	
        	listaOcorrencia.add(ocorrencia);
        }
        return listaOcorrencia;
	}
	
	public IncluirContaComplementarSaidaDTO incluirContaComplementar(IncluirContaComplementarEntradaDTO entrada) {

		IncluirContaComplementarRequest request = new IncluirContaComplementarRequest();
		
        request.setCdpessoaJuridicaContrato(entrada.getCdpessoaJuridicaContrato());
        request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
        request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());
        
        request.setCdContaComplementar(PgitUtil.verificaIntegerNulo(entrada.getCdContaComplementar()));
		
        request.setCdCorpoCnpjCpf(PgitUtil.verificaLongNulo(entrada.getCdCorpoCnpjCpf()));	
        request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjCpf()));
        request.setCdControleCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpjCpf()));
        
        request.setCdpessoaJuridicaContratoConta(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContratoConta()));
        request.setCdTipoContratoConta(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoConta()));
        request.setNrSequenciaContratoConta(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoConta()));
        
        IncluirContaComplementarSaidaDTO saida = new IncluirContaComplementarSaidaDTO();
		
        IncluirContaComplementarResponse response = getFactoryAdapter().
        	getIncluirContaComplementarPDCAdapter().invokeProcess(request);
        
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        
		return saida;
	}
	
	public AlterarContaComplementarSaidaDTO alterarContaComplementar(IncluirContaComplementarEntradaDTO entrada) {

		AlterarContaComplementarRequest request = new AlterarContaComplementarRequest();
		
        request.setCdpessoaJuridicaContrato(entrada.getCdpessoaJuridicaContrato());
        request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
        request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());
        
        request.setCdContaComplementar(PgitUtil.verificaIntegerNulo(entrada.getCdContaComplementar()));
		
        request.setCdCorpoCnpjCpf(PgitUtil.verificaLongNulo(entrada.getCdCorpoCnpjCpf()));	
        request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjCpf()));
        request.setCdControleCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpjCpf()));
        
        request.setCdpessoaJuridicaContratoConta(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContratoConta()));
        request.setCdTipoContratoConta(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoConta()));
        request.setNrSequenciaContratoConta(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoConta()));
        
        AlterarContaComplementarSaidaDTO saida = new AlterarContaComplementarSaidaDTO();
		
        AlterarContaComplementarResponse response = getFactoryAdapter().
        	getAlterarContaComplementarPDCAdapter().invokeProcess(request);
        
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        
		return saida;
	}	
	
	
	public DetalharContaComplementarSaidaDTO detalharContaComplementar(DetalharContaComplementarEntradaDTO entrada) {
		
		DetalharContaComplementarRequest request = new DetalharContaComplementarRequest();
		
		request.setCdpessoaJuridicaContrato(entrada.getCdpessoaJuridicaContrato());
		request.setCdContaComplementar(entrada.getCdContaComplementar());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		
		request.setCdCorpoCnpjCpf(entrada.getCdCorpoCnpjCpf());
		request.setCdFilialCnpjCpf(entrada.getCdFilialCnpjCpf());
		request.setCdControleCnpjCpf(entrada.getCdControleCnpjCpf());
		
		request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());
		
		DetalharContaComplementarResponse response = getFactoryAdapter().
			getDetalharContaComplementarPDCAdapter().invokeProcess(request);
		
		DetalharContaComplementarSaidaDTO saida = new DetalharContaComplementarSaidaDTO();
		
		saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saida.setCdTipoContratoConta(response.getCdTipoContratoConta());
		saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saida.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		
		saida.setCdAgencia(response.getCdAgencia());
		saida.setCdBanco(response.getCdBanco());
		saida.setCdConta(response.getCdConta());
		saida.setCdContaComplementar(response.getCdContaComplementar());
		
		saida.setCdCorpoCnpjCpf(response.getCdCorpoCnpjCpf());
		saida.setCdDigitoConta(response.getCdDigitoConta());
		saida.setCdCtrlCpfCnpj(response.getCdCtrlCpfCnpj());
		saida.setCdFilialCpfCnpj(response.getCdFilialCpfCnpj());
		
		saida.setCdpessoaJuridicaContrato(response.getCdPessoaJuridicaContrato());
		saida.setCdPessoaJuridicaContratoConta(response.getCdPessoaJuridicaContratoConta());
		
		saida.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saida.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		
		saida.setDsAgencia(response.getDsAgencia());
		saida.setDsBanco(response.getDsBanco());
		saida.setDsNomeParticipante(response.getDsNomeParticipante());
		saida.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
		saida.setHrManutencaoRegistro(response.getHrManutencaoRegistro());
		saida.setNrSequenciaContratoConta(response.getNrSequenciaContratoConta());
		saida.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
		
		return saida;
	}
	
	public AlterarContaComplementarSaidaDTO excluirContaComplementar(IncluirContaComplementarEntradaDTO entrada) {

		ExcluirContaComplementarRequest request = new ExcluirContaComplementarRequest();
		
		request.setCdContaComplementar(PgitUtil.verificaIntegerNulo(entrada.getCdContaComplementar()));
		
        request.setCdCorpoCnpjCpf(PgitUtil.verificaLongNulo(entrada.getCdCorpoCnpjCpf()));	
        request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdFilialCnpjCpf()));
        request.setCdControleCnpjCpf(PgitUtil.verificaIntegerNulo(entrada.getCdControleCnpjCpf()));
		
        request.setCdpessoaJuridicaContrato(entrada.getCdpessoaJuridicaContrato());
        request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
        request.setNrSequenciaContratoNegocio(entrada.getNrSequenciaContratoNegocio());
        
        AlterarContaComplementarSaidaDTO saida = new AlterarContaComplementarSaidaDTO();
		
        ExcluirContaComplementarResponse response = getFactoryAdapter().
        	getExcluirContaComplementarPDCAdapter().invokeProcess(request);
        
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        
		return saida;
	}	
	
	
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	public FactoryAdapter getFactoryAdapter() {
        return factoryAdapter;
    }
}