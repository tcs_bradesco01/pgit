/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarsegmentoclientes.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSegmento
     */
    private int _cdSegmento = 0;

    /**
     * keeps track of state for field: _cdSegmento
     */
    private boolean _has_cdSegmento;

    /**
     * Field _dsSegmento
     */
    private java.lang.String _dsSegmento;

    /**
     * Field _cdSituacaoSegmento
     */
    private int _cdSituacaoSegmento = 0;

    /**
     * keeps track of state for field: _cdSituacaoSegmento
     */
    private boolean _has_cdSituacaoSegmento;

    /**
     * Field _dsSituacaoSegmento
     */
    private java.lang.String _dsSituacaoSegmento;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsegmentoclientes.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSegmento
     * 
     */
    public void deleteCdSegmento()
    {
        this._has_cdSegmento= false;
    } //-- void deleteCdSegmento() 

    /**
     * Method deleteCdSituacaoSegmento
     * 
     */
    public void deleteCdSituacaoSegmento()
    {
        this._has_cdSituacaoSegmento= false;
    } //-- void deleteCdSituacaoSegmento() 

    /**
     * Returns the value of field 'cdSegmento'.
     * 
     * @return int
     * @return the value of field 'cdSegmento'.
     */
    public int getCdSegmento()
    {
        return this._cdSegmento;
    } //-- int getCdSegmento() 

    /**
     * Returns the value of field 'cdSituacaoSegmento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSegmento'.
     */
    public int getCdSituacaoSegmento()
    {
        return this._cdSituacaoSegmento;
    } //-- int getCdSituacaoSegmento() 

    /**
     * Returns the value of field 'dsSegmento'.
     * 
     * @return String
     * @return the value of field 'dsSegmento'.
     */
    public java.lang.String getDsSegmento()
    {
        return this._dsSegmento;
    } //-- java.lang.String getDsSegmento() 

    /**
     * Returns the value of field 'dsSituacaoSegmento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoSegmento'.
     */
    public java.lang.String getDsSituacaoSegmento()
    {
        return this._dsSituacaoSegmento;
    } //-- java.lang.String getDsSituacaoSegmento() 

    /**
     * Method hasCdSegmento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmento()
    {
        return this._has_cdSegmento;
    } //-- boolean hasCdSegmento() 

    /**
     * Method hasCdSituacaoSegmento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSegmento()
    {
        return this._has_cdSituacaoSegmento;
    } //-- boolean hasCdSituacaoSegmento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSegmento'.
     * 
     * @param cdSegmento the value of field 'cdSegmento'.
     */
    public void setCdSegmento(int cdSegmento)
    {
        this._cdSegmento = cdSegmento;
        this._has_cdSegmento = true;
    } //-- void setCdSegmento(int) 

    /**
     * Sets the value of field 'cdSituacaoSegmento'.
     * 
     * @param cdSituacaoSegmento the value of field
     * 'cdSituacaoSegmento'.
     */
    public void setCdSituacaoSegmento(int cdSituacaoSegmento)
    {
        this._cdSituacaoSegmento = cdSituacaoSegmento;
        this._has_cdSituacaoSegmento = true;
    } //-- void setCdSituacaoSegmento(int) 

    /**
     * Sets the value of field 'dsSegmento'.
     * 
     * @param dsSegmento the value of field 'dsSegmento'.
     */
    public void setDsSegmento(java.lang.String dsSegmento)
    {
        this._dsSegmento = dsSegmento;
    } //-- void setDsSegmento(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoSegmento'.
     * 
     * @param dsSituacaoSegmento the value of field
     * 'dsSituacaoSegmento'.
     */
    public void setDsSituacaoSegmento(java.lang.String dsSituacaoSegmento)
    {
        this._dsSituacaoSegmento = dsSituacaoSegmento;
    } //-- void setDsSituacaoSegmento(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarsegmentoclientes.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarsegmentoclientes.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarsegmentoclientes.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarsegmentoclientes.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
