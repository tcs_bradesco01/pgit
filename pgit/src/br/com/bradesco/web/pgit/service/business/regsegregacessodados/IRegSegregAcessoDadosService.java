/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.regsegregacessodados;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarRegraSegregacaoSaidaDTO;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterRegraSegregacaoAcessoDados
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IRegSegregAcessoDadosService {
	
	/**
	 * Lista regra segregacao.
	 *
	 * @param listarRegraSegregacaoEntradaDTO the listar regra segregacao entrada dto
	 * @return the list< listar regra segregacao saida dt o>
	 */
	List<ListarRegraSegregacaoSaidaDTO> listaRegraSegregacao (ListarRegraSegregacaoEntradaDTO listarRegraSegregacaoEntradaDTO);
	
	/**
	 * Listar exc regra segregacao.
	 *
	 * @param listarExcSegregacaoAcessoEntradaDTO the listar exc segregacao acesso entrada dto
	 * @return the list< listar exc regra segregacao saida dt o>
	 */
	List<ListarExcRegraSegregacaoSaidaDTO> listarExcRegraSegregacao (ListarExcRegraSegregacaoEntradaDTO listarExcSegregacaoAcessoEntradaDTO);
	
	/**
	 * Incluir regra segregacao.
	 *
	 * @param incluirRegraSegregacaoEntradaDTO the incluir regra segregacao entrada dto
	 * @return the incluir regra segregacao saida dto
	 */
	IncluirRegraSegregacaoSaidaDTO incluirRegraSegregacao (IncluirRegraSegregacaoEntradaDTO incluirRegraSegregacaoEntradaDTO);
	
	/**
	 * Incluir exc regra segregacao.
	 *
	 * @param incluirRegraSegregacaoAcessoEntradaDTO the incluir regra segregacao acesso entrada dto
	 * @return the incluir exc regra segregacao saida dto
	 */
	IncluirExcRegraSegregacaoSaidaDTO incluirExcRegraSegregacao (IncluirExcRegraSegregacaoEntradaDTO incluirRegraSegregacaoAcessoEntradaDTO);
	
	/**
	 * Detalhar regra segregacao.
	 *
	 * @param detalharRegraSegregacaoEntradaDTO the detalhar regra segregacao entrada dto
	 * @return the detalhar regra segregacao saida dto
	 */
	DetalharRegraSegregacaoSaidaDTO detalharRegraSegregacao (DetalharRegraSegregacaoEntradaDTO detalharRegraSegregacaoEntradaDTO);
	
	/**
	 * Detalhar exc regra segregacao.
	 *
	 * @param detalharExcRegraSegregacaoEntradaDTO the detalhar exc regra segregacao entrada dto
	 * @return the detalhar exc regra segregacao saida dto
	 */
	DetalharExcRegraSegregacaoSaidaDTO detalharExcRegraSegregacao (DetalharExcRegraSegregacaoEntradaDTO detalharExcRegraSegregacaoEntradaDTO);
	
	/**
	 * Excluir regra segregacao.
	 *
	 * @param excluirRegraSegregacaoEntradaDTO the excluir regra segregacao entrada dto
	 * @return the excluir regra segregacao saida dto
	 */
	ExcluirRegraSegregacaoSaidaDTO excluirRegraSegregacao (ExcluirRegraSegregacaoEntradaDTO excluirRegraSegregacaoEntradaDTO);
	
	/**
	 * Excluir exc regra segregacao.
	 *
	 * @param excluirExcRegraSegregacaoEntradaDTO the excluir exc regra segregacao entrada dto
	 * @return the excluir exc regra segregacao saida dto
	 */
	ExcluirExcRegraSegregacaoSaidaDTO excluirExcRegraSegregacao (ExcluirExcRegraSegregacaoEntradaDTO excluirExcRegraSegregacaoEntradaDTO);
	
	/**
	 * Alterar regra segregacao.
	 *
	 * @param alterarRegraSegregacaoEntradaDTO the alterar regra segregacao entrada dto
	 * @return the alterar regra segregacao saida dto
	 */
	AlterarRegraSegregacaoSaidaDTO alterarRegraSegregacao (AlterarRegraSegregacaoEntradaDTO alterarRegraSegregacaoEntradaDTO);
	
	/**
	 * Alterar exc regra segregacao.
	 *
	 * @param alterarRegraSegregacaoEntradaDTO the alterar regra segregacao entrada dto
	 * @return the alterar exc regra segregacao saida dto
	 */
	AlterarExcRegraSegregacaoSaidaDTO alterarExcRegraSegregacao (AlterarExcRegraSegregacaoEntradaDTO alterarRegraSegregacaoEntradaDTO);
	

    /**
     * M�todo de exemplo.
     */
    void sampleManterRegraSegregacaoAcessoDados();

}

