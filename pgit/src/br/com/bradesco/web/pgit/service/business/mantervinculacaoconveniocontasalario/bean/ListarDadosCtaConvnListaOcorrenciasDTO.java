/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ListarDadosCtaConvnListaOcorrenciasDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarDadosCtaConvnListaOcorrenciasDTO {

	/** Atributo cdBanco. */
	private Integer cdBanco;
	
	/** Atributo cdAgencia. */
	private Integer cdAgencia;
	
	/** Atributo cdDigitoAgencia. */
	private String cdDigitoAgencia;
	
	/** Atributo cdCta. */
	private Long cdCta;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo cdConvnNovo. */
	private Integer cdConvnNovo;
	
	/** Atributo cdConveCtaSalarial. */
	private Long cdConveCtaSalarial;
	
	/** Atributo cdCpfCnpjEmp. */
	private Long cdCpfCnpjEmp;
	
	/** Atributo cdFilialCnpjEmp. */
	private Integer cdFilialCnpjEmp;
	
	/** Atributo cdControleCnpjEmp. */
	private Integer cdControleCnpjEmp;
	
	/** Atributo dsConvn. */
	private String dsConvn;
	
	/** Atributo bancoAgConta. */
	private String bancoAgConta;
	
	/** Atributo bancoAgConta. */
	private String bancoAgenciaConta;

	/**
	 * Get: bancoFormatado.
	 *
	 * @return bancoFormatado
	 */
	public String getBancoFormatado() {
		if (getCdBanco() != 0 || getCdAgencia() != 0 || getCdCta() != 0) {
			return (getCdBanco() + "/" + getCdAgencia() +  "/" + getCdCta() + "-" +	getCdDigitoConta());
		}

		return "";
	}

	/**
	 * Get: numConvenio.
	 *
	 * @return numConvenio
	 */
	public String getNumConvenio() {
		if (cdConvnNovo == 1) {
			return "NOVO";
		}

		return getCdConveCtaSalarial().toString();
	}

	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}

	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}

	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public String getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}

	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(String cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}

	/**
	 * Get: cdCta.
	 *
	 * @return cdCta
	 */
	public Long getCdCta() {
		return cdCta;
	}

	/**
	 * Set: cdCta.
	 *
	 * @param cdCta the cd cta
	 */
	public void setCdCta(Long cdCta) {
		this.cdCta = cdCta;
	}

	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}

	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}

	/**
	 * Get: cdConvnNovo.
	 *
	 * @return cdConvnNovo
	 */
	public Integer getCdConvnNovo() {
		return cdConvnNovo;
	}

	/**
	 * Set: cdConvnNovo.
	 *
	 * @param cdConvnNovo the cd convn novo
	 */
	public void setCdConvnNovo(Integer cdConvnNovo) {
		this.cdConvnNovo = cdConvnNovo;
	}

	/**
	 * Get: cdConveCtaSalarial.
	 *
	 * @return cdConveCtaSalarial
	 */
	public Long getCdConveCtaSalarial() {
		return cdConveCtaSalarial;
	}

	/**
	 * Set: cdConveCtaSalarial.
	 *
	 * @param cdConveCtaSalarial the cd conve cta salarial
	 */
	public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
		this.cdConveCtaSalarial = cdConveCtaSalarial;
	}

	/**
	 * Get: cdCpfCnpjEmp.
	 *
	 * @return cdCpfCnpjEmp
	 */
	public Long getCdCpfCnpjEmp() {
		return cdCpfCnpjEmp;
	}

	/**
	 * Set: cdCpfCnpjEmp.
	 *
	 * @param cdCpfCnpjEmp the cd cpf cnpj emp
	 */
	public void setCdCpfCnpjEmp(Long cdCpfCnpjEmp) {
		this.cdCpfCnpjEmp = cdCpfCnpjEmp;
	}

	/**
	 * Get: cdFilialCnpjEmp.
	 *
	 * @return cdFilialCnpjEmp
	 */
	public Integer getCdFilialCnpjEmp() {
		return cdFilialCnpjEmp;
	}

	/**
	 * Set: cdFilialCnpjEmp.
	 *
	 * @param cdFilialCnpjEmp the cd filial cnpj emp
	 */
	public void setCdFilialCnpjEmp(Integer cdFilialCnpjEmp) {
		this.cdFilialCnpjEmp = cdFilialCnpjEmp;
	}

	/**
	 * Get: cdControleCnpjEmp.
	 *
	 * @return cdControleCnpjEmp
	 */
	public Integer getCdControleCnpjEmp() {
		return cdControleCnpjEmp;
	}

	/**
	 * Set: cdControleCnpjEmp.
	 *
	 * @param cdControleCnpjEmp the cd controle cnpj emp
	 */
	public void setCdControleCnpjEmp(Integer cdControleCnpjEmp) {
		this.cdControleCnpjEmp = cdControleCnpjEmp;
	}

	/**
	 * Get: dsConvn.
	 *
	 * @return dsConvn
	 */
	public String getDsConvn() {
		return dsConvn;
	}

	/**
	 * Set: dsConvn.
	 *
	 * @param dsConvn the ds convn
	 */
	public void setDsConvn(String dsConvn) {
		this.dsConvn = dsConvn;
	}

	/**
	 * @return the bancoAgConta
	 */
	public String getBancoAgConta() {
		return PgitUtil.formatBancoAgenciaConta(getCdBanco(), getCdAgencia(), getCdDigitoAgencia(), getCdCta(), getCdDigitoConta(), false);
	}

	/**
	 * @param bancoAgConta the bancoAgConta to set
	 */
	public void setBancoAgConta(String bancoAgConta) {
		this.bancoAgConta = bancoAgConta;
	}

	/**
	 * @return the bancoAgenciaConta
	 */
	public String getBancoAgenciaConta() {
		return bancoAgenciaConta;
	}

	/**
	 * @param bancoAgenciaConta the bancoAgenciaConta to set
	 */
	public void setBancoAgenciaConta(String bancoAgenciaConta) {
		this.bancoAgenciaConta = bancoAgenciaConta;
	}
}