/*
 * Nome: br.com.bradesco.web.pgit.service.business.cmpi0000.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.cmpi0000.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import br.com.bradesco.web.aq.application.log.ILogManager;
import br.com.bradesco.web.pgit.service.business.cmpi0000.ICMPI0000Service;

/**
 * Nome: Cmpi0000Bean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class Cmpi0000Bean {

	/** Atributo logger. */
	private ILogManager logger;

	/** Atributo mostraBotoes0000. */
	private boolean mostraBotoes0000;

	/** Atributo hiddenRadioCliente. */
	private int hiddenRadioCliente;

	/** Atributo hiddenRadioFiltroArgumento. */
	private int hiddenRadioFiltroArgumento;

	/** Atributo validaCpfCliente. */
	private String validaCpfCliente;

	/** Atributo cnpj1. */
	private String cnpj1;

	/** Atributo cnpj2. */
	private String cnpj2;

	/** Atributo cnpj3. */
	private String cnpj3;

	/** Atributo cnpj1aux. */
	private String cnpj1aux;

	/** Atributo cnpj2aux. */
	private String cnpj2aux;

	/** Atributo cnpj3aux. */
	private String cnpj3aux;

	/** Atributo cpfCnpjIdentificaoCliente. */
	private String cpfCnpjIdentificaoCliente;

	/** Atributo cpf1. */
	private String cpf1;

	/** Atributo cpf2. */
	private String cpf2;

	/** Atributo nomeRazaoIdentificaCliente. */
	private String nomeRazaoIdentificaCliente;

	/** Atributo bancoIdentificacaoCliente. */
	private String bancoIdentificacaoCliente;

	/** Atributo agenciaIdentificacaoCliente. */
	private String agenciaIdentificacaoCliente;

	/** Atributo contaIdentificacaoCliente. */
	private String contaIdentificacaoCliente;

	/** Atributo filtroIdentificacaoCliente. */
	private String filtroIdentificacaoCliente;

	/** Atributo cpf. */
	private String cpf;

	/** Atributo nomeRazao. */
	private String nomeRazao;

	/** Atributo nomeRazaoFundacao. */
	private String nomeRazaoFundacao;

	/** Atributo tela. */
	private String tela;

	/** Atributo diaNasc. */
	private String diaNasc;

	/** Atributo mesNasc. */
	private String mesNasc;

	/** Atributo anoNasc. */
	private String anoNasc;

	/** Atributo desabDataRec. */
	private boolean desabDataRec;

	/** Atributo desabDataFin. */
	private boolean desabDataFin;

	/** Atributo desabServico. */
	private boolean desabServico;

	/** Atributo desabCodEmpresa. */
	private boolean desabCodEmpresa;

	/** Atributo desabSeqRemessa. */
	private boolean desabSeqRemessa;

	/** Atributo desabResProc. */
	private boolean desabResProc;

	/** Atributo desabBtnConsultarRem. */
	private boolean desabBtnConsultarRem;

	/** Atributo desabResTipo. */
	private boolean desabResTipo;

	/** Atributo desabBtnConsultarRet. */
	private boolean desabBtnConsultarRet;

	/** Atributo desabDataRecRet. */
	private boolean desabDataRecRet;

	/** Atributo desabDataFinRet. */
	private boolean desabDataFinRet;

	/** Atributo desabServicoRet. */
	private boolean desabServicoRet;

	/** Atributo desabCodEmpresaRet. */
	private boolean desabCodEmpresaRet;

	/** Atributo desabSeqRemessaRet. */
	private boolean desabSeqRemessaRet;

	/** Atributo listaGridCliente. */
	private List<ListarClientesDTO> listaGridCliente;

	/** Atributo icmpi0000ServiceImpl. */
	private ICMPI0000Service icmpi0000ServiceImpl;

	/** Atributo listaControleCliente. */
	private List<SelectItem> listaControleCliente;

	/**
	 * Cmpi0000 bean.
	 */
	public Cmpi0000Bean() {

		listaControleCliente = new ArrayList<SelectItem>();

		desabDataRec = true;

		desabDataFin = true;

		desabServico = true;

		desabCodEmpresa = true;

		desabSeqRemessa = true;

		desabResProc = true;

		desabBtnConsultarRem = true;

		desabBtnConsultarRet = true;

		desabResTipo = true;

		desabDataRecRet = true;

		desabDataFinRet = true;

		desabServicoRet = true;

		desabCodEmpresaRet = true;

		desabSeqRemessaRet = true;

	}

	/**
	 * Get: anoNasc.
	 *
	 * @return anoNasc
	 */
	public String getAnoNasc() {
		return anoNasc;
	}

	/**
	 * Set: anoNasc.
	 *
	 * @param anoNasc the ano nasc
	 */
	public void setAnoNasc(String anoNasc) {
		this.anoNasc = anoNasc;
	}

	/**
	 * Get: diaNasc.
	 *
	 * @return diaNasc
	 */
	public String getDiaNasc() {
		return diaNasc;
	}

	/**
	 * Set: diaNasc.
	 *
	 * @param diaNasc the dia nasc
	 */
	public void setDiaNasc(String diaNasc) {
		this.diaNasc = diaNasc;
	}

	/**
	 * Get: mesNasc.
	 *
	 * @return mesNasc
	 */
	public String getMesNasc() {
		return mesNasc;
	}

	/**
	 * Set: mesNasc.
	 *
	 * @param mesNasc the mes nasc
	 */
	public void setMesNasc(String mesNasc) {
		this.mesNasc = mesNasc;
	}

	/**
	 * Is mostra botoes0000.
	 *
	 * @return true, if is mostra botoes0000
	 */
	public boolean isMostraBotoes0000() {
		return mostraBotoes0000;
	}

	/**
	 * Set: mostraBotoes0000.
	 *
	 * @param mostraBotoes0000 the mostra botoes0000
	 */
	public void setMostraBotoes0000(boolean mostraBotoes0000) {
		this.mostraBotoes0000 = mostraBotoes0000;
	}

	/**
	 * Get: agenciaIdentificacaoCliente.
	 *
	 * @return agenciaIdentificacaoCliente
	 */
	public String getAgenciaIdentificacaoCliente() {
		return agenciaIdentificacaoCliente;
	}

	/**
	 * Set: agenciaIdentificacaoCliente.
	 *
	 * @param agenciaIdentificacaoCliente the agencia identificacao cliente
	 */
	public void setAgenciaIdentificacaoCliente(String agenciaIdentificacaoCliente) {
		this.agenciaIdentificacaoCliente = agenciaIdentificacaoCliente;
	}

	/**
	 * Get: bancoIdentificacaoCliente.
	 *
	 * @return bancoIdentificacaoCliente
	 */
	public String getBancoIdentificacaoCliente() {
		return bancoIdentificacaoCliente;
	}

	/**
	 * Set: bancoIdentificacaoCliente.
	 *
	 * @param bancoIdentificacaoCliente the banco identificacao cliente
	 */
	public void setBancoIdentificacaoCliente(String bancoIdentificacaoCliente) {
		this.bancoIdentificacaoCliente = bancoIdentificacaoCliente;
	}

	/**
	 * Get: cnpj1.
	 *
	 * @return cnpj1
	 */
	public String getCnpj1() {
		return cnpj1;
	}

	/**
	 * Set: cnpj1.
	 *
	 * @param club the cnpj1
	 */
	public void setCnpj1(String club) {
		this.cnpj1 = club;
	}

	/**
	 * Get: contaIdentificacaoCliente.
	 *
	 * @return contaIdentificacaoCliente
	 */
	public String getContaIdentificacaoCliente() {
		return contaIdentificacaoCliente;
	}

	/**
	 * Set: contaIdentificacaoCliente.
	 *
	 * @param contaIdentificacaoCliente the conta identificacao cliente
	 */
	public void setContaIdentificacaoCliente(String contaIdentificacaoCliente) {
		this.contaIdentificacaoCliente = contaIdentificacaoCliente;
	}

	/**
	 * Get: cpfCnpjIdentificaoCliente.
	 *
	 * @return cpfCnpjIdentificaoCliente
	 */
	public String getCpfCnpjIdentificaoCliente() {
		return cpfCnpjIdentificaoCliente;
	}

	/**
	 * Set: cpfCnpjIdentificaoCliente.
	 *
	 * @param cpfCnpjIdentificaoCliente the cpf cnpj identificao cliente
	 */
	public void setCpfCnpjIdentificaoCliente(String cpfCnpjIdentificaoCliente) {
		this.cpfCnpjIdentificaoCliente = cpfCnpjIdentificaoCliente;
	}

	/**
	 * Get: hiddenRadioCliente.
	 *
	 * @return hiddenRadioCliente
	 */
	public int getHiddenRadioCliente() {
		return hiddenRadioCliente;
	}

	/**
	 * Set: hiddenRadioCliente.
	 *
	 * @param hiddenRadioCliente the hidden radio cliente
	 */
	public void setHiddenRadioCliente(int hiddenRadioCliente) {
		this.hiddenRadioCliente = hiddenRadioCliente;
	}

	/**
	 * Get: hiddenRadioFiltroArgumento.
	 *
	 * @return hiddenRadioFiltroArgumento
	 */
	public int getHiddenRadioFiltroArgumento() {
		return hiddenRadioFiltroArgumento;
	}

	/**
	 * Set: hiddenRadioFiltroArgumento.
	 *
	 * @param hiddenRadioFiltroArgumento the hidden radio filtro argumento
	 */
	public void setHiddenRadioFiltroArgumento(int hiddenRadioFiltroArgumento) {
		this.hiddenRadioFiltroArgumento = hiddenRadioFiltroArgumento;
	}

	/**
	 * Get: listaControleCliente.
	 *
	 * @return listaControleCliente
	 */
	public List<SelectItem> getListaControleCliente() {
		return listaControleCliente;
	}

	/**
	 * Set: listaControleCliente.
	 *
	 * @param listaControleCliente the lista controle cliente
	 */
	public void setListaControleCliente(List<SelectItem> listaControleCliente) {
		this.listaControleCliente = listaControleCliente;
	}

	/**
	 * Get: listaGridCliente.
	 *
	 * @return listaGridCliente
	 */
	public List<ListarClientesDTO> getListaGridCliente() {
		return listaGridCliente;
	}

	/**
	 * Set: listaGridCliente.
	 *
	 * @param listaGridCliente the lista grid cliente
	 */
	public void setListaGridCliente(List<ListarClientesDTO> listaGridCliente) {
		this.listaGridCliente = listaGridCliente;
	}

	/**
	 * Get: nomeRazaoIdentificaCliente.
	 *
	 * @return nomeRazaoIdentificaCliente
	 */
	public String getNomeRazaoIdentificaCliente() {
		return nomeRazaoIdentificaCliente;
	}

	/**
	 * Set: nomeRazaoIdentificaCliente.
	 *
	 * @param nomeRazaoIdentificaCliente the nome razao identifica cliente
	 */
	public void setNomeRazaoIdentificaCliente(String nomeRazaoIdentificaCliente) {
		this.nomeRazaoIdentificaCliente = nomeRazaoIdentificaCliente;
	}

	/**
	 * Get: validaCpfCliente.
	 *
	 * @return validaCpfCliente
	 */
	public String getValidaCpfCliente() {
		return validaCpfCliente;
	}

	/**
	 * Set: validaCpfCliente.
	 *
	 * @param validaCpfCliente the valida cpf cliente
	 */
	public void setValidaCpfCliente(String validaCpfCliente) {
		this.validaCpfCliente = validaCpfCliente;
	}

	/**
	 * Get: filtroIdentificacaoCliente.
	 *
	 * @return filtroIdentificacaoCliente
	 */
	public String getFiltroIdentificacaoCliente() {
		return filtroIdentificacaoCliente;
	}

	/**
	 * Set: filtroIdentificacaoCliente.
	 *
	 * @param filtroIdentificacaoCliente the filtro identificacao cliente
	 */
	public void setFiltroIdentificacaoCliente(String filtroIdentificacaoCliente) {
		this.filtroIdentificacaoCliente = filtroIdentificacaoCliente;
	}

	/**
	 * Get: cpf.
	 *
	 * @return cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Set: cpf.
	 *
	 * @param cpf the cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Get: nomeRazao.
	 *
	 * @return nomeRazao
	 */
	public String getNomeRazao() {
		return nomeRazao;
	}

	/**
	 * Set: nomeRazao.
	 *
	 * @param nomeRazao the nome razao
	 */
	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}

	/**
	 * Limpar campos de pesquisa.
	 */
	public void limparCamposDePesquisa() {

		logger.debug(this, "Iniciando a limpeza dos campos de pesquisa.");

		this.setCnpj1("");
		this.setCnpj2("");
		this.setCnpj3("");

		this.setCpf1("");
		this.setCpf2("");

		this.setNomeRazao("");
		this.setDiaNasc("");
		this.setMesNasc("");
		this.setAnoNasc("");

		this.setNomeRazaoIdentificaCliente("");
		
		
		this.setBancoIdentificacaoCliente("");
		this.setAgenciaIdentificacaoCliente("");
		this.setContaIdentificacaoCliente("");

		logger.debug(this, "Finalizando a limpeza dos campos de pesquisa.");
	}

	/**
	 * Limpar dados identificao cliente.
	 *
	 * @return the string
	 */
	public String limparDadosIdentificaoCliente() {
		logger.debug(this, "Iniciando a limpeza dos dados identificacao cliente.");

		this.setFiltroIdentificacaoCliente("");
		this.setCpfCnpjIdentificaoCliente("");
		this.setNomeRazaoIdentificaCliente("");
		this.setBancoIdentificacaoCliente("");
		this.setAgenciaIdentificacaoCliente("");
		this.setContaIdentificacaoCliente("");
		this.setNomeRazaoFundacao("");
		
		this.setDiaNasc("");
		this.setMesNasc("");
		this.setAnoNasc("");
		this.setCpf("");
		this.setNomeRazao("");
		logger.debug(this, "Finalizando a limpeza dos dados identificacao cliente.");

		return "ok";

	}

	/**
	 * Limpar lista clientes.
	 */
	public void limparListaClientes() {
		this.listaGridCliente = null;
	}

	/**
	 * Voltar identificacao cliente.
	 *
	 * @return the string
	 */
	public String voltarIdentificacaoCliente() {
		logger.debug(this, "Iniciando a volta identificacao cliente.");
		this.listaGridCliente = null;
		limparDadosIdentificaoCliente();
		logger.debug(this, "Finalizando a volta identificacao cliente.");
		return tela;
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		return "ok";
	}

	/**
	 * Confirmar cliente.
	 *
	 * @return the string
	 */
	public String confirmarCliente() {
		logger.debug(this, "Iniciando a confirmacao cliente.");

		ListarClientesDTO listarClienteDTO = (ListarClientesDTO) getListaGridCliente().get(getHiddenRadioCliente());
		setNomeRazaoFundacao(listarClienteDTO.getNomeRazao());
		setCpf(listarClienteDTO.getCpf());

		this.setCnpj1aux(listarClienteDTO.getCpfCnpjCliente());
		this.setCnpj2aux(listarClienteDTO.getFilialCnpj());
		this.setCnpj3aux(listarClienteDTO.getControleCpfCnpj());
		
		if (tela.equals("RETORNO")) {
					
			setDesabDataRecRet(false);
			setDesabDataFinRet(false);
			setDesabServicoRet(false);
			setDesabCodEmpresaRet(false);
			setDesabSeqRemessaRet(false);
			setDesabBtnConsultarRet(false);
			setDesabResTipo(false);
		} else {
			setDesabDataRec(false);
			setDesabDataFin(false);
			setDesabServico(false);
			setDesabCodEmpresa(false);
			setDesabSeqRemessa(false);
			setDesabResProc(true);
			setDesabBtnConsultarRem(false);
		}

		limparListaClientes();
		logger.debug(this, "Finalizando a confirmacao cliente.");
		return tela;
	}

	/**
	 * Carrega lista clientes.
	 */
	public void carregaListaClientes() {

		logger.debug(this, "Iniciando a carga da lista cliente.");

		List<SelectItem> listaCodigo = new ArrayList<SelectItem>();

		int i = 0;
		for (i = 0; i <= this.listaGridCliente.size(); i++) {
			listaCodigo.add(new SelectItem(i, " "));
		}
		this.setListaControleCliente(listaCodigo);
		if (this.getListaGridCliente().size() > 10){
			this.setMostraBotoes0000(true);
		}
		else{
			this.setMostraBotoes0000(false);
		}

		logger.debug(this, "Finalizando a carga da lista cliente.");
	}

	/**
	 * Get: icmpi0000ServiceImpl.
	 *
	 * @return icmpi0000ServiceImpl
	 */
	public ICMPI0000Service getIcmpi0000ServiceImpl() {
		return icmpi0000ServiceImpl;
	}

	/**
	 * Set: icmpi0000ServiceImpl.
	 *
	 * @param icmpi0000ServiceImpl the icmpi0000 service impl
	 */
	public void setIcmpi0000ServiceImpl(ICMPI0000Service icmpi0000ServiceImpl) {
		this.icmpi0000ServiceImpl = icmpi0000ServiceImpl;
	}

	/**
	 * Get: tela.
	 *
	 * @return tela
	 */
	public String getTela() {
		return tela;
	}

	/**
	 * Set: tela.
	 *
	 * @param tela the tela
	 */
	public void setTela(String tela) {
		this.tela = tela;
	}

	/**
	 * Get: cnpj2.
	 *
	 * @return cnpj2
	 */
	public String getCnpj2() {
		return cnpj2;
	}

	/**
	 * Set: cnpj2.
	 *
	 * @param cnpj2 the cnpj2
	 */
	public void setCnpj2(String cnpj2) {
		this.cnpj2 = cnpj2;
	}

	/**
	 * Get: cnpj3.
	 *
	 * @return cnpj3
	 */
	public String getCnpj3() {
		return cnpj3;
	}

	/**
	 * Set: cnpj3.
	 *
	 * @param cnpj3 the cnpj3
	 */
	public void setCnpj3(String cnpj3) {
		this.cnpj3 = cnpj3;
	}

	/**
	 * Get: cpf2.
	 *
	 * @return cpf2
	 */
	public String getCpf2() {
		return cpf2;
	}

	/**
	 * Set: cpf2.
	 *
	 * @param cpf2 the cpf2
	 */
	public void setCpf2(String cpf2) {
		this.cpf2 = cpf2;
	}

	/**
	 * Get: cpf1.
	 *
	 * @return cpf1
	 */
	public String getCpf1() {
		return cpf1;
	}

	/**
	 * Set: cpf1.
	 *
	 * @param cpf1 the cpf1
	 */
	public void setCpf1(String cpf1) {
		this.cpf1 = cpf1;
	}

	/**
	 * Is desab cod empresa.
	 *
	 * @return true, if is desab cod empresa
	 */
	public boolean isDesabCodEmpresa() {
		return desabCodEmpresa;
	}

	/**
	 * Set: desabCodEmpresa.
	 *
	 * @param desabCodEmpresa the desab cod empresa
	 */
	public void setDesabCodEmpresa(boolean desabCodEmpresa) {
		this.desabCodEmpresa = desabCodEmpresa;
	}

	/**
	 * Is desab data fin.
	 *
	 * @return true, if is desab data fin
	 */
	public boolean isDesabDataFin() {
		return desabDataFin;
	}

	/**
	 * Set: desabDataFin.
	 *
	 * @param desabDataFin the desab data fin
	 */
	public void setDesabDataFin(boolean desabDataFin) {
		this.desabDataFin = desabDataFin;
	}

	/**
	 * Is desab data rec.
	 *
	 * @return true, if is desab data rec
	 */
	public boolean isDesabDataRec() {
		return desabDataRec;
	}

	/**
	 * Set: desabDataRec.
	 *
	 * @param desabDataRec the desab data rec
	 */
	public void setDesabDataRec(boolean desabDataRec) {
		this.desabDataRec = desabDataRec;
	}

	/**
	 * Is desab res proc.
	 *
	 * @return true, if is desab res proc
	 */
	public boolean isDesabResProc() {
		return desabResProc;
	}

	/**
	 * Set: desabResProc.
	 *
	 * @param desabResProc the desab res proc
	 */
	public void setDesabResProc(boolean desabResProc) {
		this.desabResProc = desabResProc;
	}

	/**
	 * Is desab seq remessa.
	 *
	 * @return true, if is desab seq remessa
	 */
	public boolean isDesabSeqRemessa() {
		return desabSeqRemessa;
	}

	/**
	 * Set: desabSeqRemessa.
	 *
	 * @param desabSeqRemessa the desab seq remessa
	 */
	public void setDesabSeqRemessa(boolean desabSeqRemessa) {
		this.desabSeqRemessa = desabSeqRemessa;
	}

	/**
	 * Is desab servico.
	 *
	 * @return true, if is desab servico
	 */
	public boolean isDesabServico() {
		return desabServico;
	}

	/**
	 * Set: desabServico.
	 *
	 * @param desabServico the desab servico
	 */
	public void setDesabServico(boolean desabServico) {
		this.desabServico = desabServico;
	}

	/**
	 * Is desab btn consultar rem.
	 *
	 * @return true, if is desab btn consultar rem
	 */
	public boolean isDesabBtnConsultarRem() {
		return desabBtnConsultarRem;
	}

	/**
	 * Set: desabBtnConsultarRem.
	 *
	 * @param desabBtnConsultarRem the desab btn consultar rem
	 */
	public void setDesabBtnConsultarRem(boolean desabBtnConsultarRem) {
		this.desabBtnConsultarRem = desabBtnConsultarRem;
	}

	/**
	 * Is desab res tipo.
	 *
	 * @return true, if is desab res tipo
	 */
	public boolean isDesabResTipo() {
		return desabResTipo;
	}

	/**
	 * Set: desabResTipo.
	 *
	 * @param desabResTipo the desab res tipo
	 */
	public void setDesabResTipo(boolean desabResTipo) {
		this.desabResTipo = desabResTipo;
	}

	/**
	 * Is desab btn consultar ret.
	 *
	 * @return true, if is desab btn consultar ret
	 */
	public boolean isDesabBtnConsultarRet() {
		return desabBtnConsultarRet;
	}

	/**
	 * Set: desabBtnConsultarRet.
	 *
	 * @param desabBtnConsultarRet the desab btn consultar ret
	 */
	public void setDesabBtnConsultarRet(boolean desabBtnConsultarRet) {
		this.desabBtnConsultarRet = desabBtnConsultarRet;
	}

	/**
	 * Is desab cod empresa ret.
	 *
	 * @return true, if is desab cod empresa ret
	 */
	public boolean isDesabCodEmpresaRet() {
		return desabCodEmpresaRet;
	}

	/**
	 * Set: desabCodEmpresaRet.
	 *
	 * @param desabCodEmpresaRet the desab cod empresa ret
	 */
	public void setDesabCodEmpresaRet(boolean desabCodEmpresaRet) {
		this.desabCodEmpresaRet = desabCodEmpresaRet;
	}

	/**
	 * Is desab data fin ret.
	 *
	 * @return true, if is desab data fin ret
	 */
	public boolean isDesabDataFinRet() {
		return desabDataFinRet;
	}

	/**
	 * Set: desabDataFinRet.
	 *
	 * @param desabDataFinRet the desab data fin ret
	 */
	public void setDesabDataFinRet(boolean desabDataFinRet) {
		this.desabDataFinRet = desabDataFinRet;
	}

	/**
	 * Is desab data rec ret.
	 *
	 * @return true, if is desab data rec ret
	 */
	public boolean isDesabDataRecRet() {
		return desabDataRecRet;
	}

	/**
	 * Set: desabDataRecRet.
	 *
	 * @param desabDataRecRet the desab data rec ret
	 */
	public void setDesabDataRecRet(boolean desabDataRecRet) {
		this.desabDataRecRet = desabDataRecRet;
	}

	/**
	 * Is desab seq remessa ret.
	 *
	 * @return true, if is desab seq remessa ret
	 */
	public boolean isDesabSeqRemessaRet() {
		return desabSeqRemessaRet;
	}

	/**
	 * Set: desabSeqRemessaRet.
	 *
	 * @param desabSeqRemessaRet the desab seq remessa ret
	 */
	public void setDesabSeqRemessaRet(boolean desabSeqRemessaRet) {
		this.desabSeqRemessaRet = desabSeqRemessaRet;
	}

	/**
	 * Is desab servico ret.
	 *
	 * @return true, if is desab servico ret
	 */
	public boolean isDesabServicoRet() {
		return desabServicoRet;
	}

	/**
	 * Set: desabServicoRet.
	 *
	 * @param desabServicoRet the desab servico ret
	 */
	public void setDesabServicoRet(boolean desabServicoRet) {
		this.desabServicoRet = desabServicoRet;
	}

	/**
	 * Get: cnpj1aux.
	 *
	 * @return cnpj1aux
	 */
	public String getCnpj1aux() {
		return cnpj1aux;
	}

	/**
	 * Set: cnpj1aux.
	 *
	 * @param cnpj1aux the cnpj1aux
	 */
	public void setCnpj1aux(String cnpj1aux) {
		this.cnpj1aux = cnpj1aux;
	}

	/**
	 * Get: cnpj2aux.
	 *
	 * @return cnpj2aux
	 */
	public String getCnpj2aux() {
		return cnpj2aux;
	}

	/**
	 * Set: cnpj2aux.
	 *
	 * @param cnpj2aux the cnpj2aux
	 */
	public void setCnpj2aux(String cnpj2aux) {
		this.cnpj2aux = cnpj2aux;
	}

	/**
	 * Get: cnpj3aux.
	 *
	 * @return cnpj3aux
	 */
	public String getCnpj3aux() {
		return cnpj3aux;
	}

	/**
	 * Set: cnpj3aux.
	 *
	 * @param cnpj3aux the cnpj3aux
	 */
	public void setCnpj3aux(String cnpj3aux) {
		this.cnpj3aux = cnpj3aux;
	}

	/**
	 * Get: nomeRazaoFundacao.
	 *
	 * @return nomeRazaoFundacao
	 */
	public String getNomeRazaoFundacao() {
		return nomeRazaoFundacao;
	}

	/**
	 * Set: nomeRazaoFundacao.
	 *
	 * @param nomeRazaoFundacao the nome razao fundacao
	 */
	public void setNomeRazaoFundacao(String nomeRazaoFundacao) {
		this.nomeRazaoFundacao = nomeRazaoFundacao;
	}

	/**
	 * Get: logger.
	 *
	 * @return logger
	 */
	public ILogManager getLogger() {
		return logger;
	}

	/**
	 * Set: logger.
	 *
	 * @param logger the logger
	 */
	public void setLogger(ILogManager logger) {
		this.logger = logger;
	}
}
