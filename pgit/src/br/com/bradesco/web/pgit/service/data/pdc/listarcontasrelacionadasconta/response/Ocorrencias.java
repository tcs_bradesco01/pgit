/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasconta.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaRelacionada
     */
    private long _cdPessoaJuridicaRelacionada = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaRelacionada
     */
    private boolean _has_cdPessoaJuridicaRelacionada;

    /**
     * Field _cdTipoContratoRelacionada
     */
    private int _cdTipoContratoRelacionada = 0;

    /**
     * keeps track of state for field: _cdTipoContratoRelacionada
     */
    private boolean _has_cdTipoContratoRelacionada;

    /**
     * Field _nrSequenciaContratoRelacionada
     */
    private long _nrSequenciaContratoRelacionada = 0;

    /**
     * keeps track of state for field:
     * _nrSequenciaContratoRelacionada
     */
    private boolean _has_nrSequenciaContratoRelacionada;

    /**
     * Field _cdTipoRelacionamento
     */
    private int _cdTipoRelacionamento = 0;

    /**
     * keeps track of state for field: _cdTipoRelacionamento
     */
    private boolean _has_cdTipoRelacionamento;

    /**
     * Field _dsTipoRelacionamento
     */
    private java.lang.String _dsTipoRelacionamento;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _dsPessoa
     */
    private java.lang.String _dsPessoa;

    /**
     * Field _cdCpfCnpj
     */
    private java.lang.String _cdCpfCnpj;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _cdAgenciaContabil
     */
    private int _cdAgenciaContabil = 0;

    /**
     * keeps track of state for field: _cdAgenciaContabil
     */
    private boolean _has_cdAgenciaContabil;

    /**
     * Field _dsAgenciaContabil
     */
    private java.lang.String _dsAgenciaContabil;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _dsTipoConta
     */
    private java.lang.String _dsTipoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasconta.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaContabil
     * 
     */
    public void deleteCdAgenciaContabil()
    {
        this._has_cdAgenciaContabil= false;
    } //-- void deleteCdAgenciaContabil() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridicaRelacionada
     * 
     */
    public void deleteCdPessoaJuridicaRelacionada()
    {
        this._has_cdPessoaJuridicaRelacionada= false;
    } //-- void deleteCdPessoaJuridicaRelacionada() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteCdTipoContratoRelacionada
     * 
     */
    public void deleteCdTipoContratoRelacionada()
    {
        this._has_cdTipoContratoRelacionada= false;
    } //-- void deleteCdTipoContratoRelacionada() 

    /**
     * Method deleteCdTipoRelacionamento
     * 
     */
    public void deleteCdTipoRelacionamento()
    {
        this._has_cdTipoRelacionamento= false;
    } //-- void deleteCdTipoRelacionamento() 

    /**
     * Method deleteNrSequenciaContratoRelacionada
     * 
     */
    public void deleteNrSequenciaContratoRelacionada()
    {
        this._has_nrSequenciaContratoRelacionada= false;
    } //-- void deleteNrSequenciaContratoRelacionada() 

    /**
     * Returns the value of field 'cdAgenciaContabil'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaContabil'.
     */
    public int getCdAgenciaContabil()
    {
        return this._cdAgenciaContabil;
    } //-- int getCdAgenciaContabil() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return String
     * @return the value of field 'cdCpfCnpj'.
     */
    public java.lang.String getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- java.lang.String getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaRelacionada'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaRelacionada'.
     */
    public long getCdPessoaJuridicaRelacionada()
    {
        return this._cdPessoaJuridicaRelacionada;
    } //-- long getCdPessoaJuridicaRelacionada() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'cdTipoContratoRelacionada'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoRelacionada'.
     */
    public int getCdTipoContratoRelacionada()
    {
        return this._cdTipoContratoRelacionada;
    } //-- int getCdTipoContratoRelacionada() 

    /**
     * Returns the value of field 'cdTipoRelacionamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelacionamento'.
     */
    public int getCdTipoRelacionamento()
    {
        return this._cdTipoRelacionamento;
    } //-- int getCdTipoRelacionamento() 

    /**
     * Returns the value of field 'dsAgenciaContabil'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaContabil'.
     */
    public java.lang.String getDsAgenciaContabil()
    {
        return this._dsAgenciaContabil;
    } //-- java.lang.String getDsAgenciaContabil() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'dsPessoa'.
     * 
     * @return String
     * @return the value of field 'dsPessoa'.
     */
    public java.lang.String getDsPessoa()
    {
        return this._dsPessoa;
    } //-- java.lang.String getDsPessoa() 

    /**
     * Returns the value of field 'dsTipoConta'.
     * 
     * @return String
     * @return the value of field 'dsTipoConta'.
     */
    public java.lang.String getDsTipoConta()
    {
        return this._dsTipoConta;
    } //-- java.lang.String getDsTipoConta() 

    /**
     * Returns the value of field 'dsTipoRelacionamento'.
     * 
     * @return String
     * @return the value of field 'dsTipoRelacionamento'.
     */
    public java.lang.String getDsTipoRelacionamento()
    {
        return this._dsTipoRelacionamento;
    } //-- java.lang.String getDsTipoRelacionamento() 

    /**
     * Returns the value of field 'nrSequenciaContratoRelacionada'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoRelacionada'.
     */
    public long getNrSequenciaContratoRelacionada()
    {
        return this._nrSequenciaContratoRelacionada;
    } //-- long getNrSequenciaContratoRelacionada() 

    /**
     * Method hasCdAgenciaContabil
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaContabil()
    {
        return this._has_cdAgenciaContabil;
    } //-- boolean hasCdAgenciaContabil() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridicaRelacionada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaRelacionada()
    {
        return this._has_cdPessoaJuridicaRelacionada;
    } //-- boolean hasCdPessoaJuridicaRelacionada() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasCdTipoContratoRelacionada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoRelacionada()
    {
        return this._has_cdTipoContratoRelacionada;
    } //-- boolean hasCdTipoContratoRelacionada() 

    /**
     * Method hasCdTipoRelacionamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelacionamento()
    {
        return this._has_cdTipoRelacionamento;
    } //-- boolean hasCdTipoRelacionamento() 

    /**
     * Method hasNrSequenciaContratoRelacionada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoRelacionada()
    {
        return this._has_nrSequenciaContratoRelacionada;
    } //-- boolean hasNrSequenciaContratoRelacionada() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaContabil'.
     * 
     * @param cdAgenciaContabil the value of field
     * 'cdAgenciaContabil'.
     */
    public void setCdAgenciaContabil(int cdAgenciaContabil)
    {
        this._cdAgenciaContabil = cdAgenciaContabil;
        this._has_cdAgenciaContabil = true;
    } //-- void setCdAgenciaContabil(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(java.lang.String cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
    } //-- void setCdCpfCnpj(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaRelacionada'.
     * 
     * @param cdPessoaJuridicaRelacionada the value of field
     * 'cdPessoaJuridicaRelacionada'.
     */
    public void setCdPessoaJuridicaRelacionada(long cdPessoaJuridicaRelacionada)
    {
        this._cdPessoaJuridicaRelacionada = cdPessoaJuridicaRelacionada;
        this._has_cdPessoaJuridicaRelacionada = true;
    } //-- void setCdPessoaJuridicaRelacionada(long) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoRelacionada'.
     * 
     * @param cdTipoContratoRelacionada the value of field
     * 'cdTipoContratoRelacionada'.
     */
    public void setCdTipoContratoRelacionada(int cdTipoContratoRelacionada)
    {
        this._cdTipoContratoRelacionada = cdTipoContratoRelacionada;
        this._has_cdTipoContratoRelacionada = true;
    } //-- void setCdTipoContratoRelacionada(int) 

    /**
     * Sets the value of field 'cdTipoRelacionamento'.
     * 
     * @param cdTipoRelacionamento the value of field
     * 'cdTipoRelacionamento'.
     */
    public void setCdTipoRelacionamento(int cdTipoRelacionamento)
    {
        this._cdTipoRelacionamento = cdTipoRelacionamento;
        this._has_cdTipoRelacionamento = true;
    } //-- void setCdTipoRelacionamento(int) 

    /**
     * Sets the value of field 'dsAgenciaContabil'.
     * 
     * @param dsAgenciaContabil the value of field
     * 'dsAgenciaContabil'.
     */
    public void setDsAgenciaContabil(java.lang.String dsAgenciaContabil)
    {
        this._dsAgenciaContabil = dsAgenciaContabil;
    } //-- void setDsAgenciaContabil(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoa'.
     * 
     * @param dsPessoa the value of field 'dsPessoa'.
     */
    public void setDsPessoa(java.lang.String dsPessoa)
    {
        this._dsPessoa = dsPessoa;
    } //-- void setDsPessoa(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoConta'.
     * 
     * @param dsTipoConta the value of field 'dsTipoConta'.
     */
    public void setDsTipoConta(java.lang.String dsTipoConta)
    {
        this._dsTipoConta = dsTipoConta;
    } //-- void setDsTipoConta(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoRelacionamento'.
     * 
     * @param dsTipoRelacionamento the value of field
     * 'dsTipoRelacionamento'.
     */
    public void setDsTipoRelacionamento(java.lang.String dsTipoRelacionamento)
    {
        this._dsTipoRelacionamento = dsTipoRelacionamento;
    } //-- void setDsTipoRelacionamento(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoRelacionada'.
     * 
     * @param nrSequenciaContratoRelacionada the value of field
     * 'nrSequenciaContratoRelacionada'.
     */
    public void setNrSequenciaContratoRelacionada(long nrSequenciaContratoRelacionada)
    {
        this._nrSequenciaContratoRelacionada = nrSequenciaContratoRelacionada;
        this._has_nrSequenciaContratoRelacionada = true;
    } //-- void setNrSequenciaContratoRelacionada(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasconta.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasconta.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasconta.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarcontasrelacionadasconta.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
