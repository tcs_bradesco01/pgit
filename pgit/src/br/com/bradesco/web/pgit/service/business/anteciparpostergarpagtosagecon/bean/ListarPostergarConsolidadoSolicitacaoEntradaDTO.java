package br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 23/05/17.
 *
 * @author : todo!
 * @version :
 */
public class ListarPostergarConsolidadoSolicitacaoEntradaDTO {

    /** The max ocorrencias. */
    private Integer maxOcorrencias = 100;

    /** The cd solicitacao pagamento integrado. */
    private Integer cdSolicitacaoPagamentoIntegrado;

    /** The nr solicitacao pagamento integrado. */
    private Integer nrSolicitacaoPagamentoIntegrado;

    /** The cdpessoa juridica contrato. */
    private Long cdpessoaJuridicaContrato;

    /** The cd tipo contrato negocio. */
    private Integer cdTipoContratoNegocio;

    /** The nr sequencia contrato negocio. */
    private Long nrSequenciaContratoNegocio;

    /**
     * Set max ocorrencias.
     *
     * @param maxOcorrencias the max ocorrencias
     */
    public void setMaxOcorrencias(Integer maxOcorrencias) {
        this.maxOcorrencias = maxOcorrencias;
    }

    /**
     * Get max ocorrencias.
     *
     * @return the max ocorrencias
     */
    public Integer getMaxOcorrencias() {
        return this.maxOcorrencias;
    }

    /**
     * Set cd solicitacao pagamento integrado.
     *
     * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
     */
    public void setCdSolicitacaoPagamentoIntegrado(Integer cdSolicitacaoPagamentoIntegrado) {
        this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
    }

    /**
     * Get cd solicitacao pagamento integrado.
     *
     * @return the cd solicitacao pagamento integrado
     */
    public Integer getCdSolicitacaoPagamentoIntegrado() {
        return this.cdSolicitacaoPagamentoIntegrado;
    }

    /**
     * Set nr solicitacao pagamento integrado.
     *
     * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
     */
    public void setNrSolicitacaoPagamentoIntegrado(Integer nrSolicitacaoPagamentoIntegrado) {
        this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
    }

    /**
     * Get nr solicitacao pagamento integrado.
     *
     * @return the nr solicitacao pagamento integrado
     */
    public Integer getNrSolicitacaoPagamentoIntegrado() {
        return this.nrSolicitacaoPagamentoIntegrado;
    }

    /**
     * Set cdpessoa juridica contrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Get cdpessoa juridica contrato.
     *
     * @return the cdpessoa juridica contrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return this.cdpessoaJuridicaContrato;
    }

    /**
     * Set cd tipo contrato negocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get cd tipo contrato negocio.
     *
     * @return the cd tipo contrato negocio
     */
    public Integer getCdTipoContratoNegocio() {
        return this.cdTipoContratoNegocio;
    }

    /**
     * Set nr sequencia contrato negocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get nr sequencia contrato negocio.
     *
     * @return the nr sequencia contrato negocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return this.nrSequenciaContratoNegocio;
    }
}