/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCriptonimoFuncionarioBradesco
     */
    private long _cdCriptonimoFuncionarioBradesco = 0;

    /**
     * keeps track of state for field:
     * _cdCriptonimoFuncionarioBradesco
     */
    private boolean _has_cdCriptonimoFuncionarioBradesco;

    /**
     * Field _dsCriptonimoFuncionarioBradesco
     */
    private java.lang.String _dsCriptonimoFuncionarioBradesco;

    /**
     * Field _cdJuncaoPertencente
     */
    private int _cdJuncaoPertencente = 0;

    /**
     * keeps track of state for field: _cdJuncaoPertencente
     */
    private boolean _has_cdJuncaoPertencente;

    /**
     * Field _cdSecao
     */
    private int _cdSecao = 0;

    /**
     * keeps track of state for field: _cdSecao
     */
    private boolean _has_cdSecao;

    /**
     * Field _cdCargo
     */
    private long _cdCargo = 0;

    /**
     * keeps track of state for field: _cdCargo
     */
    private boolean _has_cdCargo;

    /**
     * Field _dsCriptonimoCargo
     */
    private java.lang.String _dsCriptonimoCargo;

    /**
     * Field _cdCgcCpf
     */
    private long _cdCgcCpf = 0;

    /**
     * keeps track of state for field: _cdCgcCpf
     */
    private boolean _has_cdCgcCpf;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCargo
     * 
     */
    public void deleteCdCargo()
    {
        this._has_cdCargo= false;
    } //-- void deleteCdCargo() 

    /**
     * Method deleteCdCgcCpf
     * 
     */
    public void deleteCdCgcCpf()
    {
        this._has_cdCgcCpf= false;
    } //-- void deleteCdCgcCpf() 

    /**
     * Method deleteCdCriptonimoFuncionarioBradesco
     * 
     */
    public void deleteCdCriptonimoFuncionarioBradesco()
    {
        this._has_cdCriptonimoFuncionarioBradesco= false;
    } //-- void deleteCdCriptonimoFuncionarioBradesco() 

    /**
     * Method deleteCdJuncaoPertencente
     * 
     */
    public void deleteCdJuncaoPertencente()
    {
        this._has_cdJuncaoPertencente= false;
    } //-- void deleteCdJuncaoPertencente() 

    /**
     * Method deleteCdSecao
     * 
     */
    public void deleteCdSecao()
    {
        this._has_cdSecao= false;
    } //-- void deleteCdSecao() 

    /**
     * Returns the value of field 'cdCargo'.
     * 
     * @return long
     * @return the value of field 'cdCargo'.
     */
    public long getCdCargo()
    {
        return this._cdCargo;
    } //-- long getCdCargo() 

    /**
     * Returns the value of field 'cdCgcCpf'.
     * 
     * @return long
     * @return the value of field 'cdCgcCpf'.
     */
    public long getCdCgcCpf()
    {
        return this._cdCgcCpf;
    } //-- long getCdCgcCpf() 

    /**
     * Returns the value of field
     * 'cdCriptonimoFuncionarioBradesco'.
     * 
     * @return long
     * @return the value of field 'cdCriptonimoFuncionarioBradesco'.
     */
    public long getCdCriptonimoFuncionarioBradesco()
    {
        return this._cdCriptonimoFuncionarioBradesco;
    } //-- long getCdCriptonimoFuncionarioBradesco() 

    /**
     * Returns the value of field 'cdJuncaoPertencente'.
     * 
     * @return int
     * @return the value of field 'cdJuncaoPertencente'.
     */
    public int getCdJuncaoPertencente()
    {
        return this._cdJuncaoPertencente;
    } //-- int getCdJuncaoPertencente() 

    /**
     * Returns the value of field 'cdSecao'.
     * 
     * @return int
     * @return the value of field 'cdSecao'.
     */
    public int getCdSecao()
    {
        return this._cdSecao;
    } //-- int getCdSecao() 

    /**
     * Returns the value of field 'dsCriptonimoCargo'.
     * 
     * @return String
     * @return the value of field 'dsCriptonimoCargo'.
     */
    public java.lang.String getDsCriptonimoCargo()
    {
        return this._dsCriptonimoCargo;
    } //-- java.lang.String getDsCriptonimoCargo() 

    /**
     * Returns the value of field
     * 'dsCriptonimoFuncionarioBradesco'.
     * 
     * @return String
     * @return the value of field 'dsCriptonimoFuncionarioBradesco'.
     */
    public java.lang.String getDsCriptonimoFuncionarioBradesco()
    {
        return this._dsCriptonimoFuncionarioBradesco;
    } //-- java.lang.String getDsCriptonimoFuncionarioBradesco() 

    /**
     * Method hasCdCargo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCargo()
    {
        return this._has_cdCargo;
    } //-- boolean hasCdCargo() 

    /**
     * Method hasCdCgcCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCgcCpf()
    {
        return this._has_cdCgcCpf;
    } //-- boolean hasCdCgcCpf() 

    /**
     * Method hasCdCriptonimoFuncionarioBradesco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCriptonimoFuncionarioBradesco()
    {
        return this._has_cdCriptonimoFuncionarioBradesco;
    } //-- boolean hasCdCriptonimoFuncionarioBradesco() 

    /**
     * Method hasCdJuncaoPertencente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdJuncaoPertencente()
    {
        return this._has_cdJuncaoPertencente;
    } //-- boolean hasCdJuncaoPertencente() 

    /**
     * Method hasCdSecao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSecao()
    {
        return this._has_cdSecao;
    } //-- boolean hasCdSecao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCargo'.
     * 
     * @param cdCargo the value of field 'cdCargo'.
     */
    public void setCdCargo(long cdCargo)
    {
        this._cdCargo = cdCargo;
        this._has_cdCargo = true;
    } //-- void setCdCargo(long) 

    /**
     * Sets the value of field 'cdCgcCpf'.
     * 
     * @param cdCgcCpf the value of field 'cdCgcCpf'.
     */
    public void setCdCgcCpf(long cdCgcCpf)
    {
        this._cdCgcCpf = cdCgcCpf;
        this._has_cdCgcCpf = true;
    } //-- void setCdCgcCpf(long) 

    /**
     * Sets the value of field 'cdCriptonimoFuncionarioBradesco'.
     * 
     * @param cdCriptonimoFuncionarioBradesco the value of field
     * 'cdCriptonimoFuncionarioBradesco'.
     */
    public void setCdCriptonimoFuncionarioBradesco(long cdCriptonimoFuncionarioBradesco)
    {
        this._cdCriptonimoFuncionarioBradesco = cdCriptonimoFuncionarioBradesco;
        this._has_cdCriptonimoFuncionarioBradesco = true;
    } //-- void setCdCriptonimoFuncionarioBradesco(long) 

    /**
     * Sets the value of field 'cdJuncaoPertencente'.
     * 
     * @param cdJuncaoPertencente the value of field
     * 'cdJuncaoPertencente'.
     */
    public void setCdJuncaoPertencente(int cdJuncaoPertencente)
    {
        this._cdJuncaoPertencente = cdJuncaoPertencente;
        this._has_cdJuncaoPertencente = true;
    } //-- void setCdJuncaoPertencente(int) 

    /**
     * Sets the value of field 'cdSecao'.
     * 
     * @param cdSecao the value of field 'cdSecao'.
     */
    public void setCdSecao(int cdSecao)
    {
        this._cdSecao = cdSecao;
        this._has_cdSecao = true;
    } //-- void setCdSecao(int) 

    /**
     * Sets the value of field 'dsCriptonimoCargo'.
     * 
     * @param dsCriptonimoCargo the value of field
     * 'dsCriptonimoCargo'.
     */
    public void setDsCriptonimoCargo(java.lang.String dsCriptonimoCargo)
    {
        this._dsCriptonimoCargo = dsCriptonimoCargo;
    } //-- void setDsCriptonimoCargo(java.lang.String) 

    /**
     * Sets the value of field 'dsCriptonimoFuncionarioBradesco'.
     * 
     * @param dsCriptonimoFuncionarioBradesco the value of field
     * 'dsCriptonimoFuncionarioBradesco'.
     */
    public void setDsCriptonimoFuncionarioBradesco(java.lang.String dsCriptonimoFuncionarioBradesco)
    {
        this._dsCriptonimoFuncionarioBradesco = dsCriptonimoFuncionarioBradesco;
    } //-- void setDsCriptonimoFuncionarioBradesco(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistafuncionariobradesco.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
