/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

/**
 * Nome: ListarTipoLayoutContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarTipoLayoutContratoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo cdRelacionamentoProdutoProduto. */
	private Integer cdRelacionamentoProdutoProduto;
	
	/** Atributo cdTipoLayout. */
	private Integer cdTipoLayout;
	
	/** Atributo dsTipoLayout. */
	private String dsTipoLayout;
	
	/** Atributo cdNovoLayout. */
	private Integer cdNovoLayout;
	
	/** Atributo dsNovoLayout. */
	private String dsNovoLayout;
	
	/** Atributo check. */
	private boolean check;

	/** Atributo listaNovosLayouts. */
    private List<SelectItem> listaNovosLayouts = new ArrayList<SelectItem>();

	/**
	 * Get: cdRelacionamentoProdutoProduto.
	 *
	 * @return cdRelacionamentoProdutoProduto
	 */
	public Integer getCdRelacionamentoProdutoProduto() {
		return cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProdutoProduto.
	 *
	 * @param cdRelacionamentoProdutoProduto the cd relacionamento produto produto
	 */
	public void setCdRelacionamentoProdutoProduto(
			Integer cdRelacionamentoProdutoProduto) {
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Get: cdTipoLayout.
	 *
	 * @return cdTipoLayout
	 */
	public Integer getCdTipoLayout() {
		return cdTipoLayout;
	}
	
	/**
	 * Set: cdTipoLayout.
	 *
	 * @param cdTipoLayout the cd tipo layout
	 */
	public void setCdTipoLayout(Integer cdTipoLayout) {
		this.cdTipoLayout = cdTipoLayout;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsTipoLayout.
	 *
	 * @return dsTipoLayout
	 */
	public String getDsTipoLayout() {
		return dsTipoLayout;
	}
	
	/**
	 * Set: dsTipoLayout.
	 *
	 * @param dsTipoLayout the ds tipo layout
	 */
	public void setDsTipoLayout(String dsTipoLayout) {
		this.dsTipoLayout = dsTipoLayout;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	/**
	 * Get: cdNovoLayout.
	 *
	 * @return cdNovoLayout
	 */
	public Integer getCdNovoLayout() {
		return cdNovoLayout;
	}
	
	/**
	 * Set: cdNovoLayout.
	 *
	 * @param cdNovoLayout the cd novo layout
	 */
	public void setCdNovoLayout(Integer cdNovoLayout) {
		this.cdNovoLayout = cdNovoLayout;
	}
	
	/**
	 * Get: dsNovoLayout.
	 *
	 * @return dsNovoLayout
	 */
	public String getDsNovoLayout() {
		return dsNovoLayout;
	}
	
	/**
	 * Set: dsNovoLayout.
	 *
	 * @param dsNovoLayout the ds novo layout
	 */
	public void setDsNovoLayout(String dsNovoLayout) {
		this.dsNovoLayout = dsNovoLayout;
	}

    /**
     * Nome: getListaNovosLayouts
     *
     * @return listaNovosLayouts
     */
    public List<SelectItem> getListaNovosLayouts() {
        return listaNovosLayouts;
    }

    /**
     * Nome: setListaNovosLayouts
     *
     * @param listaNovosLayouts
     */
    public void setListaNovosLayouts(List<SelectItem> listaNovosLayouts) {
        this.listaNovosLayouts = listaNovosLayouts;
    }

}
