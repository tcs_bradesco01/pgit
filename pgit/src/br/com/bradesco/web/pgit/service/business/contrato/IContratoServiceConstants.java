/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 18/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato;

/**
 * Nome: IContratoServiceConstants
 * <p>
 * Prop�sito:
 * </p>
 * 
 * @author : todo!
 * 
 * @version :
 */
public interface IContratoServiceConstants {

    /** Atributo ERRO_IMPRESSAO_CONTRATO. */
    public static final String ERRO_IMPRESSAO_CONTRATO = "erro_impressao_contrato";
}
