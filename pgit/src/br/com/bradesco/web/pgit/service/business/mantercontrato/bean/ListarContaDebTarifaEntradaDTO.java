package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

public class ListarContaDebTarifaEntradaDTO {

    private Integer nrOcorrencias;
    private Long cdPessoaJuridicaNegocio;
    private Integer cdTipoContratoNegocio;
    private Long nrSequenciaContratoNegocio;
    private Long cdCorpoCpfCnpj;
    private Integer cdControleCpfCnpj;
    private Integer cdDigitoCpfCnpj;
    private Integer cdBanco;
    private Integer cdAgencia;
    private Integer cdDigitoAgencia;
    private Long cdConta;
    private String cdDigitoConta;
    private Integer cdTipoConta;
    
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	public Long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}
	public Integer getCdControleCpfCnpj() {
		return cdControleCpfCnpj;
	}
	public void setCdControleCpfCnpj(Integer cdControleCpfCnpj) {
		this.cdControleCpfCnpj = cdControleCpfCnpj;
	}
	public Integer getCdDigitoCpfCnpj() {
		return cdDigitoCpfCnpj;
	}
	public void setCdDigitoCpfCnpj(Integer cdDigitoCpfCnpj) {
		this.cdDigitoCpfCnpj = cdDigitoCpfCnpj;
	}
	public Integer getCdBanco() {
		return cdBanco;
	}
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	public Long getCdConta() {
		return cdConta;
	}
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
    
}
