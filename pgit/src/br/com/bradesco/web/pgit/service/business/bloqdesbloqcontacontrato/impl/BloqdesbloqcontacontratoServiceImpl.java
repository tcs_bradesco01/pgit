/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.impl;



import br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.IBloqdesbloqcontacontratoService;
import br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.bean.BloquearDesbloquearContaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.bean.BloquearDesbloquearContaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontacontrato.request.BloquearDesbloquearContaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontacontrato.response.BloquearDesbloquearContaContratoResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p> 
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: Bloqdesbloqcontacontrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class BloqdesbloqcontacontratoServiceImpl implements IBloqdesbloqcontacontratoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.IBloqdesbloqcontacontratoService#bloquearDesbloquearContaContrato(br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.bean.BloquearDesbloquearContaContratoEntradaDTO)
	 */
	public BloquearDesbloquearContaContratoSaidaDTO bloquearDesbloquearContaContrato(BloquearDesbloquearContaContratoEntradaDTO entradaDTO){
		
		BloquearDesbloquearContaContratoSaidaDTO saidaDTO = new BloquearDesbloquearContaContratoSaidaDTO();
		BloquearDesbloquearContaContratoRequest request = new BloquearDesbloquearContaContratoRequest();
		BloquearDesbloquearContaContratoResponse response = new BloquearDesbloquearContaContratoResponse();
		
		request.setCdBanco(entradaDTO.getCdBanco());
		request.setCdComercialAgenciaContabil(entradaDTO.getCdComercialAgenciaContabil());
		request.setCdConta(entradaDTO.getCdConta());
		request.setCdDigitoConta(entradaDTO.getCdDigitoConta());
		request.setCdMotivoBloqueioConta(entradaDTO.getCdMotivoBloqueioConta());
		request.setCdPessoaJuridicaContrato(entradaDTO.getCdPessoaJuridicaContrato());
		request.setCdPessoaJuridicaVinculo(entradaDTO.getCdPessoaJuridicaVinculo());
		request.setCdSituacaoContrato(entradaDTO.getCdSituacaoContrato());
		request.setCdTipoAcao(entradaDTO.getCdTipoAcao());
		request.setCdTipoContratoConta(entradaDTO.getCdTipoContratoConta());
		request.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		request.setCdTipoContratoVinculo(entradaDTO.getCdTipoContratoVinculo());
		request.setCdTipoVinculoContrato(entradaDTO.getCdTipoVinculoContrato());
		request.setDtInicioVinculacaoContrato(entradaDTO.getDtInicioVinculacaoContrato());
		request.setNrSequenciaContratoNegocio(entradaDTO.getNrSequenciaContratoNegocio());
		request.setNrSequenciaContratoVinculo(entradaDTO.getNrSequenciaContratoVinculo());
		
		response = getFactoryAdapter().getBloquearDesbloquearContaContratoPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}
	
	
	
}

