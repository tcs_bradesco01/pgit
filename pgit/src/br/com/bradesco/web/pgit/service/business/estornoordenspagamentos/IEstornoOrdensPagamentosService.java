/*
 * Nome: br.com.bradesco.web.pgit.service.business.estornoordenspagamentos
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.estornoordenspagamentos;

import br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean.EstornoOrdensPagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean.EstornoOrdensPagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean.IncluirEstornoOrdensPagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.estornoordenspagamentos.bean.IncluirEstornoOrdensPagamentosSaidaDTO;

/**
 * Nome: IEstornoOrdensPagamentosService
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface IEstornoOrdensPagamentosService {

	/**
	 * Consultar pagtos estorno op.
	 *
	 * @param entrada the entrada
	 * @return the estorno ordens pagamentos saida dto
	 */
	public EstornoOrdensPagamentosSaidaDTO consultarPagtosEstornoOP(EstornoOrdensPagamentosEntradaDTO entrada);

	/**
	 * Estornar pagamentos op.
	 *
	 * @param entrada the entrada
	 * @return the incluir estorno ordens pagamentos saida dto
	 */
	public IncluirEstornoOrdensPagamentosSaidaDTO estornarPagamentosOP(IncluirEstornoOrdensPagamentosEntradaDTO entrada);

}
