/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestorno.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarPagtosEstornoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarPagtosEstornoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoPesquisa
     */
    private int _cdTipoPesquisa = 0;

    /**
     * keeps track of state for field: _cdTipoPesquisa
     */
    private boolean _has_cdTipoPesquisa;

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _dgAgenciaBancaria
     */
    private int _dgAgenciaBancaria = 0;

    /**
     * keeps track of state for field: _dgAgenciaBancaria
     */
    private boolean _has_dgAgenciaBancaria;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _nrArquivoRemssaPagamento
     */
    private long _nrArquivoRemssaPagamento = 0;

    /**
     * keeps track of state for field: _nrArquivoRemssaPagamento
     */
    private boolean _has_nrArquivoRemssaPagamento;

    /**
     * Field _cdParticipante
     */
    private long _cdParticipante = 0;

    /**
     * keeps track of state for field: _cdParticipante
     */
    private boolean _has_cdParticipante;

    /**
     * Field _cdprodutoServicoOperacao
     */
    private int _cdprodutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdprodutoServicoOperacao
     */
    private boolean _has_cdprodutoServicoOperacao;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _cdControlePagamentoDe
     */
    private java.lang.String _cdControlePagamentoDe;

    /**
     * Field _vlPagamentoDe
     */
    private java.math.BigDecimal _vlPagamentoDe = new java.math.BigDecimal("0");

    /**
     * Field _vlPagamentoAte
     */
    private java.math.BigDecimal _vlPagamentoAte = new java.math.BigDecimal("0");

    /**
     * Field _cdFavorecidoClientePagador
     */
    private long _cdFavorecidoClientePagador = 0;

    /**
     * keeps track of state for field: _cdFavorecidoClientePagador
     */
    private boolean _has_cdFavorecidoClientePagador;

    /**
     * Field _cdInscricaoFavorecido
     */
    private long _cdInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecido
     */
    private boolean _has_cdInscricaoFavorecido;

    /**
     * Field _cdIdentificacaoInscricaoFavorecido
     */
    private int _cdIdentificacaoInscricaoFavorecido = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificacaoInscricaoFavorecido
     */
    private boolean _has_cdIdentificacaoInscricaoFavorecido;

    /**
     * Field _dtEstornoInicio
     */
    private java.lang.String _dtEstornoInicio;

    /**
     * Field _dtEstornoFim
     */
    private java.lang.String _dtEstornoFim;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarPagtosEstornoRequest() 
     {
        super();
        setVlPagamentoDe(new java.math.BigDecimal("0"));
        setVlPagamentoAte(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestorno.request.ConsultarPagtosEstornoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdFavorecidoClientePagador
     * 
     */
    public void deleteCdFavorecidoClientePagador()
    {
        this._has_cdFavorecidoClientePagador= false;
    } //-- void deleteCdFavorecidoClientePagador() 

    /**
     * Method deleteCdIdentificacaoInscricaoFavorecido
     * 
     */
    public void deleteCdIdentificacaoInscricaoFavorecido()
    {
        this._has_cdIdentificacaoInscricaoFavorecido= false;
    } //-- void deleteCdIdentificacaoInscricaoFavorecido() 

    /**
     * Method deleteCdInscricaoFavorecido
     * 
     */
    public void deleteCdInscricaoFavorecido()
    {
        this._has_cdInscricaoFavorecido= false;
    } //-- void deleteCdInscricaoFavorecido() 

    /**
     * Method deleteCdParticipante
     * 
     */
    public void deleteCdParticipante()
    {
        this._has_cdParticipante= false;
    } //-- void deleteCdParticipante() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoPesquisa
     * 
     */
    public void deleteCdTipoPesquisa()
    {
        this._has_cdTipoPesquisa= false;
    } //-- void deleteCdTipoPesquisa() 

    /**
     * Method deleteCdprodutoServicoOperacao
     * 
     */
    public void deleteCdprodutoServicoOperacao()
    {
        this._has_cdprodutoServicoOperacao= false;
    } //-- void deleteCdprodutoServicoOperacao() 

    /**
     * Method deleteDgAgenciaBancaria
     * 
     */
    public void deleteDgAgenciaBancaria()
    {
        this._has_dgAgenciaBancaria= false;
    } //-- void deleteDgAgenciaBancaria() 

    /**
     * Method deleteNrArquivoRemssaPagamento
     * 
     */
    public void deleteNrArquivoRemssaPagamento()
    {
        this._has_nrArquivoRemssaPagamento= false;
    } //-- void deleteNrArquivoRemssaPagamento() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdControlePagamentoDe'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamentoDe'.
     */
    public java.lang.String getCdControlePagamentoDe()
    {
        return this._cdControlePagamentoDe;
    } //-- java.lang.String getCdControlePagamentoDe() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdFavorecidoClientePagador'.
     * 
     * @return long
     * @return the value of field 'cdFavorecidoClientePagador'.
     */
    public long getCdFavorecidoClientePagador()
    {
        return this._cdFavorecidoClientePagador;
    } //-- long getCdFavorecidoClientePagador() 

    /**
     * Returns the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     */
    public int getCdIdentificacaoInscricaoFavorecido()
    {
        return this._cdIdentificacaoInscricaoFavorecido;
    } //-- int getCdIdentificacaoInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecido'.
     */
    public long getCdInscricaoFavorecido()
    {
        return this._cdInscricaoFavorecido;
    } //-- long getCdInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdParticipante'.
     * 
     * @return long
     * @return the value of field 'cdParticipante'.
     */
    public long getCdParticipante()
    {
        return this._cdParticipante;
    } //-- long getCdParticipante() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoPesquisa'.
     * 
     * @return int
     * @return the value of field 'cdTipoPesquisa'.
     */
    public int getCdTipoPesquisa()
    {
        return this._cdTipoPesquisa;
    } //-- int getCdTipoPesquisa() 

    /**
     * Returns the value of field 'cdprodutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdprodutoServicoOperacao'.
     */
    public int getCdprodutoServicoOperacao()
    {
        return this._cdprodutoServicoOperacao;
    } //-- int getCdprodutoServicoOperacao() 

    /**
     * Returns the value of field 'dgAgenciaBancaria'.
     * 
     * @return int
     * @return the value of field 'dgAgenciaBancaria'.
     */
    public int getDgAgenciaBancaria()
    {
        return this._dgAgenciaBancaria;
    } //-- int getDgAgenciaBancaria() 

    /**
     * Returns the value of field 'dtEstornoFim'.
     * 
     * @return String
     * @return the value of field 'dtEstornoFim'.
     */
    public java.lang.String getDtEstornoFim()
    {
        return this._dtEstornoFim;
    } //-- java.lang.String getDtEstornoFim() 

    /**
     * Returns the value of field 'dtEstornoInicio'.
     * 
     * @return String
     * @return the value of field 'dtEstornoInicio'.
     */
    public java.lang.String getDtEstornoInicio()
    {
        return this._dtEstornoInicio;
    } //-- java.lang.String getDtEstornoInicio() 

    /**
     * Returns the value of field 'nrArquivoRemssaPagamento'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRemssaPagamento'.
     */
    public long getNrArquivoRemssaPagamento()
    {
        return this._nrArquivoRemssaPagamento;
    } //-- long getNrArquivoRemssaPagamento() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Returns the value of field 'vlPagamentoAte'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoAte'.
     */
    public java.math.BigDecimal getVlPagamentoAte()
    {
        return this._vlPagamentoAte;
    } //-- java.math.BigDecimal getVlPagamentoAte() 

    /**
     * Returns the value of field 'vlPagamentoDe'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamentoDe'.
     */
    public java.math.BigDecimal getVlPagamentoDe()
    {
        return this._vlPagamentoDe;
    } //-- java.math.BigDecimal getVlPagamentoDe() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdFavorecidoClientePagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoClientePagador()
    {
        return this._has_cdFavorecidoClientePagador;
    } //-- boolean hasCdFavorecidoClientePagador() 

    /**
     * Method hasCdIdentificacaoInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificacaoInscricaoFavorecido()
    {
        return this._has_cdIdentificacaoInscricaoFavorecido;
    } //-- boolean hasCdIdentificacaoInscricaoFavorecido() 

    /**
     * Method hasCdInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecido()
    {
        return this._has_cdInscricaoFavorecido;
    } //-- boolean hasCdInscricaoFavorecido() 

    /**
     * Method hasCdParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParticipante()
    {
        return this._has_cdParticipante;
    } //-- boolean hasCdParticipante() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoPesquisa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoPesquisa()
    {
        return this._has_cdTipoPesquisa;
    } //-- boolean hasCdTipoPesquisa() 

    /**
     * Method hasCdprodutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdprodutoServicoOperacao()
    {
        return this._has_cdprodutoServicoOperacao;
    } //-- boolean hasCdprodutoServicoOperacao() 

    /**
     * Method hasDgAgenciaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasDgAgenciaBancaria()
    {
        return this._has_dgAgenciaBancaria;
    } //-- boolean hasDgAgenciaBancaria() 

    /**
     * Method hasNrArquivoRemssaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRemssaPagamento()
    {
        return this._has_nrArquivoRemssaPagamento;
    } //-- boolean hasNrArquivoRemssaPagamento() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdControlePagamentoDe'.
     * 
     * @param cdControlePagamentoDe the value of field
     * 'cdControlePagamentoDe'.
     */
    public void setCdControlePagamentoDe(java.lang.String cdControlePagamentoDe)
    {
        this._cdControlePagamentoDe = cdControlePagamentoDe;
    } //-- void setCdControlePagamentoDe(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdFavorecidoClientePagador'.
     * 
     * @param cdFavorecidoClientePagador the value of field
     * 'cdFavorecidoClientePagador'.
     */
    public void setCdFavorecidoClientePagador(long cdFavorecidoClientePagador)
    {
        this._cdFavorecidoClientePagador = cdFavorecidoClientePagador;
        this._has_cdFavorecidoClientePagador = true;
    } //-- void setCdFavorecidoClientePagador(long) 

    /**
     * Sets the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     * 
     * @param cdIdentificacaoInscricaoFavorecido the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     */
    public void setCdIdentificacaoInscricaoFavorecido(int cdIdentificacaoInscricaoFavorecido)
    {
        this._cdIdentificacaoInscricaoFavorecido = cdIdentificacaoInscricaoFavorecido;
        this._has_cdIdentificacaoInscricaoFavorecido = true;
    } //-- void setCdIdentificacaoInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdInscricaoFavorecido'.
     * 
     * @param cdInscricaoFavorecido the value of field
     * 'cdInscricaoFavorecido'.
     */
    public void setCdInscricaoFavorecido(long cdInscricaoFavorecido)
    {
        this._cdInscricaoFavorecido = cdInscricaoFavorecido;
        this._has_cdInscricaoFavorecido = true;
    } //-- void setCdInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'cdParticipante'.
     * 
     * @param cdParticipante the value of field 'cdParticipante'.
     */
    public void setCdParticipante(long cdParticipante)
    {
        this._cdParticipante = cdParticipante;
        this._has_cdParticipante = true;
    } //-- void setCdParticipante(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoPesquisa'.
     * 
     * @param cdTipoPesquisa the value of field 'cdTipoPesquisa'.
     */
    public void setCdTipoPesquisa(int cdTipoPesquisa)
    {
        this._cdTipoPesquisa = cdTipoPesquisa;
        this._has_cdTipoPesquisa = true;
    } //-- void setCdTipoPesquisa(int) 

    /**
     * Sets the value of field 'cdprodutoServicoOperacao'.
     * 
     * @param cdprodutoServicoOperacao the value of field
     * 'cdprodutoServicoOperacao'.
     */
    public void setCdprodutoServicoOperacao(int cdprodutoServicoOperacao)
    {
        this._cdprodutoServicoOperacao = cdprodutoServicoOperacao;
        this._has_cdprodutoServicoOperacao = true;
    } //-- void setCdprodutoServicoOperacao(int) 

    /**
     * Sets the value of field 'dgAgenciaBancaria'.
     * 
     * @param dgAgenciaBancaria the value of field
     * 'dgAgenciaBancaria'.
     */
    public void setDgAgenciaBancaria(int dgAgenciaBancaria)
    {
        this._dgAgenciaBancaria = dgAgenciaBancaria;
        this._has_dgAgenciaBancaria = true;
    } //-- void setDgAgenciaBancaria(int) 

    /**
     * Sets the value of field 'dtEstornoFim'.
     * 
     * @param dtEstornoFim the value of field 'dtEstornoFim'.
     */
    public void setDtEstornoFim(java.lang.String dtEstornoFim)
    {
        this._dtEstornoFim = dtEstornoFim;
    } //-- void setDtEstornoFim(java.lang.String) 

    /**
     * Sets the value of field 'dtEstornoInicio'.
     * 
     * @param dtEstornoInicio the value of field 'dtEstornoInicio'.
     */
    public void setDtEstornoInicio(java.lang.String dtEstornoInicio)
    {
        this._dtEstornoInicio = dtEstornoInicio;
    } //-- void setDtEstornoInicio(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRemssaPagamento'.
     * 
     * @param nrArquivoRemssaPagamento the value of field
     * 'nrArquivoRemssaPagamento'.
     */
    public void setNrArquivoRemssaPagamento(long nrArquivoRemssaPagamento)
    {
        this._nrArquivoRemssaPagamento = nrArquivoRemssaPagamento;
        this._has_nrArquivoRemssaPagamento = true;
    } //-- void setNrArquivoRemssaPagamento(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Sets the value of field 'vlPagamentoAte'.
     * 
     * @param vlPagamentoAte the value of field 'vlPagamentoAte'.
     */
    public void setVlPagamentoAte(java.math.BigDecimal vlPagamentoAte)
    {
        this._vlPagamentoAte = vlPagamentoAte;
    } //-- void setVlPagamentoAte(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlPagamentoDe'.
     * 
     * @param vlPagamentoDe the value of field 'vlPagamentoDe'.
     */
    public void setVlPagamentoDe(java.math.BigDecimal vlPagamentoDe)
    {
        this._vlPagamentoDe = vlPagamentoDe;
    } //-- void setVlPagamentoDe(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarPagtosEstornoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestorno.request.ConsultarPagtosEstornoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestorno.request.ConsultarPagtosEstornoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestorno.request.ConsultarPagtosEstornoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosestorno.request.ConsultarPagtosEstornoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
