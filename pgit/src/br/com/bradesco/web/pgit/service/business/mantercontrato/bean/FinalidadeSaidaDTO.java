/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: FinalidadeSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FinalidadeSaidaDTO {
	
	/** Atributo cdFinalidade. */
	private int cdFinalidade;
	
	/** Atributo dsFinalidade. */
	private String dsFinalidade;
	
	/** Atributo check. */
	private boolean check;
	
	/**
	 * Get: cdFinalidade.
	 *
	 * @return cdFinalidade
	 */
	public int getCdFinalidade() {
		return cdFinalidade;
	}
	
	/**
	 * Set: cdFinalidade.
	 *
	 * @param cdFinalidade the cd finalidade
	 */
	public void setCdFinalidade(int cdFinalidade) {
		this.cdFinalidade = cdFinalidade;
	}
	
	/**
	 * Get: dsFinalidade.
	 *
	 * @return dsFinalidade
	 */
	public String getDsFinalidade() {
		return dsFinalidade;
	}
	
	/**
	 * Set: dsFinalidade.
	 *
	 * @param dsFinalidade the ds finalidade
	 */
	public void setDsFinalidade(String dsFinalidade) {
		this.dsFinalidade = dsFinalidade;
	}
	
	/**
	 * Is check.
	 *
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * Set: check.
	 *
	 * @param check the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	
}
