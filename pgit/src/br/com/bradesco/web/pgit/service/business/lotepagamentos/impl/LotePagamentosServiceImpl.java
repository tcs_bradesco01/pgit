/**
 * Nome: br.com.bradesco.web.pgit.service.business.lotepagamentos.impl
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.lotepagamentos.impl;

import br.com.bradesco.web.pgit.service.business.lotepagamentos.ILotePagamentosService;

/**
 * Nome: LotePagamentosServiceImpl
 * <p>
 * Prop�sito: Implementa��o do adaptador LotePagamentos
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see ILotePagamentosService
 */
public class LotePagamentosServiceImpl implements ILotePagamentosService {

}