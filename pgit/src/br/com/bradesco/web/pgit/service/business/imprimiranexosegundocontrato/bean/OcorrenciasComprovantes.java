/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

/**
 * Nome: OcorrenciasComprovantes
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasComprovantes {
	
	/** Atributo cdComprovanteTipoComprovante. */
	private String cdComprovanteTipoComprovante;
	
	/** Atributo dsComprovantePeriodicidadeEmissao. */
	private String dsComprovantePeriodicidadeEmissao;
	
	/** Atributo dsComprovanteDestino. */
	private String dsComprovanteDestino;
	
	/** Atributo dsComprovantePermissaoCorrespondenteAbertura. */
	private String dsComprovantePermissaoCorrespondenteAbertura;
	
	/** Atributo dsDemonstrativoInformacaoReservada. */
	private String dsDemonstrativoInformacaoReservada;
	
	/** Atributo qtViasEmitir. */
	private Integer qtViasEmitir;
	
	/** Atributo dsAgrupamentoCorrespondente. */
	private String dsAgrupamentoCorrespondente;
	
	/**
	 * Get: cdComprovanteTipoComprovante.
	 *
	 * @return cdComprovanteTipoComprovante
	 */
	public String getCdComprovanteTipoComprovante() {
		return cdComprovanteTipoComprovante;
	}
	
	/**
	 * Set: cdComprovanteTipoComprovante.
	 *
	 * @param cdComprovanteTipoComprovante the cd comprovante tipo comprovante
	 */
	public void setCdComprovanteTipoComprovante(String cdComprovanteTipoComprovante) {
		this.cdComprovanteTipoComprovante = cdComprovanteTipoComprovante;
	}
	
	/**
	 * Get: dsComprovantePeriodicidadeEmissao.
	 *
	 * @return dsComprovantePeriodicidadeEmissao
	 */
	public String getDsComprovantePeriodicidadeEmissao() {
		return dsComprovantePeriodicidadeEmissao;
	}
	
	/**
	 * Set: dsComprovantePeriodicidadeEmissao.
	 *
	 * @param dsComprovantePeriodicidadeEmissao the ds comprovante periodicidade emissao
	 */
	public void setDsComprovantePeriodicidadeEmissao(
			String dsComprovantePeriodicidadeEmissao) {
		this.dsComprovantePeriodicidadeEmissao = dsComprovantePeriodicidadeEmissao;
	}
	
	/**
	 * Get: dsComprovanteDestino.
	 *
	 * @return dsComprovanteDestino
	 */
	public String getDsComprovanteDestino() {
		return dsComprovanteDestino;
	}
	
	/**
	 * Set: dsComprovanteDestino.
	 *
	 * @param dsComprovanteDestino the ds comprovante destino
	 */
	public void setDsComprovanteDestino(String dsComprovanteDestino) {
		this.dsComprovanteDestino = dsComprovanteDestino;
	}
	
	/**
	 * Get: dsComprovantePermissaoCorrespondenteAbertura.
	 *
	 * @return dsComprovantePermissaoCorrespondenteAbertura
	 */
	public String getDsComprovantePermissaoCorrespondenteAbertura() {
		return dsComprovantePermissaoCorrespondenteAbertura;
	}
	
	/**
	 * Set: dsComprovantePermissaoCorrespondenteAbertura.
	 *
	 * @param dsComprovantePermissaoCorrespondenteAbertura the ds comprovante permissao correspondente abertura
	 */
	public void setDsComprovantePermissaoCorrespondenteAbertura(
			String dsComprovantePermissaoCorrespondenteAbertura) {
		this.dsComprovantePermissaoCorrespondenteAbertura = dsComprovantePermissaoCorrespondenteAbertura;
	}
	
	/**
	 * Get: dsDemonstrativoInformacaoReservada.
	 *
	 * @return dsDemonstrativoInformacaoReservada
	 */
	public String getDsDemonstrativoInformacaoReservada() {
		return dsDemonstrativoInformacaoReservada;
	}
	
	/**
	 * Set: dsDemonstrativoInformacaoReservada.
	 *
	 * @param dsDemonstrativoInformacaoReservada the ds demonstrativo informacao reservada
	 */
	public void setDsDemonstrativoInformacaoReservada(
			String dsDemonstrativoInformacaoReservada) {
		this.dsDemonstrativoInformacaoReservada = dsDemonstrativoInformacaoReservada;
	}
	
	/**
	 * Get: qtViasEmitir.
	 *
	 * @return qtViasEmitir
	 */
	public Integer getQtViasEmitir() {
		return qtViasEmitir;
	}
	
	/**
	 * Set: qtViasEmitir.
	 *
	 * @param qtViasEmitir the qt vias emitir
	 */
	public void setQtViasEmitir(Integer qtViasEmitir) {
		this.qtViasEmitir = qtViasEmitir;
	}
	
	/**
	 * Get: dsAgrupamentoCorrespondente.
	 *
	 * @return dsAgrupamentoCorrespondente
	 */
	public String getDsAgrupamentoCorrespondente() {
		return dsAgrupamentoCorrespondente;
	}
	
	/**
	 * Set: dsAgrupamentoCorrespondente.
	 *
	 * @param dsAgrupamentoCorrespondente the ds agrupamento correspondente
	 */
	public void setDsAgrupamentoCorrespondente(String dsAgrupamentoCorrespondente) {
		this.dsAgrupamentoCorrespondente = dsAgrupamentoCorrespondente;
	}
     
}
