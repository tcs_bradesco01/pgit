/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarassversaolayoutloteservico.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarAssVersaoLayoutLoteServicoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarAssVersaoLayoutLoteServicoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _nrVersaoLayoutArquivo
     */
    private int _nrVersaoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _nrVersaoLayoutArquivo
     */
    private boolean _has_nrVersaoLayoutArquivo;

    /**
     * Field _cdTipoLoteLayout
     */
    private int _cdTipoLoteLayout = 0;

    /**
     * keeps track of state for field: _cdTipoLoteLayout
     */
    private boolean _has_cdTipoLoteLayout;

    /**
     * Field _dsTipoLoteLayout
     */
    private java.lang.String _dsTipoLoteLayout;

    /**
     * Field _nrVersaoLoteLayout
     */
    private int _nrVersaoLoteLayout = 0;

    /**
     * keeps track of state for field: _nrVersaoLoteLayout
     */
    private boolean _has_nrVersaoLoteLayout;

    /**
     * Field _cdTipoServicoCnab
     */
    private int _cdTipoServicoCnab = 0;

    /**
     * keeps track of state for field: _cdTipoServicoCnab
     */
    private boolean _has_cdTipoServicoCnab;

    /**
     * Field _dsTipoServicoCnab
     */
    private java.lang.String _dsTipoServicoCnab;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarAssVersaoLayoutLoteServicoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarassversaolayoutloteservico.response.ConsultarAssVersaoLayoutLoteServicoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoLoteLayout
     * 
     */
    public void deleteCdTipoLoteLayout()
    {
        this._has_cdTipoLoteLayout= false;
    } //-- void deleteCdTipoLoteLayout() 

    /**
     * Method deleteCdTipoServicoCnab
     * 
     */
    public void deleteCdTipoServicoCnab()
    {
        this._has_cdTipoServicoCnab= false;
    } //-- void deleteCdTipoServicoCnab() 

    /**
     * Method deleteNrVersaoLayoutArquivo
     * 
     */
    public void deleteNrVersaoLayoutArquivo()
    {
        this._has_nrVersaoLayoutArquivo= false;
    } //-- void deleteNrVersaoLayoutArquivo() 

    /**
     * Method deleteNrVersaoLoteLayout
     * 
     */
    public void deleteNrVersaoLoteLayout()
    {
        this._has_nrVersaoLoteLayout= false;
    } //-- void deleteNrVersaoLoteLayout() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoLoteLayout'.
     * 
     * @return int
     * @return the value of field 'cdTipoLoteLayout'.
     */
    public int getCdTipoLoteLayout()
    {
        return this._cdTipoLoteLayout;
    } //-- int getCdTipoLoteLayout() 

    /**
     * Returns the value of field 'cdTipoServicoCnab'.
     * 
     * @return int
     * @return the value of field 'cdTipoServicoCnab'.
     */
    public int getCdTipoServicoCnab()
    {
        return this._cdTipoServicoCnab;
    } //-- int getCdTipoServicoCnab() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsTipoLoteLayout'.
     * 
     * @return String
     * @return the value of field 'dsTipoLoteLayout'.
     */
    public java.lang.String getDsTipoLoteLayout()
    {
        return this._dsTipoLoteLayout;
    } //-- java.lang.String getDsTipoLoteLayout() 

    /**
     * Returns the value of field 'dsTipoServicoCnab'.
     * 
     * @return String
     * @return the value of field 'dsTipoServicoCnab'.
     */
    public java.lang.String getDsTipoServicoCnab()
    {
        return this._dsTipoServicoCnab;
    } //-- java.lang.String getDsTipoServicoCnab() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrVersaoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'nrVersaoLayoutArquivo'.
     */
    public int getNrVersaoLayoutArquivo()
    {
        return this._nrVersaoLayoutArquivo;
    } //-- int getNrVersaoLayoutArquivo() 

    /**
     * Returns the value of field 'nrVersaoLoteLayout'.
     * 
     * @return int
     * @return the value of field 'nrVersaoLoteLayout'.
     */
    public int getNrVersaoLoteLayout()
    {
        return this._nrVersaoLoteLayout;
    } //-- int getNrVersaoLoteLayout() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoLoteLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLoteLayout()
    {
        return this._has_cdTipoLoteLayout;
    } //-- boolean hasCdTipoLoteLayout() 

    /**
     * Method hasCdTipoServicoCnab
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoServicoCnab()
    {
        return this._has_cdTipoServicoCnab;
    } //-- boolean hasCdTipoServicoCnab() 

    /**
     * Method hasNrVersaoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrVersaoLayoutArquivo()
    {
        return this._has_nrVersaoLayoutArquivo;
    } //-- boolean hasNrVersaoLayoutArquivo() 

    /**
     * Method hasNrVersaoLoteLayout
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrVersaoLoteLayout()
    {
        return this._has_nrVersaoLoteLayout;
    } //-- boolean hasNrVersaoLoteLayout() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoLoteLayout'.
     * 
     * @param cdTipoLoteLayout the value of field 'cdTipoLoteLayout'
     */
    public void setCdTipoLoteLayout(int cdTipoLoteLayout)
    {
        this._cdTipoLoteLayout = cdTipoLoteLayout;
        this._has_cdTipoLoteLayout = true;
    } //-- void setCdTipoLoteLayout(int) 

    /**
     * Sets the value of field 'cdTipoServicoCnab'.
     * 
     * @param cdTipoServicoCnab the value of field
     * 'cdTipoServicoCnab'.
     */
    public void setCdTipoServicoCnab(int cdTipoServicoCnab)
    {
        this._cdTipoServicoCnab = cdTipoServicoCnab;
        this._has_cdTipoServicoCnab = true;
    } //-- void setCdTipoServicoCnab(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLoteLayout'.
     * 
     * @param dsTipoLoteLayout the value of field 'dsTipoLoteLayout'
     */
    public void setDsTipoLoteLayout(java.lang.String dsTipoLoteLayout)
    {
        this._dsTipoLoteLayout = dsTipoLoteLayout;
    } //-- void setDsTipoLoteLayout(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoServicoCnab'.
     * 
     * @param dsTipoServicoCnab the value of field
     * 'dsTipoServicoCnab'.
     */
    public void setDsTipoServicoCnab(java.lang.String dsTipoServicoCnab)
    {
        this._dsTipoServicoCnab = dsTipoServicoCnab;
    } //-- void setDsTipoServicoCnab(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrVersaoLayoutArquivo'.
     * 
     * @param nrVersaoLayoutArquivo the value of field
     * 'nrVersaoLayoutArquivo'.
     */
    public void setNrVersaoLayoutArquivo(int nrVersaoLayoutArquivo)
    {
        this._nrVersaoLayoutArquivo = nrVersaoLayoutArquivo;
        this._has_nrVersaoLayoutArquivo = true;
    } //-- void setNrVersaoLayoutArquivo(int) 

    /**
     * Sets the value of field 'nrVersaoLoteLayout'.
     * 
     * @param nrVersaoLoteLayout the value of field
     * 'nrVersaoLoteLayout'.
     */
    public void setNrVersaoLoteLayout(int nrVersaoLoteLayout)
    {
        this._nrVersaoLoteLayout = nrVersaoLoteLayout;
        this._has_nrVersaoLoteLayout = true;
    } //-- void setNrVersaoLoteLayout(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarAssVersaoLayoutLoteServicoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarassversaolayoutloteservico.response.ConsultarAssVersaoLayoutLoteServicoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarassversaolayoutloteservico.response.ConsultarAssVersaoLayoutLoteServicoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarassversaolayoutloteservico.response.ConsultarAssVersaoLayoutLoteServicoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarassversaolayoutloteservico.response.ConsultarAssVersaoLayoutLoteServicoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
