/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdFormaPagamento
     */
    private int _cdFormaPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaPagamento
     */
    private boolean _has_cdFormaPagamento;

    /**
     * Field _dsFormaPagamento
     */
    private java.lang.String _dsFormaPagamento;

    /**
     * Field _qtPagamento
     */
    private int _qtPagamento = 0;

    /**
     * keeps track of state for field: _qtPagamento
     */
    private boolean _has_qtPagamento;

    /**
     * Field _vlPagamento
     */
    private java.math.BigDecimal _vlPagamento = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFormaPagamento
     * 
     */
    public void deleteCdFormaPagamento()
    {
        this._has_cdFormaPagamento= false;
    } //-- void deleteCdFormaPagamento() 

    /**
     * Method deleteQtPagamento
     * 
     */
    public void deleteQtPagamento()
    {
        this._has_qtPagamento= false;
    } //-- void deleteQtPagamento() 

    /**
     * Returns the value of field 'cdFormaPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaPagamento'.
     */
    public int getCdFormaPagamento()
    {
        return this._cdFormaPagamento;
    } //-- int getCdFormaPagamento() 

    /**
     * Returns the value of field 'dsFormaPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaPagamento'.
     */
    public java.lang.String getDsFormaPagamento()
    {
        return this._dsFormaPagamento;
    } //-- java.lang.String getDsFormaPagamento() 

    /**
     * Returns the value of field 'qtPagamento'.
     * 
     * @return int
     * @return the value of field 'qtPagamento'.
     */
    public int getQtPagamento()
    {
        return this._qtPagamento;
    } //-- int getQtPagamento() 

    /**
     * Returns the value of field 'vlPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlPagamento'.
     */
    public java.math.BigDecimal getVlPagamento()
    {
        return this._vlPagamento;
    } //-- java.math.BigDecimal getVlPagamento() 

    /**
     * Method hasCdFormaPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaPagamento()
    {
        return this._has_cdFormaPagamento;
    } //-- boolean hasCdFormaPagamento() 

    /**
     * Method hasQtPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtPagamento()
    {
        return this._has_qtPagamento;
    } //-- boolean hasQtPagamento() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFormaPagamento'.
     * 
     * @param cdFormaPagamento the value of field 'cdFormaPagamento'
     */
    public void setCdFormaPagamento(int cdFormaPagamento)
    {
        this._cdFormaPagamento = cdFormaPagamento;
        this._has_cdFormaPagamento = true;
    } //-- void setCdFormaPagamento(int) 

    /**
     * Sets the value of field 'dsFormaPagamento'.
     * 
     * @param dsFormaPagamento the value of field 'dsFormaPagamento'
     */
    public void setDsFormaPagamento(java.lang.String dsFormaPagamento)
    {
        this._dsFormaPagamento = dsFormaPagamento;
    } //-- void setDsFormaPagamento(java.lang.String) 

    /**
     * Sets the value of field 'qtPagamento'.
     * 
     * @param qtPagamento the value of field 'qtPagamento'.
     */
    public void setQtPagamento(int qtPagamento)
    {
        this._qtPagamento = qtPagamento;
        this._has_qtPagamento = true;
    } //-- void setQtPagamento(int) 

    /**
     * Sets the value of field 'vlPagamento'.
     * 
     * @param vlPagamento the value of field 'vlPagamento'.
     */
    public void setVlPagamento(java.math.BigDecimal vlPagamento)
    {
        this._vlPagamento = vlPagamento;
    } //-- void setVlPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartiporetirada.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
