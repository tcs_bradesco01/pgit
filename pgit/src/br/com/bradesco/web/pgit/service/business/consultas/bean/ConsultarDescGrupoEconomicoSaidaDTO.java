/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultas.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultas.bean;

/**
 * Nome: ConsultarDescGrupoEconomicoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDescGrupoEconomicoSaidaDTO {

    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdGrupoEconomico. */
    private Long cdGrupoEconomico;
    
    /** Atributo dsGrupoEconomico. */
    private String dsGrupoEconomico;
    
    /** Atributo dsGrupoEconomicoFormatado. */
    private String dsGrupoEconomicoFormatado;
    
    /**
     * Get: cdGrupoEconomico.
     *
     * @return cdGrupoEconomico
     */
    public Long getCdGrupoEconomico() {
        return cdGrupoEconomico;
    }
    
    /**
     * Set: cdGrupoEconomico.
     *
     * @param cdGrupoEconomico the cd grupo economico
     */
    public void setCdGrupoEconomico(Long cdGrupoEconomico) {
        this.cdGrupoEconomico = cdGrupoEconomico;
    }
    
    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }
    
    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }
    
    /**
     * Get: dsGrupoEconomico.
     *
     * @return dsGrupoEconomico
     */
    public String getDsGrupoEconomico() {
        return dsGrupoEconomico;
    }
    
    /**
     * Set: dsGrupoEconomico.
     *
     * @param dsGrupoEconomico the ds grupo economico
     */
    public void setDsGrupoEconomico(String dsGrupoEconomico) {
        this.dsGrupoEconomico = dsGrupoEconomico;
    }
    
    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }
    
    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    
    /**
     * Get: dsGrupoEconomicoFormatado.
     *
     * @return dsGrupoEconomicoFormatado
     */
    public String getDsGrupoEconomicoFormatado() {
	if(getDsGrupoEconomico() != null){
	    setDsGrupoEconomicoFormatado(getCdGrupoEconomico() + " - " + getDsGrupoEconomico());
	}
	
        return dsGrupoEconomicoFormatado;
    }
    
    /**
     * Set: dsGrupoEconomicoFormatado.
     *
     * @param dsGrupoEconomicoFormatado the ds grupo economico formatado
     */
    public void setDsGrupoEconomicoFormatado(String dsGrupoEconomicoFormatado) {
        this.dsGrupoEconomicoFormatado = dsGrupoEconomicoFormatado;
    }
    
    
    
}
