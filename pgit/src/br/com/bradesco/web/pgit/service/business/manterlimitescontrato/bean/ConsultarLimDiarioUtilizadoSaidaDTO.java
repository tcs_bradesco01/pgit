/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;
import java.math.BigDecimal;

/**
 * Nome: ConsultarLimDiarioUtilizadoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarLimDiarioUtilizadoSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo dtAgendamentoPagamento. */
	private String dtAgendamentoPagamento;
	
	/** Atributo vlLimiteDiaUtilizado. */
	private BigDecimal vlLimiteDiaUtilizado;
	
	/** Atributo dtAgendamentoFormatada. */
	private String dtAgendamentoFormatada;

	/**
	 * Get: dtAgendamentoFormatada.
	 *
	 * @return dtAgendamentoFormatada
	 */
	public String getDtAgendamentoFormatada() {
	    return dtAgendamentoFormatada;
	}

	/**
	 * Set: dtAgendamentoFormatada.
	 *
	 * @param dtAgendamentoFormatada the dt agendamento formatada
	 */
	public void setDtAgendamentoFormatada(String dtAgendamentoFormatada) {
	    this.dtAgendamentoFormatada = dtAgendamentoFormatada;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: dtAgendamentoPagamento.
	 *
	 * @return dtAgendamentoPagamento
	 */
	public String getDtAgendamentoPagamento(){
		return dtAgendamentoPagamento;
	}

	/**
	 * Set: dtAgendamentoPagamento.
	 *
	 * @param dtAgendamentoPagamento the dt agendamento pagamento
	 */
	public void setDtAgendamentoPagamento(String dtAgendamentoPagamento){
		this.dtAgendamentoPagamento = dtAgendamentoPagamento;
	}

	/**
	 * Get: vlLimiteDiaUtilizado.
	 *
	 * @return vlLimiteDiaUtilizado
	 */
	public BigDecimal getVlLimiteDiaUtilizado(){
		return vlLimiteDiaUtilizado;
	}

	/**
	 * Set: vlLimiteDiaUtilizado.
	 *
	 * @param vlLimiteDiaUtilizado the vl limite dia utilizado
	 */
	public void setVlLimiteDiaUtilizado(BigDecimal vlLimiteDiaUtilizado){
		this.vlLimiteDiaUtilizado = vlLimiteDiaUtilizado;
	}
}