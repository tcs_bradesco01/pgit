/*
 * Nome: br.com.bradesco.web.pgit.service.business.solrecupcompexp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solrecupcompexp.bean;

import java.util.Date;

import br.com.bradesco.web.pgit.service.business.cmpi0000.bean.Cmpi0000Bean;
import br.com.bradesco.web.pgit.service.business.solrecupcompexp.ISolRecupCompExpService;

/**
 * Nome: SolRecupCompExpBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolRecupCompExpBean {
	
	/** Atributo codListaRadio. */
	private int codListaRadio;
	
	/** Atributo filtro. */
	private int filtro;
	
	/** Atributo mostraBotoes. */
	private boolean mostraBotoes;

	/** Atributo situacaoFiltro. */
	private String situacaoFiltro;
	
	/** Atributo periodoFiltro. */
	private String periodoFiltro;
	
	/** Atributo dataInicioPeriodoFiltro. */
	private Date dataInicioPeriodoFiltro;
	
	/** Atributo dataFimPeriodoFiltro. */
	private Date dataFimPeriodoFiltro;

	/** Atributo dataHora. */
	private String dataHora;
	
	/** Atributo terminal. */
	private String terminal;
	
	/** Atributo usuario. */
	private String usuario;
	
	
	
	
	/** Atributo cmpi0000Bean. */
	private Cmpi0000Bean cmpi0000Bean;	
	
	/** Atributo manterSolicitacaoRecuperacaoComprovanteExpiradoImpl. */
	private ISolRecupCompExpService manterSolicitacaoRecuperacaoComprovanteExpiradoImpl;
	

	//private List<SelectItem> listaSituacao = new ArrayList<SelectItem>();

	/**
	 * Get: codListaRadio.
	 *
	 * @return codListaRadio
	 */
	public int getCodListaRadio() {
		return codListaRadio;
	}

	/**
	 * Set: codListaRadio.
	 *
	 * @param codListaRadio the cod lista radio
	 */
	public void setCodListaRadio(int codListaRadio) {
		this.codListaRadio = codListaRadio;
	}

	/**
	 * Get: dataFimPeriodoFiltro.
	 *
	 * @return dataFimPeriodoFiltro
	 */
	public Date getDataFimPeriodoFiltro() {
		if (dataFimPeriodoFiltro == null){
			dataFimPeriodoFiltro = new Date(); 
		}
		return dataFimPeriodoFiltro;
	}

	/**
	 * Set: dataFimPeriodoFiltro.
	 *
	 * @param dataFimPeriodoFiltro the data fim periodo filtro
	 */
	public void setDataFimPeriodoFiltro(Date dataFimPeriodoFiltro) {
		this.dataFimPeriodoFiltro = dataFimPeriodoFiltro;
	}

	/**
	 * Get: dataInicioPeriodoFiltro.
	 *
	 * @return dataInicioPeriodoFiltro
	 */
	public Date getDataInicioPeriodoFiltro() {
		if (dataInicioPeriodoFiltro == null){
			dataInicioPeriodoFiltro = new Date(); 
		}
		return dataInicioPeriodoFiltro;
	}

	/**
	 * Set: dataInicioPeriodoFiltro.
	 *
	 * @param dataInicioPeriodoFiltro the data inicio periodo filtro
	 */
	public void setDataInicioPeriodoFiltro(Date dataInicioPeriodoFiltro) {
		this.dataInicioPeriodoFiltro = dataInicioPeriodoFiltro;
	}

	/**
	 * Get: filtro.
	 *
	 * @return filtro
	 */
	public int getFiltro() {
		return filtro;
	}

	/**
	 * Set: filtro.
	 *
	 * @param filtro the filtro
	 */
	public void setFiltro(int filtro) {
		this.filtro = filtro;
	}


	/**
	 * Get: periodoFiltro.
	 *
	 * @return periodoFiltro
	 */
	public String getPeriodoFiltro() {
		return periodoFiltro;
	}

	/**
	 * Set: periodoFiltro.
	 *
	 * @param periodoFiltro the periodo filtro
	 */
	public void setPeriodoFiltro(String periodoFiltro) {
		this.periodoFiltro = periodoFiltro;
	}

	/**
	 * Get: situacaoFiltro.
	 *
	 * @return situacaoFiltro
	 */
	public String getSituacaoFiltro() {
		return situacaoFiltro;
	}

	/**
	 * Set: situacaoFiltro.
	 *
	 * @param situacaoFiltro the situacao filtro
	 */
	public void setSituacaoFiltro(String situacaoFiltro) {
		this.situacaoFiltro = situacaoFiltro;
	}
	
	/*public List<SelectItem> getListaSituacao() {
		return listaSituacao;
	}

	public void setListaSituacao(List<SelectItem> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}*/
	
	/**
	 * Consultar cliente.
	 *
	 * @return the string
	 */
	public String consultarCliente(){		
		cmpi0000Bean.setTela("MANTER_SOLIC_REC_COMPROV_EXP");		
		if (filtro == 1){
			cmpi0000Bean.limparListaClientes();
			return "CONSULTAR_CLIENTE";
		}
		return "OK";
	}
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados(){
		this.cmpi0000Bean.setCpf("");
		this.cmpi0000Bean.setNomeRazao("");
		setPeriodoFiltro("");
		setDataInicioPeriodoFiltro(null);
		setDataFimPeriodoFiltro(null);
		
		
		setFiltro(0);
		setCodListaRadio(0);
		
		
		return "OK";
	}

	/**
	 * Get: cmpi0000Bean.
	 *
	 * @return cmpi0000Bean
	 */
	public Cmpi0000Bean getCmpi0000Bean() {
		return cmpi0000Bean;
	}

	/**
	 * Set: cmpi0000Bean.
	 *
	 * @param cmpi0000Bean the cmpi0000 bean
	 */
	public void setCmpi0000Bean(Cmpi0000Bean cmpi0000Bean) {
		this.cmpi0000Bean = cmpi0000Bean;
	}

	/**
	 * Get: manterSolicitacaoRecuperacaoComprovanteExpiradoImpl.
	 *
	 * @return manterSolicitacaoRecuperacaoComprovanteExpiradoImpl
	 */
	public ISolRecupCompExpService getManterSolicitacaoRecuperacaoComprovanteExpiradoImpl() {
		return manterSolicitacaoRecuperacaoComprovanteExpiradoImpl;
	}

	/**
	 * Set: manterSolicitacaoRecuperacaoComprovanteExpiradoImpl.
	 *
	 * @param manterSolicitacaoRecuperacaoComprovanteExpiradoImpl the manter solicitacao recuperacao comprovante expirado impl
	 */
	public void setManterSolicitacaoRecuperacaoComprovanteExpiradoImpl(
			ISolRecupCompExpService manterSolicitacaoRecuperacaoComprovanteExpiradoImpl) {
		this.manterSolicitacaoRecuperacaoComprovanteExpiradoImpl = manterSolicitacaoRecuperacaoComprovanteExpiradoImpl;
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){			
		return "AVANCAR_DETALHAR";
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){			
		return "AVANCAR_INCLUIR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){			
		return "AVANCAR_EXCLUIR";
	}
	
	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar(){			
		return "VOLTAR_CONSULTAR";
	}
	
	/**
	 * Confirmar inclusao.
	 *
	 * @return the string
	 */
	public String confirmarInclusao(){			
		return "ok";
	}
	
	/**
	 * Limpar tela.
	 *
	 * @return the string
	 */
	public String limparTela(){			
		return "ok";
	}
	
	/**
	 * Confirmar exclusao.
	 *
	 * @return the string
	 */
	public String confirmarExclusao(){			
		return "ok";
	}

	/**
	 * Is mostra botoes.
	 *
	 * @return true, if is mostra botoes
	 */
	public boolean isMostraBotoes() {
		return mostraBotoes;
	}

	/**
	 * Set: mostraBotoes.
	 *
	 * @param mostraBotoes the mostra botoes
	 */
	public void setMostraBotoes(boolean mostraBotoes) {
		this.mostraBotoes = mostraBotoes;
	}

	/**
	 * Get: dataHora.
	 *
	 * @return dataHora
	 */
	public String getDataHora() {
		return dataHora;
	}

	/**
	 * Set: dataHora.
	 *
	 * @param dataHora the data hora
	 */
	public void setDataHora(String dataHora) {
		this.dataHora = dataHora;
	}

	/**
	 * Get: terminal.
	 *
	 * @return terminal
	 */
	public String getTerminal() {
		return terminal;
	}

	/**
	 * Set: terminal.
	 *
	 * @param terminal the terminal
	 */
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	/**
	 * Get: usuario.
	 *
	 * @return usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * Set: usuario.
	 *
	 * @param usuario the usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

}
