/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarlinhamsgcomprovante.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdControleMensagemSalarial
     */
    private int _cdControleMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdControleMensagemSalarial
     */
    private boolean _has_cdControleMensagemSalarial;

    /**
     * Field _dsControleMensagemSalarial
     */
    private java.lang.String _dsControleMensagemSalarial;

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _nrEventoMensagemNegocio
     */
    private int _nrEventoMensagemNegocio = 0;

    /**
     * keeps track of state for field: _nrEventoMensagemNegocio
     */
    private boolean _has_nrEventoMensagemNegocio;

    /**
     * Field _dsEventoMensagemNegocio
     */
    private java.lang.String _dsEventoMensagemNegocio;

    /**
     * Field _cdReciboGeradorMensagem
     */
    private int _cdReciboGeradorMensagem = 0;

    /**
     * keeps track of state for field: _cdReciboGeradorMensagem
     */
    private boolean _has_cdReciboGeradorMensagem;

    /**
     * Field _dsReciboGeradorMensagem
     */
    private java.lang.String _dsReciboGeradorMensagem;

    /**
     * Field _cdIdiomaTextoMensagem
     */
    private int _cdIdiomaTextoMensagem = 0;

    /**
     * keeps track of state for field: _cdIdiomaTextoMensagem
     */
    private boolean _has_cdIdiomaTextoMensagem;

    /**
     * Field _dsIdiomaTextoMensagem
     */
    private java.lang.String _dsIdiomaTextoMensagem;

    /**
     * Field _nrOrdemLinhaMensagem
     */
    private long _nrOrdemLinhaMensagem = 0;

    /**
     * keeps track of state for field: _nrOrdemLinhaMensagem
     */
    private boolean _has_nrOrdemLinhaMensagem;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarlinhamsgcomprovante.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdControleMensagemSalarial
     * 
     */
    public void deleteCdControleMensagemSalarial()
    {
        this._has_cdControleMensagemSalarial= false;
    } //-- void deleteCdControleMensagemSalarial() 

    /**
     * Method deleteCdIdiomaTextoMensagem
     * 
     */
    public void deleteCdIdiomaTextoMensagem()
    {
        this._has_cdIdiomaTextoMensagem= false;
    } //-- void deleteCdIdiomaTextoMensagem() 

    /**
     * Method deleteCdReciboGeradorMensagem
     * 
     */
    public void deleteCdReciboGeradorMensagem()
    {
        this._has_cdReciboGeradorMensagem= false;
    } //-- void deleteCdReciboGeradorMensagem() 

    /**
     * Method deleteNrEventoMensagemNegocio
     * 
     */
    public void deleteNrEventoMensagemNegocio()
    {
        this._has_nrEventoMensagemNegocio= false;
    } //-- void deleteNrEventoMensagemNegocio() 

    /**
     * Method deleteNrOrdemLinhaMensagem
     * 
     */
    public void deleteNrOrdemLinhaMensagem()
    {
        this._has_nrOrdemLinhaMensagem= false;
    } //-- void deleteNrOrdemLinhaMensagem() 

    /**
     * Returns the value of field 'cdControleMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdControleMensagemSalarial'.
     */
    public int getCdControleMensagemSalarial()
    {
        return this._cdControleMensagemSalarial;
    } //-- int getCdControleMensagemSalarial() 

    /**
     * Returns the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @return int
     * @return the value of field 'cdIdiomaTextoMensagem'.
     */
    public int getCdIdiomaTextoMensagem()
    {
        return this._cdIdiomaTextoMensagem;
    } //-- int getCdIdiomaTextoMensagem() 

    /**
     * Returns the value of field 'cdReciboGeradorMensagem'.
     * 
     * @return int
     * @return the value of field 'cdReciboGeradorMensagem'.
     */
    public int getCdReciboGeradorMensagem()
    {
        return this._cdReciboGeradorMensagem;
    } //-- int getCdReciboGeradorMensagem() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'dsControleMensagemSalarial'.
     * 
     * @return String
     * @return the value of field 'dsControleMensagemSalarial'.
     */
    public java.lang.String getDsControleMensagemSalarial()
    {
        return this._dsControleMensagemSalarial;
    } //-- java.lang.String getDsControleMensagemSalarial() 

    /**
     * Returns the value of field 'dsEventoMensagemNegocio'.
     * 
     * @return String
     * @return the value of field 'dsEventoMensagemNegocio'.
     */
    public java.lang.String getDsEventoMensagemNegocio()
    {
        return this._dsEventoMensagemNegocio;
    } //-- java.lang.String getDsEventoMensagemNegocio() 

    /**
     * Returns the value of field 'dsIdiomaTextoMensagem'.
     * 
     * @return String
     * @return the value of field 'dsIdiomaTextoMensagem'.
     */
    public java.lang.String getDsIdiomaTextoMensagem()
    {
        return this._dsIdiomaTextoMensagem;
    } //-- java.lang.String getDsIdiomaTextoMensagem() 

    /**
     * Returns the value of field 'dsReciboGeradorMensagem'.
     * 
     * @return String
     * @return the value of field 'dsReciboGeradorMensagem'.
     */
    public java.lang.String getDsReciboGeradorMensagem()
    {
        return this._dsReciboGeradorMensagem;
    } //-- java.lang.String getDsReciboGeradorMensagem() 

    /**
     * Returns the value of field 'nrEventoMensagemNegocio'.
     * 
     * @return int
     * @return the value of field 'nrEventoMensagemNegocio'.
     */
    public int getNrEventoMensagemNegocio()
    {
        return this._nrEventoMensagemNegocio;
    } //-- int getNrEventoMensagemNegocio() 

    /**
     * Returns the value of field 'nrOrdemLinhaMensagem'.
     * 
     * @return long
     * @return the value of field 'nrOrdemLinhaMensagem'.
     */
    public long getNrOrdemLinhaMensagem()
    {
        return this._nrOrdemLinhaMensagem;
    } //-- long getNrOrdemLinhaMensagem() 

    /**
     * Method hasCdControleMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleMensagemSalarial()
    {
        return this._has_cdControleMensagemSalarial;
    } //-- boolean hasCdControleMensagemSalarial() 

    /**
     * Method hasCdIdiomaTextoMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdiomaTextoMensagem()
    {
        return this._has_cdIdiomaTextoMensagem;
    } //-- boolean hasCdIdiomaTextoMensagem() 

    /**
     * Method hasCdReciboGeradorMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdReciboGeradorMensagem()
    {
        return this._has_cdReciboGeradorMensagem;
    } //-- boolean hasCdReciboGeradorMensagem() 

    /**
     * Method hasNrEventoMensagemNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrEventoMensagemNegocio()
    {
        return this._has_nrEventoMensagemNegocio;
    } //-- boolean hasNrEventoMensagemNegocio() 

    /**
     * Method hasNrOrdemLinhaMensagem
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOrdemLinhaMensagem()
    {
        return this._has_nrOrdemLinhaMensagem;
    } //-- boolean hasNrOrdemLinhaMensagem() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdControleMensagemSalarial'.
     * 
     * @param cdControleMensagemSalarial the value of field
     * 'cdControleMensagemSalarial'.
     */
    public void setCdControleMensagemSalarial(int cdControleMensagemSalarial)
    {
        this._cdControleMensagemSalarial = cdControleMensagemSalarial;
        this._has_cdControleMensagemSalarial = true;
    } //-- void setCdControleMensagemSalarial(int) 

    /**
     * Sets the value of field 'cdIdiomaTextoMensagem'.
     * 
     * @param cdIdiomaTextoMensagem the value of field
     * 'cdIdiomaTextoMensagem'.
     */
    public void setCdIdiomaTextoMensagem(int cdIdiomaTextoMensagem)
    {
        this._cdIdiomaTextoMensagem = cdIdiomaTextoMensagem;
        this._has_cdIdiomaTextoMensagem = true;
    } //-- void setCdIdiomaTextoMensagem(int) 

    /**
     * Sets the value of field 'cdReciboGeradorMensagem'.
     * 
     * @param cdReciboGeradorMensagem the value of field
     * 'cdReciboGeradorMensagem'.
     */
    public void setCdReciboGeradorMensagem(int cdReciboGeradorMensagem)
    {
        this._cdReciboGeradorMensagem = cdReciboGeradorMensagem;
        this._has_cdReciboGeradorMensagem = true;
    } //-- void setCdReciboGeradorMensagem(int) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'dsControleMensagemSalarial'.
     * 
     * @param dsControleMensagemSalarial the value of field
     * 'dsControleMensagemSalarial'.
     */
    public void setDsControleMensagemSalarial(java.lang.String dsControleMensagemSalarial)
    {
        this._dsControleMensagemSalarial = dsControleMensagemSalarial;
    } //-- void setDsControleMensagemSalarial(java.lang.String) 

    /**
     * Sets the value of field 'dsEventoMensagemNegocio'.
     * 
     * @param dsEventoMensagemNegocio the value of field
     * 'dsEventoMensagemNegocio'.
     */
    public void setDsEventoMensagemNegocio(java.lang.String dsEventoMensagemNegocio)
    {
        this._dsEventoMensagemNegocio = dsEventoMensagemNegocio;
    } //-- void setDsEventoMensagemNegocio(java.lang.String) 

    /**
     * Sets the value of field 'dsIdiomaTextoMensagem'.
     * 
     * @param dsIdiomaTextoMensagem the value of field
     * 'dsIdiomaTextoMensagem'.
     */
    public void setDsIdiomaTextoMensagem(java.lang.String dsIdiomaTextoMensagem)
    {
        this._dsIdiomaTextoMensagem = dsIdiomaTextoMensagem;
    } //-- void setDsIdiomaTextoMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsReciboGeradorMensagem'.
     * 
     * @param dsReciboGeradorMensagem the value of field
     * 'dsReciboGeradorMensagem'.
     */
    public void setDsReciboGeradorMensagem(java.lang.String dsReciboGeradorMensagem)
    {
        this._dsReciboGeradorMensagem = dsReciboGeradorMensagem;
    } //-- void setDsReciboGeradorMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrEventoMensagemNegocio'.
     * 
     * @param nrEventoMensagemNegocio the value of field
     * 'nrEventoMensagemNegocio'.
     */
    public void setNrEventoMensagemNegocio(int nrEventoMensagemNegocio)
    {
        this._nrEventoMensagemNegocio = nrEventoMensagemNegocio;
        this._has_nrEventoMensagemNegocio = true;
    } //-- void setNrEventoMensagemNegocio(int) 

    /**
     * Sets the value of field 'nrOrdemLinhaMensagem'.
     * 
     * @param nrOrdemLinhaMensagem the value of field
     * 'nrOrdemLinhaMensagem'.
     */
    public void setNrOrdemLinhaMensagem(long nrOrdemLinhaMensagem)
    {
        this._nrOrdemLinhaMensagem = nrOrdemLinhaMensagem;
        this._has_nrOrdemLinhaMensagem = true;
    } //-- void setNrOrdemLinhaMensagem(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarlinhamsgcomprovante.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarlinhamsgcomprovante.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarlinhamsgcomprovante.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarlinhamsgcomprovante.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
