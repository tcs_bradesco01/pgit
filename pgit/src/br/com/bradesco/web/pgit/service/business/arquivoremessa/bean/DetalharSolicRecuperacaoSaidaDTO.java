/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoremessa.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoremessa.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: DetalharSolicRecuperacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolicRecuperacaoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo hrSolicitacaoRecuperacao. */
    private String hrSolicitacaoRecuperacao;
   
    /** Atributo cdUsuarioSolicitacao. */
    private String cdUsuarioSolicitacao;
    
    /** Atributo dsSituacaoRecuperacao. */
    private String dsSituacaoRecuperacao;
    
    /** Atributo dsMotvtoSituacaoRecuperacao. */
    private String dsMotvtoSituacaoRecuperacao;
    
    /** Atributo dsDestinoRelatorio. */
    private String dsDestinoRelatorio;
    
    /** Atributo dsDestinoArqIsd. */
    private String dsDestinoArqIsd;
    
    /** Atributo dsDestinoArquivoRetorno. */
    private String dsDestinoArquivoRetorno;
    
    /** Atributo cdCanalInclusao. */
    private Integer cdCanalInclusao;    
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdAutenticacaoSegregacaoInclusao. */
    private String cdAutenticacaoSegregacaoInclusao;
    
    /** Atributo nmOperacaoFluxoInclusao. */
    private String nmOperacaoFluxoInclusao;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdCanalManutencao. */
    private Integer cdCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
    /** Atributo cdAutenticacaoSegregacaoManutencao. */
    private String cdAutenticacaoSegregacaoManutencao;
    
    /** Atributo nmOperacaoFluxoManutencao. */
    private String nmOperacaoFluxoManutencao;
    
    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;
    
    /** Atributo nrSolicitacao. */
    private Integer nrSolicitacao;    
    
    /** Atributo dataFormatada. */
    private String dataFormatada;
    
    /** Atributo dataFormatadaInclusaoRemessa. */
    private String dataFormatadaInclusaoRemessa;
    
    /** Atributo dataFormatadaManutencaoRemessa. */
    private String dataFormatadaManutencaoRemessa;
    
    /** Atributo listaDetalharSolicRecuperacao. */
    private List<OcorrenciasDetalharSolicRecuperacaoSaidaDTO> listaDetalharSolicRecuperacao = new ArrayList<OcorrenciasDetalharSolicRecuperacaoSaidaDTO>();
    
	/**
	 * Get: cdAutenticacaoSegregacaoInclusao.
	 *
	 * @return cdAutenticacaoSegregacaoInclusao
	 */
	public String getCdAutenticacaoSegregacaoInclusao() {
		return cdAutenticacaoSegregacaoInclusao;
	}
	
	/**
	 * Set: cdAutenticacaoSegregacaoInclusao.
	 *
	 * @param cdAutenticacaoSegregacaoInclusao the cd autenticacao segregacao inclusao
	 */
	public void setCdAutenticacaoSegregacaoInclusao(
			String cdAutenticacaoSegregacaoInclusao) {
		this.cdAutenticacaoSegregacaoInclusao = cdAutenticacaoSegregacaoInclusao;
	}
	
	/**
	 * Get: dataFormatada.
	 *
	 * @return dataFormatada
	 */
	public String getDataFormatada() {
		return dataFormatada;
	}
	
	/**
	 * Set: dataFormatada.
	 *
	 * @param dataFormatada the data formatada
	 */
	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}
	
	/**
	 * Get: dataFormatadaInclusaoRemessa.
	 *
	 * @return dataFormatadaInclusaoRemessa
	 */
	public String getDataFormatadaInclusaoRemessa() {
		return dataFormatadaInclusaoRemessa;
	}
	
	/**
	 * Set: dataFormatadaInclusaoRemessa.
	 *
	 * @param dataFormatadaInclusaoRemessa the data formatada inclusao remessa
	 */
	public void setDataFormatadaInclusaoRemessa(String dataFormatadaInclusaoRemessa) {
		this.dataFormatadaInclusaoRemessa = dataFormatadaInclusaoRemessa;
	}
	
	/**
	 * Get: cdAutenticacaoSegregacaoManutencao.
	 *
	 * @return cdAutenticacaoSegregacaoManutencao
	 */
	public String getCdAutenticacaoSegregacaoManutencao() {
		return cdAutenticacaoSegregacaoManutencao;
	}
	
	/**
	 * Set: cdAutenticacaoSegregacaoManutencao.
	 *
	 * @param cdAutenticacaoSegregacaoManutencao the cd autenticacao segregacao manutencao
	 */
	public void setCdAutenticacaoSegregacaoManutencao(
			String cdAutenticacaoSegregacaoManutencao) {
		this.cdAutenticacaoSegregacaoManutencao = cdAutenticacaoSegregacaoManutencao;
	}
	

	/**
	 * Get: dataFormatadaManutencaoRemessa.
	 *
	 * @return dataFormatadaManutencaoRemessa
	 */
	public String getDataFormatadaManutencaoRemessa() {
		return dataFormatadaManutencaoRemessa;
	}
	
	/**
	 * Set: dataFormatadaManutencaoRemessa.
	 *
	 * @param dataFormatadaManutencaoRemessa the data formatada manutencao remessa
	 */
	public void setDataFormatadaManutencaoRemessa(
			String dataFormatadaManutencaoRemessa) {
		this.dataFormatadaManutencaoRemessa = dataFormatadaManutencaoRemessa;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: cdUsuarioSolicitacao.
	 *
	 * @return cdUsuarioSolicitacao
	 */
	public String getCdUsuarioSolicitacao() {
		return cdUsuarioSolicitacao;
	}
	
	/**
	 * Set: cdUsuarioSolicitacao.
	 *
	 * @param cdUsuarioSolicitacao the cd usuario solicitacao
	 */
	public void setCdUsuarioSolicitacao(String cdUsuarioSolicitacao) {
		this.cdUsuarioSolicitacao = cdUsuarioSolicitacao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: dsDestinoArqIsd.
	 *
	 * @return dsDestinoArqIsd
	 */
	public String getDsDestinoArqIsd() {
		return dsDestinoArqIsd;
	}
	
	/**
	 * Set: dsDestinoArqIsd.
	 *
	 * @param dsDestinoArqIsd the ds destino arq isd
	 */
	public void setDsDestinoArqIsd(String dsDestinoArqIsd) {
		this.dsDestinoArqIsd = dsDestinoArqIsd;
	}
	
	/**
	 * Get: dsDestinoArquivoRetorno.
	 *
	 * @return dsDestinoArquivoRetorno
	 */
	public String getDsDestinoArquivoRetorno() {
		return dsDestinoArquivoRetorno;
	}
	
	/**
	 * Set: dsDestinoArquivoRetorno.
	 *
	 * @param dsDestinoArquivoRetorno the ds destino arquivo retorno
	 */
	public void setDsDestinoArquivoRetorno(String dsDestinoArquivoRetorno) {
		this.dsDestinoArquivoRetorno = dsDestinoArquivoRetorno;
	}
	
	/**
	 * Get: dsDestinoRelatorio.
	 *
	 * @return dsDestinoRelatorio
	 */
	public String getDsDestinoRelatorio() {
		return dsDestinoRelatorio;
	}
	
	/**
	 * Set: dsDestinoRelatorio.
	 *
	 * @param dsDestinoRelatorio the ds destino relatorio
	 */
	public void setDsDestinoRelatorio(String dsDestinoRelatorio) {
		this.dsDestinoRelatorio = dsDestinoRelatorio;
	}
	
	/**
	 * Get: dsMotvtoSituacaoRecuperacao.
	 *
	 * @return dsMotvtoSituacaoRecuperacao
	 */
	public String getDsMotvtoSituacaoRecuperacao() {
		return dsMotvtoSituacaoRecuperacao;
	}
	
	/**
	 * Set: dsMotvtoSituacaoRecuperacao.
	 *
	 * @param dsMotvtoSituacaoRecuperacao the ds motvto situacao recuperacao
	 */
	public void setDsMotvtoSituacaoRecuperacao(String dsMotvtoSituacaoRecuperacao) {
		this.dsMotvtoSituacaoRecuperacao = dsMotvtoSituacaoRecuperacao;
	}
	
	/**
	 * Get: dsSituacaoRecuperacao.
	 *
	 * @return dsSituacaoRecuperacao
	 */
	public String getDsSituacaoRecuperacao() {
		return dsSituacaoRecuperacao;
	}
	
	/**
	 * Set: dsSituacaoRecuperacao.
	 *
	 * @param dsSituacaoRecuperacao the ds situacao recuperacao
	 */
	public void setDsSituacaoRecuperacao(String dsSituacaoRecuperacao) {
		this.dsSituacaoRecuperacao = dsSituacaoRecuperacao;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}
	
	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}
	
	/**
	 * Get: hrSolicitacaoRecuperacao.
	 *
	 * @return hrSolicitacaoRecuperacao
	 */
	public String getHrSolicitacaoRecuperacao() {
		return hrSolicitacaoRecuperacao;
	}
	
	/**
	 * Set: hrSolicitacaoRecuperacao.
	 *
	 * @param hrSolicitacaoRecuperacao the hr solicitacao recuperacao
	 */
	public void setHrSolicitacaoRecuperacao(String hrSolicitacaoRecuperacao) {
		this.hrSolicitacaoRecuperacao = hrSolicitacaoRecuperacao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}
	
	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}
	
	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}
	
	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}
	
	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public Integer getNrSolicitacao() {
		return nrSolicitacao;
	}
	
	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(Integer nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}
	
	/**
	 * Get: listaDetalharSolicRecuperacao.
	 *
	 * @return listaDetalharSolicRecuperacao
	 */
	public List<OcorrenciasDetalharSolicRecuperacaoSaidaDTO> getListaDetalharSolicRecuperacao() {
		return listaDetalharSolicRecuperacao;
	}
	
	/**
	 * Set: listaDetalharSolicRecuperacao.
	 *
	 * @param listaDetalharSolicRecuperacao the lista detalhar solic recuperacao
	 */
	public void setListaDetalharSolicRecuperacao(
			List<OcorrenciasDetalharSolicRecuperacaoSaidaDTO> listaDetalharSolicRecuperacao) {
		this.listaDetalharSolicRecuperacao = listaDetalharSolicRecuperacao;
	}

	
    
    
	
	
	
	

}
