/*
 * Nome: br.com.bradesco.web.pgit.service.report.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato;

import java.awt.Color;
import java.util.Date;

import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.report.base.BasicPdfReport;
import br.com.bradesco.web.pgit.utils.DateUtils;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Nome: EmissaoComprovanteSalarialReport
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EmissaoComprovanteSalarialReport extends BasicPdfReport {
	
	/** Atributo fTexto. */
	private Font fTexto = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
	
	/** Atributo fTextoTabela. */
	private Font fTextoTabela = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
	
	/** Atributo fTextoTabelaCinza. */
	private Font fTextoTabelaCinza = new Font(Font.TIMES_ROMAN, 8, Font.BOLD,Color.GRAY);
	
	/** Atributo total. */
	private String total;
	
	/** Atributo parcial. */
	private String parcial;
	
	/** Atributo imagemRelatorio. */
	private Image imagemRelatorio = null;
	
	/** Atributo saidaImprimirContrato. */
	private ImprimirContratoSaidaDTO saidaImprimirContrato;
	
	/**
	 * Emissao comprovante salarial report.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public EmissaoComprovanteSalarialReport(ImprimirContratoSaidaDTO saidaImprimirContrato) {
		super();
		this.saidaImprimirContrato = saidaImprimirContrato;
		this.imagemRelatorio = getImagemRelatorio(); 
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.report.base.BasicPdfReport#popularPdf(com.lowagie.text.Document)
	 */
	@Override
	protected void popularPdf(Document document) throws DocumentException {
		// TODO Auto-generated method stub
		
		preencherCabec(document);
		document.add(Chunk.NEWLINE);
		preencherAnexo(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherTitulo(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo1(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo2(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo3(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo4(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo5(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo6(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo7(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo8(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo9(document);
		preencherParagrafo9a(document);
		preencherParagrafo9b(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo10(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo11(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo12(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo13(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo14(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo15(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo16(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo17(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo18(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo19(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo20(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo21(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo22(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo23(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo24(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafo25(document);
		
		document.add(Chunk.NEXTPAGE);
		document.add(Chunk.NEWLINE);
		preencherData(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaUm(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherAssinaturas3(document);
		
		document.add(Chunk.NEWLINE);
		assinaturaDois(document);
		
		document.add(Chunk.NEXTPAGE);
		preencherCarta(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherCartaParagrafo1(document);
		preencherCartaParagrafo2(document);
		preencherCartaParagrafo3(document);
		document.add(Chunk.NEWLINE);
		preencherCartaParagrafo4(document);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherCartaParagrafo5(document);
		preencherCartaParagrafo6(document);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherCartaParagrafo7(document);
		document.add(Chunk.NEWLINE);
		preencherCartaParagrafo8(document);
		preencherCartaParagrafo9(document);
		preencherCartaParagrafo10(document);
		preencherCartaParagrafo11(document);
		preencherCartaParagrafo12(document);
		preencherCartaParagrafo13(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherCartaParagrafo14(document);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherCartaParagrafo15(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherCartaParagrafo16(document);
		
	}
	
	/**
	 * Preencher cabec.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCabec(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		tabela.setWidths(new float[] {30.0f, 70.0f});
		
		Paragraph pLinha1Esq = new Paragraph(new Chunk("ANEXO AO CONTRATO DE PRESTA��O DE SERVI�OS DE PAGAMENTOS.", fTextoTabelaCinza));
		pLinha1Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfPCell.NO_BORDER);
		celula1.addElement(imagemRelatorio);
		
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher anexo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAnexo(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("ANEXO 6", fTextoTabelaCinza));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher titulo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherTitulo(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("Emiss�o de Comprovante Salarial.", fTextoTabela));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher paragrafo1.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo1(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("As partes nomeadas e qualificadas no pre�mbulo do ", fTexto));
		pLinha.add(new Chunk("Contrato", fTextoTabela));
		pLinha.add(new Chunk(", ao qual este Anexo faz parte, ", fTexto));
		pLinha.add(new Chunk("denominadas ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" e ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", resolvem estabelecer as condi��es operacionais e comerciais ", fTexto));
		pLinha.add(new Chunk("espec�ficas � presta��o dos servi�os contratados.", fTexto));
		
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo2.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo2(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Primeira: ", fTextoTabela));
		pLinha.add(new Chunk("Este Anexo tem por objeto a presta��o, pelo ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" ao ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", dos servi�os de emiss�o ", fTexto));		
		pLinha.add(new Chunk("de comprovantes salariais de seus empregados, pelos meios de divulga��o nos terminais do Bradesco Dia e ", fTexto));
		pLinha.add(new Chunk("Noite e Bradesco Internet Banking (site: www.bradesco.com.br), por meio de transmiss�o de dados, via computador, que conecta o ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" diretamente ao ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));	
		pLinha.add(new Chunk(", observadas as cl�usulas e condi��es adiante enunciadas.", fTexto));
		
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo3.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo3(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" disponibilizar� os comprovantes salariais dos empregados do ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" por meio do Bradesco Dia e Noite e do Bradesco Internet Banking, pelo prazo de ( ", fTexto));
		pLinha.add(new Chunk(saidaImprimirContrato.getQtMesComprovante().toString(), fTextoTabela));
		pLinha.add(new Chunk(" ) meses.", fTexto));
		
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo4.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo4(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Segunda: ", fTextoTabela));
		pLinha.add(new Chunk("Os servi�os prestados pelo ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", em conformidade com a cl�usula primeira, ser�o ", fTexto));
		pLinha.add(new Chunk("realizados por meio de transmiss�o de arquivos em meios magn�ticos, contendo todos os dados necess�rios � ", fTexto));
		pLinha.add(new Chunk("consecu��o dos servi�os ora contratados, os quais se encontram mencionados nos modelo fornecido pelo ", fTexto));
		pLinha.add(new Chunk("Banco ou software que, eventualmente, poder� ser licenciado pelo ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" ao ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(".", fTexto));
		
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo5.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo5(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Terceira: ", fTextoTabela));
		pLinha.add(new Chunk("Para a execu��o dos servi�os objeto deste Anexo, os arquivos dever�o ser transmitidos e ", fTexto));
		pLinha.add(new Chunk("estar em poder do ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", no Centro de Processamento de Dados, localizado na sede do ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", doravante ", fTexto));
		pLinha.add(new Chunk("denominado �Centro�, com 48 (quarenta e oito) horas de anteced�ncia da data de libera��o para os canais ", fTexto));
		pLinha.add(new Chunk("de impress�o, para que se viabilize o cumprimento dos servi�os aqui contratados.", fTexto));
		
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo6.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo6(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo �nico: ", fTextoTabela));
		pLinha.add(new Chunk("Os arquivos devem conter todas as informa��es e dados necess�rios ", fTexto));
		pLinha.add(new Chunk("� realiza��o da impress�o dos comprovantes salariais dos empregados do ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk("." , fTexto));
				
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo7.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo7(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Quarta: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", depois de conclu�da a transmiss�o/processamento dos dados pelo ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk("tornar� dispon�vel o arquivo retorno contendo as consist�ncias, ficando sob a responsabilidade do ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" a constata��o, confer�ncia e confirma��o das informa��es contidas nesses arquivos, no mesmo dia ", fTexto));
		pLinha.add(new Chunk("do agendamento. O ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" deve, em seguida, capturar o arquivo retorno para confer�ncia dos dados e ", fTexto));
		pLinha.add(new Chunk("proceder ao cancelamento, altera��o de dados e lotes inconsistentes ou rejeitados.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo8.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo8(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente", fTexto));
		pLinha.add(new Chunk(" pode optar, em caso de algum comprovante apresentar inconsist�ncia, ", fTexto));
		pLinha.add(new Chunk("pela devolu��o de todos os arquivos para corre��o e envio novamente de todos os comprovantes salariais, ", fTexto));
		pLinha.add(new Chunk("ou pela devolu��o parcial dos arquivos, somente com rela��o aos comprovantes salariais com irregularidade ", fTexto));
		pLinha.add(new Chunk("para que sejam corrigidos e enviados novamente.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo9.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo9(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Segundo: ", fTextoTabela));
		pLinha.add(new Chunk("Neste Anexo o ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" optou pela seguinte op��o de rejei��o:", fTexto));

		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Descricao.
	 */
	public void descricao(){
		if (saidaImprimirContrato.getDsTipoRejeicao().equals("T")){
			setTotal("X");
		}else{
			setTotal("");
		}
		
		if (saidaImprimirContrato.getDsTipoRejeicao().equals("P")){
			setParcial("X");
		}else{
			setParcial("");
		}	
	}
	
	/**
	 * Preencher paragrafo9a.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo9a(Document document) throws DocumentException {
		
		descricao();
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("a) ( " + getTotal()+ " ) Total");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo9b.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo9b(Document document) throws DocumentException {
		
		descricao();
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("b) ( " + getParcial()+ " ) Parcial");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher paragrafo10.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo10(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Quinta: ", fTextoTabela));
		pLinha.add(new Chunk("Na hip�tese de ocorr�ncias de situa��es at�picas que impe�am o agendamento, o ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" tomar� as medidas necess�rias para que as emiss�es de comprovantes salariais sejam disponibilizadas sob responsabilidade do ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", o fornecimento dos comprovantes salariais a seus empregados por outro meio.", fTexto));

		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo11.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo11(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Sexta: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));		
		pLinha.add(new Chunk(" responsabiliza-se pelo correto conte�do das informa��es contidas nos arquivos, as quais ser�o fornecidas ao ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", contendo obrigatoriamente nos comprovantes salariais todos os dados necess�rios, tais como raz�o social e CNPJ do ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", m�s a que se refere o pagamento, tipo de comprovante salarial, valor bruto do sal�rio, descontos efetuados, ", fTexto));
		pLinha.add(new Chunk("valor recolhido ao FGTS, data de cr�dito e demais informa��es constantes no modelo e que s�o exigidas por lei.", fTexto));

		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo12.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo12(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula S�tima: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" obriga-se a tomar todas as cautelas necess�rias para a correta transcri��o ", fTexto));
		pLinha.add(new Chunk("dos dados dos comprovantes salariais de seus empregados a serem realizados com base neste Anexo, isentando o ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", neste ato, de toda e qualquer responsabilidade relativa a eventuais reclama��es, preju�zos, ", fTexto));
		pLinha.add(new Chunk("perdas e danos, lucros cessantes e/ou emergentes, inclusive perante terceiros, decorrentes de erros, ", fTexto));
		pLinha.add(new Chunk("falhas, irregularidades e omiss�es dos dados constantes de cada comprovante salarial.", fTexto));
		
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo13.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo13(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		pLinha.add(new Chunk("No caso do ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" ficar impossibilitado de disponibilizar os comprovantes salariais ", fTexto));
		pLinha.add(new Chunk("para os empregados por falha do ", fTexto)); 
		pLinha.add(new Chunk("Cliente" , fTextoTabela));
		pLinha.add(new Chunk(", o ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" se responsabiliza a comunicar os fatos a seus empregados, ", fTexto));
		pLinha.add(new Chunk("ficando sob sua responsabilidade o fornecimento dos comprovantes salariais a seus empregados por outros meios.", fTexto));

		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo14.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo14(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Oitava: ", fTextoTabela));
		pLinha.add(new Chunk("Desde que cumpridas todas as obriga��es assumidas pelo ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" neste Anexo, o ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" obriga-se a disponibilizar os comprovantes salariais nas datas dos seus respectivos agendamentos, ", fTexto));
		pLinha.add(new Chunk("nas contas correntes tituladas pelos empregados do ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" por ela indicados, em qualquer ag�ncia do ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" onde eles mantenham conta corrente e/ou conta poupan�a.", fTexto));

		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo15.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo15(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Nona: ", fTextoTabela));
		pLinha.add(new Chunk("Pela presta��o dos servi�os objeto deste Anexo, o ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" pagar� ao ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", por cada comprovante emitido, as tarifas �Via Internet - Bradesco Internet Banking� e ", fTexto));
		pLinha.add(new Chunk("�Via Auto-Atendimento - Bradesco Dia e Noite - BDN� previstas na Tabela de �Tarifas de Servi�os Banc�rios� ", fTexto));
		pLinha.add(new Chunk("afixada nas Ag�ncias do Banco e no site www.bradesco.com.br, nos termos das normas editadas pelo ", fTexto));
		pLinha.add(new Chunk("Conselho Monet�rio Nacional e Banco Central do Brasil.", fTexto));
		
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo16.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo16(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();

		pLinha.add(new Chunk("Par�grafo �nico: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" obriga-se a provisionar em sua conta corrente n.� " + saidaImprimirContrato.getCdContaAgenciaGestor()+ "-" + saidaImprimirContrato.getCdDigitoContaGestor(), fTexto));
		pLinha.add(new Chunk(", cadastrada na ag�ncia " + saidaImprimirContrato.getCdAgenciaGestorContrato() + "-" + saidaImprimirContrato.getCdDigitoAgenciaGestor() + ", do Banco Bradesco S.A.", fTexto));
		pLinha.add(new Chunk(", os recursos que permitam o integral acolhimento dos d�bitos relativos ao montante das tarifas, sendo que o ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" n�o se responsabiliza pela n�o realiza��o do agendamento nos seguintes casos: ", fTexto));
		pLinha.add(new Chunk("a) falhas ou omiss�es nas informa��es prestadas pelo ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk("; e b) atraso na entrega das informa��es pelo ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk("."));

		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo17.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo17(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Dez: ", fTextoTabela));
		pLinha.add(new Chunk("Caso o ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" venha a disponibilizar os comprovantes salariais e, em conseq��ncia, a conta corrente do ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" apresentar saldo devedor devido � cobran�a de tarifa, o ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" obriga-se a efetuar imediata cobertura do d�bito, acrescida do respectivo valor do imposto sobre ", fTexto));
		pLinha.add(new Chunk("opera��es de cr�dito e dos juros de mora devidos, sob pena de rescis�o imediata deste contrato, ", fTexto));
		pLinha.add(new Chunk("independentemente de qualquer aviso ou notifica��o.", fTexto));

		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo18.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo18(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Onze: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" dever� agendar a emiss�o dos comprovantes salariais de seus empregados ", fTexto));
		pLinha.add(new Chunk("sempre para um dia �til. Os arquivos ser�o invalidados na hip�tese dos agendamentos reca�rem em ", fTexto));
		pLinha.add(new Chunk("dia n�o �til (s�bado, domingo ou feriado) e, em conseq��ncia, os comprovantes ", fTexto));
		pLinha.add(new Chunk("salariais dos empregados n�o ser�o efetivados.", fTexto));
		
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo19.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo19(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Doze: ", fTextoTabela));
		pLinha.add(new Chunk("Cabe ao ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));		
		pLinha.add(new Chunk(" divulgar a seus empregados os servi�os objeto deste Anexo, bem ", fTexto));
		pLinha.add(new Chunk("como as regras e prazo de disponibiliza��o dos comprovantes salariais, esclarecendo, inclusive, que no caso ", fTexto));
		pLinha.add(new Chunk("de alguma diverg�ncia ou questionamento dos dados constantes dos comprovantes salariais, os esclarecimentos ", fTexto));
		pLinha.add(new Chunk("dever�o ser prestados por ele, ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));				
		pLinha.add(new Chunk(", que se obriga a obter de seus empregados e manter em seu ", fTexto));
		pLinha.add(new Chunk("poder autoriza��o expressa (�Carta de Autoriza��o�, Adendo A a este Anexo) para disponibilizar ao ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" as informa��es relativas aos comprovantes salariais dos seus empregados, para serem emitidos ", fTexto));
		pLinha.add(new Chunk("pelos meios de divulga��o Bradesco Dia e Noite e Bradesco Internet Banking.", fTexto));

		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo20.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo20(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Treze: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));				
		pLinha.add(new Chunk(" se responsabiliza pela verifica��o da regularidade da Carta de Autoriza��o ", fTexto));
		pLinha.add(new Chunk("outorgada por seu empregado e assume a obriga��o de guarda desse documento na qualidade ", fTexto));
		pLinha.add(new Chunk("de fiel deposit�rio, representado pelo ________________________________________________________ ", fTexto));
		pLinha.add(new Chunk("(inserir nome e qualificar). Em caso de solicita��o do ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", o ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" dever� apresentar a c�pia da Carta de Autoriza��o solicitada em at� 48 (quarenta e oito) ", fTexto));
		pLinha.add(new Chunk("horas contadas da solicita��o por escrito que lhe for apresentada pelo Banco.", fTexto));

		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo21.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo21(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Quatorze: ", fTextoTabela));
		pLinha.add(new Chunk("O fornecimento de comprovantes salariais que n�o estejam na base de dados do ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", ou para aqueles empregados que n�o tenham sido inclu�dos nos arquivos enviados pelo ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" e recebidos em ordem pelo ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", ou no caso de fiscaliza��es, deve ser realizado pelo ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", devendo esta manter a guarda das informa��es para futuras necessidades.", fTexto));

		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo22.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo22(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Quinze: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" n�o se responsabiliza por eventuais a��es propostas pelos empregados do ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", provenientes de contesta��es quanto ao conte�do dos dados disponibilizados, ", fTexto));
		pLinha.add(new Chunk("ou pelos riscos de quebra de sigilo na divulga��o de dados adotados neste Anexo.", fTexto));
		
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo23.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo23(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Dezesseis: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" autoriza a impress�o, junto com os comprovantes salariais de seus empregados, de ", fTexto));
		pLinha.add(new Chunk("mensagem com frase padr�o, notificando os seus empregados acerca da responsabilidade do ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(" pelas informa��es constantes nos comprovantes salariais, bem como autoriza, sem qualquer �nus, a impress�o de mensagens que o ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" fa�a para a divulga��o de seus produtos e servi�os, sendo as informa��es de responsabilidade exclusiva da empresa pagadora.", fTexto));

		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo24.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo24(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		
		pLinha.add(new Chunk("Cl�usula Dezessete: ", fTextoTabela));
		pLinha.add(new Chunk("Qualquer altera��o na legisla��o ou advento de Conven��o Coletiva de Trabalho ", fTexto));
		pLinha.add(new Chunk("de categoria(s) profissional(is) do(s) empregado(s) a respeito da emiss�o de comprovante ", fTexto));
		pLinha.add(new Chunk("salarial ou de divulga��o de rendimentos, deve ser comunicada ao ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(" pelo ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(".", fTexto));
		
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo25.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafo25(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Por se acharem de pleno acordo, com tudo aqui pactuado, as partes firmam este ");
		textoClausula.append("Anexo em 03 (tr�s) vias de igual teor e forma, juntamente com as testemunhas abaixo:");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher data.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherData(Document document) throws DocumentException {
		Paragraph data = new Paragraph(new Chunk("Osasco, " + DateUtils.formatarDataPorExtenso(new Date()) + ".", fTexto));
		data.setAlignment(Paragraph.ALIGN_RIGHT);
		document.add(data);
	}
	
	/**
	 * Assinatura um.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaUm(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  CLIENTE:", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  BANCO BRADESCO S/A:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  Ag�ncia:", fTexto));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Esq);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher assinaturas3.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAssinaturas3(Document document) throws DocumentException {
		Paragraph testemunha = new Paragraph(new Chunk("  TESTEMUNHAS:"));
		testemunha.setAlignment(Element.ALIGN_LEFT);
		document.add(testemunha);
	}
	
	/**
	 * Assinatura dois.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaDois(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha3Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
		celula1.addElement(pLinha3Esq);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Dir);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher carta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCarta(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("Carta Autoriza��o.", fTextoTabela));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher carta paragrafo1.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo1(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("�");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo2.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo2(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Nome da Empresa: ___________________________________________________________");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo3.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo3(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Endere�o: __________________________________________________________________");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo4.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo4(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Ref.: Solicita��o de Emiss�o de Comprovante Salarial.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTextoTabela));
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo5.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo5(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Pela presente, autorizo a emiss�o de meu Comprovante Salarial Mensal (�holerith�), por meio ");
		textoClausula.append("m�quinas de auto-atendimento da Rede Bradesco Dia&Noite e pela Internet ");
		textoClausula.append("por meio do site (www.bradesco.com.br).");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo6.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo6(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Para isso, autorizo essa empresa a disponibilizar ao Banco Bradesco S.A. todas as informa��es a mim relativas.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo7.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo7(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Estou ciente e de acordo de que:");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTextoTabela));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo8.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo8(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("a) o acesso �s m�quinas de auto-atendimento da Rede Bradesco Dia&Noite ser�, ");
		textoClausula.append("exclusivamente, mediante o uso do cart�o magn�tico e senha espec�fica, ");
		textoClausula.append("que desde j� autorizo sua emiss�o caso n�o o possua:");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo9.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo9(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("b) somente haver� acesso � internet mediante senha espec�fica j� utilizada para  ");
		textoClausula.append("movimenta��o da minha conta banc�ria, frase secreta e chave de seguran�a Bradesco/Token, ");
		textoClausula.append("ficando automaticamente liberado o acesso ao respectivo comprovante;");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo10.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo10(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("c) em se tratando de conta conjunta, as informa��es estar�o dispon�veis ao co�-titular da conta; e,");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo11.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo11(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("d) as informa��es estar�o dispon�veis em ambos os meios (m�quinas de auto-atendimento da Rede ");
		textoClausula.append("Bradesco Dia&Noite e Internet), pelo prazo definido em contrato assinado entre a empresa e o Banco Bradesco S.A.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo12.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo12(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Qualquer informa��o ap�s esse per�odo deve ser solicitada � empresa.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo13.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo13(Document document) throws DocumentException {
		StringBuilder textoClausula = new StringBuilder();
		textoClausula.append("Esta autoriza��o vigorar� por tempo indeterminado. Entretanto, comprometo-me ");
		textoClausula.append("a comunicar oportunamente essa empresa, por escrito, caso haja interesse neste cancelamento.");
		Paragraph paragrafo = new Paragraph();
		paragrafo.add(new Chunk(textoClausula.toString(), fTexto));
		paragrafo.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(paragrafo);
	}
	
	/**
	 * Preencher carta paragrafo14.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo14(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("Atenciosamente,", fTextoTabelaCinza));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher carta paragrafo15.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo15(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("_________________________________________", fTexto));
		Paragraph titulo1 = new Paragraph(new Chunk("Osasco, " + DateUtils.formatarDataPorExtenso(new Date()) + ".", fTexto));
		titulo.setAlignment(Element.ALIGN_CENTER);
		titulo1.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
		document.add(titulo1);
	}
	
	/**
	 * Preencher carta paragrafo16.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCartaParagrafo16(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("_________________________________________", fTexto));
		Paragraph titulo1 = new Paragraph(new Chunk("Nome do Funcion�rio:", fTexto));
		titulo.setAlignment(Element.ALIGN_CENTER);
		titulo1.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
		document.add(titulo1);
	}

	/**
	 * Get: saidaImprimirContrato.
	 *
	 * @return saidaImprimirContrato
	 */
	public ImprimirContratoSaidaDTO getSaidaImprimirContrato() {
		return saidaImprimirContrato;
	}

	/**
	 * Set: saidaImprimirContrato.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public void setSaidaImprimirContrato(
			ImprimirContratoSaidaDTO saidaImprimirContrato) {
		this.saidaImprimirContrato = saidaImprimirContrato;
	}

	/**
	 * Get: parcial.
	 *
	 * @return parcial
	 */
	public String getParcial() {
		return parcial;
	}

	/**
	 * Set: parcial.
	 *
	 * @param parcial the parcial
	 */
	public void setParcial(String parcial) {
		this.parcial = parcial;
	}

	/**
	 * Get: total.
	 *
	 * @return total
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * Set: total.
	 *
	 * @param total the total
	 */
	public void setTotal(String total) {
		this.total = total;
	}
}
