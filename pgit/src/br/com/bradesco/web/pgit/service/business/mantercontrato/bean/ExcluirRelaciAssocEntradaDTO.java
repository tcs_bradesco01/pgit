/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ExcluirRelaciAssocEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirRelaciAssocEntradaDTO{
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdPessoaJuridicaVinculo. */
	private Long cdPessoaJuridicaVinculo;
	
	/** Atributo cdTipoContratoVinculo. */
	private Integer cdTipoContratoVinculo;
	
	/** Atributo nrSequenciaContratoVinculo. */
	private Long nrSequenciaContratoVinculo;
	
	/** Atributo cdTipoVincContrato. */
	private Integer cdTipoVincContrato;
	
	/** Atributo cdTipoRelacionamentoConta. */
	private Integer cdTipoRelacionamentoConta;
	
	/** Atributo cdTipoTesteAuxiliar. */
	private String cdTipoTesteAuxiliar;
	
	/** Atributo cdPssoa. */
	private Long cdPssoa;

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato(){
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato){
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio(){
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio){
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio(){
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio){
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public Long getCdPessoaJuridicaVinculo(){
		return cdPessoaJuridicaVinculo;
	}

	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo){
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}

	/**
	 * Get: cdTipoContratoVinculo.
	 *
	 * @return cdTipoContratoVinculo
	 */
	public Integer getCdTipoContratoVinculo(){
		return cdTipoContratoVinculo;
	}

	/**
	 * Set: cdTipoContratoVinculo.
	 *
	 * @param cdTipoContratoVinculo the cd tipo contrato vinculo
	 */
	public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo){
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}

	/**
	 * Get: nrSequenciaContratoVinculo.
	 *
	 * @return nrSequenciaContratoVinculo
	 */
	public Long getNrSequenciaContratoVinculo(){
		return nrSequenciaContratoVinculo;
	}

	/**
	 * Set: nrSequenciaContratoVinculo.
	 *
	 * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
	 */
	public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo){
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}

	/**
	 * Get: cdTipoVincContrato.
	 *
	 * @return cdTipoVincContrato
	 */
	public Integer getCdTipoVincContrato(){
		return cdTipoVincContrato;
	}

	/**
	 * Set: cdTipoVincContrato.
	 *
	 * @param cdTipoVincContrato the cd tipo vinc contrato
	 */
	public void setCdTipoVincContrato(Integer cdTipoVincContrato){
		this.cdTipoVincContrato = cdTipoVincContrato;
	}

	/**
	 * Get: cdTipoRelacionamentoConta.
	 *
	 * @return cdTipoRelacionamentoConta
	 */
	public Integer getCdTipoRelacionamentoConta(){
		return cdTipoRelacionamentoConta;
	}

	/**
	 * Set: cdTipoRelacionamentoConta.
	 *
	 * @param cdTipoRelacionamentoConta the cd tipo relacionamento conta
	 */
	public void setCdTipoRelacionamentoConta(Integer cdTipoRelacionamentoConta){
		this.cdTipoRelacionamentoConta = cdTipoRelacionamentoConta;
	}

	/**
	 * Get: cdTipoTesteAuxiliar.
	 *
	 * @return cdTipoTesteAuxiliar
	 */
	public String getCdTipoTesteAuxiliar(){
		return cdTipoTesteAuxiliar;
	}

	/**
	 * Set: cdTipoTesteAuxiliar.
	 *
	 * @param cdTipoTesteAuxiliar the cd tipo teste auxiliar
	 */
	public void setCdTipoTesteAuxiliar(String cdTipoTesteAuxiliar){
		this.cdTipoTesteAuxiliar = cdTipoTesteAuxiliar;
	}

	public Long getCdPssoa() {
		return cdPssoa;
	}

	public void setCdPssoa(Long cdPssoa) {
		this.cdPssoa = cdPssoa;
	}

	
}