/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantefavorecido.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarSolEmiComprovanteFavorecidoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarSolEmiComprovanteFavorecidoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dtInicioSolicitacao
     */
    private java.lang.String _dtInicioSolicitacao;

    /**
     * Field _dtFimSolicitacao
     */
    private java.lang.String _dtFimSolicitacao;

    /**
     * Field _cdSituacaoSolicitacao
     */
    private int _cdSituacaoSolicitacao = 0;

    /**
     * keeps track of state for field: _cdSituacaoSolicitacao
     */
    private boolean _has_cdSituacaoSolicitacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarSolEmiComprovanteFavorecidoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantefavorecido.request.ConsultarSolEmiComprovanteFavorecidoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoSolicitacao
     * 
     */
    public void deleteCdSituacaoSolicitacao()
    {
        this._has_cdSituacaoSolicitacao= false;
    } //-- void deleteCdSituacaoSolicitacao() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacao'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacao'.
     */
    public int getCdSituacaoSolicitacao()
    {
        return this._cdSituacaoSolicitacao;
    } //-- int getCdSituacaoSolicitacao() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'dtFimSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtFimSolicitacao'.
     */
    public java.lang.String getDtFimSolicitacao()
    {
        return this._dtFimSolicitacao;
    } //-- java.lang.String getDtFimSolicitacao() 

    /**
     * Returns the value of field 'dtInicioSolicitacao'.
     * 
     * @return String
     * @return the value of field 'dtInicioSolicitacao'.
     */
    public java.lang.String getDtInicioSolicitacao()
    {
        return this._dtInicioSolicitacao;
    } //-- java.lang.String getDtInicioSolicitacao() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoSolicitacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacao()
    {
        return this._has_cdSituacaoSolicitacao;
    } //-- boolean hasCdSituacaoSolicitacao() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacao'.
     * 
     * @param cdSituacaoSolicitacao the value of field
     * 'cdSituacaoSolicitacao'.
     */
    public void setCdSituacaoSolicitacao(int cdSituacaoSolicitacao)
    {
        this._cdSituacaoSolicitacao = cdSituacaoSolicitacao;
        this._has_cdSituacaoSolicitacao = true;
    } //-- void setCdSituacaoSolicitacao(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'dtFimSolicitacao'.
     * 
     * @param dtFimSolicitacao the value of field 'dtFimSolicitacao'
     */
    public void setDtFimSolicitacao(java.lang.String dtFimSolicitacao)
    {
        this._dtFimSolicitacao = dtFimSolicitacao;
    } //-- void setDtFimSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioSolicitacao'.
     * 
     * @param dtInicioSolicitacao the value of field
     * 'dtInicioSolicitacao'.
     */
    public void setDtInicioSolicitacao(java.lang.String dtInicioSolicitacao)
    {
        this._dtInicioSolicitacao = dtInicioSolicitacao;
    } //-- void setDtInicioSolicitacao(java.lang.String) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarSolEmiComprovanteFavorecidoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantefavorecido.request.ConsultarSolEmiComprovanteFavorecidoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantefavorecido.request.ConsultarSolEmiComprovanteFavorecidoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantefavorecido.request.ConsultarSolEmiComprovanteFavorecidoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantefavorecido.request.ConsultarSolEmiComprovanteFavorecidoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
