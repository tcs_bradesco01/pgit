/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarpendenciacontratopgit.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsPendenciaPagamentoIntegrado
     */
    private java.lang.String _dsPendenciaPagamentoIntegrado;

    /**
     * Field _dsUnidadeOrganizacional
     */
    private java.lang.String _dsUnidadeOrganizacional;

    /**
     * Field _dsTipoUnidadeOrganizacional
     */
    private java.lang.String _dsTipoUnidadeOrganizacional;

    /**
     * Field _cdPessoaUnidadeOrganizacional
     */
    private int _cdPessoaUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdPessoaUnidadeOrganizaciona
     */
    private boolean _has_cdPessoaUnidadeOrganizacional;

    /**
     * Field _cdDigitoUnidadeOrganizacional
     */
    private java.lang.String _cdDigitoUnidadeOrganizacional;

    /**
     * Field _dsTipoBaixa
     */
    private java.lang.String _dsTipoBaixa;

    /**
     * Field _dsSituacaoPendenciaContrato
     */
    private java.lang.String _dsSituacaoPendenciaContrato;

    /**
     * Field _cdMotivoSituacaoPendencia
     */
    private java.lang.String _cdMotivoSituacaoPendencia;

    /**
     * Field _dtAtendimentoPendenciaContrato
     */
    private java.lang.String _dtAtendimentoPendenciaContrato;

    /**
     * Field _hrBaixaPendenciaContrato
     */
    private java.lang.String _hrBaixaPendenciaContrato;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExter
     */
    private java.lang.String _cdUsuarioInclusaoExter;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExter
     */
    private java.lang.String _cdUsuarioManutencaoExter;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpendenciacontratopgit.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdPessoaUnidadeOrganizacional
     * 
     */
    public void deleteCdPessoaUnidadeOrganizacional()
    {
        this._has_cdPessoaUnidadeOrganizacional= false;
    } //-- void deleteCdPessoaUnidadeOrganizacional() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdDigitoUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'cdDigitoUnidadeOrganizacional'.
     */
    public java.lang.String getCdDigitoUnidadeOrganizacional()
    {
        return this._cdDigitoUnidadeOrganizacional;
    } //-- java.lang.String getCdDigitoUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdMotivoSituacaoPendencia'.
     * 
     * @return String
     * @return the value of field 'cdMotivoSituacaoPendencia'.
     */
    public java.lang.String getCdMotivoSituacaoPendencia()
    {
        return this._cdMotivoSituacaoPendencia;
    } //-- java.lang.String getCdMotivoSituacaoPendencia() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdPessoaUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdPessoaUnidadeOrganizacional'.
     */
    public int getCdPessoaUnidadeOrganizacional()
    {
        return this._cdPessoaUnidadeOrganizacional;
    } //-- int getCdPessoaUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExter'.
     */
    public java.lang.String getCdUsuarioInclusaoExter()
    {
        return this._cdUsuarioInclusaoExter;
    } //-- java.lang.String getCdUsuarioInclusaoExter() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExter'.
     */
    public java.lang.String getCdUsuarioManutencaoExter()
    {
        return this._cdUsuarioManutencaoExter;
    } //-- java.lang.String getCdUsuarioManutencaoExter() 

    /**
     * Returns the value of field 'dsPendenciaPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'dsPendenciaPagamentoIntegrado'.
     */
    public java.lang.String getDsPendenciaPagamentoIntegrado()
    {
        return this._dsPendenciaPagamentoIntegrado;
    } //-- java.lang.String getDsPendenciaPagamentoIntegrado() 

    /**
     * Returns the value of field 'dsSituacaoPendenciaContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoPendenciaContrato'.
     */
    public java.lang.String getDsSituacaoPendenciaContrato()
    {
        return this._dsSituacaoPendenciaContrato;
    } //-- java.lang.String getDsSituacaoPendenciaContrato() 

    /**
     * Returns the value of field 'dsTipoBaixa'.
     * 
     * @return String
     * @return the value of field 'dsTipoBaixa'.
     */
    public java.lang.String getDsTipoBaixa()
    {
        return this._dsTipoBaixa;
    } //-- java.lang.String getDsTipoBaixa() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsTipoUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeOrganizacional'.
     */
    public java.lang.String getDsTipoUnidadeOrganizacional()
    {
        return this._dsTipoUnidadeOrganizacional;
    } //-- java.lang.String getDsTipoUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeOrganizacional'.
     */
    public java.lang.String getDsUnidadeOrganizacional()
    {
        return this._dsUnidadeOrganizacional;
    } //-- java.lang.String getDsUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dtAtendimentoPendenciaContrato'.
     * 
     * @return String
     * @return the value of field 'dtAtendimentoPendenciaContrato'.
     */
    public java.lang.String getDtAtendimentoPendenciaContrato()
    {
        return this._dtAtendimentoPendenciaContrato;
    } //-- java.lang.String getDtAtendimentoPendenciaContrato() 

    /**
     * Returns the value of field 'hrBaixaPendenciaContrato'.
     * 
     * @return String
     * @return the value of field 'hrBaixaPendenciaContrato'.
     */
    public java.lang.String getHrBaixaPendenciaContrato()
    {
        return this._hrBaixaPendenciaContrato;
    } //-- java.lang.String getHrBaixaPendenciaContrato() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Method hasCdPessoaUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaUnidadeOrganizacional()
    {
        return this._has_cdPessoaUnidadeOrganizacional;
    } //-- boolean hasCdPessoaUnidadeOrganizacional() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdDigitoUnidadeOrganizacional'.
     * 
     * @param cdDigitoUnidadeOrganizacional the value of field
     * 'cdDigitoUnidadeOrganizacional'.
     */
    public void setCdDigitoUnidadeOrganizacional(java.lang.String cdDigitoUnidadeOrganizacional)
    {
        this._cdDigitoUnidadeOrganizacional = cdDigitoUnidadeOrganizacional;
    } //-- void setCdDigitoUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'cdMotivoSituacaoPendencia'.
     * 
     * @param cdMotivoSituacaoPendencia the value of field
     * 'cdMotivoSituacaoPendencia'.
     */
    public void setCdMotivoSituacaoPendencia(java.lang.String cdMotivoSituacaoPendencia)
    {
        this._cdMotivoSituacaoPendencia = cdMotivoSituacaoPendencia;
    } //-- void setCdMotivoSituacaoPendencia(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaUnidadeOrganizacional'.
     * 
     * @param cdPessoaUnidadeOrganizacional the value of field
     * 'cdPessoaUnidadeOrganizacional'.
     */
    public void setCdPessoaUnidadeOrganizacional(int cdPessoaUnidadeOrganizacional)
    {
        this._cdPessoaUnidadeOrganizacional = cdPessoaUnidadeOrganizacional;
        this._has_cdPessoaUnidadeOrganizacional = true;
    } //-- void setCdPessoaUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @param cdUsuarioInclusaoExter the value of field
     * 'cdUsuarioInclusaoExter'.
     */
    public void setCdUsuarioInclusaoExter(java.lang.String cdUsuarioInclusaoExter)
    {
        this._cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
    } //-- void setCdUsuarioInclusaoExter(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @param cdUsuarioManutencaoExter the value of field
     * 'cdUsuarioManutencaoExter'.
     */
    public void setCdUsuarioManutencaoExter(java.lang.String cdUsuarioManutencaoExter)
    {
        this._cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
    } //-- void setCdUsuarioManutencaoExter(java.lang.String) 

    /**
     * Sets the value of field 'dsPendenciaPagamentoIntegrado'.
     * 
     * @param dsPendenciaPagamentoIntegrado the value of field
     * 'dsPendenciaPagamentoIntegrado'.
     */
    public void setDsPendenciaPagamentoIntegrado(java.lang.String dsPendenciaPagamentoIntegrado)
    {
        this._dsPendenciaPagamentoIntegrado = dsPendenciaPagamentoIntegrado;
    } //-- void setDsPendenciaPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoPendenciaContrato'.
     * 
     * @param dsSituacaoPendenciaContrato the value of field
     * 'dsSituacaoPendenciaContrato'.
     */
    public void setDsSituacaoPendenciaContrato(java.lang.String dsSituacaoPendenciaContrato)
    {
        this._dsSituacaoPendenciaContrato = dsSituacaoPendenciaContrato;
    } //-- void setDsSituacaoPendenciaContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoBaixa'.
     * 
     * @param dsTipoBaixa the value of field 'dsTipoBaixa'.
     */
    public void setDsTipoBaixa(java.lang.String dsTipoBaixa)
    {
        this._dsTipoBaixa = dsTipoBaixa;
    } //-- void setDsTipoBaixa(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoUnidadeOrganizacional'.
     * 
     * @param dsTipoUnidadeOrganizacional the value of field
     * 'dsTipoUnidadeOrganizacional'.
     */
    public void setDsTipoUnidadeOrganizacional(java.lang.String dsTipoUnidadeOrganizacional)
    {
        this._dsTipoUnidadeOrganizacional = dsTipoUnidadeOrganizacional;
    } //-- void setDsTipoUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeOrganizacional'.
     * 
     * @param dsUnidadeOrganizacional the value of field
     * 'dsUnidadeOrganizacional'.
     */
    public void setDsUnidadeOrganizacional(java.lang.String dsUnidadeOrganizacional)
    {
        this._dsUnidadeOrganizacional = dsUnidadeOrganizacional;
    } //-- void setDsUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'dtAtendimentoPendenciaContrato'.
     * 
     * @param dtAtendimentoPendenciaContrato the value of field
     * 'dtAtendimentoPendenciaContrato'.
     */
    public void setDtAtendimentoPendenciaContrato(java.lang.String dtAtendimentoPendenciaContrato)
    {
        this._dtAtendimentoPendenciaContrato = dtAtendimentoPendenciaContrato;
    } //-- void setDtAtendimentoPendenciaContrato(java.lang.String) 

    /**
     * Sets the value of field 'hrBaixaPendenciaContrato'.
     * 
     * @param hrBaixaPendenciaContrato the value of field
     * 'hrBaixaPendenciaContrato'.
     */
    public void setHrBaixaPendenciaContrato(java.lang.String hrBaixaPendenciaContrato)
    {
        this._hrBaixaPendenciaContrato = hrBaixaPendenciaContrato;
    } //-- void setHrBaixaPendenciaContrato(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarpendenciacontratopgit.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarpendenciacontratopgit.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarpendenciacontratopgit.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpendenciacontratopgit.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
