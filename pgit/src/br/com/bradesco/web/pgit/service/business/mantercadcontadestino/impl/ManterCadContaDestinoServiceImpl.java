/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.impl
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.AlterarCadContasDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.AlterarCadContasDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConVinculoContaSalarioDestEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConVinculoContaSalarioDestSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarDetHistContaSalarioDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarDetHistContaSalarioDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ExcluirVinculoContaSalarioDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ExcluirVinculoContaSalarioDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.IncluirCadContaDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.IncluirCadContaDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListaVinculoContaSalarioDestEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListaVinculoContaSalarioDestSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListarHistVinculoContaSalarioDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListarHistVinculoContaSalarioDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ValidarServicoContratadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ValidarServicoContratadoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarvinculocontasalariodestino.request.AlterarVinculoContaSalarioDestinoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarvinculocontasalariodestino.response.AlterarVinculoContaSalarioDestinoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.request.ConsultarBancoAgenciaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.response.ConsultarBancoAgenciaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhevinculocontasalariodestino.request.ConsultarDetalheVinculoContaSalarioDestinoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardetalhevinculocontasalariodestino.response.ConsultarDetalheVinculoContaSalarioDestinoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardethistcontasalariodestino.request.ConsultarDetHistContaSalarioDestinoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardethistcontasalariodestino.response.ConsultarDetHistContaSalarioDestinoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinculocontasalariodestino.request.ExcluirVinculoContaSalarioDestinoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirvinculocontasalariodestino.response.ExcluirVinculoContaSalarioDestinoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinculocontasalariodestino.request.IncluirVinculoContaSalarioDestinoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirvinculocontasalariodestino.response.IncluirVinculoContaSalarioDestinoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistvinculocontasalariodestino.request.ListarHistVinculoContaSalarioDestinoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarhistvinculocontasalariodestino.response.ListarHistVinculoContaSalarioDestinoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarvinculocontasalariodestino.request.ListarVinculoContaSalarioDestinoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarvinculocontasalariodestino.response.ListarVinculoContaSalarioDestinoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarvinculocontasalariodestino.response.Ocorrencias;
import br.com.bradesco.web.pgit.service.data.pdc.validarservicocontratado.request.ValidarServicoContratadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.validarservicocontratado.response.ValidarServicoContratadoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterCadContaDestino
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterCadContaDestinoServiceImpl implements
		IManterCadContaDestinoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Manter cad conta destino service impl.
	 */
	public ManterCadContaDestinoServiceImpl() {
		super();
	}

	/*
	 * Consultar
	 */
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService#pesquisarCadContaSalarioDestino(br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListaVinculoContaSalarioDestEntradaDTO)
	 */
	public List<ListaVinculoContaSalarioDestSaidaDTO> pesquisarCadContaSalarioDestino(
			ListaVinculoContaSalarioDestEntradaDTO entrada) {

		ListarVinculoContaSalarioDestinoRequest request = new ListarVinculoContaSalarioDestinoRequest();

		request.setCdAgencia(entrada.getCdAgencia() == null ? 0 : entrada
				.getCdAgencia());
		request.setCdBanco(entrada.getCdBanco() == null ? 0 : entrada
				.getCdBanco());
		request.setCdConta(entrada.getCdConta() == null ? 0 : entrada
				.getCdConta());
		request
				.setCdPessoaJuridicaNegocio(entrada
						.getCdPessoaJuridicaNegocio());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setDgConta(entrada.getDgConta() == null ? "0" : entrada
				.getDgConta());
		request.setDgCpf(entrada.getDgCpf() == null ? 0 : entrada.getDgCpf());
		request.setNrCpf(entrada.getNrCpf() == null ? 0 : entrada.getNrCpf());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setNumeroOcorrencias(entrada.getNumeroOcorrencias());

		ListarVinculoContaSalarioDestinoResponse response = getFactoryAdapter()
				.getListarVinculoContaSalarioDestinoPDCAdapter().invokeProcess(
						request);
		List<ListaVinculoContaSalarioDestSaidaDTO> list = new ArrayList<ListaVinculoContaSalarioDestSaidaDTO>();

		for (Ocorrencias saida : response.getOcorrencias()) {
			list.add(new ListaVinculoContaSalarioDestSaidaDTO(saida));
		}

		return list;
	}

	/*
	 * Detalhar
	 */
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService#detalharVinculoContaSalarioDest(br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConVinculoContaSalarioDestEntradaDTO)
	 */
	public ConVinculoContaSalarioDestSaidaDTO detalharVinculoContaSalarioDest(
			ConVinculoContaSalarioDestEntradaDTO entrada) {

		ConsultarDetalheVinculoContaSalarioDestinoRequest request = new ConsultarDetalheVinculoContaSalarioDestinoRequest();

		request.setCdPessoaJuridNegocio(PgitUtil.verificaLongNulo(entrada
				.getCdPessoaJuridNegocio()));
		request.setCdPessoaJuridVinc(PgitUtil.verificaLongNulo(entrada
				.getCdPessoaJuridVinc()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setCdTipoContratoVinc(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoVinc()));
		request.setCdTipoVinculoContrato(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoVinculoContrato()));
		request.setNrSeqContratoNegocio(PgitUtil.verificaLongNulo(entrada
				.getNrSeqContratoNegocio()));
		request.setNrSeqContratoVinc(PgitUtil.verificaLongNulo(entrada
				.getNrSeqContratoVinc()));

		ConsultarDetalheVinculoContaSalarioDestinoResponse response = getFactoryAdapter()
				.getConsultarDetalheVinculoContaSalarioDestinoPDCAdapter()
				.invokeProcess(request);

		if (response == null) {
			return null;
		}

		ConVinculoContaSalarioDestSaidaDTO saida = new ConVinculoContaSalarioDestSaidaDTO();

		saida.setCdControleCpfCnpj(response.getOcorrencias(0)
				.getCdControleCpfCnpj());
		saida.setCdOperCanalInclusao(response.getOcorrencias(0)
				.getCdOperCanalInclusao());
		saida.setCdOperCanalManutencao(response.getOcorrencias(0)
				.getCdOperCanalManutencao());
		saida.setCdTipoCanalInclusao(response.getOcorrencias(0)
				.getCdTipoCanalInclusao());
		saida.setCdTipoCanalManutencao(response.getOcorrencias(0)
				.getCdTipoCanalManutencao());
		saida.setCdUsuarioInclusao(response.getOcorrencias(0)
				.getCdUsuarioInclusao());
		saida.setCdUsuarioInclusaoExter(response.getOcorrencias(0)
				.getCdUsuarioInclusaoExter());
		saida.setCdUsuarioManutencao(response.getOcorrencias(0)
				.getCdUsuarioManutencaoExter());
		saida.setCdUsuarioManutencaoExter(response.getOcorrencias(0)
				.getCdUsuarioManutencaoExter());
		saida.setDgCpfCnpj(response.getOcorrencias(0).getDgCpfCnpj());
		saida.setDsCanalInclusao(response.getOcorrencias(0)
				.getDsCanalInclusao());
		saida.setDsCanalManutencao(response.getOcorrencias(0)
				.getDsCanalManutencao());
		saida.setDsNome(response.getOcorrencias(0).getDsNome());
		saida.setDtInclusao(response.getOcorrencias(0).getDtInclusao());
		saida.setDtManutencao(response.getOcorrencias(0).getDtManutencao());
		saida.setHrInclusao(response.getOcorrencias(0).getHrInclusao());
		saida.setHrManutencao(response.getOcorrencias(0).getHrManutencao());
		saida.setNrCpfCnpj(response.getOcorrencias(0).getNrCpfCnpj());

		return saida;
	}

	/*
	 * Incluir
	 */
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService#incluirVinculoContaSalarioDest(br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.IncluirCadContaDestinoEntradaDTO)
	 */
	public IncluirCadContaDestinoSaidaDTO incluirVinculoContaSalarioDest(
			IncluirCadContaDestinoEntradaDTO entrada) {

		IncluirVinculoContaSalarioDestinoRequest request = new IncluirVinculoContaSalarioDestinoRequest();

		request.setCdAgencia(entrada.getCdAgencia());
		request.setCdAgenciaSalario(entrada.getCdAgenciaSalario());
		request.setCdBanco(entrada.getCdBanco());
		request.setCdBancoSalario(entrada.getCdBancoSalario());
		request.setCdConta(entrada.getCdConta());
		request.setCdContaSalario(entrada.getCdContaSalario());
		request.setCdPessoaJuridNegocio(entrada.getCdPessoaJuridNegocio());
		request.setCdTipoConta(entrada.getCdTipoConta());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());
		request.setCdDigitoAgencia(entrada.getCdDigitoAgencia() == null ? 0
				: entrada.getCdDigitoAgencia());
		request
				.setCdDigitoAgenciaSalario(entrada.getCdDigitoAgenciaSalario() == null ? 0
						: entrada.getCdDigitoAgenciaSalario());
		request.setCdDigitoConta(entrada.getCdDigitoConta() == null ? ""
				: entrada.getCdDigitoConta());
		request
				.setCdDigitoContaSalario(entrada.getCdDigitoContaSalario() == null ? ""
						: entrada.getCdDigitoContaSalario());
		request.setNrSeqContratoNegocio(entrada.getNrSeqContratoNegocio());
		request.setCdContingenciaTed(entrada.getCdContingenciaTed());

		IncluirVinculoContaSalarioDestinoResponse response = getFactoryAdapter()
				.getIncluirVinculoContaSalarioDestinoPDCAdapter()
				.invokeProcess(request);

		if (response == null) {
			return null;
		}

		IncluirCadContaDestinoSaidaDTO saida = new IncluirCadContaDestinoSaidaDTO(
				response.getCodMensagem(), response.getMensagem());

		return saida;
	}

	/*
	 * Excluir
	 */
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService#excluirVincContaSalarioDestino(br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ExcluirVinculoContaSalarioDestinoEntradaDTO)
	 */
	public ExcluirVinculoContaSalarioDestinoSaidaDTO excluirVincContaSalarioDestino(
			ExcluirVinculoContaSalarioDestinoEntradaDTO entrada) {

		ExcluirVinculoContaSalarioDestinoRequest request = new ExcluirVinculoContaSalarioDestinoRequest();

		request
				.setCdPessoaJuridicaNegocio(entrada
						.getCdPessoaJuridicaNegocio());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request
				.setCdPessoaJuridicaVinculo(entrada
						.getCdPessoaJuridicaVinculo());
		request.setCdTipoContratoVinculo(entrada.getCdTipoContratoVinculo());
		request.setNrSequenciaContratoVinculo(entrada
				.getNrSequenciaContratoVinculo());
		request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());

		ExcluirVinculoContaSalarioDestinoResponse response = getFactoryAdapter()
				.getExcluirVinculoContaSalarioDestinoPDCAdapter()
				.invokeProcess(request);

		if (response == null) {
			return null;
		}

		ExcluirVinculoContaSalarioDestinoSaidaDTO saida = new ExcluirVinculoContaSalarioDestinoSaidaDTO(
				response.getCodMensagem(), response.getMensagem());

		return saida;
	}

	/*
	 * Historico
	 */
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService#consultarHistoricoContaSalarioDest(br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListarHistVinculoContaSalarioDestinoEntradaDTO)
	 */
	public List<ListarHistVinculoContaSalarioDestinoSaidaDTO> consultarHistoricoContaSalarioDest(
			ListarHistVinculoContaSalarioDestinoEntradaDTO entrada) {

		ListarHistVinculoContaSalarioDestinoRequest request = new ListarHistVinculoContaSalarioDestinoRequest();

		request.setCdPessoaJuridNegocio(entrada.getCdPessoaJuridNegocio());
		request.setCdPessoaJuridVinc(entrada.getCdPessoaJuridVinc());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoContratoVinc(entrada.getCdTipoContratoVinc());
		request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());
		request.setNrSeqContratoNegocio(entrada.getNrSeqContratoNegocio());
		request.setNrSeqContratoVinc(entrada.getNrSeqContratoVinc());
		request.setNrOcorrencias(40);
		request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
		request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada
				.getCdAgencia()));
		request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
		request.setCdDigitoConta(PgitUtil.verificaStringNula(entrada
				.getCdDigitoConta()));
		request.setDataInicio(PgitUtil.verificaStringNula(entrada
				.getDataInicio()));
		request.setDataFim(PgitUtil.verificaStringNula(entrada.getDataFim()));
		request.setCdPesquisaLista(PgitUtil.verificaIntegerNulo(entrada
				.getCdPesquisaLista()));

		ListarHistVinculoContaSalarioDestinoResponse response = getFactoryAdapter()
				.getListarHistVinculoContaSalarioDestinoPDCAdapter()
				.invokeProcess(request);

		if (response == null) {
			return null;
		}

		List<ListarHistVinculoContaSalarioDestinoSaidaDTO> list = new ArrayList<ListarHistVinculoContaSalarioDestinoSaidaDTO>();

		for (br.com.bradesco.web.pgit.service.data.pdc.listarhistvinculocontasalariodestino.response.Ocorrencias ocorrencia : response
				.getOcorrencias()) {
			list.add(new ListarHistVinculoContaSalarioDestinoSaidaDTO(
					ocorrencia));
		}

		return list;
	}

	/*
	 * Alterar
	 */
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService#alterarCadastroContaDestino(br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.AlterarCadContasDestinoEntradaDTO)
	 */
	public AlterarCadContasDestinoSaidaDTO alterarCadastroContaDestino(
			AlterarCadContasDestinoEntradaDTO entrada) {

		AlterarVinculoContaSalarioDestinoRequest request = new AlterarVinculoContaSalarioDestinoRequest();

		request.setCdAgencia(entrada.getCdAgencia());
		request.setCdBanco(entrada.getCdBanco());
		request.setCdConta(entrada.getCdConta());
		request.setCdDigitoAgencia(entrada.getCdDigitoAgencia() == null ? 0
				: entrada.getCdDigitoAgencia());
		request.setCdDigitoConta(entrada.getCdDigitoConta() == null ? ""
				: entrada.getCdDigitoConta());
		request
				.setCdPessoaJuridicaNegocio(entrada
						.getCdPessoaJuridicaNegocio());
		request
				.setCdPessoaJuridicaVinculo(entrada
						.getCdPessoaJuridicaVinculo());
		request.setCdTipoConta(entrada.getCdTipoConta());
		request.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		request.setCdTipoContratoVinculo(entrada.getCdTipoContratoVinculo());
		request.setCdTipoVinculoContrato(entrada.getCdTipoVinculoContrato());
		request.setNrSequenciaContratoNegocio(entrada
				.getNrSequenciaContratoNegocio());
		request.setNrSequencialContratoVinculo(entrada
				.getNrSequenciaContratoVinculo());
		request.setCdContingenciaTed(entrada.getCdContingenciaTed());

		AlterarVinculoContaSalarioDestinoResponse response = getFactoryAdapter()
				.getAlterarVinculoContaSalarioDestinoPDCAdapter()
				.invokeProcess(request);

		if (response == null) {
			return null;
		}

		AlterarCadContasDestinoSaidaDTO saida = new AlterarCadContasDestinoSaidaDTO(
				response.getCodMensagem(), response.getMensagem());

		return saida;
	}

	/*
	 * Detalhe Historico
	 */
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService#detalharHistVinculoContaSalarioDest(br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarDetHistContaSalarioDestinoEntradaDTO)
	 */
	public ConsultarDetHistContaSalarioDestinoSaidaDTO detalharHistVinculoContaSalarioDest(
			ConsultarDetHistContaSalarioDestinoEntradaDTO entradaDetHistorico) {

		ConsultarDetHistContaSalarioDestinoRequest request = new ConsultarDetHistContaSalarioDestinoRequest();

		request.setCdPessoaJuridNegocio(entradaDetHistorico
				.getCdPessoaJuridNegocio());
		request
				.setCdPessoaJuridVinc(entradaDetHistorico
						.getCdPessoaJuridVinc());
		request.setCdTipoContratoNegocio(entradaDetHistorico
				.getCdTipoContratoNegocio());
		request.setCdTipoContratoVinc(entradaDetHistorico
				.getCdTipoContratoVinc());
		request.setCdTipoVincContrato(entradaDetHistorico
				.getCdTipoVincContrato());
		request.setHrInclusaoRegistro(entradaDetHistorico
				.getHrInclusaoRegistro());
		request.setNrSeqContratoNegocio(entradaDetHistorico
				.getNrSeqContratoNegocio());
		request
				.setNrSeqContratoVinc(entradaDetHistorico
						.getNrSeqContratoVinc());

		ConsultarDetHistContaSalarioDestinoResponse response = getFactoryAdapter()
				.getConsultarDetHistContaSalarioDestinoPDCAdapter()
				.invokeProcess(request);

		return new ConsultarDetHistContaSalarioDestinoSaidaDTO(response
				.getOcorrencias(0));
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService#consultarDescricaoBancoAgencia(br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO)
	 */
	public ConsultarBancoAgenciaSaidaDTO consultarDescricaoBancoAgencia(
			ConsultarBancoAgenciaEntradaDTO entradaConsultarBancoAgencia) {

		ConsultarBancoAgenciaRequest request = new ConsultarBancoAgenciaRequest();
		ConsultarBancoAgenciaResponse response = new ConsultarBancoAgenciaResponse();

		request.setCdAgencia(PgitUtil
				.verificaIntegerNulo(entradaConsultarBancoAgencia
						.getCdAgencia()));
		request
				.setCdBanco(PgitUtil
						.verificaIntegerNulo(entradaConsultarBancoAgencia
								.getCdBanco()));
		request.setCdDigitoAgencia(PgitUtil
				.verificaIntegerNulo(entradaConsultarBancoAgencia
						.getCdDigitoAgencia()));
		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaConsultarBancoAgencia
						.getCdPessoaJuridicaContrato()));

		response = getFactoryAdapter().getConsultarBancoAgenciaPDCAdapter()
				.invokeProcess(request);

		if (response == null) {
			return new ConsultarBancoAgenciaSaidaDTO();
		}

		ConsultarBancoAgenciaSaidaDTO saidaRetorno = new ConsultarBancoAgenciaSaidaDTO();

		saidaRetorno.setDsBanco(response.getDsBanco());
		saidaRetorno.setDsAgencia(response.getDsAgencia());

		saidaRetorno.setCodMensagem(response.getCodMensagem());
		saidaRetorno.setMensagem(response.getMensagem());

		saidaRetorno.setCdEndereco(response.getCdEndereco());
		saidaRetorno.setLogradouro(response.getLogradouro());
		saidaRetorno.setNumero(response.getNumero());
		saidaRetorno.setBairro(response.getBairro());
		saidaRetorno.setComplemento(response.getComplemento());
		saidaRetorno.setCidade(response.getCidade());
		saidaRetorno.setEstado(response.getEstado());
		saidaRetorno.setPais(response.getPais());
		saidaRetorno.setContato(response.getContato());
		saidaRetorno.setEmail(response.getEmail());
		saidaRetorno.setCep(response.getCep());
		saidaRetorno.setComplementoCep(response.getComplementoCep());

		return saidaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService#validarServicoContratado(br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ValidarServicoContratadoEntradaDTO)
	 */
	public ValidarServicoContratadoSaidaDTO validarServicoContratado(ValidarServicoContratadoEntradaDTO entrada) {
		ValidarServicoContratadoRequest request = new ValidarServicoContratadoRequest();
		ValidarServicoContratadoResponse response = new ValidarServicoContratadoResponse();

		request.setCdProdutoServicoCdps(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServico()));
		request.setNumeroOcorrencias(entrada.getOcorrencias().size());
		for (int i = 0; i < entrada.getOcorrencias().size(); i++) {
			br.com.bradesco.web.pgit.service.data.pdc.validarservicocontratado.request.Ocorrencias ocorrenciaRequest = new br.com.bradesco.web.pgit.service.data.pdc.validarservicocontratado.request.Ocorrencias();
			
			ocorrenciaRequest.setCdProdutoServicoOperacao(entrada.getOcorrencias().get(i));
			request.addOcorrencias(ocorrenciaRequest);
		}

		response = getFactoryAdapter().getValidarServicoContratadoPDCAdapter().invokeProcess(request);

		if (response == null) {
			return new ValidarServicoContratadoSaidaDTO();
		}

		return new ValidarServicoContratadoSaidaDTO(response.getMensagem(), response.getCodMensagem());
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
}