/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;


/**
 * Nome: AlterarParticipanteContratoPgitEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarParticipanteContratoPgitEntradaDTO {

    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;

    /** Atributo cdTipoContrato. */
    private Integer cdTipoContrato;

    /** Atributo nrSequenciaContrato. */
    private Long nrSequenciaContrato;

    /** Atributo cdTipoParticipante. */
    private Integer cdTipoParticipante;

    /** Atributo cdPessoaParticipacao. */
    private Long cdPessoaParticipacao;

    /** Atributo cdclassificacaoAreaParticipante. */
    private Integer cdclassificacaoAreaParticipante;

    /**
     * Set: cdPessoaJuridica.
     *
     * @param cdPessoaJuridica the cd pessoa juridica
     */
    public void setCdPessoaJuridica(Long cdPessoaJuridica) {
        this.cdPessoaJuridica = cdPessoaJuridica;
    }

    /**
     * Get: cdPessoaJuridica.
     *
     * @return cdPessoaJuridica
     */
    public Long getCdPessoaJuridica() {
        return this.cdPessoaJuridica;
    }

    /**
     * Set: cdTipoContrato.
     *
     * @param cdTipoContrato the cd tipo contrato
     */
    public void setCdTipoContrato(Integer cdTipoContrato) {
        this.cdTipoContrato = cdTipoContrato;
    }

    /**
     * Get: cdTipoContrato.
     *
     * @return cdTipoContrato
     */
    public Integer getCdTipoContrato() {
        return this.cdTipoContrato;
    }

    /**
     * Set: nrSequenciaContrato.
     *
     * @param nrSequenciaContrato the nr sequencia contrato
     */
    public void setNrSequenciaContrato(Long nrSequenciaContrato) {
        this.nrSequenciaContrato = nrSequenciaContrato;
    }

    /**
     * Get: nrSequenciaContrato.
     *
     * @return nrSequenciaContrato
     */
    public Long getNrSequenciaContrato() {
        return this.nrSequenciaContrato;
    }

    /**
     * Set: cdTipoParticipante.
     *
     * @param cdTipoParticipante the cd tipo participante
     */
    public void setCdTipoParticipante(Integer cdTipoParticipante) {
        this.cdTipoParticipante = cdTipoParticipante;
    }

    /**
     * Get: cdTipoParticipante.
     *
     * @return cdTipoParticipante
     */
    public Integer getCdTipoParticipante() {
        return this.cdTipoParticipante;
    }

    /**
     * Set: cdPessoaParticipacao.
     *
     * @param cdPessoaParticipacao the cd pessoa participacao
     */
    public void setCdPessoaParticipacao(Long cdPessoaParticipacao) {
        this.cdPessoaParticipacao = cdPessoaParticipacao;
    }

    /**
     * Get: cdPessoaParticipacao.
     *
     * @return cdPessoaParticipacao
     */
    public Long getCdPessoaParticipacao() {
        return this.cdPessoaParticipacao;
    }

    /**
     * Set: cdclassificacaoAreaParticipante.
     *
     * @param cdclassificacaoAreaParticipante the cdclassificacao area participante
     */
    public void setCdclassificacaoAreaParticipante(Integer cdclassificacaoAreaParticipante) {
        this.cdclassificacaoAreaParticipante = cdclassificacaoAreaParticipante;
    }

    /**
     * Get: cdclassificacaoAreaParticipante.
     *
     * @return cdclassificacaoAreaParticipante
     */
    public Integer getCdclassificacaoAreaParticipante() {
        return this.cdclassificacaoAreaParticipante;
    }
}