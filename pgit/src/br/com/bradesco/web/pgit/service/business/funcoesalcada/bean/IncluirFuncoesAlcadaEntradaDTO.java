/*
 * Nome: br.com.bradesco.web.pgit.service.business.funcoesalcada.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.funcoesalcada.bean;

/**
 * Nome: IncluirFuncoesAlcadaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirFuncoesAlcadaEntradaDTO {
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo codigoAplicacao. */
	private String codigoAplicacao;
	
	/** Atributo acao. */
	private int acao;
	
	/** Atributo regra. */
	private int regra;
	
	/** Atributo servico. */
	private int servico;
	
	/** Atributo operacao. */
	private int operacao;
	
	/** Atributo tipoTratamento. */
	private int tipoTratamento;
	
	/** Atributo tipoRegra. */
	private int tipoRegra;
	
	/** Atributo tipoAcesso. */
	private int tipoAcesso; 
	
	/** Atributo centroCustoRetorno. */
	private String centroCustoRetorno;
	
	/** Atributo codigoAplicacaoRetorno. */
	private String codigoAplicacaoRetorno;
	
	/**
	 * Get: tipoAcesso.
	 *
	 * @return tipoAcesso
	 */
	public int getTipoAcesso() {
		return tipoAcesso;
	}
	
	/**
	 * Set: tipoAcesso.
	 *
	 * @param tipoAcesso the tipo acesso
	 */
	public void setTipoAcesso(int tipoAcesso) {
		this.tipoAcesso = tipoAcesso;
	}
	
	/**
	 * Get: acao.
	 *
	 * @return acao
	 */
	public int getAcao() {
		return acao;
	}
	
	/**
	 * Set: acao.
	 *
	 * @param acao the acao
	 */
	public void setAcao(int acao) {
		this.acao = acao;
	}
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	/**
	 * Get: codigoAplicacao.
	 *
	 * @return codigoAplicacao
	 */
	public String getCodigoAplicacao() {
		return codigoAplicacao;
	}
	
	/**
	 * Set: codigoAplicacao.
	 *
	 * @param codigoAplicacao the codigo aplicacao
	 */
	public void setCodigoAplicacao(String codigoAplicacao) {
		this.codigoAplicacao = codigoAplicacao;
	}
	
	/**
	 * Get: operacao.
	 *
	 * @return operacao
	 */
	public int getOperacao() {
		return operacao;
	}
	
	/**
	 * Set: operacao.
	 *
	 * @param operacao the operacao
	 */
	public void setOperacao(int operacao) {
		this.operacao = operacao;
	}
	
	/**
	 * Get: regra.
	 *
	 * @return regra
	 */
	public int getRegra() {
		return regra;
	}
	
	/**
	 * Set: regra.
	 *
	 * @param regra the regra
	 */
	public void setRegra(int regra) {
		this.regra = regra;
	}
	
	/**
	 * Get: servico.
	 *
	 * @return servico
	 */
	public int getServico() {
		return servico;
	}
	
	/**
	 * Set: servico.
	 *
	 * @param servico the servico
	 */
	public void setServico(int servico) {
		this.servico = servico;
	}
	
	/**
	 * Get: tipoRegra.
	 *
	 * @return tipoRegra
	 */
	public int getTipoRegra() {
		return tipoRegra;
	}
	
	/**
	 * Set: tipoRegra.
	 *
	 * @param tipoRegra the tipo regra
	 */
	public void setTipoRegra(int tipoRegra) {
		this.tipoRegra = tipoRegra;
	}
	
	/**
	 * Get: tipoTratamento.
	 *
	 * @return tipoTratamento
	 */
	public int getTipoTratamento() {
		return tipoTratamento;
	}
	
	/**
	 * Set: tipoTratamento.
	 *
	 * @param tipoTratamento the tipo tratamento
	 */
	public void setTipoTratamento(int tipoTratamento) {
		this.tipoTratamento = tipoTratamento;
	}
	
	/**
	 * Get: centroCustoRetorno.
	 *
	 * @return centroCustoRetorno
	 */
	public String getCentroCustoRetorno() {
		return centroCustoRetorno;
	}
	
	/**
	 * Set: centroCustoRetorno.
	 *
	 * @param centroCustoRetorno the centro custo retorno
	 */
	public void setCentroCustoRetorno(String centroCustoRetorno) {
		this.centroCustoRetorno = centroCustoRetorno;
	}
	
	/**
	 * Get: codigoAplicacaoRetorno.
	 *
	 * @return codigoAplicacaoRetorno
	 */
	public String getCodigoAplicacaoRetorno() {
		return codigoAplicacaoRetorno;
	}
	
	/**
	 * Set: codigoAplicacaoRetorno.
	 *
	 * @param codigoAplicacaoRetorno the codigo aplicacao retorno
	 */
	public void setCodigoAplicacaoRetorno(String codigoAplicacaoRetorno) {
		this.codigoAplicacaoRetorno = codigoAplicacaoRetorno;
	}
	
	

}
