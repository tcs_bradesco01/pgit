/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.ConsultarPagtosConsAutorizadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.ConsultarPagtosConsAutorizadosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DesautorizarIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DesautorizarIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DesautorizarParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DesautorizarParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DetalharPagtosConsAutorizadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarpagamentosconsolidados.bean.DetalharPagtosConsAutorizadosSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: DesautorizarPagamentosConsolidados
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IDesautorizarPagamentosConsolidadosService {

	/**
	 * Desautorizar integral.
	 *
	 * @param desautorizarIntegralEntradaDTO the desautorizar integral entrada dto
	 * @return the desautorizar integral saida dto
	 */
	DesautorizarIntegralSaidaDTO desautorizarIntegral(DesautorizarIntegralEntradaDTO desautorizarIntegralEntradaDTO);
	
	/**
	 * Desautorizar parcial.
	 *
	 * @param listaEntradaDTO the lista entrada dto
	 * @return the desautorizar parcial saida dto
	 */
	DesautorizarParcialSaidaDTO desautorizarParcial(List<DesautorizarParcialEntradaDTO> listaEntradaDTO);
	
	/**
	 * Detalhar pagtos cons autorizados.
	 *
	 * @param detalharPagtosConsAutorizadosEntradaDTO the detalhar pagtos cons autorizados entrada dto
	 * @return the detalhar pagtos cons autorizados saida dto
	 */
	DetalharPagtosConsAutorizadosSaidaDTO detalharPagtosConsAutorizados(DetalharPagtosConsAutorizadosEntradaDTO detalharPagtosConsAutorizadosEntradaDTO);
	
	/**
	 * Consultar pagtos cons autorizados.
	 *
	 * @param consultarPagtosConsAutorizadosEntradaDTO the consultar pagtos cons autorizados entrada dto
	 * @return the list< consultar pagtos cons autorizados saida dt o>
	 */
	List<ConsultarPagtosConsAutorizadosSaidaDTO> consultarPagtosConsAutorizados(ConsultarPagtosConsAutorizadosEntradaDTO consultarPagtosConsAutorizadosEntradaDTO);


}

