package br.com.bradesco.web.pgit.service.business.combo.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 22/01/16.
 */
public class ListarParametrosEntradaDTO {

    /** The nr maximo ocorrencias. */
    private Integer nrMaximoOcorrencias = null;

    /**
     * Sets the nr maximo ocorrencias.
     *
     * @param nrMaximoOcorrencias the nr maximo ocorrencias
     */
    public void setNrMaximoOcorrencias(Integer nrMaximoOcorrencias) {
        this.nrMaximoOcorrencias = nrMaximoOcorrencias;
    }

    /**
     * Gets the nr maximo ocorrencias.
     *
     * @return the nr maximo ocorrencias
     */
    public Integer getNrMaximoOcorrencias() {
        return this.nrMaximoOcorrencias;
    }
}