package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: IncluirRepresentanteSaidaDTO
 * <p>
 * Propůsito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirRepresentanteSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;

	/**
	 * @return the codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * @param codMensagem the codMensagem to set
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
