/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.tipocomprovante.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.ITipoComprovanteConstants;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.ITipoComprovanteService;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.AlterarTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.AlterarTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.DetalharTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.DetalharTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ExcluirTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ExcluirTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.IncluirTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.IncluirTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ListarTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ListarTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterartipocomprovante.request.AlterarTipoComprovanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterartipocomprovante.response.AlterarTipoComprovanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartipocomprovante.request.ConsultarDetalheTipoComprovanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalhartipocomprovante.response.ConsultarDetalheTipoComprovanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtipocomprovante.request.ExcluirTipoComprovanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirtipocomprovante.response.ExcluirTipoComprovanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirtipocomprovante.request.IncluirTipoComprovanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirtipocomprovante.response.IncluirTipoComprovanteResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.request.ListarMsgComprovanteSalrlRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsgcomprovantesalrl.response.ListarMsgComprovanteSalrlResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listartipocomprovante.request.ListarTipoComprovanteRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listartipocomprovante.response.ListarTipoComprovanteResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterTipoComprovante
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class TipoComprovanteServiceImpl implements ITipoComprovanteService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
    /**
     * Construtor.
     */
    public TipoComprovanteServiceImpl() {
        // TODO: Implementa��o
    }
    
    
    
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}



	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}



	/**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.tipocomprovante.IManterTipoComprovante#sampleManterTipoComprovante()
     */
    public void sampleManterTipoComprovante() {
        // TODO: Implementa�ao
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.tipocomprovante.ITipoComprovanteService#listarTipoComprovante(br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ListarTipoComprovanteEntradaDTO)
     */
    public List<ListarTipoComprovanteSaidaDTO> listarTipoComprovante (ListarTipoComprovanteEntradaDTO listarTipoComprovanteEntradaDTO) {
		
    	List<ListarTipoComprovanteSaidaDTO> listaRetorno = new ArrayList<ListarTipoComprovanteSaidaDTO>();
		ListarTipoComprovanteRequest request = new ListarTipoComprovanteRequest();
		ListarTipoComprovanteResponse response = new ListarTipoComprovanteResponse();
		
		request.setCdTipoComprovanteSalario(listarTipoComprovanteEntradaDTO.getCdTipoComprovante());
		request.setQtConsultas(ITipoComprovanteConstants.QTDE_CONSULTAS_LISTAR);
		
		response = getFactoryAdapter().getListarTipoComprovantePDCAdapter().invokeProcess(request);
		
		ListarTipoComprovanteSaidaDTO saidaDTO;
	
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarTipoComprovanteSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCdComprovanteSalarial(response.getOcorrencias(i).getCdTipoComprovanteSalarial());
			saidaDTO.setDsComprovanteSalarial(response.getOcorrencias(i).getDsTipoComprovanteSalarial());
			saidaDTO.setCdMensagemPadraoOrganizacao(response.getOcorrencias(i).getCdControleMensagemSalarial());
			saidaDTO.setDsMensagemPadraoOrganizacao(response.getOcorrencias(i).getCdControleMensagemSalarial() + " - " + response.getOcorrencias(i).getDsControleMensagemSalarial());
					
			listaRetorno.add(saidaDTO);
		}
		
		return listaRetorno;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.tipocomprovante.ITipoComprovanteService#incluirTipoComprovante(br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.IncluirTipoComprovanteEntradaDTO)
     */
    public IncluirTipoComprovanteSaidaDTO incluirTipoComprovante (IncluirTipoComprovanteEntradaDTO incluirTipoComprovanteEntradaDTO) {
		
    	IncluirTipoComprovanteSaidaDTO incluirTipoComprovanteSaidaDTO = new IncluirTipoComprovanteSaidaDTO();
    	IncluirTipoComprovanteRequest incluirTipoComprovanteRequest = new IncluirTipoComprovanteRequest();
    	IncluirTipoComprovanteResponse incluirTipoComprovanteResponse = new IncluirTipoComprovanteResponse();
		
    	incluirTipoComprovanteRequest.setCdTipoComprovanteSalarial(incluirTipoComprovanteEntradaDTO.getCdTipoComprovante());
    	incluirTipoComprovanteRequest.setDsTipoComprovanteSalarial(incluirTipoComprovanteEntradaDTO.getDsTipoComprovante());
    	incluirTipoComprovanteRequest.setCdControleMensagemSalarial(incluirTipoComprovanteEntradaDTO.getMsgPadraoOrganizacional());
    	    	
    	incluirTipoComprovanteResponse = getFactoryAdapter().getIncluirTipoComprovantePDCAdapter().invokeProcess(incluirTipoComprovanteRequest);
		
    	incluirTipoComprovanteSaidaDTO.setCodMensagem(incluirTipoComprovanteResponse.getCodMensagem());
    	incluirTipoComprovanteSaidaDTO.setMensagem(incluirTipoComprovanteResponse.getMensagem());
						
	
		return incluirTipoComprovanteSaidaDTO;
	}
    
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.tipocomprovante.ITipoComprovanteService#detalharTipoComprovante(br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.DetalharTipoComprovanteEntradaDTO)
     */
    public DetalharTipoComprovanteSaidaDTO detalharTipoComprovante (DetalharTipoComprovanteEntradaDTO detalharTipoComprovanteEntradaDTO) {
		
    	DetalharTipoComprovanteSaidaDTO saidaDTO = new DetalharTipoComprovanteSaidaDTO();
        ConsultarDetalheTipoComprovanteRequest request = new ConsultarDetalheTipoComprovanteRequest();
        ConsultarDetalheTipoComprovanteResponse response = new ConsultarDetalheTipoComprovanteResponse();
        
		request.setCdTipoComprovanteSalarial(detalharTipoComprovanteEntradaDTO.getCdComprovanteSalarial());
		request.setQtConsultas(0);
		
		response = getFactoryAdapter().getDetalharTipoComprovantePDCAdapter().invokeProcess(request);
		
		
		saidaDTO = new DetalharTipoComprovanteSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdTipoComprovante(response.getOcorrencias(0).getCdTipoComprovanteSalarial());
		saidaDTO.setDsTipoComprovante(response.getOcorrencias(0).getDsTipoComprovanteSalarial());
		saidaDTO.setCdMsgPadraoOrganizacional(response.getOcorrencias(0).getCdControleMensagemSalarial());
		saidaDTO.setDsMsgPadraoOrganizacional(response.getOcorrencias(0).getRsControleMensagemSalarial());
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao());
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenticacaoSegurancaInclusao());
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenticacaoSegurancaManutencao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNrOperacaoFluxoInclusao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNrOperacaoFluxoManutencao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
	
		
		return saidaDTO;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.tipocomprovante.ITipoComprovanteService#excluirTipoComprovante(br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ExcluirTipoComprovanteEntradaDTO)
     */
    public ExcluirTipoComprovanteSaidaDTO excluirTipoComprovante (ExcluirTipoComprovanteEntradaDTO excluirTipoComprovanteEntradaDTO){
    	
    	ExcluirTipoComprovanteSaidaDTO excluirTipoComprovanteSaidaDTO = new ExcluirTipoComprovanteSaidaDTO();
    	ExcluirTipoComprovanteRequest excluirTipoComprovanteRequest = new ExcluirTipoComprovanteRequest();
    	ExcluirTipoComprovanteResponse excluirTipoComprovanteResponse = new ExcluirTipoComprovanteResponse();
    	
		excluirTipoComprovanteRequest.setCdTipoComprovanteSalarial(excluirTipoComprovanteEntradaDTO.getCdTipoComprovanteSalarial());
		excluirTipoComprovanteRequest.setQtConsultas(0);
		
		excluirTipoComprovanteResponse = getFactoryAdapter().getExcluirTipoComprovantePDCAdapter().invokeProcess(excluirTipoComprovanteRequest);
		
		excluirTipoComprovanteSaidaDTO.setCodMensagem(excluirTipoComprovanteResponse.getCodMensagem());
		excluirTipoComprovanteSaidaDTO.setMensagem(excluirTipoComprovanteResponse.getMensagem());
			
	
		return excluirTipoComprovanteSaidaDTO;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.tipocomprovante.ITipoComprovanteService#alterarTipoComprovante(br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.AlterarTipoComprovanteEntradaDTO)
     */
    public AlterarTipoComprovanteSaidaDTO alterarTipoComprovante (AlterarTipoComprovanteEntradaDTO alterarTipoComprovanteEntradaDTO) {
		
    	AlterarTipoComprovanteSaidaDTO alterarTipoComprovanteSaidaDTO = new AlterarTipoComprovanteSaidaDTO();
		AlterarTipoComprovanteRequest alterarTipoComprovanteRequest = new AlterarTipoComprovanteRequest();
		AlterarTipoComprovanteResponse alterarTipoComprovanteResponse = new AlterarTipoComprovanteResponse();
		
		alterarTipoComprovanteRequest.setCdTipoComprovanteSalarial(alterarTipoComprovanteEntradaDTO.getCdTipoComprovante());
		alterarTipoComprovanteRequest.setDsTipoComprovanteSalarial(alterarTipoComprovanteEntradaDTO.getDsTipoComprovante());
		alterarTipoComprovanteRequest.setCdControleMensagemSalarial(alterarTipoComprovanteEntradaDTO.getMsgPadraoOrganizacional());
		
		alterarTipoComprovanteResponse = getFactoryAdapter().getAlterarTipoComprovantePDCAdapter().invokeProcess(alterarTipoComprovanteRequest);
	
		alterarTipoComprovanteSaidaDTO.setCodMensagem(alterarTipoComprovanteResponse.getCodMensagem());
		alterarTipoComprovanteSaidaDTO.setMensagem(alterarTipoComprovanteResponse.getMensagem());
		
				
		return alterarTipoComprovanteSaidaDTO;
		
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.tipocomprovante.ITipoComprovanteService#listarMsgComprovanteSalrl(br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlEntradaDTO)
     */
    public List<ListarMsgComprovanteSalrlSaidaDTO> listarMsgComprovanteSalrl(ListarMsgComprovanteSalrlEntradaDTO listarMsgComprovanteSalrlEntradaDTO) {
		
		List<ListarMsgComprovanteSalrlSaidaDTO> listaRetorno = new ArrayList<ListarMsgComprovanteSalrlSaidaDTO>();		
		ListarMsgComprovanteSalrlRequest request = new ListarMsgComprovanteSalrlRequest();
		ListarMsgComprovanteSalrlResponse response = new ListarMsgComprovanteSalrlResponse();
		
		request.setCdControleMensagemSalarial(listarMsgComprovanteSalrlEntradaDTO.getCdControleMensagemSalarial());		
		request.setCdTipoComprovanteSalarial(listarMsgComprovanteSalrlEntradaDTO.getCdTipoComprovanteSalarial());
		request.setCdTipoMensagemSalarial(listarMsgComprovanteSalrlEntradaDTO.getCdTipoMensagemSalarial());
		request.setCdRestMensagemSalarial(listarMsgComprovanteSalrlEntradaDTO.getCdRestMensagemSalarial());
		
		request.setNumeroOcorrencias(50);
		
		response = getFactoryAdapter().getListarMsgComprovanteSalrlPDCAdapter().invokeProcess(request);

		ListarMsgComprovanteSalrlSaidaDTO saidaDTO;
		
		for (int i=0; i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListarMsgComprovanteSalrlSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCodigo(response.getOcorrencias(i).getCdControleMensagemSalarial());
			saidaDTO.setDescricao(response.getOcorrencias(i).getDsControleMensagemSalarial());
			saidaDTO.setNivelMensagem(response.getOcorrencias(i).getCdTipoMensagemSalarial());
			saidaDTO.setRestricao(response.getOcorrencias(i).getCdRestMensagemSalarial());
			listaRetorno.add(saidaDTO);
			
		}
		
		return listaRetorno;
	}    
    
}

