/*
 * Nome: br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Nome: IncluirContratoPgitEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirContratoPgitEntradaDTO {

	/** Atributo faseContrato. */
	private Integer faseContrato;

	/** Atributo cdPessoaJuridicaNegocio. */
	private Long cdPessoaJuridicaNegocio;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;

	/** Atributo cdAcaoRelacionamento. */
	private Long cdAcaoRelacionamento;

	/** Atributo cdContratoNegocioPagamento. */
	private String cdContratoNegocioPagamento;

	/** Atributo cdIndicadorEconomicoMoeda. */
	private Integer cdIndicadorEconomicoMoeda;

	/** Atributo cdIdioma. */
	private Integer cdIdioma;

	/** Atributo dsContratoNegocio. */
	private String dsContratoNegocio;

	/** Atributo cdTipoResponsavelOrganizacao. */
	private Integer cdTipoResponsavelOrganizacao;

	/** Atributo cdFuncionalidadeGerContrato. */
	private Long cdFuncionalidadeGerContrato;

	/** Atributo cdAgenciaContrato. */
	private Integer cdAgenciaContrato;

	/** Atributo cdPessoa. */
	private Long cdPessoa;

	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;

	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;

	/** Atributo qtServico. */
	private Integer qtServico;

	/** Atributo qtContas. */
	private Integer qtContas;

	/** Atributo cdProdutoServico. */
	private Integer cdProdutoServico;

	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;

	/** Atributo cdSetor. */
	private Integer cdSetor;

	/** Atributo ocorrencias. */
	private List<OcorrenciasDTO> ocorrencias = new ArrayList<OcorrenciasDTO>();

	/** Atributo ocorrenciasConta. */
	private List<OcorrenciasContaDTO> ocorrenciasConta = new ArrayList<OcorrenciasContaDTO>();

	/** Atributo cdCpfCnpjConve. */
	private Long cdCpfCnpjConve;
	
	/** Atributo cdFilialCnpjConve. */
	private Integer cdFilialCnpjConve;
	
	/** Atributo cdControleCnpjConve. */
	private Integer cdControleCnpjConve;
	
	/** Atributo dsConveCtaSalrl. */
	private String dsConveCtaSalrl;
	
	/** Atributo cdBcoEmprConvn. */
	private Integer cdBcoEmprConvn;
	
	/** Atributo cdAgEmprConvn. */
	private Integer cdAgEmprConvn;
	
	/** Atributo cdCtaEmpresaConvn. */
	private Long cdCtaEmpresaConvn;
	
	/** Atributo cdDigEmpresaConvn. */
	private String cdDigEmpresaConvn;
	
	/** Atributo cdConveCtaSalarial. */
	private Long cdConveCtaSalarial;
	
	/** Atributo cdIndicadorConvnNovo. */
	private Integer cdIndicadorConvnNovo;

	/**
	 * Get: cdAcaoRelacionamento.
	 *
	 * @return cdAcaoRelacionamento
	 */
	public Long getCdAcaoRelacionamento() {
		return cdAcaoRelacionamento;
	}

	/**
	 * Set: cdAcaoRelacionamento.
	 *
	 * @param cdAcaoRelacionamento the cd acao relacionamento
	 */
	public void setCdAcaoRelacionamento(Long cdAcaoRelacionamento) {
		this.cdAcaoRelacionamento = cdAcaoRelacionamento;
	}

	/**
	 * Get: cdAgenciaContrato.
	 *
	 * @return cdAgenciaContrato
	 */
	public Integer getCdAgenciaContrato() {
		return cdAgenciaContrato;
	}

	/**
	 * Set: cdAgenciaContrato.
	 *
	 * @param cdAgenciaContrato the cd agencia contrato
	 */
	public void setCdAgenciaContrato(Integer cdAgenciaContrato) {
		this.cdAgenciaContrato = cdAgenciaContrato;
	}

	/**
	 * Get: cdContratoNegocioPagamento.
	 *
	 * @return cdContratoNegocioPagamento
	 */
	public String getCdContratoNegocioPagamento() {
		return cdContratoNegocioPagamento;
	}

	/**
	 * Set: cdContratoNegocioPagamento.
	 *
	 * @param cdContratoNegocioPagamento the cd contrato negocio pagamento
	 */
	public void setCdContratoNegocioPagamento(String cdContratoNegocioPagamento) {
		this.cdContratoNegocioPagamento = cdContratoNegocioPagamento;
	}

	/**
	 * Get: cdFuncionalidadeGerContrato.
	 *
	 * @return cdFuncionalidadeGerContrato
	 */
	public Long getCdFuncionalidadeGerContrato() {
		return cdFuncionalidadeGerContrato;
	}

	/**
	 * Set: cdFuncionalidadeGerContrato.
	 *
	 * @param cdFuncionalidadeGerContrato the cd funcionalidade ger contrato
	 */
	public void setCdFuncionalidadeGerContrato(Long cdFuncionalidadeGerContrato) {
		this.cdFuncionalidadeGerContrato = cdFuncionalidadeGerContrato;
	}

	/**
	 * Get: cdIdioma.
	 *
	 * @return cdIdioma
	 */
	public Integer getCdIdioma() {
		return cdIdioma;
	}

	/**
	 * Set: cdIdioma.
	 *
	 * @param cdIdioma the cd idioma
	 */
	public void setCdIdioma(Integer cdIdioma) {
		this.cdIdioma = cdIdioma;
	}

	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public Integer getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}

	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(Integer cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}

	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdPessoaJuridicaNegocio.
	 *
	 * @return cdPessoaJuridicaNegocio
	 */
	public Long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}

	/**
	 * Set: cdPessoaJuridicaNegocio.
	 *
	 * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
	 */
	public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}

	/**
	 * Get: cdProdutoServico.
	 *
	 * @return cdProdutoServico
	 */
	public Integer getCdProdutoServico() {
		return cdProdutoServico;
	}

	/**
	 * Set: cdProdutoServico.
	 *
	 * @param cdProdutoServico the cd produto servico
	 */
	public void setCdProdutoServico(Integer cdProdutoServico) {
		this.cdProdutoServico = cdProdutoServico;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: cdTipoResponsavelOrganizacao.
	 *
	 * @return cdTipoResponsavelOrganizacao
	 */
	public Integer getCdTipoResponsavelOrganizacao() {
		return cdTipoResponsavelOrganizacao;
	}

	/**
	 * Set: cdTipoResponsavelOrganizacao.
	 *
	 * @param cdTipoResponsavelOrganizacao the cd tipo responsavel organizacao
	 */
	public void setCdTipoResponsavelOrganizacao(
			Integer cdTipoResponsavelOrganizacao) {
		this.cdTipoResponsavelOrganizacao = cdTipoResponsavelOrganizacao;
	}

	/**
	 * Get: dsContratoNegocio.
	 *
	 * @return dsContratoNegocio
	 */
	public String getDsContratoNegocio() {
		return dsContratoNegocio;
	}

	/**
	 * Set: dsContratoNegocio.
	 *
	 * @param dsContratoNegocio the ds contrato negocio
	 */
	public void setDsContratoNegocio(String dsContratoNegocio) {
		this.dsContratoNegocio = dsContratoNegocio;
	}

	/**
	 * Get: faseContrato.
	 *
	 * @return faseContrato
	 */
	public Integer getFaseContrato() {
		return faseContrato;
	}

	/**
	 * Set: faseContrato.
	 *
	 * @param faseContrato the fase contrato
	 */
	public void setFaseContrato(Integer faseContrato) {
		this.faseContrato = faseContrato;
	}

	/**
	 * Get: ocorrencias.
	 *
	 * @return ocorrencias
	 */
	public List<OcorrenciasDTO> getOcorrencias() {
		return ocorrencias;
	}

	/**
	 * Set: ocorrencias.
	 *
	 * @param ocorrencias the ocorrencias
	 */
	public void setOcorrencias(List<OcorrenciasDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

	/**
	 * Get: qtServico.
	 *
	 * @return qtServico
	 */
	public Integer getQtServico() {
		return qtServico;
	}

	/**
	 * Set: qtServico.
	 *
	 * @param qtServico the qt servico
	 */
	public void setQtServico(Integer qtServico) {
		this.qtServico = qtServico;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdSetor.
	 *
	 * @return cdSetor
	 */
	public Integer getCdSetor() {
		return cdSetor;
	}

	/**
	 * Set: cdSetor.
	 *
	 * @param cdSetor the cd setor
	 */
	public void setCdSetor(Integer cdSetor) {
		this.cdSetor = cdSetor;
	}

	/**
	 * Get: ocorrenciasConta.
	 *
	 * @return ocorrenciasConta
	 */
	public List<OcorrenciasContaDTO> getOcorrenciasConta() {
		return ocorrenciasConta;
	}

	/**
	 * Set: ocorrenciasConta.
	 *
	 * @param ocorrenciasConta the ocorrencias conta
	 */
	public void setOcorrenciasConta(List<OcorrenciasContaDTO> ocorrenciasConta) {
		this.ocorrenciasConta = ocorrenciasConta;
	}

	/**
	 * Get: qtContas.
	 *
	 * @return qtContas
	 */
	public Integer getQtContas() {
		return qtContas;
	}

	/**
	 * Set: qtContas.
	 *
	 * @param qtContas the qt contas
	 */
	public void setQtContas(Integer qtContas) {
		this.qtContas = qtContas;
	}

	/**
	 * Get: cdCpfCnpjConve.
	 *
	 * @return cdCpfCnpjConve
	 */
	public Long getCdCpfCnpjConve() {
		return cdCpfCnpjConve;
	}

	/**
	 * Set: cdCpfCnpjConve.
	 *
	 * @param cdCpfCnpjConve the cd cpf cnpj conve
	 */
	public void setCdCpfCnpjConve(Long cdCpfCnpjConve) {
		this.cdCpfCnpjConve = cdCpfCnpjConve;
	}

	/**
	 * Get: cdFilialCnpjConve.
	 *
	 * @return cdFilialCnpjConve
	 */
	public Integer getCdFilialCnpjConve() {
		return cdFilialCnpjConve;
	}

	/**
	 * Set: cdFilialCnpjConve.
	 *
	 * @param cdFilialCnpjConve the cd filial cnpj conve
	 */
	public void setCdFilialCnpjConve(Integer cdFilialCnpjConve) {
		this.cdFilialCnpjConve = cdFilialCnpjConve;
	}

	/**
	 * Get: cdControleCnpjConve.
	 *
	 * @return cdControleCnpjConve
	 */
	public Integer getCdControleCnpjConve() {
		return cdControleCnpjConve;
	}

	/**
	 * Set: cdControleCnpjConve.
	 *
	 * @param cdControleCnpjConve the cd controle cnpj conve
	 */
	public void setCdControleCnpjConve(Integer cdControleCnpjConve) {
		this.cdControleCnpjConve = cdControleCnpjConve;
	}

	/**
	 * Get: dsConveCtaSalrl.
	 *
	 * @return dsConveCtaSalrl
	 */
	public String getDsConveCtaSalrl() {
		return dsConveCtaSalrl;
	}

	/**
	 * Set: dsConveCtaSalrl.
	 *
	 * @param dsConveCtaSalrl the ds conve cta salrl
	 */
	public void setDsConveCtaSalrl(String dsConveCtaSalrl) {
		this.dsConveCtaSalrl = dsConveCtaSalrl;
	}

	/**
	 * Get: cdBcoEmprConvn.
	 *
	 * @return cdBcoEmprConvn
	 */
	public Integer getCdBcoEmprConvn() {
		return cdBcoEmprConvn;
	}

	/**
	 * Set: cdBcoEmprConvn.
	 *
	 * @param cdBcoEmprConvn the cd bco empr convn
	 */
	public void setCdBcoEmprConvn(Integer cdBcoEmprConvn) {
		this.cdBcoEmprConvn = cdBcoEmprConvn;
	}

	/**
	 * Get: cdAgEmprConvn.
	 *
	 * @return cdAgEmprConvn
	 */
	public Integer getCdAgEmprConvn() {
		return cdAgEmprConvn;
	}

	/**
	 * Set: cdAgEmprConvn.
	 *
	 * @param cdAgEmprConvn the cd ag empr convn
	 */
	public void setCdAgEmprConvn(Integer cdAgEmprConvn) {
		this.cdAgEmprConvn = cdAgEmprConvn;
	}

	/**
	 * Get: cdCtaEmpresaConvn.
	 *
	 * @return cdCtaEmpresaConvn
	 */
	public Long getCdCtaEmpresaConvn() {
		return cdCtaEmpresaConvn;
	}

	/**
	 * Set: cdCtaEmpresaConvn.
	 *
	 * @param cdCtaEmpresaConvn the cd cta empresa convn
	 */
	public void setCdCtaEmpresaConvn(Long cdCtaEmpresaConvn) {
		this.cdCtaEmpresaConvn = cdCtaEmpresaConvn;
	}

	/**
	 * Get: cdDigEmpresaConvn.
	 *
	 * @return cdDigEmpresaConvn
	 */
	public String getCdDigEmpresaConvn() {
		return cdDigEmpresaConvn;
	}

	/**
	 * Set: cdDigEmpresaConvn.
	 *
	 * @param cdDigEmpresaConvn the cd dig empresa convn
	 */
	public void setCdDigEmpresaConvn(String cdDigEmpresaConvn) {
		this.cdDigEmpresaConvn = cdDigEmpresaConvn;
	}

	/**
	 * Get: cdConveCtaSalarial.
	 *
	 * @return cdConveCtaSalarial
	 */
	public Long getCdConveCtaSalarial() {
		return cdConveCtaSalarial;
	}

	/**
	 * Set: cdConveCtaSalarial.
	 *
	 * @param cdConveCtaSalarial the cd conve cta salarial
	 */
	public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
		this.cdConveCtaSalarial = cdConveCtaSalarial;
	}

	/**
	 * Get: cdIndicadorConvnNovo.
	 *
	 * @return cdIndicadorConvnNovo
	 */
	public Integer getCdIndicadorConvnNovo() {
		return cdIndicadorConvnNovo;
	}

	/**
	 * Set: cdIndicadorConvnNovo.
	 *
	 * @param cdIndicadorConvnNovo the cd indicador convn novo
	 */
	public void setCdIndicadorConvnNovo(Integer cdIndicadorConvnNovo) {
		this.cdIndicadorConvnNovo = cdIndicadorConvnNovo;
	}
}