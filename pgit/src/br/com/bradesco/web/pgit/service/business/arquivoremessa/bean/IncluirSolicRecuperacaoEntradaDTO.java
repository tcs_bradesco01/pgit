/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoremessa.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoremessa.bean;

import java.util.List;

/**
 * Nome: IncluirSolicRecuperacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirSolicRecuperacaoEntradaDTO {
	
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdDestinoRelat. */
    private String cdDestinoRelat;
    
    /** Atributo cdDestinoIsd. */
    private String cdDestinoIsd;
    
    /** Atributo cdDestinoRet. */
    private String cdDestinoRet;      
    
    /** Atributo listaIncluirSolicRecuperacao. */
    private List<ConsultarArquivoRecuperacaoSaidaDTO> listaIncluirSolicRecuperacao;
    
    /** Atributo qtdeOcorrencias. */
    private Integer qtdeOcorrencias;
    
	/**
	 * Get: cdDestinoIsd.
	 *
	 * @return cdDestinoIsd
	 */
	public String getCdDestinoIsd() {
		return cdDestinoIsd;
	}
	
	/**
	 * Set: cdDestinoIsd.
	 *
	 * @param cdDestinoIsd the cd destino isd
	 */
	public void setCdDestinoIsd(String cdDestinoIsd) {
		this.cdDestinoIsd = cdDestinoIsd;
	}
	
	/**
	 * Get: cdDestinoRelat.
	 *
	 * @return cdDestinoRelat
	 */
	public String getCdDestinoRelat() {
		return cdDestinoRelat;
	}
	
	/**
	 * Set: cdDestinoRelat.
	 *
	 * @param cdDestinoRelat the cd destino relat
	 */
	public void setCdDestinoRelat(String cdDestinoRelat) {
		this.cdDestinoRelat = cdDestinoRelat;
	}
	
	/**
	 * Get: cdDestinoRet.
	 *
	 * @return cdDestinoRet
	 */
	public String getCdDestinoRet() {
		return cdDestinoRet;
	}
	
	/**
	 * Set: cdDestinoRet.
	 *
	 * @param cdDestinoRet the cd destino ret
	 */
	public void setCdDestinoRet(String cdDestinoRet) {
		this.cdDestinoRet = cdDestinoRet;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: listaIncluirSolicRecuperacao.
	 *
	 * @return listaIncluirSolicRecuperacao
	 */
	public List<ConsultarArquivoRecuperacaoSaidaDTO> getListaIncluirSolicRecuperacao() {
		return listaIncluirSolicRecuperacao;
	}
	
	/**
	 * Set: listaIncluirSolicRecuperacao.
	 *
	 * @param listaIncluirSolicRecuperacao the lista incluir solic recuperacao
	 */
	public void setListaIncluirSolicRecuperacao(
			List<ConsultarArquivoRecuperacaoSaidaDTO> listaIncluirSolicRecuperacao) {
		this.listaIncluirSolicRecuperacao = listaIncluirSolicRecuperacao;
	}
	
	/**
	 * Get: qtdeOcorrencias.
	 *
	 * @return qtdeOcorrencias
	 */
	public Integer getQtdeOcorrencias() {
		return qtdeOcorrencias;
	}
	
	/**
	 * Set: qtdeOcorrencias.
	 *
	 * @param qtdeOcorrencias the qtde ocorrencias
	 */
	public void setQtdeOcorrencias(Integer qtdeOcorrencias) {
		this.qtdeOcorrencias = qtdeOcorrencias;
	}
    
    
    
    
  
    
	
    
    
    

}
