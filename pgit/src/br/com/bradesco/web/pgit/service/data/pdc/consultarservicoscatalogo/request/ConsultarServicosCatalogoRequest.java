/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarServicosCatalogoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarServicosCatalogoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdProducaoServicoCdps
     */
    private int _cdProducaoServicoCdps = 0;

    /**
     * keeps track of state for field: _cdProducaoServicoCdps
     */
    private boolean _has_cdProducaoServicoCdps;

    /**
     * Field _cdProdtRelacionadoCdps
     */
    private int _cdProdtRelacionadoCdps = 0;

    /**
     * keeps track of state for field: _cdProdtRelacionadoCdps
     */
    private boolean _has_cdProdtRelacionadoCdps;

    /**
     * Field _cdProdutoPgit
     */
    private long _cdProdutoPgit = 0;

    /**
     * keeps track of state for field: _cdProdutoPgit
     */
    private boolean _has_cdProdutoPgit;

    /**
     * Field _cdTipoAcesso
     */
    private int _cdTipoAcesso = 0;

    /**
     * keeps track of state for field: _cdTipoAcesso
     */
    private boolean _has_cdTipoAcesso;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarServicosCatalogoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.request.ConsultarServicosCatalogoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdProdtRelacionadoCdps
     * 
     */
    public void deleteCdProdtRelacionadoCdps()
    {
        this._has_cdProdtRelacionadoCdps= false;
    } //-- void deleteCdProdtRelacionadoCdps() 

    /**
     * Method deleteCdProducaoServicoCdps
     * 
     */
    public void deleteCdProducaoServicoCdps()
    {
        this._has_cdProducaoServicoCdps= false;
    } //-- void deleteCdProducaoServicoCdps() 

    /**
     * Method deleteCdProdutoPgit
     * 
     */
    public void deleteCdProdutoPgit()
    {
        this._has_cdProdutoPgit= false;
    } //-- void deleteCdProdutoPgit() 

    /**
     * Method deleteCdTipoAcesso
     * 
     */
    public void deleteCdTipoAcesso()
    {
        this._has_cdTipoAcesso= false;
    } //-- void deleteCdTipoAcesso() 

    /**
     * Returns the value of field 'cdProdtRelacionadoCdps'.
     * 
     * @return int
     * @return the value of field 'cdProdtRelacionadoCdps'.
     */
    public int getCdProdtRelacionadoCdps()
    {
        return this._cdProdtRelacionadoCdps;
    } //-- int getCdProdtRelacionadoCdps() 

    /**
     * Returns the value of field 'cdProducaoServicoCdps'.
     * 
     * @return int
     * @return the value of field 'cdProducaoServicoCdps'.
     */
    public int getCdProducaoServicoCdps()
    {
        return this._cdProducaoServicoCdps;
    } //-- int getCdProducaoServicoCdps() 

    /**
     * Returns the value of field 'cdProdutoPgit'.
     * 
     * @return long
     * @return the value of field 'cdProdutoPgit'.
     */
    public long getCdProdutoPgit()
    {
        return this._cdProdutoPgit;
    } //-- long getCdProdutoPgit() 

    /**
     * Returns the value of field 'cdTipoAcesso'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcesso'.
     */
    public int getCdTipoAcesso()
    {
        return this._cdTipoAcesso;
    } //-- int getCdTipoAcesso() 

    /**
     * Method hasCdProdtRelacionadoCdps
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdtRelacionadoCdps()
    {
        return this._has_cdProdtRelacionadoCdps;
    } //-- boolean hasCdProdtRelacionadoCdps() 

    /**
     * Method hasCdProducaoServicoCdps
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProducaoServicoCdps()
    {
        return this._has_cdProducaoServicoCdps;
    } //-- boolean hasCdProducaoServicoCdps() 

    /**
     * Method hasCdProdutoPgit
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoPgit()
    {
        return this._has_cdProdutoPgit;
    } //-- boolean hasCdProdutoPgit() 

    /**
     * Method hasCdTipoAcesso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcesso()
    {
        return this._has_cdTipoAcesso;
    } //-- boolean hasCdTipoAcesso() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdProdtRelacionadoCdps'.
     * 
     * @param cdProdtRelacionadoCdps the value of field
     * 'cdProdtRelacionadoCdps'.
     */
    public void setCdProdtRelacionadoCdps(int cdProdtRelacionadoCdps)
    {
        this._cdProdtRelacionadoCdps = cdProdtRelacionadoCdps;
        this._has_cdProdtRelacionadoCdps = true;
    } //-- void setCdProdtRelacionadoCdps(int) 

    /**
     * Sets the value of field 'cdProducaoServicoCdps'.
     * 
     * @param cdProducaoServicoCdps the value of field
     * 'cdProducaoServicoCdps'.
     */
    public void setCdProducaoServicoCdps(int cdProducaoServicoCdps)
    {
        this._cdProducaoServicoCdps = cdProducaoServicoCdps;
        this._has_cdProducaoServicoCdps = true;
    } //-- void setCdProducaoServicoCdps(int) 

    /**
     * Sets the value of field 'cdProdutoPgit'.
     * 
     * @param cdProdutoPgit the value of field 'cdProdutoPgit'.
     */
    public void setCdProdutoPgit(long cdProdutoPgit)
    {
        this._cdProdutoPgit = cdProdutoPgit;
        this._has_cdProdutoPgit = true;
    } //-- void setCdProdutoPgit(long) 

    /**
     * Sets the value of field 'cdTipoAcesso'.
     * 
     * @param cdTipoAcesso the value of field 'cdTipoAcesso'.
     */
    public void setCdTipoAcesso(int cdTipoAcesso)
    {
        this._cdTipoAcesso = cdTipoAcesso;
        this._has_cdTipoAcesso = true;
    } //-- void setCdTipoAcesso(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarServicosCatalogoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.request.ConsultarServicosCatalogoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.request.ConsultarServicosCatalogoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.request.ConsultarServicosCatalogoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarservicoscatalogo.request.ConsultarServicosCatalogoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
