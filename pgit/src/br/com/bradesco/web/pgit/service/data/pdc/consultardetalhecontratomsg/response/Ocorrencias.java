/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCtrlMensagemSalarial
     */
    private int _cdCtrlMensagemSalarial = 0;

    /**
     * keeps track of state for field: _cdCtrlMensagemSalarial
     */
    private boolean _has_cdCtrlMensagemSalarial;

    /**
     * Field _dsCtrlMensagemSalarial
     */
    private java.lang.String _dsCtrlMensagemSalarial;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSeqContrato
     */
    private long _nrSeqContrato = 0;

    /**
     * keeps track of state for field: _nrSeqContrato
     */
    private boolean _has_nrSeqContrato;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenSegrcInclusao
     */
    private java.lang.String _cdAutenSegrcInclusao;

    /**
     * Field _tpUsuarioInclusao
     */
    private java.lang.String _tpUsuarioInclusao;

    /**
     * Field _nmOperFluxoInclusao
     */
    private java.lang.String _nmOperFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenSegrcManutencao
     */
    private java.lang.String _cdAutenSegrcManutencao;

    /**
     * Field _tpUsuarioManutencao
     */
    private java.lang.String _tpUsuarioManutencao;

    /**
     * Field _nmOperFluxoManutencao
     */
    private java.lang.String _nmOperFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdCtrlMensagemSalarial
     * 
     */
    public void deleteCdCtrlMensagemSalarial()
    {
        this._has_cdCtrlMensagemSalarial= false;
    } //-- void deleteCdCtrlMensagemSalarial() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteNrSeqContrato
     * 
     */
    public void deleteNrSeqContrato()
    {
        this._has_nrSeqContrato= false;
    } //-- void deleteNrSeqContrato() 

    /**
     * Returns the value of field 'cdAutenSegrcInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcInclusao'.
     */
    public java.lang.String getCdAutenSegrcInclusao()
    {
        return this._cdAutenSegrcInclusao;
    } //-- java.lang.String getCdAutenSegrcInclusao() 

    /**
     * Returns the value of field 'cdAutenSegrcManutencao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcManutencao'.
     */
    public java.lang.String getCdAutenSegrcManutencao()
    {
        return this._cdAutenSegrcManutencao;
    } //-- java.lang.String getCdAutenSegrcManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdCtrlMensagemSalarial'.
     * 
     * @return int
     * @return the value of field 'cdCtrlMensagemSalarial'.
     */
    public int getCdCtrlMensagemSalarial()
    {
        return this._cdCtrlMensagemSalarial;
    } //-- int getCdCtrlMensagemSalarial() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsCtrlMensagemSalarial'.
     * 
     * @return String
     * @return the value of field 'dsCtrlMensagemSalarial'.
     */
    public java.lang.String getDsCtrlMensagemSalarial()
    {
        return this._dsCtrlMensagemSalarial;
    } //-- java.lang.String getDsCtrlMensagemSalarial() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'nmOperFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoInclusao'.
     */
    public java.lang.String getNmOperFluxoInclusao()
    {
        return this._nmOperFluxoInclusao;
    } //-- java.lang.String getNmOperFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoManutencao'.
     */
    public java.lang.String getNmOperFluxoManutencao()
    {
        return this._nmOperFluxoManutencao;
    } //-- java.lang.String getNmOperFluxoManutencao() 

    /**
     * Returns the value of field 'nrSeqContrato'.
     * 
     * @return long
     * @return the value of field 'nrSeqContrato'.
     */
    public long getNrSeqContrato()
    {
        return this._nrSeqContrato;
    } //-- long getNrSeqContrato() 

    /**
     * Returns the value of field 'tpUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'tpUsuarioInclusao'.
     */
    public java.lang.String getTpUsuarioInclusao()
    {
        return this._tpUsuarioInclusao;
    } //-- java.lang.String getTpUsuarioInclusao() 

    /**
     * Returns the value of field 'tpUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'tpUsuarioManutencao'.
     */
    public java.lang.String getTpUsuarioManutencao()
    {
        return this._tpUsuarioManutencao;
    } //-- java.lang.String getTpUsuarioManutencao() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdCtrlMensagemSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlMensagemSalarial()
    {
        return this._has_cdCtrlMensagemSalarial;
    } //-- boolean hasCdCtrlMensagemSalarial() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasNrSeqContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSeqContrato()
    {
        return this._has_nrSeqContrato;
    } //-- boolean hasNrSeqContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenSegrcInclusao'.
     * 
     * @param cdAutenSegrcInclusao the value of field
     * 'cdAutenSegrcInclusao'.
     */
    public void setCdAutenSegrcInclusao(java.lang.String cdAutenSegrcInclusao)
    {
        this._cdAutenSegrcInclusao = cdAutenSegrcInclusao;
    } //-- void setCdAutenSegrcInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenSegrcManutencao'.
     * 
     * @param cdAutenSegrcManutencao the value of field
     * 'cdAutenSegrcManutencao'.
     */
    public void setCdAutenSegrcManutencao(java.lang.String cdAutenSegrcManutencao)
    {
        this._cdAutenSegrcManutencao = cdAutenSegrcManutencao;
    } //-- void setCdAutenSegrcManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdCtrlMensagemSalarial'.
     * 
     * @param cdCtrlMensagemSalarial the value of field
     * 'cdCtrlMensagemSalarial'.
     */
    public void setCdCtrlMensagemSalarial(int cdCtrlMensagemSalarial)
    {
        this._cdCtrlMensagemSalarial = cdCtrlMensagemSalarial;
        this._has_cdCtrlMensagemSalarial = true;
    } //-- void setCdCtrlMensagemSalarial(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsCtrlMensagemSalarial'.
     * 
     * @param dsCtrlMensagemSalarial the value of field
     * 'dsCtrlMensagemSalarial'.
     */
    public void setDsCtrlMensagemSalarial(java.lang.String dsCtrlMensagemSalarial)
    {
        this._dsCtrlMensagemSalarial = dsCtrlMensagemSalarial;
    } //-- void setDsCtrlMensagemSalarial(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoInclusao'.
     * 
     * @param nmOperFluxoInclusao the value of field
     * 'nmOperFluxoInclusao'.
     */
    public void setNmOperFluxoInclusao(java.lang.String nmOperFluxoInclusao)
    {
        this._nmOperFluxoInclusao = nmOperFluxoInclusao;
    } //-- void setNmOperFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoManutencao'.
     * 
     * @param nmOperFluxoManutencao the value of field
     * 'nmOperFluxoManutencao'.
     */
    public void setNmOperFluxoManutencao(java.lang.String nmOperFluxoManutencao)
    {
        this._nmOperFluxoManutencao = nmOperFluxoManutencao;
    } //-- void setNmOperFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrSeqContrato'.
     * 
     * @param nrSeqContrato the value of field 'nrSeqContrato'.
     */
    public void setNrSeqContrato(long nrSeqContrato)
    {
        this._nrSeqContrato = nrSeqContrato;
        this._has_nrSeqContrato = true;
    } //-- void setNrSeqContrato(long) 

    /**
     * Sets the value of field 'tpUsuarioInclusao'.
     * 
     * @param tpUsuarioInclusao the value of field
     * 'tpUsuarioInclusao'.
     */
    public void setTpUsuarioInclusao(java.lang.String tpUsuarioInclusao)
    {
        this._tpUsuarioInclusao = tpUsuarioInclusao;
    } //-- void setTpUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'tpUsuarioManutencao'.
     * 
     * @param tpUsuarioManutencao the value of field
     * 'tpUsuarioManutencao'.
     */
    public void setTpUsuarioManutencao(java.lang.String tpUsuarioManutencao)
    {
        this._tpUsuarioManutencao = tpUsuarioManutencao;
    } //-- void setTpUsuarioManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhecontratomsg.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
