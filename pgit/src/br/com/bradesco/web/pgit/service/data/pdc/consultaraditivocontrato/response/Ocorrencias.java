/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrAditivoContratoNegocio
     */
    private long _nrAditivoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrAditivoContratoNegocio
     */
    private boolean _has_nrAditivoContratoNegocio;

    /**
     * Field _dtInclusaoAditivoContrato
     */
    private java.lang.String _dtInclusaoAditivoContrato;

    /**
     * Field _hrInclusaoAditivoContrato
     */
    private java.lang.String _hrInclusaoAditivoContrato;

    /**
     * Field _cdSituacaoAditivoContrato
     */
    private int _cdSituacaoAditivoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoAditivoContrato
     */
    private boolean _has_cdSituacaoAditivoContrato;

    /**
     * Field _dsSitucaoAditivoContrato
     */
    private java.lang.String _dsSitucaoAditivoContrato;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoAditivoContrato
     * 
     */
    public void deleteCdSituacaoAditivoContrato()
    {
        this._has_cdSituacaoAditivoContrato= false;
    } //-- void deleteCdSituacaoAditivoContrato() 

    /**
     * Method deleteNrAditivoContratoNegocio
     * 
     */
    public void deleteNrAditivoContratoNegocio()
    {
        this._has_nrAditivoContratoNegocio= false;
    } //-- void deleteNrAditivoContratoNegocio() 

    /**
     * Returns the value of field 'cdSituacaoAditivoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoAditivoContrato'.
     */
    public int getCdSituacaoAditivoContrato()
    {
        return this._cdSituacaoAditivoContrato;
    } //-- int getCdSituacaoAditivoContrato() 

    /**
     * Returns the value of field 'dsSitucaoAditivoContrato'.
     * 
     * @return String
     * @return the value of field 'dsSitucaoAditivoContrato'.
     */
    public java.lang.String getDsSitucaoAditivoContrato()
    {
        return this._dsSitucaoAditivoContrato;
    } //-- java.lang.String getDsSitucaoAditivoContrato() 

    /**
     * Returns the value of field 'dtInclusaoAditivoContrato'.
     * 
     * @return String
     * @return the value of field 'dtInclusaoAditivoContrato'.
     */
    public java.lang.String getDtInclusaoAditivoContrato()
    {
        return this._dtInclusaoAditivoContrato;
    } //-- java.lang.String getDtInclusaoAditivoContrato() 

    /**
     * Returns the value of field 'hrInclusaoAditivoContrato'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoAditivoContrato'.
     */
    public java.lang.String getHrInclusaoAditivoContrato()
    {
        return this._hrInclusaoAditivoContrato;
    } //-- java.lang.String getHrInclusaoAditivoContrato() 

    /**
     * Returns the value of field 'nrAditivoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrAditivoContratoNegocio'.
     */
    public long getNrAditivoContratoNegocio()
    {
        return this._nrAditivoContratoNegocio;
    } //-- long getNrAditivoContratoNegocio() 

    /**
     * Method hasCdSituacaoAditivoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoAditivoContrato()
    {
        return this._has_cdSituacaoAditivoContrato;
    } //-- boolean hasCdSituacaoAditivoContrato() 

    /**
     * Method hasNrAditivoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrAditivoContratoNegocio()
    {
        return this._has_nrAditivoContratoNegocio;
    } //-- boolean hasNrAditivoContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoAditivoContrato'.
     * 
     * @param cdSituacaoAditivoContrato the value of field
     * 'cdSituacaoAditivoContrato'.
     */
    public void setCdSituacaoAditivoContrato(int cdSituacaoAditivoContrato)
    {
        this._cdSituacaoAditivoContrato = cdSituacaoAditivoContrato;
        this._has_cdSituacaoAditivoContrato = true;
    } //-- void setCdSituacaoAditivoContrato(int) 

    /**
     * Sets the value of field 'dsSitucaoAditivoContrato'.
     * 
     * @param dsSitucaoAditivoContrato the value of field
     * 'dsSitucaoAditivoContrato'.
     */
    public void setDsSitucaoAditivoContrato(java.lang.String dsSitucaoAditivoContrato)
    {
        this._dsSitucaoAditivoContrato = dsSitucaoAditivoContrato;
    } //-- void setDsSitucaoAditivoContrato(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusaoAditivoContrato'.
     * 
     * @param dtInclusaoAditivoContrato the value of field
     * 'dtInclusaoAditivoContrato'.
     */
    public void setDtInclusaoAditivoContrato(java.lang.String dtInclusaoAditivoContrato)
    {
        this._dtInclusaoAditivoContrato = dtInclusaoAditivoContrato;
    } //-- void setDtInclusaoAditivoContrato(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoAditivoContrato'.
     * 
     * @param hrInclusaoAditivoContrato the value of field
     * 'hrInclusaoAditivoContrato'.
     */
    public void setHrInclusaoAditivoContrato(java.lang.String hrInclusaoAditivoContrato)
    {
        this._hrInclusaoAditivoContrato = hrInclusaoAditivoContrato;
    } //-- void setHrInclusaoAditivoContrato(java.lang.String) 

    /**
     * Sets the value of field 'nrAditivoContratoNegocio'.
     * 
     * @param nrAditivoContratoNegocio the value of field
     * 'nrAditivoContratoNegocio'.
     */
    public void setNrAditivoContratoNegocio(long nrAditivoContratoNegocio)
    {
        this._nrAditivoContratoNegocio = nrAditivoContratoNegocio;
        this._has_nrAditivoContratoNegocio = true;
    } //-- void setNrAditivoContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultaraditivocontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
