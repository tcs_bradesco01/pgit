/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: IncluirExcRegraSegregacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirExcRegraSegregacaoEntradaDTO {
	
	/** Atributo codigoRegra. */
	private int codigoRegra;	
	
	/** Atributo tipoUnidadeOrganizacionalUsuario. */
	private int tipoUnidadeOrganizacionalUsuario;
	
	/** Atributo empresaConglomeradoUsuario. */
	private long empresaConglomeradoUsuario;
	
	/** Atributo unidadeOrganizacionalUsuario. */
	private int unidadeOrganizacionalUsuario;
	
	/** Atributo tipoUnidadeOrganizacionalProprietario. */
	private int tipoUnidadeOrganizacionalProprietario;
	
	/** Atributo empresaConglomeradoProprietario. */
	private long empresaConglomeradoProprietario;
	
	/** Atributo unidadeOrganizacionalProprietario. */
	private int unidadeOrganizacionalProprietario;
	
	/** Atributo tipoExcecao. */
	private int tipoExcecao;
	
	/** Atributo tipoAbrangenciaExcecao. */
	private int tipoAbrangenciaExcecao;
	
	
	/**
	 * Get: codigoRegra.
	 *
	 * @return codigoRegra
	 */
	public int getCodigoRegra() {
		return codigoRegra;
	}
	
	/**
	 * Set: codigoRegra.
	 *
	 * @param codigoRegra the codigo regra
	 */
	public void setCodigoRegra(int codigoRegra) {
		this.codigoRegra = codigoRegra;
	}
	
	/**
	 * Get: empresaConglomeradoProprietario.
	 *
	 * @return empresaConglomeradoProprietario
	 */
	public long getEmpresaConglomeradoProprietario() {
		return empresaConglomeradoProprietario;
	}
	
	/**
	 * Set: empresaConglomeradoProprietario.
	 *
	 * @param empresaConglomeradoProprietario the empresa conglomerado proprietario
	 */
	public void setEmpresaConglomeradoProprietario(
			long empresaConglomeradoProprietario) {
		this.empresaConglomeradoProprietario = empresaConglomeradoProprietario;
	}
	
	/**
	 * Get: empresaConglomeradoUsuario.
	 *
	 * @return empresaConglomeradoUsuario
	 */
	public long getEmpresaConglomeradoUsuario() {
		return empresaConglomeradoUsuario;
	}
	
	/**
	 * Set: empresaConglomeradoUsuario.
	 *
	 * @param empresaConglomeradoUsuario the empresa conglomerado usuario
	 */
	public void setEmpresaConglomeradoUsuario(long empresaConglomeradoUsuario) {
		this.empresaConglomeradoUsuario = empresaConglomeradoUsuario;
	}
	
	/**
	 * Get: tipoAbrangenciaExcecao.
	 *
	 * @return tipoAbrangenciaExcecao
	 */
	public int getTipoAbrangenciaExcecao() {
		return tipoAbrangenciaExcecao;
	}
	
	/**
	 * Set: tipoAbrangenciaExcecao.
	 *
	 * @param tipoAbrangenciaExcecao the tipo abrangencia excecao
	 */
	public void setTipoAbrangenciaExcecao(int tipoAbrangenciaExcecao) {
		this.tipoAbrangenciaExcecao = tipoAbrangenciaExcecao;
	}
	
	/**
	 * Get: tipoExcecao.
	 *
	 * @return tipoExcecao
	 */
	public int getTipoExcecao() {
		return tipoExcecao;
	}
	
	/**
	 * Set: tipoExcecao.
	 *
	 * @param tipoExcecao the tipo excecao
	 */
	public void setTipoExcecao(int tipoExcecao) {
		this.tipoExcecao = tipoExcecao;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalProprietario.
	 *
	 * @return tipoUnidadeOrganizacionalProprietario
	 */
	public int getTipoUnidadeOrganizacionalProprietario() {
		return tipoUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalProprietario.
	 *
	 * @param tipoUnidadeOrganizacionalProprietario the tipo unidade organizacional proprietario
	 */
	public void setTipoUnidadeOrganizacionalProprietario(
			int tipoUnidadeOrganizacionalProprietario) {
		this.tipoUnidadeOrganizacionalProprietario = tipoUnidadeOrganizacionalProprietario;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @return tipoUnidadeOrganizacionalUsuario
	 */
	public int getTipoUnidadeOrganizacionalUsuario() {
		return tipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @param tipoUnidadeOrganizacionalUsuario the tipo unidade organizacional usuario
	 */
	public void setTipoUnidadeOrganizacionalUsuario(
			int tipoUnidadeOrganizacionalUsuario) {
		this.tipoUnidadeOrganizacionalUsuario = tipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: unidadeOrganizacionalProprietario.
	 *
	 * @return unidadeOrganizacionalProprietario
	 */
	public int getUnidadeOrganizacionalProprietario() {
		return unidadeOrganizacionalProprietario;
	}
	
	/**
	 * Set: unidadeOrganizacionalProprietario.
	 *
	 * @param unidadeOrganizacionalProprietario the unidade organizacional proprietario
	 */
	public void setUnidadeOrganizacionalProprietario(
			int unidadeOrganizacionalProprietario) {
		this.unidadeOrganizacionalProprietario = unidadeOrganizacionalProprietario;
	}
	
	/**
	 * Get: unidadeOrganizacionalUsuario.
	 *
	 * @return unidadeOrganizacionalUsuario
	 */
	public int getUnidadeOrganizacionalUsuario() {
		return unidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: unidadeOrganizacionalUsuario.
	 *
	 * @param unidadeOrganizacionalUsuario the unidade organizacional usuario
	 */
	public void setUnidadeOrganizacionalUsuario(int unidadeOrganizacionalUsuario) {
		this.unidadeOrganizacionalUsuario = unidadeOrganizacionalUsuario;
	}
	

	

}
