/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantes.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;

/**
 * Nome: ListarParticipantesSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarParticipantesSaidaDTO {
	
    /** Atributo cdParticipante. */
    private Long cdParticipante;
    
    /** Atributo dsParticipante. */
    private String dsParticipante;
    
    /**
     * Listar participantes saida dto.
     *
     * @param ocorrencia the ocorrencia
     */
    public ListarParticipantesSaidaDTO(Ocorrencias ocorrencia) {
		super();
		this.cdParticipante = ocorrencia.getCdPessoa();
		this.dsParticipante = CpfCnpjUtils.formatarCpfCnpj(ocorrencia.getCdCpfCnpj(), ocorrencia.getCdFilialCnpj(), ocorrencia.getCdControleCnpj())  + " - " + ocorrencia.getNmRazao();
	}
	
	/**
	 * Get: cdParticipante.
	 *
	 * @return cdParticipante
	 */
	public Long getCdParticipante() {
		return cdParticipante;
	}
	
	/**
	 * Set: cdParticipante.
	 *
	 * @param cdParticipante the cd participante
	 */
	public void setCdParticipante(Long cdParticipante) {
		this.cdParticipante = cdParticipante;
	}
	
	/**
	 * Get: dsParticipante.
	 *
	 * @return dsParticipante
	 */
	public String getDsParticipante() {
		return dsParticipante;
	}
	
	/**
	 * Set: dsParticipante.
	 *
	 * @param dsParticipante the ds participante
	 */
	public void setDsParticipante(String dsParticipante) {
		this.dsParticipante = dsParticipante;
	}


}
