/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.impl;

import static br.com.bradesco.web.pgit.utils.CpfCnpjUtils.formatCpfCnpjCompleto;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatAgencia;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatBancoAgenciaConta;
import static br.com.bradesco.web.pgit.utils.PgitUtil.formatConta;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaBigDecimalNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formatarData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualServiceConstants;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarHistConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarHistConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarImprimirPgtoIndividualSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarManutencaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarManutencaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarPagamentosIndividuaisEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarPagamentosIndividuaisSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarParticipanteContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarParticipanteContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharHistConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharHistConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoIndCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoIndCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.request.ConsultarHistConsultaSaldoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarhistconsultasaldo.response.ConsultarHistConsultaSaldoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarimprimirpgtoindividual.request.ConsultarImprimirPgtoIndividualRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarimprimirpgtoindividual.response.ConsultarImprimirPgtoIndividualResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaopagamento.request.ConsultarManutencaoPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaopagamento.response.ConsultarManutencaoPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentosindividuais.request.ConsultarPagamentosIndividuaisRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentosindividuais.response.ConsultarPagamentosIndividuaisResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantecontrato.request.ConsultarParticipanteContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarparticipantecontrato.response.ConsultarParticipanteContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistconsultasaldo.request.DetalharHistConsultaSaldoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharhistconsultasaldo.response.DetalharHistConsultaSaldoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocodigobarras.request.DetalharManPagtoCodigoBarrasRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocodigobarras.response.DetalharManPagtoCodigoBarrasResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocreditoconta.request.DetalharManPagtoCreditoContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtocreditoconta.response.DetalharManPagtoCreditoContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodarf.request.DetalharManPagtoDARFRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodarf.response.DetalharManPagtoDARFResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodebitoconta.request.DetalharManPagtoDebitoContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodebitoconta.response.DetalharManPagtoDebitoContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodebitoveiculos.request.DetalharManPagtoDebitoVeiculosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodebitoveiculos.response.DetalharManPagtoDebitoVeiculosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodepidentificado.request.DetalharManPagtoDepIdentificadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodepidentificado.response.DetalharManPagtoDepIdentificadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodoc.request.DetalharManPagtoDOCRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtodoc.response.DetalharManPagtoDOCResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogare.request.DetalharManPagtoGARERequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogare.response.DetalharManPagtoGAREResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogps.request.DetalharManPagtoGPSRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtogps.response.DetalharManPagtoGPSResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtoordemcredito.request.DetalharManPagtoOrdemCreditoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtoordemcredito.response.DetalharManPagtoOrdemCreditoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtoordempagto.request.DetalharManPagtoOrdemPagtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtoordempagto.response.DetalharManPagtoOrdemPagtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtoted.request.DetalharManPagtoTEDRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtoted.response.DetalharManPagtoTEDResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtotitulobradesco.request.DetalharManPagtoTituloBradescoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtotitulobradesco.response.DetalharManPagtoTituloBradescoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtotitulooutrosbancos.request.DetalharManPagtoTituloOutrosBancosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmanpagtotitulooutrosbancos.response.DetalharManPagtoTituloOutrosBancosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtocodigobarras.request.DetalharPagtoCodigoBarrasRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtocodigobarras.response.DetalharPagtoCodigoBarrasResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtocreditoconta.request.DetalharPagtoCreditoContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtocreditoconta.response.DetalharPagtoCreditoContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodarf.request.DetalharPagtoDARFRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodarf.response.DetalharPagtoDARFResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodebitoconta.request.DetalharPagtoDebitoContaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodebitoconta.response.DetalharPagtoDebitoContaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodebitoveiculos.request.DetalharPagtoDebitoVeiculosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodebitoveiculos.response.DetalharPagtoDebitoVeiculosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodepidentificado.request.DetalharPagtoDepIdentificadoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodepidentificado.response.DetalharPagtoDepIdentificadoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodoc.request.DetalharPagtoDOCRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtodoc.response.DetalharPagtoDOCResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtogare.request.DetalharPagtoGARERequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtogare.response.DetalharPagtoGAREResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtogps.request.DetalharPagtoGPSRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtogps.response.DetalharPagtoGPSResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtoordemcredito.request.DetalharPagtoOrdemCreditoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtoordemcredito.response.DetalharPagtoOrdemCreditoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtoordempagto.request.DetalharPagtoOrdemPagtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtoordempagto.response.DetalharPagtoOrdemPagtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtoted.request.DetalharPagtoTEDRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtoted.response.DetalharPagtoTEDResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtotitulobradesco.request.DetalharPagtoTituloBradescoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtotitulobradesco.response.DetalharPagtoTituloBradescoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtotitulooutrosbancos.request.DetalharPagtoTituloOutrosBancosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagtotitulooutrosbancos.response.DetalharPagtoTituloOutrosBancosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.imprimirpagamentosindividuais.request.ImprimirPagamentosIndividuaisRequest;
import br.com.bradesco.web.pgit.service.data.pdc.imprimirpagamentosindividuais.response.ImprimirPagamentosIndividuaisResponse;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ConsultarPagamentosIndividual
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConsultarPagamentosIndividualServiceImpl implements
IConsultarPagamentosIndividualService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 * 
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 * 
	 * @param factoryAdapter
	 *            the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * Construtor.
	 */
	public ConsultarPagamentosIndividualServiceImpl() {

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#consultarHistConsultaSaldo(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarHistConsultaSaldoEntradaDTO)
	 */
	public List<ConsultarHistConsultaSaldoSaidaDTO> consultarHistConsultaSaldo(
			ConsultarHistConsultaSaldoEntradaDTO entradaDTO) {

		ConsultarHistConsultaSaldoRequest request = new ConsultarHistConsultaSaldoRequest();
		ConsultarHistConsultaSaldoResponse response = new ConsultarHistConsultaSaldoResponse();
		List<ConsultarHistConsultaSaldoSaidaDTO> listaSaida = new ArrayList<ConsultarHistConsultaSaldoSaidaDTO>();

		String retiraChaves = PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamento());
		retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
		request.setCdControlePagamento(retiraChaves);
		
		request.setCdModalidade(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdModalidade()));
		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoCanal(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdTipoCanal()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setNrOcorrencias(30);
		request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil.verificaLongNulo(entradaDTO
				.getCdContaDebito()));
		request.setCdDigitoAgencia(PgitUtil.DIGITO_CONTA);
		request.setDtPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtPagamento().replaceAll("/", ".")));
		

		ConsultarHistConsultaSaldoSaidaDTO saida;
		response = getFactoryAdapter()
		.getConsultarHistConsultaSaldoPDCAdapter().invokeProcess(
				request);

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ConsultarHistConsultaSaldoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setDsBancoDebito(response.getDsBancoDebito());
			saida.setDsAgenciaDebito(response.getDsAgenciaDebito());
			saida.setDsTipoConta(response.getDsTipoConta());
			saida.setCdSituacaoContrato(response.getCdSituacaoContrato());
			saida.setDsSituacaoContrato(response.getDsSituacaoContrato());
			saida.setDsContrato(response.getDsContrato());
			saida.setDtVencimento(response.getDtVencimento());
			saida.setNumeroConsultas(response.getNumeroConsultas());
			saida.setHrConsultasSaldoPagamento(response.getOcorrencias(i)
					.getHrConsultasSaldoPagamento());
			saida.setCdSituacao(response.getOcorrencias(i).getCdSituacao());
			saida.setCdValor(response.getOcorrencias(i).getCdValor());
			saida.setCdSituacaoPagamentoCliente(response
					.getCdSituacaoPagamentoCliente());
			saida.setDsSituacaoPagamentoCliente(response
					.getDsSituacaoPagamentoCliente());
			saida.setCdMotivoPagamentoCliente(response
					.getCdMotivoPagamentoCliente());
			saida.setDsMotivoPagamentoCliente(response
					.getDsMotivoPagamentoCliente());
			saida.setCdBancoCredito(response.getCdBancoCredito());
			saida.setDsBancoCredito(response.getDsBancoCredito());
			saida.setCdAgenciaBancariaCredito(response
					.getCdAgenciaBancariaCredito());
			saida.setCdDigitoAgenciaCredito(response
					.getCdDigitoAgenciaCredito());
			saida.setDsAgenciaCredito(response.getDsAgenciaCredito());
			saida.setCdContaBancariaCredito(response
					.getCdContaBancariaCredito());
			saida.setCdDigitoContaCredito(response.getCdDigitoContaCredito());
			saida.setCdTipoContaCredito(response.getCdTipoContaCredito());
			saida.setCdTipoInscricaoFavorecido(response
					.getCdTipoInscricaoFavorecido());
			saida.setInscricaoFavorecido(response.getInscricaoFavorecido());
			saida.setDsSituacaoPagamentoCliente(response
					.getDsSituacaoPagamentoCliente());
			saida.setDsMotivoPagamentoCliente(response
					.getDsMotivoPagamentoCliente());
			saida.setCdBancoCreditoFavorecido(response.getCdBancoCredito());
			saida.setCdAgenciaBancariaCreditoFavorecido(response
					.getCdAgenciaBancariaCredito());
			saida.setCdDigitoAgenciaCreditoFavorecido(response
					.getCdDigitoAgenciaCredito());
			saida.setCdContaBancariaCreditoFavorecido(response
					.getCdContaBancariaCredito());
			saida.setCdDigitoContaCreditoFavorecido(response
					.getCdDigitoContaCredito());
			saida.setCdTipoContaCreditoFavorecido(response
					.getCdTipoContaCredito());
			saida.setSinal(response.getOcorrencias(i).getDsSinal());

			saida.setBancoFavorecidoFormatado(PgitUtil.formatBanco(saida
					.getCdBancoCreditoFavorecido(), saida.getDsBancoCredito(),
					false));
			saida.setAgenciaFavorecidoFormatada(PgitUtil.formatAgencia(saida
					.getCdAgenciaBancariaCreditoFavorecido(), saida
					.getCdDigitoAgenciaCreditoFavorecido(), saida
					.getDsAgenciaCredito(), false));
			saida.setContaFavorecidoFormatada(PgitUtil.formatConta(saida
					.getCdContaBancariaCreditoFavorecido(), saida
					.getCdDigitoContaCreditoFavorecido(), false));

			SimpleDateFormat formato1 = new SimpleDateFormat(
			"yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");
			try {
				saida.setDataHoraFormatada(formato2.format(formato1
						.parse(response.getOcorrencias(i)
								.getHrConsultasSaldoPagamento())));
			} catch (IndexOutOfBoundsException e) {

			} catch (java.text.ParseException e) {
				saida.setDataHoraFormatada("");
			}
			listaSaida.add(saida);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#consultarManutencaoPagamento(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarManutencaoPagamentoEntradaDTO)
	 */
	public List<ConsultarManutencaoPagamentoSaidaDTO> consultarManutencaoPagamento(
			ConsultarManutencaoPagamentoEntradaDTO entradaDTO) {
		List<ConsultarManutencaoPagamentoSaidaDTO> listaSaida = new ArrayList<ConsultarManutencaoPagamentoSaidaDTO>();
		ConsultarManutencaoPagamentoRequest request = new ConsultarManutencaoPagamentoRequest();
		ConsultarManutencaoPagamentoResponse response = new ConsultarManutencaoPagamentoResponse();

		String retiraChaves = PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamento());
		retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
		request.setCdControlePagamento(retiraChaves);
		
		request.setCdModalidade(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdModalidade()));
		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoCanal(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdTipoCanal()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request
		.setNrOcorrencias(IConsultarPagamentosIndividualServiceConstants.NUMERO_OCORRENCIAS_CONSULTA_MANUTENCAO_PAGAMENTO);
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdAgendadosPagoNaoPago()));
		request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil.verificaLongNulo(entradaDTO
				.getCdContaDebito()));
		request.setCdDigitoAgenciaDebito(PgitUtil
				.complementaDigitoConta(entradaDTO.getCdDigitoAgenciaDebito()));
		request.setDtPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtPagamento()));
		
		response = getFactoryAdapter()
		.getConsultarManutencaoPagamentoPDCAdapter().invokeProcess(
				request);

		ConsultarManutencaoPagamentoSaidaDTO saida;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ConsultarManutencaoPagamentoSaidaDTO();
			saida.setCodMensagem(response.getCodMensagem());
			saida.setMensagem(response.getMensagem());
			saida.setCdListaDebitoPagamento(response
					.getCdListaDebitoPagamento() == 0L ? "" : String
							.valueOf(response.getCdListaDebitoPagamento()));
			saida.setCdLote(response.getCdLote() == 0L ? "" : String
					.valueOf(response.getCdLote()));
			saida.setCdModalidade(response.getCdModalidade());
			saida.setCdTipoCanal(response.getCdTipoCanal());
			saida.setDsTipoCanal(response.getDsTipoCanal());
			saida.setNrArquivoRemessaPagamento(response
					.getNrArquivoRemessaPagamento() == 0L ? "" : String
							.valueOf(response.getNrArquivoRemessaPagamento()));
			saida.setVlPagamento(response.getVlPagamento());
			saida.setDsTipoCanalLista(response.getOcorrencias(i)
					.getDsTipoCanalLista());
			saida.setDsTipoManutencao(response.getOcorrencias(i)
					.getDsTipoManutencao());
			saida.setDsUsuarioManutencao(response.getOcorrencias(i)
					.getDsUsuarioManutencao());
			saida.setDtHoraManutencao(response.getOcorrencias(i)
					.getDtHoraManutencao());
			saida.setCdTipoCanalLista(response.getOcorrencias(i)
					.getCdTipoCanalLista());
			saida.setCdTipoManutencao(response.getOcorrencias(i)
					.getCdTipoManutencao());
			saida.setCdUsuarioManutencao(response.getOcorrencias(i)
					.getCdUsuarioManutencao());
			saida.setDsBancoDebito(response.getDsBancoDebito());
			saida.setDsAgenciaDebito(response.getDsAgenciaDebito());
			saida.setDsTipoContaDebito(response.getDsTipoContaDebito());
			saida.setNumeroConsultas(response.getNumeroConsultas());
			saida.setCdSituacaoContrato(response.getCdSituacaoContrato());
			saida.setDsSituacaoContrato(response.getDsSituacaoContrato());
			saida.setDsContrato(response.getDsContrato());
			saida.setDtVencimento(response.getDtVencimento());
			saida.setCdTipoInscricaoFavorecido(response
					.getCdTipoInscricaoFavorecido());
			saida.setInscricaoFavorecido(response.getInscricaoFavorecido());
			saida.setDsSituacaoPagamentoCliente(response
					.getDsSituacaoPagamentoCliente());
			saida.setDsMotivoPagamentoCliente(response
					.getDsMotivoPagamentoCliente());
			saida.setCdBancoCreditoFavorecido(response.getCdBancoCredito());
			saida.setCdAgenciaBancariaCreditoFavorecido(response
					.getCdAgenciaBancariaCredito());
			saida.setCdDigitoAgenciaCreditoFavorecido(response
					.getCdDigitoAgenciaCredito());
			saida.setCdContaBancariaCreditoFavorecido(response
					.getCdContaBancariaCredito());
			saida.setCdDigitoContaCreditoFavorecido(response
					.getCdDigitoContaCredito());
			saida.setCdTipoContaCreditoFavorecido(response
					.getCdTipoContaCredito());
			saida.setDsBancoCreditoFavorecido(response.getDsBancoCredito());
			saida.setDsAgenciaCreditoFavorecido(response.getDsAgenciaCredito());

			saida.setBancoFavorecidoFormatado(PgitUtil.formatBanco(saida
					.getCdBancoCreditoFavorecido(), saida
					.getDsBancoCreditoFavorecido(), false));
			saida.setAgenciaFavorecidoFormatada(PgitUtil.formatAgencia(saida
					.getCdAgenciaBancariaCreditoFavorecido(), saida
					.getCdDigitoAgenciaCreditoFavorecido(), saida
					.getDsAgenciaCreditoFavorecido(), false));
			saida.setContaFavorecidoFormatada(PgitUtil.formatConta(saida
					.getCdContaBancariaCreditoFavorecido(), saida
					.getCdDigitoContaCreditoFavorecido(), false));

			saida.setDtVencimentoFormatada(DateUtils
					.formartarDataPDCparaPadraPtBr(response.getDtVencimento(),
							"dd.MM.yyyy", "dd/MM/yyyy"));
			saida.setDtPagamento(FormatarData.formataDiaMesAnoFromPdc(response
					.getDtPagamento()));

			saida.setDataHoraFormatada(FormatarData.formatarDataTrilha(response
					.getOcorrencias(i).getDtHoraManutencao()));

			saida.setVlPagamentoFormatado(NumberUtils.format(response
					.getVlPagamento(), "#,##0.00"));
			saida.setUsuarioManutencaoFormatado(PgitUtil.concatenarCampos(saida
					.getCdUsuarioManutencao(), saida.getDsUsuarioManutencao()));
			listaSaida.add(saida);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManCreditoConta(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoIndCreditoContaEntradaDTO)
	 */
	public DetalharManPagtoIndCreditoContaSaidaDTO detalharManCreditoConta(
			DetalharManPagtoIndCreditoContaEntradaDTO detalharManPagtoIndCreditoContaEntradaDTO) {

		DetalharManPagtoCreditoContaRequest request = new DetalharManPagtoCreditoContaRequest();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoIndCreditoContaEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoIndCreditoContaEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoIndCreditoContaEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoIndCreditoContaEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoIndCreditoContaEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoIndCreditoContaEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoIndCreditoContaEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoIndCreditoContaEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoIndCreditoContaEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoIndCreditoContaEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoIndCreditoContaEntradaDTO
						.getCdContaCredito()));
		request.setCdAgenPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoIndCreditoContaEntradaDTO
						.getCdAgenPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoIndCreditoContaEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoIndCreditoContaEntradaDTO
						.getDtHoraManutencao()));

		DetalharManPagtoCreditoContaResponse response = getFactoryAdapter()
		.getDetalharManPagtoCreditoContaPDCAdapter().invokeProcess(
				request);

		DetalharManPagtoIndCreditoContaSaidaDTO saidaDTO = new DetalharManPagtoIndCreditoContaSaidaDTO();

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setDtNascimentoBeneficiario(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtNascimentoBeneficiario(), "dd.MM.yyyy",
				"dd/MM/yyyy"));
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlDeducao(response.getVlDeducao());
		saidaDTO.setVlAcrescimo(response.getVlAcrescimo());
		saidaDTO.setVlImpostoRenda(response.getVlImpostoRenda());
		saidaDTO.setVlIss(response.getVlIss());
		saidaDTO.setVlIof(response.getVlIof());
		saidaDTO.setVlInss(response.getVlInss());
		saidaDTO.setCdBancoDestino(response.getCdBancoDestino());
		saidaDTO.setCdAgenciaDestino(response.getCdAgenciaDestino());
		saidaDTO
		.setCdDigitoAgenciaDestino(response.getCdDigitoAgenciaDestino());
		saidaDTO.setCdContaDestino(response.getCdContaDestino());
		saidaDTO.setCdDigitoContaDestino(response.getCdDigitoContaDestino());
		saidaDTO.setDsBancoDestino(response.getDsBancoDestino());
		saidaDTO.setDsAgenciaDestino(response.getDsAgenciaDestino());
		saidaDTO.setDsTipoContaDestino(response.getDsTipoContaDestino());
		saidaDTO.setCdIndicadorModalidade(response.getCdIndicadorModalidade());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlrAgendamento(response.getVlrAgendamento());
		saidaDTO.setVlrEfetivacao(response.getVlrEfetivacao());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO
		.setCdTipoInscricaoPagador(response.getCdTipoInscricaoPagador());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setCdStatusTransmissao(response.getCdStatusTransmissao());
		saidaDTO.setDsStatusTransmissao(response.getDsStatusTransmissao());
		saidaDTO.setCdBancoFinal(response.getCdBancoFinal());
		saidaDTO.setCdAgenciaFinal(response.getCdAgenciaFinal());
		saidaDTO.setCdDigitoAgenciaFinal(response.getCdDigitoAgenciaFinal());
		saidaDTO.setCdContaFinal(response.getCdContaFinal());
		saidaDTO.setCdDigitoContaFinal(response.getCdDigitoContaFinal());
		saidaDTO.setDtHoraTransmissao(response.getDtHoraTransmissao());
		saidaDTO.setDtHoraDevolucao(response.getDtHoraDevolucao());
		saidaDTO.setVlEmpresa(response.getVlEmpresa());
		saidaDTO.setVlLeasing(response.getVlLeasing());
		saidaDTO.setVlDebitoAut(response.getVlDebitoAut());
		saidaDTO.setDsMoeda(response.getDsMoeda());
		saidaDTO.setCdSituacaoTranferenciaAutomatica(response
				.getCdSituacaoTranferenciaAutomatica());

		saidaDTO.setBancoDestinoFormatado(PgitUtil
				.concatenarCampos(
						SiteUtil.formatNumber(String.valueOf(PgitUtil
								.verificaIntegerNulo(saidaDTO
										.getCdBancoDestino())), 3), PgitUtil
										.verificaStringNula(saidaDTO
												.getDsBancoDestino())));
		saidaDTO.setAgenciaDestinoFormatada(PgitUtil
				.concatenarCampos(SiteUtil.formatNumber(String.valueOf(PgitUtil
						.verificaIntegerNulo(saidaDTO.getCdAgenciaDestino())),
						5), SiteUtil.formatNumber(String.valueOf(PgitUtil
								.verificaIntegerNulo(saidaDTO
										.getCdDigitoAgenciaDestino())), 5)));
		saidaDTO.setAgenciaDestinoFormatada(PgitUtil.concatenarCampos(saidaDTO
				.getAgenciaDestinoFormatada(), PgitUtil
				.verificaStringNula(saidaDTO.getDsAgenciaDestino())));
		saidaDTO.setContaDestinoFormatada(PgitUtil.concatenarCampos(SiteUtil
				.formatNumber(String.valueOf(PgitUtil.verificaLongNulo(saidaDTO
						.getCdContaDestino())), 13), SiteUtil
						.formatNumber(PgitUtil.verificaStringNula(saidaDTO
								.getCdDigitoContaDestino()), 2)));
		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoCreditoConta(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO)
	 */
	public DetalharPagtoCreditoContaSaidaDTO detalharPagtoCreditoConta(
			DetalharPagtoCreditoContaEntradaDTO entradaDTO) {

		DetalharPagtoCreditoContaSaidaDTO saidaDTO = new DetalharPagtoCreditoContaSaidaDTO();
		DetalharPagtoCreditoContaRequest request = new DetalharPagtoCreditoContaRequest();
		DetalharPagtoCreditoContaResponse response = new DetalharPagtoCreditoContaResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil.verificaStringNula(entradaDTO
				.getDsEfetivacaoPagamento()));
		
		String retiraChaves = PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamento());
		retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
		request.setCdControlePagamento(retiraChaves);
		
		request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil.verificaLongNulo(entradaDTO
				.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil.verificaLongNulo(entradaDTO
				.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil.verificaStringNula(entradaDTO
				.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharPagtoCreditoContaPDCAdapter()
		.invokeProcess(request);
		
		saidaDTO.setDsIdentificadorTransferenciaPagto(response.getDsIdentificadorTransferenciaPagto());
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlDeducao(response.getVlDeducao());
		saidaDTO.setVlAcrescimo(response.getVlAcrescimo());
		saidaDTO.setVlImpostoRenda(response.getVlImpostoRenda());
		saidaDTO.setVlIss(response.getVlIss());
		saidaDTO.setVlIof(response.getVlIof());
		saidaDTO.setVlInss(response.getVlInss());
		saidaDTO.setCdBancoDestino(response.getCdBancoDestino());
		saidaDTO.setCdAgenciaDestino(response.getCdAgenciaDestino());
		saidaDTO
		.setCdDigitoAgenciaDestino(response.getCdDigitoAgenciaDestino());
		saidaDTO.setCdContaDestino(response.getCdContaDestino());
		saidaDTO.setCdDigitoContaDestino(response.getCdDigitoContaDestino());
		saidaDTO.setDsBancoDestino(response.getDsBancoDestino());
		saidaDTO.setDsAgenciaDestino(response.getDsAgenciaDestino());
		saidaDTO.setDsTipoContaDestino(response.getDsTipoContaDestino());
		saidaDTO.setCdIndicadorModalidade(response.getCdIndicadorModalidade());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlrAgendamento(response.getVlrAgendamento());
		saidaDTO.setVlrEfetivacao(response.getVlrEfetivacao());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());
		saidaDTO.setCdSituacaoTranferenciaAutomatica(response
				.getCdSituacaoTranferenciaAutomatica());
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO
		.setNmInscriBeneficio(response.getNmInscriBeneficio() == 0L ? ""
				: String.valueOf(response.getNmInscriBeneficio()));
		saidaDTO.setCdTipoInscriBeneficio(response.getCdTipoInscriBeneficio());
		saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(String.valueOf(response
				.getNrSequenciaContratoNegocio()));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());

		saidaDTO.setBancoDestinoFormatado(PgitUtil.formatBanco(saidaDTO
				.getCdBancoDestino(), saidaDTO.getDsBancoDestino(), false));
		saidaDTO.setAgenciaDestinoFormatada(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaDestino(), saidaDTO.getCdDigitoAgenciaDestino(),
				saidaDTO.getDsAgenciaDestino(), false));
		saidaDTO.setContaDestinoFormatada(PgitUtil
				.formatConta(saidaDTO.getCdContaDestino(), saidaDTO
						.getCdDigitoContaDestino(), false));
		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));

		saidaDTO.setCdBancoOriginal(response.getCdBancoOriginal());
		saidaDTO.setDsBancoOriginal(response.getDsBancoOriginal());
		saidaDTO.setCdAgenciaBancariaOriginal(response
				.getCdAgenciaBancariaOriginal());
		saidaDTO.setCdDigitoAgenciaOriginal(response
				.getCdDigitoAgenciaOriginal());
		saidaDTO.setDsAgenciaOriginal(response.getDsAgenciaOriginal());
		saidaDTO.setCdContaBancariaOriginal(response
				.getCdContaBancariaOriginal());
		saidaDTO.setCdDigitoContaOriginal(response.getCdDigitoContaOriginal());
		saidaDTO.setDsTipoContaOriginal(response.getDsTipoContaOriginal());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());
		saidaDTO.setNumControleInternoLote(response.getNumControleInternoLote());
		saidaDTO.setDsEstornoPagamento(response.getDsEstornoPagamento().replaceAll("\\.", "/"));

		saidaDTO.setCdLoteInterno(response.getCdLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());
		saidaDTO.setVlEfetivacaoDebitoPagamento(response.getVlrEfetivacao());
		saidaDTO.setVlEfetivacaoCreditoPagamento(response.getVlEfetivacaoCreditoPagamento());
		saidaDTO.setVlDescontoPagamento(response.getVlDescontoPagamento());
		saidaDTO.setHrEfetivacaoCreditoPagamento(response.getHrEfetivacaoCreditoPagamento());
		saidaDTO.setHrEnvioCreditoPagamento(response.getHrEnvioCreditoPagamento());	
		saidaDTO.setCdIdentificadorTransferenciaPagto(response.getCdIdentificadorTransferenciaPagto());
		saidaDTO.setCdMensagemLinExtrato(response.getCdMensagemLinExtrato());				
		saidaDTO.setCdConveCtaSalarial(response.getCdConveCtaSalarial());			
		saidaDTO.setCdIspbPagtoDestino(response.getCdIspbPagtoDestino());
		saidaDTO.setContaPagtoDestino(response.
				getContaPagtoDestino());
			
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoDebitoConta(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO)
	 */
	public DetalharPagtoDebitoContaSaidaDTO detalharPagtoDebitoConta(
			DetalharPagtoDebitoContaEntradaDTO detalharPagtoDebitoContaEntradaDTO) {

		DetalharPagtoDebitoContaSaidaDTO saidaDTO = new DetalharPagtoDebitoContaSaidaDTO();
		DetalharPagtoDebitoContaRequest request = new DetalharPagtoDebitoContaRequest();
		DetalharPagtoDebitoContaResponse response = new DetalharPagtoDebitoContaResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoDebitoContaEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoContaEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoDebitoContaEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoDebitoContaEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoDebitoContaEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoContaEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoContaEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoDebitoContaEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoContaEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoContaEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoDebitoContaEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoContaEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoContaEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoDebitoContaEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharPagtoDebitoContaPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());

		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlDeducao(response.getVlDeducao());
		saidaDTO.setVlAcrescimo(response.getVlAcrescimo());
		saidaDTO.setVlImpostoRenda(response.getVlImpostoRenda());
		saidaDTO.setVlIss(response.getVlIss());
		saidaDTO.setVlIof(response.getVlIof());
		saidaDTO.setVlInss(response.getVlInss());
		saidaDTO.setCdBancoDestino(response.getCdBancoDestino());
		saidaDTO.setCdAgenciaDestino(response.getCdAgenciaDestino());

		saidaDTO
		.setCdDigitoAgenciaDestino(response.getCdDigitoAgenciaDestino());

		saidaDTO.setCdContaDestino(response.getCdContaDestino());
		saidaDTO.setCdDigitoContaDestino(response.getCdDigitoContaDestino());
		saidaDTO.setDsBancoDestino(response.getDsBancoDestino());
		saidaDTO.setDsAgenciaDestino(response.getDsAgenciaDestino());
		saidaDTO.setDsTipoContaDestino(response.getDsTipoContaDestino());
		saidaDTO.setCdIndicadorModalidade(response.getCdIndicadorModalidade());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlrAgendamento(response.getVlrAgendamento());
		saidaDTO.setVlrEfetivacao(response.getVlrEfetivacao());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));

		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());

		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());
		saidaDTO.setCdSituacaoTranferenciaAutomatica(response
				.getCdSituacaoTranferenciaAutomatica());
		saidaDTO.setBancoDestinoFormatado(PgitUtil.formatBanco(saidaDTO
				.getCdBancoDestino(), saidaDTO.getDsBancoDestino(), false));
		saidaDTO.setAgenciaDestinoFormatada(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaDestino(), saidaDTO.getCdDigitoAgenciaDestino(),
				saidaDTO.getDsAgenciaDestino(), false));
		saidaDTO.setContaDestinoFormatada(PgitUtil
				.formatConta(saidaDTO.getCdContaDestino(), saidaDTO
						.getCdDigitoContaDestino(), false));
		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// Cliente
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());

		// Conta de D�bito
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		// Favorecido
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());

		// Benefeciario
		saidaDTO.setNmInscriBeneficio(response.getNmInscriBeneficio());
		saidaDTO.setCdTipoInscriBeneficio(response.getCdTipoInscriBeneficio());
		saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());

		// Pagamento
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());

		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoDebitoConta(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoContaEntradaDTO)
	 */
	public DetalharManPagtoDebitoContaSaidaDTO detalharManPagtoDebitoConta(
			DetalharManPagtoDebitoContaEntradaDTO detalharManPagtoDebitoContaEntradaDTO) {

		DetalharManPagtoDebitoContaRequest request = new DetalharManPagtoDebitoContaRequest();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoDebitoContaEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoContaEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoDebitoContaEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoDebitoContaEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoDebitoContaEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoContaEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoContaEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoDebitoContaEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoContaEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoContaEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoDebitoContaEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoContaEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoContaEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoDebitoContaEntradaDTO
						.getDtHoraManutencao()));

		DetalharManPagtoDebitoContaResponse response = getFactoryAdapter()
		.getDetalharManPagtoDebitoContaPDCAdapter().invokeProcess(
				request);

		DetalharManPagtoDebitoContaSaidaDTO saidaDTO = new DetalharManPagtoDebitoContaSaidaDTO();

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlDeducao(response.getVlDeducao());
		saidaDTO.setVlAcrescimo(response.getVlAcrescimo());
		saidaDTO.setVlImpostoRenda(response.getVlImpostoRenda());
		saidaDTO.setVlIss(response.getVlIss());
		saidaDTO.setVlIof(response.getVlIof());
		saidaDTO.setVlInss(response.getVlInss());
		saidaDTO.setCdBancoDestino(response.getCdBancoDestino());
		saidaDTO.setCdAgenciaDestino(response.getCdAgenciaDestino());
		saidaDTO
		.setCdDigitoAgenciaDestino(response.getCdDigitoAgenciaDestino());
		saidaDTO.setCdContaDestino(response.getCdContaDestino());
		saidaDTO.setCdDigitoContaDestino(response.getCdDigitoContaDestino());
		saidaDTO.setDsBancoDestino(response.getDsBancoDestino());
		saidaDTO.setDsAgenciaDestino(response.getDsAgenciaDestino());
		saidaDTO.setDsTipoContaDestino(response.getDsTipoContaDestino());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidade());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlrAgendamento());
		saidaDTO.setVlEfetivo(response.getVlrEfetivacao());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());

		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setCdSituacaoTranferenciaAutomatica(response
				.getCdSituacaoTranferenciaAutomatica());
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());

		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());

		saidaDTO.setNmInscriBeneficio(response.getNmInscriBeneficio());
		saidaDTO.setCdTipoInscriBeneficio(response.getCdTipoInscriBeneficio());
		saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#consultarPagamentosIndividuais(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarPagamentosIndividuaisEntradaDTO)
	 */
	public List<ConsultarPagamentosIndividuaisSaidaDTO> consultarPagamentosIndividuais(
			ConsultarPagamentosIndividuaisEntradaDTO entradaDTO) {

		ConsultarPagamentosIndividuaisRequest request = new ConsultarPagamentosIndividuaisRequest();
		ConsultarPagamentosIndividuaisResponse response = new ConsultarPagamentosIndividuaisResponse();
		List<ConsultarPagamentosIndividuaisSaidaDTO> listaSaida = new ArrayList<ConsultarPagamentosIndividuaisSaidaDTO>();

		request.setCdAgencia(verificaIntegerNulo(entradaDTO.getCdAgencia()));
		request.setCdAgenciaBeneficiario(verificaIntegerNulo(entradaDTO.getCdAgenciaBeneficiario()));
		request.setCdAgendadosPagosNaoPagos(verificaIntegerNulo(entradaDTO.getCdAgendadosPagosNaoPagos()));
		request.setCdBanco(verificaIntegerNulo(entradaDTO.getCdBanco()));
		request.setCdBancoBeneficiario(verificaIntegerNulo(entradaDTO.getCdBancoBeneficiario()));
		request.setCdBarraDocumento(verificaStringNula(entradaDTO.getCdBarraDocumento()));
		request.setCdBeneficio(verificaLongNulo(entradaDTO.getCdBeneficio()));
		request.setCdClassificacao(verificaIntegerNulo(entradaDTO.getCdClassificacao()));
		request.setCdConta(verificaLongNulo(entradaDTO.getCdConta()));
		request.setCdContaBeneficiario(verificaLongNulo(entradaDTO.getCdContaBeneficiario()));
		request.setCdControlePagamentoAte(verificaStringNula(entradaDTO.getCdControlePagamentoAte()));
		request.setCdControlePagamentoDe(verificaStringNula(entradaDTO.getCdControlePagamentoDe()));
		request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
		request.setCdDigitoContaBeneficiario(PgitUtil.DIGITO_CONTA);
		request.setCdEspecieBeneficioInss(verificaIntegerNulo(entradaDTO.getCdEspecieBeneficioInss()));
		request.setCdFavorecidoClientePagador(verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
		request.setCdIdentificacaoInscricaoFavorecido(verificaIntegerNulo(
				entradaDTO.getCdIdentificacaoInscricaoFavorecido()));
		request.setCdInscricaoFavorecido(verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));
		request.setCdMotivoSituacaoPagamento(verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
		request.setCdParticipante(verificaLongNulo(entradaDTO.getCdParticipante()));
		request.setCdPessoaJuridicaContrato(verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdProdutoServicoOperacao(verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdProdutoServicoRelacionado(verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
		request.setCdSituacaoOperacaoPagamento(verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setCdTipoPesquisa(verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
		request.setDtCreditoPagamentoFim(verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
		request.setDtCreditoPagamentoInicio(verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
		request.setNrArquivoRemssaPagamento(verificaLongNulo(entradaDTO.getNrArquivoRemssaPagamento()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setNrUnidadeOrganizacional(verificaIntegerNulo(entradaDTO.getNrUnidadeOrganizacional()));
		request.setVlPagamentoAte(verificaBigDecimalNulo(entradaDTO.getVlPagamentoAte()));
		request.setVlPagamentoDe(verificaBigDecimalNulo(entradaDTO.getVlPagamentoDe()));
		request.setCdEmpresaContrato(verificaLongNulo(entradaDTO.getCdEmpresaContrato()));
		request.setCdTipoConta(verificaIntegerNulo(entradaDTO.getCdTipoConta()));
		request.setNrContrato(verificaLongNulo(entradaDTO.getNrContrato()));
		request.setNrOcorrencias(IConsultarPagamentosIndividualServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
		request.setCdCpfCnpjParticipante(verificaLongNulo(entradaDTO.getCdCpfCnpjParticipante()));
		request.setCdFilialCnpjParticipante(verificaIntegerNulo(entradaDTO.getCdFilialCnpjParticipante()));
		request.setCdControleCnpjParticipante(verificaIntegerNulo(entradaDTO.getCdControleCnpjParticipante()));
		request.setCdPessoaContratoDebito(verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
		request.setCdTipoContratoDebito(verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
		request.setNrSequenciaContratoDebito(verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
		request.setCdPessoaContratoCredt(verificaLongNulo(entradaDTO.getCdPessoaContratoCredt()));
		request.setCdTipoContratoCredt(verificaIntegerNulo(entradaDTO.getCdTipoContratoCredt()));
		request.setNrSequenciaContratoCredt(verificaLongNulo(entradaDTO.getNrSequenciaContratoCredt()));
		request.setCdIndiceSimulaPagamento(verificaIntegerNulo(entradaDTO.getCdIndiceSimulaPagamento()));
		request.setCdOrigemRecebidoEfetivacao(verificaIntegerNulo(entradaDTO.getCdOrigemRecebidoEfetivacao()));
		request.setCdTituloPgtoRastreado(verificaIntegerNulo(entradaDTO.getCdTituloPgtoRastreado()));
		request.setCdBancoSalarial(verificaIntegerNulo(entradaDTO.getCdBancoSalarial()));
		request.setCdAgencaSalarial(verificaIntegerNulo(entradaDTO.getCdAgencaSalarial()));
		request.setCdContaSalarial(verificaLongNulo(entradaDTO.getCdContaSalarial()));
		if("".equals(PgitUtil.verificaStringNula(entradaDTO.getCdIspbPagtoDestino()))){
			request.setCdIspbPagtoDestino("");
		}else{
			request.setCdIspbPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getCdIspbPagtoDestino(), 8));
		}
		request.setContaPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getContaPagtoDestino(), 20));
				
		response = getFactoryAdapter()
		.getConsultarPagamentosIndividuaisPDCAdapter().invokeProcess(
				request);

		ConsultarPagamentosIndividuaisSaidaDTO saidaDTO;

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {

			saidaDTO = new ConsultarPagamentosIndividuaisSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
			saidaDTO.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
			saidaDTO.setNrDigitoCnpjCpf(response.getOcorrencias(i).getNrDigitoCnpjCpf());
			saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
			saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
			saidaDTO.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
			saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
			saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
			saidaDTO.setNrContrato(response.getOcorrencias(i).getNrContrato());
			saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
			saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
			saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
			saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());

			saidaDTO.setDtCreditoPagamento(response.getOcorrencias(i).getDtCreditoPagamento() != null
					&& !response.getOcorrencias(i).getDtCreditoPagamento().equals("") ? formatarData(response
							.getOcorrencias(i).getDtCreditoPagamento()) : "");
			saidaDTO.setVlEfetivoPagamento(response.getOcorrencias(i).getVlEfetivoPagamento());
			saidaDTO.setVlEfetivoPagamentoFormatado(NumberUtils.format(
					response.getOcorrencias(i).getVlEfetivoPagamento(), "#,##0.00"));
			saidaDTO.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido() == 0L ? "" : String
					.valueOf(response.getOcorrencias(i).getCdInscricaoFavorecido()));
			saidaDTO.setDsFavorecido(response.getOcorrencias(i).getDsFavorecido());
			saidaDTO.setFavorecidoBeneficiarioFormatado(PgitUtil.concatenarCampos(saidaDTO.getCdInscricaoFavorecido(),
					saidaDTO.getDsFavorecido(), "-"));
			saidaDTO.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
			saidaDTO.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
			saidaDTO.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
			saidaDTO.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
			saidaDTO.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
			saidaDTO.setCdBancoCredito(response.getOcorrencias(i).getCdBancoCredito());
			saidaDTO.setCdAgenciaCredito(response.getOcorrencias(i).getCdAgenciaCredito());
			saidaDTO.setCdDigitoAgenciaCredito(response.getOcorrencias(i).getCdDigitoAgenciaCredito());
			saidaDTO.setCdContaCredito(response.getOcorrencias(i).getCdContaCredito());
			saidaDTO.setCdDigitoContaCredito(response.getOcorrencias(i).getCdDigitoContaCredito());
			saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
			saidaDTO.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
			saidaDTO.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
			saidaDTO.setDsMotivoSituacaoPagamento(response.getOcorrencias(i).getDsMotivoSituacaoPagamento());
			saidaDTO.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
			saidaDTO.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
			saidaDTO.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
			saidaDTO.setDsIndicadorPagamento(response.getOcorrencias(i)
					.getDsIndicadorPagamento());
			saidaDTO.setAgenciaCreditoFormatada(formatAgencia(response.getOcorrencias(i).getCdAgenciaCredito(), 
					response.getOcorrencias(i).getCdDigitoAgenciaCredito(), false));
			saidaDTO.setContaCreditoFormatada(formatConta(response.getOcorrencias(i).getCdContaCredito(), 
					response.getOcorrencias(i).getCdDigitoContaCredito(), false));
			saidaDTO.setCdCpfCnpjFormatado(formatCpfCnpjCompleto(response.getOcorrencias(i).getNrCnpjCpf(), 
					response.getOcorrencias(i).getNrFilialCnpjCpf(), response.getOcorrencias(i).getNrDigitoCnpjCpf()));
			saidaDTO.setBancoAgenciaContaDebitoFormatado(formatBancoAgenciaConta(saidaDTO.getCdBancoDebito(),
					saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigitoAgenciaDebito(), saidaDTO
					.getCdContaDebito(), saidaDTO.getCdDigitoContaDebito(), true));
			saidaDTO.setAgenciaDebitoFormatada(formatAgencia(saidaDTO
					.getCdAgenciaDebito(), saidaDTO.getCdDigitoAgenciaDebito(),false));
			saidaDTO.setContaDebitoFormatada(formatConta(saidaDTO
					.getCdContaDebito(), saidaDTO.getCdDigitoContaDebito(),false));
			saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
			saidaDTO.setCdAgenciaSalario(response.getOcorrencias(i).getCdAgenciaSalario());
			saidaDTO.setCdDigitoAgenciaSalarial(response.getOcorrencias(i).getCdDigitoAgenciaSalarial());
			saidaDTO.setCdContaSalarial(response.getOcorrencias(i).getCdContaSalarial());
			saidaDTO.setCdDigitoContaSalarial(response.getOcorrencias(i).getCdDigitoContaSalarial());
			saidaDTO.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
			saidaDTO.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
			String cdContaPagamentoDestino = response.getOcorrencias(i).getContaPagtoDestino();
			if(cdContaPagamentoDestino != null && !"".equals(cdContaPagamentoDestino) && !"0".equals(cdContaPagamentoDestino)){
				saidaDTO.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoIspbConta(saidaDTO.getCdBancoCredito(), 
						saidaDTO.getCdIspbPagtoDestino(), cdContaPagamentoDestino, true));
			}else{
				saidaDTO.setBancoAgenciaContaCreditoFormatado(formatBancoAgenciaConta(saidaDTO.getCdBancoCredito(),
						saidaDTO.getCdAgenciaCredito(), saidaDTO.getCdDigitoAgenciaCredito(), saidaDTO
						.getCdContaCredito(), saidaDTO.getCdDigitoContaCredito(), true));			
			}
			

			String retiraChaves = Arrays.toString(response.getOcorrencias(i).getCdControlePagamento());
			retiraChaves = retiraChaves.replace("[", "").replace("]", "");
			saidaDTO.setCdControlePagamento(retiraChaves);
			
			listaSaida.add(saidaDTO);
		}
		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoTED(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO)
	 */
	public DetalharPagtoTEDSaidaDTO detalharPagtoTED(
			DetalharPagtoTEDEntradaDTO detalharPagtoTEDEntradaDTO) {

		DetalharPagtoTEDSaidaDTO saidaDTO = new DetalharPagtoTEDSaidaDTO();
		DetalharPagtoTEDResponse response = new DetalharPagtoTEDResponse();
		DetalharPagtoTEDRequest request = new DetalharPagtoTEDRequest();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoTEDEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoTEDEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoTEDEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoTEDEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoTEDEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTEDEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTEDEntradaDTO
						.getCdAgenciaDebito()));
		request
		.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoTEDEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTEDEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTEDEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoTEDEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoTEDEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoTEDEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoTEDEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharPagtoTEDPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdCamaraCentral(response.getCdCamaraCentral() == 0 ? ""
				: String.valueOf(response.getCdCamaraCentral()));
		saidaDTO.setCdFinalidadeDoc(response.getCdFinalidadeDoc());
		saidaDTO.setCdFinalidadeTed(response.getCdFinalidadeTed());
		saidaDTO.setCdIdentificacaoTransferencia(response
				.getCdIdentificacaoTransferencia() == 0 ? "" : String
						.valueOf(response.getCdIdentificacaoTransferencia()));
		saidaDTO
		.setCdIdentificacaoTitular(response.getCdIdentificacaoTitular() == 0 ? ""
				: String.valueOf(response.getCdIdentificacaoTitular()));
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatidoCredito(response.getVlAbatidoCredito());
		saidaDTO.setVlMultaCredito(response.getVlMultaCredito());
		saidaDTO.setVlMoraJuros(response.getVlMoraJuros());
		saidaDTO.setVlOutrasDeducoes(response.getVlOutrasDeducoes());
		saidaDTO.setVlOutroAcrescimo(response.getVlOutroAcrescimo());
		saidaDTO.setVlIrCredito(response.getVlIrCredito());
		saidaDTO.setVlIssCredito(response.getVlIssCredito());
		saidaDTO.setVlIofCredito(response.getVlIofCredito());
		saidaDTO.setVlInssCredito(response.getVlInssCredito());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDsIdentificacaoTransferenciaPagto(response
				.getDsIdentificacaoTransferenciaPagto());
		saidaDTO.setDsidentificacaoTitularPagto(response
				.getDsidentificacaoTitularPagto());
		saidaDTO.setDsFinalidadeDocTed(response.getDsFinalidadeDocTed());
		saidaDTO.setCdIndentificadorJudicial(response
				.getCdIndentificadorJudicial().equals("0") ? "" : response
						.getCdIndentificadorJudicial());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setDsBancoCredito(response.getDsBancoFavorecido());
		saidaDTO.setCdIspbPagamento(response.getCdIspbPagamento());
		saidaDTO.setDsNomeReclamante(response.getDsNomeReclamante());
		saidaDTO.setNrProcessoDepositoJudicial(response.getNrProcessoDepositoJudicial());
		saidaDTO.setNrPisPasep(response.getNrPisPasep());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// BENEFICI�RIO
		saidaDTO
		.setNmInscriBeneficio(response.getNmInscriBeneficio() == 0L ? ""
				: String.valueOf(response.getNmInscriBeneficio()));
		saidaDTO.setCdTipoInscriBeneficio(response.getCdTipoInscriBeneficio());
		saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		saidaDTO.setNumeroInscricaoBeneficiarioFormatado(saidaDTO
				.getNmInscriBeneficio());

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtFloatingPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivFloatPgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());
		saidaDTO.setCdLoteInterno(response.getCdLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoDOC(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO)
	 */
	public DetalharPagtoDOCSaidaDTO detalharPagtoDOC(
			DetalharPagtoDOCEntradaDTO detalharPagtoDOCEntradaDTO) {

		DetalharPagtoDOCRequest request = new DetalharPagtoDOCRequest();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoDOCEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoDOCEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoDOCEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoDOCEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoDOCEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDOCEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDOCEntradaDTO
						.getCdAgenciaDebito()));
		request
		.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoDOCEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDOCEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDOCEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoDOCEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoDOCEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoDOCEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoDOCEntradaDTO
						.getDtHoraManutencao()));

		DetalharPagtoDOCResponse response = getFactoryAdapter()
		.getDetalharPagtoDOCPDCAdapter().invokeProcess(request);

		DetalharPagtoDOCSaidaDTO saidaDTO = new DetalharPagtoDOCSaidaDTO();

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdCamaraCentral(response.getCdCamaraCentral() == 0 ? ""
				: String.valueOf(response.getCdCamaraCentral()));
		saidaDTO.setCdFinalidadeDoc(response.getCdFinalidadeDoc()); // novo
		// campo
		saidaDTO.setCdFinalidadeTed(response.getCdFinalidadeTed()); // novo
		// campo
		saidaDTO.setCdIdentificacaoTransferencia(response
				.getCdIdentificacaoTransferencia() == 0 ? "" : String
						.valueOf(response.getCdIdentificacaoTransferencia()));
		saidaDTO
		.setCdIdentificacaoTitular(response.getCdIdentificacaoTitular() == 0 ? ""
				: String.valueOf(response.getCdIdentificacaoTitular()));
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlDeducao(response.getVlDeducao());
		saidaDTO.setVlAcrescimo(response.getVlAcrescimo());
		saidaDTO.setVlImpostoRenda(response.getVlImpostoRenda());
		saidaDTO.setVlIss(response.getVlIss());
		saidaDTO.setVlIof(response.getVlIof());
		saidaDTO.setVlInss(response.getVlInss());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDsIdentificacaoTransferenciaPagto(response
				.getDsIdentificacaoTransferenciaPagto());
		saidaDTO.setDsidentificacaoTitularPagto(response
				.getDsidentificacaoTitularPagto());
		saidaDTO.setDsFinalidadeDocTed(response.getDsFinalidadeDocTed());

		saidaDTO.setCdIndentificadorJudicial(response
				.getCdIndentificadorJudicial());

		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));
		saidaDTO
		.setNmInscriBeneficio(response.getNmInscriBeneficio() == 0L ? ""
				: String.valueOf(response.getNmInscriBeneficio()));
		saidaDTO.setCdTipoInscriBeneficio(response.getCdTipoInscriBeneficio());
		saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(String.valueOf(response
				.getNrSequenciaContratoNegocio()));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtFloatingPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivFloatPgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());
		saidaDTO.setCdLoteInterno(response.getCdLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoGARE(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO)
	 */
	public DetalharPagtoGARESaidaDTO detalharPagtoGARE(
			DetalharPagtoGAREEntradaDTO detalharPagtoGAREEntradaDTO) {

		DetalharPagtoGARESaidaDTO saidaDTO = new DetalharPagtoGARESaidaDTO();
		DetalharPagtoGARERequest request = new DetalharPagtoGARERequest();
		DetalharPagtoGAREResponse response = new DetalharPagtoGAREResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoGAREEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoGAREEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoGAREEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoGAREEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoGAREEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoGAREEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoGAREEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoGAREEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoGAREEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoGAREEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoGAREEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoGAREEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoGAREEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoGAREEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharPagtoGAREPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdDddContribuinte(response.getCdDddContribuinte() == 0 ? ""
				: String.valueOf(response.getCdDddContribuinte()));
		saidaDTO
		.setNrTelefoneContribuinte(response.getNrTelefoneContribuinte());
		saidaDTO.setCdInscricaoEstadualContribuinte(response
				.getCdInscricaoEstadualContribuinte() == 0L ? "" : String
						.valueOf(response.getCdInscricaoEstadualContribuinte()));
		saidaDTO.setCdReceita(response.getCdReceita());
		saidaDTO.setNrReferencia(response.getNrReferencia() == 0L ? "" : String
				.valueOf(response.getNrReferencia()));
		saidaDTO.setNrCota(response.getNrCota() == 0 ? "" : String
				.valueOf(response.getNrCota()));
		saidaDTO.setCdPercentualReceita(response.getCdPercentualReceita());
		saidaDTO.setDsApuracaoTributo(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDsApuracaoTributo(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtReferencia(FormatarData.formatarDataInteira(response
				.getDtReferencia()));
		saidaDTO.setCdCnae(response.getCdCnae() == 0 ? "" : String
				.valueOf(response.getCdCnae()));
		saidaDTO.setCdPlacaVeiculo(response.getCdPlacaVeiculo());
		saidaDTO.setNrParcela(response.getNrParcela() == 0L ? "" : String
				.valueOf(response.getNrParcela()));
		saidaDTO.setCdOrFavorecido(response.getCdOrFavorecido() == 0L ? ""
				: String.valueOf(response.getCdOrFavorecido()));
		saidaDTO.setDsOrFavorecido(response.getDsOrFavorecido());
		saidaDTO.setCdUfFavorecido(response.getCdUfFavorecido());
		saidaDTO.setDsUfFavorecido(response.getDsUfFavorecido());
		saidaDTO.setVlReceita(response.getVlReceita());
		saidaDTO.setVlPrincipal(response.getVlPrincipal());
		saidaDTO.setVlAtual(response.getVlAtual());
		saidaDTO.setVlAcrescimo(response.getVlAcrescimo());
		saidaDTO.setVlHonorario(response.getVlHonorario());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlTotal(response.getVlTotal());
		saidaDTO.setDsTributo(response.getDsTributo());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtPagamentoGare(FormatarData
				.formataDiaMesAnoFromPdc(response.getDtPagamento()));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdLoteInterno(response.getNrLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoOrdemPagto(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO)
	 */
	public DetalharPagtoOrdemPagtoSaidaDTO detalharPagtoOrdemPagto(
			DetalharPagtoOrdemPagtoEntradaDTO detalharPagtoOrdemPagtoEntradaDTO) {

		DetalharPagtoOrdemPagtoSaidaDTO saidaDTO = new DetalharPagtoOrdemPagtoSaidaDTO();
		DetalharPagtoOrdemPagtoRequest request = new DetalharPagtoOrdemPagtoRequest();
		DetalharPagtoOrdemPagtoResponse response = new DetalharPagtoOrdemPagtoResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoOrdemPagtoEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemPagtoEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoOrdemPagtoEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoOrdemPagtoEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoOrdemPagtoEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemPagtoEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemPagtoEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoOrdemPagtoEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemPagtoEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemPagtoEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoOrdemPagtoEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemPagtoEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemPagtoEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoOrdemPagtoEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharPagtoOrdemPagtoPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setDtExpedicaoCredito(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtExpedicaoCredito(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrOperacao(response.getNrOperacao() == 0 ? "" : String
				.valueOf(response.getNrOperacao()));
		saidaDTO.setCdInsPagamento(response.getCdInsPagamento());
		saidaDTO.setNrCheque(response.getNrCheque() == 0 ? "" : String
				.valueOf(response.getNrCheque()));
		saidaDTO.setNrSerieCheque(response.getNrSerieCheque());
		saidaDTO.setCdIdentificacaoUnidadeAgencia(response
				.getCdIdentificacaoUnidadeAgencia() == 0 ? "" : String
						.valueOf(response.getCdIdentificacaoUnidadeAgencia()));
		saidaDTO.setCdIdentificacaoTipoRetirada(response
				.getCdIdentificacaoTipoRetirada() == 0 ? "" : String
						.valueOf(response.getCdIdentificacaoTipoRetirada()));
		saidaDTO.setCdRetirada(response.getCdRetirada() == 0 ? "" : String
				.valueOf(response.getCdRetirada()));
		saidaDTO.setDtRetirada(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtRetirada(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlImpostoMovimentacaoFinanceira(response
				.getVlImpostoMovimentacaoFinanceira());
		saidaDTO.setVlDescontoCredito(response.getVlDescontoCredito());
		saidaDTO.setVlAbatidoCredito(response.getVlAbatidoCredito());
		saidaDTO.setVlMultaCredito(response.getVlMultaCredito());
		saidaDTO.setVlMoraJuros(response.getVlMoraJuros());
		saidaDTO.setVlOutrasDeducoes(response.getVlOutrasDeducoes());
		saidaDTO.setVlOutroAcrescimo(response.getVlOutroAcrescimo());
		saidaDTO.setVlIrCredito(response.getVlIrCredito());
		saidaDTO.setVlIssCredito(response.getVlIssCredito());
		saidaDTO.setVlIofCredito(response.getVlIofCredito());
		saidaDTO.setVlInssCredito(response.getVlInssCredito());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDsIdentificacaoTipoRetirada(response
				.getDsIdentificacaoTipoRetirada());
		saidaDTO.setDsIdentificacaoRetirada(response
				.getDsIdentificacaoRetirada());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));
		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// BENEFICI�RIO
		saidaDTO
		.setNmInscriBeneficio(response.getNmInscriBeneficio() == 0L ? ""
				: String.valueOf(response.getNmInscriBeneficio()));
		saidaDTO.setCdTipoInscriBeneficio(response.getCdTipoInscriBeneficio());
		saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		saidaDTO.setNumeroInscricaoBeneficiarioFormatado(saidaDTO
				.getNmInscriBeneficio());

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDsUnidadeAgencia(response.getDsUnidadeAgencia());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());
		saidaDTO.setCdFuncEmissaoOp(response.getCdFuncEmissaoOp());
		saidaDTO.setCdTerminalCaixaEmissaoOp(response
				.getCdTerminalCaixaEmissaoOp());
		saidaDTO.setDtEmissaoOp(response.getDtEmissaoOp());
		saidaDTO.setHrEmissaoOp(response.getHrEmissaoOp());
		saidaDTO.setCdLoteInterno(response.getCdLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());
		saidaDTO.setCdBancoEstorno(response.getCdBancoEstorno());
		saidaDTO.setCdAgenciaEstorno(response.getCdAgenciaEstorno());
		saidaDTO.setCdDigitoAgenciaEstorno(response.getCdDigitoAgenciaEstorno());
		saidaDTO.setCdContaEstorno(response.getCdContaEstorno());
		saidaDTO.setCdDigitoContaEstorno(response.getCdDigitoContaEstorno());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoTED(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTEDEntradaDTO)
	 */
	public DetalharManPagtoTEDSaidaDTO detalharManPagtoTED(
			DetalharManPagtoTEDEntradaDTO detalharManPagtoTEDEntradaDTO) {

		DetalharManPagtoTEDSaidaDTO saidaDTO = new DetalharManPagtoTEDSaidaDTO();
		DetalharManPagtoTEDRequest request = new DetalharManPagtoTEDRequest();
		DetalharManPagtoTEDResponse response = new DetalharManPagtoTEDResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoTEDEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTEDEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoTEDEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoTEDEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoTEDEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTEDEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTEDEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoTEDEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTEDEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTEDEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoTEDEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTEDEntradaDTO
						.getCdAgenPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTEDEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoTEDEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharManPagtoTEDPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setVlImpostoRenda(response.getVlImpostoRenda());
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setCdCamaraCentral(response.getCdCamaraCentral() == 0 ? ""
				: String.valueOf(response.getCdCamaraCentral()));
		saidaDTO.setCdFinalidadeDoc(response.getCdFinalidadeDoc());
		saidaDTO.setCdFinalidadeTed(response.getCdFinalidadeTed());
		saidaDTO.setCdIdentificacaoTransferencia(response
				.getCdIdentificacaoTransferencia() == 0 ? "" : String
						.valueOf(response.getCdIdentificacaoTransferencia()));
		saidaDTO
		.setCdIdentificacaoTitular(response.getCdIdentificacaoTitular() == 0 ? ""
				: String.valueOf(response.getCdIdentificacaoTitular()));
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlDeducao(response.getVlDeducao());
		saidaDTO.setVlAcrescimo(response.getVlAcrescimo());
		saidaDTO.setVlIss(response.getVlIss());
		saidaDTO.setVlIof(response.getVlIof());
		saidaDTO.setVlInss(response.getVlInss());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDsIdentificacaoTransferenciaPagto(response
				.getDsIdentificacaoTransferenciaPagto());
		saidaDTO.setDsidentificacaoTitularPagto(response
				.getDsidentificacaoTitularPagto());
		saidaDTO.setDsFinalidadeDocTed(response.getDsFinalidadeDocTed());
		saidaDTO.setCdIndentificadorJudicial(response
				.getCdIndentificadorJudicial().equals("0") ? "" : response
						.getCdIndentificadorJudicial());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setDsBancoCredito(response.getDsBancoFavorecido());
		saidaDTO.setCdIspbPagamento(response.getCdIspbPagamento());
		saidaDTO.setDsNomeReclamante(response.getDsNomeReclamante());
	    saidaDTO.setNrProcessoDepositoJudicial(response.getNrProcessoDepositoJudicial());
	    saidaDTO.setNrPisPasep(response.getNrPisPasep());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// BENEFICI�RIO
		saidaDTO
		.setNmInscriBeneficio(response.getNmInscriBeneficio() == 0L ? ""
				: String.valueOf(response.getNmInscriBeneficio()));
		saidaDTO.setCdTipoInscriBeneficio(response.getCdTipoInscriBeneficio());
		saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		saidaDTO.setNumeroInscricaoBeneficiarioFormatado(saidaDTO
				.getNmInscriBeneficio());

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtFloatingPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivFloatPgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());
		saidaDTO.setCdLoteInterno(response.getCdLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());


		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoCodigoBarras(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO)
	 */
	public DetalharPagtoCodigoBarrasSaidaDTO detalharPagtoCodigoBarras(
			DetalharPagtoCodigoBarrasEntradaDTO detalharPagtoCodigoBarrasEntradaDTO) {

		DetalharPagtoCodigoBarrasSaidaDTO saidaDTO = new DetalharPagtoCodigoBarrasSaidaDTO();
		DetalharPagtoCodigoBarrasRequest request = new DetalharPagtoCodigoBarrasRequest();
		DetalharPagtoCodigoBarrasResponse response = new DetalharPagtoCodigoBarrasResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoCodigoBarrasEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoCodigoBarrasEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoCodigoBarrasEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoCodigoBarrasEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoCodigoBarrasEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoCodigoBarrasEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoCodigoBarrasEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoCodigoBarrasEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoCodigoBarrasEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoCodigoBarrasEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoCodigoBarrasEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoCodigoBarrasEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoCodigoBarrasEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoCodigoBarrasEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharPagtoCodigoBarrasPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdDddTelefone(response.getCdDddTelefone() == 0 ? ""
				: String.valueOf(response.getCdDddTelefone()));
		saidaDTO.setNrTelefone(response.getNrTelefone());
		saidaDTO
		.setCdInscricaoEstadual(response.getCdInscricaoEstadual() == 0L ? ""
				: String.valueOf(response.getCdInscricaoEstadual()));
		saidaDTO.setCdTipoInscricaoDividaAtiva(response
				.getCdTipoInscricaoDividaAtiva());
		saidaDTO.setDsCodigoBarraTributo(response.getDsCodigoBarraTributo());
		saidaDTO
		.setCdAgenteArrecadador(response.getCdAgenteArrecadador() == 0 ? ""
				: String.valueOf(response.getCdAgenteArrecadador()));
		saidaDTO
		.setCdTributoArrecadado(response.getCdTributoArrecadado() == 0 ? ""
				: String.valueOf(response.getCdTributoArrecadado()));
		saidaDTO.setCdReceitaTributo(response.getCdReceitaTributo());
		saidaDTO
		.setNrReferenciaTributo(response.getNrReferenciaTributo() == 0 ? ""
				: String.valueOf(response.getNrReferenciaTributo()));
		saidaDTO
		.setNrCotaParcelaTributo(response.getNrCotaParcelaTributo() == 0 ? ""
				: String.valueOf(response.getNrCotaParcelaTributo()));
		saidaDTO.setVlReceitaTributo(response.getVlReceitaTributo());
		saidaDTO.setCdPercentualTributo(response.getCdPercentualTributo());
		saidaDTO.setCdPeriodoApuracao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getCdPeriodoApuracao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setCdAnoExercicioTributo(response.getCdAnoExercicioTributo() == 0 ? ""
				: String.valueOf(response.getCdAnoExercicioTributo()));
		saidaDTO.setDtReferenciaTributo(FormatarData
				.formatarDataInteira(response.getDtReferenciaTributo()));
		saidaDTO.setDtCompensacao(FormatarData.formatarDataInteira(response
				.getDtCompensacao()));
		saidaDTO.setCdCnae(response.getCdCnae() == 0 ? "" : String
				.valueOf(response.getCdCnae()));
		saidaDTO.setCdPlacaVeiculo(response.getCdPlacaVeiculo());
		saidaDTO
		.setNrParcelamentoTributo(response.getNrParcelamentoTributo() == 0L ? ""
				: String.valueOf(response.getNrParcelamentoTributo()));
		saidaDTO
		.setCdOrgaoFavorecido(response.getCdOrgaoFavorecido() == 0L ? ""
				: String.valueOf(response.getCdOrgaoFavorecido()));
		saidaDTO.setDsOrgaoFavorecido(response.getDsOrgaoFavorecido());
		saidaDTO.setCdUfFavorecido(response.getCdUfFavorecido());
		saidaDTO.setDsUfFavorecido(response.getDsUfFavorecido());
		saidaDTO.setVlReceita(response.getVlReceita());
		saidaDTO.setVlPrincipal(response.getVlPrincipal());
		saidaDTO.setVlAtualizado(response.getVlAtualizado());
		saidaDTO.setVlAcrescimo(response.getVlAcrescimo());
		saidaDTO.setVlHonorario(response.getVlHonorario());
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlTotal(response.getVlTotal());
		saidaDTO.setDsObservacao(response.getDsObservacao());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdLoteInterno(response.getNrLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoDebitoVeiculos(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO)
	 */
	public DetalharPagtoDebitoVeiculosSaidaDTO detalharPagtoDebitoVeiculos(
			DetalharPagtoDebitoVeiculosEntradaDTO detalharPagtoDebitoVeiculosEntradaDTO) {

		DetalharPagtoDebitoVeiculosSaidaDTO saidaDTO = new DetalharPagtoDebitoVeiculosSaidaDTO();
		DetalharPagtoDebitoVeiculosRequest request = new DetalharPagtoDebitoVeiculosRequest();
		DetalharPagtoDebitoVeiculosResponse response = new DetalharPagtoDebitoVeiculosResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoDebitoVeiculosEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoVeiculosEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoDebitoVeiculosEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoDebitoVeiculosEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoDebitoVeiculosEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoVeiculosEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoVeiculosEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoDebitoVeiculosEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoVeiculosEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoVeiculosEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoDebitoVeiculosEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoVeiculosEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoDebitoVeiculosEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoDebitoVeiculosEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter()
		.getDetalharPagtoDebitoVeiculosPDCAdapter().invokeProcess(
				request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setNrCotaParcelaTrib(response.getNrCotaParcelaTrib() == 0 ? ""
				: String.valueOf(response.getNrCotaParcelaTrib()));
		saidaDTO.setDtAnoExercTributo(response.getDtAnoExercTributo() == 0 ? ""
				: String.valueOf(response.getDtAnoExercTributo()));
		saidaDTO.setCdPlacaVeiculo(response.getCdPlacaVeiculo());
		saidaDTO.setCdRenavamVeicTributo(response.getCdRenavamVeicTributo());
		saidaDTO
		.setCdMunicArrecadTrib(response.getCdMunicArrecadTrib() == 0L ? ""
				: String.valueOf(response.getCdMunicArrecadTrib()));
		saidaDTO.setCdUfTributo(response.getCdUfTributo());
		saidaDTO.setNrNsuTributo(response.getNrNsuTributo() == 0L ? "" : String
				.valueOf(response.getNrNsuTributo()));
		saidaDTO.setCdOpcaoTributo(response.getCdOpcaoTributo() == 0 ? ""
				: String.valueOf(response.getCdOpcaoTributo()));
		saidaDTO.setCdOpcaoRetirada(response.getCdOpcaoRetirada() == 0 ? ""
				: String.valueOf(response.getCdOpcaoRetirada()));
		saidaDTO.setVlPrincipalTributo(response.getVlPrincipalTributo());
		saidaDTO.setVlAtualMonetar(response.getVlAtualMonetar());
		saidaDTO.setVlAcrescimoFinanc(response.getVlAcrescimoFinanc());
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMoraJuros(response.getVlMoraJuros());
		saidaDTO.setVlTotalTributo(response.getVlTotalTributo());
		saidaDTO.setDsObservacaoTributo(response.getDsObservacaoTributo());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtFloatingPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivFloatPgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoTituloBradesco(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloBradescoEntradaDTO)
	 */
	public DetalharManPagtoTituloBradescoSaidaDTO detalharManPagtoTituloBradesco(
			DetalharManPagtoTituloBradescoEntradaDTO detalharManPagtoTituloBradescoEntradaDTO) {
		DetalharManPagtoTituloBradescoSaidaDTO saidaDTO = new DetalharManPagtoTituloBradescoSaidaDTO();
		DetalharManPagtoTituloBradescoRequest request = new DetalharManPagtoTituloBradescoRequest();
		DetalharManPagtoTituloBradescoResponse response = new DetalharManPagtoTituloBradescoResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoTituloBradescoEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloBradescoEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoTituloBradescoEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoTituloBradescoEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoTituloBradescoEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloBradescoEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloBradescoEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoTituloBradescoEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloBradescoEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloBradescoEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoTituloBradescoEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloBradescoEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloBradescoEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoTituloBradescoEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter()
		.getDetalharManPagtoTituloBradescoPDCAdapter().invokeProcess(
				request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setCdBarras(response.getCdBarras());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMoraJuros(response.getVlMoraJuros());
		saidaDTO.setVlOutrasDeducoes(response.getVlOutrasDeducoes());
		saidaDTO.setVlOutrosAcrescimos(response.getVlOutrosAcrescimos());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());
		saidaDTO.setNossoNumero(PgitUtil.formatarNossoNumero(response
				.getNossoNumero()));
		saidaDTO.setCdOrigemPagamento(response.getCdOrigemPagamento());
		saidaDTO.setDsOrigemPagamento(response.getDsOrigemPagamento());
		saidaDTO.setDtLimiteDescontoPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtLimiteDescontoPagamento(), "dd.MM.yyyy",
				"dd/MM/yyyy"));
		saidaDTO.setCarteira(response.getCdCartaoTituloCobranca() == 0 ? ""
				: String.valueOf(response.getCdCartaoTituloCobranca()));

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtLimitePagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtLimitePgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setCdRastreado(response.getCdRastreado());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtFloatingPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivFloatPgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());
		saidaDTO.setVlBonfcao(response.getVlBonfcao());
		saidaDTO.setValorBonificacao(response.getVlBonificacao());
		saidaDTO.setCdLoteInterno(response.getCdLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoDOC(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDOCEntradaDTO)
	 */
	public DetalharManPagtoDOCSaidaDTO detalharManPagtoDOC(
			DetalharManPagtoDOCEntradaDTO detalharManPagtoDOCEntradaDTO) {

		DetalharManPagtoDOCSaidaDTO saidaDTO = new DetalharManPagtoDOCSaidaDTO();
		DetalharManPagtoDOCRequest request = new DetalharManPagtoDOCRequest();
		DetalharManPagtoDOCResponse response = new DetalharManPagtoDOCResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoDOCEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDOCEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoDOCEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoDOCEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoDOCEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDOCEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDOCEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoDOCEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDOCEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDOCEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoDOCEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDOCEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDOCEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoDOCEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharManPagtoDOCPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdCamaraCentral(response.getCdCamaraCentral() == 0 ? ""
				: String.valueOf(response.getCdCamaraCentral()));
		saidaDTO.setCdFinalidadeDoc(response.getCdFinalidadeDoc());
		saidaDTO.setCdFinalidadeTed(response.getCdFinalidadeTed());
		saidaDTO.setCdIdentificacaoTransferencia(response
				.getCdIdentificacaoTransferencia() == 0 ? "" : String
						.valueOf(response.getCdIdentificacaoTransferencia()));
		saidaDTO
		.setCdIdentificacaoTitular(response.getCdIdentificacaoTitular() == 0 ? ""
				: String.valueOf(response.getCdIdentificacaoTitular()));
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlDeducao(response.getVlDeducao());
		saidaDTO.setVlAcrescimo(response.getVlAcrescimo());
		saidaDTO.setVlImpostoRenda(response.getVlImpostoRenda());
		saidaDTO.setVlIss(response.getVlIss());
		saidaDTO.setVlIof(response.getVlIof());
		saidaDTO.setVlInss(response.getVlInss());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));
		saidaDTO
		.setNmInscriBeneficio(response.getNmInscriBeneficio() == 0L ? ""
				: String.valueOf(response.getNmInscriBeneficio()));
		saidaDTO.setCdTipoInscriBeneficio(response.getCdTipoInscriBeneficio());
		saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		saidaDTO.setDtPagamento(response.getDtPagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(String.valueOf(response
				.getNrSequenciaContratoNegocio()));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());

		saidaDTO.setDsIdentificacaoTransferenciaPagto(response
				.getDsIdentificacaoTransferenciaPagto());
		saidaDTO.setDsidentificacaoTitularPagto(response
				.getDsidentificacaoTitularPagto());
		saidaDTO.setDsFinalidadeDocTed(response.getDsFinalidadeDocTed());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtFloatingPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivFloatPgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());
		saidaDTO.setCdLoteInterno(response.getCdLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoTituloOutrosBancos(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloOutrosBancosEntradaDTO)
	 */
	public DetalharManPagtoTituloOutrosBancosSaidaDTO detalharManPagtoTituloOutrosBancos(
			DetalharManPagtoTituloOutrosBancosEntradaDTO detalharManPagtoTituloOutrosBancosEntradaDTO) {
		DetalharManPagtoTituloOutrosBancosSaidaDTO saidaDTO = new DetalharManPagtoTituloOutrosBancosSaidaDTO();
		DetalharManPagtoTituloOutrosBancosRequest request = new DetalharManPagtoTituloOutrosBancosRequest();
		DetalharManPagtoTituloOutrosBancosResponse response = new DetalharManPagtoTituloOutrosBancosResponse();

		request.setCdpessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request
		.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request
		.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getDsEfetivacaoPagamento()));
		request
		.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getCdControlePagamento()));
		request
		.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getCdBancoDebito()));
		request
		.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getCdContaDebito()));
		request
		.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getCdBancoCredito()));
		request
		.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getCdContaCredito()));
		request
		.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request
		.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request
		.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoTituloOutrosBancosEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter()
		.getDetalharManPagtoTituloOutrosBancosPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setCdBarras(response.getCdBarras());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMoraJuros(response.getVlMoraJuros());
		saidaDTO.setVlOutrasDeducoes(response.getVlOutrasDeducoes());
		saidaDTO.setVlOutrosAcrescimos(response.getVlOutrosAcrescimos());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());
		saidaDTO.setNossoNumero(PgitUtil.formatarNossoNumero(response
				.getNossoNumero()));
		saidaDTO.setCdOrigemPagamento(response.getCdOrigemPagamento());
		saidaDTO.setDsOrigemPagamento(response.getDsOrigemPagamento());
		saidaDTO.setDtLimiteDescontoPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtLimiteDescontoPagamento(), "dd.MM.yyyy",
				"dd/MM/yyyy"));
		saidaDTO.setCarteira(response.getCdCartaoTituloCobranca() == 0 ? ""
				: String.valueOf(response.getCdCartaoTituloCobranca()));

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// SACADOR/AVALISTA
		
		
		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtLimitePagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtLimitePgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setCdRastreado(response.getCdRastreado());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtFloatingPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivFloatPgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());
		saidaDTO.setVlBonfcao(response.getVlBonfcao());
		saidaDTO.setValorBonificacao(response.getVlBonificacao());
		saidaDTO.setCdLoteInterno(response.getCdLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoDepIdentificado(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDepIdentificadoEntradaDTO)
	 */
	public DetalharManPagtoDepIdentificadoSaidaDTO detalharManPagtoDepIdentificado(
			DetalharManPagtoDepIdentificadoEntradaDTO detalharManPagtoDepIdentificadoEntradaDTO) {
		DetalharManPagtoDepIdentificadoSaidaDTO saidaDTO = new DetalharManPagtoDepIdentificadoSaidaDTO();
		DetalharManPagtoDepIdentificadoRequest request = new DetalharManPagtoDepIdentificadoRequest();
		DetalharManPagtoDepIdentificadoResponse response = new DetalharManPagtoDepIdentificadoResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoDepIdentificadoEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDepIdentificadoEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoDepIdentificadoEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoDepIdentificadoEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoDepIdentificadoEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDepIdentificadoEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDepIdentificadoEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoDepIdentificadoEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDepIdentificadoEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDepIdentificadoEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoDepIdentificadoEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDepIdentificadoEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDepIdentificadoEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoDepIdentificadoEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter()
		.getDetalharManPagtoDepIdentificadoPDCAdapter().invokeProcess(
				request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdTipoDepositoId(response.getCdTipoDepositoId());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO
		.setCdClienteDepositoId(response.getCdClienteDepositoId() == 0L ? ""
				: String.valueOf(response.getCdClienteDepositoId()));
		saidaDTO
		.setCdProdutoDepositoId(response.getCdProdutoDepositoId() == 0 ? ""
				: String.valueOf(response.getCdProdutoDepositoId()));
		saidaDTO.setCdSequenciaProdutoDepositoId(response
				.getCdSequenciaProdutoDepositoId() == 0 ? "" : String
						.valueOf(response.getCdSequenciaProdutoDepositoId()));
		saidaDTO.setCdBancoCartaoDepositanteId(response
				.getCdBancoCartaoDepositanteId() == 0 ? "" : String
						.valueOf(response.getCdBancoCartaoDepositanteId()));
		saidaDTO.setCdCartaoDepositante(response.getCdCartaoDepositante());
		saidaDTO.setCdBinCartaoDepositanteId(response
				.getCdBinCartaoDepositanteId() == 0 ? "" : String
						.valueOf(response.getCdBinCartaoDepositanteId()));
		saidaDTO.setCdViaCartaoDepositanteId(response
				.getCdViaCartaoDepositanteId() == 0 ? "" : String
						.valueOf(response.getCdViaCartaoDepositanteId()));
		saidaDTO.setCdDepositoAnteriorId(response.getCdDepositoAnteriorId());
		saidaDTO.setDsDepositoAnteriorId(response.getDsDepositoAnteriorId());
		saidaDTO.setVlDescontoCreditoPagamento(response
				.getVlDescontoCreditoPagamento());
		saidaDTO.setVlAbatidoCreditoPagamento(response
				.getVlAbatidoCreditoPagamento());
		saidaDTO.setVlMultaCreditoPagamento(response
				.getVlMultaCreditoPagamento());
		saidaDTO.setVlMoraJurosCreditoPagamento(response
				.getVlMoraJurosCreditoPagamento());
		saidaDTO.setVlOutraDeducaoCreditoPagamento(response
				.getVlOutraDeducaoCreditoPagamento());
		saidaDTO.setVloutroAcrescimoCreditoPagamento(response
				.getVloutroAcrescimoCreditoPagamento());
		saidaDTO.setVlIrCreditoPagamento(response.getVlIrCreditoPagamento());
		saidaDTO.setVlIssCreditoPagamento(response.getVlIssCreditoPagamento());
		saidaDTO.setVlIofCreditoPagamento(response.getVlIofCreditoPagamento());
		saidaDTO
		.setVlInssCreditoPagamento(response.getVlInssCreditoPagamento());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// BENEFICI�RIO
		/*
		 * saidaDTO.setNmInscriBeneficio(response.getNmInscriBeneficio() == 0L ?
		 * "" : String.valueOf(response.getNmInscriBeneficio()));
		 * saidaDTO.setCdTipoInscriBeneficio
		 * (response.getCdTipoInscriBeneficio());
		 * saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		 * saidaDTO.setNumeroInscricaoBeneficiarioFormatado
		 * (saidaDTO.getNmInscriBeneficio());
		 */

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoOrdemCredito(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemCreditoEntradaDTO)
	 */
	public DetalharManPagtoOrdemCreditoSaidaDTO detalharManPagtoOrdemCredito(
			DetalharManPagtoOrdemCreditoEntradaDTO detalharManPagtoOrdemCreditoEntradaDTO) {
		DetalharManPagtoOrdemCreditoSaidaDTO saidaDTO = new DetalharManPagtoOrdemCreditoSaidaDTO();
		DetalharManPagtoOrdemCreditoRequest request = new DetalharManPagtoOrdemCreditoRequest();
		DetalharManPagtoOrdemCreditoResponse response = new DetalharManPagtoOrdemCreditoResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoOrdemCreditoEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemCreditoEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoOrdemCreditoEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoOrdemCreditoEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoOrdemCreditoEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemCreditoEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemCreditoEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoOrdemCreditoEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemCreditoEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemCreditoEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoOrdemCreditoEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemCreditoEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemCreditoEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoOrdemCreditoEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter()
		.getDetalharManPagtoOrdemCreditoPDCAdapter().invokeProcess(
				request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdPagador(response.getCdPagador());
		saidaDTO.setCdOrdemCreditoPagamento(response
				.getCdOrdemCreditoPagamento() == 0L ? "" : String
						.valueOf(response.getCdOrdemCreditoPagamento()));
		saidaDTO.setVlDescontoCreditoPagamento(response
				.getVlDescontoCreditoPagamento());
		saidaDTO.setVlAbatidoCreditoPagamento(response
				.getVlAbatidoCreditoPagamento());
		saidaDTO.setVlMultaCreditoPagamento(response
				.getVlMultaCreditoPagamento());
		saidaDTO.setVlMoraJurosCreditoPagamento(response
				.getVlMoraJurosCreditoPagamento());
		saidaDTO.setVlOutraDeducaoCreditoPagamento(response
				.getVlOutraDeducaoCreditoPagamento());
		saidaDTO.setVlOutroAcrescimoCreditoPagamento(response
				.getVlOutroAcrescimoCreditoPagamento());
		saidaDTO.setVlIrCreditoPagamento(response.getVlIrCreditoPagamento());
		saidaDTO.setVlIssCreditoPagamento(response.getVlIssCreditoPagamento());
		saidaDTO.setVlIofCreditoPagamento(response.getVlIofCreditoPagamento());
		saidaDTO
		.setVlInssCreditoPagamento(response.getVlInssCreditoPagamento());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// BENEFICI�RIO
		/*
		 * saidaDTO.setNmInscriBeneficio(response.getNmInscriBeneficio() == 0L ?
		 * "" : String.valueOf(response.getNmInscriBeneficio()));
		 * saidaDTO.setCdTipoInscriBeneficio
		 * (response.getCdTipoInscriBeneficio());
		 * saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		 * saidaDTO.setNumeroInscricaoBeneficiarioFormatado
		 * (saidaDTO.getNmInscriBeneficio());
		 */

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoCodigoBarras(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoCodigoBarrasEntradaDTO)
	 */
	public DetalharManPagtoCodigoBarrasSaidaDTO detalharManPagtoCodigoBarras(
			DetalharManPagtoCodigoBarrasEntradaDTO detalharManPagtoCodigoBarrasEntradaDTO) {
		DetalharManPagtoCodigoBarrasSaidaDTO saidaDTO = new DetalharManPagtoCodigoBarrasSaidaDTO();
		DetalharManPagtoCodigoBarrasRequest request = new DetalharManPagtoCodigoBarrasRequest();
		DetalharManPagtoCodigoBarrasResponse response = new DetalharManPagtoCodigoBarrasResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoCodigoBarrasEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoCodigoBarrasEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoCodigoBarrasEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoCodigoBarrasEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoCodigoBarrasEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoCodigoBarrasEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoCodigoBarrasEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoCodigoBarrasEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoCodigoBarrasEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoCodigoBarrasEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoCodigoBarrasEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoCodigoBarrasEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoCodigoBarrasEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoCodigoBarrasEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter()
		.getDetalharManPagtoCodigoBarrasPDCAdapter().invokeProcess(
				request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		saidaDTO.setCdDddTelefone(response.getCdDddTelefone() == 0 ? ""
				: String.valueOf(response.getCdDddTelefone()));
		saidaDTO.setNrTelefone(response.getNrTelefone());
		saidaDTO
		.setCdInscricaoEstadual(response.getCdInscricaoEstadual() == 0L ? ""
				: String.valueOf(response.getCdInscricaoEstadual()));
		saidaDTO.setCdTipoInscricaoDividaAtiva(response
				.getCdTipoInscricaoDividaAtiva());
		saidaDTO.setDsCodigoBarraTributo(response.getDsCodigoBarraTributo());
		saidaDTO
		.setCdAgenteArrecadador(response.getCdAgenteArrecadador() == 0 ? ""
				: String.valueOf(response.getCdAgenteArrecadador()));
		saidaDTO
		.setCdTributoArrecadado(response.getCdTributoArrecadado() == 0 ? ""
				: String.valueOf(response.getCdTributoArrecadado()));
		saidaDTO.setCdReceitaTributo(response.getCdReceitaTributo());
		saidaDTO
		.setNrReferenciaTributo(response.getNrReferenciaTributo() == 0L ? ""
				: String.valueOf(response.getNrReferenciaTributo()));
		saidaDTO
		.setNrCotaParcelaTributo(response.getNrCotaParcelaTributo() == 0 ? ""
				: String.valueOf(response.getNrCotaParcelaTributo()));
		saidaDTO.setVlReceitaTributo(response.getVlReceitaTributo());
		saidaDTO.setCdPercentualTributo(response.getCdPercentualTributo());
		saidaDTO.setCdPeriodoApuracao(response.getCdPeriodoApuracao());
		saidaDTO
		.setCdAnoExercicioTributo(response.getCdAnoExercicioTributo() == 0 ? ""
				: String.valueOf(response.getCdAnoExercicioTributo()));
		saidaDTO.setDtReferenciaTributo(FormatarData
				.formatarDataInteira(response.getDtReferenciaTributo()));
		saidaDTO.setDtCompensacao(FormatarData.formatarDataInteira(response
				.getDtCompensacao()));
		saidaDTO.setCdCnae(response.getCdCnae() == 0 ? "" : String
				.valueOf(response.getCdCnae()));
		saidaDTO.setCdPlacaVeiculo(response.getCdPlacaVeiculo());
		saidaDTO
		.setNrParcelamentoTributo(response.getNrParcelamentoTributo() == 0L ? ""
				: String.valueOf(response.getNrParcelamentoTributo()));
		saidaDTO
		.setCdOrgaoFavorecido(response.getCdOrgaoFavorecido() == 0L ? ""
				: String.valueOf(response.getCdOrgaoFavorecido()));
		saidaDTO.setDsOrgaoFavorecido(response.getDsOrgaoFavorecido());
		saidaDTO.setCdUfFavorecido(response.getCdUfFavorecido());
		saidaDTO.setDsUfFavorecido(response.getDsUfFavorecido());
		saidaDTO.setVlReceita(response.getVlReceita());
		saidaDTO.setVlPrincipal(response.getVlPrincipal());
		saidaDTO.setVlAtualizado(response.getVlAtualizado());
		saidaDTO.setVlAcrescimo(response.getVlAcrescimo());
		saidaDTO.setVlHonorario(response.getVlHonorario());
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlTotal(response.getVlTotal());
		saidaDTO.setDsObservacao(response.getDsObservacao());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdLoteInterno(response.getNrLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoDebitoVeiculos(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoVeiculosEntradaDTO)
	 */
	public DetalharManPagtoDebitoVeiculosSaidaDTO detalharManPagtoDebitoVeiculos(
			DetalharManPagtoDebitoVeiculosEntradaDTO detalharManPagtoDebitoVeiculosEntradaDTO) {
		DetalharManPagtoDebitoVeiculosSaidaDTO saidaDTO = new DetalharManPagtoDebitoVeiculosSaidaDTO();
		DetalharManPagtoDebitoVeiculosRequest request = new DetalharManPagtoDebitoVeiculosRequest();
		DetalharManPagtoDebitoVeiculosResponse response = new DetalharManPagtoDebitoVeiculosResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoDebitoVeiculosEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoVeiculosEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoDebitoVeiculosEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoDebitoVeiculosEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoDebitoVeiculosEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoVeiculosEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoVeiculosEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoDebitoVeiculosEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoVeiculosEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoVeiculosEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoDebitoVeiculosEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoVeiculosEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDebitoVeiculosEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoDebitoVeiculosEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter()
		.getDetalharManPagtoDebitoVeiculosPDCAdapter().invokeProcess(
				request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setNrCotaParcelaTrib(response.getNrCotaParcelaTrib() == 0 ? ""
				: String.valueOf(response.getNrCotaParcelaTrib()));
		saidaDTO.setDtAnoExercTributo(response.getDtAnoExercTributo() == 0 ? ""
				: String.valueOf(response.getDtAnoExercTributo()));
		saidaDTO.setCdPlacaVeiculo(response.getCdPlacaVeiculo());
		saidaDTO.setCdRenavamVeicTributo(response.getCdRenavamVeicTributo());
		saidaDTO
		.setCdMunicArrecadTrib(response.getCdMunicArrecadTrib() == 0L ? ""
				: String.valueOf(response.getCdMunicArrecadTrib()));
		saidaDTO.setCdUfTributo(response.getCdUfTributo());
		saidaDTO.setNrNsuTributo(response.getNrNsuTributo() == 0L ? "" : String
				.valueOf(response.getNrNsuTributo()));
		saidaDTO.setCdOpcaoTributo(response.getCdOpcaoTributo() == 0 ? ""
				: String.valueOf(response.getCdOpcaoTributo()));
		saidaDTO.setCdOpcaoRetirada(response.getCdOpcaoRetirada() == 0 ? ""
				: String.valueOf(response.getCdOpcaoRetirada()));
		saidaDTO.setVlPrincipalTributo(response.getVlPrincipalTributo());
		saidaDTO.setVlAtualMonetar(response.getVlAtualMonetar());
		saidaDTO.setVlAcrescimoFinanc(response.getVlAcrescimoFinanc());
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMoraJuros(response.getVlMoraJuros());
		saidaDTO.setVlTotalTributo(response.getVlTotalTributo());
		saidaDTO.setDsObservacaoTributo(response.getDsObservacaoTributo());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtFloatingPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivFloatPgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoGARE(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGAREEntradaDTO)
	 */
	public DetalharManPagtoGARESaidaDTO detalharManPagtoGARE(
			DetalharManPagtoGAREEntradaDTO detalharManPagtoGAREEntradaDTO) {

		DetalharManPagtoGARESaidaDTO saidaDTO = new DetalharManPagtoGARESaidaDTO();
		DetalharManPagtoGARERequest request = new DetalharManPagtoGARERequest();
		DetalharManPagtoGAREResponse response = new DetalharManPagtoGAREResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoGAREEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGAREEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoGAREEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoGAREEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoGAREEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGAREEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGAREEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoGAREEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGAREEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGAREEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoGAREEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGAREEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGAREEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoGAREEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharManPagtoGAREPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdDddContribuinte(response.getCdDddContribuinte() == 0 ? ""
				: String.valueOf(response.getCdDddContribuinte()));
		saidaDTO
		.setNrTelefoneContribuinte(response.getNrTelefoneContribuinte());
		saidaDTO.setCdInscricaoEstadualContribuinte(response
				.getCdInscricaoEstadualContribuinte() == 0L ? "" : String
						.valueOf(response.getCdInscricaoEstadualContribuinte()));
		saidaDTO.setCdReceita(response.getCdReceita());
		saidaDTO.setNrReferencia(response.getNrReferencia() == 0L ? "" : String
				.valueOf(response.getNrReferencia()));
		saidaDTO.setNrCota(response.getNrCota() == 0 ? "" : String
				.valueOf(response.getNrCota()));
		saidaDTO.setCdPercentualReceita(response.getCdPercentualReceita());
		saidaDTO.setDsApuracaoTributo(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDsApuracaoTributo(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtReferencia(FormatarData.formatarDataInteira(response
				.getDtReferencia()));
		saidaDTO.setCdCnae(response.getCdCnae() == 0 ? "" : String
				.valueOf(response.getCdCnae()));
		saidaDTO.setCdPlacaVeiculo(response.getCdPlacaVeiculo());
		saidaDTO.setNrParcela(response.getNrParcela() == 0L ? "" : String
				.valueOf(response.getNrParcela()));
		saidaDTO.setCdOrFavorecido(response.getCdOrFavorecido() == 0L ? ""
				: String.valueOf(response.getCdOrFavorecido()));
		saidaDTO.setDsOrFavorecido(response.getDsOrFavorecido());
		saidaDTO.setCdUfFavorecido(response.getCdUfFavorecido());
		saidaDTO.setDsUfFavorecido(response.getDsUfFavorecido());
		saidaDTO.setVlReceita(response.getVlReceita());
		saidaDTO.setVlPrincipal(response.getVlPrincipal());
		saidaDTO.setVlAtual(response.getVlAtual());
		saidaDTO.setVlAcrescimo(response.getVlAcrescimo());
		saidaDTO.setVlHonorario(response.getVlHonorario());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlTotal(response.getVlTotal());
		saidaDTO.setDsTributo(response.getDsTributo());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// PAGAMENTO
		saidaDTO.setDtPagamento(FormatarData.formataDiaMesAnoFromPdc(response
				.getDtPagamento()));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdLoteInterno(response.getNrLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoGPS(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGPSEntradaDTO)
	 */
	public DetalharManPagtoGPSSaidaDTO detalharManPagtoGPS(
			DetalharManPagtoGPSEntradaDTO detalharManPagtoGPSEntradaDTO) {

		DetalharManPagtoGPSSaidaDTO saidaDTO = new DetalharManPagtoGPSSaidaDTO();
		DetalharManPagtoGPSRequest request = new DetalharManPagtoGPSRequest();
		DetalharManPagtoGPSResponse response = new DetalharManPagtoGPSResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoGPSEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGPSEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoGPSEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoGPSEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoGPSEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGPSEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGPSEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoGPSEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGPSEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGPSEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoGPSEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGPSEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoGPSEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoGPSEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharManPagtoGPSPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdDddContribuinte(response.getCdDddContribuinte() == 0 ? ""
				: String.valueOf(response.getCdDddContribuinte()));
		saidaDTO.setCdTelContribuinte(response.getCdTelContribuinte());
		saidaDTO.setCdInss(response.getCdInss() == 0 ? "" : String
				.valueOf(response.getCdInss()));
		saidaDTO.setDsMesAnoCompt(FormatarData.formatarDataInteira(response
				.getDsMesAnoCompt()));
		saidaDTO.setVlPrincipal(response.getVlPrincipal());
		saidaDTO.setVlOutraEntidade(response.getVlOutraEntidade());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlTotal(response.getVlTotal());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdLoteInterno(response.getNrLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoDepIdentificado(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO)
	 */
	public DetalharPagtoDepIdentificadoSaidaDTO detalharPagtoDepIdentificado(
			DetalharPagtoDepIdentificadoEntradaDTO detalharPagtoDepIdentificadoEntradaDTO) {
		DetalharPagtoDepIdentificadoSaidaDTO saidaDTO = new DetalharPagtoDepIdentificadoSaidaDTO();
		DetalharPagtoDepIdentificadoRequest request = new DetalharPagtoDepIdentificadoRequest();
		DetalharPagtoDepIdentificadoResponse response = new DetalharPagtoDepIdentificadoResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoDepIdentificadoEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoDepIdentificadoEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoDepIdentificadoEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoDepIdentificadoEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoDepIdentificadoEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDepIdentificadoEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDepIdentificadoEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoDepIdentificadoEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDepIdentificadoEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDepIdentificadoEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoDepIdentificadoEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoDepIdentificadoEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoDepIdentificadoEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoDepIdentificadoEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter()
		.getDetalharPagtoDepIdentificadoPDCAdapter().invokeProcess(
				request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdTipoDepositoId(response.getCdTipoDepositoId());
		saidaDTO
		.setCdClienteDepositoId(response.getCdClienteDepositoId() == 0L ? ""
				: String.valueOf(response.getCdClienteDepositoId()));
		saidaDTO
		.setCdProdutoDepositoId(response.getCdProdutoDepositoId() == 0 ? ""
				: String.valueOf(response.getCdProdutoDepositoId()));
		saidaDTO.setCdSequenciaProdutoDepositoId(response
				.getCdSequenciaProdutoDepositoId() == 0 ? "" : String
						.valueOf(response.getCdSequenciaProdutoDepositoId()));
		saidaDTO.setCdBancoCartaoDepositanteId(response
				.getCdBancoCartaoDepositanteId() == 0 ? "" : String
						.valueOf(response.getCdBancoCartaoDepositanteId()));
		saidaDTO.setCdCartaoDepositante(response.getCdCartaoDepositante());
		saidaDTO.setCdBinCartaoDepositanteId(response
				.getCdBinCartaoDepositanteId() == 0 ? "" : String
						.valueOf(response.getCdBinCartaoDepositanteId()));
		saidaDTO.setCdViaCartaoDepositanteId(response
				.getCdViaCartaoDepositanteId() == 0 ? "" : String
						.valueOf(response.getCdViaCartaoDepositanteId()));
		saidaDTO.setCdDepositoAnteriorId(response.getCdDepositoAnteriorId());
		saidaDTO.setDsDepositoAnteriorId(response.getDsDepositoAnteriorId());
		saidaDTO.setVlDescontoCreditoPagamento(response
				.getVlDescontoCreditoPagamento());
		saidaDTO.setVlAbatidoCreditoPagamento(response
				.getVlAbatidoCreditoPagamento());
		saidaDTO.setVlMultaCreditoPagamento(response
				.getVlMultaCreditoPagamento());
		saidaDTO.setVlMoraJurosCreditoPagamento(response
				.getVlMoraJurosCreditoPagamento());
		saidaDTO.setVlOutraDeducaoCreditoPagamento(response
				.getVlOutraDeducaoCreditoPagamento());
		saidaDTO.setVloutroAcrescimoCreditoPagamento(response
				.getVloutroAcrescimoCreditoPagamento());
		saidaDTO.setVlIrCreditoPagamento(response.getVlIrCreditoPagamento());
		saidaDTO.setVlIssCreditoPagamento(response.getVlIssCreditoPagamento());
		saidaDTO.setVlIofCreditoPagamento(response.getVlIofCreditoPagamento());
		saidaDTO
		.setVlInssCreditoPagamento(response.getVlInssCreditoPagamento());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// BENEFICI�RIO
		// saidaDTO.setNmInscriBeneficio(response.get == 0L ? "" :
		// String.valueOf(response.getNmInscriBeneficio()));
		// saidaDTO.setCdTipoInscriBeneficio(response.getCdTipoInscriBeneficio());
		// saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		// saidaDTO.setNumeroInscricaoBeneficiarioFormatado(saidaDTO.getNmInscriBeneficio());

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoDARF(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDARFEntradaDTO)
	 */
	public DetalharManPagtoDARFSaidaDTO detalharManPagtoDARF(
			DetalharManPagtoDARFEntradaDTO detalharManPagtoDARFEntradaDTO) {
		DetalharManPagtoDARFSaidaDTO saidaDTO = new DetalharManPagtoDARFSaidaDTO();
		DetalharManPagtoDARFRequest request = new DetalharManPagtoDARFRequest();
		DetalharManPagtoDARFResponse response = new DetalharManPagtoDARFResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoDARFEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDARFEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoDARFEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoDARFEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoDARFEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDARFEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDARFEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoDARFEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDARFEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDARFEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoDARFEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDARFEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoDARFEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoDARFEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharManPagtoDARFPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdDddTelefone(response.getCdDddTelefone() == 0 ? ""
				: String.valueOf(response.getCdDddTelefone()));
		saidaDTO.setCdTelefone(response.getCdTelefone());
		saidaDTO
		.setCdInscricaoEstadual(response.getCdInscricaoEstadual() == 0L ? ""
				: String.valueOf(response.getCdInscricaoEstadual()));
		saidaDTO.setCdAgenteArrecad(response.getCdAgenteArrecad() == 0 ? ""
				: String.valueOf(response.getCdAgenteArrecad()));
		saidaDTO.setCdReceitaTributo(response.getCdReceitaTributo());
		saidaDTO
		.setNrReferenciaTributo(response.getNrReferenciaTributo() == 0L ? ""
				: String.valueOf(response.getNrReferenciaTributo()));
		saidaDTO.setNrCotaParcela(response.getNrCotaParcela() == 0 ? ""
				: String.valueOf(response.getNrCotaParcela()));
		saidaDTO.setCdPercentualReceita(response.getCdPercentualReceita());
		saidaDTO.setDsPeriodoApuracao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDsPeriodoApuracao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setCdDanoExercicio(response.getCdDanoExercicio() == 0 ? ""
				: String.valueOf(response.getCdDanoExercicio()));
		saidaDTO.setDtReferenciaTributo(FormatarData
				.formatarDataInteira(response.getDtReferenciaTributo()));
		saidaDTO.setVlReceita(response.getVlReceita());
		saidaDTO.setVlPrincipal(response.getVlPrincipal());
		saidaDTO.setVlAtualMonet(response.getVlAtualMonet());
		saidaDTO.setVlAbatimentoCredito(response.getVlAbatimentoCredito());
		saidaDTO.setVlMultaCredito(response.getVlMultaCredito());
		saidaDTO.setVlMoraJuros(response.getVlMoraJuros());
		saidaDTO.setVlTotalTributo(response.getVlTotalTributo());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdLoteInterno(response.getNrLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharManPagtoOrdemPagto(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemPagtoEntradaDTO)
	 */
	public DetalharManPagtoOrdemPagtoSaidaDTO detalharManPagtoOrdemPagto(
			DetalharManPagtoOrdemPagtoEntradaDTO detalharManPagtoOrdemPagtoEntradaDTO) {

		DetalharManPagtoOrdemPagtoSaidaDTO saidaDTO = new DetalharManPagtoOrdemPagtoSaidaDTO();
		DetalharManPagtoOrdemPagtoRequest request = new DetalharManPagtoOrdemPagtoRequest();
		DetalharManPagtoOrdemPagtoResponse response = new DetalharManPagtoOrdemPagtoResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharManPagtoOrdemPagtoEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemPagtoEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharManPagtoOrdemPagtoEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharManPagtoOrdemPagtoEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharManPagtoOrdemPagtoEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemPagtoEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemPagtoEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharManPagtoOrdemPagtoEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemPagtoEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemPagtoEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharManPagtoOrdemPagtoEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemPagtoEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharManPagtoOrdemPagtoEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharManPagtoOrdemPagtoEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter()
		.getDetalharManPagtoOrdemPagtoPDCAdapter().invokeProcess(
				request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setDtExpedicaoCredito(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtExpedicaoCredito(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrOperacao(response.getNrOperacao() == 0 ? "" : String
				.valueOf(response.getNrOperacao()));
		saidaDTO.setCdInsPagamento(response.getCdInsPagamento());
		saidaDTO.setNrCheque(response.getNrCheque() == 0 ? "" : String
				.valueOf(response.getNrCheque()));
		saidaDTO.setNrSerieCheque(response.getNrSerieCheque());
		saidaDTO.setCdIdentificacaoUnidadeAgencia(response
				.getCdIdentificacaoUnidadeAgencia() == 0 ? "" : String
						.valueOf(response.getCdIdentificacaoUnidadeAgencia()));
		saidaDTO.setCdIdentificacaoTipoRetirada(response
				.getCdIdentificacaoTipoRetirada() == 0 ? "" : String
						.valueOf(response.getCdIdentificacaoTipoRetirada()));
		saidaDTO.setCdRetirada(response.getCdRetirada() == 0 ? "" : String
				.valueOf(response.getCdRetirada()));
		saidaDTO.setDtRetirada(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtRetirada(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlImpostoMovimentacaoFinanceira(response
				.getVlImpostoMovimentacaoFinanceira());
		saidaDTO.setVlDescontoCredito(response.getVlDescontoCredito());
		saidaDTO.setVlAbatidoCredito(response.getVlAbatidoCredito());
		saidaDTO.setVlMultaCredito(response.getVlMultaCredito());
		saidaDTO.setVlMoraJuros(response.getVlMoraJuros());
		saidaDTO.setVlOutrasDeducoes(response.getVlOutrasDeducoes());
		saidaDTO.setVlOutroAcrescimo(response.getVlOutroAcrescimo());
		saidaDTO.setVlIrCredito(response.getVlIrCredito());
		saidaDTO.setVlIssCredito(response.getVlIssCredito());
		saidaDTO.setVlIofCredito(response.getVlIofCredito());
		saidaDTO.setVlInssCredito(response.getVlInssCredito());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDsIdentificacaoTipoRetirada(response
				.getDsIdentificacaoTipoRetirada());
		saidaDTO.setDsIdentificacaoRetirada(response
				.getDsIdentificacaoRetirada());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// BENEFICI�RIO
		saidaDTO
		.setNmInscriBeneficio(response.getNmInscriBeneficio() == 0L ? ""
				: String.valueOf(response.getNmInscriBeneficio()));
		saidaDTO.setCdTipoInscriBeneficio(response.getCdTipoInscriBeneficio());
		saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		saidaDTO.setNumeroInscricaoBeneficiarioFormatado(saidaDTO
				.getNmInscriBeneficio());

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDsUnidadeAgencia(response.getDsUnidadeAgencia());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoGPS(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO)
	 */
	public DetalharPagtoGPSSaidaDTO detalharPagtoGPS(
			DetalharPagtoGPSEntradaDTO detalharPagtoGPSEntradaDTO) {

		DetalharPagtoGPSSaidaDTO saidaDTO = new DetalharPagtoGPSSaidaDTO();
		DetalharPagtoGPSRequest request = new DetalharPagtoGPSRequest();
		DetalharPagtoGPSResponse response = new DetalharPagtoGPSResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoGPSEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoGPSEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoGPSEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoGPSEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoGPSEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoGPSEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoGPSEntradaDTO
						.getCdAgenciaDebito()));
		request
		.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoGPSEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoGPSEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoGPSEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoGPSEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoGPSEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoGPSEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoGPSEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharPagtoGPSPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdDddContribuinte(response.getCdDddContribuinte() == 0 ? ""
				: String.valueOf(response.getCdDddContribuinte()));
		saidaDTO.setCdTelContribuinte(response.getCdTelContribuinte());
		saidaDTO.setCdInss(response.getCdInss() == 0 ? "" : String
				.valueOf(response.getCdInss()));
		saidaDTO.setDsMesAnoCompt(FormatarData.formatarDataInteira(response
				.getDsMesAnoCompt()));
		saidaDTO.setVlPrincipal(response.getVlPrincipal());
		saidaDTO.setVlOutraEntidade(response.getVlOutraEntidade());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMora(response.getVlMora());
		saidaDTO.setVlTotal(response.getVlTotal());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(response.getDtVencimento());
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));
		// PAGAMENTO
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdLoteInterno(response.getNrLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoTituloOutrosBancos(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO)
	 */
	public DetalharPagtoTituloOutrosBancosSaidaDTO detalharPagtoTituloOutrosBancos(
			DetalharPagtoTituloOutrosBancosEntradaDTO detalharPagtoTituloOutrosBancosEntradaDTO) {
		DetalharPagtoTituloOutrosBancosSaidaDTO saidaDTO = new DetalharPagtoTituloOutrosBancosSaidaDTO();
		DetalharPagtoTituloOutrosBancosRequest request = new DetalharPagtoTituloOutrosBancosRequest();
		DetalharPagtoTituloOutrosBancosResponse response = new DetalharPagtoTituloOutrosBancosResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoTituloOutrosBancosEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloOutrosBancosEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoTituloOutrosBancosEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoTituloOutrosBancosEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoTituloOutrosBancosEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloOutrosBancosEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloOutrosBancosEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoTituloOutrosBancosEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloOutrosBancosEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloOutrosBancosEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoTituloOutrosBancosEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloOutrosBancosEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloOutrosBancosEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoTituloOutrosBancosEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter()
		.getDetalharPagtoTituloOutrosBancosPDCAdapter().invokeProcess(
				request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setCdBarras(PgitUtil.formatarLinhaDigitavel(response
				.getCdBarras()));
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMoraJuros(response.getVlMoraJuros());
		saidaDTO.setVlOutrasDeducoes(response.getVlOutrasDeducoes());
		saidaDTO.setVlOutrosAcrescimos(response.getVlOutrosAcrescimos());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());
		saidaDTO.setNossoNumero(PgitUtil.formatarNossoNumero(response
				.getNossoNumero()));
		saidaDTO.setCdOrigemPagamento(response.getCdOrigemPagamento());
		saidaDTO.setDsOrigemPagamento(response.getDsOrigemPagamento());
		saidaDTO.setDtLimiteDescontoPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtLimiteDescontoPagamento(), "dd.MM.yyyy",
				"dd/MM/yyyy"));
		saidaDTO.setCarteira(response.getCdCartaoTituloCobranca() == 0 ? ""
				: String.valueOf(response.getCdCartaoTituloCobranca()));

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtLimitePagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtLimitePgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setCdRastreado(response.getCdRastreado());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// SACADOR/AVALISTA
		if (response.getCdCpfCnpjSacadorAvalista() == 0) {
			saidaDTO.setNumeroInscricaoSacadorAvalista("");
		}else{
			saidaDTO.setNumeroInscricaoSacadorAvalista(formatCpfCnpjCompleto(response.getCdCpfCnpjSacadorAvalista(), 
					Long.bitCount(response.getCdFilialCnpjSacadorAvalista()) , response.getCdControleCnpjSacadorAvalista()));
		}
		
		saidaDTO.setDsTipoSacadorAvalista(response.getDsTipoInscricaoSacadorAvalista());
		saidaDTO.setDsSacadorAvalista(response.getDsNomeSacadorAvalista());
		
		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtFloatingPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivFloatPgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());
		saidaDTO.setVlBonfcao(response.getVlBonfcao());
		saidaDTO.setVlBonificacao(response.getVlBonificacao());
		saidaDTO.setCdLoteInterno(response.getCdLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoDARF(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO)
	 */
	public DetalharPagtoDARFSaidaDTO detalharPagtoDARF(
			DetalharPagtoDARFEntradaDTO detalharPagtoDARFEntradaDTO) {
		DetalharPagtoDARFSaidaDTO saidaDTO = new DetalharPagtoDARFSaidaDTO();
		DetalharPagtoDARFRequest request = new DetalharPagtoDARFRequest();
		DetalharPagtoDARFResponse response = new DetalharPagtoDARFResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoDARFEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoDARFEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoDARFEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoDARFEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoDARFEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDARFEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDARFEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoDARFEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDARFEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoDARFEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoDARFEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoDARFEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoDARFEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoDARFEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharPagtoDARFPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdDddTelefone(response.getCdDddTelefone() == 0 ? ""
				: String.valueOf(response.getCdDddTelefone()));
		saidaDTO.setCdTelefone(response.getCdTelefone());
		saidaDTO
		.setCdInscricaoEstadual(response.getCdInscricaoEstadual() == 0L ? ""
				: String.valueOf(response.getCdInscricaoEstadual()));
		saidaDTO.setCdAgenteArrecad(response.getCdAgenteArrecad() == 0 ? ""
				: String.valueOf(response.getCdAgenteArrecad()));
		saidaDTO.setCdReceitaTributo(response.getCdReceitaTributo());
		saidaDTO
		.setNrReferenciaTributo(response.getNrReferenciaTributo() == 0L ? ""
				: String.valueOf(response.getNrReferenciaTributo()));
		saidaDTO.setNrCotaParcela(response.getNrCotaParcela() == 0 ? ""
				: String.valueOf(response.getNrCotaParcela()));
		saidaDTO.setCdPercentualReceita(response.getCdPercentualReceita());
		saidaDTO.setDsPeriodoApuracao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDsPeriodoApuracao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setCdDanoExercicio(response.getCdDanoExercicio() == 0 ? ""
				: String.valueOf(response.getCdDanoExercicio()));
		saidaDTO.setDtReferenciaTributo(FormatarData
				.formatarDataInteira(response.getDtReferenciaTributo()));
		saidaDTO.setVlReceita(response.getVlReceita());
		saidaDTO.setVlPrincipal(response.getVlPrincipal());
		saidaDTO.setVlAtualMonet(response.getVlAtualMonet());
		saidaDTO.setVlAbatimentoCredito(response.getVlAbatimentoCredito());
		saidaDTO.setVlMultaCredito(response.getVlMultaCredito());
		saidaDTO.setVlMoraJuros(response.getVlMoraJuros());
		saidaDTO.setVlTotalTributo(response.getVlTotalTributo());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(response.getDtVencimento());
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdLoteInterno(response.getNrLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoTituloBradesco(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO)
	 */
	public DetalharPagtoTituloBradescoSaidaDTO detalharPagtoTituloBradesco(
			DetalharPagtoTituloBradescoEntradaDTO detalharPagtoTituloBradescoEntradaDTO) {
		DetalharPagtoTituloBradescoSaidaDTO saidaDTO = new DetalharPagtoTituloBradescoSaidaDTO();
		DetalharPagtoTituloBradescoRequest request = new DetalharPagtoTituloBradescoRequest();
		DetalharPagtoTituloBradescoResponse response = new DetalharPagtoTituloBradescoResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoTituloBradescoEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloBradescoEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoTituloBradescoEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoTituloBradescoEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoTituloBradescoEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloBradescoEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloBradescoEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoTituloBradescoEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloBradescoEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloBradescoEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoTituloBradescoEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloBradescoEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoTituloBradescoEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoTituloBradescoEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter()
		.getDetalharPagtoTituloBradescoPDCAdapter().invokeProcess(
				request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdBarras(PgitUtil.formatarLinhaDigitavel(response
				.getCdBarras()));
		saidaDTO.setVlDesconto(response.getVlDesconto());
		saidaDTO.setVlAbatimento(response.getVlAbatimento());
		saidaDTO.setVlMulta(response.getVlMulta());
		saidaDTO.setVlMoraJuros(response.getVlMoraJuros());
		saidaDTO.setVlOutrasDeducoes(response.getVlOutrasDeducoes());
		saidaDTO.setVlOutrosAcrescimos(response.getVlOutrosAcrescimos());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());
		saidaDTO.setNossoNumero(PgitUtil.formatarNossoNumero(response
				.getNossoNumero()));
		saidaDTO.setCdOrigemPagamento(response.getCdOrigemPagamento());
		saidaDTO.setDsOrigemPagamento(response.getDsOrigemPagamento());
		saidaDTO.setDtLimiteDescontoPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtLimiteDescontoPagamento(), "dd.MM.yyyy",
				"dd/MM/yyyy"));
		saidaDTO.setCarteira(response.getCdCartaoTituloCobranca() == 0 ? ""
				: String.valueOf(response.getCdCartaoTituloCobranca()));
		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());
		saidaDTO.setDtDevolucaoEstorno(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtDevolucaoEstorno(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtLimitePagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtLimitePgto(), "dd.MM.yyyy", "dd/MM/yyyy"));

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// SACADOR/AVALISTA
		if (response.getCdCpfCnpjSacadorAvalista() == 0) {
			saidaDTO.setNumeroInscricaoSacadorAvalista("");
		}else{
			saidaDTO.setNumeroInscricaoSacadorAvalista(formatCpfCnpjCompleto(response.getCdCpfCnpjSacadorAvalista(), 
					Long.bitCount(response.getCdFilialCnpjSacadorAvalista()) , response.getCdControleCnpjSacadorAvalista()));
		}
		
		saidaDTO.setDsTipoSacadorAvalista(response.getDsTipoInscricaoSacadorAvalista());
		saidaDTO.setDsSacadorAvalista(response.getDsNomeSacadorAvalista());
		
		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setCdRastreado(response.getCdRastreado());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response
						.getDtFloatingPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivFloatPgto(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());

		saidaDTO.setVlBonificacao(response.getVlBonificacao());
		saidaDTO.setVlBonfcao(response.getVlBonfcao());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());
		saidaDTO.setCdLoteInterno(response.getCdLoteInterno());
		saidaDTO
		.setDsIndicadorAutorizacao(response.getDsIndicadorAutorizacao());
		saidaDTO.setDsTipoLayout(response.getDsTipoLayout());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharPagtoOrdemCredito(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO)
	 */
	public DetalharPagtoOrdemCreditoSaidaDTO detalharPagtoOrdemCredito(
			DetalharPagtoOrdemCreditoEntradaDTO detalharPagtoOrdemCreditoEntradaDTO) {

		DetalharPagtoOrdemCreditoSaidaDTO saidaDTO = new DetalharPagtoOrdemCreditoSaidaDTO();
		DetalharPagtoOrdemCreditoRequest request = new DetalharPagtoOrdemCreditoRequest();
		DetalharPagtoOrdemCreditoResponse response = new DetalharPagtoOrdemCreditoResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(detalharPagtoOrdemCreditoEntradaDTO
						.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemCreditoEntradaDTO
						.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(detalharPagtoOrdemCreditoEntradaDTO
						.getNrSequenciaContratoNegocio()));
		request.setDsEfetivacaoPagamento(PgitUtil
				.verificaStringNula(detalharPagtoOrdemCreditoEntradaDTO
						.getDsEfetivacaoPagamento()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(detalharPagtoOrdemCreditoEntradaDTO
						.getCdControlePagamento()));
		request.setCdBancoDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemCreditoEntradaDTO
						.getCdBancoDebito()));
		request.setCdAgenciaDebito(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemCreditoEntradaDTO
						.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil
				.verificaLongNulo(detalharPagtoOrdemCreditoEntradaDTO
						.getCdContaDebito()));
		request.setCdBancoCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemCreditoEntradaDTO
						.getCdBancoCredito()));
		request.setCdAgenciaCredito(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemCreditoEntradaDTO
						.getCdAgenciaCredito()));
		request.setCdContaCredito(PgitUtil
				.verificaLongNulo(detalharPagtoOrdemCreditoEntradaDTO
						.getCdContaCredito()));
		request.setCdAgendadosPagoNaoPago(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemCreditoEntradaDTO
						.getCdAgendadosPagoNaoPago()));
		request.setCdProdutoServicoRelacionado(PgitUtil
				.verificaIntegerNulo(detalharPagtoOrdemCreditoEntradaDTO
						.getCdProdutoServicoRelacionado()));
		request.setDtHoraManutencao(PgitUtil
				.verificaStringNula(detalharPagtoOrdemCreditoEntradaDTO
						.getDtHoraManutencao()));

		response = getFactoryAdapter().getDetalharPagtoOrdemCreditoPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdPagador(response.getCdPagador());
		saidaDTO.setCdOrdemCreditoPagamento(response
				.getCdOrdemCreditoPagamento() == 0L ? "" : String
						.valueOf(response.getCdOrdemCreditoPagamento()));
		saidaDTO.setVlDescontoCreditoPagamento(response
				.getVlDescontoCreditoPagamento());
		saidaDTO.setVlAbatidoCreditoPagamento(response
				.getVlAbatidoCreditoPagamento());
		saidaDTO.setVlMultaCreditoPagamento(response
				.getVlMultaCreditoPagamento());
		saidaDTO.setVlMoraJurosCreditoPagamento(response
				.getVlMoraJurosCreditoPagamento());
		saidaDTO.setVlOutraDeducaoCreditoPagamento(response
				.getVlOutraDeducaoCreditoPagamento());
		saidaDTO.setVlOutroAcrescimoCreditoPagamento(response
				.getVlOutroAcrescimoCreditoPagamento());
		saidaDTO.setVlIrCreditoPagamento(response.getVlIrCreditoPagamento());
		saidaDTO.setVlIssCreditoPagamento(response.getVlIssCreditoPagamento());
		saidaDTO.setVlIofCreditoPagamento(response.getVlIofCreditoPagamento());
		saidaDTO
		.setVlInssCreditoPagamento(response.getVlInssCreditoPagamento());
		saidaDTO.setCdIndicadorModalidadePgit(response
				.getCdIndicadorModalidadePgit());
		saidaDTO.setDsPagamento(response.getDsPagamento());
		saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
		saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
		saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
		saidaDTO.setDsContrato(response.getDsContrato());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDtAgendamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtAgendamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setVlAgendado(response.getVlAgendado());
		saidaDTO.setVlEfetivo(response.getVlEfetivo());
		saidaDTO.setCdIndicadorEconomicoMoeda(response
				.getCdIndicadorEconomicoMoeda());
		saidaDTO.setQtMoeda(response.getQtMoeda());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsMensagemPrimeiraLinha(response
				.getDsMensagemPrimeiraLinha());
		saidaDTO
		.setDsMensagemSegundaLinha(response.getDsMensagemSegundaLinha());
		saidaDTO.setDsUsoEmpresa(response.getDsUsoEmpresa());
		saidaDTO.setCdSituacaoOperacaoPagamento(response
				.getCdSituacaoOperacaoPagamento());
		saidaDTO.setCdMotivoSituacaoPagamento(response
				.getCdMotivoSituacaoPagamento());
		saidaDTO.setCdListaDebito(response.getCdListaDebito() == 0L ? ""
				: String.valueOf(response.getCdListaDebito()));
		saidaDTO.setCdTipoInscricaoFavorecido(response
				.getCdTipoIsncricaoFavorecido());
		saidaDTO.setNrDocumento(response.getNrDocumento() == 0L ? "" : String
				.valueOf(response.getNrDocumento()));
		saidaDTO.setCdSerieDocumento(response.getCdSerieDocumento());
		saidaDTO.setCdTipoDocumento(response.getCdTipoDocumento());
		saidaDTO.setDsTipoDocumento(response.getDsTipoDocumento());
		saidaDTO.setVlDocumento(response.getVlDocumento());
		saidaDTO.setDtEmissaoDocumento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEmissaoDocumento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setNrSequenciaArquivoRemessa(response
				.getNrSequenciaArquivoRemessa() == 0L ? "" : String
						.valueOf(response.getNrSequenciaArquivoRemessa()));
		saidaDTO
		.setNrLoteArquivoRemessa(response.getNrLoteArquivoRemessa() == 0L ? ""
				: String.valueOf(response.getNrLoteArquivoRemessa()));
		saidaDTO.setCdFavorecido(response.getCdFavorecido() == 0L ? "" : String
				.valueOf(response.getCdFavorecido()));
		saidaDTO.setDsBancoFavorecido(response.getDsBancoFavorecido());
		saidaDTO.setDsAgenciaFavorecido(response.getDsAgenciaFavorecido());
		saidaDTO.setCdTipoContaFavorecido(response.getCdTipoContaFavorecido());
		saidaDTO.setDsTipoContaFavorecido(response.getDsTipoContaFavorecido());
		saidaDTO.setDsMotivoSituacao(response.getDsMotivoSituacao());
		saidaDTO.setCdTipoManutencao(response.getCdTipoManutencao());
		saidaDTO.setDsTipoManutencao(response.getDsTipoManutencao());
		saidaDTO.setDtInclusao(DateUtils.formartarDataPDCparaPadraPtBr(response
				.getDtInclusao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusaoInterno(response
				.getCdUsuarioInclusaoInterno());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setDtManutencao(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtManutencao(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdUsuarioManutencaoInterno(response
				.getCdUsuarioManutencaoInterno());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioManutencaoExterno());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO.setDsMoeda(response.getDsMoeda());

		saidaDTO.setDataHoraInclusaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtInclusao(), saidaDTO.getHrInclusao()));
		saidaDTO.setUsuarioInclusaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioInclusaoInterno(), saidaDTO
						.getCdUsuarioInclusaoExterno()));
		saidaDTO.setTipoCanalInclusaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(), saidaDTO
				.getDsTipoCanalInclusao()));
		saidaDTO
		.setComplementoInclusaoFormatado(saidaDTO.getCdFluxoInclusao() == null
				|| saidaDTO.getCdFluxoInclusao().equals("0") ? ""
						: saidaDTO.getCdFluxoInclusao());
		saidaDTO.setDataHoraManutencaoFormatada(PgitUtil.concatenarCampos(
				saidaDTO.getDtManutencao(), saidaDTO.getHrManutencao()));
		saidaDTO.setUsuarioManutencaoFormatado(PgitUtil
				.verificaUsuarioInternoExterno(saidaDTO
						.getCdUsuarioManutencaoInterno(), saidaDTO
						.getCdUsuarioManutencaoExterno()));
		saidaDTO.setTipoCanalManutencaoFormatado(PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalManutencao(), saidaDTO
				.getDsTipoCanalManutencao()));
		saidaDTO.setComplementoManutencaoFormatado(saidaDTO
				.getCdFluxoManutencao() == null
				|| saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
						.getCdFluxoManutencao());

		// CLIENTE
		saidaDTO
		.setCpfCnpjClienteFormatado(response.getCdCpfCnpjCliente() == 0L ? ""
				: PgitUtil.formatCpfCnpj(String.valueOf(response
						.getCdCpfCnpjCliente())));
		saidaDTO.setCdCpfCnpjCliente(response.getCdCpfCnpjCliente());
		saidaDTO.setNomeCliente(response.getNomeCliente());
		saidaDTO.setDsPessoaJuridicaContrato(response
				.getDsPessoaJuridicaContrato());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio() == 0L ? "" : String
						.valueOf(response.getNrSequenciaContratoNegocio()));

		// CONTA DE D�BITO
		saidaDTO.setCdBancoDebito(response.getCdBancoDebito());
		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setCdDigAgenciaDebto(response.getCdDigAgenciaDebto());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setDsDigAgenciaDebito(response.getDsDigAgenciaDebito());

		saidaDTO.setBancoDebitoFormatado(PgitUtil.formatBanco(response
				.getCdBancoDebito(), response.getDsBancoDebito(), false));
		saidaDTO.setAgenciaDebitoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaDebito(), response.getCdDigAgenciaDebto(),
				response.getDsAgenciaDebito(), false));
		saidaDTO.setContaDebitoFormatada(PgitUtil.formatConta(response
				.getCdContaDebito(), response.getDsDigAgenciaDebito(), false));

		// FAVORECIDO
		saidaDTO
		.setNmInscriFavorecido(response.getNmInscriFavorecido() == 0L ? ""
				: String.valueOf(response.getNmInscriFavorecido()));
		saidaDTO.setDsNomeFavorecido(response.getDsNomeFavorecido());
		saidaDTO.setCdBancoCredito(response.getCdBancoCredito());
		saidaDTO.setCdAgenciaCredito(response.getCdAgenciaCredito());
		saidaDTO.setCdDigAgenciaCredito(response.getCdDigAgenciaCredito());
		saidaDTO.setCdContaCredito(response.getCdContaCredito());
		saidaDTO.setCdDigContaCredito(response.getCdDigContaCredito());
		saidaDTO.setNumeroInscricaoFavorecidoFormatado(PgitUtil
				.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
						saidaDTO.getNmInscriFavorecido()));

		saidaDTO.setBancoCreditoFormatado(PgitUtil.formatBanco(response
				.getCdBancoCredito(), response.getDsBancoFavorecido(), false));
		saidaDTO.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response
				.getCdAgenciaCredito(), response.getCdDigAgenciaCredito(),
				response.getDsAgenciaFavorecido(), false));
		saidaDTO.setContaCreditoFormatada(PgitUtil.formatConta(response
				.getCdContaCredito(), response.getCdDigContaCredito(), false));

		// BENEFICI�RIO
		// saidaDTO.setNmInscriBeneficio(response.get == 0L ? "" :
		// String.valueOf(response.getNmInscriBeneficio()));
		// saidaDTO.setCdTipoInscriBeneficio(response.getCdTipoInscriBeneficio());
		// saidaDTO.setCdNomeBeneficio(response.getCdNomeBeneficio());
		// saidaDTO.setNumeroInscricaoBeneficiarioFormatado(saidaDTO.getNmInscriBeneficio());
			
		
		// PAGAMENTO
		saidaDTO.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDsTipoServicoOperacao(response.getDsTipoServicoOperacao());
		saidaDTO.setDsModalidadeRelacionado(response
				.getDsModalidadeRelacionado());
		saidaDTO.setCdControlePagamento(response.getCdControlePagamento());
		saidaDTO.setCdSituacaoPagamento(response.getCdSituacaoPagamento());
		saidaDTO.setCdIndicadorAuto(response.getCdIndicadorAuto());
		saidaDTO
		.setCdIndicadorSemConsulta(response.getCdIndicadorSemConsulta());
		saidaDTO.setDtFloatingPagamento(DateUtils
				.formartarDataPDCparaPadraPtBr(response.getDtFloatPgto(),
						"dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO
		.setDtEfetivFloatPgto(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtEfetivacaoFloatPgto(), "dd.MM.yyyy",
		"dd/MM/yyyy"));
		saidaDTO.setVlFloatingPagamento(response.getVlFloatingPagamento());
		saidaDTO.setCdOperacaoDcom(response.getCdOperacaoDcom());
		saidaDTO.setDsSituacaoDcom(response.getDsSituacaoDcom());

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#consultarParticipanteContrato(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarParticipanteContratoEntradaDTO)
	 */
	public List<ConsultarParticipanteContratoSaidaDTO> consultarParticipanteContrato(
			ConsultarParticipanteContratoEntradaDTO entrada) {
		ConsultarParticipanteContratoSaidaDTO saida = new ConsultarParticipanteContratoSaidaDTO();
		ConsultarParticipanteContratoRequest request = new ConsultarParticipanteContratoRequest();

		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada
				.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratonegocio(PgitUtil.verificaLongNulo(entrada
				.getNrSequenciaContratonegocio()));
		request.setCdControleCpfParticipante(PgitUtil
				.verificaIntegerNulo(entrada.getCdControleCpfParticipante()));
		request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entrada
				.getCdCpfCnpjParticipante()));
		request.setCdFilialCnpjParticipante(PgitUtil
				.verificaIntegerNulo(entrada.getCdFilialCnpjParticipante()));
		request
		.setNrOcorrencias(IConsultarPagamentosIndividualServiceConstants.NUMERO_OCORRENCIAS_PARTICIPANTE_CONTRATO);

		ConsultarParticipanteContratoResponse response = getFactoryAdapter()
		.getConsultarParticipanteContratoPDCAdapter().invokeProcess(
				request);

		List<ConsultarParticipanteContratoSaidaDTO> listaSaida = new ArrayList<ConsultarParticipanteContratoSaidaDTO>();

		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saida = new ConsultarParticipanteContratoSaidaDTO();
			saida.setCdControleCpfParticipante(response.getOcorrencias(i)
					.getCdControleCpfParticipante());
			saida.setCdCpfCnpjParticipante(response.getOcorrencias(i)
					.getCdCpfCnpjParticipante());
			saida.setCdFilialCnpjParticipante(response.getOcorrencias(i)
					.getCdFilialCnpjParticipante());
			saida.setCdParticipante(response.getOcorrencias(i)
					.getCdParticipante());
			saida.setDsParticipante(response.getOcorrencias(i)
					.getDsParticipante());
			saida.setDsSituacaoParticipacao(response.getOcorrencias(i)
					.getDsSituacaoParticipacao());
			saida.setDsTipoParticipacao(response.getOcorrencias(i)
					.getDsTipoParticipacao());
			listaSaida.add(saida);
		}

		return listaSaida;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#detalharHistConsultaSaldo(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharHistConsultaSaldoEntradaDTO)
	 */
	public DetalharHistConsultaSaldoSaidaDTO detalharHistConsultaSaldo(
			DetalharHistConsultaSaldoEntradaDTO entradaDTO) {
		DetalharHistConsultaSaldoSaidaDTO saidaDTO = new DetalharHistConsultaSaldoSaidaDTO();
		DetalharHistConsultaSaldoRequest request = new DetalharHistConsultaSaldoRequest();
		DetalharHistConsultaSaldoResponse response = new DetalharHistConsultaSaldoResponse();

		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setCdTipoCanal(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoCanal()));
		request.setCdControlePagamento(PgitUtil
				.verificaStringNula(entradaDTO.getCdControlePagamento()));
		request.setCdAgencia(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdAgencia()));
		request.setCdBanco(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdBanco()));
		request.setCdConta(PgitUtil
				.verificaLongNulo(entradaDTO.getCdConta()));
		request.setCdModalidade(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdModalidade()));
		request.setHrConsultasSaldoPagamento(PgitUtil
				.verificaStringNula(entradaDTO.getHrConsultasSaldoPagamento()));

		request.setDtPagamento(PgitUtil.verificaStringNula(
		    entradaDTO.getDtPagamento().replace("/", "."))); 
		
		response = getFactoryAdapter().getDetalharHistConsultaSaldoPDCAdapter()
		.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdCanal(response.getCdCanal());
		saidaDTO.setCdFluxoInclusao(response.getCdFluxoInclusao());
		saidaDTO.setCdFluxoManutencao(response.getCdFluxoManutencao());
		saidaDTO
		.setCdListaDebitoPagamento(response.getCdListaDebitoPagamento() == 0L ? ""
				: String.valueOf(response.getCdListaDebitoPagamento()));
		saidaDTO
		.setCdPessoaLoteRemessa(response.getCdPessoaLoteRemessa() == 0L ? ""
				: String.valueOf(response.getCdPessoaLoteRemessa()));
		saidaDTO.setCdTipoCanal(response.getCdTipoCanal());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setDsAgencia(response.getDsAgencia());
		saidaDTO.setDsBanco(response.getDsBanco());
		saidaDTO.setDsCanal(response.getDsCanal());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setDsTipoConta(response.getDsTipoConta());
		saidaDTO.setDtCreditoPagamento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtCreditoPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setDtInclusao(response.getDtInclusao());
		saidaDTO.setDtManutencao(response.getDtManutencao());
		saidaDTO.setDtVencimento(DateUtils.formartarDataPDCparaPadraPtBr(
				response.getDtVencimento(), "dd.MM.yyyy", "dd/MM/yyyy"));
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setNrArquivoRemessaPagamento(response
				.getNrArquivoRemessaPagamento() == 0L ? "" : String
						.valueOf(response.getNrArquivoRemessaPagamento()));
		saidaDTO.setSinal(response.getDsSinal());
		saidaDTO.setVlPagamento(response.getVlPagamento());
		saidaDTO.setVlSaldoAgregadoCdb(response.getVlSaldoAgregadoCdb());
		saidaDTO.setVlSaldoAgregadoFundo(response.getVlSaldoAgregadoFundo());
		saidaDTO.setVlSaldoAgregadoPoupanca(response
				.getVlSaldoAgregadoPoupanca());
		saidaDTO.setVlSaldoAnteriorLancamento(response
				.getVlSaldoAnteriorLancamento());
		saidaDTO.setVlSaldoComReserva(response.getVlSaldoComReserva());
		saidaDTO
		.setVlSaldoCreditoRotativo(response.getVlSaldoCreditoRotativo());
		saidaDTO.setVlSaldoOperacional(response.getVlSaldoOperacional());
		saidaDTO.setVlSaldoSemReserva(response.getVlSaldoSemReserva());
		saidaDTO.setVlSaldoVinculadoAdministrativo(response
				.getVlSaldoVinculadoAdministrativo());
		saidaDTO.setVlSaldoVinculadoAdministrativoLp(response
				.getVlSaldoVinculadoAdministrativoLp());
		saidaDTO.setVlSaldoVinculadoJudicial(response
				.getVlSaldoVinculadoJudicial());
		saidaDTO.setVlSaldoVinculadoSeguranca(response
				.getVlSaldoVinculadoSeguranca());
		saidaDTO
		.setNumControleInternoLote(response.getNumControleInternoLote());

		return saidaDTO;

	}

    public ConsultarImprimirPgtoIndividualSaidaDTO consultarImprimirPgtoIndividual(
        ConsultarPagamentosIndividuaisEntradaDTO entradaDTO) {

        ConsultarImprimirPgtoIndividualSaidaDTO saida = new ConsultarImprimirPgtoIndividualSaidaDTO();
        ConsultarImprimirPgtoIndividualRequest request = new ConsultarImprimirPgtoIndividualRequest();
        ConsultarImprimirPgtoIndividualResponse response = null;

        request.setCdAgencia(verificaIntegerNulo(entradaDTO.getCdAgencia()));
        request.setCdAgenciaBeneficiario(verificaIntegerNulo(entradaDTO.getCdAgenciaBeneficiario()));
        request.setCdAgendadosPagosNaoPagos(verificaIntegerNulo(entradaDTO.getCdAgendadosPagosNaoPagos()));
        request.setCdBanco(verificaIntegerNulo(entradaDTO.getCdBanco()));
        request.setCdBancoBeneficiario(verificaIntegerNulo(entradaDTO.getCdBancoBeneficiario()));
        request.setCdBarraDocumento(verificaStringNula(entradaDTO.getCdBarraDocumento()));
        request.setCdBeneficio(verificaLongNulo(entradaDTO.getCdBeneficio()));
        request.setCdClassificacao(verificaIntegerNulo(entradaDTO.getCdClassificacao()));
        request.setCdConta(verificaLongNulo(entradaDTO.getCdConta()));
        request.setCdContaBeneficiario(verificaLongNulo(entradaDTO.getCdContaBeneficiario()));
        request.setCdControlePagamentoAte(verificaStringNula(entradaDTO.getCdControlePagamentoAte()));
        request.setCdControlePagamentoDe(verificaStringNula(entradaDTO.getCdControlePagamentoDe()));
        request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
        request.setCdDigitoContaBeneficiario(PgitUtil.DIGITO_CONTA);
        request.setCdEspecieBeneficioInss(verificaIntegerNulo(entradaDTO.getCdEspecieBeneficioInss()));
        request.setCdFavorecidoClientePagador(verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
        request.setCdIdentificacaoInscricaoFavorecido(verificaIntegerNulo(
                entradaDTO.getCdIdentificacaoInscricaoFavorecido()));
        request.setCdInscricaoFavorecido(verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));
        request.setCdMotivoSituacaoPagamento(verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
        request.setCdParticipante(verificaLongNulo(entradaDTO.getCdParticipante()));
        request.setCdPessoaJuridicaContrato(verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdProdutoServicoOperacao(verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
        request.setCdSituacaoOperacaoPagamento(verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setCdTipoPesquisa(verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
        request.setDtCreditoPagamentoFim(verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
        request.setDtCreditoPagamentoInicio(verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
        request.setNrArquivoRemssaPagamento(verificaLongNulo(entradaDTO.getNrArquivoRemssaPagamento()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setNrUnidadeOrganizacional(verificaIntegerNulo(entradaDTO.getNrUnidadeOrganizacional()));
        request.setVlPagamentoAte(verificaBigDecimalNulo(entradaDTO.getVlPagamentoAte()));
        request.setVlPagamentoDe(verificaBigDecimalNulo(entradaDTO.getVlPagamentoDe()));
        request.setCdEmpresaContrato(verificaLongNulo(entradaDTO.getCdEmpresaContrato()));
        request.setCdTipoConta(verificaIntegerNulo(entradaDTO.getCdTipoConta()));
        request.setNrContrato(verificaLongNulo(entradaDTO.getNrContrato()));
        request.setNrOcorrencias(IConsultarPagamentosIndividualServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
        request.setCdCpfCnpjParticipante(verificaLongNulo(entradaDTO.getCdCpfCnpjParticipante()));
        request.setCdFilialCnpjParticipante(verificaIntegerNulo(entradaDTO.getCdFilialCnpjParticipante()));
        request.setCdControleCnpjParticipante(verificaIntegerNulo(entradaDTO.getCdControleCnpjParticipante()));
        request.setCdPessoaContratoDebito(verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
        request.setCdTipoContratoDebito(verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
        request.setNrSequenciaContratoDebito(verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
        request.setCdPessoaContratoCredt(verificaLongNulo(entradaDTO.getCdPessoaContratoCredt()));
        request.setCdTipoContratoCredt(verificaIntegerNulo(entradaDTO.getCdTipoContratoCredt()));
        request.setNrSequenciaContratoCredt(verificaLongNulo(entradaDTO.getNrSequenciaContratoCredt()));
        request.setCdIndiceSimulaPagamento(verificaIntegerNulo(entradaDTO.getCdIndiceSimulaPagamento()));
        request.setCdOrigemRecebidoEfetivacao(verificaIntegerNulo(entradaDTO.getCdOrigemRecebidoEfetivacao()));
        request.setCdTituloPgtoRastreado(verificaIntegerNulo(entradaDTO.getCdTituloPgtoRastreado()));
        request.setCdBancoSalarial(verificaIntegerNulo(entradaDTO.getCdBancoSalarial()));
        request.setCdAgencaSalarial(verificaIntegerNulo(entradaDTO.getCdAgencaSalarial()));
        request.setCdContaSalarial(verificaLongNulo(entradaDTO.getCdContaSalarial()));

        response = getFactoryAdapter().getConsultarImprimirPgtoIndividualPDCAdapter().invokeProcess(request);
        
        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setIndicadorImpressao(response.getIndicadorImpressao());
        saida.setQtdeRegistros(response.getQtdeRegistros());
        
        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService#imprimirPagamentosIndividuais(br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarPagamentosIndividuaisEntradaDTO)
     */
    public List<ConsultarPagamentosIndividuaisSaidaDTO> imprimirPagamentosIndividuais(
        ConsultarPagamentosIndividuaisEntradaDTO entradaDTO) {

        ImprimirPagamentosIndividuaisRequest request = new ImprimirPagamentosIndividuaisRequest();
        ImprimirPagamentosIndividuaisResponse response = null;
        List<ConsultarPagamentosIndividuaisSaidaDTO> listaSaida = new ArrayList<ConsultarPagamentosIndividuaisSaidaDTO>();

        request.setCdAgencia(verificaIntegerNulo(entradaDTO.getCdAgencia()));
        request.setCdAgenciaBeneficiario(verificaIntegerNulo(entradaDTO.getCdAgenciaBeneficiario()));
        request.setCdAgendadosPagosNaoPagos(verificaIntegerNulo(entradaDTO.getCdAgendadosPagosNaoPagos()));
        request.setCdBanco(verificaIntegerNulo(entradaDTO.getCdBanco()));
        request.setCdBancoBeneficiario(verificaIntegerNulo(entradaDTO.getCdBancoBeneficiario()));
        request.setCdBarraDocumento(verificaStringNula(entradaDTO.getCdBarraDocumento()));
        request.setCdBeneficio(verificaLongNulo(entradaDTO.getCdBeneficio()));
        request.setCdClassificacao(verificaIntegerNulo(entradaDTO.getCdClassificacao()));
        request.setCdConta(verificaLongNulo(entradaDTO.getCdConta()));
        request.setCdContaBeneficiario(verificaLongNulo(entradaDTO.getCdContaBeneficiario()));
        request.setCdControlePagamentoAte(verificaStringNula(entradaDTO.getCdControlePagamentoAte()));
        request.setCdControlePagamentoDe(verificaStringNula(entradaDTO.getCdControlePagamentoDe()));
        request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
        request.setCdDigitoContaBeneficiario(PgitUtil.DIGITO_CONTA);
        request.setCdEspecieBeneficioInss(verificaIntegerNulo(entradaDTO.getCdEspecieBeneficioInss()));
        request.setCdFavorecidoClientePagador(verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
        request.setCdIdentificacaoInscricaoFavorecido(verificaIntegerNulo(
                entradaDTO.getCdIdentificacaoInscricaoFavorecido()));
        request.setCdInscricaoFavorecido(verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));
        request.setCdMotivoSituacaoPagamento(verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
        request.setCdParticipante(verificaLongNulo(entradaDTO.getCdParticipante()));
        request.setCdPessoaJuridicaContrato(verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdProdutoServicoOperacao(verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
        request.setCdSituacaoOperacaoPagamento(verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setCdTipoPesquisa(verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
        request.setDtCreditoPagamentoFim(verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
        request.setDtCreditoPagamentoInicio(verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
        request.setNrArquivoRemssaPagamento(verificaLongNulo(entradaDTO.getNrArquivoRemssaPagamento()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setNrUnidadeOrganizacional(verificaIntegerNulo(entradaDTO.getNrUnidadeOrganizacional()));
        request.setVlPagamentoAte(verificaBigDecimalNulo(entradaDTO.getVlPagamentoAte()));
        request.setVlPagamentoDe(verificaBigDecimalNulo(entradaDTO.getVlPagamentoDe()));
        request.setCdEmpresaContrato(verificaLongNulo(entradaDTO.getCdEmpresaContrato()));
        request.setCdTipoConta(verificaIntegerNulo(entradaDTO.getCdTipoConta()));
        request.setNrContrato(verificaLongNulo(entradaDTO.getNrContrato()));
        request.setNrOcorrencias(IConsultarPagamentosIndividualServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
        request.setCdCpfCnpjParticipante(verificaLongNulo(entradaDTO.getCdCpfCnpjParticipante()));
        request.setCdFilialCnpjParticipante(verificaIntegerNulo(entradaDTO.getCdFilialCnpjParticipante()));
        request.setCdControleCnpjParticipante(verificaIntegerNulo(entradaDTO.getCdControleCnpjParticipante()));
        request.setCdPessoaContratoDebito(verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
        request.setCdTipoContratoDebito(verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
        request.setNrSequenciaContratoDebito(verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
        request.setCdPessoaContratoCredt(verificaLongNulo(entradaDTO.getCdPessoaContratoCredt()));
        request.setCdTipoContratoCredt(verificaIntegerNulo(entradaDTO.getCdTipoContratoCredt()));
        request.setNrSequenciaContratoCredt(verificaLongNulo(entradaDTO.getNrSequenciaContratoCredt()));
        request.setCdIndiceSimulaPagamento(verificaIntegerNulo(entradaDTO.getCdIndiceSimulaPagamento()));
        request.setCdOrigemRecebidoEfetivacao(verificaIntegerNulo(entradaDTO.getCdOrigemRecebidoEfetivacao()));
        request.setCdTituloPgtoRastreado(verificaIntegerNulo(entradaDTO.getCdTituloPgtoRastreado()));
        request.setCdBancoSalarial(verificaIntegerNulo(entradaDTO.getCdBancoSalarial()));
        request.setCdAgencaSalarial(verificaIntegerNulo(entradaDTO.getCdAgencaSalarial()));
        request.setCdContaSalarial(verificaLongNulo(entradaDTO.getCdContaSalarial()));
		if("".equals(PgitUtil.verificaStringNula(entradaDTO.getCdIspbPagtoDestino()))){
			request.setCdIspbPagtoDestino("");
		}else{
			request.setCdIspbPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getCdIspbPagtoDestino(), 8));
		}
		request.setContaPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getContaPagtoDestino(), 20));
		
		response = getFactoryAdapter().getImprimirPagamentosIndividuaisPDCAdapter().invokeProcess(request);

        ConsultarPagamentosIndividuaisSaidaDTO saidaDTO;

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {

            saidaDTO = new ConsultarPagamentosIndividuaisSaidaDTO();
            saidaDTO.setCodMensagem(response.getCodMensagem());
            saidaDTO.setMensagem(response.getMensagem());
            saidaDTO.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
            saidaDTO.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
            saidaDTO.setNrDigitoCnpjCpf(response.getOcorrencias(i).getNrDigitoCnpjCpf());
            saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
            saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
            saidaDTO.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
            saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
            saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
            saidaDTO.setNrContrato(response.getOcorrencias(i).getNrContrato());
            saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
            saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
            saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
            saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
            saidaDTO.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
            saidaDTO.setDtCreditoPagamento(response.getOcorrencias(i).getDtCreditoPagamento() != null
                    && !response.getOcorrencias(i).getDtCreditoPagamento().equals("") ? formatarData(response
                            .getOcorrencias(i).getDtCreditoPagamento()) : "");
            saidaDTO.setVlEfetivoPagamento(response.getOcorrencias(i).getVlEfetivoPagamento());
            saidaDTO.setVlEfetivoPagamentoFormatado(NumberUtils.format(
                    response.getOcorrencias(i).getVlEfetivoPagamento(), "#,##0.00"));
            saidaDTO.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido() == 0L ? "" : String
                    .valueOf(response.getOcorrencias(i).getCdInscricaoFavorecido()));
            saidaDTO.setDsFavorecido(response.getOcorrencias(i).getDsFavorecido());
            saidaDTO.setFavorecidoBeneficiarioFormatado(PgitUtil.concatenarCampos(saidaDTO.getCdInscricaoFavorecido(),
                    saidaDTO.getDsFavorecido(), "-"));
            saidaDTO.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
            saidaDTO.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
            saidaDTO.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
            saidaDTO.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
            saidaDTO.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
            saidaDTO.setCdBancoCredito(response.getOcorrencias(i).getCdBancoCredito());
            saidaDTO.setCdAgenciaCredito(response.getOcorrencias(i).getCdAgenciaCredito());
            saidaDTO.setCdDigitoAgenciaCredito(response.getOcorrencias(i).getCdDigitoAgenciaCredito());
            saidaDTO.setCdContaCredito(response.getOcorrencias(i).getCdContaCredito());
            saidaDTO.setCdDigitoContaCredito(response.getOcorrencias(i).getCdDigitoContaCredito());
            saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
            saidaDTO.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
            saidaDTO.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
            saidaDTO.setDsMotivoSituacaoPagamento(response.getOcorrencias(i).getDsMotivoSituacaoPagamento());
            saidaDTO.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
            saidaDTO.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
            saidaDTO.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
            saidaDTO.setDsIndicadorPagamento(response.getOcorrencias(i)
                    .getDsIndicadorPagamento());
            saidaDTO.setAgenciaCreditoFormatada(formatAgencia(response.getOcorrencias(i).getCdAgenciaCredito(), 
                    response.getOcorrencias(i).getCdDigitoAgenciaCredito(), false));
            saidaDTO.setContaCreditoFormatada(formatConta(response.getOcorrencias(i).getCdContaCredito(), 
                    response.getOcorrencias(i).getCdDigitoContaCredito(), false));
            saidaDTO.setCdCpfCnpjFormatado(formatCpfCnpjCompleto(response.getOcorrencias(i).getNrCnpjCpf(), 
                    response.getOcorrencias(i).getNrFilialCnpjCpf(), response.getOcorrencias(i).getNrDigitoCnpjCpf()));
            String cdContaPagamentoDestino = response.getOcorrencias(i).getContaPagtoDestino();
            if(cdContaPagamentoDestino != null && !"".equals(cdContaPagamentoDestino) && !"0".equals(cdContaPagamentoDestino)){
				saidaDTO.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoIspbConta(saidaDTO.getCdBancoCredito(), 
						response.getOcorrencias(i).getCdIspbPagtoDestino(), cdContaPagamentoDestino, true));
			}else{
				saidaDTO.setBancoAgenciaContaCreditoFormatado(formatBancoAgenciaConta(saidaDTO.getCdBancoCredito(),
						saidaDTO.getCdAgenciaCredito(), saidaDTO.getCdDigitoAgenciaCredito(), saidaDTO
						.getCdContaCredito(), saidaDTO.getCdDigitoContaCredito(), true));			
			}
            saidaDTO.setBancoAgenciaContaDebitoFormatado(formatBancoAgenciaConta(saidaDTO.getCdBancoDebito(),
                    saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigitoAgenciaDebito(), saidaDTO
                    .getCdContaDebito(), saidaDTO.getCdDigitoContaDebito(), true));
            saidaDTO.setAgenciaDebitoFormatada(formatAgencia(saidaDTO
                    .getCdAgenciaDebito(), saidaDTO.getCdDigitoAgenciaDebito(),false));
            saidaDTO.setContaDebitoFormatada(formatConta(saidaDTO
                    .getCdContaDebito(), saidaDTO.getCdDigitoContaDebito(),false));
            saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
            saidaDTO.setCdAgenciaSalario(response.getOcorrencias(i).getCdAgenciaSalario());
            saidaDTO.setCdDigitoAgenciaSalarial(response.getOcorrencias(i).getCdDigitoAgenciaSalarial());
            saidaDTO.setCdContaSalarial(response.getOcorrencias(i).getCdContaSalarial());
            saidaDTO.setCdDigitoContaSalarial(response.getOcorrencias(i).getCdDigitoContaSalarial());
            saidaDTO.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
    		saidaDTO.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
    			
            listaSaida.add(saidaDTO);
        }
        
        return listaSaida;
    }

}