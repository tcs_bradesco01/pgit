/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean;

/**
 * Nome: ConsultarListaPendenciasContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaPendenciasContratoEntradaDTO {
	
	

	/** Atributo codPessoaJuridicaContrato. */
	private int codPessoaJuridicaContrato;
	
	/** Atributo codTipoContratoNegocio. */
	private int codTipoContratoNegocio;
	
	/** Atributo numSequenciaContratoNegocio. */
	private Long numSequenciaContratoNegocio;
	
	/** Atributo codPendenciaPagamentoIntegrado. */
	private int codPendenciaPagamentoIntegrado;
	
	/** Atributo codSituacaoPendenciaContrato. */
	private int codSituacaoPendenciaContrato;
	
	/** Atributo codPessoaJuridica. */
	private long codPessoaJuridica;
	
	/** Atributo numSequenciaUnidadeOrg. */
	private int numSequenciaUnidadeOrg;
	
	/** Atributo codTipoBaixa. */
	private int codTipoBaixa;
	
	/** Atributo dataInicioBaixa. */
	private String dataInicioBaixa;
	
	/** Atributo dataFimBaixa. */
	private String dataFimBaixa;
	
	/** Atributo codAcesso. */
	private int codAcesso;
	
	/** Atributo cdTipoUnidade. */
	private int cdTipoUnidade;
	 
	
	
	/**
	 * Get: codPendenciaPagamentoIntegrado.
	 *
	 * @return codPendenciaPagamentoIntegrado
	 */
	public int getCodPendenciaPagamentoIntegrado() {
		return codPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Set: codPendenciaPagamentoIntegrado.
	 *
	 * @param codPendenciaPagamentoIntegrado the cod pendencia pagamento integrado
	 */
	public void setCodPendenciaPagamentoIntegrado(int codPendenciaPagamentoIntegrado) {
		this.codPendenciaPagamentoIntegrado = codPendenciaPagamentoIntegrado;
	}
	
	/**
	 * Get: codPessoaJuridica.
	 *
	 * @return codPessoaJuridica
	 */
	public long getCodPessoaJuridica() {
		return codPessoaJuridica;
	}
	
	/**
	 * Set: codPessoaJuridica.
	 *
	 * @param codPessoaJuridica the cod pessoa juridica
	 */
	public void setCodPessoaJuridica(long codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}
	
	/**
	 * Get: codPessoaJuridicaContrato.
	 *
	 * @return codPessoaJuridicaContrato
	 */
	public int getCodPessoaJuridicaContrato() {
		return codPessoaJuridicaContrato;
	}
	
	/**
	 * Set: codPessoaJuridicaContrato.
	 *
	 * @param codPessoaJuridicaContrato the cod pessoa juridica contrato
	 */
	public void setCodPessoaJuridicaContrato(int codPessoaJuridicaContrato) {
		this.codPessoaJuridicaContrato = codPessoaJuridicaContrato;
	}
	
	/**
	 * Get: codSituacaoPendenciaContrato.
	 *
	 * @return codSituacaoPendenciaContrato
	 */
	public int getCodSituacaoPendenciaContrato() {
		return codSituacaoPendenciaContrato;
	}
	
	/**
	 * Set: codSituacaoPendenciaContrato.
	 *
	 * @param codSituacaoPendenciaContrato the cod situacao pendencia contrato
	 */
	public void setCodSituacaoPendenciaContrato(int codSituacaoPendenciaContrato) {
		this.codSituacaoPendenciaContrato = codSituacaoPendenciaContrato;
	}
	
	/**
	 * Get: codTipoBaixa.
	 *
	 * @return codTipoBaixa
	 */
	public int getCodTipoBaixa() {
		return codTipoBaixa;
	}
	
	/**
	 * Set: codTipoBaixa.
	 *
	 * @param codTipoBaixa the cod tipo baixa
	 */
	public void setCodTipoBaixa(int codTipoBaixa) {
		this.codTipoBaixa = codTipoBaixa;
	}
	
	/**
	 * Get: codTipoContratoNegocio.
	 *
	 * @return codTipoContratoNegocio
	 */
	public int getCodTipoContratoNegocio() {
		return codTipoContratoNegocio;
	}
	
	/**
	 * Set: codTipoContratoNegocio.
	 *
	 * @param codTipoContratoNegocio the cod tipo contrato negocio
	 */
	public void setCodTipoContratoNegocio(int codTipoContratoNegocio) {
		this.codTipoContratoNegocio = codTipoContratoNegocio;
	}
	
	/**
	 * Get: dataFimBaixa.
	 *
	 * @return dataFimBaixa
	 */
	public String getDataFimBaixa() {
		return dataFimBaixa;
	}
	
	/**
	 * Get: codAcesso.
	 *
	 * @return codAcesso
	 */
	public int getCodAcesso() {
		return codAcesso;
	}
	
	/**
	 * Set: codAcesso.
	 *
	 * @param codAcesso the cod acesso
	 */
	public void setCodAcesso(int codAcesso) {
		this.codAcesso = codAcesso;
	}
	
	/**
	 * Set: dataFimBaixa.
	 *
	 * @param dataFimBaixa the data fim baixa
	 */
	public void setDataFimBaixa(String dataFimBaixa) {
		this.dataFimBaixa = dataFimBaixa;
	}
	
	/**
	 * Get: dataInicioBaixa.
	 *
	 * @return dataInicioBaixa
	 */
	public String getDataInicioBaixa() {
		return dataInicioBaixa;
	}
	
	/**
	 * Set: dataInicioBaixa.
	 *
	 * @param dataInicioBaixa the data inicio baixa
	 */
	public void setDataInicioBaixa(String dataInicioBaixa) {
		this.dataInicioBaixa = dataInicioBaixa;
	}
	
	/**
	 * Get: numSequenciaContratoNegocio.
	 *
	 * @return numSequenciaContratoNegocio
	 */
	public Long getNumSequenciaContratoNegocio() {
		return numSequenciaContratoNegocio;
	}
	
	/**
	 * Set: numSequenciaContratoNegocio.
	 *
	 * @param numSequenciaContratoNegocio the num sequencia contrato negocio
	 */
	public void setNumSequenciaContratoNegocio(Long numSequenciaContratoNegocio) {
		this.numSequenciaContratoNegocio = numSequenciaContratoNegocio;
	}
	
	/**
	 * Get: numSequenciaUnidadeOrg.
	 *
	 * @return numSequenciaUnidadeOrg
	 */
	public int getNumSequenciaUnidadeOrg() {
		return numSequenciaUnidadeOrg;
	}
	
	/**
	 * Set: numSequenciaUnidadeOrg.
	 *
	 * @param numSequenciaUnidadeOrg the num sequencia unidade org
	 */
	public void setNumSequenciaUnidadeOrg(int numSequenciaUnidadeOrg) {
		this.numSequenciaUnidadeOrg = numSequenciaUnidadeOrg;
	}
	
	/**
	 * Get: cdTipoUnidade.
	 *
	 * @return cdTipoUnidade
	 */
	public int getCdTipoUnidade() {
		return cdTipoUnidade;
	}
	
	/**
	 * Set: cdTipoUnidade.
	 *
	 * @param cdTipoUnidade the cd tipo unidade
	 */
	public void setCdTipoUnidade(int cdTipoUnidade) {
		this.cdTipoUnidade = cdTipoUnidade;
	}
	
	
	
	



}
