/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.impl;

import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.IBloquearDesbloquearContratosService;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.bean.BloquearDesbloquearContratosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.bean.BloquearDesbloquearContratosSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontrato.request.BloquearDesbloquearContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearcontrato.response.BloquearDesbloquearContratoResponse;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: BloquearDesbloquearContratos
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class BloquearDesbloquearContratosServiceImpl implements IBloquearDesbloquearContratosService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;    

	/**
     * Construtor.
     */
    public BloquearDesbloquearContratosServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * M�todo de exemplo.
     *
     * @see br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.IBloquearDesbloquearContratos#sampleBloquearDesbloquearContratos()
     */
    public void sampleBloquearDesbloquearContratos() {
        // TODO: Implementa�ao
    }
    
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.IBloquearDesbloquearContratosService#bloquearDesbloquearContratos(br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.bean.BloquearDesbloquearContratosEntradaDTO)
	 */
	public BloquearDesbloquearContratosSaidaDTO bloquearDesbloquearContratos(BloquearDesbloquearContratosEntradaDTO entradaDTO){
		BloquearDesbloquearContratosSaidaDTO bloquearDesbloquearContratosSaidaDTO = new BloquearDesbloquearContratosSaidaDTO();
		BloquearDesbloquearContratoRequest bloquearDesbloquearContratosRequest = new BloquearDesbloquearContratoRequest();
		BloquearDesbloquearContratoResponse bloquearDesbloquearContratosResponse = new BloquearDesbloquearContratoResponse();
		
		bloquearDesbloquearContratosRequest.setCdMotivoBloqueioContrato(entradaDTO.getCdMotivoBloqueioContrato());
		bloquearDesbloquearContratosRequest.setCdPessoaJuridica(entradaDTO.getCdPessoaJuridica());
		bloquearDesbloquearContratosRequest.setCdSituacaoContrato(entradaDTO.getCdSituacaoContrato());
		bloquearDesbloquearContratosRequest.setCdTipoContratoNegocio(entradaDTO.getCdTipoContratoNegocio());
		bloquearDesbloquearContratosRequest.setIndicadorBloqueioDesbloqueioContrato(entradaDTO.getIndicadorBloqueioDesbloqueioContrato());
		bloquearDesbloquearContratosRequest.setNrSequenciaContratoNegocio(entradaDTO.getNrSequenciaContratoNegocio());
		
		bloquearDesbloquearContratosResponse = getFactoryAdapter().getBloquearDesbloquearContratoPDCAdapter().invokeProcess(bloquearDesbloquearContratosRequest);

		bloquearDesbloquearContratosSaidaDTO.setCodMensagem(bloquearDesbloquearContratosResponse.getCodMensagem());
		bloquearDesbloquearContratosSaidaDTO.setMensagem(bloquearDesbloquearContratosResponse.getMensagem());
		
		return bloquearDesbloquearContratosSaidaDTO;
	}
}

