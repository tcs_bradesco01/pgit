/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean;

/**
 * Nome: ExcluirServicoLancamentoCnabEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirServicoLancamentoCnabEntradaDTO{
	
	/** Atributo cdFormaLancamentoCnab. */
	private Integer cdFormaLancamentoCnab;
	
	/** Atributo cdTipoServicoCnab. */
	private Integer cdTipoServicoCnab;


	/**
	 * Get: cdFormaLancamentoCnab.
	 *
	 * @return cdFormaLancamentoCnab
	 */
	public Integer getCdFormaLancamentoCnab(){
		return cdFormaLancamentoCnab;
	}

	/**
	 * Set: cdFormaLancamentoCnab.
	 *
	 * @param cdFormaLancamentoCnab the cd forma lancamento cnab
	 */
	public void setCdFormaLancamentoCnab(Integer cdFormaLancamentoCnab){
		this.cdFormaLancamentoCnab = cdFormaLancamentoCnab;
	}

	/**
	 * Get: cdTipoServicoCnab.
	 *
	 * @return cdTipoServicoCnab
	 */
	public Integer getCdTipoServicoCnab(){
		return cdTipoServicoCnab;
	}

	/**
	 * Set: cdTipoServicoCnab.
	 *
	 * @param cdTipoServicoCnab the cd tipo servico cnab
	 */
	public void setCdTipoServicoCnab(Integer cdTipoServicoCnab){
		this.cdTipoServicoCnab = cdTipoServicoCnab;
	}

}