package br.com.bradesco.web.pgit.service.business.parametrocontrato;

import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ExcluirParametroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ExcluirParametroContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.IncluirParametroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.IncluirParametroContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ListarParametroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ListarParametroContratoSaidaDTO;


// TODO: Auto-generated Javadoc
/**
 * The Interface IParametroContratoService.
 */
public interface IParametroContratoService {

	/**
	 * Excluir parametro contrato.
	 *
	 * @param entrada the entrada
	 * @return the excluir parametro contrato saida dto
	 */
	ExcluirParametroContratoSaidaDTO excluirParametroContrato (ExcluirParametroContratoEntradaDTO entrada); 
	
	/**
	 * Incluir parametro contrato.
	 *
	 * @param entrada the entrada
	 * @return the incluir parametro contrato saida dto
	 */
	IncluirParametroContratoSaidaDTO incluirParametroContrato (IncluirParametroContratoEntradaDTO entrada);
	
	/**
	 * Listar parametro contrato.
	 *
	 * @param entrada the entrada
	 * @return the listar parametro contrato saida dto
	 */
	ListarParametroContratoSaidaDTO listarParametroContrato (ListarParametroContratoEntradaDTO entrada); 
   
}

