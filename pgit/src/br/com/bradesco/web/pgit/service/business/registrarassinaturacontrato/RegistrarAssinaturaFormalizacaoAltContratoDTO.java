/*
 * Nome: br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

/**
 * Nome: RegistrarAssinaturaFormalizacaoAltContratoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RegistrarAssinaturaFormalizacaoAltContratoDTO {
	
//	atributos dados
	/** Atributo cnpjCpfMaster. */
private String cnpjCpfMaster;
	
	/** Atributo nomeRazaoSocialMaster. */
	private String nomeRazaoSocialMaster;
	
	/** Atributo grupoEconomico. */
	private String grupoEconomico;
	
	/** Atributo segmento. */
	private String segmento;
	
	/** Atributo subSegmento. */
	private String subSegmento;
	
	/** Atributo atividadeEconomico. */
	private String atividadeEconomico;
	
	/** Atributo cnpjCpf. */
	private String cnpjCpf;
	
	/** Atributo empresa. */
	private String empresa;
	
	/** Atributo situacao. */
	private String situacao;
	
	/** Atributo tipo. */
	private String tipo;
	
	/** Atributo numero. */
	private String numero;
	
	/** Atributo motivo. */
	private String motivo;
	
	/** Atributo participacao. */
	private String participacao;
	
	/** Atributo possuiAditivos. */
	private String possuiAditivos;
	
	/** Atributo dataHoraCad. */
	private String dataHoraCad;
	
	/** Atributo inicioVigencia. */
	private String inicioVigencia;
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;
	
	//atributo date
	/** Atributo dataDeFiltro. */
	private Date dataDeFiltro;
	
	//Lista do tipo ArrayList
	
	/** Atributo listaMotivoAtual. */
	private List<SelectItem> listaMotivoAtual = new ArrayList<SelectItem>();
	//nao esquecer de preencher a Lista Selecione, Motivo 1 2 e3

	/**
	 * Get: atividadeEconomico.
	 *
	 * @return atividadeEconomico
	 */
	public String getAtividadeEconomico() {
		return atividadeEconomico;
	}

	/**
	 * Set: atividadeEconomico.
	 *
	 * @param atividadeEconomico the atividade economico
	 */
	public void setAtividadeEconomico(String atividadeEconomico) {
		this.atividadeEconomico = atividadeEconomico;
	}

	/**
	 * Get: cnpjCpf.
	 *
	 * @return cnpjCpf
	 */
	public String getCnpjCpf() {
		return cnpjCpf;
	}

	/**
	 * Set: cnpjCpf.
	 *
	 * @param cnpjCpf the cnpj cpf
	 */
	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}

	/**
	 * Get: cnpjCpfMaster.
	 *
	 * @return cnpjCpfMaster
	 */
	public String getCnpjCpfMaster() {
		return cnpjCpfMaster;
	}

	/**
	 * Set: cnpjCpfMaster.
	 *
	 * @param cnpjCpfMaster the cnpj cpf master
	 */
	public void setCnpjCpfMaster(String cnpjCpfMaster) {
		this.cnpjCpfMaster = cnpjCpfMaster;
	}

	/**
	 * Get: dataDeFiltro.
	 *
	 * @return dataDeFiltro
	 */
	public Date getDataDeFiltro() {
		return dataDeFiltro;
	}

	/**
	 * Set: dataDeFiltro.
	 *
	 * @param dataDeFiltro the data de filtro
	 */
	public void setDataDeFiltro(Date dataDeFiltro) {
		this.dataDeFiltro = dataDeFiltro;
	}

	/**
	 * Get: dataHoraCad.
	 *
	 * @return dataHoraCad
	 */
	public String getDataHoraCad() {
		return dataHoraCad;
	}

	/**
	 * Set: dataHoraCad.
	 *
	 * @param dataHoraCad the data hora cad
	 */
	public void setDataHoraCad(String dataHoraCad) {
		this.dataHoraCad = dataHoraCad;
	}

	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Get: grupoEconomico.
	 *
	 * @return grupoEconomico
	 */
	public String getGrupoEconomico() {
		return grupoEconomico;
	}

	/**
	 * Set: grupoEconomico.
	 *
	 * @param grupoEconomico the grupo economico
	 */
	public void setGrupoEconomico(String grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}

	/**
	 * Get: inicioVigencia.
	 *
	 * @return inicioVigencia
	 */
	public String getInicioVigencia() {
		return inicioVigencia;
	}

	/**
	 * Set: inicioVigencia.
	 *
	 * @param inicioVigencia the inicio vigencia
	 */
	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	/**
	 * Get: listaMotivoAtual.
	 *
	 * @return listaMotivoAtual
	 */
	public List<SelectItem> getListaMotivoAtual() {
		return listaMotivoAtual;
	}

	/**
	 * Set: listaMotivoAtual.
	 *
	 * @param listaMotivoAtual the lista motivo atual
	 */
	public void setListaMotivoAtual(List<SelectItem> listaMotivoAtual) {
		this.listaMotivoAtual = listaMotivoAtual;
	}

	/**
	 * Get: motivo.
	 *
	 * @return motivo
	 */
	public String getMotivo() {
		return motivo;
	}

	/**
	 * Set: motivo.
	 *
	 * @param motivo the motivo
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Get: nomeRazaoSocialMaster.
	 *
	 * @return nomeRazaoSocialMaster
	 */
	public String getNomeRazaoSocialMaster() {
		return nomeRazaoSocialMaster;
	}

	/**
	 * Set: nomeRazaoSocialMaster.
	 *
	 * @param nomeRazaoSocialMaster the nome razao social master
	 */
	public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
		this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
	}

	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Get: participacao.
	 *
	 * @return participacao
	 */
	public String getParticipacao() {
		return participacao;
	}

	/**
	 * Set: participacao.
	 *
	 * @param participacao the participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	/**
	 * Get: possuiAditivos.
	 *
	 * @return possuiAditivos
	 */
	public String getPossuiAditivos() {
		return possuiAditivos;
	}

	/**
	 * Set: possuiAditivos.
	 *
	 * @param possuiAditivos the possui aditivos
	 */
	public void setPossuiAditivos(String possuiAditivos) {
		this.possuiAditivos = possuiAditivos;
	}

	/**
	 * Get: segmento.
	 *
	 * @return segmento
	 */
	public String getSegmento() {
		return segmento;
	}

	/**
	 * Set: segmento.
	 *
	 * @param segmento the segmento
	 */
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}

	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	/**
	 * Get: subSegmento.
	 *
	 * @return subSegmento
	 */
	public String getSubSegmento() {
		return subSegmento;
	}

	/**
	 * Set: subSegmento.
	 *
	 * @param subSegmento the sub segmento
	 */
	public void setSubSegmento(String subSegmento) {
		this.subSegmento = subSegmento;
	}

	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	

}
