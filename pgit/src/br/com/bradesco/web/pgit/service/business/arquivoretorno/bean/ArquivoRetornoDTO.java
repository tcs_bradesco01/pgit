/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivoretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivoretorno.bean;

/**
 * Nome: ArquivoRetornoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ArquivoRetornoDTO {
	
	/** Atributo cpf_cliente. */
	private String cpf_cliente;
	
	/** Atributo cpf_filial. */
	private String cpf_filial;
	
	/** Atributo cpf_controle. */
	private String cpf_controle;
	
	/** Atributo cpf_completo. */
	private String cpf_completo;
	
	/** Atributo nome. */
	private String nome;
	
	/** Atributo perfil. */
	private String perfil;
	
	/** Atributo produto. */
	private String produto;
	
	/** Atributo bancoDebito. */
	private String bancoDebito;
	
	/** Atributo agenciaDebito. */
	private String agenciaDebito;
	
	/** Atributo razaoContaDebito. */
	private String razaoContaDebito;
	
	/** Atributo contaBancaria. */
	private String contaBancaria;
	
	/** Atributo lancamentoContaDebito. */
	private String lancamentoContaDebito;
	
	/** Atributo meioTransmissaoArquivo. */
	private String meioTransmissaoArquivo;
	
	/** Atributo numeroArquivoRetorno. */
	private String numeroArquivoRetorno;
	
	/** Atributo horaGeracaoRetorno. */
	private String horaGeracaoRetorno;
	
	/** Atributo tipoRetorno. */
	private String tipoRetorno;
	
	/** Atributo situacaoRetorno. */
	private String situacaoRetorno;
	
	/** Atributo tipoLayout. */
	private String tipoLayout;	
	
	/** Atributo codigoAmbiente. */
	private String codigoAmbiente;
	
	/** Atributo qtdeOcorrencias. */
	private String qtdeOcorrencias;	
	
	/** Atributo valorRegistroArquivoRetorno. */
	private String valorRegistroArquivoRetorno;
	
	/** Atributo descricaoTipoArquivo. */
	private String descricaoTipoArquivo;
	
	/** Atributo descricaoSituacao. */
	private String descricaoSituacao;
	
	/** Atributo horaRecepcaoInicio. */
	private String horaRecepcaoInicio;
	
	/** Atributo horaRecepcaoFim. */
	private String horaRecepcaoFim;
	
	/** Atributo codigoSistemaLegado. */
	private String codigoSistemaLegado;
    
    /** Atributo tipoArquivoRetorno. */
    private String tipoArquivoRetorno;
    
    /** Atributo situacaoGeracaoRetorno. */
    private String situacaoGeracaoRetorno;
    
    /** Atributo horaInclusao. */
    private String horaInclusao;
    
    /** Atributo numeroArquivoRetornoLong. */
    private Long numeroArquivoRetornoLong;
    
    /** Atributo totalRegistros. */
    private int totalRegistros;
    
	/**
	 * Get: totalRegistros.
	 *
	 * @return totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}
	
	/**
	 * Set: totalRegistros.
	 *
	 * @param totalRegistros the total registros
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	
	/**
	 * Get: agenciaDebito.
	 *
	 * @return agenciaDebito
	 */
	public String getAgenciaDebito() {
		return agenciaDebito;
	}
	
	/**
	 * Set: agenciaDebito.
	 *
	 * @param agenciaDebito the agencia debito
	 */
	public void setAgenciaDebito(String agenciaDebito) {
		this.agenciaDebito = agenciaDebito;
	}
	
	/**
	 * Get: bancoDebito.
	 *
	 * @return bancoDebito
	 */
	public String getBancoDebito() {
		return bancoDebito;
	}
	
	/**
	 * Set: bancoDebito.
	 *
	 * @param bancoDebito the banco debito
	 */
	public void setBancoDebito(String bancoDebito) {
		this.bancoDebito = bancoDebito;
	}
	
	/**
	 * Get: codigoAmbiente.
	 *
	 * @return codigoAmbiente
	 */
	public String getCodigoAmbiente() {
		return codigoAmbiente;
	}
	
	/**
	 * Set: codigoAmbiente.
	 *
	 * @param codigoAmbiente the codigo ambiente
	 */
	public void setCodigoAmbiente(String codigoAmbiente) {
		this.codigoAmbiente = codigoAmbiente;
	}
	
	/**
	 * Get: codigoSistemaLegado.
	 *
	 * @return codigoSistemaLegado
	 */
	public String getCodigoSistemaLegado() {
		return codigoSistemaLegado;
	}
	
	/**
	 * Set: codigoSistemaLegado.
	 *
	 * @param codigoSistemaLegado the codigo sistema legado
	 */
	public void setCodigoSistemaLegado(String codigoSistemaLegado) {
		this.codigoSistemaLegado = codigoSistemaLegado;
	}
	
	/**
	 * Get: contaBancaria.
	 *
	 * @return contaBancaria
	 */
	public String getContaBancaria() {
		return contaBancaria;
	}
	
	/**
	 * Set: contaBancaria.
	 *
	 * @param contaBancaria the conta bancaria
	 */
	public void setContaBancaria(String contaBancaria) {
		this.contaBancaria = contaBancaria;
	}
	
	/**
	 * Get: cpf_cliente.
	 *
	 * @return cpf_cliente
	 */
	public String getCpf_cliente() {
		return cpf_cliente;
	}
	
	/**
	 * Set: cpf_cliente.
	 *
	 * @param cpfCliente the cpf_cliente
	 */
	public void setCpf_cliente(String cpfCliente) {
		this.cpf_cliente = cpfCliente;
	}
	
	/**
	 * Get: cpf_completo.
	 *
	 * @return cpf_completo
	 */
	public String getCpf_completo() {
		return cpf_completo;
	}
	
	/**
	 * Set: cpf_completo.
	 *
	 * @param cpfCompleto the cpf_completo
	 */
	public void setCpf_completo(String cpfCompleto) {
		this.cpf_completo = cpfCompleto;
	}
	
	/**
	 * Get: cpf_controle.
	 *
	 * @return cpf_controle
	 */
	public String getCpf_controle() {
		return cpf_controle;
	}
	
	/**
	 * Set: cpf_controle.
	 *
	 * @param cpfControle the cpf_controle
	 */
	public void setCpf_controle(String cpfControle) {
		this.cpf_controle = cpfControle;
	}
	
	/**
	 * Get: cpf_filial.
	 *
	 * @return cpf_filial
	 */
	public String getCpf_filial() {
		return cpf_filial;
	}
	
	/**
	 * Set: cpf_filial.
	 *
	 * @param cpfFilial the cpf_filial
	 */
	public void setCpf_filial(String cpfFilial) {
		this.cpf_filial = cpfFilial;
	}
	
	/**
	 * Get: horaGeracaoRetorno.
	 *
	 * @return horaGeracaoRetorno
	 */
	public String getHoraGeracaoRetorno() {
		return horaGeracaoRetorno;
	}
	
	/**
	 * Set: horaGeracaoRetorno.
	 *
	 * @param horaGeracaoRetorno the hora geracao retorno
	 */
	public void setHoraGeracaoRetorno(String horaGeracaoRetorno) {
		this.horaGeracaoRetorno = horaGeracaoRetorno;
	}
	
	/**
	 * Get: horaRecepcaoFim.
	 *
	 * @return horaRecepcaoFim
	 */
	public String getHoraRecepcaoFim() {
		return horaRecepcaoFim;
	}
	
	/**
	 * Set: horaRecepcaoFim.
	 *
	 * @param horaRecepcaoFim the hora recepcao fim
	 */
	public void setHoraRecepcaoFim(String horaRecepcaoFim) {
		this.horaRecepcaoFim = horaRecepcaoFim;
	}
	
	/**
	 * Get: horaRecepcaoInicio.
	 *
	 * @return horaRecepcaoInicio
	 */
	public String getHoraRecepcaoInicio() {
		return horaRecepcaoInicio;
	}
	
	/**
	 * Set: horaRecepcaoInicio.
	 *
	 * @param horaRecepcaoInicio the hora recepcao inicio
	 */
	public void setHoraRecepcaoInicio(String horaRecepcaoInicio) {
		this.horaRecepcaoInicio = horaRecepcaoInicio;
	}
	
	/**
	 * Get: lancamentoContaDebito.
	 *
	 * @return lancamentoContaDebito
	 */
	public String getLancamentoContaDebito() {
		return lancamentoContaDebito;
	}
	
	/**
	 * Set: lancamentoContaDebito.
	 *
	 * @param lancamentoContaDebito the lancamento conta debito
	 */
	public void setLancamentoContaDebito(String lancamentoContaDebito) {
		this.lancamentoContaDebito = lancamentoContaDebito;
	}
	
	/**
	 * Get: meioTransmissaoArquivo.
	 *
	 * @return meioTransmissaoArquivo
	 */
	public String getMeioTransmissaoArquivo() {
		return meioTransmissaoArquivo;
	}
	
	/**
	 * Set: meioTransmissaoArquivo.
	 *
	 * @param meioTransmissaoArquivo the meio transmissao arquivo
	 */
	public void setMeioTransmissaoArquivo(String meioTransmissaoArquivo) {
		this.meioTransmissaoArquivo = meioTransmissaoArquivo;
	}
	
	/**
	 * Get: nome.
	 *
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Set: nome.
	 *
	 * @param nome the nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Get: numeroArquivoRetorno.
	 *
	 * @return numeroArquivoRetorno
	 */
	public String getNumeroArquivoRetorno() {
		return numeroArquivoRetorno;
	}
	
	/**
	 * Set: numeroArquivoRetorno.
	 *
	 * @param numeroArquivoRetorno the numero arquivo retorno
	 */
	public void setNumeroArquivoRetorno(String numeroArquivoRetorno) {
		this.numeroArquivoRetorno = numeroArquivoRetorno;
	}
	
	/**
	 * Get: perfil.
	 *
	 * @return perfil
	 */
	public String getPerfil() {
		return perfil;
	}
	
	/**
	 * Set: perfil.
	 *
	 * @param perfil the perfil
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	/**
	 * Get: produto.
	 *
	 * @return produto
	 */
	public String getProduto() {
		return produto;
	}
	
	/**
	 * Set: produto.
	 *
	 * @param produto the produto
	 */
	public void setProduto(String produto) {
		this.produto = produto;
	}
	
	/**
	 * Get: razaoContaDebito.
	 *
	 * @return razaoContaDebito
	 */
	public String getRazaoContaDebito() {
		return razaoContaDebito;
	}
	
	/**
	 * Set: razaoContaDebito.
	 *
	 * @param razaoContaDebito the razao conta debito
	 */
	public void setRazaoContaDebito(String razaoContaDebito) {
		this.razaoContaDebito = razaoContaDebito;
	}
	
	/**
	 * Get: situacaoGeracaoRetorno.
	 *
	 * @return situacaoGeracaoRetorno
	 */
	public String getSituacaoGeracaoRetorno() {
		return situacaoGeracaoRetorno;
	}
	
	/**
	 * Set: situacaoGeracaoRetorno.
	 *
	 * @param situacaoGeracaoRetorno the situacao geracao retorno
	 */
	public void setSituacaoGeracaoRetorno(String situacaoGeracaoRetorno) {
		this.situacaoGeracaoRetorno = situacaoGeracaoRetorno;
	}
	
	/**
	 * Get: situacaoRetorno.
	 *
	 * @return situacaoRetorno
	 */
	public String getSituacaoRetorno() {
		return situacaoRetorno;
	}
	
	/**
	 * Set: situacaoRetorno.
	 *
	 * @param situacaoRetorno the situacao retorno
	 */
	public void setSituacaoRetorno(String situacaoRetorno) {
		this.situacaoRetorno = situacaoRetorno;
	}
	
	/**
	 * Get: tipoArquivoRetorno.
	 *
	 * @return tipoArquivoRetorno
	 */
	public String getTipoArquivoRetorno() {
		return tipoArquivoRetorno;
	}
	
	/**
	 * Set: tipoArquivoRetorno.
	 *
	 * @param tipoArquivoRetorno the tipo arquivo retorno
	 */
	public void setTipoArquivoRetorno(String tipoArquivoRetorno) {
		this.tipoArquivoRetorno = tipoArquivoRetorno;
	}
	
	/**
	 * Get: tipoLayout.
	 *
	 * @return tipoLayout
	 */
	public String getTipoLayout() {
		return tipoLayout;
	}
	
	/**
	 * Set: tipoLayout.
	 *
	 * @param tipoLayout the tipo layout
	 */
	public void setTipoLayout(String tipoLayout) {
		this.tipoLayout = tipoLayout;
	}
	
	/**
	 * Get: tipoRetorno.
	 *
	 * @return tipoRetorno
	 */
	public String getTipoRetorno() {
		return tipoRetorno;
	}
	
	/**
	 * Set: tipoRetorno.
	 *
	 * @param tipoRetorno the tipo retorno
	 */
	public void setTipoRetorno(String tipoRetorno) {
		this.tipoRetorno = tipoRetorno;
	}
	
	/**
	 * Get: valorRegistroArquivoRetorno.
	 *
	 * @return valorRegistroArquivoRetorno
	 */
	public String getValorRegistroArquivoRetorno() {
		return valorRegistroArquivoRetorno;
	}
	
	/**
	 * Set: valorRegistroArquivoRetorno.
	 *
	 * @param valorRegistroArquivoRetorno the valor registro arquivo retorno
	 */
	public void setValorRegistroArquivoRetorno(String valorRegistroArquivoRetorno) {
		this.valorRegistroArquivoRetorno = valorRegistroArquivoRetorno;
	}
	
	/**
	 * Get: qtdeOcorrencias.
	 *
	 * @return qtdeOcorrencias
	 */
	public String getQtdeOcorrencias() {
		return qtdeOcorrencias;
	}
	
	/**
	 * Set: qtdeOcorrencias.
	 *
	 * @param quantidadeRegistroArquivoRetorno the qtde ocorrencias
	 */
	public void setQtdeOcorrencias(
			String quantidadeRegistroArquivoRetorno) {
		this.qtdeOcorrencias = quantidadeRegistroArquivoRetorno;
	}
	
	/**
	 * Get: horaInclusao.
	 *
	 * @return horaInclusao
	 */
	public String getHoraInclusao() {
		return horaInclusao;
	}
	
	/**
	 * Set: horaInclusao.
	 *
	 * @param horaInclusao the hora inclusao
	 */
	public void setHoraInclusao(String horaInclusao) {
		this.horaInclusao = horaInclusao;
	}
	
	/**
	 * Get: numeroArquivoRetornoLong.
	 *
	 * @return numeroArquivoRetornoLong
	 */
	public Long getNumeroArquivoRetornoLong() {
		return numeroArquivoRetornoLong;
	}
	
	/**
	 * Set: numeroArquivoRetornoLong.
	 *
	 * @param numeroArquivoRetornoLong the numero arquivo retorno long
	 */
	public void setNumeroArquivoRetornoLong(Long numeroArquivoRetornoLong) {
		this.numeroArquivoRetornoLong = numeroArquivoRetornoLong;
	}
	
	/**
	 * Get: descricaoTipoArquivo.
	 *
	 * @return descricaoTipoArquivo
	 */
	public String getDescricaoTipoArquivo() {
		return descricaoTipoArquivo;
	}
	
	/**
	 * Set: descricaoTipoArquivo.
	 *
	 * @param descricaoTipoArquivo the descricao tipo arquivo
	 */
	public void setDescricaoTipoArquivo(String descricaoTipoArquivo) {
		this.descricaoTipoArquivo = descricaoTipoArquivo;
	}
	
	/**
	 * Get: descricaoSituacao.
	 *
	 * @return descricaoSituacao
	 */
	public String getDescricaoSituacao() {
		return descricaoSituacao;
	}
	
	/**
	 * Set: descricaoSituacao.
	 *
	 * @param descricaoSituacao the descricao situacao
	 */
	public void setDescricaoSituacao(String descricaoSituacao) {
		this.descricaoSituacao = descricaoSituacao;
	}	
	
	
	
}