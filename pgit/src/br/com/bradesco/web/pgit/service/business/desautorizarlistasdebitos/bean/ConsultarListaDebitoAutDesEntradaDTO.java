/*
 * Nome: br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean;

/**
 * Nome: ConsultarListaDebitoAutDesEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaDebitoAutDesEntradaDTO {    
    
    /** Atributo cdTipoPesquisa. */
    private Integer cdTipoPesquisa;
    
    /** Atributo cdPessoaParticipacaoContrato. */
    private Long cdPessoaParticipacaoContrato;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo dtCreditoPagamentoInicial. */
    private String dtCreditoPagamentoInicial;
    
    /** Atributo dtCreditoPagamentoFinal. */
    private String dtCreditoPagamentoFinal;
    
    /** Atributo cdListaDebitoPagamentoInicial. */
    private Long cdListaDebitoPagamentoInicial;
    
    /** Atributo cdListaDebitoPagamentoFinal. */
    private Long cdListaDebitoPagamentoFinal;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo cdAgencua. */
    private Integer cdAgencua;
    
    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo cdTipoOperacao. */
    private Integer cdTipoOperacao;
    
    /** Atributo cdServicoCompostoPagamento. */
    private Long cdServicoCompostoPagamento;
    
	/**
	 * Get: cdTipoOperacao.
	 *
	 * @return cdTipoOperacao
	 */
	public Integer getCdTipoOperacao() {
		return cdTipoOperacao;
	}
	
	/**
	 * Set: cdTipoOperacao.
	 *
	 * @param cdTipoOperacao the cd tipo operacao
	 */
	public void setCdTipoOperacao(Integer cdTipoOperacao) {
		this.cdTipoOperacao = cdTipoOperacao;
	}
	
	/**
	 * Get: cdAgencua.
	 *
	 * @return cdAgencua
	 */
	public Integer getCdAgencua() {
		return cdAgencua;
	}
	
	/**
	 * Set: cdAgencua.
	 *
	 * @param cdAgencua the cd agencua
	 */
	public void setCdAgencua(Integer cdAgencua) {
		this.cdAgencua = cdAgencua;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	
	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdListaDebitoPagamentoFinal.
	 *
	 * @return cdListaDebitoPagamentoFinal
	 */
	public Long getCdListaDebitoPagamentoFinal() {
		return cdListaDebitoPagamentoFinal;
	}
	
	/**
	 * Set: cdListaDebitoPagamentoFinal.
	 *
	 * @param cdListaDebitoPagamentoFinal the cd lista debito pagamento final
	 */
	public void setCdListaDebitoPagamentoFinal(Long cdListaDebitoPagamentoFinal) {
		this.cdListaDebitoPagamentoFinal = cdListaDebitoPagamentoFinal;
	}
	
	/**
	 * Get: cdListaDebitoPagamentoInicial.
	 *
	 * @return cdListaDebitoPagamentoInicial
	 */
	public Long getCdListaDebitoPagamentoInicial() {
		return cdListaDebitoPagamentoInicial;
	}
	
	/**
	 * Set: cdListaDebitoPagamentoInicial.
	 *
	 * @param cdListaDebitoPagamentoInicial the cd lista debito pagamento inicial
	 */
	public void setCdListaDebitoPagamentoInicial(Long cdListaDebitoPagamentoInicial) {
		this.cdListaDebitoPagamentoInicial = cdListaDebitoPagamentoInicial;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdPessoaParticipacaoContrato.
	 *
	 * @return cdPessoaParticipacaoContrato
	 */
	public Long getCdPessoaParticipacaoContrato() {
		return cdPessoaParticipacaoContrato;
	}
	
	/**
	 * Set: cdPessoaParticipacaoContrato.
	 *
	 * @param cdPessoaParticipacaoContrato the cd pessoa participacao contrato
	 */
	public void setCdPessoaParticipacaoContrato(Long cdPessoaParticipacaoContrato) {
		this.cdPessoaParticipacaoContrato = cdPessoaParticipacaoContrato;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoPesquisa.
	 *
	 * @return cdTipoPesquisa
	 */
	public Integer getCdTipoPesquisa() {
		return cdTipoPesquisa;
	}
	
	/**
	 * Set: cdTipoPesquisa.
	 *
	 * @param cdTipoPesquisa the cd tipo pesquisa
	 */
	public void setCdTipoPesquisa(Integer cdTipoPesquisa) {
		this.cdTipoPesquisa = cdTipoPesquisa;
	}
	
	/**
	 * Get: dtCreditoPagamentoFinal.
	 *
	 * @return dtCreditoPagamentoFinal
	 */
	public String getDtCreditoPagamentoFinal() {
		return dtCreditoPagamentoFinal;
	}
	
	/**
	 * Set: dtCreditoPagamentoFinal.
	 *
	 * @param dtCreditoPagamentoFinal the dt credito pagamento final
	 */
	public void setDtCreditoPagamentoFinal(String dtCreditoPagamentoFinal) {
		this.dtCreditoPagamentoFinal = dtCreditoPagamentoFinal;
	}
	
	/**
	 * Get: dtCreditoPagamentoInicial.
	 *
	 * @return dtCreditoPagamentoInicial
	 */
	public String getDtCreditoPagamentoInicial() {
		return dtCreditoPagamentoInicial;
	}
	
	/**
	 * Set: dtCreditoPagamentoInicial.
	 *
	 * @param dtCreditoPagamentoInicial the dt credito pagamento inicial
	 */
	public void setDtCreditoPagamentoInicial(String dtCreditoPagamentoInicial) {
		this.dtCreditoPagamentoInicial = dtCreditoPagamentoInicial;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdServicoCompostoPagamento.
	 *
	 * @return cdServicoCompostoPagamento
	 */
	public Long getCdServicoCompostoPagamento() {
		return cdServicoCompostoPagamento;
	}
	
	/**
	 * Set: cdServicoCompostoPagamento.
	 *
	 * @param cdServicoCompostoPagamento the cd servico composto pagamento
	 */
	public void setCdServicoCompostoPagamento(Long cdServicoCompostoPagamento) {
		this.cdServicoCompostoPagamento = cdServicoCompostoPagamento;
	}
    
}
