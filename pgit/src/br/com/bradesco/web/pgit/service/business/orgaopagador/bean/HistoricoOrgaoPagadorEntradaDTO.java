/*
 * Nome: br.com.bradesco.web.pgit.service.business.orgaopagador.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.orgaopagador.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Nome: HistoricoOrgaoPagadorEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class HistoricoOrgaoPagadorEntradaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 4886242110966704266L;

	/** Atributo cdOrgaoPagador. */
	private Integer cdOrgaoPagador;

    /** Atributo dtDe. */
    private Date dtDe;

    /** Atributo dtAte. */
    private Date dtAte;

    /** Atributo qtConsultas. */
    private Integer qtConsultas;

    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;

	/**
	 * Get: cdOrgaoPagador.
	 *
	 * @return cdOrgaoPagador
	 */
	public Integer getCdOrgaoPagador() {
		return cdOrgaoPagador;
	}

	/**
	 * Set: cdOrgaoPagador.
	 *
	 * @param cdOrgaoPagador the cd orgao pagador
	 */
	public void setCdOrgaoPagador(Integer cdOrgaoPagador) {
		this.cdOrgaoPagador = cdOrgaoPagador;
	}

	/**
	 * Get: dtAte.
	 *
	 * @return dtAte
	 */
	public Date getDtAte() {
		return dtAte;
	}

	/**
	 * Set: dtAte.
	 *
	 * @param dtAte the dt ate
	 */
	public void setDtAte(Date dtAte) {
		this.dtAte = dtAte;
	}

	/**
	 * Get: dtDe.
	 *
	 * @return dtDe
	 */
	public Date getDtDe() {
		return dtDe;
	}

	/**
	 * Set: dtDe.
	 *
	 * @param dtDe the dt de
	 */
	public void setDtDe(Date dtDe) {
		this.dtDe = dtDe;
	}

	/**
	 * Get: qtConsultas.
	 *
	 * @return qtConsultas
	 */
	public Integer getQtConsultas() {
		return qtConsultas;
	}

	/**
	 * Set: qtConsultas.
	 *
	 * @param qtConsultas the qt consultas
	 */
	public void setQtConsultas(Integer qtConsultas) {
		this.qtConsultas = qtConsultas;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

}
