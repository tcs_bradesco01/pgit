/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

/**
 * Nome: ListaVinculoContaSalarioDestEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListaVinculoContaSalarioDestEntradaDTO {
	
	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;
    
    /** Atributo cdPessoaJuridicaNegocio. */
    private Long cdPessoaJuridicaNegocio;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo dgConta. */
    private String dgConta;
    
    /** Atributo nrCpf. */
    private Long nrCpf;
    
    /** Atributo dgCpf. */
    private Integer dgCpf;
    
    /**
     * Get: dgCpf.
     *
     * @return dgCpf
     */
    public Integer getDgCpf() {
		return dgCpf;
	}

	/**
	 * Set: dgCpf.
	 *
	 * @param dgCpf the dg cpf
	 */
	public void setDgCpf(Integer dgCpf) {
		this.dgCpf = dgCpf;
	}

	/**
	 * Get: nrCpf.
	 *
	 * @return nrCpf
	 */
	public Long getNrCpf() {
		return nrCpf;
	}

	/**
	 * Set: nrCpf.
	 *
	 * @param nrCpf the nr cpf
	 */
	public void setNrCpf(Long nrCpf) {
		this.nrCpf = nrCpf;
	}

	/**
	 * Lista vinculo conta salario dest entrada dto.
	 */
	public ListaVinculoContaSalarioDestEntradaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdPessoaJuridicaNegocio.
	 *
	 * @return cdPessoaJuridicaNegocio
	 */
	public Long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Set: cdPessoaJuridicaNegocio.
	 *
	 * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
	 */
	public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dgConta.
	 *
	 * @return dgConta
	 */
	public String getDgConta() {
		return dgConta;
	}
	
	/**
	 * Set: dgConta.
	 *
	 * @param dgConta the dg conta
	 */
	public void setDgConta(String dgConta) {
		this.dgConta = dgConta;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}

}
