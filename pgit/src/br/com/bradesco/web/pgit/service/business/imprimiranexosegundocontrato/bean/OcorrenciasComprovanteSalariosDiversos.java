/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

/**
 * Nome: OcorrenciasComprovanteSalariosDiversos
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasComprovanteSalariosDiversos {
	
	/** Atributo dsCsdTipoComprovante. */
	private String dsCsdTipoComprovante;
    
    /** Atributo dsCsdTipoRejeicaoLote. */
    private String dsCsdTipoRejeicaoLote;
    
    /** Atributo dsCsdMeioDisponibilizacaoCorrentista. */
    private String dsCsdMeioDisponibilizacaoCorrentista;
    
    /** Atributo dsCsdMediaDisponibilidadeCorrentista. */
    private String dsCsdMediaDisponibilidadeCorrentista;
    
    /** Atributo dsCsdMediaDisponibilidadeNumeroCorrentistas. */
    private String dsCsdMediaDisponibilidadeNumeroCorrentistas;
    
    /** Atributo dsCsdDestino. */
    private String dsCsdDestino;
    
    /** Atributo qtCsdMesEmissao. */
    private Integer qtCsdMesEmissao;
    
    /** Atributo dsCsdCadastroConservadorEndereco. */
    private String dsCsdCadastroConservadorEndereco;
    
    /** Atributo cdCsdQuantidadeViasEmitir. */
    private Integer cdCsdQuantidadeViasEmitir;
    
	/**
	 * Get: dsCsdTipoComprovante.
	 *
	 * @return dsCsdTipoComprovante
	 */
	public String getDsCsdTipoComprovante() {
		return dsCsdTipoComprovante;
	}
	
	/**
	 * Set: dsCsdTipoComprovante.
	 *
	 * @param dsCsdTipoComprovante the ds csd tipo comprovante
	 */
	public void setDsCsdTipoComprovante(String dsCsdTipoComprovante) {
		this.dsCsdTipoComprovante = dsCsdTipoComprovante;
	}
	
	/**
	 * Get: dsCsdTipoRejeicaoLote.
	 *
	 * @return dsCsdTipoRejeicaoLote
	 */
	public String getDsCsdTipoRejeicaoLote() {
		return dsCsdTipoRejeicaoLote;
	}
	
	/**
	 * Set: dsCsdTipoRejeicaoLote.
	 *
	 * @param dsCsdTipoRejeicaoLote the ds csd tipo rejeicao lote
	 */
	public void setDsCsdTipoRejeicaoLote(String dsCsdTipoRejeicaoLote) {
		this.dsCsdTipoRejeicaoLote = dsCsdTipoRejeicaoLote;
	}
	
	/**
	 * Get: dsCsdMeioDisponibilizacaoCorrentista.
	 *
	 * @return dsCsdMeioDisponibilizacaoCorrentista
	 */
	public String getDsCsdMeioDisponibilizacaoCorrentista() {
		return dsCsdMeioDisponibilizacaoCorrentista;
	}
	
	/**
	 * Set: dsCsdMeioDisponibilizacaoCorrentista.
	 *
	 * @param dsCsdMeioDisponibilizacaoCorrentista the ds csd meio disponibilizacao correntista
	 */
	public void setDsCsdMeioDisponibilizacaoCorrentista(
			String dsCsdMeioDisponibilizacaoCorrentista) {
		this.dsCsdMeioDisponibilizacaoCorrentista = dsCsdMeioDisponibilizacaoCorrentista;
	}
	
	/**
	 * Get: dsCsdMediaDisponibilidadeCorrentista.
	 *
	 * @return dsCsdMediaDisponibilidadeCorrentista
	 */
	public String getDsCsdMediaDisponibilidadeCorrentista() {
		return dsCsdMediaDisponibilidadeCorrentista;
	}
	
	/**
	 * Set: dsCsdMediaDisponibilidadeCorrentista.
	 *
	 * @param dsCsdMediaDisponibilidadeCorrentista the ds csd media disponibilidade correntista
	 */
	public void setDsCsdMediaDisponibilidadeCorrentista(
			String dsCsdMediaDisponibilidadeCorrentista) {
		this.dsCsdMediaDisponibilidadeCorrentista = dsCsdMediaDisponibilidadeCorrentista;
	}
	
	/**
	 * Get: dsCsdMediaDisponibilidadeNumeroCorrentistas.
	 *
	 * @return dsCsdMediaDisponibilidadeNumeroCorrentistas
	 */
	public String getDsCsdMediaDisponibilidadeNumeroCorrentistas() {
		return dsCsdMediaDisponibilidadeNumeroCorrentistas;
	}
	
	/**
	 * Set: dsCsdMediaDisponibilidadeNumeroCorrentistas.
	 *
	 * @param dsCsdMediaDisponibilidadeNumeroCorrentistas the ds csd media disponibilidade numero correntistas
	 */
	public void setDsCsdMediaDisponibilidadeNumeroCorrentistas(
			String dsCsdMediaDisponibilidadeNumeroCorrentistas) {
		this.dsCsdMediaDisponibilidadeNumeroCorrentistas = dsCsdMediaDisponibilidadeNumeroCorrentistas;
	}
	
	/**
	 * Get: dsCsdDestino.
	 *
	 * @return dsCsdDestino
	 */
	public String getDsCsdDestino() {
		return dsCsdDestino;
	}
	
	/**
	 * Set: dsCsdDestino.
	 *
	 * @param dsCsdDestino the ds csd destino
	 */
	public void setDsCsdDestino(String dsCsdDestino) {
		this.dsCsdDestino = dsCsdDestino;
	}
	
	/**
	 * Get: qtCsdMesEmissao.
	 *
	 * @return qtCsdMesEmissao
	 */
	public Integer getQtCsdMesEmissao() {
		return qtCsdMesEmissao;
	}
	
	/**
	 * Set: qtCsdMesEmissao.
	 *
	 * @param qtCsdMesEmissao the qt csd mes emissao
	 */
	public void setQtCsdMesEmissao(Integer qtCsdMesEmissao) {
		this.qtCsdMesEmissao = qtCsdMesEmissao;
	}
	
	/**
	 * Get: dsCsdCadastroConservadorEndereco.
	 *
	 * @return dsCsdCadastroConservadorEndereco
	 */
	public String getDsCsdCadastroConservadorEndereco() {
		return dsCsdCadastroConservadorEndereco;
	}
	
	/**
	 * Set: dsCsdCadastroConservadorEndereco.
	 *
	 * @param dsCsdCadastroConservadorEndereco the ds csd cadastro conservador endereco
	 */
	public void setDsCsdCadastroConservadorEndereco(
			String dsCsdCadastroConservadorEndereco) {
		this.dsCsdCadastroConservadorEndereco = dsCsdCadastroConservadorEndereco;
	}
	
	/**
	 * Get: cdCsdQuantidadeViasEmitir.
	 *
	 * @return cdCsdQuantidadeViasEmitir
	 */
	public Integer getCdCsdQuantidadeViasEmitir() {
		return cdCsdQuantidadeViasEmitir;
	}
	
	/**
	 * Set: cdCsdQuantidadeViasEmitir.
	 *
	 * @param cdCsdQuantidadeViasEmitir the cd csd quantidade vias emitir
	 */
	public void setCdCsdQuantidadeViasEmitir(Integer cdCsdQuantidadeViasEmitir) {
		this.cdCsdQuantidadeViasEmitir = cdCsdQuantidadeViasEmitir;
	}
    
}
