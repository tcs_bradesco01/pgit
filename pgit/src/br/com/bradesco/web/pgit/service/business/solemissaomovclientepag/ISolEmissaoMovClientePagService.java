/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solemissaomovclientepag;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ConsultarSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ConsultarSolEmiAviMovtoCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.DetalharSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.DetalharSolEmiAviMovtoCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ExcluirSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ExcluirSolEmiAviMovtoCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.IncluirSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.IncluirSolEmiAviMovtoCliePagadorSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: SolEmissaoMovClientePag
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ISolEmissaoMovClientePagService {
	
	/**
	 * Consultar sol emi avi movto clie pagador.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the list< consultar sol emi avi movto clie pagador saida dt o>
	 */
	List<ConsultarSolEmiAviMovtoCliePagadorSaidaDTO> consultarSolEmiAviMovtoCliePagador (ConsultarSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO);
	
	/**
	 * Detalhar sol emi avi movto clie pagador.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the detalhar sol emi avi movto clie pagador saida dto
	 */
	DetalharSolEmiAviMovtoCliePagadorSaidaDTO detalharSolEmiAviMovtoCliePagador (DetalharSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO);
	
	/**
	 * Excluir sol emi avi movto clie pagador.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the excluir sol emi avi movto clie pagador saida dto
	 */
	ExcluirSolEmiAviMovtoCliePagadorSaidaDTO excluirSolEmiAviMovtoCliePagador (ExcluirSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO);
	
	/**
	 * Incluir sol emi avi movto clie pagador.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the incluir sol emi avi movto clie pagador saida dto
	 */
	IncluirSolEmiAviMovtoCliePagadorSaidaDTO incluirSolEmiAviMovtoCliePagador (IncluirSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO);
}

