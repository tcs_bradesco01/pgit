/**
 * 
 */
package br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean;

import java.math.BigDecimal;


/**
 * @author tcordeiro
 *
 */
public class IncluirSolicRastFavSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo dtInicioRastreamento. */
    private String dtInicioRastreamento;
    
    /** Atributo dtFimRastreamento. */
    private String dtFimRastreamento;
    
    /** Atributo cdServico. */
    private Integer cdServico;
    
    /** Atributo cdModalidade. */
    private Integer cdModalidade;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo dgConta. */
    private String dgConta;
    
    /** Atributo cdIndicadorInclusaoFavorecido. */
    private Integer cdIndicadorInclusaoFavorecido;
    
    /** Atributo cdIndicadorDestinoRetorno. */
    private Integer cdIndicadorDestinoRetorno;
    
    /** Atributo vlTarifaBonificacao. */
    private BigDecimal vlTarifaBonificacao;
    
    
    /**
     * Get: digitoConta.
     *
     * @return digitoConta
     */
    public String getDigitoConta() {
		if (dgConta != null && !dgConta.trim().equals("")) {
			return cdConta == 0 ? "" : cdConta + "-" +  this.dgConta;
		}else {
			return String.valueOf(cdConta);
		}
	}
    
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}

	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}

	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}

	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}

	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}

	/**
	 * Get: cdIndicadorDestinoRetorno.
	 *
	 * @return cdIndicadorDestinoRetorno
	 */
	public Integer getCdIndicadorDestinoRetorno() {
		return cdIndicadorDestinoRetorno;
	}

	/**
	 * Set: cdIndicadorDestinoRetorno.
	 *
	 * @param cdIndicadorDestinoRetorno the cd indicador destino retorno
	 */
	public void setCdIndicadorDestinoRetorno(Integer cdIndicadorDestinoRetorno) {
		this.cdIndicadorDestinoRetorno = cdIndicadorDestinoRetorno;
	}

	/**
	 * Get: cdIndicadorInclusaoFavorecido.
	 *
	 * @return cdIndicadorInclusaoFavorecido
	 */
	public Integer getCdIndicadorInclusaoFavorecido() {
		return cdIndicadorInclusaoFavorecido;
	}

	/**
	 * Set: cdIndicadorInclusaoFavorecido.
	 *
	 * @param cdIndicadorInclusaoFavorecido the cd indicador inclusao favorecido
	 */
	public void setCdIndicadorInclusaoFavorecido(Integer cdIndicadorInclusaoFavorecido) {
		this.cdIndicadorInclusaoFavorecido = cdIndicadorInclusaoFavorecido;
	}

	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public Integer getCdModalidade() {
		return cdModalidade;
	}

	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(Integer cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}

	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: dgConta.
	 *
	 * @return dgConta
	 */
	public String getDgConta() {
		return dgConta;
	}

	/**
	 * Set: dgConta.
	 *
	 * @param dgConta the dg conta
	 */
	public void setDgConta(String dgConta) {
		this.dgConta = dgConta;
	}

	/**
	 * Get: dtFimRastreamento.
	 *
	 * @return dtFimRastreamento
	 */
	public String getDtFimRastreamento() {
		return dtFimRastreamento;
	}

	/**
	 * Set: dtFimRastreamento.
	 *
	 * @param dtFimRastreamento the dt fim rastreamento
	 */
	public void setDtFimRastreamento(String dtFimRastreamento) {
		this.dtFimRastreamento = dtFimRastreamento;
	}

	/**
	 * Get: dtInicioRastreamento.
	 *
	 * @return dtInicioRastreamento
	 */
	public String getDtInicioRastreamento() {
		return dtInicioRastreamento;
	}

	/**
	 * Set: dtInicioRastreamento.
	 *
	 * @param dtInicioRastreamento the dt inicio rastreamento
	 */
	public void setDtInicioRastreamento(String dtInicioRastreamento) {
		this.dtInicioRastreamento = dtInicioRastreamento;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: vlTarifaBonificacao.
	 *
	 * @return vlTarifaBonificacao
	 */
	public BigDecimal getVlTarifaBonificacao() {
		return vlTarifaBonificacao;
	}

	/**
	 * Set: vlTarifaBonificacao.
	 *
	 * @param vlTarifaBonificacao the vl tarifa bonificacao
	 */
	public void setVlTarifaBonificacao(BigDecimal vlTarifaBonificacao) {
		this.vlTarifaBonificacao = vlTarifaBonificacao;
	}

	/**
	 * Incluir solic rast fav saida dto.
	 */
	public IncluirSolicRastFavSaidaDTO() {
		
	}
	
	/**
	 * Incluir solic rast fav saida dto.
	 *
	 * @param codMensagem the cod mensagem
	 * @param mensagem the mensagem
	 */
	public IncluirSolicRastFavSaidaDTO(String codMensagem, String mensagem) {
		this.codMensagem = codMensagem;
	    this.mensagem = mensagem;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
