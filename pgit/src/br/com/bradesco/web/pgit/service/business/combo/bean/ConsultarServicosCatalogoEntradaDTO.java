/*
 * Nome: ${package_name}
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: ${date}
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 03/05/16.
 */
public class ConsultarServicosCatalogoEntradaDTO {

    /** The cd producao servico cdps. */
    private Integer cdProducaoServicoCdps;

    /** The cd prodt relacionado cdps. */
    private Integer cdProdtRelacionadoCdps;

    /** The cd produto pgit. */
    private Long cdProdutoPgit;

    /** The cd tipo acesso. */
    private Integer cdTipoAcesso;

    /**
     * Set cd producao servico cdps.
     *
     * @param cdProducaoServicoCdps the cd producao servico cdps
     */
    public void setCdProducaoServicoCdps(Integer cdProducaoServicoCdps) {
        this.cdProducaoServicoCdps = cdProducaoServicoCdps;
    }

    /**
     * Get cd producao servico cdps.
     *
     * @return the cd producao servico cdps
     */
    public Integer getCdProducaoServicoCdps() {
        return this.cdProducaoServicoCdps;
    }

    /**
     * Set cd prodt relacionado cdps.
     *
     * @param cdProdtRelacionadoCdps the cd prodt relacionado cdps
     */
    public void setCdProdtRelacionadoCdps(Integer cdProdtRelacionadoCdps) {
        this.cdProdtRelacionadoCdps = cdProdtRelacionadoCdps;
    }

    /**
     * Get cd prodt relacionado cdps.
     *
     * @return the cd prodt relacionado cdps
     */
    public Integer getCdProdtRelacionadoCdps() {
        return this.cdProdtRelacionadoCdps;
    }

    /**
     * Set cd produto pgit.
     *
     * @param cdProdutoPgit the cd produto pgit
     */
    public void setCdProdutoPgit(Long cdProdutoPgit) {
        this.cdProdutoPgit = cdProdutoPgit;
    }

    /**
     * Get cd produto pgit.
     *
     * @return the cd produto pgit
     */
    public Long getCdProdutoPgit() {
        return this.cdProdutoPgit;
    }

    /**
     * Set cd tipo acesso.
     *
     * @param cdTipoAcesso the cd tipo acesso
     */
    public void setCdTipoAcesso(Integer cdTipoAcesso) {
        this.cdTipoAcesso = cdTipoAcesso;
    }

    /**
     * Get cd tipo acesso.
     *
     * @return the cd tipo acesso
     */
    public Integer getCdTipoAcesso() {
        return this.cdTipoAcesso;
    }
}