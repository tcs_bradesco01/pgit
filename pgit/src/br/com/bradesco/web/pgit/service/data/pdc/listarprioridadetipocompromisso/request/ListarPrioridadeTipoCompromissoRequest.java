/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarprioridadetipocompromisso.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarPrioridadeTipoCompromissoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarPrioridadeTipoCompromissoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCompromissoPrioridadeProduto
     */
    private int _cdCompromissoPrioridadeProduto = 0;

    /**
     * keeps track of state for field:
     * _cdCompromissoPrioridadeProduto
     */
    private boolean _has_cdCompromissoPrioridadeProduto;

    /**
     * Field _cdCustoPrioridadeProduto
     */
    private java.lang.String _cdCustoPrioridadeProduto;

    /**
     * Field _cdOrdemPrioridadeCompromisso
     */
    private int _cdOrdemPrioridadeCompromisso = 0;

    /**
     * keeps track of state for field: _cdOrdemPrioridadeCompromisso
     */
    private boolean _has_cdOrdemPrioridadeCompromisso;

    /**
     * Field _dtInicioVigenciaCompromisso
     */
    private java.lang.String _dtInicioVigenciaCompromisso;

    /**
     * Field _qtConsultas
     */
    private int _qtConsultas = 0;

    /**
     * keeps track of state for field: _qtConsultas
     */
    private boolean _has_qtConsultas;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarPrioridadeTipoCompromissoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarprioridadetipocompromisso.request.ListarPrioridadeTipoCompromissoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCompromissoPrioridadeProduto
     * 
     */
    public void deleteCdCompromissoPrioridadeProduto()
    {
        this._has_cdCompromissoPrioridadeProduto= false;
    } //-- void deleteCdCompromissoPrioridadeProduto() 

    /**
     * Method deleteCdOrdemPrioridadeCompromisso
     * 
     */
    public void deleteCdOrdemPrioridadeCompromisso()
    {
        this._has_cdOrdemPrioridadeCompromisso= false;
    } //-- void deleteCdOrdemPrioridadeCompromisso() 

    /**
     * Method deleteQtConsultas
     * 
     */
    public void deleteQtConsultas()
    {
        this._has_qtConsultas= false;
    } //-- void deleteQtConsultas() 

    /**
     * Returns the value of field 'cdCompromissoPrioridadeProduto'.
     * 
     * @return int
     * @return the value of field 'cdCompromissoPrioridadeProduto'.
     */
    public int getCdCompromissoPrioridadeProduto()
    {
        return this._cdCompromissoPrioridadeProduto;
    } //-- int getCdCompromissoPrioridadeProduto() 

    /**
     * Returns the value of field 'cdCustoPrioridadeProduto'.
     * 
     * @return String
     * @return the value of field 'cdCustoPrioridadeProduto'.
     */
    public java.lang.String getCdCustoPrioridadeProduto()
    {
        return this._cdCustoPrioridadeProduto;
    } //-- java.lang.String getCdCustoPrioridadeProduto() 

    /**
     * Returns the value of field 'cdOrdemPrioridadeCompromisso'.
     * 
     * @return int
     * @return the value of field 'cdOrdemPrioridadeCompromisso'.
     */
    public int getCdOrdemPrioridadeCompromisso()
    {
        return this._cdOrdemPrioridadeCompromisso;
    } //-- int getCdOrdemPrioridadeCompromisso() 

    /**
     * Returns the value of field 'dtInicioVigenciaCompromisso'.
     * 
     * @return String
     * @return the value of field 'dtInicioVigenciaCompromisso'.
     */
    public java.lang.String getDtInicioVigenciaCompromisso()
    {
        return this._dtInicioVigenciaCompromisso;
    } //-- java.lang.String getDtInicioVigenciaCompromisso() 

    /**
     * Returns the value of field 'qtConsultas'.
     * 
     * @return int
     * @return the value of field 'qtConsultas'.
     */
    public int getQtConsultas()
    {
        return this._qtConsultas;
    } //-- int getQtConsultas() 

    /**
     * Method hasCdCompromissoPrioridadeProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCompromissoPrioridadeProduto()
    {
        return this._has_cdCompromissoPrioridadeProduto;
    } //-- boolean hasCdCompromissoPrioridadeProduto() 

    /**
     * Method hasCdOrdemPrioridadeCompromisso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrdemPrioridadeCompromisso()
    {
        return this._has_cdOrdemPrioridadeCompromisso;
    } //-- boolean hasCdOrdemPrioridadeCompromisso() 

    /**
     * Method hasQtConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtConsultas()
    {
        return this._has_qtConsultas;
    } //-- boolean hasQtConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCompromissoPrioridadeProduto'.
     * 
     * @param cdCompromissoPrioridadeProduto the value of field
     * 'cdCompromissoPrioridadeProduto'.
     */
    public void setCdCompromissoPrioridadeProduto(int cdCompromissoPrioridadeProduto)
    {
        this._cdCompromissoPrioridadeProduto = cdCompromissoPrioridadeProduto;
        this._has_cdCompromissoPrioridadeProduto = true;
    } //-- void setCdCompromissoPrioridadeProduto(int) 

    /**
     * Sets the value of field 'cdCustoPrioridadeProduto'.
     * 
     * @param cdCustoPrioridadeProduto the value of field
     * 'cdCustoPrioridadeProduto'.
     */
    public void setCdCustoPrioridadeProduto(java.lang.String cdCustoPrioridadeProduto)
    {
        this._cdCustoPrioridadeProduto = cdCustoPrioridadeProduto;
    } //-- void setCdCustoPrioridadeProduto(java.lang.String) 

    /**
     * Sets the value of field 'cdOrdemPrioridadeCompromisso'.
     * 
     * @param cdOrdemPrioridadeCompromisso the value of field
     * 'cdOrdemPrioridadeCompromisso'.
     */
    public void setCdOrdemPrioridadeCompromisso(int cdOrdemPrioridadeCompromisso)
    {
        this._cdOrdemPrioridadeCompromisso = cdOrdemPrioridadeCompromisso;
        this._has_cdOrdemPrioridadeCompromisso = true;
    } //-- void setCdOrdemPrioridadeCompromisso(int) 

    /**
     * Sets the value of field 'dtInicioVigenciaCompromisso'.
     * 
     * @param dtInicioVigenciaCompromisso the value of field
     * 'dtInicioVigenciaCompromisso'.
     */
    public void setDtInicioVigenciaCompromisso(java.lang.String dtInicioVigenciaCompromisso)
    {
        this._dtInicioVigenciaCompromisso = dtInicioVigenciaCompromisso;
    } //-- void setDtInicioVigenciaCompromisso(java.lang.String) 

    /**
     * Sets the value of field 'qtConsultas'.
     * 
     * @param qtConsultas the value of field 'qtConsultas'.
     */
    public void setQtConsultas(int qtConsultas)
    {
        this._qtConsultas = qtConsultas;
        this._has_qtConsultas = true;
    } //-- void setQtConsultas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarPrioridadeTipoCompromissoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarprioridadetipocompromisso.request.ListarPrioridadeTipoCompromissoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarprioridadetipocompromisso.request.ListarPrioridadeTipoCompromissoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarprioridadetipocompromisso.request.ListarPrioridadeTipoCompromissoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarprioridadetipocompromisso.request.ListarPrioridadeTipoCompromissoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
