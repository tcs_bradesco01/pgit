/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.msgalertagestor;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.AlterarMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.AlterarMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.DetalharMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.DetalharMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ExcluirMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ExcluirMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.IncluirMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.IncluirMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ListarMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ListarMsgAlertaGestorSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterMensagemAlertaGestor
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IMsgAlertaGestorService {

   
	/**
	 * Listar mensagem alerta gestor.
	 *
	 * @param msgAlertaGestorEntradaDTO the msg alerta gestor entrada dto
	 * @return the list< listar msg alerta gestor saida dt o>
	 */
	List<ListarMsgAlertaGestorSaidaDTO> listarMensagemAlertaGestor(ListarMsgAlertaGestorEntradaDTO msgAlertaGestorEntradaDTO);
	
	/**
	 * Incluir mensagem alerta gestor.
	 *
	 * @param incluirMsgAlertaGestorEntradaDTO the incluir msg alerta gestor entrada dto
	 * @return the incluir msg alerta gestor saida dto
	 */
	IncluirMsgAlertaGestorSaidaDTO incluirMensagemAlertaGestor(IncluirMsgAlertaGestorEntradaDTO incluirMsgAlertaGestorEntradaDTO);
	
	/**
	 * Alterar mensagem alerta gestor.
	 *
	 * @param alterarMsgAlertaGestorEntradaDTO the alterar msg alerta gestor entrada dto
	 * @return the alterar msg alerta gestor saida dto
	 */
	AlterarMsgAlertaGestorSaidaDTO alterarMensagemAlertaGestor(AlterarMsgAlertaGestorEntradaDTO alterarMsgAlertaGestorEntradaDTO);
	
	/**
	 * Detalhar mensagem alerta gestaor.
	 *
	 * @param detalharMsgAlertaGestorEntradaDTO the detalhar msg alerta gestor entrada dto
	 * @return the detalhar msg alerta gestor saida dto
	 */
	DetalharMsgAlertaGestorSaidaDTO detalharMensagemAlertaGestaor(DetalharMsgAlertaGestorEntradaDTO detalharMsgAlertaGestorEntradaDTO);
	
	/**
	 * Excluir mensagem alerta gestor.
	 *
	 * @param msgAlertaGestorEntradaDTO the msg alerta gestor entrada dto
	 * @return the excluir msg alerta gestor saida dto
	 */
	ExcluirMsgAlertaGestorSaidaDTO excluirMensagemAlertaGestor(ExcluirMsgAlertaGestorEntradaDTO msgAlertaGestorEntradaDTO);	
}

