/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarAplicativoFormatacaoArquivoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarAplicativoFormatacaoArquivoEntradaDTO {
	
	/** Atributo nrOcorrencias. */
	private Integer nrOcorrencias;
    
    /** Atributo cdSituacao. */
    private Integer cdSituacao;
    
    /** Atributo cdAplictvTransmPgto. */
    private Integer cdAplictvTransmPgto;
    
    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;
    
	/**
	 * Get: nrOcorrencias.
	 *
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}
	
	/**
	 * Set: nrOcorrencias.
	 *
	 * @param nrOcorrencias the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}
	
	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao() {
		return cdSituacao;
	}
	
	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(Integer cdSituacao) {
		this.cdSituacao = cdSituacao;
	}
	
	/**
	 * Get: cdAplictvTransmPgto.
	 *
	 * @return cdAplictvTransmPgto
	 */
	public Integer getCdAplictvTransmPgto() {
		return cdAplictvTransmPgto;
	}
	
	/**
	 * Set: cdAplictvTransmPgto.
	 *
	 * @param cdAplictvTransmPgto the cd aplictv transm pgto
	 */
	public void setCdAplictvTransmPgto(Integer cdAplictvTransmPgto) {
		this.cdAplictvTransmPgto = cdAplictvTransmPgto;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
}