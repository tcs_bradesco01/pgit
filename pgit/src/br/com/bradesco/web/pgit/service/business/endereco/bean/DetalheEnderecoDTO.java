/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;

import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;

/**
 * Nome: DetalheEnderecoDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalheEnderecoDTO {

	/** Atributo cliente. */
	private ConsultarListaClientePessoasSaidaDTO cliente = new ConsultarListaClientePessoasSaidaDTO();

	/** Atributo contrato. */
	private ConsultarListaContratosPessoasSaidaDTO contrato = new ConsultarListaContratosPessoasSaidaDTO();

	/** Atributo participante. */
	private ListarEnderecoParticipanteOcorrenciasSaidaDTO participante = new ListarEnderecoParticipanteOcorrenciasSaidaDTO();

	/** Atributo manutencao. */
	private DatalharEnderecoParticipanteSaidaDTO manutencao = new DatalharEnderecoParticipanteSaidaDTO();

	/**
	 * Detalhe endereco dto.
	 *
	 * @param cliente the cliente
	 * @param contrato the contrato
	 */
	public DetalheEnderecoDTO(ConsultarListaClientePessoasSaidaDTO cliente, ConsultarListaContratosPessoasSaidaDTO contrato) {
		super();
		this.cliente = cliente;
		this.contrato = contrato;
	}

	/**
	 * Detalhe endereco dto.
	 *
	 * @param cliente the cliente
	 * @param contrato the contrato
	 * @param participante the participante
	 */
	public DetalheEnderecoDTO(ConsultarListaClientePessoasSaidaDTO cliente, ConsultarListaContratosPessoasSaidaDTO contrato, ListarEnderecoParticipanteOcorrenciasSaidaDTO participante) {
		super();
		this.cliente = cliente;
		this.contrato = contrato;
		this.participante = participante;
	}

	/**
	 * Detalhe endereco dto.
	 *
	 * @param cliente the cliente
	 * @param contrato the contrato
	 * @param participante the participante
	 * @param manutencao the manutencao
	 */
	public DetalheEnderecoDTO(ConsultarListaClientePessoasSaidaDTO cliente, ConsultarListaContratosPessoasSaidaDTO contrato, ListarEnderecoParticipanteOcorrenciasSaidaDTO participante, DatalharEnderecoParticipanteSaidaDTO manutencao) {
		super();
		this.cliente = cliente;
		this.contrato = contrato;
		this.participante = participante;
		this.manutencao = manutencao;
	}

	/**
	 * Detalhe endereco dto.
	 */
	public DetalheEnderecoDTO() {
		super();
	}

	/**
	 * Get: cliente.
	 *
	 * @return cliente
	 */
	public ConsultarListaClientePessoasSaidaDTO getCliente() {
		return cliente;
	}

	/**
	 * Set: cliente.
	 *
	 * @param cliente the cliente
	 */
	public void setCliente(ConsultarListaClientePessoasSaidaDTO cliente) {
		this.cliente = cliente;
	}

	/**
	 * Get: contrato.
	 *
	 * @return contrato
	 */
	public ConsultarListaContratosPessoasSaidaDTO getContrato() {
		return contrato;
	}

	/**
	 * Set: contrato.
	 *
	 * @param contrato the contrato
	 */
	public void setContrato(ConsultarListaContratosPessoasSaidaDTO contrato) {
		this.contrato = contrato;
	}

	/**
	 * Get: manutencao.
	 *
	 * @return manutencao
	 */
	public DatalharEnderecoParticipanteSaidaDTO getManutencao() {
		return manutencao;
	}

	/**
	 * Set: manutencao.
	 *
	 * @param manutencao the manutencao
	 */
	public void setManutencao(DatalharEnderecoParticipanteSaidaDTO manutencao) {
		this.manutencao = manutencao;
	}

	/**
	 * Get: participante.
	 *
	 * @return participante
	 */
	public ListarEnderecoParticipanteOcorrenciasSaidaDTO getParticipante() {
		return participante;
	}

	/**
	 * Set: participante.
	 *
	 * @param participante the participante
	 */
	public void setParticipante(ListarEnderecoParticipanteOcorrenciasSaidaDTO participante) {
		this.participante = participante;
	}

}
