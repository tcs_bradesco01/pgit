/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaorecuperacao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarPagtosSolicitacaoRecuperacaoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarPagtosSolicitacaoRecuperacaoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _dsBancoDebito
     */
    private java.lang.String _dsBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdDigitoAgenciaDebito
     */
    private java.lang.String _cdDigitoAgenciaDebito;

    /**
     * Field _dsAgenciaDebito
     */
    private java.lang.String _dsAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _cdDigitoContaDebito
     */
    private java.lang.String _cdDigitoContaDebito;

    /**
     * Field _cdTipoContaDebito
     */
    private int _cdTipoContaDebito = 0;

    /**
     * keeps track of state for field: _cdTipoContaDebito
     */
    private boolean _has_cdTipoContaDebito;

    /**
     * Field _dsContaDebito
     */
    private java.lang.String _dsContaDebito;

    /**
     * Field _dtInicioPeriodoPesquisa
     */
    private java.lang.String _dtInicioPeriodoPesquisa;

    /**
     * Field _dtFimPeriodoPesquisa
     */
    private java.lang.String _dtFimPeriodoPesquisa;

    /**
     * Field _cdProduto
     */
    private int _cdProduto = 0;

    /**
     * keeps track of state for field: _cdProduto
     */
    private boolean _has_cdProduto;

    /**
     * Field _cdProdutoRelacionado
     */
    private int _cdProdutoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoRelacionado
     */
    private boolean _has_cdProdutoRelacionado;

    /**
     * Field _nrSequenciaArquivoRemessa
     */
    private long _nrSequenciaArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrSequenciaArquivoRemessa
     */
    private boolean _has_nrSequenciaArquivoRemessa;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _dsPessoas
     */
    private java.lang.String _dsPessoas;

    /**
     * Field _cdControlePagamentoInicial
     */
    private java.lang.String _cdControlePagamentoInicial;

    /**
     * Field _cdControlePagamentoFinal
     */
    private java.lang.String _cdControlePagamentoFinal;

    /**
     * Field _vlrInicio
     */
    private java.math.BigDecimal _vlrInicio = new java.math.BigDecimal("0");

    /**
     * Field _vlFim
     */
    private java.math.BigDecimal _vlFim = new java.math.BigDecimal("0");

    /**
     * Field _cdSituacaoPagamento
     */
    private int _cdSituacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdSituacaoPagamento
     */
    private boolean _has_cdSituacaoPagamento;

    /**
     * Field _dsSituacaoPagamento
     */
    private java.lang.String _dsSituacaoPagamento;

    /**
     * Field _cdMotivoSituacaoPagamento
     */
    private int _cdMotivoSituacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoPagamento
     */
    private boolean _has_cdMotivoSituacaoPagamento;

    /**
     * Field _dsMotivoSituacaoPagamento
     */
    private java.lang.String _dsMotivoSituacaoPagamento;

    /**
     * Field _linhaDigitavel
     */
    private java.lang.String _linhaDigitavel;

    /**
     * Field _cdFavorecidoCliente
     */
    private long _cdFavorecidoCliente = 0;

    /**
     * keeps track of state for field: _cdFavorecidoCliente
     */
    private boolean _has_cdFavorecidoCliente;

    /**
     * Field _cdTipoInscricaoRecebedor
     */
    private int _cdTipoInscricaoRecebedor = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoRecebedor
     */
    private boolean _has_cdTipoInscricaoRecebedor;

    /**
     * Field _cdInscricaoRecebedor
     */
    private long _cdInscricaoRecebedor = 0;

    /**
     * keeps track of state for field: _cdInscricaoRecebedor
     */
    private boolean _has_cdInscricaoRecebedor;

    /**
     * Field _cdCpfCnpjRecebedor
     */
    private long _cdCpfCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjRecebedor
     */
    private boolean _has_cdCpfCnpjRecebedor;

    /**
     * Field _cdFilialCnpjRecebedor
     */
    private int _cdFilialCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjRecebedor
     */
    private boolean _has_cdFilialCnpjRecebedor;

    /**
     * Field _cdControleCpfRecebedor
     */
    private int _cdControleCpfRecebedor = 0;

    /**
     * keeps track of state for field: _cdControleCpfRecebedor
     */
    private boolean _has_cdControleCpfRecebedor;

    /**
     * Field _cdInscricaoFavorecido
     */
    private long _cdInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecido
     */
    private boolean _has_cdInscricaoFavorecido;

    /**
     * Field _dsNomeFavorecido
     */
    private java.lang.String _dsNomeFavorecido;

    /**
     * Field _cdBancoCredito
     */
    private int _cdBancoCredito = 0;

    /**
     * keeps track of state for field: _cdBancoCredito
     */
    private boolean _has_cdBancoCredito;

    /**
     * Field _dsBancoCredito
     */
    private java.lang.String _dsBancoCredito;

    /**
     * Field _cdAgenciaCredito
     */
    private int _cdAgenciaCredito = 0;

    /**
     * keeps track of state for field: _cdAgenciaCredito
     */
    private boolean _has_cdAgenciaCredito;

    /**
     * Field _cdDigitoAgenciaCredito
     */
    private java.lang.String _cdDigitoAgenciaCredito;

    /**
     * Field _dsAgenciaCredito
     */
    private java.lang.String _dsAgenciaCredito;

    /**
     * Field _cdContaCredito
     */
    private long _cdContaCredito = 0;

    /**
     * keeps track of state for field: _cdContaCredito
     */
    private boolean _has_cdContaCredito;

    /**
     * Field _digitoContaCredito
     */
    private java.lang.String _digitoContaCredito;

    /**
     * Field _cdTipoContaCredito
     */
    private int _cdTipoContaCredito = 0;

    /**
     * keeps track of state for field: _cdTipoContaCredito
     */
    private boolean _has_cdTipoContaCredito;

    /**
     * Field _dsContaCredito
     */
    private java.lang.String _dsContaCredito;

    /**
     * Field _dtRecuperacaoPagamento
     */
    private java.lang.String _dtRecuperacaoPagamento;

    /**
     * Field _dtNovoVencimentoPagamento
     */
    private java.lang.String _dtNovoVencimentoPagamento;

    /**
     * Field _vlTarifaPadrao
     */
    private java.math.BigDecimal _vlTarifaPadrao = new java.math.BigDecimal("0");

    /**
     * Field _vlTarifaNegociacao
     */
    private java.math.BigDecimal _vlTarifaNegociacao = new java.math.BigDecimal("0");

    /**
     * Field _percentualBonificacaoTarifa
     */
    private java.math.BigDecimal _percentualBonificacaoTarifa = new java.math.BigDecimal("0");

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAcaoManutencao
     */
    private int _cdAcaoManutencao = 0;

    /**
     * keeps track of state for field: _cdAcaoManutencao
     */
    private boolean _has_cdAcaoManutencao;

    /**
     * Field _eCbcoSalario
     */
    private int _eCbcoSalario = 0;

    /**
     * keeps track of state for field: _eCbcoSalario
     */
    private boolean _has_eCbcoSalario;

    /**
     * Field _dsCbcoSalario
     */
    private java.lang.String _dsCbcoSalario;

    /**
     * Field _eAgenciaSalario
     */
    private int _eAgenciaSalario = 0;

    /**
     * keeps track of state for field: _eAgenciaSalario
     */
    private boolean _has_eAgenciaSalario;

    /**
     * Field _dsAgenciaSalario
     */
    private java.lang.String _dsAgenciaSalario;

    /**
     * Field _eContaSalario
     */
    private long _eContaSalario = 0;

    /**
     * keeps track of state for field: _eContaSalario
     */
    private boolean _has_eContaSalario;

    /**
     * Field _eDigContaSalario
     */
    private java.lang.String _eDigContaSalario;

    /**
     * Field _eDestPgtoRecup
     */
    private int _eDestPgtoRecup = 0;

    /**
     * keeps track of state for field: _eDestPgtoRecup
     */
    private boolean _has_eDestPgtoRecup;

    /**
     * Field _eLoteInterno
     */
    private long _eLoteInterno = 0;

    /**
     * keeps track of state for field: _eLoteInterno
     */
    private boolean _has_eLoteInterno;

    /**
     * Field _eCbcoPgto
     */
    private int _eCbcoPgto = 0;

    /**
     * keeps track of state for field: _eCbcoPgto
     */
    private boolean _has_eCbcoPgto;

    /**
     * Field _eIspbPgtoDest
     */
    private java.lang.String _eIspbPgtoDest;

    /**
     * Field _eContaPgtoDest
     */
    private long _eContaPgtoDest = 0;

    /**
     * keeps track of state for field: _eContaPgtoDest
     */
    private boolean _has_eContaPgtoDest;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarPagtosSolicitacaoRecuperacaoResponse() 
     {
        super();
        setVlrInicio(new java.math.BigDecimal("0"));
        setVlFim(new java.math.BigDecimal("0"));
        setVlTarifaPadrao(new java.math.BigDecimal("0"));
        setVlTarifaNegociacao(new java.math.BigDecimal("0"));
        setPercentualBonificacaoTarifa(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaorecuperacao.response.ConsultarPagtosSolicitacaoRecuperacaoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAcaoManutencao
     * 
     */
    public void deleteCdAcaoManutencao()
    {
        this._has_cdAcaoManutencao= false;
    } //-- void deleteCdAcaoManutencao() 

    /**
     * Method deleteCdAgenciaCredito
     * 
     */
    public void deleteCdAgenciaCredito()
    {
        this._has_cdAgenciaCredito= false;
    } //-- void deleteCdAgenciaCredito() 

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdBancoCredito
     * 
     */
    public void deleteCdBancoCredito()
    {
        this._has_cdBancoCredito= false;
    } //-- void deleteCdBancoCredito() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdContaCredito
     * 
     */
    public void deleteCdContaCredito()
    {
        this._has_cdContaCredito= false;
    } //-- void deleteCdContaCredito() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdControleCpfRecebedor
     * 
     */
    public void deleteCdControleCpfRecebedor()
    {
        this._has_cdControleCpfRecebedor= false;
    } //-- void deleteCdControleCpfRecebedor() 

    /**
     * Method deleteCdCpfCnpjRecebedor
     * 
     */
    public void deleteCdCpfCnpjRecebedor()
    {
        this._has_cdCpfCnpjRecebedor= false;
    } //-- void deleteCdCpfCnpjRecebedor() 

    /**
     * Method deleteCdFavorecidoCliente
     * 
     */
    public void deleteCdFavorecidoCliente()
    {
        this._has_cdFavorecidoCliente= false;
    } //-- void deleteCdFavorecidoCliente() 

    /**
     * Method deleteCdFilialCnpjRecebedor
     * 
     */
    public void deleteCdFilialCnpjRecebedor()
    {
        this._has_cdFilialCnpjRecebedor= false;
    } //-- void deleteCdFilialCnpjRecebedor() 

    /**
     * Method deleteCdInscricaoFavorecido
     * 
     */
    public void deleteCdInscricaoFavorecido()
    {
        this._has_cdInscricaoFavorecido= false;
    } //-- void deleteCdInscricaoFavorecido() 

    /**
     * Method deleteCdInscricaoRecebedor
     * 
     */
    public void deleteCdInscricaoRecebedor()
    {
        this._has_cdInscricaoRecebedor= false;
    } //-- void deleteCdInscricaoRecebedor() 

    /**
     * Method deleteCdMotivoSituacaoPagamento
     * 
     */
    public void deleteCdMotivoSituacaoPagamento()
    {
        this._has_cdMotivoSituacaoPagamento= false;
    } //-- void deleteCdMotivoSituacaoPagamento() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdProduto
     * 
     */
    public void deleteCdProduto()
    {
        this._has_cdProduto= false;
    } //-- void deleteCdProduto() 

    /**
     * Method deleteCdProdutoRelacionado
     * 
     */
    public void deleteCdProdutoRelacionado()
    {
        this._has_cdProdutoRelacionado= false;
    } //-- void deleteCdProdutoRelacionado() 

    /**
     * Method deleteCdSituacaoPagamento
     * 
     */
    public void deleteCdSituacaoPagamento()
    {
        this._has_cdSituacaoPagamento= false;
    } //-- void deleteCdSituacaoPagamento() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContaCredito
     * 
     */
    public void deleteCdTipoContaCredito()
    {
        this._has_cdTipoContaCredito= false;
    } //-- void deleteCdTipoContaCredito() 

    /**
     * Method deleteCdTipoContaDebito
     * 
     */
    public void deleteCdTipoContaDebito()
    {
        this._has_cdTipoContaDebito= false;
    } //-- void deleteCdTipoContaDebito() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoInscricaoRecebedor
     * 
     */
    public void deleteCdTipoInscricaoRecebedor()
    {
        this._has_cdTipoInscricaoRecebedor= false;
    } //-- void deleteCdTipoInscricaoRecebedor() 

    /**
     * Method deleteEAgenciaSalario
     * 
     */
    public void deleteEAgenciaSalario()
    {
        this._has_eAgenciaSalario= false;
    } //-- void deleteEAgenciaSalario() 

    /**
     * Method deleteECbcoPgto
     * 
     */
    public void deleteECbcoPgto()
    {
        this._has_eCbcoPgto= false;
    } //-- void deleteECbcoPgto() 

    /**
     * Method deleteECbcoSalario
     * 
     */
    public void deleteECbcoSalario()
    {
        this._has_eCbcoSalario= false;
    } //-- void deleteECbcoSalario() 

    /**
     * Method deleteEContaPgtoDest
     * 
     */
    public void deleteEContaPgtoDest()
    {
        this._has_eContaPgtoDest= false;
    } //-- void deleteEContaPgtoDest() 

    /**
     * Method deleteEContaSalario
     * 
     */
    public void deleteEContaSalario()
    {
        this._has_eContaSalario= false;
    } //-- void deleteEContaSalario() 

    /**
     * Method deleteEDestPgtoRecup
     * 
     */
    public void deleteEDestPgtoRecup()
    {
        this._has_eDestPgtoRecup= false;
    } //-- void deleteEDestPgtoRecup() 

    /**
     * Method deleteELoteInterno
     * 
     */
    public void deleteELoteInterno()
    {
        this._has_eLoteInterno= false;
    } //-- void deleteELoteInterno() 

    /**
     * Method deleteNrSequenciaArquivoRemessa
     * 
     */
    public void deleteNrSequenciaArquivoRemessa()
    {
        this._has_nrSequenciaArquivoRemessa= false;
    } //-- void deleteNrSequenciaArquivoRemessa() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAcaoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdAcaoManutencao'.
     */
    public int getCdAcaoManutencao()
    {
        return this._cdAcaoManutencao;
    } //-- int getCdAcaoManutencao() 

    /**
     * Returns the value of field 'cdAgenciaCredito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaCredito'.
     */
    public int getCdAgenciaCredito()
    {
        return this._cdAgenciaCredito;
    } //-- int getCdAgenciaCredito() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdBancoCredito'.
     * 
     * @return int
     * @return the value of field 'cdBancoCredito'.
     */
    public int getCdBancoCredito()
    {
        return this._cdBancoCredito;
    } //-- int getCdBancoCredito() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdContaCredito'.
     * 
     * @return long
     * @return the value of field 'cdContaCredito'.
     */
    public long getCdContaCredito()
    {
        return this._cdContaCredito;
    } //-- long getCdContaCredito() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdControleCpfRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfRecebedor'.
     */
    public int getCdControleCpfRecebedor()
    {
        return this._cdControleCpfRecebedor;
    } //-- int getCdControleCpfRecebedor() 

    /**
     * Returns the value of field 'cdControlePagamentoFinal'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamentoFinal'.
     */
    public java.lang.String getCdControlePagamentoFinal()
    {
        return this._cdControlePagamentoFinal;
    } //-- java.lang.String getCdControlePagamentoFinal() 

    /**
     * Returns the value of field 'cdControlePagamentoInicial'.
     * 
     * @return String
     * @return the value of field 'cdControlePagamentoInicial'.
     */
    public java.lang.String getCdControlePagamentoInicial()
    {
        return this._cdControlePagamentoInicial;
    } //-- java.lang.String getCdControlePagamentoInicial() 

    /**
     * Returns the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjRecebedor'.
     */
    public long getCdCpfCnpjRecebedor()
    {
        return this._cdCpfCnpjRecebedor;
    } //-- long getCdCpfCnpjRecebedor() 

    /**
     * Returns the value of field 'cdDigitoAgenciaCredito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoAgenciaCredito'.
     */
    public java.lang.String getCdDigitoAgenciaCredito()
    {
        return this._cdDigitoAgenciaCredito;
    } //-- java.lang.String getCdDigitoAgenciaCredito() 

    /**
     * Returns the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoAgenciaDebito'.
     */
    public java.lang.String getCdDigitoAgenciaDebito()
    {
        return this._cdDigitoAgenciaDebito;
    } //-- java.lang.String getCdDigitoAgenciaDebito() 

    /**
     * Returns the value of field 'cdDigitoContaDebito'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaDebito'.
     */
    public java.lang.String getCdDigitoContaDebito()
    {
        return this._cdDigitoContaDebito;
    } //-- java.lang.String getCdDigitoContaDebito() 

    /**
     * Returns the value of field 'cdFavorecidoCliente'.
     * 
     * @return long
     * @return the value of field 'cdFavorecidoCliente'.
     */
    public long getCdFavorecidoCliente()
    {
        return this._cdFavorecidoCliente;
    } //-- long getCdFavorecidoCliente() 

    /**
     * Returns the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjRecebedor'.
     */
    public int getCdFilialCnpjRecebedor()
    {
        return this._cdFilialCnpjRecebedor;
    } //-- int getCdFilialCnpjRecebedor() 

    /**
     * Returns the value of field 'cdInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecido'.
     */
    public long getCdInscricaoFavorecido()
    {
        return this._cdInscricaoFavorecido;
    } //-- long getCdInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdInscricaoRecebedor'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoRecebedor'.
     */
    public long getCdInscricaoRecebedor()
    {
        return this._cdInscricaoRecebedor;
    } //-- long getCdInscricaoRecebedor() 

    /**
     * Returns the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoPagamento'.
     */
    public int getCdMotivoSituacaoPagamento()
    {
        return this._cdMotivoSituacaoPagamento;
    } //-- int getCdMotivoSituacaoPagamento() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdProduto'.
     * 
     * @return int
     * @return the value of field 'cdProduto'.
     */
    public int getCdProduto()
    {
        return this._cdProduto;
    } //-- int getCdProduto() 

    /**
     * Returns the value of field 'cdProdutoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoRelacionado'.
     */
    public int getCdProdutoRelacionado()
    {
        return this._cdProdutoRelacionado;
    } //-- int getCdProdutoRelacionado() 

    /**
     * Returns the value of field 'cdSituacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoPagamento'.
     */
    public int getCdSituacaoPagamento()
    {
        return this._cdSituacaoPagamento;
    } //-- int getCdSituacaoPagamento() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContaCredito'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaCredito'.
     */
    public int getCdTipoContaCredito()
    {
        return this._cdTipoContaCredito;
    } //-- int getCdTipoContaCredito() 

    /**
     * Returns the value of field 'cdTipoContaDebito'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaDebito'.
     */
    public int getCdTipoContaDebito()
    {
        return this._cdTipoContaDebito;
    } //-- int getCdTipoContaDebito() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoInscricaoRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoRecebedor'.
     */
    public int getCdTipoInscricaoRecebedor()
    {
        return this._cdTipoInscricaoRecebedor;
    } //-- int getCdTipoInscricaoRecebedor() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'digitoContaCredito'.
     * 
     * @return String
     * @return the value of field 'digitoContaCredito'.
     */
    public java.lang.String getDigitoContaCredito()
    {
        return this._digitoContaCredito;
    } //-- java.lang.String getDigitoContaCredito() 

    /**
     * Returns the value of field 'dsAgenciaCredito'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaCredito'.
     */
    public java.lang.String getDsAgenciaCredito()
    {
        return this._dsAgenciaCredito;
    } //-- java.lang.String getDsAgenciaCredito() 

    /**
     * Returns the value of field 'dsAgenciaDebito'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaDebito'.
     */
    public java.lang.String getDsAgenciaDebito()
    {
        return this._dsAgenciaDebito;
    } //-- java.lang.String getDsAgenciaDebito() 

    /**
     * Returns the value of field 'dsAgenciaSalario'.
     * 
     * @return String
     * @return the value of field 'dsAgenciaSalario'.
     */
    public java.lang.String getDsAgenciaSalario()
    {
        return this._dsAgenciaSalario;
    } //-- java.lang.String getDsAgenciaSalario() 

    /**
     * Returns the value of field 'dsBancoCredito'.
     * 
     * @return String
     * @return the value of field 'dsBancoCredito'.
     */
    public java.lang.String getDsBancoCredito()
    {
        return this._dsBancoCredito;
    } //-- java.lang.String getDsBancoCredito() 

    /**
     * Returns the value of field 'dsBancoDebito'.
     * 
     * @return String
     * @return the value of field 'dsBancoDebito'.
     */
    public java.lang.String getDsBancoDebito()
    {
        return this._dsBancoDebito;
    } //-- java.lang.String getDsBancoDebito() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsCbcoSalario'.
     * 
     * @return String
     * @return the value of field 'dsCbcoSalario'.
     */
    public java.lang.String getDsCbcoSalario()
    {
        return this._dsCbcoSalario;
    } //-- java.lang.String getDsCbcoSalario() 

    /**
     * Returns the value of field 'dsContaCredito'.
     * 
     * @return String
     * @return the value of field 'dsContaCredito'.
     */
    public java.lang.String getDsContaCredito()
    {
        return this._dsContaCredito;
    } //-- java.lang.String getDsContaCredito() 

    /**
     * Returns the value of field 'dsContaDebito'.
     * 
     * @return String
     * @return the value of field 'dsContaDebito'.
     */
    public java.lang.String getDsContaDebito()
    {
        return this._dsContaDebito;
    } //-- java.lang.String getDsContaDebito() 

    /**
     * Returns the value of field 'dsMotivoSituacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsMotivoSituacaoPagamento'.
     */
    public java.lang.String getDsMotivoSituacaoPagamento()
    {
        return this._dsMotivoSituacaoPagamento;
    } //-- java.lang.String getDsMotivoSituacaoPagamento() 

    /**
     * Returns the value of field 'dsNomeFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsNomeFavorecido'.
     */
    public java.lang.String getDsNomeFavorecido()
    {
        return this._dsNomeFavorecido;
    } //-- java.lang.String getDsNomeFavorecido() 

    /**
     * Returns the value of field 'dsPessoas'.
     * 
     * @return String
     * @return the value of field 'dsPessoas'.
     */
    public java.lang.String getDsPessoas()
    {
        return this._dsPessoas;
    } //-- java.lang.String getDsPessoas() 

    /**
     * Returns the value of field 'dsSituacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoPagamento'.
     */
    public java.lang.String getDsSituacaoPagamento()
    {
        return this._dsSituacaoPagamento;
    } //-- java.lang.String getDsSituacaoPagamento() 

    /**
     * Returns the value of field 'dtFimPeriodoPesquisa'.
     * 
     * @return String
     * @return the value of field 'dtFimPeriodoPesquisa'.
     */
    public java.lang.String getDtFimPeriodoPesquisa()
    {
        return this._dtFimPeriodoPesquisa;
    } //-- java.lang.String getDtFimPeriodoPesquisa() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtInicioPeriodoPesquisa'.
     * 
     * @return String
     * @return the value of field 'dtInicioPeriodoPesquisa'.
     */
    public java.lang.String getDtInicioPeriodoPesquisa()
    {
        return this._dtInicioPeriodoPesquisa;
    } //-- java.lang.String getDtInicioPeriodoPesquisa() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'dtNovoVencimentoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtNovoVencimentoPagamento'.
     */
    public java.lang.String getDtNovoVencimentoPagamento()
    {
        return this._dtNovoVencimentoPagamento;
    } //-- java.lang.String getDtNovoVencimentoPagamento() 

    /**
     * Returns the value of field 'dtRecuperacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dtRecuperacaoPagamento'.
     */
    public java.lang.String getDtRecuperacaoPagamento()
    {
        return this._dtRecuperacaoPagamento;
    } //-- java.lang.String getDtRecuperacaoPagamento() 

    /**
     * Returns the value of field 'eAgenciaSalario'.
     * 
     * @return int
     * @return the value of field 'eAgenciaSalario'.
     */
    public int getEAgenciaSalario()
    {
        return this._eAgenciaSalario;
    } //-- int getEAgenciaSalario() 

    /**
     * Returns the value of field 'eCbcoPgto'.
     * 
     * @return int
     * @return the value of field 'eCbcoPgto'.
     */
    public int getECbcoPgto()
    {
        return this._eCbcoPgto;
    } //-- int getECbcoPgto() 

    /**
     * Returns the value of field 'eCbcoSalario'.
     * 
     * @return int
     * @return the value of field 'eCbcoSalario'.
     */
    public int getECbcoSalario()
    {
        return this._eCbcoSalario;
    } //-- int getECbcoSalario() 

    /**
     * Returns the value of field 'eContaPgtoDest'.
     * 
     * @return long
     * @return the value of field 'eContaPgtoDest'.
     */
    public long getEContaPgtoDest()
    {
        return this._eContaPgtoDest;
    } //-- long getEContaPgtoDest() 

    /**
     * Returns the value of field 'eContaSalario'.
     * 
     * @return long
     * @return the value of field 'eContaSalario'.
     */
    public long getEContaSalario()
    {
        return this._eContaSalario;
    } //-- long getEContaSalario() 

    /**
     * Returns the value of field 'eDestPgtoRecup'.
     * 
     * @return int
     * @return the value of field 'eDestPgtoRecup'.
     */
    public int getEDestPgtoRecup()
    {
        return this._eDestPgtoRecup;
    } //-- int getEDestPgtoRecup() 

    /**
     * Returns the value of field 'eDigContaSalario'.
     * 
     * @return String
     * @return the value of field 'eDigContaSalario'.
     */
    public java.lang.String getEDigContaSalario()
    {
        return this._eDigContaSalario;
    } //-- java.lang.String getEDigContaSalario() 

    /**
     * Returns the value of field 'eIspbPgtoDest'.
     * 
     * @return String
     * @return the value of field 'eIspbPgtoDest'.
     */
    public java.lang.String getEIspbPgtoDest()
    {
        return this._eIspbPgtoDest;
    } //-- java.lang.String getEIspbPgtoDest() 

    /**
     * Returns the value of field 'eLoteInterno'.
     * 
     * @return long
     * @return the value of field 'eLoteInterno'.
     */
    public long getELoteInterno()
    {
        return this._eLoteInterno;
    } //-- long getELoteInterno() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'linhaDigitavel'.
     * 
     * @return String
     * @return the value of field 'linhaDigitavel'.
     */
    public java.lang.String getLinhaDigitavel()
    {
        return this._linhaDigitavel;
    } //-- java.lang.String getLinhaDigitavel() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrSequenciaArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaArquivoRemessa'.
     */
    public long getNrSequenciaArquivoRemessa()
    {
        return this._nrSequenciaArquivoRemessa;
    } //-- long getNrSequenciaArquivoRemessa() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'percentualBonificacaoTarifa'.
     * 
     * @return BigDecimal
     * @return the value of field 'percentualBonificacaoTarifa'.
     */
    public java.math.BigDecimal getPercentualBonificacaoTarifa()
    {
        return this._percentualBonificacaoTarifa;
    } //-- java.math.BigDecimal getPercentualBonificacaoTarifa() 

    /**
     * Returns the value of field 'vlFim'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlFim'.
     */
    public java.math.BigDecimal getVlFim()
    {
        return this._vlFim;
    } //-- java.math.BigDecimal getVlFim() 

    /**
     * Returns the value of field 'vlTarifaNegociacao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaNegociacao'.
     */
    public java.math.BigDecimal getVlTarifaNegociacao()
    {
        return this._vlTarifaNegociacao;
    } //-- java.math.BigDecimal getVlTarifaNegociacao() 

    /**
     * Returns the value of field 'vlTarifaPadrao'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTarifaPadrao'.
     */
    public java.math.BigDecimal getVlTarifaPadrao()
    {
        return this._vlTarifaPadrao;
    } //-- java.math.BigDecimal getVlTarifaPadrao() 

    /**
     * Returns the value of field 'vlrInicio'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrInicio'.
     */
    public java.math.BigDecimal getVlrInicio()
    {
        return this._vlrInicio;
    } //-- java.math.BigDecimal getVlrInicio() 

    /**
     * Method hasCdAcaoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAcaoManutencao()
    {
        return this._has_cdAcaoManutencao;
    } //-- boolean hasCdAcaoManutencao() 

    /**
     * Method hasCdAgenciaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaCredito()
    {
        return this._has_cdAgenciaCredito;
    } //-- boolean hasCdAgenciaCredito() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdBancoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoCredito()
    {
        return this._has_cdBancoCredito;
    } //-- boolean hasCdBancoCredito() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaCredito()
    {
        return this._has_cdContaCredito;
    } //-- boolean hasCdContaCredito() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdControleCpfRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfRecebedor()
    {
        return this._has_cdControleCpfRecebedor;
    } //-- boolean hasCdControleCpfRecebedor() 

    /**
     * Method hasCdCpfCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjRecebedor()
    {
        return this._has_cdCpfCnpjRecebedor;
    } //-- boolean hasCdCpfCnpjRecebedor() 

    /**
     * Method hasCdFavorecidoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoCliente()
    {
        return this._has_cdFavorecidoCliente;
    } //-- boolean hasCdFavorecidoCliente() 

    /**
     * Method hasCdFilialCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjRecebedor()
    {
        return this._has_cdFilialCnpjRecebedor;
    } //-- boolean hasCdFilialCnpjRecebedor() 

    /**
     * Method hasCdInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecido()
    {
        return this._has_cdInscricaoFavorecido;
    } //-- boolean hasCdInscricaoFavorecido() 

    /**
     * Method hasCdInscricaoRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoRecebedor()
    {
        return this._has_cdInscricaoRecebedor;
    } //-- boolean hasCdInscricaoRecebedor() 

    /**
     * Method hasCdMotivoSituacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoPagamento()
    {
        return this._has_cdMotivoSituacaoPagamento;
    } //-- boolean hasCdMotivoSituacaoPagamento() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProduto()
    {
        return this._has_cdProduto;
    } //-- boolean hasCdProduto() 

    /**
     * Method hasCdProdutoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoRelacionado()
    {
        return this._has_cdProdutoRelacionado;
    } //-- boolean hasCdProdutoRelacionado() 

    /**
     * Method hasCdSituacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoPagamento()
    {
        return this._has_cdSituacaoPagamento;
    } //-- boolean hasCdSituacaoPagamento() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContaCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaCredito()
    {
        return this._has_cdTipoContaCredito;
    } //-- boolean hasCdTipoContaCredito() 

    /**
     * Method hasCdTipoContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaDebito()
    {
        return this._has_cdTipoContaDebito;
    } //-- boolean hasCdTipoContaDebito() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoInscricaoRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoRecebedor()
    {
        return this._has_cdTipoInscricaoRecebedor;
    } //-- boolean hasCdTipoInscricaoRecebedor() 

    /**
     * Method hasEAgenciaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasEAgenciaSalario()
    {
        return this._has_eAgenciaSalario;
    } //-- boolean hasEAgenciaSalario() 

    /**
     * Method hasECbcoPgto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasECbcoPgto()
    {
        return this._has_eCbcoPgto;
    } //-- boolean hasECbcoPgto() 

    /**
     * Method hasECbcoSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasECbcoSalario()
    {
        return this._has_eCbcoSalario;
    } //-- boolean hasECbcoSalario() 

    /**
     * Method hasEContaPgtoDest
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasEContaPgtoDest()
    {
        return this._has_eContaPgtoDest;
    } //-- boolean hasEContaPgtoDest() 

    /**
     * Method hasEContaSalario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasEContaSalario()
    {
        return this._has_eContaSalario;
    } //-- boolean hasEContaSalario() 

    /**
     * Method hasEDestPgtoRecup
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasEDestPgtoRecup()
    {
        return this._has_eDestPgtoRecup;
    } //-- boolean hasEDestPgtoRecup() 

    /**
     * Method hasELoteInterno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasELoteInterno()
    {
        return this._has_eLoteInterno;
    } //-- boolean hasELoteInterno() 

    /**
     * Method hasNrSequenciaArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaArquivoRemessa()
    {
        return this._has_nrSequenciaArquivoRemessa;
    } //-- boolean hasNrSequenciaArquivoRemessa() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAcaoManutencao'.
     * 
     * @param cdAcaoManutencao the value of field 'cdAcaoManutencao'
     */
    public void setCdAcaoManutencao(int cdAcaoManutencao)
    {
        this._cdAcaoManutencao = cdAcaoManutencao;
        this._has_cdAcaoManutencao = true;
    } //-- void setCdAcaoManutencao(int) 

    /**
     * Sets the value of field 'cdAgenciaCredito'.
     * 
     * @param cdAgenciaCredito the value of field 'cdAgenciaCredito'
     */
    public void setCdAgenciaCredito(int cdAgenciaCredito)
    {
        this._cdAgenciaCredito = cdAgenciaCredito;
        this._has_cdAgenciaCredito = true;
    } //-- void setCdAgenciaCredito(int) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdBancoCredito'.
     * 
     * @param cdBancoCredito the value of field 'cdBancoCredito'.
     */
    public void setCdBancoCredito(int cdBancoCredito)
    {
        this._cdBancoCredito = cdBancoCredito;
        this._has_cdBancoCredito = true;
    } //-- void setCdBancoCredito(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdContaCredito'.
     * 
     * @param cdContaCredito the value of field 'cdContaCredito'.
     */
    public void setCdContaCredito(long cdContaCredito)
    {
        this._cdContaCredito = cdContaCredito;
        this._has_cdContaCredito = true;
    } //-- void setCdContaCredito(long) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdControleCpfRecebedor'.
     * 
     * @param cdControleCpfRecebedor the value of field
     * 'cdControleCpfRecebedor'.
     */
    public void setCdControleCpfRecebedor(int cdControleCpfRecebedor)
    {
        this._cdControleCpfRecebedor = cdControleCpfRecebedor;
        this._has_cdControleCpfRecebedor = true;
    } //-- void setCdControleCpfRecebedor(int) 

    /**
     * Sets the value of field 'cdControlePagamentoFinal'.
     * 
     * @param cdControlePagamentoFinal the value of field
     * 'cdControlePagamentoFinal'.
     */
    public void setCdControlePagamentoFinal(java.lang.String cdControlePagamentoFinal)
    {
        this._cdControlePagamentoFinal = cdControlePagamentoFinal;
    } //-- void setCdControlePagamentoFinal(java.lang.String) 

    /**
     * Sets the value of field 'cdControlePagamentoInicial'.
     * 
     * @param cdControlePagamentoInicial the value of field
     * 'cdControlePagamentoInicial'.
     */
    public void setCdControlePagamentoInicial(java.lang.String cdControlePagamentoInicial)
    {
        this._cdControlePagamentoInicial = cdControlePagamentoInicial;
    } //-- void setCdControlePagamentoInicial(java.lang.String) 

    /**
     * Sets the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @param cdCpfCnpjRecebedor the value of field
     * 'cdCpfCnpjRecebedor'.
     */
    public void setCdCpfCnpjRecebedor(long cdCpfCnpjRecebedor)
    {
        this._cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
        this._has_cdCpfCnpjRecebedor = true;
    } //-- void setCdCpfCnpjRecebedor(long) 

    /**
     * Sets the value of field 'cdDigitoAgenciaCredito'.
     * 
     * @param cdDigitoAgenciaCredito the value of field
     * 'cdDigitoAgenciaCredito'.
     */
    public void setCdDigitoAgenciaCredito(java.lang.String cdDigitoAgenciaCredito)
    {
        this._cdDigitoAgenciaCredito = cdDigitoAgenciaCredito;
    } //-- void setCdDigitoAgenciaCredito(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoAgenciaDebito'.
     * 
     * @param cdDigitoAgenciaDebito the value of field
     * 'cdDigitoAgenciaDebito'.
     */
    public void setCdDigitoAgenciaDebito(java.lang.String cdDigitoAgenciaDebito)
    {
        this._cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
    } //-- void setCdDigitoAgenciaDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdDigitoContaDebito'.
     * 
     * @param cdDigitoContaDebito the value of field
     * 'cdDigitoContaDebito'.
     */
    public void setCdDigitoContaDebito(java.lang.String cdDigitoContaDebito)
    {
        this._cdDigitoContaDebito = cdDigitoContaDebito;
    } //-- void setCdDigitoContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdFavorecidoCliente'.
     * 
     * @param cdFavorecidoCliente the value of field
     * 'cdFavorecidoCliente'.
     */
    public void setCdFavorecidoCliente(long cdFavorecidoCliente)
    {
        this._cdFavorecidoCliente = cdFavorecidoCliente;
        this._has_cdFavorecidoCliente = true;
    } //-- void setCdFavorecidoCliente(long) 

    /**
     * Sets the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @param cdFilialCnpjRecebedor the value of field
     * 'cdFilialCnpjRecebedor'.
     */
    public void setCdFilialCnpjRecebedor(int cdFilialCnpjRecebedor)
    {
        this._cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
        this._has_cdFilialCnpjRecebedor = true;
    } //-- void setCdFilialCnpjRecebedor(int) 

    /**
     * Sets the value of field 'cdInscricaoFavorecido'.
     * 
     * @param cdInscricaoFavorecido the value of field
     * 'cdInscricaoFavorecido'.
     */
    public void setCdInscricaoFavorecido(long cdInscricaoFavorecido)
    {
        this._cdInscricaoFavorecido = cdInscricaoFavorecido;
        this._has_cdInscricaoFavorecido = true;
    } //-- void setCdInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'cdInscricaoRecebedor'.
     * 
     * @param cdInscricaoRecebedor the value of field
     * 'cdInscricaoRecebedor'.
     */
    public void setCdInscricaoRecebedor(long cdInscricaoRecebedor)
    {
        this._cdInscricaoRecebedor = cdInscricaoRecebedor;
        this._has_cdInscricaoRecebedor = true;
    } //-- void setCdInscricaoRecebedor(long) 

    /**
     * Sets the value of field 'cdMotivoSituacaoPagamento'.
     * 
     * @param cdMotivoSituacaoPagamento the value of field
     * 'cdMotivoSituacaoPagamento'.
     */
    public void setCdMotivoSituacaoPagamento(int cdMotivoSituacaoPagamento)
    {
        this._cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
        this._has_cdMotivoSituacaoPagamento = true;
    } //-- void setCdMotivoSituacaoPagamento(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdProduto'.
     * 
     * @param cdProduto the value of field 'cdProduto'.
     */
    public void setCdProduto(int cdProduto)
    {
        this._cdProduto = cdProduto;
        this._has_cdProduto = true;
    } //-- void setCdProduto(int) 

    /**
     * Sets the value of field 'cdProdutoRelacionado'.
     * 
     * @param cdProdutoRelacionado the value of field
     * 'cdProdutoRelacionado'.
     */
    public void setCdProdutoRelacionado(int cdProdutoRelacionado)
    {
        this._cdProdutoRelacionado = cdProdutoRelacionado;
        this._has_cdProdutoRelacionado = true;
    } //-- void setCdProdutoRelacionado(int) 

    /**
     * Sets the value of field 'cdSituacaoPagamento'.
     * 
     * @param cdSituacaoPagamento the value of field
     * 'cdSituacaoPagamento'.
     */
    public void setCdSituacaoPagamento(int cdSituacaoPagamento)
    {
        this._cdSituacaoPagamento = cdSituacaoPagamento;
        this._has_cdSituacaoPagamento = true;
    } //-- void setCdSituacaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContaCredito'.
     * 
     * @param cdTipoContaCredito the value of field
     * 'cdTipoContaCredito'.
     */
    public void setCdTipoContaCredito(int cdTipoContaCredito)
    {
        this._cdTipoContaCredito = cdTipoContaCredito;
        this._has_cdTipoContaCredito = true;
    } //-- void setCdTipoContaCredito(int) 

    /**
     * Sets the value of field 'cdTipoContaDebito'.
     * 
     * @param cdTipoContaDebito the value of field
     * 'cdTipoContaDebito'.
     */
    public void setCdTipoContaDebito(int cdTipoContaDebito)
    {
        this._cdTipoContaDebito = cdTipoContaDebito;
        this._has_cdTipoContaDebito = true;
    } //-- void setCdTipoContaDebito(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoRecebedor'.
     * 
     * @param cdTipoInscricaoRecebedor the value of field
     * 'cdTipoInscricaoRecebedor'.
     */
    public void setCdTipoInscricaoRecebedor(int cdTipoInscricaoRecebedor)
    {
        this._cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
        this._has_cdTipoInscricaoRecebedor = true;
    } //-- void setCdTipoInscricaoRecebedor(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'digitoContaCredito'.
     * 
     * @param digitoContaCredito the value of field
     * 'digitoContaCredito'.
     */
    public void setDigitoContaCredito(java.lang.String digitoContaCredito)
    {
        this._digitoContaCredito = digitoContaCredito;
    } //-- void setDigitoContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaCredito'.
     * 
     * @param dsAgenciaCredito the value of field 'dsAgenciaCredito'
     */
    public void setDsAgenciaCredito(java.lang.String dsAgenciaCredito)
    {
        this._dsAgenciaCredito = dsAgenciaCredito;
    } //-- void setDsAgenciaCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaDebito'.
     * 
     * @param dsAgenciaDebito the value of field 'dsAgenciaDebito'.
     */
    public void setDsAgenciaDebito(java.lang.String dsAgenciaDebito)
    {
        this._dsAgenciaDebito = dsAgenciaDebito;
    } //-- void setDsAgenciaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsAgenciaSalario'.
     * 
     * @param dsAgenciaSalario the value of field 'dsAgenciaSalario'
     */
    public void setDsAgenciaSalario(java.lang.String dsAgenciaSalario)
    {
        this._dsAgenciaSalario = dsAgenciaSalario;
    } //-- void setDsAgenciaSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoCredito'.
     * 
     * @param dsBancoCredito the value of field 'dsBancoCredito'.
     */
    public void setDsBancoCredito(java.lang.String dsBancoCredito)
    {
        this._dsBancoCredito = dsBancoCredito;
    } //-- void setDsBancoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsBancoDebito'.
     * 
     * @param dsBancoDebito the value of field 'dsBancoDebito'.
     */
    public void setDsBancoDebito(java.lang.String dsBancoDebito)
    {
        this._dsBancoDebito = dsBancoDebito;
    } //-- void setDsBancoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsCbcoSalario'.
     * 
     * @param dsCbcoSalario the value of field 'dsCbcoSalario'.
     */
    public void setDsCbcoSalario(java.lang.String dsCbcoSalario)
    {
        this._dsCbcoSalario = dsCbcoSalario;
    } //-- void setDsCbcoSalario(java.lang.String) 

    /**
     * Sets the value of field 'dsContaCredito'.
     * 
     * @param dsContaCredito the value of field 'dsContaCredito'.
     */
    public void setDsContaCredito(java.lang.String dsContaCredito)
    {
        this._dsContaCredito = dsContaCredito;
    } //-- void setDsContaCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsContaDebito'.
     * 
     * @param dsContaDebito the value of field 'dsContaDebito'.
     */
    public void setDsContaDebito(java.lang.String dsContaDebito)
    {
        this._dsContaDebito = dsContaDebito;
    } //-- void setDsContaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoSituacaoPagamento'.
     * 
     * @param dsMotivoSituacaoPagamento the value of field
     * 'dsMotivoSituacaoPagamento'.
     */
    public void setDsMotivoSituacaoPagamento(java.lang.String dsMotivoSituacaoPagamento)
    {
        this._dsMotivoSituacaoPagamento = dsMotivoSituacaoPagamento;
    } //-- void setDsMotivoSituacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeFavorecido'.
     * 
     * @param dsNomeFavorecido the value of field 'dsNomeFavorecido'
     */
    public void setDsNomeFavorecido(java.lang.String dsNomeFavorecido)
    {
        this._dsNomeFavorecido = dsNomeFavorecido;
    } //-- void setDsNomeFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoas'.
     * 
     * @param dsPessoas the value of field 'dsPessoas'.
     */
    public void setDsPessoas(java.lang.String dsPessoas)
    {
        this._dsPessoas = dsPessoas;
    } //-- void setDsPessoas(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoPagamento'.
     * 
     * @param dsSituacaoPagamento the value of field
     * 'dsSituacaoPagamento'.
     */
    public void setDsSituacaoPagamento(java.lang.String dsSituacaoPagamento)
    {
        this._dsSituacaoPagamento = dsSituacaoPagamento;
    } //-- void setDsSituacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtFimPeriodoPesquisa'.
     * 
     * @param dtFimPeriodoPesquisa the value of field
     * 'dtFimPeriodoPesquisa'.
     */
    public void setDtFimPeriodoPesquisa(java.lang.String dtFimPeriodoPesquisa)
    {
        this._dtFimPeriodoPesquisa = dtFimPeriodoPesquisa;
    } //-- void setDtFimPeriodoPesquisa(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioPeriodoPesquisa'.
     * 
     * @param dtInicioPeriodoPesquisa the value of field
     * 'dtInicioPeriodoPesquisa'.
     */
    public void setDtInicioPeriodoPesquisa(java.lang.String dtInicioPeriodoPesquisa)
    {
        this._dtInicioPeriodoPesquisa = dtInicioPeriodoPesquisa;
    } //-- void setDtInicioPeriodoPesquisa(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtNovoVencimentoPagamento'.
     * 
     * @param dtNovoVencimentoPagamento the value of field
     * 'dtNovoVencimentoPagamento'.
     */
    public void setDtNovoVencimentoPagamento(java.lang.String dtNovoVencimentoPagamento)
    {
        this._dtNovoVencimentoPagamento = dtNovoVencimentoPagamento;
    } //-- void setDtNovoVencimentoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dtRecuperacaoPagamento'.
     * 
     * @param dtRecuperacaoPagamento the value of field
     * 'dtRecuperacaoPagamento'.
     */
    public void setDtRecuperacaoPagamento(java.lang.String dtRecuperacaoPagamento)
    {
        this._dtRecuperacaoPagamento = dtRecuperacaoPagamento;
    } //-- void setDtRecuperacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'eAgenciaSalario'.
     * 
     * @param eAgenciaSalario the value of field 'eAgenciaSalario'.
     */
    public void setEAgenciaSalario(int eAgenciaSalario)
    {
        this._eAgenciaSalario = eAgenciaSalario;
        this._has_eAgenciaSalario = true;
    } //-- void setEAgenciaSalario(int) 

    /**
     * Sets the value of field 'eCbcoPgto'.
     * 
     * @param eCbcoPgto the value of field 'eCbcoPgto'.
     */
    public void setECbcoPgto(int eCbcoPgto)
    {
        this._eCbcoPgto = eCbcoPgto;
        this._has_eCbcoPgto = true;
    } //-- void setECbcoPgto(int) 

    /**
     * Sets the value of field 'eCbcoSalario'.
     * 
     * @param eCbcoSalario the value of field 'eCbcoSalario'.
     */
    public void setECbcoSalario(int eCbcoSalario)
    {
        this._eCbcoSalario = eCbcoSalario;
        this._has_eCbcoSalario = true;
    } //-- void setECbcoSalario(int) 

    /**
     * Sets the value of field 'eContaPgtoDest'.
     * 
     * @param eContaPgtoDest the value of field 'eContaPgtoDest'.
     */
    public void setEContaPgtoDest(long eContaPgtoDest)
    {
        this._eContaPgtoDest = eContaPgtoDest;
        this._has_eContaPgtoDest = true;
    } //-- void setEContaPgtoDest(long) 

    /**
     * Sets the value of field 'eContaSalario'.
     * 
     * @param eContaSalario the value of field 'eContaSalario'.
     */
    public void setEContaSalario(long eContaSalario)
    {
        this._eContaSalario = eContaSalario;
        this._has_eContaSalario = true;
    } //-- void setEContaSalario(long) 

    /**
     * Sets the value of field 'eDestPgtoRecup'.
     * 
     * @param eDestPgtoRecup the value of field 'eDestPgtoRecup'.
     */
    public void setEDestPgtoRecup(int eDestPgtoRecup)
    {
        this._eDestPgtoRecup = eDestPgtoRecup;
        this._has_eDestPgtoRecup = true;
    } //-- void setEDestPgtoRecup(int) 

    /**
     * Sets the value of field 'eDigContaSalario'.
     * 
     * @param eDigContaSalario the value of field 'eDigContaSalario'
     */
    public void setEDigContaSalario(java.lang.String eDigContaSalario)
    {
        this._eDigContaSalario = eDigContaSalario;
    } //-- void setEDigContaSalario(java.lang.String) 

    /**
     * Sets the value of field 'eIspbPgtoDest'.
     * 
     * @param eIspbPgtoDest the value of field 'eIspbPgtoDest'.
     */
    public void setEIspbPgtoDest(java.lang.String eIspbPgtoDest)
    {
        this._eIspbPgtoDest = eIspbPgtoDest;
    } //-- void setEIspbPgtoDest(java.lang.String) 

    /**
     * Sets the value of field 'eLoteInterno'.
     * 
     * @param eLoteInterno the value of field 'eLoteInterno'.
     */
    public void setELoteInterno(long eLoteInterno)
    {
        this._eLoteInterno = eLoteInterno;
        this._has_eLoteInterno = true;
    } //-- void setELoteInterno(long) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'linhaDigitavel'.
     * 
     * @param linhaDigitavel the value of field 'linhaDigitavel'.
     */
    public void setLinhaDigitavel(java.lang.String linhaDigitavel)
    {
        this._linhaDigitavel = linhaDigitavel;
    } //-- void setLinhaDigitavel(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaArquivoRemessa'.
     * 
     * @param nrSequenciaArquivoRemessa the value of field
     * 'nrSequenciaArquivoRemessa'.
     */
    public void setNrSequenciaArquivoRemessa(long nrSequenciaArquivoRemessa)
    {
        this._nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
        this._has_nrSequenciaArquivoRemessa = true;
    } //-- void setNrSequenciaArquivoRemessa(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'percentualBonificacaoTarifa'.
     * 
     * @param percentualBonificacaoTarifa the value of field
     * 'percentualBonificacaoTarifa'.
     */
    public void setPercentualBonificacaoTarifa(java.math.BigDecimal percentualBonificacaoTarifa)
    {
        this._percentualBonificacaoTarifa = percentualBonificacaoTarifa;
    } //-- void setPercentualBonificacaoTarifa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlFim'.
     * 
     * @param vlFim the value of field 'vlFim'.
     */
    public void setVlFim(java.math.BigDecimal vlFim)
    {
        this._vlFim = vlFim;
    } //-- void setVlFim(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaNegociacao'.
     * 
     * @param vlTarifaNegociacao the value of field
     * 'vlTarifaNegociacao'.
     */
    public void setVlTarifaNegociacao(java.math.BigDecimal vlTarifaNegociacao)
    {
        this._vlTarifaNegociacao = vlTarifaNegociacao;
    } //-- void setVlTarifaNegociacao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlTarifaPadrao'.
     * 
     * @param vlTarifaPadrao the value of field 'vlTarifaPadrao'.
     */
    public void setVlTarifaPadrao(java.math.BigDecimal vlTarifaPadrao)
    {
        this._vlTarifaPadrao = vlTarifaPadrao;
    } //-- void setVlTarifaPadrao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrInicio'.
     * 
     * @param vlrInicio the value of field 'vlrInicio'.
     */
    public void setVlrInicio(java.math.BigDecimal vlrInicio)
    {
        this._vlrInicio = vlrInicio;
    } //-- void setVlrInicio(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarPagtosSolicitacaoRecuperacaoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaorecuperacao.response.ConsultarPagtosSolicitacaoRecuperacaoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaorecuperacao.response.ConsultarPagtosSolicitacaoRecuperacaoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaorecuperacao.response.ConsultarPagtosSolicitacaoRecuperacaoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagtossolicitacaorecuperacao.response.ConsultarPagtosSolicitacaoRecuperacaoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
