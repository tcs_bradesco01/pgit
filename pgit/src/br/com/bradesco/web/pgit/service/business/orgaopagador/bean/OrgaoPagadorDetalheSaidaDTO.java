/*
 * Nome: br.com.bradesco.web.pgit.service.business.orgaopagador.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.orgaopagador.bean;

import java.io.Serializable;

/**
 * Nome: OrgaoPagadorDetalheSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OrgaoPagadorDetalheSaidaDTO implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -1426506333372381694L;

	/** Atributo cdOrgaoPagador. */
	private Integer cdOrgaoPagador;

    /** Atributo cdTipoUnidade. */
    private Integer cdTipoUnidade;

    /** Atributo dsTipoUnidade. */
    private String dsTipoUnidade;

    /** Atributo cdPessoa. */
    private Long cdPessoa;

    /** Atributo dsPessoa. */
    private String dsPessoa;

    /** Atributo nrSeqUnidadeOrganizacional. */
    private Integer nrSeqUnidadeOrganizacional;

    /** Atributo dsSeqUnidadeOrganizacional. */
    private String dsSeqUnidadeOrganizacional;

    /** Atributo cdSituacaoOrgaoPagador. */
    private Integer cdSituacaoOrgaoPagador;

    /** Atributo cdTarifOrgaoPagador. */
    private Integer cdTarifOrgaoPagador;

    /** Atributo cdCanalInclusao. */
    private Integer cdCanalInclusao;

    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;

    /** Atributo cdAutenSegrcInclusao. */
    private String cdAutenSegrcInclusao;

    /** Atributo nmOperFluxoInclusao. */
    private String nmOperFluxoInclusao;

    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;

    /** Atributo cdCanalManutencao. */
    private Integer cdCanalManutencao;

    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;

    /** Atributo cdAutenSegrcManutencao. */
    private String cdAutenSegrcManutencao;

    /** Atributo nmOperFluxoManutencao. */
    private String nmOperFluxoManutencao;

    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;

    /** Atributo dsMotivoBloqueio. */
    private String dsMotivoBloqueio;

	/**
	 * Get: cdAutenSegrcInclusao.
	 *
	 * @return cdAutenSegrcInclusao
	 */
	public String getCdAutenSegrcInclusao() {
		return cdAutenSegrcInclusao;
	}

	/**
	 * Set: cdAutenSegrcInclusao.
	 *
	 * @param cdAutenSegrcInclusao the cd auten segrc inclusao
	 */
	public void setCdAutenSegrcInclusao(String cdAutenSegrcInclusao) {
		this.cdAutenSegrcInclusao = cdAutenSegrcInclusao;
	}

	/**
	 * Get: cdAutenSegrcManutencao.
	 *
	 * @return cdAutenSegrcManutencao
	 */
	public String getCdAutenSegrcManutencao() {
		return cdAutenSegrcManutencao;
	}

	/**
	 * Set: cdAutenSegrcManutencao.
	 *
	 * @param cdAutenSegrcManutencao the cd auten segrc manutencao
	 */
	public void setCdAutenSegrcManutencao(String cdAutenSegrcManutencao) {
		this.cdAutenSegrcManutencao = cdAutenSegrcManutencao;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public Integer getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(Integer cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: cdOrgaoPagador.
	 *
	 * @return cdOrgaoPagador
	 */
	public Integer getCdOrgaoPagador() {
		return cdOrgaoPagador;
	}

	/**
	 * Set: cdOrgaoPagador.
	 *
	 * @param cdOrgaoPagador the cd orgao pagador
	 */
	public void setCdOrgaoPagador(Integer cdOrgaoPagador) {
		this.cdOrgaoPagador = cdOrgaoPagador;
	}

	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Get: cdSituacaoOrgaoPagador.
	 *
	 * @return cdSituacaoOrgaoPagador
	 */
	public Integer getCdSituacaoOrgaoPagador() {
		return cdSituacaoOrgaoPagador;
	}

	/**
	 * Set: cdSituacaoOrgaoPagador.
	 *
	 * @param cdSituacaoOrgaoPagador the cd situacao orgao pagador
	 */
	public void setCdSituacaoOrgaoPagador(Integer cdSituacaoOrgaoPagador) {
		this.cdSituacaoOrgaoPagador = cdSituacaoOrgaoPagador;
	}

	/**
	 * Get: cdTarifOrgaoPagador.
	 *
	 * @return cdTarifOrgaoPagador
	 */
	public Integer getCdTarifOrgaoPagador() {
		return cdTarifOrgaoPagador;
	}

	/**
	 * Set: cdTarifOrgaoPagador.
	 *
	 * @param cdTarifOrgaoPagador the cd tarif orgao pagador
	 */
	public void setCdTarifOrgaoPagador(Integer cdTarifOrgaoPagador) {
		this.cdTarifOrgaoPagador = cdTarifOrgaoPagador;
	}

	/**
	 * Get: cdTipoUnidade.
	 *
	 * @return cdTipoUnidade
	 */
	public Integer getCdTipoUnidade() {
		return cdTipoUnidade;
	}

	/**
	 * Set: cdTipoUnidade.
	 *
	 * @param cdTipoUnidade the cd tipo unidade
	 */
	public void setCdTipoUnidade(Integer cdTipoUnidade) {
		this.cdTipoUnidade = cdTipoUnidade;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsMotivoBloqueio.
	 *
	 * @return dsMotivoBloqueio
	 */
	public String getDsMotivoBloqueio() {
		return dsMotivoBloqueio;
	}

	/**
	 * Set: dsMotivoBloqueio.
	 *
	 * @param dsMotivoBloqueio the ds motivo bloqueio
	 */
	public void setDsMotivoBloqueio(String dsMotivoBloqueio) {
		this.dsMotivoBloqueio = dsMotivoBloqueio;
	}

	/**
	 * Get: dsPessoa.
	 *
	 * @return dsPessoa
	 */
	public String getDsPessoa() {
		return dsPessoa;
	}

	/**
	 * Set: dsPessoa.
	 *
	 * @param dsPessoa the ds pessoa
	 */
	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}

	/**
	 * Get: dsSeqUnidadeOrganizacional.
	 *
	 * @return dsSeqUnidadeOrganizacional
	 */
	public String getDsSeqUnidadeOrganizacional() {
		return dsSeqUnidadeOrganizacional;
	}

	/**
	 * Set: dsSeqUnidadeOrganizacional.
	 *
	 * @param dsSeqUnidadeOrganizacional the ds seq unidade organizacional
	 */
	public void setDsSeqUnidadeOrganizacional(String dsSeqUnidadeOrganizacional) {
		this.dsSeqUnidadeOrganizacional = dsSeqUnidadeOrganizacional;
	}

	/**
	 * Get: dsTipoUnidade.
	 *
	 * @return dsTipoUnidade
	 */
	public String getDsTipoUnidade() {
		return dsTipoUnidade;
	}

	/**
	 * Set: dsTipoUnidade.
	 *
	 * @param dsTipoUnidade the ds tipo unidade
	 */
	public void setDsTipoUnidade(String dsTipoUnidade) {
		this.dsTipoUnidade = dsTipoUnidade;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: nmOperFluxoInclusao.
	 *
	 * @return nmOperFluxoInclusao
	 */
	public String getNmOperFluxoInclusao() {
		return nmOperFluxoInclusao;
	}

	/**
	 * Set: nmOperFluxoInclusao.
	 *
	 * @param nmOperFluxoInclusao the nm oper fluxo inclusao
	 */
	public void setNmOperFluxoInclusao(String nmOperFluxoInclusao) {
		this.nmOperFluxoInclusao = nmOperFluxoInclusao;
	}

	/**
	 * Get: nmOperFluxoManutencao.
	 *
	 * @return nmOperFluxoManutencao
	 */
	public String getNmOperFluxoManutencao() {
		return nmOperFluxoManutencao;
	}

	/**
	 * Set: nmOperFluxoManutencao.
	 *
	 * @param nmOperFluxoManutencao the nm oper fluxo manutencao
	 */
	public void setNmOperFluxoManutencao(String nmOperFluxoManutencao) {
		this.nmOperFluxoManutencao = nmOperFluxoManutencao;
	}

	/**
	 * Get: nrSeqUnidadeOrganizacional.
	 *
	 * @return nrSeqUnidadeOrganizacional
	 */
	public Integer getNrSeqUnidadeOrganizacional() {
		return nrSeqUnidadeOrganizacional;
	}

	/**
	 * Set: nrSeqUnidadeOrganizacional.
	 *
	 * @param nrSeqUnidadeOrganizacional the nr seq unidade organizacional
	 */
	public void setNrSeqUnidadeOrganizacional(Integer nrSeqUnidadeOrganizacional) {
		this.nrSeqUnidadeOrganizacional = nrSeqUnidadeOrganizacional;
	}

	/**
	 * Get: canalInclusao.
	 *
	 * @return canalInclusao
	 */
	public String getCanalInclusao() {
		if (getCdCanalInclusao() != null && getCdCanalInclusao() != 0) {
			return getCdCanalInclusao() + " - " + getDsCanalInclusao();
		}
		return "";
	}
	
	/**
	 * Get: canalManutencao.
	 *
	 * @return canalManutencao
	 */
	public String getCanalManutencao() {
		if (getCdCanalManutencao() != null && getCdCanalManutencao() != 0) {
			return getCdCanalManutencao() + " - " + getDsCanalManutencao();
		}
		return "";
	}
}
