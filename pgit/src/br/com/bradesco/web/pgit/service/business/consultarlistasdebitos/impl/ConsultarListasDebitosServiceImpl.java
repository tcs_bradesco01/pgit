/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.IConsultarListasDebitosService;
import br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.IConsultarListasDebitosServiceConstants;
import br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.bean.ConsultarListaDebitoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.bean.ConsultarListaDebitoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.bean.DetalharListaDebitoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.bean.DetalharListaDebitoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebito.request.ConsultarListaDebitoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistadebito.response.ConsultarListaDebitoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebito.request.DetalharListaDebitoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharlistadebito.response.DetalharListaDebitoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ConsultarListasDebitos
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConsultarListasDebitosServiceImpl implements IConsultarListasDebitosService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;	
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}
	
	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.IConsultarListasDebitosService#consultarListaDebito(br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.bean.ConsultarListaDebitoEntradaDTO)
	 */
	public List<ConsultarListaDebitoSaidaDTO> consultarListaDebito(ConsultarListaDebitoEntradaDTO entrada) {
	   	List<ConsultarListaDebitoSaidaDTO> listaRetorno = new ArrayList<ConsultarListaDebitoSaidaDTO>();
	   	ConsultarListaDebitoRequest request = new ConsultarListaDebitoRequest();
	   	ConsultarListaDebitoResponse response = new ConsultarListaDebitoResponse();
    	
    	request.setNrOcorrencias(IConsultarListasDebitosServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entrada.getCdTipoPesquisa()));
    	request.setCdPessoaParticipanteContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaParticipanteContrato()));
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
    	request.setDtCreditoPagamentoInicial(PgitUtil.verificaStringNula(entrada.getDtCreditoPagamentoInicial()));
    	request.setDtCreditoPagamentoFinal(PgitUtil.verificaStringNula(entrada.getDtCreditoPagamentoFinal()));
    	request.setCdListaDebitoPagamentoInicial(PgitUtil.verificaLongNulo(entrada.getCdListaDebitoPagamentoInicial()));
    	request.setCdListaDebitoPagamentoFinal(PgitUtil.verificaLongNulo(entrada.getCdListaDebitoPagamentoFinal()));
    	request.setCdBanco(PgitUtil.verificaIntegerNulo(entrada.getCdBanco()));
    	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdAgencia()));
    	request.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(entrada.getCdDigitoAgencia()));
    	request.setCdConta(PgitUtil.verificaLongNulo(entrada.getCdConta()));
    	request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
    	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
    	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
    	request.setCdSituacaoListaDebito(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoListaDebito()));
    	request.setCdServicoCompostoPagamento(PgitUtil.verificaLongNulo(entrada.getCdServicoCompostoPagamento()));
    	
        response = getFactoryAdapter().getConsultarListaDebitoPDCAdapter().invokeProcess(request);
        
        for(int i = 0;i<response.getOcorrenciasCount();i++){
        	ConsultarListaDebitoSaidaDTO saidaDTO = new ConsultarListaDebitoSaidaDTO();
        	
        	saidaDTO.setCodMensagem(response.getCodMensagem());
        	saidaDTO.setMensagem(response.getMensagem());
        	saidaDTO.setNumeroCpfCnpj(response.getOcorrencias(i).getNumeroCpfCnpj());
        	saidaDTO.setDsPessoaEmpresa(response.getOcorrencias(i).getDsPessoaEmpresa());
        	saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
        	saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
        	saidaDTO.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
        	saidaDTO.setDsTipoContratoNegocio(response.getOcorrencias(i).getDsTipoContratoNegocio());
        	saidaDTO.setCdListaDebitoPagamento(response.getOcorrencias(i).getCdListaDebitoPagamento());
        	saidaDTO.setCdSituacaoListaDebito(response.getOcorrencias(i).getCdSituacaoListaDebito());
        	saidaDTO.setDsSituacaoListaDebito(response.getOcorrencias(i).getDsSituacaoListaDebito());
        	saidaDTO.setDtPrevistaPagamento(response.getOcorrencias(i).getDtPrevistaPagamento());
        	saidaDTO.setDtPrevistaPagamentoFormatada(FormatarData.formatarDataFromPdc(response.getOcorrencias(i).getDtPrevistaPagamento()));
        	saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
        	saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
        	saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
        	saidaDTO.setDsProdutoServicoRelacionado(response.getOcorrencias(i).getDsProdutoServicoRelacionado());
        	saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
        	saidaDTO.setDsBanco(response.getOcorrencias(i).getDsBanco());
        	saidaDTO.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
        	saidaDTO.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
        	saidaDTO.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
        	saidaDTO.setCdConta(response.getOcorrencias(i).getCdConta());
        	saidaDTO.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
        	saidaDTO.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
        	saidaDTO.setDsTipoConta(response.getOcorrencias(i).getDsTipoConta());
        	saidaDTO.setQtPagamentoAutorizado(response.getOcorrencias(i).getQtPagamentoAutorizado());
        	saidaDTO.setVlPagamentoAutorizado(response.getOcorrencias(i).getVlPagamentoAutorizado());
        	saidaDTO.setQtPagamentoDesautorizado(response.getOcorrencias(i).getQtPagamentoDesautorizado());
        	saidaDTO.setVlPagamentoDesautorizado(response.getOcorrencias(i).getVlPagamentoDesautorizado());
        	saidaDTO.setQtPagamendoEfetivado(response.getOcorrencias(i).getQtPagamendoEfetivado());
        	saidaDTO.setVlPagamentoEfetivado(response.getOcorrencias(i).getVlPagamentoEfetivado());
        	saidaDTO.setQtPagamentoNaoEfetivado(response.getOcorrencias(i).getQtPagamentoNaoEfetivado());
        	saidaDTO.setVlPagamentoNaoEfetivado(response.getOcorrencias(i).getVlPagamentoNaoEfetivado());
        	saidaDTO.setQtTotalPagamento(response.getOcorrencias(i).getQtTotalPagamento());
        	saidaDTO.setVlTotalPagamentos(response.getOcorrencias(i).getVlTotalPagamentos());
        	saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
        	saidaDTO.setContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(saidaDTO.getCdBanco(), saidaDTO.getCdAgencia(), saidaDTO.getCdDigitoAgencia(), saidaDTO.getCdConta(), saidaDTO.getCdDigitoConta(), true));
        	saidaDTO.setCdServicoCompostoPagamento(response.getOcorrencias(i).getCdServicoCompostoPagamento());
        	
        	saidaDTO.setNumeroCpfCnpjFormatado(SiteUtil.formatNumber(saidaDTO.getNumeroCpfCnpj(), 15));
        	if(!saidaDTO.getNumeroCpfCnpjFormatado().equals("000000000000000")){
        		if(saidaDTO.getNumeroCpfCnpjFormatado().substring(9, 13).equals("0000")){
        			saidaDTO.setNumeroCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpj(saidaDTO.getNumeroCpfCnpjFormatado(), 1));
        		}else{
        			saidaDTO.setNumeroCpfCnpjFormatado(CpfCnpjUtils.formatCpfCnpj(saidaDTO.getNumeroCpfCnpjFormatado(), 2));
        		}
        	}else{
        		saidaDTO.setNumeroCpfCnpjFormatado("");
        	}
    		
            listaRetorno.add(saidaDTO);
        }
    	
    	return listaRetorno;    
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.IConsultarListasDebitosService#detalharListaDebito(br.com.bradesco.web.pgit.service.business.consultarlistasdebitos.bean.DetalharListaDebitoEntradaDTO)
	 */
	public List<DetalharListaDebitoSaidaDTO> detalharListaDebito(DetalharListaDebitoEntradaDTO entrada) {
	   	List<DetalharListaDebitoSaidaDTO> listaRetorno = new ArrayList<DetalharListaDebitoSaidaDTO>();
	   	DetalharListaDebitoRequest request = new DetalharListaDebitoRequest();
	   	DetalharListaDebitoResponse response = new DetalharListaDebitoResponse();
	   	
    	request.setNrOcorrencias(IConsultarListasDebitosServiceConstants.NUMERO_OCORRENCIAS_DETALHAR);
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
    	request.setCdTipoCanal(PgitUtil.verificaIntegerNulo(entrada.getCdTipoCanal()));
    	request.setDtPagamento(PgitUtil.verificaStringNula(entrada.getDtPagamento()));
    	request.setCdListaDebitoPagamento(PgitUtil.verificaLongNulo(entrada.getCdListaDebitoPagamento()));
    	
        response = getFactoryAdapter().getDetalharListaDebitoPDCAdapter().invokeProcess(request);
        
        for(int i = 0;i<response.getOcorrenciasCount();i++){
        	DetalharListaDebitoSaidaDTO saidaDTO = new DetalharListaDebitoSaidaDTO();
        	
        	saidaDTO.setCodMensagem(response.getCodMensagem());
        	saidaDTO.setMensagem(response.getMensagem());
        	saidaDTO.setNumeroLinhas(response.getNumeroLinhas());
        	saidaDTO.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
        	saidaDTO.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
        	saidaDTO.setCdFavorecidoBeneficiario(response.getOcorrencias(i).getCdFavorecidoBeneficiario());
        	saidaDTO.setDsFavorecidoBeneficiario(response.getOcorrencias(i).getDsFavorecidoBeneficiario());
        	saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
        	saidaDTO.setDsBanco(response.getOcorrencias(i).getDsBanco());
        	saidaDTO.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
        	saidaDTO.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
        	saidaDTO.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
        	saidaDTO.setCdConta(response.getOcorrencias(i).getCdConta());
        	saidaDTO.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
        	saidaDTO.setDsContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBanco(), response.getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(), response.getOcorrencias(i).getCdConta(), response.getOcorrencias(i).getCdDigitoConta(), true));
        	saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
        	saidaDTO.setDsSituacaoPagamento(response.getOcorrencias(i).getDsSituacaoPagamento());
        	saidaDTO.setCdSituacaoPagamentoLista(response.getOcorrencias(i).getCdSituacaoPagamentoLista());
        	saidaDTO.setDsMotivoPagamento(response.getOcorrencias(i).getDsMotivoPagamento());
        	saidaDTO.setDtEfetivacaoPagamento(response.getOcorrencias(i).getDtEfetivacaoPagamento());
        	saidaDTO.setFavorecidoBeneficiario(PgitUtil.concatenarCampos(response.getOcorrencias(i).getCdFavorecidoBeneficiario(), response.getOcorrencias(i).getDsFavorecidoBeneficiario()));

        	saidaDTO.setDsSituacaoPagamentoLista(response.getOcorrencias(i).getDsSituacaoPagamentoLista());
        	saidaDTO.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
        	saidaDTO.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
        	saidaDTO.setCdTabelaConsulta(response.getOcorrencias(i).getCdTabelaConsulta());
        	
            listaRetorno.add(saidaDTO);
        }
	   	
	   	return listaRetorno;
	}
}