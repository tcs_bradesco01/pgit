/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class CMPIW034CONSULTASSAIDA.
 * 
 * @version $Revision$ $Date$
 */
public class CMPIW034CONSULTASSAIDA implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _sequenciaRegistroRemessa
     */
    private long _sequenciaRegistroRemessa = 0;

    /**
     * keeps track of state for field: _sequenciaRegistroRemessa
     */
    private boolean _has_sequenciaRegistroRemessa;

    /**
     * Field _instrucaoMovimento
     */
    private int _instrucaoMovimento = 0;

    /**
     * keeps track of state for field: _instrucaoMovimento
     */
    private boolean _has_instrucaoMovimento;

    /**
     * Field _decricaoInstrucaoMovimento
     */
    private java.lang.String _decricaoInstrucaoMovimento;

    /**
     * Field _controlePagamento
     */
    private java.lang.String _controlePagamento;


      //----------------/
     //- Constructors -/
    //----------------/

    public CMPIW034CONSULTASSAIDA() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteInstrucaoMovimento
     * 
     */
    public void deleteInstrucaoMovimento()
    {
        this._has_instrucaoMovimento= false;
    } //-- void deleteInstrucaoMovimento() 

    /**
     * Method deleteSequenciaRegistroRemessa
     * 
     */
    public void deleteSequenciaRegistroRemessa()
    {
        this._has_sequenciaRegistroRemessa= false;
    } //-- void deleteSequenciaRegistroRemessa() 

    /**
     * Returns the value of field 'controlePagamento'.
     * 
     * @return String
     * @return the value of field 'controlePagamento'.
     */
    public java.lang.String getControlePagamento()
    {
        return this._controlePagamento;
    } //-- java.lang.String getControlePagamento() 

    /**
     * Returns the value of field 'decricaoInstrucaoMovimento'.
     * 
     * @return String
     * @return the value of field 'decricaoInstrucaoMovimento'.
     */
    public java.lang.String getDecricaoInstrucaoMovimento()
    {
        return this._decricaoInstrucaoMovimento;
    } //-- java.lang.String getDecricaoInstrucaoMovimento() 

    /**
     * Returns the value of field 'instrucaoMovimento'.
     * 
     * @return int
     * @return the value of field 'instrucaoMovimento'.
     */
    public int getInstrucaoMovimento()
    {
        return this._instrucaoMovimento;
    } //-- int getInstrucaoMovimento() 

    /**
     * Returns the value of field 'sequenciaRegistroRemessa'.
     * 
     * @return long
     * @return the value of field 'sequenciaRegistroRemessa'.
     */
    public long getSequenciaRegistroRemessa()
    {
        return this._sequenciaRegistroRemessa;
    } //-- long getSequenciaRegistroRemessa() 

    /**
     * Method hasInstrucaoMovimento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasInstrucaoMovimento()
    {
        return this._has_instrucaoMovimento;
    } //-- boolean hasInstrucaoMovimento() 

    /**
     * Method hasSequenciaRegistroRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasSequenciaRegistroRemessa()
    {
        return this._has_sequenciaRegistroRemessa;
    } //-- boolean hasSequenciaRegistroRemessa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'controlePagamento'.
     * 
     * @param controlePagamento the value of field
     * 'controlePagamento'.
     */
    public void setControlePagamento(java.lang.String controlePagamento)
    {
        this._controlePagamento = controlePagamento;
    } //-- void setControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'decricaoInstrucaoMovimento'.
     * 
     * @param decricaoInstrucaoMovimento the value of field
     * 'decricaoInstrucaoMovimento'.
     */
    public void setDecricaoInstrucaoMovimento(java.lang.String decricaoInstrucaoMovimento)
    {
        this._decricaoInstrucaoMovimento = decricaoInstrucaoMovimento;
    } //-- void setDecricaoInstrucaoMovimento(java.lang.String) 

    /**
     * Sets the value of field 'instrucaoMovimento'.
     * 
     * @param instrucaoMovimento the value of field
     * 'instrucaoMovimento'.
     */
    public void setInstrucaoMovimento(int instrucaoMovimento)
    {
        this._instrucaoMovimento = instrucaoMovimento;
        this._has_instrucaoMovimento = true;
    } //-- void setInstrucaoMovimento(int) 

    /**
     * Sets the value of field 'sequenciaRegistroRemessa'.
     * 
     * @param sequenciaRegistroRemessa the value of field
     * 'sequenciaRegistroRemessa'.
     */
    public void setSequenciaRegistroRemessa(long sequenciaRegistroRemessa)
    {
        this._sequenciaRegistroRemessa = sequenciaRegistroRemessa;
        this._has_sequenciaRegistroRemessa = true;
    } //-- void setSequenciaRegistroRemessa(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return CMPIW034CONSULTASSAIDA
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinconsistencia.response.CMPIW034CONSULTASSAIDA unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
