/*
 * Nome: br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean;

/**
 * Nome: IncluirSolicCriacaoContaTipoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirSolicCriacaoContaTipoEntradaDTO {

	/** Atributo cdPessoa. */
	private Long cdPessoa;
    
    /** Atributo cdCpfCnpjPssoa. */
    private Long cdCpfCnpjPssoa;
    
    /** Atributo cdFilialCnpjPssoa. */
    private Integer cdFilialCnpjPssoa;
    
    /** Atributo cdControleCnpjPssoa. */
    private Integer cdControleCnpjPssoa;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdGrupoContabil. */
    private Integer cdGrupoContabil;
    
    /** Atributo cdSubGrupoContabil. */
    private Integer cdSubGrupoContabil;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdControleCnpjPssoa.
	 *
	 * @return cdControleCnpjPssoa
	 */
	public Integer getCdControleCnpjPssoa() {
		return cdControleCnpjPssoa;
	}
	
	/**
	 * Set: cdControleCnpjPssoa.
	 *
	 * @param cdControleCnpjPssoa the cd controle cnpj pssoa
	 */
	public void setCdControleCnpjPssoa(Integer cdControleCnpjPssoa) {
		this.cdControleCnpjPssoa = cdControleCnpjPssoa;
	}
	
	/**
	 * Get: cdCpfCnpjPssoa.
	 *
	 * @return cdCpfCnpjPssoa
	 */
	public Long getCdCpfCnpjPssoa() {
		return cdCpfCnpjPssoa;
	}
	
	/**
	 * Set: cdCpfCnpjPssoa.
	 *
	 * @param cdCpfCnpjPssoa the cd cpf cnpj pssoa
	 */
	public void setCdCpfCnpjPssoa(Long cdCpfCnpjPssoa) {
		this.cdCpfCnpjPssoa = cdCpfCnpjPssoa;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdFilialCnpjPssoa.
	 *
	 * @return cdFilialCnpjPssoa
	 */
	public Integer getCdFilialCnpjPssoa() {
		return cdFilialCnpjPssoa;
	}
	
	/**
	 * Set: cdFilialCnpjPssoa.
	 *
	 * @param cdFilialCnpjPssoa the cd filial cnpj pssoa
	 */
	public void setCdFilialCnpjPssoa(Integer cdFilialCnpjPssoa) {
		this.cdFilialCnpjPssoa = cdFilialCnpjPssoa;
	}
	
	/**
	 * Get: cdGrupoContabil.
	 *
	 * @return cdGrupoContabil
	 */
	public Integer getCdGrupoContabil() {
		return cdGrupoContabil;
	}
	
	/**
	 * Set: cdGrupoContabil.
	 *
	 * @param cdGrupoContabil the cd grupo contabil
	 */
	public void setCdGrupoContabil(Integer cdGrupoContabil) {
		this.cdGrupoContabil = cdGrupoContabil;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdSubGrupoContabil.
	 *
	 * @return cdSubGrupoContabil
	 */
	public Integer getCdSubGrupoContabil() {
		return cdSubGrupoContabil;
	}
	
	/**
	 * Set: cdSubGrupoContabil.
	 *
	 * @param cdSubGrupoContabil the cd sub grupo contabil
	 */
	public void setCdSubGrupoContabil(Integer cdSubGrupoContabil) {
		this.cdSubGrupoContabil = cdSubGrupoContabil;
	}

}