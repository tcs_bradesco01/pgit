/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

/**
 * Nome: AlterarCadContasDestinoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarCadContasDestinoSaidaDTO {

	 /** Atributo codMensagem. */
 	private String codMensagem;
     
     /** Atributo mensagem. */
     private String mensagem;
     
	/**
	 * Alterar cad contas destino saida dto.
	 */
	public AlterarCadContasDestinoSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Alterar cad contas destino saida dto.
	 *
	 * @param codMensagem the cod mensagem
	 * @param mensagem the mensagem
	 */
	public AlterarCadContasDestinoSaidaDTO(String codMensagem, String mensagem) {
		this.codMensagem = codMensagem;
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
     
     
}
