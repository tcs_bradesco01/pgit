/*
 * Nome: br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean;

/**
 * Nome: ListarVincMsgLancOperEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarVincMsgLancOperEntradaDTO {
	
	/** Atributo pessoaJurContrato. */
	private long pessoaJurContrato;
	
	/** Atributo tipoContrato. */
	private int tipoContrato;
	
	/** Atributo numeroContrato. */
	private long numeroContrato;
	
	/** Atributo tipoServico. */
	private int tipoServico;
	
	/** Atributo modalidadeServico. */
	private int modalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	/** Atributo codigoHistorico. */
	private int codigoHistorico;
	
	/**
	 * Get: codigoHistorico.
	 *
	 * @return codigoHistorico
	 */
	public int getCodigoHistorico() {
		return codigoHistorico;
	}
	
	/**
	 * Set: codigoHistorico.
	 *
	 * @param codigoHistorico the codigo historico
	 */
	public void setCodigoHistorico(int codigoHistorico) {
		this.codigoHistorico = codigoHistorico;
	}
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public int getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(int modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public long getNumeroContrato() {
		return numeroContrato;
	}
	
	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(long numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	
	/**
	 * Get: pessoaJurContrato.
	 *
	 * @return pessoaJurContrato
	 */
	public long getPessoaJurContrato() {
		return pessoaJurContrato;
	}
	
	/**
	 * Set: pessoaJurContrato.
	 *
	 * @param pessoaJurContrato the pessoa jur contrato
	 */
	public void setPessoaJurContrato(long pessoaJurContrato) {
		this.pessoaJurContrato = pessoaJurContrato;
	}
	
	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public int getTipoContrato() {
		return tipoContrato;
	}
	
	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(int tipoContrato) {
		this.tipoContrato = tipoContrato;
	}
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	
	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public int getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	
}
