/*
 * Nome: br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;

import java.util.Date;

/**
 * Nome: ListarManutencaoContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarManutencaoContratoEntradaDTO {

	/** Atributo cdPessoaJuridicaContrato. */
	private long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private int cdTipoContratoNegocio;
	
	/** Atributo cdTipoManutencaoContrato. */
	private int cdTipoManutencaoContrato;	
	
	/** Atributo nrSequenciaContratoNegocio. */
	private long nrSequenciaContratoNegocio;
	
	/** Atributo cdAcao. */
	private int cdAcao;
	
	/** Atributo dtInicio. */
	private Date dtInicio;
	
	/** Atributo dtFim. */
	private Date dtFim;
	
	/** Atributo cdSituacaoManutencaoContrato. */
	private int cdSituacaoManutencaoContrato;

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoManutencaoContrato.
	 *
	 * @return cdTipoManutencaoContrato
	 */
	public int getCdTipoManutencaoContrato() {
		return cdTipoManutencaoContrato;
	}
	
	/**
	 * Set: cdTipoManutencaoContrato.
	 *
	 * @param cdTipoManutencaoContrato the cd tipo manutencao contrato
	 */
	public void setCdTipoManutencaoContrato(int cdTipoManutencaoContrato) {
		this.cdTipoManutencaoContrato = cdTipoManutencaoContrato;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdAcao.
	 *
	 * @return cdAcao
	 */
	public int getCdAcao() {
		return cdAcao;
	}
	
	/**
	 * Set: cdAcao.
	 *
	 * @param cdAcao the cd acao
	 */
	public void setCdAcao(int cdAcao) {
		this.cdAcao = cdAcao;
	}
	
	/**
	 * Get: dtFim.
	 *
	 * @return dtFim
	 */
	public Date getDtFim() {
		return dtFim;
	}
	
	/**
	 * Set: dtFim.
	 *
	 * @param dtFim the dt fim
	 */
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	
	/**
	 * Get: dtInicio.
	 *
	 * @return dtInicio
	 */
	public Date getDtInicio() {
		return dtInicio;
	}
	
	/**
	 * Set: dtInicio.
	 *
	 * @param dtInicio the dt inicio
	 */
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	
	/**
	 * Get: cdSituacaoManutencaoContrato.
	 *
	 * @return cdSituacaoManutencaoContrato
	 */
	public int getCdSituacaoManutencaoContrato() {
		return cdSituacaoManutencaoContrato;
	}
	
	/**
	 * Set: cdSituacaoManutencaoContrato.
	 *
	 * @param cdSituacaoManutencaoContrato the cd situacao manutencao contrato
	 */
	public void setCdSituacaoManutencaoContrato(int cdSituacaoManutencaoContrato) {
		this.cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
	}

}