/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: TipoProcessoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoProcessoEntradaDTO {

	/** Atributo cdNaturezaServico. */
	private Integer cdNaturezaServico;
	
	/** Atributo cdTipoProcsSistema. */
	private Integer cdTipoProcsSistema;
	
	/**
	 * Get: cdNaturezaServico.
	 *
	 * @return cdNaturezaServico
	 */
	public Integer getCdNaturezaServico() {
		return cdNaturezaServico;
	}
	
	/**
	 * Set: cdNaturezaServico.
	 *
	 * @param cdNaturezaServico the cd natureza servico
	 */
	public void setCdNaturezaServico(Integer cdNaturezaServico) {
		this.cdNaturezaServico = cdNaturezaServico;
	}
	
	/**
	 * Get: cdTipoProcsSistema.
	 *
	 * @return cdTipoProcsSistema
	 */
	public Integer getCdTipoProcsSistema() {
		return cdTipoProcsSistema;
	}
	
	/**
	 * Set: cdTipoProcsSistema.
	 *
	 * @param cdTipoProcsSistema the cd tipo procs sistema
	 */
	public void setCdTipoProcsSistema(Integer cdTipoProcsSistema) {
		this.cdTipoProcsSistema = cdTipoProcsSistema;
	}
}
