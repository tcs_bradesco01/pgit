/*
 * Nome: br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.bean;

/**
 * Nome: RegistrarAssinaturaContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RegistrarAssinaturaContratoEntradaDTO {
	
	
	/** Atributo cdPessoaJuridicaContrato. */
	private int cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private int  cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private String  nrSequenciaContratoNegocio;	
	
	/** Atributo dtAssinaturaAditivo. */
	private String  dtAssinaturaAditivo;
	
	/** Atributo numContratoFisico. */
	private String numContratoFisico;
	
	

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public int getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(int cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public int getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(int cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dtAssinaturaAditivo.
	 *
	 * @return dtAssinaturaAditivo
	 */
	public String getDtAssinaturaAditivo() {
		return dtAssinaturaAditivo;
	}
	
	/**
	 * Set: dtAssinaturaAditivo.
	 *
	 * @param dtAssinaturaAditivo the dt assinatura aditivo
	 */
	public void setDtAssinaturaAditivo(String dtAssinaturaAditivo) {
		this.dtAssinaturaAditivo = dtAssinaturaAditivo;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public String getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(String nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: numContratoFisico.
	 *
	 * @return numContratoFisico
	 */
	public String getNumContratoFisico() {
		return numContratoFisico;
	}
	
	/**
	 * Set: numContratoFisico.
	 *
	 * @param numContratoFisico the num contrato fisico
	 */
	public void setNumContratoFisico(String numContratoFisico) {
		this.numContratoFisico = numContratoFisico;
	}
	
	
	
	

}
