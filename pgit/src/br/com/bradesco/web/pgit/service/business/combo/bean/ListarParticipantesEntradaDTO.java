/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarParticipantesEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarParticipantesEntradaDTO {
	
	/** Atributo cdPessoaJuridica. */
	private long cdPessoaJuridica;
	
	/** Atributo cdTipoContrato. */
	private int cdTipoContrato;
	
	/** Atributo nrSequenciaContrato. */
	private long nrSequenciaContrato;
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public int getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(int cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	

}
