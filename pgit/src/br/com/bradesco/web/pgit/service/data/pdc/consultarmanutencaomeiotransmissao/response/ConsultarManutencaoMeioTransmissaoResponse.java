/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaomeiotransmissao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarManutencaoMeioTransmissaoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarManutencaoMeioTransmissaoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdTipoManutencaoContrato
     */
    private int _cdTipoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoManutencaoContrato
     */
    private boolean _has_cdTipoManutencaoContrato;

    /**
     * Field _nrManutencaoContratoNegocio
     */
    private long _nrManutencaoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrManutencaoContratoNegocio
     */
    private boolean _has_nrManutencaoContratoNegocio;

    /**
     * Field _cdSituacaoManutencaoContrato
     */
    private int _cdSituacaoManutencaoContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoManutencaoContrato
     */
    private boolean _has_cdSituacaoManutencaoContrato;

    /**
     * Field _dsSituacaoManutContr
     */
    private java.lang.String _dsSituacaoManutContr;

    /**
     * Field _cdMotivoManutContr
     */
    private int _cdMotivoManutContr = 0;

    /**
     * keeps track of state for field: _cdMotivoManutContr
     */
    private boolean _has_cdMotivoManutContr;

    /**
     * Field _dsMotivoManuContr
     */
    private java.lang.String _dsMotivoManuContr;

    /**
     * Field _nrAditivoContratoNegocio
     */
    private long _nrAditivoContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrAditivoContratoNegocio
     */
    private boolean _has_nrAditivoContratoNegocio;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsIndicadorTipoManutencao
     */
    private java.lang.String _dsIndicadorTipoManutencao;

    /**
     * Field _cdPerfilTrocaArq
     */
    private long _cdPerfilTrocaArq = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArq
     */
    private boolean _has_cdPerfilTrocaArq;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsCodTipoLayout
     */
    private java.lang.String _dsCodTipoLayout;

    /**
     * Field _dsLayoutProprio
     */
    private java.lang.String _dsLayoutProprio;

    /**
     * Field _cdAplicacaoTransmPagamento
     */
    private long _cdAplicacaoTransmPagamento = 0;

    /**
     * keeps track of state for field: _cdAplicacaoTransmPagamento
     */
    private boolean _has_cdAplicacaoTransmPagamento;

    /**
     * Field _dsAplicFormat
     */
    private java.lang.String _dsAplicFormat;

    /**
     * Field _cdMeioPrincipalRemessa
     */
    private int _cdMeioPrincipalRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRemessa
     */
    private boolean _has_cdMeioPrincipalRemessa;

    /**
     * Field _dsCodMeioPrincipalRemessa
     */
    private java.lang.String _dsCodMeioPrincipalRemessa;

    /**
     * Field _cdMeioAlternRemessa
     */
    private int _cdMeioAlternRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioAlternRemessa
     */
    private boolean _has_cdMeioAlternRemessa;

    /**
     * Field _dsCodMeioAlternRemessa
     */
    private java.lang.String _dsCodMeioAlternRemessa;

    /**
     * Field _cdMeioPrincipalRetorno
     */
    private int _cdMeioPrincipalRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRetorno
     */
    private boolean _has_cdMeioPrincipalRetorno;

    /**
     * Field _cdCodMeioPrincipalRetorno
     */
    private java.lang.String _cdCodMeioPrincipalRetorno;

    /**
     * Field _cdMeioAltrnRetorno
     */
    private int _cdMeioAltrnRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioAltrnRetorno
     */
    private boolean _has_cdMeioAltrnRetorno;

    /**
     * Field _dsMeioAltrnRetorno
     */
    private java.lang.String _dsMeioAltrnRetorno;

    /**
     * Field _cdResponsavelCustoEmpresa
     */
    private int _cdResponsavelCustoEmpresa = 0;

    /**
     * keeps track of state for field: _cdResponsavelCustoEmpresa
     */
    private boolean _has_cdResponsavelCustoEmpresa;

    /**
     * Field _dsResponsavelCustoEmpresa
     */
    private java.lang.String _dsResponsavelCustoEmpresa;

    /**
     * Field _cdPessoaJuridicaParceiro
     */
    private long _cdPessoaJuridicaParceiro = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaParceiro
     */
    private boolean _has_cdPessoaJuridicaParceiro;

    /**
     * Field _dsEmpresa
     */
    private java.lang.String _dsEmpresa;

    /**
     * Field _pcCustoOrganizacaoTransmissao
     */
    private java.math.BigDecimal _pcCustoOrganizacaoTransmissao = new java.math.BigDecimal("0");

    /**
     * Field _dsUtilizacaoEmpresaVan
     */
    private java.lang.String _dsUtilizacaoEmpresaVan;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _qtMesRegistroTrafg
     */
    private long _qtMesRegistroTrafg = 0;

    /**
     * keeps track of state for field: _qtMesRegistroTrafg
     */
    private boolean _has_qtMesRegistroTrafg;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoexterno
     */
    private java.lang.String _cdUsuarioManutencaoexterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarManutencaoMeioTransmissaoResponse() 
     {
        super();
        setPcCustoOrganizacaoTransmissao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaomeiotransmissao.response.ConsultarManutencaoMeioTransmissaoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAplicacaoTransmPagamento
     * 
     */
    public void deleteCdAplicacaoTransmPagamento()
    {
        this._has_cdAplicacaoTransmPagamento= false;
    } //-- void deleteCdAplicacaoTransmPagamento() 

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdMeioAlternRemessa
     * 
     */
    public void deleteCdMeioAlternRemessa()
    {
        this._has_cdMeioAlternRemessa= false;
    } //-- void deleteCdMeioAlternRemessa() 

    /**
     * Method deleteCdMeioAltrnRetorno
     * 
     */
    public void deleteCdMeioAltrnRetorno()
    {
        this._has_cdMeioAltrnRetorno= false;
    } //-- void deleteCdMeioAltrnRetorno() 

    /**
     * Method deleteCdMeioPrincipalRemessa
     * 
     */
    public void deleteCdMeioPrincipalRemessa()
    {
        this._has_cdMeioPrincipalRemessa= false;
    } //-- void deleteCdMeioPrincipalRemessa() 

    /**
     * Method deleteCdMeioPrincipalRetorno
     * 
     */
    public void deleteCdMeioPrincipalRetorno()
    {
        this._has_cdMeioPrincipalRetorno= false;
    } //-- void deleteCdMeioPrincipalRetorno() 

    /**
     * Method deleteCdMotivoManutContr
     * 
     */
    public void deleteCdMotivoManutContr()
    {
        this._has_cdMotivoManutContr= false;
    } //-- void deleteCdMotivoManutContr() 

    /**
     * Method deleteCdPerfilTrocaArq
     * 
     */
    public void deleteCdPerfilTrocaArq()
    {
        this._has_cdPerfilTrocaArq= false;
    } //-- void deleteCdPerfilTrocaArq() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdPessoaJuridicaParceiro
     * 
     */
    public void deleteCdPessoaJuridicaParceiro()
    {
        this._has_cdPessoaJuridicaParceiro= false;
    } //-- void deleteCdPessoaJuridicaParceiro() 

    /**
     * Method deleteCdResponsavelCustoEmpresa
     * 
     */
    public void deleteCdResponsavelCustoEmpresa()
    {
        this._has_cdResponsavelCustoEmpresa= false;
    } //-- void deleteCdResponsavelCustoEmpresa() 

    /**
     * Method deleteCdSituacaoManutencaoContrato
     * 
     */
    public void deleteCdSituacaoManutencaoContrato()
    {
        this._has_cdSituacaoManutencaoContrato= false;
    } //-- void deleteCdSituacaoManutencaoContrato() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoManutencaoContrato
     * 
     */
    public void deleteCdTipoManutencaoContrato()
    {
        this._has_cdTipoManutencaoContrato= false;
    } //-- void deleteCdTipoManutencaoContrato() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteNrAditivoContratoNegocio
     * 
     */
    public void deleteNrAditivoContratoNegocio()
    {
        this._has_nrAditivoContratoNegocio= false;
    } //-- void deleteNrAditivoContratoNegocio() 

    /**
     * Method deleteNrManutencaoContratoNegocio
     * 
     */
    public void deleteNrManutencaoContratoNegocio()
    {
        this._has_nrManutencaoContratoNegocio= false;
    } //-- void deleteNrManutencaoContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteQtMesRegistroTrafg
     * 
     */
    public void deleteQtMesRegistroTrafg()
    {
        this._has_qtMesRegistroTrafg= false;
    } //-- void deleteQtMesRegistroTrafg() 

    /**
     * Returns the value of field 'cdAplicacaoTransmPagamento'.
     * 
     * @return long
     * @return the value of field 'cdAplicacaoTransmPagamento'.
     */
    public long getCdAplicacaoTransmPagamento()
    {
        return this._cdAplicacaoTransmPagamento;
    } //-- long getCdAplicacaoTransmPagamento() 

    /**
     * Returns the value of field 'cdCodMeioPrincipalRetorno'.
     * 
     * @return String
     * @return the value of field 'cdCodMeioPrincipalRetorno'.
     */
    public java.lang.String getCdCodMeioPrincipalRetorno()
    {
        return this._cdCodMeioPrincipalRetorno;
    } //-- java.lang.String getCdCodMeioPrincipalRetorno() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdMeioAlternRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioAlternRemessa'.
     */
    public int getCdMeioAlternRemessa()
    {
        return this._cdMeioAlternRemessa;
    } //-- int getCdMeioAlternRemessa() 

    /**
     * Returns the value of field 'cdMeioAltrnRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioAltrnRetorno'.
     */
    public int getCdMeioAltrnRetorno()
    {
        return this._cdMeioAltrnRetorno;
    } //-- int getCdMeioAltrnRetorno() 

    /**
     * Returns the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRemessa'.
     */
    public int getCdMeioPrincipalRemessa()
    {
        return this._cdMeioPrincipalRemessa;
    } //-- int getCdMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRetorno'.
     */
    public int getCdMeioPrincipalRetorno()
    {
        return this._cdMeioPrincipalRetorno;
    } //-- int getCdMeioPrincipalRetorno() 

    /**
     * Returns the value of field 'cdMotivoManutContr'.
     * 
     * @return int
     * @return the value of field 'cdMotivoManutContr'.
     */
    public int getCdMotivoManutContr()
    {
        return this._cdMotivoManutContr;
    } //-- int getCdMotivoManutContr() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdPerfilTrocaArq'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArq'.
     */
    public long getCdPerfilTrocaArq()
    {
        return this._cdPerfilTrocaArq;
    } //-- long getCdPerfilTrocaArq() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaParceiro'.
     */
    public long getCdPessoaJuridicaParceiro()
    {
        return this._cdPessoaJuridicaParceiro;
    } //-- long getCdPessoaJuridicaParceiro() 

    /**
     * Returns the value of field 'cdResponsavelCustoEmpresa'.
     * 
     * @return int
     * @return the value of field 'cdResponsavelCustoEmpresa'.
     */
    public int getCdResponsavelCustoEmpresa()
    {
        return this._cdResponsavelCustoEmpresa;
    } //-- int getCdResponsavelCustoEmpresa() 

    /**
     * Returns the value of field 'cdSituacaoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoManutencaoContrato'.
     */
    public int getCdSituacaoManutencaoContrato()
    {
        return this._cdSituacaoManutencaoContrato;
    } //-- int getCdSituacaoManutencaoContrato() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoManutencaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoManutencaoContrato'.
     */
    public int getCdTipoManutencaoContrato()
    {
        return this._cdTipoManutencaoContrato;
    } //-- int getCdTipoManutencaoContrato() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoexterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoexterno'.
     */
    public java.lang.String getCdUsuarioManutencaoexterno()
    {
        return this._cdUsuarioManutencaoexterno;
    } //-- java.lang.String getCdUsuarioManutencaoexterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsAplicFormat'.
     * 
     * @return String
     * @return the value of field 'dsAplicFormat'.
     */
    public java.lang.String getDsAplicFormat()
    {
        return this._dsAplicFormat;
    } //-- java.lang.String getDsAplicFormat() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsCodMeioAlternRemessa'.
     * 
     * @return String
     * @return the value of field 'dsCodMeioAlternRemessa'.
     */
    public java.lang.String getDsCodMeioAlternRemessa()
    {
        return this._dsCodMeioAlternRemessa;
    } //-- java.lang.String getDsCodMeioAlternRemessa() 

    /**
     * Returns the value of field 'dsCodMeioPrincipalRemessa'.
     * 
     * @return String
     * @return the value of field 'dsCodMeioPrincipalRemessa'.
     */
    public java.lang.String getDsCodMeioPrincipalRemessa()
    {
        return this._dsCodMeioPrincipalRemessa;
    } //-- java.lang.String getDsCodMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'dsCodTipoLayout'.
     * 
     * @return String
     * @return the value of field 'dsCodTipoLayout'.
     */
    public java.lang.String getDsCodTipoLayout()
    {
        return this._dsCodTipoLayout;
    } //-- java.lang.String getDsCodTipoLayout() 

    /**
     * Returns the value of field 'dsEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsEmpresa'.
     */
    public java.lang.String getDsEmpresa()
    {
        return this._dsEmpresa;
    } //-- java.lang.String getDsEmpresa() 

    /**
     * Returns the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorTipoManutencao'.
     */
    public java.lang.String getDsIndicadorTipoManutencao()
    {
        return this._dsIndicadorTipoManutencao;
    } //-- java.lang.String getDsIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'dsLayoutProprio'.
     * 
     * @return String
     * @return the value of field 'dsLayoutProprio'.
     */
    public java.lang.String getDsLayoutProprio()
    {
        return this._dsLayoutProprio;
    } //-- java.lang.String getDsLayoutProprio() 

    /**
     * Returns the value of field 'dsMeioAltrnRetorno'.
     * 
     * @return String
     * @return the value of field 'dsMeioAltrnRetorno'.
     */
    public java.lang.String getDsMeioAltrnRetorno()
    {
        return this._dsMeioAltrnRetorno;
    } //-- java.lang.String getDsMeioAltrnRetorno() 

    /**
     * Returns the value of field 'dsMotivoManuContr'.
     * 
     * @return String
     * @return the value of field 'dsMotivoManuContr'.
     */
    public java.lang.String getDsMotivoManuContr()
    {
        return this._dsMotivoManuContr;
    } //-- java.lang.String getDsMotivoManuContr() 

    /**
     * Returns the value of field 'dsResponsavelCustoEmpresa'.
     * 
     * @return String
     * @return the value of field 'dsResponsavelCustoEmpresa'.
     */
    public java.lang.String getDsResponsavelCustoEmpresa()
    {
        return this._dsResponsavelCustoEmpresa;
    } //-- java.lang.String getDsResponsavelCustoEmpresa() 

    /**
     * Returns the value of field 'dsSituacaoManutContr'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoManutContr'.
     */
    public java.lang.String getDsSituacaoManutContr()
    {
        return this._dsSituacaoManutContr;
    } //-- java.lang.String getDsSituacaoManutContr() 

    /**
     * Returns the value of field 'dsUtilizacaoEmpresaVan'.
     * 
     * @return String
     * @return the value of field 'dsUtilizacaoEmpresaVan'.
     */
    public java.lang.String getDsUtilizacaoEmpresaVan()
    {
        return this._dsUtilizacaoEmpresaVan;
    } //-- java.lang.String getDsUtilizacaoEmpresaVan() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrAditivoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrAditivoContratoNegocio'.
     */
    public long getNrAditivoContratoNegocio()
    {
        return this._nrAditivoContratoNegocio;
    } //-- long getNrAditivoContratoNegocio() 

    /**
     * Returns the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrManutencaoContratoNegocio'.
     */
    public long getNrManutencaoContratoNegocio()
    {
        return this._nrManutencaoContratoNegocio;
    } //-- long getNrManutencaoContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'pcCustoOrganizacaoTransmissao'.
     * 
     * @return BigDecimal
     * @return the value of field 'pcCustoOrganizacaoTransmissao'.
     */
    public java.math.BigDecimal getPcCustoOrganizacaoTransmissao()
    {
        return this._pcCustoOrganizacaoTransmissao;
    } //-- java.math.BigDecimal getPcCustoOrganizacaoTransmissao() 

    /**
     * Returns the value of field 'qtMesRegistroTrafg'.
     * 
     * @return long
     * @return the value of field 'qtMesRegistroTrafg'.
     */
    public long getQtMesRegistroTrafg()
    {
        return this._qtMesRegistroTrafg;
    } //-- long getQtMesRegistroTrafg() 

    /**
     * Method hasCdAplicacaoTransmPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAplicacaoTransmPagamento()
    {
        return this._has_cdAplicacaoTransmPagamento;
    } //-- boolean hasCdAplicacaoTransmPagamento() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdMeioAlternRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAlternRemessa()
    {
        return this._has_cdMeioAlternRemessa;
    } //-- boolean hasCdMeioAlternRemessa() 

    /**
     * Method hasCdMeioAltrnRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAltrnRetorno()
    {
        return this._has_cdMeioAltrnRetorno;
    } //-- boolean hasCdMeioAltrnRetorno() 

    /**
     * Method hasCdMeioPrincipalRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRemessa()
    {
        return this._has_cdMeioPrincipalRemessa;
    } //-- boolean hasCdMeioPrincipalRemessa() 

    /**
     * Method hasCdMeioPrincipalRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRetorno()
    {
        return this._has_cdMeioPrincipalRetorno;
    } //-- boolean hasCdMeioPrincipalRetorno() 

    /**
     * Method hasCdMotivoManutContr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoManutContr()
    {
        return this._has_cdMotivoManutContr;
    } //-- boolean hasCdMotivoManutContr() 

    /**
     * Method hasCdPerfilTrocaArq
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArq()
    {
        return this._has_cdPerfilTrocaArq;
    } //-- boolean hasCdPerfilTrocaArq() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdPessoaJuridicaParceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaParceiro()
    {
        return this._has_cdPessoaJuridicaParceiro;
    } //-- boolean hasCdPessoaJuridicaParceiro() 

    /**
     * Method hasCdResponsavelCustoEmpresa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdResponsavelCustoEmpresa()
    {
        return this._has_cdResponsavelCustoEmpresa;
    } //-- boolean hasCdResponsavelCustoEmpresa() 

    /**
     * Method hasCdSituacaoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoManutencaoContrato()
    {
        return this._has_cdSituacaoManutencaoContrato;
    } //-- boolean hasCdSituacaoManutencaoContrato() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoManutencaoContrato()
    {
        return this._has_cdTipoManutencaoContrato;
    } //-- boolean hasCdTipoManutencaoContrato() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasNrAditivoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrAditivoContratoNegocio()
    {
        return this._has_nrAditivoContratoNegocio;
    } //-- boolean hasNrAditivoContratoNegocio() 

    /**
     * Method hasNrManutencaoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrManutencaoContratoNegocio()
    {
        return this._has_nrManutencaoContratoNegocio;
    } //-- boolean hasNrManutencaoContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasQtMesRegistroTrafg
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtMesRegistroTrafg()
    {
        return this._has_qtMesRegistroTrafg;
    } //-- boolean hasQtMesRegistroTrafg() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAplicacaoTransmPagamento'.
     * 
     * @param cdAplicacaoTransmPagamento the value of field
     * 'cdAplicacaoTransmPagamento'.
     */
    public void setCdAplicacaoTransmPagamento(long cdAplicacaoTransmPagamento)
    {
        this._cdAplicacaoTransmPagamento = cdAplicacaoTransmPagamento;
        this._has_cdAplicacaoTransmPagamento = true;
    } //-- void setCdAplicacaoTransmPagamento(long) 

    /**
     * Sets the value of field 'cdCodMeioPrincipalRetorno'.
     * 
     * @param cdCodMeioPrincipalRetorno the value of field
     * 'cdCodMeioPrincipalRetorno'.
     */
    public void setCdCodMeioPrincipalRetorno(java.lang.String cdCodMeioPrincipalRetorno)
    {
        this._cdCodMeioPrincipalRetorno = cdCodMeioPrincipalRetorno;
    } //-- void setCdCodMeioPrincipalRetorno(java.lang.String) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdMeioAlternRemessa'.
     * 
     * @param cdMeioAlternRemessa the value of field
     * 'cdMeioAlternRemessa'.
     */
    public void setCdMeioAlternRemessa(int cdMeioAlternRemessa)
    {
        this._cdMeioAlternRemessa = cdMeioAlternRemessa;
        this._has_cdMeioAlternRemessa = true;
    } //-- void setCdMeioAlternRemessa(int) 

    /**
     * Sets the value of field 'cdMeioAltrnRetorno'.
     * 
     * @param cdMeioAltrnRetorno the value of field
     * 'cdMeioAltrnRetorno'.
     */
    public void setCdMeioAltrnRetorno(int cdMeioAltrnRetorno)
    {
        this._cdMeioAltrnRetorno = cdMeioAltrnRetorno;
        this._has_cdMeioAltrnRetorno = true;
    } //-- void setCdMeioAltrnRetorno(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @param cdMeioPrincipalRemessa the value of field
     * 'cdMeioPrincipalRemessa'.
     */
    public void setCdMeioPrincipalRemessa(int cdMeioPrincipalRemessa)
    {
        this._cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
        this._has_cdMeioPrincipalRemessa = true;
    } //-- void setCdMeioPrincipalRemessa(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @param cdMeioPrincipalRetorno the value of field
     * 'cdMeioPrincipalRetorno'.
     */
    public void setCdMeioPrincipalRetorno(int cdMeioPrincipalRetorno)
    {
        this._cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
        this._has_cdMeioPrincipalRetorno = true;
    } //-- void setCdMeioPrincipalRetorno(int) 

    /**
     * Sets the value of field 'cdMotivoManutContr'.
     * 
     * @param cdMotivoManutContr the value of field
     * 'cdMotivoManutContr'.
     */
    public void setCdMotivoManutContr(int cdMotivoManutContr)
    {
        this._cdMotivoManutContr = cdMotivoManutContr;
        this._has_cdMotivoManutContr = true;
    } //-- void setCdMotivoManutContr(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPerfilTrocaArq'.
     * 
     * @param cdPerfilTrocaArq the value of field 'cdPerfilTrocaArq'
     */
    public void setCdPerfilTrocaArq(long cdPerfilTrocaArq)
    {
        this._cdPerfilTrocaArq = cdPerfilTrocaArq;
        this._has_cdPerfilTrocaArq = true;
    } //-- void setCdPerfilTrocaArq(long) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @param cdPessoaJuridicaParceiro the value of field
     * 'cdPessoaJuridicaParceiro'.
     */
    public void setCdPessoaJuridicaParceiro(long cdPessoaJuridicaParceiro)
    {
        this._cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
        this._has_cdPessoaJuridicaParceiro = true;
    } //-- void setCdPessoaJuridicaParceiro(long) 

    /**
     * Sets the value of field 'cdResponsavelCustoEmpresa'.
     * 
     * @param cdResponsavelCustoEmpresa the value of field
     * 'cdResponsavelCustoEmpresa'.
     */
    public void setCdResponsavelCustoEmpresa(int cdResponsavelCustoEmpresa)
    {
        this._cdResponsavelCustoEmpresa = cdResponsavelCustoEmpresa;
        this._has_cdResponsavelCustoEmpresa = true;
    } //-- void setCdResponsavelCustoEmpresa(int) 

    /**
     * Sets the value of field 'cdSituacaoManutencaoContrato'.
     * 
     * @param cdSituacaoManutencaoContrato the value of field
     * 'cdSituacaoManutencaoContrato'.
     */
    public void setCdSituacaoManutencaoContrato(int cdSituacaoManutencaoContrato)
    {
        this._cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
        this._has_cdSituacaoManutencaoContrato = true;
    } //-- void setCdSituacaoManutencaoContrato(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoManutencaoContrato'.
     * 
     * @param cdTipoManutencaoContrato the value of field
     * 'cdTipoManutencaoContrato'.
     */
    public void setCdTipoManutencaoContrato(int cdTipoManutencaoContrato)
    {
        this._cdTipoManutencaoContrato = cdTipoManutencaoContrato;
        this._has_cdTipoManutencaoContrato = true;
    } //-- void setCdTipoManutencaoContrato(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoexterno'.
     * 
     * @param cdUsuarioManutencaoexterno the value of field
     * 'cdUsuarioManutencaoexterno'.
     */
    public void setCdUsuarioManutencaoexterno(java.lang.String cdUsuarioManutencaoexterno)
    {
        this._cdUsuarioManutencaoexterno = cdUsuarioManutencaoexterno;
    } //-- void setCdUsuarioManutencaoexterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsAplicFormat'.
     * 
     * @param dsAplicFormat the value of field 'dsAplicFormat'.
     */
    public void setDsAplicFormat(java.lang.String dsAplicFormat)
    {
        this._dsAplicFormat = dsAplicFormat;
    } //-- void setDsAplicFormat(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsCodMeioAlternRemessa'.
     * 
     * @param dsCodMeioAlternRemessa the value of field
     * 'dsCodMeioAlternRemessa'.
     */
    public void setDsCodMeioAlternRemessa(java.lang.String dsCodMeioAlternRemessa)
    {
        this._dsCodMeioAlternRemessa = dsCodMeioAlternRemessa;
    } //-- void setDsCodMeioAlternRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsCodMeioPrincipalRemessa'.
     * 
     * @param dsCodMeioPrincipalRemessa the value of field
     * 'dsCodMeioPrincipalRemessa'.
     */
    public void setDsCodMeioPrincipalRemessa(java.lang.String dsCodMeioPrincipalRemessa)
    {
        this._dsCodMeioPrincipalRemessa = dsCodMeioPrincipalRemessa;
    } //-- void setDsCodMeioPrincipalRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsCodTipoLayout'.
     * 
     * @param dsCodTipoLayout the value of field 'dsCodTipoLayout'.
     */
    public void setDsCodTipoLayout(java.lang.String dsCodTipoLayout)
    {
        this._dsCodTipoLayout = dsCodTipoLayout;
    } //-- void setDsCodTipoLayout(java.lang.String) 

    /**
     * Sets the value of field 'dsEmpresa'.
     * 
     * @param dsEmpresa the value of field 'dsEmpresa'.
     */
    public void setDsEmpresa(java.lang.String dsEmpresa)
    {
        this._dsEmpresa = dsEmpresa;
    } //-- void setDsEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorTipoManutencao'.
     * 
     * @param dsIndicadorTipoManutencao the value of field
     * 'dsIndicadorTipoManutencao'.
     */
    public void setDsIndicadorTipoManutencao(java.lang.String dsIndicadorTipoManutencao)
    {
        this._dsIndicadorTipoManutencao = dsIndicadorTipoManutencao;
    } //-- void setDsIndicadorTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsLayoutProprio'.
     * 
     * @param dsLayoutProprio the value of field 'dsLayoutProprio'.
     */
    public void setDsLayoutProprio(java.lang.String dsLayoutProprio)
    {
        this._dsLayoutProprio = dsLayoutProprio;
    } //-- void setDsLayoutProprio(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioAltrnRetorno'.
     * 
     * @param dsMeioAltrnRetorno the value of field
     * 'dsMeioAltrnRetorno'.
     */
    public void setDsMeioAltrnRetorno(java.lang.String dsMeioAltrnRetorno)
    {
        this._dsMeioAltrnRetorno = dsMeioAltrnRetorno;
    } //-- void setDsMeioAltrnRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsMotivoManuContr'.
     * 
     * @param dsMotivoManuContr the value of field
     * 'dsMotivoManuContr'.
     */
    public void setDsMotivoManuContr(java.lang.String dsMotivoManuContr)
    {
        this._dsMotivoManuContr = dsMotivoManuContr;
    } //-- void setDsMotivoManuContr(java.lang.String) 

    /**
     * Sets the value of field 'dsResponsavelCustoEmpresa'.
     * 
     * @param dsResponsavelCustoEmpresa the value of field
     * 'dsResponsavelCustoEmpresa'.
     */
    public void setDsResponsavelCustoEmpresa(java.lang.String dsResponsavelCustoEmpresa)
    {
        this._dsResponsavelCustoEmpresa = dsResponsavelCustoEmpresa;
    } //-- void setDsResponsavelCustoEmpresa(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoManutContr'.
     * 
     * @param dsSituacaoManutContr the value of field
     * 'dsSituacaoManutContr'.
     */
    public void setDsSituacaoManutContr(java.lang.String dsSituacaoManutContr)
    {
        this._dsSituacaoManutContr = dsSituacaoManutContr;
    } //-- void setDsSituacaoManutContr(java.lang.String) 

    /**
     * Sets the value of field 'dsUtilizacaoEmpresaVan'.
     * 
     * @param dsUtilizacaoEmpresaVan the value of field
     * 'dsUtilizacaoEmpresaVan'.
     */
    public void setDsUtilizacaoEmpresaVan(java.lang.String dsUtilizacaoEmpresaVan)
    {
        this._dsUtilizacaoEmpresaVan = dsUtilizacaoEmpresaVan;
    } //-- void setDsUtilizacaoEmpresaVan(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrAditivoContratoNegocio'.
     * 
     * @param nrAditivoContratoNegocio the value of field
     * 'nrAditivoContratoNegocio'.
     */
    public void setNrAditivoContratoNegocio(long nrAditivoContratoNegocio)
    {
        this._nrAditivoContratoNegocio = nrAditivoContratoNegocio;
        this._has_nrAditivoContratoNegocio = true;
    } //-- void setNrAditivoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrManutencaoContratoNegocio'.
     * 
     * @param nrManutencaoContratoNegocio the value of field
     * 'nrManutencaoContratoNegocio'.
     */
    public void setNrManutencaoContratoNegocio(long nrManutencaoContratoNegocio)
    {
        this._nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
        this._has_nrManutencaoContratoNegocio = true;
    } //-- void setNrManutencaoContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'pcCustoOrganizacaoTransmissao'.
     * 
     * @param pcCustoOrganizacaoTransmissao the value of field
     * 'pcCustoOrganizacaoTransmissao'.
     */
    public void setPcCustoOrganizacaoTransmissao(java.math.BigDecimal pcCustoOrganizacaoTransmissao)
    {
        this._pcCustoOrganizacaoTransmissao = pcCustoOrganizacaoTransmissao;
    } //-- void setPcCustoOrganizacaoTransmissao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'qtMesRegistroTrafg'.
     * 
     * @param qtMesRegistroTrafg the value of field
     * 'qtMesRegistroTrafg'.
     */
    public void setQtMesRegistroTrafg(long qtMesRegistroTrafg)
    {
        this._qtMesRegistroTrafg = qtMesRegistroTrafg;
        this._has_qtMesRegistroTrafg = true;
    } //-- void setQtMesRegistroTrafg(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarManutencaoMeioTransmissaoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaomeiotransmissao.response.ConsultarManutencaoMeioTransmissaoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaomeiotransmissao.response.ConsultarManutencaoMeioTransmissaoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaomeiotransmissao.response.ConsultarManutencaoMeioTransmissaoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmanutencaomeiotransmissao.response.ConsultarManutencaoMeioTransmissaoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
