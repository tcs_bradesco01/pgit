/**
 * 
 */
package br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean;


/**
 * @author user
 *
 */
public class HistoricoLanctoPersonalizadoSaidaDTO {
	
	/** Atributo codigoLancamentoPersonalizado. */
	private Integer codigoLancamentoPersonalizado;
	
	/** Atributo dataManutencaoDesc. */
	private String dataManutencaoDesc;
	
	/** Atributo responsavel. */
	private String responsavel;
	
	/** Atributo tipoManutencao. */
	private Integer tipoManutencao;
	
	/** Atributo dataManutencao. */
	private String dataManutencao;
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;	
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: codigoLancamentoPersonalizado.
	 *
	 * @return codigoLancamentoPersonalizado
	 */
	public Integer getCodigoLancamentoPersonalizado() {
		return codigoLancamentoPersonalizado;
	}
	
	/**
	 * Set: codigoLancamentoPersonalizado.
	 *
	 * @param codigoLancamentoPersonalizado the codigo lancamento personalizado
	 */
	public void setCodigoLancamentoPersonalizado(
			Integer codigoLancamentoPersonalizado) {
		this.codigoLancamentoPersonalizado = codigoLancamentoPersonalizado;
	}
	
	/**
	 * Get: dataManutencao.
	 *
	 * @return dataManutencao
	 */
	public String getDataManutencao() {
		return dataManutencao;
	}
	
	/**
	 * Set: dataManutencao.
	 *
	 * @param dataManutencao the data manutencao
	 */
	public void setDataManutencao(String dataManutencao) {
		this.dataManutencao = dataManutencao;
	}
	
	/**
	 * Get: responsavel.
	 *
	 * @return responsavel
	 */
	public String getResponsavel() {
		return responsavel;
	}
	
	/**
	 * Set: responsavel.
	 *
	 * @param responsavel the responsavel
	 */
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	/**
	 * Get: tipoManutencao.
	 *
	 * @return tipoManutencao
	 */
	public Integer getTipoManutencao() {
		return tipoManutencao;
	}
	
	/**
	 * Set: tipoManutencao.
	 *
	 * @param tipoManutencao the tipo manutencao
	 */
	public void setTipoManutencao(Integer tipoManutencao) {
		this.tipoManutencao = tipoManutencao;
	}
	
	/**
	 * Get: dataManutencaoDesc.
	 *
	 * @return dataManutencaoDesc
	 */
	public String getDataManutencaoDesc() {
		return dataManutencaoDesc;
	}
	
	/**
	 * Set: dataManutencaoDesc.
	 *
	 * @param dataManutencaoDesc the data manutencao desc
	 */
	public void setDataManutencaoDesc(String dataManutencaoDesc) {
		this.dataManutencaoDesc = dataManutencaoDesc;
	}

}
