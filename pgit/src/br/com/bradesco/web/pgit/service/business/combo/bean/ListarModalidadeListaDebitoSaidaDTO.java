/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarModalidadeListaDebitoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarModalidadeListaDebitoSaidaDTO {

	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
    
    /** Atributo dsProdutoOperacaoRelacionada. */
    private String dsProdutoOperacaoRelacionada;
    
    /** Atributo cdRelacionamentoProduto. */
    private Integer cdRelacionamentoProduto;
    
    /** Atributo cdServicoCompostoPgit. */
    private Long cdServicoCompostoPgit;
	
	/**
	 * Listar modalidade lista debito saida dto.
	 */
	public ListarModalidadeListaDebitoSaidaDTO(){
	}

	/**
	 * Listar modalidade lista debito saida dto.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 * @param dsProdutoOperacaoRelacionada the ds produto operacao relacionada
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 * @param cdServicoCompostoPgit the cd servico composto pgit
	 */
	public ListarModalidadeListaDebitoSaidaDTO(Integer cdProdutoOperacaoRelacionado, String dsProdutoOperacaoRelacionada, Integer cdRelacionamentoProduto, Long cdServicoCompostoPgit){
		
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
		this.dsProdutoOperacaoRelacionada = dsProdutoOperacaoRelacionada;
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
		this.cdServicoCompostoPgit = cdServicoCompostoPgit;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}

	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}

	/**
	 * Get: cdServicoCompostoPgit.
	 *
	 * @return cdServicoCompostoPgit
	 */
	public Long getCdServicoCompostoPgit() {
		return cdServicoCompostoPgit;
	}

	/**
	 * Set: cdServicoCompostoPgit.
	 *
	 * @param cdServicoCompostoPgit the cd servico composto pgit
	 */
	public void setCdServicoCompostoPgit(Long cdServicoCompostoPgit) {
		this.cdServicoCompostoPgit = cdServicoCompostoPgit;
	}

	/**
	 * Get: dsProdutoOperacaoRelacionada.
	 *
	 * @return dsProdutoOperacaoRelacionada
	 */
	public String getDsProdutoOperacaoRelacionada() {
		return dsProdutoOperacaoRelacionada;
	}

	/**
	 * Set: dsProdutoOperacaoRelacionada.
	 *
	 * @param dsProdutoOperacaoRelacionada the ds produto operacao relacionada
	 */
	public void setDsProdutoOperacaoRelacionada(String dsProdutoOperacaoRelacionada) {
		this.dsProdutoOperacaoRelacionada = dsProdutoOperacaoRelacionada;
	}

}
