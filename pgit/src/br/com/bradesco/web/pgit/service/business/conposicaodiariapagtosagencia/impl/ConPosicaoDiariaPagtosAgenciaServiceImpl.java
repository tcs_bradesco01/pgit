/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.IConPosicaoDiariaPagtosAgenciaService;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.IConPosicaoDiariaPagtosAgenciaServiceConstants;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ConsultarPosDiaPagtoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ConsultarPosDiaPagtoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ConsultarSaldoAtualEntradaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ConsultarSaldoAtualSaidaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.DetalharPagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.DetalharPagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.DetalharPorContaDebitoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.DetalharPorContaDebitoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ValidarAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ValidarAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarposdiapagtoagencia.request.ConsultarPosDiaPagtoAgenciaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarposdiapagtoagencia.response.ConsultarPosDiaPagtoAgenciaResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.request.ConsultarSaldoAtualRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.response.ConsultarSaldoAtualResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentos.request.DetalharPagamentosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharpagamentos.response.DetalharPagamentosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.request.DetalharPorContaDebitoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.response.DetalharPorContaDebitoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.validaragencia.request.ValidarAgenciaRequest;
import br.com.bradesco.web.pgit.service.data.pdc.validaragencia.response.ValidarAgenciaResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;



/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ConPosicaoDiariaPagtosAgencia
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConPosicaoDiariaPagtosAgenciaServiceImpl implements IConPosicaoDiariaPagtosAgenciaService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;	
		
	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.IConPosicaoDiariaPagtosAgenciaService#consultarPosicaoDiariaPagtosAgencia(br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ConsultarPosDiaPagtoAgenciaEntradaDTO)
     */
    public List<ConsultarPosDiaPagtoAgenciaSaidaDTO> consultarPosicaoDiariaPagtosAgencia(ConsultarPosDiaPagtoAgenciaEntradaDTO entradaDTO){
    	ConsultarPosDiaPagtoAgenciaRequest request = new ConsultarPosDiaPagtoAgenciaRequest();
    	ConsultarPosDiaPagtoAgenciaResponse response = new ConsultarPosDiaPagtoAgenciaResponse();
    	List<ConsultarPosDiaPagtoAgenciaSaidaDTO> listaSaida = new ArrayList<ConsultarPosDiaPagtoAgenciaSaidaDTO>();
    		
    	request.setNrOcorrencias(IConPosicaoDiariaPagtosAgenciaServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entradaDTO.getCdUnidadeOrganizacional()));
    	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
    	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
    	
    	response = getFactoryAdapter().getConsultarPosDiaPagtoAgenciaPDCAdapter().invokeProcess(request);
    	
    	ConsultarPosDiaPagtoAgenciaSaidaDTO saida;
    	
    	for(int i=0;i<response.getOcorrenciasCount();i++){
    		saida = new ConsultarPosDiaPagtoAgenciaSaidaDTO();
    		
    		saida.setCodMensagem(response.getCodMensagem());
    		saida.setMensagem(response.getMensagem());
    		saida.setNumeroConsultas(response.getNumeroConsultas());
    	
    		saida.setCdCorpoCnpjCpf(response.getOcorrencias(i).getCdCorpoCnpjCpf());
    		saida.setCdCnpjCpfFilial(response.getOcorrencias(i).getCdCnpjCpfFilial());
    		saida.setCdCnpjCpfDigito(response.getOcorrencias(i).getCdCnpjCpfDigito());
    		
    		saida.setCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(response.getOcorrencias(i).getCdCorpoCnpjCpf(),response.getOcorrencias(i).getCdCnpjCpfFilial(),response.getOcorrencias(i).getCdCnpjCpfDigito()));
    		
    		saida.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
    		saida.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
    		saida.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
    		saida.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
    		saida.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
    		saida.setCdTipoContratoRec(response.getOcorrencias(i).getCdTipoContratoRec());
    		saida.setDsTipoContratoNegocio(response.getOcorrencias(i).getDsTipoContratoNegocio());
    		saida.setCdContratoOrigemRec(response.getOcorrencias(i).getCdContratoOrigemRec());
    		saida.setCdServico(response.getOcorrencias(i).getCdServico());
    		saida.setDsServico(response.getOcorrencias(i).getDsServico());
    		saida.setCdModalidade(response.getOcorrencias(i).getCdModalidade());
    		saida.setDsModalidade(response.getOcorrencias(i).getDsModalidade());
    		
    		saida.setVlPagamentoAgendado(response.getOcorrencias(i).getVlPagamentoAgendado());    		
    		saida.setVlPagtAgenFormatado(NumberUtils.format(response.getOcorrencias(i).getVlPagamentoAgendado()));
    		
    		saida.setVlPagamentoPendente(response.getOcorrencias(i).getVlPagamentoPendente());
    		saida.setVlPagtPendFormatado(NumberUtils.format(response.getOcorrencias(i).getVlPagamentoPendente()));
    		
    		saida.setVlPagamentoSemSaldo(response.getOcorrencias(i).getVlPagamentoSemSaldo());
    		saida.setVlPagtSemSaldoFormatado(NumberUtils.format(response.getOcorrencias(i).getVlPagamentoSemSaldo()));
    		
    		saida.setVlPagamentoEfetivado(response.getOcorrencias(i).getVlPagamentoEfetivado());
    		saida.setVlPagtEfetFormatado(NumberUtils.format(response.getOcorrencias(i).getVlPagamentoEfetivado()));
    		
    		saida.setVlTotalPagamentos(response.getOcorrencias(i).getVlTotalPagamentos());
    		saida.setVlTotalPagtFormatado(NumberUtils.format(response.getOcorrencias(i).getVlTotalPagamentos()));
    		 
    		listaSaida.add(saida);   		
    	
    	}
    	
    	return listaSaida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.IConPosicaoDiariaPagtosAgenciaService#detalharPorContaDebito(br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.DetalharPorContaDebitoEntradaDTO)
     */
    public List<DetalharPorContaDebitoSaidaDTO> detalharPorContaDebito(DetalharPorContaDebitoEntradaDTO entradaDTO){
    	DetalharPorContaDebitoRequest request = new DetalharPorContaDebitoRequest();
    	DetalharPorContaDebitoResponse response = new DetalharPorContaDebitoResponse();
    	List<DetalharPorContaDebitoSaidaDTO> listaSaida = new ArrayList<DetalharPorContaDebitoSaidaDTO>();
    	
    	request.setNrOcorrencias(IConPosicaoDiariaPagtosAgenciaServiceConstants.NUMERO_OCORRENCIAS_DET_CONTA_DEBITO);
    	request.setCdCpfCnpj(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpj()));
    	request.setCdFilialCpfCnpj(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCpfCnpj()));
    	request.setCdDigitoCpfCnpj(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDigitoCpfCnpj()));
    	request.setCdUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entradaDTO.getCdUnidadeOrganizacional()));    
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
        request.setDsComplemento(PgitUtil.verificaStringNula(entradaDTO.getDsComplemento()));
    
    	response = getFactoryAdapter().getDetalharPorContaDebitoPDCAdapter().invokeProcess(request);    	
    	DetalharPorContaDebitoSaidaDTO saida;
    	
    	for(int i=0;i<response.getOcorrenciasCount();i++){
    		saida = new DetalharPorContaDebitoSaidaDTO();
    		
    		saida.setCodMensagem(response.getCodMensagem());
    		saida.setMensagem(response.getMensagem());
    		saida.setNrConsultas(response.getNrConsultas());
    		
    		saida.setCdBanco(response.getOcorrencias(i).getCdBanco());
    		saida.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
    		saida.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
    		saida.setCdContaBancaria(response.getOcorrencias(i).getCdContaBancaria());
    		saida.setCdDigitoContaBancaria(response.getOcorrencias(i).getCdDigitoContaBancaria());
    		saida.setVlrPagamentoPendente(response.getOcorrencias(i).getVlrPagamentoPendente());
    		saida.setVlrPagamentoSemSaldo(response.getOcorrencias(i).getVlrPagamentoSemSaldo());
    		saida.setVlrPagamentoAgendado(response.getOcorrencias(i).getVlrPagamentoAgendado());
    		saida.setVlrPagamentoEfetivado(response.getOcorrencias(i).getVlrPagamentoEfetivado());
    		saida.setVlTotalPagamentos(response.getOcorrencias(i).getVlTotalPagamentos());
    		saida.setCdTipoConta(response.getOcorrencias(i).getCdTipoConta());
    		
    	    saida.setCdPessoaContratoDebito(response.getOcorrencias(i).getCdPessoaContratoDebito());
    	    saida.setCdTipoContratoDebito(response.getOcorrencias(i).getCdTipoContratoDebito());
    	    saida.setNrSequenciaContratoDebito(response.getOcorrencias(i).getNrSequenciaContratoDebito());  
    		
    		saida.setBancoAgenciaConta(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBanco(), response.getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(), response.getOcorrencias(i).getCdContaBancaria(), response.getOcorrencias(i).getCdDigitoContaBancaria(), true));
    		
    		listaSaida.add(saida);       		
    	}
    	
      return listaSaida;
    }  
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.IConPosicaoDiariaPagtosAgenciaService#detalharPagamentos(br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.DetalharPagamentosEntradaDTO)
     */
    public List<DetalharPagamentosSaidaDTO> detalharPagamentos(DetalharPagamentosEntradaDTO entradaDTO){
    	DetalharPagamentosRequest request = new DetalharPagamentosRequest();
    	DetalharPagamentosResponse response = new DetalharPagamentosResponse();    
        List<DetalharPagamentosSaidaDTO> listaSaida = new ArrayList<DetalharPagamentosSaidaDTO>();
        

    	request.setNrOcorrencias(IConPosicaoDiariaPagtosAgenciaServiceConstants.NUMERO_OCORRENCIAS_DET_PAGAMENTOS);
    	request.setCdCpfCnpj(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpj()));
    	request.setCdFilialCpfCnpj(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCpfCnpj()));
    	request.setCdDigitoCpfCnpj(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDigitoCpfCnpj()));
    	request.setCdUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entradaDTO.getCdUnidadeOrganizacional()));    
    	request.setCdServico(PgitUtil.verificaIntegerNulo(entradaDTO.getCdServico()));
    	request.setCdModalidade(PgitUtil.verificaIntegerNulo(entradaDTO.getCdModalidade()));
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoDebito()));
    	request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaDebito()));
    	request.setCdContaDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdContaDebito()));
    	request.setDigitoContaDebito(PgitUtil.verificaStringNula(entradaDTO.getDigitoContaDebito()));
    	request.setCdDigitoAgenciaDebito(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoAgenciaDebito()));
    	request.setDsComplemento(PgitUtil.verificaStringNula(entradaDTO.getDsComplemento()));
    	
    	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
    	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
    	request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
    	
    	response = getFactoryAdapter().getDetalharPagamentosPDCAdapter().invokeProcess(request); 
    	
    	for(int i=0;i<response.getOcorrenciasCount();i++){
    		DetalharPagamentosSaidaDTO saida = new DetalharPagamentosSaidaDTO();
    		
    		saida.setCodMensagem(response.getCodMensagem());
    		saida.setMensagem(response.getMensagem());
    		saida.setNumeroConsultas(response.getNumeroConsultas());
    		
    		saida.setCdCorpoCnpjCpf(response.getOcorrencias(i).getCdCorpoCnpjCpf());
    		saida.setCdFilialCnpjCpf(response.getOcorrencias(i).getCdFilialCnpjCpf());
    		saida.setCdDigitoCnpjCpf(response.getOcorrencias(i).getCdDigitoCnpjCpf());
    		
    		saida.setCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(response.getOcorrencias(i).getCdCorpoCnpjCpf(),response.getOcorrencias(i).getCdFilialCnpjCpf(),response.getOcorrencias(i).getCdDigitoCnpjCpf()));
    		
    		saida.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
    		saida.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
    		saida.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
       		saida.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());	
       		saida.setCdNumeroContrato(response.getOcorrencias(i).getCdNumeroContrato());
       		saida.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
       		saida.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
       		saida.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
       		saida.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
       		saida.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
       		saida.setDtCreditoPagamento(DateUtils.trocarSeparadorDeData(response.getOcorrencias(i).getDtCreditoPagamento(), ".", "/"));
       		saida.setVlEfetivacaoPagamentoCliente(response.getOcorrencias(i).getVlEfetivacaoPagamentoCliente());
       		saida.setCdInscricaoFavorecidoCliente(response.getOcorrencias(i).getCdInscricaoFavorecidoCliente());
       		saida.setDsAbreviacaoFavorecidoCliente(response.getOcorrencias(i).getDsAbreviacaoFavorecidoCliente());
       		saida.setBeneficiarioFavorecido(PgitUtil.concatenarCampos(PgitUtil.verificaZero(response.getOcorrencias(i).getCdInscricaoFavorecidoCliente()),response.getOcorrencias(i).getDsAbreviacaoFavorecidoCliente()));
       		
       		saida.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
       		saida.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
       		saida.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
       		saida.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
      		saida.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
      		saida.setContaDebitoFormatada(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoDebito(),response.getOcorrencias(i).getCdAgenciaDebito(),response.getOcorrencias(i).getCdDigitoAgenciaDebito(),response.getOcorrencias(i).getCdContaDebito(),response.getOcorrencias(i).getCdDigitoContaDebito(), true));

      		saida.setCdBancoCredito(response.getOcorrencias(i).getCdBancoCredito());
      		saida.setCdAgenciaCredito(response.getOcorrencias(i).getCdAgenciaCredito());
      		saida.setCdDigitoAgenciaCredito(response.getOcorrencias(i).getCdDigitoAgenciaCredito());
      		saida.setCdContaCredito(response.getOcorrencias(i).getCdContaCredito());
      		saida.setCdDigitoContaCredito(response.getOcorrencias(i).getCdDigitoContaCredito());

      		saida.setBancoCreditoFormatado(PgitUtil.formatBanco(response.getOcorrencias(i).getCdBancoCredito(), false));
      		saida.setAgenciaCreditoFormatada(PgitUtil.formatAgencia(response.getOcorrencias(i).getCdAgenciaCredito(), response.getOcorrencias(i).getCdDigitoAgenciaCredito(), false));
      		saida.setContaDigitoCreditoFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdContaCredito(),response.getOcorrencias(i).getCdDigitoContaCredito(), false));
      		saida.setContaCreditoFormatada(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoCredito(),response.getOcorrencias(i).getCdAgenciaCredito(),response.getOcorrencias(i).getCdDigitoAgenciaCredito(),response.getOcorrencias(i).getCdContaCredito(),response.getOcorrencias(i).getCdDigitoContaCredito(), true));
      		
      		saida.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
      		saida.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
      		saida.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
      		saida.setDsMotivoSituacaoPagamento(response.getOcorrencias(i).getDsMotivoSituacaoPagamento());
      		saida.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
      		saida.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());  
      		saida.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
      		saida.setDsIndicadorPagamento(response.getOcorrencias(i).getDsIndicadorPagamento());
      		saida.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
    		saida.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());
    			      		
       	    listaSaida.add(saida);     		
    	}
    	
     return listaSaida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.IConPosicaoDiariaPagtosAgenciaService#consultarSaldoAtual(br.com.bradesco.web.pgit.service.business.conposicaodiariapagtosagencia.bean.ConsultarSaldoAtualEntradaDTO)
     */
    public ConsultarSaldoAtualSaidaDTO consultarSaldoAtual(ConsultarSaldoAtualEntradaDTO entradaDTO){
    	ConsultarSaldoAtualRequest request = new ConsultarSaldoAtualRequest();
    	ConsultarSaldoAtualResponse response = new ConsultarSaldoAtualResponse();    	   	
      	
    	request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
    	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
    	request.setNrCnpjCpf(PgitUtil.verificaLongNulo(entradaDTO.getNrCnpjCpf()));
    	request.setCdCnpjCpfFilial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCnpjCpfFilial()));
    	request.setCdCnpjCpfDigito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCnpjCpfDigito()));
    	request.setDsComplemento(PgitUtil.verificaStringNula(entradaDTO.getDsComplemento()));
    	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
    	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
    	request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
    	
    	response = getFactoryAdapter().getConsultarSaldoAtualPDCAdapter().invokeProcess(request);
    	
      	ConsultarSaldoAtualSaidaDTO saidaDTO = new ConsultarSaldoAtualSaidaDTO();
      	
    	saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
    	saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
    	saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
    	saidaDTO.setDsTipoSaldo(response.getDsTipoSaldo());
    	saidaDTO.setQtSemSaldo(response.getQtSemSaldo());
    	saidaDTO.setVlSemSaldo(response.getVlSemSaldo());
    	saidaDTO.setQtPendenteSemSaldo(response.getQtPendenteSemSaldo());
    	saidaDTO.setVlPendenteSemSaldo(response.getVlPendenteSemSaldo());
    	saidaDTO.setQtAgendadoSemSaldo(response.getQtAgendadoSemSaldo());
    	saidaDTO.setVlAgendadoSemSaldo(response.getVlAgendadoSemSaldo());
    	saidaDTO.setQtEfetivadoSemSaldo(response.getQtEfetivadoSemSaldo());
    	saidaDTO.setVlEfetivadoSemSaldo(response.getVlEfetivadoSemSaldo());
    	saidaDTO.setVlSaldoLivre(response.getVlSaldoLivre());
    	saidaDTO.setVlReservaDisponivel(response.getVlReservaDisponivel());
    	saidaDTO.setVlReservaNaoDisponivel(response.getVlReservaNaoDisponivel());
    	saidaDTO.setVlTotal(response.getVlTotal());
    	
    	return saidaDTO;
    }

	public ValidarAgenciaSaidaDTO validarAgencia(
			ValidarAgenciaEntradaDTO entradaDTO) {
		
		ValidarAgenciaRequest request = new ValidarAgenciaRequest();
		request.setMaxOcorrencias(verificaIntegerNulo(entradaDTO.getMaxOcorrencias()));
		
		
		ValidarAgenciaResponse response = getFactoryAdapter().getValidarAgenciaPDCAdapter().invokeProcess(request);
		
		ValidarAgenciaSaidaDTO saidaDTO = new ValidarAgenciaSaidaDTO();
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
	    saidaDTO.setMensagem(response.getMensagem());
	    saidaDTO.setCdTipoUnidadeOrganizacional(response.getCdTipoUnidadeOrganizacional());
	    saidaDTO.setCdUnidadeOrganizacional(response.getCdUnidadeOrganizacional());
	    saidaDTO.setCdIndicadorBloq(response.getCdIndicadorBloq());

		return saidaDTO;
		
	}
}

