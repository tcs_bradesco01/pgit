/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean;

/**
 * Nome: ManterEmpresaAcompanhadaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterEmpresaAcompanhadaEntradaDTO {
	
	/** Atributo cdCnpjCpf. */
	private Long cdCnpjCpf;
    
    /** Atributo cdFilialCnpjCpf. */
    private Integer cdFilialCnpjCpf;
    
    /** Atributo cdCtrlCnpjCPf. */
    private Integer cdCtrlCnpjCPf;
    
    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;
    
    /** Atributo cdJuncaoAgencia. */
    private Integer cdJuncaoAgencia;
    
    /** Atributo cdContaCorrente. */
    private Integer cdContaCorrente;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo cdConveCtaSalarial. */
    private Long cdConveCtaSalarial;
    
    /** Atributo cdIndicadorPrevidencia. */
    private String cdIndicadorPrevidencia;
    
	/**
	 * Get: cdCnpjCpf.
	 *
	 * @return cdCnpjCpf
	 */
	public Long getCdCnpjCpf() {
		return cdCnpjCpf;
	}
	
	/**
	 * Set: cdCnpjCpf.
	 *
	 * @param cdCnpjCpf the cd cnpj cpf
	 */
	public void setCdCnpjCpf(Long cdCnpjCpf) {
		this.cdCnpjCpf = cdCnpjCpf;
	}
	
	/**
	 * Get: cdFilialCnpjCpf.
	 *
	 * @return cdFilialCnpjCpf
	 */
	public Integer getCdFilialCnpjCpf() {
		return cdFilialCnpjCpf;
	}
	
	/**
	 * Set: cdFilialCnpjCpf.
	 *
	 * @param cdFilialCnpjCpf the cd filial cnpj cpf
	 */
	public void setCdFilialCnpjCpf(Integer cdFilialCnpjCpf) {
		this.cdFilialCnpjCpf = cdFilialCnpjCpf;
	}
	
	/**
	 * Get: cdCtrlCnpjCPf.
	 *
	 * @return cdCtrlCnpjCPf
	 */
	public Integer getCdCtrlCnpjCPf() {
		return cdCtrlCnpjCPf;
	}
	
	/**
	 * Set: cdCtrlCnpjCPf.
	 *
	 * @param cdCtrlCnpjCPf the cd ctrl cnpj c pf
	 */
	public void setCdCtrlCnpjCPf(Integer cdCtrlCnpjCPf) {
		this.cdCtrlCnpjCPf = cdCtrlCnpjCPf;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: cdJuncaoAgencia.
	 *
	 * @return cdJuncaoAgencia
	 */
	public Integer getCdJuncaoAgencia() {
		return cdJuncaoAgencia;
	}
	
	/**
	 * Set: cdJuncaoAgencia.
	 *
	 * @param cdJuncaoAgencia the cd juncao agencia
	 */
	public void setCdJuncaoAgencia(Integer cdJuncaoAgencia) {
		this.cdJuncaoAgencia = cdJuncaoAgencia;
	}
	
	/**
	 * Get: cdContaCorrente.
	 *
	 * @return cdContaCorrente
	 */
	public Integer getCdContaCorrente() {
		return cdContaCorrente;
	}
	
	/**
	 * Set: cdContaCorrente.
	 *
	 * @param cdContaCorrente the cd conta corrente
	 */
	public void setCdContaCorrente(Integer cdContaCorrente) {
		this.cdContaCorrente = cdContaCorrente;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdConveCtaSalarial.
	 *
	 * @return cdConveCtaSalarial
	 */
	public Long getCdConveCtaSalarial() {
		return cdConveCtaSalarial;
	}
	
	/**
	 * Set: cdConveCtaSalarial.
	 *
	 * @param cdConveCtaSalarial the cd conve cta salarial
	 */
	public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
		this.cdConveCtaSalarial = cdConveCtaSalarial;
	}
	
	/**
	 * Get: cdIndicadorPrevidencia.
	 *
	 * @return cdIndicadorPrevidencia
	 */
	public String getCdIndicadorPrevidencia() {
		return cdIndicadorPrevidencia;
	}
	
	/**
	 * Set: cdIndicadorPrevidencia.
	 *
	 * @param cdIndicadorPrevidencia the cd indicador previdencia
	 */
	public void setCdIndicadorPrevidencia(String cdIndicadorPrevidencia) {
		this.cdIndicadorPrevidencia = cdIndicadorPrevidencia;
	}
}