/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultardetalhehistoricocontafavorecido.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarDetalheHistoricoContaFavorecidoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarDetalheHistoricoContaFavorecidoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdBloqueioContaFavorecido
     */
    private int _cdBloqueioContaFavorecido = 0;

    /**
     * keeps track of state for field: _cdBloqueioContaFavorecido
     */
    private boolean _has_cdBloqueioContaFavorecido;

    /**
     * Field _dsBloqueioConta
     */
    private java.lang.String _dsBloqueioConta;

    /**
     * Field _dsObservacaoContaFavorecido
     */
    private java.lang.String _dsObservacaoContaFavorecido;

    /**
     * Field _cdTipoContaFavorecido
     */
    private int _cdTipoContaFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoContaFavorecido
     */
    private boolean _has_cdTipoContaFavorecido;

    /**
     * Field _cdIndicadorRetornoCliente
     */
    private int _cdIndicadorRetornoCliente = 0;

    /**
     * keeps track of state for field: _cdIndicadorRetornoCliente
     */
    private boolean _has_cdIndicadorRetornoCliente;

    /**
     * Field _cdSituacaoContaFavorecido
     */
    private int _cdSituacaoContaFavorecido = 0;

    /**
     * keeps track of state for field: _cdSituacaoContaFavorecido
     */
    private boolean _has_cdSituacaoContaFavorecido;

    /**
     * Field _hrBloqueioContaFavorecido
     */
    private java.lang.String _hrBloqueioContaFavorecido;

    /**
     * Field _cdIndicadorTipoManutencao
     */
    private int _cdIndicadorTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdIndicadorTipoManutencao
     */
    private boolean _has_cdIndicadorTipoManutencao;

    /**
     * Field _dsTipoManutencao
     */
    private java.lang.String _dsTipoManutencao;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _dtInclusao
     */
    private java.lang.String _dtInclusao;

    /**
     * Field _hrInclusao
     */
    private java.lang.String _hrInclusao;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterna
     */
    private java.lang.String _cdUsuarioManutencaoExterna;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarDetalheHistoricoContaFavorecidoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhehistoricocontafavorecido.response.ConsultarDetalheHistoricoContaFavorecidoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdBloqueioContaFavorecido
     * 
     */
    public void deleteCdBloqueioContaFavorecido()
    {
        this._has_cdBloqueioContaFavorecido= false;
    } //-- void deleteCdBloqueioContaFavorecido() 

    /**
     * Method deleteCdIndicadorRetornoCliente
     * 
     */
    public void deleteCdIndicadorRetornoCliente()
    {
        this._has_cdIndicadorRetornoCliente= false;
    } //-- void deleteCdIndicadorRetornoCliente() 

    /**
     * Method deleteCdIndicadorTipoManutencao
     * 
     */
    public void deleteCdIndicadorTipoManutencao()
    {
        this._has_cdIndicadorTipoManutencao= false;
    } //-- void deleteCdIndicadorTipoManutencao() 

    /**
     * Method deleteCdSituacaoContaFavorecido
     * 
     */
    public void deleteCdSituacaoContaFavorecido()
    {
        this._has_cdSituacaoContaFavorecido= false;
    } //-- void deleteCdSituacaoContaFavorecido() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoContaFavorecido
     * 
     */
    public void deleteCdTipoContaFavorecido()
    {
        this._has_cdTipoContaFavorecido= false;
    } //-- void deleteCdTipoContaFavorecido() 

    /**
     * Returns the value of field 'cdBloqueioContaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdBloqueioContaFavorecido'.
     */
    public int getCdBloqueioContaFavorecido()
    {
        return this._cdBloqueioContaFavorecido;
    } //-- int getCdBloqueioContaFavorecido() 

    /**
     * Returns the value of field 'cdIndicadorRetornoCliente'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRetornoCliente'.
     */
    public int getCdIndicadorRetornoCliente()
    {
        return this._cdIndicadorRetornoCliente;
    } //-- int getCdIndicadorRetornoCliente() 

    /**
     * Returns the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorTipoManutencao'.
     */
    public int getCdIndicadorTipoManutencao()
    {
        return this._cdIndicadorTipoManutencao;
    } //-- int getCdIndicadorTipoManutencao() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdSituacaoContaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoContaFavorecido'.
     */
    public int getCdSituacaoContaFavorecido()
    {
        return this._cdSituacaoContaFavorecido;
    } //-- int getCdSituacaoContaFavorecido() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoContaFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoContaFavorecido'.
     */
    public int getCdTipoContaFavorecido()
    {
        return this._cdTipoContaFavorecido;
    } //-- int getCdTipoContaFavorecido() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterna'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterna'.
     */
    public java.lang.String getCdUsuarioManutencaoExterna()
    {
        return this._cdUsuarioManutencaoExterna;
    } //-- java.lang.String getCdUsuarioManutencaoExterna() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsBloqueioConta'.
     * 
     * @return String
     * @return the value of field 'dsBloqueioConta'.
     */
    public java.lang.String getDsBloqueioConta()
    {
        return this._dsBloqueioConta;
    } //-- java.lang.String getDsBloqueioConta() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsObservacaoContaFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsObservacaoContaFavorecido'.
     */
    public java.lang.String getDsObservacaoContaFavorecido()
    {
        return this._dsObservacaoContaFavorecido;
    } //-- java.lang.String getDsObservacaoContaFavorecido() 

    /**
     * Returns the value of field 'dsTipoManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoManutencao'.
     */
    public java.lang.String getDsTipoManutencao()
    {
        return this._dsTipoManutencao;
    } //-- java.lang.String getDsTipoManutencao() 

    /**
     * Returns the value of field 'dtInclusao'.
     * 
     * @return String
     * @return the value of field 'dtInclusao'.
     */
    public java.lang.String getDtInclusao()
    {
        return this._dtInclusao;
    } //-- java.lang.String getDtInclusao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'hrBloqueioContaFavorecido'.
     * 
     * @return String
     * @return the value of field 'hrBloqueioContaFavorecido'.
     */
    public java.lang.String getHrBloqueioContaFavorecido()
    {
        return this._hrBloqueioContaFavorecido;
    } //-- java.lang.String getHrBloqueioContaFavorecido() 

    /**
     * Returns the value of field 'hrInclusao'.
     * 
     * @return String
     * @return the value of field 'hrInclusao'.
     */
    public java.lang.String getHrInclusao()
    {
        return this._hrInclusao;
    } //-- java.lang.String getHrInclusao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Method hasCdBloqueioContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBloqueioContaFavorecido()
    {
        return this._has_cdBloqueioContaFavorecido;
    } //-- boolean hasCdBloqueioContaFavorecido() 

    /**
     * Method hasCdIndicadorRetornoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRetornoCliente()
    {
        return this._has_cdIndicadorRetornoCliente;
    } //-- boolean hasCdIndicadorRetornoCliente() 

    /**
     * Method hasCdIndicadorTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorTipoManutencao()
    {
        return this._has_cdIndicadorTipoManutencao;
    } //-- boolean hasCdIndicadorTipoManutencao() 

    /**
     * Method hasCdSituacaoContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoContaFavorecido()
    {
        return this._has_cdSituacaoContaFavorecido;
    } //-- boolean hasCdSituacaoContaFavorecido() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoContaFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContaFavorecido()
    {
        return this._has_cdTipoContaFavorecido;
    } //-- boolean hasCdTipoContaFavorecido() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdBloqueioContaFavorecido'.
     * 
     * @param cdBloqueioContaFavorecido the value of field
     * 'cdBloqueioContaFavorecido'.
     */
    public void setCdBloqueioContaFavorecido(int cdBloqueioContaFavorecido)
    {
        this._cdBloqueioContaFavorecido = cdBloqueioContaFavorecido;
        this._has_cdBloqueioContaFavorecido = true;
    } //-- void setCdBloqueioContaFavorecido(int) 

    /**
     * Sets the value of field 'cdIndicadorRetornoCliente'.
     * 
     * @param cdIndicadorRetornoCliente the value of field
     * 'cdIndicadorRetornoCliente'.
     */
    public void setCdIndicadorRetornoCliente(int cdIndicadorRetornoCliente)
    {
        this._cdIndicadorRetornoCliente = cdIndicadorRetornoCliente;
        this._has_cdIndicadorRetornoCliente = true;
    } //-- void setCdIndicadorRetornoCliente(int) 

    /**
     * Sets the value of field 'cdIndicadorTipoManutencao'.
     * 
     * @param cdIndicadorTipoManutencao the value of field
     * 'cdIndicadorTipoManutencao'.
     */
    public void setCdIndicadorTipoManutencao(int cdIndicadorTipoManutencao)
    {
        this._cdIndicadorTipoManutencao = cdIndicadorTipoManutencao;
        this._has_cdIndicadorTipoManutencao = true;
    } //-- void setCdIndicadorTipoManutencao(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoContaFavorecido'.
     * 
     * @param cdSituacaoContaFavorecido the value of field
     * 'cdSituacaoContaFavorecido'.
     */
    public void setCdSituacaoContaFavorecido(int cdSituacaoContaFavorecido)
    {
        this._cdSituacaoContaFavorecido = cdSituacaoContaFavorecido;
        this._has_cdSituacaoContaFavorecido = true;
    } //-- void setCdSituacaoContaFavorecido(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoContaFavorecido'.
     * 
     * @param cdTipoContaFavorecido the value of field
     * 'cdTipoContaFavorecido'.
     */
    public void setCdTipoContaFavorecido(int cdTipoContaFavorecido)
    {
        this._cdTipoContaFavorecido = cdTipoContaFavorecido;
        this._has_cdTipoContaFavorecido = true;
    } //-- void setCdTipoContaFavorecido(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterna'.
     * 
     * @param cdUsuarioManutencaoExterna the value of field
     * 'cdUsuarioManutencaoExterna'.
     */
    public void setCdUsuarioManutencaoExterna(java.lang.String cdUsuarioManutencaoExterna)
    {
        this._cdUsuarioManutencaoExterna = cdUsuarioManutencaoExterna;
    } //-- void setCdUsuarioManutencaoExterna(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsBloqueioConta'.
     * 
     * @param dsBloqueioConta the value of field 'dsBloqueioConta'.
     */
    public void setDsBloqueioConta(java.lang.String dsBloqueioConta)
    {
        this._dsBloqueioConta = dsBloqueioConta;
    } //-- void setDsBloqueioConta(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsObservacaoContaFavorecido'.
     * 
     * @param dsObservacaoContaFavorecido the value of field
     * 'dsObservacaoContaFavorecido'.
     */
    public void setDsObservacaoContaFavorecido(java.lang.String dsObservacaoContaFavorecido)
    {
        this._dsObservacaoContaFavorecido = dsObservacaoContaFavorecido;
    } //-- void setDsObservacaoContaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoManutencao'.
     * 
     * @param dsTipoManutencao the value of field 'dsTipoManutencao'
     */
    public void setDsTipoManutencao(java.lang.String dsTipoManutencao)
    {
        this._dsTipoManutencao = dsTipoManutencao;
    } //-- void setDsTipoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusao'.
     * 
     * @param dtInclusao the value of field 'dtInclusao'.
     */
    public void setDtInclusao(java.lang.String dtInclusao)
    {
        this._dtInclusao = dtInclusao;
    } //-- void setDtInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrBloqueioContaFavorecido'.
     * 
     * @param hrBloqueioContaFavorecido the value of field
     * 'hrBloqueioContaFavorecido'.
     */
    public void setHrBloqueioContaFavorecido(java.lang.String hrBloqueioContaFavorecido)
    {
        this._hrBloqueioContaFavorecido = hrBloqueioContaFavorecido;
    } //-- void setHrBloqueioContaFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusao'.
     * 
     * @param hrInclusao the value of field 'hrInclusao'.
     */
    public void setHrInclusao(java.lang.String hrInclusao)
    {
        this._hrInclusao = hrInclusao;
    } //-- void setHrInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarDetalheHistoricoContaFavorecidoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultardetalhehistoricocontafavorecido.response.ConsultarDetalheHistoricoContaFavorecidoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultardetalhehistoricocontafavorecido.response.ConsultarDetalheHistoricoContaFavorecidoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultardetalhehistoricocontafavorecido.response.ConsultarDetalheHistoricoContaFavorecidoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultardetalhehistoricocontafavorecido.response.ConsultarDetalheHistoricoContaFavorecidoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
