/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarservicos.response.Ocorrencias;

/**
 * Nome: ListarServicosSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ListarServicosSaidaDTO {

	/** Atributo cdServico. */
	private Integer cdServico;

	/** Atributo dsServico. */
	private String dsServico;

	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;

	/** Atributo dsProdutoServicoOperacao. */
	private String dsProdutoServicoOperacao;

	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;

	/** Atributo check. */
	private boolean check;

	/** Atributo cdParametroTela. */
	private Integer cdParametroTela;

	/**
	 * Listar servicos saida dto.
	 */
	public ListarServicosSaidaDTO() {
		this.check = false;
	}

	/**
	 * Listar servicos saida dto.
	 * 
	 * @param cdServico
	 *            the cd servico
	 * @param dsServico
	 *            the ds servico
	 */
	public ListarServicosSaidaDTO(Integer cdServico, String dsServico) {
		super();
		this.cdServico = cdServico;
		this.dsServico = dsServico;
	}

	/**
	 * Listar servicos saida dto.
	 * 
	 * @param cdServico
	 *            the cd servico
	 * @param dsServico
	 *            the ds servico
	 * @param cdProdutoServicoOperacao
	 *            the cd produto servico operacao
	 * @param dsProdutoServicoOperacao
	 *            the ds produto servico operacao
	 * @param cdTipoServico
	 *            the cd tipo servico
	 */
	public ListarServicosSaidaDTO(Integer cdServico, String dsServico,
			Integer cdProdutoServicoOperacao, String dsProdutoServicoOperacao,
			Integer cdTipoServico, Integer cdParametroTela) {
		super();
		this.cdServico = cdServico;
		this.dsServico = dsServico;
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
		this.cdTipoServico = cdTipoServico;
		this.cdParametroTela = cdParametroTela;
	}

	/**
	 * Listar servicos saida dto.
	 * 
	 * @param saida
	 *            the saida
	 */
	public ListarServicosSaidaDTO(Ocorrencias saida) {
		this.cdServico = saida.getCdServico();
		this.dsServico = saida.getDsServico();
	}

	/**
	 * Get: cdServico.
	 * 
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}

	/**
	 * Set: cdServico.
	 * 
	 * @param cdServico
	 *            the cd servico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Get: dsServico.
	 * 
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}

	/**
	 * Set: dsServico.
	 * 
	 * @param dsServico
	 *            the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}

	/**
	 * Is check.
	 * 
	 * @return true, if is check
	 */
	public boolean isCheck() {
		return check;
	}

	/**
	 * Set: check.
	 * 
	 * @param check
	 *            the check
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 * 
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 * 
	 * @param cdProdutoServicoOperacao
	 *            the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: dsProdutoServicoOperacao.
	 * 
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}

	/**
	 * Set: dsProdutoServicoOperacao.
	 * 
	 * @param dsProdutoServicoOperacao
	 *            the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}

	/**
	 * Get: cdTipoServico.
	 * 
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}

	/**
	 * Set: cdTipoServico.
	 * 
	 * @param cdTipoServico
	 *            the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}

	/**
	 * @return the cdParametroTela
	 */
	public Integer getCdParametroTela() {
		return cdParametroTela;
	}

	/**
	 * @param cdParametroTela
	 *            the cdParametroTela to set
	 */
	public void setCdParametroTela(Integer cdParametroTela) {
		this.cdParametroTela = cdParametroTela;
	}

}
