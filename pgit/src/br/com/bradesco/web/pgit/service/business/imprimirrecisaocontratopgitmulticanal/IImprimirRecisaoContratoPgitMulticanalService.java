/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal;

import br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.bean.ImprimirRecisaoContratoPgitMulticanalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.bean.ImprimirRecisaoContratoPgitMulticanalSaidaDTO;

/**
 * Nome: IImprimirRecisaoContratoPgitMulticanalService
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface IImprimirRecisaoContratoPgitMulticanalService {

	/**
	 * Imprimir recisao contrato.
	 *
	 * @param entrada the entrada
	 * @return the imprimir recisao contrato pgit multicanal saida dto
	 */
	ImprimirRecisaoContratoPgitMulticanalSaidaDTO imprimirRecisaoContrato(ImprimirRecisaoContratoPgitMulticanalEntradaDTO entrada);
}