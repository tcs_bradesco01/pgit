/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: FinalidadeEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FinalidadeEntradaDTO {
	
	/** Atributo cdFinalidade. */
	private int cdFinalidade;
	
	/** Atributo dsFinalidade. */
	private String dsFinalidade;
	
	/**
	 * Get: cdFinalidade.
	 *
	 * @return cdFinalidade
	 */
	public int getCdFinalidade() {
		return cdFinalidade;
	}
	
	/**
	 * Set: cdFinalidade.
	 *
	 * @param cdFinalidade the cd finalidade
	 */
	public void setCdFinalidade(int cdFinalidade) {
		this.cdFinalidade = cdFinalidade;
	}
	
	/**
	 * Get: dsFinalidade.
	 *
	 * @return dsFinalidade
	 */
	public String getDsFinalidade() {
		return dsFinalidade;
	}
	
	/**
	 * Set: dsFinalidade.
	 *
	 * @param dsFinalidade the ds finalidade
	 */
	public void setDsFinalidade(String dsFinalidade) {
		this.dsFinalidade = dsFinalidade;
	}
	
	
}
