/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean;

/**
 * Nome: ExcluirVinculoContaSalarioDestinoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirVinculoContaSalarioDestinoEntradaDTO {
	
	/** Atributo cdPessoaJuridicaNegocio. */
	private Long cdPessoaJuridicaNegocio;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdPessoaJuridicaVinculo. */
    private Long cdPessoaJuridicaVinculo;
    
    /** Atributo cdTipoContratoVinculo. */
    private Integer cdTipoContratoVinculo;
    
    /** Atributo nrSequenciaContratoVinculo. */
    private Long nrSequenciaContratoVinculo;
    
    /** Atributo cdTipoVinculoContrato. */
    private Integer cdTipoVinculoContrato;
    
    
	/**
	 * Excluir vinculo conta salario destino entrada dto.
	 */
	public ExcluirVinculoContaSalarioDestinoEntradaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Get: cdPessoaJuridicaNegocio.
	 *
	 * @return cdPessoaJuridicaNegocio
	 */
	public Long getCdPessoaJuridicaNegocio() {
		return cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Set: cdPessoaJuridicaNegocio.
	 *
	 * @param cdPessoaJuridicaNegocio the cd pessoa juridica negocio
	 */
	public void setCdPessoaJuridicaNegocio(Long cdPessoaJuridicaNegocio) {
		this.cdPessoaJuridicaNegocio = cdPessoaJuridicaNegocio;
	}
	
	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public Long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoContratoVinculo.
	 *
	 * @return cdTipoContratoVinculo
	 */
	public Integer getCdTipoContratoVinculo() {
		return cdTipoContratoVinculo;
	}
	
	/**
	 * Set: cdTipoContratoVinculo.
	 *
	 * @param cdTipoContratoVinculo the cd tipo contrato vinculo
	 */
	public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}
	
	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public Integer getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}
	
	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoVinculo.
	 *
	 * @return nrSequenciaContratoVinculo
	 */
	public Long getNrSequenciaContratoVinculo() {
		return nrSequenciaContratoVinculo;
	}
	
	/**
	 * Set: nrSequenciaContratoVinculo.
	 *
	 * @param nrSequenciaContratoVinculo the nr sequencia contrato vinculo
	 */
	public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}
    

}
