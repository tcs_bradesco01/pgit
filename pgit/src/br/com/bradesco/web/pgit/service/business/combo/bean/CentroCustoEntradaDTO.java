/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: CentroCustoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class CentroCustoEntradaDTO {

	/** Atributo cdSistema. */
	private String cdSistema;

	/**
	 * Centro custo entrada dto.
	 */
	public CentroCustoEntradaDTO() {
		super();
	}

	/**
	 * Centro custo entrada dto.
	 *
	 * @param cdSistema the cd sistema
	 */
	public CentroCustoEntradaDTO(String cdSistema) {
		super();
		this.cdSistema = cdSistema;
	}

	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}

	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}
	
}
