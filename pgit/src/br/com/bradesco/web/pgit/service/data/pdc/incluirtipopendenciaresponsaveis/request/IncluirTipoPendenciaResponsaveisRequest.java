/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirtipopendenciaresponsaveis.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirTipoPendenciaResponsaveisRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirTipoPendenciaResponsaveisRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPendenciaPagamentoIntegrado
     */
    private long _cdPendenciaPagamentoIntegrado = 0;

    /**
     * keeps track of state for field: _cdPendenciaPagamentoIntegrad
     */
    private boolean _has_cdPendenciaPagamentoIntegrado;

    /**
     * Field _cdTipoUnidadeOrganizacional
     */
    private int _cdTipoUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeOrganizacional
     */
    private boolean _has_cdTipoUnidadeOrganizacional;

    /**
     * Field _cdPessoaJuridica
     */
    private long _cdPessoaJuridica = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridica
     */
    private boolean _has_cdPessoaJuridica;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _dsPendenciaPagamentoIntegrado
     */
    private java.lang.String _dsPendenciaPagamentoIntegrado;

    /**
     * Field _rsResumoPendenciaPagamento
     */
    private java.lang.String _rsResumoPendenciaPagamento;

    /**
     * Field _cdTipoBaixaPendencia
     */
    private int _cdTipoBaixaPendencia = 0;

    /**
     * keeps track of state for field: _cdTipoBaixaPendencia
     */
    private boolean _has_cdTipoBaixaPendencia;

    /**
     * Field _cdIndicadorResponsavelPagamento
     */
    private int _cdIndicadorResponsavelPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdIndicadorResponsavelPagamento
     */
    private boolean _has_cdIndicadorResponsavelPagamento;

    /**
     * Field _dsEmailRespPgto
     */
    private java.lang.String _dsEmailRespPgto;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirTipoPendenciaResponsaveisRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirtipopendenciaresponsaveis.request.IncluirTipoPendenciaResponsaveisRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorResponsavelPagamento
     * 
     */
    public void deleteCdIndicadorResponsavelPagamento()
    {
        this._has_cdIndicadorResponsavelPagamento= false;
    } //-- void deleteCdIndicadorResponsavelPagamento() 

    /**
     * Method deleteCdPendenciaPagamentoIntegrado
     * 
     */
    public void deleteCdPendenciaPagamentoIntegrado()
    {
        this._has_cdPendenciaPagamentoIntegrado= false;
    } //-- void deleteCdPendenciaPagamentoIntegrado() 

    /**
     * Method deleteCdPessoaJuridica
     * 
     */
    public void deleteCdPessoaJuridica()
    {
        this._has_cdPessoaJuridica= false;
    } //-- void deleteCdPessoaJuridica() 

    /**
     * Method deleteCdTipoBaixaPendencia
     * 
     */
    public void deleteCdTipoBaixaPendencia()
    {
        this._has_cdTipoBaixaPendencia= false;
    } //-- void deleteCdTipoBaixaPendencia() 

    /**
     * Method deleteCdTipoUnidadeOrganizacional
     * 
     */
    public void deleteCdTipoUnidadeOrganizacional()
    {
        this._has_cdTipoUnidadeOrganizacional= false;
    } //-- void deleteCdTipoUnidadeOrganizacional() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Returns the value of field
     * 'cdIndicadorResponsavelPagamento'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorResponsavelPagamento'.
     */
    public int getCdIndicadorResponsavelPagamento()
    {
        return this._cdIndicadorResponsavelPagamento;
    } //-- int getCdIndicadorResponsavelPagamento() 

    /**
     * Returns the value of field 'cdPendenciaPagamentoIntegrado'.
     * 
     * @return long
     * @return the value of field 'cdPendenciaPagamentoIntegrado'.
     */
    public long getCdPendenciaPagamentoIntegrado()
    {
        return this._cdPendenciaPagamentoIntegrado;
    } //-- long getCdPendenciaPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdPessoaJuridica'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridica'.
     */
    public long getCdPessoaJuridica()
    {
        return this._cdPessoaJuridica;
    } //-- long getCdPessoaJuridica() 

    /**
     * Returns the value of field 'cdTipoBaixaPendencia'.
     * 
     * @return int
     * @return the value of field 'cdTipoBaixaPendencia'.
     */
    public int getCdTipoBaixaPendencia()
    {
        return this._cdTipoBaixaPendencia;
    } //-- int getCdTipoBaixaPendencia() 

    /**
     * Returns the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeOrganizacional'.
     */
    public int getCdTipoUnidadeOrganizacional()
    {
        return this._cdTipoUnidadeOrganizacional;
    } //-- int getCdTipoUnidadeOrganizacional() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'dsEmailRespPgto'.
     * 
     * @return String
     * @return the value of field 'dsEmailRespPgto'.
     */
    public java.lang.String getDsEmailRespPgto()
    {
        return this._dsEmailRespPgto;
    } //-- java.lang.String getDsEmailRespPgto() 

    /**
     * Returns the value of field 'dsPendenciaPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'dsPendenciaPagamentoIntegrado'.
     */
    public java.lang.String getDsPendenciaPagamentoIntegrado()
    {
        return this._dsPendenciaPagamentoIntegrado;
    } //-- java.lang.String getDsPendenciaPagamentoIntegrado() 

    /**
     * Returns the value of field 'rsResumoPendenciaPagamento'.
     * 
     * @return String
     * @return the value of field 'rsResumoPendenciaPagamento'.
     */
    public java.lang.String getRsResumoPendenciaPagamento()
    {
        return this._rsResumoPendenciaPagamento;
    } //-- java.lang.String getRsResumoPendenciaPagamento() 

    /**
     * Method hasCdIndicadorResponsavelPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorResponsavelPagamento()
    {
        return this._has_cdIndicadorResponsavelPagamento;
    } //-- boolean hasCdIndicadorResponsavelPagamento() 

    /**
     * Method hasCdPendenciaPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPendenciaPagamentoIntegrado()
    {
        return this._has_cdPendenciaPagamentoIntegrado;
    } //-- boolean hasCdPendenciaPagamentoIntegrado() 

    /**
     * Method hasCdPessoaJuridica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridica()
    {
        return this._has_cdPessoaJuridica;
    } //-- boolean hasCdPessoaJuridica() 

    /**
     * Method hasCdTipoBaixaPendencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoBaixaPendencia()
    {
        return this._has_cdTipoBaixaPendencia;
    } //-- boolean hasCdTipoBaixaPendencia() 

    /**
     * Method hasCdTipoUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeOrganizacional()
    {
        return this._has_cdTipoUnidadeOrganizacional;
    } //-- boolean hasCdTipoUnidadeOrganizacional() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorResponsavelPagamento'.
     * 
     * @param cdIndicadorResponsavelPagamento the value of field
     * 'cdIndicadorResponsavelPagamento'.
     */
    public void setCdIndicadorResponsavelPagamento(int cdIndicadorResponsavelPagamento)
    {
        this._cdIndicadorResponsavelPagamento = cdIndicadorResponsavelPagamento;
        this._has_cdIndicadorResponsavelPagamento = true;
    } //-- void setCdIndicadorResponsavelPagamento(int) 

    /**
     * Sets the value of field 'cdPendenciaPagamentoIntegrado'.
     * 
     * @param cdPendenciaPagamentoIntegrado the value of field
     * 'cdPendenciaPagamentoIntegrado'.
     */
    public void setCdPendenciaPagamentoIntegrado(long cdPendenciaPagamentoIntegrado)
    {
        this._cdPendenciaPagamentoIntegrado = cdPendenciaPagamentoIntegrado;
        this._has_cdPendenciaPagamentoIntegrado = true;
    } //-- void setCdPendenciaPagamentoIntegrado(long) 

    /**
     * Sets the value of field 'cdPessoaJuridica'.
     * 
     * @param cdPessoaJuridica the value of field 'cdPessoaJuridica'
     */
    public void setCdPessoaJuridica(long cdPessoaJuridica)
    {
        this._cdPessoaJuridica = cdPessoaJuridica;
        this._has_cdPessoaJuridica = true;
    } //-- void setCdPessoaJuridica(long) 

    /**
     * Sets the value of field 'cdTipoBaixaPendencia'.
     * 
     * @param cdTipoBaixaPendencia the value of field
     * 'cdTipoBaixaPendencia'.
     */
    public void setCdTipoBaixaPendencia(int cdTipoBaixaPendencia)
    {
        this._cdTipoBaixaPendencia = cdTipoBaixaPendencia;
        this._has_cdTipoBaixaPendencia = true;
    } //-- void setCdTipoBaixaPendencia(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeOrganizacional'.
     * 
     * @param cdTipoUnidadeOrganizacional the value of field
     * 'cdTipoUnidadeOrganizacional'.
     */
    public void setCdTipoUnidadeOrganizacional(int cdTipoUnidadeOrganizacional)
    {
        this._cdTipoUnidadeOrganizacional = cdTipoUnidadeOrganizacional;
        this._has_cdTipoUnidadeOrganizacional = true;
    } //-- void setCdTipoUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'dsEmailRespPgto'.
     * 
     * @param dsEmailRespPgto the value of field 'dsEmailRespPgto'.
     */
    public void setDsEmailRespPgto(java.lang.String dsEmailRespPgto)
    {
        this._dsEmailRespPgto = dsEmailRespPgto;
    } //-- void setDsEmailRespPgto(java.lang.String) 

    /**
     * Sets the value of field 'dsPendenciaPagamentoIntegrado'.
     * 
     * @param dsPendenciaPagamentoIntegrado the value of field
     * 'dsPendenciaPagamentoIntegrado'.
     */
    public void setDsPendenciaPagamentoIntegrado(java.lang.String dsPendenciaPagamentoIntegrado)
    {
        this._dsPendenciaPagamentoIntegrado = dsPendenciaPagamentoIntegrado;
    } //-- void setDsPendenciaPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'rsResumoPendenciaPagamento'.
     * 
     * @param rsResumoPendenciaPagamento the value of field
     * 'rsResumoPendenciaPagamento'.
     */
    public void setRsResumoPendenciaPagamento(java.lang.String rsResumoPendenciaPagamento)
    {
        this._rsResumoPendenciaPagamento = rsResumoPendenciaPagamento;
    } //-- void setRsResumoPendenciaPagamento(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirTipoPendenciaResponsaveisRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirtipopendenciaresponsaveis.request.IncluirTipoPendenciaResponsaveisRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirtipopendenciaresponsaveis.request.IncluirTipoPendenciaResponsaveisRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirtipopendenciaresponsaveis.request.IncluirTipoPendenciaResponsaveisRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirtipopendenciaresponsaveis.request.IncluirTipoPendenciaResponsaveisRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
