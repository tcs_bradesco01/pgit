/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroidentificao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroidentificao.bean;

/**
 * Nome: ConsultarListaFuncionarioBradescoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarListaFuncionarioBradescoSaidaDTO {
	
	/** Atributo cdCriptonimoFuncionarioBradesco. */
	private Long cdCriptonimoFuncionarioBradesco;
    
    /** Atributo dsCriptonimoFuncionarioBradesco. */
    private String dsCriptonimoFuncionarioBradesco;
    
    /** Atributo cdJuncaoPertencente. */
    private Integer cdJuncaoPertencente;
    
    /** Atributo cdSecao. */
    private Integer cdSecao;
    
    /** Atributo cdCargo. */
    private Long cdCargo;
    
    /** Atributo dsCriptonimoCargo. */
    private String dsCriptonimoCargo;
    
    /** Atributo cdCgcCpf. */
    private Long cdCgcCpf;
    
	/**
	 * Consultar lista funcionario bradesco saida dto.
	 */
	public ConsultarListaFuncionarioBradescoSaidaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Get: cdCargo.
	 *
	 * @return cdCargo
	 */
	public Long getCdCargo() {
		return cdCargo;
	}
	
	/**
	 * Set: cdCargo.
	 *
	 * @param cdCargo the cd cargo
	 */
	public void setCdCargo(Long cdCargo) {
		this.cdCargo = cdCargo;
	}
	
	/**
	 * Get: cdCgcCpf.
	 *
	 * @return cdCgcCpf
	 */
	public Long getCdCgcCpf() {
		return cdCgcCpf;
	}
	
	/**
	 * Set: cdCgcCpf.
	 *
	 * @param cdCgcCpf the cd cgc cpf
	 */
	public void setCdCgcCpf(Long cdCgcCpf) {
		this.cdCgcCpf = cdCgcCpf;
	}
	
	/**
	 * Get: cdCriptonimoFuncionarioBradesco.
	 *
	 * @return cdCriptonimoFuncionarioBradesco
	 */
	public Long getCdCriptonimoFuncionarioBradesco() {
		return cdCriptonimoFuncionarioBradesco;
	}
	
	/**
	 * Set: cdCriptonimoFuncionarioBradesco.
	 *
	 * @param cdCriptonimoFuncionarioBradesco the cd criptonimo funcionario bradesco
	 */
	public void setCdCriptonimoFuncionarioBradesco(
			Long cdCriptonimoFuncionarioBradesco) {
		this.cdCriptonimoFuncionarioBradesco = cdCriptonimoFuncionarioBradesco;
	}
	
	/**
	 * Get: cdJuncaoPertencente.
	 *
	 * @return cdJuncaoPertencente
	 */
	public Integer getCdJuncaoPertencente() {
		return cdJuncaoPertencente;
	}
	
	/**
	 * Set: cdJuncaoPertencente.
	 *
	 * @param cdJuncaoPertencente the cd juncao pertencente
	 */
	public void setCdJuncaoPertencente(Integer cdJuncaoPertencente) {
		this.cdJuncaoPertencente = cdJuncaoPertencente;
	}
	
	/**
	 * Get: cdSecao.
	 *
	 * @return cdSecao
	 */
	public Integer getCdSecao() {
		return cdSecao;
	}
	
	/**
	 * Set: cdSecao.
	 *
	 * @param cdSecao the cd secao
	 */
	public void setCdSecao(Integer cdSecao) {
		this.cdSecao = cdSecao;
	}
	
	/**
	 * Get: dsCriptonimoCargo.
	 *
	 * @return dsCriptonimoCargo
	 */
	public String getDsCriptonimoCargo() {
		return dsCriptonimoCargo;
	}
	
	/**
	 * Set: dsCriptonimoCargo.
	 *
	 * @param dsCriptonimoCargo the ds criptonimo cargo
	 */
	public void setDsCriptonimoCargo(String dsCriptonimoCargo) {
		this.dsCriptonimoCargo = dsCriptonimoCargo;
	}
	
	/**
	 * Get: dsCriptonimoFuncionarioBradesco.
	 *
	 * @return dsCriptonimoFuncionarioBradesco
	 */
	public String getDsCriptonimoFuncionarioBradesco() {
		return dsCriptonimoFuncionarioBradesco;
	}
	
	/**
	 * Set: dsCriptonimoFuncionarioBradesco.
	 *
	 * @param dsCriptonimoFuncionarioBradesco the ds criptonimo funcionario bradesco
	 */
	public void setDsCriptonimoFuncionarioBradesco(
			String dsCriptonimoFuncionarioBradesco) {
		this.dsCriptonimoFuncionarioBradesco = dsCriptonimoFuncionarioBradesco;
	}

}
