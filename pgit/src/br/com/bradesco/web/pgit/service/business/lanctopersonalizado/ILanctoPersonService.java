/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.lanctopersonalizado;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.AlterarLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.AlterarLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharHistoricoLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharHistoricoLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ExcluirLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ExcluirLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.HistoricoLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.HistoricoLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.IncluirLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.IncluirLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ListarLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ListarLanctoPersonalizadoSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterLancamentoPersonalizado
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface ILanctoPersonService {

    /**
     * M�todo de exemplo.
     */
    void sampleManterLancamentoPersonalizado();
    
    /**
     * Listar lancto personalizado.
     *
     * @param listarLanctoPersonalizadoEntradaDTO the listar lancto personalizado entrada dto
     * @return the list< listar lancto personalizado saida dt o>
     */
    List<ListarLanctoPersonalizadoSaidaDTO> listarLanctoPersonalizado(ListarLanctoPersonalizadoEntradaDTO listarLanctoPersonalizadoEntradaDTO) ;
    
    /**
     * Incluir lancto personalizado.
     *
     * @param incluirLanctoPersonalizadoEntradaDTO the incluir lancto personalizado entrada dto
     * @return the incluir lancto personalizado saida dto
     */
    IncluirLanctoPersonalizadoSaidaDTO incluirLanctoPersonalizado(IncluirLanctoPersonalizadoEntradaDTO incluirLanctoPersonalizadoEntradaDTO);
    
    /**
     * Detalhar lancto personalizado.
     *
     * @param detalharLanctoPersonalizadoEntradaDTO the detalhar lancto personalizado entrada dto
     * @return the detalhar lancto personalizado saida dto
     */
    DetalharLanctoPersonalizadoSaidaDTO detalharLanctoPersonalizado(DetalharLanctoPersonalizadoEntradaDTO detalharLanctoPersonalizadoEntradaDTO);
    
    /**
     * Excluir lancto personalizado.
     *
     * @param excluirLanctoPersonalizadoEntradaDTO the excluir lancto personalizado entrada dto
     * @return the excluir lancto personalizado saida dto
     */
    ExcluirLanctoPersonalizadoSaidaDTO excluirLanctoPersonalizado(ExcluirLanctoPersonalizadoEntradaDTO excluirLanctoPersonalizadoEntradaDTO);
    
    /**
     * Alterar lancto personalizado.
     *
     * @param alterarLanctoPersonalizadoEntradaDTO the alterar lancto personalizado entrada dto
     * @return the alterar lancto personalizado saida dto
     */
    AlterarLanctoPersonalizadoSaidaDTO alterarLanctoPersonalizado(AlterarLanctoPersonalizadoEntradaDTO alterarLanctoPersonalizadoEntradaDTO);
    
    /**
     * Historico lancto personalizado.
     *
     * @param historicoLanctoPersonalizadoEntradaDTO the historico lancto personalizado entrada dto
     * @return the list< historico lancto personalizado saida dt o>
     */
    List<HistoricoLanctoPersonalizadoSaidaDTO> historicoLanctoPersonalizado(HistoricoLanctoPersonalizadoEntradaDTO historicoLanctoPersonalizadoEntradaDTO) ;
    
    /**
     * Detalhar historico lancto personalizado.
     *
     * @param detalharHistoricoLanctoPersonalizadoEntradaDTO the detalhar historico lancto personalizado entrada dto
     * @return the detalhar historico lancto personalizado saida dto
     */
    DetalharHistoricoLanctoPersonalizadoSaidaDTO detalharHistoricoLanctoPersonalizado(DetalharHistoricoLanctoPersonalizadoEntradaDTO detalharHistoricoLanctoPersonalizadoEntradaDTO);

}

