/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMotivoBloqueioFavorecido
     */
    private int _cdMotivoBloqueioFavorecido = 0;

    /**
     * keeps track of state for field: _cdMotivoBloqueioFavorecido
     */
    private boolean _has_cdMotivoBloqueioFavorecido;

    /**
     * Field _dtMotivoBloqueioFavorecido
     */
    private java.lang.String _dtMotivoBloqueioFavorecido;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMotivoBloqueioFavorecido
     * 
     */
    public void deleteCdMotivoBloqueioFavorecido()
    {
        this._has_cdMotivoBloqueioFavorecido= false;
    } //-- void deleteCdMotivoBloqueioFavorecido() 

    /**
     * Returns the value of field 'cdMotivoBloqueioFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdMotivoBloqueioFavorecido'.
     */
    public int getCdMotivoBloqueioFavorecido()
    {
        return this._cdMotivoBloqueioFavorecido;
    } //-- int getCdMotivoBloqueioFavorecido() 

    /**
     * Returns the value of field 'dtMotivoBloqueioFavorecido'.
     * 
     * @return String
     * @return the value of field 'dtMotivoBloqueioFavorecido'.
     */
    public java.lang.String getDtMotivoBloqueioFavorecido()
    {
        return this._dtMotivoBloqueioFavorecido;
    } //-- java.lang.String getDtMotivoBloqueioFavorecido() 

    /**
     * Method hasCdMotivoBloqueioFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoBloqueioFavorecido()
    {
        return this._has_cdMotivoBloqueioFavorecido;
    } //-- boolean hasCdMotivoBloqueioFavorecido() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMotivoBloqueioFavorecido'.
     * 
     * @param cdMotivoBloqueioFavorecido the value of field
     * 'cdMotivoBloqueioFavorecido'.
     */
    public void setCdMotivoBloqueioFavorecido(int cdMotivoBloqueioFavorecido)
    {
        this._cdMotivoBloqueioFavorecido = cdMotivoBloqueioFavorecido;
        this._has_cdMotivoBloqueioFavorecido = true;
    } //-- void setCdMotivoBloqueioFavorecido(int) 

    /**
     * Sets the value of field 'dtMotivoBloqueioFavorecido'.
     * 
     * @param dtMotivoBloqueioFavorecido the value of field
     * 'dtMotivoBloqueioFavorecido'.
     */
    public void setDtMotivoBloqueioFavorecido(java.lang.String dtMotivoBloqueioFavorecido)
    {
        this._dtMotivoBloqueioFavorecido = dtMotivoBloqueioFavorecido;
    } //-- void setDtMotivoBloqueioFavorecido(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarmotivobloqueiofavorecido.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
