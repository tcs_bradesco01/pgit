/*
 * Nome: ${package_name}
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: ${date}
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 03/05/16.
 */
public class ConsultarServicosCatalogoSaidaDTO {

    /** The cod mensagem. */
    private String codMensagem;

    /** The mensagem. */
    private String mensagem;

    /** The cd produ servico cdps. */
    private Integer cdProduServicoCdps;

    /** The cd prodt relacionado cdps. */
    private Integer cdProdtRelacionadoCdps;

    /** The cd tipo relacionado cdps. */
    private Integer cdTipoRelacionadoCdps;

    /** The cd prodt pgit. */
    private Long cdProdtPgit;

    /** The cd situacao servc. */
    private Integer cdSituacaoServc;

    /**
     * Set cod mensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Get cod mensagem.
     *
     * @return the cod mensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Set mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Set cd produ servico cdps.
     *
     * @param cdProduServicoCdps the cd produ servico cdps
     */
    public void setCdProduServicoCdps(Integer cdProduServicoCdps) {
        this.cdProduServicoCdps = cdProduServicoCdps;
    }

    /**
     * Get cd produ servico cdps.
     *
     * @return the cd produ servico cdps
     */
    public Integer getCdProduServicoCdps() {
        return this.cdProduServicoCdps;
    }

    /**
     * Set cd prodt relacionado cdps.
     *
     * @param cdProdtRelacionadoCdps the cd prodt relacionado cdps
     */
    public void setCdProdtRelacionadoCdps(Integer cdProdtRelacionadoCdps) {
        this.cdProdtRelacionadoCdps = cdProdtRelacionadoCdps;
    }

    /**
     * Get cd prodt relacionado cdps.
     *
     * @return the cd prodt relacionado cdps
     */
    public Integer getCdProdtRelacionadoCdps() {
        return this.cdProdtRelacionadoCdps;
    }

    /**
     * Set cd tipo relacionado cdps.
     *
     * @param cdTipoRelacionadoCdps the cd tipo relacionado cdps
     */
    public void setCdTipoRelacionadoCdps(Integer cdTipoRelacionadoCdps) {
        this.cdTipoRelacionadoCdps = cdTipoRelacionadoCdps;
    }

    /**
     * Get cd tipo relacionado cdps.
     *
     * @return the cd tipo relacionado cdps
     */
    public Integer getCdTipoRelacionadoCdps() {
        return this.cdTipoRelacionadoCdps;
    }

    /**
     * Set cd prodt pgit.
     *
     * @param cdProdtPgit the cd prodt pgit
     */
    public void setCdProdtPgit(Long cdProdtPgit) {
        this.cdProdtPgit = cdProdtPgit;
    }

    /**
     * Get cd prodt pgit.
     *
     * @return the cd prodt pgit
     */
    public Long getCdProdtPgit() {
        return this.cdProdtPgit;
    }

    /**
     * Set cd situacao servc.
     *
     * @param cdSituacaoServc the cd situacao servc
     */
    public void setCdSituacaoServc(Integer cdSituacaoServc) {
        this.cdSituacaoServc = cdSituacaoServc;
    }

    /**
     * Get cd situacao servc.
     *
     * @return the cd situacao servc
     */
    public Integer getCdSituacaoServc() {
        return this.cdSituacaoServc;
    }
}