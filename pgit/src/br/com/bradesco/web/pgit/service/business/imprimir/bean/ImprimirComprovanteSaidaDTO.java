/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimir.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimir.bean;
import java.util.ArrayList;
import java.util.List;

/**
 * Nome: ImprimirComprovanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirComprovanteSaidaDTO{
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdWidtfd. */
	private String cdWidtfd;
	
	/** Atributo quantidadeOcorrencia. */
	private Integer quantidadeOcorrencia;
	
	/** Atributo protocolo. */
	private Long protocolo;
	
	/** Atributo listaOcorrencia. */
	private List<OcorrenciaImprimirComprovanteSaidaDTO> listaOcorrencia = new ArrayList<OcorrenciaImprimirComprovanteSaidaDTO>();

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem(){
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem){
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem(){
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem){
		this.mensagem = mensagem;
	}

	/**
	 * Get: cdWidtfd.
	 *
	 * @return cdWidtfd
	 */
	public String getCdWidtfd(){
		return cdWidtfd;
	}

	/**
	 * Set: cdWidtfd.
	 *
	 * @param cdWidtfd the cd widtfd
	 */
	public void setCdWidtfd(String cdWidtfd){
		this.cdWidtfd = cdWidtfd;
	}

	/**
	 * Get: quantidadeOcorrencia.
	 *
	 * @return quantidadeOcorrencia
	 */
	public Integer getQuantidadeOcorrencia(){
		return quantidadeOcorrencia;
	}

	/**
	 * Set: quantidadeOcorrencia.
	 *
	 * @param quantidadeOcorrencia the quantidade ocorrencia
	 */
	public void setQuantidadeOcorrencia(Integer quantidadeOcorrencia){
		this.quantidadeOcorrencia = quantidadeOcorrencia;
	}

	/**
	 * Get: listaOcorrencia.
	 *
	 * @return listaOcorrencia
	 */
	public List<OcorrenciaImprimirComprovanteSaidaDTO> getListaOcorrencia(){
		return listaOcorrencia;
	}

	/**
	 * Set: listaOcorrencia.
	 *
	 * @param listaOcorrencia the lista ocorrencia
	 */
	public void setListaOcorrencia(List<OcorrenciaImprimirComprovanteSaidaDTO> listaOcorrencia){
		this.listaOcorrencia = listaOcorrencia;
	}

	/**
	 * Get: protocolo.
	 *
	 * @return protocolo
	 */
	public Long getProtocolo() {
		return protocolo;
	}

	/**
	 * Set: protocolo.
	 *
	 * @param protocolo the protocolo
	 */
	public void setProtocolo(Long protocolo) {
		this.protocolo = protocolo;
	}
	
	
}