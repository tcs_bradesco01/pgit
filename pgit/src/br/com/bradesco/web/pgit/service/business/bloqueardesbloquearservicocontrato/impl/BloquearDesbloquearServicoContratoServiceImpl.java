/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.impl;

import java.util.ArrayList;

import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.IBloquearDesbloquearServicoContratoService;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean.BloquearDesbloquearProdServConSaidaDTO;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean.BloquearDesbloquearProdServContEntradaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.BloquearDesbloquearProdutoServContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.response.BloquearDesbloquearProdutoServContratoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: BloquearDesbloquearServicoContrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class BloquearDesbloquearServicoContratoServiceImpl implements IBloquearDesbloquearServicoContratoService {

    /**
     * Construtor.
     */
    public BloquearDesbloquearServicoContratoServiceImpl() {
        // TODO: Implementa��o
    }
    
    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.IBloquearDesbloquearServicoContratoService#bloquearDesbloquearProdServContratos(br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean.BloquearDesbloquearProdServContEntradaDTO)
	 */
	public BloquearDesbloquearProdServConSaidaDTO bloquearDesbloquearProdServContratos (BloquearDesbloquearProdServContEntradaDTO entrada){
		
		BloquearDesbloquearProdutoServContratoRequest request = new BloquearDesbloquearProdutoServContratoRequest();
		BloquearDesbloquearProdutoServContratoResponse response = new BloquearDesbloquearProdutoServContratoResponse();
		BloquearDesbloquearProdServConSaidaDTO saidaDTO = new BloquearDesbloquearProdServConSaidaDTO();

		request.setCdMotivoServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdMotivoServicoRelacionado()));
		request.setCdParametro(PgitUtil.verificaIntegerNulo(entrada.getCdParametro()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaContrato()));
		request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request.setCdSituacaoServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoServicoRelacionado()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias> lista = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias>();
		
		for (int i=0; i< entrada.getListaOcorrencias().size(); i++){
			br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias();		
			ocorrencia.setCdServicoRelacionado(PgitUtil.verificaIntegerNulo(entrada.getListaOcorrencias().get(i).getCdServicoRelacionado()));
			lista.add(ocorrencia);		
		}
		request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias[]) lista.toArray(new br.com.bradesco.web.pgit.service.data.pdc.bloqueardesbloquearprodutoservcontrato.request.Ocorrencias[0]));
		
		response = getFactoryAdapter().getBloquearDesbloquearProdutoServContratoPDCAdapter().invokeProcess(request);
				
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}	
    
    
    
}

