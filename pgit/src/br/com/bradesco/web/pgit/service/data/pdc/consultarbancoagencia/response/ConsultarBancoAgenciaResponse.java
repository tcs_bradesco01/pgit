/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarBancoAgenciaResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarBancoAgenciaResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _dsBanco
     */
    private java.lang.String _dsBanco;

    /**
     * Field _dsAgencia
     */
    private java.lang.String _dsAgencia;

    /**
     * Field _cdEndereco
     */
    private long _cdEndereco = 0;

    /**
     * keeps track of state for field: _cdEndereco
     */
    private boolean _has_cdEndereco;

    /**
     * Field _logradouro
     */
    private java.lang.String _logradouro;

    /**
     * Field _numero
     */
    private int _numero = 0;

    /**
     * keeps track of state for field: _numero
     */
    private boolean _has_numero;

    /**
     * Field _bairro
     */
    private java.lang.String _bairro;

    /**
     * Field _complemento
     */
    private java.lang.String _complemento;

    /**
     * Field _cidade
     */
    private java.lang.String _cidade;

    /**
     * Field _estado
     */
    private java.lang.String _estado;

    /**
     * Field _pais
     */
    private java.lang.String _pais;

    /**
     * Field _contato
     */
    private java.lang.String _contato;

    /**
     * Field _email
     */
    private java.lang.String _email;

    /**
     * Field _cep
     */
    private int _cep = 0;

    /**
     * keeps track of state for field: _cep
     */
    private boolean _has_cep;

    /**
     * Field _complementoCep
     */
    private int _complementoCep = 0;

    /**
     * keeps track of state for field: _complementoCep
     */
    private boolean _has_complementoCep;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarBancoAgenciaResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.response.ConsultarBancoAgenciaResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdEndereco
     * 
     */
    public void deleteCdEndereco()
    {
        this._has_cdEndereco= false;
    } //-- void deleteCdEndereco() 

    /**
     * Method deleteCep
     * 
     */
    public void deleteCep()
    {
        this._has_cep= false;
    } //-- void deleteCep() 

    /**
     * Method deleteComplementoCep
     * 
     */
    public void deleteComplementoCep()
    {
        this._has_complementoCep= false;
    } //-- void deleteComplementoCep() 

    /**
     * Method deleteNumero
     * 
     */
    public void deleteNumero()
    {
        this._has_numero= false;
    } //-- void deleteNumero() 

    /**
     * Returns the value of field 'bairro'.
     * 
     * @return String
     * @return the value of field 'bairro'.
     */
    public java.lang.String getBairro()
    {
        return this._bairro;
    } //-- java.lang.String getBairro() 

    /**
     * Returns the value of field 'cdEndereco'.
     * 
     * @return long
     * @return the value of field 'cdEndereco'.
     */
    public long getCdEndereco()
    {
        return this._cdEndereco;
    } //-- long getCdEndereco() 

    /**
     * Returns the value of field 'cep'.
     * 
     * @return int
     * @return the value of field 'cep'.
     */
    public int getCep()
    {
        return this._cep;
    } //-- int getCep() 

    /**
     * Returns the value of field 'cidade'.
     * 
     * @return String
     * @return the value of field 'cidade'.
     */
    public java.lang.String getCidade()
    {
        return this._cidade;
    } //-- java.lang.String getCidade() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'complemento'.
     * 
     * @return String
     * @return the value of field 'complemento'.
     */
    public java.lang.String getComplemento()
    {
        return this._complemento;
    } //-- java.lang.String getComplemento() 

    /**
     * Returns the value of field 'complementoCep'.
     * 
     * @return int
     * @return the value of field 'complementoCep'.
     */
    public int getComplementoCep()
    {
        return this._complementoCep;
    } //-- int getComplementoCep() 

    /**
     * Returns the value of field 'contato'.
     * 
     * @return String
     * @return the value of field 'contato'.
     */
    public java.lang.String getContato()
    {
        return this._contato;
    } //-- java.lang.String getContato() 

    /**
     * Returns the value of field 'dsAgencia'.
     * 
     * @return String
     * @return the value of field 'dsAgencia'.
     */
    public java.lang.String getDsAgencia()
    {
        return this._dsAgencia;
    } //-- java.lang.String getDsAgencia() 

    /**
     * Returns the value of field 'dsBanco'.
     * 
     * @return String
     * @return the value of field 'dsBanco'.
     */
    public java.lang.String getDsBanco()
    {
        return this._dsBanco;
    } //-- java.lang.String getDsBanco() 

    /**
     * Returns the value of field 'email'.
     * 
     * @return String
     * @return the value of field 'email'.
     */
    public java.lang.String getEmail()
    {
        return this._email;
    } //-- java.lang.String getEmail() 

    /**
     * Returns the value of field 'estado'.
     * 
     * @return String
     * @return the value of field 'estado'.
     */
    public java.lang.String getEstado()
    {
        return this._estado;
    } //-- java.lang.String getEstado() 

    /**
     * Returns the value of field 'logradouro'.
     * 
     * @return String
     * @return the value of field 'logradouro'.
     */
    public java.lang.String getLogradouro()
    {
        return this._logradouro;
    } //-- java.lang.String getLogradouro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'numero'.
     * 
     * @return int
     * @return the value of field 'numero'.
     */
    public int getNumero()
    {
        return this._numero;
    } //-- int getNumero() 

    /**
     * Returns the value of field 'pais'.
     * 
     * @return String
     * @return the value of field 'pais'.
     */
    public java.lang.String getPais()
    {
        return this._pais;
    } //-- java.lang.String getPais() 

    /**
     * Method hasCdEndereco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEndereco()
    {
        return this._has_cdEndereco;
    } //-- boolean hasCdEndereco() 

    /**
     * Method hasCep
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCep()
    {
        return this._has_cep;
    } //-- boolean hasCep() 

    /**
     * Method hasComplementoCep
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasComplementoCep()
    {
        return this._has_complementoCep;
    } //-- boolean hasComplementoCep() 

    /**
     * Method hasNumero
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumero()
    {
        return this._has_numero;
    } //-- boolean hasNumero() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'bairro'.
     * 
     * @param bairro the value of field 'bairro'.
     */
    public void setBairro(java.lang.String bairro)
    {
        this._bairro = bairro;
    } //-- void setBairro(java.lang.String) 

    /**
     * Sets the value of field 'cdEndereco'.
     * 
     * @param cdEndereco the value of field 'cdEndereco'.
     */
    public void setCdEndereco(long cdEndereco)
    {
        this._cdEndereco = cdEndereco;
        this._has_cdEndereco = true;
    } //-- void setCdEndereco(long) 

    /**
     * Sets the value of field 'cep'.
     * 
     * @param cep the value of field 'cep'.
     */
    public void setCep(int cep)
    {
        this._cep = cep;
        this._has_cep = true;
    } //-- void setCep(int) 

    /**
     * Sets the value of field 'cidade'.
     * 
     * @param cidade the value of field 'cidade'.
     */
    public void setCidade(java.lang.String cidade)
    {
        this._cidade = cidade;
    } //-- void setCidade(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'complemento'.
     * 
     * @param complemento the value of field 'complemento'.
     */
    public void setComplemento(java.lang.String complemento)
    {
        this._complemento = complemento;
    } //-- void setComplemento(java.lang.String) 

    /**
     * Sets the value of field 'complementoCep'.
     * 
     * @param complementoCep the value of field 'complementoCep'.
     */
    public void setComplementoCep(int complementoCep)
    {
        this._complementoCep = complementoCep;
        this._has_complementoCep = true;
    } //-- void setComplementoCep(int) 

    /**
     * Sets the value of field 'contato'.
     * 
     * @param contato the value of field 'contato'.
     */
    public void setContato(java.lang.String contato)
    {
        this._contato = contato;
    } //-- void setContato(java.lang.String) 

    /**
     * Sets the value of field 'dsAgencia'.
     * 
     * @param dsAgencia the value of field 'dsAgencia'.
     */
    public void setDsAgencia(java.lang.String dsAgencia)
    {
        this._dsAgencia = dsAgencia;
    } //-- void setDsAgencia(java.lang.String) 

    /**
     * Sets the value of field 'dsBanco'.
     * 
     * @param dsBanco the value of field 'dsBanco'.
     */
    public void setDsBanco(java.lang.String dsBanco)
    {
        this._dsBanco = dsBanco;
    } //-- void setDsBanco(java.lang.String) 

    /**
     * Sets the value of field 'email'.
     * 
     * @param email the value of field 'email'.
     */
    public void setEmail(java.lang.String email)
    {
        this._email = email;
    } //-- void setEmail(java.lang.String) 

    /**
     * Sets the value of field 'estado'.
     * 
     * @param estado the value of field 'estado'.
     */
    public void setEstado(java.lang.String estado)
    {
        this._estado = estado;
    } //-- void setEstado(java.lang.String) 

    /**
     * Sets the value of field 'logradouro'.
     * 
     * @param logradouro the value of field 'logradouro'.
     */
    public void setLogradouro(java.lang.String logradouro)
    {
        this._logradouro = logradouro;
    } //-- void setLogradouro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'numero'.
     * 
     * @param numero the value of field 'numero'.
     */
    public void setNumero(int numero)
    {
        this._numero = numero;
        this._has_numero = true;
    } //-- void setNumero(int) 

    /**
     * Sets the value of field 'pais'.
     * 
     * @param pais the value of field 'pais'.
     */
    public void setPais(java.lang.String pais)
    {
        this._pais = pais;
    } //-- void setPais(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarBancoAgenciaResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.response.ConsultarBancoAgenciaResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.response.ConsultarBancoAgenciaResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.response.ConsultarBancoAgenciaResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarbancoagencia.response.ConsultarBancoAgenciaResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
