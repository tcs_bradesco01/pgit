/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarOperacaoContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarOperacaoContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencia
     */
    private int _numeroOcorrencia = 0;

    /**
     * keeps track of state for field: _numeroOcorrencia
     */
    private boolean _has_numeroOcorrencia;

    /**
     * Field _cdClub
     */
    private long _cdClub = 0;

    /**
     * keeps track of state for field: _cdClub
     */
    private boolean _has_cdClub;

    /**
     * Field _cdTipoContrato
     */
    private int _cdTipoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoContrato
     */
    private boolean _has_cdTipoContrato;

    /**
     * Field _nrSequenciaContrato
     */
    private long _nrSequenciaContrato = 0;

    /**
     * keeps track of state for field: _nrSequenciaContrato
     */
    private boolean _has_nrSequenciaContrato;

    /**
     * Field _cdProdutoServico
     */
    private int _cdProdutoServico = 0;

    /**
     * keeps track of state for field: _cdProdutoServico
     */
    private boolean _has_cdProdutoServico;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarOperacaoContratoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.request.ListarOperacaoContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClub
     * 
     */
    public void deleteCdClub()
    {
        this._has_cdClub= false;
    } //-- void deleteCdClub() 

    /**
     * Method deleteCdProdutoServico
     * 
     */
    public void deleteCdProdutoServico()
    {
        this._has_cdProdutoServico= false;
    } //-- void deleteCdProdutoServico() 

    /**
     * Method deleteCdTipoContrato
     * 
     */
    public void deleteCdTipoContrato()
    {
        this._has_cdTipoContrato= false;
    } //-- void deleteCdTipoContrato() 

    /**
     * Method deleteNrSequenciaContrato
     * 
     */
    public void deleteNrSequenciaContrato()
    {
        this._has_nrSequenciaContrato= false;
    } //-- void deleteNrSequenciaContrato() 

    /**
     * Method deleteNumeroOcorrencia
     * 
     */
    public void deleteNumeroOcorrencia()
    {
        this._has_numeroOcorrencia= false;
    } //-- void deleteNumeroOcorrencia() 

    /**
     * Returns the value of field 'cdClub'.
     * 
     * @return long
     * @return the value of field 'cdClub'.
     */
    public long getCdClub()
    {
        return this._cdClub;
    } //-- long getCdClub() 

    /**
     * Returns the value of field 'cdProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServico'.
     */
    public int getCdProdutoServico()
    {
        return this._cdProdutoServico;
    } //-- int getCdProdutoServico() 

    /**
     * Returns the value of field 'cdTipoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoContrato'.
     */
    public int getCdTipoContrato()
    {
        return this._cdTipoContrato;
    } //-- int getCdTipoContrato() 

    /**
     * Returns the value of field 'nrSequenciaContrato'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContrato'.
     */
    public long getNrSequenciaContrato()
    {
        return this._nrSequenciaContrato;
    } //-- long getNrSequenciaContrato() 

    /**
     * Returns the value of field 'numeroOcorrencia'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencia'.
     */
    public int getNumeroOcorrencia()
    {
        return this._numeroOcorrencia;
    } //-- int getNumeroOcorrencia() 

    /**
     * Method hasCdClub
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClub()
    {
        return this._has_cdClub;
    } //-- boolean hasCdClub() 

    /**
     * Method hasCdProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServico()
    {
        return this._has_cdProdutoServico;
    } //-- boolean hasCdProdutoServico() 

    /**
     * Method hasCdTipoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContrato()
    {
        return this._has_cdTipoContrato;
    } //-- boolean hasCdTipoContrato() 

    /**
     * Method hasNrSequenciaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContrato()
    {
        return this._has_nrSequenciaContrato;
    } //-- boolean hasNrSequenciaContrato() 

    /**
     * Method hasNumeroOcorrencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencia()
    {
        return this._has_numeroOcorrencia;
    } //-- boolean hasNumeroOcorrencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClub'.
     * 
     * @param cdClub the value of field 'cdClub'.
     */
    public void setCdClub(long cdClub)
    {
        this._cdClub = cdClub;
        this._has_cdClub = true;
    } //-- void setCdClub(long) 

    /**
     * Sets the value of field 'cdProdutoServico'.
     * 
     * @param cdProdutoServico the value of field 'cdProdutoServico'
     */
    public void setCdProdutoServico(int cdProdutoServico)
    {
        this._cdProdutoServico = cdProdutoServico;
        this._has_cdProdutoServico = true;
    } //-- void setCdProdutoServico(int) 

    /**
     * Sets the value of field 'cdTipoContrato'.
     * 
     * @param cdTipoContrato the value of field 'cdTipoContrato'.
     */
    public void setCdTipoContrato(int cdTipoContrato)
    {
        this._cdTipoContrato = cdTipoContrato;
        this._has_cdTipoContrato = true;
    } //-- void setCdTipoContrato(int) 

    /**
     * Sets the value of field 'nrSequenciaContrato'.
     * 
     * @param nrSequenciaContrato the value of field
     * 'nrSequenciaContrato'.
     */
    public void setNrSequenciaContrato(long nrSequenciaContrato)
    {
        this._nrSequenciaContrato = nrSequenciaContrato;
        this._has_nrSequenciaContrato = true;
    } //-- void setNrSequenciaContrato(long) 

    /**
     * Sets the value of field 'numeroOcorrencia'.
     * 
     * @param numeroOcorrencia the value of field 'numeroOcorrencia'
     */
    public void setNumeroOcorrencia(int numeroOcorrencia)
    {
        this._numeroOcorrencia = numeroOcorrencia;
        this._has_numeroOcorrencia = true;
    } //-- void setNumeroOcorrencia(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarOperacaoContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.request.ListarOperacaoContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.request.ListarOperacaoContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.request.ListarOperacaoContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaocontrato.request.ListarOperacaoContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
