/*
 * Nome: br.com.bradesco.web.pgit.service.report.jasper.contrato.domain
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.jasper.contrato;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import br.com.bradesco.web.pgit.service.report.base.exception.ReportException;
import br.com.bradesco.web.pgit.service.report.jasper.data.impl.JasperGenericParameterImpl;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.ImpressaoContrato;
import br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data.ContratoPrestacaoServico;

/**
 * Nome: ImpressaoContratoJasper
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 * 
 * @see ImpressaoContrato
 */
public class ImpressaoContratoJasper implements ImpressaoContrato {

    /**
     * Instancia um novo ImpressaoContratoJasper
     * 
     * @see
     */
    public ImpressaoContratoJasper() {
        super();
    }

    /**
     * (non-Javadoc)
     * 
     * @param contrato
     *            the contrato
     * @param caminhoWeb
     *            the caminhoWeb
     * @param outputStream
     *            the outputStream
     * @exception
     * 
     * @see br.com.bradesco.web.pgit.service.report.contrato.ImpressaoContrato#imprimirContratoPrestacaoServico
     *      (br.com.bradesco.web.pgit.service.report.contrato.data.ContratoPrestacaoServico, java.lang.String,
     *      java.io.OutputStream)
     */
    public void imprimirContratoPrestacaoServico(ContratoPrestacaoServico contrato, String caminhoWeb,
        OutputStream outputStream) {
        String caminho = caminhoWeb.replaceAll("\\\\", "/");
        String caminhoRelatorio = caminho + "/relatorios/contratoPrestacaoServico/contratoPrestacaoServico.jasper";

        Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("IMAGE_PATH", caminho + "/images/report/logo_brad.png");
        parametros.put("SUBREPORT_DIR", caminho + "/relatorios/contratoPrestacaoServico/");

        parametros.put("contrato", new JasperGenericParameterImpl(contrato));

        try {
            JasperPrint print = JasperFillManager.fillReport(caminhoRelatorio, parametros, new JREmptyDataSource());
            JasperExportManager.exportReportToPdfStream(print, outputStream);
        } catch (JRException e) {
            throw new ReportException(e.getMessage(), e);
        }
    }

}
