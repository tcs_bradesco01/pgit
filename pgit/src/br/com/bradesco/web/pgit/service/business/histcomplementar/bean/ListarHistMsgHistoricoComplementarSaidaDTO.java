/*
 * Nome: br.com.bradesco.web.pgit.service.business.histcomplementar.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.histcomplementar.bean;

/**
 * Nome: ListarHistMsgHistoricoComplementarSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarHistMsgHistoricoComplementarSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo codigoHistoricoComplementar. */
	private int codigoHistoricoComplementar;
	
	/** Atributo dataManutencao. */
	private String dataManutencao;
	
	/** Atributo dataManutencaoDesc. */
	private String dataManutencaoDesc;
	
	/** Atributo responsavel. */
	private String responsavel;
	
	/** Atributo tipoManutencao. */
	private int tipoManutencao;
	
	/**
	 * Get: codigoHistoricoComplementar.
	 *
	 * @return codigoHistoricoComplementar
	 */
	public int getCodigoHistoricoComplementar() {
		return codigoHistoricoComplementar;
	}
	
	/**
	 * Set: codigoHistoricoComplementar.
	 *
	 * @param codigoHistoricoComplementar the codigo historico complementar
	 */
	public void setCodigoHistoricoComplementar(int codigoHistoricoComplementar) {
		this.codigoHistoricoComplementar = codigoHistoricoComplementar;
	}
	
	/**
	 * Get: dataManutencao.
	 *
	 * @return dataManutencao
	 */
	public String getDataManutencao() {
		return dataManutencao;
	}
	
	/**
	 * Set: dataManutencao.
	 *
	 * @param dataManutencao the data manutencao
	 */
	public void setDataManutencao(String dataManutencao) {
		this.dataManutencao = dataManutencao;
	}
	
	/**
	 * Get: responsavel.
	 *
	 * @return responsavel
	 */
	public String getResponsavel() {
		return responsavel;
	}
	
	/**
	 * Set: responsavel.
	 *
	 * @param responsavel the responsavel
	 */
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	/**
	 * Get: tipoManutencao.
	 *
	 * @return tipoManutencao
	 */
	public int getTipoManutencao() {
		return tipoManutencao;
	}
	
	/**
	 * Set: tipoManutencao.
	 *
	 * @param tipoManutencao the tipo manutencao
	 */
	public void setTipoManutencao(int tipoManutencao) {
		this.tipoManutencao = tipoManutencao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: dataManutencaoDesc.
	 *
	 * @return dataManutencaoDesc
	 */
	public String getDataManutencaoDesc() {
		return dataManutencaoDesc;
	}
	
	/**
	 * Set: dataManutencaoDesc.
	 *
	 * @param dataManutencaoDesc the data manutencao desc
	 */
	public void setDataManutencaoDesc(String dataManutencaoDesc) {
		this.dataManutencaoDesc = dataManutencaoDesc;
	}
	
	

}


