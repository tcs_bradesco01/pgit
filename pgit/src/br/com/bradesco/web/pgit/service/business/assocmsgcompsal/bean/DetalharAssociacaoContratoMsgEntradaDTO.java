/*
 * Nome: br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean;

/**
 * Nome: DetalharAssociacaoContratoMsgEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharAssociacaoContratoMsgEntradaDTO {
	
	/** Atributo tipoMensagem. */
	private int tipoMensagem;
	
	/** Atributo contratoPssoaJuridContr. */
	private long contratoPssoaJuridContr;
	
	/** Atributo contratoTpoContrNegoc. */
	private int contratoTpoContrNegoc;
	
	/** Atributo contratoSeqContrNegoc. */
	private long contratoSeqContrNegoc;

	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public int getTipoMensagem() {
		return tipoMensagem;
	}

	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(int tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

	/**
	 * Get: contratoPssoaJuridContr.
	 *
	 * @return contratoPssoaJuridContr
	 */
	public long getContratoPssoaJuridContr() {
		return contratoPssoaJuridContr;
	}

	/**
	 * Set: contratoPssoaJuridContr.
	 *
	 * @param contratoPssoaJuridContr the contrato pssoa jurid contr
	 */
	public void setContratoPssoaJuridContr(long contratoPssoaJuridContr) {
		this.contratoPssoaJuridContr = contratoPssoaJuridContr;
	}

	/**
	 * Get: contratoSeqContrNegoc.
	 *
	 * @return contratoSeqContrNegoc
	 */
	public long getContratoSeqContrNegoc() {
		return contratoSeqContrNegoc;
	}

	/**
	 * Set: contratoSeqContrNegoc.
	 *
	 * @param contratoSeqContrNegoc the contrato seq contr negoc
	 */
	public void setContratoSeqContrNegoc(long contratoSeqContrNegoc) {
		this.contratoSeqContrNegoc = contratoSeqContrNegoc;
	}

	/**
	 * Get: contratoTpoContrNegoc.
	 *
	 * @return contratoTpoContrNegoc
	 */
	public int getContratoTpoContrNegoc() {
		return contratoTpoContrNegoc;
	}

	/**
	 * Set: contratoTpoContrNegoc.
	 *
	 * @param contratoTpoContrNegoc the contrato tpo contr negoc
	 */
	public void setContratoTpoContrNegoc(int contratoTpoContrNegoc) {
		this.contratoTpoContrNegoc = contratoTpoContrNegoc;
	}
	
}


