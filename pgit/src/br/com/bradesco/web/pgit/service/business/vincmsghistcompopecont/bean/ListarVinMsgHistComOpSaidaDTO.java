/*
 * Nome: br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean;

/**
 * Nome: ListarVinMsgHistComOpSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarVinMsgHistComOpSaidaDTO {
	
	/** Atributo cdMensagemLinhaExtrato. */
	private int cdMensagemLinhaExtrato; //C�digo Hist�rico Complementar
	
	/** Atributo dsMensagemLinhaDebito. */
	private String dsMensagemLinhaDebito; //Mensagem Lan�amento D�bito
	
	/** Atributo dsMensagemLinhaCredito. */
	private String dsMensagemLinhaCredito; //Mensagem Lan�amento Cr�dito
	
	/** Atributo cdProdutoServicoOperacao. */
	private int cdProdutoServicoOperacao; //Tipo de servi�o
	
	/** Atributo dsProdutoServicoOperacao. */
	private String dsProdutoServicoOperacao; //Tipo de servi�o
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private int cdProdutoOperacaoRelacionado; //Modalidade Servi�o
	
	/** Atributo dsProdutoOperacaoRelacionado. */
	private String dsProdutoOperacaoRelacionado; //Modalidade Servi�o
	
	/** Atributo cdRelacionamentoProduto. */
	private int cdRelacionamentoProduto;//Tipo Relacionamento 
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo dsMensagem. */
	private String dsMensagem;
	
	/**
	 * Get: cdMensagemLinhaExtrato.
	 *
	 * @return cdMensagemLinhaExtrato
	 */
	public int getCdMensagemLinhaExtrato() {
		return cdMensagemLinhaExtrato;
	}
	
	/**
	 * Set: cdMensagemLinhaExtrato.
	 *
	 * @param cdMensagemLinhaExtrato the cd mensagem linha extrato
	 */
	public void setCdMensagemLinhaExtrato(int cdMensagemLinhaExtrato) {
		this.cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public int getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public int getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public int getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(int cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}
	
	/**
	 * Get: dsMensagemLinhaCredito.
	 *
	 * @return dsMensagemLinhaCredito
	 */
	public String getDsMensagemLinhaCredito() {
		return dsMensagemLinhaCredito;
	}
	
	/**
	 * Set: dsMensagemLinhaCredito.
	 *
	 * @param dsMensagemLinhaCredito the ds mensagem linha credito
	 */
	public void setDsMensagemLinhaCredito(String dsMensagemLinhaCredito) {
		this.dsMensagemLinhaCredito = dsMensagemLinhaCredito;
	}
	
	/**
	 * Get: dsMensagemLinhaDebito.
	 *
	 * @return dsMensagemLinhaDebito
	 */
	public String getDsMensagemLinhaDebito() {
		return dsMensagemLinhaDebito;
	}
	
	/**
	 * Set: dsMensagemLinhaDebito.
	 *
	 * @param dsMensagemLinhaDebito the ds mensagem linha debito
	 */
	public void setDsMensagemLinhaDebito(String dsMensagemLinhaDebito) {
		this.dsMensagemLinhaDebito = dsMensagemLinhaDebito;
	}
	
	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	
	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsMensagem.
	 *
	 * @return dsMensagem
	 */
	public String getDsMensagem() {
		return dsMensagem;
	}
	
	/**
	 * Set: dsMensagem.
	 *
	 * @param dsMensagem the ds mensagem
	 */
	public void setDsMensagem(String dsMensagem) {
		this.dsMensagem = dsMensagem;
	}
	  
	
	
}
