package br.com.bradesco.web.pgit.service.business.combo.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 11/12/15.
 */
public class ListarServicoRelacionadoPgitCdpsEntradaDTO {

    /** The max ocorrencias. */
    private Integer maxOcorrencias;

    /** The cd produto servico operacao. */
    private Integer cdProdutoServicoOperacao;

    /** The cd relacionamento produto. */
    private Integer cdRelacionamentoProduto;

    /** The cd produto operacao relacionado. */
    private Integer cdProdutoOperacaoRelacionado;

    /**
     * Sets the max ocorrencias.
     *
     * @param maxOcorrencias the max ocorrencias
     */
    public void setMaxOcorrencias(Integer maxOcorrencias) {
        this.maxOcorrencias = maxOcorrencias;
    }

    /**
     * Gets the max ocorrencias.
     *
     * @return the max ocorrencias
     */
    public Integer getMaxOcorrencias() {
        return this.maxOcorrencias;
    }

    /**
     * Sets the cd produto servico operacao.
     *
     * @param cdProdutoServicoOperacao the cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Gets the cd produto servico operacao.
     *
     * @return the cd produto servico operacao
     */
    public Integer getCdProdutoServicoOperacao() {
        return this.cdProdutoServicoOperacao;
    }

    /**
     * Sets the cd relacionamento produto.
     *
     * @param cdRelacionamentoProduto the cd relacionamento produto
     */
    public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
        this.cdRelacionamentoProduto = cdRelacionamentoProduto;
    }

    /**
     * Gets the cd relacionamento produto.
     *
     * @return the cd relacionamento produto
     */
    public Integer getCdRelacionamentoProduto() {
        return this.cdRelacionamentoProduto;
    }

    /**
     * Sets the cd produto operacao relacionado.
     *
     * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
     */
    public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
        this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
    }

    /**
     * Gets the cd produto operacao relacionado.
     *
     * @return the cd produto operacao relacionado
     */
    public Integer getCdProdutoOperacaoRelacionado() {
        return this.cdProdutoOperacaoRelacionado;
    }
}