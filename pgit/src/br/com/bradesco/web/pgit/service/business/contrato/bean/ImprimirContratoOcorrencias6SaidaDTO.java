/*
 * Nome: br.com.bradesco.web.pgit.service.business.contrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 19/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.contrato.bean;


/**
 * Nome: ImprimirContratoOcorrencias6SaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoOcorrencias6SaidaDTO {

    /** The ds produto salario fone facil. */
    private String dsProdutoSalarioFoneFacil = null;

    /** The ds servico salario fone facil. */
    private String dsServicoSalarioFoneFacil = null;

    /**
     * Instancia um novo ImprimirContratoOcorrencias6SaidaDTO.
     * 
     * @see
     */
    public ImprimirContratoOcorrencias6SaidaDTO() {
        super();
    }

    /**
     * Sets the ds produto salario fone facil.
     *
     * @param dsProdutoSalarioFoneFacil the ds produto salario fone facil
     */
    public void setDsProdutoSalarioFoneFacil(String dsProdutoSalarioFoneFacil) {
        this.dsProdutoSalarioFoneFacil = dsProdutoSalarioFoneFacil;
    }

    /**
     * Gets the ds produto salario fone facil.
     *
     * @return the ds produto salario fone facil
     */
    public String getDsProdutoSalarioFoneFacil() {
        return this.dsProdutoSalarioFoneFacil;
    }

    /**
     * Sets the ds servico salario fone facil.
     *
     * @param dsServicoSalarioFoneFacil the ds servico salario fone facil
     */
    public void setDsServicoSalarioFoneFacil(String dsServicoSalarioFoneFacil) {
        this.dsServicoSalarioFoneFacil = dsServicoSalarioFoneFacil;
    }

    /**
     * Gets the ds servico salario fone facil.
     *
     * @return the ds servico salario fone facil
     */
    public String getDsServicoSalarioFoneFacil() {
        return this.dsServicoSalarioFoneFacil;
    }
}