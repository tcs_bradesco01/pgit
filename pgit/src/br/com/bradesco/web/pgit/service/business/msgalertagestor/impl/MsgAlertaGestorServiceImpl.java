/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.msgalertagestor.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.msgalertagestor.IMsgAlertaGestorConstants;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.IMsgAlertaGestorService;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.AlterarMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.AlterarMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.DetalharMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.DetalharMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ExcluirMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ExcluirMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.IncluirMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.IncluirMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ListarMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ListarMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsgalertagestor.request.AlterarMsgAlertaGestorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarmsgalertagestor.response.AlterarMsgAlertaGestorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsgalertagestor.request.ConsultarDetalheMsgAlertaGestorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharmsgalertagestor.response.ConsultarDetalheMsgAlertaGestorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsgalertagestor.request.ExlcuirMsgAlertaGestorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirmsgalertagestor.response.ExlcuirMsgAlertaGestorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsgalertagestor.request.IncluirMsgAlertaGestorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirmsgalertagestor.response.IncluirMsgAlertaGestorResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsgalertagestor.request.ListarMsgAlertaGestorRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarmsgalertagestor.response.ListarMsgAlertaGestorResponse;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterMensagemAlertaGestor
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class MsgAlertaGestorServiceImpl implements IMsgAlertaGestorService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.msgalertagestor.IMsgAlertaGestorService#listarMensagemAlertaGestor(br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ListarMsgAlertaGestorEntradaDTO)
	 */
	public List<ListarMsgAlertaGestorSaidaDTO> listarMensagemAlertaGestor(ListarMsgAlertaGestorEntradaDTO msgAlertaGestorEntradaDTO) {
		
		List<ListarMsgAlertaGestorSaidaDTO> listaMsgAlertaGestor = new ArrayList<ListarMsgAlertaGestorSaidaDTO>();
		ListarMsgAlertaGestorRequest listarMsgAlertaGestorRequest = new ListarMsgAlertaGestorRequest();
		ListarMsgAlertaGestorResponse listarMsgAlertaGestorResponse = new ListarMsgAlertaGestorResponse();
		
		listarMsgAlertaGestorRequest.setCdCistProcs(msgAlertaGestorEntradaDTO.getCentroCusto()); //CCIST-PROCS
		listarMsgAlertaGestorRequest.setCdProcsSistema(msgAlertaGestorEntradaDTO.getTipoProcesso()); //CPROCS-SIST
		listarMsgAlertaGestorRequest.setNrSeqMensagemAlerta(0); //NSEQ-MSGEM-ALERT
		listarMsgAlertaGestorRequest.setCdPessoa(Long.valueOf(msgAlertaGestorEntradaDTO.getCodPessoa())); //CPSSOA-FIS		
		listarMsgAlertaGestorRequest.setCdCist(msgAlertaGestorEntradaDTO.getPrefixo()); //CCIS
		listarMsgAlertaGestorRequest.setNrEventoMensagemNegocio(msgAlertaGestorEntradaDTO.getTipoMensagem());
		listarMsgAlertaGestorRequest.setCdRecGedorMensagem(msgAlertaGestorEntradaDTO.getRecurso());
		listarMsgAlertaGestorRequest.setCdIdiomaTextoMensagem(msgAlertaGestorEntradaDTO.getIdioma());		
		listarMsgAlertaGestorRequest.setQtConsultas(IMsgAlertaGestorConstants.QTDE_CONSULTAS_LISTAR);
		
		
			
		listarMsgAlertaGestorResponse = getFactoryAdapter().getListarMsgAlertaGestorPDCAdapter().invokeProcess(listarMsgAlertaGestorRequest);

		ListarMsgAlertaGestorSaidaDTO listarMsgAlertaGestorSaidaDTO;
		for (int i=0; i<listarMsgAlertaGestorResponse.getOcorrenciasCount();i++){
			
			listarMsgAlertaGestorSaidaDTO = new ListarMsgAlertaGestorSaidaDTO();
			listarMsgAlertaGestorSaidaDTO.setCodMensagem(listarMsgAlertaGestorResponse.getCodMensagem());
			listarMsgAlertaGestorSaidaDTO.setMensagem(listarMsgAlertaGestorResponse.getMensagem());
			listarMsgAlertaGestorSaidaDTO.setCentroCusto(listarMsgAlertaGestorResponse.getOcorrencias(i).getCdCistProcs());
			listarMsgAlertaGestorSaidaDTO.setTipoProcesso(listarMsgAlertaGestorResponse.getOcorrencias(i).getCdTipoProcsSistema());
			listarMsgAlertaGestorSaidaDTO.setDsTipoProcesso(listarMsgAlertaGestorResponse.getOcorrencias(i).getDsTipoProcsSistema());
			listarMsgAlertaGestorSaidaDTO.setCdProcsSistema(listarMsgAlertaGestorResponse.getOcorrencias(i).getCdProcsSistema());
			listarMsgAlertaGestorSaidaDTO.setNrSeqMensagemAlerta(listarMsgAlertaGestorResponse.getOcorrencias(i).getNrSeqMensagemAlerta());
			listarMsgAlertaGestorSaidaDTO.setCdPessoa(listarMsgAlertaGestorResponse.getOcorrencias(i).getCdPessoa());
			listarMsgAlertaGestorSaidaDTO.setDsPessoa(listarMsgAlertaGestorResponse.getOcorrencias(i).getDsPessoa());
			listarMsgAlertaGestorSaidaDTO.setCdCist(listarMsgAlertaGestorResponse.getOcorrencias(i).getCdCist());
			listarMsgAlertaGestorSaidaDTO.setTipoMensagem(listarMsgAlertaGestorResponse.getOcorrencias(i).getNrEventoMensagemNegocio());
			listarMsgAlertaGestorSaidaDTO.setDsTipoMensagem(listarMsgAlertaGestorResponse.getOcorrencias(i).getDsMensagemAlerta());
			listarMsgAlertaGestorSaidaDTO.setRecurso(listarMsgAlertaGestorResponse.getOcorrencias(i).getCdRecGedorMensagem());
			listarMsgAlertaGestorSaidaDTO.setDsRecurso(listarMsgAlertaGestorResponse.getOcorrencias(i).getDsRecGedorMensagem());
			listarMsgAlertaGestorSaidaDTO.setIdioma(listarMsgAlertaGestorResponse.getOcorrencias(i).getCdIdiomaTextoMensagem());
			listarMsgAlertaGestorSaidaDTO.setDsIdioma(listarMsgAlertaGestorResponse.getOcorrencias(i).getDsIdiomaTextoMensagem());
							
			listaMsgAlertaGestor.add(listarMsgAlertaGestorSaidaDTO);
		}
					
		
		
		return listaMsgAlertaGestor;	
		
	}
	
	

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.msgalertagestor.IMsgAlertaGestorService#alterarMensagemAlertaGestor(br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.AlterarMsgAlertaGestorEntradaDTO)
	 */
	public AlterarMsgAlertaGestorSaidaDTO alterarMensagemAlertaGestor(AlterarMsgAlertaGestorEntradaDTO alterarMsgAlertaGestorEntradaDTO) {
		
		
		AlterarMsgAlertaGestorRequest alterarMsgAlertaGestorRequest = new AlterarMsgAlertaGestorRequest();
		AlterarMsgAlertaGestorResponse alterarMsgAlertaGestorResponse = new AlterarMsgAlertaGestorResponse();
		
		AlterarMsgAlertaGestorSaidaDTO msgAlertaGestorSaidaDTO = new AlterarMsgAlertaGestorSaidaDTO();
		
		alterarMsgAlertaGestorRequest.setCdCistProcs(alterarMsgAlertaGestorEntradaDTO.getCentroCusto());
		alterarMsgAlertaGestorRequest.setCdPessoa(alterarMsgAlertaGestorEntradaDTO.getFuncionario());
		alterarMsgAlertaGestorRequest.setCdCist(alterarMsgAlertaGestorEntradaDTO.getPrefixo());
		alterarMsgAlertaGestorRequest.setNrEventoMensagemNegocio(alterarMsgAlertaGestorEntradaDTO.getTipoMensagem());
		alterarMsgAlertaGestorRequest.setCdRecGedorMensagem(alterarMsgAlertaGestorEntradaDTO.getRecurso());
		alterarMsgAlertaGestorRequest.setCdIdiomaTextoMensagem(alterarMsgAlertaGestorEntradaDTO.getIdioma());
		alterarMsgAlertaGestorRequest.setCdIndicadorMeioTransmissao(alterarMsgAlertaGestorEntradaDTO.getMeioTransmissao());
		alterarMsgAlertaGestorRequest.setCdProcsSistema(alterarMsgAlertaGestorEntradaDTO.getCdProcsSistema());
		alterarMsgAlertaGestorRequest.setNrSeqMensagemAlerta(alterarMsgAlertaGestorEntradaDTO.getNrSeqMensagemAlerta());
		
	
		alterarMsgAlertaGestorResponse = getFactoryAdapter().getAlterarMsgAlertaGestorPDCAdapter().invokeProcess(alterarMsgAlertaGestorRequest);
				
		msgAlertaGestorSaidaDTO.setCodMensagem(alterarMsgAlertaGestorResponse.getCodMensagem());
		msgAlertaGestorSaidaDTO.setMensagem(alterarMsgAlertaGestorResponse.getMensagem());
				
		
		return msgAlertaGestorSaidaDTO;
		
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.msgalertagestor.IMsgAlertaGestorService#excluirMensagemAlertaGestor(br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ExcluirMsgAlertaGestorEntradaDTO)
	 */
	public ExcluirMsgAlertaGestorSaidaDTO excluirMensagemAlertaGestor(ExcluirMsgAlertaGestorEntradaDTO msgAlertaGestorEntradaDTO) {
		
		ExcluirMsgAlertaGestorSaidaDTO excluirMsgAlertaGestorSaidaDTO = new ExcluirMsgAlertaGestorSaidaDTO();
			
		ExlcuirMsgAlertaGestorRequest excluirMsgAlertaGestorRequest = new ExlcuirMsgAlertaGestorRequest();
		ExlcuirMsgAlertaGestorResponse excluirMsgAlertaGestorResponse = new ExlcuirMsgAlertaGestorResponse();
		
		excluirMsgAlertaGestorRequest.setCdCistProcs(msgAlertaGestorEntradaDTO.getCentroCusto());
		excluirMsgAlertaGestorRequest.setCdPessoa(msgAlertaGestorEntradaDTO.getFuncionario());
		excluirMsgAlertaGestorRequest.setCdCist(msgAlertaGestorEntradaDTO.getPrefixo());
		excluirMsgAlertaGestorRequest.setNrEventoMensagemNegocio(msgAlertaGestorEntradaDTO.getTipoMensagem());
		excluirMsgAlertaGestorRequest.setCdRecGedorMensagem(msgAlertaGestorEntradaDTO.getRecurso());
		excluirMsgAlertaGestorRequest.setCdIdiomaTextoMensagem(msgAlertaGestorEntradaDTO.getIdioma());
		excluirMsgAlertaGestorRequest.setCdIndicadorMeioTransmissao(msgAlertaGestorEntradaDTO.getMeioTransmissao());
		excluirMsgAlertaGestorRequest.setCdProcsSistema(msgAlertaGestorEntradaDTO.getCdProcsSistema());
		excluirMsgAlertaGestorRequest.setNrSeqMensagemAlerta(msgAlertaGestorEntradaDTO.getNrSeqMensagemAlerta());
		
		excluirMsgAlertaGestorResponse = getFactoryAdapter().getExcluirMsgAlertaGestorPDCAdapter().invokeProcess(excluirMsgAlertaGestorRequest);
				
		excluirMsgAlertaGestorSaidaDTO.setCodMensagem(excluirMsgAlertaGestorResponse.getCodMensagem());
		excluirMsgAlertaGestorSaidaDTO.setMensagem(excluirMsgAlertaGestorResponse.getMensagem());
		
				
		return excluirMsgAlertaGestorSaidaDTO;
		
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.msgalertagestor.IMsgAlertaGestorService#incluirMensagemAlertaGestor(br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.IncluirMsgAlertaGestorEntradaDTO)
	 */
	public IncluirMsgAlertaGestorSaidaDTO incluirMensagemAlertaGestor(IncluirMsgAlertaGestorEntradaDTO incluirMsgAlertaGestorEntradaDTO) {
		
		IncluirMsgAlertaGestorSaidaDTO incluirMsgAlertaGestorSaidaDTO = new IncluirMsgAlertaGestorSaidaDTO();
		IncluirMsgAlertaGestorRequest incluirMsgAlertaGestorRequest = new IncluirMsgAlertaGestorRequest();
		IncluirMsgAlertaGestorResponse incluirMsgAlertaGestorResponse = new IncluirMsgAlertaGestorResponse();
		
		incluirMsgAlertaGestorRequest.setCdCistProcs(incluirMsgAlertaGestorEntradaDTO.getCentroCusto());
		incluirMsgAlertaGestorRequest.setCdProcsSistema(incluirMsgAlertaGestorEntradaDTO.getTipoProcesso());
		incluirMsgAlertaGestorRequest.setNrSeqMensagemAlerta(incluirMsgAlertaGestorEntradaDTO.getNrSeqMensagemAlerta());
		incluirMsgAlertaGestorRequest.setCdPessoa(incluirMsgAlertaGestorEntradaDTO.getCdFuncionario());
		incluirMsgAlertaGestorRequest.setCdCist(incluirMsgAlertaGestorEntradaDTO.getPrefixo());
		incluirMsgAlertaGestorRequest.setNrEventoMensagemNegocio(incluirMsgAlertaGestorEntradaDTO.getTipoMensagem());		
		incluirMsgAlertaGestorRequest.setCdRecGedorMensagem(incluirMsgAlertaGestorEntradaDTO.getRecurso());
		incluirMsgAlertaGestorRequest.setCdIdiomaTextoMensagem(incluirMsgAlertaGestorEntradaDTO.getIdioma());
		incluirMsgAlertaGestorRequest.setCdIndicadorMeioTransmissao(incluirMsgAlertaGestorEntradaDTO.getMeioTransmissao());
		
		incluirMsgAlertaGestorResponse = getFactoryAdapter().getIncluirMsgAlertaGestorPDCAdapter().invokeProcess(incluirMsgAlertaGestorRequest);
				
		incluirMsgAlertaGestorSaidaDTO.setCodMensagem(incluirMsgAlertaGestorResponse.getCodMensagem());
		incluirMsgAlertaGestorSaidaDTO.setMensagem(incluirMsgAlertaGestorResponse.getMensagem());
				
		return incluirMsgAlertaGestorSaidaDTO;
		
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.msgalertagestor.IMsgAlertaGestorService#detalharMensagemAlertaGestaor(br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.DetalharMsgAlertaGestorEntradaDTO)
	 */
	public DetalharMsgAlertaGestorSaidaDTO detalharMensagemAlertaGestaor(DetalharMsgAlertaGestorEntradaDTO detalharMsgAlertaGestorEntradaDTO) {
		
		DetalharMsgAlertaGestorSaidaDTO saidaDTO = new DetalharMsgAlertaGestorSaidaDTO();
		
		ConsultarDetalheMsgAlertaGestorRequest request = new ConsultarDetalheMsgAlertaGestorRequest();
		ConsultarDetalheMsgAlertaGestorResponse response = new ConsultarDetalheMsgAlertaGestorResponse();
	
		request.setCdCist(detalharMsgAlertaGestorEntradaDTO.getPrefixo());
		request.setCdCistProcs(detalharMsgAlertaGestorEntradaDTO.getCentroCusto());
		request.setCdIdiomaTextoMensagem(detalharMsgAlertaGestorEntradaDTO.getIdioma());
		request.setCdPessoa(detalharMsgAlertaGestorEntradaDTO.getCdFuncionario());
		request.setCdProcsSistema(detalharMsgAlertaGestorEntradaDTO.getCdProcsSistema());
		request.setCdRecGedorMensagem(detalharMsgAlertaGestorEntradaDTO.getRecurso());
		request.setNrEventoMensagemNegocio(detalharMsgAlertaGestorEntradaDTO.getTipoMensagem());
		request.setNrSeqMensagemAlerta(detalharMsgAlertaGestorEntradaDTO.getNrSeqMensagemAlerta());
		request.setQtConsultas(0);
		
		response = getFactoryAdapter().getDetalharMsgAlertaGestorPDCAdapter().invokeProcess(request);
	
		response.getOcorrencias(0).getCdIndicadorMeioTransmissao();
		saidaDTO = new DetalharMsgAlertaGestorSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		saidaDTO.setCentroCusto(response.getOcorrencias(0).getCdCist());
		saidaDTO.setCdTipoProcesso(response.getOcorrencias(0).getCdProcsSistema());
		saidaDTO.setDsTipoProcesso(response.getOcorrencias(0).getDsProcsSistema());
		saidaDTO.setCdFuncionario(response.getOcorrencias(0).getCdPessoa());
		saidaDTO.setDsFuncionario(response.getOcorrencias(0).getDsPessoa());
		saidaDTO.setCdTipoMensagem(response.getOcorrencias(0).getNrEventoMensagemNegocio());
		saidaDTO.setDsTipoMensagem(response.getOcorrencias(0).getDsMensagemAlerta());
		saidaDTO.setPrefixo(response.getOcorrencias(0).getCdCistEvendoMensagem());
		saidaDTO.setCdRecurso(response.getOcorrencias(0).getCdRecGedorMensagem());
		saidaDTO.setDsRecurso(response.getOcorrencias(0).getDsRecGedorMensagem());
		saidaDTO.setCdIdioma(response.getOcorrencias(0).getCdIdiomaTextoMensagem());
		saidaDTO.setDsIdioma(response.getOcorrencias(0).getDsIdiomaTextoMensagem());
		saidaDTO.setCdIndicadorMeioTransmissao(response.getOcorrencias(0).getCdIndicadorMeioTransmissao());
		saidaDTO.setCdProcSist(response.getOcorrencias(0).getCdProcsSistema());
		
		saidaDTO.setCdCanalInclusao(response.getOcorrencias(0).getCdCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsCanalInclusao()); 
		saidaDTO.setUsuarioInclusao(response.getOcorrencias(0).getCdAutenSegrcInclusao());
		saidaDTO.setComplementoInclusao(response.getOcorrencias(0).getNmOperFluxoInclusao());
		saidaDTO.setDataHoraInclusao(response.getOcorrencias(0).getHrInclusaoRegistro());
		
		saidaDTO.setCdCanalManutencao(response.getOcorrencias(0).getCdCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsCanalManutencao()); 
		saidaDTO.setUsuarioManutencao(response.getOcorrencias(0).getCdAutenSegrcManutencao());
		saidaDTO.setComplementoManutencao(response.getOcorrencias(0).getNmOperFluxoManutencao());
		saidaDTO.setDataHoraManutencao(response.getOcorrencias(0).getHrManutencaoRegistro());
		
		
		return saidaDTO;
		
	}
    
}

