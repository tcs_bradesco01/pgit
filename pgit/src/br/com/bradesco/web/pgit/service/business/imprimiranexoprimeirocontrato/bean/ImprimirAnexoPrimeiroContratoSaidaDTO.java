/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean;

import java.util.List;

/**
 * Nome: ImprimirAnexoPrimeiroContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirAnexoPrimeiroContratoSaidaDTO {
	
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo nrSequenciaContrato. */
    private Long nrSequenciaContrato;
    
    /** Atributo dtInclusaoContrato. */
    private String dtInclusaoContrato;
    
    /** Atributo dtInicioVigencia. */
    private String dtInicioVigencia;
    
    /** Atributo dtFinalVigencia. */
    private String dtFinalVigencia;
    
    /** Atributo cdCpfCnpjRepresentante. */
    private String cdCpfCnpjRepresentante;
    
    /** Atributo dsNomeRepresentante. */
    private String dsNomeRepresentante;
    
    /** Atributo cdGrupoEconomico. */
    private Long cdGrupoEconomico;
    
    /** Atributo dsGrupoEconomico. */
    private String dsGrupoEconomico;
    
    /** Atributo cdAtividadeEconomica. */
    private Integer cdAtividadeEconomica;
    
    /** Atributo dsAtividadeEconomica. */
    private String dsAtividadeEconomica;
    
    /** Atributo cdSegmentoEconomico. */
    private Integer cdSegmentoEconomico;
    
    /** Atributo dsSegmentoEconomico. */
    private String dsSegmentoEconomico;
    
    /** Atributo cdSubSegmento. */
    private Integer cdSubSegmento;
    
    /** Atributo dsSubSegmento. */
    private String dsSubSegmento;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo cdAgenciaGestoraContrato. */
    private Integer cdAgenciaGestoraContrato;
    
    /** Atributo dsAgenciaGestoraContrato. */
    private String dsAgenciaGestoraContrato;
    
    /** Atributo qtdeParticipante. */
    private Integer qtdeParticipante;
    
    /** Atributo listaParticipantes. */
    private List<OcorrenciasParticipantes> listaParticipantes;    
    
    /** Atributo qtdeConta. */
    private Integer qtdeConta;    
    
    /** Atributo listaContas. */
    private List<OcorrenciasContas> listaContas;    
    
    /** Atributo qtLayout. */
    private Integer qtLayout;    
    
    /** Atributo listaLayouts. */
    private List<OcorrenciasLayout> listaLayouts;
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
	
	/**
	 * Get: dtInclusaoContrato.
	 *
	 * @return dtInclusaoContrato
	 */
	public String getDtInclusaoContrato() {
		return dtInclusaoContrato;
	}
	
	/**
	 * Set: dtInclusaoContrato.
	 *
	 * @param dtInclusaoContrato the dt inclusao contrato
	 */
	public void setDtInclusaoContrato(String dtInclusaoContrato) {
		this.dtInclusaoContrato = dtInclusaoContrato;
	}
	
	/**
	 * Get: dtInicioVigencia.
	 *
	 * @return dtInicioVigencia
	 */
	public String getDtInicioVigencia() {
		return dtInicioVigencia;
	}
	
	/**
	 * Set: dtInicioVigencia.
	 *
	 * @param dtInicioVigencia the dt inicio vigencia
	 */
	public void setDtInicioVigencia(String dtInicioVigencia) {
		this.dtInicioVigencia = dtInicioVigencia;
	}
	
	/**
	 * Get: dtFinalVigencia.
	 *
	 * @return dtFinalVigencia
	 */
	public String getDtFinalVigencia() {
		return dtFinalVigencia;
	}
	
	/**
	 * Set: dtFinalVigencia.
	 *
	 * @param dtFinalVigencia the dt final vigencia
	 */
	public void setDtFinalVigencia(String dtFinalVigencia) {
		this.dtFinalVigencia = dtFinalVigencia;
	}
	
	/**
	 * Get: cdCpfCnpjRepresentante.
	 *
	 * @return cdCpfCnpjRepresentante
	 */
	public String getCdCpfCnpjRepresentante() {
		return cdCpfCnpjRepresentante;
	}
	
	/**
	 * Set: cdCpfCnpjRepresentante.
	 *
	 * @param cdCpfCnpjRepresentante the cd cpf cnpj representante
	 */
	public void setCdCpfCnpjRepresentante(String cdCpfCnpjRepresentante) {
		this.cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
	}
	
	/**
	 * Get: dsNomeRepresentante.
	 *
	 * @return dsNomeRepresentante
	 */
	public String getDsNomeRepresentante() {
		return dsNomeRepresentante;
	}
	
	/**
	 * Set: dsNomeRepresentante.
	 *
	 * @param dsNomeRepresentante the ds nome representante
	 */
	public void setDsNomeRepresentante(String dsNomeRepresentante) {
		this.dsNomeRepresentante = dsNomeRepresentante;
	}
	
	/**
	 * Get: cdGrupoEconomico.
	 *
	 * @return cdGrupoEconomico
	 */
	public Long getCdGrupoEconomico() {
		return cdGrupoEconomico;
	}
	
	/**
	 * Set: cdGrupoEconomico.
	 *
	 * @param cdGrupoEconomico the cd grupo economico
	 */
	public void setCdGrupoEconomico(Long cdGrupoEconomico) {
		this.cdGrupoEconomico = cdGrupoEconomico;
	}
	
	/**
	 * Get: dsGrupoEconomico.
	 *
	 * @return dsGrupoEconomico
	 */
	public String getDsGrupoEconomico() {
		return dsGrupoEconomico;
	}
	
	/**
	 * Set: dsGrupoEconomico.
	 *
	 * @param dsGrupoEconomico the ds grupo economico
	 */
	public void setDsGrupoEconomico(String dsGrupoEconomico) {
		this.dsGrupoEconomico = dsGrupoEconomico;
	}
	
	/**
	 * Get: cdAtividadeEconomica.
	 *
	 * @return cdAtividadeEconomica
	 */
	public Integer getCdAtividadeEconomica() {
		return cdAtividadeEconomica;
	}
	
	/**
	 * Set: cdAtividadeEconomica.
	 *
	 * @param cdAtividadeEconomica the cd atividade economica
	 */
	public void setCdAtividadeEconomica(Integer cdAtividadeEconomica) {
		this.cdAtividadeEconomica = cdAtividadeEconomica;
	}
	
	/**
	 * Get: dsAtividadeEconomica.
	 *
	 * @return dsAtividadeEconomica
	 */
	public String getDsAtividadeEconomica() {
		return dsAtividadeEconomica;
	}
	
	/**
	 * Set: dsAtividadeEconomica.
	 *
	 * @param dsAtividadeEconomica the ds atividade economica
	 */
	public void setDsAtividadeEconomica(String dsAtividadeEconomica) {
		this.dsAtividadeEconomica = dsAtividadeEconomica;
	}
	
	/**
	 * Get: cdSegmentoEconomico.
	 *
	 * @return cdSegmentoEconomico
	 */
	public Integer getCdSegmentoEconomico() {
		return cdSegmentoEconomico;
	}
	
	/**
	 * Set: cdSegmentoEconomico.
	 *
	 * @param cdSegmentoEconomico the cd segmento economico
	 */
	public void setCdSegmentoEconomico(Integer cdSegmentoEconomico) {
		this.cdSegmentoEconomico = cdSegmentoEconomico;
	}
	
	/**
	 * Get: dsSegmentoEconomico.
	 *
	 * @return dsSegmentoEconomico
	 */
	public String getDsSegmentoEconomico() {
		return dsSegmentoEconomico;
	}
	
	/**
	 * Set: dsSegmentoEconomico.
	 *
	 * @param dsSegmentoEconomico the ds segmento economico
	 */
	public void setDsSegmentoEconomico(String dsSegmentoEconomico) {
		this.dsSegmentoEconomico = dsSegmentoEconomico;
	}
	
	/**
	 * Get: cdSubSegmento.
	 *
	 * @return cdSubSegmento
	 */
	public Integer getCdSubSegmento() {
		return cdSubSegmento;
	}
	
	/**
	 * Set: cdSubSegmento.
	 *
	 * @param cdSubSegmento the cd sub segmento
	 */
	public void setCdSubSegmento(Integer cdSubSegmento) {
		this.cdSubSegmento = cdSubSegmento;
	}
	
	/**
	 * Get: dsSubSegmento.
	 *
	 * @return dsSubSegmento
	 */
	public String getDsSubSegmento() {
		return dsSubSegmento;
	}
	
	/**
	 * Set: dsSubSegmento.
	 *
	 * @param dsSubSegmento the ds sub segmento
	 */
	public void setDsSubSegmento(String dsSubSegmento) {
		this.dsSubSegmento = dsSubSegmento;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: cdAgenciaGestoraContrato.
	 *
	 * @return cdAgenciaGestoraContrato
	 */
	public Integer getCdAgenciaGestoraContrato() {
		return cdAgenciaGestoraContrato;
	}
	
	/**
	 * Set: cdAgenciaGestoraContrato.
	 *
	 * @param cdAgenciaGestoraContrato the cd agencia gestora contrato
	 */
	public void setCdAgenciaGestoraContrato(Integer cdAgenciaGestoraContrato) {
		this.cdAgenciaGestoraContrato = cdAgenciaGestoraContrato;
	}
	
	/**
	 * Get: dsAgenciaGestoraContrato.
	 *
	 * @return dsAgenciaGestoraContrato
	 */
	public String getDsAgenciaGestoraContrato() {
		return dsAgenciaGestoraContrato;
	}
	
	/**
	 * Set: dsAgenciaGestoraContrato.
	 *
	 * @param dsAgenciaGestoraContrato the ds agencia gestora contrato
	 */
	public void setDsAgenciaGestoraContrato(String dsAgenciaGestoraContrato) {
		this.dsAgenciaGestoraContrato = dsAgenciaGestoraContrato;
	}
	
	/**
	 * Get: qtdeParticipante.
	 *
	 * @return qtdeParticipante
	 */
	public Integer getQtdeParticipante() {
		return qtdeParticipante;
	}
	
	/**
	 * Set: qtdeParticipante.
	 *
	 * @param qtdeParticipante the qtde participante
	 */
	public void setQtdeParticipante(Integer qtdeParticipante) {
		this.qtdeParticipante = qtdeParticipante;
	}
	
	/**
	 * Get: listaParticipantes.
	 *
	 * @return listaParticipantes
	 */
	public List<OcorrenciasParticipantes> getListaParticipantes() {
		return listaParticipantes;
	}
	
	/**
	 * Set: listaParticipantes.
	 *
	 * @param listaParticipantes the lista participantes
	 */
	public void setListaParticipantes(
			List<OcorrenciasParticipantes> listaParticipantes) {
		this.listaParticipantes = listaParticipantes;
	}
	
	/**
	 * Get: qtdeConta.
	 *
	 * @return qtdeConta
	 */
	public Integer getQtdeConta() {
		return qtdeConta;
	}
	
	/**
	 * Set: qtdeConta.
	 *
	 * @param qtdeConta the qtde conta
	 */
	public void setQtdeConta(Integer qtdeConta) {
		this.qtdeConta = qtdeConta;
	}
	
	/**
	 * Get: listaContas.
	 *
	 * @return listaContas
	 */
	public List<OcorrenciasContas> getListaContas() {
		return listaContas;
	}
	
	/**
	 * Set: listaContas.
	 *
	 * @param listaContas the lista contas
	 */
	public void setListaContas(List<OcorrenciasContas> listaContas) {
		this.listaContas = listaContas;
	}
	
	/**
	 * Get: qtLayout.
	 *
	 * @return qtLayout
	 */
	public Integer getQtLayout() {
		return qtLayout;
	}
	
	/**
	 * Set: qtLayout.
	 *
	 * @param qtLayout the qt layout
	 */
	public void setQtLayout(Integer qtLayout) {
		this.qtLayout = qtLayout;
	}
	
	/**
	 * Get: listaLayouts.
	 *
	 * @return listaLayouts
	 */
	public List<OcorrenciasLayout> getListaLayouts() {
		return listaLayouts;
	}
	
	/**
	 * Set: listaLayouts.
	 *
	 * @param listaLayouts the lista layouts
	 */
	public void setListaLayouts(List<OcorrenciasLayout> listaLayouts) {
		this.listaLayouts = listaLayouts;
	}  
     
}
