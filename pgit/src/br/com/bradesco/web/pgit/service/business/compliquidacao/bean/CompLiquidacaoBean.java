/*
 * Nome: br.com.bradesco.web.pgit.service.business.compliquidacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.compliquidacao.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import br.com.bradesco.web.pgit.service.business.arquivoretorno.bean.ArquivoRetornoDTO;

/**
 * Nome: CompLiquidacaoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class CompLiquidacaoBean {
	
	/** Atributo mostraBotoes0010. */
	private boolean mostraBotoes0010;
	
	/** Atributo codListaRadio. */
	private int codListaRadio;
	
	/** Atributo filtro. */
	private int filtro;
	
	/** Atributo formaLiquidacaoFiltro. */
	private String formaLiquidacaoFiltro;
	
	/** Atributo listaFormaLiquidacao. */
	private List<SelectItem> listaFormaLiquidacao = new ArrayList<SelectItem>();	
	
	/** Atributo produtoFiltro. */
	private String produtoFiltro;
	
	/** Atributo listaProduto. */
	private List<SelectItem> listaProduto = new ArrayList<SelectItem>();
	
	/** Atributo operacaoFiltro. */
	private String operacaoFiltro;
	
	/** Atributo listaOperacao. */
	private List<SelectItem> listaOperacao = new ArrayList<SelectItem>();
	
	/** Atributo tipoServicoFiltro. */
	private String tipoServicoFiltro;
	
	/** Atributo formaLancamentoFiltro. */
	private String formaLancamentoFiltro;
	
	/** Atributo listaFormaLancamento. */
	private List<SelectItem> listaFormaLancamento = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();
	
	/** Atributo listaGridComposicao. */
	private List<ArquivoRetornoDTO> listaGridComposicao;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo formaLiquidacao. */
	private String formaLiquidacao;
	
	/** Atributo dataHora. */
	private String dataHora;
	
	/** Atributo terminal. */
	private String terminal;
	
	/** Atributo usuario. */
	private String usuario;
	
	/** Atributo operacao. */
	private String operacao;
	
	/** Atributo produto. */
	private String produto;
	
	/** Atributo tipoServico. */
	private String tipoServico;
	
	/** Atributo formaLancamento. */
	private String formaLancamento;
	
	/**
	 * Is mostra botoes0010.
	 *
	 * @return true, if is mostra botoes0010
	 */
	public boolean isMostraBotoes0010() {
		return mostraBotoes0010;
	}

	/**
	 * Set: mostraBotoes0010.
	 *
	 * @param mostraBotoes0010 the mostra botoes0010
	 */
	public void setMostraBotoes0010(boolean mostraBotoes0010) {
		this.mostraBotoes0010 = mostraBotoes0010;
	}

	/**
	 * Get: dataHora.
	 *
	 * @return dataHora
	 */
	public String getDataHora() {
		return dataHora;
	}

	/**
	 * Set: dataHora.
	 *
	 * @param dataHora the data hora
	 */
	public void setDataHora(String dataHora) {
		this.dataHora = dataHora;
	}

	/**
	 * Get: formaLancamento.
	 *
	 * @return formaLancamento
	 */
	public String getFormaLancamento() {
		return formaLancamento;
	}

	/**
	 * Set: formaLancamento.
	 *
	 * @param formaLancamento the forma lancamento
	 */
	public void setFormaLancamento(String formaLancamento) {
		this.formaLancamento = formaLancamento;
	}

	/**
	 * Get: formaLiquidacao.
	 *
	 * @return formaLiquidacao
	 */
	public String getFormaLiquidacao() {
		return formaLiquidacao;
	}

	/**
	 * Set: formaLiquidacao.
	 *
	 * @param formaLiquidacao the forma liquidacao
	 */
	public void setFormaLiquidacao(String formaLiquidacao) {
		this.formaLiquidacao = formaLiquidacao;
	}

	/**
	 * Get: operacao.
	 *
	 * @return operacao
	 */
	public String getOperacao() {
		return operacao;
	}

	/**
	 * Set: operacao.
	 *
	 * @param operacao the operacao
	 */
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	/**
	 * Get: produto.
	 *
	 * @return produto
	 */
	public String getProduto() {
		return produto;
	}

	/**
	 * Set: produto.
	 *
	 * @param produto the produto
	 */
	public void setProduto(String produto) {
		this.produto = produto;
	}

	/**
	 * Get: terminal.
	 *
	 * @return terminal
	 */
	public String getTerminal() {
		return terminal;
	}

	/**
	 * Set: terminal.
	 *
	 * @param terminal the terminal
	 */
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public String getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Get: usuario.
	 *
	 * @return usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * Set: usuario.
	 *
	 * @param usuario the usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * Get: codListaRadio.
	 *
	 * @return codListaRadio
	 */
	public int getCodListaRadio() {
		return codListaRadio;
	}

	/**
	 * Set: codListaRadio.
	 *
	 * @param codListaRadio the cod lista radio
	 */
	public void setCodListaRadio(int codListaRadio) {
		this.codListaRadio = codListaRadio;
	}

	/**
	 * Get: filtro.
	 *
	 * @return filtro
	 */
	public int getFiltro() {
		return filtro;
	}

	/**
	 * Set: filtro.
	 *
	 * @param filtro the filtro
	 */
	public void setFiltro(int filtro) {
		this.filtro = filtro;
	}

	/**
	 * Get: formaLiquidacaoFiltro.
	 *
	 * @return formaLiquidacaoFiltro
	 */
	public String getFormaLiquidacaoFiltro() {
		return formaLiquidacaoFiltro;
	}

	/**
	 * Set: formaLiquidacaoFiltro.
	 *
	 * @param formaLiquidacaoFiltro the forma liquidacao filtro
	 */
	public void setFormaLiquidacaoFiltro(String formaLiquidacaoFiltro) {
		this.formaLiquidacaoFiltro = formaLiquidacaoFiltro;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaFormaLiquidacao.
	 *
	 * @return listaFormaLiquidacao
	 */
	public List<SelectItem> getListaFormaLiquidacao() {
		return listaFormaLiquidacao;
	}

	/**
	 * Set: listaFormaLiquidacao.
	 *
	 * @param listaFormaLiquidacao the lista forma liquidacao
	 */
	public void setListaFormaLiquidacao(List<SelectItem> listaFormaLiquidacao) {
		this.listaFormaLiquidacao = listaFormaLiquidacao;
	}

	/**
	 * Get: listaGridComposicao.
	 *
	 * @return listaGridComposicao
	 */
	public List<ArquivoRetornoDTO> getListaGridComposicao() {
		return listaGridComposicao;
	}

	/**
	 * Set: listaGridComposicao.
	 *
	 * @param listaGridComposicao the lista grid composicao
	 */
	public void setListaGridComposicao(List<ArquivoRetornoDTO> listaGridComposicao) {
		this.listaGridComposicao = listaGridComposicao;
	}

	/**
	 * Get: listaOperacao.
	 *
	 * @return listaOperacao
	 */
	public List<SelectItem> getListaOperacao() {
		return listaOperacao;
	}

	/**
	 * Set: listaOperacao.
	 *
	 * @param listaOperacao the lista operacao
	 */
	public void setListaOperacao(List<SelectItem> listaOperacao) {
		this.listaOperacao = listaOperacao;
	}

	/**
	 * Get: listaProduto.
	 *
	 * @return listaProduto
	 */
	public List<SelectItem> getListaProduto() {
		return listaProduto;
	}

	/**
	 * Set: listaProduto.
	 *
	 * @param listaProduto the lista produto
	 */
	public void setListaProduto(List<SelectItem> listaProduto) {
		this.listaProduto = listaProduto;
	}

	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: operacaoFiltro.
	 *
	 * @return operacaoFiltro
	 */
	public String getOperacaoFiltro() {
		return operacaoFiltro;
	}

	/**
	 * Set: operacaoFiltro.
	 *
	 * @param operacaoFiltro the operacao filtro
	 */
	public void setOperacaoFiltro(String operacaoFiltro) {
		this.operacaoFiltro = operacaoFiltro;
	}

	/**
	 * Get: produtoFiltro.
	 *
	 * @return produtoFiltro
	 */
	public String getProdutoFiltro() {
		return produtoFiltro;
	}

	/**
	 * Set: produtoFiltro.
	 *
	 * @param produtoFiltro the produto filtro
	 */
	public void setProdutoFiltro(String produtoFiltro) {
		this.produtoFiltro = produtoFiltro;
	}

	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public String getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(String tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		return "INCLUIR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		return "EXCLUIR";
	}
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados(){
		this.setFormaLancamentoFiltro("");
		this.setProdutoFiltro("");
		this.setOperacaoFiltro("");
		this.setTipoServicoFiltro("");
		this.setFormaLancamentoFiltro("");
		return "ok";
	}
	
	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista(){
		if (this.getListaGridComposicao().size() > 5){
			this.setMostraBotoes0010(true);
		}
		else{
			this.setMostraBotoes0010(false);
		}

		return "ok";
	}
	

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		return "ok";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		return "AVANCAR_INCLUIR";
	}
	
	/**
	 * Confirmar.
	 *
	 * @return the string
	 */
	public String confirmar(){
		return "CONFIRMAR";
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		return "DETALHAR";
	}


	/**
	 * Get: formaLancamentoFiltro.
	 *
	 * @return formaLancamentoFiltro
	 */
	public String getFormaLancamentoFiltro() {
		return formaLancamentoFiltro;
	}

	/**
	 * Set: formaLancamentoFiltro.
	 *
	 * @param formaLancamentoFiltro the forma lancamento filtro
	 */
	public void setFormaLancamentoFiltro(String formaLancamentoFiltro) {
		this.formaLancamentoFiltro = formaLancamentoFiltro;
	}

	/**
	 * Get: listaFormaLancamento.
	 *
	 * @return listaFormaLancamento
	 */
	public List<SelectItem> getListaFormaLancamento() {
		return listaFormaLancamento;
	}

	/**
	 * Set: listaFormaLancamento.
	 *
	 * @param listaFormaLancamento the lista forma lancamento
	 */
	public void setListaFormaLancamento(List<SelectItem> listaFormaLancamento) {
		this.listaFormaLancamento = listaFormaLancamento;
	}


}
