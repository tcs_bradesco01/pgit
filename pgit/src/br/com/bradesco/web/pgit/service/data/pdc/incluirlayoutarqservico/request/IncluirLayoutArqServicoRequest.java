/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirlayoutarqservico.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirLayoutArqServicoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirLayoutArqServicoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdIdentificadorLayoutNegocio
     */
    private int _cdIdentificadorLayoutNegocio = 0;

    /**
     * keeps track of state for field: _cdIdentificadorLayoutNegocio
     */
    private boolean _has_cdIdentificadorLayoutNegocio;

    /**
     * Field _cdIndicadorLayoutDefault
     */
    private int _cdIndicadorLayoutDefault = 0;

    /**
     * keeps track of state for field: _cdIndicadorLayoutDefault
     */
    private boolean _has_cdIndicadorLayoutDefault;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirLayoutArqServicoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirlayoutarqservico.request.IncluirLayoutArqServicoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIdentificadorLayoutNegocio
     * 
     */
    public void deleteCdIdentificadorLayoutNegocio()
    {
        this._has_cdIdentificadorLayoutNegocio= false;
    } //-- void deleteCdIdentificadorLayoutNegocio() 

    /**
     * Method deleteCdIndicadorLayoutDefault
     * 
     */
    public void deleteCdIndicadorLayoutDefault()
    {
        this._has_cdIndicadorLayoutDefault= false;
    } //-- void deleteCdIndicadorLayoutDefault() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdIdentificadorLayoutNegocio'.
     * 
     * @return int
     * @return the value of field 'cdIdentificadorLayoutNegocio'.
     */
    public int getCdIdentificadorLayoutNegocio()
    {
        return this._cdIdentificadorLayoutNegocio;
    } //-- int getCdIdentificadorLayoutNegocio() 

    /**
     * Returns the value of field 'cdIndicadorLayoutDefault'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorLayoutDefault'.
     */
    public int getCdIndicadorLayoutDefault()
    {
        return this._cdIndicadorLayoutDefault;
    } //-- int getCdIndicadorLayoutDefault() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Method hasCdIdentificadorLayoutNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificadorLayoutNegocio()
    {
        return this._has_cdIdentificadorLayoutNegocio;
    } //-- boolean hasCdIdentificadorLayoutNegocio() 

    /**
     * Method hasCdIndicadorLayoutDefault
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorLayoutDefault()
    {
        return this._has_cdIndicadorLayoutDefault;
    } //-- boolean hasCdIndicadorLayoutDefault() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIdentificadorLayoutNegocio'.
     * 
     * @param cdIdentificadorLayoutNegocio the value of field
     * 'cdIdentificadorLayoutNegocio'.
     */
    public void setCdIdentificadorLayoutNegocio(int cdIdentificadorLayoutNegocio)
    {
        this._cdIdentificadorLayoutNegocio = cdIdentificadorLayoutNegocio;
        this._has_cdIdentificadorLayoutNegocio = true;
    } //-- void setCdIdentificadorLayoutNegocio(int) 

    /**
     * Sets the value of field 'cdIndicadorLayoutDefault'.
     * 
     * @param cdIndicadorLayoutDefault the value of field
     * 'cdIndicadorLayoutDefault'.
     */
    public void setCdIndicadorLayoutDefault(int cdIndicadorLayoutDefault)
    {
        this._cdIndicadorLayoutDefault = cdIndicadorLayoutDefault;
        this._has_cdIndicadorLayoutDefault = true;
    } //-- void setCdIndicadorLayoutDefault(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirLayoutArqServicoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirlayoutarqservico.request.IncluirLayoutArqServicoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirlayoutarqservico.request.IncluirLayoutArqServicoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirlayoutarqservico.request.IncluirLayoutArqServicoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirlayoutarqservico.request.IncluirLayoutArqServicoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
