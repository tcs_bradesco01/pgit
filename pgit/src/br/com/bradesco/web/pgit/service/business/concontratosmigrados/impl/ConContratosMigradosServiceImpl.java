/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.concontratosmigrados.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.concontratosmigrados.IConContratosMigradosService;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.IConContratosMigradosServiceConstants;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.ConContratoMigradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.ConContratoMigradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.DetalharContratoMigradoHsbcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.DetalharContratoMigradoHsbcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.RevertContratoMigradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.RevertContratoMigradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.concontratomigrado.request.ConContratoMigradoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.concontratomigrado.response.ConContratoMigradoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultardadosbasicocontrato.request.ConsultarDadosBasicoContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultardadosbasicocontrato.response.ConsultarDadosBasicoContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontratomigradohsbc.request.DetalharContratoMigradoHsbcRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcontratomigradohsbc.response.DetalharContratoMigradoHsbcResponse;
import br.com.bradesco.web.pgit.service.data.pdc.revertcontratomigrado.request.RevertContratoMigradoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.revertcontratomigrado.response.RevertContratoMigradoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ConContratosMigrados
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ConContratosMigradosServiceImpl implements IConContratosMigradosService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.concontratosmigrados.IConContratosMigradosService#conContratoMigrado(br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.ConContratoMigradoEntradaDTO)
	 */
	public List<ConContratoMigradoSaidaDTO> conContratoMigrado(ConContratoMigradoEntradaDTO conContratoMigradoEntradaDTO) {
		
		ConContratoMigradoRequest request = new ConContratoMigradoRequest();
		ConContratoMigradoResponse response = new ConContratoMigradoResponse();
		
		List<ConContratoMigradoSaidaDTO> listaSaida = new ArrayList<ConContratoMigradoSaidaDTO>();
		
		
    	request.setNrOcorrencias(IConContratosMigradosServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	request.setCdAgencia(PgitUtil.verificaIntegerNulo(conContratoMigradoEntradaDTO.getCdAgencia()));
    	request.setCdCorpoCpfCnpj(PgitUtil.verificaLongNulo(conContratoMigradoEntradaDTO.getCdCorpoCpfCnpj()));
    	request.setCentroCustoOrigem(PgitUtil.verificaIntegerNulo(conContratoMigradoEntradaDTO.getCentroCustoOrigem()));
    	request.setDtInicio(PgitUtil.verificaStringNula(conContratoMigradoEntradaDTO.getDtInicio()));
    	request.setDtFim(PgitUtil.verificaStringNula(conContratoMigradoEntradaDTO.getDtFim()));
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(conContratoMigradoEntradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(conContratoMigradoEntradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(conContratoMigradoEntradaDTO.getNrSequenciaContratoNegocio()));
    	request.setFiliaCnpj(PgitUtil.verificaIntegerNulo(conContratoMigradoEntradaDTO.getFiliaCnpj()));
    	request.setDigitoCnpj(PgitUtil.verificaIntegerNulo(conContratoMigradoEntradaDTO.getDigitoCnpj()));
    	request.setCdPerfil(PgitUtil.verificaLongNulo(conContratoMigradoEntradaDTO.getCdPerfil()));
    	request.setCdConta(PgitUtil.verificaIntegerNulo(conContratoMigradoEntradaDTO.getCdConta()));
    	request.setCdSituacaoContrato(PgitUtil.verificaIntegerNulo(conContratoMigradoEntradaDTO.getCdSituacaoContrato()));
    	
    	
    	response = getFactoryAdapter().getConContratoMigradoPDCAdapter().invokeProcess(request);
		
    	ConContratoMigradoSaidaDTO saida;
   	 
    	for(int i=0;i<response.getOcorrenciasCount();i++){
    		
    		saida = new ConContratoMigradoSaidaDTO();
    		
    		saida.setCodMensagem(response.getCodMensagem());
    		saida.setMensagem(response.getMensagem());
    	    
    		saida.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
    		saida.setCentroCustoOrigem(response.getOcorrencias(i).getCentroCustoOrigem());
    		saida.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
    		saida.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
    		saida.setCdClub(response.getOcorrencias(i).getCdClub());
    		saida.setCdCorpoCpfCnpj(response.getOcorrencias(i).getCdCorpoCpfCnpj());
    		saida.setFiliaCnpj(response.getOcorrencias(i).getFiliaCnpj());
    		saida.setDigitoCnpj(response.getOcorrencias(i).getDigitoCnpj());
    		saida.setDsNome(response.getOcorrencias(i).getDsNome());
    		saida.setCdPerfil(response.getOcorrencias(i).getCdPerfil());
    		saida.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
    		saida.setDsAgencia(response.getOcorrencias(i).getCdAgencia() == 0? "": String.valueOf(response.getOcorrencias(i).getCdAgencia()));
    		saida.setRazao(response.getOcorrencias(i).getRazao());
    		saida.setDsRazao(response.getOcorrencias(i).getRazao() == 0? "": String.valueOf(response.getOcorrencias(i).getRazao()));
    		saida.setCdConta(response.getOcorrencias(i).getCdConta());
    		saida.setDigitoConta(response.getOcorrencias(i).getDigitoConta());
    		saida.setCdLancamento(response.getOcorrencias(i).getCdLancamento());
    		saida.setDsLancamento(response.getOcorrencias(i).getCdLancamento()== 0? "": String.valueOf(response.getOcorrencias(i).getCdLancamento()));
    		saida.setCdCpfCnpjRepresentante(response.getOcorrencias(i).getCdCpfCnpjRepresentante());
    		saida.setCdFilialRepresentante(response.getOcorrencias(i).getCdFilialRepresentante());
    		saida.setCdCpfDigitoRepresentante(response.getOcorrencias(i).getCdCpfDigitoRepresentante());
    		saida.setDsRezaoSocialRepresentente(response.getOcorrencias(i).getDsRezaoSocialRepresentente());
    		saida.setDsPessoaJuridicaContrato(response.getOcorrencias(i).getDsPessoaJuridicaContrato());
    		saida.setDsTipoContratoNegocio(response.getOcorrencias(i).getDsTipoContratoNegocio());
    		saida.setCdSituacaoContrato(response.getOcorrencias(i).getCdSituacaoContrato());
    		
    		saida.setCpfCnpjRepresentanteFormatado(CpfCnpjUtils.formatarCpfCnpj(
    				response.getOcorrencias(i).getCdCpfCnpjRepresentante(),
    			    	response.getOcorrencias(i).getCdFilialRepresentante(),
    			    		response.getOcorrencias(i).getCdCpfDigitoRepresentante()));
    		
    		saida.setCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(
    				response.getOcorrencias(i).getCdCorpoCpfCnpj(),
    			    	response.getOcorrencias(i).getFiliaCnpj(),
    			    		response.getOcorrencias(i).getDigitoCnpj()));
    		
    		saida.setContaFormatada(PgitUtil.formatConta(Long.parseLong(String.valueOf(response.getOcorrencias(i).getCdConta())), 
    				Integer.toString(response.getOcorrencias(i).getDigitoConta()), false));
    		
    		SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd");		
    		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
    		try{
    			saida.setDtMigracao(formato2.format(formato1.parse(response.getOcorrencias(i).getDtMigracao())));
			} catch (IndexOutOfBoundsException e) {

			} catch (java.text.ParseException e) {
				saida.setDtMigracao("");
			}
    		
    		
    		listaSaida.add(saida);
    	}
		
		return listaSaida;
	}
	
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.concontratosmigrados.IConContratosMigradosService#consultarDadosBasicoContrato(br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoEntradaDTO)
     */
    public ConsultarDadosBasicoContratoSaidaDTO consultarDadosBasicoContrato (ConsultarDadosBasicoContratoEntradaDTO consultarDadosBasicoContratoEntradaDTO) {
    	
    	ConsultarDadosBasicoContratoSaidaDTO saidaDTO = new ConsultarDadosBasicoContratoSaidaDTO();
    	ConsultarDadosBasicoContratoRequest request = new ConsultarDadosBasicoContratoRequest();
    	ConsultarDadosBasicoContratoResponse response = new ConsultarDadosBasicoContratoResponse();
		
    	request.setCdPessoaJuridicaContrato(consultarDadosBasicoContratoEntradaDTO.getCdPessoaJuridicaContrato());
		request.setCdTipoContratoNegocio(consultarDadosBasicoContratoEntradaDTO.getCdTipoContrato());
		request.setNrSequenciaContratoNegocio(consultarDadosBasicoContratoEntradaDTO.getNumeroSequenciaContrato());
		request.setCdClub(consultarDadosBasicoContratoEntradaDTO.getCdClub());
		request.setCdCnpjCpf(consultarDadosBasicoContratoEntradaDTO.getCdCgcCpf());
		request.setCdFilialCnpjCpf(consultarDadosBasicoContratoEntradaDTO.getCdFilialCgcCpf());
		request.setCdControleCnpjCpf(consultarDadosBasicoContratoEntradaDTO.getCdControleCgcCpf());
			
		response = getFactoryAdapter().getConsultarDadosBasicoContratoPDCAdapter().invokeProcess(request);
		
		saidaDTO = new ConsultarDadosBasicoContratoSaidaDTO();
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdGrupoEconomicoParticipante(response.getCdGrupoEconomicoMaster());
		saidaDTO.setDsGrupoEconomico(response.getDsGrupoEconomicoMaster());
		saidaDTO.setCdAtividadeEconomicaParticipante(response.getCdAtividadeEconomicaMaster());
		saidaDTO.setDsAtividadeEconomica(response.getDsAtividadeEconomicaMaster());
		saidaDTO.setCdSegmentoEconomicoParticipante(response.getCdSegmentoParticipanteMaster());
		saidaDTO.setDsSegmentoEconomico(response.getDsSegmentoParticipanteMaster());
		saidaDTO.setCdSubSegmentoEconomico(response.getCdSubSegmentoMaster());
		saidaDTO.setDsSubSegmentoEconomico(response.getDsSubSegmentoMaster());
		saidaDTO.setNomeContrato(response.getDsContratoNegocio());
		saidaDTO.setCdIdioma(response.getCdIdioma());
		saidaDTO.setDsIdioma(response.getDsIdioma());
		saidaDTO.setCdMoeda(response.getCdMoeda());
		saidaDTO.setDsMoeda(response.getDsMoeda());
		saidaDTO.setCdOrigem(response.getCdOrigem());
		saidaDTO.setDsOrigem(response.getDeOrigem());
		saidaDTO.setDsPossuiAditivo(response.getIndicadorAditivo());
		saidaDTO.setCdSituacaoContrato(response.getCdSituacaoContrato());
		saidaDTO.setDsSituacaoContrato(response.getDsSituacaoContrato());
		saidaDTO.setCdSituacaoMotivoContrato(response.getCdSituacaoMotivoContrato());
		saidaDTO.setDsSituacaoMotivoContrato(response.getDsSituacaoMotivoContrato());
		saidaDTO.setDsContratoNegocio(response.getDsContratoNegocio());
		saidaDTO.setDtVigenciaContrato(response.getDtVigenciaContrato());
		saidaDTO.setDataCadastroContrato(response.getDtCadastroContrato());
		saidaDTO.setHoraCadastroContrato(response.getHrCadastroContrato());
		saidaDTO.setDataVigenciaContrato(response.getDtVigenciaContrato());
		saidaDTO.setHoraVigenciaContrato(response.getHrVigenciaContrato());
		saidaDTO.setNumeroComercialContrato(response.getNrComercialContrato());
		saidaDTO.setDsTipoParticipacaoContrato(response.getTipoParticipacaoContrato());
		saidaDTO.setDataAssinaturaContrato(response.getDtAssinaturaContratoNegocio());
		saidaDTO.setHoraAssinaturaContrato(response.getHrAssinaturaContratoNegocio());
		saidaDTO.setCdIndicaPermiteEncerramento(response.getCdIndicadorEncerramentoContrato());
		saidaDTO.setDsIndicaPermiteEncerramento(response.getDsindicadorEncerramentoContrato());
		saidaDTO.setCdAgenciaOperadora(response.getCdUnidadeOperacionalContrato());
		saidaDTO.setNomeAgenciaOperadora(response.getNrUnidadeOperacionalContrato());
		saidaDTO.setCdAgenciaContabil(response.getCdUnidadeContabilContrato());
		saidaDTO.setNomeAgenciaContabil(response.getNrUnidadeContabilContrato());
		saidaDTO.setCdDeptoGestor(response.getCdUnidadeGestoraContrato());
		saidaDTO.setDsDeptoGestor(response.getNrUnidadeGestoraContrato());
		saidaDTO.setCdFuncionarioBradesco(response.getCdFuncionarioBradesco());
		saidaDTO.setDsFuncionarioBradesco(PgitUtil.concatenarCampos(response.getCdFuncionarioBradesco(), response.getNomeFuncionarioBradesco()));
		saidaDTO.setCdPessoaJuridica(response.getCdPessoaJuridica());
		saidaDTO.setNumeroSequencialUnidadeOrganizacional(response.getNrSequenciaUnidadeOrganizacional());
		saidaDTO.setDataInclusaoContrato(response.getDtInclusao());
		saidaDTO.setHoraInclusaoContrato(response.getHrInclusao());
		saidaDTO.setUsuarioInclusaoContrato(response.getCdUsuarioInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
		saidaDTO.setOperacaoFluxoInclusao(response.getOperacaoFluxoInclusao());
		saidaDTO.setDataManutencaoContrato(response.getDtManutencao());
		saidaDTO.setHoraManutencaoContrato(response.getHrManutencao());
		saidaDTO.setUsuarioManutencaoContrato(response.getCdUsuarioManutencao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsTipoCanalManutencao(response.getDsTipoCanalManutencao());
		saidaDTO.setOperacaoFluxoManutencao(response.getOperacaoFluxoManutencao());
		saidaDTO.setCdSetorContrato(response.getCdSetorContrato());
		saidaDTO.setDsSetorContrato(response.getDsSetorContrato());
		saidaDTO.setCdFormaAutorizacaoPagamento(response.getCdFormaAutorizacaoPagamento());
		saidaDTO.setDsFormaAutorizacaoPagamento(response.getDsFormaAutorizacaoPagamento());

		return saidaDTO;
		
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.concontratosmigrados.IConContratosMigradosService
	 * #revertContratoMigrado(br.com.bradesco.web.pgit.service.business.
	 * concontratosmigrados.bean.RevertContratoMigradoEntradaDTO)
	 */
	public RevertContratoMigradoSaidaDTO revertContratoMigrado(RevertContratoMigradoEntradaDTO entradaDTO) {
		RevertContratoMigradoSaidaDTO saidaDTO = new RevertContratoMigradoSaidaDTO();
		RevertContratoMigradoRequest request = new RevertContratoMigradoRequest();
		RevertContratoMigradoResponse response = new RevertContratoMigradoResponse();
    	
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdAcao(verificaIntegerNulo(entradaDTO.getCdAcao()));
    	request.setCdpessoaJuridicaContrato(verificaLongNulo(entradaDTO.getCdpessoaJuridicaContrato()));
    	request.setCentroCustoOrigem(verificaIntegerNulo(entradaDTO.getCentroCustoOrigem()));
    	
    	
    	response = getFactoryAdapter().getRevertContratoMigradoPDCAdapter().invokeProcess(request);
    	
    	saidaDTO.setCodMensagem(response.getCodMensagem());
    	saidaDTO.setMensagem(response.getMensagem());
    	
    	return saidaDTO;
	}

	public DetalharContratoMigradoHsbcSaidaDTO detalharContratoMigradoHsbc(
			DetalharContratoMigradoHsbcEntradaDTO entrada) {
		
		DetalharContratoMigradoHsbcRequest request = new DetalharContratoMigradoHsbcRequest();
		
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
	    request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
	    request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
	    
	    DetalharContratoMigradoHsbcResponse response =  
	    	getFactoryAdapter().getDetalharContratoMigradoHsbcPDCAdapter().invokeProcess(request);
	    
	    DetalharContratoMigradoHsbcSaidaDTO saida = new DetalharContratoMigradoHsbcSaidaDTO(); 
	    
	    saida.setCodMensagem(response.getCodMensagem());
	    saida.setMensagem(response.getMensagem());
	    saida.setCdCpfCnpjPagador(response.getCdCpfCnpjPagador());
	    saida.setCdFilialCpfCnpjPagador(response.getCdFilialCpfCnpjPagador());
	    saida.setCdControleCpfCnpjPagador(response.getCdControleCpfCnpjPagador());
	    saida.setCdBanco(response.getCdBanco());
	    saida.setCdAgenciaBancaria(response.getCdAgenciaBancaria());
	    saida.setCdContaBancaria(response.getCdContaBancaria());
	    saida.setCdDigitoContaBancaria(response.getCdDigitoContaBancaria());
	    
		return saida;
	}

   
    
}

