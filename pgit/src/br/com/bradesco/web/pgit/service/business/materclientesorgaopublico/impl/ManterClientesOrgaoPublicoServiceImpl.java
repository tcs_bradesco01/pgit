/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.AlterarParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.AlterarParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ConsultarUsuarioSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.DetalharParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.DetalharParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.IncluirParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.IncluirParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.IncluirSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarCtasVincPartOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarCtasVincPartOrgaoPublicoOcorrencias;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarCtasVincPartOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarParticipantesOrgaoOcorrencias;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ValidarContratoPagSalPartOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ValidarContratoPagSalPartOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantesorgaopublico.request.AlterarParticipantesOrgaoPublicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.alterarparticipantesorgaopublico.response.AlterarParticipantesOrgaoPublicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarusuariosolirelatorioorgaospublicos.request.ConsultarUsuarioSoliRelatorioOrgaosPublicosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarusuariosolirelatorioorgaospublicos.response.ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharparticipantesorgaopublico.request.DetalharParticipantesOrgaoPublicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharparticipantesorgaopublico.response.DetalharParticipantesOrgaoPublicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirparticipantesorgaopublico.request.ExcluirParticipantesOrgaoPublicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirparticipantesorgaopublico.response.ExcluirParticipantesOrgaoPublicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolirelatorioorgaospublicos.request.ExcluirSoliRelatorioOrgaosPublicosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolirelatorioorgaospublicos.response.ExcluirSoliRelatorioOrgaosPublicosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantesorgaopublico.request.IncluirParticipantesOrgaoPublicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirparticipantesorgaopublico.response.IncluirParticipantesOrgaoPublicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioorgaospublicos.request.IncluirSoliRelatorioOrgaosPublicosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatorioorgaospublicos.response.IncluirSoliRelatorioOrgaosPublicosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarctasvincpartorgaopublico.request.ListarCtasVincPartOrgaoPublicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarctasvincpartorgaopublico.response.ListarCtasVincPartOrgaoPublicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesorgaopublico.request.ListarParticipantesOrgaoPublicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarparticipantesorgaopublico.response.ListarParticipantesOrgaoPublicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioorgaospublicos.request.ListarSoliRelatorioOrgaosPublicosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatorioorgaospublicos.response.ListarSoliRelatorioOrgaosPublicosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.validarcontratopagsalpartorgaopublico.request.ValidarContratoPagSalPartOrgaoPublicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.validarcontratopagsalpartorgaopublico.response.ValidarContratoPagSalPartOrgaoPublicoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterCompTipoServicoFormaLanc
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterClientesOrgaoPublicoServiceImpl implements IManterClientesOrgaoPublicoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}


	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}


	/**
     * Construtor.
     */
    public ManterClientesOrgaoPublicoServiceImpl() {
        
    }
    
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService#listarSoliRelatorioOrgaosPublicos(br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosEntradaDTO)
	 */
	public ListarSoliRelatorioOrgaosPublicosSaidaDTO listarSoliRelatorioOrgaosPublicos(ListarSoliRelatorioOrgaosPublicosEntradaDTO entrada){
		List<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO> listaSaida = new ArrayList<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO>();
		ListarSoliRelatorioOrgaosPublicosRequest request = new ListarSoliRelatorioOrgaosPublicosRequest();
		ListarSoliRelatorioOrgaosPublicosResponse response = new ListarSoliRelatorioOrgaosPublicosResponse();
		
		request.setNumeroOcorrencias(100);
		request.setDtInicioSolicitacao(PgitUtil.verificaStringNula(entrada.getDtInicioSolicitacao()));
		request.setDtFimSolicitacao(PgitUtil.verificaStringNula(entrada.getDtFimSolicitacao()));
		
		response = getFactoryAdapter().getListarSoliRelatorioOrgaosPublicosPDCAdapter().invokeProcess(request);
		
		ListarSoliRelatorioOrgaosPublicosSaidaDTO saida = new ListarSoliRelatorioOrgaosPublicosSaidaDTO();
		saida.setNumeroLinhas(response.getNumeroLinhas());
		
		for(int i=0;i<response.getOcorrenciasCount();i++){
			
			ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO saidaOcorr = new ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO(); 
			saidaOcorr.setCdSituacao(response.getOcorrencias(i).getCdSituacao());
			saidaOcorr.setCdSolicitacaoPagamento(response.getOcorrencias(i).getCdSolicitacaoPagamento());
			saidaOcorr.setCdUsuario(response.getOcorrencias(i).getCdUsuario());
			saidaOcorr.setDsDataSolicitacao(response.getOcorrencias(i).getDsDataSolicitacao());
			saidaOcorr.setDsHoraSolicitacao(response.getOcorrencias(i).getDsHoraSolicitacao());
			saidaOcorr.setDsNomeUsuario(response.getOcorrencias(i).getDsNomeUsuario());
			saidaOcorr.setNrSolicitacao(response.getOcorrencias(i).getNrSolicitacao());
			
			listaSaida.add(saidaOcorr);
		}
		
		saida.setOcorrencias(listaSaida);
		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService#excluirSoliRelatorioOrgaosPublicos(br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirSoliRelatorioOrgaosPublicosEntradaDTO)
	 */
	public ExcluirSoliRelatorioOrgaosPublicosSaidaDTO excluirSoliRelatorioOrgaosPublicos(ExcluirSoliRelatorioOrgaosPublicosEntradaDTO entrada) {
		ExcluirSoliRelatorioOrgaosPublicosRequest request = new ExcluirSoliRelatorioOrgaosPublicosRequest();
		ExcluirSoliRelatorioOrgaosPublicosResponse response = new ExcluirSoliRelatorioOrgaosPublicosResponse();
		
		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada.getCdSolicitacaoPagamento()));
		request.setNrSolicitacao(PgitUtil.verificaIntegerNulo(entrada.getNrSolicitacao()));
		
		response = getFactoryAdapter().getExcluirSoliRelatorioOrgaosPublicosPDCAdapter().invokeProcess(request);
		
		ExcluirSoliRelatorioOrgaosPublicosSaidaDTO saidaDTO = new ExcluirSoliRelatorioOrgaosPublicosSaidaDTO();	
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());		
		
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService#consultarUsuarioSoliRelatorioOrgaosPublicos(br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ConsultarUsuarioSoliRelatorioOrgaosPublicosEntradaDTO)
	 */
	public ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO consultarUsuarioSoliRelatorioOrgaosPublicos(ConsultarUsuarioSoliRelatorioOrgaosPublicosEntradaDTO entrada) {
		ConsultarUsuarioSoliRelatorioOrgaosPublicosRequest request = new ConsultarUsuarioSoliRelatorioOrgaosPublicosRequest();
		ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse response = new ConsultarUsuarioSoliRelatorioOrgaosPublicosResponse();
		
		request.setCdSituacaoVinculacaoConta(PgitUtil.verificaIntegerNulo(entrada.getCdSituacaoVinculacaoConta()));
		request.setNrOcorrencias(PgitUtil.verificaIntegerNulo(entrada.getNrOcorrencias()));
		
		response = getFactoryAdapter().getConsultarUsuarioSoliRelatorioOrgaosPublicosPDCAdapter().invokeProcess(request);
		
		ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO saidaDTO = new ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO();	
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setDsNomeUsuarioInclusao(response.getDsNomeUsuarioInclusao());
		saidaDTO.setDtInclusao(response.getDtInclusao());
		
		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService#incluirSoliRelatorioOrgaosPublicosSaidaDTO()
	 */
	public IncluirSoliRelatorioOrgaosPublicosSaidaDTO incluirSoliRelatorioOrgaosPublicosSaidaDTO() {
		IncluirSoliRelatorioOrgaosPublicosRequest request = new IncluirSoliRelatorioOrgaosPublicosRequest();
		IncluirSoliRelatorioOrgaosPublicosResponse response = new IncluirSoliRelatorioOrgaosPublicosResponse();
		
		
		response = getFactoryAdapter().getIncluirSoliRelatorioOrgaosPublicosPDCAdapter().invokeProcess(request);
		
		IncluirSoliRelatorioOrgaosPublicosSaidaDTO saidaDTO = new IncluirSoliRelatorioOrgaosPublicosSaidaDTO();	
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		return saidaDTO;
	}
	
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService#listarParticipantesOrgaoPublico(br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarParticipantesOrgaoPublicoEntradaDTO)
	 */
	public ListarParticipantesOrgaoPublicoSaidaDTO listarParticipantesOrgaoPublico(ListarParticipantesOrgaoPublicoEntradaDTO entrada) {
		ListarParticipantesOrgaoPublicoRequest request = new ListarParticipantesOrgaoPublicoRequest();
		ListarParticipantesOrgaoPublicoResponse response = new ListarParticipantesOrgaoPublicoResponse();
		List<ListarParticipantesOrgaoOcorrencias> listaSaida = new ArrayList<ListarParticipantesOrgaoOcorrencias>();
		
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		request.setCdSituacaoRegistro(PgitUtil.verificaStringNula(entrada.getCdSituacaoRegistro()));
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNumeroOcorrencias(PgitUtil.verificaIntegerNulo(entrada.getNumeroOcorrencias()));
		
		response = getFactoryAdapter().getListarParticipantesOrgaoPublicoPDCAdapter().invokeProcess(request);
		
		ListarParticipantesOrgaoPublicoSaidaDTO saidaDTO = new ListarParticipantesOrgaoPublicoSaidaDTO();	
		
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setNumeroLinhas(response.getNumeroLinhas());
		
		for(int i=0;i<response.getOcorrenciasCount();i++){
		ListarParticipantesOrgaoOcorrencias  objLista = new ListarParticipantesOrgaoOcorrencias();
		
		objLista.setCdpessoaJuridicaContrato(response.getOcorrencias(i).getCdpessoaJuridicaContrato());
		objLista.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
		objLista.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
		objLista.setCdPessoaJuridicaVinculo(response.getOcorrencias(i).getCdPessoaJuridicaVinculo());
		objLista.setCdTipoContratoVinculo(response.getOcorrencias(i).getCdTipoContratoVinculo());
		objLista.setNrSequenciaContratoVinculo(response.getOcorrencias(i).getNrSequenciaContratoVinculo());
		objLista.setCdTipoVincContrato(response.getOcorrencias(i).getCdTipoVincContrato());
		objLista.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
		objLista.setCdSituacaoRegistro(response.getOcorrencias(i).getCdSituacaoRegistro());
		objLista.setDsSituacaoRegistro(response.getOcorrencias(i).getDsSituacaoRegistro());
		objLista.setCdCorpoCpfCnpj(response.getOcorrencias(i).getCdCorpoCpfCnpj());
		objLista.setCdFilialCnpj(response.getOcorrencias(i).getCdFilialCnpj());
		objLista.setCdControleCPF(response.getOcorrencias(i).getCdControleCPF());
		objLista.setDsNomeRazaoSocial(response.getOcorrencias(i).getDsNomeRazaoSocial());
		objLista.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
		objLista.setDsNomeAgenciaDebito(response.getOcorrencias(i).getDsNomeAgenciaDebito());
		objLista.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
		objLista.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
	    objLista.setCodAgenciaDesc(response.getOcorrencias(i).getCdAgenciaDebito() + " - " + response.getOcorrencias(i).getDsNomeAgenciaDebito());
	    objLista.setContaDigito(response.getOcorrencias(i).getCdContaDebito() + " - " + response.getOcorrencias(i).getCdDigitoContaDebito());
	    objLista.setCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(response.getOcorrencias(i).getCdCorpoCpfCnpj(), response.getOcorrencias(i).getCdFilialCnpj(),response.getOcorrencias(i).getCdControleCPF()));

		listaSaida.add(objLista);
		
	   }
		saidaDTO.setOcorrencias(listaSaida);
	
		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService#detalharParticipanteOrgaoPublico(br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.DetalharParticipantesOrgaoPublicoEntradaDTO)
	 */
	public DetalharParticipantesOrgaoPublicoSaidaDTO detalharParticipanteOrgaoPublico(DetalharParticipantesOrgaoPublicoEntradaDTO entrada) {
		DetalharParticipantesOrgaoPublicoRequest request = new DetalharParticipantesOrgaoPublicoRequest();
		DetalharParticipantesOrgaoPublicoResponse response = new DetalharParticipantesOrgaoPublicoResponse();
		
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdPessoaJuridicaVinculo(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaVinculo()));
		request.setCdTipoContratoVinculo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoVinculo()));
		request.setNrSequenciaContratoVinculo(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoVinculo()));
		request.setCdTipoVincContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoVincContrato()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		
		response = getFactoryAdapter().getDetalharParticipantesOrgaoPublicoPDCAdapter().invokeProcess(request);
		
		DetalharParticipantesOrgaoPublicoSaidaDTO saidaDTO = new DetalharParticipantesOrgaoPublicoSaidaDTO();	
		
	    saidaDTO.setCodMensagem(response.getCodMensagem());
	    saidaDTO.setMensagem(response.getMensagem());
	    saidaDTO.setDtInclusao(response.getDtInclusao());
	    saidaDTO.setHrInclusao(response.getHrInclusao());
	    saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
	    saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
	    saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
	    saidaDTO.setDtManutencao(response.getDtManutencao());
	    saidaDTO.setHrManutencao(response.getHrManutencao());
	    saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
	    saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());
		
	    saidaDTO.setDataHoraInclusao(PgitUtil.concatenarCampos(response.getDtInclusao(), response.getHrInclusao(),"-"));
	    saidaDTO.setDataHoraManut(PgitUtil.concatenarCampos(response.getDtManutencao(), response.getHrManutencao(),"-"));
		
		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService#excluirParticipanteOrgaoPublico(br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirParticipantesOrgaoPublicoEntradaDTO)
	 */
	public ExcluirParticipantesOrgaoPublicoSaidaDTO excluirParticipanteOrgaoPublico(ExcluirParticipantesOrgaoPublicoEntradaDTO entrada) {
		ExcluirParticipantesOrgaoPublicoRequest request = new ExcluirParticipantesOrgaoPublicoRequest();
		ExcluirParticipantesOrgaoPublicoResponse response = new ExcluirParticipantesOrgaoPublicoResponse();
		
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdPessoaJuridicaVinculo(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaVinculo()));
		request.setCdTipoContratoVinculo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoVinculo()));
		request.setNrSequenciaContratoVinculo(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoVinculo()));
		request.setCdTipoVincContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoVincContrato()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		
		response = getFactoryAdapter().getExcluirParticipantesOrgaoPublicoPDCAdapter().invokeProcess(request);
		
		ExcluirParticipantesOrgaoPublicoSaidaDTO saidaDTO = new ExcluirParticipantesOrgaoPublicoSaidaDTO();	
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		
		return saidaDTO;
	}
	

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService#alterarParticipanteOrgaoPublico(br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.AlterarParticipantesOrgaoPublicoEntradaDTO)
	 */
	public AlterarParticipantesOrgaoPublicoSaidaDTO alterarParticipanteOrgaoPublico(AlterarParticipantesOrgaoPublicoEntradaDTO entrada) {
		AlterarParticipantesOrgaoPublicoRequest request = new AlterarParticipantesOrgaoPublicoRequest();
		AlterarParticipantesOrgaoPublicoResponse response = new AlterarParticipantesOrgaoPublicoResponse();
		
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdPessoaJuridicaVinculo(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaVinculo()));
		request.setCdTipoContratoVinculo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoVinculo()));
		request.setNrSequenciaContratoVinculo(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoVinculo()));
		request.setCdTipoVincContrato(PgitUtil.verificaIntegerNulo(entrada.getCdTipoVincContrato()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		request.setCdSituacaoRegistro(PgitUtil.verificaStringNula(entrada.getCdSituacaoRegistro()));

		response = getFactoryAdapter().getAlterarParticipantesOrgaoPublicoPDCAdapter().invokeProcess(request);
		
		AlterarParticipantesOrgaoPublicoSaidaDTO saidaDTO = new AlterarParticipantesOrgaoPublicoSaidaDTO();	
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService#validarParticipanteOrgaoPublico(br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ValidarContratoPagSalPartOrgaoPublicoEntradaDTO)
	 */
	public ValidarContratoPagSalPartOrgaoPublicoSaidaDTO validarParticipanteOrgaoPublico(ValidarContratoPagSalPartOrgaoPublicoEntradaDTO entrada) {
		ValidarContratoPagSalPartOrgaoPublicoRequest request = new ValidarContratoPagSalPartOrgaoPublicoRequest();
		ValidarContratoPagSalPartOrgaoPublicoResponse response = new ValidarContratoPagSalPartOrgaoPublicoResponse();
		
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));

		response = getFactoryAdapter().getValidarContratoPagSalPartOrgaoPublicoPDCAdapter().invokeProcess(request);
		
		ValidarContratoPagSalPartOrgaoPublicoSaidaDTO saidaDTO = new ValidarContratoPagSalPartOrgaoPublicoSaidaDTO();	
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		
		return saidaDTO;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService#listarCtasVincPartOrgaoPublico(br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarCtasVincPartOrgaoPublicoEntradaDTO)
	 */
	public ListarCtasVincPartOrgaoPublicoSaidaDTO listarCtasVincPartOrgaoPublico(ListarCtasVincPartOrgaoPublicoEntradaDTO entrada){
		ListarCtasVincPartOrgaoPublicoRequest request = new ListarCtasVincPartOrgaoPublicoRequest();
		ListarCtasVincPartOrgaoPublicoResponse response = new ListarCtasVincPartOrgaoPublicoResponse();
		List<ListarCtasVincPartOrgaoPublicoOcorrencias> listaSaida = new ArrayList<ListarCtasVincPartOrgaoPublicoOcorrencias>();
		
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setNumeroOcorrencias(PgitUtil.verificaIntegerNulo(entrada.getNumeroOcorrencias()));
		
		response = getFactoryAdapter().getListarCtasVincPartOrgaoPublicoPDCAdapter().invokeProcess(request);
		
		ListarCtasVincPartOrgaoPublicoSaidaDTO saida = new ListarCtasVincPartOrgaoPublicoSaidaDTO();
		saida.setNumeroLinhas(response.getNumeroLinhas());
		saida.setMensagem(response.getMensagem());
		saida.setCodMensagem(response.getCodMensagem());
		
		for(int i=0;i<response.getOcorrenciasCount();i++){
			
			ListarCtasVincPartOrgaoPublicoOcorrencias saidaOcorr = new ListarCtasVincPartOrgaoPublicoOcorrencias();
			saidaOcorr.setCdPessoaJuridicaVinculo(response.getOcorrencias(i).getCdPessoaJuridicaVinculo());
		    saidaOcorr.setCdTipoContratoVinculo(response.getOcorrencias(i).getCdTipoContratoVinculo());
		    saidaOcorr.setNrSequenciaContratoVinculo(response.getOcorrencias(i).getNrSequenciaContratoVinculo());
		    saidaOcorr.setCdPessoa(response.getOcorrencias(i).getCdPessoa());
		    saidaOcorr.setCdCorpoCpfCnpj(response.getOcorrencias(i).getCdCorpoCpfCnpj());
		    saidaOcorr.setCdFilialCnpj(response.getOcorrencias(i).getCdFilialCnpj());
		    saidaOcorr.setCdControleCPF(response.getOcorrencias(i).getCdControleCPF());
		    saidaOcorr.setDsNomeRazaoSocial(response.getOcorrencias(i).getDsNomeRazaoSocial());
		    saidaOcorr.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
		    saidaOcorr.setDsNomeAgenciaDebito(response.getOcorrencias(i).getDsNomeAgenciaDebito());
		    saidaOcorr.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
		    saidaOcorr.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
		    
		    saidaOcorr.setCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(response.getOcorrencias(i).getCdCorpoCpfCnpj(), response.getOcorrencias(i).getCdFilialCnpj(),response.getOcorrencias(i).getCdControleCPF()));
		    saidaOcorr.setCodAgenciaDesc(response.getOcorrencias(i).getCdAgenciaDebito() + " - " + response.getOcorrencias(i).getDsNomeAgenciaDebito());
		    saidaOcorr.setContaDigito(response.getOcorrencias(i).getCdContaDebito() + " - " + response.getOcorrencias(i).getCdDigitoContaDebito());
		    

			listaSaida.add(saidaOcorr);
		}
		
		saida.setOcorrencias(listaSaida);
		
		return saida;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService#incluirParticipanteOrgaoPublico(br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.IncluirParticipantesOrgaoPublicoEntradaDTO)
	 */
	public IncluirParticipantesOrgaoPublicoSaidaDTO incluirParticipanteOrgaoPublico(IncluirParticipantesOrgaoPublicoEntradaDTO entrada) {
		IncluirParticipantesOrgaoPublicoRequest request = new IncluirParticipantesOrgaoPublicoRequest();
		IncluirParticipantesOrgaoPublicoResponse response = new IncluirParticipantesOrgaoPublicoResponse();
		
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdPessoaJuridicaVinculo(PgitUtil.verificaLongNulo(entrada.getCdPessoaJuridicaVinculo()));
		request.setCdTipoContratoVinculo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoVinculo()));
		request.setNrSequenciaContratoVinculo(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoVinculo()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		
		response = getFactoryAdapter().getIncluirParticipantesOrgaoPublicoPDCAdapter().invokeProcess(request);
		
		IncluirParticipantesOrgaoPublicoSaidaDTO saidaDTO = new IncluirParticipantesOrgaoPublicoSaidaDTO();	
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		
		return saidaDTO;
	}

}

